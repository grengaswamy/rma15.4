/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Globalization;
using System.Resources;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

using CCP.ExceptionTypes;

namespace CCP.Common
{
    public static class Conversion
    {
        public enum cmnDataTypes
        {
            cdtString,
            cdtInteger,
            cdtLong,
            cdtDecimal,
            cdtDouble,
            cdtCurrency,
            cdtDate,
            cdtBool,
            cdtByte,
            cdtSingle,
            cdtTime,
            cdtDTTM
        }

        static public object ConvertData(object oInData, cmnDataTypes ToType)
        {
            object vlu;
            object inv;
            object ini;
            object ins;
            string strTemp;

            strTemp = oInData.ToString();

            if (strTemp.Length > 0)
            {
                if (oInData is string)
                    oInData = strTemp.Trim();

                inv = oInData;
                ini = oInData;
                vlu = oInData;
            }
            else
            {
                inv = "";
                ini = 0;
                vlu = "";
            }

            switch (ToType)
            {
                case cmnDataTypes.cdtString:
                    vlu = (string)inv;
                    break;
                case cmnDataTypes.cdtInteger:
                    if (NumericUtils.IsNumeric(ini))
                        if (ini.GetType().ToString() != "System.String")
                            vlu = (int)ini;
                        else
                            vlu = Int32.Parse((string)ini);
                    else
                        vlu = 0;
                    break;
                case cmnDataTypes.cdtLong:
                    if (NumericUtils.IsNumeric(ini))
                        vlu = (long)ini;
                    else
                        vlu = 0;
                    break;
                case cmnDataTypes.cdtDouble:
                    if (NumericUtils.IsNumeric(ini))
                        vlu = (double)ini;
                    else
                        vlu = 0;
                    break;
                case cmnDataTypes.cdtDate:
                    ins = (string)inv;
                    if (StringUtils.InStr(ins.ToString(), "/") == -1)
                    {
                        if (StringUtils.Len(ins.ToString()) >= 8)
                            inv = "@" + StringUtils.Mid(ins.ToString(), 4, 2) + "/" + StringUtils.Mid(ins.ToString(), 6, 2) + StringUtils.Left(ins.ToString(), 4);
                        else if (StringUtils.Len(ins.ToString()) >= 6)
                            inv = "@" + StringUtils.Mid(ins.ToString(), 2, 2) + "/" + StringUtils.Mid(ins.ToString(), 4, 2) + StringUtils.Left(ins.ToString(), 2);
                    }


                    if (DateTimeUtils.IsDate(inv.ToString()))
                    {
                        DateTime dtTemp;
                        dtTemp = DateTime.Parse((string)inv);
                        vlu = dtTemp.ToShortDateString();
                    }
                    break;
                case cmnDataTypes.cdtTime:
                    ins = (string)inv;
                    if (StringUtils.InStr(ins.ToString(), ":") == -1)
                    {
                        if (StringUtils.InStr(ins.ToString(), " ") == -1)
                        {
                            if (StringUtils.Len(ins.ToString()) >= 6)
                                inv = StringUtils.Mid(ins.ToString(), 0, 2) + ":" + StringUtils.Mid(ins.ToString(), 2, 2) + ":" + StringUtils.Mid(ins.ToString(), 4, 2);
                            else if (StringUtils.Len(ins.ToString()) >= 4)
                                inv = StringUtils.Mid(ins.ToString(), 0, 2) + ":" + StringUtils.Mid(ins.ToString(), 2, 2);
                        }
                        else if (StringUtils.InStr(ins.ToString(), " ") >= 6)
                            inv = StringUtils.Mid(ins.ToString(), 0, 2) + ":" + StringUtils.Mid(ins.ToString(), 2, 2) + ":" + StringUtils.Mid(ins.ToString(), 4, 2) + StringUtils.Mid(ins.ToString(), 6);
                        else if (StringUtils.InStr(ins.ToString(), " ") >= 4)
                            inv = StringUtils.Mid(ins.ToString(), 0, 2) + ":" + StringUtils.Mid(ins.ToString(), 2, 2) + " " + StringUtils.Mid(ins.ToString(), 4);
                    }

                    if (DateTimeUtils.IsDate(inv.ToString()))
                    {
                        DateTime dtTemp;
                        dtTemp = DateTime.Parse((string)inv);
                        vlu = (object)dtTemp.ToLongTimeString();
                    }
                    break;
                case cmnDataTypes.cdtDecimal:
                    if (NumericUtils.IsNumeric(ini))
                        vlu = (double)ini;
                    else
                        vlu = 0;
                    break;
                case cmnDataTypes.cdtBool:
                    if (inv is string)
                    {
                        strTemp = inv.ToString();
                        strTemp = strTemp.ToUpper();
                        switch (strTemp)
                        {
                            case "Y":
                            case "YES":
                            case "TRUE":
                            case "ON":
                            case "1":
                                inv = true;
                                break;
                            case "N":
                            case "NO":
                            case "OFF":
                            case "FALSE":
                            case "":
                            case "0":
                            case "F":
                                inv = false;
                                break;
                        }
                    }
                    else if(inv == null)
                        inv = false;
                    else if(NumericUtils.IsNumeric(inv))
                    {
                        if ((int)inv == 0)
                            inv = false;
                        else
                            inv = true;
                    }
                    vlu = (bool)inv;
                    break;
                case cmnDataTypes.cdtByte:
                    vlu = (byte)inv;
                    break;
                case cmnDataTypes.cdtSingle:
                    vlu = (Single)ini;
                    break;
            }
            return vlu;  
        }

        static public string EntityTableIdToOrgTableName(int tableId)
        {
            switch (tableId)
            {
                case 1005:
                    return "CLIENT";
                case 1006:
                    return "COMPANY";
                case 1007:
                    return "OPERATION";
                case 1008:
                    return "REGION";
                case 1009:
                    return "DIVISION";
                case 1010:
                    return "LOCATION";
                case 1011:
                    return "FACILITY";
                case 1012:
                    return "DEPARTMENT";
                default:
                    return "";
            }
        }
        public static string GetDate(string s)
        {
            DateTime dttm = DateTime.MinValue;
            bool boolErr = false;

            if (s == null || s == "")
                return "";
            try
            {
                //Get a Date if this is a valid representation for the current culture.
                dttm = System.DateTime.Parse(s);
            }
            catch (FormatException)
            {
                boolErr = true;
            }

            if (boolErr) // Try is passed date in yyyymmdd format already ?
                if (s.Length == 8)
                    try { dttm = ToDate(s); }
                    catch { }

            if (dttm != DateTime.MinValue)
                return dttm.ToString("yyyyMMdd");

            //All attempts at conversion failed return blank.
            return "";

        }

        public static string GetDateTime(string s)
        {
            DateTime dttm = DateTime.MinValue;
            bool boolErr = false;
            if (s == null || s == "")
                return "";

            //Get a DateTime if this is a valid representation for the current culture.
            try { dttm = System.DateTime.Parse(s); }
            catch (FormatException) { boolErr = true; }

            if (boolErr) // Try is passed date in yyyymmdd format already ?
                if (s.Length == 8 || s.Length == 14)
                    try { dttm = ToDate(s); }
                    catch { }


            if (dttm != DateTime.MinValue)
                return dttm.ToString("yyyyMMddHHmmss");

            //All attempts at conversion failed return blank.
            return "";
        }

        public static DateTime ToDate(string s)
        {
            if (s == "" || s == null)
                return DateTime.MinValue;
            if (s.Length == 8)
                return new DateTime(int.Parse(s.Substring(0, 4)), int.Parse(s.Substring(4, 2)), int.Parse(s.Substring(6, 2)));
            if (s.Length == 14)
                return new DateTime(int.Parse(s.Substring(0, 4)), int.Parse(s.Substring(4, 2)), int.Parse(s.Substring(6, 2)), int.Parse(s.Substring(8, 2)), int.Parse(s.Substring(10, 2)), int.Parse(s.Substring(12, 2)));
            return DateTime.MinValue;
        }

        //Takes a pure numeric string 1234567 or 1234567890 or 1234567890999...
        // Returns a formatted phone number 123-4567 or (123) 456-7890 or (123) 456-7890 Ext:999
        public static string ToPhoneNumber(string s)
        {
            //Nothing here to work with.
            if (s == "" || s == null)
                return "";

            //Too Short
            if (s.Length < 7)
                return s;

            //Already contains formatting characters.
            Regex reg = new Regex("[^1-9]");
            if (reg.Matches(s).Count > 0)
                return s;

            int val = Int32.Parse(s);
            int val1 = 0;
            string s1 = "";

            if (val == 0) //Non-Numeric string return empty.
                return "";

            if (s.Length == 7)
                return val.ToString("(   ) 000-0000");

            if (s.Length == 10)
                return val.ToString("(000) 000-0000");

            s1 = s.Substring(1, 10);
            val1 = Int32.Parse(s1);
            s = s.Substring(11);

            return val1.ToString("(000) 000-0000 Ext:") + s;
        }
        public static string GetTime(string s)
        {
            DateTime dttm = DateTime.MinValue;
            bool boolErr = false;

            if (s == null || s == "")
                return "";

            //Get a DateTime if this is a valid representation for the current culture.
            try { dttm = System.DateTime.Parse(s); }
            catch (FormatException) { boolErr = true; }

            if (boolErr) // Try is passed date in yyyymmdd format already ?
                if (s.Length == 14)
                {
                    try { dttm = ToDate(s); }
                    catch { }
                }
                else if (s.Length == 6)
                {
                    dttm = DateTime.Now;  //set to the date of this instance, with the time part set to 00:00:00.
                    dttm.AddHours(int.Parse(s.Substring(0, 2)));
                    dttm.AddMinutes(int.Parse(s.Substring(2, 2)));
                    dttm.AddSeconds(int.Parse(s.Substring(4, 2)));
                }

            if (dttm != DateTime.MinValue)
                return dttm.ToString("HHmmss");

            //All attempts at conversion failed return blank.
            return "";
        }

        // <summary>
        // Returns time in HHMM00 and takes time in HH:MM
        // </summary>
        // <param name="s">Format HH:MM</param>
        // <returns>HHMM00</returns>
        public static string GetTimeHHMM00(string s)
        {
            string sTemp = "000000";

            if (s == null || s == "")
                return sTemp;

            try
            {
                if (s.Length == 5)
                {
                    sTemp = s.Substring(0, 2);
                    sTemp += s.Substring(3, 2);
                    sTemp += "00";
                }
            }
            catch
            {
                return sTemp;
            }

            return sTemp;
        }

        public static string ToDbDateTime(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "";
            return (date.Year.ToString("0000") + date.Month.ToString("00") + date.Day.ToString("00") + date.Hour.ToString("00") + date.Minute.ToString("00") + date.Second.ToString("00"));
        }

        // <summary>Advanced Claims.Common.Conversion.ToDbTime.</summary>
        // <param name="date">DateTime to be converted into Advanced Claims Database time format "HHMMSS."</param>
        // <returns>String value for DateTime date in "HHMMSS" format.</returns>
        // <remarks>Returns "000000" if DateTime is equal to DateTime.MinValue.
        public static string ToDbTime(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "";
            return (date.Hour.ToString("00") + date.Minute.ToString("00") + date.Second.ToString("00"));
        }
        // <summary>Advanced Claims.Common.Conversion.ToDbDate.</summary>
        // <param name="date">DateTime to be converted into Advanced Claims Database date format "YYYYMMDD."</param>
        // <returns>String value for DateTime date in "YYYYMMDD" format.</returns>
        // <remarks>Returns "" if DateTime is equal to DateTime.MinValue.</remarks>
        public static string ToDbDate(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "";
            return (date.Year.ToString("0000") + date.Month.ToString("00") + date.Day.ToString("00"));
        }


        // <summary>
        // This overload of Advanced Claims.Common.Conversion.GetByteArr breaks an uint Array into an equivalent byte Array.</summary>
        // <param name="uintArr">Unsigned Integer array  for which to provide an equivalent byte Array.</param>
        // <returns>byte[] representing uintArr</returns>
        // <remarks>It is perfectly acceptable to us in this implementation to have the value "roll negative" during conversion.</remarks>
        // <example>This function might be usefull for encryption.</example>
        public static byte[] GetByteArr(uint[] uintArr)
        {
            byte[] byteArr = new byte[4 * uintArr.Length];
            ushort[] wordArr = new ushort[2];
            int iSrc;
            int i = 0;
            foreach (uint Item in uintArr)
            {
                //It is perfectly acceptable to us
                // to have the value "roll negative" during conversion,
                unchecked { iSrc = (int)(Item); }

                GetHILOWord(iSrc, out wordArr[0], out wordArr[1]);
                GetHILOByte(wordArr[0], out byteArr[(i * 4) + 0], out byteArr[(i * 4) + 1]);
                GetHILOByte(wordArr[1], out byteArr[(i * 4) + 2], out byteArr[(i * 4) + 3]);
                i++;
            }
            return byteArr;
        }

        // <summary>This overload of Advanced Claims.Common.Conversion.GetByteArr 
        // breaks an uint Array into an equivalent byte Array.</summary>
        // <param name="iSrc">An integer to be converted to a byte[4].</param>
        // <returns>byte[] representing int iSrc</returns>
        // <example>This function might be usefull for encryption.</example>
        public static byte[] GetByteArr(int iSrc)
        {
            byte[] byteArr = new byte[4];
            ushort[] wordArr = new ushort[2];

            GetHILOWord(iSrc, out wordArr[0], out wordArr[1]);
            GetHILOByte(wordArr[0], out byteArr[0], out byteArr[1]);
            GetHILOByte(wordArr[1], out byteArr[2], out byteArr[3]);
            return byteArr;
        }
        public static int StringToByteArr(string sStr, ref byte[] byteArr, int startIndex)
        {
            int l, f;
            l = sStr.Length;
            for (f = startIndex; f <= (startIndex + l - 1); f++)
            //for (f = startIndex; f < (startIndex + l); f++)
            {
                //byteArr[f] = Conversion.GetAscii((sStr.Substring(f - startIndex + 1, 1)));
                byteArr[f] = Conversion.GetAscii((sStr.Substring(f - startIndex, 1)));
            }
            return f;
        }


        // <summary>This overload of Advanced Claims.Common.Conversion.GetByteArr breaks 
        // an uint Array into an equivalent byte Array.</summary>
        // <param name="s">A string  to be converted to a byte[].</param>
        // <returns>byte[] representing string s.</returns>
        // <remarks>Uses the ASCII.GetBytes framework routine but checks for an empty 
        // or null string before making the call returning {0} in that case.</remarks>
        // <example>This function might be useful for encryption.</example>
        public static byte[] GetByteArr(string s)
        {
            Byte[] ret = { 0 };
            if (s == "" || s == null)
                return ret;
            return System.Text.Encoding.ASCII.GetBytes(s);
        }

        // <summary>
        // Advanced Claims.Common.Conversion.GetHILOWord.</summary>
        // <param name="Src">Integer containing the dword size value to split.</param>
        // <param name="iLO">Unsigned short containing the word size value from the lower position of Src.</param>
        // <param name="iHI">Unsigned short containing the word size value from the upper position of Src.</param>
        // <example>This function might be useful for encryption.</example>
        public static void GetHILOWord(int Src, out ushort iLO, out ushort iHI)
        {
            unchecked
            {
                iLO = (ushort)(Src & 0x0000FFFF);
                iHI = (ushort)((Src >> 16) & 0x0000FFFF);
            }
        }

        // <summary>
        // Advanced Claims.Common.Conversion.GetHILOByte.</summary>
        // <param name="Src">Word sized value to be split into a high and low byte.</param>
        // <param name="iLO">Byte sized value to contain the lower byte of Src.</param>
        // <param name="iHI">Byte sized value to contain the upper byte of Src.</param>
        // <example>This function might be useful for encryption.</example>
        public static void GetHILOByte(ushort Src, out byte iLO, out byte iHI)
        {
            unchecked
            {
                iLO = (byte)(Src & 0x00FF);
                iHI = (byte)((Src >> 8) & 0x00FF);
            }
        }
        //NOTE: This is the intentionally hacked version that only returns 
        // the first ascii character code of the string "s".

        // <summary>
        // Deprecated Advanced Claims.Common.Conversion.GetAscii.</summary>
        // <param name="s"></param>
        // <returns>Not specified.</returns>
        // <remarks>Deprecated - see source code..</remarks>
        // NOTE: Not public knowledge but for backward compatibility of our encryption\crc hashes
        //  we must keep this intentionally hacked version that only returns 
        // the first ascii character code of the string "s".
        public static byte GetAscii(string s)
        {
            if (s == "" || s == null)
                return 0;
            return System.Text.Encoding.ASCII.GetBytes(s.ToCharArray(0, 1), 0, 1)[0];
        }

        // <summary>
        // Advanced Claims.Common.Conversion.InsertTransparencyChars 
        // turns 'buf' containing 'real' data into a hexadecimal string suitable for database
        // storage. Implemented because storing NUL characters (and possibly other binary data)
        // in a database creates problems.</summary>
        // <param name="buf">Byte array of input data to be sanitized.</param>
        // <param name="len">Integer number of bytes to sanitize.</param>
        // <returns>String sanitized version of buf.</returns>
        public static string InsertTransparencyChars(byte[] buf, int len)
        {
            string sWork = "";

            for (int i = 0; i < len; i++)
                sWork += String.Format("{0:x2}", buf[i]);

            return sWork;
        }


        // <summary>
        // Advanced Claims.Common.Conversion.RemoveTransparencyChars "unsanitizes" a string of 
        // two character hex values converting each pair into a an ASCII character and 
        // returning the final byte array of converted ASCII characters.  Precisely reverses changes made by 
        // Advanced Claims.Common.Conversion.InsertTransparencyChars</summary>
        // <param name="szStr">Sanitized string value to be "unsanitized."</param>
        // <returns>Unsanitized byte[]</returns>
        public static byte[] RemoveTransparencyChars(string szStr)
        {
            byte[] sArr = new Byte[szStr.Length / 2];

            int length = szStr.Length;
            for (int i = 0; i < length; i += 2)
            {
                sArr[i / 2] = Byte.Parse(szStr.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
            }

            return sArr;
        }


        // <summary>		
        // This method will convert a string to a long value 		
        // </summary>		
        // <param name="p_sValue">The string value that will be converted to a long value</param>												
        // <returns>A long value</returns>
        public static long ConvertStrToLong(string p_sValue)
        {
            long lRetVal = 0;
            try
            {

                if ((p_sValue == null) || (p_sValue == ""))
                {
                    return lRetVal;
                }
                else
                {
                    lRetVal = Convert.ToInt64(p_sValue);
                }
            }
            catch { return 0; }
            return lRetVal;
        }

        // <summary>		
        // This method will convert the passed object to a string value
        // </summary>		
        // <param name="p_obj">The object that will be converted to a string value</param>												
        // <returns>A string value</returns>
        public static string ConvertObjToStr(object p_obj)
        {
            string sRetValue = "";
            try
            {
                if (p_obj != null)
                    sRetValue = p_obj.ToString();
            }
            catch { return ""; }
            return sRetValue;
        }
        // <summary>		
        // This method will convert a string to an integer value 		
        // </summary>		
        // <param name="p_sValue">The string value that will be converted to an integer value</param>												
        // <returns>An integer value</returns>
        public static int ConvertStrToInteger(string p_sValue)
        {
            int iRetVal = 0;
            try
            {

                if ((p_sValue == null) || (p_sValue == ""))
                {
                    return iRetVal;
                }
                else if (p_sValue.ToUpper() == "TRUE")//BSB Hack to support legacy habit of hiding booleans as integers.
                    return -1;
                else
                {
                    iRetVal = Convert.ToInt32(p_sValue);
                }
            }
            catch { return 0; }
            return iRetVal;
        }

        // <summary>		
        // This method will convert a valid date string in the specified date format		
        // </summary>		
        // <param name="p_sDataValue">Valid date string that will be formatted</param>	
        // <param name="p_sFormat">The format in which the date string will be converted</param>	
        // <returns>It will return date string in the specified format</returns>
        public static string GetDBDateFormat(string p_sDataValue, string p_sFormat)
        {
            string sReturnString = "";
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if ((p_sDataValue == null) || (p_sDataValue.Trim() == ""))
                    return "";
                else if (p_sDataValue.Trim() != "")
                {

                    datDataValue = new DateTime(int.Parse(p_sDataValue.Substring(0, 4)), int.Parse(p_sDataValue.Substring(4, 2)), int.Parse(p_sDataValue.Substring(6, 2)));
                    sReturnString = datDataValue.ToString(p_sFormat);
                }
            }
            catch { return ""; }
            return sReturnString;
        }
        // <summary>		
        // This method will return a valid date and time in the specified format		
        // </summary>		
        // <param name="p_sDataValue">A valid date time string that will be formatted</param>	
        // <param name="p_sDateFormat">The format in which the date part will be formatted</param>	
        // <param name="p_sTimeFormat">The format in which the time part will be formatted</param>	
        // <returns>It returns date time string the specified format</returns>
        public static string GetDBDTTMFormat(string p_sDataValue, string p_sDateFormat, string p_sTimeFormat)
        {
            string sReturnString = "";
            string sDatePart = "";
            string sTimePart = "";
            string sTempDataValue = "";
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if ((p_sDataValue == null) || (p_sDataValue.Trim() == ""))
                    return "";
                else if (p_sDataValue.Trim() != "")
                {
                    sTempDataValue = p_sDataValue;
                    sTempDataValue = sTempDataValue.Substring(4, 2) + "-" + sTempDataValue.Substring(6, 2) + "-" + sTempDataValue.Substring(0, 4);
                    datDataValue = System.DateTime.Parse(sTempDataValue);
                    sDatePart = datDataValue.ToString(p_sDateFormat);

                    sTempDataValue = p_sDataValue;
                    sTempDataValue = sTempDataValue.Substring(8, 2) + ":" + sTempDataValue.Substring(10, 2) + ":" + sTempDataValue.Substring(12, 2);
                    datDataValue = System.DateTime.Parse(sTempDataValue);
                    sTimePart = datDataValue.ToString(p_sTimeFormat);
                    sReturnString = sDatePart + " " + sTimePart;
                }
            }
            catch { return ""; }
            return sReturnString;
        }
        // <summary>		
        // This method will return a valid time in the specified format		
        // </summary>		
        // <param name="p_sDataValue">A valid date time string that will be formatted</param>			
        // <param name="p_sFormat">The format in which the time part will be converted</param>	
        // <returns>It will return time in the specified format</returns>
        public static string GetDBTimeFormat(string p_sDataValue, string p_sFormat)
        {
            string sReturnString = "";
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if ((p_sDataValue == null) || (p_sDataValue.Trim() == ""))
                    return "";
                else if (p_sDataValue.Trim() != "")
                {
                    p_sDataValue = p_sDataValue.Substring(0, 2) + ":" + p_sDataValue.Substring(2, 2) + ":" + p_sDataValue.Substring(4, 2);
                    datDataValue = System.DateTime.Parse(p_sDataValue);
                    sReturnString = datDataValue.ToString(p_sFormat);
                }
            }
            catch { return ""; }
            return sReturnString;
        }
        // <summary>		
        // This method returns a value in the percent format		
        // </summary>		
        // <param name="p_dblValue">Value that has to be converted in percent format</param>					
        // <returns>It returns value in the percent format</returns>
        public static string GetPercentFormat(double p_dblValue)
        {
            string sReturnValue = "";
            try
            {
                sReturnValue = string.Format("{0:P}", p_dblValue);
            }
            catch { return ""; }
            return sReturnValue;
        }

        // <summary>
        // Convert string into double
        // </summary>
        // <param name="p_sValue">Input string value</param>
        // <returns>Double value</returns>
        public static double ConvertStrToDouble(string p_sValue)
        {
            double dblRetVal = 0;
            string str = p_sValue;
            try
            {

                if ((p_sValue == null) || (p_sValue == ""))
                {
                    return dblRetVal;
                }
                else
                {
                    str = p_sValue.Replace(",", "").Replace("$", "");
                    dblRetVal = Convert.ToDouble(str);
                }
            }
            catch
            {
                return (0);
            }
            return (dblRetVal);
        }

        #region Converts bool to int
        // <summary>
        // Converts boolean value to integer
        // </summary>
        // <param name="p_bVal">boolean value to convert to integer</param>
        // <returns>Converted integer value</returns>
        public static int ConvertBoolToInt(bool p_bVal)
        {
            int iRetVal = 0;
            try
            {
                if (!(p_bVal.ToString().Equals("")))
                {
                    iRetVal = Convert.ToInt32(p_bVal);
                }
            }
            catch (Exception p_objException)
            {
                throw new ACAppException(Localization.getString("Conversion.ConvertBoolToInt.GeneralError"), p_objException);
            }
            return iRetVal;
        }
        #endregion

        #region Convert long to bool
        // <summary>
        // Converts long value To boolean
        // </summary>
        // <param name="p_lValue">long value to convert</param>
        // <returns>Converted boolean value</returns>
        public static bool ConvertLongToBool(long p_lValue)
        {
            try
            {
                if ((p_lValue.ToString().Equals("")))
                {
                    return false;
                }
            }
            catch (Exception p_objException)
            {
                throw new ACAppException(Localization.getString("Conversion.ConvertLongToBool.GeneralError"), p_objException);
            }
            return Convert.ToBoolean(p_lValue);
        }
        #endregion

        #region Covert String to UInt
        // <summary>
        // Converts passed String value  to unsigned integer value
        // </summary>
        // <param name="p_sValue">string value to convert</param>
        // <returns>Converted unsigned integer value</returns>
        public static UInt64 ConvertStringToUInt(string p_sValue)
        {
            UInt64 uRetValue = 0;
            try
            {
                if (p_sValue == null)
                {
                    return 0;
                }
                else if ((p_sValue.Equals("")))
                {
                    return 0;
                }
                else
                {
                    uRetValue = Convert.ToUInt64(p_sValue);
                }
            }
            catch (Exception p_objException)
            {
                throw new ACAppException(Localization.getString("Conversion.ConvertStringToUInt.GeneralError"), p_objException);
            }
            return uRetValue;
        }
        #endregion
        #region Convert object to Integer
        // <summary>
        // Convert passed object to integer value
        // </summary>
        // <param name="p_objValue">object to convert</param>
        // <returns>Converted integer value</returns>
        public static Int32 ConvertObjToInt(object p_objValue)
        {
            try
            {
                if (p_objValue == null || p_objValue is System.DBNull || p_objValue.ToString() == "")
                {
                    return 0;
                }
            }
            catch (Exception p_objException)
            {
                throw new ACAppException(Localization.getString("Conversion.ConvertObjToInt.GeneralError"), p_objException);
            }
            return Convert.ToInt32(p_objValue);
        }
        #endregion
        // <summary>
        // Converts Null or DBNull object to Int64
        // </summary>
        // <param name="p_ObjValue">object to convert</param>
        // <returns>Converted Int64 value</returns>
        public static Int64 ConvertObjToInt64(object p_ObjValue)
        {
            try
            {
                if (p_ObjValue == null || p_ObjValue is System.DBNull)
                {
                    return 0;
                }
            }
            catch (Exception p_objException)
            {
                throw new ACAppException(Localization.getString("Conversion.ConvertObjToInt64.GeneralError"), p_objException);
            }
            return Convert.ToInt64(p_ObjValue);
        }

        // <summary>Gets OrgHierarchy Level in Long</summary>
        // <param name="p_sLevel">Org Hierarchy Level string</param>	
        // <returns>Org Hierarchy Level Integer</returns>
        static public int GetOrgHierarchyLevel(string p_sLevel)
        {
            switch (p_sLevel.ToUpper())
            {
                case "CLIENT":
                    return 1005;
                case "COMPANY":
                    return 1006;
                case "OPERATION":
                    return 1007;
                case "REGION":
                    return 1008;
                case "DIVISION":
                    return 1009;
                case "LOCATION":
                    return 1010;
                case "FACILITY":
                    return 1011;
                case "DEPARTMENT":
                    return 1012;
                default:
                    return 1005;
            }
        }

        // <summary>
        // This method will take in the time string (in the format HH24MMSS).
        // It will return the time in AM/PM corresponding to that time string.
        // </summary>
        // <param name="p_sTimeString">Time string (in HH24MMSS format)</param>
        // <example>
        //		Returns 12:00 AM for 120000
        //		Returns 12:00 AM for 000000
        //		Returns 5:00  PM  for 170000
        // </example>
        // <returns>Time string (in AM/PM format)</returns>
        static public string GetTimeAMPM(string p_sTimeString)
        {
            int iTime = 0;
            string sReturnTimeString = "";
            try
            {
                if ((p_sTimeString == null) || (p_sTimeString.Trim() == ""))
                    return "";

                iTime = ConvertStrToInteger(p_sTimeString.Substring(0, 2));
                if (iTime >= 12)
                {
                    iTime = iTime - 12;
                    if (iTime == 0)
                        sReturnTimeString = "00" + ":" + p_sTimeString.Substring(2, 2) + " PM";
                    else
                        sReturnTimeString = iTime.ToString() + ":" + p_sTimeString.Substring(2, 2) + " PM";
                }
                else
                {
                    if (p_sTimeString.Substring(0, 2) == "00")
                        p_sTimeString = "12" + p_sTimeString.Substring(2, 2);

                    sReturnTimeString = p_sTimeString.Substring(0, 2) + ":"
                        + p_sTimeString.Substring(p_sTimeString.Length - 4, 2) + " AM";
                }
            }
            catch
            {
                return "";
            }
            return sReturnTimeString;
        }

        // <summary>
        // Convert string value to boolean value.
        // </summary>
        // <param name="p_sValue">Input string to convert to boolean value</param>
        // <returns>Converted boolean value </returns>
        public static bool ConvertStrToBool(string p_sValue)
        {
            int iValue = 0;
            if (p_sValue == "" || p_sValue == null)
                return (false);
            try
            {
                if (Common.NumericUtils.IsNumeric(p_sValue))
                {
                    iValue = Common.Conversion.ConvertStrToInteger(p_sValue);
                    if (iValue == 0)
                        return (false);
                    else
                        return (true);
                }
                else
                {
                    if (p_sValue.ToLower() == "true")
                        return true;
                    else
                        return (false);
                }
            }
            catch
            {
                return (false);
            }
        }

        // <summary>
        // Returns English-like sentence string equivalent To the input integer and decimal part of the money.
        // Example : 
        // 1234.89 will be given input by assigning 1234 as Integer part, and 89 as decimal part.
        // The return string will be "One Thousand Two Hundred Thirty Four and 89/100 Dollars".
        // </summary>
        // <param name="p_iIntgerPart">Integer Part</param>
        // <param name="p_iDecimalPart">Decimal Part</param>
        // <returns>Returns the English-like sentence string.</returns>
        public static string ConvertToMoneyString(int p_iIntgerPart, int p_iDecimalPart)
        {
            string[] arrUnits = {
									"Zero", "One", "Two", "Three", "Four",
									"Five", "Six", "Seven", "Eight", "Nine",
									"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
									"Fifteen", "Sixteen", "Seventeen", "Eighteen",
									"Nineteen"
								};

            string[] arrTens = {
								   "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
								   "Seventy", "Eighty", "Ninety"
							   };

            bool bNegative = false;
            int iTemp = 0;
            string sReturnValue = "";

            try
            {
                if (p_iIntgerPart < 0)
                {
                    p_iIntgerPart *= -1;
                    p_iDecimalPart *= -1;
                    bNegative = true;
                    sReturnValue = "(";
                }

                iTemp = p_iIntgerPart / 1000000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Billion", ref sReturnValue, arrUnits, arrTens);
                    p_iIntgerPart = p_iIntgerPart % 1000000000;
                }

                iTemp = p_iIntgerPart / 1000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Million", ref sReturnValue, arrUnits, arrTens);
                    p_iIntgerPart = p_iIntgerPart % 1000000;
                }

                iTemp = p_iIntgerPart / 1000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Thousand", ref sReturnValue, arrUnits, arrTens);
                    p_iIntgerPart = p_iIntgerPart % 1000;
                }

                CreateMoneyGroup(p_iIntgerPart, "", ref sReturnValue, arrUnits, arrTens);

                if (sReturnValue == "")
                    sReturnValue = arrUnits[0] + " ";

                sReturnValue += "and " + string.Format("{0:00}", p_iDecimalPart) + "/100";
                sReturnValue += " Dollars";

                if (bNegative)
                    sReturnValue += " )";
            }
            catch (ACAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new ACAppException(Localization.getString("Conversion.ConvertToMoneyString.Error"), p_objEx);
            }
            return (sReturnValue);
        }

        // <summary>
        // Create money groups for the given scale.
        // </summary>
        // <param name="p_iIntegerPart">Integer Part</param>
        // <param name="p_sScale">Scale Value, Possible values : "Billion" , "Million" , "Thousand" and "" </param>
        // <param name="p_sValue">Reference string in which group is need to add</param>
        // <param name="p_arrUnits">Units Array</param>
        // <param name="p_arrTens">Tens Array</param>
        private static void CreateMoneyGroup(int p_iIntegerPart, string p_sScale, ref string p_sValue, string[] p_arrUnits, string[] p_arrTens)
        {
            try
            {
                if (p_sValue != "")
                    p_sValue += " ";

                if (p_iIntegerPart >= 100)
                {
                    p_sValue += p_arrUnits[p_iIntegerPart / 100] + " Hundred ";
                    p_iIntegerPart %= 100;
                }

                if (p_iIntegerPart >= 20)
                {
                    p_sValue += p_arrTens[(p_iIntegerPart - 20) / 10];
                    if ((p_iIntegerPart %= 10) != 0)
                        p_sValue += "-" + p_arrUnits[p_iIntegerPart] + " ";
                    else
                        p_sValue += " ";
                }
                else
                {
                    if (p_iIntegerPart != 0)
                    {
                        p_sValue += p_arrUnits[p_iIntegerPart] + " ";
                    }
                }

                p_sValue += p_sScale;
            }
            catch (Exception p_objEx)
            {
                throw new ACAppException(Localization.getString("Conversion.CreateMoneyGroup.Error"), p_objEx);
            }
        }


        #region Converts Object to bool
        // <summary>
        // Converts Object value to bool
        // </summary>
        // <param name="p_objVal">Object to convert to bool</param>
        // <returns>Converted bool value</returns>
        public static bool ConvertObjToBool(object p_objVal)
        {
            bool bRetVal = false;

            //BSB Fixes for this conversion.
            if (p_objVal is bool)
                return (bool)p_objVal;

            if (p_objVal is string)
                p_objVal = (p_objVal as string).ToLower().Replace("true", "1").Replace("false", "0");

            try
            {
                bRetVal = ConvertLongToBool(ConvertStrToLong(ConvertObjToStr(p_objVal)));
            }
            catch (Exception p_objException)
            {
                throw new ACAppException(Localization.getString("Conversion.ConvertObjToBool.GeneralError"), p_objException);
            }
            return bRetVal;
        }
        #endregion

        #region Converts Object to Double
        // <summary>
        // Converts Object value to Double
        // </summary>
        // <param name="p_objVal">Object to convert to Double</param>
        // <returns>Converted Double value</returns>
        public static Double ConvertObjToDouble(object p_objVal)
        {
            double dblRetVal = 0;
            try
            {
                //Changes made by Neelima
                if (p_objVal == null || p_objVal is System.DBNull)
                    dblRetVal = 0;
                else
                    dblRetVal = Convert.ToDouble(p_objVal);
            }
            catch (Exception p_objException)
            {
                throw new ACAppException(Localization.getString("Conversion.ConvertObjToDouble.GeneralError"), p_objException);
            }
            return dblRetVal;
        }
        #endregion

    } //End Class Conversion
} //End Namespace
