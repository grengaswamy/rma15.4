using System;
using CCP.Db;
using CCP.ExceptionTypes;
using System.Collections;
using System.Diagnostics;
using CCP.Common;
using System.Text.RegularExpressions;
using System.Text;

namespace CCP.Common
{
	#region Class LocalCache Implementation
	/// <summary>
	/// This component of Common was broken out of CCP.Common to avoid a circular
	/// dependency with the Database layer code which this class consumes.
	/// This class provides a simple local in memory cache of previously requested database
	/// info (codes, etc.)
	/// </summary>
	public class LocalCache : IDisposable
	{

		//BSB Hack again for performance - we are looking up 
		// far too many "new" codes with each DataModel request.  Therefore,
		// we will cache them statically (across page requests) and include the connection string in 
		// the key to ensure that you only get the ones back from the correct
		// database even though many different databases could be in use 
		// simultaneously on a single server.
		protected class CacheTable
		{
			private static Hashtable m_StaticCache = new Hashtable(50);
			private string m_sConnectionString;
			internal CacheTable(string sConnectionString){m_sConnectionString = sConnectionString;}
			public object this[string sKey]
			{
				get{return m_StaticCache[String.Format("{0}_{1}",sKey,m_sConnectionString)];}
				set{m_StaticCache[String.Format("{0}_{1}",sKey,m_sConnectionString)]=value;}
			}
			public bool ContainsKey(object sKey)
			{
				return m_StaticCache.ContainsKey(String.Format("{0}_{1}",sKey,m_sConnectionString));
			}
			public void Add(object sKey,object objValue)
			{
				//Not critical if collection already contains this memeber -
				//just means we lost a race condition but the content data will be the same so who cares.
				try{m_StaticCache.Add(String.Format("{0}_{1}",sKey,m_sConnectionString),objValue);}
				catch{};
			}
		}
	
		protected CacheTable m_Cache=null;
		protected  DbConnection m_DbConnLookup=null;

		public  LocalCache(string connectionString)
		{
			m_DbConnLookup = DbFactory.GetDbConnection(connectionString);
			m_DbConnLookup.Open(); 
			m_Cache= new  CacheTable(m_DbConnLookup.ConnectionString);
		}
		public void Dispose()
		{
			if(m_DbConnLookup!=null)
				if(m_DbConnLookup.State != System.Data.ConnectionState.Closed)
					m_DbConnLookup.Close();
			m_DbConnLookup = null;
		}
		public void Clear(){m_Cache = new CacheTable(m_DbConnLookup.ConnectionString);}

		public int GetCodeId(string shortCode, string tableName)
		{
			string sKey = String.Format("CID_{0}_{1}",shortCode,tableName);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(int) m_Cache[sKey];
				else
				{
					int codeId = 0;
					string SQL = String.Format(@"SELECT CODE_ID FROM CODES,GLOSSARY 
																				WHERE GLOSSARY.SYSTEM_TABLE_NAME='{0}' AND 
																				GLOSSARY.TABLE_ID=CODES.TABLE_ID AND 
																				CODES.SHORT_CODE='{1}'",tableName,shortCode);
					codeId =  m_DbConnLookup.ExecuteInt(SQL);
					m_Cache.Add(sKey,codeId);
					return codeId;
				}
			}
			catch(Exception e){throw new DataModelException(Globalization.GetString("LocalCache.GetCodeId.Exception"),e);}
		}

		public int GetTableId(string tableName)
		{
			string sKey = String.Format("TID_{0}",tableName);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(int) m_Cache[sKey];
				else
				{
					int tableId = 0;
					string SQL = String.Format(@"SELECT TABLE_ID FROM GLOSSARY 
					WHERE SYSTEM_TABLE_NAME ='{0}'" ,tableName);
					tableId =  m_DbConnLookup.ExecuteInt(SQL);
					m_Cache.Add(sKey,tableId);
					return tableId;
				}
			}
			catch(Exception e){throw new DataModelException(Globalization.GetString("LocalCache.GetTableId.Exception"),e);}
		}
		public string GetShortCode(int codeId)
		{
			string sKey = String.Format("CID_{0}",codeId);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(string) m_Cache[sKey];
				else
				{
					string shortCode ="";
					string SQL = String.Format(@"SELECT SHORT_CODE FROM CODES
																				WHERE CODE_ID={0}" ,codeId);
					shortCode =  (string) m_DbConnLookup.ExecuteString(SQL);
					m_Cache.Add(sKey,shortCode);
					return shortCode;
				}
			}
			catch(Exception e){throw new DataModelException(Globalization.GetString("LocalCache.GetShortCode.Exception"),e);}
		}

		public string GetStateCode(int stateId)
		{
			string sKey = String.Format("SID_{0}",stateId);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(string) m_Cache[sKey];
				else
				{
					string shortCode ="";
					string SQL = String.Format(@"SELECT STATE_ID FROM STATES
																				WHERE STATE_ROW_ID={0}" ,stateId);
					shortCode =  m_DbConnLookup.ExecuteString(SQL);
					m_Cache.Add(sKey,shortCode);
					return shortCode;
				}
			}
			catch(Exception e){throw new DataModelException(Globalization.GetString("LocalCache.GetStateCode.Exception"),e);}
		}
		public int GetRelatedCodeId(int codeId)
		{
			string sKey = String.Format("RID_{0}",codeId);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(int) m_Cache[sKey];
				else
				{
					int relCode =0;
					string SQL = String.Format(@"SELECT RELATED_CODE_ID FROM CODES
																				WHERE CODE_ID={0}" ,codeId);
					relCode =  (int) m_DbConnLookup.ExecuteInt(SQL);
					m_Cache.Add(sKey,relCode);
					return relCode;
				}
			}
			catch(Exception e){throw new DataModelException(Globalization.GetString("LocalCache.GetRelatedCodeId.Exception"),e);}
		}
		public string GetRelatedShortCode(int codeId)
		{
			return this.GetShortCode(this.GetRelatedCodeId(codeId));
		}

		#region Get Table Name
		/// <summary>
		///GetTableName returns the GetCodeTableId(codeId)
		///for a particular codeId in the CODES table.
		/// </summary>
		/// <param name="codeId">CODE_ID for which the CODE_TABLE_ID is required</param>
		/// <returns>System table Id</returns>
		public int GetCodeTableId(int codeId)
		{
			int  tableId=0;
			string sKey = "";

			try
			{				
				if(codeId==0)
					return 0;

				sKey = String.Format("GCTID_{0}",codeId);

				if(m_Cache.ContainsKey(sKey))
					return	(int) m_Cache[sKey];
			
				tableId = m_DbConnLookup.ExecuteInt("SELECT TABLE_ID FROM CODES WHERE CODE_ID = " + codeId);
				m_Cache.Add(sKey,tableId);
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetCodeTableId.DataError"),p_objException);
			}
			finally
			{
			}
			return tableId;
		}
		#endregion

		#region Get Table Name
		/// <summary>
		///GetTableName returns the SYSTEM_TABLE_NAME
		///for a particular TABLE_ID in the GLOSSARY table.
		/// </summary>
		/// <param name="p_iTableID">TABLE_ID for which the SYSTEM_TABLE_NAME is required</param>
		/// <returns>System table name</returns>
		public string GetTableName(int p_iTableID)
		{
			string sTableName="";
			string sKey = "";

			try
			{				
				if(p_iTableID==0)
					return "";

				sKey = String.Format("GTN_{0}",p_iTableID);

				if(m_Cache.ContainsKey(sKey))
					return	(string) m_Cache[sKey];
			
				sTableName = m_DbConnLookup.ExecuteString("SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID = " + p_iTableID);
				m_Cache.Add(sKey,sTableName);
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetTableName.DataError"),p_objException);
			}
			finally
			{
			}
			return sTableName;
		}
		#endregion

		#region Fetch user table names
		/// <summary>
		/// This function fetches the table name for the give table id.
		/// </summary>
		/// <param name="p_lTableID">Table id</param>
		/// <returns>Table name corresponding to the table id.</returns>
		public string GetUserTableName(long p_lTableID)
		{
			string sRetTable = "";
			string sKey = "";

			try
			{
				if (p_lTableID == 0)
					return sRetTable;

				sKey = String.Format("UTN_{0}",p_lTableID);

				if(m_Cache.ContainsKey(sKey))
					return	(string) m_Cache[sKey];

				sRetTable = m_DbConnLookup.ExecuteString("SELECT TABLE_NAME FROM GLOSSARY_TEXT WHERE TABLE_ID = " + p_lTableID + " AND LANGUAGE_CODE = 1033");
				m_Cache.Add(sKey,sRetTable);
			}

			catch (Exception p_objException)
			{
				throw new ACAppException (Globalization.GetString("LocalCache.GetUserTable.UserTable"),p_objException);
			}

			finally {}
			return sRetTable;
		}
		#endregion

		#region Get Code Description
		/// <summary>
		/// Gets Code Description from Code Text table
		/// </summary>
		/// <param name="p_iCodeID">Code ID</param>
		/// <returns>Code Description</returns>
		public string GetCodeDesc(int p_iCodeID)
		{
			string sKey = String.Format("COD_{0}",p_iCodeID);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(string) m_Cache[sKey];
				else
				{
					string sCodeDesc ="";
					string sSQL = String.Format(@"SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID={0}" ,p_iCodeID);
					sCodeDesc =  m_DbConnLookup.ExecuteString(sSQL);
					m_Cache.Add(sKey,sCodeDesc);
					return sCodeDesc;
				}
			}
			catch(Exception p_objEx)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetCodeDesc.Exception"),p_objEx);
			}
			finally	{}
		}
		#endregion

		#region Get Code information
		/// <summary>
		///GetCodeInfo returns the SHORT_CODE, CODE_DESC
		///for a particular CODE_ID in the CODES,CODES_TEXT table.
		/// </summary>
		/// <param name="p_iCode">CODE_ID for which the SHORT_CODE, CODE_DESC is required</param>
		/// <param name="p_ShortCode">SHORT_CODE, reference parameter to store the value</param>
		/// <param name="p_sDesc">CODE_DESC, reference parameter to store the value</param>
		/// <remarks>Short code and the description must not contain 'Comma' as it is delimiter</remarks> 
		public void GetCodeInfo(int p_iCode, ref string p_ShortCode, ref string p_sDesc)
		{
			p_ShortCode="";
			p_sDesc="";

			if(p_iCode==0)
				return;
			
			DbReader objReader = null;

			string sKey = String.Format("CODINF_{0}",p_iCode);
			
			try
			{
				if(m_Cache.ContainsKey(sKey))
				{
					String[] sSplit = Regex.Split((string) m_Cache[sKey], ",");
					
					p_ShortCode = sSplit[0];
					p_sDesc = sSplit[1];

				}
				else
				{
					string sSQL = String.Format(@"SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = {0}" +
								" AND CODES.CODE_ID = CODES_TEXT.CODE_ID", p_iCode);
					
					objReader =  m_DbConnLookup.ExecuteReader(sSQL);
					
					if (objReader != null)
						if(objReader.Read())
						{
							p_ShortCode = objReader.GetString("SHORT_CODE");
							p_sDesc = objReader.GetString("CODE_DESC");
							m_Cache.Add(sKey, p_ShortCode + "," + p_sDesc);
						}		
				}
			}
			catch(Exception p_objEx)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetCodeInfo.Exception"), p_objEx);
			}

			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}
		#endregion

		#region Get system parameter information
		/// <summary>
		/// This function checks for the given system parameter in the Sys_Parms table
		/// </summary>
		/// <param name="p_sFieldName">System parameter</param>
		/// <param name="p_lCaseSen">Ref parameter containing the value corresponding to the given system parameter</param>
		public void GetSysInfo (string p_sFieldName, ref string p_sFieldValue)
		{
			string sKey = String.Format("SYSINF_{0}", p_sFieldName);
			
			DbReader objReader = null;

			try
			{
				if(m_Cache.ContainsKey(sKey))
					p_sFieldValue = (string) m_Cache[sKey];
				else
				{
					objReader =  m_DbConnLookup.ExecuteReader(@"SELECT * FROM SYS_PARMS");
					int iCntRecord = 0;
					if( objReader != null)
					{
						objReader.Read();
						while (objReader.FieldCount > iCntRecord)
						{
							if (objReader.GetName (iCntRecord).ToUpper() == p_sFieldName.ToUpper())
							{
								p_sFieldValue = objReader.GetValue(iCntRecord).ToString();
								m_Cache.Add(sKey, p_sFieldValue);
								return;
							}
							iCntRecord++;
						}
					}
				}
			}
			catch(Exception p_objEx)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetSysInfo.Exception"), p_objEx);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}
		#endregion

		#region Get State Information
		/// <summary>
		///GetStateInfo returns the STATE_ID,STATE_NAME
		///for a particular STATE_ROW_ID in the STATES table.
		/// </summary>
		/// <param name="p_iState">STATE_ROW_ID for which the SHORT_CODE, CODE_DESC is required</param>
		/// <param name="p_sAbbreviation">STATE_ID, reference parameter to store the value</param>
		/// <param name="p_sName">STATE_NAME, reference parameter to store the value</param>
		public void GetStateInfo(int p_iState, ref string p_sAbbreviation, ref string p_sName)
		{
			p_sAbbreviation = "";
			p_sName = "";

			if(p_iState == 0)
				return;
			
			DbReader objReader = null;

			string sKey = String.Format("STATINF_{0}", p_iState);
			
			try
			{
				if(m_Cache.ContainsKey(sKey))
				{
					String[] sSplit = Regex.Split((string) m_Cache[sKey], ",");
					
					p_sAbbreviation = sSplit[0];
					p_sName = sSplit[1];

				}
				else
				{
					string sSQL = String.Format(@"SELECT STATE_ID,STATE_NAME FROM STATES WHERE STATE_ROW_ID = {0}", p_iState);
					
					objReader =  m_DbConnLookup.ExecuteReader(sSQL);
					
					if (objReader != null)
						if(objReader.Read())
						{
							p_sAbbreviation=objReader.GetString("STATE_ID");
							p_sName=objReader.GetString("STATE_NAME");
							m_Cache.Add(sKey, p_sAbbreviation + "," + p_sName);
						}		
				}
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetStateInfo.DataError"), p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}	
		#endregion

		#region Fetch Entity information
		/// <summary>
		/// This function fetches the first name & last name and assigns it to the variables being passed as reference.
		/// </summary>
		/// <param name="p_lEntityID">Entity id</param>
		/// <param name="p_sFirstName">First name of the entity corresponding to the entity id</param>
		/// <param name="p_sLastName">Last name of the entity corresponding to the entity id</param>
		public void GetEntityInfo (long p_lEntityID, ref string p_sFirstName, ref string p_sLastName)
		{			
			p_sFirstName = "";
			p_sLastName = "";

			if(p_lEntityID == 0)
				return;
			
			DbReader objReader = null;

			string sKey = String.Format("ENTINF_{0}", p_lEntityID);
			
			try
			{
				if(m_Cache.ContainsKey(sKey))
				{
					String[] sSplit = Regex.Split((string) m_Cache[sKey], ",");
					
					p_sFirstName = sSplit[0];
					p_sLastName = sSplit[1];

				}
				else
				{
					string sSQL = String.Format(@"SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = {0}", p_lEntityID);
					
					objReader =  m_DbConnLookup.ExecuteReader(sSQL);
					
					if (objReader != null)
						if(objReader.Read())
						{
							p_sFirstName = objReader.GetValue(0).ToString();
							p_sLastName = objReader.GetValue(1).ToString();
							m_Cache.Add(sKey, p_sFirstName + "," + p_sLastName);
						}		
				}
			}
			catch (Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetEntityInfo.GetName"),p_objException);
			}
			finally 
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}
		#endregion

		#region Get Organization information
		/// <summary>
		///GetOrgInfo returns the ABBREVIATION, LAST_NAME
		///for a particular ENTITY_ID in the ENTITY table.
		/// </summary>
		/// <param name="p_iEntityID">ENTITY_ID for which the ABBREVIATION, LAST_NAME is required</param>
		/// <param name="p_sAbbreviation">ABBREVIATION, reference parameter to store the value</param>
		/// <param name="p_sName">LAST_NAME, reference parameter to store the value</param>
		public void GetOrgInfo(int p_iEntityID, ref string p_sAbbreviation, ref string p_sName)
		{
			p_sAbbreviation="";
			p_sName="";

			if(p_iEntityID == 0)
				return;
			
			DbReader objReader = null;

			string sKey = String.Format("ORGINF_{0}", p_iEntityID);
			
			try
			{
				if(m_Cache.ContainsKey(sKey))
				{
					String[] sSplit = Regex.Split((string) m_Cache[sKey], ",");
					
					p_sAbbreviation = sSplit[0];
					p_sName = sSplit[1];

				}
				else
				{
					string sSQL = String.Format(@"SELECT ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_ID = {0}", p_iEntityID);
					
					objReader =  m_DbConnLookup.ExecuteReader(sSQL);
					
					if (objReader != null)
						if(objReader.Read())
						{
							p_sAbbreviation = objReader.GetString("ABBREVIATION");
							p_sName = objReader.GetString("LAST_NAME");
							m_Cache.Add(sKey, p_sAbbreviation + "," + p_sName);
						}		
				}
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetOrgInfo.DataError"),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}
		#endregion

		#region Make a list of Codes
		/// <summary>Returns Code list for a query.
		/// e.g. return what goes in parenthesis in sql statement like "...WHERE CODE_ID IN(15,78,84,87)..."</summary>
		/// <param name="p_sSQL">Query string</param>
		/// <returns>String List</returns>
		public string GetCodeList(string p_sSQL)
		{
			StringBuilder  sList = null;
			DbReader objRdr = null;

			string sKey = String.Format("CDLST_{0}", p_sSQL);
			
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(string) m_Cache[sKey];
				else
				{
					sList = new StringBuilder();
					objRdr =  m_DbConnLookup.ExecuteReader(p_sSQL);
					if(objRdr != null)
					{
						while(objRdr.Read())
						{
							if (sList.ToString() == "")
								sList.Append(objRdr.GetString(0));
							else
							{
								sList.Append("," );
								sList.Append(objRdr.GetString(0));
							}
						}
					}
					m_Cache.Add(sKey, sList.ToString());
					return sList.ToString();
				}
			}			
			catch(Exception p_objExp)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetCodeList.DataError") , p_objExp);
			}
			finally
			{
				if(objRdr!=null)
				{
					objRdr.Close();
					objRdr.Dispose();
				}
			}
		}
		#endregion


		#region Get State Row ID
		/// <summary>
		/// Gets State Row ID from States
		/// </summary>
		/// <param name="stateId">State ID</param>
		/// <returns>State Row ID</returns>
		public int GetStateRowID(string p_sStateId)
		{
			string sKey = String.Format("SRID_{0}",p_sStateId);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(int) m_Cache[sKey];
				else
				{
					int iStateRowId=0;
					string SQL = String.Format(@"SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID='{0}'" ,p_sStateId);
					iStateRowId =  m_DbConnLookup.ExecuteInt(SQL);
					m_Cache.Add(sKey,iStateRowId);
					return iStateRowId;
				}
			}
			catch(Exception p_objEx)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetStateRowID.Exception"),p_objEx);
			}
			finally	{}
		}
		#endregion

		#region Get Entity Details
		public string GetEntityLastName(int entityId)
		{
			string sKey = String.Format("ELN_{0}",entityId);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(string) m_Cache[sKey];
				else
				{
					string entityLastName ="";
					string SQL = String.Format(@"SELECT LAST_NAME FROM ENTITY
																				WHERE ENTITY_ID={0}" ,entityId);
					entityLastName =  (string) m_DbConnLookup.ExecuteString(SQL);
					m_Cache.Add(sKey,entityLastName);
					return entityLastName;
				}
			}
			catch(Exception e){throw new DataModelException(Globalization.GetString("LocalCache.GetEntityLastName.Exception"),e);}
		}
		public string GetEntityLastFirstName(int entityId)
		{
			string sKey = String.Format("ELFN_{0}",entityId);
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(string) m_Cache[sKey];
				else
				{
					DbReader rdr = null;
					string entityLastName ="";
					string SQL = String.Format(@"SELECT FIRST_NAME, LAST_NAME FROM ENTITY
																				WHERE ENTITY_ID={0}" ,entityId);
					rdr =  m_DbConnLookup.ExecuteReader(SQL);
					if(rdr.Read())
					{
						entityLastName = rdr.GetString("LAST_NAME");
						if(rdr.GetString("FIRST_NAME")!="")
							entityLastName = String.Format("{0}, {1}", entityLastName, rdr.GetString("FIRST_NAME")); 
					}
					rdr.Close();
					rdr.Dispose();
					m_Cache.Add(sKey,entityLastName);
					return entityLastName;
				}
			}
			catch(Exception e){throw new DataModelException(Globalization.GetString("LocalCache.GetEntityLastFirstName.Exception"),e);}
		}
		#endregion

		#region Get System User Login Name
		/// <summary>
		///GetTableName returns the SYSTEM_TABLE_NAME
		///for a particular TABLE_ID in the GLOSSARY table.
		/// </summary>
		/// <param name="p_iTableID">TABLE_ID for which the SYSTEM_TABLE_NAME is required</param>
		/// <returns>System table name</returns>
		public string GetSystemLoginName(int userId, int dsnId, string secConnStr)
		{
			string sTableName="";
			string sKey = "";
			DbConnection conn =null;
			try
			{				
				if(userId==0 || secConnStr=="")
					return "";

				sKey = String.Format("GSLN_{0}{1}",userId,dsnId);

				if(m_Cache.ContainsKey(sKey))
					return	(string) m_Cache[sKey];

				conn = DbFactory.GetDbConnection(secConnStr);
				conn.Open();
				sTableName = conn.ExecuteString(String.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID={0} AND DSNID={1}",userId,dsnId));
				m_Cache.Add(sKey,sTableName);
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetSystemLoginName.DataError"),p_objException);
			}
			finally
			{
				if(conn !=null)
					conn.Close();
				conn = null;
			}
			return sTableName;
		}
		#endregion

		/// <summary>
		///GetOrgTableId returns the Entity Table Id.
		/// </summary>
		/// <param name="p_iEntityID">ENTITY_ID for which the Table Id is required</param>
		public int GetOrgTableId(int p_iEntityID)
		{
			int iTableId = 0;
			DbReader objReader = null;

			string sKey = String.Format("ORGTABLEID_{0}", p_iEntityID);
			
			try
			{
				if(m_Cache.ContainsKey(sKey))
					return	(int) m_Cache[sKey];
				else
				{
					string sSQL = String.Format(@"SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID = {0}", p_iEntityID);
					objReader =  m_DbConnLookup.ExecuteReader(sSQL);
					
					if (objReader != null)
						if(objReader.Read())
						{
							iTableId =  Conversion.ConvertStrToInteger(objReader["ENTITY_TABLE_ID"].ToString());
							m_Cache.Add(sKey, iTableId);
						}
					return iTableId; 
				}
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("LocalCache.GetOrgTableId.DataError"),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}

		}
}
#endregion
