/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 07/14/2008 | SI06023 |    AS      | Paramter Read/Save.
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Security.Encryption;

using CCP.ExceptionTypes;

namespace CCP.Common
{
    // The .NET string object achieves most of the functionality coded here.
    // However, these methods have been included when working with variables
    // that aren't of the string type but whoses results are being treated 
    // like a string. Just use the common method .ToString() before calling
    // these methods.
    public static class StringUtils
    {
        public static int Len(string sParam)
        {
            int iResult;
            iResult = sParam.Length;
            return iResult;
        }

        public static int InStr(string sParam, string sSearchFor)
        {
            int iResult;
            iResult = sParam.IndexOf(sSearchFor);
            return iResult;
        }
        // Start SI06023
        public static int InStrPos(string sParam, string sSearchFor, int strtIndex)
        {
            int iResult;
            iResult = sParam.IndexOf(sSearchFor, strtIndex);
            return iResult;
        }
        // End SI06023

        public static string Trim(string sParam)
        {
            string sResult = sParam.Trim();
            //return the result of the operation
            return sResult;
        }

        public static string Left(string sParam, int iLength)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            if (iLength > sParam.Length)
                iLength = sParam.Length;
            string sResult = sParam.Substring(0, iLength);
            //return the result of the operation
            return sResult;
        }

        public static string Right(string sParam, int iLength)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            if (iLength > sParam.Length)    //SIN204
                iLength = sParam.Length;    //SIN204
            string sResult = sParam.Substring(sParam.Length - iLength, iLength);
            //return the result of the operation
            return sResult;
        }

        public static string Mid(string sParam, int iStartIndex, int iLength)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            //Start SIN204
            if (iStartIndex > sParam.Length - 1)
                return "";
            if (iLength + iStartIndex > sParam.Length)
                iLength = (sParam.Length - 1) - iStartIndex;
            //End SIN204
            string sResult = sParam.Substring(iStartIndex, iLength);
            //return the result of the operation
            return sResult;
        }

        public static string Mid(string sParam, int iStartIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string sResult = sParam.Substring(iStartIndex-1);
            //return the result of the operation
            return sResult;
        }

        public static string Encrypt(string sParam)
        {
            DTGCrypt32 oCrypt;
            string sResult;

            oCrypt = new DTGCrypt32();

            sResult = oCrypt.EncryptString(sParam, PrivateConstants.CRYPTKEY);
            return sResult;
        }
       

        public static string Decrypt(string sParam)
        {
            DTGCrypt32 oCrypt;
            string sResult;

            oCrypt = new DTGCrypt32();

            sResult = oCrypt.DecryptString(sParam, PrivateConstants.CRYPTKEY);
            return sResult;
        }

        public static int UBound(string[] sParam)
        {
            if (sParam.Length == 0)
                return -1;
            else
                return sParam.Length - 1;
        }

        public static int InStrRev(string check, string find)
        {
            return check.LastIndexOf(find);
        }

        public static string LTrim(string sParam)
        {
            char[] arr = { ' ', '\r', '\n' };
            return sParam.TrimStart(arr);
        }

        public static bool ConvertToBool(string sParam)
        {
            bool inv = false;
            sParam = sParam.ToUpper();
            switch (sParam)
            {
                case "Y":
                case "YES":
                case "TRUE":
                case "ON":
                case "1":
                    inv = true;
                    break;
                case "N":
                case "NO":
                case "OFF":
                case "FALSE":
                case "":
                case "0":
                case "F":
                    inv = false;
                    break;

            }
            return inv;
        }
    } //End Class StringUtils
} //End Namespace
