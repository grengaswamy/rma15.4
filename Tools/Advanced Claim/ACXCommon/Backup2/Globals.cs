/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CCP.Common
{
    internal class Globals
    {
        public static Dictionary<int,string> errCodes;
        public static Dictionary<object,object> errXRef;
        public static Dictionary<int,int> errHelp;
    }
}
