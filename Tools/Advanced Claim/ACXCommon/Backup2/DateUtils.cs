/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

using CCP.ExceptionTypes;

namespace CCP.Common
{
    public static class DateTimeUtils
    {
        public static bool IsDate(string sDate)
        {
            DateTime dtDate;
            bool bValid = true;
            try
            {
                dtDate = DateTime.Parse(sDate);
            }
            catch 
            {
                // the Parse method failed => the string sDate cannot be converted to a date.
                bValid = false;
            }
            return bValid;
        }

    } //End Class DateTimeUtils
}
