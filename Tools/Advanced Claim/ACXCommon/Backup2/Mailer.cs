using System;
using System.Web.Mail;
using CCP.ExceptionTypes;

namespace CCP.Common
{
	/// <summary>
	///Author  :   Sumeet Rathod & Parag Sarin
	///Dated   :   5th Oct 2004
	///Purpose :   This class performs e-mail operations.
	/// </summary>
	public class Mailer:IDisposable
	{
		#region Member Variables

		/// <summary>
		/// 
		/// </summary>
		private MailMessage m_objMailMessage;

		/// <summary>
		/// 
		/// </summary>
		private string m_sSMTPServer;

		/// <summary>
		/// To address
		/// </summary>
		private string m_sTo;
		/// <summary>
		/// CC address
		/// </summary>
		private string m_sCc;
		/// <summary>
		/// From address
		/// </summary>
		private string m_sFrom;

		/// <summary>
		/// Subject of the mail
		/// </summary>
		private string m_sSubject;

		/// <summary>
		/// Body of the mail
		/// </summary>
		private string m_sBody;

		/// <summary>
		/// SMTP server name
		/// </summary>
		private const string SMTP_SERVER = "SMTPServer";

		#endregion

		#region Properties
		/// <summary>
		/// To address
		/// </summary>
		public string To {set{m_sTo = value;}}
		/// <summary>
		/// CC address
		/// </summary>
		public string Cc {set{m_sCc = value;}}
		/// <summary>
		/// From address
		/// </summary>
		public string From {set{m_sFrom = value;}}
 
		/// <summary>
		/// Subject of the mail
		/// </summary>
		public string Subject {set{m_sSubject = value;}}

		/// <summary>
		/// Body of the mail
		/// </summary>
		public string Body {set{m_sBody = value;}}
		#endregion

		#region Constructor
		/// <summary>
		/// Default constructor
		/// </summary>
		public Mailer()
		{
			m_sTo = string.Empty; 
			m_sCc = string.Empty;
			m_sBody = string.Empty;
			m_sFrom = string.Empty;
			m_sSubject = string.Empty; 
            
			m_objMailMessage = new MailMessage(); 
			m_sSMTPServer = string.Empty; 

			Configure();
		}
		#endregion

		#region Send mail
		/// <summary>
		/// Send an e-mail
		/// </summary>
		public void SendMail ()
		{
			
			try
			{
				//Create mail message
				m_objMailMessage.To = m_sTo;
				if(m_sCc!="")	
					m_objMailMessage.Cc = m_sCc;
				m_objMailMessage.From = m_sFrom;
				m_objMailMessage.Subject = m_sSubject;
				m_objMailMessage.Body = m_sBody;
				
				
				//Fetch the name of the SMTP server
				SmtpMail.SmtpServer = m_sSMTPServer;
				//Send mail
				SmtpMail.Send(m_objMailMessage);
			}
			catch (Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("Mailer.SendMail.Error"),p_objException);
			}
			finally
			{
			
			}
		}
		#endregion

		#region Add Attachment
		
		/// Name			: AddAttachment
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Adds an attachment to the mail message.
		/// </summary>
		/// <param name="p_sAttachmentFile">
		///		Fully Qualified Attachment Filename
		/// </param>
		public void AddAttachment(string p_sAttachmentFile)
		{
			MailAttachment objMailAttachment = null;
			
			try
			{
				if(p_sAttachmentFile.Length>0)
				{
					objMailAttachment = new MailAttachment(p_sAttachmentFile); 
					m_objMailMessage.Attachments.Add(objMailAttachment);  
				}
			}
			catch (Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("Mailer.AddAttachment.Error"),p_objException);
			}
			finally
			{
				objMailAttachment = null;
			}
		}
		#endregion

		#region Configure

		/// Name			: Configure
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Configures the Mailer object based on parameters in configuration file.
		/// </summary>		
		private void Configure()
		{
			ACConfigurator objConfigurator = null;
			
			try
			{
				objConfigurator = new ACConfigurator();
				m_sSMTPServer = objConfigurator.GetValue(SMTP_SERVER);   
			
			}
			catch(ACAppException p_objACAppException)
			{
				throw p_objACAppException;
			}			
			catch(Exception p_objException)
			{
				throw new ACAppException(Globalization.GetString("Mailer.Configure.Error"),p_objException);
			}	
			finally
			{
				objConfigurator = null;
			}	
			
		}

		#endregion

		#region IDisposable Members

		/// Name			: Dispose
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Release all the resources.
		/// </summary>
		public void Dispose()
		{
			// This method has been implemented for future
			// needs where this object may need to release
			// the resources used by it. All clients of this object
			// are advised to call this method.
			m_objMailMessage = null;
		}

		#endregion
	}
}