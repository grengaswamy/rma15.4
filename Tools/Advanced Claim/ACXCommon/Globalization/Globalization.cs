using System;
using System.Globalization;
using System.Resources;
using System.Collections;
using System.Reflection;

namespace CCP.Common
{
	/// <summary>
	/// CCP.Common.Globalization provides access to methods to ease internationalization efforts.</summary>
	/// <remarks>none</remarks>
	public class Globalization
	{
		/// <summary>
		/// m_resourceManagers stores a pool of resource managers to ease internationalization efforts.
		/// Using this method to get a ResourceManager in AC code 
		/// minimizes memory requirements.</summary>
		static protected Hashtable m_resourceManagers=new Hashtable();
		
		/// <summary>
		/// CCP.Common.Globalization.ResourceManager provides simplified access to a single
		/// ResourceManager object to ease internationalization efforts.</summary>
		/// <remarks>none</remarks>
		public static ResourceManager ResourceManager
		{
			get
			{
			
				Assembly assembly = Assembly.GetCallingAssembly();
				string s = assembly.FullName;
				s = s.Split(',')[0];
				s +=".Global";
				ResourceManager mgr = (ResourceManager)(m_resourceManagers[assembly]);
			
				if(mgr == null)
				{
					mgr = new ResourceManager(s, assembly);
					m_resourceManagers[assembly] = mgr;
				}
				return mgr;
			}
		}	
		/// <summary>
		/// CCP.Common.Globalization.GetResourceManager provides internal access to a single
		/// ResourceManager object for internal use by the GetString and GetObject static methods.</summary>
		private static ResourceManager GetResourceManager(Assembly assembly) 
		{
			string s = assembly.FullName;
			s = s.Split(',')[0];
			s +=".Global";
			ResourceManager mgr = (ResourceManager)(m_resourceManagers[assembly]);
			if(mgr == null)
			{
				mgr = new ResourceManager(s, assembly);
				m_resourceManagers[assembly] = mgr;
			}
			return mgr;
		}	

		/// <summary>
		/// CCP.Common.Globalization.GetString provides simplified resource string retrieval 
		/// for all AC assemblies.</summary>
		public static string GetString(string resourceName)
		{
			return GetResourceManager(Assembly.GetCallingAssembly()).GetString(resourceName);
		}		

		/// <summary>
        /// CSC.AdvancedClaims.Common.Globalization.GlobalGetString provides simplified resource string retrieval 
        /// for the Globalization assembly.</summary>
        public static string GlobalGetString(string resourceName)
        {
            string sValue;
            string sAssemblyName;
            ResourceManager rm;

            sAssemblyName = "CCP.Common.Global";
            rm = new ResourceManager(sAssemblyName, Assembly.GetExecutingAssembly());
            sValue = rm.GetString(resourceName);
            return sValue;
        }

        /// <summary>
		/// CCP.Common.Globalization.GetObject provides simplified resource object retrieval 
		/// for all AC assemblies.</summary>
		public static object GetObject(string resourceName)
		{
			return GetResourceManager(Assembly.GetCallingAssembly()).GetObject(resourceName);
		}
	}

}
