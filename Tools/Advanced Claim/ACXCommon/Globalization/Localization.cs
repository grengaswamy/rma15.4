using System;
using System.Globalization;
using System.Resources;
using System.Reflection;
using System.Threading;
using System.Web.Configuration;
using System.Configuration; 


namespace CCP.Common
{
    // <summary>
    // CSC.AdvancedClaims.Globalization provides access to methods to ease internationalization efforts.
    // The name of Resource file must be Localization.resx. This has been accepted as the project convention.
    // </summary>
    public class Localization
    {
        private static CultureInfo oCulInfo = null;

        public static ResourceManager ResourceManager
        {
            // Returns the Resource Manager for entry point assembly to be played at run-time if needed.
            get
            {
                if (System.Web.HttpContext.Current == null)
                {

                    string s = Assembly.GetEntryAssembly().FullName;
                    s = s.Split(',')[0];
                    s += ".Localization";
                    ResourceManager resMgr = new ResourceManager(s, Assembly.GetEntryAssembly());
                    return resMgr;
                }
                else
                    return null;
            }
        }

        // Returns the value of string resource
        public static string getString(string sResourceName)
        {
            string sTempString = "";
            if (System.Web.HttpContext.Current != null)
            {
                // Retrieves the culture info from <globalization> in web.config
                sTempString = System.Web.HttpContext.GetGlobalResourceObject("Localization", sResourceName).ToString();
            }
            else
            {
                // Retrieve from the App.Config
                oCulInfo = new CultureInfo(ConfigurationManager.AppSettings.Get("CultureInfo"));

                sTempString = Assembly.GetEntryAssembly().FullName;
                sTempString = sTempString.Split(',')[0];
                sTempString += ".Localization";
                ResourceManager resMgr = new ResourceManager(sTempString, Assembly.GetEntryAssembly());
                sTempString = resMgr.GetString(sResourceName, oCulInfo);
            }
            return sTempString;
        }

        // Returns the value of object resource e.g. icon, image
        public static object getObject(string sResourceName)
        {
            object oTempObject = null;
            if (System.Web.HttpContext.Current != null)
            {
                oTempObject = System.Web.HttpContext.GetGlobalResourceObject("Localization", sResourceName);
            }
            else
            {
                // Retrieve from the App.Config
                oCulInfo = new CultureInfo(ConfigurationManager.AppSettings.Get("CultureInfo").ToString());

                string s = Assembly.GetEntryAssembly().FullName;
                s = s.Split(',')[0];
                s += ".Localization";
                ResourceManager resMgr = new ResourceManager(s, Assembly.GetEntryAssembly());
                oTempObject = resMgr.GetObject(sResourceName, oCulInfo);
            }
            return oTempObject;
        }
    }
}