/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | - Calls to Utils.subInitializeGlobals() and Utils.get...() and Utils.put...() changed to pass XML
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
//using CCP.XmlComponents;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlSupport
{
    public class XMLCodes : XMLXSNodeWithList    //SIW529
    {
        /*<GLOSSARY>
        '   <GLOSSARY_TABLE>
        '      <!-- Multiple Code Entries -->
        '      <CODE_ENTRY ID="CDEXXX"                                             <-- Node
        '                  SHORT_CODE="short code"
        '                  DELETED="boolean"
        '                  DATE_TRIGGER="date trigger">
        '         <EFF_DATE>Effective Date</EFF_DATE>
        '         <EXP_DATE>Expiration Date</EXP_DATE>
        '         <LINE_OF_BUSINESS CODE="lob short code">line of business</LINE_OF_BUSINESS>
        '         <INDUSTRY_STD CODE="industry standard short code">industry standard</INDUSTRY_STD>
        '         <RELATED CODE="Related code short code">related code from related table</RELATED>
        '         <!-- Multiple text entries -->
        '         <CODE_TEXT>
        '            <TEXT>text</TEXT>
        '            <LANGUAGE CODE="language code">language</LANGUAGE>
        '            <USEAGE>table useage</USEAGE>
        '         </CODE_TEXT>
        '      </CODE_ENTRY>
        '   </GLOSSARY_TABLE>
        '</GLOSSARY>*/

        private int lCdeId;
        private XmlNode m_xmlText;
        private XmlNodeList m_xmlTextList;
        private XMLGlossary m_Glossary;

        public XMLCodes()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;
            m_xmlText = null;
            m_xmlTextList = null;
            m_Glossary = null;
            lCdeId = 0;

            sThisNodeOrder = new ArrayList(5);
            sThisNodeOrder.Add(Constants.sCDEffDate);
            sThisNodeOrder.Add(Constants.sCDExpDate);
            sThisNodeOrder.Add(Constants.sCDLineOfBusCode);
            sThisNodeOrder.Add(Constants.sCDIndStdCode);
            sThisNodeOrder.Add(Constants.sCDRelatedCode);
            sThisNodeOrder.Add(Constants.sCDCodeTextNode);
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sCDCodeIDPfx;
            }
        }

        public string CodeID
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sCDCodeID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sCDCodeIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    lCdeId = lCdeId + 1;
                    lid = lCdeId + "";
                }
                Utils.putAttribute(putNode, "", Constants.sCDCodeID, Constants.sCDCodeIDPfx + lid, NodeOrder);
            }
        }

        public string ShortCode
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sCDShortCode);
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sCDShortCode, value, NodeOrder);
            }
        }

        public string Deleted
        {
            get
            {
                return Utils.getBool(Node, "", Constants.sCDDeleted);
            }
            set
            {
                Utils.putBool(putNode, "", Constants.sCDDeleted, value, NodeOrder);
            }
        }

        public string DateTrigger
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sCDDateTrigger);
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sCDDateTrigger, value, NodeOrder);
            }
        }

        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sCDEffDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sCDEffDate, value, NodeOrder);
            }
        }

        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sCDExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sCDExpDate, value, NodeOrder);
            }
        }

        public XmlNode putLOBCode(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putNode, Constants.sCDLineOfBusCode, sDesc, sCode, NodeOrder);
        }

        public string LOBCode
        {
            get
            {
                return Utils.getData(Node, Constants.sCDLineOfBusCode);
            }
            set
            {
                Utils.putData(putNode, Constants.sCDLineOfBusCode, value, NodeOrder);
            }
        }

        public string LOBCode_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCDLineOfBusCode);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCDLineOfBusCode, value, NodeOrder);
            }
        }

        public XmlNode putIndStdCode(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putNode, Constants.sCDIndStdCode, sDesc, sCode, NodeOrder);
        }

        public string IndStdCode
        {
            get
            {
                return Utils.getData(Node, Constants.sCDIndStdCode);
            }
            set
            {
                Utils.putData(putNode, Constants.sCDIndStdCode, value, NodeOrder);
            }
        }

        public string IndStdCode_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCDIndStdCode);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCDIndStdCode, value, NodeOrder);
            }
        }

        public XmlNode putRelatedCode(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putNode, Constants.sCDRelatedCode, sDesc, sCode, NodeOrder);
        }

        public string RelatedCode
        {
            get
            {
                return Utils.getData(Node, Constants.sCDRelatedCode);
            }
            set
            {
                Utils.putData(putNode, Constants.sCDRelatedCode, value, NodeOrder);
            }
        }

        public string RelatedCode_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCDRelatedCode);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCDRelatedCode, value, NodeOrder);
            }
        }

        public XmlNode FindText(string sText)
        {
            bool rst;
            if (Text != sText)
            {
                rst = true;
                while (getText(rst) != null)
                {
                    rst = false;
                    if (Text == sText)
                        break;
                }
            }
            return m_xmlText;
        }

        public XmlNodeList TextList
        {
            get
            {
                return getNodeList(Constants.sCDCodeTextNode, ref m_xmlTextList, Node);
            }
        }

        public XmlNode getText(bool rst)
        {
            return getNode(rst, ref m_xmlTextList, ref m_xmlText);
        }

        public int TextCount
        {
            get
            {
                return TextList.Count;
            }
        }

        public XmlNode addText(string sText, string sLanguage, string sLanguageCode, string sUseage)
        {
            m_xmlText = null;

            Text = sText;
            if ((sLanguage != "" && sLanguage != null) || (sLanguageCode != "" && sLanguageCode != null))
                putTextLanguage(sLanguage, sLanguageCode);
            Useage = sUseage;

            return m_xmlText;
        }

        public XmlNode putTextNode
        {
            get
            {
                if (m_xmlText == null)
                    m_xmlText = XML.XMLaddNode(putNode, Constants.sCDCodeTextNode, NodeOrder);
                return m_xmlText;
            }
        }

        public XmlNode TextNode
        {
            get
            {
                return m_xmlText;
            }
            set
            {
                m_xmlText = value;
            }
        }

        public string Text
        {
            get
            {
                return Utils.getData(TextNode, Constants.sCDCodeText);
            }
            set
            {
                Utils.putData(putTextNode, Constants.sCDCodeText, value, Utils.sNodeOrder);	//SIW485 Changed Globals to Utils
            }
        }

        public string Useage
        {
            get
            {
                return Utils.getData(TextNode, Constants.sCDUseage);
            }
            set
            {
                Utils.putData(putTextNode, Constants.sCDUseage, value, Utils.sNodeOrder);	//SIW485 Changed Globals to Utils
            }
        }

        public XmlNode putTextLanguage(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putTextNode, Constants.sCDLanguage, sDesc, sCode, Utils.sNodeOrder);	//SIW485 Changed Globals to Utils
        }

        public string TextLanguage
        {
            get
            {
                return Utils.getData(TextNode, Constants.sCDLanguage);
            }
            set
            {
                Utils.putData(putTextNode, Constants.sCDLanguage, value, Utils.sNodeOrder);	//SIW485 Changed Globals to Utils
            }
        }

        public string TextLanguage_Code
        {
            get
            {
                return Utils.getCode(TextNode, Constants.sCDLanguage);
            }
            set
            {
                Utils.putCode(putTextNode, Constants.sCDLanguage, value, Utils.sNodeOrder);	//SIW485 Changed Globals to Utils
            }
        }

        private void subClearPointers()
        {
            m_xmlText = null;
            m_xmlTextList = null;
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        public new XMLGlossary Parent
        {
            get
            {
                if (m_Glossary == null)
                {
                    m_Glossary = new XMLGlossary();
                    m_Glossary.Codes = this;    //SIW529
                    LinkXMLObjects((XMLXCNodeBase)m_Glossary);   //SIW529
                    Node = null;
                }
                return m_Glossary;
            }
            set
            {
                m_Glossary = value;
                Node = null;
                if (m_Glossary != null)
                    ((XMLXCNodeBase)m_Glossary).LinkXMLObjects(this);  
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sCDCodeNode; }
        }

        public override XmlNode putNode
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = XML.XMLaddNode(Parent.putNode, NodeName, Parent_NodeOrder);
                return xmlThisNode;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public XmlNode addCode(string sid, string sShortCode, string sText, string sTextLang, string sTextLangCode,
                               string sDeleted, string sDateTrigger, string sEffDate, string sExpDate, string sLOB,
                               string sLOBCode, string sRelatedCodeDesc, string sRelatedCodeCode,
                               string sIndStdCodeDesc, string sIndStdCodeCode, string sUseage)
        {
            Node = null;
            CodeID = sid;
            ShortCode = sShortCode;
            Deleted = sDeleted;
            DateTrigger = sDateTrigger;
            EffectiveDate = sEffDate;
            ExpirationDate = sExpDate;
            putLOBCode(sLOB, sLOBCode);
            putRelatedCode(sRelatedCodeDesc, sRelatedCodeCode);
            putIndStdCode(sIndStdCodeDesc, sIndStdCodeCode);
            if (sText != "" && sText != null)
                addText(sText, sTextLang, sTextLangCode, sUseage);
            return Node;
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return CodeList;
            }
        }

        public XmlNodeList CodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getCodeItem(bool rst)
        {
            return getNode(rst);
        }

        public void removeCode()
        {
            if (Node != null)
                Parent_Node.RemoveChild(Node);
            Node = null;
        }

        public XmlNode findCodeByShortCode(string sShortCode, XmlDocument doc)
        {
            bool rst;
            if (ShortCode != sShortCode)
            {
                rst = true;
                while (getCodeItem(rst) != null)
                {
                    rst = false;
                    if (ShortCode == sShortCode)
                        break;
                }
            }
            return Node;
        }

        public XmlNode findCodeByID(string sid, XmlDocument doc)
        {
            bool rst;
            if (sid != CodeID)
            {
                rst = true;
                while (getCodeItem(rst) != null)
                {
                    rst = false;
                    if (CodeID == sid)
                        break;
                }
            }
            return xmlThisNode;
        }

    }
}
