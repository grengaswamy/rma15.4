﻿/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 05/28/2010 | SI06792 |    KCB     | Closed Date
 * 06/14/2010 | SI06792ndb|  NDB     | Fix for multiple injuries per ClaimsParty
 * 08/30/2010 | SIN492  |    NDB     | <ClaimsInjury> ID fix
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;
using CCP.XmlSupport;
using CCP.Constants;
using CCP.Common;
using System.Data.Odbc;
using CCP.XmlComponents;

namespace CCP.XmlSupport
{
    public class ISOUtils
    {
        private SortedDictionary<string, string> sdictCodeCache = new SortedDictionary<string, string>();	//SIW485 Removed static
        private SortedDictionary<string, string> sdictCodeDescCache = new SortedDictionary<string, string>(); // SIN492
		//Start SIW485
        private Utils m_xmlUtils;
        private XmlFormatting.XML m_xml;

        //Start SIW529
        public XmlFormatting.XML XML
        {
            get
            {
                return m_xml;
            }
            set
            {
                if (m_xml.guid != value.guid)
                    m_xml = value;
            }
        }
        //End SIW529

        public Utils Utils
        {
            get
            {
                if (m_xmlUtils == null)
                    m_xmlUtils = new Utils();
                return (Utils)m_xmlUtils;
            }
        }

        public Utils XMLUtils
        {
            get
            {
                return Utils;
            }
        }
		//End SIW485

        //SIW529 public void setCodeListInfo(XmlFormatting.XML xmlLocal, XMLISO xmlISOParent, ref XmlNode pnode,  string sCodeListId, string sCodeListName , string sCodeListOwnerCd)	//SIW485 Removed static
        public void setCodeListInfo(XMLISO xmlISOParent, ref XmlNode pnode, string sCodeListId, string sCodeListName, string sCodeListOwnerCd)  //SIW529
        {
            if (!string.IsNullOrEmpty(sCodeListName) && null != xmlISOParent)
            {
                // Add attribute
                //SIW529 xmlLocal.XMLSetAttributeValue(pnode, "codelistref", sCodeListId);
                XML.XMLSetAttributeValue(pnode, "codelistref", sCodeListId);    //SIW529

                // Add <CodeList> node
                if (!xmlISOParent.ContainsClaimInvestAddCodeListId(sCodeListId))
                {
                    xmlISOParent.addClaimInvestAddCodeListNode();
                    xmlISOParent.ClaimInvestAddCodeListID = sCodeListId;
                    xmlISOParent.ClaimInvestAddCodeListName = sCodeListName;
                    xmlISOParent.ClaimInvestAddCodeListOwnerCd = sCodeListOwnerCd;
                }
            }
        }

        //SIW529 public XmlNode putISOCodeData(XmlFormatting.XML xmlLocal, XMLISO xmlISOParent, XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder, string sCodeListId, string sCodeListName, string sCodeListOwnerCd)	//SIW485 Removed static
        public XmlNode putISOCodeData(XMLISO xmlISOParent, XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder, string sCodeListId, string sCodeListName, string sCodeListOwnerCd) //SIW529
        {

            XmlNode xmlPutNode = Utils.putData(pnode, NodeName, sNewValue, arrnodeorder);	//SIW485 removed XMLSupport specifier   //SIW529

            //SIW529 setCodeListInfo(xmlLocal, xmlISOParent, ref xmlPutNode, sCodeListId, sCodeListName, sCodeListOwnerCd);            
            setCodeListInfo(xmlISOParent, ref xmlPutNode, sCodeListId, sCodeListName, sCodeListOwnerCd);    //SIW529

            return xmlPutNode;
        }

        public string ToISOCurrency(string sCurrAmt)	//SIW485 Removed static
        {            
            string sNewValue = "";
            decimal dCurrencyAmt = new decimal();
            
            if (decimal.TryParse(sCurrAmt, out dCurrencyAmt))
            {
                sNewValue = (dCurrencyAmt * 100).ToString("00000000000");
            }

            return sNewValue;
        }

        public string FromISOCurrency(string xISOCurrAmt)	//SIW485 Removed static
        {
            string sCurrData = "";
            decimal dCurrencyAmt = new decimal();
            Regex rx = new Regex(@"^?\d{11}");

            if (rx.IsMatch(xISOCurrAmt))
            {
                dCurrencyAmt = decimal.Parse(xISOCurrAmt);
                dCurrencyAmt = dCurrencyAmt / 100;
                sCurrData = dCurrencyAmt.ToString();
            }
            
            return sCurrData;
        }

        //SIW529 public XmlNode putISOCurrency(XmlFormatting.XML xmlLocal, XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)	//SIW485 Removed static
        public XmlNode putISOCurrency(XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder) //SIW529
        {
            XmlNode xmlNode;                        

            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)   //SIW529
            {
                if (sNewValue == "" || sNewValue == null)
                {
                    sNewValue = Constants.BLANK;
                }
            }

            sNewValue = ToISOCurrency(sNewValue);            

            xmlNode = Utils.putData(pnode, NodeName, sNewValue, arrnodeorder);	//SIW485 removed XMLSupport specifier
            
            return xmlNode;
        }

        //SIW529 public string getISOCurrency(XmlFormatting.XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed static
        public string getISOCurrency(XmlNode pnode, string NodeName)    //SIW529
        {            
            string sCurrData;

            sCurrData = Utils.getData(pnode, NodeName);	//SIW485 removed XMLSupport specifier

            sCurrData = FromISOCurrency(sCurrData);

            return sCurrData;
            
        }

        //SIW529 public XmlNode putISOBool(XmlFormatting.XML xmlLocal, XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder)	//SIW485 Removed static
        public XmlNode putISOBool(XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder) //SIW529
        {
            //SIW529 return putISOBool(xmlLocal, pnode, NodeName, sNewValue, arrnodeorder,"","");
            return putISOBool(pnode, NodeName, sNewValue, arrnodeorder, "", "");    //SIW529
        }

        //SIW529 public XmlNode putISOBool(XmlFormatting.XML xmlLocal, XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder, string sTrueValue, string sFalseValue)	//SIW485 Removed static
        public XmlNode putISOBool(XmlNode pnode, string NodeName, string sNewValue, ArrayList arrnodeorder, string sTrueValue, string sFalseValue)  //SIW529
        {
            XmlNode xmlNode;
            bool bNodeVal;
            
            //SIW529 if (xmlLocal.Blanking)
            if (XML.Blanking)   //SIW529
            {
                if (sNewValue == "" || sNewValue == null)
                {
                    sNewValue = Constants.BLANK;
                }
            }

            bNodeVal = StringUtils.ConvertToBool(sNewValue);

            sNewValue = ToISOBool(sNewValue, sTrueValue, sFalseValue);

            xmlNode = Utils.putData(pnode, NodeName, sNewValue, arrnodeorder);	//SIW485 removed XMLSupport specifier

            return xmlNode;
        }

        //SIW529 public string getISOBool(XmlFormatting.XML xmlLocal, XmlNode pnode, string NodeName)	//SIW485 Removed static
        public string getISOBool(XmlNode pnode, string NodeName)    //SIW529
        {
            string sNodeVal;
            bool bNodeVal;

            sNodeVal = Utils.getData(pnode, NodeName);	//SIW485 removed XMLSupport specifier

            bNodeVal = StringUtils.ConvertToBool(sNodeVal);
            if (bNodeVal)
            {
                sNodeVal = "Y";
            }
            else
            {
                sNodeVal = "N";
            }
            return sNodeVal;
            
        }

        public string ToISOBool(string sNewValue, string sTrueValue, string sFalseValue)	//SIW485 Removed static
        {
            bool bNodeVal = StringUtils.ConvertToBool(sNewValue);

            if (bNodeVal)
            {
                if (!string.IsNullOrEmpty(sTrueValue))
                {
                    sNewValue = sTrueValue;
                }
                else
                {
                    sNewValue = "1";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(sFalseValue))
                {
                    sNewValue = sFalseValue;
                }
                else
                {
                    sNewValue = "0";
                }
            }

            return sNewValue;
        }

        public string ParseISOPhoneNumber(string ACPhoneNumber)	//SIW485 Removed static
        {
            string sISOPhoneNum;

            sISOPhoneNum = ACPhoneNumber;
            sISOPhoneNum = sISOPhoneNum.Replace("(", "");
            sISOPhoneNum = sISOPhoneNum.Replace(")", "");
            sISOPhoneNum = sISOPhoneNum.Replace("-", "");
            sISOPhoneNum = sISOPhoneNum.Replace(" ", "");
            sISOPhoneNum = sISOPhoneNum.Replace("Ext", "");
            sISOPhoneNum = sISOPhoneNum.Replace(":", "");
            sISOPhoneNum = sISOPhoneNum.Replace(".", "");

            if (sISOPhoneNum.Length >= 10)
            {
                sISOPhoneNum = "+1-" + sISOPhoneNum.Substring(0, 3) + "-" + sISOPhoneNum.Substring(3);
                if (sISOPhoneNum.Length > 21)
                {
                    sISOPhoneNum = sISOPhoneNum.Substring(0, 21);
                }

            }
            return sISOPhoneNum;
        }

        // Start SIN492
        public string ConvertCode(string acTable, string acCode, Debug mDbg)	//SIW485 Removed static
        {
            string isoCode = string.Empty;
            string isoCodeDesc = string.Empty;

            if (ConvertCode(acTable, acCode, out isoCode, out isoCodeDesc, mDbg))
            {
                return isoCode;
            }
            return "";
        }
        // End SIN492

        // SIN492 public static string ConvertCode(string acTable, string acCode, Debug mDbg)
        public bool ConvertCode(string acTable, string acCode, out string sRsltCode, out string sRsltDesc, Debug mDbg)
        {
            OdbcDataReader objDataReader = null;
            OdbcCommand objCommand = null;
            OdbcConnection conn = null;
            // SIN492 string sql = "select LU_RSLT_CODE from UCT_LOOKUP_TRANSLATE where LU_NODE = 'ISO_CODE' and LU_ATTRIBUTE = '" + acTable + "' and LU_CODE = '" + acCode + "'";
            string sql = "select LU_RSLT_CODE, LU_RSLT_DESC from UCT_LOOKUP_TRANSLATE where LU_NODE = 'ISO_CODE' and LU_ATTRIBUTE = '" + acTable + "' and LU_CODE = '" + acCode + "'";
            string cacheKey = acTable.ToUpper() + "|" + acCode.ToUpper();
            string isoCode = string.Empty;
            string isoCodeDesc = string.Empty; // SIN492

            sRsltCode = string.Empty; // SIN492
            sRsltDesc = string.Empty; // SIN492
            try
            {
                if (sdictCodeCache.ContainsKey(cacheKey))
                {
                    // SIN492 return sdictCodeCache[cacheKey];
                    sRsltCode = sdictCodeCache[cacheKey]; // SIN492
                    sRsltDesc = sdictCodeDescCache[cacheKey]; // SIN492
                    return true; // SIN492
                }

                conn = new OdbcConnection("DSN=" + mDbg.Parameters.Parameter("/UCTDSN") + ";Uid=" + mDbg.Parameters.Parameter("/UCTUSER") + ";Pwd=" + mDbg.Parameters.Parameter("/UCTPASS"));
                conn.Open();
                objCommand = new OdbcCommand(sql, conn);
                objDataReader = objCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow);
                if (objDataReader.HasRows)
                {
                    objDataReader.Read();
                    isoCode = objDataReader["LU_RSLT_CODE"].ToString();
                    isoCodeDesc = objDataReader["LU_RSLT_DESC"].ToString(); // SIN492
                    conn.Close();
                }
                //else 
                //SI N480 if (isoCode == string.Empty)
                if ((isoCode == string.Empty)|| (isoCode.Trim() == string.Empty ))      //SIN480
                {
                    //Start SI06650
                    //SI N480 if (acTable == "LOSS_CODE" || acTable == "INS_LINE_CODE")
                    if (acTable == "INS_LINE_CODE")         //SI N480
                    {
                        mDbg.Errors.PostError(ErrorGlobalConstants.ERRC_CODE_LOOKUP, "ISO Code Translation Failed, could not find conversion for AC code: " + acCode + " AC table: " + acTable);
                        throw new Exception("Process terminated due to critical error.  Please check the logs.");
                    }
                    else
                    {
                        //isoCode = acCode;
                        mDbg.LogEntry("ISO Code Lookup unavailable for AC code: " + acCode + " AC table: " + acTable + ", translation skipped.");
                        // SIN492 return "";
                        return false; // SIN492
                    }
                    //End SI06650
                }

                sdictCodeCache.Add(cacheKey, isoCode);
                sdictCodeDescCache.Add(cacheKey, isoCodeDesc); // SIN492

                // SIN492 return isoCode;
                sRsltCode = isoCode; // SIN492
                sRsltDesc = isoCodeDesc; // SIN492

                return true;  // SIN492
            }
            catch (Exception ex)
            {
                // mDbg.Errors.ProcessAppError(ErrorGlobalConstants.ERRC_DB_ERROR, System.Windows.Forms.MessageBoxButtons.OK, "Please make sure Universal Translations are set up properly", "", "DSN=" + mDbg.Parameters.Parameter("/UCTDSN"));
                mDbg.LogEntry(ex.Message);
                throw new Exception("Process terminated due to critical error.  Please check the logs.");
            }
            finally
            {
                if (conn != null && conn.State == System.Data.ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }
        
        public string ToISODate(string sDate)	//SIW485 Removed static
        {            
            string sNewValue = "";
            DateTime dtConv;

            if (!string.IsNullOrEmpty(sDate))
            {
                if (DateTime.TryParse(sDate, out dtConv))
                {
                    sNewValue = dtConv.ToString("yyyy-MM-dd");
                }
            }

            return sNewValue;
        }

        //public static void AddPartyInvolvedToISOXML(XMLISO xISO, XmlFormatting.XML cXML, string sEntityID, string sRoleCode) //SIAS
        //public static void AddPartyInvolvedToISOXML(XMLISO xISO, XmlComponents.XMLClaim xClaim, XmlFormatting.XML cXML, string sEntityID, string sRoleCode)//SIAS             //SI6650
        //public static void AddPartyInvolvedToISOXML(XMLISO xISO, XmlComponents.XMLClaim xClaim, XmlFormatting.XML cXML, string sEntityID, string sRoleCode, ArrayList alInjury) //SI6650  //SI6650
        //public static void AddPartyInvolvedToISOXML(XMLISO xISO, XmlComponents.XMLClaim xClaim, XmlFormatting.XML cXML, string sEntityID, string sRoleCode, ArrayList alInjury, Debug mDbg) //SI6650
        public void AddPartyInvolvedToISOXML(XMLISO xISO, XmlComponents.XMLClaim xClaim, XmlFormatting.XML cXML, string sEntityID, string sRoleCode, ArrayList alInjury, ArrayList alVehiclLossAggregate, ArrayList alPropertyLossAggregate, Debug mDbg) //SI06792	//SIW485 Removed static
        {
            XMLEntities xmlEntities;
            XMLEntity xmlEntity;
            //SIW529 XMLAddresses xmlAddresses;
            XMLAddress xmlAddress;
            XMLAddressReference xmlAddressReference;
            //XMLInjury xmlInjury; //SIAS
            string sEntityType;
            string sPhoneType;          //SI6650
            string[] arrayPhoneType;    //SI6650
            string sPhone;              //SI6650
            string sContactType;        //SI6650

            xISO.addClaimInvestAddClaimsPartyNode();
            xISO.ClaimInvestAddClaimsPartyId = "ENT" + sEntityID;// // Type Identifier 
            //ClaimsPartyName
            xmlEntities = new XMLEntities();
            //SIW529 xmlEntities.Document = cXML.Document;
            xmlEntities.XML = cXML; //SIW529
            //SIW529 xmlEntity = new XMLEntity();
            //SIW529 xmlEntity.Node = xmlEntities.getEntitybyID(cXML.Document, sEntityID);
            xmlEntity = xmlEntities.Entity; //SIW529
            xmlEntity.getEntitybyID(sEntityID); //SIW529
            sEntityType = xmlEntity.EntityType_Code;
            if (sEntityType == "Individual")
            {
                xISO.ClaimInvestAddClaimsPartyGeneralPartyPersonFirstName = xmlEntity.FirstName;
                xISO.ClaimInvestAddClaimsPartyGeneralPartyPersonLastName = xmlEntity.LastName;
            }
            else if (sEntityType == "Household")
            {
                xISO.ClaimInvestAddClaimsPartyGeneralPartyCommlBusinessName = xmlEntity.LegalName;
            }
            else
            {
                xISO.ClaimInvestAddClaimsPartyGeneralPartyCommlBusinessName = xmlEntity.BusinessName;
            }
            if (xmlEntity.TaxId != "")
            {
                xISO.ClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityCode = xmlEntity.TaxIdType;
                xISO.ClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityId = xmlEntity.TaxId.Replace("-","");
            }
            //ClaimsPartyAddress
            //SIW529 xmlAddresses = new XMLAddresses();
            //SIW529 xmlAddresses.Document = cXML.Document;
            //SIW529 xmlAddressReference = new XMLAddressReference();
            bool bAddrReset = true;
            //SIW529 string sAddressID = "";
            xmlAddressReference = xmlEntity.AddressReference;
            while (xmlAddressReference.getAddressReference(bAddrReset) != null)
            {
                bAddrReset = false;
                if (xmlAddressReference.Primary == "Yes" || xmlAddressReference.Primary == "True")
                {
                    //SIW529 sAddressID = xmlAddressReference.AddressID;
                    //SIW529 xmlAddress = new XMLAddress();
                    //SIW529 xmlAddress.Node = xmlAddresses.Address.getAddressbyID(sAddressID);
                    xmlAddress = xmlAddressReference.Address;   //SIW529
                    xISO.ClaimInvestAddClaimsPartyGeneralPartyAddrTypeCode = "MailingAddress";
                    if (xmlAddress.AddressLineList[0] != null)
                        xISO.ClaimInvestAddClaimsPartyGeneralPartyAddr1 = xmlAddress.AddressLineList[0].InnerText;
                    if (xmlAddress.AddressLineList[1] != null)
                        xISO.ClaimInvestAddClaimsPartyGeneralPartyAddr2 = xmlAddress.AddressLineList[1].InnerText;
                    xISO.ClaimInvestAddClaimsPartyGeneralPartyAddrCity = xmlAddress.City;
                    xISO.ClaimInvestAddClaimsPartyGeneralPartyAddrStateProvCode = xmlAddress.State_Code;
                    xISO.ClaimInvestAddClaimsPartyGeneralPartyAddrPostalCode = xmlAddress.PostalCode;
                    break;
                }
            }

            //ClaimsPartyCommunications    
            for (int iContacts = 0; iContacts < xmlEntity.ContactCount; iContacts++)
            {
                xmlEntity.ContactNode = xmlEntity.ContactList[iContacts];
                if (xmlEntity.PrimaryContactFlag == "True" || xmlEntity.PrimaryContactFlag == "Yes")
                {
                    //Begin SI6650
                    //AC XML only contains what the contact relationship is, not the contact type (phone, email, web address)
                    //Must use UCT to translate relationship to ACCORD
                    //We're only interested in phone numbers, so only contact relationships that are of the specified ACCORD contact types should be in the UCT
                    //Examples of these include Cell,Fax,Page,Phone
                    //The UCT translated value should be in the format of 'Phone,Home' or 'Phone,Business'
                    //Currently there is no distinction between Business and Personal Use for Cell, so the translated value would be 'Cell,'
                    //A comma MUST always be present.
                    sPhone = "";
                    sContactType = "";
                    sPhoneType = ConvertCode("PHONE_TYPE", xmlEntity.ContactTypeCode, mDbg);
                    //If the value is blank, there is not a translation for this type and it is considered to not be a phone
                    if (sPhoneType.Length > 0)
                    {
                        //Split the value to get the type of phone and type of phone contact (home or business)
                        arrayPhoneType = sPhoneType.Split(',');
                        sPhone = arrayPhoneType[0];
                        //If array only has 1 value, then there is not a type of phone contact (probably cell)
                        if (arrayPhoneType.Length > 0)
                            sContactType = arrayPhoneType[1];
                        //xISO.ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneTypeCode = xmlEntity.ContactType;
                        //Use the translated phone type
                        xISO.ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneTypeCode = sPhone;
                        //End SI6650
                        xISO.ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNumber = ParseISOPhoneNumber(xmlEntity.ContactInfo); // SI06650 NDB	//SIW485 don't specify ISOUtils in call

                        //Begin SI6650
                        //If a Contact Type is not valued from the UCT translation, the type will be based on Entity Type
                        if (sContactType.Length > 0)
                            xISO.ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneUseCode = sContactType;
                        else
                        {
                            if (sEntityType == "Individual")
                                xISO.ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneUseCode = "Home";
                            else
                                xISO.ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneUseCode = "Business";
                        }
                        //End SI6650
                    }
                    break;
                }
            }

            //ClaimsPartyPersonInfo
            xISO.ClaimInvestAddClaimsPartyPersonInfoGenderCode = xmlEntity.Gender_Code;
            
            if (xmlEntity.DateOfBirth != "")
            {
                xISO.ClaimInvestAddClaimsPartyPersonInfoBirthDt = ToISODate(xmlEntity.DateOfBirth);
            }

            //ClaimsPartyClaimsPartyInfo                
            xISO.ClaimInvestAddClaimsPartyClaimsPartyRoleCode = sRoleCode;


            if (xClaim.DateClosed != "")                                                            //SI06792
                xISO.ClaimInvestAddClaimsPartyClaimsPartyClosedDt = ToISODate(xClaim.DateClosed) ;  //SI06792

            //End SI6650 - Injury
            foreach (lossDetail detailItem in alInjury)
            {
                if (sEntityID == detailItem.entityID)
                {
                xISO.addClaimInvestAddClaimsInjuredInfoNode();
                xISO.ClaimInvestAddClaimsInjuredInfoId = "INJD" + detailItem.unitID;
                // SIN492 xISO.ClaimInvestAddClaimsInjuredInfoClaimsInjuryId = "INJ" + detailItem.unitID;
                xISO.ClaimInvestAddClaimsInjuredInfoClaimsInjuryNatureDesc = detailItem.unitDescription;
                if (xClaim.DateClosed != "")                                                                //SI06792
                    xISO.ClaimInvestAddClaimsPartyClaimsPartyClosedDtIDRef = "INJD" + detailItem.unitID;    //SI06792
                //SI06792ndb break;
                }
            }
            //End SI6650

            //Begin SI06792
            foreach (lossDetail detailItem in alVehiclLossAggregate)
            {
                if (sEntityID == detailItem.entityID)
                {
                    if (xClaim.DateClosed != "")
                        xISO.ClaimInvestAddClaimsPartyClaimsPartyClosedDtIDRef = "UNT" + detailItem.unitID;
                }
            }

            foreach (lossDetail detailItem in alPropertyLossAggregate)
            {
                if (sEntityID == detailItem.entityID)
                {
                    if (xClaim.DateClosed != "")
                        xISO.ClaimInvestAddClaimsPartyClaimsPartyClosedDtIDRef = "UNT" + detailItem.unitID;
                }
            }
            //End SI06792
        }

        public void AddISOClaimPartRelationship(XMLISO xISO, string sClaimParty1ID, string sClaimParty1RoleCode, string sClaimParty2ID, string sClaimParty2RoleCode)	//SIW485 Removed static
        {
            xISO.addClaimInvestAddClaimsPartyRelationshipNode();

            xISO.ClaimInvestAddClaimsPartyRelationshipClaimsParty1Ref = sClaimParty1ID;
            xISO.ClaimInvestAddClaimsPartyRelationshipPartyRole1Code = sClaimParty1RoleCode;

            xISO.ClaimInvestAddClaimsPartyRelationshipClaimsParty2Ref = sClaimParty2ID;
            xISO.ClaimInvestAddClaimsPartyRelationshipPartyRole2Code = sClaimParty2RoleCode;
        }

    }
}
