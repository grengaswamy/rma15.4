/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | - Calls to Utils.subInitializeGlobals() and Utils.get...() and Utils.put...() changed to pass XML
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
//using CCP.XmlComponents;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlSupport
{
    public class XMLBalance : XMLXSNode    //SIW529
    {
        /*<Parent Node>
        '   <BALANCES>
        '     <DATA_ITEM
        '         Name = "Data Item Name"
        '         TYPE="[BOOLEAN,DATE,TIME,CODE,STRING,NUMBER] specifies how the XML processor is to parse the data item"
        '            [data attributes corresponding to data types above as follows:
        '             BOOLEAN:  INDICATOR="[YES|NO]{Boolean}"
        '             DATE:  YEAR="year{integer}" MONTH="month{integer}" DAY="day{integer}"
        '             TIME:  HOUR="hour{integer}" MINUTE="minute{integer}" SECOND="second{integer}" TIME_ZONE="time zone{string}"
        '             CODE:  CODE="shortcode"]>
        '            [Data corresponding to the data type as follows:
        '            STRING:  data value {string}
        '            NUMBER:  data value {decimal}
        '            DATE:  formatted Date
        '            TIME:  formatted TIME
        '     </DATA_ITEM>
        '   </BALANCES>
        '</Parent Node>*/

        private XMLVarDataItem xmlDataItem;

        public XMLBalance()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            m_Parent = null;
            xmlThisNode = null;
            xmlDataItem = null;

            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sVarDataItem);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sBLNode; }
        }

        public override XmlNode Create()
        {
            return Create("","",cxmlDataType.dtString);
        }

        public XmlNode Create(string sDataName, string sData, cxmlDataType cDataType)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                if (sDataName != "" && sDataName != null && sData != "" && sData != null)
                    DataItem.addDataItem(sDataName, sData, "", cDataType);

                InsertInDocument(null, null);
                return Node;                
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLBalance.Create");
                return null;
            }
        }

        public override XMLACNode Parent
        {
            get
            {
                subClearPointers();
                return base.Parent;
            }
            set
            {
                m_Parent = value;
                subClearPointers();
            }
        }

        private void subClearPointers()
        {
            xmlDataItem = null;
        }

        public override XmlDocument Document
        {
            get
            {
                return base.Document;
            }
            protected set
            {
                base.Document = value;
                subClearPointers();
            }
        }

        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = Utils.getNode(Parent_Node, NodeName);
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (xmlDataItem == null)
                {
                    xmlDataItem = new XMLVarDataItem();
                    xmlDataItem.Parent = this;
                }
                return xmlDataItem;
            }
        }
    }
}
