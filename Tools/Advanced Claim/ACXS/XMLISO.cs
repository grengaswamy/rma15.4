﻿/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 * 01/04/2010 | SI06650 |    NDB     | Updates for CMS info
 * 05/28/2010 | SI06792 |    LAU     | Closed Date
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/01/2010 | SI N480 |    LAU     | Various ISO fixes
 * 08/20/2010 | SIN492 |    NDB     | Fixes for XML 5.2 spec
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.XmlFormatting;
using System.Xml;
using System.Collections;
using CCP.Constants;
//using CCP.ISOSupport; // SI06650

namespace CCP.XmlSupport
{
    //Begin SI6650
    public enum ISOUnitType
    {
        Inj,
        Auto,
        Prop,
        ThirdPartyProp
    }
    public struct lossDetail
    {
        public string unitID;
        public ISOUnitType unitType;
        public string unitDescription;
        public string lossDetailID;
        public string entityID;
        public string covCode;
        public string lossCode;
        public string lobCode;
    }
    //End SI6650

    public class XMLISO : XMLXFNode
    {
        private XML m_xml = null;
        private bool m_UseSharedXML = true;

        private ArrayList sNodeOrder;
        private ArrayList sStatusNodeOrder;         //SI06650
        private ArrayList sSignonNodeOrder;
        private ArrayList sSignonPswdOrder;
        private ArrayList sSignonCustIdOrder;
        private ArrayList sSignonCustPswdOrder;
        private ArrayList sSignonClientAppOrder;
        private ArrayList sClaimsSvcOrder;
        private ArrayList sClaimsInvestAddOrder;
        private ArrayList sClaimsInvestAddRsOrder;      //SI N480
        private ArrayList sClaimsInvestAddCodeListOrder;
        //SI06650 Start  
        private ArrayList sClaimsInvestAddComIsoUpdateOrder;
        private ArrayList sClaimsInvestAddComIsoUpdateOrigFldsOrder;
        private ArrayList sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsOrder;
        private ArrayList sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsItemIdOrder;
        //SI06650 End 
        private ArrayList sClaimsInvestAddMsgStatusOrder;
        private ArrayList sClaimsInvestAddPolicyOrder;
        private ArrayList sClaimsInvestAddPolicyContractOrder;
        private ArrayList sClaimsInvestAddPolicyMiscPartyOrder;
        private ArrayList sClaimsInvestAddPolicyMiscPartyItemIdOrder;
        private ArrayList sClaimsInvestAddPolicyMiscPartyMiscOrder;

        private ArrayList sClaimsInvestAddClaimsOccOrder;
        private ArrayList sClaimsInvestAddClaimsOccItemIdOrder;
        private ArrayList sClaimsInvestAddClaimsOccAddrOrder;

        private ArrayList sClaimsInvestAddClaimsPartyOrder;
        private ArrayList sClaimsInvestAddClaimsPartyGenPartyInfoOrder;
        private ArrayList sClaimsInvestAddClaimsPartyClmPartyInfoOrder;
        private ArrayList sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder;
        private ArrayList sClaimsInvestAddClaimsPartyGenPartyNameInfoOrder;
        private ArrayList sClaimsInvestAddClaimsPartyGenPartyNameCommlNameOrder;
        private ArrayList sClaimsInvestAddClaimsPartyGenPartyNamePersonNameOrder;
        private ArrayList sClaimsInvestAddClaimsPartyGenPartyNameTaxIdentityOrder;
        private ArrayList sClaimsInvestAddClaimsPartyGenPartyCommunicationsOrder;
        private ArrayList sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder;
        private ArrayList sClaimsInvestAddClaimsPartyPersonInfoOrder;

        private ArrayList sClaimInvestAddClaimsPartyRelationshipOrder;
        private ArrayList sClaimInvestAddClaimsInjuredInfoOrder;
        private ArrayList sClaimInvestAddClaimsInjuredInfoInjuryOrder;

        private ArrayList sClaimsInvestAddAdjusterPartyOrder;
        private ArrayList sClaimsInvestAddAdjusterPartyGeneralPartyOrder;
        private ArrayList sClaimsInvestAddAdjusterPartyGeneralPartyNameOrder;
        private ArrayList sClaimsInvestAddAdjusterPartyGeneralPartyNamePersonNameOrder;
        private ArrayList sClaimsInvestAddAdjusterPartyInfoOrder;

        private ArrayList sClaimsInvestAddAutoLossInfoOrder;
        private ArrayList sClaimsInvestAddAutoLossVehicleInfoOrder;

        private ArrayList sClaimsInvestAddPropertyLossOrder;
        private ArrayList sClaimsInvestAddPropertyLossItemInfoOrder;//SI06650
        private ArrayList sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder;//SI06650
        private ArrayList sClaimsInvestAddPropertyLossWatercraftOrder;
        private ArrayList sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder;
        private ArrayList sClaimsInvestAddPropertyLossPropertyScheduleOrder;
        private ArrayList sClaimsInvestAddPropertyLossPropertyScheduleItemDefinitionOrder;
        private ArrayList sClaimsInvestAddPropertyLossClaimSubjectInsuranceOrder;

        private ArrayList sClaimsInvestAddWorkCompLossOrder;
        private ArrayList sClaimsInvestAddWorkCompLossEmployeeOrder;

        private ArrayList sClaimsInvestAddLitigationOrder;
        private ArrayList sClaimsInvestAddLitigationEventOrder;

        private ArrayList sClaimsInvestAddClaimsPaymentOrder;
        private ArrayList sClaimsInvestAddClaimsPaymentCoverageOrder;

        private ArrayList sClaimsInvestAddInvestigationOrder;
        private ArrayList sClaimsInvestAddInvestigationSalvageOrder;

        private ArrayList sClaimsInvestAddRemarkTestOrder;

        private XmlNode m_xmlClaimInvestigationAddRs;           //SI N480
        private XmlNodeList m_xmlClaimInvestigationAddRsList;   //SI N480
        private XmlNode m_xmlClaimInvestigationAdd;
        private XmlNodeList m_xmlClaimInvestigationAddList;
        private XmlNode m_xmlClaimInvestAddCodeList;
        private XmlNodeList m_xmlClaimInvestAddCodeListList;
        private XmlNode m_xmlClaimInvestAddClaimsParty;
        private XmlNodeList m_xmlClaimInvestAddClaimsPartyList;
        private XmlNode m_xmlClaimInvestAddClaimsInjuredInfo;
        private XmlNodeList m_xmlClaimInvestAddClaimsInjuredInfoList;
        private XmlNode m_xmlClaimInvestAddClaimsPartyRelationship;
        private XmlNodeList m_xmlClaimInvestAddClaimsPartyRelationshipList;
        private XmlNode m_xmlClaimInvestAddSIUParty; // SIN492
        private XmlNodeList m_xmlClaimInvestAddSIUPartyList; // SIN492
        private XmlNode m_xmlClaimInvestAddAdjusterParty;
        private XmlNodeList m_xmlClaimInvestAddAdjusterPartyList;
        private XmlNode m_xmlClaimInvestAddAdjusterPartyInfo;
        private XmlNodeList m_xmlClaimInvestAddAdjusterPartyInfoList;
        private XmlNode m_xmlClaimInvestAddAutoLossInfo;
        private XmlNodeList m_xmlClaimInvestAddAutoLossInfoList;
        private XmlNode m_xmlClaimInvestAddWorkCompLossInfo;
        private XmlNodeList m_xmlClaimInvestAddWorkCompLossInfoList;
        private XmlNode m_xmlClaimInvestAddPropertyLossInfo;
        private XmlNodeList m_xmlClaimInvestAddPropertyLossInfoList;
        private XmlNode m_xmlClaimInvestAddPropertyLossPropertySchedule;
        private XmlNodeList m_xmlClaimInvestAddPropertyLossPropertyScheduleList;
        private XmlNode m_xmlClaimInvestAddLitigationInfo;
        private XmlNodeList m_xmlClaimInvestAddLitigationInfoList;
        private XmlNode m_xmlClaimInvestAddClaimsPayment;
        private XmlNodeList m_xmlClaimInvestAddClaimsPaymentList;
        private XmlNode m_xmlClaimInvestAddInvestigationInfo;
        private XmlNodeList m_xmlClaimInvestAddInvestigationInfoList;
        private XmlNode m_xmlClaimInvestAddAddlCovInfo; // SI06650        
        private XmlNode m_xmlClaimInvestAddRemarkText;
        private XmlNodeList m_xmlClaimInvestAddRemarkTextList;
        private XmlNode m_xmlClaimInvestAddClaimsPartyClaimsPartyClosedDt;  //SI06792
        private SortedList<string, XmlNode> slCodeList; // SI06650
		//Start SIW485
        private XMLUtils m_xmlUtils;
        private ISOUtils m_isoUtils;

        public Utils Utils
        {
            get
            {
                if (m_xmlUtils == null)
                    m_xmlUtils = new Utils();
                return (Utils)m_xmlUtils;
            }
        }

        public Utils XMLUtils
        {
            get
            {
                return Utils;
            }
        }

        public ISOUtils ISOUtils
        {
            get
            {
                if (m_isoUtils == null)
                    m_isoUtils = new ISOUtils();
                m_isoUtils.XML = XML;   //SIW529
                return m_isoUtils;
            }
        }
		//End SIW485

        public override XML XML
        {
            get
            {
                if (m_UseSharedXML == true)
                {
                    if (null == Utils.gControl)	//SIW485 changed Globals to Utils
                    {
                        return null;
                    }
                    else
                    {
                        return Utils.gControl.XML;	//SIW485 changed Globals to Utils
                    }
                }
                else
                {
                    return m_xml;
                }
            }
            //Start SIW139
            set
            {
                if (m_UseSharedXML == true)
                {
                    Utils.gControl.XML = value;	//SIW485 changed Globals to Utils
                }
                else
                {
                    m_xml = value;
                }
            }
            //End SIW139
        }

        public virtual bool UseSharedXMLDocument
        {
            get
            {
                return m_UseSharedXML;
            }
            set
            {
                if (value != m_UseSharedXML)
                {
                    m_UseSharedXML = value;
                    if (value == false)
                    {
                        XML = new XML();
                        XML.Document = new XmlDocument();
                        XML.Document.LoadXml(Utils.gControl.XML.Document.OuterXml);	//SIW485 changed Globals to Utils
                    }
                }
                m_UseSharedXML = value;
            }
        }

        // Start SI06650
        public XMLISO(XMLISO xiSource) : this()
        {
            UseSharedXMLDocument = xiSource.UseSharedXMLDocument;
            XML = xiSource.XML;
            slCodeList = xiSource.slCodeList;
        }
        // End SI06650

        public XMLISO()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            UseSharedXMLDocument = false;
            Document = new XmlDocument();

            xmlThisNode = null;

            sNodeOrder = new ArrayList();
            sNodeOrder.Add("Status");           //SI06650
            sNodeOrder.Add("SignonRq");
            sNodeOrder.Add("ClaimsSvcRs");     //SI N480
            sNodeOrder.Add("ClaimsSvcRq");

            sStatusNodeOrder = new ArrayList();     //SI06650
            sStatusNodeOrder.Add("StatusCd");
            sStatusNodeOrder.Add("StatusDesc");

            sSignonNodeOrder = new ArrayList();
            sSignonNodeOrder.Add("SignonPswd");
            sSignonNodeOrder.Add("ClientDt");
            sSignonNodeOrder.Add("CustLangPref");
            sSignonNodeOrder.Add("ClientApp");

            sSignonPswdOrder = new ArrayList();
            sSignonPswdOrder.Add("CustId");
            sSignonPswdOrder.Add("CustPswd");

            sSignonClientAppOrder = new ArrayList();
            sSignonClientAppOrder.Add("Org");
            sSignonClientAppOrder.Add("Name");
            sSignonClientAppOrder.Add("Version");

            sSignonCustIdOrder = new ArrayList();
            sSignonCustIdOrder.Add("SPName");
            sSignonCustIdOrder.Add("CustLoginId");

            sSignonCustPswdOrder = new ArrayList();
            sSignonCustPswdOrder.Add("EncryptionTypeCd");
            sSignonCustPswdOrder.Add("Pswd");

            sClaimsSvcOrder = new ArrayList();
            sClaimsSvcOrder.Add("RqUID");
            sClaimsSvcOrder.Add("ClaimInvestigationAddRq");
            sClaimsSvcOrder.Add("ClaimInvestigationAddRs");     //SI N480

            sClaimsInvestAddRsOrder = new ArrayList();      //SI N480
            sClaimsInvestAddRsOrder.Add("MsgStatus");       //SI N480


            sClaimsInvestAddOrder = new ArrayList();
            sClaimsInvestAddOrder.Add("RqUID");
            sClaimsInvestAddOrder.Add("TransactionRequestDt");
            sClaimsInvestAddOrder.Add("CurCd");
            sClaimsInvestAddOrder.Add("CodeList");
            sClaimsInvestAddOrder.Add("com.iso_Update");      //SI06650 Lau
            sClaimsInvestAddOrder.Add("ReplacementInd");
            sClaimsInvestAddOrder.Add("SuppressMatchInd");
            sClaimsInvestAddOrder.Add("SearchBasisCd");
            sClaimsInvestAddOrder.Add("Policy");
            sClaimsInvestAddOrder.Add("ClaimsOccurrence");
            sClaimsInvestAddOrder.Add("ClaimsParty");
            sClaimsInvestAddOrder.Add("ClaimsPartyRelationship");
            sClaimsInvestAddOrder.Add("com.iso_SIUParty");
            //SI06650 sClaimsInvestAddOrder.Add("ClaimsInjuredInfo");
            sClaimsInvestAddOrder.Add("AdjusterParty");
            sClaimsInvestAddOrder.Add("AutoLossInfo");
            sClaimsInvestAddOrder.Add("PropertyLossInfo");
            sClaimsInvestAddOrder.Add("WorkCompLossInfo");
            sClaimsInvestAddOrder.Add("LitigationInfo");
            sClaimsInvestAddOrder.Add("ClaimsPayment");
            sClaimsInvestAddOrder.Add("InvestigationInfo");
            // SI06650 sClaimsInvestAddOrder.Add("AdditionalCoverageInfo"); // unavailable in XML User Manual
            sClaimsInvestAddOrder.Add("com.iso_AddCovInfo"); // SI06650
            sClaimsInvestAddOrder.Add("RemarkText");

            sClaimsInvestAddCodeListOrder = new ArrayList();
            sClaimsInvestAddCodeListOrder.Add("CodeListName");
            sClaimsInvestAddCodeListOrder.Add("CodeListOwnerCd");

            //SI06650 Start 
            sClaimsInvestAddComIsoUpdateOrder = new ArrayList();
            sClaimsInvestAddComIsoUpdateOrder.Add("com.iso_UpdateInd");
            sClaimsInvestAddComIsoUpdateOrder.Add("com.iso_OriginalFields");

            sClaimsInvestAddComIsoUpdateOrigFldsOrder = new ArrayList();
            sClaimsInvestAddComIsoUpdateOrigFldsOrder.Add("com.iso_KeyFields");

            sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsOrder = new ArrayList();
            sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsOrder.Add("PolicyNumber");
            sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsOrder.Add("ItemIdInfo");

            sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsItemIdOrder = new ArrayList();
            sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsItemIdOrder.Add("AgencyId");
            sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsItemIdOrder.Add("InsurerId");
            //SI06650 End Lau

            sClaimsInvestAddMsgStatusOrder = new ArrayList();
            sClaimsInvestAddMsgStatusOrder.Add("MsgStatusCd");
            sClaimsInvestAddMsgStatusOrder.Add("MsgErrorCd");
            sClaimsInvestAddMsgStatusOrder.Add("MsgStatusDesc");

            sClaimsInvestAddPolicyOrder = new ArrayList();
            sClaimsInvestAddPolicyOrder.Add("PolicyNumber");
            sClaimsInvestAddPolicyOrder.Add("LOBCd");
            sClaimsInvestAddPolicyOrder.Add("ContractTerm");
            sClaimsInvestAddPolicyOrder.Add("MiscParty");
            sClaimsInvestAddPolicyOrder.Add("AssignedRiskInd");

            sClaimsInvestAddPolicyContractOrder = new ArrayList();
            sClaimsInvestAddPolicyContractOrder.Add("EffectiveDt");
            sClaimsInvestAddPolicyContractOrder.Add("ExpirationDt");
            sClaimsInvestAddPolicyContractOrder.Add("DurationPeriod");

            sClaimsInvestAddPolicyMiscPartyOrder = new ArrayList();
            sClaimsInvestAddPolicyMiscPartyOrder.Add("ItemIdInfo");
            sClaimsInvestAddPolicyMiscPartyOrder.Add("GeneralPartyInfo");
            sClaimsInvestAddPolicyMiscPartyOrder.Add("com.iso_SIUInfo");
            sClaimsInvestAddPolicyMiscPartyOrder.Add("com.iso_AddInfo");
            sClaimsInvestAddPolicyMiscPartyOrder.Add("MiscPartyInfo");

            sClaimsInvestAddPolicyMiscPartyItemIdOrder = new ArrayList();
            sClaimsInvestAddPolicyMiscPartyItemIdOrder.Add("AgencyId");

            sClaimsInvestAddPolicyMiscPartyMiscOrder = new ArrayList();
            sClaimsInvestAddPolicyMiscPartyMiscOrder.Add("MiscPartyRoleCd");

            sClaimsInvestAddClaimsOccOrder = new ArrayList();
            sClaimsInvestAddClaimsOccOrder.Add("ItemIdInfo");
            sClaimsInvestAddClaimsOccOrder.Add("LossDt");
            sClaimsInvestAddClaimsOccOrder.Add("IncidentDesc");
            sClaimsInvestAddClaimsOccOrder.Add("Addr");
            sClaimsInvestAddClaimsOccOrder.Add("TaxIdentity");
            sClaimsInvestAddClaimsOccOrder.Add("com.iso_RRECd");
            sClaimsInvestAddClaimsOccOrder.Add("com.iso_SiteID");
            sClaimsInvestAddClaimsOccOrder.Add("com.iso_SelfInsuredInd");

            sClaimsInvestAddClaimsOccItemIdOrder = new ArrayList();
            sClaimsInvestAddClaimsOccItemIdOrder.Add("InsurerId");

            sClaimsInvestAddClaimsOccAddrOrder = new ArrayList();
            sClaimsInvestAddClaimsOccAddrOrder.Add("Addr1");//SI06650
            sClaimsInvestAddClaimsOccAddrOrder.Add("City");//SI06650
            sClaimsInvestAddClaimsOccAddrOrder.Add("StateProvCd");

            sClaimsInvestAddClaimsPartyOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyOrder.Add("GeneralPartyInfo");
            sClaimsInvestAddClaimsPartyOrder.Add("PersonInfo");
            sClaimsInvestAddClaimsPartyOrder.Add("ClaimsPartyInfo");
            sClaimsInvestAddClaimsPartyOrder.Add("ClaimsInjuredInfo");     //SI06650

            sClaimsInvestAddClaimsPartyGenPartyInfoOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyGenPartyInfoOrder.Add("NameInfo");
            sClaimsInvestAddClaimsPartyGenPartyInfoOrder.Add("Addr");
            sClaimsInvestAddClaimsPartyGenPartyInfoOrder.Add("Communications");

            sClaimsInvestAddClaimsPartyGenPartyNameInfoOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyGenPartyNameInfoOrder.Add("CommlName");
            sClaimsInvestAddClaimsPartyGenPartyNameInfoOrder.Add("PersonName");
            sClaimsInvestAddClaimsPartyGenPartyNameInfoOrder.Add("TaxIdentity");

            sClaimsInvestAddClaimsPartyGenPartyNameCommlNameOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyGenPartyNameCommlNameOrder.Add("CommercialName");

            sClaimsInvestAddClaimsPartyGenPartyNamePersonNameOrder = new ArrayList();            
            sClaimsInvestAddClaimsPartyGenPartyNamePersonNameOrder.Add("Surname");
            sClaimsInvestAddClaimsPartyGenPartyNamePersonNameOrder.Add("GivenName");

            sClaimsInvestAddClaimsPartyGenPartyNameTaxIdentityOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyGenPartyNameTaxIdentityOrder.Add("TaxIdTypeCd");
            sClaimsInvestAddClaimsPartyGenPartyNameTaxIdentityOrder.Add("TaxId");

            sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder.Add("AddrTypeCd");
            sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder.Add("Addr1");
            sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder.Add("Addr2");
            sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder.Add("City");
            sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder.Add("StateProvCd");
            sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder.Add("PostalCode");

            sClaimsInvestAddClaimsPartyGenPartyCommunicationsOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyGenPartyCommunicationsOrder.Add("PhoneInfo");

            sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder.Add("PhoneTypeCd");
            sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder.Add("CommunicationUseCd");
            sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder.Add("PhoneNumber");

            sClaimsInvestAddClaimsPartyPersonInfoOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyPersonInfoOrder.Add("GenderCd");
            sClaimsInvestAddClaimsPartyPersonInfoOrder.Add("BirthDt");

            sClaimsInvestAddClaimsPartyClmPartyInfoOrder = new ArrayList();
            sClaimsInvestAddClaimsPartyClmPartyInfoOrder.Add("ClaimsPartyRoleCd");
            sClaimsInvestAddClaimsPartyClmPartyInfoOrder.Add("ClosedDt");           //SI06792

            sClaimInvestAddClaimsPartyRelationshipOrder = new ArrayList();
            sClaimInvestAddClaimsPartyRelationshipOrder.Add("ClaimsPartyRole1Cd");
            sClaimInvestAddClaimsPartyRelationshipOrder.Add("ClaimsPartyRole2Cd");

            sClaimInvestAddClaimsInjuredInfoOrder = new ArrayList();
            sClaimInvestAddClaimsInjuredInfoOrder.Add("ClaimsInjury");

            sClaimInvestAddClaimsInjuredInfoInjuryOrder = new ArrayList();
            sClaimInvestAddClaimsInjuredInfoInjuryOrder.Add("InjuryNatureCd");
            sClaimInvestAddClaimsInjuredInfoInjuryOrder.Add("InjuryNatureDesc");
            sClaimInvestAddClaimsInjuredInfoInjuryOrder.Add("LossCauseCd");
            sClaimInvestAddClaimsInjuredInfoInjuryOrder.Add("LossCauseDesc" );
            sClaimInvestAddClaimsInjuredInfoInjuryOrder.Add("BodyPartCd" );
            sClaimInvestAddClaimsInjuredInfoInjuryOrder.Add("BodyPartDesc");
            sClaimInvestAddClaimsInjuredInfoInjuryOrder.Add("ImpairmentPct");
            sClaimInvestAddClaimsInjuredInfoInjuryOrder.Add("InjurySeverityCd");

            sClaimsInvestAddAdjusterPartyOrder = new ArrayList();
            sClaimsInvestAddAdjusterPartyOrder.Add("GeneralPartyInfo");
            sClaimsInvestAddAdjusterPartyOrder.Add("AdjusterPartyInfo");    //SI06650

            sClaimsInvestAddAdjusterPartyGeneralPartyOrder = new ArrayList();
            sClaimsInvestAddAdjusterPartyGeneralPartyOrder.Add("NameInfo");

            sClaimsInvestAddAdjusterPartyGeneralPartyNameOrder = new ArrayList();
            sClaimsInvestAddAdjusterPartyGeneralPartyNameOrder.Add("PersonName");

            sClaimsInvestAddAdjusterPartyGeneralPartyNamePersonNameOrder = new ArrayList();
            sClaimsInvestAddAdjusterPartyGeneralPartyNamePersonNameOrder.Add("Surname");
            sClaimsInvestAddAdjusterPartyGeneralPartyNamePersonNameOrder.Add("GivenName");

            sClaimsInvestAddAdjusterPartyInfoOrder = new ArrayList();
            sClaimsInvestAddAdjusterPartyInfoOrder.Add("CoverageCd");
            sClaimsInvestAddAdjusterPartyInfoOrder.Add("LossCauseCd");

            sClaimsInvestAddAutoLossInfoOrder = new ArrayList();
            sClaimsInvestAddAutoLossInfoOrder.Add("VehInfo");
            sClaimsInvestAddAutoLossInfoOrder.Add("ManufacturerCd");

            sClaimsInvestAddAutoLossVehicleInfoOrder = new ArrayList();
            sClaimsInvestAddAutoLossVehicleInfoOrder.Add("Model");
            sClaimsInvestAddAutoLossVehicleInfoOrder.Add("ModelYear");
            sClaimsInvestAddAutoLossVehicleInfoOrder.Add("VehBodyTypeCd");
            sClaimsInvestAddAutoLossVehicleInfoOrder.Add("VehTypeCd");
            sClaimsInvestAddAutoLossVehicleInfoOrder.Add("VehIdentificationNumber");

            sClaimsInvestAddPropertyLossOrder = new ArrayList();
            sClaimsInvestAddPropertyLossOrder.Add("ItemInfo"); //SI06650
            sClaimsInvestAddPropertyLossOrder.Add("Watercraft");
            sClaimsInvestAddPropertyLossOrder.Add("PropertySchedule");
            sClaimsInvestAddPropertyLossOrder.Add("ClaimsSubjectInsuranceInfo");

            //Start SI06650
            sClaimsInvestAddPropertyLossItemInfoOrder = new ArrayList();
            sClaimsInvestAddPropertyLossItemInfoOrder.Add("ItemDefinition");

            sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder = new ArrayList();
            sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder.Add("ItemTypeCd");
            sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder.Add("Manufacturer");
            sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder.Add("Model");
            sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder.Add("SerialNumber");
            sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder.Add("ModelYear");
            sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder.Add("ManufacturerCd");
            sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder.Add("ModelCd");
            // End SI06650

            sClaimsInvestAddPropertyLossWatercraftOrder = new ArrayList();
            sClaimsInvestAddPropertyLossWatercraftOrder.Add("WaterUnitTypeCd");
            sClaimsInvestAddPropertyLossWatercraftOrder.Add("ItemDefinition");

            sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder = new ArrayList();
            sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder.Add("ItemTypeCd");
            sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder.Add("Manufacturer");
            sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder.Add("Model");
            sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder.Add("SerialNumber");
            sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder.Add("ModelYear");

            sClaimsInvestAddPropertyLossPropertyScheduleOrder = new ArrayList();
            sClaimsInvestAddPropertyLossPropertyScheduleOrder.Add("IsSummaryInd");
            sClaimsInvestAddPropertyLossPropertyScheduleOrder.Add("ItemDefinition");

            sClaimsInvestAddPropertyLossPropertyScheduleItemDefinitionOrder = new ArrayList();
            sClaimsInvestAddPropertyLossPropertyScheduleItemDefinitionOrder.Add("ItemTypeCd");


            sClaimsInvestAddPropertyLossClaimSubjectInsuranceOrder = new ArrayList();
            sClaimsInvestAddPropertyLossClaimSubjectInsuranceOrder.Add("SubjectInsuranceCd");


            sClaimsInvestAddWorkCompLossOrder = new ArrayList();
            sClaimsInvestAddWorkCompLossOrder.Add("WCClaimTypeCd");
            sClaimsInvestAddWorkCompLossOrder.Add("EmployeeInfo");

            sClaimsInvestAddWorkCompLossEmployeeOrder = new ArrayList();
            sClaimsInvestAddWorkCompLossEmployeeOrder.Add("HiredDt");
            sClaimsInvestAddWorkCompLossEmployeeOrder.Add("EmploymentStatusCd");

            sClaimsInvestAddLitigationOrder = new ArrayList();
            sClaimsInvestAddLitigationOrder.Add("CourtName");
            sClaimsInvestAddLitigationOrder.Add("ThresholdTypeCd");
            sClaimsInvestAddLitigationOrder.Add("ThresholdStateProvCd");
            sClaimsInvestAddLitigationOrder.Add("EventInfo");

            sClaimsInvestAddLitigationEventOrder = new ArrayList();
            sClaimsInvestAddLitigationEventOrder.Add("EventCd");
            sClaimsInvestAddLitigationEventOrder.Add("EventDt");

            sClaimsInvestAddClaimsPaymentOrder = new ArrayList();
            sClaimsInvestAddClaimsPaymentOrder.Add("ClaimsPaymentCovInfo");

            sClaimsInvestAddClaimsPaymentCoverageOrder = new ArrayList();
            sClaimsInvestAddClaimsPaymentCoverageOrder.Add("CoverageCd");
            sClaimsInvestAddClaimsPaymentCoverageOrder.Add("ClaimStatusCd");

            sClaimsInvestAddInvestigationOrder = new ArrayList();
            sClaimsInvestAddInvestigationOrder.Add("ValidVINInd");
            sClaimsInvestAddInvestigationOrder.Add("VehDispositionCd");
            sClaimsInvestAddInvestigationOrder.Add("SalvageInfo");

            sClaimsInvestAddInvestigationSalvageOrder = new ArrayList();
            sClaimsInvestAddInvestigationSalvageOrder.Add("SalvageDt");
            sClaimsInvestAddInvestigationSalvageOrder.Add("OwnerRetainingSalvageInd");

            sClaimsInvestAddRemarkTestOrder = new ArrayList();
            sClaimsInvestAddRemarkTestOrder.Add("Text");
        }

        public XmlNode Create()
        {
            return Create(null);
        }

        public XmlNode Create(XmlDocument xDocument)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmldocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            Document = new XmlDocument();
            xmlDocument = Document;

            xmldocFrag = xmlDocument.CreateDocumentFragment();

            xmlElement = xmlDocument.CreateElement("ACORD");
            Node = xmlElement;

            xmldocFrag.AppendChild(xmlElement);

            InsertInDocument(null, null);

            return xmlThisNode;
        }

        public override void InsertInDocument(XmlDocument doc, XmlNode nde)
        {
            XmlNode xmlCurrNode;

            base.InsertInDocument(doc, nde);

            if (xmlDocument.DocumentElement == null)
                xmlDocument.AppendChild(xmlNewNode);
            else
            {
                xmlCurrNode = XML.XMLGetNode(xmlDocument.DocumentElement, "ACORD");

                if (xmlCurrNode != null)
                    xmlDocument.DocumentElement.ReplaceChild(xmlNewNode, xmlCurrNode);
                else
                    if (xmlDocument.DocumentElement.FirstChild == null)
                        xmlDocument.DocumentElement.AppendChild(xmlNewNode);
                    else
                        xmlDocument.InsertBefore(xmlNewNode, xmlDocument.DocumentElement.FirstChild);
            }
        }

        public XmlNode putNode
        {
            get
            {
                if (Node == null)
                    Create();
                return Node;
            }
        }
        //SI06650 Start 
        public XmlNode StatusNode
        {
            get
            {
                return XML.XMLGetNode(Node, "Status");
            }
        }

        public XmlNode putStatusNode
        {
            get
            {
                if (StatusNode == null)
                    XML.XMLaddNode(putNode, "Status", sNodeOrder);
                return StatusNode;
            }
        }

        public string StatusStatusCd
        {
            get
            {
                return Utils.getData(StatusNode, "StatusCd");
            }
            set
            {
                Utils.putData(putStatusNode, "StatusCd", value, sStatusNodeOrder);
            }
        }

        public string StatusStatusDesc
        {
            get
            {
                return Utils.getData(StatusNode, "StatuDesc");
            }
            set
            {
                Utils.putData(putStatusNode, "StatusDesc", value, sStatusNodeOrder);
            }
        }

        //SI06650 End 
        public XmlNode SignonRqNode
        {
            get
            {
                return XML.XMLGetNode(Node, "SignonRq");
            }
        }

        public XmlNode putSignonRqNode
        {
            get
            {
                if (SignonRqNode == null)
                    XML.XMLaddNode(putNode, "SignonRq", sNodeOrder);
                return SignonRqNode;
            }
        }

        public XmlNode SignonPswdNode
        {
            get
            {
                return XML.XMLGetNode(SignonRqNode, "SignonPswd");
            }
        }

        public XmlNode putSignonPswdNode
        {
            get
            {
                if (SignonPswdNode == null)
                    XML.XMLaddNode(putSignonRqNode, "SignonPswd", sSignonNodeOrder);
                return SignonPswdNode;
            }
        }

        public XmlNode SignonCustIdNode
        {
            get
            {
                return XML.XMLGetNode(SignonPswdNode, "CustId");
            }
        }

        public XmlNode putSignonCustIdNode
        {
            get
            {
                if (SignonCustIdNode == null)
                    XML.XMLaddNode(putSignonPswdNode, "CustId", sSignonPswdOrder);
                return SignonCustIdNode;
            }
        }

        public string SignonCustIdSPName
        {
            get
            {
                return Utils.getData(SignonCustIdNode, "SPName");
            }
            set
            {
                Utils.putData(putSignonCustIdNode, "SPName", value, sSignonCustIdOrder);
            }
        }

        public string SignonCustIdCustLoginId
        {
            get
            {
                return Utils.getData(SignonCustIdNode, "CustLoginId");
            }
            set
            {
                Utils.putData(putSignonCustIdNode, "CustLoginId", value, sSignonCustIdOrder);
            }
        }

        public XmlNode SignonCustPswdNode
        {
            get
            {
                return XML.XMLGetNode(SignonPswdNode, "CustPswd");
            }
        }

        public XmlNode putSignonCustPswdNode
        {
            get
            {
                if (SignonCustPswdNode == null)
                    XML.XMLaddNode(putSignonPswdNode, "CustPswd", sSignonPswdOrder);
                return SignonCustPswdNode;
            }
        }

        public string SignonCustPswdEncryptionTypeCd
        {
            get
            {
                return Utils.getData(SignonCustPswdNode, "EncryptionTypeCd");
            }
            set
            {
                Utils.putData(putSignonCustPswdNode, "EncryptionTypeCd", value, sSignonCustPswdOrder);
            }
        }

        public string SignonCustPswdPswd
        {
            get
            {
                return Utils.getData(SignonCustPswdNode, "Pswd");
            }
            set
            {
                Utils.putData(putSignonCustPswdNode, "Pswd", value, sSignonCustPswdOrder);
            }
        }

        public string SignonClientDt
        {
            get
            {
                return Utils.getData(SignonRqNode, "ClientDt");
            }
            set
            {
                Utils.putData(putSignonRqNode, "ClientDt", value, sSignonNodeOrder);
            }
        }

        public string SignonCustLangPref
        {
            get
            {
                return Utils.getData(SignonRqNode, "CustLangPref");
            }
            set
            {
                Utils.putData(putSignonRqNode, "CustLangPref", value, sSignonNodeOrder);
            }
        }

        public XmlNode SignonClientAppNode
        {
            get
            {
                return XML.XMLGetNode(SignonRqNode, "ClientApp");
            }
        }

        public XmlNode putSignonClientAppNode
        {
            get
            {
                if (SignonClientAppNode == null)
                    XML.XMLaddNode(putSignonRqNode, "ClientApp", sSignonNodeOrder);
                return SignonClientAppNode;
            }
        }

        public string SignonClientAppOrg
        {
            get
            {
                return Utils.getData(SignonClientAppNode, "Org");
            }
            set
            {
                Utils.putData(putSignonClientAppNode, "Org", value, sSignonClientAppOrder);
            }
        }

        public string SignonClientAppName
        {
            get
            {
                return Utils.getData(SignonClientAppNode, "Name");
            }
            set
            {
                Utils.putData(putSignonClientAppNode, "Name", value, sSignonClientAppOrder);
            }
        }
        public string SignonClientAppVersion
        {
            get
            {
                return Utils.getData(SignonClientAppNode, "Version");
            }
            set
            {
                Utils.putData(putSignonClientAppNode, "Version", value, sSignonClientAppOrder);
            }
        }

        public XmlNode ClaimsSvcRqNode
        {
            get
            {
                return XML.XMLGetNode(Node, "ClaimsSvcRq");
            }
        }

        public XmlNode putClaimsSvcRqNode
        {
            get
            {
                if (ClaimsSvcRqNode == null)
                    XML.XMLaddNode(putNode, "ClaimsSvcRq", sNodeOrder);
                return ClaimsSvcRqNode;
            }
        }

        public string ClaimsSvcRqUID
        {
            get
            {
                return Utils.getData(ClaimsSvcRqNode, "RqUID");
            }
            set
            {
                Utils.putData(putClaimsSvcRqNode, "RqUID", value, sClaimsSvcOrder);
            }
        }

        protected XmlNode getNode(bool reset, ref XmlNodeList xmlnlist, ref XmlNode xnode)
        {
            if (xmlnlist != null)
            {
                IEnumerator xmlnlenum = xmlnlist.GetEnumerator();
                if (!reset)
                {
                    xmlnlenum.MoveNext();
                    while (xnode != xmlnlenum.Current && xmlnlenum.Current != null)
                        xmlnlenum.MoveNext();
                }
                if (xmlnlenum.MoveNext())
                    xnode = (XmlNode)xmlnlenum.Current;
                else
                    xnode = null;
                if (xnode == null)
                {
                    xmlnlist = null;
                }
            }
            else
                xnode = null;
            return xnode;
        }
        //SI N480 Start
        public XmlNodeList ClaimInvestigationAddRsList
        {
            get
            {
                if (m_xmlClaimInvestigationAddRsList == null)
                    m_xmlClaimInvestigationAddRsList = XML.XMLGetNodes(ClaimsSvcRsNode, "ClaimInvestigationAddRs");
                return m_xmlClaimInvestigationAddRsList;
            }
        }

        public XmlNode getClaimInvestigationAddRs(bool reset)
        {
            object o = ClaimInvestigationAddRsList;
            return getNode(reset, ref m_xmlClaimInvestigationAddRsList, ref m_xmlClaimInvestigationAddRs);
        }

        public XmlNode ClaimInvestigationAddRsNode
        {
            get
            {
                return m_xmlClaimInvestigationAddRs;
            }
            set
            {
                m_xmlClaimInvestigationAddRs = value;
            }
        }

        public XmlNode putClaimInvestigationAddRsNode
        {
            get
            {
                if (ClaimInvestigationAddRsNode == null)
                {
                    ClaimInvestigationAddRsNode = XML.XMLaddNode(putClaimsSvcRsNode, "ClaimInvestigationAddRs", sClaimsSvcOrder);
                }
                return ClaimInvestigationAddRsNode;
            }
        }

        public XmlNode addClaimInvestigationAddRsNode()
        {
            ClaimInvestigationAddRsNode = null;
            return putClaimInvestigationAddRsNode;
        }
        public XmlNode ClaimsSvcRsNode
        {
            get
            {
                return XML.XMLGetNode(Node, "ClaimsSvcRs");
            }
        }

        public XmlNode putClaimsSvcRsNode
        {
            get
            {
                if (ClaimsSvcRsNode == null)
                    XML.XMLaddNode(putNode, "ClaimsSvcRs", sNodeOrder);
                return ClaimsSvcRsNode;
            }
        }

        //SI N480 End 
        public XmlNodeList ClaimInvestigationAddRqList
        {
            get
            {
                if (m_xmlClaimInvestigationAddList == null)
                    m_xmlClaimInvestigationAddList = XML.XMLGetNodes(ClaimsSvcRqNode, "ClaimInvestigationAddRq");
                return m_xmlClaimInvestigationAddList;
            }
        }

        public XmlNode getClaimInvestigationAddRq(bool reset)
        {
            object o = ClaimInvestigationAddRqList;
            return getNode(reset, ref m_xmlClaimInvestigationAddList, ref m_xmlClaimInvestigationAdd);
        }
        
        public XmlNode ClaimInvestigationAddRqNode
        {
            get
            {
                return m_xmlClaimInvestigationAdd;
            }
            set
            {
                m_xmlClaimInvestigationAdd = value;
                ReadClaimInvAddCodeList(); // SI06650
            }
        }

        public XmlNode putClaimInvestigationAddRqNode
        {
            get
            {
                if (ClaimInvestigationAddRqNode == null)
                {
                    ClaimInvestigationAddRqNode = XML.XMLaddNode(putClaimsSvcRqNode, "ClaimInvestigationAddRq", sClaimsSvcOrder);
                    ReadClaimInvAddCodeList(); // SI06650
                }
                return ClaimInvestigationAddRqNode;
            }
        }

        public XmlNode addClaimInvestigationAddRqNode()
        {
            ClaimInvestigationAddRqNode = null;
            return putClaimInvestigationAddRqNode;
        }

        // Start SI06650 NDB

        public XmlNode FindClaimInvByClaimOccInsurerId(string sInsurerId)
        {
            bool bReset = true;
            while (getClaimInvestigationAddRq(bReset) != null)
            {
                bReset = false;
                if (ClaimInvestAddClaimsOccItemInsurerId == sInsurerId)
                {
                    break;                
                }
            }
            return ClaimInvestigationAddRqNode;
        }

        public XmlNode FindClaimInvByClaimOccId(string sClaimOccId)
        {
            bool bReset = true;
            while (getClaimInvestigationAddRq(bReset) != null)
            {
                bReset = false;
                if (ClaimInvestAddClaimsOccId == sClaimOccId)
                {
                    break;
                }
            }
            return ClaimInvestigationAddRqNode;
        }

        // End SI06650 NDB

        public string ClaimInvestAdd_iso_CustLoginId
        {
            get
            {
                return Utils.getData(ClaimInvestigationAddRqNode, "com.iso_CustLoginId");
            }
        }

        public string ClaimInvestAdd_iso_CustLoginPwd
        {
            get
            {
                return Utils.getData(ClaimInvestigationAddRqNode, "com.iso_CustLoginPwd");
            }
        }

        public string ClaimINvestAdd_iso_CustDomain
        {
            get
            {
                return Utils.getData(ClaimInvestigationAddRqNode, "com.iso_CustDomain");
            }
        }

        public string ClaimInvestAddRqUID
        {
            get
            {
                return Utils.getData(ClaimInvestigationAddRqNode, "RqUID");
            }
            set
            {
                Utils.putData(putClaimInvestigationAddRqNode, "RqUID", value, sClaimsInvestAddOrder);
            }
        }

        public string ClaimInvestAddTransactionRequestDt
        {
            get
            {
                return Utils.getData(ClaimInvestigationAddRqNode, "TransactionRequestDt");
            }
            set
            {
                Utils.putData(putClaimInvestigationAddRqNode, "TransactionRequestDt", value, sClaimsInvestAddOrder);
            }
        }

        public string ClaimInvestAddCurCd
        {
            get
            {
                return Utils.getData(ClaimInvestigationAddRqNode, "CurCd");
            }
            set
            {
                Utils.putData(putClaimInvestigationAddRqNode, "CurCd", value, sClaimsInvestAddOrder);
            }
        }

        public XmlNodeList ClaimInvestAddCodeListList
        {
            get
            {
                if (m_xmlClaimInvestAddCodeListList == null)
                    m_xmlClaimInvestAddCodeListList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "CodeList");
                return m_xmlClaimInvestAddCodeListList;
            }
        }

        public XmlNode getClaimInvestAddCodeList(bool reset)
        {
            object o = ClaimInvestAddCodeListList;
            return getNode(reset, ref m_xmlClaimInvestAddCodeListList, ref m_xmlClaimInvestAddCodeList);
        }

        public XmlNode ClaimInvestAddCodeListNode
        {
            get
            {
                return m_xmlClaimInvestAddCodeList;
            }
            set
            {
                m_xmlClaimInvestAddCodeList = value;
            }
        }

        public XmlNode putClaimInvestAddCodeListNode
        {
            get
            {
                if (ClaimInvestAddCodeListNode == null)
                    ClaimInvestAddCodeListNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "CodeList", sClaimsInvestAddOrder);
                return ClaimInvestAddCodeListNode;
            }
        }

        public XmlNode addClaimInvestAddCodeListNode()
        {
            ClaimInvestAddCodeListNode = null;
            return putClaimInvestAddCodeListNode;
        }

        public string ClaimInvestAddCodeListID
        {
            get
            {
                return Utils.getAttribute(ClaimInvestAddCodeListNode, "", "id");
            }
            set
            {
                // Start SI06650
                if (slCodeList.ContainsKey(value.ToUpper()))
                {
                    ClaimInvestAddCodeListNode = slCodeList[value.ToUpper()];
                }
                else
                {
                    // End SI06650
                    Utils.putAttribute(putClaimInvestAddCodeListNode, "", "id", value, sClaimsInvestAddOrder);
                    slCodeList.Add(value.ToUpper(), putClaimInvestAddCodeListNode); // SI06650
                }
            }
        }

        public string ClaimInvestAddCodeListName
        {
            get
            {
                return Utils.getData(ClaimInvestAddCodeListNode, "CodeListName");
            }
            set
            {
                Utils.putData(putClaimInvestAddCodeListNode, "CodeListName", value, sClaimsInvestAddCodeListOrder);
            }
        }

        public string ClaimInvestAddCodeListOwnerCd
        {
            get
            {
                return Utils.getData(ClaimInvestAddCodeListNode, "CodeListOwnerCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddCodeListNode, "CodeListOwnerCd", value, sClaimsInvestAddCodeListOrder);
            }
        }
        //SI06650 Start Lau


        public XmlNode ClaimInvestAddComIsoUpdateNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestigationAddRqNode, "com.iso_Update");
            }
        }

        public XmlNode putClaimInvestAddComIsoUpdateNode
        {
            get
            {
                if (ClaimInvestAddComIsoUpdateNode == null)
                    XML.XMLaddNode(putClaimInvestigationAddRqNode, "com.iso_Update", sClaimsInvestAddOrder);
                return ClaimInvestAddComIsoUpdateNode;
            }
        }
        public string ClaimInvestAddComIsoUpdateInd
        {
            get
            {
                return Utils.getData(ClaimInvestAddComIsoUpdateNode, "com.iso_UpdateInd");
            }
            set
            {
                Utils.putData(putClaimInvestAddComIsoUpdateNode, "com.iso_UpdateInd", value, sClaimsInvestAddComIsoUpdateOrder);
            }
        }

        public XmlNode ClaimInvestAddComIsoUpdateOrigFldsNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddComIsoUpdateNode, "com.iso_OriginalFields");
            }
        }

        public XmlNode putClaimInvestAddComIsoUpdateOrigFldsNode
        {
            get
            {
                if (ClaimInvestAddComIsoUpdateOrigFldsNode == null)
                    XML.XMLaddNode(putClaimInvestAddComIsoUpdateNode, "com.iso_OriginalFields", sClaimsInvestAddComIsoUpdateOrder);
                return ClaimInvestAddComIsoUpdateOrigFldsNode;
            }
        }
        public XmlNode ClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddComIsoUpdateOrigFldsNode, "com.iso_KeyFields");
            }
        }

        public XmlNode putClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode
        {
            get
            {
                if (ClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode == null)
                    XML.XMLaddNode(putClaimInvestAddComIsoUpdateOrigFldsNode, "com.iso_KeyFields", sClaimsInvestAddComIsoUpdateOrigFldsOrder);
                return ClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode;
            }
        }

        public string ClaimInvestAddComIsoUpdateOrigFldsKeyFldsPolicyNumber
        {
            get
            {
                return Utils.getData(ClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode, "PolicyNumber");
            }
            set
            {
                Utils.putData(putClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode, "PolicyNumber", value, sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsOrder);
            }
        }

        public XmlNode ClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode, "ItemIdInfo");
            }
        }

        public XmlNode putClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoNode
        {
            get
            {
                if (ClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode, "ItemIdInfo", sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsOrder);
                return ClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoNode;
            }
        }

        public string ClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoAgencyId
        {
            get
            {
                return Utils.getData(ClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoNode, "AgencyId");
            }
            set
            {
                Utils.putData(putClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoNode, "AgencyId", value, sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsItemIdOrder);
            }
        }
        public string ClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoInsurerId
        {
            get
            {
                return Utils.getData(ClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoNode, "InsurerId");
            }
            set
            {
                Utils.putData(putClaimInvestAddComIsoUpdateOrigFldsKeyFldsItemIdInfoNode, "InsurerId", value, sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsItemIdOrder);
            }
        }
        public string ClaimInvestAddComIsoUpdateOrigFldsKeyFldsLossDt
        {
            get
            {
                return Utils.getData(ClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode, "LossDt");
            }
            set
            {
                Utils.putData(putClaimInvestAddComIsoUpdateOrigFldsKeyFldsNode, "LossDt", value, sClaimsInvestAddComIsoUpdateOrigFldsKeyFldsOrder);
            }
        }

        //SI06650 End 
        public XmlNode ClaimInvestAddMsgStatusNode
        {
            get
            {
                //SI N480 return XML.XMLGetNode(ClaimInvestigationAddRqNode, "MsgStatus");
                return XML.XMLGetNode(ClaimInvestigationAddRsNode, "MsgStatus");        //SI N480
            }
        }

        public string ClaimInvestAddMsgStatusCd
        {
            get
            {
                return Utils.getData(ClaimInvestAddMsgStatusNode, "MsgStatusCd");
            }
        }

        public string ClaimInvestAddMsgErrorCd
        {
            get
            {
                return Utils.getData(ClaimInvestAddMsgStatusNode, "MsgErrorCd");
            }
        }

        public string ClaimInvestAddMsgStatusDesc
        {
            get
            {
                return Utils.getData(ClaimInvestAddMsgStatusNode, "MsgStatusDesc");
            }
        }

        public string ClaimInvestAddReplacementInd
        {
            get
            {
                return Utils.getData(ClaimInvestigationAddRqNode, "ReplacementInd");
            }
            set
            {
                Utils.putData(putClaimInvestigationAddRqNode, "ReplacementInd", value, sClaimsInvestAddOrder);
            }
        }

        public string ClaimInvestAddSuppressMatchInd
        {
            get
            {
                return Utils.getData(ClaimInvestigationAddRqNode, "SuppressMatchInd");
            }
            set
            {
                Utils.putData(putClaimInvestigationAddRqNode, "SuppressMatchInd", value, sClaimsInvestAddOrder);
            }
        }

        public string ClaimInvestAddSearchBasisCd
        {
            get
            {
                return Utils.getData(ClaimInvestigationAddRqNode, "SearchBasisCd");
            }
            set
            {
                // SI06650 Utils.putData(putClaimInvestigationAddRqNode, "SearchBasisCd", value, sClaimsInvestAddOrder);
                //SIW529 ISOUtils.putISOCodeData(this, putClaimInvestigationAddRqNode, "SearchBasisCd", value, sClaimsInvestAddOrder, "SearchBasisCdList", "SearchBasisCd", "ISOUS"); // SI06650
                ISOUtils.putISOCodeData(this, putClaimInvestigationAddRqNode, "SearchBasisCd", value, sClaimsInvestAddOrder, "SearchBasisCdList", "SearchBasisCd", "ISOUS");    //SIW529
            }
        }

        public XmlNode ClaimInvestAddPolicyNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestigationAddRqNode, "Policy");
            }
        }

        public XmlNode putClaimInvestAddPolicyNode
        {
            get
            {
                if (ClaimInvestAddPolicyNode == null)
                    XML.XMLaddNode(putClaimInvestigationAddRqNode, "Policy", sClaimsInvestAddOrder);
                return ClaimInvestAddPolicyNode;
            }
        }
        public string ClaimInvestAddPolicyId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddPolicyNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddPolicyNode, "id", value);
            }
        }
        public string ClaimInvestAddPolicyNumber
        {
            get
            {
                return Utils.getData(ClaimInvestAddPolicyNode, "PolicyNumber");
            }
            set
            {
                Utils.putData(putClaimInvestAddPolicyNode, "PolicyNumber", value, sClaimsInvestAddPolicyOrder);
            }
        }

        public string ClaimInvestAddPolicyLOBCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddPolicyNode, "LOBCd");
            }
            set
            {
                // SI06650 Utils.putData(putClaimInvestAddPolicyNode, "LOBCd", value, sClaimsInvestAddPolicyOrder);
                //SIW529 ISOUtils.putISOCodeData(this, putClaimInvestAddPolicyNode, "LOBCd", value, sClaimsInvestAddPolicyOrder, "PolicyTypeCdList", "PolicyTypeCd", "ISOUS"); // SI06650
                ISOUtils.putISOCodeData(this, putClaimInvestAddPolicyNode, "LOBCd", value, sClaimsInvestAddPolicyOrder, "PolicyTypeCdList", "PolicyTypeCd", "ISOUS");   //SIW529
            }
        }

        public XmlNode ClaimInvestAddPolicyContractNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPolicyNode, "ContractTerm");
            }
        }

        public XmlNode putClaimInvestAddPolicyContractNode
        {
            get
            {
                if (ClaimInvestAddPolicyContractNode == null)
                    XML.XMLaddNode(putClaimInvestAddPolicyNode, "ContractTerm", sClaimsInvestAddPolicyOrder);
                return ClaimInvestAddPolicyContractNode;
            }
        }

        public string ClaimInvestAddPolicyContractEffectiveDt
        {
            get
            {
                return Utils.getData(ClaimInvestAddPolicyContractNode, "EffectiveDt");
            }
            set
            {
                Utils.putData(putClaimInvestAddPolicyContractNode, "EffectiveDt", value, sClaimsInvestAddPolicyContractOrder);
            }
        }

        public string ClaimInvestAddPolicyContractExpirationDt
        {
            get
            {
                return Utils.getData(ClaimInvestAddPolicyContractNode, "ExpirationDt");
            }
            set
            {
                Utils.putData(putClaimInvestAddPolicyContractNode, "ExpirationDt", value, sClaimsInvestAddPolicyContractOrder);
            }
        }

        public string ClaimInvestAddPolicyContractDurationPeriod
        {
            get
            {
                XmlNode durationNode = XML.XMLGetNode(ClaimInvestAddPolicyContractNode, "DurationPeriod");
                return Utils.getData(durationNode, "NumUnits");
            }
            set
            {
                if (XML.XMLGetNode(ClaimInvestAddPolicyContractNode, "DurationPeriod") == null)
                    XML.XMLaddNode(putClaimInvestAddPolicyContractNode, "DurationPeriod", sClaimsInvestAddPolicyContractOrder);
                Utils.putData(XML.XMLGetNode(ClaimInvestAddPolicyContractNode, "DurationPeriod"), "NumUnits", value, Utils.sNodeOrder);	//SIW485 changed Globals to Utils
            }
        }

        public XmlNode ClaimInvestAddPolicyMiscPartyNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPolicyNode, "MiscParty");
            }
        }

        public XmlNode putClaimInvestAddPolicyMiscPartyNode
        {
            get
            {
                if (ClaimInvestAddPolicyMiscPartyNode == null)
                    XML.XMLaddNode(putClaimInvestAddPolicyNode, "MiscParty", sClaimsInvestAddPolicyOrder);
                return ClaimInvestAddPolicyMiscPartyNode;
            }
        }

        public XmlNode ClaimInvestAddPolicyMiscPartyItemIdInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPolicyMiscPartyNode, "ItemIdInfo");
            }
        }

        public XmlNode putClaimInvestAddPolicyMiscPartyItemIdInfoNode
        {
            get
            {
                if (ClaimInvestAddPolicyMiscPartyItemIdInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddPolicyMiscPartyNode, "ItemIdInfo", sClaimsInvestAddPolicyMiscPartyOrder);
                return ClaimInvestAddPolicyMiscPartyItemIdInfoNode;
            }
        }

        public XmlNode ClaimInvestAddPolicyMiscPartyMiscInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPolicyMiscPartyNode, "MiscPartyInfo");
            }
        }

        public XmlNode putClaimInvestAddPolicyMiscPartyMiscInfoNode
        {
            get
            {
                if (ClaimInvestAddPolicyMiscPartyMiscInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddPolicyMiscPartyNode, "MiscPartyInfo", sClaimsInvestAddPolicyMiscPartyOrder);
                return ClaimInvestAddPolicyMiscPartyMiscInfoNode;
            }
        }

        public string ClaimInvestAddPolicyMiscItemAgencyId
        {
            get
            {
                return Utils.getData(ClaimInvestAddPolicyMiscPartyItemIdInfoNode, "AgencyId");
            }
            set
            {
                Utils.putData(putClaimInvestAddPolicyMiscPartyItemIdInfoNode, "AgencyId", value, sClaimsInvestAddPolicyMiscPartyItemIdOrder);
            }
        }

        //ToDo:  Start here

        public string ClaimInvestAddPolicyAssignedRiskInd
        {
            get
            {
                return Utils.getData(ClaimInvestAddPolicyNode, "AssignedRiskInd");
            }
            set
            {
                Utils.putData(putClaimInvestAddPolicyNode, "AssignedRiskInd", value, sClaimsInvestAddPolicyOrder);
            }
        }

        public string ClaimsInvestAddPolicyMiscPartyMiscRoleCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddPolicyMiscPartyMiscInfoNode, "MiscPartyRoleCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddPolicyMiscPartyMiscInfoNode, "MiscPartyRoleCd", value, sClaimsInvestAddPolicyMiscPartyMiscOrder);
            }
        }
        public XmlNode ClaimInvestAddClaimsOccNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestigationAddRqNode, "ClaimsOccurrence");
            }
        }

        public XmlNode putClaimInvestAddClaimsOccNode
        {
            get
            {
                if (ClaimInvestAddClaimsOccNode == null)
                    XML.XMLaddNode(putClaimInvestigationAddRqNode, "ClaimsOccurrence", sClaimsInvestAddOrder);
                return ClaimInvestAddClaimsOccNode;
            }
        }
        public string ClaimInvestAddClaimsOccId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddClaimsOccNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddClaimsOccNode, "id", value);
            }
        }
        public string ClaimInvestAddClaimsOccLossDt
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsOccNode, "LossDt");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsOccNode, "LossDt", value, sClaimsInvestAddClaimsOccOrder);
            }
        }
        public string ClaimInvestAddClaimsOccIncidentDesc
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsOccNode, "IncidentDesc");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsOccNode, "IncidentDesc", value, sClaimsInvestAddClaimsOccOrder);
            }
        }
        public string ClaimInvestAddClaimsOcc_iso_RRECode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsOccNode, "com.iso_RRECd");
            }           
        }
        public string ClaimInvestAddClaimsOccTaxIdentity
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsOccNode, "TaxIdentity");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsOccNode, "TaxIdentity", value, sClaimsInvestAddClaimsOccOrder);
            }
        }
        public string ClaimInvestAddClaimsOcc_iso_SiteId
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsOccNode, "com.iso_SiteID");
            }            
        }
        public string ClaimInvestAddClaimsOcc_iso_SelfInsuredInd
        {
            get
            {
                return Utils.getBool(ClaimInvestAddClaimsOccNode, "com.iso_SelfInsuredInd","");
            }
        }
        public XmlNode ClaimInvestAddClaimsOccItemIdNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsOccNode, "ItemIdInfo");
            }
        }

        public XmlNode putClaimInvestAddClaimsOccItemIdNode
        {
            get
            {
                if (ClaimInvestAddClaimsOccItemIdNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsOccNode, "ItemIdInfo", sClaimsInvestAddClaimsOccOrder);
                return ClaimInvestAddClaimsOccItemIdNode;
            }
        }
        public string ClaimInvestAddClaimsOccItemInsurerId
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsOccItemIdNode, "InsurerId");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsOccItemIdNode, "InsurerId", value, sClaimsInvestAddClaimsOccItemIdOrder);
            }
        }


        public XmlNode ClaimInvestAddClaimsOccAddrNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsOccNode, "Addr");
            }
        }

        public XmlNode putClaimInvestAddClaimsOccAddrNode
        {
            get
            {
                if (ClaimInvestAddClaimsOccAddrNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsOccNode, "Addr", sClaimsInvestAddClaimsOccOrder);
                return ClaimInvestAddClaimsOccAddrNode;
            }
        }
        // start SI06650
        public string ClaimInvestAddClaimsOccAddrAddr1
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsOccAddrNode, "Addr1");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsOccAddrNode, "Addr1", value, sClaimsInvestAddClaimsOccAddrOrder);
            }
        }
        public string ClaimInvestAddClaimsOccAddrCity
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsOccAddrNode, "City");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsOccAddrNode, "City", value, sClaimsInvestAddClaimsOccAddrOrder);
            }
        }
        // End SI06650
        public string ClaimInvestAddClaimsOccAddrStateProvCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsOccAddrNode, "StateProvCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsOccAddrNode, "StateProvCd", value, sClaimsInvestAddClaimsOccAddrOrder);
            }
        }        
        public XmlNodeList ClaimInvestAddClaimsPartyList
        {
            get
            {
                if (m_xmlClaimInvestAddClaimsPartyList == null)
                    // SIN492 m_xmlClaimInvestAddClaimsPartyList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "RemarkText");
                    m_xmlClaimInvestAddClaimsPartyList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "ClaimsParty"); // SIN492
                return m_xmlClaimInvestAddClaimsPartyList;
            }
        }        
        // SIN492 public XmlNode getName(bool reset)
        public XmlNode getClaimInvestAddClaimsParty(bool reset) // SIN492
        {
            object o = ClaimInvestAddClaimsPartyList;
            return getNode(reset, ref m_xmlClaimInvestAddClaimsPartyList, ref m_xmlClaimInvestAddClaimsParty);
        }
        public XmlNode ClaimInvestAddClaimsPartyNode
        {
            get
            {
                return m_xmlClaimInvestAddClaimsParty;
            }
            set
            {
                m_xmlClaimInvestAddClaimsParty = value;
            }
        }
        public XmlNode putClaimInvestAddClaimsPartyNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyNode == null)
                    ClaimInvestAddClaimsPartyNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "ClaimsParty", sClaimsInvestAddOrder);
                return ClaimInvestAddClaimsPartyNode;
            }
        }        
        public XmlNode addClaimInvestAddClaimsPartyNode()
        {
            ClaimInvestAddClaimsPartyNode = null;
            return putClaimInvestAddClaimsPartyNode;
        }

        // Start SI06650
        public XmlNode FindClaimInvClaimsPartyById(string sPartyID)
        {
            bool bReset = true;            
            // SIN492 while (getName(bReset) != null)
            while (getClaimInvestAddClaimsParty(bReset) != null) // SIN492
            {
                bReset = false;
                if (ClaimInvestAddClaimsPartyId == sPartyID)
                {
                    break;
                }
            }
            return ClaimInvestAddClaimsPartyNode;
        }
        // End SI06650

        public string ClaimInvestAddClaimsPartyId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddClaimsPartyNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddClaimsPartyNode, "id", value);
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyGeneralPartyNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyNode, "GeneralPartyInfo");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyGeneralPartyNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyGeneralPartyNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyNode, "GeneralPartyInfo", sClaimsInvestAddClaimsPartyOrder);
                return ClaimInvestAddClaimsPartyGeneralPartyNode;
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyPersonInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyNode, "PersonInfo");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyPersonInfoNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyPersonInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyNode, "PersonInfo", sClaimsInvestAddClaimsPartyOrder);
                return ClaimInvestAddClaimsPartyPersonInfoNode;
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyClaimsPartyInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyNode, "ClaimsPartyInfo");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyClaimsPartyInfoNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyClaimsPartyInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyNode, "ClaimsPartyInfo", sClaimsInvestAddClaimsPartyOrder);
                return ClaimInvestAddClaimsPartyClaimsPartyInfoNode;
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyGeneralPartyNameNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyGeneralPartyNode, "NameInfo");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyGeneralPartyNameNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyGeneralPartyNameNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyGeneralPartyNode, "NameInfo", sClaimsInvestAddClaimsPartyGenPartyInfoOrder);
                return ClaimInvestAddClaimsPartyGeneralPartyNameNode;
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyGeneralPartyAddrNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyGeneralPartyNode, "Addr");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyGeneralPartyAddrNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyGeneralPartyAddrNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyGeneralPartyNode, "Addr", sClaimsInvestAddClaimsPartyGenPartyInfoOrder);
                return ClaimInvestAddClaimsPartyGeneralPartyAddrNode;
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyGeneralPartyCommunicationsNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyGeneralPartyNode, "Communications");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyGeneralPartyCommunicationsNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyGeneralPartyCommunicationsNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyGeneralPartyNode, "Communications", sClaimsInvestAddClaimsPartyGenPartyInfoOrder);
                return ClaimInvestAddClaimsPartyGeneralPartyCommunicationsNode;
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyGeneralPartyCommlNameNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyGeneralPartyNameNode, "CommlName");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyGeneralPartyCommlNameNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyGeneralPartyCommlNameNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyGeneralPartyNameNode, "CommlName", sClaimsInvestAddClaimsPartyGenPartyNameInfoOrder);
                return ClaimInvestAddClaimsPartyGeneralPartyCommlNameNode;
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyGeneralPartyPersonNameNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyGeneralPartyNameNode, "PersonName");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyGeneralPartyPersonNameNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyGeneralPartyPersonNameNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyGeneralPartyNameNode, "PersonName", sClaimsInvestAddClaimsPartyGenPartyNameInfoOrder);
                return ClaimInvestAddClaimsPartyGeneralPartyPersonNameNode;
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyCommlBusinessName
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyCommlNameNode, "CommercialName");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyCommlNameNode, "CommercialName", value, sClaimsInvestAddClaimsPartyGenPartyNameCommlNameOrder);
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyPersonFirstName
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyPersonNameNode, "GivenName");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyPersonNameNode, "GivenName", value, sClaimsInvestAddClaimsPartyGenPartyNamePersonNameOrder);
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyPersonLastName
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyPersonNameNode, "Surname");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyPersonNameNode, "Surname", value, sClaimsInvestAddClaimsPartyGenPartyNamePersonNameOrder);
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyGeneralPartyNameNode, "TaxIdentity");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyGeneralPartyNameNode, "TaxIdentity", sClaimsInvestAddClaimsPartyGenPartyNameInfoOrder);
                return ClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityNode;
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityNode, "TaxIdTypeCd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityNode, "TaxIdTypeCd", value, sClaimsInvestAddClaimsPartyGenPartyNameTaxIdentityOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityNode, "TaxIdTypeCd", value, sClaimsInvestAddClaimsPartyGenPartyNameTaxIdentityOrder, "TaxIdTypeCdList", "TaxIdTypeCd", "ISOUS");
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityId
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityNode, "TaxId");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyNameTaxIdentityNode, "TaxId", value, sClaimsInvestAddClaimsPartyGenPartyNameTaxIdentityOrder);
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyAddrTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyAddrNode, "AddrTypeCd");
            }
            set
            {
                // SI06650 Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyAddrNode, "AddrTypeCd", value, sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPartyGeneralPartyAddrNode, "AddrTypeCd", value, sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder, "AddressTypeList", "AddressType", "ACORD"); // SI06650
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyAddr1
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyAddrNode, "Addr1");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyAddrNode, "Addr1", value, sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder);
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyAddr2
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyAddrNode, "Addr2");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyAddrNode, "Addr2", value, sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder);
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyAddrCity
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyAddrNode, "City");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyAddrNode, "City", value, sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder);
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyAddrStateProvCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyAddrNode, "StateProvCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyAddrNode, "StateProvCd", value, sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder);
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyAddrPostalCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyAddrNode, "PostalCode");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyAddrNode, "PostalCode", value, sClaimsInvestAddClaimsPartyGenPartyAddrInfoOrder);
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPartyGeneralPartyCommunicationsNode, "PhoneInfo");
            }
        }

        public XmlNode putClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPartyGeneralPartyCommunicationsNode, "PhoneInfo", sClaimsInvestAddClaimsPartyGenPartyCommunicationsOrder);
                return ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode;
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode, "PhoneTypeCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode, "PhoneTypeCd", value, sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder);
                //ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode, "PhoneTypeCd", value, sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder, "PhoneType","ACORD"); // SI06650
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneUseCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode, "CommunicationUseCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode, "CommunicationUseCd", value, sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder);
                //ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode, "CommunicationUseCd", value, sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder,"CommunicationUseCd","ACORD"); // SI06650
            }
        }
        public string ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNumber
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode, "PhoneNumber");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyGeneralPartyCommunicationsPhoneNode, "PhoneNumber", value, sClaimsInvestAddClaimsPartyGenPartyCommunicationsPhoneInfoOrder);
            }
        }
        public string ClaimInvestAddClaimsPartyPersonInfoGenderCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyPersonInfoNode, "GenderCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyPersonInfoNode, "GenderCd", value, sClaimsInvestAddClaimsPartyPersonInfoOrder);
                //SI06650 ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPartyPersonInfoNode, "GenderCd", value, sClaimsInvestAddClaimsPartyPersonInfoOrder, "GenderCdList" ,"GenderCd", "ISOUS");  //SI06650
            }
        }
        public string ClaimInvestAddClaimsPartyPersonInfoBirthDt
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyPersonInfoNode, "BirthDt");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyPersonInfoNode, "BirthDt", value, sClaimsInvestAddClaimsPartyPersonInfoOrder);
            }
        }
        public string ClaimInvestAddClaimsPartyClaimsPartyRoleCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyClaimsPartyInfoNode, "ClaimsPartyRoleCd");
            }
            set
            {
                //Do NOT use codelist ref for ACORD roles
                switch (value)
                {
                    case "Alias":
                    case "ImpoundAgcy":
                    case "Appraiser":
                    case "PortOrigin":
                    case "DeathMaster":
                    case "RecoveringAgency":
                    case "Emergency":
                    case "SalvageBuyer":
                    case "ImpoundFac":
                        Utils.putData(putClaimInvestAddClaimsPartyClaimsPartyInfoNode, "ClaimsPartyRoleCd", value, sClaimsInvestAddClaimsPartyClmPartyInfoOrder);
                        break;
                    default:
                        ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPartyClaimsPartyInfoNode, "ClaimsPartyRoleCd", value, sClaimsInvestAddClaimsPartyClmPartyInfoOrder, "ClaimsPartyRoleCdList", "ClaimsPartyRoleCd", "ISOUS"); // SI06650 NDB
                        break;
                }

                // SI06650 Utils.putData(putClaimInvestAddClaimsPartyClaimsPartyInfoNode, "ClaimsPartyRoleCd", value, sClaimsInvestAddClaimsPartyClmPartyInfoOrder);
                //ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPartyClaimsPartyInfoNode, "ClaimsPartyRoleCd", value, sClaimsInvestAddClaimsPartyClmPartyInfoOrder, "ClaimsPartyRoleCdList", "ClaimsPartyRoleCd", "ISOUS"); // SI06650 NDB  //KCB
            }
        }
        //SI06792 Start
        public string ClaimInvestAddClaimsPartyClaimsPartyClosedDt
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyClaimsPartyInfoNode, "ClosedDt");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsPartyClaimsPartyInfoNode, "ClosedDt", value, sClaimsInvestAddClaimsPartyClmPartyInfoOrder);
            }
        }
        public XmlNode ClaimInvestAddClaimsPartyClaimsPartyClosedDtNode
        {
            get
            {
                return m_xmlClaimInvestAddClaimsPartyClaimsPartyClosedDt;
            }
            set
            {
                m_xmlClaimInvestAddClaimsPartyClaimsPartyClosedDt = value;
            }
        }
        public XmlNode putClaimInvestAddClaimsPartyClaimsPartyClosedDtNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyClaimsPartyClosedDt == null)
                    ClaimInvestAddClaimsPartyClaimsPartyClosedDtNode = XML.XMLaddNode(putClaimInvestAddClaimsPartyClaimsPartyClosedDtNode, "ClosedDt", sClaimsInvestAddClaimsPartyClmPartyInfoOrder);
                return ClaimInvestAddClaimsPartyClaimsPartyClosedDtNode;
            }
        }
        public string ClaimInvestAddClaimsPartyClaimsPartyClosedDtIDRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddClaimsPartyClaimsPartyClosedDtNode, "idref");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddClaimsPartyClaimsPartyClosedDtNode, "idref", value);
            }
        }       

        //SI06792 End
        public XmlNodeList ClaimInvestAddClaimsPartyRelationshipList
        {
            get
            {
                if (m_xmlClaimInvestAddClaimsPartyRelationshipList == null)
                    m_xmlClaimInvestAddClaimsPartyRelationshipList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "ClaimsPartyRelationship");
                return m_xmlClaimInvestAddClaimsPartyRelationshipList;
            }
        }
        public XmlNode getClaimInvestAddClaimsPartyRelationship(bool reset)
        {
            object o = ClaimInvestAddClaimsPartyRelationshipList;
            return getNode(reset, ref m_xmlClaimInvestAddClaimsPartyRelationshipList, ref m_xmlClaimInvestAddClaimsPartyRelationship);
        }
        public XmlNode ClaimInvestAddClaimsPartyRelationshipNode
        {
            get
            {
                return m_xmlClaimInvestAddClaimsPartyRelationship;
            }
            set
            {
                m_xmlClaimInvestAddClaimsPartyRelationship = value;
            }
        }
        public XmlNode putClaimInvestAddClaimsPartyRelationshipNode
        {
            get
            {
                if (ClaimInvestAddClaimsPartyRelationshipNode == null)
                    ClaimInvestAddClaimsPartyRelationshipNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "ClaimsPartyRelationship", sClaimsInvestAddOrder);
                return ClaimInvestAddClaimsPartyRelationshipNode;
            }
        }
        public XmlNode addClaimInvestAddClaimsPartyRelationshipNode()
        {
            ClaimInvestAddClaimsPartyRelationshipNode = null;
            return putClaimInvestAddClaimsPartyRelationshipNode;
        }        
        public string ClaimInvestAddClaimsPartyRelationshipClaimsParty1Ref
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddClaimsPartyRelationshipNode, "ClaimsParty1Ref");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddClaimsPartyRelationshipNode, "ClaimsParty1Ref", value);
            }
        }
        public string ClaimInvestAddClaimsPartyRelationshipClaimsParty2Ref
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddClaimsPartyRelationshipNode, "ClaimsParty2Ref");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddClaimsPartyRelationshipNode, "ClaimsParty2Ref", value);
            }
        }
        public string ClaimInvestAddClaimsPartyRelationshipPartyRole1Code
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyRelationshipNode, "ClaimsPartyRole1Cd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddClaimsPartyRelationshipNode, "ClaimsPartyRole1Cd", value, sClaimInvestAddClaimsPartyRelationshipOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPartyRelationshipNode, "ClaimsPartyRole1Cd", value, sClaimInvestAddClaimsPartyRelationshipOrder, "ClaimsPartyRole1CdList", "ClaimsPartyRole1Cd", "ISOUS");  //SI06650
            }
        }
        public string ClaimInvestAddClaimsPartyRelationshipPartyRole2Code
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPartyRelationshipNode, "ClaimsPartyRole2Cd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddClaimsPartyRelationshipNode, "ClaimsPartyRole2Cd", value, sClaimInvestAddClaimsPartyRelationshipOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPartyRelationshipNode, "ClaimsPartyRole2Cd", value, sClaimInvestAddClaimsPartyRelationshipOrder, "ClaimsPartyRole2CdList", "ClaimsPartyRole2Cd", "ISOUS");  //SI06650
            }
        }

        // Start SIN492

        public XmlNodeList ClaimInvestAddSIUPartyList
        {
            get
            {
                if (m_xmlClaimInvestAddSIUPartyList == null)
                    m_xmlClaimInvestAddSIUPartyList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "com.iso_SIUParty");
                return m_xmlClaimInvestAddSIUPartyList;
            }
        }
        public XmlNode getClaimInvestAddSIUParty(bool reset)
        {
            object o = ClaimInvestAddSIUPartyList;
            return getNode(reset, ref m_xmlClaimInvestAddSIUPartyList, ref m_xmlClaimInvestAddSIUParty);
        }

        public XmlNode ClaimInvestAddSIUPartyNode
        {
            get
            {
                return m_xmlClaimInvestAddSIUParty;
            }
            set
            {
                m_xmlClaimInvestAddSIUParty = value;
            }
        }
        
        public XmlNode putClaimInvestAddSIUPartyNode
        {
            get
            {
                if (ClaimInvestAddSIUPartyNode == null)
                    ClaimInvestAddSIUPartyNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "com.iso_SIUParty", sClaimsInvestAddOrder);
                return ClaimInvestAddSIUPartyNode;
            }
        }

        // End SIN492

        public XmlNodeList ClaimInvestAddClaimsInjuredInfoList
        {
            get
            {
                if (m_xmlClaimInvestAddClaimsInjuredInfoList == null)
                    //SIN492 m_xmlClaimInvestAddClaimsInjuredInfoList = XML.XMLGetNodes(ClaimInvestAddClaimsPartyClaimsPartyInfoNode , "ClaimsInjuredInfo");
                    m_xmlClaimInvestAddClaimsInjuredInfoList = XML.XMLGetNodes(ClaimInvestAddClaimsPartyNode , "ClaimsInjuredInfo"); // SIN492
                return m_xmlClaimInvestAddClaimsInjuredInfoList;
            }
        }
        public XmlNode getClaimInvestAddClaimsInjuredInfo(bool reset)
        {
            object o = ClaimInvestAddClaimsInjuredInfoList;
            return getNode(reset, ref m_xmlClaimInvestAddClaimsInjuredInfoList, ref m_xmlClaimInvestAddClaimsInjuredInfo);
        }
        public XmlNode ClaimInvestAddClaimsInjuredInfoNode
        {
            get
            {
                return m_xmlClaimInvestAddClaimsInjuredInfo;
            }
            set
            {
                m_xmlClaimInvestAddClaimsInjuredInfo = value;
            }
        }
        public XmlNode putClaimInvestAddClaimsInjuredInfoNode
        {
            get
            {
                if (ClaimInvestAddClaimsInjuredInfoNode == null)
                    ClaimInvestAddClaimsInjuredInfoNode = XML.XMLaddNode(putClaimInvestAddClaimsPartyNode, "ClaimsInjuredInfo", sClaimsInvestAddClaimsPartyOrder);
                return ClaimInvestAddClaimsInjuredInfoNode;
            }
        }
        public string ClaimInvestAddClaimsInjuredInfoId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddClaimsInjuredInfoNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddClaimsInjuredInfoNode, "id", value);
            }
        }
        public XmlNode addClaimInvestAddClaimsInjuredInfoNode()
        {
            ClaimInvestAddClaimsInjuredInfoNode = null;
            return putClaimInvestAddClaimsInjuredInfoNode;
        }
        public XmlNode ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsInjuredInfoNode, "ClaimsInjury");
            }
        }

        public XmlNode putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode
        {
            get
            {
                if (ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsInjuredInfoNode, "ClaimsInjury", sClaimInvestAddClaimsInjuredInfoOrder);
                return ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode;
            }
        }
        //Start SI06650 // Start SI06650: Code uncommented, as this is a required field
        // Start SIN492 - <ClaimsInjury> doesn't have an @ID attribute based on XML 5.2 spec
        //public string ClaimInvestAddClaimsInjuredInfoClaimsInjuryId
        //{
        //    get
        //    {
        //        return XML.XMLGetAttributeValue(ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "id");
        //    }
        //    set
        //    {
        //        XML.XMLSetAttributeValue(putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "id", value);
        //    }
        //}
        // End SIN492 
        //End SI06650 // End SI06650: Code uncommented, as this is a required field
        public string ClaimInvestAddClaimsInjuredInfoClaimsInjuryNatureCd
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "InjuryNatureCd");
            }
            set
            {
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "InjuryNatureCd", value, sClaimInvestAddClaimsInjuredInfoInjuryOrder, "InjuryNatureCdList", "InjuryNatureCd", "ISOUS");
            }
        }
        public string ClaimInvestAddClaimsInjuredInfoClaimsInjuryNatureDesc
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "InjuryNatureDesc");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "InjuryNatureDesc", value, sClaimInvestAddClaimsInjuredInfoInjuryOrder);
            }
        }
        public string ClaimInvestAddClaimsInjuredInfoClaimsInjuryLossCauseCd
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "LossCauseCd");
            }
            set
            {
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "LossCauseCd", value, sClaimInvestAddClaimsInjuredInfoInjuryOrder, "LossTypeCdList", "LossTypeCd", "ISOUS");
            }
        }
        public string ClaimInvestAddClaimsInjuredInfoClaimsInjuryLossCauseDesc
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "LossCauseDesc");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "LossCauseDesc", value, sClaimInvestAddClaimsInjuredInfoInjuryOrder);
            }
        }
        public string ClaimInvestAddClaimsInjuredInfoClaimsInjuryBodyPartCd
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "BodyPartCd");
            }
            set
            {
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "BodyPartCd", value, sClaimInvestAddClaimsInjuredInfoInjuryOrder, "BodyPartCdList", "BodyPartCd", "ISOUS");
            }
        }
        public string ClaimInvestAddClaimsInjuredInfoClaimsInjuryBodyPartDesc
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "BodyPartDesc");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "BodyPartDesc", value, sClaimInvestAddClaimsInjuredInfoInjuryOrder);
            }
        }
        public string ClaimInvestAddClaimsInjuredInfoClaimsInjuryImpairmentPct
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "ImpairmentPct");
            }
            set
            {
                Utils.putData(putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "ImpairmentPct", value, sClaimInvestAddClaimsInjuredInfoInjuryOrder);
            }
        }
        public string ClaimInvestAddClaimsInjuredInfoClaimsInjurySeverityCd
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "InjurySeverityCd");
            }
            set
            {
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsInjuredInfoClaimsInjuryNode, "InjurySeverityCd", value, sClaimInvestAddClaimsInjuredInfoInjuryOrder, "InjurySeverityCdList", "InjurySeverityCd", "ISOUS");
            }
        }

        public XmlNodeList ClaimInvestAddAdjusterPartyList
        {
            get
            {
                if (m_xmlClaimInvestAddAdjusterPartyList == null)
                    m_xmlClaimInvestAddAdjusterPartyList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "AdjusterParty");
                return m_xmlClaimInvestAddAdjusterPartyList;
            }
        }
        public XmlNode getClaimInvestAddAdjusterParty(bool reset)
        {
            object o = ClaimInvestAddAdjusterPartyList;
            return getNode(reset, ref m_xmlClaimInvestAddAdjusterPartyList, ref m_xmlClaimInvestAddAdjusterParty);
        }
        public XmlNode ClaimInvestAddAdjusterPartyNode
        {
            get
            {
                return m_xmlClaimInvestAddAdjusterParty;
            }
            set
            {
                m_xmlClaimInvestAddAdjusterParty = value;
            }
        }
        public XmlNode putClaimInvestAddAdjusterPartyNode
        {
            get
            {
                if (ClaimInvestAddAdjusterPartyNode == null)
                    ClaimInvestAddAdjusterPartyNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "AdjusterParty", sClaimsInvestAddOrder);
                return ClaimInvestAddAdjusterPartyNode;
            }
        }
        public XmlNode addClaimInvestAddAdjusterPartyNode()
        {
            ClaimInvestAddAdjusterPartyNode = null;
            return putClaimInvestAddAdjusterPartyNode;
        }
        public XmlNode ClaimInvestAddAdjusterPartyGeneraPartyNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddAdjusterPartyNode, "GeneralPartyInfo");
            }
        }

        public XmlNode putClaimInvestAddAdjusterPartyGeneralPartyNode
        {
            get
            {
                if (ClaimInvestAddAdjusterPartyGeneraPartyNode == null)
                    XML.XMLaddNode(putClaimInvestAddAdjusterPartyNode, "GeneralPartyInfo", sClaimsInvestAddAdjusterPartyOrder);
                return ClaimInvestAddAdjusterPartyGeneraPartyNode;
            }
        }
        public XmlNode ClaimInvestAddAdjusterPartyGeneralPartyNameNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddAdjusterPartyGeneraPartyNode, "NameInfo");
            }
        }

        public XmlNode putClaimInvestAddAdjusterPartyGeneralPartyNameNode
        {
            get
            {
                if (ClaimInvestAddAdjusterPartyGeneralPartyNameNode == null)
                    XML.XMLaddNode(putClaimInvestAddAdjusterPartyGeneralPartyNode, "NameInfo", sClaimsInvestAddAdjusterPartyGeneralPartyOrder);
                return ClaimInvestAddAdjusterPartyGeneralPartyNameNode;
            }
        }
        public XmlNode ClaimInvestAddAdjusterPartyGeneralPartyPersonNameNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddAdjusterPartyGeneralPartyNameNode, "PersonName");
            }
        }

        public XmlNode putClaimInvestAddAdjusterPartyGeneralPartyPersonNameNode
        {
            get
            {
                if (ClaimInvestAddAdjusterPartyGeneralPartyPersonNameNode == null)
                    XML.XMLaddNode(putClaimInvestAddAdjusterPartyGeneralPartyNameNode, "PersonName", sClaimsInvestAddAdjusterPartyGeneralPartyNameOrder);
                return ClaimInvestAddAdjusterPartyGeneralPartyPersonNameNode;
            }
        }
        public string ClaimInvestAddAdjusterPartyGeneralPartyPersonFirstName
        {
            get
            {
                return Utils.getData(ClaimInvestAddAdjusterPartyGeneralPartyPersonNameNode, "GivenName");
            }
            set
            {
                Utils.putData(putClaimInvestAddAdjusterPartyGeneralPartyPersonNameNode, "GivenName", value, sClaimsInvestAddAdjusterPartyGeneralPartyNamePersonNameOrder);
            }
        }
        public string ClaimInvestAddAdjusterPartyGeneralPartyPersonLastName
        {
            get
            {
                return Utils.getData(ClaimInvestAddAdjusterPartyGeneralPartyPersonNameNode, "Surname");
            }
            set
            {
                Utils.putData(putClaimInvestAddAdjusterPartyGeneralPartyPersonNameNode, "Surname", value, sClaimsInvestAddAdjusterPartyGeneralPartyNamePersonNameOrder);
            }
        }
        public XmlNodeList ClaimInvestAddAdjusterPartyInfoList
        {
            get
            {
                if (m_xmlClaimInvestAddAdjusterPartyInfoList == null)
                    m_xmlClaimInvestAddAdjusterPartyInfoList = XML.XMLGetNodes(ClaimInvestAddAdjusterPartyNode, "AdjusterPartyInfo");
                return m_xmlClaimInvestAddAdjusterPartyInfoList;
            }
        }
        public XmlNode getClaimInvestAddAdjusterPartyInfo(bool reset)
        {
            object o = ClaimInvestAddAdjusterPartyInfoList;
            return getNode(reset, ref m_xmlClaimInvestAddAdjusterPartyInfoList, ref m_xmlClaimInvestAddAdjusterPartyInfo);
        }
        public XmlNode ClaimInvestAddAdjusterPartyInfoNode
        {
            get
            {
                return m_xmlClaimInvestAddAdjusterPartyInfo;
            }
            set
            {
                m_xmlClaimInvestAddAdjusterPartyInfo = value;
            }
        }
        public XmlNode putClaimInvestAddAdjusterPartyInfoNode
        {
            get
            {
                if (ClaimInvestAddAdjusterPartyInfoNode == null)
                    ClaimInvestAddAdjusterPartyInfoNode = XML.XMLaddNode(putClaimInvestAddAdjusterPartyNode, "AdjusterPartyInfo", sClaimsInvestAddAdjusterPartyOrder);
                return ClaimInvestAddAdjusterPartyInfoNode;
            }
        }
        public XmlNode addClaimInvestAddAdjusterPartyInfoNode()
        {
            ClaimInvestAddAdjusterPartyInfoNode = null;
            return putClaimInvestAddAdjusterPartyInfoNode;
        }        
        public string ClaimInvestAddAdjusterPartyInfoAssignmentRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddAdjusterPartyInfoNode, "AssignmentRef");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddAdjusterPartyInfoNode, "AssignmentRef", value);
            }
        }
        public string ClaimInvestAddAdjusterPartyInfoCoverageCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddAdjusterPartyInfoNode, "CoverageCd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddAdjusterPartyInfoNode, "CoverageCd", value, sClaimsInvestAddAdjusterPartyInfoOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddAdjusterPartyInfoNode, "CoverageCd", value, sClaimsInvestAddAdjusterPartyInfoOrder, "CoverageCdList", "CoverageCd", "ISOUS");  //SI06650
            }
        }
        public string ClaimInvestAddAdjusterPartyInfoLossCauseCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddAdjusterPartyInfoNode, "LossCauseCd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddAdjusterPartyInfoNode, "LossCauseCd", value, sClaimsInvestAddAdjusterPartyInfoOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddAdjusterPartyInfoNode, "LossCauseCd", value, sClaimsInvestAddAdjusterPartyInfoOrder, "LossTypeCdList", "LossTypeCd", "ISOUS");  //SI06650
            }
        }
        public XmlNodeList ClaimInvestAddAutoLossInfoList
        {
            get
            {
                if (m_xmlClaimInvestAddAutoLossInfoList == null)
                    m_xmlClaimInvestAddAutoLossInfoList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "AutoLossInfo");
                return m_xmlClaimInvestAddAutoLossInfoList;
            }
        }
        public XmlNode getClaimInvestAddAutoLossInfo(bool reset)
        {
            object o = ClaimInvestAddAutoLossInfoList;
            return getNode(reset, ref m_xmlClaimInvestAddAutoLossInfoList, ref m_xmlClaimInvestAddAutoLossInfo);
        }
        public XmlNode ClaimInvestAddAutoLossInfoNode
        {
            get
            {
                return m_xmlClaimInvestAddAutoLossInfo;
            }
            set
            {
                m_xmlClaimInvestAddAutoLossInfo = value;
            }
        }
        public XmlNode putClaimInvestAddAutoLossInfoNode
        {
            get
            {
                if (ClaimInvestAddAutoLossInfoNode == null)
                    ClaimInvestAddAutoLossInfoNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "AutoLossInfo", sClaimsInvestAddOrder);
                return ClaimInvestAddAutoLossInfoNode;
            }
        }
        public XmlNode addClaimInvestAddAutoLossInfoNode()
        {
            ClaimInvestAddAutoLossInfoNode = null;
            return putClaimInvestAddAutoLossInfoNode;
        }
        public string ClaimInvestAddAutoLossId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddAutoLossInfoNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddAutoLossInfoNode, "id", value);
            }
        }
        public string ClaimInvestAddAutoLossClaimsPartyRefs
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddAutoLossInfoNode, "ClaimsPartyRefs");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddAutoLossInfoNode, "ClaimsPartyRefs", value);
            }
        }
        public string ClaimInvestAddAutoLossManufacturerCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddAutoLossInfoNode, "ManufacturerCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddAutoLossInfoNode, "ManufacturerCd", value, sClaimsInvestAddAutoLossInfoOrder);                
            }
        }
        public XmlNode ClaimInvestAddAutoLossVehicleInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddAutoLossInfoNode, "VehInfo");
            }
        }

        public XmlNode putClaimInvestAddAutoLossVehicleInfoNode
        {
            get
            {
                if (ClaimInvestAddAutoLossVehicleInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddAutoLossInfoNode, "VehInfo", sClaimsInvestAddAutoLossInfoOrder);
                return ClaimInvestAddAutoLossVehicleInfoNode;
            }
        }
        public string ClaimInvestAddAutoLossVehicleId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddAutoLossVehicleInfoNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddAutoLossVehicleInfoNode, "id", value);
            }
        }
        public string ClaimInvestAddAutoLossVehicleModel
        {
            get
            {
                return Utils.getData(ClaimInvestAddAutoLossVehicleInfoNode, "Model");
            }
            set
            {
                Utils.putData(putClaimInvestAddAutoLossVehicleInfoNode, "Model", value, sClaimsInvestAddAutoLossVehicleInfoOrder);
            }
        }
        public string ClaimInvestAddAutoLossVehicleModelYear
        {
            get
            {
                return Utils.getData(ClaimInvestAddAutoLossVehicleInfoNode, "ModelYear");
            }
            set
            {
                Utils.putData(putClaimInvestAddAutoLossVehicleInfoNode, "ModelYear", value, sClaimsInvestAddAutoLossVehicleInfoOrder);
            }
        }
        public string ClaimInvestAddAutoLossVehicleIdentificationNumber
        {
            get
            {
                return Utils.getData(ClaimInvestAddAutoLossVehicleInfoNode, "VehIdentificationNumber");
            }
            set
            {
                Utils.putData(putClaimInvestAddAutoLossVehicleInfoNode, "VehIdentificationNumber", value, sClaimsInvestAddAutoLossVehicleInfoOrder);
            }
        }
        public string ClaimInvestAddAutoLossVehicleTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddAutoLossVehicleInfoNode, "VehTypeCd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddAutoLossVehicleInfoNode, "VehTypeCd", value, sClaimsInvestAddAutoLossVehicleInfoOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddAutoLossVehicleInfoNode, "VehTypeCd", value, sClaimsInvestAddAutoLossVehicleInfoOrder, "VehTypeCdList", "VehTypeCd", "ISOUS");  //SI06650
            }
        }
        public string ClaimInvestAddAutoLossVehicleBodyTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddAutoLossVehicleInfoNode, "VehBodyTypeCd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddAutoLossVehicleInfoNode, "VehBodyTypeCd", value, sClaimsInvestAddAutoLossVehicleInfoOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddAutoLossVehicleInfoNode, "VehBodyTypeCd", value, sClaimsInvestAddAutoLossVehicleInfoOrder, "VehBodyTypeCdList", "VehBodyTypeCd", "ISOUS"); //SI06650
            }
        }
        public XmlNodeList ClaimInvestAddPropertyLossInfoList
        {
            get
            {
                if (m_xmlClaimInvestAddPropertyLossInfoList == null)
                    m_xmlClaimInvestAddPropertyLossInfoList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "PropertyLossInfo");
                return m_xmlClaimInvestAddPropertyLossInfoList;
            }
        }
        public XmlNode getClaimInvestAddPropertyLossInfo(bool reset)
        {
            object o = ClaimInvestAddPropertyLossInfoList;
            return getNode(reset, ref m_xmlClaimInvestAddPropertyLossInfoList, ref m_xmlClaimInvestAddPropertyLossInfo);
        }
        public XmlNode ClaimInvestAddPropertyLossInfoNode
        {
            get
            {
                return m_xmlClaimInvestAddPropertyLossInfo;
            }
            set
            {
                m_xmlClaimInvestAddPropertyLossInfo = value;
            }
        }
        public XmlNode putClaimInvestAddPropertyLossInfoNode
        {
            get
            {
                if (ClaimInvestAddPropertyLossInfoNode == null)
                    ClaimInvestAddPropertyLossInfoNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "PropertyLossInfo", sClaimsInvestAddOrder);
                return ClaimInvestAddPropertyLossInfoNode;
            }
        }
        public XmlNode addClaimInvestAddPropertyLossInfoNode()
        {
            ClaimInvestAddPropertyLossInfoNode = null;
            return putClaimInvestAddPropertyLossInfoNode;
        }       
        public string ClaimInvestAddPropertyLossId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddPropertyLossInfoNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddPropertyLossInfoNode, "id", value);
            }
        }
        public string ClaimInvestAddPropertyLossClaimsPartyRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddPropertyLossInfoNode, "ClaimsPartyRefs");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddPropertyLossInfoNode, "ClaimsPartyRefs", value);
            }
        }
        // Start SI06650
        public XmlNode ClaimInvestAddPropertyLossItemInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPropertyLossInfoNode, "ItemInfo");
            }
        }

        public XmlNode putClaimInvestAddPropertyLossItemInfoNode
        {
            get
            {
                if (ClaimInvestAddPropertyLossItemInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddPropertyLossInfoNode, "ItemInfo", sClaimsInvestAddPropertyLossOrder);
                return ClaimInvestAddPropertyLossItemInfoNode;
            }
        }
        public XmlNode ClaimInvestAddPropertyLossItemInfoItemDefinitionNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPropertyLossItemInfoNode, "ItemDefinition");
            }
        }

        public XmlNode putClaimInvestAddPropertyLossItemInfoItemDefinitionNode
        {
            get
            {
                if (ClaimInvestAddPropertyLossItemInfoItemDefinitionNode == null)
                    XML.XMLaddNode(putClaimInvestAddPropertyLossItemInfoNode, "ItemDefinition", sClaimsInvestAddPropertyLossItemInfoOrder);
                return ClaimInvestAddPropertyLossItemInfoItemDefinitionNode;
            }
        }
        public string ClaimInvestAddPropertyLossItemInfoItemDefinitionItemTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "ItemTypeCd");
            }
            set
            {
                    ISOUtils.putISOCodeData(this, putClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "ItemTypeCd", value, sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder, "SubjectInsuranceCdList", "ItemTypeCd", "ISOUS");  //SI06650
            }
        }
        public string ClaimInvestAddPropertyLossItemInfoItemDefinitionManufacturer
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "Manufacturer");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "Manufacturer", value, sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder);
            }
        }
        public string ClaimInvestAddPropertyLossItemInfoItemDefinitionModel
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "Model");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "Model", value, sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder);
            }
        }
        public string ClaimInvestAddPropertyLossItemInfoItemDefinitionSerialNumber
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "SerialNumber");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "SerialNumber", value, sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder);
            }
        }
        public string ClaimInvestAddPropertyLossItemInfoItemDefinitionModelYear
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "ModelYear");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "ModelYear", value, sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder);
            }
        }
        public string ClaimInvestAddPropertyLossItemInfoItemDefinitionManufacturerCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "ManufacturerCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "ManufacturerCd", value, sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder);
            }
        }
        public string ClaimInvestAddPropertyLossItemInfoItemDefinitionModelCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "ModelCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossItemInfoItemDefinitionNode, "ModelCd", value, sClaimsInvestAddPropertyLossItemInfoItemDefinitionOrder);
            }
        }
        // End SI06650
        public XmlNode ClaimInvestAddPropertyLossWaterCraftNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPropertyLossInfoNode, "Watercraft");
            }
        }

        public XmlNode putClaimInvestAddPropertyLossWaterCraftNode
        {
            get
            {
                if (ClaimInvestAddPropertyLossWaterCraftNode == null)
                    XML.XMLaddNode(putClaimInvestAddPropertyLossInfoNode, "Watercraft", sClaimsInvestAddPropertyLossOrder);
                return ClaimInvestAddPropertyLossWaterCraftNode;
            }
        }
        public string ClaimInvestAddPropertyLossWaterCraftUnitTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossWaterCraftNode, "WaterUnitTypeCd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddPropertyLossWaterCraftNode, "WaterUnitTypeCd", value, sClaimsInvestAddPropertyLossWatercraftOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddPropertyLossWaterCraftNode, "WaterUnitTypeCd", value, sClaimsInvestAddPropertyLossWatercraftOrder, "WaterUnitTypeCdList", "WaterUnitTypeCd", "ISOUS");   //SI06650
            }
        }
        public XmlNode ClaimInvestAddPropertyLossWatercraftDefinitionNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPropertyLossWaterCraftNode, "ItemDefinition");
            }
        }

        public XmlNode putClaimInvestAddPropertyLossWatercraftDefinitionNode
        {
            get
            {
                if (ClaimInvestAddPropertyLossWatercraftDefinitionNode == null)
                    XML.XMLaddNode(putClaimInvestAddPropertyLossWaterCraftNode, "ItemDefinition", sClaimsInvestAddPropertyLossWatercraftOrder);
                return ClaimInvestAddPropertyLossWatercraftDefinitionNode;
            }
        }
        public string ClaimInvestAddPropertyLossWatercraftDefinitionItemTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossWatercraftDefinitionNode, "ItemTypeCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossWatercraftDefinitionNode, "ItemTypeCd", value, sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder);
                //SI06650 ISOUtils.putISOCodeData(this, putClaimInvestAddPropertyLossWatercraftDefinitionNode, "ItemTypeCd", value, sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder, "ItemTypeCdList", "ItemTypeCd", "ISOUS");   //SI06650
            }
        }
        public string ClaimInvestAddPropertyLossWatercraftDefinitionManufacturer
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossWatercraftDefinitionNode, "Manufacturer");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossWatercraftDefinitionNode, "Manufacturer", value, sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder);
            }
        }
        public string ClaimInvestAddPropertyLossWatercraftDefinitionModel
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossWatercraftDefinitionNode, "Model");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossWatercraftDefinitionNode, "Model", value, sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder);
            }
        }
        public string ClaimInvestAddPropertyLossWatercraftDefinitionSerialNumber
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossWatercraftDefinitionNode, "SerialNumber");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossWatercraftDefinitionNode, "SerialNumber", value, sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder);
            }
        }
        public string ClaimInvestAddPropertyLossWatercraftDefinitionModelYear
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossWatercraftDefinitionNode, "ModelYear");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossWatercraftDefinitionNode, "ModelYear", value, sClaimsInvestAddPropertyLossWatercraftItemDefinitionOrder);
            }
        }
        //Start SIAS
        public XmlNodeList ClaimInvestAddPropertyLossPropertyScheduleList
        {
            get
            {
                if (m_xmlClaimInvestAddPropertyLossPropertyScheduleList == null)
                    m_xmlClaimInvestAddPropertyLossPropertyScheduleList = XML.XMLGetNodes(ClaimInvestAddPropertyLossInfoNode, "PropertySchedule");
                return m_xmlClaimInvestAddPropertyLossPropertyScheduleList;
            }
        }
        public XmlNode getClaimInvestAddPropertyLossPropertySchedule(bool reset)
        {
            object o = ClaimInvestAddPropertyLossPropertyScheduleList;
            return getNode(reset, ref m_xmlClaimInvestAddPropertyLossPropertyScheduleList, ref m_xmlClaimInvestAddPropertyLossPropertySchedule);
        }
        public XmlNode ClaimInvestAddPropertyLossPropertyScheduleNode
        {
            get
            {
                return m_xmlClaimInvestAddPropertyLossPropertySchedule;
            }
            set
            {
                m_xmlClaimInvestAddPropertyLossPropertySchedule = value;
            }
        }

        public XmlNode putClaimInvestAddPropertyLossPropertyScheduleNode
        {
            get
            {
                if (ClaimInvestAddPropertyLossPropertyScheduleNode == null)
                    ClaimInvestAddPropertyLossPropertyScheduleNode = XML.XMLaddNode(putClaimInvestAddPropertyLossInfoNode, "PropertySchedule", sClaimsInvestAddPropertyLossOrder);
                return ClaimInvestAddPropertyLossPropertyScheduleNode;
            }
        }
        public XmlNode addClaimInvestAddPropertyLossPropertyScheduleNode()
        {
            ClaimInvestAddPropertyLossPropertyScheduleNode = null;
            return putClaimInvestAddPropertyLossPropertyScheduleNode;
        }   
        public string ClaimInvestAddPropertyLossPropertyScheduleIsSummaryIndCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossPropertyScheduleNode, "IsSummaryInd");
            }
            set
            {
                Utils.putData(putClaimInvestAddPropertyLossPropertyScheduleNode, "IsSummaryInd", value, sClaimsInvestAddPropertyLossPropertyScheduleOrder);
            }
        }
            public XmlNode ClaimInvestAddPropertyLossPropertyScheduleDefinitionNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPropertyLossPropertyScheduleNode, "ItemDefinition");
            }
        }

        public XmlNode putClaimInvestAddPropertyLossPropertyScheduleDefinitionNode
        {
            get
            {
                if (ClaimInvestAddPropertyLossPropertyScheduleDefinitionNode == null)
                    XML.XMLaddNode(putClaimInvestAddPropertyLossPropertyScheduleNode, "ItemDefinition", sClaimsInvestAddPropertyLossPropertyScheduleOrder);
                return ClaimInvestAddPropertyLossPropertyScheduleDefinitionNode;
            }
        }
        public string ClaimInvestAddPropertyLossPropertyScheduleDefinitionItemTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddPropertyLossPropertyScheduleDefinitionNode, "ItemTypeCd");
            }
            set
            {
                //SI00650 Utils.putData(putClaimInvestAddPropertyLossPropertyScheduleDefinitionNode, "ItemTypeCd", value, sClaimsInvestAddPropertyLossPropertyScheduleItemDefinitionOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddPropertyLossPropertyScheduleDefinitionNode, "ItemTypeCd", value, sClaimsInvestAddPropertyLossPropertyScheduleItemDefinitionOrder, "SubjectInsuranceCdList", "ItemTypeCd", "ISOUS");  //SI06650
            }
        }
        public XmlNode ClaimInvestAddPropertyLossClaimsSubjectInsuranceNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddPropertyLossInfoNode, "ClaimsSubjectInsuranceInfo");
            }
        }

        public XmlNode putClaimInvestAddPropertyLossClaimsSubjectInsuranceNode
        {
            get
            {
                if (ClaimInvestAddPropertyLossClaimsSubjectInsuranceNode == null)
                    XML.XMLaddNode(putClaimInvestAddPropertyLossInfoNode, "ClaimsSubjectInsuranceInfo", sClaimsInvestAddPropertyLossOrder);
                return ClaimInvestAddPropertyLossClaimsSubjectInsuranceNode;
            }
        }
        public string ClaimInvestAddPropertyLossClaimsSubjectInsuranceCode
        {
            get
            {
                //SI06650 return Utils.getData(ClaimInvestAddPropertyLossClaimsSubjectInsuranceNode, "ClaimsSubjectInsuranceCd");
                return Utils.getData(ClaimInvestAddPropertyLossClaimsSubjectInsuranceNode, "SubjectInsuranceCd");   //SI06650
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddPropertyLossClaimsSubjectInsuranceNode, "ClaimsSubjectInsuranceCd", value, sClaimsInvestAddPropertyLossClaimSubjectInsuranceOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddPropertyLossClaimsSubjectInsuranceNode, "SubjectInsuranceCd", value, sClaimsInvestAddPropertyLossClaimSubjectInsuranceOrder, "SubjectInsuranceCdList", "SubjectInsuranceCd", "ISOUS");  //SI06650
            }
        }
        public XmlNodeList ClaimInvestAddWorkCompLossInfoList
        {
            get
            {
                if (m_xmlClaimInvestAddWorkCompLossInfoList == null)
                    m_xmlClaimInvestAddWorkCompLossInfoList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "WorkCompLossInfo");
                return m_xmlClaimInvestAddWorkCompLossInfoList;
            }
        }
        public XmlNode getClaimInvestAddWorkCompLossInfo(bool reset)
        {
            object o = ClaimInvestAddWorkCompLossInfoList;
            return getNode(reset, ref m_xmlClaimInvestAddWorkCompLossInfoList, ref m_xmlClaimInvestAddWorkCompLossInfo);
        }
        public XmlNode ClaimInvestAddWorkCompLossInfoNode
        {
            get
            {
                return m_xmlClaimInvestAddWorkCompLossInfo;
            }
            set
            {
                m_xmlClaimInvestAddWorkCompLossInfo = value;
            }
        }
        public XmlNode putClaimInvestAddWorkCompLossInfoNode
        {
            get
            {
                if (ClaimInvestAddWorkCompLossInfoNode == null)
                    ClaimInvestAddWorkCompLossInfoNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "WorkCompLossInfo", sClaimsInvestAddOrder);
                return ClaimInvestAddWorkCompLossInfoNode;
            }
        }
        public XmlNode addClaimInvestAddWorkCompLossInfoNode()
        {
            ClaimInvestAddWorkCompLossInfoNode = null;
            return putClaimInvestAddWorkCompLossInfoNode;
        }     
        // Start SIAS
        public string ClaimInvestAddWorkCompLossId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddWorkCompLossInfoNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddWorkCompLossInfoNode, "id", value);
            }
        }
        // End SIAS
        public string ClaimInvestAddWorkCompLossTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddWorkCompLossInfoNode, "WCClaimTypeCd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddWorkCompLossInfoNode, "WCClaimTypeCd", value, sClaimsInvestAddWorkCompLossOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddWorkCompLossInfoNode, "WCClaimTypeCd", value, sClaimsInvestAddWorkCompLossOrder, "WCClaimTypeCdList", "WCClaimTypeCd", "ISOUS");  //SI06650
            }
        }
        public XmlNode ClaimInvestAddWorkCompLossEmployeeInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddWorkCompLossInfoNode, "EmployeeInfo");
            }
        }
        public XmlNode putClaimInvestAddWorkCompLossEmployeeInfoNode
        {
            get
            {
                if (ClaimInvestAddWorkCompLossEmployeeInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddWorkCompLossInfoNode, "EmployeeInfo", sClaimsInvestAddWorkCompLossOrder);
                return ClaimInvestAddWorkCompLossEmployeeInfoNode;
            }
        }
        public string ClaimInvestAddWorkCompLossEmployeeId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddWorkCompLossEmployeeInfoNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddWorkCompLossEmployeeInfoNode, "id", value);
            }
        }
        public string ClaimInvestAddWorkCompLossEmployeeHiredDate
        {
            get
            {
                return Utils.getData(ClaimInvestAddWorkCompLossEmployeeInfoNode, "HiredDt");
            }
            set
            {
                Utils.putData(putClaimInvestAddWorkCompLossEmployeeInfoNode, "HiredDt", value, sClaimsInvestAddWorkCompLossEmployeeOrder);
            }
        }
        public string ClaimInvestAddWorkCompLossEmploymentStatusCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddWorkCompLossEmployeeInfoNode, "EmploymentStatusCd");
            }
            set
            {
                ISOUtils.putISOCodeData(this, putClaimInvestAddWorkCompLossEmployeeInfoNode, "EmploymentStatusCd", value, sClaimsInvestAddWorkCompLossEmployeeOrder, "EmploymentStatusCdList", "EmploymentStatusCd", "ISOUS");
            }
        }
        public XmlNodeList ClaimInvestAddLitigationInfoList
        {
            get
            {
                if (m_xmlClaimInvestAddLitigationInfoList == null)
                    m_xmlClaimInvestAddLitigationInfoList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "LitigationInfo");
                return m_xmlClaimInvestAddLitigationInfoList;
            }
        }
        public XmlNode getClaimInvestAddLitigationInfo(bool reset)
        {
            object o = ClaimInvestAddLitigationInfoList;
            return getNode(reset, ref m_xmlClaimInvestAddLitigationInfoList, ref m_xmlClaimInvestAddLitigationInfo);
        }
        public XmlNode ClaimInvestAddLitigationInfoNode
        {
            get
            {
                return m_xmlClaimInvestAddLitigationInfo;
            }
            set
            {
                m_xmlClaimInvestAddLitigationInfo = value;
            }
        }
        public XmlNode putClaimInvestAddLitigationInfoNode
        {
            get
            {
                if (ClaimInvestAddLitigationInfoNode == null)
                    ClaimInvestAddLitigationInfoNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "LitigationInfo", sClaimsInvestAddOrder);
                return ClaimInvestAddLitigationInfoNode;
            }
        }
        public XmlNode addClaimInvestAddLitigationInfoNode()
        {
            ClaimInvestAddLitigationInfoNode = null;
            return putClaimInvestAddLitigationInfoNode;
        }       
        public string ClaimInvestAddLitigationId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddLitigationInfoNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddLitigationInfoNode, "id", value);
            }
        }
        public string ClaimInvestAddLitigationCourtName
        {
            get
            {
                return Utils.getData(ClaimInvestAddLitigationInfoNode, "CourtName");
            }
            set
            {
                Utils.putData(putClaimInvestAddLitigationInfoNode, "CourtName", value, sClaimsInvestAddLitigationOrder);
            }
        }
        public string ClaimInvestAddLitigationThresholdTypeCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddLitigationInfoNode, "ThresholdTypeCd");
            }
            set
            {
                //SI066Utils.putData(putClaimInvestAddLitigationInfoNode, "ThresholdTypeCd", value, sClaimsInvestAddLitigationOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddLitigationInfoNode, "ThresholdTypeCd", value, sClaimsInvestAddLitigationOrder, "ThresholdTypeCdList", "ThresholdTypeCd", "ISOUS");  //SI06650
            }
        }
        public string ClaimInvestAddLitigationThresholdStateProvCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddLitigationInfoNode, "ThresholdStateProvCd");
            }
            set
            {
                Utils.putData(putClaimInvestAddLitigationInfoNode, "ThresholdStateProvCd", value, sClaimsInvestAddLitigationOrder);
            }
        }
        public XmlNode ClaimInvestAddLitigationEventInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddLitigationInfoNode, "EventInfo");
            }
        }

        public XmlNode putClaimInvestAddLitigationEventInfoNode
        {
            get
            {
                if (ClaimInvestAddLitigationEventInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddLitigationInfoNode, "EventInfo", sClaimsInvestAddLitigationOrder);
                return ClaimInvestAddLitigationEventInfoNode;
            }
        }
        public string ClaimInvestAddLitigationEventCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddLitigationEventInfoNode, "EventCd");
            }
            set
            {
                // SI06650 Utils.putData(putClaimInvestAddLitigationEventInfoNode, "EventCd", value, sClaimsInvestAddLitigationEventOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddLitigationEventInfoNode, "EventCd", value, sClaimsInvestAddLitigationEventOrder, "EventCdList", "EventCd", "ISOUS"); // SI06650
            }
        }
        public string ClaimInvestAddLitigationEventDate
        {
            get
            {
                return Utils.getData(ClaimInvestAddLitigationEventInfoNode, "EventDt");
            }
            set
            {
                Utils.putData(putClaimInvestAddLitigationEventInfoNode, "EventDt", value, sClaimsInvestAddLitigationEventOrder);
            }
        }
        public XmlNodeList ClaimInvestAddClaimsPaymentList
        {
            get
            {
                if (m_xmlClaimInvestAddClaimsPaymentList == null)
                    m_xmlClaimInvestAddClaimsPaymentList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "ClaimsPayment");
                return m_xmlClaimInvestAddClaimsPaymentList;
            }
        }
        public XmlNode getClaimInvestAddClaimsPayment(bool reset)
        {
            object o = ClaimInvestAddClaimsPaymentList;
            return getNode(reset, ref m_xmlClaimInvestAddClaimsPaymentList, ref m_xmlClaimInvestAddClaimsPayment);
        }
        public XmlNode ClaimInvestAddClaimsPaymentNode
        {
            get
            {
                return m_xmlClaimInvestAddClaimsPayment;
            }
            set
            {
                m_xmlClaimInvestAddClaimsPayment = value;
            }
        }
        //Begin kcb
        public XmlNode putClaimInvestAddClaimsPropLossSubPaymentNode
        {
            get
            {
                if (ClaimInvestAddClaimsPaymentNode == null)
                    ClaimInvestAddClaimsPaymentNode = XML.XMLaddNode(putClaimInvestAddPropertyLossClaimsSubjectInsuranceNode, "ClaimsPayment", sClaimsInvestAddOrder);
                return ClaimInvestAddClaimsPaymentNode;
            }
        }
        //End kcb
        public XmlNode putClaimInvestAddClaimsPaymentNode
        {
            get
            {
                if (ClaimInvestAddClaimsPaymentNode == null)
                    ClaimInvestAddClaimsPaymentNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "ClaimsPayment", sClaimsInvestAddOrder);
                return ClaimInvestAddClaimsPaymentNode;
            }
        }
        //Begin kcb
        public XmlNode addClaimInvestAddClaimsPropLossSubPaymentNode()
        {
            ClaimInvestAddClaimsPaymentNode = null;
            return putClaimInvestAddClaimsPropLossSubPaymentNode;
        }        
        //End kcb
        public XmlNode addClaimInvestAddClaimsPaymentNode()
        {
            ClaimInvestAddClaimsPaymentNode = null;
            return putClaimInvestAddClaimsPaymentNode;
        }        
        public string ClaimInvestAddClaimsPaymentId
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddClaimsPaymentNode, "id");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddClaimsPaymentNode, "id", value);
            }
        }
        public string ClaimInvestAddClaimsPaymentIDRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddClaimsPaymentNode, "idref");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddClaimsPaymentNode, "idref", value);
            }
        }
        public string ClaimInvestAddClaimsPaymentClaimsPartyRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddClaimsPaymentNode, "ClaimsPartyRef");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddClaimsPaymentNode, "ClaimsPartyRef", value);
            }
        }
        public XmlNode ClaimInvestAddClaimsPaymentCoverageNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddClaimsPaymentNode, "ClaimsPaymentCovInfo");
            }
        }
        public XmlNode putClaimInvestAddClaimsPaymentCoverageNode
        {
            get
            {
                if (ClaimInvestAddClaimsPaymentCoverageNode == null)
                    XML.XMLaddNode(putClaimInvestAddClaimsPaymentNode, "ClaimsPaymentCovInfo", sClaimsInvestAddClaimsPaymentOrder);
                return ClaimInvestAddClaimsPaymentCoverageNode;
            }
        }
        public string ClaimInvestAddClaimsPaymentCoverageCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPaymentCoverageNode, "CoverageCd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddClaimsPaymentCoverageNode, "CoverageCd", value, sClaimsInvestAddClaimsPaymentCoverageOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPaymentCoverageNode, "CoverageCd", value, sClaimsInvestAddClaimsPaymentCoverageOrder, "CoverageCdList", "CoverageCd", "ISOUS");  //SI06650
            }
        }
        public string ClaimInvestAddClaimsPaymentClaimStatusCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddClaimsPaymentCoverageNode, "ClaimStatusCd");
            }
            set
            {
                //SI00650 Utils.putData(putClaimInvestAddClaimsPaymentCoverageNode, "ClaimStatusCd", value, sClaimsInvestAddClaimsPaymentCoverageOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddClaimsPaymentCoverageNode, "ClaimStatusCd", value, sClaimsInvestAddClaimsPaymentCoverageOrder, "ClaimStatusCdList", "ClaimStatusCd", "ISOUS");  //SI06650
            }
        }
        public XmlNodeList ClaimInvestAddInvestigationInfoList
        {
            get
            {
                if (m_xmlClaimInvestAddInvestigationInfoList == null)
                    m_xmlClaimInvestAddInvestigationInfoList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "InvestigationInfo");
                return m_xmlClaimInvestAddInvestigationInfoList;
            }
        }
        public XmlNode getClaimInvestAddInvestigationInfo(bool reset)
        {
            object o = ClaimInvestAddInvestigationInfoList;
            return getNode(reset, ref m_xmlClaimInvestAddInvestigationInfoList, ref m_xmlClaimInvestAddInvestigationInfo);
        }
        public XmlNode ClaimInvestAddInvestigationInfoNode
        {
            get
            {
                return m_xmlClaimInvestAddInvestigationInfo;
            }
            set
            {
                m_xmlClaimInvestAddInvestigationInfo = value;
            }
        }
        public XmlNode putClaimInvestAddInvestigationInfoNode
        {
            get
            {
                if (ClaimInvestAddInvestigationInfoNode == null)
                    ClaimInvestAddInvestigationInfoNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "InvestigationInfo", sClaimsInvestAddOrder);
                return ClaimInvestAddInvestigationInfoNode;
            }
        }
        public XmlNode addClaimInvestAddInvestigationInfoNode()
        {
            ClaimInvestAddInvestigationInfoNode = null;
            return putClaimInvestAddInvestigationInfoNode;
        }        
        public string ClaimInvestAddInvestigationItemRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddInvestigationInfoNode, "ItemRef");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddInvestigationInfoNode, "ItemRef", value);
            }
        }
        public string ClaimInvestAddInvestigationValidVINInd 
        {
            get
            {
                return Utils.getBool(ClaimInvestAddInvestigationInfoNode, "ValidVINInd","");
            }            
        }
        public string ClaimInvestAddInvestigationVehDispositionCode
        {
            get
            {
                return Utils.getData(ClaimInvestAddInvestigationInfoNode, "VehDispositionCd");
            }
            set
            {
                //SI06650 Utils.putData(putClaimInvestAddInvestigationInfoNode, "VehDispositionCd", value, sClaimsInvestAddInvestigationOrder);
                ISOUtils.putISOCodeData(this, putClaimInvestAddInvestigationInfoNode, "VehDispositionCd", value, sClaimsInvestAddInvestigationOrder, "VehDispositionCdList", "VehDispositionCd", "ISOUS");   //SI06650
            }
        }
        public XmlNode ClaimInvestAddInvestigationSalvageInfoNode
        {
            get
            {
                return XML.XMLGetNode(ClaimInvestAddInvestigationInfoNode, "SalvageInfo");
            }
        }
        public XmlNode putClaimInvestAddInvestigationSalvageInfoNode
        {
            get
            {
                if (ClaimInvestAddInvestigationSalvageInfoNode == null)
                    XML.XMLaddNode(putClaimInvestAddInvestigationInfoNode, "SalvageInfo", sClaimsInvestAddInvestigationOrder);
                return ClaimInvestAddInvestigationSalvageInfoNode;
            }
        }
        public string ClaimInvestAddInvestigationSalvageItemRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddInvestigationSalvageInfoNode, "ItemRef");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddInvestigationSalvageInfoNode, "ItemRef", value);
            }
        }
        public string ClaimInvestAddInvestigationSalvageAgencyRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddInvestigationSalvageInfoNode, "SalvageAgencyRef");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddInvestigationSalvageInfoNode, "SalvageAgencyRef", value);
            }
        }
        public string ClaimInvestAddInvestigationSalvageBuyerRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddInvestigationSalvageInfoNode, "BuyerRef");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddInvestigationSalvageInfoNode, "BuyerRef", value);
            }
        }
        public string ClaimInvestAddInvestigationSalvageDate
        {
            get
            {
                return Utils.getData(ClaimInvestAddInvestigationSalvageInfoNode, "SalvageDt");
            }
            set
            {
                Utils.putData(putClaimInvestAddInvestigationSalvageInfoNode, "SalvageDt", value, sClaimsInvestAddInvestigationSalvageOrder);
            }
        }
        public string ClaimInvestAddInvestigationSalvageOwnerRetainingSalvageInd 
        {
            get
            {
                //SIW485 return Utils.getBool(ClaimInvestAddInvestigationSalvageInfoNode, "OwnerRetainingSalvageInd","");
                return Utils.getData(ClaimInvestAddInvestigationSalvageInfoNode, "OwnerRetainingSalvageInd");  //SIW485
            }
            set
            {
                //SIW485 Utils.putBool(putClaimInvestAddInvestigationSalvageInfoNode, "OwnerRetainingSalvageInd","", value, sClaimsInvestAddInvestigationSalvageOrder);
                Utils.putData(putClaimInvestAddInvestigationSalvageInfoNode, "OwnerRetainingSalvageInd", value, sClaimsInvestAddInvestigationSalvageOrder);
            }
        }

        //Start SI06650
        public XmlNode ClaimInvestAddAddlCovInfoNode
        {
            get
            {
                return m_xmlClaimInvestAddAddlCovInfo;
            }
            set
            {
                m_xmlClaimInvestAddAddlCovInfo = value;
            }
        }
        public XmlNode putClaimInvestAddAddlCovInfoNode
        {
            get
            {
                if (ClaimInvestAddAddlCovInfoNode == null)
                    ClaimInvestAddAddlCovInfoNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "com.iso_AddCovInfo", sClaimsInvestAddOrder);
                return ClaimInvestAddAddlCovInfoNode;
            }
        }
        public XmlNode addClaimInvestAddAddlCovInfoNode()
        {
            ClaimInvestAddAddlCovInfoNode = null;
            return putClaimInvestAddAddlCovInfoNode;
        }        
        // End SI06650
        
  
        public XmlNodeList ClaimInvestAddRemarkTextList
        {
            get
            {
                if (m_xmlClaimInvestAddRemarkTextList == null)
                    m_xmlClaimInvestAddRemarkTextList = XML.XMLGetNodes(ClaimInvestigationAddRqNode, "RemarkText");
                return m_xmlClaimInvestAddRemarkTextList;
            }
        }
        public XmlNode getClaimInvestAddRemarkText(bool reset)
        {
            object o = ClaimInvestAddRemarkTextList;
            return getNode(reset, ref m_xmlClaimInvestAddRemarkTextList, ref m_xmlClaimInvestAddRemarkText);
        }
        public XmlNode ClaimInvestAddRemarkTextNode
        {
            get
            {
                return m_xmlClaimInvestAddRemarkText;
            }
            set
            {
                m_xmlClaimInvestAddRemarkText = value;
            }
        }
        public XmlNode putClaimInvestAddRemarkTextNode
        {
            get
            {
                if (ClaimInvestAddRemarkTextNode == null)
                    ClaimInvestAddRemarkTextNode = XML.XMLaddNode(putClaimInvestigationAddRqNode, "RemarkText", sClaimsInvestAddOrder);
                return ClaimInvestAddRemarkTextNode;
            }
        }
        public XmlNode addClaimInvestAddRemarkTextNode()
        {
            ClaimInvestAddRemarkTextNode = null;
            return putClaimInvestAddRemarkTextNode;
        }
        public string ClaimInvestAddRemarkTextIDRef
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddRemarkTextNode, "idref");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddRemarkTextNode, "idref", value);
            }
        }       
        public string ClaimInvestAddRemarkText
        {
            get
            {
                return XML.XMLGetAttributeValue(ClaimInvestAddRemarkTextNode, "Text");
            }
            set
            {
                XML.XMLSetAttributeValue(putClaimInvestAddRemarkTextNode, "Text", value);
            }
        }

        // Start SI06650
        private void ReadClaimInvAddCodeList()
        {
            bool bReset = true;

            slCodeList = new SortedList<string, XmlNode>();

            while (null != getClaimInvestAddCodeList(bReset))
            {
                bReset = false;

                if(!string.IsNullOrEmpty(ClaimInvestAddCodeListName))
                {
                    if(!slCodeList.ContainsKey(ClaimInvestAddCodeListName.ToUpper()))
                    {
                        slCodeList.Add(ClaimInvestAddCodeListName.ToUpper(), ClaimInvestAddCodeListNode);
                    }
                }
            }
        }

        public bool ContainsClaimInvestAddCodeListId(string sCodeListId)
        {
            return slCodeList.ContainsKey(sCodeListId.ToUpper());
        }

        // End SI06650
                
    }
}
