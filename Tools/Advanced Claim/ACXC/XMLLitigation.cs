/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 03/23/2010 | SIW254  |    AS      | Add HTML field to xml object
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/16/2010 | SIW490  |    ASM     | Tree Label   
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 01/31/2011 | SIW529  |    AS      | Fix for non-static litigation object
 * 12/26/2011 | SIW7983 |  Vineet    |  Adding six new fields on the Litigation screen.
 * ********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLLitigation : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<CLAIM>
        '  <!-- Multiple Litigation Blocks Allowed-->
        '  <LITIGATION ID="LITxxx">
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '     <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '     <LITIGATION_TYPE CODE="xx">litigation type</LITIGATION_TYPE>
        '     <MATTER_NUMBER>sdasdad</MATTER_NUMBER>
        '     <DOCKET_NUMBER>dsdasd</DOCKET_NUMBER>
        '     <SUIT_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <COURT_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <VENUE_STATE CODE="xx">state</VENUE_STATE>
        '     <COUNTY CODE="xx">county<COUNTY>
        'Start SIW7983
        '     <MATTER_NAME>sdasdad</MATTER_NAME>
        '     <V_COMPANY INDICATOR="True|False"/>
        '     <JURISDICTION CODE="xx">jurisdiction</VENUE_STATE>
        '     <FEDERAL_DISTRICT CODE="xx">district</VENUE_STATE>
        '     <COUNTRY CODE="xx">country</VENUE_STATE>  
        'End SIW7983
        '     <LITIGATION_STATUS CODE="xx">litigation status</LITIGATION_STATUS>
        '     <SUIT_STATUS CODE="xx">Status</SUIT_STATUS>
        '     <SUIT_REASON CODE="xx">reason</SUIT_REASON>
        '     <SUIT_CITY>city</SUIT_CITY>
        '     <SERVED_BY CODE="">dasdas</SERVED_BY>
        '     <MEDIA_ARBITRATION INDICATOR="True|False"/>
        '     <SUIT_IN_DEFAULT INDICATOR="True|False"/>
        '     <RESULTS_IN_DEFAULT CODE="xx">sdasda</RESULTS_IN_DEFAULT>
        '     <COURT_VENUE CODE="xx">sdsad</COURT_VENUE>
        '     <APPEAL_REASON CODE="">xsdsdasd</APPEAL_REASON>
        '     <DEMAND_ALLEGATIONS>demands and allegations block</DEMAND_ALLEGATIONS>
        '     <APPEAL_REASON_HTML>xsdsdasd</APPEAL_REASON_HTML>                         'SIW254
        '     <DEMAND_ALLEG_HTML>demands and allegations block</DEMAND_ALLEG_HTML>      'SIW254
        '      <!Multiples Allowed>
        '      <RELATED_ARBITRATION ID="ARBxxx"/>
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Demand Offers Allowed>
        '      <DEMAND_OFFER> ... </DEMAND_OFFER>
        '      <!Multiple Activity_Status Allowed>
        '      <ACTIVITY_HISTORY> ... </ACTIVITY_HISTORY>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>        
        '  </LITIGATION_INFO>
        '</CLAIM>*/

        private XMLClaim m_Claim;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLDemandOffer m_DemandOffer;
        private XMLVarDataItem m_DataItem;
        private XMLActivityHistory m_ActivityHistory;
        private XMLArbitration m_Arbitration;                   //SI04744 - Implemented in SI06333
        
        public XMLLitigation()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;

            //sThisNodeOrder = new ArrayList(24);               //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(25);               //(SI06023 - Implemented in SI06333) //SIW254
            //sThisNodeOrder = new ArrayList(27);                 //SIW254  //SIW490
            //sThisNodeOrder = new ArrayList(28);                 //SIW490 //SIW7983
            sThisNodeOrder = new ArrayList(38);                 //SIW7983
            sThisNodeOrder.Add(Constants.sStdTechKey);           //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);        //SIW490
            sThisNodeOrder.Add(Constants.sLITType);
            sThisNodeOrder.Add(Constants.sLITMatter);
            sThisNodeOrder.Add(Constants.sLITDocket);
            sThisNodeOrder.Add(Constants.sLITSuitDate);
            sThisNodeOrder.Add(Constants.sLITCourtDate);
            sThisNodeOrder.Add(Constants.sLITCity);
            sThisNodeOrder.Add(Constants.sLITCounty);
            sThisNodeOrder.Add(Constants.sLITVenueState);
            sThisNodeOrder.Add(Constants.sLITStatus);
            sThisNodeOrder.Add(Constants.sLITSuitStatus);
            sThisNodeOrder.Add(Constants.sLITSuitReason);
            sThisNodeOrder.Add(Constants.sLITServedBy);
            sThisNodeOrder.Add(Constants.sLITMediaArbitration);
            sThisNodeOrder.Add(Constants.sLITSuitInDefault);
            sThisNodeOrder.Add(Constants.sLITResultsInDefault);
            sThisNodeOrder.Add(Constants.sLITCourtVenue);
            sThisNodeOrder.Add(Constants.sLITAppealReason);
            sThisNodeOrder.Add(Constants.sLITAppealReasonHTML); //SIW254
            sThisNodeOrder.Add(Constants.sLITDemandAllegation);
            sThisNodeOrder.Add(Constants.sLITDemandAllegationHTML); //SIW254
            //sThisNodeOrder.Add(Constants.sLITRelatedArb);     //SI06333 (SI No. not found)
            sThisNodeOrder.Add(Constants.sARBNode);             //SI06333 (SI No. not found)
            sThisNodeOrder.Add(Constants.sLITPartyInvolved);
            sThisNodeOrder.Add(Constants.sLITDemandOffer);
            sThisNodeOrder.Add(Constants.sLITActivityHistory);
            sThisNodeOrder.Add(Constants.sLITCommentReference);
            sThisNodeOrder.Add(Constants.sLITDataItem);
            //sThisNodeOrder.Add(Constants.sLITechKey);           //(SI06023 - Implemented in SI06333)//SIW360
            //Start SIW7983
            sThisNodeOrder.Add(Constants.sLITMatterName);
            sThisNodeOrder.Add(Constants.sLITVCompany);
            sThisNodeOrder.Add(Constants.sLITJurisdiction);
            sThisNodeOrder.Add(Constants.sLITFederalDistrict);
            sThisNodeOrder.Add(Constants.sLITCountry);
            //End SIW7983

            sThisNodeOrder.Add(Constants.sLitDttmAdded);
            sThisNodeOrder.Add(Constants.sLitDttmUpdated);
            sThisNodeOrder.Add(Constants.sLitPreparedByUser);
            sThisNodeOrder.Add(Constants.sLitUpdatedByUser);
            sThisNodeOrder.Add(Constants.sLITClaimantLawFirm);
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sLITNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "", "", "", "");
        }

        public XmlNode Create(string sLitId, string sLitigationType, string sLitigationTypecode, string sDocket,
                              string sMatter, string sSuitDate, string sCourtDate, string sSuitReason,
                              string sSuitReasonCode, string sVenueState, string sVenueStateCode)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                ID = sLitId;
                putLitigationType(sLitigationType, sLitigationTypecode);
                Docket = sDocket;
                Matter = sMatter;
                SuitDate = sSuitDate;
                CourtDate = sCourtDate;
                putSuitReason(sSuitReason, sSuitReasonCode);
                putVenueState(sVenueState, sVenueStateCode);

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLLitigation.Create");
                return null;
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sLITIDPfx;
            }
        }

        public string ID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sLITID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sLITIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                    else
                        lid = strdata;
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lLitId = Globals.lLitId + 1;
                    lid = Globals.lLitId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sLITID, Constants.sLITIDPfx + (lid + ""));
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
                if (xmlThisNode != null)
                    DemandOffer.getFirst();
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                Node = null;
                getFirst();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Claim == null)
                {
                    m_Claim = new XMLClaim();
                    m_Claim.Litigation = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Claim);   //SIW529
                    ResetParent();
                }
                return m_Claim;
            }
            set
            {
                m_Claim = value;
                ResetParent();
                if (m_Claim != null) //SIW529
                    ((XMLXCNodeBase)m_Claim).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        private void subClearPointers()
        {
            PartyInvolved = null;
            CommentReference = null;
            DemandOffer = null;
            DataItem = null;
            ActivityHistory = null;
        }

        public string Matter
        {
            get
            {
                return Utils.getData(Node, Constants.sLITMatter);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITMatter, value, NodeOrder);
            }
        }

        public string Docket
        {
            get
            {
                return Utils.getData(Node, Constants.sLITDocket);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITDocket, value, NodeOrder);
            }
        }

        public string SuitCity
        {
            get
            {
                return Utils.getData(Node, Constants.sLITCity);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITCity, value, NodeOrder);
            }
        }

        public string DemandAllegation
        {
            get
            {
                return Utils.getData(Node, Constants.sLITDemandAllegation);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITDemandAllegation, value, NodeOrder);
            }
        }
        // Start SIW254
        public string DemandAllegationHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sLITDemandAllegationHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITDemandAllegationHTML, value, NodeOrder);
            }
        }
        // End SIW254
        public string SuitDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sLITSuitDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sLITSuitDate, value, NodeOrder);
            }
        }

        public string CourtDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sLITCourtDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sLITCourtDate, value, NodeOrder);
            }
        }

        public string MediaArbitration
        {
            get
            {
                return Utils.getBool(Node, Constants.sLITMediaArbitration, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sLITMediaArbitration, "", value, NodeOrder);
            }
        }

        public string SuitInDefault
        {
            get
            {
                return Utils.getBool(Node, Constants.sLITSuitInDefault, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sLITSuitInDefault, "", value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putLitigationType(string sDesc, string sCode)
        {
            return putLitigationType(sDesc, sCode, "");
        }

        public XmlNode putLitigationType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string LitigationType
        {
            get
            {
                return Utils.getData(Node, Constants.sLITType);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITType, value, NodeOrder);
            }
        }

        public string LitigationType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITType, value, NodeOrder);
            }
        }

        public string LitigationType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITType, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putVenueState(string sDesc, string sCode)
        {
            return putVenueState(sDesc, sCode, "");
        }

        public XmlNode putVenueState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITVenueState, sDesc, sCode, NodeOrder, scodeid);
        }

        public string VenueState
        {
            get
            {
                return Utils.getData(Node, Constants.sLITVenueState);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITVenueState, value, NodeOrder);
            }
        }

        public string VenueState_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITVenueState);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITVenueState, value, NodeOrder);
            }
        }

        public string VenueState_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITVenueState);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITVenueState, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putCounty(string sDesc, string sCode)
        {
            return putCounty(sDesc, sCode, "");
        }

        public XmlNode putCounty(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITCounty, sDesc, sCode, NodeOrder, scodeid);
        }

        public string County
        {
            get
            {
                return Utils.getData(Node, Constants.sLITCounty);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITCounty, value, NodeOrder);
            }
        }

        public string County_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITCounty);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITCounty, value, NodeOrder);
            }
        }

        public string County_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITCounty);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITCounty, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putStatus(string sDesc, string sCode)
        {
            return putStatus(sDesc, sCode, "");
        }

        public XmlNode putStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITStatus, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Status
        {
            get
            {
                return Utils.getData(Node, Constants.sLITStatus);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITStatus, value, NodeOrder);
            }
        }

        public string Status_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITStatus);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITStatus, value, NodeOrder);
            }
        }

        public string Status_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITStatus);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITStatus, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putSuitStatus(string sDesc, string sCode)
        {
            return putSuitStatus(sDesc, sCode, "");
        }

        public XmlNode putSuitStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITSuitStatus, sDesc, sCode, NodeOrder, scodeid);
        }

        public string SuitStatus
        {
            get
            {
                return Utils.getData(Node, Constants.sLITSuitStatus);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITSuitStatus, value, NodeOrder);
            }
        }

        public string SuitStatus_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITSuitStatus);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITSuitStatus, value, NodeOrder);
            }
        }


        public string PreparedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sLitPreparedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sLitPreparedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string UpdatedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sLitUpdatedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sLitUpdatedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ClaimantLawFirm
        {
            get
            {
                return Utils.getData(Node, Constants.sLITClaimantLawFirm);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sLITClaimantLawFirm, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string DttmUpdated
        {
            get
            {
                return Utils.getData(Node, Constants.sLitDttmUpdated);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sLitDttmUpdated, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmAdded
        {
            get
            {
                return Utils.getData(Node, Constants.sLitDttmAdded);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sLitDttmAdded, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }



        public string SuitStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITSuitStatus);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITSuitStatus, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putSuitReason(string sDesc, string sCode)
        {
            return putSuitReason(sDesc, sCode, "");
        }

        public XmlNode putSuitReason(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITSuitReason, sDesc, sCode, NodeOrder, scodeid);
        }

        public string SuitReason
        {
            get
            {
                return Utils.getData(Node, Constants.sLITSuitReason);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITSuitReason, value, NodeOrder);
            }
        }

        public string SuitReason_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITSuitReason);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITSuitReason, value, NodeOrder);
            }
        }

        public string SuitReason_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITSuitReason);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITSuitReason, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putServedBy(string sDesc, string sCode)
        {
            return putServedBy(sDesc, sCode, "");
        }

        public XmlNode putServedBy(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITServedBy, sDesc, sCode, NodeOrder, scodeid);
        }

        public string ServedBy
        {
            get
            {
                return Utils.getData(Node, Constants.sLITServedBy);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITServedBy, value, NodeOrder);
            }
        }

        public string ServedBy_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITServedBy);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITServedBy, value, NodeOrder);
            }
        }

        public string ServedBy_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITServedBy);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITServedBy, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putResultsInDefault(string sDesc, string sCode)
        {
            return putResultsInDefault(sDesc, sCode, "");
        }

        public XmlNode putResultsInDefault(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITResultsInDefault, sDesc, sCode, NodeOrder, scodeid);
        }

        public string ResultsInDefault
        {
            get
            {
                return Utils.getData(Node, Constants.sLITResultsInDefault);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITResultsInDefault, value, NodeOrder);
            }
        }

        public string ResultsInDefault_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITResultsInDefault);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITResultsInDefault, value, NodeOrder);
            }
        }

        public string ResultsInDefault_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITResultsInDefault);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITResultsInDefault, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putCourtVenue(string sDesc, string sCode)
        {
            return putCourtVenue(sDesc, sCode, "");
        }

        public XmlNode putCourtVenue(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITCourtVenue, sDesc, sCode, NodeOrder, scodeid);
        }

        public string CourtVenue
        {
            get
            {
                return Utils.getData(Node, Constants.sLITCourtVenue);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITCourtVenue, value, NodeOrder);
            }
        }

        public string CourtVenue_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITCourtVenue);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITCourtVenue, value, NodeOrder);
            }
        }

        public string CourtVenue_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITCourtVenue);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITCourtVenue, value, NodeOrder);
            }
        }
        //End SIW139

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //Start SIW490
        public string TreeLabel 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //End SIW490

        //Start SIW139
        public XmlNode putAppealReason(string sDesc, string sCode)
        {
            return putAppealReason(sDesc, sCode, "");
        }

        public XmlNode putAppealReason(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITAppealReason, sDesc, sCode, NodeOrder, scodeid);
        }

        public string AppealReason
        {
            get
            {
                return Utils.getData(Node, Constants.sLITAppealReason);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITAppealReason, value, NodeOrder);
            }
        }

        public string AppealReason_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITAppealReason);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITAppealReason, value, NodeOrder);
            }
        }

        public string AppealReason_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITAppealReason);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITAppealReason, value, NodeOrder);
            }
        }
        //End SIW139
        // Start SIW254
        public string AppealReasonHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sLITAppealReasonHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITAppealReasonHTML, value, NodeOrder);
            }
        }
        // End SIW254
        //Start SIW7983
        public string MatterName
        {
            get
            {
                return Utils.getData(Node, Constants.sLITMatterName);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITMatterName, value, NodeOrder);
            }
        }
        public string VCompanyFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sLITVCompany, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sLITVCompany, "", value, NodeOrder);
            }
        }

        public XmlNode putJurisdiction(string sDesc, string sCode)
        {
            return putJurisdiction(sDesc, sCode, "");
        }

        public XmlNode putJurisdiction(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITJurisdiction, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Jurisdiction
        {
            get
            {
                return Utils.getData(Node, Constants.sLITJurisdiction);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITJurisdiction, value, NodeOrder);
            }
        }

        public string Jurisdiction_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITJurisdiction);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITJurisdiction, value, NodeOrder);
            }
        }

        public string Jurisdiction_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITJurisdiction);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITJurisdiction, value, NodeOrder);
            }
        }

        public XmlNode putFederalDistrict(string sDesc, string sCode)
        {
            return putFederalDistrict(sDesc, sCode, "");
        }

        public XmlNode putFederalDistrict(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITFederalDistrict, sDesc, sCode, NodeOrder, scodeid);
        }

        public string FederalDistrict
        {
            get
            {
                return Utils.getData(Node, Constants.sLITFederalDistrict);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITFederalDistrict, value, NodeOrder);
            }
        }

        public string FederalDistrict_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITFederalDistrict);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITFederalDistrict, value, NodeOrder);
            }
        }

        public string FederalDistrict_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITFederalDistrict);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITFederalDistrict, value, NodeOrder);
            }
        }

        public XmlNode putCountry(string sDesc, string sCode)
        {
            return putCountry(sDesc, sCode, "");
        }

        public XmlNode putCountry(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLITCountry, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Country
        {
            get
            {
                return Utils.getData(Node, Constants.sLITCountry);
            }
            set
            {
                Utils.putData(putNode, Constants.sLITCountry, value, NodeOrder);
            }
        }

        public string Country_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLITCountry);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLITCountry, value, NodeOrder);
            }
        }

        public string Country_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLITCountry);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLITCountry, value, NodeOrder);
            }
        }
        //End SIW7983
        //Start SI04744 - Implemented in SI06333
        //public string RelatedArbitrationID
        //{
        //    get
        //    {
        //        string strdata = XML.XMLGetAttributeValue(Node, Constants.sLITRelatedArb);
        //        string lid = "";
        //        if (strdata != "" && strdata != null)
        //            if (StringUtils.Left(strdata, 3) == Constants.sARBIDPfx && strdata.Length >= 4)
        //                lid = StringUtils.Right(strdata, strdata.Length - 3);
        //            else
        //                lid = strdata;
        //        return lid;
        //    }
        //    set
        //    {
        //        XML.XMLSetAttributeValue(putNode, Constants.sLITRelatedArb, Constants.sARBIDPfx + value);
        //    }
        //}

        public XMLArbitration Arbitration
        {
            get
            {
                m_Arbitration = new XMLArbitration();
                m_Arbitration.Parent = this;
                //SIW529 LinkXMLObjects(m_Arbitration);
                m_Arbitration.getFirst();
                return m_Arbitration;
            }
            set
            {
                m_Arbitration = value;
            }

        }
        
        //End SI04744 - Implemented in SI06333

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        public XMLDemandOffer DemandOffer
        {
            get
            {
                if (m_DemandOffer == null)
                {
                    m_DemandOffer = new XMLDemandOffer();
                    m_DemandOffer.Parent = this;
                    //SIW529 LinkXMLObjects(m_DemandOffer);
                    m_DemandOffer.getFirst();
                }
                return m_DemandOffer;
            }
            set
            {
                m_DemandOffer = value;
            }
        }

        public XMLActivityHistory ActivityHistory
        {
            get
            {
                if (m_ActivityHistory == null)
                {
                    m_ActivityHistory = new XMLActivityHistory();
                    m_ActivityHistory.Parent = this;
                    //SIW529 LinkXMLObjects(m_ActivityHistory);
                    m_ActivityHistory.getFirst();
                }
                return m_ActivityHistory;
            }
            set
            {
                m_ActivityHistory = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getLitigation(bool reset)
        {
            return getNode(reset);
        }
        
        public XmlNode FindByID(string sID)
        {
            if (ID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (ID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }
    }
}
