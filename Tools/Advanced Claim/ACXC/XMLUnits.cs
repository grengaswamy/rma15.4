/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/06/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 08/27/2008 | SI06423 |    CB      | Field Mapping
 * 11/07/2008 | SI06423a|   KCB      | Update to previous field Mapping
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/01/2010 | SIW493  |   ASM      | Correct rest logic in NodeList
 * 09/08/2010 | SIW493  |    AS      | Fix in xc object constructor
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 12/30/2010 | SIW344  |   SW       | Liability Unit type corrected.
 * 05/24/2012 | SIN8529 |    PV      | Implemented Organization Hierarchy
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLUnit : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        ' The cXMLUnit class provides the functionality to support and manage a
        ' ClaimsPro Unit Unit Node
        '*****************************************************************************
        
        '<UNITS_INVOLVED>
        '   <UNIT UNIT_TYPE="L" ID="UNTxxx">
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  'SI06023 - Implemented in SI06333 'SIW360
        '      <UNIT_NUMBER>00001</UNIT_NUMBER>
        '      <DESCRIPTION>description</DESCRIPTION>
        '      <TREE_LABEL>Tree description</TREE_LABEL>
        '      <GENERAL_CONDITION CODE="xx">General Condition</GENERAL_CONDITION>
        '      <DATE_PURCHASED YEAR="2002" MONTH="08" DAY="01" />
        '      <ORIGINAL_COST>original cose</ORIGINAL_COST>
        '      <MARKET_VALUE>market value</MARKET_VALUE>
        '      <REPLACEMENT_VALUE>replacement value</REPLACEMENT_VALUE>
        '      <POL_HLD_IS_OWNER INDICATOR="YES|NO">                          'SI04895
        '      <OTHER_INSURANCE INDICATOR="YES|NO">                           'SI04895
        '      <LOCATION_DATA>
        '         <ADDRESS_REFERENCE ID="ADR2"></ADDRESS_REFERENCE>
        '         <LOC_STATE CODE="TX">Texas </LOC_STATE>
        '      </LOCATION_DATA>
        '      <BUILDING>
        '         <NUMBER>1</NUMBER>
        '         <DESCRIPTION>Building Description<\DESCRIPTION>
        '         <CONSTRUCTION CODE="constcode">description<CONSTRUCTION>
        '         <YEAR_OF_CONSTRUCTION>year</YEAR_OF_CONSTRUCTION>
        '         <SQUARE_FOOTAGE>SquareFoot</SQUARE_FOOTAGE>
        '      </BUILDING>
        '        <!--Multiple Buildings Allowed -->
        '      <VEHICLE MAKE="Ford" MODEL=" Explorer" YEAR="1995">
        '         <VEHICLE_TYPE CODE="PP">PP</VEH_TYPE>
        '         <SERIAL_NO>MX183273688</SERIAL_NO>
        '         <BODY_TYPE CODE="">bodytype</BODY_TYPE>
        '         <ENGINE_SIZE UNIT="CUIN|CC" CODE="xx">size</ENGINE_SIZE>
        '         <CHASSIS CODE="xx">typeofchassis</CHASSIS>
        '         <TRANSMISSION CODE="AUTO|MANUAL">TypeofTransmission</TRANSMISSION>
        '         <COLOR CODE=code>description</COLOR>
        '         <PURPOSE_OF_USE CODE="xx">purpose of use</PURPOSE_OF_USE>
        '         <GROSS_WEIGHT>weight</GROSS_WEIGHT>
        '         <DATE_LAST_SERVICE YEAR="2002" MONTH="08" DAY="01" />
        '         <TYPE_OF_SERVICE CODE="xx">type of service</TYPE_OF_SERVICE>
        '         <GARAGE_STATE CODE="xx">state</GARAGE_STATE>
        '         <REGISTRATION>
        '            <STATE_REGISTRATION>StateRegistrationNumber</STATE_REGISTRATION>
        '            <STATE CODE="SC">South Carolina</STATE>
        '            <LICENSE>licensenumber</LICENSE>
        '            <DATE YEAR="2002" MONTH="08" DAY="01" />
        '         </REGISTRATION>
        '         <LEASE INDICATOR ="YES|NO">                                    'SI04900
        '            <LEASE_NUMBER>lease number</LEASE_NUMBER>
        '            <LEASE_TERM>term</LEASE_TERM>
        '            <AMOUNT>leaseamount</AMOUNT>
        '            <DATE_EXPIRES YEAR="2002" MONTH="08" DAY="01" />
        '         </LEASE>
        '         <ORG_DEPT_EID ENTITY_ID="ENTXXX"></ORG_DEPT_EID> 'SIN8529
        '      </VEHICLE>
        '      <PARTY_INVOLVED ENTITY_ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '      <COMMENT_REFERENCE COMMENT_ID="CMTxx"/>
        '        <!-- multiple entries -->
        '      <DOCUMENT_REFERENCE DOCUMENT_ID="DOCxx"/>        'SI05360
        '        <!-- multiple entries -->                      'SI05360        
        '   </UNIT>
        '</UNITS_INVOLVED>*/

        private XmlNode m_xmlBuilding;
        private XmlNodeList m_xmlBuildingList;
        //private IEnumerator m_xmlBuildingListEnum;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLDocumentReference m_Document;
        private ArrayList sLocNodeOrder;
        private ArrayList sBldNodeOrder;
        private ArrayList sVehNodeOrder;
        private ArrayList sRegNodeOrder;
        private ArrayList sLeaseNodeOrder;
        private Dictionary<object, object> dctUntType;
        
        public XMLUnit()
        {
            //SIW529 Utils.subInitializeGlobals(XML); //SIW493
            xmlThisNode = null;
            xmlNodeList = null;
            m_Parent = null;

            //sThisNodeOrder = new ArrayList(15);                       //SI06023 - Implemented in SI06333
            //sThisNodeOrder = new ArrayList(16);                       //SI06023 - Implemented in SI06333 //SI06423
            sThisNodeOrder = new ArrayList(17);                         //SI06423 - Field mapping //SI06423a - moved sUntNonVehPropDamage
            sThisNodeOrder.Add(Constants.sStdTechKey);                  //SIW360
            sThisNodeOrder.Add(Constants.sUntNumber);
            sThisNodeOrder.Add(Constants.sUntDescription);
            sThisNodeOrder.Add(Constants.sStdTreeLabel);//SIW490
            sThisNodeOrder.Add(Constants.sUntGeneralCond);
            sThisNodeOrder.Add(Constants.sUntDatePurchased);
            sThisNodeOrder.Add(Constants.sUntOrigCost);
            sThisNodeOrder.Add(Constants.sUntMarketValue);
            sThisNodeOrder.Add(Constants.sUntReplacementValue);
            sThisNodeOrder.Add(Constants.sUntPolHldIsVehOwner);
            sThisNodeOrder.Add(Constants.sUntOtherInsurance);
            sThisNodeOrder.Add(Constants.sUntInsured);                   //SI06423 - Field mapping
            //sThisNodeOrder.Add(Constants.sUntNonVehPropDamage);        //SI06423 - Field mapping //SI06423a - Moved
            sThisNodeOrder.Add(Constants.sUntLocation);
            sThisNodeOrder.Add(Constants.sUntBuilding);
            sThisNodeOrder.Add(Constants.sUntVehicle);
            sThisNodeOrder.Add(Constants.sUntPartyInvolved);
            sThisNodeOrder.Add(Constants.sUntComment);
            sThisNodeOrder.Add(Constants.sUntDocument);
            //sThisNodeOrder.Add(Constants.sUntTechKey);                  //SI06023 - Implemented in SI06333 //SIW360

            sLocNodeOrder = new ArrayList(2);
            sLocNodeOrder.Add(Constants.sUntLocAddress);
            sLocNodeOrder.Add(Constants.sUntLocState);

            sBldNodeOrder = new ArrayList(5);
            sBldNodeOrder.Add(Constants.sUntBldngNumber);
            sBldNodeOrder.Add(Constants.sUntBldngDescription);
            sBldNodeOrder.Add(Constants.sUntBldngConstruction);
            sBldNodeOrder.Add(Constants.sUntBldngYearOfConst);
            sBldNodeOrder.Add(Constants.sUntBldngSqrFt);

            //sVehNodeOrder = new ArrayList(14);//SIN8529
            sVehNodeOrder = new ArrayList(15);//SIN8529
            sVehNodeOrder.Add(Constants.sUntVehType);
            sVehNodeOrder.Add(Constants.sUntVehSerialNo);
            sVehNodeOrder.Add(Constants.sUntVehBodyType);
            sVehNodeOrder.Add(Constants.sUntVehEngineSize);
            sVehNodeOrder.Add(Constants.sUntVehChassis);
            sVehNodeOrder.Add(Constants.sUntVehTransmission);
            sVehNodeOrder.Add(Constants.sUntVehColor);
            sVehNodeOrder.Add(Constants.sUntVehUse);
            sVehNodeOrder.Add(Constants.sUntVehGrossWgt);
            sVehNodeOrder.Add(Constants.sUntVehDateLastSrvc);
            sVehNodeOrder.Add(Constants.sUntVehTypeofService);
            sVehNodeOrder.Add(Constants.sUntVehGarageState);
            sVehNodeOrder.Add(Constants.sUntVehRegistration);
            sVehNodeOrder.Add(Constants.sUntVehLeaseNode);
            sVehNodeOrder.Add(Constants.sUntVehOrgDept);//SIN8529

            sLeaseNodeOrder = new ArrayList(4);
            sLeaseNodeOrder.Add(Constants.sUntVehLeaseNumber);
            sLeaseNodeOrder.Add(Constants.sUntVehLeaseTerm);
            sLeaseNodeOrder.Add(Constants.sUntVehLeaseAmount);
            sLeaseNodeOrder.Add(Constants.sUntVehLeaseExpDate);

            sRegNodeOrder = new ArrayList(4);
            sRegNodeOrder.Add(Constants.sUntVehStateRegNum);
            sRegNodeOrder.Add(Constants.sUntVehRegState);
            sRegNodeOrder.Add(Constants.sUntVehRegLicense);
            sRegNodeOrder.Add(Constants.sUntVehRegDate);

            dctUntType = new Dictionary<object, object>();
            dctUntType.Add(Constants.iUndefined, Constants.sUndefined);
            dctUntType.Add(Constants.sUndefined, Constants.iUndefined);
            dctUntType.Add(Constants.iUntTypeLoc, Constants.sUntTypeLoc);
            dctUntType.Add(Constants.sUntTypeLoc, Constants.iUntTypeLoc);
            dctUntType.Add(Constants.iUntTypeVeh, Constants.sUntTypeVeh);
            dctUntType.Add(Constants.sUntTypeVeh, Constants.iUntTypeVeh);
            dctUntType.Add(Constants.iUntTypeProp, Constants.sUntTypeProp);     //(SI06023- Implemented in SI06333)
            dctUntType.Add(Constants.sUntTypeProp, Constants.iUntTypeProp);     //(SI06023- Implemented in SI06333)
            dctUntType.Add(Constants.iUntTypeLiab, Constants.sUntTypeLiab);     //SIW344    
            dctUntType.Add(Constants.sUntTypeLiab, Constants.iUntTypeLiab);     //SIW344    
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sUntNode; }
        }

        public XmlNode Create(xcUntType cUnitType, string sUnitID, string sUnitNumber, string sdescription)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = (XmlNode)xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                UnitID = sUnitID;
                UnitType = cUnitType;
                UnitNumber = sUnitNumber;
                Description = sdescription;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLUnit.Create");
                return null;
            }
        }

        public string Description
        {
            get
            {
                return Utils.getData(Node, Constants.sUntDescription);
            }
            set
            {
                Utils.putData(putNode, Constants.sUntDescription, value, sThisNodeOrder);
            }
        }
        // Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, sThisNodeOrder);
            }
        }
        // End SIW490
        //Start SIN8529
        public string OrgDeptEID
        {
            get
            {

                return Utils.getEntityRef(Node, Constants.sUntVehOrgDept);
            }
            set
            {

                Utils.putEntityRef(putNode, Constants.sUntVehOrgDept, value, NodeOrder);
            }
        }
        public string OrgDeptEntDisplayName
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sUntVehOrgDept, Constants.sEntDisplayName);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sUntVehOrgDept, Constants.sEntDisplayName, value, NodeOrder);
            }
        }
        //End SIN8529
        //Start SIW139
        public XmlNode putGeneralCondition(string sDesc, string sCode)
        {
            return putGeneralCondition(sDesc, sCode, "");
        }

        public XmlNode putGeneralCondition(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sUntGeneralCond, sDesc, sCode, NodeOrder, scodeid);
        }

        public string GeneralCondition
        {
            get
            {
                return Utils.getData(Node, Constants.sUntGeneralCond);
            }
            set
            {
                Utils.putData(putNode, Constants.sUntGeneralCond, value, sThisNodeOrder);
            }
        }

        public string GeneralCondition_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sUntGeneralCond);
            }
            set
            {
                Utils.putCode(putNode, Constants.sUntGeneralCond, value, sThisNodeOrder);
            }
        }

        public string GeneralCondition_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sUntGeneralCond);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sUntGeneralCond, value, sThisNodeOrder);
            }
        }
        //End SIW139

        public string DatePurchased
        {
            get
            {
                return Utils.getDate(Node, Constants.sUntDatePurchased);
            }
            set
            {
                Utils.putDate(putNode, Constants.sUntDatePurchased, value, NodeOrder);
            }
        }

        public string OriginalCost
        {
            get
            {
                return Utils.getData(Node, Constants.sUntOrigCost);
            }
            set
            {
                Utils.putData(putNode, Constants.sUntOrigCost, value, NodeOrder);
            }
        }

        public string ReplacementValue
        {
            get
            {
                return Utils.getData(Node, Constants.sUntReplacementValue);
            }
            set
            {
                Utils.putData(putNode, Constants.sUntReplacementValue, value, NodeOrder);
            }
        }

        public string MarketValue
        {
            get
            {
                return Utils.getData(Node, Constants.sUntMarketValue);
            }
            set
            {
                Utils.putData(putNode, Constants.sUntMarketValue, value, NodeOrder);
            }
        }

        public string UnitNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sUntNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sUntNumber, value, sThisNodeOrder);
            }
        }

        public string UnitType_Code
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sUntType);
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sUntType, value, sThisNodeOrder);
            }
        }

        public xcUntType UnitType
        {
            get
            {
                string sdata;
                object stype;
                stype = xcUntType.xcUTUndefined;
                sdata = Utils.getAttribute(Node, "", Constants.sUntType);
                if (dctUntType.ContainsKey(sdata))
                    dctUntType.TryGetValue(sdata, out stype);
                return (xcUntType)stype;
            }
            set
            {
                object otype;
                if (dctUntType.ContainsKey((int)value))
                {
                    dctUntType.TryGetValue((int)value, out otype);
                    Utils.putAttribute(putNode, "", Constants.sUntType, otype.ToString(), sThisNodeOrder);
                    switch (value)
                    {
                        case xcUntType.xcUTLocation:
                            RemoveVehicle();
                            break;
                        case xcUntType.xcUTProperty://Start SI06023 - Implemented in SI06333
                            RemoveVehicle();
                            break;//End SI06023 - Implemented in SI06333
                        case xcUntType.xcUTVehicle:
                            RemoveLocation();
                            RemoveBuildings();
                            break;
                        //Start SIW344
                        case xcUntType.xcUTLiability:
                            RemoveVehicle();
                            RemoveBuildings();
                            break;
                        //End SIW344
                        case xcUntType.xcUTUndefined:
                            RemoveVehicle();
                            RemoveLocation();
                            RemoveBuildings();
                            break;
                    }
                }
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_INVALID_VALUE, "XMLUnit.UnitType", value + "", Document, Node);
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sUntIDPfx;
            }
        }

        public string UnitID
        {
            get
            {
                string strdata;
                string lid = "";
                strdata = Utils.getAttribute(Node, "", Constants.sUntID);
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sUntIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lUntId = Globals.lUntId + 1;
                    lid = Globals.lUntId + "";
                }

                Utils.putAttribute(putNode, "", Constants.sUntID, Constants.sUntIDPfx + (lid + ""), NodeOrder);
            }
        }

        public string PolHldIsVehOwnFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sUntPolHldIsVehOwner, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sUntPolHldIsVehOwner, "", value, NodeOrder);
            }
        }

        public string OtherInsuranceFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sUntOtherInsurance, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sUntOtherInsurance, "", value, NodeOrder);
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            base.InsertInDocument(Doc, nde);
            Parent.Unit = this;
            Parent.Count = Parent.Count + 1;
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        private void subClearPointers()
        {
            m_xmlBuilding = null;
            m_xmlBuildingList = null;

            PartyInvolved = null;
            CommentReference = null;
            DocumentReference = null;
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        public XMLDocumentReference DocumentReference
        {
            get
            {
                if (m_Document == null)
                {
                    m_Document = ((Utils)Utils).funSetupDocumentReference(m_Document, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Document.getFirst();  //SI06420
                }
                return m_Document;
            }
            set
            {
                m_Document = value;
            }
        }

        public new XMLUnits Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLUnits();                                       
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ((XMLUnits)m_Parent).Unit = this;   //SIW529 
                    ResetParent();
                }
                //SIW529 ((XMLUnits)m_Parent).Unit = this;
                return (XMLUnits)m_Parent;
            }
            set
            {
                m_Parent = value;
                if (m_Parent != null)
                    ((XMLUnits)m_Parent).Unit = this;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey); //SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    Create(xcUntType.xcUTLocation, "", "", "");
                return xmlThisNode;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        public XmlNode VehicleNode
        {
            get
            {
                return XML.XMLGetNode(Node, Constants.sUntVehicle);
            }
        }

        private void RemoveVehicle()
        {
            XmlNode xmlNode = VehicleNode;
            if (xmlNode != null)
                Node.RemoveChild(xmlNode);
        }

        private XmlNode putVehNode
        {
            get
            {
                if (VehicleNode == null)
                    XML.XMLaddNode(putNode, Constants.sUntVehicle, NodeOrder);
                return VehicleNode;
            }
        }

        public XmlNode putVehicle(string smake, string smodel, string syear, string sType, string stypecode,
                                  string sserialno, string sregstate, string sregstatecode,
                                  string sreglicense, string sregdate)
        {
            RemoveLocation();
            RemoveBuildings();
            VehicleMake = smake;
            VehicleModel = smodel;
            VehicleYear = syear;
            VehicleSerialNo = sserialno;
            putVehicleType(sType, stypecode);
            putVehicleRegState(sregstate, sregstatecode);
            VehicleRegDate = sregdate;
            VehicleRegLicense = sreglicense;
            return VehicleNode;
        }

        public string VehicleMake
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sUntVehicle, Constants.sUntVehMake);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sUntVehicle, Constants.sUntVehMake, value, sThisNodeOrder);
            }
        }

        public string VehicleModel
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sUntVehicle, Constants.sUntVehModel);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sUntVehicle, Constants.sUntVehModel, value, sThisNodeOrder);
            }
        }

        public string VehicleYear
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sUntVehicle, Constants.sUntVehYear);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sUntVehicle, Constants.sUntVehYear, value, sThisNodeOrder);
            }
        }

        public string VehicleSerialNo
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehSerialNo);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehSerialNo, value, sVehNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putVehicleType(string sDesc, string sCode)
        {
            return putVehicleType(sDesc, sCode, "");
        }

        public XmlNode putVehicleType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehNode, Constants.sUntVehType, sDesc, sCode, sVehNodeOrder, scodeid);
        }

        public string VehicleType
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehType);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehType, value, sVehNodeOrder);
            }
        }

        public string VehicleType_Code
        {
            get
            {
                return Utils.getCode(VehicleNode, Constants.sUntVehType);
            }
            set
            {
                Utils.putCode(putVehNode, Constants.sUntVehType, value, sVehNodeOrder);
            }
        }

        public string VehicleType_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleNode, Constants.sUntVehType);
            }
            set
            {
                Utils.putCodeID(putVehNode, Constants.sUntVehType, value, sVehNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putVehicleUse(string sDesc, string sCode)
        {
            return putVehicleUse(sDesc, sCode, "");
        }

        public XmlNode putVehicleUse(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehNode, Constants.sUntVehUse, sDesc, sCode, sVehNodeOrder, scodeid);
        }

        public string VehicleUse
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehUse);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehUse, value, sVehNodeOrder);
            }
        }

        public string VehicleUse_Code
        {
            get
            {
                return Utils.getCode(VehicleNode, Constants.sUntVehUse);
            }
            set
            {
                Utils.putCode(putVehNode, Constants.sUntVehUse, value, sVehNodeOrder);
            }
        }

        public string VehicleUse_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleNode, Constants.sUntVehUse);
            }
            set
            {
                Utils.putCodeID(putVehNode, Constants.sUntVehUse, value, sVehNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putVehicleBodyType(string sDesc, string sCode)
        {
            return putVehicleBodyType(sDesc, sCode, "");
        }

        public XmlNode putVehicleBodyType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehNode, Constants.sUntVehBodyType, sDesc, sCode, sVehNodeOrder, scodeid);
        }

        public string VehicleBodyType
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehBodyType);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehBodyType, value, sVehNodeOrder);
            }
        }

        public string VehicleBodyType_Code
        {
            get
            {
                return Utils.getCode(VehicleNode, Constants.sUntVehBodyType);
            }
            set
            {
                Utils.putCode(putVehNode, Constants.sUntVehBodyType, value, sVehNodeOrder);
            }
        }

        public string VehicleBodyType_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleNode, Constants.sUntVehBodyType);
            }
            set
            {
                Utils.putCodeID(putVehNode, Constants.sUntVehBodyType, value, sVehNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putVehicleEngineSize(string sDesc, string sCode)
        {
            return putVehicleEngineSize(sDesc, sCode, "");
        }

        public XmlNode putVehicleEngineSize(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehNode, Constants.sUntVehEngineSize, sDesc, sCode, sVehNodeOrder, scodeid);
        }

        public string VehicleEngineSize
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehEngineSize);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehEngineSize, value, sVehNodeOrder);
            }
        }

        public string VehicleEngineSize_Code
        {
            get
            {
                return Utils.getCode(VehicleNode, Constants.sUntVehEngineSize);
            }
            set
            {
                Utils.putCode(putVehNode, Constants.sUntVehEngineSize, value, sVehNodeOrder);
            }
        }

        public string VehicleEngineSize_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleNode, Constants.sUntVehEngineSize);
            }
            set
            {
                Utils.putCodeID(putVehNode, Constants.sUntVehEngineSize, value, sVehNodeOrder);
            }
        }
        //End SIW139

        public string VehicleEngineSizeUnit
        {
            get
            {
                return Utils.getAttribute(VehicleNode, Constants.sUntVehEngineSizeUnit, Constants.sUntVehEngineSize);
            }
            set
            {
                Utils.putAttribute(putVehNode, Constants.sUntVehEngineSizeUnit, Constants.sUntVehEngineSize, value, sVehNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putVehicleChassis(string sDesc, string sCode)
        {
            return putVehicleChassis(sDesc, sCode, "");
        }

        public XmlNode putVehicleChassis(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehNode, Constants.sUntVehChassis, sDesc, sCode, sVehNodeOrder, scodeid);
        }

        public string VehicleChassis
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehChassis);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehChassis, value, sVehNodeOrder);
            }
        }

        public string VehicleChassis_Code
        {
            get
            {
                return Utils.getCode(VehicleNode, Constants.sUntVehChassis);
            }
            set
            {
                Utils.putCode(putVehNode, Constants.sUntVehChassis, value, sVehNodeOrder);
            }
        }

        public string VehicleChassis_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleNode, Constants.sUntVehChassis);
            }
            set
            {
                Utils.putCodeID(putVehNode, Constants.sUntVehChassis, value, sVehNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putVehicleTransmission(string sDesc, string sCode)
        {
            return putVehicleTransmission(sDesc, sCode, "");
        }

        public XmlNode putVehicleTransmission(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehNode, Constants.sUntVehTransmission, sDesc, sCode, sVehNodeOrder, scodeid);
        }

        public string VehicleTransmission
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehTransmission);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehTransmission, value, sVehNodeOrder);
            }
        }

        public string VehicleTransimission_Code
        {
            get
            {
                return Utils.getCode(VehicleNode, Constants.sUntVehTransmission);
            }
            set
            {
                Utils.putCode(putVehNode, Constants.sUntVehTransmission, value, sVehNodeOrder);
            }
        }

        public string VehicleTransimission_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleNode, Constants.sUntVehTransmission);
            }
            set
            {
                Utils.putCodeID(putVehNode, Constants.sUntVehTransmission, value, sVehNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putVehicleColor(string sDesc, string sCode)
        {
            return putVehicleColor(sDesc, sCode, "");
        }

        public XmlNode putVehicleColor(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehNode, Constants.sUntVehColor, sDesc, sCode, sVehNodeOrder, scodeid);
        }

        public string VehicleColor
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehColor);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehColor, value, sVehNodeOrder);
            }
        }

        public string VehicleColor_Code
        {
            get
            {
                return Utils.getCode(VehicleNode, Constants.sUntVehColor);
            }
            set
            {
                Utils.putCode(putVehNode, Constants.sUntVehColor, value, sVehNodeOrder);
            }
        }

        public string VehicleColor_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleNode, Constants.sUntVehColor);
            }
            set
            {
                Utils.putCodeID(putVehNode, Constants.sUntVehColor, value, sVehNodeOrder);
            }
        }
        //End SIW139

        private XmlNode VehicleRegNode
        {
            get
            {
                return XML.XMLGetNode(VehicleNode, Constants.sUntVehRegistration);
            }
        }

        private XmlNode putVehRegNode
        {
            get
            {
                XmlNode xmlNode = VehicleRegNode;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putVehNode, Constants.sUntVehRegistration, sVehNodeOrder);
                return xmlNode;
            }
        }

        public string VehicleStateReg
        {
            get
            {
                return Utils.getData(VehicleRegNode, Constants.sUntVehStateRegNum);
            }
            set
            {
                Utils.putData(putVehRegNode, Constants.sUntVehStateRegNum, value, sRegNodeOrder);
            }
        }

        public string VehicleRegLicense
        {
            get
            {
                return Utils.getData(VehicleRegNode, Constants.sUntVehRegLicense);
            }
            set
            {
                Utils.putData(putVehRegNode, Constants.sUntVehRegLicense, value, sRegNodeOrder);
            }
        }

        public string VehicleRegDate
        {
            get
            {
                return Utils.getDate(VehicleRegNode, Constants.sUntVehRegDate);
            }
            set
            {
                Utils.putDate(putVehRegNode, Constants.sUntVehRegDate, value, sRegNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putVehicleRegState(string sDesc, string sCode)
        {
            return putVehicleRegState(sDesc, sCode, "");
        }

        public XmlNode putVehicleRegState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehRegNode, Constants.sUntVehRegState, sDesc, sCode, sRegNodeOrder, scodeid);
        }

        public string VehicleRegState
        {
            get
            {
                return Utils.getData(VehicleRegNode, Constants.sUntVehRegState);
            }
            set
            {
                Utils.putData(putVehRegNode, Constants.sUntVehRegState, value, sRegNodeOrder);
            }
        }

        public string VehicleRegState_Code
        {
            get
            {
                return Utils.getCode(VehicleRegNode, Constants.sUntVehRegState);
            }
            set
            {
                Utils.putCode(putVehRegNode, Constants.sUntVehRegState, value, sRegNodeOrder);
            }
        }

        public string VehicleRegState_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleRegNode, Constants.sUntVehRegState);
            }
            set
            {
                Utils.putCodeID(putVehRegNode, Constants.sUntVehRegState, value, sRegNodeOrder);
            }
        }
        //End SIW139

        public string GrossWeight
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehGrossWgt);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehGrossWgt, value, sVehNodeOrder);
            }
        }

        public string DateLaastService
        {
            get
            {
                return Utils.getDate(VehicleNode, Constants.sUntVehDateLastSrvc);
            }
            set
            {
                Utils.putDate(putVehNode, Constants.sUntVehDateLastSrvc, value, sVehNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putTypeOfService(string sDesc, string sCode)
        {
            return putTypeOfService(sDesc, sCode, "");
        }

        public XmlNode putTypeOfService(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehNode, Constants.sUntVehTypeofService, sDesc, sCode, sVehNodeOrder, scodeid);
        }

        public string TypeOfService
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehTypeofService);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehTypeofService, value, sVehNodeOrder);
            }
        }

        public string TypeOfService_Code
        {
            get
            {
                return Utils.getCode(VehicleNode, Constants.sUntVehTypeofService);
            }
            set
            {
                Utils.putCode(putVehNode, Constants.sUntVehTypeofService, value, sVehNodeOrder);
            }
        }

        public string TypeOfService_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleNode, Constants.sUntVehTypeofService);
            }
            set
            {
                Utils.putCodeID(putVehNode, Constants.sUntVehTypeofService, value, sVehNodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putGarageState(string sDesc, string sCode)
        {
            return putGarageState(sDesc, sCode, "");
        }

        public XmlNode putGarageState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putVehNode, Constants.sUntVehGarageState, sDesc, sCode, sVehNodeOrder, scodeid);
        }

        public string GarageState
        {
            get
            {
                return Utils.getData(VehicleNode, Constants.sUntVehGarageState);
            }
            set
            {
                Utils.putData(putVehNode, Constants.sUntVehGarageState, value, sVehNodeOrder);
            }
        }

        public string GarageState_Code
        {
            get
            {
                return Utils.getCode(VehicleNode, Constants.sUntVehGarageState);
            }
            set
            {
                Utils.putCode(putVehNode, Constants.sUntVehGarageState, value, sVehNodeOrder);
            }
        }

        public string GarageState_CodeID
        {
            get
            {
                return Utils.getCodeID(VehicleNode, Constants.sUntVehGarageState);
            }
            set
            {
                Utils.putCodeID(putVehNode, Constants.sUntVehGarageState, value, sVehNodeOrder);
            }
        }
        //End SIW139

        private XmlNode VehicleLeaseNode
        {
            get
            {
                return Utils.getNode(VehicleNode, Constants.sUntVehLeaseNode);
            }
        }

        private XmlNode putVehLeaseNode
        {
            get
            {
                if (VehicleLeaseNode == null)
                    XML.XMLaddNode(putVehNode, Constants.sUntVehLeaseNode, sVehNodeOrder);
                return VehicleLeaseNode;
            }
        }

        public string VehicleLeaseInd
        {
            get
            {
                return Utils.getBool(VehicleLeaseNode, "", "");
            }
            set
            {
                Utils.putBool(putVehLeaseNode, "", "", value, sLeaseNodeOrder);
            }
        }

        //Begin SI06423a - Moved
        ////Begin SI06423
        //public string NonVehiclePropertyDamageFlag
        //{
        //    get
        //    {
        //        return Utils.getBool(Node, Constants.sUntNonVehPropDamage, "");
        //    }
        //    set
        //    {
        //        Utils.putBool(putNode, Constants.sUntNonVehPropDamage, "", value, NodeOrder);
        //    }
        //}
        ////End SI06423
        //End SI06423a

        //Begin SI06423
        public string InsuredUnitFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sUntInsured, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sUntInsured, "", value, NodeOrder);
            }
        }
        //End SI06423

        public string VehicleLeaseNum
        {
            get
            {
                return Utils.getData(VehicleLeaseNode, Constants.sUntVehLeaseNumber);
            }
            set
            {
                Utils.putData(putVehLeaseNode, Constants.sUntVehLeaseNumber, value, sLeaseNodeOrder);
            }
        }

        public string VehicleLeaseTerm
        {
            get
            {
                return Utils.getData(VehicleLeaseNode, Constants.sUntVehLeaseTerm);
            }
            set
            {
                Utils.putData(putVehLeaseNode, Constants.sUntVehLeaseTerm, value, sLeaseNodeOrder);
            }
        }

        public string VehicleLeaseAmount
        {
            get
            {
                return Utils.getData(VehicleLeaseNode, Constants.sUntVehLeaseAmount);
            }
            set
            {
                Utils.putData(putVehLeaseNode, Constants.sUntVehLeaseAmount, value, sLeaseNodeOrder);
            }
        }

        public string VehicleLeaseExpDate
        {
            get
            {
                return Utils.getDate(VehicleLeaseNode, Constants.sUntVehLeaseExpDate);
            }
            set
            {
                Utils.putDate(putVehLeaseNode, Constants.sUntVehLeaseExpDate, value, sLeaseNodeOrder);
            }
        }

        public XmlNode LocationNode
        {
            get
            {
                return XML.XMLGetNode(Node, Constants.sUntLocation);
            }
        }

        private void RemoveLocation()
        {
            XmlNode xmlNode = LocationNode;
            if (xmlNode != null)
                Node.RemoveChild(xmlNode);
        }

        private XmlNode putLocNode
        {
            get
            {
                if (LocationNode == null)
                    XML.XMLaddNode(putNode, Constants.sUntLocation, sThisNodeOrder);
                return LocationNode;
            }
        }

        public XmlNode putLocation(string sID, string sstate, string sstatecode)
        {
            RemoveVehicle();
            LocationAddressRefID = sID;
            putLocationState(sstate, sstatecode);
            return LocationNode;
        }

        public string LocationAddressRefID
        {
            get
            {
                return Utils.getAddressRef(LocationNode, Constants.sUntLocAddress);
            }
            set
            {
                Utils.putAddressRef(putLocNode, Constants.sUntLocAddress, value, sLocNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putLocationState(string sDesc, string sCode)
        {
            return putLocationState(sDesc, sCode, "");
        }

        public XmlNode putLocationState(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putLocNode, Constants.sUntLocState, sDesc, sCode, sLocNodeOrder, scodeid);
        }

        public string LocationState
        {
            get
            {
                return Utils.getData(LocationNode, Constants.sUntLocState);
            }
            set
            {
                Utils.putData(putLocNode, Constants.sUntLocState, value, sLocNodeOrder);
            }
        }

        public string LocationState_Code
        {
            get
            {
                return Utils.getCode(LocationNode, Constants.sUntLocState);
            }
            set
            {
                Utils.putCode(putLocNode, Constants.sUntLocState, value, sLocNodeOrder);
            }
        }

        public string LocationState_CodeID
        {
            get
            {
                return Utils.getCodeID(LocationNode, Constants.sUntLocState);
            }
            set
            {
                Utils.putCodeID(putLocNode, Constants.sUntLocState, value, sLocNodeOrder);
            }
        }
        //End SIW139

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }
        
        public XmlNode getUnit(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode putBuildingNode
        {
            get
            {
                if (BuildingNode == null)
                    AddBuilding();
                return m_xmlBuilding;
            }
        }

        public XmlNode BuildingNode
        {
            get
            {
                return m_xmlBuilding;
            }
            set
            {
                m_xmlBuilding = value;
            }
        }

        public ArrayList BuildingNodeOrder
        {
            get
            {
                return sBldNodeOrder;
            }
        }

        public XmlNodeList BuildingList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlBuildingList)
                {
                    return getNodeList(Constants.sUntBuilding, ref m_xmlBuildingList, Node);
                }
                else
                {
                    return m_xmlBuildingList;
                }
                //End SIW163
            }
        }

        public int BuildingCount
        {
            get
            {
                return BuildingList.Count;
            }
        }

        public XmlNode getBuilding(bool reset)
        {
            //Start SIW163
            if (null != BuildingList)
            {
                return getNode(reset, ref m_xmlBuildingList, ref m_xmlBuilding);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        private void RemoveBuilding()
        {
            if (BuildingNode != null)
                Node.RemoveChild(m_xmlBuilding);
        }

        private void RemoveBuildings()
        {
            while (getBuilding(true) != null)
                Node.RemoveChild(m_xmlBuilding);
        }

        public XmlNode AddBuilding()
        {
            return AddBuilding("", "", "", "", "");
        }

        public XmlNode AddBuilding(string snumber, string sdescription, string sConsDesc, string sConsCode, string sConsYear)
        {
            m_xmlBuilding = XML.XMLaddNode(putNode, Constants.sUntBuilding, sThisNodeOrder);
            if (snumber != "" && snumber != null)
                BuildingNumber = snumber;
            if (sdescription != "" && sdescription != null)
                BuildingDescription = sdescription;
            if (sConsDesc != "" && sConsDesc != null)
                Construction = sConsDesc;
            if (sConsCode != "" && sConsCode != null)
                Construction_Code = sConsCode;
            if (sConsYear != "" && sConsYear != null)
                YearOfConstruction = sConsYear;
            return BuildingNode;
        }

        public string BuildingNumber
        {
            get
            {
                return Utils.getData(BuildingNode, Constants.sUntBldngNumber);
            }
            set
            {
                Utils.putData(putBuildingNode, Constants.sUntBldngNumber, value, sBldNodeOrder);
            }
        }

        public string BuildingDescription
        {
            get
            {
                return Utils.getData(BuildingNode, Constants.sUntBldngDescription);
            }
            set
            {
                Utils.putData(putBuildingNode, Constants.sUntBldngDescription, value, sBldNodeOrder);
            }
        }

        public string YearOfConstruction
        {
            get
            {
                return Utils.getData(BuildingNode, Constants.sUntBldngYearOfConst);
            }
            set
            {
                Utils.putData(putBuildingNode, Constants.sUntBldngYearOfConst, value, sBldNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putConstruction(string sDesc, string sCode)
        {
            return putConstruction(sDesc, sCode, "");
        }

        public XmlNode putConstruction(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putBuildingNode, Constants.sUntBldngConstruction, sDesc, sCode, sBldNodeOrder, scodeid);
        }

        public string Construction
        {
            get
            {
                return Utils.getData(BuildingNode, Constants.sUntBldngConstruction);
            }
            set
            {
                Utils.putData(putBuildingNode, Constants.sUntBldngConstruction, value, sBldNodeOrder);
            }
        }

        public string Construction_Code
        {
            get
            {
                return Utils.getCode(BuildingNode, Constants.sUntBldngConstruction);
            }
            set
            {
                Utils.putCode(putBuildingNode, Constants.sUntBldngConstruction, value, sBldNodeOrder);
            }
        }

        public string Construction_CodeID
        {
            get
            {
                return Utils.getCodeID(BuildingNode, Constants.sUntBldngConstruction);
            }
            set
            {
                Utils.putCodeID(putBuildingNode, Constants.sUntBldngConstruction, value, sBldNodeOrder);
            }
        }
        //End SIW139

        public string SquareFootage
        {
            get
            {
                return Utils.getData(BuildingNode, Constants.sUntBldngSqrFt);
            }
            set
            {
                Utils.putData(putBuildingNode, Constants.sUntBldngSqrFt, value, sBldNodeOrder);
            }
        }

        public XmlNode getUnitbyID(string sID)
        {
            if (UnitID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (UnitID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getUnitbyNumber(string snum)
        {
            if (UnitNumber != snum)
            {
                getFirst();
                while (Node != null)
                {
                    if (UnitNumber == snum)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getUnitbySerialNumber(string snum)
        {
            if (VehicleSerialNo != snum)
            {
                getFirst();
                while (Node != null)
                {
                    if (UnitType == xcUntType.xcUTVehicle && VehicleSerialNo == snum)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getUnitbyRegistration(string sste, string snum)
        {
            if (VehicleRegState != sste || VehicleRegLicense != snum)
            {
                getFirst();
                while (Node != null)
                {
                    if (UnitType == xcUntType.xcUTVehicle && VehicleRegState_Code == sste && VehicleRegLicense == snum)
                        break;
                    getNext();
                }
            }
            return Node;
        }
        //Start SIW493
        public XmlNode getUnitbyTechkey(string stc)
        {
            bool rst = false;
            if (stc != Techkey)
            {
                rst = true;
                while (getUnit(rst) != null)
                {
                    if (Techkey == stc)
                        break;
                }
            }
            return Node;
        }
        //End SIW493

        public XmlNode getBuildingbyNumber(string snum)
        {
            if (BuildingNumber != snum)
            {
                getBuilding(true);
                while (BuildingNode != null)
                {
                    if (BuildingNumber == snum)
                        break;
                    getBuilding(false);
                }
            }
            return BuildingNode;
        }
    }

    public class XMLUnits : XMLXCNode
    {
        /*<INVOLVED_UNITS COUNT="unitcount">
        '   <UNIT UNIT_TYPE="L" ID="UNTxxx">
        '     ......
        '   </UNIT>
        '</INVOLVED_UNITS>*/

        XMLUnit m_Unit;
        
        public XMLUnits()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            m_Unit = null;

            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sUntNode);

            Utils.subSetupDocumentNodeOrder();

            Node = Utils.getNode(Parent.Node, NodeName);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sUntsInvolved; }
        }

        public override XmlNode Create()
        {
            return Create(null);
        }

        public XmlNode Create(XmlDocument xmlDoc)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = (XmlNode)xmlElement;

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLUnits.Create");
                return null;
            }
        }

        public int Count
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sUntsCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sUntsCount, value + "", sThisNodeOrder);
            }
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLClaim();
                    ((XMLClaim)m_Parent).InvolvedUnits = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return (XMLClaim)m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            //Start SIW529
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = XML.XMLGetNode(Parent.Node, NodeName);
                return xmlThisNode;
            }
            //End SIW529
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            //Node = XML.XMLGetNode(Parent.Node, NodeName); //SIW529
            Node = null; //SIW529
        }

        private void subClearPointers()
        {
            Unit = null;
        }

        public XmlNode getUnit(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return Unit.getUnit(reset);
        }

        public XMLUnit Unit
        {
            get
            {
                if (m_Unit == null)
                {
                    m_Unit = new XMLUnit();
                    m_Unit.Parent = this;
                    //SIW529 LinkXMLObjects(m_Unit);
                    //m_Unit.getUnit(Constants.xcGetFirst);   //SI06420  //SI06488
                }
                //Begin SI06488
                if (Node != null)
                {
                    if (m_Unit.Node == null)
                        m_Unit.getUnit(Constants.xcGetFirst);
                }
                //End SI06488
                return m_Unit;
            }
            set
            {
                m_Unit = value;
            }
        }

        public XmlNode addUnit(xcUntType cType, string sID, string snum, string sDesc)
        {
            return Unit.Create(cType, sID, snum, sDesc);
        }

        public XmlNode getUnitbyID(XmlDocument xdoc, string sID)
        {
            if (xdoc != null)
                Document = xdoc;
            return Unit.getUnitbyID(sID);
        }

        public XmlNode getUnitbyNumber(XmlDocument xdoc, string snum)
        {
            if (xdoc != null)
                Document = xdoc;
            return Unit.getUnitbyNumber(snum);
        }

        public XmlNode getUnitbyRegistration(XmlDocument xdoc, string sste, string snum)
        {
            if (xdoc != null)
                Document = xdoc;
            return Unit.getUnitbyRegistration(sste, snum);
        }

        public XmlNode getUnitbySerialNumber(XmlDocument xdoc, string snum)
        {
            if (xdoc != null)
                Document = xdoc;
            return Unit.getUnitbySerialNumber(snum);
        }

        public XmlNode getBuilding(bool rst)
        {
            return Unit.getBuilding(rst);
        }

        public XmlNode getBuildingbyNumber(string snum)
        {
            return Unit.getBuildingbyNumber(snum);
        }
    }
}
