﻿/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 09/17/2011 | SIW7531 |    AP      |  Initial Version of Out of Office.
**********************************************************************************************
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLOutOfOffice : XMLXCNodeWithList
    {
        private XmlNode m_xmlOutOfOffice;
        private XmlNode m_xmlUsers;
        private XmlNode m_xmlUser;
        private XmlNodeList m_xmlGroupList;
        private XmlNode m_xmlGroup;
        private XmlNode m_xmlGroups;
        private XmlNodeList m_xmlUserList;
        public XMLOutOfOffice()
        {
            xmlThisNode = null;
            xmlNodeList = null;
            sThisNodeOrder = new ArrayList(7);
            sThisNodeOrder.Add(Constants.sAssignedUser);
            sThisNodeOrder.Add(Constants.sTaskId); 
            sThisNodeOrder.Add(Constants.sLeavingDate);
            sThisNodeOrder.Add(Constants.sLeavingTime);
            sThisNodeOrder.Add(Constants.sReturningDate);
            sThisNodeOrder.Add(Constants.sReturningTime);
            sThisNodeOrder.Add(Constants.sComment);
        }

        public override XmlNode Create()
        {
            return putUser("","","", "", "", "", "","");
        }
        public XmlNode Create(string sAssignedUserId, string sTaskId, string sLeavingDate, string sLeavingTime, string sReturningDate, string sReturniingTime, string sComment)
        {
            return CreateUser(sAssignedUserId, sTaskId, sLeavingDate, sLeavingTime, sReturningDate, sReturniingTime, sComment);
        }
        public XmlNode CreateUser(string sAssignedUserId, string sTaskId, string sLeavingDate, string sLeavingTime, string sReturningDate, string sReturniingTime, string sComment)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;
            xmlThisNode = null;
            Errors.Clear();
            xmlDocument = Document;
            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();
                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false, null, null);
                Node = xmlElement;
                xmlDocFrag.AppendChild(xmlElement);
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                AssignedUser = sAssignedUserId;
                TaskId = sTaskId;
                ReturningDate  = sReturningDate;
                ReturningTime  = sReturniingTime;
                LeavingDate = sLeavingDate;
                LeavingTime = sLeavingTime;
                Comment = sComment;
                InsertInDocument(null, null);
                XML.Blanking = bblanking;   
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLComment.Create");
                return null;
            }
        }
        //Update
        public XmlNode UserOutOfOffice
        {
            get
            {
                return m_xmlOutOfOffice;
            }
            set
            {
                m_xmlOutOfOffice = value;
            }
        }
        public XmlNode getUser(bool reset)
        {
            object o = UserList;
            return getNode(reset, ref m_xmlUserList, ref m_xmlUser);
        }
        public XmlNodeList UserList
        {
            get
            {
                if (null == m_xmlUserList)
                {
                    return getNodeList(Constants.sSecUser, ref m_xmlUserList, UsersNode);
                }
                else
                {
                    return m_xmlUserList;
                }
            }
        }
        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        private void subClearPointers()
        {
            m_xmlUser = null;
            m_xmlUsers = null;
        }
        //Node that will be  returned
        protected override string DefaultNodeName
        {
            get { return Constants.sSecSecurity; }
        }
        public string AssignedUser
        {
            get
            {
                return Utils.getData(UserNode, Constants.sAssignedUser);
            }
            set
            {
                Utils.putData(putUserNode, Constants.sAssignedUser, value, NodeOrder);
            }
        }

        public string CalenderId
        {
            get
            {
                return Utils.getData(UserNode, Constants.sCalenderId);
            }
            set
            {
                Utils.putData(putUserNode, Constants.sCalenderId, value, NodeOrder);
            }
        }

        public string TaskId
        {
            get
            {
                return Utils.getData(UserNode, Constants.sTaskId);
            }
            set
            {
                Utils.putData(putUserNode, Constants.sTaskId, value, NodeOrder);
            }
        }

        public string LeavingDate
        {
            get
            {
                return Utils.getData(UserNode, Constants.sLeavingDate);
            }
            set
            {
                Utils.putData(putUserNode, Constants.sLeavingDate, value, NodeOrder);
            }
        }

        public string LeavingTime
        {
            get
            {
                return Utils.getData(UserNode, Constants.sLeavingTime);
            }
            set
            {
                Utils.putData(putUserNode, Constants.sLeavingTime, value, NodeOrder);
            }
        }

        public string ReturningDate
        {
            get
            {
                return Utils.getData(UserNode, Constants.sReturningDate);
            }
            set
            {
                Utils.putData(putUserNode, Constants.sReturningDate, value, NodeOrder);
            }
        }

        public string ReturningTime
        {
            get
            {
                return Utils.getData(UserNode, Constants.sReturningTime);
            }
            set
            {
                Utils.putData(putUserNode, Constants.sReturningTime, value, NodeOrder);
            }
        }

        public string Comment
        {
            get
            {
                return Utils.getData(UserNode, Constants.sComment);
            }
            set
            {
                Utils.putData(putUserNode, Constants.sComment, value, NodeOrder);
            }
        }
        //Node that is returned
        public XmlNode UserNode
        {
            get
            {
                return m_xmlUser;
            }
            set
            {
                m_xmlUser = value;
            }
        }

        public int UserCount
        {
            get
            {
                string strdata = Utils.getAttribute(UsersNode, "", Constants.sSecUserCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putUsersNode, "", Constants.sSecUserCount, value + "", Utils.sNodeOrder);	//SIW485
            }
        }
        public XmlNode putUsersNode
        {
            get
            {
                if (UsersNode == null)
                {
                    UsersNode = XML.XMLaddNode(putNode, Constants.sSecUsers, NodeOrder);                    
                    UserCount = 0;
                }
                return UsersNode;
            }
        }
        public XmlNode putUserNode
        {
            get
            {
                if (UserNode == null)
                    
                    UserNode = XML.XMLaddNode(putUsersNode, Constants.sSecUser, Utils.sNodeOrder);	//SIW485
                return UserNode;
            }
        }
        public XmlNode UsersNode
        {
            get
            {
                if (m_xmlUsers == null)
                    m_xmlUsers = XML.XMLGetNode(Node, Constants.sSecUsers);
                return m_xmlUsers;
            }
            set
            {
                m_xmlUsers = value;
            }
        }
        public XmlNode addUser()
        {
            UserOutOfOffice = null;
            UserCount = UserCount + 1;
            return putUserNode;
        }

        public XmlNode putUser(int sCalenderId, string sAssignedUser, string sTaskId, string sLeavingDate, string sLeavingTime, string sReturningDate, string sReturningTime, string sComment)
        {
            return putUser(sCalenderId + "", sAssignedUser, sTaskId, sLeavingDate, sLeavingTime, sReturningDate, sReturningTime, sComment );
        }

        public XmlNode putUser(string sCalenderId, string sAssignedUser,string sTaskId, string sLeavingDate, string sLeavingTime, string sReturningDate, string sReturningTime, string sComment)
        {
            if (sCalenderId != "" && sCalenderId != null)
                CalenderId = sCalenderId;
            if (sAssignedUser != "" && sAssignedUser != null)
                AssignedUser = sAssignedUser;
            if (sTaskId != "" && sTaskId != null)
                TaskId = sTaskId;
            if (sLeavingDate != "" && sLeavingDate != null)
                LeavingDate = sLeavingDate;
            if (sLeavingTime != "" && sLeavingTime != null)
                LeavingTime = sLeavingTime;
            if (sReturningDate != "" && sReturningDate != null)
                ReturningDate = sReturningDate;
            if (sReturningTime != "" && sReturningTime != null)
                ReturningTime = sReturningTime;
            if (sComment != "" && sComment != null)
                Comment = sComment;
            return UserNode;
        }
    }
}
