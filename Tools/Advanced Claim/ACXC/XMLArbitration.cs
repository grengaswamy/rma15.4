/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/01/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey  
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment   
 * 08/13/2010 | SIW490  |    ASM     | Tree Label   
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLArbitration : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<CLAIM>
        '<LITIGATION>                                            'SI04744
        '<SUBROGATION>                                           'SI04744
        ' <ARBITRATION ID="ARBxxx">
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333)//SIW360
        '     <TREE_LABEL>component label</TREE_LABEL>                    'SIW490
        '     <TYPE CODE="xx">type</TYPE>
        '     <ADVERSE_CLAIM_NUM>claimnumber</ADVERSE_CLAIM_NUM>
        '     <FILE_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <HEARING_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <ADDITIONAL_ADVERSE INDICATOR="TRUE|FALSE">           'SI06023
        '     <PARTY CODE="xx">description</PARTY>                  'SI06023
        '     <STATUS CODE="xx">status</STATUS>
        '     <AWARD_AMOUNT>award</AWARD_AMOUNT>
        '      <        !Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Comments Allowed>
        '      <!Multiple Activity_Status Allowed>
        '      <ACTIVITY_HISTORY> ... </ACTIVITY_HISTORY>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>
        '  </ARBITRATION>
        '</CLAIM>*/

        private new object m_Parent;                    //SI06333
        //private XMLClaim m_Claim;                 //SI06333
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLDemandOffer m_DemandOffer;
        private XMLVarDataItem m_DataItem;
        private XMLActivityHistory m_ActivityHistory;

        public XMLArbitration()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;

            //sThisNodeOrder = new ArrayList(17);               //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(15);                 //(SI06023 - Implemented in SI06333) //SIW490
            sThisNodeOrder = new ArrayList(16);                 //SIW490
            sThisNodeOrder.Add(Constants.sStdTechKey);          //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);       //SIW490
            sThisNodeOrder.Add(Constants.sARBType);
            sThisNodeOrder.Add(Constants.sARBAdvClaim);
            sThisNodeOrder.Add(Constants.sARBFileDate);
            sThisNodeOrder.Add(Constants.sARBHearingDate);
            sThisNodeOrder.Add(Constants.sARBAwardAmount);
            sThisNodeOrder.Add(Constants.sARBCompNegligence);
            //sThisNodeOrder.Add(Constants.sARBAddAdverse);
            //sThisNodeOrder.Add(Constants.sARBStatus);
            //sThisNodeOrder.Add(Constants.sARBStatusDate);
            //sThisNodeOrder.Add(Constants.sARBStatusDesc);
            sThisNodeOrder.Add(Constants.sARBParty);
            sThisNodeOrder.Add(Constants.sStdComment);
            //sThisNodeOrder.Add(Constants.sARBRelatedSub);
            //sThisNodeOrder.Add(Constants.sARBRelatedLit);
            sThisNodeOrder.Add(Constants.sARBPartyInvolved);
            sThisNodeOrder.Add(Constants.sARBDemandOffer);
            sThisNodeOrder.Add(Constants.sARBActivityHistory);
            sThisNodeOrder.Add(Constants.sARBCommentReference);
            sThisNodeOrder.Add(Constants.sARBDataItem);
            //sThisNodeOrder.Add(Constants.sARBTechKey);                  //(SI06023 - Implemented in SI06333) //SIW360
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "");
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sARBNode; }
        }

        public XmlNode Create(string sID, string sArbitrationType, string sArbitrationTypeCode,
                              string sFileDate, string sHearingDate)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                ID = sID;
                putArbitrationType(sArbitrationType, sArbitrationTypeCode);
                FileDate = sFileDate;
                HearingDate = sHearingDate;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLArbitration.Create");
                return null;
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sARBIDPfx;
            }
        }

        public string ID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sARBID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sARBIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lArbId = Globals.lArbId + 1;
                    lid = Globals.lArbId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sARBID, Constants.sARBIDPfx + (lid + ""));
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
                if (xmlThisNode != null)
                    DemandOffer.getFirst();
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                Node = null;
                getFirst();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        //Start (SI04744 - Implemented in SI06333)
        //Default parent is XMLClaim
        //public new XMLClaim Parent
        //{
        //    get
        //    {
        //        if (m_Claim == null)
        //        {
        //            m_Claim = new XMLClaim();
        //            m_Claim.Arbitration = this;
        //            ResetParent();
        //        }
        //        return m_Claim;
        //    }
        //    set
        //    {
        //        m_Claim = value;
        //        ResetParent();
        //    }
        //}
        public new object Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    XMLClaim m_Claim = new XMLClaim();
                    m_Claim.Arbitration = this;
                    m_Parent = m_Claim;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
              
                if (value != null && value.GetType().Name.ToString() != "XMLClaim" && 
                                     value.GetType().Name.ToString() != "XMLLitigation" && 
                                     value.GetType().Name.ToString() != "XMLSubrogation"    )
                {
                   Errors.PostError(ErrorGlobalConstants.ERRC_NEED_PARENT_COMP, "XMLArbitration.Parent", value + "", Document, Node);
                   return;
                }
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }
        //End(SI04744 - Implemented in SI06333)

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        //Start SI06333
        public override XmlNode Parent_Node
        {
            get
            {
                if (Parent.GetType().Name.Contains("XMLClaim"))
                {
                    XMLClaim m_Claim = (XMLClaim)m_Parent;
                    return m_Claim.putNode;
                }
                else if (Parent.GetType().Name.Contains("XMLLitigation"))
                {
                    XMLLitigation m_Litigation = (XMLLitigation)m_Parent;
                    return m_Litigation.putNode;
                }
                else if (Parent.GetType().Name.Contains("XMLSubrogation"))
                {
                    XMLSubrogation m_Subrogation = (XMLSubrogation)m_Parent;
                    return m_Subrogation.putNode;
                }
                else return null;
                //return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                if (Parent.GetType().Name.Contains("XMLClaim"))
                {
                    XMLClaim m_Claim = (XMLClaim)m_Parent;
                    return m_Claim.NodeOrder;
                }
                else if (Parent.GetType().Name.Contains("XMLLitigation"))
                {
                    XMLLitigation m_Litigation = (XMLLitigation)m_Parent;
                    return m_Litigation.NodeOrder;
                }
                else if (Parent.GetType().Name.Contains("XMLSubrogation"))
                {
                    XMLSubrogation m_Subrogation = (XMLSubrogation)m_Parent;
                    return m_Subrogation.NodeOrder;
                }
                else return null;
                //return Parent.NodeOrder;
            }
        }
        //End SI06333

        private void subClearPointers()
        {
            PartyInvolved = null;
            CommentReference = null;
            DemandOffer = null;
            DataItem = null;
            ActivityHistory = null;
        }

        //Start SIW139
        public XmlNode putArbitrationType(string sDesc, string sCode)
        {
            return putArbitrationType(sDesc, sCode, "");
        }

        public XmlNode putArbitrationType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sARBType, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string ArbitrationType
        {
            get
            {
                return Utils.getData(Node, Constants.sARBType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sARBType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ArbitrationType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sARBType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sARBType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string ArbitrationType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sARBType);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sARBType, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        public string AdverseClaimNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sARBAdvClaim);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sARBAdvClaim, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string FileDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sARBFileDate);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sARBFileDate, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string HearingDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sARBHearingDate);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sARBHearingDate, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string AwardAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sARBAwardAmount);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sARBAwardAmount, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        //Start (SI06023 - Implemented in SI06333)
        //public string ComparativeNegligence
        //{
        //    get
        //    {
        //        return Utils.getBool(Node, Constants.sARBCompNegligence, "");
        //    }
        //    set
        //    {
        //        Utils.putBool(putNode, Constants.sARBCompNegligence, "", value, NodeOrder);
        //    }
        //}
                
        // <COMMENT>amount</COMMENT>
        public string Comment
        {
            get
            {
                return Utils.getData(Node, Constants.sStdComment);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sStdComment, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
               
        //Start SIW139
        //<PARTY CODE="xx">description</PARTY>                   
        public XmlNode putParty(string sDesc, string sCode)
        {
            return putParty(sDesc, sCode, "");
        }

        public XmlNode putParty(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sARBParty, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string Party
        {
            get
            {
                return Utils.getData(Node, Constants.sARBParty);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sARBParty, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Party_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sARBParty);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sARBParty, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End (SI06023 - Implemented in SI06333)

        public string Party_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sARBParty);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sARBParty, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139
        
        public string AdditionalAdverse
        {
            get
            {
                return Utils.getData(Node, Constants.sARBAddAdverse);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sARBAddAdverse, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public XmlNode putStatus(string sDesc, string sCode)
        {
            return putStatus(sDesc, sCode, "");
        }

        public XmlNode putStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sARBStatus, sDesc, sCode, NodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string Status
        {
            get
            {
                return Utils.getData(Node, Constants.sARBStatus);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sARBStatus, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Status_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sARBStatus);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sARBStatus, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Status_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sARBStatus);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sARBStatus, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        //Star (SI06023 - Implemented in SI06333)
        //public string StatusDate
        //{
        //    get
        //    {
        //        return Utils.getDate(Node, Constants.sARBStatusDate);
        //    }
        //    set
        //    {
        //        Utils.putDate(putNode, Constants.sARBStatusDate, value, NodeOrder);
        //    }
        //}
    
        //public string StatusDescription
        //{
        //    get
        //    {
        //        return Utils.getData(Node, Constants.sARBStatusDesc);
        //    }
        //    set
        //    {
        //        Utils.putData(putNode, Constants.sARBStatusDesc, value, NodeOrder);
        //    }
        //}
        
        //public string RelatedLitigationID
        //{
        //    get
        //    {
        //        string strdata = XML.XMLGetAttributeValue(Node, Constants.sARBRelatedLit);
        //        string lid = "";
        //        if (strdata != "" && strdata != null)
        //            if (StringUtils.Left(strdata, 3) == Constants.sLITIDPfx && strdata.Length >= 4)
        //                lid = StringUtils.Right(strdata, strdata.Length - 3);
        //            else
        //                lid = strdata;
        //        return lid;
        //    }
        //    set
        //    {
        //        XML.XMLSetAttributeValue(putNode, Constants.sARBRelatedLit, Constants.sLITIDPfx + value);
        //    }
        //}

        //public string RelatedSubrogationID
        //{
        //    get
        //    {
        //        string strdata = XML.XMLGetAttributeValue(Node, Constants.sARBRelatedSub);
        //        string lid = "";
        //        if (strdata != "" && strdata != null)
        //            if (StringUtils.Left(strdata, 3) == Constants.sSUBIDPfx && strdata.Length >= 4)
        //                lid = StringUtils.Right(strdata, strdata.Length - 3);
        //            else
        //                lid = strdata;
        //        return lid;
        //    }
        //    set
        //    {
        //        XML.XMLSetAttributeValue(putNode, Constants.sARBRelatedSub, Constants.sSUBIDPfx + value);
        //    }
        //}
        //End (SI06023 - Implemented in SI06333)

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }
        //Start (SI06023 - Implemented in SI06333)
        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey); //SIW360    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360    //SIW529 Remove local XML parameter
            }
        }
        //End (SI06023 - Implemented in SI06333)

        //Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);     //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW490

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }

        public XMLDemandOffer DemandOffer
        {
            get
            {
                if (m_DemandOffer == null)
                {
                    m_DemandOffer = new XMLDemandOffer();
                    m_DemandOffer.Parent = this;
                    //SIW529 LinkXMLObjects(m_DemandOffer);
                    m_DemandOffer.getFirst();
                }
                return m_DemandOffer;
            }
            set
            {
                m_DemandOffer = value;
            }
        }

        public XMLActivityHistory ActivityHistory
        {
            get
            {
                if (m_ActivityHistory == null)
                {
                    m_ActivityHistory = new XMLActivityHistory();
                    m_ActivityHistory.Parent = this;
                    //SIW529 LinkXMLObjects(m_ActivityHistory);
                    m_ActivityHistory.getFirst();
                }
                return m_ActivityHistory;
            }
            set
            {
                m_ActivityHistory = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getArbitration(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode FindByID(string sID)
        {
            if (ID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (ID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }
    }
}
