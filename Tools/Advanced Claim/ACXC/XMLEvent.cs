/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 11/21/2007 | 05583   |    Lau     | Add COUNTY_OF_ORIGIN
 * 06/17/2008 | SI06369 |    sw      | Implemented HTML to replace RTF in RadEditors  
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 07/08/2009 | SIW10   |   sw       | EventDescriptionHTML and LocationDescriptionHTML fields save/load
 * 05/12/2010 | SIW321  |   AP       | Event location not saving on Event page.
 * 05/24/2010 | SIW321  |   SW       | Tech Key added to XMLEvent
 * 06/01/2010 | SIW481  |   SW       | Comments performance Issue.
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label  
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 05/24/2012 | SIN8529 |    PV      | Implemented Organization Hierarchy
 ***********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLEvent : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*<EVENT ID="EVTxx">
        '   <!--Event is an optional level that can relate multiple Events to the same event-->
        '   <NUMBER>Specific Number or 'GENERATE' to generate an event number {string}</NUMBER>
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>     'SIW321
        '   <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '   <DESCRIPTION>Description of the Event {string}</DESCRIPTION>
        '   <DESCRIPTION_HTML>Description of the Event {string}in HTML format</DESCRIPTION_HTML>  //SI06369//SIW10
        '   <DATE YEAR="2002" MONTH="03" DAY="07" />
        '   <TIME HOUR="17" MINUTE="35" SECOND="04" />
        '   <DATE_REPORTED YEAR="2002" MONTH="03" DAY="07" />
        '   <TIME_REPORTED HOUR="17" MINUTE="35" SECOND="04" />
        '   <TYPE CODE="xx">Event Type</TYPE>
        '   <CATASTROPHE INTERNAL_CODE="xx" REPORTING_CODE="xx">Catastrophe</CATASTROPHE>
        '   <FICO CODE="xx">FICO</FICO>
        '   <STATUS CODE="O">Open</STATUS>
        '   <ON_PREMISES INDICATOR="YES|NO"/>
        '   <INDICATOR CODE="code">Indicator</INDICATOR>
        '   <NUMBER_OF_INJURIES>number</NUMBER_OF_INJURIES>
        '   <NUMBER_OF_FATALATIES>number</NUMBER_OF_FATALATIES>
        '   <LOCATION_DESCRIPTION>Area Description {string} [ADDRESS_TYPE]</LOCATION_DESCRIPTION>
        '   <LOC_DESC_HTML>Area Description {string} [ADDRESS_TYPE] in HTML format</LOC_DESC_HTML> //SI06369//SIW10
        '   <PRIMARY_LOCATION CODE="code">Primary Location</PRIMARY_LOCATION>
        '   <LOCATION_TYPE CODE="loctype">Location Type</LOCATION_TYPE>
        '   <ADDRESS_REFERENCE ID="ADRid  References an address in the Addresses Collection {id}"
        '                      CODE="Type of Address Code (Billing Mailing, Home, etc.) {string} [ADDRESS_TYPE]">Address Type Translated {string}</ADDRESS_REFERENCE>
        '   <PARTY_INVOLVED ENTITY_ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '   <COMMENT_REFERENCE COMMENT_ID="CMTxx"/>
        '        <!-- multiple entries -->
        '   <COUNTY_OF_ORIGIN CODE="xx">County of Origin</COUNTY_OF_ORIGIN>
        '   <ORG_DEPT_EID ENTITY_ID="ENTXXX" ></ORG_DEPT_EID> 'SIN8529
        '</EVENT>*/

        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLAddressReference m_AddressReference;
        private int lEvtId;
        private XMLVarDataItem m_DataItem;  //SI06420
        
        public XMLEvent()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;

            //sThisNodeOrder = new ArrayList(20);//SI06369
            //sThisNodeOrder = new ArrayList(22);//SI06369 //SIW490
            //sThisNodeOrder = new ArrayList(23);//SIW490//SIN8529
            sThisNodeOrder = new ArrayList(28); //SIN8529
            sThisNodeOrder.Add(Constants.sEvtNumber);
            sThisNodeOrder.Add(Constants.sStdTechKey); //SIW321
            sThisNodeOrder.Add(Constants.sStdTreeLabel); //SIW490
            sThisNodeOrder.Add(Constants.sEvtDesc);
            sThisNodeOrder.Add(Constants.sEvtDate);
            sThisNodeOrder.Add(Constants.sEvtTime);
            sThisNodeOrder.Add(Constants.sEvtReportedDate);
            sThisNodeOrder.Add(Constants.sEvtReportedTime);
            sThisNodeOrder.Add(Constants.sEvtType);
            sThisNodeOrder.Add(Constants.sEvtCatastrophe);
            sThisNodeOrder.Add(Constants.sEvtFICO);
            sThisNodeOrder.Add(Constants.sEvtStatus);
            sThisNodeOrder.Add(Constants.sEvtOnPremises);
            sThisNodeOrder.Add(Constants.sEvtIndicator);
            sThisNodeOrder.Add(Constants.sEvtNumOfInj);
            sThisNodeOrder.Add(Constants.sEvtNumOfFat);
            sThisNodeOrder.Add(Constants.sEvtLocDesc);
            sThisNodeOrder.Add(Constants.sEvtLocPrimary);
            sThisNodeOrder.Add(Constants.sEvtLocType);
            sThisNodeOrder.Add(Constants.sEvtPartyInvolved);
            sThisNodeOrder.Add(Constants.sEvtComment);
            sThisNodeOrder.Add(Constants.sEvtCountyOfOrigin);
            sThisNodeOrder.Add(Constants.sEvtDescHTML);         //SI06369
            sThisNodeOrder.Add(Constants.sEvtLocDescHTML);      //SI06369
            sThisNodeOrder.Add(Constants.sEvtOrgDept);      //SIN8529

            sThisNodeOrder.Add(Constants.sEveDttmAdded);
            sThisNodeOrder.Add(Constants.sEveDttmUpdated);
            sThisNodeOrder.Add(Constants.sEvePreparedByUser);
            sThisNodeOrder.Add(Constants.sEveUpdatedByUser);
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sEvtNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "");
        }

        public XmlNode Create(string sID, string snumber, string sType, string stypecode, string sdrpt, string strpt, string sdesc)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                EventID = sID;
                Number = snumber;
                DateReported = sdrpt;
                TimeReported = strpt;
                putEventType(sType, stypecode);
                Description = sdesc;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLEvent.Create");
                return null;
            }
        }

        public string Number
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtNumber, value, NodeOrder);
            }
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sEvtIDPfx;
            }
        }

        public string EventID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sEvtId);
                string sEId = "";

                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sEvtIDPfx && strdata.Length >= 4)
                        sEId = StringUtils.Right(strdata, strdata.Length - 3);
                return sEId;
            }
            set
            {
                string lid;
                if (value == "" || value == null || value == "0")
                {
                    lEvtId = lEvtId + 1;
                    lid = lEvtId + "";
                }
                else
                    lid = value;
                XML.XMLSetAttributeValue(putNode, Constants.sEvtId, Constants.sEvtIDPfx + lid);
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        private void subClearPointers()
        {
            PartyInvolved = null;
            CommentReference = null;
            AddressReference = null;
        }

        //Start SIW139
        public XmlNode putEventType(string sDesc, string sCode)
        {
            return putEventType(sDesc, sCode, "");
        }

        public XmlNode putEventType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEvtType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string EventType
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtType);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtType, value, NodeOrder);
            }
        }

        public string EventType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEvtType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEvtType, value, NodeOrder);
            }
        }

        public string EventType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEvtType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEvtType, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putStatus(string sDesc, string sCode)
        {
            return putStatus(sDesc, sCode, "");
        }

        public XmlNode putStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEvtStatus, sDesc, sCode, NodeOrder, scodeid);
        }


        public string PreparedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sEvePreparedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sEvePreparedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string UpdatedByUser
        {
            get
            {
                return Utils.getData(Node, Constants.sEveUpdatedByUser);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sEveUpdatedByUser, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmUpdated
        {
            get
            {
                return Utils.getData(Node, Constants.sEveDttmUpdated);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sEveDttmUpdated, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string DttmAdded
        {
            get
            {
                return Utils.getData(Node, Constants.sEveDttmAdded);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sEveDttmAdded, value, NodeOrder);    //SIW529 Remove local XML parameter
            }
        }





        public string Status
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtStatus);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtStatus, value, NodeOrder);
            }
        }

        public string Status_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEvtStatus);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEvtStatus, value, NodeOrder);
            }
        }

        public string Status_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEvtStatus);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEvtStatus, value, NodeOrder);
            }
        }
        //End SIW139

        public XmlNode putCatastrophe(string sdesc, string scode, string sRprtCode)
        {
            //Start SIW321
            //Catastrophe = sdesc;
            //Catastrophe_Code = scode;
            //Catastrophe_ReportingCode = sRprtCode;
            //return Utils.getNode(Node, Constants.sEvtCatastrophe);
            return putCatastrophe(sdesc, scode, sRprtCode, "", "");
            //End SIW321
        }
        //Start SIW321
        public XmlNode putCatastrophe(string sdesc, string scode, string sRprtCode, string vcodeid, string vRprtCodeID)
        {
            Catastrophe = sdesc;
            Catastrophe_Code = scode;
            Catastrophe_ReportingCode = sRprtCode;
            Catastrophe_CodeID = vcodeid;
            Catastrophe_ReportingCodeID = vRprtCodeID;
            return Utils.getNode(Node, Constants.sEvtCatastrophe);
        }
        public string Catastrophe_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEvtCatastrophe);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEvtCatastrophe, value, NodeOrder);
            }
        }
        public string Catastrophe_ReportingCodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEvtCatastrophe, "RID");
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEvtCatastrophe, value, NodeOrder);
            }
        }
        //End SIW321

        public string Catastrophe
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtCatastrophe);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtCatastrophe, value, NodeOrder);
            }
        }

        public string Catastrophe_Code
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sEvtCatastrophe, Constants.sEvtCatInternal);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sEvtCatastrophe, Constants.sEvtCatInternal, value, NodeOrder);
            }
        }

        public string Catastrophe_ReportingCode
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sEvtCatastrophe, Constants.sEvtCatReporting);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sEvtCatastrophe, Constants.sEvtCatReporting, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putFICO(string sDesc, string sCode)
        {
            return putFICO(sDesc, sCode, "");
        }

        public XmlNode putFICO(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEvtFICO, sDesc, sCode, NodeOrder, scodeid);
        }

        public string FICO
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtFICO);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtFICO, value, NodeOrder);
            }
        }

        public string FICO_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEvtFICO);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEvtFICO, value, NodeOrder);
            }
        }

        public string FICO_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEvtFICO);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEvtFICO, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putIndicator(string sDesc, string sCode)
        {
            return putIndicator(sDesc, sCode, "");
        }

        public XmlNode putIndicator(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEvtIndicator, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Indicator
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtIndicator);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtIndicator, value, NodeOrder);
            }
        }

        public string Indicator_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEvtIndicator);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEvtIndicator, value, NodeOrder);
            }
        }

        public string Indicator_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEvtIndicator);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEvtIndicator, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIN8529
        public string OrgDeptEID
        {
            get
            {

                return Utils.getEntityRef(Node, Constants.sEvtOrgDept);
            }
            set
            {

                Utils.putEntityRef(putNode, Constants.sEvtOrgDept, value, NodeOrder);
            }
        }
        public string OrgDeptEntDisplayName
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sEvtOrgDept, Constants.sEntDisplayName);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sEvtOrgDept, Constants.sEntDisplayName, value, NodeOrder);
            }
        }
        //End SIN8529
        public string DateReported
        {
            get
            {
                return Utils.getDate(Node, Constants.sEvtReportedDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEvtReportedDate, value, NodeOrder);
            }
        }

        public string EventDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEvtDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEvtDate, value, NodeOrder);
            }
        }

        public string EventTime
        {
            get
            {
                return Utils.getTime(Node, Constants.sEvtTime);
            }
            set
            {
                Utils.putTime(putNode, Constants.sEvtTime, value, NodeOrder);
            }
        }

        public string Description
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtDesc);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtDesc, value, NodeOrder);
            }
        }

        //Start SI06369
        public string DescriptionHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtDescHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtDescHTML, value, NodeOrder);
            }
        }
        //End SI06369

        public string NumberOfInjuries
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtNumOfInj);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtNumOfInj, value, NodeOrder);
            }
        }

        public string NumberOfFatalities
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtNumOfFat);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtNumOfFat, value, NodeOrder);
            }
        }

        public string TimeReported
        {
            get
            {
                return Utils.getTime(Node, Constants.sEvtReportedTime);
            }
            set
            {
                Utils.putTime(putNode, Constants.sEvtReportedTime, value, NodeOrder);
            }
        }

        public string LocationDescription
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtLocDesc);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtLocDesc, value, NodeOrder);
            }
        }

        //Start SI06369
        public string LocationDescriptionHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtLocDescHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtLocDescHTML, value, NodeOrder);
            }
        }
        //End SI06369

        //Start SIW139
        public XmlNode putPrimaryLocation(string sDesc, string sCode)
        {
            return putPrimaryLocation(sDesc, sCode, "");
        }

        public XmlNode putPrimaryLocation(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEvtLocPrimary, sDesc, sCode, NodeOrder, scodeid);
        }

        public string PrimaryLocation
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtLocPrimary);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtLocPrimary, value, NodeOrder);
            }
        }

        public string PrimaryLocation_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEvtLocPrimary);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEvtLocPrimary, value, NodeOrder);
            }
        }

        public string PrimaryLocation_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEvtLocPrimary);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEvtLocPrimary, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putLocationType(string sDesc, string sCode)
        {
            return putLocationType(sDesc, sCode, "");
        }

        public XmlNode putLocationType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEvtLocType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string LocationType
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtLocType);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtLocType, value, NodeOrder);
            }
        }

        public string LocationType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEvtLocType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEvtLocType, value, NodeOrder);
            }
        }

        public string LocationType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEvtLocType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEvtLocType, value, NodeOrder);
            }
        }
        //End SIW139
        
        //Start SIW139
        public XmlNode putCountyOfOrigin(string sDesc, string sCode)
        {
            return putCountyOfOrigin(sDesc, sCode, "");
        }

        //SI05583 Start
        public XmlNode putCountyOfOrigin(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEvtCountyOfOrigin, sDesc, sCode, NodeOrder, scodeid);
        }

        public string CountyOfOrigin
        {
            get
            {
                return Utils.getData(Node, Constants.sEvtCountyOfOrigin);
            }
            set
            {
                Utils.putData(putNode, Constants.sEvtCountyOfOrigin, value, NodeOrder);
            }
        }

        public string CountyOfOrigin_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEvtCountyOfOrigin);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEvtCountyOfOrigin, value, NodeOrder);
            }
        }
        //SI05583 End 

        public string CountyOfOrigin_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEvtCountyOfOrigin);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEvtCountyOfOrigin, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW321
        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);
            }
        }
        //End SIW321

        //Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //End SIW490

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        //Begin SI06420
        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }
        //End SI06420

        public XMLAddressReference AddressReference
        {
            get
            {
                if (m_AddressReference == null)
                    m_AddressReference = ((Utils)Utils).funSetupAddressReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                return m_AddressReference;
            }
            set
            {
                m_AddressReference = value;
                if (m_AddressReference != null)
                    ((Utils)Utils).funSetupAddressReference(m_AddressReference, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public new XML Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = value;
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return EventList;
            }
        }

        public XmlNodeList EventList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getEvent(XmlDocument Doc, bool reset)
        {
            if (Doc != null)
                Document = Doc;
            object o = EventList;
            return getNode(reset);
        }        
        //Start SIW481
        public XmlNode FindEvent()
        {
            return FindEvent("", "", "", null);
        }
        public XmlNode FindEvent(string TchID, string EvtID, string EvtNb, XmlDocument Doc)
        {
            bool rst = true;
            getNode(rst);
            if ((TchID != "" && Techkey == TchID) || (EvtNb != "" && Number == EvtNb) || (EvtID != "" && EventID == EvtID))
                return Node;
            while (getEvent(Doc, rst) != null)
            {
                rst = false;
                if (TchID != "")
                    if (Techkey == TchID)
                        break;
                if (EvtNb != "")
                    if (Number == EvtNb)
                        break;
                if (EvtID != "")
                    if (EventID == EvtID)
                        break;
            }
            return Node;
        }
        //End SIW481    
        public void removeEvent()
        {
            if (xmlThisNode != null)
                Node.RemoveChild(xmlThisNode);
            xmlThisNode = null;
        }

        public string OnPremiseIndicator
        {
            get
            {
                return Utils.getBool(Node, Constants.sEvtOnPremises, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sEvtOnPremises, "", value, NodeOrder);
            }
        }

        //Start SIW481
        public XmlNode FindEventbyTechID(string tid)
        {
            return FindEventbyTechID(tid, null);
        }
        public XmlNode FindEventbyTechID(string tid, XmlDocument Doc)
        {
              
            bool rst = true;
            getNode(rst);
            if (Techkey != tid)
            {
                while (getEvent(Doc, rst) != null)
                {
                    rst = false;
                    if (Techkey == tid)
                        break;
                }
            }
            return Node;
        }

        public XmlNode FindEventbyID(string eid)
        {
            return FindEventbyID(eid, null); 
        }
        public XmlNode FindEventbyID(string eid, XmlDocument Doc)
        {
            //getEvent(Doc, true);
            //while (Node != null)
            //{
            //    if (EventID == eid)
            //        break;
            //    getEvent(Doc, false);
            //}            
            bool rst = true;
            getNode(rst); 
            if (EventID != eid)
            {
                while (getEvent(Doc, rst) != null)
                {
                    rst = false;
                    if (EventID == eid)
                        break;
                }
            }
            return Node;
        }
        public XmlNode FindEventbyNumber(string nbr)
        {
            return FindEventbyNumber(nbr, null);
        }
        public XmlNode FindEventbyNumber(string nbr, XmlDocument Doc)
        {
            //getEvent(Doc, true);
            //while (Node != null)
            //{
            //    if (Number == nbr)
            //        break;
            //    getEvent(Doc, false);
            //}
            bool rst = true;
            getNode(rst);
            if (Number != nbr)
            {
                while (getEvent(Doc, rst) != null)
                {
                    rst = false;
                    if (Number == nbr)
                        break;
                }
            }
            return Node;
        }
        //End SIW481
    }
}
