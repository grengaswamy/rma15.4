/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 11/17/2008 | SI06468 |    ASM     | Code IDs
 * 11/17/2008 | SI06420 |    ASM     | Child Objects
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.Common;
using CCP.Constants;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public class XMLTrigger : XMLXCNodeWithList
    {
        /*****************************************************************************
        '<PARENT_NODE>
        ' <TRIGGERS COUNT='xxxx'>
        '  <TRIGGER ID='TRGxxx'>
        '    <TRIGGER_TYPE CODE="triggercode">Description of trigger</TRIGGER_TYPE>
        '    <DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</DATE>]
        '    <TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</TIME>]
        '    <USER>Login name of user that issued the trigger</USER>
        '    <UPLOADED INDICATOR={Y|N})>
        '    <UPLOADED_RqUID>guid for the process that uploaded the trigger</UPLOADED_RqUID>
        '    <RELATED_TABLE TABLE_ID="tableid">tablename</RELATED_TABLE>
        '    <RELATED_RECORD_KEY>recordkey</RELATED_RECORD_KEY>
        '    <SERVICE_LOG ID="SLOGxxxx">
        '      <!-- Multiple Service Logs Allowed -->
        '      <STATUS CODE="statuscode">Current status description</STATUS>
        '      <PROCESS_DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</PROCESS_DATE>]
        '      <PROCESS_TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</PROCESS_TIME>]
        '      <COMPLETE_DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</COMPLETE_DATE>]
        '      <COMPLETE_TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</COMPLETE_TIME>]
        '      <REQUEST>
        '        <NAME>service request name</NAME>
        '        <RqUID>document ID where trigger was reported</RqUID>
        '        <DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</DATE>]
        '        <TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</TIME>]
        '        <DESCRIPTION>service description</DESCRIPTION>
        '      </REQUEST>
        '      <RESPONSE>
        '        <NAME>Name of the response document</NAME>
        '        <RqUID>ID of the response document</RqUID>
        '        <DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</DATE>]
        '        <TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</TIME>]
        '      </RESPONSE>
        '      <ERROR CODE="errorcode">errordescription</ERROR>
        '    </SERVICE_LOG>
        '    <DATA_ITEM> ... </DATA_ITEM>
        '  </TRIGGER>
        ' </TRIGGERS>
        '</PARENT_NODE>
        ******************************************************************************/

        private XmlNode xmlServiceLog;
        private XmlNodeList xmlServiceLogList;
        //private IEnumerator xmlServiceLogListEnum;
        private XMLVarDataItem m_DataItem;

        private ArrayList sSLogNodeOrder;
        private ArrayList sRqstNodeOrder;
        private ArrayList sRespNodeOrder;
        
        public XMLTrigger()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlNodeList = null;
            xmlServiceLog = null;
            xmlServiceLogList = null;
            m_DataItem = null;

            sThisNodeOrder = new ArrayList(10);
            sThisNodeOrder.Add(Constants.sTrgrType);
            sThisNodeOrder.Add(Constants.sTrgrDate);
            sThisNodeOrder.Add(Constants.sTrgrTime);
            sThisNodeOrder.Add(Constants.sTrgrUser);
            sThisNodeOrder.Add(Constants.sTrgrUploaded);
            sThisNodeOrder.Add(Constants.sTrgrRqUID);
            sThisNodeOrder.Add(Constants.sTrgrRelatedTable);
            sThisNodeOrder.Add(Constants.sTrgrRelatedRecordKey);
            sThisNodeOrder.Add(Constants.sTrgrServiceLog);
            sThisNodeOrder.Add(Constants.sTrgrDataItem);

            sSLogNodeOrder = new ArrayList(8);
            sSLogNodeOrder.Add(Constants.sTrgrStatus);
            sSLogNodeOrder.Add(Constants.sTrgrProcDate);
            sSLogNodeOrder.Add(Constants.sTrgrProcTime);
            sSLogNodeOrder.Add(Constants.sTrgrCompleteDate);
            sSLogNodeOrder.Add(Constants.sTrgrCompleteTime);
            sSLogNodeOrder.Add(Constants.sTrgrRequest);
            sSLogNodeOrder.Add(Constants.sTrgrResponse);
            sSLogNodeOrder.Add(Constants.sTrgrError);

            sRqstNodeOrder = new ArrayList(5);
            sRqstNodeOrder.Add(Constants.sTrgrName);
            sRqstNodeOrder.Add(Constants.sTrgrRqUID);
            sRqstNodeOrder.Add(Constants.sTrgrDate);
            sRqstNodeOrder.Add(Constants.sTrgrTime);
            sRqstNodeOrder.Add(Constants.sTrgrDesc);

            sRespNodeOrder = new ArrayList(4);
            sRespNodeOrder.Add(Constants.sTrgrName);
            sRespNodeOrder.Add(Constants.sTrgrRqUID);
            sRespNodeOrder.Add(Constants.sTrgrDate);
            sRespNodeOrder.Add(Constants.sTrgrTime);
        }

        public override XmlNode Create()
        {
            return Create("0","","","","");
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sTrgrNode; }
        }

        public XmlNode Create(string sID, string stypecode, string sTypeDesc, string sDate, string sTime)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false, null, null);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                TriggerID = sID;
                putTriggerType(sTypeDesc, stypecode);
                DateofTrigger = sDate;
                TimeofTrigger = sTime;

                InsertInDocument(null, null);
                Parent.Count = Parent.Count + 1;

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLTrigger.Create");
                return null;
            }
        }

        public new XMLTriggers Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLTriggers();
                    ((XMLTriggers)m_Parent).Trigger = this; //SIW529
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                //SIW529 ((XMLTriggers)m_Parent).Trigger = this;
                return (XMLTriggers)m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        protected override void ResetParent()
        {
            xmlThisNode = null;
            xmlNodeList = null;
            subClearPointers();
        }

        private void subClearPointers()
        {
            xmlServiceLog = null;
            xmlServiceLogList = null;
            m_DataItem = null;
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                //subClearPointers();   //SI06420
                ResetParent();          //SI06420
            }
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                {
                    Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
                    Parent.Count = Parent.Count + 1;
                }
                return Node;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            get
            {
                //SIW485 return Globals.sNodeOrder;
                return Utils.sNodeOrder;	//SIW485
            }
        }

        public new int Count
        {
            get
            {
                return TriggerList.Count;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return TriggerList;
            }
        }

        public XmlNodeList TriggerList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getTrigger(bool reset)
        {
            return getNode(reset);
        }

        public void removeTrigger()
        {
            if (Node != null)
                Parent_Node.RemoveChild(Node);
            getTrigger(true);
        }

        public string IDPrefix
        {
            get
            {
                return Constants.sTrgrIDPFX;
            }
        }

        public string TriggerID
        {
            get
            {
                string strdata;
                strdata = stripID(Utils.getAttribute(Node, "",Constants.sTrgrID));
                return strdata;
            }
            set
            {
                string sid;
                if (value == null || value == "" || value == "0")
                    sid = Parent.getNextID(null);
                else
                    sid = value;
                Utils.putAttribute(putNode, "", Constants.sTrgrID, formatID(sid), NodeOrder);
            }
        }

        public XmlNode putTriggerType(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putNode, Constants.sTrgrType, sDesc, sCode, sThisNodeOrder);
        }
        //Start SI06468
        public XmlNode putTriggerType(string sDesc, string sCode,string sId)
        {
            return Utils.putCodeItem(putNode, Constants.sTrgrType, sDesc, sCode, sThisNodeOrder,sId);
        }
        //End SI06468
        public string TypeTrigger
        {
            get
            {
                return Utils.getData(Node, Constants.sTrgrType);
            }
            set
            {
                Utils.putData(putNode, Constants.sTrgrType, value, sThisNodeOrder);
            }
        }

        public string TypeTrigger_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sTrgrType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sTrgrType, value, sThisNodeOrder);
            }
        }
        //Start SI06468
        public string TypeTrigger_CodeID
        {
            get
            {
                return Utils.getCode(Node, Constants.sTrgrType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sTrgrType, value, sThisNodeOrder);
            }
        }
        //End SI06468
        public string DateofTrigger
        {
            get
            {
                return Utils.getDate(Node, Constants.sTrgrDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sTrgrDate, value, sThisNodeOrder);
            }
        }

        public string TimeofTrigger
        {
            get
            {
                return Utils.getTime(Node, Constants.sTrgrTime);
            }
            set
            {
                Utils.putTime(putNode, Constants.sTrgrTime, value, sThisNodeOrder);
            }
        }

        public string User
        {
            get
            {
                return Utils.getData(Node, Constants.sTrgrUser);
            }
            set
            {
                Utils.putData(putNode, Constants.sTrgrUser, value, sThisNodeOrder);
            }
        }

        public string Upload
        {
            get
            {
                return Utils.getBool(Node, Constants.sTrgrUploaded, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sTrgrUploaded, "", value, sThisNodeOrder);
            }
        }

        public string UploadRqUID
        {
            get
            {
                return Utils.getData(Node, Constants.sTrgrRqUID);
            }
            set
            {
                Utils.putData(putNode, Constants.sTrgrRqUID, value, sThisNodeOrder);
            }
        }

        public XmlNode putRelatedTable(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putNode, Constants.sTrgrRelatedTable, sDesc, sCode, sThisNodeOrder);
        }

        public string RelatedTable
        {
            get
            {
                return Utils.getData(Node, Constants.sTrgrError);
            }
            set
            {
                Utils.putData(putNode, Constants.sTrgrError, value, sThisNodeOrder);
            }
        }

        public string RelatedTable_ID
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sTrgrRelatedTable, Constants.sTrgrRelatedTableID);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sTrgrRelatedTable, Constants.sTrgrRelatedTableID, value, sThisNodeOrder);
            }
        }

        public string RelatedRecordKey
        {
            get
            {
                return Utils.getData(Node, Constants.sTrgrRelatedRecordKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sTrgrRelatedRecordKey, value, sThisNodeOrder);
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();  //SI06420
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }

        public XmlNode addSLog(string sID)
        {
            SLogNode = null;
            SLogID = sID;
            return SLogNode;
        }

        public XmlNode putSLogNode
        {
            get
            {
                if (SLogNode == null)
                    SLogNode = XML.XMLaddNode(putNode, Constants.sTrgrServiceLog, sThisNodeOrder);
                return SLogNode;
            }
        }

        public XmlNode SLogNode
        {
            get
            {
                return xmlServiceLog;
            }
            set
            {
                xmlServiceLog = value;
            }
        }

        public int SLogCount
        {
            get
            {
                return SLogList.Count;
            }
        }

        public XmlNodeList SLogList
        {
            get
            {
                //Start SIW163
                if (null == xmlServiceLogList)
                {
                    return getNodeList(Constants.sTrgrServiceLog, ref xmlServiceLogList, Node);
                }
                else
                {
                    return xmlServiceLogList;
                }
                //End SIW163
            }
        }

        public XmlNode getSLog(bool reset)
        {
            //Start SIW163
            if (null != SLogList)
            {
                return getNode(reset, ref xmlServiceLogList, ref xmlServiceLog);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public XmlNode getFirstSLog()
        {
            return getSLog(true);
        }

        public XmlNode getNextSLog()
        {
            return getSLog(false);
        }

        public void removeSLog()
        {
            if (SLogNode != null)
                Node.RemoveChild(SLogNode);
            getFirstSLog();
        }

        public string SLogIDPrefix
        {
            get
            {
                return Constants.sTrgrSLogIDPFX;
            }
        }

        public string SLogID
        {
            get
            {
                string strdata = Utils.getAttribute(SLogNode, "", Constants.sTrgrID);
                if (StringUtils.Left(strdata, 4) == Constants.sTrgrSLogIDPFX && strdata.Length >= 5)
                    strdata = StringUtils.Right(strdata, strdata.Length - 4);
                return strdata;
            }
            set
            {
                string sid;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lSLOGId = Globals.lSLOGId + 1;
                    sid = Globals.lSLOGId + "";
                }
                else
                    sid = value;
                if (StringUtils.Left(sid, 4) != Constants.sTrgrSLogIDPFX)
                    sid = Constants.sTrgrSLogIDPFX + sid;
                Utils.putAttribute(putSLogNode, "", Constants.sTrgrID, sid, sThisNodeOrder);
            }
        }

        public string ProcessDate
        {
            get
            {
                return Utils.getDate(SLogNode, Constants.sTrgrProcDate);
            }
            set
            {
                Utils.putDate(putSLogNode, Constants.sTrgrProcDate, value, sSLogNodeOrder);
            }
        }

        public string ProcessTime
        {
            get
            {
                return Utils.getTime(SLogNode, Constants.sTrgrProcTime);
            }
            set
            {
                Utils.putTime(putSLogNode, Constants.sTrgrProcTime, value, sSLogNodeOrder);
            }
        }

        public string CompleteDate
        {
            get
            {
                return Utils.getDate(SLogNode, Constants.sTrgrCompleteDate);
            }
            set
            {
                Utils.putDate(putSLogNode, Constants.sTrgrCompleteDate, value, sSLogNodeOrder);
            }
        }

        public string CompleteTime
        {
            get
            {
                return Utils.getTime(SLogNode, Constants.sTrgrCompleteTime);
            }
            set
            {
                Utils.putTime(putSLogNode, Constants.sTrgrCompleteTime, value, sSLogNodeOrder);
            }
        }

        public XmlNode putStatus(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putSLogNode, Constants.sTrgrStatus, sDesc, sCode, sSLogNodeOrder);
        }
        //Start SI06468
        public XmlNode putStatus(string sDesc, string sCode, string sId)
        {
            return Utils.putCodeItem(putSLogNode, Constants.sTrgrStatus, sDesc, sCode, sSLogNodeOrder, sId);
        }
        //End SI06468
        public string Status
        {
            get
            {
                return Utils.getData(SLogNode, Constants.sTrgrStatus);
            }
            set
            {
                Utils.putData(putSLogNode, Constants.sTrgrStatus, value, sSLogNodeOrder);
            }
        }

        public string Status_Code
        {
            get
            {
                return Utils.getCode(SLogNode, Constants.sTrgrStatus);
            }
            set
            {
                Utils.putCode(putSLogNode, Constants.sTrgrStatus, value, NodeOrder);
            }
        }
        //Start SI06468
        public string Status_CodeID
        {
            get
            {
                return Utils.getCode(SLogNode, Constants.sTrgrStatus);
            }
            set
            {
                Utils.putCode(putSLogNode, Constants.sTrgrStatus, value, NodeOrder);
            }
        }
        //End SI06468


        public XmlNode RequestNode
        {
            get
            {
                return Utils.getNode(SLogNode, Constants.sTrgrRequest);
            }
        }

        public XmlNode putRequestNode
        {
            get
            {
                if (RequestNode == null)
                    XML.XMLaddNode(putSLogNode, Constants.sTrgrRequest, sSLogNodeOrder);
                return RequestNode;
            }
        }

        public string RequestName
        {
            get
            {
                return Utils.getData(RequestNode, Constants.sTrgrName);
            }
            set
            {
                Utils.putData(putRequestNode, Constants.sTrgrName, value, sRqstNodeOrder);
            }
        }

        public string RequestUID
        {
            get
            {
                return Utils.getData(RequestNode, Constants.sTrgrRqUID);
            }
            set
            {
                Utils.putData(putRequestNode, Constants.sTrgrRqUID, value, sRqstNodeOrder);
            }
        }

        public string RequestDate
        {
            get
            {
                return Utils.getDate(RequestNode, Constants.sTrgrDate);
            }
            set
            {
                Utils.putDate(putRequestNode, Constants.sTrgrDate, value, sRqstNodeOrder);
            }
        }

        public string RequestTime
        {
            get
            {
                return Utils.getTime(RequestNode, Constants.sTrgrTime);
            }
            set
            {
                Utils.putTime(putRequestNode, Constants.sTrgrTime, value, sRqstNodeOrder);
            }
        }

        public string RequestDescription
        {
            get
            {
                return Utils.getData(RequestNode, Constants.sTrgrDesc);
            }
            set
            {
                Utils.putData(putRequestNode, Constants.sTrgrDesc, value, sRqstNodeOrder);
            }
        }

        public XmlNode ResponseNode
        {
            get
            {
                return Utils.getNode(SLogNode, Constants.sTrgrResponse);
            }
        }

        public XmlNode putResponseNode
        {
            get
            {
                if (ResponseNode == null)
                    XML.XMLaddNode(putSLogNode, Constants.sTrgrResponse, sSLogNodeOrder);
                return ResponseNode;
            }
        }

        public string ResponseName
        {
            get
            {
                return Utils.getData(ResponseNode, Constants.sTrgrName);
            }
            set
            {
                Utils.putData(putResponseNode, Constants.sTrgrName, value, sRespNodeOrder);
            }
        }

        public string ResponseUID
        {
            get
            {
                return Utils.getData(ResponseNode, Constants.sTrgrRqUID);
            }
            set
            {
                Utils.putData(putResponseNode, Constants.sTrgrRqUID, value, sRespNodeOrder);
            }
        }

        public string ResponseDate
        {
            get
            {
                return Utils.getDate(ResponseNode, Constants.sTrgrDate);
            }
            set
            {
                Utils.putDate(putResponseNode, Constants.sTrgrDate, value, sRespNodeOrder);
            }
        }

        public string ResponseTime
        {
            get
            {
                return Utils.getTime(ResponseNode, Constants.sTrgrTime);
            }
            set
            {
                Utils.putTime(putResponseNode, Constants.sTrgrTime, value, sRespNodeOrder);
            }
        }

        public XmlNode putError(string sDesc, string sCode)
        {
            return Utils.putCodeItem(putSLogNode, Constants.sTrgrError, sDesc, sCode, sSLogNodeOrder);
        }

        public string Error
        {
            get
            {
                return Utils.getData(SLogNode, Constants.sTrgrError);
            }
            set
            {
                Utils.putData(putSLogNode, Constants.sTrgrError, value, sSLogNodeOrder);
            }
        }

        public string Error_Code
        {
            get
            {
                return Utils.getCode(SLogNode, Constants.sTrgrError);
            }
            set
            {
                Utils.putCode(putSLogNode, Constants.sTrgrError, value, sSLogNodeOrder);
            }
        }

        public string stripID(string sID)
        {
            if (StringUtils.Left(sID, 3) == Constants.sTrgrIDPFX && sID.Length >= 4)
                return StringUtils.Right(sID, sID.Length - 3);
            else
                return sID;
        }

        public string formatID(string sID)
        {
            string strdata = sID.Trim();
            if (StringUtils.Left(strdata, 3) != Constants.sTrgrIDPFX)
                return Constants.sTrgrIDPFX + strdata;
            else
                return strdata;
        }

        public XmlNode getTriggerbyID(string sID)
        {
            if (TriggerID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (TriggerID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getTriggerbyReqName(string sName)
        {
            sName = sName.Trim();
            if (RequestName != sName)
            {
                getFirst();
                while (Node != null)
                {
                    if (RequestName == sName)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getTriggerbyType(string sCode, string sType)
        {
            sCode.Trim();
            sType.Trim();
            if ((sCode != null && sCode != "" && TypeTrigger_Code != sCode) ||
                (sType != null && sType != "" && TypeTrigger != sType))
            {
                getFirst();
                while (Node != null)
                {
                    if (sType == "" || sType == null)
                    {
                        if (TypeTrigger_Code == sCode)
                            break;
                    }
                    else
                    {
                        if (sCode == "" || sCode == null)
                        {
                            if (TypeTrigger == sType)
                                break;
                        }
                        else
                            if (TypeTrigger == sType && TypeTrigger_Code == sCode)
                                break;
                    }
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getSLogbyID(string sID)
        {
            if (SLogID != sID)
            {
                getFirstSLog();
                while (SLogNode != null)
                {
                    if (SLogID == sID)
                        break;
                    getNextSLog();
                }
            }
            return SLogNode;
        }

        public XmlNode getSLogbyRequest(string sReqName, string sReqUID)
        {
            if ((sReqName != "" && sReqName != null && sReqName != RequestName) ||
                (sReqUID != "" && sReqUID != null && sReqUID != RequestUID))
            {
                getFirstSLog();
                while (SLogNode != null)
                {
                    if (sReqName != "" && sReqName != null && sReqName == RequestName &&
                       sReqUID != "" && sReqUID != null && sReqUID == RequestUID)
                        break;
                    getNextSLog();
                }
            }
            return SLogNode;
        }

        public XmlNode getSLogbyResponse(string sRespName, string sRespUID)
        {
            if ((sRespName != "" && sRespName != null && sRespName != ResponseName) ||
               (sRespUID != "" && sRespUID != null && sRespUID != ResponseUID))
            {
                getFirstSLog();
                while (SLogNode != null)
                {
                    if (sRespName != "" && sRespName != null && sRespName == ResponseName &&
                       sRespUID != "" && sRespUID != null && sRespUID == ResponseUID)
                        break;
                    getNextSLog();
                }
            }
            return SLogNode;
        }
    }

    public class XMLTriggers : XMLXCNode
    {
        /*'<TRIGGERS COUNT='xxxx'>
        '    <TRIGGER ID='TRGxxx'>
        '      ....
        '    </TRIGGER>
        '  </TRIGGERS>*/

        private XMLTrigger m_Trigger;

        public XMLTriggers()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            m_Parent = null;

            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sTrgrNode);

            Node = Utils.getNode(Parent.Node, NodeName);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sTrgrList; }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                ResetParent();                  //SI06420
            }
        }

        public override XmlNode Create()
        {
            return Create(null);
        }

        public XmlNode Create(XmlDocument xmlDoc)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = xmlElement;

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLTriggers.Create");
                return null;
            }
        }

        public int Count
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sTrgrCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sTrgrCount, value + "", NodeOrder);
            }
        }

        public override XmlNode Node
        {
            get//Start SI06420
            {
                if (xmlThisNode != null)
                   ResetParent();
               return xmlThisNode; 
            }//End SI06420
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        private void subClearPointers()
        {
            //Start SI06420
            //Trigger = null;
            if (m_Trigger != null)
                //SIW529 m_Trigger.Node = null;
                m_Trigger.Parent = this;    //SIW529 Resets the Trigger object
            //End SI06420
        }

        protected override void ResetParent()
        {
            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }

        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLClaim();
                    ((XMLClaim)m_Parent).Triggers = this;   //SIW529
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                XmlNode xmlNode;        //SI06420
                xmlNode = xmlThisNode;  //SI06420 
                m_Parent = value;
                xmlThisNode = xmlNode;  //SI06420
                xmlNode = null;         //SI06420
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public XmlNode AddTrigger()
        {
            return Trigger.Create();
        }

        public XMLTrigger Trigger
        {
            get
            {
                if (m_Trigger == null)
                {
                    m_Trigger = new XMLTrigger();
                    m_Trigger.Parent = this;
                    //SIW529 LinkXMLObjects(m_Trigger);
                    //m_Trigger.getFirst();   //SI06420
                }
                if (Node != null)//Start SI06420
                {
                    if (m_Trigger.Node == null)
                        m_Trigger.getFirst();
                }//End SI06420
                return m_Trigger;
            }
            set
            {
                m_Trigger = value;
                if (m_Trigger != null)     //SI06420
                {
                    m_Trigger.Parent = this;  //SI06420
                    //SIW529 LinkXMLObjects(m_Trigger);
                }
            }
        }

        public XmlNode getTrigger(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return Trigger.getTrigger(reset);
        }

        public XmlNode getTriggerbyID(XmlDocument xdoc, string sTrgID)
        {
            if (xdoc != null)
                Document = xdoc;
            return Trigger.getTriggerbyID(sTrgID);
        }

        public XmlNode getTriggerbyReqName(XmlDocument xdoc, string sReqName)
        {
            if (xdoc != null)
                Document = xdoc;
            return Trigger.getTriggerbyReqName(sReqName);
        }

        public XmlNode getTriggerbyType(XmlDocument xdoc, string stypecode, string sTypeName)
        {
            if (xdoc != null)
                Document = xdoc;
            return Trigger.getTriggerbyType(stypecode, sTypeName);
        }

        public string getNextID(XmlNode xnode)
        {
            int id;
            if (xnode != null)
                Node = xnode;
            id = NextID;
            NextID = NextID + 1;
            return id + "";
        }

        public int NextID
        {
            get
            {
                if (Globals.lTRGId > 1)
                    return 1;
                else
                    return Globals.lTRGId;
            }
            set
            {
                if (value > 1)
                    Globals.lTRGId = 1;
                else
                    Globals.lTRGId = value;
            }
        }
    }
}
