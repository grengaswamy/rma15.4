/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 11/12/2007 |         |    JTC     | Created
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class Utils : XmlFormatting.XMLUtils
    {
        public XMLCommentReference funSetupCommentReference(XMLCommentReference obPI,   // SIW529 XmlDocument Doc,
                                                                   XmlNode prntnode, XMLACNode prnt,
                                                                   ArrayList norder)	//SIW485 Removed static SIW529 Removed Doc parameter
        {
            XMLCommentReference oPI;
            oPI = obPI;
            if (oPI == null)
                oPI = new XMLCommentReference();

            if (prntnode != null)
            {
                //SIW529 oPI.Document = Doc;
                oPI.Parent = prnt;
            }
            return oPI;
        }

        public XMLPartyInvolved funSetupPartyInvolved(XMLPartyInvolved oPI, //SIW529 XmlDocument Doc,
                                                             XmlNode prntnode, XMLACNode prnt, ArrayList norder)	//SIW485 Removed static
        {
            if (oPI == null)
                oPI = new XMLPartyInvolved();

            if (prntnode != null)
                oPI.Parent = prnt;

            return oPI;
        }

        public XMLAddressReference funSetupAddressReference(XMLAddressReference oPI, //SIW529 XmlDocument Doc,
                                                                   XmlNode prntnode, XMLACNode prnt, ArrayList norder)	//SIW485 Removed static
        {
            if (oPI == null)
                oPI = new XMLAddressReference();
            if (prntnode != null)
            {
                //SIW529 oPI.Document = Doc;
                oPI.Parent = prnt;
            }
            return oPI;
        }

        public XMLDocumentReference funSetupDocumentReference(XMLDocumentReference oCR, //SIW529 XmlDocument Doc,
                                                                     XmlNode prntnode, XMLACNode prnt, ArrayList norder)	//SIW485 Removed static
        {
            if (oCR == null)
                oCR = new XMLDocumentReference();
            if (prntnode != null)
            {
                //SIW529 oCR.Document = Doc;
                oCR.Parent = prnt;
            }
            return oCR;
        }

        public XMLDiaryReference funSetupDiaryReference(XMLDiaryReference oDR, //SIW529 XmlDocument Doc,
                                                               XmlNode prntnode, XMLACNode prnt, ArrayList norder)	//SIW485 Removed static
        {
            if (oDR == null)
                oDR = new XMLDiaryReference();
            if (prntnode != null)
            {
                //SIW529 oDR.Document = Doc;
                oDR.Parent = prnt;
            }
            return oDR;
        }

        public string extDocumentID(string strdata)	//SIW485 Removed static
        {
            string lid = "";

            if (strdata != "" && strdata != null)
                if (StringUtils.Left(strdata, 3) == Constants.sDocIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
            return lid;
        }

        public string extDiaryID(string strdata)	//SIW485 Removed static
        {
            string lid = "";

            if (strdata != "" && strdata != null)
                if (StringUtils.Left(strdata, 3) == Constants.sDiaIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
            return lid;
        }

        public string extCommentID(string strdata)	//SIW485 Removed static
        {
            string lid = "";
            if (strdata != "" && strdata != null)
                if (StringUtils.Left(strdata, 3) == Constants.sCmtIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
            return lid;
        }

        public string extSearchID(string strdata)	//SIW485 Removed static
        {
            string lid = "";

            if (strdata != "" && strdata != null)
                if (StringUtils.Left(strdata, 3) == Constants.sSrchIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
            return lid;
        }
    }
}
