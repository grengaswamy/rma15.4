/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/25/2007 |         |    JTC     | Created
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public class XMLDiaryReference : XMLXCReferenceNode
    {
        /******************************************************************************************
        ' The cXMLDiaryReference class provides the functionality to support and
        ' manage a Diary Reference Node
        '******************************************************************************************
        '<!--Multiple levels -->
        '   <DIARY_REFERENCE DIARY_ID="DIA###">
        '     <!-- Multiple Occurrences -->
        '******************************************************************************************/

        private XMLDiary m_Diary;
        
        public XMLDiaryReference()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;
            m_Diary = null;

            sThisNodeOrder = new ArrayList();
            sThisNodeOrder.Add("");
        }

        protected override void CheckEntry()
        {
            xEntry(DiaryID);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sDiaReference; }
        }

        public XmlNode putDiary(string sID)
        {
            Node = null;
            if (sID != "" && sID != null)
                findDiaryRefbyId(sID);
            if (Node == null)
                Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
            DiaryID = sID;
            return Node;
        }

        public XmlNode getDiary(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode findDiaryRefbyId(string sID)
        {
            if (sID != DiaryID)
            {
                getDiary(true);
                while (Node != null)
                {
                    if (sID == DiaryID)
                        break;
                    getDiary(false);
                }
            }
            return Node;
        }

        public string DiaryID
        {
            get
            {
                string strdata = "";
                if (xmlThisNode != null)
                    strdata = XML.XMLGetAttributeValue(xmlThisNode, Constants.sDiaID);
                return ((Utils)Utils).extDiaryID(strdata);
            }
            set
            {
                if (xmlThisNode == null)
                    putDiary(value);
                else
                    XML.XMLSetAttributeValue(xmlThisNode, Constants.sDiaID, Constants.sDiaIDPfx + value);
                if (value == "" || value == "0")
                    CheckEntry();
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                m_Diary = null;
            }
        }

        public override XMLACNode Parent
        {
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        protected override void ResetParent()
        {
            xmlThisNode = null;
            xmlNodeList = null;
            Node = null;
        }

        public XMLDiary Diary
        {
            get
            {
                if (m_Diary == null)
                {   //SIW529
                    m_Diary = new XMLDiary();
                    LinkXMLObjects(m_Diary);    //SIW529
                }   //SIW529
                m_Diary.getDiarybyID(DiaryID);
                return m_Diary;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }


    }
}
