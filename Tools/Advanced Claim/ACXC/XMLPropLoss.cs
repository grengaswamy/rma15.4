/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 06/17/2008 | SI06369 |    sw      | Implemented HTML to replace RTF in RadEditors 
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label  
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/01/2010 | SIW493  |   ASM      | Correct rest logic in NodeList 
 * 09/08/2010 | SIW493  |    AS      | Fix in xc object constructor
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 08/18/2011 | SIW7419  |    RG     | Demand Offer added
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLPropLoss : XMLXCNodeWithList, XMLXCIntSItem, XMLXCIntPartyInvolved  //SIW122
    {
        /*-----------------------------------------------------------------------
        '<PROPERTY_LOSS>
        '   <UNIT_REFERENCE ID="UNTxxx"/>
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '   <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '   <SCHEDULED_ITEM> ...   </SCHEDULED_ITEM>
        '   <LOSS_PROP_TYPE CODE="code>description</LOSS_PROP_TYPE>
        '   <THEFT_LOCATION CODE="code>description</THEFT_LOCATION>
        '   <INCENDIARY_FIRE INDICATOR="YES|NO"/>
        '   <UNDER_CONSTRUCTION INDICATOR="YES|NO"/>
        '   <VACANT INDICATOR="YES|NO"/>
        '   <LOSS_INFORMATION> ... </LOSS_INFORMATION>  
        '      <!Multiple Demand Offers Allowed>            //SIW7419
        '      <DEMAND_OFFER> ... </DEMAND_OFFER>
        '</PROPERTY_LOSS>*/

        private XMLLossInfo m_LossInfo;
        private XMLSchedItem m_SchedItem;
        private XMLDemandOffer m_DemandOffer;     //SIW7419

        private Dictionary<object, object> dctRPRType;
        
        public XMLPropLoss()
        {
            //SIW529 Utils.subInitializeGlobals(XML); //SIW493
            xmlNodeList = null;
            xmlThisNode = null;
            m_Parent = null;
            m_LossInfo = null;

            //sThisNodeOrder = new ArrayList(8);                //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(9);                  //(SI06023 - Implemented in SI06333)  //SIW490
            sThisNodeOrder = new ArrayList(10);                  //SIW490
            sThisNodeOrder.Add(Constants.sPLUnitReference);
            sThisNodeOrder.Add(Constants.sStdTechKey);           //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);         //SIW490
            sThisNodeOrder.Add(Constants.sPLSchedItem);
            sThisNodeOrder.Add(Constants.sPLLossPropType);
            sThisNodeOrder.Add(Constants.sPLTheftLocation);
            sThisNodeOrder.Add(Constants.sPLIncendiaryFire);
            sThisNodeOrder.Add(Constants.sPLUnderConstruction);
            sThisNodeOrder.Add(Constants.sPLVacant);
            sThisNodeOrder.Add(Constants.sPLLossInfo);
            sThisNodeOrder.Add(Constants.sPLDemandOffer);        //SIW7419
            //sThisNodeOrder.Add(Constants.sPLTechKey);           //(SI06023 - Implemented in SI06333)//SIW360

            dctRPRType = new Dictionary<object, object>();
            dctRPRType.Add(Constants.iUndefined, Constants.sUndefined);
            dctRPRType.Add(Constants.sUndefined, Constants.iUndefined);
            dctRPRType.Add(Constants.iRepairTypeReplace, Constants.sRepairTypeReplace);
            dctRPRType.Add(Constants.sRepairTypeReplace, Constants.iRepairTypeReplace);
            dctRPRType.Add(Constants.iRepairTypeRepair, Constants.sRepairTypeRepair);
            dctRPRType.Add(Constants.sRepairTypeRepair, Constants.iRepairTypeRepair);
            dctRPRType.Add(Constants.iRepairTypeNew, Constants.sRepairTypeNew);
            dctRPRType.Add(Constants.sRepairTypeNew, Constants.iRepairTypeNew);
            dctRPRType.Add(Constants.iRepairTypeRebuilt, Constants.sRepairTypeRebuilt);
            dctRPRType.Add(Constants.sRepairTypeRebuilt, Constants.iRepairTypeRebuilt);
        }

        public override XmlNode Create()
        {
            return Create("", "");
        }        

        protected override string DefaultNodeName
        {
            get { return Constants.sPLNode; }
        }

        public XmlNode Create(string sUnitReference, string sdescription)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(Constants.sPLNode, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                UnitReferenceID = sUnitReference;
                LossDescription = sdescription;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLPropLoss.Create");
                return null;
            }
        }

        public string UnitReferenceID
        {
            get
            {
                string strdata = Utils.getAttribute(Node, Constants.sPLUnitReference, Constants.sPLUnitReferenceID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sUntIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sPLUnitReference, Constants.sPLUnitReferenceID, Constants.sUntIDPfx + value, NodeOrder);
            }
        }

        public XMLUnit Unit
        {
            get
            {
                XMLUnit xmlUnit = new XMLUnit();
                LinkXMLObjects(xmlUnit);//SIW529                
                xmlUnit.Parent.Parent = Parent;
                if (UnitReferenceID != "" && UnitReferenceID != null)
                    xmlUnit.getUnitbyID(UnitReferenceID);
                return xmlUnit;
            }
        }

        public XMLSchedItem ScheduledItem
        {
            get
            {
                if (m_SchedItem == null)
                {
                    m_SchedItem = new XMLSchedItem();
                    m_SchedItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_SchedItem);
                    m_SchedItem.getScheduledItem(Constants.xcGetFirst); //SI06420
                }
                return m_SchedItem;
            }
            set
            {
                m_SchedItem = value;
            }
        }

        //Start SIW139
        public XmlNode putLossPropType(string sDesc, string sCode)
        {
            return putLossPropType(sDesc, sCode, "");
        }

        public XmlNode putLossPropType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sPLLossPropType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string LossPropType
        {
            get
            {
                return Utils.getData(Node, Constants.sPLLossPropType);
            }
            set
            {
                Utils.putData(putNode, Constants.sPLLossPropType, value, NodeOrder);
            }
        }

        public string LossPropType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sPLLossPropType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sPLLossPropType, value, NodeOrder);
            }
        }

        public string LossPropType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sPLLossPropType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sPLLossPropType, value, NodeOrder);
            }
        }
        //End SIW139

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //START SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //END SIW490

        public XmlNode putTheftLocation(string sDesc, string sCode)
        {
            return putTheftLocation(sDesc, sCode, "");
        }
        
        public XmlNode putTheftLocation(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sPLTheftLocation, sDesc, sCode, NodeOrder, scodeid);
        }

        public string TheftLocation
        {
            get
            {
                return Utils.getData(Node, Constants.sPLTheftLocation);
            }
            set
            {
                Utils.putData(putNode, Constants.sPLTheftLocation, value, NodeOrder);
            }
        }

        public string TheftLocation_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sPLTheftLocation);
            }
            set
            {
                Utils.putCode(putNode, Constants.sPLTheftLocation, value, NodeOrder);
            }
        }

        public string TheftLocation_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sPLTheftLocation);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sPLTheftLocation, value, NodeOrder);
            }
        }
        //End SIW139

        public string IncendiaryFire
        {
            get
            {
                return Utils.getBool(Node, Constants.sPLIncendiaryFire, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sPLIncendiaryFire, "", value, NodeOrder);
            }
        }

        public string UnderConstruction
        {
            get
            {
                return Utils.getBool(Node, Constants.sPLUnderConstruction, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sPLUnderConstruction, "", value, NodeOrder);
            }
        }

        public string Vacant
        {
            get
            {
                return Utils.getBool(Node, Constants.sPLVacant, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sPLVacant, "", value, NodeOrder);
            }
        }

        public XMLLossInfo LossInfo
        {
            get
            {
                if (m_LossInfo == null)
                {
                    m_LossInfo = new XMLLossInfo();
                    m_LossInfo.Parent = this;
                    //SIW529 LinkXMLObjects(m_LossInfo);
                }
                return m_LossInfo;
            }
            set
            {
                m_LossInfo = value;
            }
        }

        public string LossDescription
        {
            get
            {
                return LossInfo.LossDescription;
            }
            set
            {
                LossInfo.LossDescription = value;
            }
        }

        public string UsedWithPermission
        {
            get
            {
                return LossInfo.UsedWithPermission;
            }
            set
            {
                LossInfo.UsedWithPermission = value;
            }
        }

        public string DamageDescription
        {
            get
            {
                return LossInfo.DamageDescription;
            }
            set
            {
                LossInfo.DamageDescription = value;
            }
        }

        //Start SI06369
        public string DamageDescriptionHTML
        {
            get
            {
                return LossInfo.DamageDescriptionHTML;
            }
            set
            {
                LossInfo.DamageDescriptionHTML = value;
            }
        }
        //End SI06369

        public string EstDamageAmount
        {
            get
            {
                return LossInfo.EstDamageAmount;
            }
            set
            {
                LossInfo.EstDamageAmount = value;
            }
        }

        public string ActDamageAmount
        {
            get
            {
                return LossInfo.ActDamageAmount;
            }
            set
            {
                LossInfo.ActDamageAmount = value;
            }
        }

        public string RentalCharge
        {
            get
            {
                return LossInfo.RentalCharge;
            }
            set
            {
                LossInfo.RentalCharge = value;
            }
        }

        public string RentalStartDate
        {
            get
            {
                return LossInfo.RentalStartDate;
            }
            set
            {
                LossInfo.RentalStartDate = value;
            }
        }

        public string RentalEndDate
        {
            get
            {
                return LossInfo.RentalEndDate;
            }
            set
            {
                LossInfo.RentalEndDate = value;
            }
        }

        public string RentalVendor
        {
            get
            {
                return LossInfo.RentalVendor;
            }
            set
            {
                LossInfo.RentalVendor = value;
            }
        }

        public string RentalVendorID
        {
            get
            {
                return LossInfo.RentalVendorID;
            }
            set
            {
                LossInfo.RentalVendorID = value;
            }
        }

        public XMLEntity RentalVendorEntity
        {
            get
            {
                return LossInfo.RentalVendorEntity;
            }
        }

        public XmlNode addRprEst()
        {
            return LossInfo.addRprEst();
        }

        public XmlNode getRprEst(bool reset)
        {
            return LossInfo.getRprEst(reset);
        }

        public XmlNode putRprEstItem(string sdesc, string scode)
        {
            return LossInfo.putRprEstItem(sdesc, scode);
        }

        public string RprEstItem
        {
            get
            {
                return LossInfo.RprEstItem;
            }
            set
            {
                LossInfo.RprEstItem = value;
            }
        }

        public string RprEstItem_Code
        {
            get
            {
                return LossInfo.RprEstItem_Code;
            }
            set
            {
                LossInfo.RprEstItem_Code = value;
            }
        }

        public string RprEstCost
        {
            get
            {
                return LossInfo.RprEstCost;
            }
            set
            {
                LossInfo.RprEstCost = value;
            }
        }

        public string RprEstLaborCost
        {
            get
            {
                return LossInfo.RprEstLaborCost;
            }
            set
            {
                LossInfo.RprEstLaborCost = value;
            }
        }

        public string RprEstLaborHours
        {
            get
            {
                return LossInfo.RprEstLaborHours;
            }
            set
            {
                LossInfo.RprEstLaborHours = value;
            }
        }

        public string RprEstLaborRate
        {
            get
            {
                return LossInfo.RprEstLaborHours;
            }
            set
            {
                LossInfo.RprEstLaborHours = value;
            }
        }

        public string RprEstUCR
        {
            get
            {
                return LossInfo.RprEstUCR;
            }
            set
            {
                LossInfo.RprEstUCR = value;
            }
        }

        public string RprEstUCRSource
        {
            get
            {
                return LossInfo.RprEstUCRSource;
            }
            set
            {
                LossInfo.RprEstUCRSource = value;
            }
        }

        public xcRepairType RprEstItemTypeRepair
        {
            get
            {
                return LossInfo.RprEstItemTypeRepair;
            }
            set
            {
                LossInfo.RprEstItemTypeRepair = value;
            }
        }

        public string RprEstItemTypeRepair_Code
        {
            get
            {
                return LossInfo.RprEstItemTypeRepair_Code;
            }
            set
            {
                LossInfo.RprEstItemTypeRepair_Code = value;
            }
        }


        public string RprEstItemVendor
        {
            get
            {
                return LossInfo.RprEstItemVendor;
            }
            set
            {
                LossInfo.RprEstItemVendor = value;
            }
        }

        public string RprEstItemVendorID
        {
            get
            {
                return LossInfo.RprEstItemVendorID;
            }
            set
            {
                LossInfo.RprEstItemVendorID = value;
            }
        }

        public XMLEntity RprEstItemVendorEntity
        {
            get
            {
                return LossInfo.RprEstItemVendorEntity;
            }
        }

        public string CanBeSeenWhere
        {
            get
            {
                return LossInfo.CanBeSeenWhere;
            }
            set
            {
                LossInfo.CanBeSeenWhere = value;
            }
        }

        public string CanBeSeenWhereID
        {
            get
            {
                return LossInfo.CanBeSeenWhereID;
            }
            set
            {
                LossInfo.CanBeSeenWhereID = value;
            }
        }

        public XMLAddress CanBeSeenAddress
        {
            get
            {
                return LossInfo.CanBeSeenAddress;
            }
        }

        public string CanBeSeenDate
        {
            get
            {
                return LossInfo.CanBeSeenDate;
            }
            set
            {
                LossInfo.CanBeSeenDate = value;
            }
        }

        public string CanBeSeenAfterTime
        {
            get
            {
                return LossInfo.CanBeSeenAfterTime;
            }
            set
            {
                LossInfo.CanBeSeenAfterTime = value;
            }
        }

        public string CanBeSeenBeforeTime
        {
            get
            {
                return LossInfo.CanBeSeenBeforeTime;
            }
            set
            {
                LossInfo.CanBeSeenBeforeTime = value;
            }
        }

        public int ChargeCount
        {
            get
            {
                return LossInfo.ChargeCount;
            }
        }

        public XmlNode addCharge()
        {
            return LossInfo.addCharge();
        }

        public XmlNode getCharge(bool reset)
        {
            return LossInfo.getCharge(reset);
        }

        public string ChargeAmount
        {
            get
            {
                return LossInfo.ChargeAmount;
            }
            set
            {
                LossInfo.ChargeAmount = value;
            }
        }

        public string ChargeType
        {
            get
            {
                return LossInfo.ChargeType;
            }
            set
            {
                LossInfo.ChargeType = value;
            }
        }

        public string OthrInsPolicy
        {
            get
            {
                return LossInfo.OthrInsPolicy;
            }
            set
            {
                LossInfo.OthrInsPolicy = value;
            }
        }

        public string OthrInsCarrier
        {
            get
            {
                return LossInfo.OthrInsCarrier;
            }
            set
            {
                LossInfo.OthrInsCarrier = value;
            }
        }

        public string OthrInsCarrierID
        {
            get
            {
                return LossInfo.OthrInsCarrierID;
            }
            set
            {
                LossInfo.OthrInsCarrierID = value;
            }
        }

        public XMLEntity OthrInsCarrierEntity
        {
            get
            {
                return LossInfo.OthrInsCarrierEntity;
            }
        }

        public string OthrInsEffectiveDate
        {
            get
            {
                return LossInfo.OthrInsEffectiveDate;
            }
            set
            {
                LossInfo.OthrInsEffectiveDate = value;
            }
        }

        public string OthrInsExpirationDate
        {
            get
            {
                return LossInfo.OthrInsExpirationDate;
            }
            set
            {
                LossInfo.OthrInsExpirationDate = value;
            }
        }

        public int OthrInsCoverageCount
        {
            get
            {
                return LossInfo.OthrInsCoverageCount;
            }
        }

        public XmlNode addOtherInsCoverage()
        {
            return LossInfo.addOthrInsCoverage();
        }

        public XmlNode getOthrInsCoverage(bool reset)
        {
            return LossInfo.getOthrInsCoverage(reset);
        }

        public XmlNode putOthrInsCovType(string sdesc, string scode)
        {
            return LossInfo.putOthrInsCovType(sdesc, scode);
        }

        public string OthrInsCovType
        {
            get
            {
                return LossInfo.OthrInsCovType;
            }
            set
            {
                LossInfo.OthrInsCovType = value;
            }
        }

        public string OthrInsCovType_Code
        {
            get
            {
                return LossInfo.OthrInsCovType_Code;
            }
            set
            {
                LossInfo.OthrInsCovType_Code = value;
            }
        }

        public XmlNode addOthrInsCovLimit()
        {
            return LossInfo.addOthrInsCovLimit();
        }

        public int OthrInsCovLimitCount
        {
            get
            {
                return LossInfo.OthrInsCovLimitCount;
            }
        }

        public XmlNode getOthrInsCovLimit(bool reset)
        {
            return LossInfo.getOthrInsCovLimit(reset);
        }

        public XmlNode putOthrInsCovLimit(string sdesc, string scode)
        {
            return LossInfo.putOthrInsCovLimit(sdesc, scode);
        }

        public string OthrInsCovLimit
        {
            get
            {
                return LossInfo.OthrInsCovLimit;
            }
            set
            {
                LossInfo.OthrInsCovLimit = value;
            }
        }

        public string OthrInsCovLimit_Code
        {
            get
            {
                return LossInfo.OthrInsCovLimit_Code;
            }
            set
            {
                LossInfo.OthrInsCovLimit_Code = value;
            }
        }

        public string OthrInsCovLimitAmount
        {
            get
            {
                return LossInfo.OthrInsCovLimitAmount;
            }
            set
            {
                LossInfo.OthrInsCovLimitAmount = value;
            }
        }

        public XmlNode addOthrInsCovDeductible()
        {
            return LossInfo.addOthrInsCovDeductible();
        }

        public int OthrInsCovDeductibleCount
        {
            get
            {
                return LossInfo.OthrInsCovDeductibleCount;
            }
        }

        public XmlNode getOthrInsCovDeductible(bool reset)
        {
            return LossInfo.getOthrInsCovDeductible(reset);
        }

        public XmlNode putOthrInsCovDeductible(string sdesc, string scode)
        {
            return LossInfo.putOthrInsCovDeductible(sdesc, scode);
        }

        public string OthrInsCovDeductible
        {
            get
            {
                return LossInfo.OthrInsCovDeductible;
            }
            set
            {
                LossInfo.OthrInsCovDeductible = value;
            }
        }

        public string OthrInsCovDeductible_Code
        {
            get
            {
                return LossInfo.OthrInsCovDeductible_Code;
            }
            set
            {
                LossInfo.OthrInsCovDeductible_Code = value;
            }
        }

        public string OthrInsCovDeductibleAmount
        {
            get
            {
                return LossInfo.OthrInsCovDeductibleAmount;
            }
            set
            {
                LossInfo.OthrInsCovDeductibleAmount = value;
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        private void subClearPointers()
        {
            LossInfo = null;
            DemandOffer = null;          //SIW7419
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                return LossInfo.PartyInvolved;
            }
            set
            {
                LossInfo.PartyInvolved = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                return LossInfo.CommentReference;
            }
            set
            {
                LossInfo.CommentReference = value;
            }
        }

        //Start SIW7419
        public XMLDemandOffer DemandOffer
        {
            get
            {
                if (m_DemandOffer == null)
                {
                    m_DemandOffer = new XMLDemandOffer();
                    m_DemandOffer.Parent = this;
                    //SIW529 LinkXMLObjects(m_DemandOffer);
                    m_DemandOffer.getFirst();
                }
                return m_DemandOffer;
            }
            set
            {
                m_DemandOffer = value;
            }
        }

        //End SIW7419

        public new XMLClaim Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLClaim();
                    ((XMLClaim)m_Parent).PropertyLoss = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return (XMLClaim)m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
                if (xmlThisNode != null)                   //SIW7419
                    DemandOffer.getFirst();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(Constants.sPLNode);
            }
        }
        
        public XmlNode getPropLoss(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode getPropLossByReference(string sref)
        {
            if (sref != UnitReferenceID)
            {
                getFirst();
                while (Node != null)
                {
                    if (sref == UnitReferenceID)
                        break;
                    getNext();
                }
            }
            return Node;
        }
        //Start SIW493
        public XmlNode getPropLossByTechkey(string stc)
        {
            bool rst = false;
            if (stc != Techkey)
            {
                rst = true;
                while (getPropLoss(rst) != null)
                {
                    rst = false;
                    if (stc == Techkey)
                        break;
                }
            }
            return Node;
        }
        //End SIW493
    }
}
