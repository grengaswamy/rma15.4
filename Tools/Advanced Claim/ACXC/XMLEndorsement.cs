/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLEndorsement : XMLXCNodeWithList
    {
        /*<parent>
        '  <ENDORSEMENT>
        '     <FORM_NUMBER>formnumber</FORM_NUMBER>
        '     <FORMDATE>200108</FORMDATE>
        '     <DESCRIPTION>description</DESCRIPTION>
        '     <EFFECTIVE_DATE YEAR="2001" MONTH="08" DAY="01" />
        '     <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '     <SPECIAL_CONDITIONS>text</SPECIAL_CONDITIONS>
        '        <!-- Multiple Endorsements allowed -->
        '  </ENDORSEMENT>
        '    <!-- Multiple Endorsements allowed -->
        '</parent>*/

        private XmlNode m_xmlSpcCnd;
        private XmlNodeList m_xmlSpcCndList;
        //private IEnumerator m_xmlSpcCntListEnum;
        
        public XMLEndorsement()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlNodeList = null;
            xmlThisNode = null;
            m_Parent = null;

            sThisNodeOrder = new ArrayList(6);
            sThisNodeOrder.Add(Constants.sEndFormNumber);
            sThisNodeOrder.Add(Constants.sEndFormDate);
            sThisNodeOrder.Add(Constants.sEndFormDescription);
            sThisNodeOrder.Add(Constants.sEndFormEffDate);
            sThisNodeOrder.Add(Constants.sEndFormExpDate);
            sThisNodeOrder.Add(Constants.sEndFormSpclCond);
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sEndNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "");
        }

        public XmlNode Create(string sFormNumber, string sFormDate, string sFormDesc, string sEffectiveDate, string sExpirationDate)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);

                Node = (XmlNode)xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                FormNumber = sFormNumber;
                FormDate = sFormDate;
                Description = sFormDesc;
                EffectiveDate = sEffectiveDate;
                ExpirationDate = sExpirationDate;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLEndorsement.Create");
                return null;
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            if (m_Parent != null)
                ((XMLXCIntEndorsement)m_Parent).Endorsement = this;
            Node = null;
        }

        private void subClearPointers()
        {
            m_xmlSpcCnd = null;
            m_xmlSpcCndList = null;
        }

        public string FormNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sEndFormNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sEndFormNumber, value, NodeOrder);
            }
        }

        public string FormDate
        {
            get
            {
                return Utils.getData(Node, Constants.sEndFormDate);
            }
            set
            {
                Utils.putData(putNode, Constants.sEndFormDate, value, NodeOrder);
            }
        }

        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEndFormEffDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEndFormEffDate, value, NodeOrder);
            }
        }

        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEndFormExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEndFormExpDate, value, NodeOrder);
            }
        }

        public string Description
        {
            get
            {
                return Utils.getData(Node, Constants.sEndFormDescription);
            }
            set
            {
                Utils.putData(putNode, Constants.sEndFormDescription, value, NodeOrder);
            }
        }

        public string SpecialCondition
        {
            get
            {
                return Utils.getData(SpCondNode, "");
            }
            set
            {
                Utils.putData(putSpCondNode, "", value, NodeOrder);
            }
        }

        public XmlNode addSpecialCondition(string stext)
        {
            SpCondNode = null;
            if (stext != "" && stext != null)
                SpecialCondition = stext;
            return SpCondNode;
        }

        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLPolicy();
                    ((XMLPolicy)m_Parent).Endorsement = this;   //SIW529
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getEndorsement(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode putSpCondNode
        {
            get
            {
                if (SpCondNode == null)
                    addSpecialCondition("");
                return SpCondNode;
            }
        }

        public XmlNode SpCondNode
        {
            get
            {
                return m_xmlSpcCnd;
            }
            set
            {
                m_xmlSpcCnd = value;
            }
        }

        public XmlNodeList SpCondNodeList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlSpcCndList)
                {
                    return getNodeList(Constants.sEndFormSpclCond, ref m_xmlSpcCndList, Node);
                }
                else
                {
                    return m_xmlSpcCndList;
                }
                //End SIW163
            }
        }

        public int SpcCondCount
        {
            get
            {
                return SpCondNodeList.Count;
            }
        }

        public XmlNode getSpcCond(bool rst)
        {
            //Start SIW163
            if (null != SpCondNodeList)
            {
                return getNode(rst, ref m_xmlSpcCndList, ref m_xmlSpcCnd);
            }
            else
            {
                return null;
            }
            //End SIW163
        }
    }
}
