/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 11/12/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public class Globals : XMLGlobals
    {
        //Used to bring the Globals over to XC
    }
}
