﻿/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 06/16/2010 | W344    |    AP      | Created
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label   
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/01/2010 | SIW493  |   ASM      | Correct rest logic in NodeList 
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLLiabilityLoss : XMLXCNodeWithList, XMLXCIntPartyInvolved
    {
        /*
            '<LIABILITY_LOSS ID="LLxx">
            '  <UNIT_ID ID="UNTxxx"/>
            '  <TECH_KEY>AC Technical key for the element </TECH_KEY>
            '  <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
            '  <LIABILITY_TYPE CODE="xx">type </LIABILITY_TYPE>
            '  <INJURY_TYPE CODE="xx">type </INJURY_TYPE>
            '  <DAMAGE_TYPE CODE ="xx" >TYPE</DAMAGE_TYPE>
            '  <AREA_OF_PRACTICE CODE="xx">TYPE</AREA_OF_PRACTICE>
            '  <PLNTF_CLMT_TYPE CODE="XX">TYPE </PLNTF_CLMT_TYPE>
            '  <COVERAGE_FORM CODE="XX">TYPE </COVERAGE_FORM>
            '  <WITHIN_BIZ_SCOPE CODE="XX">TYPE</WITHIN_BIZ_SCOPE>
            '  <TYPE_OF_PREMISES CODE="XX">type </TYPE_OF_PREMISES>
            '  <TYPE_OF_PRODUCT CODE="XX">TYPE</TYPE_OF_PRODUCT>
            '  <PRODUCT_IND CODE ="XX" >INDICATOR</PRODUCT_IND>
            '  <AREA_OF_PRAC CODE="XX">TYPE</AREA_OF_PRAC>
            '  <HIRE_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
            '  <EMP_TITLE CODE ="XX" >TITLE</EMP_TITLE>
            '  <YEARS_IN_POSITION> years</YEARS_IN_POSITION>
            '  <TERM_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
            '  <PRODUCT_MANUFACTURER>MANUFACTURER NAME</PRODUCT_MANUFACTURER>
            '  <PRODUCT_BRAND_NAME>BRAND NAME</PRODUCT_BRAND_NAME>
            '  <PRODUCT_GENERIC_NAME>PRODUCT GENERIC NAME</PRODUCT_GENERIC_NAME>
            '  <PRODUCT_ALLEGED_HARM> PRODUCT ALLEGEDHARM </PRODUCT_ALLEGED_HARM>
            '  <PREMISES_OTHER>Other</PREMISES_OTHER>
            '  <PREMISES_OTHER_HTML>Other</PREMISES_OTHER_HTML>
            '  <GENERAL_DAMAGES>GENERAL DAMAGE</GENERAL_DAMAGES>
            '  <GENERAL_DAMAGES_HTML>GENERAL DAMAGE HTML</GENERAL_DAMAGES_HTML>
            '  <SPECIAL_DAMAGE>SPECICAL DAMAGE</SPECIAL_DAMAGE>
            '  <SPECIAL_DAMAGE_HTML>SPECICAL DAMAGE</SPECIAL_DAMAGE_HTML>
            '  <WHERE_PRODUCT_SEEN>PRODUCT SEEN</WHERE_PRODUCT_SEEN>
            '  <WHERE_PRODUCT_SEEN_HTML>PRODUCT SEEN HTML</WHERE_PRODUCT_SEEN_HTML>
            '  <PRODUCT_OTHER>PRODUCT OTHER</PRODUCT_OTHER>
            '  <PRODUCT_OTHER_HTML>PRODUCT OTHER</PRODUCT_OTHER_HTML>
            '  <BG_CHECK CODE ="XX">TYPE </BG_CHECK>
            '  <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Affected Party</PARTY_INVOLVED>
            '  <LOSS_INFORMATION> ... </LOSS_INFORMATION>
            '</LIABILITY_LOSS>         
         */
        private XMLLossInfo m_LossInfo;
        private XMLPartyInvolved m_PartyInvolved;

        public XMLLiabilityLoss()
        {
            xmlThisNode = null;
            xmlNodeList = null;
            m_Parent = null;
            sThisNodeOrder = new ArrayList();
            sThisNodeOrder.Add(Constants.sLLNode);
            sThisNodeOrder.Add(Constants.sLLUnitReference);
            sThisNodeOrder.Add(Constants.sLLUnitReferenceID);
            sThisNodeOrder.Add(Constants.sStdTechKey);
            sThisNodeOrder.Add(Constants.sStdTreeLabel);    //SIW490

            sThisNodeOrder.Add(Constants.sLLType);
            sThisNodeOrder.Add(Constants.SLLInjuryType);
            sThisNodeOrder.Add(Constants.sLLDamageType);
            sThisNodeOrder.Add(Constants.sLLAreaOfPractice);
            sThisNodeOrder.Add(Constants.sLLPlntfClmtType);
            sThisNodeOrder.Add(Constants.sLLCovergaeForm);
            sThisNodeOrder.Add(Constants.sLLWithInBizScope);
            sThisNodeOrder.Add(Constants.sLLTypeOfPremises);
            sThisNodeOrder.Add(Constants.sLLTypeOfProduct);
            sThisNodeOrder.Add(Constants.sLLProductInd);
            sThisNodeOrder.Add(Constants.sLLAreaOfPractice);
            sThisNodeOrder.Add(Constants.sLLEmpTitle);

            sThisNodeOrder.Add(Constants.sLLHireDate);
            sThisNodeOrder.Add(Constants.sLLYearsInPosition);
            sThisNodeOrder.Add(Constants.sLLTermDate);

            sThisNodeOrder.Add(Constants.sLLProductManufacturer);
            sThisNodeOrder.Add(Constants.sLLProductBrandName);
            sThisNodeOrder.Add(Constants.sLLProductGenericName);
            sThisNodeOrder.Add(Constants.sLLProductAllegedName);

            sThisNodeOrder.Add(Constants.sLLPremisesOther);
            sThisNodeOrder.Add(Constants.sLLPremisesOtherHTML);
            sThisNodeOrder.Add(Constants.sLLGeneralDamages);
            sThisNodeOrder.Add(Constants.sLLGeneralDamagesHTML);
            sThisNodeOrder.Add(Constants.sLLSpecialDamages);
            sThisNodeOrder.Add(Constants.sLLSpecialDamagesHTML);
            sThisNodeOrder.Add(Constants.sLLWhereProductSeen);
            sThisNodeOrder.Add(Constants.sLLWhereProductSeenHTML);
            sThisNodeOrder.Add(Constants.sLLProductOther);
            sThisNodeOrder.Add(Constants.sLLProductOtherHTML);
            sThisNodeOrder.Add(Constants.sLLProductIncidentIndDt);
            sThisNodeOrder.Add(Constants.sLLBGCheck);
            sThisNodeOrder.Add(Constants.sPartyInvolvedNode);

            sThisNodeOrder.Add(Constants.sLLLossInfo);

        }

        protected override string DefaultNodeName
        {
            get { return Constants.sLLNode; }
        }
        public override XmlNode Create()
        {
            return Create("", "");
        }

        public XmlNode Create(string sUnitReference, string sdescription)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();
                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;
                xmlDocFrag.AppendChild(xmlElement);
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                UnitReferenceID = sUnitReference;
                InsertInDocument(null, null);
                XML.Blanking = bblanking;
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLLiabLoss.Create");
                return null;
            }
        }

        public XMLUnit Unit
        {
            get
            {
                XMLUnit xmlUnit = new XMLUnit();
                LinkXMLObjects(xmlUnit); //SIW529
                xmlUnit.Parent.Parent = Parent;
                if (UnitReferenceID != "" && UnitReferenceID != null)
                    xmlUnit.getUnitbyID(UnitReferenceID);
                return xmlUnit;
            }
        }
        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);
            }
        }

        //Start SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //End SIW490

        public XmlNode putLiabilityType(string sDesc, string sCode)
        {
            return putLiabilityType(sDesc, sCode, "");
        }
        public XmlNode putLiabilityType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLType, sDesc, sCode, NodeOrder, scodeid);
        }
        public string LiabilityType
        {
            get
            {
                return Utils.getData(Node, Constants.sLLType);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLType, value, NodeOrder);
            }
        }
        public string LiabilityType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLType, value, NodeOrder);
            }
        }
        public string LiabilityType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLType, value, NodeOrder);
            }
        }

        public XmlNode putInjuryType(string sDesc, string sCode)
        {
            return putInjuryType(sDesc, sCode, "");
        }
        public XmlNode putInjuryType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.SLLInjuryType, sDesc, sCode, NodeOrder, scodeid);
        }
        public string InjuryType
        {
            get
            {
                return Utils.getData(Node, Constants.SLLInjuryType);
            }
            set
            {
                Utils.putData(putNode, Constants.SLLInjuryType, value, NodeOrder);
            }
        }
        public string InjuryType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.SLLInjuryType);
            }
            set
            {
                Utils.putCode(putNode, Constants.SLLInjuryType, value, NodeOrder);
            }
        }
        public string InjuryType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.SLLInjuryType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.SLLInjuryType, value, NodeOrder);
            }
        }

        public XmlNode putDamageType(string sDesc, string sCode)
        {
            return putDamageType(sDesc, sCode, "");
        }
        public XmlNode putDamageType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLDamageType, sDesc, sCode, NodeOrder, scodeid);
        }
        public string DamageType
        {
            get
            {
                return Utils.getData(Node, Constants.sLLDamageType);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLDamageType, value, NodeOrder);
            }
        }
        public string DamageType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLDamageType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLDamageType, value, NodeOrder);
            }
        }
        public string DamageType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLDamageType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLDamageType, value, NodeOrder);
            }
        }

        public XmlNode putAreaOfPractice(string sDesc, string sCode)
        {
            return putAreaOfPractice(sDesc, sCode, "");
        }
        public XmlNode putAreaOfPractice(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLAreaOfPractice, sDesc, sCode, NodeOrder, scodeid);
        }
        public string AreaOfPractice
        {
            get
            {
                return Utils.getData(Node, Constants.sLLAreaOfPractice);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLAreaOfPractice, value, NodeOrder);
            }
        }
        public string AreaOfPractice_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLAreaOfPractice);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLAreaOfPractice, value, NodeOrder);
            }
        }
        public string AreaOfPractice_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLAreaOfPractice);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLAreaOfPractice, value, NodeOrder);
            }
        }

        public string HireDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sLLHireDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sLLHireDate, value, NodeOrder);
            }
        }
        public string YearsInPosition
        {
            get
            {
                return Utils.getData(Node, Constants.sLLYearsInPosition);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLYearsInPosition, value, NodeOrder);
            }
        }

        public XmlNode putEmpTitle(string sDesc, string sCode)
        {
            return putEmpTitle(sDesc, sCode, "");
        }
        public XmlNode putEmpTitle(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLEmpTitle, sDesc, sCode, NodeOrder, scodeid);
        }
        public string EmpTitle
        {
            get
            {
                return Utils.getData(Node, Constants.sLLEmpTitle);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLEmpTitle, value, NodeOrder);
            }
        }
        public string EmpTitle_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLEmpTitle);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLEmpTitle, value, NodeOrder);
            }
        }
        public string EmpTitle_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLEmpTitle);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLEmpTitle, value, NodeOrder);
            }
        }

        public string TermDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sLLTermDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sLLTermDate, value, NodeOrder);
            }
        }
        public string ProductIncidentIndDt
        {
            get
            {
                return Utils.getDate(Node, Constants.sLLProductIncidentIndDt);
            }
            set
            {
                Utils.putDate(putNode, Constants.sLLProductIncidentIndDt, value, NodeOrder);
            }
        }

        public XmlNode putPlntfClmt(string sDesc, string sCode)
        {
            return putPlntfClmt(sDesc, sCode, "");
        }
        public XmlNode putPlntfClmt(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLPlntfClmtType, sDesc, sCode, NodeOrder, scodeid);
        }
        public string PlntfClmt
        {
            get
            {
                return Utils.getData(Node, Constants.sLLPlntfClmtType);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLPlntfClmtType, value, NodeOrder);
            }
        }
        public string PlntfClmt_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLPlntfClmtType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLPlntfClmtType, value, NodeOrder);
            }
        }
        public string PlntfClmt_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLPlntfClmtType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLPlntfClmtType, value, NodeOrder);
            }
        }

        public XmlNode putCoverageForm(string sDesc, string sCode)
        {
            return putCoverageForm(sDesc, sCode, "");
        }
        public XmlNode putCoverageForm(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLCovergaeForm, sDesc, sCode, NodeOrder, scodeid);
        }
        public string CoverageForm
        {
            get
            {
                return Utils.getData(Node, Constants.sLLCovergaeForm);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLCovergaeForm, value, NodeOrder);
            }
        }
        public string CoverageForm_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLCovergaeForm);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLCovergaeForm, value, NodeOrder);
            }
        }
        public string CoverageForm_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLCovergaeForm);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLCovergaeForm, value, NodeOrder);
            }
        }

        public XmlNode putWithInBizScope(string sDesc, string sCode)
        {
            return putWithInBizScope(sDesc, sCode, "");
        }
        public XmlNode putWithInBizScope(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLWithInBizScope, sDesc, sCode, NodeOrder, scodeid);
        }
        public string WithInBizScope
        {
            get
            {
                return Utils.getData(Node, Constants.sLLWithInBizScope);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLWithInBizScope, value, NodeOrder);
            }
        }
        public string WithInBizScope_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLWithInBizScope);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLWithInBizScope, value, NodeOrder);
            }
        }
        public string WithInBizScope_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLWithInBizScope);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLWithInBizScope, value, NodeOrder);
            }
        }

        public XmlNode putTypeOfPremises(string sDesc, string sCode)
        {
            return putTypeOfPremises(sDesc, sCode, "");
        }
        public XmlNode putTypeOfPremises(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLTypeOfPremises, sDesc, sCode, NodeOrder, scodeid);
        }
        public string TypeOfPremises
        {
            get
            {
                return Utils.getData(Node, Constants.sLLTypeOfPremises);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLTypeOfPremises, value, NodeOrder);
            }
        }
        public string TypeOfPremises_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLTypeOfPremises);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLTypeOfPremises, value, NodeOrder);
            }
        }
        public string TypeOfPremises_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLTypeOfPremises);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLTypeOfPremises, value, NodeOrder);
            }
        }

        public XmlNode putTypeOfProduct(string sDesc, string sCode)
        {
            return putTypeOfProduct(sDesc, sCode, "");
        }
        public XmlNode putTypeOfProduct(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLTypeOfProduct, sDesc, sCode, NodeOrder, scodeid);
        }
        public string TypeOfProduct
        {
            get
            {
                return Utils.getData(Node, Constants.sLLTypeOfProduct);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLTypeOfProduct, value, NodeOrder);
            }
        }
        public string TypeOfProduct_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLTypeOfProduct);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLTypeOfProduct, value, NodeOrder);
            }
        }
        public string TypeOfProduct_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLTypeOfProduct);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLTypeOfProduct, value, NodeOrder);
            }
        }

        public XmlNode putProductInd(string sDesc, string sCode)
        {
            return putProductInd(sDesc, sCode, "");
        }
        public XmlNode putProductInd(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLProductInd, sDesc, sCode, NodeOrder, scodeid);
        }
        public string ProductInd
        {
            get
            {
                return Utils.getData(Node, Constants.sLLProductInd);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLProductInd, value, NodeOrder);
            }
        }
        public string ProductInd_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLProductInd);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLProductInd, value, NodeOrder);
            }
        }
        public string ProductInd_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLProductInd);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLProductInd, value, NodeOrder);
            }
        }

        public XmlNode putAreaOfPrac(string sDesc, string sCode)
        {
            return putAreaOfPrac(sDesc, sCode, "");
        }
        public XmlNode putAreaOfPrac(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLAreaOfPractice, sDesc, sCode, NodeOrder, scodeid);
        }
        public string AreaOfPrac
        {
            get
            {
                return Utils.getData(Node, Constants.sLLAreaOfPractice);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLAreaOfPractice, value, NodeOrder);
            }
        }
        public string AreaOfPrac_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLAreaOfPractice);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLAreaOfPractice, value, NodeOrder);
            }
        }
        public string AreaOfPrac_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLAreaOfPractice);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLAreaOfPractice, value, NodeOrder);
            }
        }

        public string ProductManufacturer
        {
            get
            {
                return Utils.getData(Node, Constants.sLLProductManufacturer);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLProductManufacturer, value, NodeOrder);
            }
        }
        public string ProductBrandName
        {
            get
            {
                return Utils.getData(Node, Constants.sLLProductBrandName);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLProductBrandName, value, NodeOrder);
            }
        }

        public string ProductGenericName
        {
            get
            {
                return Utils.getData(Node, Constants.sLLProductGenericName);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLProductGenericName, value, NodeOrder);
            }
        }
        public string ProductAllegedName
        {
            get
            {
                return Utils.getData(Node, Constants.sLLProductAllegedName);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLProductAllegedName, value, NodeOrder);
            }
        }


        public string PremisesOther
        {
            get
            {
                return Utils.getData(Node, Constants.sLLPremisesOther);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLPremisesOther, value, NodeOrder);
            }
        }
        public string PremisesOtherHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sLLPremisesOtherHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLPremisesOtherHTML, value, NodeOrder);
            }
        }
        public string GeneralDamages
        {
            get
            {
                return Utils.getData(Node, Constants.sLLGeneralDamages);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLGeneralDamages, value, NodeOrder);
            }
        }
        public string GeneralDamagesHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sLLGeneralDamagesHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLGeneralDamagesHTML, value, NodeOrder);
            }
        }
        public string SpecialDamages
        {
            get
            {
                return Utils.getData(Node, Constants.sLLSpecialDamages);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLSpecialDamages, value, NodeOrder);
            }
        }
        public string SpecialDamagesHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sLLSpecialDamagesHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLSpecialDamagesHTML, value, NodeOrder);
            }
        }
        public string WhereProductSeen
        {
            get
            {
                return Utils.getData(Node, Constants.sLLWhereProductSeen);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLWhereProductSeen, value, NodeOrder);
            }
        }
        public string WhereProductSeenHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sLLWhereProductSeenHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLWhereProductSeenHTML, value, NodeOrder);
            }
        }
        public string ProductOther
        {
            get
            {
                return Utils.getData(Node, Constants.sLLProductOther);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLProductOther, value, NodeOrder);
            }
        }
        public string ProductOtherHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sLLProductOtherHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLProductOtherHTML, value, NodeOrder);
            }
        }

        public XmlNode putBGCheck(string sDesc, string sCode)
        {
            return putBGCheck(sDesc, sCode, "");
        }
        public XmlNode putBGCheck(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLLBGCheck, sDesc, sCode, NodeOrder, scodeid);
        }
        public string BGCheck
        {
            get
            {
                return Utils.getData(Node, Constants.sLLBGCheck);
            }
            set
            {
                Utils.putData(putNode, Constants.sLLBGCheck, value, NodeOrder);
            }
        }
        public string BGCheck_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLLBGCheck);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLLBGCheck, value, NodeOrder);
            }
        }
        public string BGCheck_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLLBGCheck);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLLBGCheck, value, NodeOrder);
            }
        }



        private void subClearPointers()
        {
            LossInfo = null;
        }
        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }
        public XMLLossInfo LossInfo
        {
            get
            {
                if (m_LossInfo == null)
                {
                    m_LossInfo = new XMLLossInfo();
                    m_LossInfo.Parent = this;
                    //SIW529 LinkXMLObjects(m_LossInfo);
                }
                return m_LossInfo;
            }
            set
            {
                m_LossInfo = value;
            }
        }
        public string OthrInsPolicy
        {
            get
            {
                return LossInfo.OthrInsPolicy;
            }
            set
            {
                LossInfo.OthrInsPolicy = value;
            }
        }
        public string OthrInsCarrier
        {
            get
            {
                return LossInfo.OthrInsCarrier;
            }
            set
            {
                LossInfo.OthrInsCarrier = value;
            }
        }
        public string OthrInsCarrierID
        {
            get
            {
                return LossInfo.OthrInsCarrierID;
            }
            set
            {
                LossInfo.OthrInsCarrierID = value;
            }
        }
        public int OthrInsCoverageCount
        {
            get
            {
                return LossInfo.OthrInsCoverageCount;
            }
        }
        public XmlNode addOtherInsCoverage()
        {
            return LossInfo.addOthrInsCoverage();
        }
        public XmlNode getOthrInsCoverage(bool reset)
        {
            return LossInfo.getOthrInsCoverage(reset);
        }
        public XmlNode putOthrInsCovType(string sdesc, string scode)
        {
            return LossInfo.putOthrInsCovType(sdesc, scode);
        }
        public string OthrInsCovType
        {
            get
            {
                return LossInfo.OthrInsCovType;
            }
            set
            {
                LossInfo.OthrInsCovType = value;
            }
        }
        public string OthrInsCovType_Code
        {
            get
            {
                return LossInfo.OthrInsCovType_Code;
            }
            set
            {
                LossInfo.OthrInsCovType_Code = value;
            }
        }
        public XmlNode addOthrInsCovLimit()
        {
            return LossInfo.addOthrInsCovLimit();
        }

        public int OthrInsCovLimitCount
        {
            get
            {
                return LossInfo.OthrInsCovLimitCount;
            }
        }

        public XmlNode getOthrInsCovLimit(bool reset)
        {
            return LossInfo.getOthrInsCovLimit(reset);
        }

        public XmlNode putOthrInsCovLimit(string sdesc, string scode)
        {
            return LossInfo.putOthrInsCovLimit(sdesc, scode);
        }

        public string OthrInsCovLimit
        {
            get
            {
                return LossInfo.OthrInsCovLimit;
            }
            set
            {
                LossInfo.OthrInsCovLimit = value;
            }
        }

        public string OthrInsCovLimit_Code
        {
            get
            {
                return LossInfo.OthrInsCovLimit_Code;
            }
            set
            {
                LossInfo.OthrInsCovLimit_Code = value;
            }
        }

        public string OthrInsCovLimitAmount
        {
            get
            {
                return LossInfo.OthrInsCovLimitAmount;
            }
            set
            {
                LossInfo.OthrInsCovLimitAmount = value;
            }
        }


        public string LossDescription
        {
            get
            {
                return LossInfo.LossDescription;
            }
            set
            {
                LossInfo.LossDescription = value;
            }
        }
        public string UnitReferenceID
        {
            get
            {
                string strdata = Utils.getAttribute(Node, Constants.sLLUnitReference, Constants.sLLUnitReferenceID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sUntIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sLLUnitReference, Constants.sLLUnitReferenceID,
                                   Constants.sUntIDPfx + value, NodeOrder);
            }
        }

        public new XMLClaim Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLClaim();
                    ((XMLClaim)m_Parent).LiabilityLoss = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();  //SIW529
                }
                return (XMLClaim)m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null)
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);
            }
        }

        //Start SIW529
        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }
        //End SIW529

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }
        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }
        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }
        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }
        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }
        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }
        public XmlNode getLiabLoss(bool reset)
        {
            return getNode(reset);
        }
        
        public XmlNode getLiabLossByReference(string sref)
        {
            if (sref != UnitReferenceID)
            {
                getFirst();
                while (Node != null)
                {
                    if (sref == UnitReferenceID)
                        break;
                    getNext();
                }
            }
            return Node;
        }
        //Start SIW493
        public XmlNode getLiabLossByTechkey(string stc)
        {
            bool rst = false;
            if (stc != Techkey)
            {
                rst = true;
                while ((getLiabLoss(rst) != null))
                {
                    rst = false;
                    if (Techkey == stc)
                        break;
                }
            }
            return Node;
        }
        //End SIW493
    }
}


