/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 05/17/2010 | SIW360  |   AP       | Techkey position reodered in .NET XML components similar to ordering in CCPXC components.
 * 05/19/2010 | SIW360  |    AS      | Match .NET xml object with XC object
 * 05/21/2010 | SIW321  |    SW      | Effective date and Expiration date added.
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label   
 * 09/01/2010 | SIW493  |   ASM      | Correct rest logic in NodeList
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 12/01/2010 | SIW360  |    AS      | Address ref ID updated for backward compatibility
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 01/24/2011 | SIW529  |    AS      | Added addresses link to load address object attached to address reference
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.XmlFormatting;
using CCP.Common;
using CCP.Constants;

namespace CCP.XmlComponents
{
    public class XMLAddressReference:XMLXCReferenceNode
    {
        /*'  <!--Multiple levels -->
        '   <ADDRESS_REFERENCE ID="RELATIONID"                                                          'SIW360
        '                      ADDRESS_ID="ARDid  References an address in the Addresses Collection {id}"
        '                      CODE="Type of Address Code (Billing Mailing, Home, etc.) {string} [ADDRESS_TYPE]"
        '                      PRIMARY="YES|NO">                                'SI06543
        '     <TREE_LABEL>component label</TREE_LABEL>                    'SIW490
        '      <TYPE>Address Type Translated {string}</TYPE>
        '      <DOING_BUSINESS_AS>dbaname</DOING_BUSINESS_AS>
        '      <EFFECTIVE_DATE YEAR="year" MONTH="month" DAY="day" />
        '      <EXPIRY_DATE YEAR="year" MONTH="month" DAY="day" />
        '      <PARTY_INVOLVED ID="ENTxxx" CODE="Role">
        '       ...
        '      </PARTY_INVOLVED>
        '      <DATA_ITEM/>
        '   </ADDRESS_REFERENCE>
        '     <!-- Multiple Occurrences -->*/

        private XMLVarDataItem m_DataItem;
        private XMLAddress m_Address;
        private XMLAddresses m_Addressess;        //SIW529
        private XMLPartyInvolved m_PartyInvolved; //SIW321
        private XMLCommentReference m_Comment;    //SIW321

        //SIW529 public XMLAddressReference() : this(false) { }

        //SIW529 public XMLAddressReference(bool bUseSharedXMLDocument)
        public XMLAddressReference()    //SIW529
        {
            //SIW529 UseSharedXMLDocument = bUseSharedXMLDocument;
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;
            xmlNodeList = null;
            m_Parent = null;
            m_DataItem = null;
            m_Address = null;
            m_Addressess = null; //SIW529

            //sThisNodeOrder = new ArrayList(7);//SIW360
            sThisNodeOrder = new ArrayList(8);//SIW490
            sThisNodeOrder.Add(Constants.sStdTechKey);//SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel); //SIW490
            sThisNodeOrder.Add(Constants.sARType);
            sThisNodeOrder.Add(Constants.sARDBA);
            sThisNodeOrder.Add(Constants.sAREffDate);//SIW360 
            sThisNodeOrder.Add(Constants.sARExpDate);//SIW360 
            sThisNodeOrder.Add(Constants.sPartyInvolvedNode);//SIW360 
            sThisNodeOrder.Add(Constants.sCmtReference);//SIW360 
            sThisNodeOrder.Add(Constants.sARDataItem);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sAddressReferenceNode; }
        }

        private void CheckAREntry()
        {
            CheckEntry();
        }

        protected override void CheckEntry()
        {
            xEntry(Role, RoleCode, AddressID, Node.ChildNodes.Count);
        }

        public XmlNode putAddressReference(string adrid, string ARrole, string ARrolecode, string ARPrimary,
                                           string ARDBA)
        {
            //SIW360 Starts
            //Node = null;
            //if (adrid != "" && adrid != null && ARrolecode != "" && ARrolecode != null)
            //    findAddressRef(adrid, ARrolecode);
            //if (Node == null)
            //    Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
            //if (adrid != "" && adrid != null)
            //    AddressID = adrid;
            //if (ARrole != "" && ARrole != null)
            //    Role = ARrole;
            //if (ARrolecode != "" && ARrolecode != null)
            //    RoleCode = ARrolecode;
            //if (ARPrimary != "" && ARPrimary != null)
            //    Primary = ARPrimary;
            //if (ARDBA != "" && ARDBA != null)
            //    DoingBusinessAs = ARDBA;
            //return Node;
            return putAddressReference(adrid, ARrole, ARrolecode, ARPrimary,ARDBA, "", "");
            //SIW360 End
        }
        //SIW360 Starts
        public XmlNode putAddressReference(string adrid, string ARrole, string ARrolecode, string ARPrimary,
                                   string ARDBA, string AdrRefID, string sTechKey)
        {
            Node = null;
            if (adrid != "" && adrid != null && ARrolecode != "" && ARrolecode != null)
                findAddressRef(adrid, ARrolecode);
            if (Node == null)
                Node = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
            if (adrid != "" && adrid != null)
                AddressID = adrid;
            if (ARrole != "" && ARrole != null)
                Role = ARrole;
            if (ARrolecode != "" && ARrolecode != null)
                RoleCode = ARrolecode;
            if (ARPrimary != "" && ARPrimary != null)
                Primary = ARPrimary;
            if (ARDBA != "" && ARDBA != null)
                DoingBusinessAs = ARDBA;
            if (sTechKey != "" && sTechKey != null)
                Techkey  = sTechKey;
            if (AdrRefID != "" && AdrRefID != null) //SIW360
                AddressRefID = AdrRefID; //SIW360
            return Node;
        }
        //SIW360 Ends

        public XmlNode getAddressReference(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode findAddressRefbyID(string val)
        {
            string sID = val;
            if (sID != AddressID)
            {
                getFirst();
                while (Node != null)
                {
                    if (sID == AddressID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode findAddressRefbyRole(string srole)
        {
            if (srole != RoleCode)
            {
                getFirst();
                while (Node != null)
                {
                    if (srole == RoleCode)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode findAddressRef(string sID, string srole)
        {
            if (sID != AddressID || srole != RoleCode)
            {
                getFirst();
                while (Node != null)
                {
                    if (sID == AddressID && srole == RoleCode)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public string Role
        {
            get
            {
                return Utils.getData(Node, Constants.sARType);    //SIW529 Remove local XML parameter
            }
            set
            {
                //SIW485 Utils.putData(putNode, Constants.sARType, value, Globals.sNodeOrder);
                Utils.putData(putNode, Constants.sARType, value, Utils.sNodeOrder);	//SIW485    //SIW529 Remove local XML parameter
                if (value == "" || value == null)
                    CheckAREntry();
            }
        }

        public string RoleCode
        {
            get
            {
                return Utils.getCode(Node, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, "", value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        //Start SIW139
        public XmlNode putRole(string sdesc, string scode)
        {
            return putRole(sdesc, scode, "");
        }

        public XmlNode putRole(string sdesc, string scode, string scodeid)
        {
            return Utils.putCodeItem(putNode, "", sdesc, scode, sThisNodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string RoleCodeID
        {
            get
            {
                return Utils.getCodeID(Node, "");    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, "", value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        public string Primary
        {
            get
            {
                return Utils.getBool(Node, "", Constants.sARPrimary);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putBool(putNode, "", Constants.sARPrimary, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
                if (value == "")
                    CheckAREntry();
            }
        }

        public string IDPrefix
        {
            get
            {
                //return Constants.sCCPXmlAddressIDPrefix;
                return Constants.sADREFIDPFX; //SIW360
            }
        }

        public string AddressID
        {
            get
            {
                if (Utils.getAddressRef(Node, "", Constants.sARID) == "")  //SIW493    //SIW529 Remove local XML parameter
                    return AddressRefID;                                        //SIW493
                else                                                            //SIW493
                    return Utils.getAddressRef(Node, "", Constants.sARID);//SIW360    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putAddressRef(Node, "", value, sThisNodeOrder, Constants.sARID); //SIW360    //SIW529 Remove local XML parameter
                if (value == "" || value == null || value == "0")
                    CheckAREntry();
            }
        }

        public string DoingBusinessAs
        {
            get
            {
                return Utils.getData(xmlThisNode, Constants.sARDBA);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sARDBA, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        protected override void ResetParent()
        {
            xmlThisNode = null;
            xmlNodeList = null;
        }

        public override XMLACNode Parent
        {
            set
            {
                m_Parent = value;
                ResetParent();  //SIW529
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        protected override bool AllowNodeNameOverride
        {
            get { return true; }
        }
        
        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                Node = null;
            }
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    putAddressReference("", "", "", "", "");
                return xmlThisNode;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                base.Node = value;
                m_DataItem = null;
                m_Address = null;
                m_Addressess = null; //SIW529
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        public XMLAddress Address
        {
            get
            {
                //Start SIW529
                if (m_Addressess == null)
                {
                    m_Addressess = new XMLAddresses();
                    LinkXMLObjects(m_Addressess);
                }
                if (m_Address == null)
                {   //SIW529 
                    //m_Address = new XMLAddress();
                    //LinkXMLObjects(m_Address);  //SIW529
                    m_Address = m_Addressess.Address;   
                }   //SIW529 
                //End SIW529
                m_Address.getAddressbyID(AddressID);
                return m_Address;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();      //SI06420
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }
        //SIW360 Starts
        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);    //SIW529 Remove local XML parameter
            }
            set
            {
                //SIW485 Utils.putData(putNode, Constants.sStdTechKey, value, Globals.sNodeOrder);
                Utils.putData(putNode, Constants.sStdTechKey, value, Utils.sNodeOrder);	//SIW485    //SIW529 Remove local XML parameter
            }
        }

        //Start SIW490 
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);    //SIW529 Remove local XML parameter
            }
            set
            {
                //SI485 Utils.putData(putNode, Constants.sStdTreeLabel, value, Globals.sNodeOrder);
                Utils.putData(putNode, Constants.sStdTreeLabel, value, Utils.sNodeOrder);  //SI485    //SIW529 Remove local XML parameter
            }
        }
        //End SIW490
        //Start SIW321
        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sAREffDate);    //SIW529 Remove local XML parameter
            }
            set
            {
                //SIW485 Utils.putDate(putNode, Constants.sAREffDate, value, Globals.sNodeOrder);
                Utils.putDate(putNode, Constants.sAREffDate, value, Utils.sNodeOrder);	//SIW485    //SIW529 Remove local XML parameter
            }
        }
        public string ExpiryDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sARExpDate);    //SIW529 Remove local XML parameter
            }
            set
            {
                //SIW485 Utils.putDate(putNode, Constants.sARExpDate, value, Globals.sNodeOrder);
                Utils.putDate(putNode, Constants.sARExpDate, value, Utils.sNodeOrder);	//SIW485    //SIW529 Remove local XML parameter
            }
        }
        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    //SIW485 m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Document, Node, this, Globals.sNodeOrder);
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, Utils.sNodeOrder);	//SIW485    //SIW529 Removed Document parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    //SIW485 ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Document, Node, this, Globals.sNodeOrder);
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, Utils.sNodeOrder);	//SIW485    //SIW529 Removed Document parameter
            }
        }
        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    //SIW485 m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Document, Node, this, Globals.sNodeOrder);
                    m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, Utils.sNodeOrder);	//SIW485    //SIW529 Removed Document parameter
                    m_Comment.getFirst();  
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    //SIW485 ((Utils)Utils).funSetupCommentReference(m_Comment, Document, Node, this, Globals.sNodeOrder);
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, Utils.sNodeOrder);	//SIW485    //SIW529 Removed Document parameter
            }
        }
        //End SIW321

        public string AddressRefID
        {
            get
            {
                string sAdrRefID, strdata;
                sAdrRefID = "";
                strdata = XML.XMLGetAttributeValue(Node, Constants.sADREFID); // SIW360
                if (strdata.Trim() != "")
                {
                    if ((strdata.Substring(0, 2) == Constants.sADREFIDPFX) && (strdata.Length >= 3))
                    {
                        sAdrRefID = StringUtils.Right(strdata,(strdata.Length - 2));                       
                    }// Start SIW360:  for backward compatibility
                    else if ((strdata.Substring(0, 3) == Constants.sARIDPfx) && (strdata.Length >= 4))
                    {
                        sAdrRefID = StringUtils.Right(strdata, (strdata.Length - 3)); 
                    }//End SIW360
                }
                return sAdrRefID;
            }
            set
            {
                string lid = string.Empty;
                if (value == null || value == "" || value == "0")
                {
                    XMLGlobals.lARID = XMLGlobals.lARID + 1;                    
                    lid = XMLGlobals.lARID.ToString();
                }
                else
                {
                    lid = value;
                }
                XML.XMLSetAttributeValue(putNode, Constants.sADREFID, Constants.sADREFIDPFX + lid.Trim());
            }
        }
        //SIW360 Ends
    }
}
