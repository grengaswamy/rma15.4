﻿/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 11/11/2011 | SIW7182 |    RG     | Handling Entity Account XML
 * 09/07/2011 | SIW7473 |    RG      |  Added Account ID(CheckBook Id)
 * 03/30/2012 | SIW8315 |   PS       | Entity Account Adding Primary flag
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLAccount : XMLXCNodeWithList
    {
        /*'*****************************************************************************
        ' The cXMLAccount class provides the functionality to support and manage a
        ' Entity's Account Node
        '*****************************************************************************
'<ENTITY>
'  <!-- Multiple Account Blocks Allowed-->
'   <ACCOUNT ID='ACC123'>
'       <TECH_KEY>AC Technical key for the element </TECH_KEY>
'       <TREE_LABEL>component label</TREE_LABEL>
'       <CHECK_BOOK_ID>ARV1</CHECK_BOOK_ID>                  //SIW7473
'       <ACCOUNT_NAME>Carol de costa</ACCOUNT_NAME>
'       <ACCOUNT_CATEGORY CODE="ACH">arch</ACCOUNT_CATEGORY>
'       <ACCOUNT_TYPE CODE="CHK">checkings</ACCOUNT_TYPE>
'       <BANK_NAME>martha international</BANK_NAME>
'       <BANK_ADDRESS1>Dallas</BANK_ADDRESS1>
'       <BANK_ADDRESS2>Texas</BANK_ADDRESS2>
'       <ACCOUNT_NUMBER></ACCOUNT_NUMBER> 
'       <ROUTING_NUMBER></ROUTING_NUMBER> 
'       <IBAN></IBAN>
'       <SORT_CODE></SORT_CODE>
'       <SWIFT_CODE></SWIFT_CODE>
'       <ADDITIONAL_DETAILS> turn of the indicator<ADDITIONAL_DETAILS>
'       <CURRENCY_ID  CODE=""></CURRENCY_ID>
'       <DATE_PRE_NOTE_SENT YEAR="2011" MONTH="03" DAY="07" />
'       <EFFECTIVE_DATE YEAR="2011" MONTH="03" DAY="07" />
'       <EXPIRATION_DATE YEAR="2011" MONTH="03" DAY="07" />
'       <INACTIVATE_EFT INDICATOR="True|False"/>
'       <FINANCIAL_INFO_STATUS CODE="CRT">created</FINANCIAL_INFO_STATUS>
'       <DISALLOW_PAYMENT_REASON CODE="PRO">vendor</DISALLOW_PAYMENT_REASON>
'   </ACCOUNT>
'</ENTITY>
        '*****************************************************************************/


        private XmlNode m_xmlAccount;
        private XMLEntity m_Entity;
        private XMLCommentReference m_Comment;

        public XMLAccount()
        {
            xmlNodeList = null;

            //sThisNodeOrder = new ArrayList(22);        //SIW7473
            //Start SIW8315
			// sThisNodeOrder = new ArrayList(23);        //SIW7473   
            sThisNodeOrder = new ArrayList(24);
            //End SIW8315
            sThisNodeOrder.Add(Constants.sStdTechKey);         
            sThisNodeOrder.Add(Constants.sStdTreeLabel);
            sThisNodeOrder.Add(Constants.sChkBookId);        //SIW7473
            sThisNodeOrder.Add(Constants.sAccNm);
            sThisNodeOrder.Add(Constants.sAccCat);
            sThisNodeOrder.Add(Constants.sAccType);
            sThisNodeOrder.Add(Constants.sBankName);
            sThisNodeOrder.Add(Constants.sBankAddress1);
            sThisNodeOrder.Add(Constants.sBankAddress2);
            sThisNodeOrder.Add(Constants.sAccNumber);
            sThisNodeOrder.Add(Constants.sRoutNumber);
            sThisNodeOrder.Add(Constants.sIban);
            sThisNodeOrder.Add(Constants.sSortCd);
            sThisNodeOrder.Add(Constants.sSwiftCd);
            sThisNodeOrder.Add(Constants.sAdditionalDetail);
            sThisNodeOrder.Add(Constants.sCurrencyId);
            sThisNodeOrder.Add(Constants.sDatePreNoteSnt);
            sThisNodeOrder.Add(Constants.sEffectiveDate);
            sThisNodeOrder.Add(Constants.sExpirationDate);
            sThisNodeOrder.Add(Constants.sInactivateEft);
            sThisNodeOrder.Add(Constants.sFinancialInfoStatus);
            sThisNodeOrder.Add(Constants.sDisallowPaymentRsn);
            sThisNodeOrder.Add(Constants.sAccComment);
            sThisNodeOrder.Add(Constants.sPrimaryAccount); //SIW8315

        }

        protected override string DefaultNodeName
        {
            get { return Constants.sAccNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "","");
        }

        public XmlNode Create(string saccid, string sacctype, string sacctypecode, string sbankname, string saccnum, string sroutenum)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false); 
                Node = (XmlNode)xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                bool bblanking = XML.Blanking;
                XML.Blanking = false;


                AccountID = saccid;
                AccountType = sacctype;
                AccountType_Code = sacctypecode;
                BankName = sbankname;
                AccountNumber = saccnum;
                RoutingNumber = sroutenum;

                InsertInDocument(null, null);

                XML.Blanking = bblanking; 
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XML.Account.Create");
                return null;
            }
        }

        //Text Fields
        //Start SIW7473
        public string CheckBookId
        {
            get
            {
                return Utils.getData(Node, Constants.sChkBookId);
            }
            set
            {
                Utils.putData(putNode, Constants.sChkBookId, value, NodeOrder);
            }
        }
        //End SIW7473
        public string AccountName
        {
            get
            {
                return Utils.getData(Node, Constants.sAccNm);    
            }
            set
            {
                Utils.putData(putNode, Constants.sAccNm, value, NodeOrder);    
            }
        }

        public string BankName
        {
            get
            {
                return Utils.getData(Node, Constants.sBankName);    
            }
            set
            {
                Utils.putData(putNode, Constants.sBankName, value, NodeOrder);    
            }
        }

        public string BankAddress1
        {
            get
            {
                return Utils.getData(Node, Constants.sBankAddress1);
            }
            set
            {
                Utils.putData(putNode, Constants.sBankAddress1, value, NodeOrder);
            }
        }

        public string BankAddress2
        {
            get
            {
                return Utils.getData(Node, Constants.sBankAddress2);
            }
            set
            {
                Utils.putData(putNode, Constants.sBankAddress2, value, NodeOrder);
            }
        }

        public string AccountNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sAccNumber);    
            }
            set
            {
                Utils.putData(putNode, Constants.sAccNumber, value, NodeOrder);    
            }
        }


        public string RoutingNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sRoutNumber);    
            }
            set
            {
                Utils.putData(putNode, Constants.sRoutNumber, value, NodeOrder);    
            }
        }

        public string Iban
        {
            get
            {
                return Utils.getData(Node, Constants.sIban);    
            }
            set
            {
                Utils.putData(putNode, Constants.sIban, value, NodeOrder);    
            }
        }

        public string SortCode
        {
            get
            {
                return Utils.getData(Node, Constants.sSortCd);    
            }
            set
            {
                Utils.putData(putNode, Constants.sSortCd, value, NodeOrder);    
            }
        }

        public string SwiftCode
        {
            get
            {
                return Utils.getData(Node, Constants.sSwiftCd);    
            }
            set
            {
                Utils.putData(putNode, Constants.sSwiftCd, value, NodeOrder);    
            }
        }


        public string AdditionalDetail
        {
            get
            {
                return Utils.getData(Node, Constants.sAdditionalDetail);    
            }
            set
            {
                Utils.putData(putNode, Constants.sAdditionalDetail, value, NodeOrder);    
            }
        }

        //Date Fields
        public string DataPreNoteSent
        {
            get
            {
                return Utils.getDate(Node, Constants.sDatePreNoteSnt);
            }
            set
            {
                Utils.putDate(putNode, Constants.sDatePreNoteSnt, value, NodeOrder);
            }
        }

        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEffectiveDate);    
            }
            set
            {
                Utils.putDate(putNode, Constants.sEffectiveDate, value, NodeOrder);    
            }
        }
        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sExpirationDate);    
            }
            set
            {
                Utils.putDate(putNode, Constants.sExpirationDate, value, NodeOrder);    
            }
        }
        //Code Properties

        public XmlNode putAccountCategory(string sDesc, string sCode)
        {
            return putAccountCategory(sDesc, sCode, "");
        }

        public XmlNode putAccountCategory(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sAccCat, sDesc, sCode, NodeOrder, scodeid);    
        }

        public string AccountCategory
        {
            get
            {
                return Utils.getData(Node, Constants.sAccCat);    
            }
            set
            {
                Utils.putData(putNode, Constants.sAccCat, value, NodeOrder);    
            }
        }

        public string AccountCategory_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sAccCat);    
            }
            set
            {
                Utils.putCode(putNode, Constants.sAccCat, value, NodeOrder);    
            }
        }

        public string AccountCategory_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sAccCat);    
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sAccCat, value, NodeOrder);    
            }
        }

        public XmlNode putAccountType(string sDesc, string sCode)
        {
            return putAccountType(sDesc, sCode, "");
        }

        public XmlNode putAccountType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sAccType, sDesc, sCode, NodeOrder, scodeid);    
        }

        public string AccountType
        {
            get
            {
                return Utils.getData(Node, Constants.sAccType);    
            }
            set
            {
                Utils.putData(putNode, Constants.sAccType, value, NodeOrder);    
            }
        }

        public string AccountType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sAccType);    
            }
            set
            {
                Utils.putCode(putNode, Constants.sAccType, value, NodeOrder);    
            }
        }

        public string AccountType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sAccType);    
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sAccType, value, NodeOrder);    
            }
        }

        public XmlNode putCurrencyId(string sDesc, string sCode)
        {
            return putCurrencyId(sDesc, sCode, "");
        }

        public XmlNode putCurrencyId(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sCurrencyId, sDesc, sCode, NodeOrder, scodeid);    
        }

        public string CurrencyId
        {
            get
            {
                return Utils.getData(Node, Constants.sCurrencyId);    
            }
            set
            {
                Utils.putData(putNode, Constants.sCurrencyId, value, NodeOrder);    
            }
        }

        public string CurrencyId_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCurrencyId);    
            }
            set
            {
                Utils.putCode(putNode, Constants.sCurrencyId, value, NodeOrder);    
            }
        }

        public string CurrencyId_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sCurrencyId);    
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sCurrencyId, value, NodeOrder);    
            }
        }

        public XmlNode putFinancialInfoSts(string sDesc, string sCode)
        {
            return putFinancialInfoSts(sDesc, sCode, "");
        }

        public XmlNode putFinancialInfoSts(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sFinancialInfoStatus, sDesc, sCode, NodeOrder, scodeid);    
        }

        public string FinancialInfoSts
        {
            get
            {
                return Utils.getData(Node, Constants.sFinancialInfoStatus);    
            }
            set
            {
                Utils.putData(putNode, Constants.sFinancialInfoStatus, value, NodeOrder);    
            }
        }

        public string FinancialInfoSts_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sFinancialInfoStatus);    
            }
            set
            {
                Utils.putCode(putNode, Constants.sFinancialInfoStatus, value, NodeOrder);    
            }
        }

        public string FinancialInfoSts_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sFinancialInfoStatus);    
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sFinancialInfoStatus, value, NodeOrder);    
            }
        }

        public XmlNode putDisallowPaymentRsn(string sDesc, string sCode)
        {
            return putDisallowPaymentRsn(sDesc, sCode, "");
        }

        public XmlNode putDisallowPaymentRsn(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sDisallowPaymentRsn, sDesc, sCode, NodeOrder, scodeid);    
        }

        public string DisallowPaymentRsn
        {
            get
            {
                return Utils.getData(Node, Constants.sDisallowPaymentRsn);    
            }
            set
            {
                Utils.putData(putNode, Constants.sDisallowPaymentRsn, value, NodeOrder);    
            }
        }

        public string DisallowPaymentRsn_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDisallowPaymentRsn);    
            }
            set
            {
                Utils.putCode(putNode, Constants.sDisallowPaymentRsn, value, NodeOrder);    
            }
        }

        public string DisallowPaymentRsn_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sDisallowPaymentRsn);    
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sDisallowPaymentRsn, value, NodeOrder);    
            }
        }
       
        //Bool fields
        public string InactivateEft
        {
            get
            {
                return Utils.getBool(Node, Constants.sInactivateEft, "");    
            }
            set
            {
                Utils.putBool(putNode, Constants.sInactivateEft, "", value, NodeOrder);    
            }
        }

        //Start SIW8315
        public string PrimaryAccount
        {
            get
            {
                return Utils.getBool(Node, Constants.sPrimaryAccount, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sPrimaryAccount, "", value, NodeOrder);
            }
        }
        //End SIW8315

        public string IDPrefix
        {
            get
            {
                return Constants.sACCIDPfx;
            }
        }

        public string AccountID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sAccID);
                string aid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sACCIDPfx && strdata.Length >= 4)
                        aid = StringUtils.Right(strdata, strdata.Length - 3);
                    else
                        aid = strdata;
                return aid;
            }
            set
            {
                string aid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lAccId = Globals.lAccId + 1;
                    aid = Globals.lAccId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sAccID, Constants.sACCIDPfx + (aid + ""));
            }
        }

        public string Techkey
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);    
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);    
            }
        }

        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);    
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);    
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getAccount(bool reset)
        {
            return getNode(reset);
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLEntity Parent
        {
            get
            {
                if (m_Entity == null)
                {
                    m_Entity = new XMLEntity();
                    m_Entity.Account = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Entity);
                    ResetParent();
                }
                return m_Entity;
            }
            set
            {
                m_Entity = value;
                ResetParent();
                if (m_Entity != null)
                    ((XMLXCNodeBase)m_Entity).LinkXMLObjects(this);
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);    
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);    
            }
        }

        private void subClearPointers()
        {
            CommentReference = null;
        }

        public XmlNode getAccountbyID(string saccid)
        {
            if (AccountID != saccid)
            {
                bool rst = true;
                while (getAccount(rst) != null)
                {
                    rst = false;
                    if (AccountID == saccid)
                        break;
                }
            }
            return Node;
        }

    }
}
