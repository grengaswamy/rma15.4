/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/13/2010 | SIW493  |    AS      | Tree label added
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLInsUnit : XMLXCNodeWithList, XMLXCIntEndorsement, XMLXCIntSItem, XMLXCIntPartyInvolved  //SIW122
    {
        /*<INSURED_UNIT UNIT_NUMBER="xx">
        '   <UNIT_REFERENCE ID="UNTxxx"/>
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '   <TREE_LABEL>Tree label </TREE_LABEL>  'SIW493
        '   <INSURANCE_LINE CODE="WC"></INSURANCE_LINE>
        '   <EFFECTIVE_DATE YEAR="2001" MONTH="08" DAY="01" />
        '   <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '   <LIMIT> ...... </LIMIT>
        '     <!-- Multiple Limits -->
        '   <DEDUCTIBLE> ...... </DEDUCTIBLE>
        '     <!-- Multiple Deductibles -->
        '   <SCHEDULED_ITEM> ...   </SCHEDULED_ITEM>
        '   <COVERAGE_DATA> ... </COVERAGE_DATA>
        '   <ASSIGNED_DRIVER ID="ENTrefid" NUMBER="nn" RELATION_TO_INSURED="reltnCode">Relation to Insured</ASSIGNED_DRIVER>
        '   <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '     <!--Muliple Party Involved nodes allowed -->
        '   <COMMENT_REFERENCE ID="CMTxx"/>
        '     <!-- multiple entries -->
        '   <DATA_ITEM> ... </DATA_ITEM>                   'SI04920        
        '</INSURED_UNIT>
        '  <!-- Multiple Units Allowed -->*/

        private XmlNode m_xmlDriver;
        private XmlNodeList m_xmlDriverList;
        //private IEnumerator m_xmlDriverListEnum;
        private XMLUnit m_Unit;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLCoverage m_Coverage;
        private XMLLimit m_Limit;
        private XMLDeductible m_Deductible;
        private XMLSchedItem m_SchedItem;
        private new XMLPolicy m_Parent;
        private XMLEndorsement m_Endorsement;
        private XMLVarDataItem m_DataItem;
        private int lIutID; //SIW490
        
        public XMLInsUnit()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;
            m_Parent = null;

            lIutID = 0;

            //sThisNodeOrder = new ArrayList(12);               //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(13);                 //(SI06023 - Implemented in SI06333) //SIW493
            sThisNodeOrder = new ArrayList(14);                 //SIW493
            sThisNodeOrder.Add(Constants.sInsUntReference);
            sThisNodeOrder.Add(Constants.sStdTechKey);       //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);       //SIW493
            sThisNodeOrder.Add(Constants.sInsUntInsLine);
            sThisNodeOrder.Add(Constants.sInsUntEffDate);
            sThisNodeOrder.Add(Constants.sInsUntExpDate);
            sThisNodeOrder.Add(Constants.sInsUntLimit);
            sThisNodeOrder.Add(Constants.sInsUntDeductible);
            sThisNodeOrder.Add(Constants.sInsUntSchedItem);
            sThisNodeOrder.Add(Constants.sInsUntCoverage);
            sThisNodeOrder.Add(Constants.sInsUntEndorsement);
            sThisNodeOrder.Add(Constants.sInsUntPartyInvolved);
            sThisNodeOrder.Add(Constants.sInsUntComment);
            sThisNodeOrder.Add(Constants.sInsUntDataItem);
            //sThisNodeOrder.Add(Constants.sInsUntTechKey);       //(SI06023 - Implemented in SI06333)//SIW360
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sInsUntNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "");
        }

        public XmlNode Create(string sUnitID, string sUnitNumber, string sInsuaranceLine, string sInsuranceLinecode,
                              string sEffectiveDate, string sExpirationDate)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                UnitReferenceID = sUnitID;
                UnitNumber = sUnitNumber;
                putInsLine(sInsuaranceLine, sInsuranceLinecode);
                EffectiveDate = sEffectiveDate;
                ExpirationDate = sExpirationDate;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLInsUnit.Create");
                return null;
            }
        }

        public string UnitNumber
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sInsUntNumber);
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sInsUntNumber, value, NodeOrder);
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            if (m_Parent != null)
                m_Parent.Unit = this;
            Node = null;
        }

        private void subClearPointers()
        {
            m_xmlDriver = null;
            m_xmlDriverList = null;

            PartyInvolved = null;
            CommentReference = null;
            Coverage = null;
            Limit = null;
            Deductible = null;
            Unit = null;
            DataItem = null;
        }

        public string UnitReferenceID
        {
            get
            {
                string strdata = Utils.getAttribute(Node, Constants.sInsUntReference, Constants.sInsUntRefID);
                string lid = "";
                if (strdata != "")
                    if (StringUtils.Left(strdata, 3) == Constants.sUntIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sInsUntReference, Constants.sInsUntRefID, Constants.sUntIDPfx + value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putInsLine(string sDesc, string sCode)
        {
            return putInsLine(sDesc, sCode, "");
        }

        public XmlNode putInsLine(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sInsUntInsLine, sDesc, sCode, NodeOrder, scodeid);
        }

        public string InsuranceLine
        {
            get
            {
                return Utils.getData(Node, Constants.sInsUntInsLine);
            }
            set
            {
                Utils.putData(putNode, Constants.sInsUntInsLine, value, NodeOrder);
            }
        }

        public string InsuranceLine_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sInsUntInsLine);
            }
            set
            {
                Utils.putCode(putNode, Constants.sInsUntInsLine, value, NodeOrder);
            }
        }

        public string InsuranceLine_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sInsUntInsLine);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sInsUntInsLine, value, NodeOrder);
            }
        }
        //End SIW139

        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sInsUntEffDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInsUntEffDate, value, NodeOrder);
            }
        }

        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sInsUntExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInsUntExpDate, value, NodeOrder);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey); //SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)
        //Start SIW493
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //End SIW493

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLCoverage Coverage
        {
            get
            {
                if (m_Coverage == null)
                {
                    m_Coverage = new XMLCoverage();
                    m_Coverage.Parent = this;
                    //SIW529 LinkXMLObjects(m_Coverage);
                    m_Coverage.getCoverage(Constants.xcGetFirst);   //SI06420
                }
                return m_Coverage;
            }
            set
            {
                m_Coverage = value;
            }
        }

        public new XMLPolicy Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLPolicy();
                    ((XMLPolicy)m_Parent).Unit = this;  //SIW529
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                //SIN204 return Parent.putNode;
                return Parent.Node; //SIN204
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getInsUnit(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode AddAssignedDriver(string snumber, string sEntID, string sRltnToInsDesc, string sRltnToInsCode)
        {
            m_xmlDriver = XML.XMLaddNode(putNode, Constants.sInsUntVehAsgnDriver, NodeOrder);
            if (snumber != "" && snumber != null)
                DriverNumber = snumber;
            if (sEntID != "" && sEntID != null)
                DriverEntID = sEntID;
            if ((sRltnToInsDesc != "" && sRltnToInsDesc != null) || (sRltnToInsCode != "" && sRltnToInsCode != null))
                putDriverRltnToInsured(sRltnToInsDesc, sRltnToInsCode);
            return DriverNode;
        }

        public XmlNode putDriverNode
        {
            get
            {
                if (DriverNode == null)
                    AddAssignedDriver("", "", "", "");
                return DriverNode;
            }
        }

        public XmlNode DriverNode
        {
            get
            {
                return m_xmlDriver;
            }
            set
            {
                m_xmlDriver = value;
            }
        }

        public XmlNodeList DriverList
        {
            get
            {
                //Start SIW163
                if (null == m_xmlDriverList)
                {
                    return getNodeList(Constants.sInsUntVehAsgnDriver, ref m_xmlDriverList, Node);
                }
                else
                {
                    return m_xmlDriverList;
                }
                //End SIW163
            }
        }

        public int DriverCount
        {
            get
            {
                return DriverList.Count;
            }
        }

        public XmlNode getDriver(bool reset)
        {
            //Start SIW163
            if (null != DriverList)
            {
                return getNode(reset, ref m_xmlDriverList, ref m_xmlDriver);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void RemoveDriver()
        {
            if (m_xmlDriver != null)
                Node.RemoveChild(m_xmlDriver);
        }

        public string DriverNumber
        {
            get
            {
                return Utils.getAttribute(DriverNode, "", Constants.sInsUntVehAsgnDrvrNumber);
            }
            set
            {
                Utils.putAttribute(putDriverNode, "", Constants.sInsUntVehAsgnDrvrNumber, value, NodeOrder);
            }
        }

        public string DriverEntID
        {
            get
            {
                return Utils.getEntityRef(DriverNode, "");
            }
            set
            {
                Utils.putEntityRef(putDriverNode, "", value, NodeOrder);
            }
        }

        public XmlNode putDriverRltnToInsured(string sDesc, string sCode)
        {
            if (sDesc != "" && sDesc != null)
                DriverRelationToInsured = sDesc;
            if (sCode != "" && sCode != null)
                DriverRelationToInsured_Code = sCode;
            return m_xmlDriver;
        }

        public string DriverRelationToInsured
        {
            get
            {
                return Utils.getData(DriverNode, "");
            }
            set
            {
                Utils.putData(putDriverNode, "", value, NodeOrder);
            }
        }

        public string DriverRelationToInsured_Code
        {
            get
            {
                return Utils.getAttribute(DriverNode, "", Constants.sInsUntVehAsgnDrvrRltnToIns);
            }
            set
            {
                Utils.putAttribute(putDriverNode, "", Constants.sInsUntVehAsgnDrvrRltnToIns, value, NodeOrder);
            }
        }

        public XMLSchedItem ScheduledItem
        {
            get
            {
                if (m_SchedItem == null)
                {
                    m_SchedItem = new XMLSchedItem();
                    m_SchedItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_SchedItem);
                    m_SchedItem.getScheduledItem(Constants.xcGetFirst); //SI06420
                }
                return m_SchedItem;
            }
            set
            {
                m_SchedItem = value;
            }
        }

        public XMLLimit Limit
        {
            get
            {
                if (m_Limit == null)
                {
                    m_Limit = new XMLLimit();
                    m_Limit.Parent = this;
                    //SIW529 LinkXMLObjects(m_Limit);
                    m_Limit.getFirst();     //SI06420
                }
                return m_Limit;
            }
            set
            {
                m_Limit = value;
            }
        }

        public XMLDeductible Deductible
        {
            get
            {
                if (m_Deductible == null)
                {
                    m_Deductible = new XMLDeductible();
                    m_Deductible.Parent = this;
                    //SIW529 LinkXMLObjects(m_Deductible);
                    m_Deductible.getFirst();    //SI06420
                }
                return m_Deductible;
            }
            set
            {
                m_Deductible = value;
            }
        }

        public XmlNode getUnitbyID(string sID)
        {
            if (UnitReferenceID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (UnitReferenceID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getUnitbyNumber(string snum)
        {
            if (UnitNumber != snum)
            {
                getFirst();
                while (Node != null)
                {
                    if (UnitNumber == snum)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XMLUnit Unit
        {
            get
            {
                if (m_Unit == null)
                {
                    m_Unit = new XMLUnit();
                    m_Unit.Parent = Parent.Parent.InvolvedUnits;
                }
                if (UnitReferenceID != "")
                    if (m_Unit.getUnitbyID(UnitReferenceID) == null)
                        m_Unit.Create(xcUntType.xcUTUndefined, UnitReferenceID,"","");
                return m_Unit;
            }
            set
            {
                m_Unit = value;
            }
        }

        public string Description
        {
            get
            {
                return Unit.Description;
            }
            set
            {
                Unit.Description = value;
            }
        }

        public string UnitType_Code
        {
            get
            {
                return Unit.UnitType_Code;
            }
            set
            {
                Unit.UnitType_Code = value;
            }
        }

        public xcUntType UnitType
        {
            get
            {
                return Unit.UnitType;
            }
            set
            {
                Unit.UnitType = value;
            }
        }

        public string UnitID
        {
            get
            {
                return Unit.UnitID;
            }
            set
            {
                Unit.UnitID = value;
            }
        }

        public string VehicleMake
        {
            get
            {
                return Unit.VehicleMake;
            }
            set
            {
                Unit.VehicleMake = value;
            }
        }

        public string VehicleModel
        {
            get
            {
                return Unit.VehicleModel;
            }
            set
            {
                Unit.VehicleModel = value;
            }
        }

        public string VehicleYear
        {
            get
            {
                return Unit.VehicleYear;
            }
            set
            {
                Unit.VehicleYear = value;
            }
        }

        public string VehicleSerialNo
        {
            get
            {
                return Unit.VehicleSerialNo;
            }
            set
            {
                Unit.VehicleSerialNo = value;
            }
        }

        public XmlNode putVehicleType(string sType, string stypecode)
        {
            return Unit.putVehicleType(sType, stypecode);
        }

        public string VehicleType
        {
            get
            {
                return Unit.VehicleType;
            }
            set
            {
                Unit.VehicleType = value;
            }
        }

        public string VehicleType_Code
        {
            get
            {
                return Unit.VehicleType_Code;
            }
            set
            {
                Unit.VehicleType_Code = value;
            }
        }

        public string VehicleRegLicense
        {
            get
            {
                return Unit.VehicleRegLicense;
            }
            set
            {
                Unit.VehicleRegLicense = value;
            }
        }

        public string VehicleRegDate
        {
            get
            {
                return Unit.VehicleRegDate;
            }
            set
            {
                Unit.VehicleRegDate = value;
            }
        }

        public XmlNode putVehicleRegState(string sstate, string sstatecode)
        {
            return Unit.putVehicleRegState(sstate, sstatecode);
        }

        public string VehicleRegState
        {
            get
            {
                return Unit.VehicleRegState;
            }
            set
            {
                Unit.VehicleRegState = value;
            }
        }

        public string VehicleRegState_Code
        {
            get
            {
                return Unit.VehicleRegState_Code;
            }
            set
            {
                Unit.VehicleRegState_Code = value;
            }
        }

        public XmlNode putLocation(string sID, string sstate, string sstatecode)
        {
            return Unit.putLocation(sID, sstate, sstatecode);
        }

        public string LocationAddressRefID
        {
            get
            {
                return Unit.LocationAddressRefID;
            }
            set
            {
                Unit.LocationAddressRefID = value;
            }
        }

        public string LocationState
        {
            get
            {
                return Unit.LocationState;
            }
            set
            {
                Unit.LocationState = value;
            }
        }

        public string LocationState_Code
        {
            get
            {
                return Unit.LocationState_Code;
            }
            set
            {
                Unit.LocationState_Code = value;
            }
        }

        public XmlNode AddBuilding(string snumber, string sdescription, string sConsDesc, string sConsCode, string sConsYear)
        {
            return Unit.AddBuilding(snumber, sdescription, sConsDesc, sConsCode, sConsYear);
        }

        public string BuildingNumber
        {
            get
            {
                return Unit.BuildingNumber;
            }
            set
            {
                Unit.BuildingNumber = value;
            }
        }

        public string BuildingDescription
        {
            get
            {
                return Unit.BuildingDescription;
            }
            set
            {
                Unit.BuildingDescription = value;
            }
        }

        public string YearOfConstruction
        {
            get
            {
                return Unit.YearOfConstruction;
            }
            set
            {
                Unit.YearOfConstruction = value;
            }
        }

        public XmlNode putConstruction(string sConst, string sConstCode)
        {
            return Unit.putConstruction(sConst, sConstCode);
        }

        public string Construction
        {
            get
            {
                return Unit.Construction;
            }
            set
            {
                Unit.Construction = value;
            }
        }

        public string Construction_Code
        {
            get
            {
                return Unit.Construction_Code;
            }
            set
            {
                Unit.Construction_Code = value;
            }
        }

        public XMLEndorsement Endorsement
        {
            get
            {
                if (m_Endorsement == null)
                {
                    m_Endorsement = new XMLEndorsement();
                    m_Endorsement.Parent = this;
                    //SIW529 LinkXMLObjects(m_Endorsement);
                    m_Endorsement.getEndorsement(Constants.xcGetFirst); //SI06420
                }
                return m_Endorsement;
            }
            set
            {
                m_Endorsement = value;
            }
        }

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();      //SI06420
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }
        // Start SIW490
        public string InsUnitID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sInsID);
                string lid = "";
                if (strdata != "")
                    if (StringUtils.Left(strdata, 3) == Constants.sInsIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value; ;
                if (value == null || value == "" || value == "0")
                {
                    lIutID = lIutID + 1;
                    lid = lIutID + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sInsID, Constants.sInsIDPfx + lid);
            }
        }
        public string IDPrefix
        {
            get
            {
                return Constants.sInsIDPfx;
            }
        }        
        // End SIW490
    }
}
