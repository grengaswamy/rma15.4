/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 06/17/2008 | SI06369 |    sw      | Implemented HTML to replace RTF in RadEditors 
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 10/31/2008 | SI06467 |    ASM     | Remove the Treament Code from this class and add it to its own.
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 07/16/2009 | SIW189  |   SW       | Implemented HTML for fields of OshaInfo, added NO_RULES_FLAG and RECORDABLE_FLAG
 * 09/09/2009 | SIW44   |   SW       | Field YearLastExposed fixed
 * 10/05/2009 | SIW209  |    JTC     | Fixes from retrofits
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490  |    ASM     | Tree Label    
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean.
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *08/11/2011 | SIW7309 | umahajan2   | Added New fields
 * 08/19/2011 | SIW7419  |    RG     | Demand Offer added
  * 05/24/2012 | SIN8529 |    PV      | Implemented Organization Hierarchy
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;
using CCP.XmlFormatting;
using System.Collections;
using CCP.Constants;
using System.Xml;

namespace CCP.XmlComponents
{
    public class XMLInjury : XMLXCNodeWithList, XMLXCIntPartyInvolved  //SIW122
    {
        /*****************************************************************************
        '<CLAIM>
        '   <INJURY ID="INJxxx">
        '   <!-- Multiple Injuries Allowed -->
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '      <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '      <INJURED_PARTY ENTITY_ID="ENTxxx"/>
        '      <SPECIFICS ON_PREMISE_IND="YES|NO {boolean}"
        '                 SAFETY_EQP_USED="YES|NO {boolean}"
        '                 SAFETY_EQP_AVAIL="YES|NO {boolean}"/>
        '      <ILLNESS INDICATOR="TRUE|FALSE"/>
        '      <INJURY INDICATOR="TRUE|FALSE"/>
        '      <NATURE_OF_INJURY CODE="80">Default</NATURE_OF_INJURY>
        '      <CAUSE_OF_INJURY CODE="99">Default</CAUSE_OF_INJURY>
        '      <ESTIMATED_LENGTH_DISABILITY>0</ESTIMATED_LENGTH_DISABILITY>
        '      <WORK_ACTIVITY>The specific work activity that the employee was engage in. {string}</WORK_ACTIVITY>
        '      <WORK_ACTIVITY_HTML>The specific work activity that the employee was engage in.HTML format {string}</WORK_ACTIVITY_HTML> 'SIW189
        '      <WORK_PROCESS>The work process that the employee was engaged in  {string}</WORK_PROCESS>
        '      <WORK_PROCESS_HTML>The work process that the employee was engaged in.HTML format {string}</WORK_PROCESS_HTML> 'SIW189
        '      <DESCRIPTION>Describe injury or accident {string}</DESCRIPTION>
        '      <DESCRIPTION_HTML>Describe injury or accident, HTML format {string}</DESCRIPTION_HTML> 'SIW189
        '      <DESCRIPTION_OF_SYMPTOMS>Describe the symptoms{string}</DESCRIPTION_OF_SYMPTOMS>
        '      <DESC_SYMPT_HTML>Describe the symptoms{string} in HTML format</DESC_SYMPT_HTML>    'SI06369
        '      <VOCATIONAL_REHAB INDICATOR="No" />
        '      <SURGERY INDICATOR="No" />
        '      <PRIOR_INJURY INDICATOR="No" />                      'SI05300
        '      <PRIOR_CLAIM INDICATOR="No" />                       'SI05300
        '      <RECORDED_INTERVIEW INDICATOR="No" />                'SI05300
        '      <EOB INDICATOR="No" />                               'SI05300
        '      <LOST_CONSCIOUSNESS INDICATOR="No" />
        '      <INJURY_CONSISTENT_WITH_POLICE_REPORT INDICATOR="YES|NO">
        '      <CATASTROPHIC_INJURY INDICATOR="YES|NO"/>
        '      <HOSPITAL_ER_VISIT INDICATORY="YES|NO"/>
        '      <NO_RULES_FLAG INDICATOR="YES|NO" />   'SIW189
        '      <RECORDABLE_FLAG INDICATOR="YES|NO" /> 'SIW189
        '      <FILE_CLOSE_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <COLLATERAL_SOURCE CODE="">collateral source</COLLATERAL_SOURCE>
        '      <MANAGED_CARE_ORG_TYPE CODE="typeoforg">Organization Type</MANAGED_CARE_ORG_TYPE>
        '      <DATE_OF_INJURY YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <TIME_OF_INJURY YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <DATE_DISABILITY_BEGAN YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <DATE_DISABILITY_END YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <DATE_MAXIMUM_MEDICAL_IMPROVEMENT YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <PERCENT_OF_DISABILITY>percent</PERCENT_OF_DISABILITY>
        '      <DATE_OF_DEATH YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <DEPARTMENT CODE="Department Code {string} [ORG_HIERARCHY]">Deparment Where Injury Occurred {string}</DEPARTMENT>
        '      <YEAR_LAST_EXPOSED YEAR="yyyy/>
        '      <MATERIAL_INVOLVED>List of materials (Chemical, Biological, Equipment) involved {string}</MATERIAL_INVOLVED>
        '          <!-- Multiple Lines Allowed -->
        '      <BODY_PART CODE="Body Part Code {string} [BODY_PART]">Translated Body Part {string}</BODY_PART>
        '          <!-- Multiple Body Parts Allowed -->
        '      <PRE_EXISTING_CONDITION CODE="code">ncci diagnosis codes</PRE_EXISTING_CONDITION>
        '      <DIAGNOSIS>
        '         <SPECIFIC CODE="ICD-9 Code {string} [ICD9]">Diagnosis Translated {string}</SPECIFIC>
        '         <DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      </DIAGNOSIS>
        '           <!-- Multiple Diagonsis Codes Allowed -->
        '      <TREATMENT>
        '         <SPECIFIC CODE="Standard CPT Treatment Code {string} [TREATMENT_CODE]" TYPE="Code Type (CPT,HCFA, etc.) {string} [CPT,HCFA]">description</SPECIFIC>
        '         <STARTED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <ENDED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <TREATED_BY ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</TREATED_BY>
        '         <DATE_INITIAL_VISIT YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>      'SI04900
        '         <PHYSICIAN_SPECIALITY CODE ="DOC">description</PHYSICIAN_SPECIALITY>      'SI04900
        '         <PRE_CERTIFICATION><!--Multiple Pre-Cert allowed--></PRE_CERTIFICATION>       'SI06467
        '         <IME><!--Multiple IME allowed--></IME>                                        'SI06467
        '      </TREATMENT>
        '         <!--Multiple treatments allowed-->
        '      <MEDICATION CODE="" DOSE="">description</MEDICATION>
        '         <!--Multiple Medication allowed-->
        '      <WEEKLY_OFFSET>Amount</WEEKLY_OFFSET>       'SI04900
        'Start SIW7309
        '   <DESCRIPTION_OF_PROGNOSIS>Describe the prognosis{string}</DESCRIPTION_OF_PROGNOSIS>
        '   <DESCRIPTION_OF_PROGNOSIS_HTML>Describe the prognosis{string}</DESCRIPTION_OF_PROGNOSIS_HTML>
        '   <IMPAIRMENT_FLAG INDICATOR="YES|NO"/>
        '   <WHERE_INJURED_TAKEN>Address (string)</WHEN_INJURED_TAKEN>
        'End SIW7309         
        '      <EMPLOYEE>
        '         ..Employee Information
        '      </EMPLOYEE>
        '      <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '           <!--Multiple Parties Involved-->
        '      <!Multiple Demand Offers Allowed>               //SIW7419
        '      <DEMAND_OFFER> ... </DEMAND_OFFER>
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '           <!-- multiple entries -->   
        '   <ORG_DEPT_EID ENTITY_ID="ENTXXX"></ORG_DEPT_EID> 'SIN8529
        '   </INJURY>
        '</CLAIM>*/

        //SIW529 private XMLTreatment m_Treatment;  //SI06467
        private XmlNode xmlMaterial;
        private XmlNode xmlMedication;
        private XmlNode xmlBodyPart;
        private XmlNode xmlDiagnosis;
        private XmlNode xmlTreatment;   //SIW139
        //Start SIW7309
        private XmlNode xmlPrognosis;
        private XmlNode xmlPrognosisHTML;
        private XmlNode xmlImpairmentFlag;
        private XmlNode xmlWhereInjuredTaken;
        //End SIW7309
        private XmlNodeList xmlMaterialList;
        //private IEnumerator xmlMaterialListEnum;
        private XmlNodeList xmlMedicationList;
        //private IEnumerator xmlMedicationListEnum;
        private XmlNodeList xmlBodyPartList;
        //private IEnumerator xmlBodypartListEnum;
        private XmlNodeList xmlDiagnosisList;
        //private IEnumerator xmlDiagnosisListEnum;
        private XmlNodeList xmlTreatmentList;   //SIW139
        //private IEnumerator xmlTreatmentListEnum;
        private XMLEmployee m_Employee;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLDemandOffer m_DemandOffer;     //SIW7419
        private XMLCommentReference m_Comment;
		//SIW529 private XMLClaim m_Claim;
        private ArrayList sDiagNodeOrder;
        private ArrayList sTreatNodeOrder;
        private XMLVarDataItem m_DataItem;  //SI06420
        
        public XMLInjury()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            xmlNodeList = null;

            //sThisNodeOrder = new ArrayList(44);               //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(48);                 //SIW189  //SIW490
            //sThisNodeOrder = new ArrayList(49);                 //SIW490 //SIW7309
            //sThisNodeOrder = new ArrayList(53);                 //SIW7309 //SIW7419
            //sThisNodeOrder = new ArrayList(54);                 //SIW7419 //SIN8529
            sThisNodeOrder = new ArrayList(55); //SIN8529
            sThisNodeOrder.Add(Constants.sStdTechKey);              //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel); //SIW490
            sThisNodeOrder.Add(Constants.sInjParty);
            sThisNodeOrder.Add(Constants.sInjSpecifics);
            sThisNodeOrder.Add(Constants.sInjIllness);
            sThisNodeOrder.Add(Constants.sInjInjury);
            sThisNodeOrder.Add(Constants.sInjNatureOfInjury);
            sThisNodeOrder.Add(Constants.sInjCauseOfInjury);
            sThisNodeOrder.Add(Constants.sInjEstLenOfDisability);
            sThisNodeOrder.Add(Constants.sInjWorkActivity);
            sThisNodeOrder.Add(Constants.sInjWorkActivityHTML); //SI906369
            sThisNodeOrder.Add(Constants.sInjWorkProcess);
            sThisNodeOrder.Add(Constants.sInjWorkProcessHTML);  //SIW189
            sThisNodeOrder.Add(Constants.sInjDescriptionOfInjury);
            sThisNodeOrder.Add(Constants.sInjDescriptionOfInjuryHTML); //SIW189
            sThisNodeOrder.Add(Constants.sInjDescriptionOfSymptoms);
            sThisNodeOrder.Add(Constants.sInjDescriptionOfSymptomsHTML); //SIW189
            sThisNodeOrder.Add(Constants.sInjVocRehabilition);
            sThisNodeOrder.Add(Constants.sInjSurgery);
            sThisNodeOrder.Add(Constants.sInjPriorInjury);
            sThisNodeOrder.Add(Constants.sInjPriorClaim);
            sThisNodeOrder.Add(Constants.sInjRecordedInterview);
            sThisNodeOrder.Add(Constants.sInjEOB);
            sThisNodeOrder.Add(Constants.sInjLostCons);
            sThisNodeOrder.Add(Constants.sInjConWPolRpt);
            sThisNodeOrder.Add(Constants.sInjCatasInjury);
            sThisNodeOrder.Add(Constants.sInjHospER);
            sThisNodeOrder.Add(Constants.sInjNoRulesFlag);   //SIW189
            sThisNodeOrder.Add(Constants.sInjRecordableFlag);//SIW189    
            sThisNodeOrder.Add(Constants.sInjFileCloseDate);
            sThisNodeOrder.Add(Constants.sInjCollSource);
            sThisNodeOrder.Add(Constants.sInjManagedCareOrgType);
            sThisNodeOrder.Add(Constants.sInjDateOfInjury);
            sThisNodeOrder.Add(Constants.sInjTimeOfInjury);
            sThisNodeOrder.Add(Constants.sInjDateDisBegan);
            sThisNodeOrder.Add(Constants.sInjDateDisEnd);
            sThisNodeOrder.Add(Constants.sInjDateMaxMedImp);
            sThisNodeOrder.Add(Constants.sInjPrctOfDis);
            sThisNodeOrder.Add(Constants.sInjDateOfDeath);
            sThisNodeOrder.Add(Constants.sInjInvolvedDepartment);
            sThisNodeOrder.Add(Constants.sInjYrLastExposed);
            sThisNodeOrder.Add(Constants.sInjMaterialInvolved);
            sThisNodeOrder.Add(Constants.sInjBodyPart);
            sThisNodeOrder.Add(Constants.sInjPreExistCond);
            sThisNodeOrder.Add(Constants.sInjDiagnosis);
            sThisNodeOrder.Add(Constants.sInjTreatment);
            sThisNodeOrder.Add(Constants.sInjMedication);
            sThisNodeOrder.Add(Constants.sInjEmployee);
            sThisNodeOrder.Add(Constants.sInjPartyInvolved);
            sThisNodeOrder.Add(Constants.sInjDemandOffer);            //SIW7419
            sThisNodeOrder.Add(Constants.sInjComment);
            sThisNodeOrder.Add(Constants.sInjWeeklyOffset);
            sThisNodeOrder.Add(Constants.sInjOrgDept);//SIN8529
            //SIW7309 Start
            sThisNodeOrder.Add(Constants.sInjDescriptionOfPrognosis);
            sThisNodeOrder.Add(Constants.sInjDescriptionOfPrognosisHTML);
            sThisNodeOrder.Add(Constants.sInjImpairmentFlag);
            sThisNodeOrder.Add(Constants.sInjWhereInjuredTaken);
            //SIW7309 END
            //sThisNodeOrder.Add(Constants.sInjTechKey);              //(SI06023 - Implemented in SI06333)//SIW360

            sDiagNodeOrder = new ArrayList(2);
            sDiagNodeOrder.Add(Constants.sInjDiagSpecific);
            sDiagNodeOrder.Add(Constants.sInjDiagDate);

            /*Start SI06467*/   //SIW209 Undo comment out
            sTreatNodeOrder = new ArrayList(6);
            sTreatNodeOrder.Add(Constants.sInjTreatSpecific);
            sTreatNodeOrder.Add(Constants.sInjTreatStarted);
            sTreatNodeOrder.Add(Constants.sInjTreatEnded);
            sTreatNodeOrder.Add(Constants.sInjTreatedBy);
            sTreatNodeOrder.Add(Constants.sInjDateInitialVisit);
            sTreatNodeOrder.Add(Constants.sInjPhysicianSpeciality);
             /*End SI06467 */   //SIW209 Undo comment out
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sInjNode; }
        }

        public override XmlNode Create()
        {
            //SIW189return Create("", "", "", "", "", "", "", "", "", "", "", "", "", "");
            return Create("", "", "", "", "", "", "", "", "", "", "", "", "", "","","",""); //SIW189
        }

        //SIW189 public XmlNode Create(string sInjuryId, string sInjuredEntityID, string sOnPremise, string sSafetyEqptUsed,
        //                      string sSaftetyEqptAvailable, string sNatureOfInjury, string sNatureOfInjuryCode,
        //                      string sCauseOfInjury, string sCauseOfInjuryCode, string sInjuryDate, string sInjuryTime,
        //                      string sdescription, string sWorkActivity, string sWorkProcess)
        public XmlNode Create(string sInjuryId, string sInjuredEntityID, string sOnPremise, string sSafetyEqptUsed,
                              string sSaftetyEqptAvailable, string sNatureOfInjury, string sNatureOfInjuryCode,
                              string sCauseOfInjury, string sCauseOfInjuryCode, string sInjuryDate, string sInjuryTime,
                              string sdescription,string sdescriptionHTML, string sWorkActivity,string sWorkActivityHTML,
                              string sWorkProcess, string sWorkProcessHTML) //SIW189
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;


            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                InjuryID = sInjuryId;
                InjuredParty = sInjuredEntityID;
                if (sOnPremise != "" && sOnPremise != null)
                    OnPremise = sOnPremise;
                if (sSaftetyEqptAvailable != "" && sSaftetyEqptAvailable != null)
                    SafetyEqpAvailable = sSaftetyEqptAvailable;
                if (sSafetyEqptUsed != "" && sSafetyEqptUsed != null)
                    SafetyEqpUsed = sSafetyEqptUsed;
                putNatureOfInjury(sNatureOfInjury, sNatureOfInjuryCode);
                putCauseOfInjury(sCauseOfInjury, sCauseOfInjuryCode);
                DateOfInjury = sInjuryDate;
                TimeOfInjury = sInjuryTime;
                DescriptionOfInjury = sdescription;
                DescriptionOfInjuryHTML = sdescriptionHTML; //SIW189
                WorkActivity = sWorkActivity;
                WorkActivityHTML = sWorkActivityHTML; //SIW189 
                WorkProcess = sWorkProcess;
                WorkProcessHTML = sWorkProcessHTML; //SIW189

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLInjury.Create");
                return null;
            }
        }

        public string InjuredParty
        {
            get
            {
                return Utils.getEntityRef(Node, Constants.sInjParty);
            }
            set
            {
                Utils.putEntityRef(putNode, Constants.sInjParty, value, NodeOrder);
            }
        }

        public XMLEntity InjuredEntity
        {
            get
            {
                XMLEntity xmlEntity = new XMLEntity();
                //SIW529 xmlEntity.Document = Document;
                LinkXMLObjects(xmlEntity);  //SIW529
                if (InjuredParty != "" && InjuredParty != null)
                    xmlEntity.getEntitybyID(InjuredParty);
                return xmlEntity;
            }
        }

        public string OnPremise
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjSpecifics, Constants.sInjOnPremise);
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjSpecifics, Constants.sInjOnPremise, value, NodeOrder);
            }
        }

        public string SafetyEqpUsed
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjSpecifics, Constants.sInjSafetyEqpUsed);
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjSpecifics, Constants.sInjSafetyEqpUsed, value, NodeOrder);
            }
        }

        public string SafetyEqpAvailable
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjSpecifics, Constants.sInjSafetyEqpAvailable);
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjSpecifics, Constants.sInjSafetyEqpAvailable, value, NodeOrder);
            }
        }

        public string VocRehabilition
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjVocRehabilition, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjVocRehabilition, "", value, NodeOrder);
            }
        }

        public string Surgery
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjSurgery, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjSurgery, "", value, NodeOrder);
            }
        }

        public string PriorInjury
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjPriorInjury, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjPriorInjury, "", value, NodeOrder);
            }
        }

        public string PriorClaim
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjPriorClaim, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjPriorClaim, "", value, NodeOrder);
            }
        }

        public string RecordedInterview
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjRecordedInterview, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjRecordedInterview, "", value, NodeOrder);
            }
        }

        public string EOB
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjEOB, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjEOB, "", value, NodeOrder);
            }
        }

        public string LostConsciousness
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjLostCons, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjLostCons, "", value, NodeOrder);
            }
        }

        public string InjConWPolRpt
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjConWPolRpt, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjConWPolRpt, "", value, NodeOrder);
            }
        }

        public string CatastrophicInjury
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjCatasInjury, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjCatasInjury, "", value, NodeOrder);
            }
        }

        public string HospitalERVisit
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjHospER, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjHospER, "", value, NodeOrder);
            }
        }
        //Start SIW189
        public string NoRulesFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjNoRulesFlag, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjNoRulesFlag, "", value, NodeOrder);
            }
        }
        public string RecordableFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjRecordableFlag, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjRecordableFlag, "", value, NodeOrder);
            }
        }
        //End SIW189

        public string FileCloseDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sInjFileCloseDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInjFileCloseDate, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putCollateralSource(string sDesc, string sCode)
        {
            return putCollateralSource(sDesc, sCode, "");
        }

        public XmlNode putCollateralSource(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sInjCollSource, sDesc, sCode, NodeOrder, scodeid);
        }

        public string CollateralSource
        {
            get
            {
                return Utils.getData(Node, Constants.sInjCollSource);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjCollSource, value, NodeOrder);
            }
        }

        public string CollateralSource_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sInjCollSource);
            }
            set
            {
                Utils.putCode(putNode, Constants.sInjCollSource, value, NodeOrder);
            }
        }

        public string CollateralSource_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sInjCollSource);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sInjCollSource, value, NodeOrder);
            }
        }
        //End SIW139

        public string Illness
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjIllness, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjIllness, "", value, NodeOrder);
            }
        }

        public string Injury
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjInjury, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjInjury, "", value, NodeOrder);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //Start SIW490
        public string TreeLabel 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //End SIW490
        public string IDPrefix
        {
            get
            {
                return Constants.sInjIDPfx;
            }
        }

        public string InjuryID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sInjID);
                string lid = "";

                if (StringUtils.Left(strdata, 3) == Constants.sInjIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lInjId = Globals.lInjId + 1;
                    lid = Globals.lInjId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sInjID, Constants.sInjIDPfx + lid);
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
                if (xmlThisNode != null)               //SIW7419
                    DemandOffer.getFirst();    
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLClaim Parent
        {
            get
            {// Start SIW529
                if (m_Parent == null)
                {
                    m_Parent = new XMLClaim();
                    ((XMLClaim)m_Parent).Injury = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   
                    ResetParent();
                }
                return (XMLClaim)m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    
            }// End SIW529
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        private void subClearPointers()
        {
            xmlMaterial = null;
            xmlMaterialList = null;
            xmlBodyPart = null;
            xmlBodyPartList = null;
            xmlDiagnosis = null;
            xmlDiagnosisList = null;
            //SI06467 xmlTreatment = null;
            //SI06467 xmlTreatmentList = null;
            xmlMedication = null;
            xmlMedicationList = null;

            Employee = null;
            PartyInvolved = null;
            DemandOffer = null;            //SIW7419
            CommentReference = null;
        }

        public string WorkActivity
        {
            get
            {
                return Utils.getData(Node, Constants.sInjWorkActivity);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjWorkActivity, value, NodeOrder);
            }
        }
        //Start SIW189
        public string WorkActivityHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sInjWorkActivityHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjWorkActivityHTML, value, NodeOrder);
            }
        }
        //End SIW189
        //Start SIN8529
        public string OrgDeptEID
        {
            get
            {
            
                return Utils.getEntityRef(Node, Constants.sInjOrgDept);
            }
            set
            {
      
                Utils.putEntityRef(putNode, Constants.sInjOrgDept, value, NodeOrder);
            }
        }
        public string OrgDeptEntDisplayName
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sInjOrgDept, Constants.sEntDisplayName); 
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sInjOrgDept, Constants.sEntDisplayName, value, NodeOrder); 
            }
        }
        //End SIN8529
        public string WorkProcess
        {
            get
            {
                return Utils.getData(Node, Constants.sInjWorkProcess);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjWorkProcess, value, NodeOrder);
            }
        }
        //Start SIW189
        public string WorkProcessHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sInjWorkProcessHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjWorkProcessHTML, value, NodeOrder);
            }
        }
        //End SIW189
        public string EstimatedLengthOfDisability
        {
            get
            {
                return Utils.getData(Node, Constants.sInjEstLenOfDisability);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjEstLenOfDisability, value, NodeOrder);
            }
        }

        public string DescriptionOfInjury
        {
            get
            {
                return Utils.getData(Node, Constants.sInjDescriptionOfInjury);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjDescriptionOfInjury, value, NodeOrder);
            }
        }
        //Start SIW189
        public string DescriptionOfInjuryHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sInjDescriptionOfInjuryHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjDescriptionOfInjuryHTML, value, NodeOrder);
            }
        }
        //End SIW189

        public string DescriptionOfSymptoms
        {
            get
            {
                return Utils.getData(Node, Constants.sInjDescriptionOfSymptoms);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjDescriptionOfSymptoms, value, NodeOrder);
            }
        }
        
        //Start SI06369
        public string DescriptionOfSymptomsHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sInjDescriptionOfSymptomsHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjDescriptionOfSymptomsHTML, value, NodeOrder);
            }
        }
        //End SI06369

        public XmlNode putCauseOfInjury(string sDesc, string sCode)
        {
            return putCauseOfInjury(sDesc, sCode, "");
        }

        public XmlNode putCauseOfInjury(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sInjCauseOfInjury, sDesc, sCode, NodeOrder, scodeid);
        }

        public string CauseOfInjury
        {
            get
            {
                return Utils.getData(Node, Constants.sInjCauseOfInjury);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjCauseOfInjury, value, NodeOrder);
            }
        }

        public string CauseOfInjury_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sInjCauseOfInjury);
            }
            set
            {
                Utils.putCode(putNode, Constants.sInjCauseOfInjury, value, NodeOrder);
            }
        }

        public string CauseOfInjury_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sInjCauseOfInjury);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sInjCauseOfInjury, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putNatureOfInjury(string sDesc, string sCode)
        {
            return putNatureOfInjury(sDesc, sCode, "");
        }

        public XmlNode putNatureOfInjury(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sInjNatureOfInjury, sDesc, sCode, NodeOrder, scodeid);
        }

        public string NatureOfInjury
        {
            get
            {
                return Utils.getData(Node, Constants.sInjNatureOfInjury);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjNatureOfInjury, value, NodeOrder);
            }
        }

        public string NatureOfInjury_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sInjNatureOfInjury);
            }
            set
            {
                Utils.putCode(putNode, Constants.sInjNatureOfInjury, value, NodeOrder);
            }
        }

        public string NatureOfInjury_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sInjNatureOfInjury);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sInjNatureOfInjury, value, NodeOrder);
            }
        }
        //End SIW139

        public XmlNode putPreExistingCondition(string sDesc, string sCode)
        {
            return putPreExistingCondition(sDesc, sCode, "");
        }

        public XmlNode putPreExistingCondition(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sInjPreExistCond, sDesc, sCode, NodeOrder, scodeid);
        }

        public string PreExistingCondition
        {
            get
            {
                return Utils.getData(Node, Constants.sInjPreExistCond);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjPreExistCond, value, NodeOrder);
            }
        }

        public string PreExistingCondition_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sInjPreExistCond);
            }
            set
            {
                Utils.putCode(putNode, Constants.sInjPreExistCond, value, NodeOrder);
            }
        }

        public string PreExistingCondition_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sInjPreExistCond);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sInjPreExistCond, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putManagedCareOrgType(string sDesc, string sCode)
        {
            return putManagedCareOrgType(sDesc, sCode, "");
        }

        public XmlNode putManagedCareOrgType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sInjManagedCareOrgType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string ManagedCareOrgType
        {
            get
            {
                return Utils.getData(Node, Constants.sInjManagedCareOrgType);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjManagedCareOrgType, value, NodeOrder);
            }
        }

        public string ManagedCareOrgType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sInjManagedCareOrgType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sInjManagedCareOrgType, value, NodeOrder);
            }
        }

        public string ManagedCareOrgType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sInjManagedCareOrgType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sInjManagedCareOrgType, value, NodeOrder);
            }
        }
        //End SIW139

        //Start SIW139
        public XmlNode putInvolvedDepartment(string sDesc, string sCode)
        {
            return putInvolvedDepartment(sDesc, sCode, "");
        }

        public XmlNode putInvolvedDepartment(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sInjInvolvedDepartment, sDesc, sCode, NodeOrder, scodeid);
        }

        public string InvolvedDepartment
        {
            get
            {
                return Utils.getData(Node, Constants.sInjInvolvedDepartment);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjInvolvedDepartment, value, NodeOrder);
            }
        }

        public string InvolvedDepartment_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sInjInvolvedDepartment);
            }
            set
            {
                Utils.putCode(putNode, Constants.sInjInvolvedDepartment, value, NodeOrder);
            }
        }

        public string InvolvedDepartment_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sInjInvolvedDepartment);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sInjInvolvedDepartment, value, NodeOrder);
            }
        }

        public string TimeOfInjury
        {
            get
            {
                return Utils.getTime(Node, Constants.sInjTimeOfInjury);
            }
            set
            {
                Utils.putTime(putNode, Constants.sInjTimeOfInjury, value, NodeOrder);
            }
        }

        public string DateOfInjury
        {
            get
            {
                return Utils.getDate(Node, Constants.sInjDateOfInjury);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInjDateOfInjury, value, NodeOrder);
            }
        }

        public string YearLastExposed
        {
            get
            {
                //SIW44 return Utils.getDate(Node, Constants.sInjYrLastExposed);
                return Utils.getAttribute(Node, Constants.sInjYrLastExposed, Constants.sInjYrLastExposedAtt); //SIW44
            }
            set
            {
                //SIW44 Utils.putDate(putNode, Constants.sInjYrLastExposed, value, NodeOrder);
                Utils.putAttribute(putNode, Constants.sInjYrLastExposed, Constants.sInjYrLastExposedAtt, value, sThisNodeOrder);//SIW44
            }
        }

        public string DateOfDeath
        {
            get
            {
                return Utils.getDate(Node, Constants.sInjDateOfDeath);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInjDateOfDeath, value, NodeOrder);
            }
        }

        public string DateDisabilityBegan
        {
            get
            {
                return Utils.getDate(Node, Constants.sInjDateDisBegan);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInjDateDisBegan, value, NodeOrder);
            }
        }

        public string DateDisabilityEnd
        {
            get
            {
                return Utils.getDate(Node, Constants.sInjDateDisEnd);
            }
            set
            {
                Utils.putDate(putNode, Constants.sInjDateDisEnd, value, NodeOrder);
            }
        }

        public string DateMaxMedicalImprovement
        {
            get
            {
                return Utils.getDate(Node, Constants.sInjDateMaxMedImp);
            }
            set
            {   
                Utils.putDate(putNode, Constants.sInjDateMaxMedImp, value, NodeOrder);
            }
        }

        public string PercentDisability
        {
            get
            {
                return Utils.getData(Node, Constants.sInjPrctOfDis);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjPrctOfDis, value, NodeOrder);
            }
        }

        public string WeeklyOffset
        {
            get
            {
                return Utils.getData(Node, Constants.sInjWeeklyOffset);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjWeeklyOffset, value, NodeOrder);
            }
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        //Begin SI06420
        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }
        //End SI06420

        public XMLEmployee Employee
        {
            get
            {
                if (m_Employee == null)
                {
                    m_Employee = new XMLEmployee();
                    m_Employee.Parent = this;
                    //SIW529 LinkXMLObjects(m_Employee);
                }
                return m_Employee;
            }
            set
            {
                m_Employee = value;
            }
        }

        //Start SIW7419
        public XMLDemandOffer DemandOffer
        {
            get
            {
                if (m_DemandOffer == null)
                {
                    m_DemandOffer = new XMLDemandOffer();
                    m_DemandOffer.Parent = this;
                    //SIW529 LinkXMLObjects(m_DemandOffer);
                    m_DemandOffer.getFirst();
                }
                return m_DemandOffer;
            }
            set
            {
                m_DemandOffer = value;
            }
        }
        //End SIW7419

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getInjury(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode getInjurybyID(string sInjID)
        {
            getFirst();
            while (Node != null)
            {
                if (InjuryID == sInjID)
                    break;
                getNext();
            }
            return Node;
        }

        public XmlNode getInjurybyInjParty(string sInjID)
        {
            getFirst();
            while (Node != null)
            {
                if (InjuredParty == sInjID)
                    break;
                getNext();
            }
            return Node;
        }

        public int MaterialInvolvedCount
        {
            get
            {
                return MaterialInvolvedList.Count;
            }
        }

        public XmlNodeList MaterialInvolvedList
        {
            get
            {
                //Start SIW163
                if (null == xmlMaterialList)
                {
                    return getNodeList(Constants.sInjMaterialInvolved, ref xmlMaterialList, Node);
                }
                else
                {
                    return xmlMaterialList;
                }
                //End SIW163
            }
        }

        public XmlNode getMaterialInvolved(bool reset)
        {
            //Start SIW163
            if (null != MaterialInvolvedList)
            {
                return getNode(reset, ref xmlMaterialList, ref xmlMaterial);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeMaterialInvolved()
        {
            if (MaterialInvolvedNode != null)
                Node.RemoveChild(MaterialInvolvedNode);
            MaterialInvolvedNode = null;
        }

        public XmlNode putMaterialInvolvedNode
        {
            get
            {
                if (MaterialInvolvedNode == null)
                    MaterialInvolvedNode = XML.XMLaddNode(putNode, Constants.sInjMaterialInvolved, NodeOrder);
                return MaterialInvolvedNode;
            }
        }

        public XmlNode MaterialInvolvedNode
        {
            get
            {
                return xmlMaterial;
            }
            set
            {
                xmlMaterial = value;
            }
        }

        public XmlNode addMaterialInvolved(string sMaterial)
        {
            MaterialInvolvedNode = null;
            MaterialInvolved = sMaterial;
            return MaterialInvolvedNode;
        }

        private void CheckMaterial()
        {
            if (MaterialInvolved == "")
                removeMaterialInvolved();
        }

        public string MaterialInvolved
        {
            get
            {
                return Utils.getData(MaterialInvolvedNode, "");
            }
            set
            {
                Utils.putData(putMaterialInvolvedNode, "", value, NodeOrder);
            }
        }

        public int BodyPartCount
        {
            get
            {
                return BodyPartList.Count;
            }
        }

        public XmlNodeList BodyPartList
        {
            get
            {
                //Start SIW163
                if (null == xmlBodyPartList)
                {
                    return getNodeList(Constants.sInjBodyPart, ref xmlBodyPartList, Node);
                }
                else
                {
                    return xmlBodyPartList;
                }
                //End SIW163
            }
        }

        public XmlNode getBodyPart(bool reset)
        {
            //Start SIW163
            if (null != BodyPartList)
            {
                return getNode(reset, ref xmlBodyPartList, ref xmlBodyPart);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeBodyPart()
        {
            if (BodyPartNode != null)
                Node.RemoveChild(BodyPartNode);
            BodyPartNode = null;
        }

        public XmlNode putBodyPartNode
        {
            get
            {
                if (BodyPartNode == null)
                    BodyPartNode = XML.XMLaddNode(putNode, Constants.sInjBodyPart, NodeOrder);
                return BodyPartNode;
            }
        }

        public XmlNode BodyPartNode
        {
            get
            {
                return xmlBodyPart;
            }
            set
            {
                xmlBodyPart = value;
            }
        }

        //Start SIW139
        public XmlNode addBodyPart(string sBodyPart, string sCode)
        {
            return addBodyPart(sBodyPart, sCode, "");
        }

        public XmlNode addBodyPart(string sBodyPart, string sCode, string scodeid)
        {
            BodyPartNode = null;
            putBodyPart(sBodyPart, sCode, scodeid);
            return BodyPartNode;
        }

        public XmlNode putBodyPart(string sBodyPart, string sCode)
        {
            return putBodyPart(sBodyPart, sCode, "");
        }

        public XmlNode putBodyPart(string sBodyPart, string sCode, string scodeid)
        {
            BodyPartNode = Utils.putCodeItem(putBodyPartNode, "", sBodyPart, sCode, NodeOrder, scodeid);
            return BodyPartNode;
        }

        public string BodyPart
        {
            get
            {
                return Utils.getData(BodyPartNode, "");
            }
            set
            {
                Utils.putData(putBodyPartNode, "", value, NodeOrder);
            }
        }

        public string BodyPart_Code
        {
            get
            {
                return Utils.getCode(BodyPartNode, "");
            }
            set
            {
                Utils.putCode(putBodyPartNode, "", value, NodeOrder);
            }
        }

        public string BodyPart_CodeID
        {
            get
            {
                return Utils.getCodeID(BodyPartNode, "");
            }
            set
            {
                Utils.putCodeID(putBodyPartNode, "", value, NodeOrder);
            }
        }
        //End SIW139

        public int DiagnosisCount
        {
            get
            {
                return DiagnosisList.Count;
            }
        }

        public XmlNodeList DiagnosisList
        {
            get
            {
                //Start SIW163
                if (null == xmlDiagnosisList)
                {
                    return getNodeList(Constants.sInjDiagnosis, ref xmlDiagnosisList, Node);
                }
                else
                {
                    return xmlDiagnosisList;
                }
                //End SIW163
            }
        }

        public XmlNode getDiagnosis(bool reset)
        {
            //Start SIW163
            if (null != DiagnosisList)
            {
                return getNode(reset, ref xmlDiagnosisList, ref xmlDiagnosis);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeDiagnosis()
        {
            if (DiagnosisNode != null)
                Node.RemoveChild(DiagnosisNode);
            DiagnosisNode = null;
        }

        private XmlNode putDiagnosisNode
        {
            get
            {
                if (DiagnosisNode == null)
                    DiagnosisNode = XML.XMLaddNode(putNode, Constants.sInjDiagnosis, NodeOrder);
                return DiagnosisNode;
            }
        }

        private XmlNode DiagnosisNode
        {
            get
            {
                return xmlDiagnosis;
            }
            set
            {
                xmlDiagnosis = value;
            }
        }

        //Start SIW139
        public XmlNode addDiagnosis(string sDiagnosis, string sDiagnosisCode, string sDate)
        {
            return addDiagnosis(sDiagnosis, sDiagnosisCode, sDate, "");
        }

        public XmlNode addDiagnosis(string sDiagnosis, string sDiagnosisCode, string sDate, string scodeid)
        {
            DiagnosisNode = null;
            putDiagnosis(sDiagnosis, sDiagnosisCode, scodeid);
            DiagnosisDate = sDate;
            return DiagnosisNode;
        }

        public XmlNode putDiagnosis(string sDesc, string sCode)
        {
            return putDiagnosis(sDesc, sCode, "");
        }

        public XmlNode putDiagnosis(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putDiagnosisNode, Constants.sInjDiagSpecific, sDesc, sCode, sDiagNodeOrder, scodeid);
        }

        public string Diagnosis
        {
            get
            {
                return Utils.getData(DiagnosisNode, Constants.sInjDiagSpecific);
            }
            set
            {
                Utils.putData(putDiagnosisNode, Constants.sInjDiagSpecific, value, sDiagNodeOrder);
            }
        }

        public string Diagnosis_Code
        {
            get
            {
                return Utils.getCode(DiagnosisNode, Constants.sInjDiagSpecific);
            }
            set
            {
                Utils.putCode(putDiagnosisNode, Constants.sInjDiagSpecific, value, sDiagNodeOrder);
            }
        }

        public string Diagnosis_CodeID
        {
            get
            {
                return Utils.getCodeID(DiagnosisNode, Constants.sInjDiagSpecific);
            }
            set
            {
                Utils.putCodeID(putDiagnosisNode, Constants.sInjDiagSpecific, value, sDiagNodeOrder);
            }
        }
        //End SIW139

        public string DiagnosisDate
        {
            get
            {
                return Utils.getDate(DiagnosisNode, Constants.sInjDiagDate);
            }
            set
            {
                Utils.putDate(putDiagnosisNode, Constants.sInjDiagDate, value, sDiagNodeOrder);
            }
        }

        //Start SIW139
        public int TreatmentCount
        {
            get
            {
                return TreatmentList.Count;
            }
        }

        public XmlNodeList TreatmentList
        {
            get
            {
                //Start SIW163
                if (null == xmlTreatmentList)
                {
                    return getNodeList(Constants.sInjTreatment, ref xmlTreatmentList, Node);
                }
                else
                {
                    return xmlTreatmentList;
                }
                //End SIW163
            }
        }

        public XmlNode getTreatment(bool reset)
        {
            //Start SIW163
            if (null != TreatmentList)
            {
                return getNode(reset, ref xmlTreatmentList, ref xmlTreatment);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeTreatment()
        {
            if (TreatmentNode != null)
                Node.RemoveChild(TreatmentNode);
            TreatmentNode = null;
        }

        private XmlNode putTreatmentNode
        {
            get
            {
                if (TreatmentNode == null)
                    TreatmentNode = XML.XMLaddNode(putNode, Constants.sInjTreatment, NodeOrder);
                return TreatmentNode;
            }
        }

        private XmlNode TreatmentNode
        {
            get
            {
                return xmlTreatment;
            }
            set
            {
                xmlTreatment = value;
            }
        }

        public XmlNode addTreatment(string sTreatment, string sTreatmentCode, string sStartDate, string sEndDate,
                                    string sTreatedBy, string sDateInitialVisit, string sPhysicianSpeciality,
                                    string sPhysicianSpecialityCode)
        {
            TreatmentNode = null;
            putTreatment(sTreatment, sTreatmentCode);
            TreatmentStartDate = sStartDate;
            TreatmentEndDate = sEndDate;
            TreatedBy = sTreatedBy;
            DateInitialVisit = sDateInitialVisit;
            putPhysicianSpeciality(sPhysicianSpeciality, sPhysicianSpecialityCode);
            return TreatmentNode;
        }

        public XmlNode putTreatment(string sTreatment, string sTreatmentCode)
        {
            return putTreatment(sTreatment, sTreatmentCode, "");
        }

        public XmlNode putTreatment(string sTreatment, string sTreatmentCode, string scodeid)
        {
            Treatment = sTreatment;
            Treatment_Code = sTreatmentCode;
            Treatment_CodeID = scodeid;
            return TreatmentNode;
        }

        public string Treatment
        {
            get
            {
                return Utils.getData(TreatmentNode, Constants.sInjTreatSpecific);
            }
            set
            {
                Utils.putData(putTreatmentNode, Constants.sInjTreatSpecific, value, sTreatNodeOrder);
            }
        }

        public string Treatment_Code
        {
            get
            {
                return Utils.getCode(TreatmentNode, Constants.sInjTreatSpecific);
            }
            set
            {
                Utils.putCode(putTreatmentNode, Constants.sInjTreatSpecific, value, sTreatNodeOrder);
            }
        }

        public string Treatment_CodeID
        {
            get
            {
                return Utils.getCodeID(TreatmentNode, Constants.sInjTreatSpecific);
            }
            set
            {
                Utils.putCodeID(putTreatmentNode, Constants.sInjTreatSpecific, value, sTreatNodeOrder);
            }
        }

        public string TreatmentStartDate
        {
            get
            {
                return Utils.getDate(TreatmentNode, Constants.sInjTreatStarted);
            }
            set
            {
                Utils.putDate(putTreatmentNode, Constants.sInjTreatStarted, value, sTreatNodeOrder);
            }
        }

        public string TreatmentEndDate
        {
            get
            {
                return Utils.getDate(TreatmentNode, Constants.sInjTreatEnded);
            }
            set
            {
                Utils.putDate(putTreatmentNode, Constants.sInjTreatEnded, value, sTreatNodeOrder);
            }
        }

        public string TreatedBy
        {
            get
            {
                return Utils.getEntityRef(TreatmentNode, Constants.sInjTreatedBy);
            }
            set
            {
                Utils.putEntityRef(putTreatmentNode, Constants.sInjTreatedBy, value, sTreatNodeOrder);
            }
        }

        public string DateInitialVisit
        {
            get
            {
                return Utils.getDate(TreatmentNode, Constants.sInjDateInitialVisit);
            }
            set
            {
                Utils.putDate(putTreatmentNode, Constants.sInjDateInitialVisit, value, sTreatNodeOrder);
            }
        }

        public XmlNode putPhysicianSpeciality(string sDesc, string sCode)
        {
            return putPhysicianSpeciality(sDesc, sCode, "");
        }

        public XmlNode putPhysicianSpeciality(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putTreatmentNode, Constants.sInjPhysicianSpeciality, sDesc, sCode, sTreatNodeOrder, scodeid);
        }

        public string PhysicianSpeciality
        {
            get
            {
                return Utils.getData(TreatmentNode, Constants.sInjPhysicianSpeciality);
            }
            set
            {
                Utils.putData(putTreatmentNode, Constants.sInjPhysicianSpeciality, value, sTreatNodeOrder);
            }
        }

        public string PhysicianSpeciality_Code
        {
            get
            {
                return Utils.getCode(TreatmentNode, Constants.sInjPhysicianSpeciality);
            }
            set
            {
                Utils.putCode(putTreatmentNode, Constants.sInjPhysicianSpeciality, value, sTreatNodeOrder);
            }
        }

        public string PhysicianSpeciality_CodeID
        {
            get
            {
                return Utils.getCodeID(TreatmentNode, Constants.sInjPhysicianSpeciality);
            }
            set
            {
                Utils.putCodeID(putTreatmentNode, Constants.sInjPhysicianSpeciality, value, sTreatNodeOrder);
            }
        }
        //End SIW139

        public int MedicationCount
        {
            get
            {
                return MedicationList.Count;
            }
        }

        public XmlNodeList MedicationList
        {
            get
            {
                //Start SIW163
                if (null == xmlMedicationList)
                {
                    return getNodeList(Constants.sInjMedication, ref xmlMedicationList, Node);
                }
                else
                {
                    return xmlMedicationList;
                }
                //End SIW163
            }
        }

        public XmlNode getMedication(bool reset)
        {
            //Start SIW163
            if (null != MedicationList)
            {
                return getNode(reset, ref xmlMedicationList, ref xmlMedication);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public void removeMedication()
        {
            if (MedicationNode != null)
                Node.RemoveChild(MedicationNode);
            MedicationNode = null;
        }

        private XmlNode putMedicationNode
        {
            get
            {
                if (MedicationNode == null)
                    MedicationNode = XML.XMLaddNode(putNode, Constants.sInjMedication, NodeOrder);
                return MedicationNode;
            }
        }

        private XmlNode MedicationNode
        {
            get
            {
                return xmlMedication;
            }
            set
            {
                xmlMedication = value;
            }
        }

        //Start SIW139
        public XmlNode addMedication(string sMedication, string sMedicationCode, string sDose)
        {
            return addMedication(sMedication, sMedicationCode, sDose, "");
        }

        public XmlNode addMedication(string sMedication, string sMedicationCode, string sDose, string scodeid)
        {
            MedicationNode = null;
            putMedication(sMedication, sMedicationCode, scodeid);
            MedicationDose = sDose;
            return MedicationNode;
        }

        public XmlNode putMedication(string sDesc, string sCode)
        {
            return putMedication(sDesc, sCode, "");
        }

        public XmlNode putMedication(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMedicationNode, "", sDesc, sCode, NodeOrder, scodeid);
        }

        public string Medication
        {
            get
            {
                return Utils.getData(MedicationNode, "");
            }
            set
            {
                Utils.putData(putMedicationNode, "", value, NodeOrder);
            }
        }

        public string Medication_Code
        {
            get
            {
                return Utils.getCode(MedicationNode, "");
            }
            set
            {
                Utils.putCode(putMedicationNode, "", value, NodeOrder);
            }
        }

        public string Medication_CodeID
        {
            get
            {
                return Utils.getCodeID(MedicationNode, "");
            }
            set
            {
                Utils.putCodeID(putMedicationNode, "", value, NodeOrder);
            }
        }
        //End SIW139

        public string MedicationDose
        {
            get
            {
                return Utils.getAttribute(MedicationNode, "", Constants.sInjMedicationDose);
            }
            set
            {
                Utils.putAttribute(putMedicationNode, "", Constants.sInjMedicationDose, value, NodeOrder);
            }
        }
        //Start SIW7309
        public string DescriptionOfPrognosis
        {
            get
            {
                return Utils.getData(Node, Constants.sInjDescriptionOfPrognosis);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjDescriptionOfPrognosis, value, NodeOrder);
            }
        }
        public string DescriptionOfPrognosisHTML
        {
            get
            {
                return Utils.getData(Node, Constants.sInjDescriptionOfPrognosisHTML);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjDescriptionOfPrognosisHTML, value, NodeOrder);
            }
        }
        public string ImpairmentFlag
        {
            get
            {
                return Utils.getBool(Node, Constants.sInjImpairmentFlag,"");
            }
            set
            {
                Utils.putBool(putNode, Constants.sInjImpairmentFlag,"", value, NodeOrder);
            }
        }
        public string WhereInjuredTaken
        {
            get
            {
                return Utils.getData(Node, Constants.sInjWhereInjuredTaken);
            }
            set
            {
                Utils.putData(putNode, Constants.sInjWhereInjuredTaken, value, NodeOrder);
            }
        }
        //End SIW7309
    }
}
