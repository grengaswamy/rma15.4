/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLSchedItem : XMLXCNodeWithList
    {
        /*<parent>
        '   <SCHEDULED_ITEM  ID="SIxxx">
        '     <ITEM_TYPE CODE="JEW|COMP|...">Jewlery<ITEM_TYPE>
        '     <DESCRIPTION>Item Description</DESCRIPTION>
        '     <SERIAL_NUMBER>Serial Number</SERIAL_NUMBER>
        '     <DECLARED_VALUE>$$$$$$</DECLARED_VALUE>
        '     <REPLACEMENT_VALUE>$$$$</REPLACEMENT_VALUE>
        '     <EFFECTIVE_DATE YEAR="2001" MONTH="08" DAY="01" />
        '     <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '     <LIMIT> ...... </LIMIT>
        '        <!-- Multiple Limits -->
        '     <DEDUCTIBLE> ...... </DEDUCTIBLE>
        '        <!-- Multiple Deductibles -->
        '     <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '   </SCHEDULED_ITEM>
        '     <!-- Multiple Items -->
        '</parent>*/

        private XMLCommentReference m_Comment;
        private XMLLimit m_Limit;
        private XMLDeductible m_Deductible;
        
        public XMLSchedItem()
        {
            xmlThisNode = null;
            xmlNodeList = null;
            m_Parent = null;

            Globals.lItmId = 0;

            sThisNodeOrder = new ArrayList(10);
            sThisNodeOrder.Add(Constants.sSItmType);
            sThisNodeOrder.Add(Constants.sSItmDescription);
            sThisNodeOrder.Add(Constants.sSItmSerialNum);
            sThisNodeOrder.Add(Constants.sSItmDeclValue);
            sThisNodeOrder.Add(Constants.sSItmRplcValue);
            sThisNodeOrder.Add(Constants.sSItmEffDate);
            sThisNodeOrder.Add(Constants.sSItmExpDate);
            sThisNodeOrder.Add(Constants.sSItmLimit);
            sThisNodeOrder.Add(Constants.sSItmDeductible);
            sThisNodeOrder.Add(Constants.sSItmComment);
        }

        public override XmlNode Create()
        {
            return Create("", "", "");
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sSItmNode; }
        }

        public XmlNode Create(string sItemID, string sEffectiveDate, string sExpirationDate)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;
                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                ItemID = sItemID;
                EffectiveDate = sEffectiveDate;
                ExpirationDate = sExpirationDate;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLSchedItem.Create");
                return null;
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            if (m_Parent != null)
                ((XMLXCIntSItem)m_Parent).ScheduledItem = this;
            Node = null;
        }

        private void subClearPointers()
        {
            CommentReference = null;
            Limit = null;
            Deductible = null;
        }

        public string ItemID
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sSItmID);
                string lid = "";
                if (strdata != "")
                    if (StringUtils.Left(strdata, 3) == Constants.sSItmIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lItmId = Globals.lItmId + 1;
                    lid = Globals.lItmId + "";
                }
                Utils.putAttribute(putNode, "", Constants.sSItmID, Constants.sSItmIDPfx + (lid + ""), NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putItemType(string sDesc, string sCode)
        {
            return putItemType(sDesc, sCode, "");
        }

        public XmlNode putItemType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sSItmType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string ItemType
        {
            get
            {
                return Utils.getData(Node, Constants.sSItmType);
            }
            set
            {
                Utils.putData(putNode, Constants.sSItmType, value, NodeOrder);
            }
        }

        public string ItemType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sSItmType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sSItmType, value, NodeOrder);
            }
        }

        public string ItemType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sSItmType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sSItmType, value, NodeOrder);
            }
        }
        //End SIW139

        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sSItmEffDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSItmEffDate, value, NodeOrder);
            }
        }

        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sSItmExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSItmExpDate, value, NodeOrder);
            }
        }

        public string Description
        {
            get
            {
                return Utils.getData(Node, Constants.sSItmDescription);
            }
            set
            {
                Utils.putData(putNode, Constants.sSItmDescription, value, NodeOrder);
            }
        }

        public string SerialNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sSItmSerialNum);
            }
            set
            {
                Utils.putData(putNode, Constants.sSItmSerialNum, value, NodeOrder);
            }
        }

        public string DeclaredValue
        {
            get
            {
                return Utils.getData(Node, Constants.sSItmDeclValue);
            }
            set
            {
                Utils.putData(putNode, Constants.sSItmDeclValue, value, NodeOrder);
            }
        }

        public string ReplacementValue
        {
            get
            {
                return Utils.getData(Node, Constants.sSItmRplcValue);
            }
            set
            {
                Utils.putData(putNode, Constants.sSItmRplcValue, value, NodeOrder);
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);    //SIW529 Removed Document parameter
            }
        }

        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLInsUnit();
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
                subClearPointers();
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getScheduledItem(bool reset)
        {
            return getNode(reset);
        }

        public XMLLimit Limit
        {
            get
            {
                if (m_Limit == null)
                {
                    m_Limit = new XMLLimit();
                    m_Limit.Parent = this;
                    //SIW529 LinkXMLObjects(m_Limit);
                    m_Limit.getFirst();     //SI06420
                }
                return m_Limit;
            }
            set
            {
                m_Limit = value;
            }
        }

        public XMLDeductible Deductible
        {
            get
            {
                if (m_Deductible == null)
                {
                    m_Deductible = new XMLDeductible();
                    m_Deductible.Parent = this;
                    //SIW529 LinkXMLObjects(m_Deductible);
                    m_Deductible.getFirst();    //SI06420
                }
                return m_Deductible;
            }
            set
            {
                m_Deductible = value;
            }
        }

        public XmlNode getItembyID(string sID)
        {
            if (ItemID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (ItemID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getItembySerialNumber(string snum)
        {
            if (SerialNumber != snum)
            {
                getFirst();
                while (Node != null)
                {
                    if (SerialNumber == snum)
                        break;
                    getNext();
                }
            }
            return Node;
        }
    }
}
