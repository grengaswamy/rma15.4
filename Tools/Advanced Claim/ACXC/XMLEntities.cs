/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/06/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 05/08/2008 | SI06333 |    SW      | Add Entity DISPLAY_NAME
 * 07/24/2008 | SI06249 |    sw      | Add Effective start date and End date to Address/Contact XML
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 11/07/2008 | SI06468 |    ASM     | Code IDs
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 02/25/2009 | SIW163  |    NB      | Fix for iterating through sub-lists
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/28/2009 | SIW12   |    SW      | Entity PartyInvolved
 * 10/23/2009 | SIW12   |    SW      | Entity Type chnaged to Uppercase to match with VB XC
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 05/18/2010 | SIW316  |    AP      | Need data entry for Entity ID and Entity Category.Values show on Entity Tree.  Need to be able to Add, Edit, Delete from tree.    
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment  
*' SIW489 7/15/2010 hfw - changes for WEB based Combined Pay
 * 08/13/2010 | SIW490  |    ASM     | Tree Label    
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 09/08/2010 | SIW493  |    AS      | Fix in xc object constructor
 * 09/17/2010 | SIW490  |    SW      | Removed method which takes reset as string in place of boolean. 
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *05/13/2011 |SIW7127 |   BS       |   AC needs to be able to set the expiry date on an Entity using the XML import
 *06/01/2011 |SIW7127 |   ASR      |   AC needs to be able to set the Effective Start Date and Effective End Date on an Entity using the XML import
 **  06/15/2011 |SIW7214  |    AV      | Add medicare information in entity node   
 * 06/06/2011 | SIW7182  |    RG     | Making Entity's relationshiop with Entity Account(s)    
*  09/19/2011 | SIN7541 |    ASR     |  Added Disallow Payments Checkbox in Other Information tab of Entity Screen
 * 09/28/2011 | SIN7587 |    UM     |   Added New Nodes and Updated existing one for ID Number 
 * 10/10/2011 | SIW7620 |    AL     |  Added properties for Physician and MediStaff screens.
 * 10/10/2011 | SIW7620 |    SW     |  IC#63 - corrected xmlobject for code values
 * 09/22/2011 | SIW7076  |    AV     | Add contact id node to the Contact node.   
 * 10/26/2011 | SIW7214  |   KCB     | Medicare Eligible
 * 11/15/2011 | SIW7790  |   AV      | Add Contact info tech key node.
 * 11/18/2011 | SIW7076  |    AV     | Change ID node of contact node to attribute from child node.
 * 11/24/2011 | SIW7620  |   SS      | Added properties for Physician and MediStaff screen
 * 11/24/2011 | SIW7790  |   AV      | Add a new relation ship id in contact node in order to update contact type.
 * 11/26/2011 | SIW7620  |   AS      | Fix for physician and medstaff license state
 * 01/24/2012 | SIW8067  |   SAS     | ID Number is not working for Converion ID Number type
 * 01/17/2012 | SIN8050  |   SAS     | Display Name in Entity Node not in correct place
 * 01/06/2012 | SIN8034  |   SS      | Added Sub Speciality field.
 * 04/26/2012 | SIN8440  |   SAS     | Prefix suffix save fix on individual entity
 * 05/14/2012 | SIN8499  |   SW      | Entity reference OrgSec and OrgHier
 * 06/11/2012 | N8584     |   SS      | Add Effective start date and End date to Entity XML
 *************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.Constants;
using CCP.Common;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public class XMLEntity : XMLXCNodeWithList, XMLXCIntPartyInvolved //SIW12
    {
        /*****************************************************************************
        ' The cXMLEntity class provides the functionality to support and manage an
        ' Advanced Claims Entity Node
        '*****************************************************************************
        '   <ENTITY ID="ENTid {id}" TYPE="Business, Individual, Household {string} [ENTITY_TYPE]">
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333)'SIW360
        '   <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '      <ENTITY_NAME>
        '         <DISPLAY_NAME>Entity Display Name<DISPLAY_NAME>   'SIN8050 - moved from Entity to Entity_name
        '         <INDIVIDUAL>
        '            <PREFIX ID="CodeID" CODE="ShortCode">(MR|MISS|MS|MRS|etc.) {string} [PREFIX]</PREFIX>   'SIN8440
        '            <FIRST_NAME>First Name {string}</FIRST_NAME>
        '            <MIDDLE_NAME>Middle Name {string}</MIDDLE_NAME>
        '            <LAST_NAME>Last Name {string}</LAST_NAME>
        '            <SUFFIX ID="CodeID" CODE="ShortCode">(II|III|JR|SR|etc.) {string} [SUFFIX]</SUFFIX>     'SIN8440
        '            <INITIALS>mdm</INITIALS>
        '            <AKA>alsoknownas</AKA>
        '            <USER_ID> ... </USER_ID>  
                   'SIW316
        '         </INDIVIDUAL>
        '         <BUSINESS>
        '            <BUSINESS_NAME>business name {string)</BUSINESS_NAME>      'SI05532 'SI04556
        '            <LEGAL_NAME>Legal Business Name {string}</BUSINESS_NAME>
        '            <ABBREVIATION>LBN {string}</ABBREVIATION>
        '            <DOING_BUS_AS>Doing Business As Name {string}</DOING_BUS_AS>
        '         </BUSINESS>
        '      </ENTITY_NAME>
        '         <EXPIRATION_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</EXPIRATION_DATE>   'SIW7127
        '         <EFFECTIVE_DATE YEAR='yyyy' MONTH='mm' DAY='dd'>displaydate</EFFECTIVE_DATE> 'SIW7127
        '      <TAX_ID TYPE="FEIN|SSN {string} [TIN, SSN]">Tax ID Number {string}</TAX_ID>
        '      <REPORTABLE_TYPE_1099 [ID=codeid] CODE="">description</1099_REPORTABLE_TYPE>             'SI06468
        '      <W9_INFO VALID='True'|'False'>                                                           'SI05532
        '         <DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</DATE>                             'SI05532
        '         <REQUEST_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</REQUEST_DATE>             'SI05532
        '         <FILING_NUMBER>filingnumber</FILING_NUMBER>                                           'SI05532
        '         <LAST_IRS_EXCP_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</LAST_IRS_EXCP_DATE> 'SI05532
        '      </W9_INFO> 
        '     <ENT_EFFECTIVE_START_DATE YEAR="yyyy" MONTH="mm" DAY="dd"/>     'N8584
        '     <ENT_EFFECTIVE_END_DATE YEAR="yyyy" MONTH="mm" DAY="dd"/>       'N8584                                                            'SI05532
        '      <BUWH ENABLED='True'|'False'>                                                            'SI05532
        '         <START_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</START_DATE>                 'SI05532
        '         <END_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</END_DATE>                     'SI05532
        '         <PERCENT>percent</PERCENT>                                                            'SI05532
        '      </BUWH>                                                                                  'SI05532
        '      <DEMOGRAPHICS _OPTIONAL_="">
        '         <GENDER [ID=codeid] CODE="M|F|U {string} [GENDER]">Gender Translated {string}</GENDER>   'SI06468
        '         <DATE_OF_BIRTH YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <DATE_OF_DEATH YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <MARITAL_STATUS [ID=codeid] STATUS="M|S {string} [MARITAL_STATUS]" SPOUSE="ENTid  References a specific entity {id}">Marrital Status Translated {string}</MARITAL_STATUS>  'SI06468
        '         <EDUCATION [ID=codeid] CODE="*">Default</EDUCATION>                    'SI06468
        '         <PRIMARY_LANGUAGE [ID=codeid]  CODE="*">Default</PRIMARY_LANGUAGE>     'SI06468
        '         <DRIVERS_LICENSE>
        '            <STATE [ID=codeid] CODE="sc">STATE</STATE>                          'SI06468
        '            <NUMBER>dkskldk</NUMBER>
        '            <EFFECTIVE_DATE YEAR="" MONTH="" DAY=""/>
        '            <EXPIRATION_DATE YEAR="" MONTH="" DAY=""/>
        '            <QUALIFER CODE="">qualifier</QUALIFIER>
        '                <!--Multiples allowed-->
        '                <!--Restrictions and priveleges-->
        '         </DRIVERS_LICENSE>
        '      </DEMOGRAPHICS>
        '      <BUSINESS_DATA>
        '        <SIC>SICCode {string}</SIC>
        '        <NAIC>naiccode</NAIC>
        '        <NATURE_OF_BUSINESS>Nature of business {string}</NATURE_OF_BUSINESS>
        '        <UNEMP_INS_ACCT>Unemployement Insurance Account Number {string}</UNEMP_INS_ACCT>
        '        <WC_FILING_NUMBER>filingnumber</WC_FILING_NUMBER>                 'SI05532
        '        <TYPE_OF_BUSINESS CODE="CORP">Corporation</TYPE_OF_BUSINESS>
        '        <CONTACT_NAME>person to contact</CONTACT_NAME>                    'SI04670
        '        <BUSINESS_LICENSE>
        '            <STATE CODE="sc">STATE</STATE>
        '            <NUMBER>dkskldk</NUMBER>
        '            <EFFECTIVE_DATE YEAR="" MONTH="" DAY=""/>
        '            <EXPIRATION_DATE YEAR="" MONTH="" DAY=""/>
        '            <QUALIFER CODE="">qualifier</QUALIFIER>
        '                <!--Multiples allowed-->
        '                <!--Restrictions and priveleges-->
        '        </BUSINESS_LICENSE>
        '      </BUSINESS_DATA>
        '      <CONTACT PRIMARY="YES|NO" ID="123">                        'SIW7790
                <TechKey>123</TechKey>                                    'SIW7790
        '         <!--Mulitiple contacts allowed per entity-->
        '         <WHO ENTITY_ID="Entity Contact ID {id}"/>
        '         <TYPE CODE="Type of Contact [ENT_RELATIONSHIPS]">Business Phone, Home Phone, etc. {string}</TYPE>
        '         <INFORMATION>contact data</INFORMATION>
        '         <AFTER HOUR="Hour (2) {integer} [0-23]" MINUTE="Minute (2) {integer} [0-60]" SECOND="Second (2) {integer} [0-60]"/>
        '         <BEFORE HOUR="Hour (2) {integer} [0-23]" MINUTE="Minute (2) {integer} [0-60]" SECOND="Second (2) {integer} [0-60]"/>
        '         <EFFECTIVE_START_DATE YEAR="yyyy" MONTH="mm" DAY="dd"/>     'SI06249
        '         <EFFECTIVE_END_DATE YEAR="yyyy" MONTH="mm" DAY="dd"/>       'SI06249  
        '         <Rel_ID>123</Rel_ID>                                         'SIW7790
        '      </CONTACT>
        '        <!-- multiple entries -->
        '      <ACCOUNT> .... </ACCOUNT>              //SIW7182
        '        <!-- multiple entries -->      
        '      <ADDRESS_REFERENCE> ... </ADDRESS_REFERENCE>
        '        <!-- multiple entries -->
        '      <RELATED_ENTITY ID="related entity id" CODE="relationshipcode">relationship</RELATED_ENTITY>
        '        <!-- multiple entries -->
        '      <CATEGORY CODE="code">category</CATEGORY>
        '        <!-- multiple entries -->
        'SIN7587 Start
        '    //<ID_NUMBER TYPE="type of id number>idnumber</ID_NUMBER>
        'Start SIW8067 - Corrected ID_Number xml schema and xml object properties     
        '   <ID_NUMBER ID='Row_ID or relative ID'>
        '       <TECH_KEY>AC ROW ID to ID Number Table</TECH_KEY> 
        '       <TYPE CODE="idnbrtypeshortcode" CODE_ID='idnbrtypecodeid'>typedescription</TYPE>
        '       <NUMBER>idnumber</NUMBER>
        '       <EXPIRATION_DATE YEAR='yyyy' MONTH='mm' DAY='dd'>displaydate</EXPIRATION_DATE>
        '       <EFFECTIVE_DATE YEAR='yyyy' MONTH='mm' DAY='dd'>displaydate</EFFECTIVE_DATE>
        '   </ID_NUMBER>
        'End SIW8067
        'SIN7587 End
        '        <!-- multiple entries -->
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '      <DOCUMENT_REFERENCE ID="DOCxx"/>             'SI05360
        '        <!-- multiple entries -->                  'SI05360
        '      <TRIGGERS> ... </TRIGGERS>                   'SI05532      
        ' Begin SIW489
        '      <COMBINED_PAY ID="CPAYxxx">
        '         <!--Mulitiple combined pay entries allowed per entity-->
        '           <TECH_KEY>AC Technical key for the element </TECH_KEY>  
        '           <ADDRESS_REFERENCE ID="RELATIONID"
        '                      ADDRESS_ID="ADRid  References an address in the Addresses Collection {id}"
        '                      CODE="Relationship Code">Type of</ADDRESS>
        '                     <TECH_KEY>technical key value for relationship</TECH_KEY>
        '           </ADDRESS_REFERENCE >
        '           <ACCOUNT CODE="banknumber">
        '               <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '               <NAME>AccountName</NAME>
        '               <ROUTING_NUMBER>Routing</ROUTING_NUAMBER>
        '               <ACCOUNT_NUMBER>AccountNumber</ACCOUNT_NUMBER>
        '               <FRACTIONAL_NUMBER>fractionalnumber</FRACTIONAL_NUMBER>
        '           </ACCOUNT>
        '           <NEXT_PRINT_DATE YEAR="year" MONTH="month" DAY="day"/>
        '      </COMBINED_PAY>
        ' End SIW489
        ' Start SIW7620
        '   <!-- Start Entity Physician nodes -->
        '    <PHYSICIAN>
        '        <TECH_KEY>Physician row id</TECH_KEY>
        '        <PHYS_EID>76</PHYS_EID>
        '        <PHYSICIAN_NUMBER>87458745</PHYSICIAN_NUMBER>
        '        <MED_STAFF_NUMBER>56452015</MED_STAFF_NUMBER>
        '        <MEDICARE_NUMBER>98956</MEDICARE_NUMBER>
        '        <PRIMARY_SPECIALITY CODE="Code" ID="123">Primary Specialty Code Desc</PRIMARY_SPECIALITY>
        '    Start SIN8034
        '         <SUB_SPECIALITIES COUNT="2" NEXTID="3">
        '              <SUB_SPECIALITY>
        '                 <SUB_SPECIALITY_CODE CODE="CA">CARDIOLOGIST</SUB_SPECIALITY_CODE> 
        '              </SUB_SPECIALITY>
        '              <SUB_SPECIALITY>
        '                <SUB_SPECIALITY_CODE CODE="FP">FAMILY PRACITIONER</SUB_SPECIALITY_CODE> 
        '              </SUB_SPECIALITY>
        '          </SUB_SPECIALITIES>
        '    End SIN8034
        '        <STAFF_STATUS_CODE CODE="code" ID="123" >Staff Status Code Desc</STAFF_STATUS_CODE>
        '        <STAFF_TYPE_CODE CODE="code" ID="123">Staff Type code Desc</STAFF_TYPE_CODE>
        '        <STAFF_CAT_CODE CODE="code" ID="123" >Staff Cat code Desc</STAFF_CAT_CODE>
        '        <INTERNAL_NUMBER>989875</INTERNAL_NUMBER>
        '        <DEPT_ASSIGNED_EID>0</DEPT_ASSIGNED_EID>
        '        <APPOINT_DATE YEAR="2011" MONTH="10" DAY="06"/>
        '        <LIC_STATE>7</LIC_STATE>
        '        <LIC_NUM>98985456</LIC_NUM>
        '        <LIC_ISSUE_DATE YEAR="2004" MONTH="01" DAY="02"/>
        '        <LIC_DEA_NUM>8787546</LIC_DEA_NUM>
        '        <LIC_DEA_EXP_DATE YEAR="2020" MONTH="10" DAY="28"/>
        '        <MEMBERSHIP>YES</MEMBERSHIP>
        '        <CONT_EDUCATION>NO</CONT_EDUCATION>
        '        <TEACHING_EXP>5 years</TEACHING_EXP>
        '        <INS_COMPANY>United Insurance</INS_COMPANY>
        '        <INS_POLICY>787548</INS_POLICY>
        '      </PHYSICIAN>
        '   <-- multiple physician priveleges-->
        '      <PHY_PRIVILEGE>
        '         <TECH_KEY>Physician privilege row id</TECH_KEY>
        '         <PRIV_ID>18</PRIV_ID>
        '         <PHYS_EID>76</PHYS_EID>
        '         <CATEGORY_CODE CODE="code" ID="123" />
        '         <TYPE_CODE="code" ID="123"  />
        '         <STATUS_CODE CODE="2865" />
        '      </PHY_PRIVILEGE>
        '   < multiple physicain certification>
        '      <PHY_CERTS>
        '         <TECH_KEY>Physician Cert row id</TECH_KEY>
        '         <CERT_ID>14</CERT_ID>
        '         <PHYS_EID>76</PHYS_EID>
        '         <NAME_CODE CODE="code" ID="123"  />
        '         <STATUS_CODE CODE="code" ID="123"  />
        '         <BOARD_CODE CODE="code" ID="123"  />
        '      </PHY_CERTS>
        '   <-- multiple physician education nodes-->
        '      <PHY_EDUCATION>
        '        <TECH_KEY>Physician education row id</TECH_KEY>
        '        <EDUC_ID>16</EDUC_ID>
        '        <PHYS_EID>76</PHYS_EID>
        '        <EDUC_TYPE_CODE CODE="code" ID="123" ></EDUC_TYPE_CODE>
        '        <INSTITUTION_EID Code="306"></INSTITUTION_EID>
        '        <DEGREE_TYPE CODE="code" ID="123" ></DEGREE_TYPE>
        '        <DEGREE_DATE YEAR="1999" MONTH="07" DAY="03"></DEGREE_DATE>
        '      </PHY_EDUCATION>
        '   < --multiple physician previous hospitals-->
        '      <PHY_PREV_HOSP>
        '        <TECH_KEY>Physician previous hospital row id</TECH_KEY>
        '        <PREV_HOSP_ID>12</PREV_HOSP_ID>
        '        <PHYS_EID>76</PHYS_EID>
        '        <STATUS_CODE CODE="code" ID="123" ></STATUS_CODE>
        '        <HOSPITAL_EID>350</HOSPITAL_EID>
        '        <PRIV_CODE CODE="code" ID="123" ></PRIV_CODE>
        '        <INT_DATE YEAR="" MONTH="" DAY=""></INT_DATE>
        '        <END_DATE YEAR="" MONTH="" DAY=""></END_DATE>
        '      </PHY_PREV_HOSP>
        '   <!-- End Entity Physician nodes -->
        '   <!-- Start Entity Medical Staff nodes -->
        '      <MED_STAFF_DATA>
        '        <TECH_KEY>Medical staff row id</TECH_KEY>
        '        <STAFF_EID>76</STAFF_EID>
        '        <MED_STAFF_NUMBER>8787878</MED_STAFF_NUMBER>
        '        <STAFF_STATUS_CODE CODE="code" ID="123" ></STAFF_STATUS_CODE>
        '        <STAFF_POS_CODE CODE="code" ID="123" ></STAFF_POS_CODE>
        '        <STAFF_CAT_CODE CODE="code" ID="123" ></STAFF_CAT_CODE>
        '        <HIRE_DATE YEAR="2010" MONTH="10" DAY="03"></HIRE_DATE>
        '        <LIC_NUM>14545445</LIC_NUM>
        '        <LIC_STATE>7</LIC_STATE>
        '        <LIC_ISSUE_DATE YEAR="2009" MONTH="10" DAY="08"></LIC_ISSUE_DATE>
        '        <LIC_EXPIRY_DATE YEAR="2022" MONTH="10" DAY="21"></LIC_EXPIRY_DATE>
        '        <LIC_DEA_NUM>13543454</LIC_DEA_NUM>
        '        <LIC_DEA_EXP_DATE YEAR="2019" MONTH="10" DAY="10"></LIC_DEA_EXP_DATE>
        '        <DEPT_ASSIGNED_EID ID="0"></DEPT_ASSIGNED_EID>
        '      </MED_STAFF_DATA>
        '   < --multiple medical staff certification -->
        '      <MED_STAFF_CERTS>
        '         <TECH_KEY>Medical staff certification row id</TECH_KEY>
        '         <CERT_ID>14</CERT_ID>
        '         <PHYS_EID>76</PHYS_EID>
        '         <NAME_CODE CODE="code" ID="123"  />
        '         <STATUS_CODE CODE="code" ID="123"  />
        '         <BOARD_CODE CODE="code" ID="123"  />
        '      </MED_STAFF_CERTS>
        '   < --multiple physician priveleges-->
        '      <ED_STAFF_PRIVILEGE>
        '         <TECH_KEY>Medical staff privilege row id</TECH_KEY>
        '         <PRIV_ID>18</PRIV_ID>
        '         <PHYS_EID>76</PHYS_EID>
        '         <CATEGORY_CODE CODE="code" ID="123"  />
        '         <TYPE_CODE CODE="code" ID="123"  />
        '         <STATUS_CODE CODE="code" ID="123"  />
        '      </MED_STAFF_PRIVILEGE>
        '   <!-- End Entity Medical Staff nodes -->
        ' End SIW7620
        '   </ENTITY>
        '*****************************************************************************/
        private XmlNode m_xmlSubSpec; //SIN8034
        private ArrayList sSubSpecNodeOrder; //SIN8034
        private XmlNode m_xmlSubSpecs; //SIN8034
        private XmlNodeList m_xmlSubSpecList;//SIN8034
        private XmlNode m_xmlDLData;
        private XmlNode m_xmlBLData;
        private XMLEntities m_Entities;
        private XMLPartyInvolved m_PartyInvolved;
        private XMLAddressReference m_AddressReference;
        private XMLAccount m_Account;     //SIW7182
        private XMLCommentReference m_Comment;
        private XMLDocumentReference m_Document;
        private XMLTriggers m_Triggers;
        private XMLCombPay m_CombPay; // SIW489
        //private XMLPhyPrivilege m_PhyPrivilege; 
        private Dictionary<object,object> dctEntType;
        private Dictionary<object,object> dctTaxIdType;
        private ArrayList sNameNodeOrder; //SIN8050
        private ArrayList sIndNameNodeOrder;
        private ArrayList sBusNameNodeOrder;
        private ArrayList sDemoNodeOrder;
        private ArrayList sW9NodeOrder;
        private ArrayList sBUWHNodeOrder;
        private ArrayList sDrvLicNodeOrder;
        private ArrayList sBusLicNodeOrder;
        private ArrayList sBusDataNodeOrder;
        private ArrayList sContactNodeOrder;
        private ArrayList sIDNbrNodeOrder;                  //SIW8067
        private XmlNodeList xmlContactList;
        private XmlNode xmlContact;
        private XmlNodeList xmlCategoryList;
        private XmlNode xmlCategory;
        private XmlNodeList xmlIDNList;
        private XmlNode xmlIDN;
        private XmlNodeList xmlDLQList;
        private XmlNode xmlDLQ;
        private XmlNodeList xmlBLQList;
        private XmlNode xmlBLQ;
        private XMLVarDataItem m_DataItem;  //SI06420

        // Start SIW7620
        private XmlNode xmlPhysician; 
        private ArrayList sPhysicianNodeOrder; 
        private XmlNodeList xmlPhysicianList; 
        private XmlNode xmlMedStaff; 
        private ArrayList sMedStaffNodeOrder; 
        private XmlNodeList xmlMedStaffList;
        private XmlNode xmlPHYPrivilage;
        private ArrayList sPHYPrivilageNodeOrder;
        private XmlNodeList xmlPHYPrivilageList;
        private XmlNode xmlPHYCertification;
        private ArrayList sPHYCertificationNodeOrder;
        private XmlNodeList xmlPHYCertificationList;
        private XmlNode xmlPHYEducation;
        private ArrayList sPHYEducationNodeOrder;
        private XmlNodeList xmlPHYEducationList;
        private XmlNode xmlPHYHospital;
        private ArrayList sPHYHospitalNodeOrder;
        private XmlNodeList xmlPHYHospitalList;
        private XmlNode xmlMEDPrivilage;
        private ArrayList sMEDPrivilageNodeOrder;
        private XmlNodeList xmlMEDPrivilageList;
        private XmlNode xmlMEDCertification;
        private ArrayList sMEDCertificationNodeOrder;
        private XmlNodeList xmlMEDCertificationList;
        // End SIW7620
        
        public XMLEntity()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            xmlThisNode = null;

            dctEntType = new Dictionary<object, object>();
            dctEntType.Add(Constants.iEntTypeBus, Constants.sEntTypeBus);
            dctEntType.Add(Constants.iEntTypeInd, Constants.sEntTypeInd);
            dctEntType.Add(Constants.iEntTypeHou, Constants.sEntTypeHou);
            dctEntType.Add(Constants.iUndefined, Constants.sUndefined);
            dctEntType.Add(Constants.iEntTypeClient, Constants.sEntTypeClient);
            dctEntType.Add(Constants.iEntTypeCompany, Constants.sEntTypeCompany);
            dctEntType.Add(Constants.iEntTypeOperation, Constants.sEntTypeOperation);
            dctEntType.Add(Constants.iEntTypeRegion, Constants.sEntTypeRegion);
            dctEntType.Add(Constants.iEntTypeDivision, Constants.sEntTypeDivision);
            dctEntType.Add(Constants.iEntTypeLocation, Constants.sEntTypeLocation);
            dctEntType.Add(Constants.iEntTypeFacility, Constants.sEntTypeFacility);
            dctEntType.Add(Constants.iEntTypeDepartment, Constants.sEntTypeDepartment);
            //Start SIN8499
            dctEntType.Add(Constants.iEntTypeClientSec, Constants.sEntTypeClientSec);
            dctEntType.Add(Constants.iEntTypeCompanySec, Constants.sEntTypeCompanySec);
            dctEntType.Add(Constants.iEntTypeOperationSec, Constants.sEntTypeOperationSec);
            dctEntType.Add(Constants.iEntTypeRegionSec, Constants.sEntTypeRegionSec);
            dctEntType.Add(Constants.iEntTypeDivisionSec, Constants.sEntTypeDivisionSec);
            dctEntType.Add(Constants.iEntTypeLocationSec, Constants.sEntTypeLocationSec);
            dctEntType.Add(Constants.iEntTypeFacilitySec, Constants.sEntTypeFacilitySec);
            dctEntType.Add(Constants.iEntTypeDepartmentSec, Constants.sEntTypeDepartmentSec);
            //End SIN8499
            dctEntType.Add(Constants.sEntTypeBus, Constants.iEntTypeBus);
            dctEntType.Add(Constants.sEntTypeInd, Constants.iEntTypeInd);
            dctEntType.Add(Constants.sEntTypeHou, Constants.iEntTypeHou);
            dctEntType.Add(Constants.sUndefined, Constants.iUndefined);
            dctEntType.Add(Constants.sEntTypeClient, Constants.iEntTypeClient);
            dctEntType.Add(Constants.sEntTypeCompany, Constants.iEntTypeCompany);
            dctEntType.Add(Constants.sEntTypeOperation, Constants.iEntTypeOperation);
            dctEntType.Add(Constants.sEntTypeRegion, Constants.iEntTypeRegion);
            dctEntType.Add(Constants.sEntTypeDivision, Constants.iEntTypeDivision);
            dctEntType.Add(Constants.sEntTypeLocation, Constants.iEntTypeLocation);
            dctEntType.Add(Constants.sEntTypeFacility, Constants.iEntTypeFacility);
            dctEntType.Add(Constants.sEntTypeDepartment, Constants.iEntTypeDepartment);
            //Start SIN8499
            dctEntType.Add(Constants.sEntTypeClientSec, Constants.iEntTypeClientSec);
            dctEntType.Add(Constants.sEntTypeCompanySec, Constants.iEntTypeCompanySec);
            dctEntType.Add(Constants.sEntTypeOperationSec, Constants.iEntTypeOperationSec);
            dctEntType.Add(Constants.sEntTypeRegionSec, Constants.iEntTypeRegionSec);
            dctEntType.Add(Constants.sEntTypeDivisionSec, Constants.iEntTypeDivisionSec);
            dctEntType.Add(Constants.sEntTypeLocationSec, Constants.iEntTypeLocationSec);
            dctEntType.Add(Constants.sEntTypeFacilitySec, Constants.iEntTypeFacilitySec);
            dctEntType.Add(Constants.sEntTypeDepartmentSec, Constants.iEntTypeDepartmentSec);
            //End SIN8499

            dctTaxIdType = new Dictionary<object, object>();
            dctTaxIdType.Add(Constants.iUndefined, Constants.sUndefined);
            dctTaxIdType.Add(Constants.iEntTaxIdTypeNA, Constants.sEntTaxIdTypeNA);
            dctTaxIdType.Add(Constants.iEntTaxIdTypeSSN, Constants.sEntTaxIdTypeSSN);
            dctTaxIdType.Add(Constants.iEntTaxIdTypeFEIN, Constants.sEntTaxIdTypeFEIN);
            dctTaxIdType.Add(Constants.sUndefined, Constants.iUndefined);
            dctTaxIdType.Add(Constants.sEntTaxIdTypeNA, Constants.iEntTaxIdTypeNA);
            dctTaxIdType.Add(Constants.sEntTaxIdTypeSSN, Constants.iEntTaxIdTypeSSN);
            dctTaxIdType.Add(Constants.sEntTaxIdTypeFEIN, Constants.iEntTaxIdTypeFEIN);

            // Start SIN8050
            sNameNodeOrder = new ArrayList(3);
            sNameNodeOrder.Add(Constants.sEntDisplayName);
            sNameNodeOrder.Add(Constants.sEntIndName);
            sNameNodeOrder.Add(Constants.sEntBusName);
            // End SIN8050

            sIndNameNodeOrder = new ArrayList(8); //SIW316
            sIndNameNodeOrder.Add(Constants.sEntPrefix);
            sIndNameNodeOrder.Add(Constants.sEntFName);
            sIndNameNodeOrder.Add(Constants.sEntMName);
            sIndNameNodeOrder.Add(Constants.sEntLName);
            sIndNameNodeOrder.Add(Constants.sEntSuffix);
            sIndNameNodeOrder.Add(Constants.sEntInitials);
            sIndNameNodeOrder.Add(Constants.sEntAKA);
            sIndNameNodeOrder.Add(Constants.sEntUuserId);//SIW316

            sBusNameNodeOrder = new ArrayList(4);
            sBusNameNodeOrder.Add(Constants.sEntBusinessName);
            sBusNameNodeOrder.Add(Constants.sEntLglName);
            sBusNameNodeOrder.Add(Constants.sEntAbbrev);
            sBusNameNodeOrder.Add(Constants.sEntDBAName);

            //sThisNodeOrder = new ArrayList(15);               //(SI06023,SI06345 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(17);                 //(SI06023,SI06345 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(18);                 // SIW489  //SIW490
            //SIW7127 sThisNodeOrder = new ArrayList(19);                  //SIW490
            //sThisNodeOrder = new ArrayList(25);          //SIW7214 //N8584
            sThisNodeOrder = new ArrayList(27);          //N8584
            sThisNodeOrder.Add(Constants.sStdTechKey);          //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);        //SIW490
            sThisNodeOrder.Add(Constants.sEntName);
            sThisNodeOrder.Add(Constants.sEntTaxID);
            sThisNodeOrder.Add(Constants.sEntExpDate);    //SIW7127
            sThisNodeOrder.Add(Constants.sEntEfcDate);    //SIW7127
            sThisNodeOrder.Add(Constants.sEnt1099RptType);
            sThisNodeOrder.Add(Constants.sEntW9Info);
            sThisNodeOrder.Add(Constants.sEntEffStartDt);   //N8584
            sThisNodeOrder.Add(Constants.sEntEffEndDt);     //N8584
            sThisNodeOrder.Add(Constants.sEntBUWH);
            sThisNodeOrder.Add(Constants.sEntAddress);
            sThisNodeOrder.Add(Constants.sEntAccount);       //SIW7182
            sThisNodeOrder.Add(Constants.sEntDemographics);
            sThisNodeOrder.Add(Constants.sEntBusData);
            sThisNodeOrder.Add(Constants.sEntContact);
            sThisNodeOrder.Add(Constants.sEntRelatedEnt);
            sThisNodeOrder.Add(Constants.sEntCategory);
            sThisNodeOrder.Add(Constants.sEntIDNumber);
            sThisNodeOrder.Add(Constants.sEntComment);
            sThisNodeOrder.Add(Constants.sEntDocument);
            sThisNodeOrder.Add(Constants.sEntTriggers);
            //sThisNodeOrder.Add(Constants.sEntTechKey);          //(SI06023 - Implemented in SI06333)//SIW360
            //sThisNodeOrder.Add(Constants.sEntDisplayName);      //SI06345 - Implemented in SI06333) //SIN8050
            sThisNodeOrder.Add(Constants.sEntCombPay);      // SIW489
            //Start SIW7214
            sThisNodeOrder.Add(Constants.sCMSHICN);
            sThisNodeOrder.Add(Constants.sCMSEligible); 
            sThisNodeOrder.Add(Constants.sCMSEffectiveDt);
            //End SIW7214

            sW9NodeOrder = new ArrayList(3);
            sW9NodeOrder.Add(Constants.sEntW9Date);
            sW9NodeOrder.Add(Constants.sEntW9ReqDate);
            sW9NodeOrder.Add(Constants.sEntW9LastIRSExcepDate);

            sBUWHNodeOrder = new ArrayList(3);
            sBUWHNodeOrder.Add(Constants.sEntBUWHStartDate);
            sBUWHNodeOrder.Add(Constants.sEntBUWHEndDate);
            sBUWHNodeOrder.Add(Constants.sEntBUWHPercent);

            sDemoNodeOrder = new ArrayList(7);
            sDemoNodeOrder.Add(Constants.sEntGender);
            sDemoNodeOrder.Add(Constants.sEntDOB);
            sDemoNodeOrder.Add(Constants.sEntDOD);
            sDemoNodeOrder.Add(Constants.sEntMarStatus);
            sDemoNodeOrder.Add(Constants.sEntDriverLic);
            sDemoNodeOrder.Add(Constants.sEntEducation);
            sDemoNodeOrder.Add(Constants.sEntPrimaryLanguage);

            sBusDataNodeOrder = new ArrayList(8);
            sBusDataNodeOrder.Add(Constants.sEntSIC);
            sBusDataNodeOrder.Add(Constants.sEntNAIC);
            sBusDataNodeOrder.Add(Constants.sEntNatofBus);
            sBusDataNodeOrder.Add(Constants.sEntUnempInsAcct);
            sBusDataNodeOrder.Add(Constants.sEntWCFilingNum);
            sBusDataNodeOrder.Add(Constants.sEntBusType);
            sBusDataNodeOrder.Add(Constants.sEntBusContactName);
            sBusDataNodeOrder.Add(Constants.sEntBusLicense);

            //sContactNodeOrder = new ArrayList(5);               //SI06249
            //SIW7076  sContactNodeOrder = new ArrayList(7);
            //SIW7790  sContactNodeOrder = new ArrayList(8); //SIW7076
            //SIW7790   sContactNodeOrder = new ArrayList(9); //SIW7790
            sContactNodeOrder = new ArrayList(10);  //SIW7790
            sContactNodeOrder.Add(Constants.sEntContWho);
            sContactNodeOrder.Add(Constants.sEntContType);
            sContactNodeOrder.Add(Constants.sEntContInfo);
            sContactNodeOrder.Add(Constants.sEntContAfter);
            sContactNodeOrder.Add(Constants.sEntContBefore);
            sContactNodeOrder.Add(Constants.sEntContEffStartDt);   //SI06249
            sContactNodeOrder.Add(Constants.sEntContEffEndDt);     //SI06249  
            sContactNodeOrder.Add(Constants.sEntContactID); //SIW7076
            sContactNodeOrder.Add(Constants.sStdTechKey);  //SIW7790
            sContactNodeOrder.Add(Constants.sContactRelId); //SIW7790

            // Start SIW7620
            sPhysicianNodeOrder = new ArrayList(23);                //SIN8034
            sPhysicianNodeOrder.Add(Constants.sStdTechKey);   
            sPhysicianNodeOrder.Add(Constants.sEntPHYSEID);
            sPhysicianNodeOrder.Add(Constants.sEntPHYSICIANNUMBER);
            sPhysicianNodeOrder.Add(Constants.sEntMEDSTAFFNUMBER);
            sPhysicianNodeOrder.Add(Constants.sEntMEDICARENUMBER);
            sPhysicianNodeOrder.Add(Constants.sEntPRIMARYSPECIALTY);
            sPhysicianNodeOrder.Add(Constants.sEntSubSpecialities); //SIN8034
            sPhysicianNodeOrder.Add(Constants.sEntSTAFFSTATUSCODE);   
            sPhysicianNodeOrder.Add(Constants.sEntSTAFFTYPECODE);
            sPhysicianNodeOrder.Add(Constants.sEntSTAFFCATCODE);
            sPhysicianNodeOrder.Add(Constants.sEntINTERNALNUMBER);
            sPhysicianNodeOrder.Add(Constants.sEntDEPTASSIGNEDEID);
            sPhysicianNodeOrder.Add(Constants.sEntAPPOINTDATE);
            sPhysicianNodeOrder.Add(Constants.sEntLICSTATE);
            sPhysicianNodeOrder.Add(Constants.sEntLICNUM);
            sPhysicianNodeOrder.Add(Constants.sEntLICISSUEDATE);
            sPhysicianNodeOrder.Add(Constants.sEntLICDEANUM);
            sPhysicianNodeOrder.Add(Constants.sEntLICDEAEXPDATE);
            sPhysicianNodeOrder.Add(Constants.sEntMEMBERSHIP);
            sPhysicianNodeOrder.Add(Constants.sEntCONTEDUCATION);
            sPhysicianNodeOrder.Add(Constants.sEntTEACHINGEXP);
            sPhysicianNodeOrder.Add(Constants.sEntINSCOMPANY);
            sPhysicianNodeOrder.Add(Constants.sEntINSPOLICY);
            //SIN8034 start

            sSubSpecNodeOrder = new ArrayList(3);
            sSubSpecNodeOrder.Add(Constants.sEntSubSpecPEID);
            sSubSpecNodeOrder.Add(Constants.sEntSubSpecCode);
            sSubSpecNodeOrder.Add(Constants.sEntSubSpecEntry);

            //SIN8034 ends


            sMedStaffNodeOrder = new ArrayList(14);
            sMedStaffNodeOrder.Add(Constants.sStdTechKey);    
            sMedStaffNodeOrder.Add(Constants.sEntSTAFFEID);
            sMedStaffNodeOrder.Add(Constants.sEntMEDISTAFFNUMBER);
            sMedStaffNodeOrder.Add(Constants.sEntMEDSTAFFSTATUSCODE);
            sMedStaffNodeOrder.Add(Constants.sEntMEDSTAFFPOSCODE);
            sMedStaffNodeOrder.Add(Constants.sEntMEDSTAFFCATCODE);
            sMedStaffNodeOrder.Add(Constants.sEntMedDEPTASSINEDEID);
            sMedStaffNodeOrder.Add(Constants.sEntHIREDATE);
            sMedStaffNodeOrder.Add(Constants.sEntMEDLICNUM);
            sMedStaffNodeOrder.Add(Constants.sEntMEDLICSTATE);
            sMedStaffNodeOrder.Add(Constants.sEntMEDLICISSUEDATE);
            sMedStaffNodeOrder.Add(Constants.sEntMEDLICEXPIRYDATE);
            sMedStaffNodeOrder.Add(Constants.sEntMEDLICDEANUM);
            sMedStaffNodeOrder.Add(Constants.sEntMEDLICEXPIRYDATE);

            sPHYPrivilageNodeOrder  = new ArrayList(9); //SIW7620
            sPHYPrivilageNodeOrder.Add(Constants.sStdTechKey);    
            sPHYPrivilageNodeOrder.Add(Constants.sEntPHYPRIVID);
            sPHYPrivilageNodeOrder.Add(Constants.sEntPRIVPHYSEID);
            sPHYPrivilageNodeOrder.Add(Constants.sEntPHYCATEGORY_CODE);
            sPHYPrivilageNodeOrder.Add(Constants.sEntPHYTYPE_CODE);
            sPHYPrivilageNodeOrder.Add(Constants.sEntPHYSTATUS_CODE);
            sPHYPrivilageNodeOrder.Add(Constants.sEntPHYINT_DATE);
            sPHYPrivilageNodeOrder.Add(Constants.sEntPHYEND_DATE);
            sPHYPrivilageNodeOrder.Add(Constants.sAHDeleteFlag); // SIW7620

            sPHYCertificationNodeOrder = new ArrayList(9); // SIW7620
            sPHYCertificationNodeOrder.Add(Constants.sStdTechKey); 
            sPHYCertificationNodeOrder.Add(Constants.sEntPHYCERTID);
            sPHYCertificationNodeOrder.Add(Constants.sEntPRIVPHYSEID);
            sPHYCertificationNodeOrder.Add(Constants.sEntPHYNAMECODE);
            sPHYCertificationNodeOrder.Add(Constants.sEntPHYSTATUS_CODE);
            sPHYCertificationNodeOrder.Add(Constants.sEntPHYBOARDCODE);
            sPHYCertificationNodeOrder.Add(Constants.sEntPHYINT_DATE);
            sPHYCertificationNodeOrder.Add(Constants.sEntPHYEND_DATE);
            sPHYCertificationNodeOrder.Add(Constants.sAHDeleteFlag); // SIW7620

            sPHYEducationNodeOrder = new ArrayList(8); //SIW7620
            sPHYEducationNodeOrder.Add(Constants.sStdTechKey);
            sPHYEducationNodeOrder.Add(Constants.sEntPHYEDUCID);
            sPHYEducationNodeOrder.Add(Constants.sEntPRIVPHYSEID);
            sPHYEducationNodeOrder.Add(Constants.sEntPHYEDUCTYPECODE);
            sPHYEducationNodeOrder.Add(Constants.sEntPHYINSTITUTIONEID);
            sPHYEducationNodeOrder.Add(Constants.sEntPHYDEGREETYPE);
            sPHYEducationNodeOrder.Add(Constants.sEntPHYDEGREEDATE);
            sPHYEducationNodeOrder.Add(Constants.sAHDeleteFlag); // SIW7620

            sPHYHospitalNodeOrder = new ArrayList(9); // SIW7620
            sPHYHospitalNodeOrder.Add(Constants.sStdTechKey);
            sPHYHospitalNodeOrder.Add(Constants.sPrevHospID);
            sPHYHospitalNodeOrder.Add(Constants.sPhyEntityID);
            sPHYHospitalNodeOrder.Add(Constants.sHospStatusCode);
            sPHYHospitalNodeOrder.Add(Constants.sHospEntID);
            sPHYHospitalNodeOrder.Add(Constants.sHospPrivCode);
            sPHYHospitalNodeOrder.Add(Constants.sHospIntDate);
            sPHYHospitalNodeOrder.Add(Constants.sHospEndDate);
            sPHYHospitalNodeOrder.Add(Constants.sAHDeleteFlag); //SIW7620

            sMEDPrivilageNodeOrder = new ArrayList(9); // SIW7620
            sMEDPrivilageNodeOrder.Add(Constants.sStdTechKey);
            sMEDPrivilageNodeOrder.Add(Constants.sEntMEDPRIVID);
            sMEDPrivilageNodeOrder.Add(Constants.sEntMEDID);
            sMEDPrivilageNodeOrder.Add(Constants.sEntMEDCATEGORYCODE);
            sMEDPrivilageNodeOrder.Add(Constants.sEntMEDTYPECODE);
            sMEDPrivilageNodeOrder.Add(Constants.sEntMEDSTATUSCODE);
            sMEDPrivilageNodeOrder.Add(Constants.sEntMEDINTDATE);
            sMEDPrivilageNodeOrder.Add(Constants.sEntMEDENDDATE);
            sMEDPrivilageNodeOrder.Add(Constants.sAHDeleteFlag); //SIW7620

            sMEDCertificationNodeOrder = new ArrayList(9); // SIW7620
            sMEDCertificationNodeOrder.Add(Constants.sStdTechKey);
            sMEDCertificationNodeOrder.Add(Constants.sEntMEDCERTID);
            sMEDCertificationNodeOrder.Add(Constants.sEntMEDID);
            sMEDCertificationNodeOrder.Add(Constants.sEntMEDNAMECODE);
            sMEDCertificationNodeOrder.Add(Constants.sEntMEDSTATUSCODE);
            sMEDCertificationNodeOrder.Add(Constants.sEntMEDBOARDCODE);
            sMEDCertificationNodeOrder.Add(Constants.sEntMEDINTDATE);
            sMEDCertificationNodeOrder.Add(Constants.sEntMEDENDDATE);
            sMEDCertificationNodeOrder.Add(Constants.sAHDeleteFlag); //SIW7620

            // End SIW7620

            sDrvLicNodeOrder = new ArrayList(5);
            sDrvLicNodeOrder.Add(Constants.sEntDLState);
            sDrvLicNodeOrder.Add(Constants.sEntDLNumber);
            sDrvLicNodeOrder.Add(Constants.sEntDLEffDt);
            sDrvLicNodeOrder.Add(Constants.sEntDLExpDt);
            sDrvLicNodeOrder.Add(Constants.sEntDLQualifier);

            sBusLicNodeOrder = new ArrayList(5);
            sBusLicNodeOrder.Add(Constants.sEntBusLicState);
            sBusLicNodeOrder.Add(Constants.sEntBusLicNumber);
            sBusLicNodeOrder.Add(Constants.sEntBusLicEffDt);
            sBusLicNodeOrder.Add(Constants.sEntBusLicExpDt);
            sBusLicNodeOrder.Add(Constants.sEntBusLicQualifier);

//Start SIW8067
            sIDNbrNodeOrder = new ArrayList(5);
            sIDNbrNodeOrder.Add(Constants.sStdTechKey);
            sIDNbrNodeOrder.Add(Constants.sEntIDNType);
            sIDNbrNodeOrder.Add(Constants.sEntIDNumberNbr);
            sIDNbrNodeOrder.Add(Constants.sEntIDNumberEffStartDate);
            sIDNbrNodeOrder.Add(Constants.sEntIDNumberEffEndDate);
//End SIW8067
        }
        protected override string DefaultNodeName
        {
            get { return Constants.sEntNode; }
        }

        public override XmlNode Create()
        {
            return Create("", CCP.Constants.entType.entTypeInd, "",entTaxIdType.entTaxIdSSN,
                          "","","","","");
        }

        public XmlNode Create(string sID, entType cType, string staxid, entTaxIdType ctaxidtype,
                              string sname1, string sname2, string sname3, string sPrefix, string sSuffix)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLElementWithEntity_Node(NodeName, "0");
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                EntityID = sID;
                EntityType = ((int)cType).ToString();

                if (staxid != "" && staxid != null)
                {
                    TaxId = staxid;
                    TaxIdType = ((int)ctaxidtype).ToString();
                }

                if (IndividualEntity)
                    putIndividualName(sPrefix, sname1, sname2, sname3, sSuffix,"","");
                else
                    putBusinessName(sname1, sname2, sname3);

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLEntity.Create");
                return null;
            }
        }

        public XmlNode Category_Node
        {
            get
            {
                return Utils.getNode(xmlThisNode, Constants.sEntCategory);
            }
        }

        public XmlNode Name_Node
        {
            get
            {
                return Utils.getNode(Node, Constants.sEntName);
            }
        }

        public XmlNode putName_Node
        {
            get
            {
                XmlNode xmlNode;
                xmlNode = Name_Node;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putNode, Constants.sEntName, sThisNodeOrder);
                return xmlNode;
            }
        }

        public XmlNode Demographics_Node
        {
            get
            {
                return Utils.getNode(Node, Constants.sEntDemographics);
            }
        }

        public XmlNode putDemographics_Node
        {
            get
            {
                XmlNode xmlNode;
                xmlNode = Demographics_Node;
                if(xmlNode == null)
                    if (IndividualEntity)
                    {
                        removeBusinessData();
                        xmlNode = XML.XMLaddNode(putNode, Constants.sEntDemographics, sThisNodeOrder);
                    }
                return xmlNode;
            }
        }

        public XmlNode DriverLicense_Node
        {
            get
            {
                if (m_xmlDLData == null)
                    m_xmlDLData = Utils.getNode(Demographics_Node, Constants.sEntDriverLic);
                return m_xmlDLData;
            }
        }

        public XmlNode putDriverLicense_Node
        {
            get
            {
                object n = DriverLicense_Node;
                if(m_xmlDLData == null)
                    if (IndividualEntity)
                    {
                        removeBusinessData();
                        m_xmlDLData = XML.XMLaddNode(putDemographics_Node, Constants.sEntDriverLic, sDemoNodeOrder);
                    }
                return m_xmlDLData;
            }
        }

        public XmlNode BusinessData_Node
        {
            get
            {
                return Utils.getNode(Node, Constants.sEntBusData);
            }
        }

        public XmlNode putBusinessData_Node
        {
            get
            {
                XmlNode xmlNode = BusinessData_Node;
                if (xmlNode == null)
                {
                    if (!IndividualEntity)
                    {
                        removeDemographics();
                        xmlNode = XML.XMLaddNode(putNode, Constants.sEntBusData, sThisNodeOrder);
                    }
                }
                return xmlNode;
            }
        }

        public XmlNode BusinessLicense_Node
        {
            get
            {
                if (m_xmlBLData == null)
                    m_xmlBLData = Utils.getNode(BusinessData_Node, Constants.sEntBusLicense);
                return m_xmlBLData;
            }
        }

        public XmlNode putBusinessLicense_Node
        {
            get
            {
                object n = BusinessLicense_Node;
                if (m_xmlBLData == null)
                    if (BusinessEntity)
                    {
                        removeDemographics();
                        m_xmlBLData = XML.XMLaddNode(putBusinessData_Node, Constants.sEntBusLicense, sBusDataNodeOrder);
                    }
                return m_xmlBLData;
            }
        }

        public XmlNode IndName_Node
        {
            get
            {
                return Utils.getNode(Name_Node, Constants.sEntIndName);
            }
        }

        public XmlNode putIndName_Node
        {
            get
            {
                XmlNode xmlNode = IndName_Node;
                if(xmlNode == null)
                    if (IndividualEntity)
                    {
                        removeBusinessName();
                        xmlNode = XML.XMLNewElement(Constants.sEntIndName, true, Document, putName_Node);
                    }
                return xmlNode;
            }
        }

        public XmlNode BusName_Node
        {
            get
            {
                return Utils.getNode(Name_Node, Constants.sEntBusName);
            }
        }

        public XmlNode putBusName_Node
        {
            get
            {
                XmlNode xmlNode = BusName_Node;
                if(xmlNode == null)
                    if (BusinessEntity || HouseholdEntity)
                    {
                        removeIndividualName();
                        xmlNode = XML.XMLNewElement(Constants.sEntBusName, true, Document, putName_Node);
                    }
                return xmlNode;
            }
        }
        //Start SI06468
        public XmlNode put1099Type(string sType, string stypecode) 
        {
            return Utils.putCodeItem(putNode, Constants.sEnt1099RptType, sType, stypecode, sThisNodeOrder); 
        }
        
        public XmlNode put1099Type(string sType, string stypecode, string stypeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEnt1099RptType, sType, stypecode, sThisNodeOrder, stypeid); 
        }
        //End SI06468

        public string Ten99RptType
        {
            get
            {
                return Utils.getData(Node, Constants.sEnt1099RptType);
            }
            set
            {
                Utils.putData(putNode, Constants.sEnt1099RptType, value, sThisNodeOrder);
            }
        }

        public string Ten99RptType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEnt1099RptType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sEnt1099RptType, value, sThisNodeOrder);
            }
        }
        //Start SI06468
        public string Ten99RptType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEnt1099RptType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEnt1099RptType, value, sThisNodeOrder);
            }
        }
        //End SI06468
        //Start SIW7127
        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntExpDate, value, sThisNodeOrder);
            }
        }
        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntEfcDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntEfcDate, value, sThisNodeOrder);
            }
        }
        //End SIW7127
        public XmlNode putTaxID(string staxid, string sType)
        {
            return Utils.putTypeItem(putNode, Constants.sEntTaxID, staxid, sType, sThisNodeOrder);
        }

        public string TaxId
        {
            get
            {
                return Utils.getData(Node, Constants.sEntTaxID);
            }
            set
            {
                if (XML.AutoEncrypt)
                    if (StringUtils.Left(value, 1) != "~")
                        value = "~" + value;
                Utils.putData(putNode, Constants.sEntTaxID, value, sThisNodeOrder);
            }
        }

        public string AddressSeqNum
        {
            get
            {
                return Utils.getData(Node, "AddrSeqNo");
            }
            set
            {
                Utils.putData(Node, "AddrSeqNo", value, sThisNodeOrder);
            }
        }

        public string TaxIdTranslate(object otype)
        {
            object ret;
            if (dctTaxIdType.TryGetValue(otype, out ret))
                return ret + "";
            else
                return "";
        }
        
        public string TaxIdType
        {
            get
            {
                return Utils.getType(Node, Constants.sEntTaxID);
            }
            set
            {
                object odata;
                int iType;
                string sdata = value;
                bool i = false;

                if (Int32.TryParse(value, out iType))
                {
                    i = true;
                    odata = iType;
                }
                else
                    odata = value;

                if (dctTaxIdType.ContainsKey(odata))
                {
                    if (i)
                    {
                        dctTaxIdType.TryGetValue(iType, out odata);
                    }
                    sdata = (string)odata;
                    Utils.putType(putNode, Constants.sEntTaxID, sdata, sThisNodeOrder);
                }
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_INVALID_VALUE, "XMLEntity.TaxIdType",
                                     value + "", Document, Node);
            }
        }

        public void removeDemographics()
        {
            if (Demographics_Node != null)
                xmlThisNode.RemoveChild(Demographics_Node);
        }

        public void putDemographics(string sgender, string sgendercode, string sdob, string smarstatus,
                                    string smarstatuscode, int ispouseid)
        {
            if ((sgendercode != null && sgendercode != "") || (sgender != "" && sgender != null))
                putGender(sgender, sgendercode);
            if ((smarstatuscode != "" && smarstatuscode != null) || (smarstatus != "" && smarstatus != null))
                putMaritalStatus(smarstatus, smarstatuscode, ispouseid);
            if (sdob != null && sdob != "")
                DateOfBirth = sdob;
        }

        //Start SI06468
        public void putGender(string sgender, string sgendercode)
        {
            putGender(sgender, sgendercode, "");
        }
        //End SI06468
        //public void putGender(string sgender, string sgendercode) //SI06468
        public void putGender(string sgender, string sgendercode, string sgendercodeid) //SI06468
        {
            Gender = sgender;
            Gender_Code = sgendercode;
            Gender_CodeID = sgendercodeid;  //SI06468
        }

        public string Gender
        {
            get
            {
                return Utils.getData(Demographics_Node, Constants.sEntGender);
            }
            set
            {
                Utils.putData(putDemographics_Node, Constants.sEntGender, value, sDemoNodeOrder);
            }
        }

        public string Gender_Code
        {
            get
            {
                return Utils.getCode(Demographics_Node, Constants.sEntGender);
            }
            set
            {
                Utils.putCode(putDemographics_Node, Constants.sEntGender, value, sDemoNodeOrder);
            }
        }

        //Start SI06468
        public string Gender_CodeID
        {
            get
            {
                return Utils.getCodeID(Demographics_Node, Constants.sEntGender);
            }
            set
            {
                Utils.putCodeID(putDemographics_Node, Constants.sEntGender, value, sDemoNodeOrder);
            }
        }
        //End SI06468

        //Start SI06468
        public void putMaritalStatus(string sms, string smscode, int spsid)
        {
            putMaritalStatus(sms, smscode, spsid, "");
        }
        //End SI06468

        //public void putMaritalStatus(string sms, string smscode, int spsid) //SI06468
        public void putMaritalStatus(string sms, string smscode, int spsid, string smscodeid) //SI06468
        {
            MaritalStatus = sms;
            MaritalStatus_Code = smscode;
            SpouseID = spsid + "";
            MaritalStatus_CodeID = smscodeid; //SI06468
        }

        public string SpouseID
        {
            get
            {
                return Utils.getEntityRef(Demographics_Node, Constants.sEntMarStatus);
            }
            set
            {
                Utils.putEntityRef(putDemographics_Node, Constants.sEntMarStatus, value, sDemoNodeOrder);
            }
        }

        public string MaritalStatus
        {
            get
            {
                return Utils.getData(Demographics_Node, Constants.sEntMarStatus);
            }
            set
            {
                Utils.putData(putDemographics_Node, Constants.sEntMarStatus, value, sDemoNodeOrder);
            }
        }

        public string MaritalStatus_Code
        {
            get
            {
                return Utils.getCode(Demographics_Node, Constants.sEntMarStatus);
            }
            set
            {
                Utils.putCode(putDemographics_Node, Constants.sEntMarStatus, value, sDemoNodeOrder);
            }
        }

        //Start SI06468
        public string MaritalStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(Demographics_Node, Constants.sEntMarStatus);
            }
            set
            {
                Utils.putCodeID(putDemographics_Node, Constants.sEntMarStatus, value, sDemoNodeOrder);
            }
        }
        //End SI06468
        public string DateOfBirth
        {
            get
            {
                return Utils.getDate(Demographics_Node, Constants.sEntDOB);
            }
            set
            {
                Utils.putDate(putDemographics_Node, Constants.sEntDOB, value, sDemoNodeOrder);
            }
        }

        public string DateOfDeath
        {
            get
            {
                return Utils.getDate(Demographics_Node, Constants.sEntDOD);
            }
            set
            {
                Utils.putDate(putDemographics_Node, Constants.sEntDOD, value, sDemoNodeOrder);
            }
        }
        //Start SI06468
        public void putEducation(string sEducation, string sEducationCode)
        {
             putEducation(sEducation, sEducationCode, "");
        }
        //End SI06468

        //public void putEducation(string sEducation, string sEducationCode) //SI06468
        public void putEducation(string sEducation, string sEducationCode, string sEducationCodeID) //SI06468
        {
            Education = sEducation;
            Education_Code = sEducationCode;
            Education_CodeID = sEducationCodeID; //SI06468
        }

        public string Education
        {
            get
            {
                return Utils.getData(Demographics_Node, Constants.sEntEducation);
            }
            set
            {
                Utils.putData(putDemographics_Node, Constants.sEntEducation, value, sDemoNodeOrder);
            }
        }

        public string Education_Code
        {
            get
            {
                return Utils.getCode(Demographics_Node, Constants.sEntEducation);
            }
            set
            {
                Utils.putCode(putDemographics_Node, Constants.sEntEducation, value, sDemoNodeOrder);
            }
        }

        //Start SI06468
        public string Education_CodeID
        {
            get
            {
                return Utils.getCodeID(Demographics_Node, Constants.sEntEducation);
            }
            set
            {
                Utils.putCodeID(putDemographics_Node, Constants.sEntEducation, value, sDemoNodeOrder);
            }
        }
        //End SI06468
        
        //Start SI06468
        public void putPrimaryLanguage(string sPrimaryLanguage, string sPrimaryLanguageCode)
        {
             putPrimaryLanguage(sPrimaryLanguage, sPrimaryLanguageCode, ""); 
        }
        //End Si06468

        //public void putPrimaryLanguage(string sPrimaryLanguage, string sPrimaryLanguageCode) //SI06468
        public void putPrimaryLanguage(string sPrimaryLanguage, string sPrimaryLanguageCode, string sPrimaryLanguageCodeID) //SI06468    
        {
            PrimaryLanguage = sPrimaryLanguage;
            PrimaryLanguage_Code = sPrimaryLanguageCode;
            PrimaryLanguage_CodeID = sPrimaryLanguageCodeID;  //SI06468
        }

        public string PrimaryLanguage
        {
            get
            {
                return Utils.getData(Demographics_Node, Constants.sEntPrimaryLanguage);
            }
            set
            {
                Utils.putData(putDemographics_Node, Constants.sEntPrimaryLanguage, value, sDemoNodeOrder);
            }
        }

        public string PrimaryLanguage_Code
        {
            get
            {
                return Utils.getCode(Demographics_Node, Constants.sEntPrimaryLanguage);
            }
            set
            {
                Utils.putCode(putDemographics_Node, Constants.sEntPrimaryLanguage, value, sDemoNodeOrder);
            }
        }

        //Start SI06468
        public string PrimaryLanguage_CodeID
        {
            get
            {
                return Utils.getCodeID(Demographics_Node, Constants.sEntPrimaryLanguage);
            }
            set
            {
                Utils.putCodeID(putDemographics_Node, Constants.sEntPrimaryLanguage, value, sDemoNodeOrder);
            }
        }
        //End SI06468

        public void removeDriversLicense()
        {
            XmlNode xmlNode = Utils.getNode(Demographics_Node, Constants.sEntDriverLic);
            if (xmlNode != null)
                Demographics_Node.RemoveChild(xmlNode);
        }

        public void putDriverLicense(string sstate, string sstatecode, string snumber, string seff,
                                     string sexp, string squal, string squalcode)
        {
            if ((sstatecode != "" && sstatecode != null) || (sstate != "" && sstate != null))
                putDriverLicState(sstate, sstatecode);
            if (snumber != "" && snumber != null)
                DriverLicNumber = snumber;
            if (seff != null && seff != "")
                DriverLicEffDate = seff;
            if (sexp != null && sexp != "")
                DriverLicExpDate = sexp;
            if ((squalcode != "" && squalcode != null) || (squal != "" && squal != null))
                putDriverLicQual(squal, squalcode);
        }

        //Start SI06468
        public void putDriverLicState(string sstate, string sstatecode)
        {
            putDriverLicState(sstate, sstatecode, "");
        }
        //End SI06468

        //public void putDriverLicState(string sstate, string sstatecode) //SI06468
        public void putDriverLicState(string sstate, string sstatecode, string sstatecodeid) //SI06468
        {
            DriverLicState = sstate;
            DriverLicState_Code = sstatecode;
            DriverLicState_CodeID = sstatecodeid; //SI06468
        }

        public string DriverLicState
        {
            get
            {
                return Utils.getData(DriverLicense_Node, Constants.sEntDLState);
            }
            set
            {
                Utils.putData(putDriverLicense_Node, Constants.sEntDLState, value, sDrvLicNodeOrder);
            }
        }

        public string DriverLicState_Code
        {
            get
            {
                return Utils.getCode(DriverLicense_Node, Constants.sEntDLState);
            }
            set
            {
                Utils.putCode(putDriverLicense_Node, Constants.sEntDLState, value, sDrvLicNodeOrder);
            }
        }

        //Start SI06468
        public string DriverLicState_CodeID
        {
            get
            {
                return Utils.getCodeID(DriverLicense_Node, Constants.sEntDLState);
            }
            set
            {
                Utils.putCodeID(putDriverLicense_Node, Constants.sEntDLState, value, sDrvLicNodeOrder);
            }
        }
        //End SI06468
        public string DriverLicNumber
        {
            get
            {
                return Utils.getData(DriverLicense_Node, Constants.sEntDLNumber);
            }
            set
            {
                if (XML.AutoEncrypt)
                    if (StringUtils.Left(value, 1) != "~")
                        value = "~" + value;
                Utils.putData(putDriverLicense_Node, Constants.sEntDLNumber, value, sDrvLicNodeOrder);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey); //SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //START SIW490
        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel); 
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //END SIW490

        public string DisplayName //Start SI06345 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Name_Node, Constants.sEntDisplayName);
            }
            set
            {
                // Start SIN8050 - PutData of displayname in nodeorder of Entity_Name instead of Entity 
                //Utils.putData(putName_Node, Constants.sEntDisplayName, value, NodeOrder); 
                Utils.putData(putName_Node, Constants.sEntDisplayName, value, sNameNodeOrder); 
                //  End SIN8050
            }
        }//End SI06345  - Implemented in SI06333)

        public string DriverLicEffDate
        {
            get
            {
                return Utils.getDate(DriverLicense_Node, Constants.sEntDLEffDt);
            }
            set
            {
                Utils.putDate(putDriverLicense_Node, Constants.sEntDLEffDt, value, sDrvLicNodeOrder);
            }
        }

        public string DriverLicExpDate
        {
            get
            {
                return Utils.getDate(DriverLicense_Node, Constants.sEntDLExpDt);
            }
            set
            {
                Utils.putDate(putDriverLicense_Node, Constants.sEntDLExpDt, value, sDrvLicNodeOrder);
            }
        }

        public int DriverLicQualCount
        {
            get
            {
                object l = DriverLicQualList;
                return xmlDLQList.Count;
            }
        }

        public XmlNodeList DriverLicQualList
        {
            get
            {
                //Start SIW163
                if (null == xmlDLQList)
                {
                    return getNodeList(Constants.sEntDLQualifier, ref xmlDLQList, DriverLicense_Node);
                }
                else
                {
                    return xmlDLQList;
                }
                //End SIW163
            }
        }

        public XmlNode getDriverLicQual(bool reset)
        {
            //Start SIW163
            if (null != DriverLicQualList)
            {
                return getNode(reset, ref xmlDLQList, ref xmlDLQ);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public XmlNode addDriverLicQual()
        {
            xmlDLQ = XML.XMLaddNode(putDriverLicense_Node, Constants.sEntDLQualifier, sDrvLicNodeOrder);
            return xmlDLQ;
        }

        public void putDriverLicQual(string sType, string stypecode)
        {
            addDriverLicQual();
            putDriverLicQualData(sType, stypecode);
        }

        public string DriverLicQualCode
        {
            get
            {
                string strdata = "";
                if (xmlDLQ != null)
                    strdata = XML.XMLExtractCodeAttribute(xmlDLQ);
                return strdata;
            }
        }

        public string DriverLicQual
        {
            get
            {
                string strdata = "";
                if (xmlDLQ != null)
                    strdata = xmlDLQ.InnerText;
                return strdata;
            }
        }

        public void putDriverLicQualData(string sType, string stypecode)
        {
            if (xmlDLQ == null)
                addDriverLicQual();
            XML.XMLInsertCode_Node(xmlDLQ, sType, stypecode);
        }

        public void removeBusinessLicense()
        {
            XmlNode xmlPrnt, xmlNode;
            xmlPrnt = Demographics_Node;
            xmlNode = BusinessLicense_Node;
            if (xmlPrnt != null && xmlNode != null)
                xmlPrnt.RemoveChild(xmlNode);
        }

        public void putBusinessLicense(string sstate, string sstatecode, string snumber, string seff,
                                       string sexp, string squal, string squalcode)
        {
            if ((sstatecode != "" && sstatecode != null) || (sstate != "" && sstate != null))
                putBusinessLicState(sstate, sstatecode);
            if (snumber != "" && snumber != null)
                BusinessLicNumber = snumber;
            if (seff != "" && seff != null)
                BusinessLicEffDate = seff;
            if (sexp != "" && sexp != null)
                BusinessLicExpDate = sexp;
            if ((squalcode != "" && squalcode != null) || (squal != "" && squal != null))
                putBusinessLicQual(squal, squalcode);
        }

        //Start SI06468
        public void putBusinessLicState(string sstate, string sstatecode)
        {
            putBusinessLicState(sstate, sstatecode, "");
        }
        //End SI06468
        //public void putBusinessLicState(string sstate, string sstatecode) //SI06468
        public void putBusinessLicState(string sstate, string sstatecode, string sstatecodeid) //SI06468
        {
            BusinessLicState = sstate;
            BusinessLicState_Code = sstatecode;
            BusinessLicState_CodeID = sstatecodeid;  //SI06468
        }

        public string BusinessLicState
        {
            get
            {
                return Utils.getData(BusinessLicense_Node, Constants.sEntBusLicState);
            }
            set
            {
                Utils.putData(putBusinessLicense_Node, Constants.sEntBusLicState, value, sBusLicNodeOrder);
            }
        }

        public string BusinessLicState_Code
        {
            get
            {
                return Utils.getCode(BusinessLicense_Node, Constants.sEntBusLicState);
            }
            set
            {
                Utils.putCode(putBusinessLicense_Node, Constants.sEntBusLicState, value, sBusLicNodeOrder);
            }
        }

        //Start SI06468
        public string BusinessLicState_CodeID
        {
            get
            {
                return Utils.getCodeID(BusinessLicense_Node, Constants.sEntBusLicState);
            }
            set
            {
                Utils.putCodeID(putBusinessLicense_Node, Constants.sEntBusLicState, value, sBusLicNodeOrder);
            }
        }
        //End SI06468
        public string BusinessLicNumber
        {
            get
            {
                return Utils.getData(BusinessLicense_Node, Constants.sEntBusLicNumber);
            }
            set
            {
                if (XML.AutoEncrypt)
                    if (StringUtils.Left(value, 1) != "~")
                        value = "~" + value;
                Utils.putData(putBusinessLicense_Node, Constants.sEntBusLicNumber, value, sBusLicNodeOrder);
            }
        }

        public string BusinessLicEffDate
        {
            get
            {
                return Utils.getDate(BusinessLicense_Node, Constants.sEntBusLicEffDt);
            }
            set
            {
                Utils.putDate(putBusinessLicense_Node, Constants.sEntBusLicEffDt, value, sBusLicNodeOrder);
            }
        }

        public string BusinessLicExpDate
        {
            get
            {
                return Utils.getDate(BusinessLicense_Node, Constants.sEntBusLicExpDt);
            }
            set
            {
                Utils.putDate(putBusinessLicense_Node, Constants.sEntBusLicExpDt, value, sBusLicNodeOrder);
            }
        }

        public int BusinessLicQualCount
        {
            get
            {
                return BusinessLicQualList.Count;
            }
        }

        public XmlNodeList BusinessLicQualList
        {
            get
            {
                //Start SIW163
                if (null == xmlBLQList)
                {
                    return getNodeList(Constants.sEntBusLicQualifier, ref xmlBLQList, BusinessLicense_Node);
                }
                else
                {
                    return xmlBLQList;
                }
                //End SIW163
            }
        }

        public XmlNode getBusinessLicQual(bool reset)
        {
            //Start SIW163
            if (null != BusinessLicQualList)
            {
                return getNode(reset, ref xmlBLQList, ref xmlBLQ);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public XmlNode addBusinessLicQual()
        {
            xmlBLQ = XML.XMLaddNode(putBusinessLicense_Node, Constants.sEntBusLicQualifier, sBusLicNodeOrder);
            return xmlBLQ;
        }

        //Start SI06468
        public void putBusinessLicQual(string sType, string stypecode)
        {
            putBusinessLicQual(sType, stypecode, "");
        }
        //End SI06468

        //public void putBusinessLicQual(string sType, string stypecode)  //SI06468
        public void putBusinessLicQual(string sType, string stypecode, string stypeid)  //SI06468
        {
            addBusinessLicQual();
            //putBusinessLicQualData(sType, stypecode);   //SI06468
            putBusinessLicQualData(sType, stypecode, stypeid);   //SI06468
        }

        public string BusinessLicQualCode
        {
            get
            {
                string strdata = "";
                if (xmlBLQ != null)
                    strdata = XML.XMLExtractCodeAttribute(xmlBLQ);
                return strdata;
            }
        }
        //Start SI06468
        public string BusinessLicQualCodeID
        {
            get
            {
                string strdata = "";
                if (xmlBLQ != null)
                    strdata = XML.XMLExtractCodeAttribute(xmlBLQ);
                return strdata;
            }
        }

        //End SI06468
        public string BusinessLicQual
        {
            get
            {
                string strdata = "";
                if (xmlBLQ != null)
                    strdata = xmlBLQ.InnerText;
                return strdata;
            }
        }
        //Start SI06468
        public void putBusinessLicQualData(string sType, string stypecode)
        {
             putBusinessLicQualData(sType, stypecode, "");   
        }
        //End Si06468
        //public void putBusinessLicQualData(string sType, string stypecode) //SI06468
        public void putBusinessLicQualData(string sType, string stypecode, string stypeid) //SI06468    
        {
            if (xmlBLQ == null)
                addBusinessLicQual();
            //XML.XMLInsertCode_Node(xmlBLQ, sType, stypecode);        //SI06468
            XML.XMLInsertCode_Node(xmlBLQ, sType, stypecode, stypeid); //SI06468
        }

        public int ContactCount
        {
            get
            {
                return ContactList.Count;
            }
        }

        public XmlNodeList ContactList
        {
            get
            {
                //Start SIW163
                if (null == xmlContactList)
                {
                    return getNodeList(Constants.sEntContact, ref xmlContactList, Node);
                }
                else
                {
                    return xmlContactList;
                }
                //End SIW163
            }
        }

        public XmlNode getContact(bool reset)
        {
            //Start SIW163
            if (null != ContactList)
            {
                return getNode(reset, ref xmlContactList, ref xmlContact);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public XmlNode addContact()
        {
            ContactNode = null;
            return putContactNode;
        }

        //Start SIW7076
        //Start SIW7076
            //public string ContactID
            //{
            //    get
            //    {
            //        return Utils.getData(ContactNode, Constants.sEntContactID);
            //    }
            //    set
            //    {
            //        Utils.putData(putContactNode, Constants.sEntContactID, value, sContactNodeOrder);
            //    }
            //}
        //End SIW7076

        public string ContactID
        {
            get
            {
                return Utils.getAttribute(ContactNode, "", Constants.sEntContactID);
            }
            set
            {
                Utils.putAttribute(putContactNode, "", Constants.sEntContactID, value, sContactNodeOrder);
            }
        }
        //End SIW7076


        //Start SIW7790
        public string TechKeyContactInfo
        {
            get
            {
                return Utils.getData(ContactNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putContactNode, Constants.sStdTechKey, value, sContactNodeOrder);
            }
        }
        //End SIW7790
        //Start SIW7790
        public string Rel_ID
        {
            get
            {
                return Utils.getData(ContactNode, Constants.sContactRelId);
            }
            set
            {
                Utils.putData(putContactNode, Constants.sContactRelId, value, sContactNodeOrder);
            }
        }
        //End SIW7790
        public XmlNode putContactNode
        {
            get
            {
                if (ContactNode == null)
                    ContactNode = XML.XMLaddNode(Node, Constants.sEntContact, sThisNodeOrder);
                return ContactNode;
            }
        }

        public XmlNode ContactNode
        {
            get
            {
                return xmlContact;
            }
            set
            {
                xmlContact = value;
            }
        }

        //SIW7076 public XmlNode putContact(string sEntID, string sData, string sType, string stypecode,
        //SIW7076                          string safter, string sbefore, string bprimary)
             public XmlNode putContact(string sEntID, string sData, string sType, string stypecode,
                                  string safter, string sbefore, string bprimary,string sConID)
        {
            if (sEntID != "" && sEntID != null)
                ContactWhoID = sEntID;
            if (sData != "" && sData != null)
                ContactInfo = sData;
            if ((stypecode != "" && stypecode != null) || (sType != "" && sType != null))
                putContactType(sType, stypecode);
            if (safter != "" && safter != null)
                ContactAfter = safter;
            if (sbefore != "" && sbefore != null)
                ContactBefore = sbefore;
            if (bprimary != "" && bprimary != null)
                PrimaryContactFlag = bprimary;
            if (sConID != "" && sConID != null)
                ContactID = sConID;
            return ContactNode;
        }

        public string PrimaryContactFlag
        {
            get
            {
                return Utils.getBool(ContactNode, "", Constants.sEntContPrimary);
            }
            set
            {
                Utils.putBool(putContactNode, "", Constants.sEntContPrimary, value, NodeOrder);
            }
        }

        public string ContactWhoID
        {
            get
            {
                return Utils.getEntityRef(ContactNode, Constants.sEntContWho);
            }
            set
            {
                Utils.putEntityRef(putContactNode, Constants.sEntContWho, value, sContactNodeOrder);
            }
        }

        public string ContactInfo
        {
            get
            {
                return Utils.getData(ContactNode, Constants.sEntContInfo);
            }
            set
            {
                Utils.putData(putContactNode, Constants.sEntContInfo, value, sContactNodeOrder);
            }
        }

        public string ContactType
        {
            get
            {
                return Utils.getData(ContactNode, Constants.sEntContType);
            }
            set
            {
                Utils.putData(putContactNode, Constants.sEntContType, value, sContactNodeOrder);
            }
        }

        public string ContactTypeCode
        {
            get
            {
                return Utils.getCode(ContactNode, Constants.sEntContType);
            }
            set
            {
                Utils.putCode(putContactNode, Constants.sEntContType, value, sContactNodeOrder);
            }
        }
        //Start SI06468
        public string ContactTypeCodeID
        {
            get
            {
                return Utils.getCodeID(ContactNode, Constants.sEntContType);
            }
            set
            {
                Utils.putCodeID(putContactNode, Constants.sEntContType, value, sContactNodeOrder);
            }
        }
        //End SI06468

        //Start SI06468
        public XmlNode putContactType(string sDesc, string sCode)
        {
            return putContactType(sDesc, sCode, "");
        }
        //End SI06468

        //public XmlNode putContactType(string sDesc, string sCode) //SI06468
        public XmlNode putContactType(string sDesc, string sCode, string stypecodeid)  //SI06468
        {
            //return Utils.putCodeItem(putContactNode, Constants.sEntContType, sDesc, sCode, sContactNodeOrder); //SI06468
            return Utils.putCodeItem(putContactNode, Constants.sEntContType, sDesc, sCode, sContactNodeOrder, stypecodeid); //SI06468
        }

        public string ContactAfter
        {
            get
            {
                return Utils.getTime(ContactNode, Constants.sEntContAfter);
            }
            set
            {
                Utils.putTime(putContactNode, Constants.sEntContAfter, value, sContactNodeOrder);
            }
        }

        public string ContactBefore
        {
            get
            {
                return Utils.getTime(ContactNode, Constants.sEntContBefore);
            }
            set
            {
                Utils.putTime(putContactNode, Constants.sEntContBefore, value, sContactNodeOrder);
            }
        }
        
        //Start SI06249
        public string ContactEffectiveStartDate
        {
            get
            {
                //return Utils.getDate(Node, Constants.sEntContEffStartDt);
                return Utils.getDate(ContactNode, Constants.sEntContEffStartDt);
            }
            set
            {
                //Utils.putDate(putNode, Constants.sEntContEffStartDt, value, NodeOrder);
                Utils.putDate(putContactNode, Constants.sEntContEffStartDt, value, NodeOrder);
            }
        }
        public string ContactEffectiveEndDate
        {
            get
            {
                //return Utils.getDate(Node, Constants.sEntContEffEndDt);
                return Utils.getDate(ContactNode, Constants.sEntContEffEndDt);
            }
            set
            {
                //Utils.putDate(putNode, Constants.sEntContEffEndDt, value, NodeOrder);
                Utils.putDate(putContactNode, Constants.sEntContEffEndDt, value, NodeOrder);
            }
        }
        //End SI06249

        // Start SIW7620 
        public string TechkeyPhycian 
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sStdTechKey); 
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sStdTechKey, value, sPhysicianNodeOrder);
            }
        }
        public XmlNodeList PhysicianList
        {
            get
            {
                
                if (null == xmlPhysicianList)
                {
                    return getNodeList(Constants.sEntPHYSICIAN, ref xmlPhysicianList, Node);
                }
                else
                {
                    return xmlPhysicianList;
                }
                
            }
        }
        public XmlNode getPhysician(bool reset)
        {
            
            if (null != PhysicianList)
            {
                return getNode(reset, ref xmlPhysicianList, ref xmlPhysician);
            }
            else
            {
                return null;
            }
            
        }
        public XmlNode addPhysician()
        {
            PhysicianNode = null;
            return putPhysicianNode;
        }
        public XmlNode PhysicianNode
        {
            get
            {
                return xmlPhysician;
            }
            set
            {
                xmlPhysician = value;
            }
        }
        public XmlNode putPhysicianNode
        {
            get
            {
                if (PhysicianNode == null)
                    PhysicianNode = XML.XMLaddNode(Node, Constants.sEntPHYSICIAN, sThisNodeOrder);
                return PhysicianNode;
            }
        }
        public string PhyEntityID
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntPHYSEID);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntPHYSEID, value, sPhysicianNodeOrder);
            }
        }
        public string PhysicianNbr
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntPHYSICIANNUMBER);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntPHYSICIANNUMBER, value, sPhysicianNodeOrder);
            }
        }
        public string PhyMedStaffNbr
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntMEDSTAFFNUMBER);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntMEDSTAFFNUMBER, value, sPhysicianNodeOrder);
            }
        }
        public string PhyMedicareNbr
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntMEDICARENUMBER);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntMEDICARENUMBER, value, sPhysicianNodeOrder);
            }
        }
        
        public XmlNode putPrimarySpecialty(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPhysicianNode, Constants.sEntPRIMARYSPECIALTY, sDesc, sCode, sPhysicianNodeOrder, scodeid);
        }

        public string PrimarySpecialty
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntPRIMARYSPECIALTY);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntPRIMARYSPECIALTY, value, sPhysicianNodeOrder);
            }
        }

        public string PrimarySpecialty_Code
        {
            get
            {
                return Utils.getCode(PhysicianNode, Constants.sEntPRIMARYSPECIALTY);
            }
            set
            {
                Utils.putCode(putPhysicianNode, Constants.sEntPRIMARYSPECIALTY, value, sPhysicianNodeOrder);
            }
        }

        public string PrimarySpecialty_CodeID
        {
            get
            {
                return Utils.getCodeID(PhysicianNode, Constants.sEntPRIMARYSPECIALTY); 
            }
            set
            {
                Utils.putCodeID(putPhysicianNode, Constants.sEntPRIMARYSPECIALTY, value, sPhysicianNodeOrder);
            }
        }

        //SIN8034 start
        public string SubSpeciality
        {
            get
            {
                return Utils.getData(SubSpecialityNode, Constants.sEntSubSpecCode);
            }
            set
            {
                Utils.putData(putSubSpecialityNode, Constants.sEntSubSpecCode, value, sSubSpecNodeOrder);
            }
        }
        public string SubSpeciality_Code
        {
            get
            {
                return Utils.getCode(SubSpecialityNode, Constants.sEntSubSpecCode);
            }
            set
            {
                Utils.putCode(putSubSpecialityNode, Constants.sEntSubSpecCode, value, sSubSpecNodeOrder);
            }
        }
        public string SubSpeciality_CodeID
        {
            get
            {
                return Utils.getCodeID(SubSpecialityNode, Constants.sEntSubSpecCode);
            }
            set
            {
                Utils.putCodeID(putSubSpecialityNode, Constants.sEntSubSpecCode, value, sSubSpecNodeOrder);
            }
        }
        public XmlNode addSubSpeciality()
        {
            SubSpecialityNode = null;
            SubSpecialityCount = SubSpecialityCount + 1;
            NextSubSpecialityID = Convert.ToString(SubSpecialityCount + 1);
            return putSubSpecialityNode;
        }
        public XmlNode SubSpecialityNode
        {
            get
            {
                return m_xmlSubSpec;
            }
            set
            {
                m_xmlSubSpec = value;
            }
        }
        public int SubSpecialityCount
        {
            get
            {
                string strdata = Utils.getAttribute(SubSpecialitiesNode, "", Constants.sEntSubSpecCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putSubSpecialitiesNode, "", Constants.sEntSubSpecCount, value + "", sSubSpecNodeOrder);
            }
        }
        public XmlNode putSubSpecialityNode
        {
            get
            {
                if (SubSpecialityNode == null)
                    //SIW485 ActivityNode = XML.XMLaddNode(putActivitiesNode, Constants.sDiaActivity, Globals.sNodeOrder);
                    SubSpecialityNode = XML.XMLaddNode(putSubSpecialitiesNode, Constants.sEntSubSpeciality, sPhysicianNodeOrder);	//SIW485
                return SubSpecialityNode;
            }
        }   
        public XmlNode SubSpecialitiesNode
        {
            get
            {
                //start SI06267
                if (m_xmlSubSpecs == null)
                    m_xmlSubSpecs = XML.XMLGetNode(PhysicianNode, Constants.sEntSubSpecialities);
                //end SI06267
                return m_xmlSubSpecs;
            }
            set
            {
                m_xmlSubSpecs = value;
            }
        }
        public XmlNode putSubSpecialitiesNode
        {
            get
            {
                if (SubSpecialitiesNode == null)
                {
                    //SubSpecialitiesNode = XML.XMLaddNode(Parent_Node, NodeName, Parent_NodeOrder);
                    SubSpecialitiesNode = XML.XMLaddNode(putPhysicianNode, Constants.sEntSubSpecialities, sPhysicianNodeOrder);
                    NextSubSpecialityID = "1";
                    SubSpecialityCount = 0;
                }
                return SubSpecialitiesNode;
            }
        }
        public string NextSubSpecialityID
        {
            get
            {
                return Utils.getAttribute(SubSpecialitiesNode, "", Constants.sEntSubSpecNextID);
            }
            set
            {
                if (value == "")
                    value = "1";
                Utils.putAttribute(putSubSpecialitiesNode, "", Constants.sEntSubSpecNextID, value, sSubSpecNodeOrder);
            }
        }
        public XmlNodeList SubSpecialityList
        {
            get
            {
                //SI06267 return getNodeList(Constants.sDiaActivity, ref m_xmlActList, m_xmlActs);
                // Start SIW163
                if (null == m_xmlSubSpecList)
                {
                    return getNodeList(Constants.sEntSubSpeciality, ref m_xmlSubSpecList, SubSpecialitiesNode);   //SI06267
                }
                else
                {
                    return m_xmlSubSpecList;
                }
                //End SIW163
            }
        }

        public XmlNode getSubSpeciality(bool reset)
        {
            if (null != SubSpecialityList)
                return getNode(reset, ref m_xmlSubSpecList, ref m_xmlSubSpec);
            else
                return null;
            
        }
        public XmlNode getSubSpecialitybyID(string sID)
        {
            if (SubSpecialityID != sID)
            {
                getSubSpeciality(true);
                while (SubSpecialityNode != null)
                {
                    if (SubSpecialityID == sID)
                        break;
                    getSubSpeciality(false);
                }
            }
            return SubSpecialityNode;
        }
        public string SubSpecialityID
        {
            get
            {
                string strdata;
                string lid = "";
                if (SubSpecialityNode != null)
                {
                    strdata = XML.XMLGetAttributeValue(SubSpecialityNode, Constants.sEntSubSpecID);
                    lid = extSubSpecialityID(strdata);
                }
                return lid;
            }
            set
            {
                if (value == "" || value == null || value == "0")
                    value = getNextSubSpecialityID(null);
                XML.XMLSetAttributeValue(putSubSpecialityNode, Constants.sEntSubSpecID, Constants.sEntSubSpecIDPfx + value);
            }
        }
        public string SubSpecIDPrefix
        {
            get
            {
                return Constants.sEntSubSpecIDPfx;
            }
        }
        public string extSubSpecialityID(string strdata)
        {
            string lid = "";
            if (strdata != "" && strdata != null)
                if (StringUtils.Left(strdata, 3) == Constants.sEntSubSpecIDPfx && strdata.Length >= 4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
            return lid;
        }
        public string getNextSubSpecialityID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextSubSpecialityID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextSubSpecialityID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }
        //SIN8034 ends
        
        public XmlNode putStaffStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPhysicianNode, Constants.sEntSTAFFSTATUSCODE, sDesc, sCode, sPhysicianNodeOrder, scodeid);
        }
        public string StaffStatus
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntSTAFFSTATUSCODE);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntSTAFFSTATUSCODE, value, sPhysicianNodeOrder);
            }
        }
        public string StaffStatus_Code
        {
            get
            {
                return Utils.getCode(PhysicianNode, Constants.sEntSTAFFSTATUSCODE);
            }
            set
            {
                Utils.putCode(putPhysicianNode, Constants.sEntSTAFFSTATUSCODE, value, sPhysicianNodeOrder);
            }
        }
        public string StaffStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(PhysicianNode, Constants.sEntSTAFFSTATUSCODE);
            }
            set
            {
                Utils.putCodeID(putPhysicianNode, Constants.sEntSTAFFSTATUSCODE, value, sPhysicianNodeOrder);
            }
        }
        
        public XmlNode putStaffType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPhysicianNode, Constants.sEntSTAFFTYPECODE, sDesc, sCode, sPhysicianNodeOrder, scodeid);
        }
        public string StaffType
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntSTAFFTYPECODE);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntSTAFFTYPECODE, value, sPhysicianNodeOrder);
            }
        }
        public string StaffType_Code
        {
            get
            {
                return Utils.getCode(PhysicianNode, Constants.sEntSTAFFTYPECODE);
            }
            set
            {
                Utils.putCode(putPhysicianNode, Constants.sEntSTAFFTYPECODE, value, sPhysicianNodeOrder);
            }
        }
        public string StaffType_CodeID
        {
            get
            {
                return Utils.getCodeID(PhysicianNode, Constants.sEntSTAFFTYPECODE);
            }
            set
            {
                Utils.putCodeID(putPhysicianNode, Constants.sEntSTAFFTYPECODE, value, sPhysicianNodeOrder);
            }
        }
        
        public XmlNode putStaffCat(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPhysicianNode, Constants.sEntSTAFFCATCODE, sDesc, sCode, sPhysicianNodeOrder, scodeid);
        }
        public string StaffCat
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntSTAFFCATCODE);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntSTAFFCATCODE, value, sPhysicianNodeOrder);
            }
        }
        public string StaffCat_Code
        {
            get
            {
                return Utils.getCode(PhysicianNode, Constants.sEntSTAFFCATCODE);
            }
            set
            {
                Utils.putCode(putPhysicianNode, Constants.sEntSTAFFCATCODE, value, sPhysicianNodeOrder);
            }
        }
        public string StaffCat_CodeID
        {
            get
            {
                return Utils.getCodeID(PhysicianNode, Constants.sEntSTAFFCATCODE);
            }
            set
            {
                Utils.putCodeID(putPhysicianNode, Constants.sEntSTAFFCATCODE, value, sPhysicianNodeOrder);
            }
        }


        public string InternalNbr
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntINTERNALNUMBER);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntINTERNALNUMBER, value, sPhysicianNodeOrder);
            }
        }
        public string DeptAssignedEntID
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntDEPTASSIGNEDEID);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntDEPTASSIGNEDEID, value, sPhysicianNodeOrder);
            }
        }
        public string AppointDate
        {
            get
            {
                return Utils.getDate(PhysicianNode, Constants.sEntAPPOINTDATE);
            }
            set
            {
                Utils.putDate(putPhysicianNode, Constants.sEntAPPOINTDATE, value, sPhysicianNodeOrder);
            }
        }
        public string ReAppointDate
        {
            get
            {
                return Utils.getDate(PhysicianNode, Constants.sEntREAPPOINTDATE);
            }
            set
            {
                Utils.putDate(putPhysicianNode, Constants.sEntREAPPOINTDATE, value, sPhysicianNodeOrder);
            }
        }
        
        public void putLicState(string sstate, string sstatecode)
        {
            putLicState(sstate, sstatecode, "");
        }

        public void putLicState(string sstate, string sstatecode, string sstatecodeid)
        {
            LicState = sstate;
            LicState_Code = sstatecode;
            LicState_CodeID = sstatecodeid; 
        }

        public string LicState
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntLICSTATE);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntLICSTATE, value, sPhysicianNodeOrder);
            }
        }

        public string LicState_Code
        {
            get
            {
                return Utils.getCode(PhysicianNode, Constants.sEntLICSTATE);
            }
            set
            {
                Utils.putCode(putPhysicianNode, Constants.sEntLICSTATE, value, sPhysicianNodeOrder);
            }
        }

        public string LicState_CodeID
        {
            get
            {
                return Utils.getCodeID(PhysicianNode, Constants.sEntLICSTATE);
            }
            set
            {
                Utils.putCodeID(putPhysicianNode, Constants.sEntLICSTATE, value, sPhysicianNodeOrder);
            }
        }

        public string LicNumber
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntLICNUM);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntLICNUM, value, sPhysicianNodeOrder);
            }
        }
        public string LicIssueDate
        {
            get
            {
                return Utils.getDate(PhysicianNode, Constants.sEntLICISSUEDATE);
            }
            set
            {
                Utils.putDate(putPhysicianNode, Constants.sEntLICISSUEDATE, value, sPhysicianNodeOrder);
            }
        }
        public string LicExpDate
        {
            get
            {
                return Utils.getDate(PhysicianNode, Constants.sEntLICEXPIRYDATE);
            }
            set
            {
                Utils.putDate(putPhysicianNode, Constants.sEntLICEXPIRYDATE, value, sPhysicianNodeOrder);
            }
        }
        public string LicDEANbr
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntLICDEANUM);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntLICDEANUM, value, sPhysicianNodeOrder);
            }
        }
        public string LicDEAExpDate
        {
            get
            {
                return Utils.getDate(PhysicianNode, Constants.sEntLICDEAEXPDATE);
            }
            set
            {
                Utils.putDate(putPhysicianNode, Constants.sEntLICDEAEXPDATE, value, sPhysicianNodeOrder);
            }
        }
        public string Membership
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntMEMBERSHIP);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntMEMBERSHIP, value, sPhysicianNodeOrder);
            }
        }
        public string ContEducation
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntCONTEDUCATION);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntCONTEDUCATION, value, sPhysicianNodeOrder);
            }
        }
        public string TeachingExp
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntTEACHINGEXP);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntTEACHINGEXP, value, sPhysicianNodeOrder);
            }
        }
        public string InsCompany
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntINSCOMPANY);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntINSCOMPANY, value, sPhysicianNodeOrder);
            }
        }
        public string InsPolicy
        {
            get
            {
                return Utils.getData(PhysicianNode, Constants.sEntINSPOLICY);
            }
            set
            {
                Utils.putData(putPhysicianNode, Constants.sEntINSPOLICY, value, sPhysicianNodeOrder);
            }
        }

        //Med Staff ----------------------
        public string TechkeyMedStaff
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sStdTechKey, value, sMedStaffNodeOrder);
            }
        }

        public XmlNodeList MediStaffList
        {
            get
            {
                if (null == xmlMedStaffList)
                {
                    return getNodeList(Constants.sEntMEDSTAFF, ref xmlMedStaffList, Node);
                }
                else
                {
                    return xmlMedStaffList;
                }
            }
        }

        public XmlNode getMedSaff(bool reset)
        {
            
            if (null != MediStaffList)
            {
                return getNode(reset, ref xmlMedStaffList, ref xmlMedStaff);
            }
            else
            {
                return null;
            }
            
        }

        public XmlNode addMedStaff()
        {
            MedStaffNode = null;
            return putMedStaffNode;
        }

        public XmlNode MedStaffNode
        {
            get
            {
                return xmlMedStaff;
            }
            set
            {
                xmlMedStaff = value;
            }
        }


        public XmlNode putMedStaffNode
        {
            get
            {
                if (MedStaffNode == null)
                    MedStaffNode = XML.XMLaddNode(Node, Constants.sEntMEDSTAFF, sThisNodeOrder);
                return MedStaffNode;
            }
        }

        public string MedStaffEntID
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntSTAFFEID);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntSTAFFEID, value, sMedStaffNodeOrder);
            }
        }

        public string MedStaffNum
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntMEDSTAFFNUMBER);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntMEDSTAFFNUMBER, value, sMedStaffNodeOrder);
            }
        }             

        public XmlNode putMedStaffStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMedStaffNode, Constants.sEntMEDSTAFFSTATUSCODE, sDesc, sCode, sMedStaffNodeOrder, scodeid);
        }
        public string MedStaffStatus
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntMEDSTAFFSTATUSCODE);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntMEDSTAFFSTATUSCODE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffStatus_Code
        {
            get
            {
                return Utils.getCode(MedStaffNode, Constants.sEntMEDSTAFFSTATUSCODE);
            }
            set
            {
                Utils.putCode(putMedStaffNode, Constants.sEntMEDSTAFFSTATUSCODE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(MedStaffNode, Constants.sEntMEDSTAFFSTATUSCODE);
            }
            set
            {
                Utils.putCodeID(putMedStaffNode, Constants.sEntMEDSTAFFSTATUSCODE, value, sMedStaffNodeOrder);
            }
        }
        
        public XmlNode putMedStaffPos(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMedStaffNode, Constants.sEntMEDSTAFFPOSCODE, sDesc, sCode, sMedStaffNodeOrder, scodeid);
        }
        public string MedStaffPos
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntMEDSTAFFPOSCODE);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntMEDSTAFFPOSCODE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffPos_Code
        {
            get
            {
                return Utils.getCode(MedStaffNode, Constants.sEntMEDSTAFFPOSCODE);
            }
            set
            {
                Utils.putCode(putMedStaffNode, Constants.sEntMEDSTAFFPOSCODE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffPos_CodeID
        {
            get
            {
                return Utils.getCodeID(MedStaffNode, Constants.sEntMEDSTAFFPOSCODE);
            }
            set
            {
                Utils.putCodeID(putMedStaffNode, Constants.sEntMEDSTAFFPOSCODE, value, sMedStaffNodeOrder);
            }
        }
        
        public XmlNode putMedStaffCat(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMedStaffNode, Constants.sEntMEDSTAFFCATCODE, sDesc, sCode, sMedStaffNodeOrder, scodeid);
        }
        public string MedStaffCat
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntMEDSTAFFCATCODE);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntMEDSTAFFCATCODE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffCat_Code
        {
            get
            {
                return Utils.getCode(MedStaffNode, Constants.sEntMEDSTAFFCATCODE);
            }
            set
            {
                Utils.putCode(putMedStaffNode, Constants.sEntMEDSTAFFCATCODE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffCat_CodeID
        {
            get
            {
                return Utils.getCodeID(MedStaffNode, Constants.sEntMEDSTAFFCATCODE);
            }
            set
            {
                Utils.putCodeID(putMedStaffNode, Constants.sEntMEDSTAFFCATCODE, value, sMedStaffNodeOrder);
            }
        }


        public string MedStaffDeptAssignedEntID
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntMedDEPTASSINEDEID);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntMedDEPTASSINEDEID, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffHireDate
        {
            get
            {
                return Utils.getDate(MedStaffNode, Constants.sEntHIREDATE);
            }
            set
            {
                Utils.putDate(putMedStaffNode, Constants.sEntHIREDATE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffLicNum
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntMEDLICNUM);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntMEDLICNUM, value, sMedStaffNodeOrder);
            }
        }
       
        public void putMedStaffLicState(string sstate, string sstatecode)
        {
            putMedStaffLicState(sstate, sstatecode, "");
        }
        
        public void putMedStaffLicState(string sstate, string sstatecode, string sstatecodeid) 
        {
            MedStaffLicState = sstate;
            MedStaffLicState_Code = sstatecode;
            MedStaffLicState_CodeID = sstatecodeid;  
        }

        public string MedStaffLicState
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntMEDLICSTATE);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntMEDLICSTATE, value, sMedStaffNodeOrder);
            }
        }

        public string MedStaffLicState_Code
        {
            get
            {
                return Utils.getCode(MedStaffNode, Constants.sEntMEDLICSTATE);
            }
            set
            {
                Utils.putCode(putMedStaffNode, Constants.sEntMEDLICSTATE, value, sMedStaffNodeOrder);
            }
        }

        public string MedStaffLicState_CodeID
        {
            get
            {
                return Utils.getCodeID(MedStaffNode, Constants.sEntMEDLICSTATE);
            }
            set
            {
                Utils.putCodeID(putMedStaffNode, Constants.sEntMEDLICSTATE, value, sMedStaffNodeOrder);
            }
        }

        public string MedStaffLicIssueDate
        {
            get
            {
                return Utils.getDate(MedStaffNode, Constants.sEntMEDLICISSUEDATE);
            }
            set
            {
                Utils.putDate(putMedStaffNode, Constants.sEntMEDLICISSUEDATE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffLicExpDate
        {
            get
            {
                return Utils.getDate(MedStaffNode, Constants.sEntMEDLICEXPIRYDATE);
            }
            set
            {
                Utils.putDate(putMedStaffNode, Constants.sEntMEDLICEXPIRYDATE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffLicDEANbr
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntMEDLICDEANUM);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntMEDLICDEANUM, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffDEAExpDate
        {
            get
            {
                return Utils.getDate(MedStaffNode, Constants.sEntMEDLICDEAEXPDATE);
            }
            set
            {
                Utils.putDate(putMedStaffNode, Constants.sEntMEDLICDEAEXPDATE, value, sMedStaffNodeOrder);
            }
        }
        public string MedStaffNbr
        {
            get
            {
                return Utils.getData(MedStaffNode, Constants.sEntMEDSTAFFNUMBER);
            }
            set
            {
                Utils.putData(putMedStaffNode, Constants.sEntMEDSTAFFNUMBER, value, sMedStaffNodeOrder);
            }
        }

        //Physician -> Privilage--------------------------------------

      // SIW7620 == starts
        public string PhyPrivDelete
        {
            get
            {
                return Utils.getBool(PHYPrivilageNode, Constants.sAHDeleteFlag, ""); 
            }
            set
            {
                Utils.putBool(putPHYPrivilageNode, Constants.sAHDeleteFlag, "", value, sPHYPrivilageNodeOrder); 
            }
        }
        //SIW7620 ends
        public string TechkeyPhyPriv
        {
            get
            {
                return Utils.getData(PHYPrivilageNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putPHYPrivilageNode, Constants.sStdTechKey, value, sPHYPrivilageNodeOrder);
            }
        }
        public int PHYPrivilageCount
        {
            get
            {
                return PHYPrivilageList.Count;
            }
        }

        public XmlNodeList PHYPrivilageList
        {
            get
            {
                
                if (null == xmlPHYPrivilageList)
                {
                    return getNodeList(Constants.sEntPHYPRIVILEGE, ref xmlPHYPrivilageList, Node);
                }
                else
                {
                    return xmlPHYPrivilageList;
                }
            }
        }

        public XmlNode getPHYPrivilage(bool reset)
        {
            if (null != PHYPrivilageList)
            {
                return getNode(reset, ref xmlPHYPrivilageList, ref xmlPHYPrivilage);
            }
            else
            {
                return null;
            }
        }

        public XmlNode addPHYPrivilage()
        {
            PHYPrivilageNode = null;
            return putPHYPrivilageNode;
        }

        public XmlNode PHYPrivilageNode
        {
            get
            {
                return xmlPHYPrivilage;
            }
            set
            {
                xmlPHYPrivilage = value;
            }
        }

        public XmlNode putPHYPrivilageNode
        {
            get
            {
                if (PHYPrivilageNode == null)
                    PHYPrivilageNode = XML.XMLaddNode(Node, Constants.sEntPHYPRIVILEGE, sThisNodeOrder);
                return PHYPrivilageNode;
            }
        }

        public XmlNode putPHYPrivilege(string sKeyPrivID, string sPhyPrivCategoryDesc, string sPhyPrivCategoryCode,
                                  string sPhyPrivCategoryCodeID, string sPhyPrivTypeDesc, string sPhyPrivTypeCd, string sPhyPrivTypeID,
                                  string sPhyPrivStatusDesc, string sPhyPrivStatusCd, string sPhyPrivStatusID,
                                  string sPhyPrivStartDate, string sPhyPrivEndDate)
        {
            TechkeyPhyPriv = sKeyPrivID;
            PhyPrivCategory = sPhyPrivCategoryDesc;
            PhyPrivCategory_Code = sPhyPrivCategoryCode;
            PhyPrivCategory_CodeID = sPhyPrivCategoryCodeID;
            PhyPrivType = sPhyPrivTypeDesc;
            PhyPrivType_Code = sPhyPrivTypeCd;
            PhyPrivType_CodeID = sPhyPrivTypeID; 
            PhyPrivStatus = sPhyPrivStatusDesc;
            PhyPrivStatus_Code = sPhyPrivStatusCd;
            PhyPrivStatus_CodeID = sPhyPrivStatusID;
            PhyPrivStartDate = sPhyPrivStartDate;
            PhyPrivEndDate = sPhyPrivEndDate;
            return PHYPrivilageNode;
        }

        public string PhyPrivID
        {
            get
            {
                return Utils.getData(PHYPrivilageNode, Constants.sEntPHYPRIVID);
            }
            set
            {
                Utils.putData(putPHYPrivilageNode, Constants.sEntPHYPRIVID, value, sPHYPrivilageNodeOrder);
            }
        }

        public string PhyPrivEntID
        {
            get
            {
                return Utils.getData(PHYPrivilageNode, Constants.sEntPRIVPHYSEID);
            }
            set
            {
                Utils.putData(putPHYPrivilageNode, Constants.sEntPRIVPHYSEID, value, sPHYPrivilageNodeOrder);
            }
        }        
       
        public XmlNode putPhyPrivCategory(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPHYPrivilageNode, Constants.sEntPHYCATEGORY_CODE, sDesc, sCode, sPHYPrivilageNodeOrder, scodeid);
        }
        public string PhyPrivCategory
        {
            get
            {
                return Utils.getData(PHYPrivilageNode, Constants.sEntPHYCATEGORY_CODE);
            }
            set
            {
                Utils.putData(putPHYPrivilageNode, Constants.sEntPHYCATEGORY_CODE, value, sPHYPrivilageNodeOrder);
            }
        }
        public string PhyPrivCategory_Code
        {
            get
            {
                return Utils.getCode(PHYPrivilageNode, Constants.sEntPHYCATEGORY_CODE);
            }
            set
            {
                Utils.putCode(putPHYPrivilageNode, Constants.sEntPHYCATEGORY_CODE, value, sPHYPrivilageNodeOrder);
            }
        }
        public string PhyPrivCategory_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYPrivilageNode, Constants.sEntPHYCATEGORY_CODE);
            }
            set
            {
                Utils.putCodeID(putPHYPrivilageNode, Constants.sEntPHYCATEGORY_CODE, value, sPHYPrivilageNodeOrder);
            }
        }
        
        public XmlNode putPPhyPrivType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPHYPrivilageNode, Constants.sEntPHYTYPE_CODE, sDesc, sCode, sPHYPrivilageNodeOrder, scodeid);
        }
        public string PhyPrivType
        {
            get
            {
                return Utils.getData(PHYPrivilageNode, Constants.sEntPHYTYPE_CODE);
            }
            set
            {
                Utils.putData(putPHYPrivilageNode, Constants.sEntPHYTYPE_CODE, value, sPHYPrivilageNodeOrder);
            }
        }
        public string PhyPrivType_Code
        {
            get
            {
                return Utils.getCode(PHYPrivilageNode, Constants.sEntPHYTYPE_CODE);
            }
            set
            {
                Utils.putCode(putPHYPrivilageNode, Constants.sEntPHYTYPE_CODE, value, sPHYPrivilageNodeOrder);
            }
        }
        public string PhyPrivType_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYPrivilageNode, Constants.sEntPHYTYPE_CODE);
            }
            set
            {
                Utils.putCodeID(putPHYPrivilageNode, Constants.sEntPHYTYPE_CODE, value, sPHYPrivilageNodeOrder);
            }
        }
                
        public XmlNode putPhyPrivStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPHYPrivilageNode, Constants.sEntPHYSTATUS_CODE, sDesc, sCode, sPHYPrivilageNodeOrder, scodeid);
        }
        public string PhyPrivStatus
        {
            get
            {
                return Utils.getData(PHYPrivilageNode, Constants.sEntPHYSTATUS_CODE);
            }
            set
            {
                Utils.putData(putPHYPrivilageNode, Constants.sEntPHYSTATUS_CODE, value, sPHYPrivilageNodeOrder);
            }
        }
        public string PhyPrivStatus_Code
        {
            get
            {
                return Utils.getCode(PHYPrivilageNode, Constants.sEntPHYSTATUS_CODE);
            }
            set
            {
                Utils.putCode(putPHYPrivilageNode, Constants.sEntPHYSTATUS_CODE, value, sPHYPrivilageNodeOrder);
            }
        }
        public string PhyPrivStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYPrivilageNode, Constants.sEntPHYSTATUS_CODE);
            }
            set
            {
                Utils.putCodeID(putPHYPrivilageNode, Constants.sEntPHYSTATUS_CODE, value, sPHYPrivilageNodeOrder);
            }
        }

        public string PhyPrivStartDate
        {
            get
            {
                return Utils.getDate(PHYPrivilageNode, Constants.sEntPHYINT_DATE);
            }
            set
            {
                Utils.putDate(putPHYPrivilageNode, Constants.sEntPHYINT_DATE, value, sPHYPrivilageNodeOrder);
            }
        }
        public string PhyPrivEndDate
        {
            get
            {
                return Utils.getDate(PHYPrivilageNode, Constants.sEntPHYEND_DATE);
            }
            set
            {
                Utils.putDate(putPHYPrivilageNode, Constants.sEntPHYEND_DATE, value, sPHYPrivilageNodeOrder);
            }
        }

        //Physician -> Certification----------------------------------

        //SIW7620 == starts
        public string PhyCertDelete
        {
            get
            {
                return Utils.getBool(PHYCertNode, Constants.sAHDeleteFlag, "");
            }
            set
            {
                Utils.putBool(putPHYCerteNode, Constants.sAHDeleteFlag, "", value, sPHYCertificationNodeOrder);
            }
        }
        //SIW7620 ends

        public string TechkeyPhyCert
        {
            get
            {
                return Utils.getData(PHYCertNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putPHYCerteNode, Constants.sStdTechKey, value, sPHYCertificationNodeOrder);
            }
        }
        public int PHYCertCount
        {
            get
            {
                return PHYCertList.Count;
            }
        }

        public XmlNodeList PHYCertList
        {
            get
            {

                if (null == xmlPHYCertificationList)
                {
                    return getNodeList(Constants.sEntPHYCERTS, ref xmlPHYCertificationList, Node);
                }
                else
                {
                    return xmlPHYCertificationList;
                }
            }
        }
        public XmlNode getPHYCert(bool reset)
        {
            if (null != PHYCertList)
            {
                return getNode(reset, ref xmlPHYCertificationList, ref xmlPHYCertification);
            }
            else
            {
                return null;
            }
        }
        public XmlNode addPHYCert()
        {
            PHYCertNode = null;
            return PHYCertNode;
        }

        public XmlNode PHYCertNode
        {
            get
            {
                return xmlPHYCertification;
            }
            set
            {
                xmlPHYCertification = value;
            }
        }

        public XmlNode putPHYCerteNode
        {
            get
            {
                if (PHYCertNode == null)
                    PHYCertNode = XML.XMLaddNode(Node, Constants.sEntPHYCERTS, sThisNodeOrder);
                return PHYCertNode;
            }
        }

        public XmlNode putPhyCertification(string sID, string sEntID, string stPHYCertNameDesc, string stPHYCertNameCode, string stPHYCertNameCodeID, string stPhyCertStatusDesc, string stPhyCertStatusCode, string stPhyCertStatusCodeID,
                                string stPhyCertPhyBoardDesc, string stPhyCertPhyBoardeCode, string stPhyCertPhyBoardCodeId, string sPHYCERTINTDATE, string sPHYCERTENDDATE)
        {
            if (sID != null)
                PhyCertID = sID;
            if (sEntID != "" && sEntID != null)
                PhyCertEntID = sEntID;
            if ((stPHYCertNameDesc != "" && stPHYCertNameDesc != null) || (stPHYCertNameCode != "" && stPHYCertNameCode != null) || (stPHYCertNameCodeID != "" && stPHYCertNameCodeID != null))
                putPHYCertNameCode(stPHYCertNameDesc, stPHYCertNameCode, stPHYCertNameCodeID);
            if ((stPhyCertStatusDesc != "" && stPhyCertStatusDesc != null) || (stPhyCertStatusCode != "" && stPhyCertStatusCode != null) || (stPhyCertStatusCodeID != "" && stPhyCertStatusCodeID != null))
                putPhyCertStatusCode(stPhyCertStatusDesc, stPhyCertStatusCode, stPhyCertStatusCodeID);
            if ((stPhyCertPhyBoardDesc != "" && stPhyCertPhyBoardDesc != null) || (stPhyCertPhyBoardeCode != "" && stPhyCertPhyBoardeCode != null) || (stPhyCertPhyBoardCodeId != "" && stPhyCertPhyBoardCodeId != null))
                putPhyCertPhyBoardCode(stPhyCertPhyBoardDesc, stPhyCertPhyBoardeCode, stPhyCertPhyBoardCodeId);

            if (sPHYCERTINTDATE != null)
                PhyCertIntDate = sPHYCERTINTDATE;
            if (sPHYCERTENDDATE != null)
                PhyCertEndDate = sPHYCERTENDDATE;

            return PHYCertNode;
        }

        public XmlNode putPHYCertNameCode(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putPHYCerteNode, Constants.sEntPHYNAMECODE, sDesc, sCode, sPHYCertificationNodeOrder, stypecodeid);
        }
        public XmlNode putPhyCertStatusCode(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putPHYCerteNode, Constants.sEntPHYSTATUS_CODE, sDesc, sCode, sPHYCertificationNodeOrder, stypecodeid);
        }
        public XmlNode putPhyCertPhyBoardCode(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putPHYCerteNode, Constants.sEntPHYBOARDCODE, sDesc, sCode, sPHYCertificationNodeOrder, stypecodeid);
        }

        public string PhyCertID
        {
            get
            {
                return Utils.getData(PHYCertNode, Constants.sEntPHYCERTID);
            }
            set
            {
                Utils.putData(putPHYCerteNode, Constants.sEntPHYCERTID, value, sPHYCertificationNodeOrder);
            }
        }
        public string PhyCertEntID
        {
            get
            {
                return Utils.getData(PHYCertNode, Constants.sEntPRIVPHYSEID);
            }
            set
            {
                Utils.putData(putPHYCerteNode, Constants.sEntPRIVPHYSEID, value, sPHYCertificationNodeOrder);
            }
        }
        
        public XmlNode putPhyCertName(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPHYCerteNode, Constants.sEntPHYNAMECODE, sDesc, sCode, sPHYCertificationNodeOrder, scodeid);
        }
        public string PhyCertName
        {
            get
            {
                return Utils.getData(PHYCertNode, Constants.sEntPHYNAMECODE);
            }
            set
            {
                Utils.putData(putPHYCerteNode, Constants.sEntPHYNAMECODE, value, sPHYCertificationNodeOrder);
            }
        }
        public string PhyCertName_Code
        {
            get
            {
                return Utils.getCode(PHYCertNode, Constants.sEntPHYNAMECODE);
            }
            set
            {
                Utils.putCode(putPHYCerteNode, Constants.sEntPHYNAMECODE, value, sPHYCertificationNodeOrder);
            }
        }
        public string PhyCertName_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYCertNode, Constants.sEntPHYNAMECODE);
            }
            set
            {
                Utils.putCodeID(putPHYCerteNode, Constants.sEntPHYNAMECODE, value, sPHYCertificationNodeOrder);
            }
        }
        
        public XmlNode putPhyCertStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPHYCerteNode, Constants.sEntPHYSTATUS_CODE, sDesc, sCode, sPHYCertificationNodeOrder, scodeid);
        }
        public string PhyCertStatus
        {
            get
            {
                return Utils.getData(PHYCertNode, Constants.sEntPHYSTATUS_CODE);
            }
            set
            {
                Utils.putData(putPHYCerteNode, Constants.sEntPHYSTATUS_CODE, value, sPHYCertificationNodeOrder);
            }
        }
        public string PhyCertStatus_Code
        {
            get
            {
                return Utils.getCode(PHYCertNode, Constants.sEntPHYSTATUS_CODE);
            }
            set
            {
                Utils.putCode(putPHYCerteNode, Constants.sEntPHYSTATUS_CODE, value, sPHYCertificationNodeOrder);
            }
        }
        public string PhyCertStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYCertNode, Constants.sEntPHYSTATUS_CODE);
            }
            set
            {
                Utils.putCodeID(putPHYCerteNode, Constants.sEntPHYSTATUS_CODE, value, sPHYCertificationNodeOrder);
            }
        }
                
        public XmlNode putPhyCertBoard(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPHYCerteNode, Constants.sEntPHYBOARDCODE, sDesc, sCode, sPHYCertificationNodeOrder, scodeid);
        }
        public string PhyCertBoard
        {
            get
            {
                return Utils.getData(PHYCertNode, Constants.sEntPHYBOARDCODE);
            }
            set
            {
                Utils.putData(putPHYCerteNode, Constants.sEntPHYBOARDCODE, value, sPHYCertificationNodeOrder);
            }
        }
        public string PhyCertBoard_Code
        {
            get
            {
                return Utils.getCode(PHYCertNode, Constants.sEntPHYBOARDCODE);
            }
            set
            {
                Utils.putCode(putPHYCerteNode, Constants.sEntPHYBOARDCODE, value, sPHYCertificationNodeOrder);
            }
        }
        public string PhyCertBoard_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYCertNode, Constants.sEntPHYBOARDCODE);
            }
            set
            {
                Utils.putCodeID(putPHYCerteNode, Constants.sEntPHYBOARDCODE, value, sPHYCertificationNodeOrder);
            }
        }
        public string PhyCertIntDate
        {
            get
            {
                return Utils.getDate(PHYCertNode, Constants.sEntPHYINT_DATE);
            }
            set
            {
                Utils.putDate(putPHYCerteNode, Constants.sEntPHYINT_DATE, value, sPHYCertificationNodeOrder);
            }
        }
        public string PhyCertEndDate
        {
            get
            {
                return Utils.getDate(PHYCertNode, Constants.sEntPHYEND_DATE);
            }
            set
            {
                Utils.putDate(putPHYCerteNode, Constants.sEntPHYEND_DATE, value, sPHYCertificationNodeOrder);
            }
        }

        //Physician -> Education--------------------------------------------------
        //SIW7620 == starts
        public string PhyEduDelete
        {
            get
            {
                return Utils.getBool(PHYEDUCATIONNode, Constants.sAHDeleteFlag, "");
            }
            set
            {
                Utils.putBool(putPHYEDUCATIONNode, Constants.sAHDeleteFlag, "", value, sPHYEducationNodeOrder);
            }
        }
        //SIW7620 ends

        public string TechkeyPhyEdu
        {
            get
            {
                return Utils.getData(PHYEDUCATIONNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putPHYEDUCATIONNode, Constants.sStdTechKey, value, sPHYEducationNodeOrder);
            }
        }
        public int PHYEduCount
        {
            get
            {
                return PHYEDUList.Count;
            }
        }
        public XmlNodeList PHYEDUList
        {
            get
            {

                if (null == xmlPHYEducationList)
                {
                    return getNodeList(Constants.sEntPHYEDUCATION, ref xmlPHYEducationList, Node);
                }
                else
                {
                    return xmlPHYEducationList;
                }
            }
        }
        public XmlNode getPHYEDUCATION(bool reset)
        {
            if (null != PHYEDUList)
            {
                return getNode(reset, ref xmlPHYEducationList, ref xmlPHYEducation);
            }
            else
            {
                return null;
            }
        }
        public XmlNode addPHYEDUCATION()
        {
            PHYEDUCATIONNode = null;
            return PHYEDUCATIONNode;
        }

        public XmlNode PHYEDUCATIONNode
        {
            get
            {
                return xmlPHYEducation;
            }
            set
            {
                xmlPHYEducation = value;
            }
        }

        public XmlNode putPHYEDUCATIONNode
        {
            get
            {
                if (PHYEDUCATIONNode == null)
                    PHYEDUCATIONNode = XML.XMLaddNode(Node, Constants.sEntPHYEDUCATION, sThisNodeOrder);
                return PHYEDUCATIONNode;
            }
        }

        public XmlNode putPhyEducation(string sID, string sEntID, string stPhyEduTypeDesc, string stPhyEduTypeCode, string stPhyEduTypeCodeID, string stPHYINSTITUTIONEIDDesc, string stPHYINSTITUTIONEIDCode, string stPHYINSTITUTIONEIDCodeID,
                                string stPhyDegreeDesc, string stPhyDegreeCode, string stPhyDegreeCodeId, string sPHYDEGREEDATE)
        {
            if (sID != null)
                PhyEduID = sID;
            if (sEntID != "" && sEntID != null)
                PhyEduEntID = sEntID;
            if ((stPhyEduTypeDesc != "" && stPhyEduTypeDesc != null) || (stPhyEduTypeCode != "" && stPhyEduTypeCode != null) || (stPhyEduTypeCodeID != "" && stPhyEduTypeCodeID != null))
                putPHYEDUCTYPECODE(stPhyEduTypeDesc, stPhyEduTypeCode, stPhyEduTypeCodeID);
            if ((stPHYINSTITUTIONEIDDesc != "" && stPHYINSTITUTIONEIDDesc != null) || (stPHYINSTITUTIONEIDCode != "" && stPHYINSTITUTIONEIDCode != null) || (stPHYINSTITUTIONEIDCodeID != "" && stPHYINSTITUTIONEIDCodeID != null))
                putPHYINSTITUTIONEID(stPHYINSTITUTIONEIDDesc, stPHYINSTITUTIONEIDCode, stPHYINSTITUTIONEIDCodeID);
            if ((stPhyDegreeDesc != "" && stPhyDegreeDesc != null) || (stPhyDegreeCode != "" && stPhyDegreeCode != null) || (stPhyDegreeCodeId != "" && stPhyDegreeCodeId != null))
                putPHYDEGREETYPE(stPhyDegreeDesc, stPhyDegreeCode, stPhyDegreeCodeId);
            if (sPHYDEGREEDATE != null)
                PhyEduDegreeDate = sPHYDEGREEDATE;
            
            return PHYHospitalNode;
        }

        public XmlNode putPHYEDUCTYPECODE(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putPHYEDUCATIONNode, Constants.sEntPHYEDUCTYPECODE, sDesc, sCode, sPHYEducationNodeOrder, stypecodeid);
        }
        public XmlNode putPHYINSTITUTIONEID(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putPHYEDUCATIONNode, Constants.sEntPHYINSTITUTIONEID, sDesc, sCode, sPHYEducationNodeOrder, stypecodeid);
        }
        public XmlNode putPHYDEGREETYPE(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putPHYEDUCATIONNode, Constants.sEntPHYDEGREETYPE, sDesc, sCode, sPHYEducationNodeOrder, stypecodeid);
        }

        public string PhyEduID
        {
            get
            {
                return Utils.getData(PHYEDUCATIONNode, Constants.sEntPHYEDUCID);
            }
            set
            {
                Utils.putData(putPHYEDUCATIONNode, Constants.sEntPHYEDUCID, value, sPHYEducationNodeOrder);
            }
        }
        
        public XmlNode putPhyEduType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPHYEDUCATIONNode, Constants.sEntPHYEDUCTYPECODE, sDesc, sCode, sPHYEducationNodeOrder, scodeid);
        }
        public string PhyEduType
        {
            get
            {
                return Utils.getData(PHYEDUCATIONNode, Constants.sEntPHYEDUCTYPECODE);
            }
            set
            {
                Utils.putData(putPHYEDUCATIONNode, Constants.sEntPHYEDUCTYPECODE, value, sPHYEducationNodeOrder);
            }
        }
        public string PhyEduType_Code
        {
            get
            {
                return Utils.getCode(PHYEDUCATIONNode, Constants.sEntPHYEDUCTYPECODE);
            }
            set
            {
                Utils.putCode(putPHYEDUCATIONNode, Constants.sEntPHYEDUCTYPECODE, value, sPHYEducationNodeOrder);
            }
        }
        public string PhyEduType_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYEDUCATIONNode, Constants.sEntPHYEDUCTYPECODE);
            }
            set
            {
                Utils.putCodeID(putPHYEDUCATIONNode, Constants.sEntPHYEDUCTYPECODE, value, sPHYEducationNodeOrder);
            }
        }
        public string PhyEduEntID
        {
            get
            {
                return Utils.getData(PHYEDUCATIONNode, Constants.sEntPHYINSTITUTIONEID);
            }
            set
            {
                Utils.putData(putPHYEDUCATIONNode, Constants.sEntPHYINSTITUTIONEID, value, sPHYEducationNodeOrder);
            }
        }

        public string InstitutionID
        {
            get
            {
                return Utils.getAttribute(PHYEDUCATIONNode, Constants.sEntPHYINSTITUTIONEID, Constants.sEntInstitutionID);
            }
            set
            {
                Utils.putAttribute(putPHYEDUCATIONNode, Constants.sEntPHYINSTITUTIONEID, Constants.sEntInstitutionID, value, sPHYEducationNodeOrder);
            }
        }

        public XmlNode putPhyEduDegreeType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPHYEDUCATIONNode, Constants.sEntPHYDEGREETYPE, sDesc, sCode, sPHYEducationNodeOrder, scodeid);
        }
        public string PhyEduDegreeType
        {
            get
            {
                return Utils.getData(PHYEDUCATIONNode, Constants.sEntPHYDEGREETYPE);
            }
            set
            {
                Utils.putData(putPHYEDUCATIONNode, Constants.sEntPHYDEGREETYPE, value, sPHYEducationNodeOrder);
            }
        }
        public string PhyEduDegreeType_Code
        {
            get
            {
                return Utils.getCode(PHYEDUCATIONNode, Constants.sEntPHYDEGREETYPE);
            }
            set
            {
                Utils.putCode(putPHYEDUCATIONNode, Constants.sEntPHYDEGREETYPE, value, sPHYEducationNodeOrder);
            }
        }
        public string PhyEduDegreeType_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYEDUCATIONNode, Constants.sEntPHYDEGREETYPE);
            }
            set
            {
                Utils.putCodeID(putPHYEDUCATIONNode, Constants.sEntPHYDEGREETYPE, value, sPHYEducationNodeOrder);
            }
        }
        public string PhyEduDegreeDate
        {
            get
            {
                return Utils.getDate(PHYEDUCATIONNode, Constants.sEntPHYDEGREEDATE);
            }
            set
            {
                Utils.putDate(putPHYEDUCATIONNode, Constants.sEntPHYDEGREEDATE, value, sPHYEducationNodeOrder);
            }
        }

        //Physician -> Hospital----------------------------------------------------
        //SIW7620 == starts
        public string PhyPrevHospDelete
        {
            get
            {
                return Utils.getBool(PHYHospitalNode, Constants.sAHDeleteFlag, "");
            }
            set
            {
                Utils.putBool(putPhyHospitalNode, Constants.sAHDeleteFlag, "", value, sPHYHospitalNodeOrder);
            }
        }
        //SIW7620 ends

        public string TechkeyPhyPrevHosp
        {
            get
            {
                return Utils.getData(PHYHospitalNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putPhyHospitalNode, Constants.sStdTechKey, value, sPHYHospitalNodeOrder);
            }
        }
        public int PHYHospCount
        {
            get
            {
                return PHYHospList.Count;
            }
        }
        public XmlNodeList PHYHospList
        {
            get
            {

                if (null == xmlPHYHospitalList)
                {
                    return getNodeList(Constants.sPhyPrevHospital, ref xmlPHYHospitalList, Node);
                }
                else
                {
                    return xmlPHYHospitalList;
                }
            }
        }

        public XmlNode getPHYPrevHospital(bool reset)
        {
            if (null != PHYHospList)
            {
                return getNode(reset, ref xmlPHYHospitalList, ref xmlPHYHospital);
            }
            else
            {
                return null;
            }
        }
        public XmlNode addPHYHospital()
        {
            PHYHospitalNode = null;
            return PHYHospitalNode;
        }

        public XmlNode PHYHospitalNode
        {
            get
            {
                return xmlPHYHospital;
            }
            set
            {
                xmlPHYHospital = value;
            }
        }

        public XmlNode putPhyHospitalNode
        {
            get
            {
                if (PHYHospitalNode == null)
                    PHYHospitalNode = XML.XMLaddNode(Node, Constants.sPhyPrevHospital, sThisNodeOrder);
                return PHYHospitalNode;
            }
        }

        public XmlNode putPhyHospital(string sID, string sEntID, string stPhyHospStatusDesc, string stPhyHospStatuCode, string stPhyHospStatuCodeID, string stPhyHospEIDDesc, string stPhyHospEIDCode, string stPhyHospEIDCodeID,
                                string stPhyHospPrivDesc, string stPhyHospPrivCode, string stPhyHospPrivCodeId, string sPhyHospPrivINTDATE, string sPhyPrivENDDATE)
        {
            if (sID != null)
                PhyPrivID = sID;
            if (sEntID != "" && sEntID != null)
                PhyPrivEntID = sEntID;
            if ((stPhyHospStatusDesc != "" && stPhyHospStatusDesc != null) || (stPhyHospStatuCode != "" && stPhyHospStatuCode != null) || (stPhyHospStatuCodeID != "" && stPhyHospStatuCodeID != null))
                putPHYHOSPSTATUSCODE(stPhyHospStatusDesc, stPhyHospStatuCode, stPhyHospStatuCodeID);
            if ((stPhyHospEIDDesc != "" && stPhyHospEIDDesc != null) || (stPhyHospEIDCode != "" && stPhyHospEIDCode != null) || (stPhyHospEIDCodeID != "" && stPhyHospEIDCodeID != null))
                putPHYHOSPITALEID(stPhyHospEIDDesc, stPhyHospEIDCode, stPhyHospEIDCodeID);
            if ((stPhyHospPrivDesc != "" && stPhyHospPrivDesc != null) || (stPhyHospPrivCode != "" && stPhyHospPrivCode != null) || (stPhyHospPrivCodeId != "" && stPhyHospPrivCodeId != null))
                putPHYHOSPPRIVCODE(stPhyHospPrivDesc, stPhyHospPrivCode, stPhyHospPrivCodeId);

            if (sPhyHospPrivINTDATE != null)
                PhyPrivStartDate = sPhyHospPrivINTDATE;
            if (sPhyPrivENDDATE != null)
                PhyPrivEndDate = sPhyPrivENDDATE;

            return PHYHospitalNode;
        }

        public XmlNode putPHYHOSPSTATUSCODE(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putPhyHospitalNode, Constants.sHospStatusCode, sDesc, sCode, sPHYHospitalNodeOrder, stypecodeid);
        }
        public XmlNode putPHYHOSPITALEID(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putPhyHospitalNode, Constants.sHospEntID, sDesc, sCode, sPHYHospitalNodeOrder, stypecodeid);
        }
        public XmlNode putPHYHOSPPRIVCODE(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putPhyHospitalNode, Constants.sHospPrivCode, sDesc, sCode, sPHYHospitalNodeOrder, stypecodeid);
        }

        public string PhyPrevHospID
        {
            get
            {
                return Utils.getData(PHYHospitalNode, Constants.sPrevHospID);
            }
            set
            {
                Utils.putData(putPhyHospitalNode, Constants.sPrevHospID, value, sPHYHospitalNodeOrder);
            }
        }
        
        public XmlNode putPrevHospStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPhyHospitalNode, Constants.sHospStatusCode, sDesc, sCode, sPHYHospitalNodeOrder, scodeid);
        }
        public string PrevHospStatus
        {
            get
            {
                return Utils.getData(PHYHospitalNode, Constants.sHospStatusCode);
            }
            set
            {
                Utils.putData(putPhyHospitalNode, Constants.sHospStatusCode, value, sPHYHospitalNodeOrder);
            }
        }
        public string PrevHospStatus_Code
        {
            get
            {
                return Utils.getCode(PHYHospitalNode, Constants.sHospStatusCode);
            }
            set
            {
                Utils.putCode(putPhyHospitalNode, Constants.sHospStatusCode, value, sPHYHospitalNodeOrder);
            }
        }
        public string PrevHospStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYHospitalNode, Constants.sHospStatusCode);
            }
            set
            {
                Utils.putCodeID(putPhyHospitalNode, Constants.sHospStatusCode, value, sPHYHospitalNodeOrder);
            }
        }
        public string PhyHospEntID
        {
            get
            {
                return Utils.getData(PHYHospitalNode, Constants.sHospEntID);
            }
            set
            {
                Utils.putData(putPhyHospitalNode, Constants.sHospEntID, value, sPHYHospitalNodeOrder);
            }
        }
        public string HospitalID
        {
            get
            {
                return Utils.getAttribute(PHYHospitalNode, Constants.sHospEntID,Constants.sHospID);
            }
            set
            {
                Utils.putAttribute(putPhyHospitalNode,Constants.sHospEntID, Constants.sHospID, value, sPHYHospitalNodeOrder);
            }
        }
       
        public XmlNode putPrevHospPriv(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putPhyHospitalNode, Constants.sHospPrivCode, sDesc, sCode, sPHYHospitalNodeOrder, scodeid);
        }
        public string PrevHospPriv
        {
            get
            {
                return Utils.getData(PHYHospitalNode, Constants.sHospPrivCode);
            }
            set
            {
                Utils.putData(putPhyHospitalNode, Constants.sHospPrivCode, value, sPHYHospitalNodeOrder);
            }
        }
        public string PrevHospPriv_Code
        {
            get
            {
                return Utils.getCode(PHYHospitalNode, Constants.sHospPrivCode);
            }
            set
            {
                Utils.putCode(putPhyHospitalNode, Constants.sHospPrivCode, value, sPHYHospitalNodeOrder);
            }
        }
        public string PrevHospPriv_CodeID
        {
            get
            {
                return Utils.getCodeID(PHYHospitalNode, Constants.sHospPrivCode);
            }
            set
            {
                Utils.putCodeID(putPhyHospitalNode, Constants.sHospPrivCode, value, sPHYHospitalNodeOrder);
            }
        }
        public string PrevHospIntDate
        {
            get
            {
                return Utils.getDate(PHYHospitalNode, Constants.sHospIntDate);
            }
            set
            {
                Utils.putDate(putPhyHospitalNode, Constants.sHospIntDate, value, sPHYHospitalNodeOrder);
            }
        }
        public string PrevHospEndDate
        {
            get
            {
                return Utils.getDate(PHYHospitalNode, Constants.sHospEndDate);
            }
            set
            {
                Utils.putDate(putPhyHospitalNode, Constants.sHospEndDate, value, sPHYHospitalNodeOrder);
            }
        }

        //MedStaff -> Privilage-------------------------------------------------------
        //SIW7620 == starts
        public string MedPrivDelete
        {
            get
            {
                return Utils.getBool(MEDPrivilageNode, Constants.sAHDeleteFlag, "");
            }
            set
            {
                Utils.putBool(putMEDPrivilageNode, Constants.sAHDeleteFlag, "", value, sMEDPrivilageNodeOrder);
            }
        }
        //SIW7620 ends

        public string TechkeyMedStaffPriv
        {
            get
            {
                return Utils.getData(MEDPrivilageNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putMEDPrivilageNode, Constants.sStdTechKey, value, sMEDPrivilageNodeOrder);
            }
        }
        public int MEDPrivilageCount
        {
            get
            {
                return MEDPrivilageList.Count;
            }
        }

        public XmlNodeList MEDPrivilageList
        {
            get
            {

                if (null == xmlMEDPrivilageList)
                {
                    return getNodeList(Constants.sEntMEDPRIVILEGE, ref xmlMEDPrivilageList, Node);
                }
                else
                {
                    return xmlMEDPrivilageList;
                }
            }
        }

        public XmlNode getMEDPrivilage(bool reset)
        {
            if (null != MEDPrivilageList)
            {
                return getNode(reset, ref xmlMEDPrivilageList, ref xmlMEDPrivilage);
            }
            else
            {
                return null;
            }
        }

        public XmlNode addMEDPrivilage()
        {
            xmlMEDPrivilage = null;
            return putMEDPrivilageNode;
        }

        public XmlNode MEDPrivilageNode
        {
            get
            {
                return xmlMEDPrivilage;
            }
            set
            {
                xmlMEDPrivilage = value;
            }
        }

        public XmlNode putMEDPrivilageNode
        {
            get
            {
                if (MEDPrivilageNode == null)
                    MEDPrivilageNode = XML.XMLaddNode(Node, Constants.sEntMEDPRIVILEGE, sThisNodeOrder);
                return MEDPrivilageNode;
            }
        }

        public XmlNode putMEDPrivilage(string sID, string sEntID, string stMeDCatDesc, string stMeDCatCode, string stMeDCatCodeID, string stMeDTypeDesc, string stMeDTypeCode, string stMeDTypeCodeID,
                                 string stMeDStatusDesc, string stMeDStatusCode, string stMeDStatusCodeID, string sMEDPrivINTDATE, string sMEDPrivENDDATE)
        {
            if (sID != null)
                MedPrivID = sID;
            if (sEntID != "" && sEntID != null)
                MedPrivEntID = sEntID;
            if ((stMeDCatDesc != "" && stMeDCatDesc != null) || (stMeDCatCode != "" && stMeDCatCode != null) || (stMeDCatCodeID != "" && stMeDCatCodeID != null))
                putMEDCATEGORYCODE(stMeDCatDesc, stMeDCatCode, stMeDCatCodeID);
            if ((stMeDTypeDesc != "" && stMeDTypeDesc != null) || (stMeDTypeCode != "" && stMeDTypeCode != null) || (stMeDTypeCodeID != "" && stMeDTypeCodeID != null))
                putMEDTYPECODE(stMeDTypeDesc, stMeDTypeCode, stMeDTypeCodeID);
            if ((stMeDStatusDesc != "" && stMeDStatusDesc != null) || (stMeDStatusCode != "" && stMeDStatusCode != null) || (stMeDStatusCodeID != "" && stMeDStatusCodeID != null))
                putMEDStatusCODE(stMeDStatusDesc, stMeDStatusCode, stMeDStatusCodeID);

            if (sMEDPrivINTDATE != null)
                MedPrivStartDate = sMEDPrivINTDATE;
            if (sMEDPrivENDDATE != null)
                MedPrivEndDate = sMEDPrivENDDATE;

            return MEDPrivilageNode;
        }

        public XmlNode putMEDCATEGORYCODE(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putMEDPrivilageNode, Constants.sEntMEDCATEGORYCODE, sDesc, sCode, sMEDPrivilageNodeOrder, stypecodeid);
        }
        public XmlNode putMEDTYPECODE(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putMEDPrivilageNode, Constants.sEntMEDTYPECODE, sDesc, sCode, sMEDPrivilageNodeOrder, stypecodeid);
        }
        public XmlNode putMEDStatusCODE(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putMEDPrivilageNode, Constants.sEntMEDSTATUSCODE, sDesc, sCode, sMEDPrivilageNodeOrder, stypecodeid);
        }

        public string MedPrivID
        {
            get
            {
                return Utils.getData(MEDPrivilageNode, Constants.sEntMEDPRIVID);
            }
            set
            {
                Utils.putData(putMEDPrivilageNode, Constants.sEntMEDPRIVID, value, sMEDPrivilageNodeOrder);
            }
        }

        public string MedPrivEntID
        {
            get
            {
                return Utils.getData(MEDPrivilageNode, Constants.sEntMEDID);
            }
            set
            {
                Utils.putData(putMEDPrivilageNode, Constants.sEntMEDID, value, sMEDPrivilageNodeOrder);
            }
        }
               
        public XmlNode putMedPrivCategory(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMEDPrivilageNode, Constants.sEntMEDCATEGORYCODE, sDesc, sCode, sMEDPrivilageNodeOrder, scodeid);
        }
        public string MedPrivCategory
        {
            get
            {
                return Utils.getData(MEDPrivilageNode, Constants.sEntMEDCATEGORYCODE);
            }
            set
            {
                Utils.putData(putMEDPrivilageNode, Constants.sEntMEDCATEGORYCODE, value, sMEDPrivilageNodeOrder);
            }
        }
        public string MedPrivCategory_Code
        {
            get
            {
                return Utils.getCode(MEDPrivilageNode, Constants.sEntMEDCATEGORYCODE);
            }
            set
            {
                Utils.putCode(putMEDPrivilageNode, Constants.sEntMEDCATEGORYCODE, value, sMEDPrivilageNodeOrder);
            }
        }
        public string MedPrivCategory_CodeID
        {
            get
            {
                return Utils.getCodeID(MEDPrivilageNode, Constants.sEntMEDCATEGORYCODE);
            }
            set
            {
                Utils.putCodeID(putMEDPrivilageNode, Constants.sEntMEDCATEGORYCODE, value, sMEDPrivilageNodeOrder);
            }
        }
        
        public XmlNode putMedPrivType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMEDPrivilageNode, Constants.sEntMEDTYPECODE, sDesc, sCode, sMEDPrivilageNodeOrder, scodeid);
        }
        public string MedPrivType
        {
            get
            {
                return Utils.getData(MEDPrivilageNode, Constants.sEntMEDTYPECODE);
            }
            set
            {
                Utils.putData(putMEDPrivilageNode, Constants.sEntMEDTYPECODE, value, sMEDPrivilageNodeOrder);
            }
        }
        public string MedPrivType_Code
        {
            get
            {
                return Utils.getCode(MEDPrivilageNode, Constants.sEntMEDTYPECODE);
            }
            set
            {
                Utils.putCode(putMEDPrivilageNode, Constants.sEntMEDTYPECODE, value, sMEDPrivilageNodeOrder);
            }
        }
        public string MedPrivType_CodeID
        {
            get
            {
                return Utils.getCodeID(MEDPrivilageNode, Constants.sEntMEDTYPECODE);
            }
            set
            {
                Utils.putCodeID(putMEDPrivilageNode, Constants.sEntMEDTYPECODE, value, sMEDPrivilageNodeOrder);
            }
        }
        
        public XmlNode putMedPrivStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMEDPrivilageNode, Constants.sEntMEDSTATUSCODE, sDesc, sCode, sMEDPrivilageNodeOrder, scodeid);
        }
        public string MedPrivStatus
        {
            get
            {
                return Utils.getData(MEDPrivilageNode, Constants.sEntMEDSTATUSCODE);
            }
            set
            {
                Utils.putData(putMEDPrivilageNode, Constants.sEntMEDSTATUSCODE, value, sMEDPrivilageNodeOrder);
            }
        }
        public string MedPrivStatus_Code
        {
            get
            {
                return Utils.getCode(MEDPrivilageNode, Constants.sEntMEDSTATUSCODE);
            }
            set
            {
                Utils.putCode(putMEDPrivilageNode, Constants.sEntMEDSTATUSCODE, value, sMEDPrivilageNodeOrder);
            }
        }
        public string MedPrivStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(MEDPrivilageNode, Constants.sEntMEDSTATUSCODE);
            }
            set
            {
                Utils.putCodeID(putMEDPrivilageNode, Constants.sEntMEDSTATUSCODE, value, sMEDPrivilageNodeOrder);
            }
        }
        public string MedPrivStartDate
        {
            get
            {
                return Utils.getDate(MEDPrivilageNode, Constants.sEntMEDINTDATE);
            }
            set
            {
                Utils.putDate(putMEDPrivilageNode, Constants.sEntMEDINTDATE, value, sMEDPrivilageNodeOrder);
            }
        }
        public string MedPrivEndDate
        {
            get
            {
                return Utils.getDate(MEDPrivilageNode, Constants.sEntMEDENDDATE);
            }
            set
            {
                Utils.putDate(putMEDPrivilageNode, Constants.sEntMEDENDDATE, value, sMEDPrivilageNodeOrder);
            }
        }


        //MedStaff -Certification -------------------------------
        //SIW7620 == starts
        public string MedCertDelete
        {
            get
            {
                return Utils.getBool(MEDCertNode, Constants.sAHDeleteFlag, "");
            }
            set
            {
                Utils.putBool(putMEDCerteNode, Constants.sAHDeleteFlag, "", value, sMEDCertificationNodeOrder);
            }
        }
        //SIW7620 ends

        public string TechkeyMedStaffCert
        {
            get
            {
                return Utils.getData(MEDCertNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putMEDCerteNode, Constants.sStdTechKey, value, sMEDCertificationNodeOrder);
            }
        }
        public int MEDCertCount
        {
            get
            {
                return MEDCertList.Count;
            }
        }

        public XmlNodeList MEDCertList
        {
            get
            {

                if (null == xmlMEDCertificationList)
                {
                    return getNodeList(Constants.sEntMEDCERTS, ref xmlMEDCertificationList, Node);
                }
                else
                {
                    return xmlMEDCertificationList;
                }
            }
        }
        public XmlNode getMEDCert(bool reset)
        {
            if (null != MEDCertList)
            {
                return getNode(reset, ref xmlMEDCertificationList, ref xmlMEDCertification);
            }
            else
            {
                return null;
            }
        }
        public XmlNode addMEDCert()
        {
            xmlMEDCertification = null;
            return MEDCertNode;
        }

        public XmlNode MEDCertNode
        {
            get
            {
                return xmlMEDCertification;
            }
            set
            {
                xmlMEDCertification = value;
            }
        }

        public XmlNode putMEDCerteNode
        {
            get
            {
                if (MEDCertNode == null)
                    MEDCertNode = XML.XMLaddNode(Node, Constants.sEntMEDCERTS, sThisNodeOrder);
                return MEDCertNode;
            }
        }

        public XmlNode putMEDCert(string sID, string sEntID, string stMeDCertNameDesc, string stMeDCertNameCode, string stMeDCertNameCodeID, string stMeDCertStatusDesc, string stMeDCertStatusCode, string stMeDCertStatusCodeID,
                                 string stMeDCertBroadDesc, string stMeDCertBroadCode, string stMeDCertBroadCodeID, string sMEDCERTINTDATE, string sMEDCERTENDDATE)
        {
            if (sID != null)
                MedCertID = sID;
            if (sEntID != "" && sEntID != null)
                MedCertEntID = sEntID;
            if ((stMeDCertNameDesc != "" && stMeDCertNameDesc != null) || (stMeDCertNameCode != "" && stMeDCertNameCode != null) || (stMeDCertNameCodeID != "" && stMeDCertNameCodeID != null))
                putMEDNAMECODE(stMeDCertNameDesc, stMeDCertNameCode, stMeDCertNameCodeID);
            if ((stMeDCertStatusDesc != "" && stMeDCertStatusDesc != null) || (stMeDCertStatusCode != "" && stMeDCertStatusCode != null) || (stMeDCertStatusCodeID != "" && stMeDCertStatusCodeID != null))
                putMEDCERTSTATUSCODE(stMeDCertStatusDesc, stMeDCertStatusCode, stMeDCertStatusCodeID);
            if ((stMeDCertBroadDesc != "" && stMeDCertBroadDesc != null) || (stMeDCertBroadCode != "" && stMeDCertBroadCode != null) || (stMeDCertBroadCodeID != "" && stMeDCertBroadCodeID != null))
                putMEDBOARDCODE(stMeDCertBroadDesc, stMeDCertBroadCode, stMeDCertBroadCodeID);
            if (sMEDCERTINTDATE != null)
                MedCertIntDate = sMEDCERTINTDATE;
            if (sMEDCERTENDDATE != null)
                MedCertEndDate = sMEDCERTENDDATE;
            
            return MEDCertNode;
        }

        public XmlNode putMEDNAMECODE(string sDesc, string sCode, string stypecodeid) 
        {
            return Utils.putCodeItem(putMEDCerteNode, Constants.sEntMEDNAMECODE, sDesc, sCode, sMEDCertificationNodeOrder, stypecodeid); 
        }
        public XmlNode putMEDCERTSTATUSCODE(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putMEDCerteNode, Constants.sEntMEDSTATUSCODE, sDesc, sCode, sMEDCertificationNodeOrder, stypecodeid);
        }
        public XmlNode putMEDBOARDCODE(string sDesc, string sCode, string stypecodeid)
        {
            return Utils.putCodeItem(putMEDCerteNode, Constants.sEntMEDBOARDCODE, sDesc, sCode, sMEDCertificationNodeOrder, stypecodeid);
        }



        public string MedCertID
        {
            get
            {
                return Utils.getData(MEDCertNode, Constants.sEntMEDCERTID);
            }
            set
            {
                Utils.putData(putMEDCerteNode, Constants.sEntMEDCERTID, value, sMEDCertificationNodeOrder);
            }
        }
        public string MedCertEntID
        {
            get
            {
                return Utils.getData(MEDCertNode, Constants.sEntMEDID);
            }
            set
            {
                Utils.putData(putMEDCerteNode, Constants.sEntMEDID, value, sMEDCertificationNodeOrder);
            }
        }
        
        public XmlNode putMedCertName(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMEDCerteNode, Constants.sEntMEDNAMECODE, sDesc, sCode, sMEDCertificationNodeOrder, scodeid);
        }
        public string MedCertName
        {
            get
            {
                return Utils.getData(MEDCertNode, Constants.sEntMEDNAMECODE);
            }
            set
            {
                Utils.putData(putMEDCerteNode, Constants.sEntMEDNAMECODE, value, sMEDCertificationNodeOrder);
            }
        }
        public string MedCertName_Code
        {
            get
            {
                return Utils.getCode(MEDCertNode, Constants.sEntMEDNAMECODE);
            }
            set
            {
                Utils.putCode(putMEDCerteNode, Constants.sEntMEDNAMECODE, value, sMEDCertificationNodeOrder);
            }
        }
        public string MedCertName_CodeID
        {
            get
            {
                return Utils.getCodeID(MEDCertNode, Constants.sEntMEDNAMECODE);
            }
            set
            {
                Utils.putCodeID(putMEDCerteNode, Constants.sEntMEDNAMECODE, value, sMEDCertificationNodeOrder);
            }
        }
        
        public XmlNode putMedCertStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMEDCerteNode, Constants.sEntMEDSTATUSCODE, sDesc, sCode, sMEDCertificationNodeOrder, scodeid);
        }
        public string MedCertStatus
        {
            get
            {
                return Utils.getData(MEDCertNode, Constants.sEntMEDSTATUSCODE);
            }
            set
            {
                Utils.putData(putMEDCerteNode, Constants.sEntMEDSTATUSCODE, value, sMEDCertificationNodeOrder);
            }
        }
        public string MedCertStatus_Code
        {
            get
            {
                return Utils.getCode(MEDCertNode, Constants.sEntMEDSTATUSCODE);
            }
            set
            {
                Utils.putCode(putMEDCerteNode, Constants.sEntMEDSTATUSCODE, value, sMEDCertificationNodeOrder);
            }
        }
        public string MedCertStatus_CodeID
        {
            get
            {
                return Utils.getCodeID(MEDCertNode, Constants.sEntMEDSTATUSCODE);
            }
            set
            {
                Utils.putCodeID(putMEDCerteNode, Constants.sEntMEDSTATUSCODE, value, sMEDCertificationNodeOrder);
            }
        }
        
        public XmlNode putMedCertBoard(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putMEDCerteNode, Constants.sEntMEDBOARDCODE, sDesc, sCode, sMEDCertificationNodeOrder, scodeid);
        }
        public string MedCertBoard
        {
            get
            {
                return Utils.getData(MEDCertNode, Constants.sEntMEDBOARDCODE);
            }
            set
            {
                Utils.putData(putMEDCerteNode, Constants.sEntMEDBOARDCODE, value, sMEDCertificationNodeOrder);
            }
        }
        public string MedCertBoard_Code
        {
            get
            {
                return Utils.getCode(MEDCertNode, Constants.sEntMEDBOARDCODE);
            }
            set
            {
                Utils.putCode(putMEDCerteNode, Constants.sEntMEDBOARDCODE, value, sMEDCertificationNodeOrder);
            }
        }
        public string MedCertBoard_CodeID
        {
            get
            {
                return Utils.getCodeID(MEDCertNode, Constants.sEntMEDBOARDCODE);
            }
            set
            {
                Utils.putCodeID(putMEDCerteNode, Constants.sEntMEDBOARDCODE, value, sMEDCertificationNodeOrder);
            }
        }
        public string MedCertIntDate
        {
            get
            {
                return Utils.getDate(MEDCertNode, Constants.sEntMEDINTDATE);
            }
            set
            {
                Utils.putDate(putMEDCerteNode, Constants.sEntMEDINTDATE, value, sMEDCertificationNodeOrder);
            }
        }
        public string MedCertEndDate
        {
            get
            {
                return Utils.getDate(MEDCertNode, Constants.sEntMEDENDDATE);
            }
            set
            {
                Utils.putDate(putMEDCerteNode, Constants.sEntMEDENDDATE, value, sMEDCertificationNodeOrder);
            }
        }

        // End SIW7620

        //Start SIW8067:  Revised to match .COM definitions.
        //***************************************************************************************************
        // Entity IDNumber Processing
        //***************************************************************************************************
        public ArrayList IDNbrNodeOrder
        {
            get
            {
                return sIDNbrNodeOrder;
            }
        }

        public int IDNumberCount
        {
            get
            {
                return IDNumberList.Count;
            }
        }

        public XmlNodeList IDNumberList
        {
            get
            {
                //Start SIW163
                if (null == xmlIDNList)
                {
                    return getNodeList(Constants.sEntIDNumber, ref xmlIDNList, Node);
                }
                else
                {
                    return xmlIDNList;
                }
                //End SIW163
            }
        }

        public XmlNode getIDNumber(bool reset)
        {
            //Start SIW163
            if (null != IDNumberList)
            {
                return getNode(reset, ref xmlIDNList, ref xmlIDN);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public XmlNode putIDNNode
        {
            get
            {
                if (IDNNode == null)
                    IDNNode = XML.XMLaddNode(Node, Constants.sEntIDNumber, sThisNodeOrder);
                return IDNNode;
            }
        }

        public XmlNode IDNNode
        {
            get
            {
                return xmlIDN;
            }
            set
            {
                xmlIDN = value;
            }
        }

        public XmlNode findIDNumberByType(string idtype)
        {
            if (IDNumberType != idtype)
            {
                getIDNumber(true);
                while (IDNNode != null)
                {
                    if (IDNumberType == idtype)
                        break;
                    getIDNumber(false);
                }
            }
            return IDNNode;
        }

        public XmlNode findIDNumber(string idnum, string idtype)
        {
            getIDNumber(true);
            while (IDNNode != null)
            {
                if (IDNumber == idnum && IDNumberType == idtype)
                    break;
                getIDNumber(false);
            }
            return IDNNode;
        }

        public void removeIDNumber()
        {
            if (IDNNode != null)
                Node.RemoveChild(IDNNode);
            IDNNode = null;
        }

        private void CheckIDN()
        {
            if ((IDNumber == "" || IDNumber == null) && (IDNumberType == "" || IDNumberType == null) && (IDNumberTypeCodeID == "" || IDNumberTypeCodeID == null))
                removeIDNumber();
        }

        // Start SIW8067 - IDNumberType is updated as code field
        //public string IDNumberType
        //{
        //    get
        //    {
        //        //return Utils.getType(IDNNode, ""); //SIN7587
        //        return Utils.getData(IDNNode,Constants.sEntIDNumberType); //SIN7587 
        //    }
        //    set
        //    {
        //        //Utils.putType(putIDNNode, "", value, NodeOrder); //SIN7587
        //        Utils.putData(putIDNNode, Constants.sEntIDNumberType, value, NodeOrder); //SIN7587
        //    }
        //}
        public XmlNode putIDNumberType(string sDesc, string sCode)
        {
            return putIDNumberType(sDesc, sCode, "");
        }

        public XmlNode putIDNumberType(string sDesc, string sCode, string sCodeID)
        {
            return Utils.putCodeItem(putIDNNode, Constants.sEntIDNType, sDesc, sCode, IDNbrNodeOrder, sCodeID);   
        }

        public string IDNumberTypeDesc
        {
            get
            {
                return Utils.getData(IDNNode, Constants.sEntIDNType);   
            }
            set
            {
                Utils.putData(putIDNNode, Constants.sEntIDNType, value, IDNbrNodeOrder); 
            }
        }

        public string IDNumberType
        {
            get
            {
                return Utils.getCode(IDNNode, Constants.sEntIDNType);   
            }
            set
            {
                Utils.putCode(putIDNNode, Constants.sEntIDNType, value, IDNbrNodeOrder);   
            }
        }

        public string IDNumberTypeCodeID
        {
            get
            {
                return Utils.getCodeID(IDNNode, Constants.sEntIDNType);   
            }
            set
            {
                Utils.putCodeID(putIDNNode, Constants.sEntIDNType, value, IDNbrNodeOrder);   
            }
        }
        public string IDNumberEffStartDate
        {
            get
            {
                return Utils.getDate(IDNNode, Constants.sEntIDNumberEffStartDate);
            }
            set
            {
                Utils.putDate(putIDNNode, Constants.sEntIDNumberEffStartDate, value, IDNbrNodeOrder);
            }
        }
        public string IDNumberEffEndDate
        {
            get
            {
                return Utils.getDate(IDNNode, Constants.sEntIDNumberEffEndDate);
            }
            set
            {
                Utils.putDate(putIDNNode, Constants.sEntIDNumberEffEndDate, value, IDNbrNodeOrder);
            }
        }
        
        public string IDNumberTechKey
        {
            get
            {
                return Utils.getData(IDNNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putIDNNode, Constants.sStdTechKey, value, IDNbrNodeOrder);
            }
        }
        public string IDNumberID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(IDNNode, Constants.sEntIDNumberID);
                return strdata;
            }
            set
            {
                string EidNumberID = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lEntIDNId = Globals.lEntIDNId + 1;
                    EidNumberID = Globals.lEntIDNId + "";
                }
                XML.XMLSetAttributeValue(putIDNNode, Constants.sEntIDNumberID, Constants.sEntIDNIDPfx + EidNumberID);
            }
        }
        public string IDNumber
        {
            get
            {
                //return Utils.getData(IDNNode, ""); //SIN7587
                return Utils.getData(IDNNode, Constants.sEntIDNumberNbr);
            }
            set
            {
                //Utils.putData(putIDNNode, "", value, NodeOrder); //SIN7587
                Utils.putData(putIDNNode, Constants.sEntIDNumberNbr, value, IDNbrNodeOrder);
            }
        }

        //public string IDNumberRowID 
        //{
        //    get
        //    {
        //        return Utils.getData(IDNNode, Constants.sEntIDNumberRowID);
        //    }
        //    set
        //    {
        //        Utils.putData(putIDNNode, Constants.sEntIDNumberRowID, value, NodeOrder);
        //    }
        //}        
        //End SIW8067
        public XmlNode addIDNumber(string sidn, string sCode)
        {
            addIDNumber(sidn, sCode, "", "", "");
            return IDNNode;
        }

        public XmlNode addIDNumber(string sidn, string sCode, string sID)
        {
            addIDNumber(sidn, sCode, sID, "", "");
            return IDNNode;
        }

        public XmlNode addIDNumber(string sidn, string sCode, string sID, string sEffStartDate, string sEffEndDate)
        {
            findIDNumber(sidn, sCode);
            putIDNumber(sidn, sCode,"","","",sEffStartDate,sEffEndDate,sID);
            return IDNNode;
        }

        public XmlNode putIDNumber(string sIDNumber, string sType, string sTypeDesc, string sTypeCodeID, 
                                   string sTechKey, string sEffStartDate, string sEffEndDate, string sID)
        {
            putIDNumberType(sTypeDesc, sType, sTypeCodeID);
            if (sIDNumber != "" && sIDNumber != null)
                IDNumber = sIDNumber;
            if (sTechKey != "" && sTechKey != null)
                IDNumberTechKey = sTechKey;
            if (sEffStartDate != "" && sEffStartDate != null)
                IDNumberEffStartDate = sEffStartDate;
            if (sEffEndDate != "" && sEffEndDate != null)
                IDNumberEffEndDate = sEffEndDate;
            if (sID != "" && sID != null)
                IDNumberID = sID;
            CheckIDN();
            return IDNNode;
        }

        public XmlNode putIDNumber(string sIDNumber, string sType)
        {
            putIDNumber(sIDNumber, sType, "", "", "", "", "", "");
            return IDNNode;
        }

// End ID Number Processing
//**************************************************************************************************

        public int CategoryCount
        {
            get
            {
                return CategoryList.Count;
            }
        }

        public XmlNodeList CategoryList
        {
            get
            {
                //Start SIW163
                if (null == xmlCategoryList)
                {
                    return getNodeList(Constants.sEntCategory, ref xmlCategoryList, Node);
                }
                else
                {
                    return xmlCategoryList;
                }
                //End SIW163
            }
        }

        public XmlNode getCategory(bool reset)
        {
            //Start SIW163
            if (null != CategoryList)
            {
                return getNode(reset, ref xmlCategoryList, ref xmlCategory);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public XmlNode findCategory(string sCategory, string sCategoryCode)
        {
            getCategory(true);
            while (CategoryNode != null)
            {
                if (Category == sCategory && CategoryCode == sCategoryCode)
                    break;
                getCategory(false);
            }
            return CategoryNode;
        }

        private void CheckCategory()
        {
            if ((Category == "" || Category == null) && (CategoryCode == "" || CategoryCode == null))
                removeCategory();
        }

        public XmlNode putCategoryNode
        {
            get
            {
                if (CategoryNode == null)
                    CategoryNode = XML.XMLaddNode(Node, Constants.sEntCategory, sThisNodeOrder);
                return CategoryNode;
            }
        }

        public XmlNode CategoryNode
        {
            get
            {
                return xmlCategory;
            }
            set
            {
                xmlCategory = value;
            }
        }

        public XmlNode addCategory(string sCategory, string sCategoryCode)
        {
            if (findCategory(sCategory, sCategoryCode) == null)
                putCategory(sCategory, sCategoryCode);
            return CategoryNode;
        }

        public void removeCategory()
        {
            if (CategoryNode != null)
                Node.RemoveChild(CategoryNode);
            CategoryNode = null;
        }
        //Start SI06468
        public XmlNode putCategory(string sCategory, string sCategoryCode)
        {
            return putCategory(sCategory, sCategoryCode, "");
        }
        //End SI06468
        //public XmlNode putCategory(string sCategory, string sCategoryCode)  //SI06468
        public XmlNode putCategory(string sCategory, string sCategoryCode, string sCategoryCodeID) //SI06468
        {
            if (sCategory != "" && sCategory != null)
                Category = sCategory;
            if (sCategoryCode != "" && sCategoryCode != null)
                CategoryCode = sCategoryCode;
            //Start SI06468
            if (sCategoryCodeID != "" && sCategoryCodeID != null)
                CategoryCodeID = sCategoryCodeID;
            //End SI06468
            CheckCategory();
            return CategoryNode;
        }

        public string CategoryCode
        {
            get
            {
                return Utils.getCode(CategoryNode, "");
            }
            set
            {
                Utils.putCode(putCategoryNode, "", value, NodeOrder);
            }
        }
        //Start SI06468
        public string CategoryCodeID
        {
            get
            {
                return Utils.getCodeID(CategoryNode, "");
            }
            set
            {
                Utils.putCodeID(putCategoryNode, "", value, NodeOrder);
            }
        }
        //End SI06468
        public string Category
        {
            get
            {
                return Utils.getData(CategoryNode, "");
            }
            set
            {
                Utils.putData(putCategoryNode, "", value, NodeOrder);
            }
        }

        public void removeBusinessData()
        {
            if (BusinessData_Node != null)
                Node.RemoveChild(BusinessData_Node);
        }

        public void putBusinessData(string ssic, string snaic, string sunempinsacct, string snatofbus,
                                    string snatofbuscode, string stypeofbus, string stypeofbuscode)
        {
            if (ssic != "" && ssic != null)
                SIC = ssic;
            if (snaic != "" && snaic != null)
                NAIC = snaic;
            if (sunempinsacct != "" && sunempinsacct != null)
                UnempInsAcct = sunempinsacct;
            if ((snatofbus != "" && snatofbus != null) || (snatofbuscode != "" && snatofbuscode != null))
                putNatureofBusiness(snatofbus, snatofbuscode);
            if ((stypeofbus != "" && stypeofbus != null) || (stypeofbuscode != "" && stypeofbuscode != null))
                putTypeOfBusiness(stypeofbus, stypeofbuscode);
        }

        public string SIC
        {
            get
            {
                return Utils.getData(BusinessData_Node, Constants.sEntSIC);
            }
            set
            {
                Utils.putData(putBusinessData_Node, Constants.sEntSIC, value, sBusDataNodeOrder);
            }
        }
        //Start SI06468
        public void putTypeOfBusiness(string stob, string stobcode)
        {
            putTypeOfBusiness(stob, stobcode, "");
        }
        //End SI06468
        //public void putTypeOfBusiness(string stob, string stobcode) //SI06468
        public void putTypeOfBusiness(string stob, string stobcode, string stobcodeid) //SI06468
        {
            TypeOfBusiness = stob;
            TypeOfBusiness_Code = stobcode;
            TypeOfBusiness_CodeID = stobcodeid;  //SI06468
        }


        public string TypeOfBusiness_Code
        {
            get
            {
                return Utils.getCode(BusinessData_Node, Constants.sEntBusType);
            }
            set
            {
                Utils.putCode(putBusinessData_Node, Constants.sEntBusType, value, sBusDataNodeOrder);
            }
        }
        //Start SI06468
        public string TypeOfBusiness_CodeID
        {
            get
            {
                return Utils.getCodeID(BusinessData_Node, Constants.sEntBusType);
            }
            set
            {
                Utils.putCodeID(putBusinessData_Node, Constants.sEntBusType, value, sBusDataNodeOrder);
            }
        }
        //End SI06468
        public string TypeOfBusiness
        {
            get
            {
                return Utils.getData(BusinessData_Node, Constants.sEntBusType);
            }
            set
            {
                Utils.putData(putBusinessData_Node, Constants.sEntBusType, value, sBusDataNodeOrder);
            }
        }

        public string ContactName
        {
            get
            {
                return Utils.getData(BusinessData_Node, Constants.sEntBusContactName);
            }
            set
            {
                Utils.putData(putBusinessData_Node, Constants.sEntBusContactName, value, sBusDataNodeOrder);
            }
        }

        public string NAIC
        {
            get
            {
                return Utils.getData(BusinessData_Node, Constants.sEntNAIC);
            }
            set
            {
                Utils.putData(putBusinessData_Node, Constants.sEntNAIC, value, sBusDataNodeOrder);
            }
        }

        public void putNatureofBusiness(string snob, string snobcode)
        {
            NatureofBusiness = snob;
            NatureofBusiness_Code = snobcode;
        }

        public string NatureofBusiness
        {
            get
            {
                return Utils.getData(BusinessData_Node, Constants.sEntNatofBus);
            }
            set
            {
                Utils.putData(putBusinessData_Node, Constants.sEntNatofBus, value, sBusDataNodeOrder);
            }
        }

        public string NatureofBusiness_Code
        {
            get
            {
                return Utils.getCode(BusinessData_Node, Constants.sEntNatofBus);
            }
            set
            {
                Utils.putCode(putBusinessData_Node, Constants.sEntNatofBus, value, sBusDataNodeOrder);
            }
        }

        public string UnempInsAcct
        {
            get
            {
                return Utils.getData(BusinessData_Node, Constants.sEntUnempInsAcct);
            }
            set
            {
                Utils.putData(putBusinessData_Node, Constants.sEntUnempInsAcct, value, sBusDataNodeOrder);
            }
        }

        public string WCFilingNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sEntWCFilingNum);
            }
            set
            {
                //SIW485 Utils.putData(putNode, Constants.sEntWCFilingNum, value, Globals.sNodeOrder);
                Utils.putData(putNode, Constants.sEntWCFilingNum, value, Utils.sNodeOrder);	//SIW485
            }
        }

        public void removeBusinessName()
        {
            if (BusName_Node != null)
                Name_Node.RemoveChild(BusName_Node);
        }

        public void putBusinessName(string slglname, string sdbaname, string sbusname)
        {
            if (slglname != "" && slglname != null)
                LegalName = slglname;
            if (sdbaname != "" && sdbaname != null)
                DBAName = sdbaname;
            if (sbusname != "" && sbusname != null)
                BusinessName = sbusname;
        }

        public string LegalName
        {
            get
            {
                return Utils.getData(BusName_Node, Constants.sEntLglName);
            }
            set
            {
                Utils.putData(putBusName_Node, Constants.sEntLglName, value, sBusNameNodeOrder);
            }
        }

        public string BusinessName
        {
            get
            {
                return Utils.getData(BusName_Node, Constants.sEntBusinessName);
            }
            set
            {
                Utils.putData(putBusName_Node, Constants.sEntBusinessName, value, sBusNameNodeOrder);
            }
        }

        public string DBAName
        {
            get
            {
                return Utils.getData(BusName_Node, Constants.sEntDBAName);
            }
            set
            {
                Utils.putData(putBusName_Node, Constants.sEntDBAName, value, sBusNameNodeOrder);
            }
        }

        public string Abbreviation
        {
            get
            {
                return Utils.getData(BusName_Node, Constants.sEntAbbrev);
            }
            set
            {
                Utils.putData(putBusName_Node, Constants.sEntAbbrev, value, sBusNameNodeOrder);
            }
        }

        public void removeIndividualName()
        {
            if (IndName_Node != null)
                Name_Node.RemoveChild(IndName_Node);
        }

        public void putIndividualName(string spfx, string sfname, string smname, string slname,
                                      string ssfx, string sinitials, string saka)
        {
            if (spfx != "" && spfx != null)
                Prefix = spfx;
            if (sfname != "" && sfname != null)
                FirstName = sfname;
            if (smname != "" && smname != null)
                MiddleName = smname;
            if (slname != "" && slname != null)
                LastName = slname;
            if (ssfx != "" && ssfx != null)
                Suffix = ssfx;
            if (sinitials != "" && sinitials != null)
                Initials = sinitials;
            if (saka != "" && saka != null)
                AKA = saka;
        }

        public string Name
        {
            get
            {
                if (EntityType == ((int)entType.entTypeInd + ""))
                    return (Prefix + " " + FirstName + " " + MiddleName + " " + LastName + " " + Suffix).Trim();
                else
                    if (LegalName != "")
                        return LegalName;
                    else
                        return BusinessName;
            }
        }
        //Start SIN8440
        public void putPrefix(string sPrefix, string sPrefixCode, string sPrefixCodeID) //SI06468
        {
            Prefix = sPrefix;
            Prefix_Code = sPrefix;
            Prefix_CodeID = sPrefixCodeID;
        }
        //End SIN8440
        public string Prefix
        {
            get
            {
                return Utils.getData(IndName_Node, Constants.sEntPrefix);
            }
            set
            {
                Utils.putData(putIndName_Node, Constants.sEntPrefix, value, sIndNameNodeOrder);
            }
        }
        //Start SIN8440

        public string Prefix_Code
        {
            get
            {
                return Utils.getCode(IndName_Node, Constants.sEntPrefix);
            }
            set
            {
                Utils.putCode(putIndName_Node, Constants.sEntPrefix, value, sIndNameNodeOrder);
            }
        }

       
        public string Prefix_CodeID
        {
            get
            {
                return Utils.getCodeID(IndName_Node, Constants.sEntPrefix);
            }
            set
            {
                Utils.putCodeID(putIndName_Node, Constants.sEntPrefix, value, sIndNameNodeOrder);
            }
        }
        //End SIN8440

        public string FirstName
        {
            get
            {
                return Utils.getData(IndName_Node, Constants.sEntFName);
            }
            set
            {
                Utils.putData(putIndName_Node, Constants.sEntFName, value, sIndNameNodeOrder);
            }
        }

        public string MiddleName
        {
            get
            {
                return Utils.getData(IndName_Node, Constants.sEntMName);
            }
            set
            {
                Utils.putData(putIndName_Node, Constants.sEntMName, value, sIndNameNodeOrder);
            }
        }

        public string LastName
        {
            get
            {
                return Utils.getData(IndName_Node, Constants.sEntLName);
            }
            set
            {
                Utils.putData(putIndName_Node, Constants.sEntLName, value, sIndNameNodeOrder);
            }
        }

     
        
        //Start SIN8440
        public void putSuffix(string sSuffix, string sSuffixCode, string sSuffixCodeID) 
        {
            Suffix = sSuffix;
            Suffix_Code = sSuffix;
            Suffix_CodeID = sSuffixCodeID;
        }
        //End SIN8440
        public string Suffix
        {
            get
            {
                return Utils.getData(IndName_Node, Constants.sEntSuffix);
            }
            set
            {
                Utils.putData(putIndName_Node, Constants.sEntSuffix, value, sIndNameNodeOrder);
            }
        }
        //Start SIN8440

        public string Suffix_Code
        {
            get
            {
                return Utils.getCode(IndName_Node, Constants.sEntSuffix);
            }
            set
            {
                Utils.putCode(putIndName_Node, Constants.sEntSuffix, value, sIndNameNodeOrder);
            }
        }


        public string Suffix_CodeID
        {
            get
            {
                return Utils.getCodeID(IndName_Node, Constants.sEntSuffix);
            }
            set
            {
                Utils.putCodeID(putIndName_Node, Constants.sEntSuffix, value, sIndNameNodeOrder);
            }
        }
        //End SIN8440

        public string Initials
        {
            get
            {
                return Utils.getData(IndName_Node, Constants.sEntInitials);
            }
            set
            {
                Utils.putData(putIndName_Node, Constants.sEntInitials, value, sIndNameNodeOrder);
            }
        }

        public string AKA
        {
            get
            {
                return Utils.getData(IndName_Node, Constants.sEntAKA);
            }
            set
            {
                Utils.putData(putIndName_Node, Constants.sEntAKA, value, sIndNameNodeOrder);
            }
        }
        //SIW316 Start
        public string UserID
        {
            get
            {
                return Utils.getData(IndName_Node, Constants.sEntUuserId );
            }
            set
            {
                Utils.putData(putIndName_Node, Constants.sEntUuserId, value, sIndNameNodeOrder);
            }
        }
        //SIW316 Ends
        public string IDPrefix
        {
            get
            {
                return Constants.sCCPXmlEntityIDPrefix;
            }
        }

        public string EntityID
        {
            get
            {
                return Utils.getEntityRef(Node, "");
            }
            set
            {
                string strdata = value;
                if (strdata == "" || strdata == null || strdata == "0")
                    strdata = Parent.getNextID(null);
                Utils.putEntityRef(putNode, "", strdata, NodeOrder);
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            base.InsertInDocument(Doc, nde);
            Parent.Count = Parent.Count + 1;
        }

        public string EntityTypeTranslate(object otype)
        {
            object ret;
            if (dctEntType.TryGetValue(otype, out ret))
                return ret + "";
            else
                return "";
        }

        public string EntityType_Code
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sEntType);
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sEntType, value, NodeOrder);
            }
        }

        public string EntityType
        {
            get
            {
                object etype;
                string sdata = Utils.getAttribute(Node, "", Constants.sEntType);
                sdata = sdata.ToUpper(); //SIW12
                if (!dctEntType.TryGetValue(sdata, out etype))
                    etype = entType.entTypeUnd;
                return (int)etype + "";
            }
            set
            {
                object odata;
                int iType;
                string sdata = value;
                bool i = false;

                if (Int32.TryParse(value, out iType))
                {
                    i = true;
                    odata = iType;
                }
                else
                    odata = value.ToUpper();//SIW12

                if (dctEntType.ContainsKey(odata))
                {
                    if (i)
                    {
                        dctEntType.TryGetValue(iType, out odata);
                    }
                    sdata = (string)odata;
                    Utils.putAttribute(putNode, "", Constants.sEntType, sdata, sThisNodeOrder);
                }
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_INVALID_VALUE, "XMLEntity.EntityType",
                                     value + "", Document, Node);
            }
        }

        //vkumar258 Starts
        public string EntityRelationship
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sEntRelationship);
            }
            set
            {
                string sdata = value;
                Utils.putAttribute(putNode, "", Constants.sEntRelationship, sdata, sThisNodeOrder);
            }
        }
        //vkumar258 End


        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        protected override void ResetParent()
        {
            xmlThisNode = null;
            subClearPointers();
        }

        public new XMLEntities Parent
        {
            get
            {
                if (m_Entities == null)
                {
                    m_Entities = new XMLEntities();
                    m_Entities.Entity = this;
                    LinkXMLObjects(m_Entities); //SIW529
                    ResetParent();  //SIW529
                }
                return m_Entities;
            }
            set
            {
                m_Entities = value;
                ResetParent();
                if (m_Entities != null) //SIW529
                    m_Entities.LinkXMLObjects(this);    //SIW529
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        private void subClearPointers()
        {
            m_xmlDLData = null;
            m_xmlBLData = null;
            xmlContactList = null;
            xmlContact = null;
            xmlCategoryList = null;
            xmlCategory = null;
            xmlIDNList = null;
            xmlIDN = null;
            xmlDLQList = null;
            xmlDLQ = null;
            xmlBLQList = null;
            xmlBLQ = null;

            RelatedEntity = null;
            AddressReference = null;
            CommentReference = null;
            DocumentReference = null;
            Triggers = null;
        }

        public XMLPartyInvolved RelatedEntity
        {
            get
            {
                if (m_PartyInvolved == null)
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                m_PartyInvolved.NodeName = Constants.sEntRelatedEnt;
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                {
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.NodeName = Constants.sEntRelatedEnt;
                }
            }
        }

        //Start SIW12
        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
                if (m_PartyInvolved != null)
                    ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }
        //End SIW12

        public XMLAddressReference AddressReference
        {
            get
            {
                if (m_AddressReference == null)
                    m_AddressReference = ((Utils)Utils).funSetupAddressReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                return m_AddressReference;
            }
            set
            {
                m_AddressReference = value;
                if (m_AddressReference != null)
                    ((Utils)Utils).funSetupAddressReference(m_AddressReference, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                    m_Comment = ((Utils)Utils).funSetupCommentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                return m_Comment;
            }
            set
            {
                m_Comment = value;
                if (m_Comment != null)
                    ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        //Begin SI06420
        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }
        //End SI06420

        public XMLDocumentReference DocumentReference
        {
            get
            {
                if (m_Document == null)
                    m_Document = ((Utils)Utils).funSetupDocumentReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                return m_Document;
            }
            set
            {
                m_Document = value;
                if (m_Document != null)
                    ((Utils)Utils).funSetupDocumentReference(m_Document, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }

        public XMLAccount Account //SIW7182
        {
            get
            {
                if (m_Account == null)
                {
                    m_Account = new XMLAccount();
                    m_Account.Parent = this;
                    m_Account.getFirst();
                }
                return m_Account;
            }
            set
            {
                m_Account = value;
            }
        }
        public XmlNode getEntity(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode getEntityByName(entType centitytype, string sname1, string sname2, string sname3)
        {
            bool bMatch;
            sname1 = sname1.ToUpper();
            sname2 = sname2.ToUpper();
            sname3 = sname3.ToUpper();
            getFirst();
            while (Node != null)
            {
                bMatch = true;
                switch (centitytype)
                {
                    case CCP.Constants.entType.entTypeBus:
                    case CCP.Constants.entType.entTypeHou:
                        if (sname1 != "" && sname1 != null && sname1 != LegalName.ToUpper())
                            bMatch = false;
                        if (sname2 != "" && sname2 != null && sname2 != DBAName.ToUpper())
                            bMatch = false;
                        break;
                    case CCP.Constants.entType.entTypeInd:
                        if (sname1 != "" && sname1 != null && sname1 != FirstName.ToUpper())
                            bMatch = false;
                        if (sname2 != "" && sname2 != null && sname2 != MiddleName.ToUpper())
                            bMatch = false;
                        if (sname3 != "" && sname3 != null && sname3 != LastName.ToUpper())
                            bMatch = false;
                        break;
                    default:
                        bMatch = false;
                        break;
                }
                if (bMatch)
                    break;
                getNext();
            }
            return Node;
        }

        public XmlNode getEntityByTaxID(string staxid)
        {
            if (TaxId != staxid)
            {
                getFirst();
                while (Node != null)
                {
                    if (TaxId == staxid)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public XmlNode getEntityByIDNbr(string sID, string sType)
        {
            bool bMatched;

            if (IDNumber != sID || IDNumberType != sType)
            {
                bMatched = false;
                getFirst();
                while (!bMatched && Node != null)
                {
                    getIDNumber(true);
                    while (!bMatched && IDNNode != null)
                    {
                        if (IDNumber != sID && IDNumberType != sType)
                            bMatched = true;
                        else
                            getIDNumber(false);
                    }
                    if(!bMatched)
                        getNext();
                }
            }
            return Node;
        }

        public XmlNode getEntitybyID(string sID)
        {
            if (EntityID != sID)
            {
                getFirst();
                while (Node != null)
                {
                    if (EntityID == sID)
                        break;
                    getNext();
                }
            }
            return Node;
        }

        public bool BusinessEntity
        {
            get
            {
                switch ((entType)Int32.Parse(EntityType))
                {
                    case entType.entTypeBus:
                    case entType.entTypeClient:
                    case entType.entTypeCompany:
                    case entType.entTypeOperation:
                    case entType.entTypeRegion:
                    case entType.entTypeDivision:
                    case entType.entTypeLocation:
                    case entType.entTypeFacility:
                    case entType.entTypeDepartment:
                    //Start SIN8499
                    case entType.entTypeClientSec:
                    case entType.entTypeCompanySec:
                    case entType.entTypeOperationSec:
                    case entType.entTypeRegionSec:
                    case entType.entTypeDivisionSec:
                    case entType.entTypeLocationSec:
                    case entType.entTypeFacilitySec:
                    case entType.entTypeDepartmentSec:
                    //End SIN8499
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IndividualEntity
        {
            get
            {
                return (EntityType == (Constants.iEntTypeInd.ToString()));
            }
        }

        public bool HouseholdEntity
        {
            get
            {
                return (EntityType == (Constants.iEntTypeHou + ""));
            }
        }

        public XMLTriggers Triggers
        {
            get
            {
                if (m_Triggers == null)
                {
                    m_Triggers = new XMLTriggers();
                    m_Triggers.Parent = this;
                    //SIW529 LinkXMLObjects(m_Triggers);
                    m_Triggers.getTrigger(null, Constants.xcGetFirst);  //SI06420
                }
                return m_Triggers;
            }
            set
            {
                m_Triggers = value;
            }
        }

        //Start SIW7214
        //******* Start ******** Medicare Eligible ****************//
        public XmlNode putEntMedEligible(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sCMSEligible, sDesc, sCode, sThisNodeOrder, scodeid);
        }
        public string EntMedEligible
        {
            get
            {
                return Utils.getData(Node, Constants.sCMSEligible);
            }
            set
            {
                Utils.putData(putNode, Constants.sCMSEligible, value, sThisNodeOrder);
            }
        }
        public string EntMedEligible_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sCMSEligible);
            }
            set
            {
                Utils.putCode(putNode, Constants.sCMSEligible, value, sThisNodeOrder);
            }
        }
        public string EntMedEligible_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sCMSEligible);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sCMSEligible, value, sThisNodeOrder);
            }
        }
        //******* End ******** Medicare Eligible ****************//


        //** NOTE: DO NOT USE MEDICAREELIGIBLE!!!! USE ENTMEDELIGIBLE
        public string MedicareEligible
        {
            get
            {

                return Utils.getAttribute(Node, Constants.sCMSEligible, "INDICATOR");
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sCMSEligible, "INDICATOR", value, sThisNodeOrder);
            }
        }

        public string HICN_ID
        {
            get
            {
                return Utils.getData(Node, Constants.sCMSHICN);
            }
            set
            {
                Utils.putData(putNode, Constants.sCMSHICN, value, sThisNodeOrder);
            }
        }

        //End SIW7214
        public XmlNode W9_Node
        {
            get
            {
                return Utils.getNode(Node, Constants.sEntW9Info);
            }
        }

        public XmlNode putW9Node
        {
            get
            {
                XmlNode xmlNode = W9_Node;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putNode, Constants.sEntW9Info, sThisNodeOrder);
                return xmlNode;
            }
        }

        public string W9Valid
        {
            get
            {
                return Utils.getBool(W9_Node, "", Constants.sEntW9Valid);
            }
            set
            {
                Utils.putBool(putW9Node, "", Constants.sEntW9Valid, value, sW9NodeOrder);
            }
        }

        //Start SIN7541

        public string W9DisallowPayments
        {
            get
            {
                return Utils.getBool(W9_Node, "", Constants.sEntDisallowPayments);
            }
            set
            {
                Utils.putBool(putW9Node, "", Constants.sEntDisallowPayments, value, sW9NodeOrder);
            }
        }
       
        //End SIN7541
        public string W9date
        {
            get
            {
                return Utils.getDate(W9_Node, Constants.sEntW9Date);
            }
            set
            {
                Utils.putDate(putW9Node, Constants.sEntW9Date, value, sW9NodeOrder);
            }
        }

        public string W9RequestDate
        {
            get
            {
                return Utils.getDate(W9_Node, Constants.sEntW9ReqDate);
            }
            set
            {
                Utils.putDate(putW9Node, Constants.sEntW9ReqDate, value, sW9NodeOrder);
            }
        }

        public string W9LastIrsExcpDate
        {
            get
            {
                return Utils.getDate(W9_Node, Constants.sEntW9LastIRSExcepDate);
            }
            set
            {
                Utils.putDate(putW9Node, Constants.sEntW9LastIRSExcepDate, value, sW9NodeOrder);
            }
        }

        //Start N8584

        public string EffectiveStartDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntEffStartDt);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntEffStartDt, value, NodeOrder);   
            }
        }

        public string EffectiveEndDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntEffEndDt);  
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntEffEndDt, value, NodeOrder);   
            }
        }

        // End N8584

        public XmlNode BUWH_Node
        {
            get
            {
                return Utils.getNode(Node, Constants.sEntBUWH);
            }
        }

        public XmlNode putBUWHNode
        {
            get
            {
                XmlNode xmlNode = BUWH_Node;
                if (xmlNode == null)
                    xmlNode = XML.XMLaddNode(putNode, Constants.sEntBUWH, sThisNodeOrder);
                return xmlNode;
            }
        }

        public string BUWHEnabled
        {
            get
            {
                return Utils.getBool(BUWH_Node, "", Constants.sEntBUWHEnabled);
            }
            set
            {
                Utils.putBool(putBUWHNode, "", Constants.sEntBUWHEnabled, value, sBUWHNodeOrder);
            }
        }

        public string BUWHStartDate
        {
            get
            {
                return Utils.getDate(BUWH_Node, Constants.sEntBUWHStartDate);
            }
            set
            {
                Utils.putDate(putBUWHNode, Constants.sEntBUWHStartDate, value, sBUWHNodeOrder);
            }
        }

        public string BUWHEndDate
        {
            get
            {
                return Utils.getDate(BUWH_Node, Constants.sEntBUWHEndDate);
            }
            set
            {
                Utils.putDate(putBUWHNode, Constants.sEntBUWHEndDate, value, sBUWHNodeOrder);
            }
        }

        public string BUWHPercent
        {
            get
            {
                return Utils.getData(BUWH_Node, Constants.sEntBUWHPercent);
            }
            set
            {
                Utils.putData(putBUWHNode, Constants.sEntBUWHPercent, value, sBUWHNodeOrder);
            }
        }

// Begin SIW489
        public XMLCombPay CombPay
        {
            get
            {
                if (m_CombPay == null)
                {
                    m_CombPay = new XMLCombPay();
                    m_CombPay.Parent = this;
                    //SIW529 LinkXMLObjects(m_CombPay);
                    m_CombPay.getCombPay(Constants.xcGetFirst); 
                }
                return m_CombPay;
            }
            set
            {
                m_CombPay = value;
            }
        }
// End SIW489
        // Start SIW7620
        /*public XMLPhyPrivilege PhyPrivilege
        {
            get
            {
                if (m_PhyPrivilege == null)
                {
                    m_PhyPrivilege = new XMLPhyPrivilege();
                    m_PhyPrivilege.Parent = this;
                    m_PhyPrivilege.getCombPay(Constants.xcGetFirst);
                }
                return m_PhyPrivilege;
            }
            set
            {
                m_PhyPrivilege = value;
            }
        }*/
        // End SIW7620

    }

    /*public class XMLPhyPrivilege : XMLXCNodeWithList
    { 
       
        private XMLEntity m_Entity;
        private XMLAddressReference m_AddressReference;
        private XmlNodeList xmlAccountList;
        private XmlNode xmlAccount;
        private ArrayList sAccountNodeOrder;

        public XMLPhyPrivilege()
        {
           
            xmlThisNode = null;
            xmlNodeList = null;

            sThisNodeOrder = new ArrayList(15);           


            sThisNodeOrder = new ArrayList(8);
            sThisNodeOrder.Add(Constants.sStdTechKey);
            sThisNodeOrder.Add(Constants.sEntPHYPRIVID);
            sThisNodeOrder.Add(Constants.sEntPRIVPHYSEID);
            sThisNodeOrder.Add(Constants.sEntPHYCATEGORY_CODE);
            sThisNodeOrder.Add(Constants.sEntPHYTYPE_CODE);
            sThisNodeOrder.Add(Constants.sEntPHYSTATUS_CODE);
            sThisNodeOrder.Add(Constants.sEntPHYINT_DATE);
            sThisNodeOrder.Add(Constants.sEntPHYEND_DATE);

           
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sEntCombPay; }
        }

        public override XmlNode Create()
        {
            
            return Create("", "","", "","", "","", "","", "","","", "","");
           
        }

       
        public XmlNode Create(string sID, string snextprintdate, string sFrequency, string sFrequencyCode, string sWeekMonthCount, string sStopDate, string sStopFlag, 
                              string sNextSchedDate, string sCurrBatch, string sCurrBatchNumPays, string sEFTAcct, string sEFTRoute, 
                              string sEFTEffDate, string sEFTExpDate)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                bool bblanking = XML.Blanking;
                XML.Blanking = false;

                CombPayId = sID;
                NextPrintDate = snextprintdate;
                //SIW7240 -START
                WeekMonthCount = sWeekMonthCount;
                StopDate = sStopDate;
                StopFlag = sStopFlag;
                NextSchedDate = sNextSchedDate;
                CurrBatch = sCurrBatch;
                CurrBatchNumPays = sCurrBatchNumPays;
                EFTAcct = sEFTAcct;
                EFTRoute = sEFTRoute;
                EFTEffDate = sEFTEffDate;
                EFTExpDate = sEFTExpDate;

                putFrequency(sFrequency, sFrequencyCode);
                //SIW7240 - END
              
                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLPolicy.Create");
                return null;
            }   
        }
        //SIW7240- start
        public string CombPayId
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sEntCombPayId);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 4) == "CPAY" && strdata.Length >= 5)
                        lid = StringUtils.Right(strdata, strdata.Length - 4);
                return lid;

                //return Utils.getAttribute(Node, Constants.sEntCombPay, Constants.sEntCombPayId);
            }
            set
            {
                //Utils.putAttribute(putNode, Constants.sEntCombPay, Constants.sEntCombPayId, value, NodeOrder);
                XML.XMLSetAttributeValue(putNode, Constants.sEntCombPayId, "CPAY" + value);
                //Techkey = value;
            }
        }

        public string Techkey 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);
            }
        }

        //Start SIW7601
        public string EntityAccountID
        {
            get
            {               
                string strdata = Utils.getData(Node, Constants.sEntCombPayEntAccountID);
                string lid = "";
                if (strdata != "" && strdata != null)
                    if (StringUtils.Left(strdata, 3) == Constants.sACCIDPfx && strdata.Length >= 4)
                        lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayEntAccountID, Constants.sACCIDPfx + value, NodeOrder);
            }
        }
        //End SIW7601

        public string NextPrintDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayNextPrtDt);//SIW489
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayNextPrtDt, value, NodeOrder);//SIW489
            }
        }

        public XMLAddressReference AddressReference
        {
            get
            {
                if (m_AddressReference == null)
                {
                    m_AddressReference = ((Utils)Utils).funSetupAddressReference(null, Node, this, NodeOrder);    //SIW529 Removed Document parameter
                }
                return m_AddressReference;
            }
            set
            {
                m_AddressReference = value;
                if (m_AddressReference != null)
                    ((Utils)Utils).funSetupAddressReference(m_AddressReference, Node, this, NodeOrder);    //SIW529 Removed Document parameter
            }
        }

        public int AccountCount
        {
            get
            {
                return AccountList.Count;
            }
        }

        public XmlNodeList AccountList
        {
            get
            {
                //Start SIW163
                if (null == xmlAccountList)
                {
                    return getNodeList(Constants.sEntCombPayAcctNode, ref xmlAccountList, Node);
                }
                else
                {
                    return xmlAccountList;
                }
                //End SIW163
            }
        }

        public XmlNode getAccount(bool reset)
        {
            //Start SIW163
            if (null != AccountList)
            {
                return getNode(reset, ref xmlAccountList, ref xmlAccount);
            }
            else
            {
                return null;
            }
            //End SIW163
        }

        public XmlNode addAccount()
        {
            AccountNode = null;
            return putAccountNode;
        }

        public XmlNode putAccountNode
        {
            get
            {
                if (AccountNode == null)
                    AccountNode = XML.XMLaddNode(Node, Constants.sEntCombPayAcctNode, NodeOrder);
                return AccountNode;
            }
        }

        public XmlNode AccountNode
        {
            get
            {
                return xmlAccount;
            }
            set
            {
                xmlAccount = value;
            }
        }

        public XmlNode putAccount(string sAccountId, string sBankNumber, string sRoutingNumber,
                                  string sAccountNumber, string sFractionalNumber, string sAccountName)
        {
            if (sAccountId != "" && sAccountId != null)
                AccountTechkey = sAccountId;
            if (sAccountName != "" && sAccountName != null)
                AccountName = sAccountName;
            if (sBankNumber != "" && sBankNumber != null)
                BankNumber = sBankNumber;
            if (sRoutingNumber != "" && sRoutingNumber != null)
                RoutingNumber = sRoutingNumber;
            if (sAccountNumber != "" && sAccountNumber != null)
                AccountNumber = sAccountNumber;
            if (sFractionalNumber != "" && sFractionalNumber != null)
                FractionalNumber = sFractionalNumber;
            return AccountNode;
        }

        public string BankNumber
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sEntCombPayAcctNode, Constants.sEntCombPayBankNumber);
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sEntCombPayAcctNode, Constants.sEntCombPayBankNumber, value, sAccountNodeOrder);
            }
        }

        public string AccountTechkey
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sStdTechKey);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sStdTechKey, value, sAccountNodeOrder);
            }
        }

        public string RoutingNumber
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sEntCombPayRoutingNumber);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sEntCombPayRoutingNumber, value, sAccountNodeOrder);
            }
        }

        public string AccountName
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sEntCombPayAccountName);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sEntCombPayAccountName, value, sAccountNodeOrder);
            }
        }

        public string AccountNumber
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sEntCombPayAccountNumber);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sEntCombPayAccountNumber, value, sAccountNodeOrder);
            }
        }

        public string FractionalNumber
        {
            get
            {
                return Utils.getData(AccountNode, Constants.sEntCombPayFractionalNumber);
            }
            set
            {
                Utils.putData(putAccountNode, Constants.sEntCombPayFractionalNumber, value, sAccountNodeOrder);
            }
        }
        //SIW7240 -START
        public string Frequency
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayFrequency);   
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayFrequency, value, NodeOrder);    
            }
        }

        public string Frequency_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sEntCombPayFrequency);   
            }
            set
            {
                Utils.putCode(putNode, Constants.sEntCombPayFrequency, value, NodeOrder);   
            }
        }

        public string Frequency_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sEntCombPayFrequency);   
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sEntCombPayFrequency, value, NodeOrder);    
            }
        }

        public string WeekMonthCount
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayWeekMonthCount);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayWeekMonthCount, value, NodeOrder);
            }
        }


        public string StopDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayStopDate);    
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayStopDate, value, NodeOrder);    
            }
        }

        public string StopFlag
        {
            get
            {
                //return Utils.getBool(Node, Constants.sEntCombPayAcctNode, Constants.sEntCombPayStopFlag);//SIN7582
                return Utils.getBool(Node, Constants.sEntCombPayStopFlag, "");//SIN7582
            }
            set
            {
                // Utils.putBool(putNode, Constants.sEntCombPayAcctNode, Constants.sEntCombPayStopFlag, value, NodeOrder);//SIN7582
                Utils.putBool(putNode, Constants.sEntCombPayStopFlag, "", value, NodeOrder);//SIN7582
            }
        }


        public string LastDOM
        {
            get
            {
                return Utils.getBool(Node, Constants.sEntCombPayLastDOM,"");
            }
            set
            {
                Utils.putBool(putNode,  Constants.sEntCombPayLastDOM,"", value, NodeOrder);
            }
        }

        public string LastSchedDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayLastSchedDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayLastSchedDate, value, NodeOrder);
            }
        }

        public string NextSchedDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayNextSchedDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayNextSchedDate, value, NodeOrder);
            }
        }


        public string CurrBatch
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayCurrBatch);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayCurrBatch, value, NodeOrder);
            }
        }
        public string CurrBatchNumPays 
        {
            get
            {
                return Utils.getData(Node,Constants.sEntCombPayCurrBatchNumPays);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayCurrBatchNumPays, value, NodeOrder);
            }
        }

        public string EFTAcct
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayEFTAcct);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayEFTAcct, value, NodeOrder);
            }
        }


        public string EFTRoute
        {
            get
            {
                return Utils.getData(Node, Constants.sEntCombPayEFTRoute);
            }
            set
            {
                Utils.putData(putNode, Constants.sEntCombPayEFTRoute, value, NodeOrder);
            }
        }

        public string EFTEffDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sEntCombPayEFTEffDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayEFTEffDate, value, NodeOrder);
            }
        }

        public string EFTExpDate
        {
            get
            {
                return Utils.getDate(Node,  Constants.sEntCombPayEFTExpDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sEntCombPayEFTExpDate, value, NodeOrder);
            }
        }

        public XmlNode putFrequency(string sDesc, string sCode)
        {
            return putFrequency(sDesc, sCode, "");
        }

        public XmlNode putFrequency(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sEntCombPayAcctNode, sDesc, sCode, NodeOrder, scodeid);    
        }

        public new XMLEntity Parent
        {
            get
            {
                if (m_Entity == null)
                {
                    m_Entity = new XMLEntity();
                    m_Entity.CombPay = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Entity);   //SIW529
                    ResetParent();
                }
                return m_Entity;
            }
            set
            {
                m_Entity = value;
                ResetParent();
                if (m_Entity != null) //SIW529
                    ((XMLXCNodeBase)m_Entity).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            if (m_Entity != null)
                m_Entity.CombPay = this;
            Node = null;
            //LinkXMLObjects((XMLXCNodeBase)m_Entity);   //SIW529
        }

        private void subClearPointers()
        {
            m_AddressReference=null;
            xmlAccountList = null;
            xmlAccount =null;
       }
       public override XmlNodeList NodeList
       {
           get
           {
               return getNodeList(NodeName);
           }
       }

       public XmlNode getCombPay(bool reset)
       {
           return getNode(reset);
       }
        //SIW7240- start
       public XmlNode getCombPayID(string sCombPayID)
       {
           getFirst();
           while (Node != null)
           {
               if (CombPayId  == sCombPayID)
                   break;
               getNext();
           }
           return Node;
       }
        //SIW7240- End
    }*/

    public class XMLEntities : XMLXCNode
    {
        /*****************************************************************************
        ' The cXMLEntities class provides the functionality to support and manage a
        ' ClaimsPro Entities Collection Node
        '*****************************************************************************
        '<ENTITIES NEXT_ID="0" COUNT="xx">
        '   <ENTITY>
        '         ..........
        '   </ENTITY>
        '</ENTITIES>
        '*****************************************************************************/

        private XMLEntity m_Entity;
        
        public XMLEntities()
        {
            //SIW529 Utils.subInitializeGlobals(XML); //SIW485 //SIW493
            xmlThisNode = null;

            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sEntNode);

            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }

        protected override string DefaultNodeName
        {
            get { return Constants.sEntCollection; }
        }

        private void subClearPointers()
        {
            //Start SI06420
            //Entity = null;
            if (m_Entity != null)
                //SIW529 m_Entity.Node = null;
                m_Entity.Parent = this; //SIW529 Resets Entity object
            //End SI06420
        }

        public override XmlNode Create()
        {
            return Create(null, "");    
        }

        public XmlNode Create(XmlDocument xmlDoc, string sNextID)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();
            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = (XmlNode)xmlElement;

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                NextID = sNextID;
                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLEntities.Create");
                return null;
            }
        }

        public string getNextID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextID;
            if (nid == "" || nid == "0" || nid == null)
                nid = "1";
            NextID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public string NextID
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sEntNextID);
            }
            set
            {
                if (value == "")
                    value = "1";
                Utils.putAttribute(putNode, "", Constants.sEntNextID, value, NodeOrder);
            }
        }

        public int Count
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sEntCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sEntCount, value + "", NodeOrder);
            }
        }

        public override XmlNode Node
        {
            get//Start SI06420
            {
                if (xmlThisNode == null)
                    ResetParent();
                return xmlThisNode;
            }//End SI06420
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        protected override void ResetParent()
        {
            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }

        public override XMLACNode Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = (XML)value;
                ResetParent();
            }
        }

        public XMLEntity Entity
        {
            get
            {
                if (m_Entity == null)
                {
                    m_Entity = new XMLEntity();
                    m_Entity.Parent = this;
                    //SIW529 LinkXMLObjects(m_Entity);
                    //Start SIW529
                    //if (this.UseSharedXMLDocument == false)
                    //{
                    //    m_Entity.UseSharedXMLDocument = UseSharedXMLDocument;
                    //    m_Entity.XML = XML;
                    //}
                    //end SIW529
                    //m_Entity.getEntity(Constants.xcGetFirst);   //SI06420
                }
                if (Node != null)//Start SI06420
                { 
                  if(m_Entity.Node == null)
                      m_Entity.getEntity(Constants.xcGetFirst);
                }//End SI06420
                return m_Entity;
            }
            set
            {
                m_Entity = value;
                if (m_Entity != null)       //SI06420
                {
                    m_Entity.Parent = this; //SI06420
                    //SIW529 LinkXMLObjects(m_Entity);
                }
            }
        }

        public void AddEntity()
        {
            Entity.Create();
        }

        public XmlNode getEntity(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return Entity.getEntity(reset);
        }

        public XmlNode getEntityByName(XmlDocument xdoc, entType centitytype,
                                       string sname1, string sname2, string sname3)
        {
            if (xdoc != null)
                Document = xdoc;
            return Entity.getEntityByName(centitytype, sname1, sname2, sname3);
        }

        public XmlNode getEntityByTaxID(XmlDocument xdoc, string staxid)
        {
            if (xdoc != null)
                Document = xdoc;
            return Entity.getEntityByTaxID(staxid);
        }

        public XmlNode getEntitybyID(XmlDocument xdoc, string sID)
        {
            if (xdoc != null)
                Document = xdoc;
            return Entity.getEntitybyID(sID);
        }
    }
}
