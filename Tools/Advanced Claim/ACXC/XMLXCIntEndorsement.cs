/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public interface XMLXCIntEndorsement
    {
        //Simple interface to be implemented by classes that could be parent nodes to XMLEndorsement

        XMLEndorsement Endorsement
        {
            get;
            set;
        }
    }
}
