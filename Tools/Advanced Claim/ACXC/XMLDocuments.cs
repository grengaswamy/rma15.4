/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 05/06/2010 | SIW261  |    JTC     | Attachments
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.Common;
using CCP.Constants;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public class XMLDocument : XMLXCNodeWithList
    {
        /*****************************************************************************
        ' The cXMLDocument class provides the functionality to support and manage a
        ' ClaimsPro Document Node
        '*****************************************************************************
        '<DOCUMENTS COUNT="xx" NEXT_ID="xx"
        '   <FOLDER>Global folder name where the documents are</FOLDER>
        '   <DOCUMENT DOCUMENT_ID="DOCxxx">
        '     <FOLDER>Override Folder name for this document</FOLDER>
        '     <ARCHIVE_LVL>123</ARCHIVE_LVL>
        '     <CREATE_DATE YEAR="2002" MONTH="03" DAY="01" />
        '     <CREATE_TIME HOUR="17" MINUTE="35" SECOND="04" /> 'SIW261
        '     <CATEGORY _OPTIONAL_="" CODE="Category Code {string} [CATEGORY]">Category Name {String}</CATEGORY>
        '     <NAME>Friendly Document Name</NAME>
        '     <CLASS _OPTIONAL_="" CODE="Class Code {string} [CLASS]">Class Name {String}</CLASS>
        '     <SUBJECT>Title for the document</SUBJECT>
        '     <TYPE _OPTIONAL_="" CODE="Document Type Code {string} []">Document Type Name {String}</TYPE>
        '     <NOTES>Notes about the document</NOTES>
        '     <SECURITY_LVL>Level of security (Long)</SECURITY_LVL>
        '     <USER_ID>User ID {String}</USER_ID>
        '     <INTERNAL_TYPE>2</INTERNAL_TYPE>
        '     <CREATE_APP>Name of application that creates the attachement</CREATE_APP>
        '     <FILENAME>Name of the file to attach</FILENAME>
        '     <FILEPATH>Path to the file to attach</FILEPATH>
        '     <TABLENAME>Name of the table to which we attach the file</TABLENAME>
        '     <BASE64>The binary data for an attachment, converted to Base64 format</BASE64>    //SIW485
        '   </DOCUMENT>
        '</DOCUMENTS>
        '*****************************************************************************/
        
        public XMLDocument()
        {
            //SIW529 Utils.subInitializeGlobals(XML);
            sThisNodeOrder = new ArrayList(18);
            sThisNodeOrder.Add(Constants.sDocFolder);
            sThisNodeOrder.Add(Constants.sDocArchLvl);
            sThisNodeOrder.Add(Constants.sDocCreateDate);
            sThisNodeOrder.Add(Constants.sDocCreateTime);   //SIW261
            sThisNodeOrder.Add(Constants.sDocCategory);
            sThisNodeOrder.Add(Constants.sDocName);
            sThisNodeOrder.Add(Constants.sDocClass);
            sThisNodeOrder.Add(Constants.sDocSubject);
            sThisNodeOrder.Add(Constants.sDocType);
            sThisNodeOrder.Add(Constants.sDocNotes);
            sThisNodeOrder.Add(Constants.sDocSecLevel);
            sThisNodeOrder.Add(Constants.sDocUserID);
            sThisNodeOrder.Add(Constants.sDocInternalType);
            sThisNodeOrder.Add(Constants.sDocCreateApp);
            sThisNodeOrder.Add(Constants.sDocFileName);
            sThisNodeOrder.Add(Constants.sDocFilePath);
            sThisNodeOrder.Add(Constants.sDocTableName);
            sThisNodeOrder.Add(Constants.sDocBase64);   //SIW485
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sDocNode; }
        }

        public override XmlNode Create()
        {
            return Create("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        }

        public XmlNode Create(string sDocid, string sFolder, string sArchLevel, string sCreateDate,
                              string sCategory, string sName, string sClass, string sSubject, string sType,
                              string sNotes, string sSecLvl, string sUserID, string sInternalType,
                              string sCreateApp, string sFileName, string sFilePath, string sTablename)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false, null, null);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                DocumentID = sDocid;
                Folder = sFolder;
                ArchiveLevel = sArchLevel;
                CreateDate = sCreateDate;
                DocumentCategory = sCategory;
                Name = sName;
                DocumentType = sType;
                Notes = sNotes;
                SecLevel = sSecLvl;
                UserID = sUserID;
                InternalType = sInternalType;
                CreateApp = sCreateApp;
                FileName = sFileName;
                FilePath = sFilePath;
                TableName = sTablename;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLDocument.Create");
                return null;
            }
        }

        public string Folder
        {
            get
            {
                return Utils.getData(Node, Constants.sDocFolder);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocFolder, value, NodeOrder);
            }
        }

        public string ArchiveLevel
        {
            get
            {
                return Utils.getData(Node, Constants.sDocArchLvl);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocArchLvl, value, NodeOrder);
            }
        }

        public string CreateDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sDocCreateDate);
            }
            set
            {
                Utils.putDate(putNode, Constants.sDocCreateDate, value, NodeOrder);
            }
        }

        //Start SIW261
        public string CreateTime
        {
            get
            {
                return Utils.getTime(Node, Constants.sDocCreateTime);
            }
            set
            {
                Utils.putTime(putNode, Constants.sDocCreateTime, value, NodeOrder);
            }
        }
        //End SIW261

        public XmlNode putDocumentCategory(string sType, string stypecode)
        {
            return Utils.putCodeItem(putNode, Constants.sDocCategory, sType, stypecode, NodeOrder);    //SIW261
        }

        public string DocumentCategory
        {
            get
            {
                return Utils.getData(Node, Constants.sDocCategory);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocCategory, value, NodeOrder);
            }
        }

        public string DocumentCategory_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDocCategory);    //SIW261
            }
            set
            {
                Utils.putCode(putNode, Constants.sDocCategory, value, NodeOrder);    //SIW261
            }
        }

        public string Name
        {
            get
            {
                return Utils.getData(Node, Constants.sDocName);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocName, value, NodeOrder);
            }
        }

        public XmlNode putDocumentClass(string sType, string stypecode)
        {
            return Utils.putCodeItem(putNode, Constants.sDocClass, sType, stypecode, NodeOrder);    //SIW261
        }

        public string DocumentClass
        {
            get
            {
                return Utils.getData(Node, Constants.sDocClass);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocClass, value, NodeOrder);
            }
        }

        public string DocumentClass_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDocClass);    //SIW261
            }
            set
            {
                Utils.putCode(putNode, Constants.sDocClass, value, NodeOrder);    //SIW261
            }
        }

        public string Subject
        {
            get
            {
                return Utils.getData(Node, Constants.sDocSubject);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocSubject, value, NodeOrder);
            }
        }

        public XmlNode putDocumentType(string sType, string stypecode)
        {
            return Utils.putCodeItem(putNode, Constants.sDocType, sType, stypecode, NodeOrder);    //SIW261
        }

        public string DocumentType
        {
            get
            {
                return Utils.getData(Node, Constants.sDocType);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocType, value, NodeOrder);
            }
        }

        public string DocumentType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sDocType);    //SIW261
            }
            set
            {
                Utils.putCode(putNode, Constants.sDocType, value, NodeOrder);    //SIW261
            }
        }

        public string Notes
        {
            get
            {
                return Utils.getData(Node, Constants.sDocNotes);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocNotes, value, NodeOrder);
            }
        }

        public string SecLevel
        {
            get
            {
                return Utils.getData(Node, Constants.sDocSecLevel);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocSecLevel, value, NodeOrder);
            }
        }

        public string UserID
        {
            get
            {
                return Utils.getData(Node, Constants.sDocUserID);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocUserID, value, NodeOrder);
            }
        }

        public string InternalType
        {
            get
            {
                return Utils.getData(Node, Constants.sDocInternalType);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocInternalType, value, NodeOrder);
            }
        }

        public string CreateApp
        {
            get
            {
                return Utils.getData(Node, Constants.sDocCreateApp);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocCreateApp, value, NodeOrder);
            }
        }

        public string FileName
        {
            get
            {
                return Utils.getData(Node, Constants.sDocFileName);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocFileName, value, NodeOrder);
            }
        }

        public string FilePath
        {
            get
            {
                return Utils.getData(Node, Constants.sDocFilePath);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocFilePath, value, NodeOrder);
            }
        }
        
        public string TableName
        {
            get
            {
                return Utils.getData(Node, Constants.sDocTableName);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocTableName, value, NodeOrder);
            }
        }

        //Start SIW485
        public string Base64
        {
            get
            {
                return Utils.getData(Node, Constants.sDocBase64);
            }
            set
            {
                Utils.putData(putNode, Constants.sDocBase64, value, NodeOrder);
            }
        }
        //End SIW485

        public string IDPrefix
        {
            get
            {
                return Constants.sDocIDPfx;
            }
        }

        public string DocumentID
        {
            get
            {
                string strdata, lid;
                lid = "";
                if (Node != null)
                {
                    strdata = XML.XMLGetAttributeValue(Node, Constants.sDocID);
                    lid = ((Utils)Utils).extDocumentID(strdata);
                }
                return lid;
            }
            set
            {
                string strdata = value;
                if (strdata == "" || strdata == null || strdata == "0")
                    strdata = Parent.getNextDocID(null);
                XML.XMLSetAttributeValue(putNode, Constants.sDocID, Constants.sDocIDPfx + strdata.Trim());
            }
        }
        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            base.InsertInDocument(Doc, nde);
            Parent.Doc = this;
            Parent.Count = Parent.Count + 1;
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getDocument(bool rst)
        {
            return getNode(rst);
        }

        public override ArrayList NodeOrder
        {
            get
            {
                return base.NodeOrder;
            }
            set
            {
                sThisNodeOrder = value;
            }
        }

        protected override void ResetParent()
        {
            xmlNodeList = null;
            Node = null;
        }

        public new XMLDocuments Parent
        {
            get
            {
                if (m_Parent == null)
                {       //SIW485
                    m_Parent = new XMLDocuments();
                    //SIW529 ((XMLDocuments)m_Parent).XML = XML; //SIW485
                    //SIW529 }       //SIW485
                    ((XMLDocuments)m_Parent).Doc = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();  //SIW529
                }   //SIW529
                return (XMLDocuments)m_Parent;
            }
            set
            {
                m_Parent = value;
                if (m_Parent != null)
                    ((XMLDocuments)m_Parent).Doc = this;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public XmlNode getDocumentbyID(string sDocid)
        {
            if (sDocid != DocumentID)
            {
                getFirst();
                while (Node != null)
                {
                    if (DocumentID == sDocid)
                        break;
                    getNext();
                }
            }
            return Node;
        }
    }

    public class XMLDocuments : XMLXCNode
    {
        /*****************************************************************************
        ' The cXMLDocuments class provides the functionality to support and manage a
        ' ClaimsPro Documents Collection Node
        '*****************************************************************************
        '<DOCUMENTS COUNT="xx" NEXT_ID="xx"
        '   <DOCUMENT DOCUMENT_ID="DOCxxx">
        '   </DOCUMENT>
        '</DOCUMENTS>
        '*****************************************************************************/

        private XMLDocument m_Document;
        
        public XMLDocuments()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            sThisNodeOrder = new ArrayList(1);
            sThisNodeOrder.Add(Constants.sDocNode);

            m_Document = null;
            Utils.subSetupDocumentNodeOrder();

            Node = Utils.getNode(Parent.Node, NodeName);
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sDocCollection; }
        }

        public XMLDocument Doc
        {
            get
            {
                if (m_Document == null)
                {
                    m_Document = new XMLDocument();
                    m_Document.Parent = this;
                    //SIW529 LinkXMLObjects(m_Document);
                    m_Document.getDocument(Constants.xcGetFirst);   //SI06420
                }
                return m_Document;
            }
            set
            {
                m_Document = value;
            }
        }

        public override XmlDocument Document
        {
            protected set
            {
                XML.Document = value;
            }
        }

        public override XmlNode Create()
        {
            return Create(null,"");
        }

        public XmlNode Create(XmlDocument xmlDoc, string sNextDocID)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            if (xmlDoc != null)
                Document = xmlDoc;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(NodeName);
                Node = xmlElement;

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                NextDocID = sNextDocID;
                Count = 0;

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return xmlThisNode;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLDocuments.Create");
                return null;
            }
        }

        public string getNextDocID(XmlNode xnode)
        {
            string nid;
            if (xnode != null)
                Node = xnode;
            nid = NextDocID;
            if (nid == "" || nid == null)
                nid = "1";
            NextDocID = (Int32.Parse(nid) + 1) + "";
            return nid;
        }

        public string NextDocID
        {
            get
            {
                return Utils.getAttribute(Node, "", Constants.sDocNextID);
            }
            set
            {
                string strdata = value;
                if (strdata == "" || strdata == null)
                    strdata = "1";
                Utils.putAttribute(putNode, "", Constants.sDocNextID, strdata, NodeOrder);
            }
        }

        public int Count
        {
            get
            {
                string strdata = Utils.getAttribute(Node, "", Constants.sDocCount);
                int i;
                Int32.TryParse(strdata, out i);
                return i;
            }
            set
            {
                Utils.putAttribute(putNode, "", Constants.sDocCount, value + "", NodeOrder);
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }

        public override XMLACNode Parent
        {
            get
            {
                return XML;
            }
            set
            {
                XML = (XML)value;
                ResetParent();
            }
        }

        public XmlNode AddDocument(string sDocid, string sFolder, string sArchLevel, string sCreateDate,
                                   string sCategory, string sName, string sClass, string sSubject,
                                   string sType, string sNotes, string sSecLvl, string sUserID,
                                   string sInternalType, string sCreateApp, string sFileName, string sFilePath,
                                   string sTableName, string sClaimNumber, string sEventNumber)
        {
            return Doc.Create(sDocid, sFolder, sArchLevel, sCreateDate, sCategory, sName, sClass, sSubject, sType, sNotes,
                       sSecLvl, sUserID, sInternalType, sCreateApp, sFileName, sFilePath, sTableName);
        }

        public XmlNode getDocument(XmlDocument xdoc, bool reset)
        {
            if (xdoc != null)
                Document = xdoc;
            return Doc.getDocument(reset);
        }

        public XmlNode getDocumentbyID(XmlDocument xdoc, string sDocid)
        {
            if (xdoc != null)
                Document = xdoc;
            return Doc.getDocumentbyID(sDocid);
        }

        protected override void ResetParent()
        {
            Node = XML.XMLGetNode(Parent.Node, NodeName);
        }
    }
}
