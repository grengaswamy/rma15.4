/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 05/19/2011 | SIN7147 |    AS      | Fixing limit link to parent node
 * 09/23/2011 | SIW7123 |    AV      | Add new child nodes to the limit
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using CCP.Common;
using CCP.Constants;
using CCP.XmlFormatting;

namespace CCP.XmlComponents
{
    public class XMLLimit : XMLXCNodeWithList
    {
        /*****************************************************************************
        ' The cXMLLimit class Creates and Retrieves limit nodes within a defined
        ' parent node
        '*****************************************************************************
        //Start SIW7123
        ' <PARENT_NODE>
        '   <LIMIT ID="LMTxxx">
        '     <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '     <TYPE CODE="LIM">Limit</TYPE>
        '     <SPECIFIED CODE="OCCUR">limit description</SPECIFIED>
        '     <COVERAGE CODE="">coverage type</COVERAGE>
        '     <AMOUNT>2135</AMOUNT>
        '     <USED_AMOUNT>...</USED_AMOUNT>
        '     <PART_OF>...</PART_OF>
        '     <AGGREGATE>...</AGGREGATE>
        '     <DESCRIPTION>text</DESCRIPTION>
        '     <EFFECTIVE_DATE YEAR="year" MONTH="month" DAY="day"/>
        '     <EXPIRATION_DATE YEAR="year" MONTH="month" DAY="day"/>
        '   </LIMIT>
        '   <!-- Multiple Limits -->
        ' </PARENT_NODE>*/

        //End SIW7123
        public XMLLimit()
        {
            xmlThisNode = null;
            xmlNodeList = null;
            //SIN7147 m_Parent = null;

            //SIW7123 sThisNodeOrder = new ArrayList(4);            //(SI06023 - Implemented in SI06333)
            sThisNodeOrder = new ArrayList(11);              //SIW7123
            sThisNodeOrder.Add(Constants.sStdTechKey);      //SIN7147
            sThisNodeOrder.Add(Constants.sLmtSpecified);
            sThisNodeOrder.Add(Constants.sLmtAmt);
            sThisNodeOrder.Add(Constants.sLmtEffDate);
            sThisNodeOrder.Add(Constants.sLmtExpDate);
            //Start SIW7123
            sThisNodeOrder.Add(Constants.sLmtUsedAmount);
            sThisNodeOrder.Add(Constants.sLmtCoverage);
            sThisNodeOrder.Add(Constants.sLmtPartOf);
            sThisNodeOrder.Add(Constants.sLmtAggregate);
            sThisNodeOrder.Add(Constants.sLmtDescription);
            sThisNodeOrder.Add(Constants.sStdTreeLabel);
            sThisNodeOrder.Add(Constants.sLmtType);
            //End SIW7123
        }

        public override XmlNode Create()
        {
            return AddLimit("", "", "", "", "");
        }

        public XmlNode AddLimit(string sDesc, string sCode, string sAmount, string sEffDate, string sExpDate)
        {
            xmlThisNode = XML.XMLaddNode(Parent_Node, NodeName, sThisNodeOrder);

            if (sDesc != "" && sDesc != null)
                Description = sDesc;
            if (sCode != "" && sCode != null)
                Limit_Code = sCode;
            if (sAmount != "" && sAmount != null)
                Limit = sAmount;
            if (sEffDate != "" && sEffDate != null)
                EffectiveDate = sEffDate;
            if (sExpDate != "" && sExpDate != null)
                ExpirationDate = sExpDate;

            return xmlThisNode;
        }
        
        protected override string DefaultNodeName
        {
            get { return Constants.sLmtNode; }
        }

        public string Description
        {
            get
            {
                return Utils.getData(Node, Constants.sLmtDescription);
            }
            set
            {
                Utils.putData(putNode, Constants.sLmtDescription, value, sThisNodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putLimit(string sdesc, string scode)
        {
            return putLimit(sdesc, scode, "");
        }

        public XmlNode putLimit(string sdesc, string scode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLmtSpecified, sdesc, scode, sThisNodeOrder, scodeid);    //SIW529 Remove local XML parameter
        }

        public string Limit_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLmtSpecified);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCode(putNode, Constants.sLmtSpecified, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string Limit_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLmtSpecified);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLmtSpecified, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //End SIW139

        public string Limit
        {
            get
            {
                return Utils.getData(Node, Constants.sLmtAmt);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putData(putNode, Constants.sLmtAmt, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }

        public string EffectiveDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sLmtEffDate);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sLmtEffDate, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        //Start SIW7123

        public string LimitDescription
        {
            get
            {
                return Utils.getData(Node, Constants.sLmtSpecified);
            }
            set
            {
                Utils.putData(putNode, Constants.sLmtSpecified, value, sThisNodeOrder);
            }
        }

        public XmlNode putCoverageType(string sDesc, string sCode)
        {
            return putCoverageType(sDesc, sCode, "");
        }
        public XmlNode putCoverageType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLmtCoverage , sDesc, sCode, sThisNodeOrder, scodeid);
        }
        public string CoverageType
        {
            get
            {
                return Utils.getData(Node, Constants.sLmtCoverage);
            }
            set
            {
                Utils.putData(putNode, Constants.sLmtCoverage, value, sThisNodeOrder);
            }
        }
        public string CoverageType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sLmtCoverage);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLmtCoverage, value, sThisNodeOrder);
            }
        }
        public string CoverageType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLmtCoverage);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLmtCoverage, value, sThisNodeOrder);
            }
        }
       
        public string Type_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sLmtType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sLmtType, value, sThisNodeOrder);
            }
        }
        
        public XmlNode putType(string sDesc, string sCode)
        {
            return putType(sDesc, sCode, "");
        }
        public XmlNode putType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sLmtType, sDesc, scodeid, sThisNodeOrder, scodeid);
        }

        public string Type_Code
        {
            get
            {
               return Utils.getCode(Node, Constants.sLmtType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sLmtType, value, sThisNodeOrder);
            }
        }

        public string TypeDescription
        {
            get
            {
                return Utils.getData(Node, Constants.sLmtType);
            }
            set
            {
                Utils.putData(putNode, Constants.sLmtType, value, sThisNodeOrder);
            }
        }

        public string UsedAmount
        {
            get
            {
                return Utils.getData(Node, Constants.sLmtUsedAmount);
            }
            set
            {
                Utils.putData(putNode, Constants.sLmtUsedAmount, value, sThisNodeOrder);
            }
        }

        public string PartOf
        {
            get
            {
                return Utils.getData(Node, Constants.sLmtPartOf);
            }
            set
            {
                Utils.putData(putNode, Constants.sLmtPartOf, value, sThisNodeOrder);
            }
        }

        public string Aggregate
        {
            get
            {
              return  Utils.getData(Node, Constants.sLmtAggregate);
            }
            set
            {
                Utils.putData(putNode, Constants.sLmtAggregate, value, sThisNodeOrder);
            }
        }
          public string ID
        {
            get
            {
                string strdata = XML.XMLGetAttributeValue(Node, Constants.sLmtID);
                string lid = "";
                if(StringUtils.Left(strdata,3)==Constants.sLmtIDPFX && strdata.Length>=4)
                    lid = StringUtils.Right(strdata, strdata.Length - 3);
                return lid;
            }
            set
            {
                string lid = value;
                if (value == "" || value == null || value == "0")
                {
                    Globals.lLIMId = Globals.lLIMId + 1;
                    lid = Globals.lDEDId + "";
                }
                XML.XMLSetAttributeValue(putNode, Constants.sLmtID, Constants.sLmtIDPFX + lid);
            }
        }

        public string TreeLabel
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, sThisNodeOrder);
            }
        }

        //End SIW7123

        public string ExpirationDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sLmtExpDate);    //SIW529 Remove local XML parameter
            }
            set
            {
                Utils.putDate(putNode, Constants.sLmtExpDate, value, sThisNodeOrder);    //SIW529 Remove local XML parameter
            }
        }
        public string Techkey //Start SIN7147
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End SIN7147

        public override XMLACNode Parent
        {
            set
            {
                m_Parent = value;
                if (m_Parent != null) //SIN7147
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIN7147
            }
        }

        public override XmlNodeList NodeList
        {
            get
            {
                return List;
            }
        }

        public XmlNodeList List
        {
            get
            {
                return getNodeList(NodeName);
            }
        }

        public XmlNode getLimit(bool reset)
        {
            return getNode(reset);
        }

        public XmlNode Remove()
        {
            if (Node != null)
            {
                Parent_Node.RemoveChild(Node);
                getFirst();
            }
            return Node;
        }
    }
}
