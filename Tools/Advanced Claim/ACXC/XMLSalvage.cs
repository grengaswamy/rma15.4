/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/02/2008 | SI06333 |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 01/28/2009 | SIW122  |    JTC     | Create Party Involved interface
 * 02/09/2009 | SIW139  |    JTC     | Update for field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 11/10/2009 | SIW198  |    SW      | Location Address on Salvage Page.                                      
 * 01/20/2010 | SIW243  |    SW      | Corrected Data type for DailyFee    
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 06/02/2010 | SIW321  |    ASM     | Daily Fee field load/save
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 08/13/2010 | SIW490 |    ASM     | Tree Label    
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.XmlFormatting;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlComponents
{
    public class XMLSalvage : XMLXCNode, XMLXCIntPartyInvolved  //SIW122
    {
        /*<Parent>
        '  <SALVAGE>
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '     <TREE_LABEL>component label</TREE_LABEL>                   'SIW490
        '     <CONTROL_NUMBER>control</CONTROL_NUMBER>
        '     <TYPE CODE="xx">type</TYPE>
        '     <STATUS CODE="xx">status</STATUS>
        '     <STOCK_NUMBER>stocknumber</STOCK_NUMBER>
        '     <DATE_ARRIVED YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_CUTOFF YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_APPRAISED YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_SOLD YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_CLOSED YEAR="2002" MONTH="03" DAY="07" />
        '     <CUTOFF_REASON CODE="xx">cutoff reason</CUTOFF_REASON>
        '     <DAILY_FEE>fee</DAILY_FEE>
        '     <ACTUAL_CASH_VALUE>value</ACTUAL_CASH_VALUE>
        '     <INITIAL_TOW_CHARGE>charge</INITIAL_TOW_CHARGE>
        '     <SALE_PRICE>sale price</SALE_PRICE>
        '     <RETAINED_BY_OWNER INDICATOR="True|False"/>
        '     <NET_RECOVERED>recovered amount</NET_RECOVERED>
        '     <PERCENT_RECOVERED>recovered amount</PERCENT_RECOVERED>
        '     <STORAGE_CHARGES>storage charges</STORAGE_CHARGES>
        '     <OTHER_CHARGES>other charges</OTHER_CHARGES>
        '     <TOTAL_CHARGES>total charges</TOTAL_CHARGES>
        '     <LOCATION_ADDRESS ID=""></LOCATION_ADDRESS>                   //SIW198
        '     <ESTIMATED_SALVAGE_VALUE>value</ESTIMATED_SALVAGE_VALUE>
        '     <ADDRESS_REFERENCE ID="ADRid  References an address in the Addresses Collection {id}"
        '                      CODE="Type of Address Code (Billing Mailing, Home, etc.) {string} [ADDRESS_TYPE]">Address Type Translated {string}</ADDRESS_REFERENCE>  //SIW198
        '      <!Multiple Parties Involved Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>        
        '  </SALVAGE>
        '</Parent>*/

        private XMLPartyInvolved m_PartyInvolved;
        private XMLCommentReference m_Comment;
        private XMLAddressReference m_AddressReference;  //SIW198
        private XMLVarDataItem m_DataItem;
        
        public XMLSalvage()
        {
            //SIW529 Utils.subInitializeGlobals(XML);

            xmlThisNode = null;
            m_Parent = null;

            //sThisNodeOrder = new ArrayList(23);               //(SI06023 - Implemented in SI06333)
            //sThisNodeOrder = new ArrayList(24);                 //(SI06023 - Implemented in SI06333) //SIW490
            sThisNodeOrder = new ArrayList(25);                 //SIW490
            sThisNodeOrder.Add(Constants.sStdTechKey);         //SIW360
            sThisNodeOrder.Add(Constants.sStdTreeLabel);       //SIW490
            sThisNodeOrder.Add(Constants.sSALType);
            sThisNodeOrder.Add(Constants.sSALControlNumber);
            sThisNodeOrder.Add(Constants.sSALStatus);
            sThisNodeOrder.Add(Constants.sSALStockNumbr);
            sThisNodeOrder.Add(Constants.sSALDateArrived);
            sThisNodeOrder.Add(Constants.sSALDateCutoff);
            sThisNodeOrder.Add(Constants.sSALDateAppraised);
            sThisNodeOrder.Add(Constants.sSALDateSold);
            sThisNodeOrder.Add(Constants.sSALDateClosed);
            sThisNodeOrder.Add(Constants.sSALCutoff);
            sThisNodeOrder.Add(Constants.sSALActualCashValue);
            sThisNodeOrder.Add(Constants.sSALTowCharge);
            sThisNodeOrder.Add(Constants.sSALSalePrice);
            sThisNodeOrder.Add(Constants.sSALOwnerRetained);
            sThisNodeOrder.Add(Constants.sSALNetRecovered);
            sThisNodeOrder.Add(Constants.sSALPercentRecovered);
            sThisNodeOrder.Add(Constants.sSALStorageCharge);
            sThisNodeOrder.Add(Constants.sSALOtherCharges);
            sThisNodeOrder.Add(Constants.sSALTotalCharges);
            sThisNodeOrder.Add(Constants.sSALLocationAddress);  //SIW198
            sThisNodeOrder.Add(Constants.sSALSalvageValue);
            sThisNodeOrder.Add(Constants.sAddressReferenceNode);//SIW198 
            sThisNodeOrder.Add(Constants.sSALPartyInvolved);
            sThisNodeOrder.Add(Constants.sSALCommentReference);
            sThisNodeOrder.Add(Constants.sSALDataItem);
            //sThisNodeOrder.Add(Constants.sSALTechKey);          //(SI06023 - Implemented in SI06333) //SIW360
        }

        
        protected override string DefaultNodeName
        {
            get { return Constants.sSALNode; }
        }

        public override XmlNode Create()
        {
            return Create("");
        }

        public XmlNode Create(string sdescription)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            Node = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                //Start W139
                bool bblanking = XML.Blanking;
                XML.Blanking = false;
                //End W139

                InsertInDocument(null, null);

                XML.Blanking = bblanking;   //W139
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "XMLSalvage.Create");
                return null;
            }
        }

        //Start SIW139
        public XmlNode putSalvageType(string sDesc, string sCode)
        {
            return putSalvageType(sDesc, sCode, "");
        }

        public XmlNode putSalvageType(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sSALType, sDesc, sCode, NodeOrder, scodeid);
        }

        public string SalvageType
        {
            get
            {
                return Utils.getData(Node, Constants.sSALType);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALType, value, NodeOrder);
            }
        }

        public string SalvageType_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sSALType);
            }
            set
            {
                Utils.putCode(putNode, Constants.sSALType, value, NodeOrder);
            }
        }

        public string SalvageType_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sSALType);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sSALType, value, NodeOrder);
            }
        }
        //End SIW139

        public string ControlNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sSALControlNumber);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALControlNumber, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putStatus(string sDesc, string sCode)
        {
            return putStatus(sDesc, sCode, "");
        }

        public XmlNode putStatus(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sSALStatus, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Status
        {
            get
            {
                return Utils.getData(Node, Constants.sSALStatus);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALStatus, value, NodeOrder);
            }
        }

        public string Status_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sSALStatus);
            }
            set
            {
                Utils.putCode(putNode, Constants.sSALStatus, value, NodeOrder);
            }
        }

        public string Status_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sSALStatus);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sSALStatus, value, NodeOrder);
            }
        }
        //End SIW139

        public string StockNumber
        {
            get
            {
                return Utils.getData(Node, Constants.sSALStockNumbr);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALStockNumbr, value, NodeOrder);
            }
        }

        public string DateArrived
        {
            get
            {
                return Utils.getDate(Node, Constants.sSALDateArrived);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSALDateArrived, value, NodeOrder);
            }
        }

        public string CutoffDate
        {
            get
            {
                return Utils.getDate(Node, Constants.sSALDateCutoff);
            }
            set 
            {
                Utils.putDate(putNode, Constants.sSALDateCutoff, value, NodeOrder);
            }
        }

        public string DateAppriased
        {
            get
            {
                return Utils.getDate(Node, Constants.sSALDateAppraised);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSALDateAppraised, value, NodeOrder);
            }
        }

        public string DateSold
        {
            get
            {
                return Utils.getDate(Node, Constants.sSALDateSold);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSALDateSold, value, NodeOrder);
            }
        }

        public string DateClosed
        {
            get
            {
                return Utils.getDate(Node, Constants.sSALDateClosed);
            }
            set
            {
                Utils.putDate(putNode, Constants.sSALDateClosed, value, NodeOrder);
            }
        }

        //Start SIW139
        public XmlNode putCutoff(string sDesc, string sCode)
        {
            return putCutoff(sDesc, sCode, "");
        }

        public XmlNode putCutoff(string sDesc, string sCode, string scodeid)
        {
            return Utils.putCodeItem(putNode, Constants.sSALCutoff, sDesc, sCode, NodeOrder, scodeid);
        }

        public string Cutoff
        {
            get
            {
                return Utils.getData(Node, Constants.sSALCutoff);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALCutoff, value, NodeOrder);
            }
        }

        public string Cutoff_Code
        {
            get
            {
                return Utils.getCode(Node, Constants.sSALCutoff);
            }
            set
            {
                Utils.putCode(putNode, Constants.sSALCutoff, value, NodeOrder);
            }
        }

        public string Cutoff_CodeID
        {
            get
            {
                return Utils.getCodeID(Node, Constants.sSALCutoff);
            }
            set
            {
                Utils.putCodeID(putNode, Constants.sSALCutoff, value, NodeOrder);
            }
        }
        //End SIW139

        public string DailyFee
        {
            get
            {
                //return Utils.getData(Node, Constants.sSALActualCashValue); //SIW243 //SIW321
                return Utils.getData(Node, Constants.sSALDailyFee); //SIW321
            }
            set
            {
                Utils.putData(putNode, Constants.sSALDailyFee, value, NodeOrder); //SIW243
            }
        }

        public string ActualCashValue
        {
            get
            {
                return Utils.getData(Node, Constants.sSALActualCashValue);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALActualCashValue, value, NodeOrder);
            }
        }

        public string TowCharge
        {
            get
            {
                return Utils.getData(Node, Constants.sSALTowCharge);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALTowCharge, value, NodeOrder);
            }
        }

        public string SalePrice
        {
            get
            {
                return Utils.getData(Node, Constants.sSALSalePrice);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALSalePrice, value, NodeOrder);
            }
        }

        public string OwnerRetained
        {
            get
            {
                return Utils.getBool(Node, Constants.sSALOwnerRetained, "");
            }
            set
            {
                Utils.putBool(putNode, Constants.sSALOwnerRetained, "", value, NodeOrder);
            }
        }

        public string NetRecovered
        {
            get
            {
                return Utils.getData(Node, Constants.sSALNetRecovered);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALNetRecovered, value, NodeOrder);
            }
        }

        public string PercentRecovered
        {
            get
            {
                return Utils.getData(Node, Constants.sSALPercentRecovered);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALPercentRecovered, value, NodeOrder);
            }
        }

        public string StorageCharge
        {
            get
            {
                return Utils.getData(Node, Constants.sSALStorageCharge);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALStorageCharge, value, NodeOrder);
            }
        }

        public string OtherCharges
        {
            get
            {
                return Utils.getData(Node, Constants.sSALOtherCharges);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALOtherCharges, value, NodeOrder);
            }
        }

        public string TotalCharges
        {
            get
            {
                return Utils.getData(Node, Constants.sSALTotalCharges);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALTotalCharges, value, NodeOrder);
            }
        }
        
        //Start SIW198
        public string LocationAddressID
        {
            get
            {
                return Utils.getAttribute(Node, Constants.sSALLocationAddress, "ID");
            }
            set
            {
                Utils.putAttribute(putNode, Constants.sSALLocationAddress, "ID", value, NodeOrder);
            }
        }
        //End SIw198

        public string EstimatedSalvageValue
        {
            get
            {
                return Utils.getData(Node, Constants.sSALSalvageValue);
            }
            set
            {
                Utils.putData(putNode, Constants.sSALSalvageValue, value, NodeOrder);
            }
        }

        public string Techkey //Start (SI06023 - Implemented in SI06333)
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTechKey);//SIW360
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTechKey, value, NodeOrder);//SIW360
            }
        }//End (SI06023 - Implemented in SI06333)

        //START SIW490
        public string TreeLabel 
        {
            get
            {
                return Utils.getData(Node, Constants.sStdTreeLabel);
            }
            set
            {
                Utils.putData(putNode, Constants.sStdTreeLabel, value, NodeOrder);
            }
        }
        //END SIW490

        protected override void ResetParent()
        {
            Node = null;
        }

        private void subClearPointers()
        {
            PartyInvolved = null;
            CommentReference = null;
            DataItem = null;
        }

        public XMLPartyInvolved PartyInvolved
        {
            get
            {
                if (m_PartyInvolved == null)
                {
                    m_PartyInvolved = ((Utils)Utils).funSetupPartyInvolved(m_PartyInvolved, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_PartyInvolved.getFirst();
                }
                return m_PartyInvolved;
            }
            set
            {
                m_PartyInvolved = value;
            }
        }

        public XMLCommentReference CommentReference
        {
            get
            {
                if (m_Comment == null)
                {
                    m_Comment = ((Utils)Utils).funSetupCommentReference(m_Comment, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                    m_Comment.getFirst();   //SI06420
                }
                return m_Comment;
            }
            set
            {
                m_Comment = value;
            }
        }
        //Start SIW198
        public XMLAddressReference AddressReference
        {
            get
            {
                if (m_AddressReference == null)
                    m_AddressReference = ((Utils)Utils).funSetupAddressReference(null, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
                return m_AddressReference;
            }
            set
            {
                m_AddressReference = value;
                if (m_AddressReference != null)
                    ((Utils)Utils).funSetupAddressReference(m_AddressReference, Node, this, NodeOrder);   //SIW529 Removed Document Parameter
            }
        }
        //End SIw198

        public XMLVarDataItem DataItem
        {
            get
            {
                if (m_DataItem == null)
                {
                    m_DataItem = new XMLVarDataItem();
                    m_DataItem.Parent = this;
                    //SIW529 LinkXMLObjects(m_DataItem);
                    m_DataItem.getFirst();  //SI06420
                }
                return m_DataItem;
            }
            set
            {
                m_DataItem = value;
            }
        }

        public new XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = new XMLLossInfo();
                    ((XMLLossInfo)m_Parent).Salvage = this;
                    LinkXMLObjects((XMLXCNodeBase)m_Parent);   //SIW529
                    ResetParent();
                }
                return m_Parent;
            }
            set
            {
                m_Parent = value;
                ResetParent();
                if (m_Parent != null) //SIW529
                    ((XMLXCNodeBase)m_Parent).LinkXMLObjects(this);    //SIW529
            }
        }

        public override void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        public override XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        public override ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public override XmlNode Node
        {
            get
            {
                if (xmlThisNode == null)
                    xmlThisNode = Utils.getNode(Parent_Node, NodeName);
                return xmlThisNode;
            }
            protected set
            {
                xmlThisNode = value;
                subClearPointers();
            }
        }

        public override ArrayList NodeOrder
        {
            set
            {
                sThisNodeOrder = value;
            }
        }
    }
}
