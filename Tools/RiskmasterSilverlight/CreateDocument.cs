﻿using System;
using System.Net;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.Automation;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;

namespace RiskmasterSL
{
    public class CreateDocument
    { 
        dynamic fso = null;
        dynamic objWord = null;
        dynamic wordDoc = null;
        string strForm = string.Empty;
        string mergeHeaderFileName=string.Empty;
        string mergeFieldHeaders=string.Empty;
        dynamic Editor_Launched;
        string ValidErrorStepCheck = string.Empty;
        string ValidateClaimLetterStep32 = string.Empty;

        [ScriptableMember]
        public string GetDocumentPath(string strFile,string step1,string step2)
        {
            try
            {
                var step="";
            if (AutomationFactory.IsAvailable)
            {
                if(!string.IsNullOrEmpty(step1))
                 step=step1;
                else    
                    step = "parent.CommonValidations.InitPageSettingStep1";
                
                fso = AutomationFactory.CreateObject("Scripting.FileSystemObject");
                if(!string.IsNullOrEmpty(step2))
                 step=step2;
                else    
                step = "parent.CommonValidations.InitPageSettingStep2";
                strForm = fso.GetSpecialFolder(2).Path + "\\" + strFile;
            }
            }
            catch (Exception e)
            {
                //DisplayError(strForm);//commentd by mudabbir
                HtmlPage.Window.Invoke("DisplayErrorfromSilver", strForm);
            }
            // HtmlPage.Window.Invoke("getWinwordpath","xyz" );
           // MessageBox.Show("Hello from silver");
            return strForm;
        }
        
        [ScriptableMember]
        public string StartWord(string formfilecontent, int lDocType,string strFile)
         {
	var adTypeBinary = 1;
    var adSaveCreateOverWrite = 2;
    var adTypeText = 2;
	var step = "";
	bool bSingleField=false;
	var bNoFields = false;
	var mergeHeaderFileName="";
	var mergeFieldHeaders="";
	var createEmptyWordDoc = false;
	  //alert ("hi1");
	//save files to disk
	try
	{
        GetDocumentPath(strFile, "", "");
	    
	    if (IsWordOpen())
	    {

            step = "parent.CommonValidations.ValidCreateTemplateStep1";
		    objWord.Activate();
            return "false";
	    }
        //alert('1');
		//step = 'get template base64 text';
        step = "parent.CommonValidations.ValidCreateTemplateStep2";
	    var template = formfilecontent;
//alert('2');
	    if(template.Length > 0)
	    {
	        //use xml object to convert base64 into array of bytes
	        //step = 'instantiate xml document';
	        step = "parent.CommonValidations.ValidCreateTemplateStep3";
	        dynamic xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
	        //step = 'create b64 element';
            step = "parent.CommonValidations.ValidCreateTemplateStep4";
	        var e = xmlDoc.createElement("b64");
	        //step = 'set element.dataType to bin.base64';
            step = "parent.CommonValidations.ValidCreateTemplateStep5";
	        e.dataType = "bin.base64";
	        //step = 'set element.text to template base64 text';
            step = "parent.CommonValidations.ValidCreateTemplateStep6";
            e.text = template;
            //step = 'get bytes from template element';
            step = "parent.CommonValidations.ValidCreateTemplateStep7";
            var templateBytes = e.nodeTypedValue;
            
            //use ADODB.Stream to save template bytes to disc
            //step = 'instantiate Stream object for template';
             step = "parent.CommonValidations.ValidCreateTemplateStep8";
            dynamic stm = AutomationFactory.CreateObject("ADODB.Stream");
            //step = 'set template stm.Type';
            step = "parent.CommonValidations.ValidCreateTemplateStep9";
            stm.Type = adTypeBinary;
            //step = 'open template stream';
            step = "parent.CommonValidations.ValidCreateTemplateStep10";
            stm.Open();
            //step = 'write bytes to template stream';
            step = "parent.CommonValidations.ValidCreateTemplateStep11";
            stm.Write(templateBytes);
            //step = 'save template stream to file ' + strForm;
            step = "$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep12", "no", "yes", "").ToString() + " " + strForm;// The symbol '$' will be removed in catch block.	
            
            stm.SaveToFile(strForm, adSaveCreateOverWrite);
            //step = 'close stream';
            step = "parent.CommonValidations.ValidCreateTemplateStep13";
            stm.Close();
            stm = null;
            
            e = null;
            xmlDoc = null;
        }
        else
        {
            createEmptyWordDoc = true;
        }
	   
	    //write merge field headers to disk if they exist
        //step = 'get merge field headers';
        step = "parent.CommonValidations.ValidCreateTemplateStep14";
	    //alert ("hi2");
         mergeFieldHeaders=  HtmlPage.Window.Invoke("GenerateMergeFieldHeaderString", null) as string;
	     //alert ("hi3" +mergeFieldHeaders);
	    if(mergeFieldHeaders == "")
	    {
	       bNoFields = true;
	       //alert('For your information, there are no Riskmaster merge fields in this document.');
            HtmlPage.Window.Invoke("AnyMergeFields", null);
	    }
	    else
	    {   //alert ("hi4");
	        mergeHeaderFileName = HtmlPage.Window.Invoke("GetMergeHeaderFileName", null) as string;
	       // alert ("hi5");
	        if (mergeFieldHeaders.IndexOf("\"\t\"") == -1)
	        {
	            //this is done to suppress MS Word prompt asking "what is the record delimiter?"
			    bSingleField = true;
			}
			else
			{
			    //step = 'write merge field headers to disk at ' + mergeHeaderFileName;
                step = "$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep16", "no", "yes", "").ToString() + " " + mergeHeaderFileName;	
	            var objFile = fso.CreateTextFile(mergeHeaderFileName,true);
	            objFile.WriteLine(mergeFieldHeaders);
		        objFile.Write(mergeFieldHeaders);
	            objFile.Close();
	            objFile = null;
	        }
	    }
    }
    catch (Exception e)
    {
        //return DisplayError("The word template and headers could not be saved to this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);		
//        return DisplayError(parent.CommonValidations.ValidCreateTemplateStep17 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);		
        string ValidCreateTemplateStep17 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep17", "no", "yes", "") as string;
        string stepfromJs = string.Empty;
        if (step.Contains("$"))
        {
            step.Replace("$", "");
            stepfromJs = step;
        }
        else
        {
            stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
        }
        ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
        return HtmlPage.Window.Invoke("DisplayErrorfromSilver", ValidCreateTemplateStep17 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs) as string;
    }
    
    //open word and the template file
    try
    {
        //step="The local Microsft Word application required to complete the wizard could not be loaded.\nPlease verify that Microsoft Word is correctly installed.";
        //step = parent.CommonValidations.ValidCreateTemplateStep18).split('\n');
        step = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep18", "no", "yes", "") as string;
        step="$" + step.Split('\n').ToString();
        objWord = AutomationFactory.CreateObject("Word.Application");
        
        dynamic version = objWord.Version.ToString();
        /* tkr this hack is needed for Word 2003 and less (version 11.x and less).  
        not needed for 2007 (version 12.0 and greater).  it does not cause
        an error when called for Word 2007, but does not seem to do anything either */
        //this must occur before document loads!
        decimal decNum = Convert.ToDecimal(version);
        int verNumber = Convert.ToInt16(decNum);
        //bool bvalue = false;
        if (verNumber < 12) //version<12
        {
            //step="The 'Insert Merge Field' toolbar control could not be added to Microsoft Word.\nPlease verify that Microsoft Word is properly installed and click the [Launch Word] button again.";
           // step = (parent.CommonValidations.ValidCreateTemplateStep19).split('\\n').join('\n');
            step = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep19", "no", "yes", "") as string;
            step ="$" + step.Split('\n').ToString();
            var bMergeFieldsVisible =false;
            var ctls = objWord.Commandbars("Mail Merge").Controls;
            for (int i = 1; i < ctls.Count; i++)
            {
                if (ctls(i).Id == 30076)
                {
                    bMergeFieldsVisible = true;
                    break;
                }
            }
            if (bMergeFieldsVisible == false)
            {
                 var myCtl = ctls.Add(10,30076,"",2);
                myCtl.Reset();
            }
        }

        //step = 'Open Word Document ' + strForm;
        step = "$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep20", "no", "yes", "").ToString() + " " + strForm;
        if(createEmptyWordDoc)
        {
            wordDoc = objWord.Documents.Add();
            wordDoc.SaveAs(strForm, 0);
        }
        else
            wordDoc = objWord.Documents.Open(strForm,false,false,false);

        //step="The most recent version of the form is protected from changes.\nPlease unlock it and click the [Launch Word] button again.";
       // step = (parent.CommonValidations.ValidCreateTemplateStep21).split('\\n').join('\n');
         step = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep21", "no", "yes", "") as string;
        step="$"+ step.Split('\n').ToString();
        if( wordDoc.ProtectionType != -1) //if not totally unlocked then
		    wordDoc.UnProtect("");

        //step="The merge fields at " + mergeHeaderFileName + ' could not be added to the document.';
        step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep22", "no", "yes", "").ToString() + " " + mergeHeaderFileName + " " + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep23", "no", "yes", "").ToString();
		wordDoc.Select();
		wordDoc.MailMerge.MainDocumentType = lDocType;

	    if(!bNoFields)
		    if(!bSingleField)
			    wordDoc.MailMerge.OpenDataSource(mergeHeaderFileName,0,1);
		    else
			    wordDoc.MailMerge.CreateDataSource(mergeHeaderFileName,"","",mergeFieldHeaders);

		    //step='select and show document';
        step = "parent.CommonValidations.ValidCreateTemplateStep24";
	    objWord.visible = true;
	    objWord.Activate();	
	    
    }
    catch(Exception e)
    {
        //DisplayError("The word template and headers could not be opened on this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        string ValidCreateTemplateStep25 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep25", "no", "yes", "") as string;
         string stepfromJs = string.Empty;
         if (step.Contains('$'))
         {
             step.Replace("$", "");
             stepfromJs = step;
         }
         else
         {
             stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
         }
        ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
        HtmlPage.Window.Invoke("DisplayErrorfromSilver", ValidCreateTemplateStep25 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
       // DisplayError(parent.CommonValidations.ValidCreateTemplateStep25 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
        if(wordDoc != null)
        {
            try {
                wordDoc.Close();
            } 
            catch(Exception ex)
            {
            }

            objWord = null;
        }
        if(objWord != null)
        {
            try 
            {
                objWord.Quit(0);
            }
            
            catch(Exception ex)
            {
            }
            objWord = null;
        }
    return "false";
    }

    return "false";
         }

        [ScriptableMember]
        public bool IsWordOpen()
        {
            try
            {
                if (objWord.visible != null)
                    return true;
                else
                    return false;
            }
            catch (Exception ex) 
            {
                return false; 
            }
        }

        [ScriptableMember]
        public string Finish_CreateTemplate(string strFile)
        {
           var step = "";
           dynamic xmlDoc;
           dynamic xmlElement;
           dynamic xmlNode;
              //store word file as base64 text in hidden field
    try
    {	
        GetDocumentPath(strFile, "", "");
        if (IsWordOpen() && objWord.Documents.Count == 1) {
        //Aman ML Changes
            //step = 'activate Word';
            step = "parent.CommonValidations.ValidActivateWordStepCheck";
            objWord.Activate();

            //step='get pointer to merged document';
            step = "parent.CommonValidations.ValidPointerMergedDocCheck";
            wordDoc = objWord.Documents.Item(1);

            //step='select, save and close the document';
            step = "parent.CommonValidations.ValidSaveCloseCheck";
            wordDoc.Select();
            wordDoc.Save();   
            wordDoc.Close();           
        }
        
        //if editing, and the user never clicked "Launch Editor", then the
        //file was never written to disk
        if (fso.FileExists(strForm))
        {
            var adTypeBinary = 1;
            //step = 'instantiate Stream object';
            step = "parent.CommonValidations.ValidInstantiateStreamObjCheck";
            dynamic stm = AutomationFactory.CreateObject("ADODB.Stream");
            //step = 'set stm.Type';
            step = "parent.CommonValidations.ValidSetStmTypeCheck";
            stm.Type = adTypeBinary;
            //step = 'open stream';
            step = "parent.CommonValidations.ValidOpenStreamCheck";
            stm.Open();
            //step = 'read file ' + strForm + ' into stream';
            step = "$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidReadFileCheck", "no", "yes", "").ToString() + " " + strForm + " " + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidReadFileStreamCheck", "no", "yes", "").ToString();
            stm.LoadFromFile(strForm);
            //step = 'read bytes from stream';
            step = "parent.CommonValidations.ValidReadBytesStreamCheck";
            var bytes = stm.Read();
            //step = 'close stream';
            step = "parent.CommonValidations.ValidCloseStreamCheck";
            stm.Close();
            stm = null;

            //step = 'instantiate xml document';
            step = "parent.CommonValidations.ValidInstantiateXMLDocCheck";
             xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
            xmlDoc.async = false;
            //step = 'create root element';
            step = "parent.CommonValidations.ValidRootElementCheck";
             xmlElement = xmlDoc.createElement("root");
            xmlDoc.documentElement = xmlElement;
            xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
            //step = 'create element to hold word document';
            step = "parent.CommonValidations.ValidHoldWordDocCheck";
            xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
            xmlNode.dataType = "bin.base64";
            //step = 'read stream into element';
            step = "parent.CommonValidations.ValidReadStreamEltCheck";
            xmlNode.nodeTypedValue = bytes;
            //step = 'read base64 text from element';
            step = "parent.CommonValidations.ValidReadBase64TextCheck";
            var base64 = xmlNode.text; 
            
            //step='cleanup';
            xmlDoc = null;
            xmlElement = null;
            xmlNode = null;
            
            HtmlPage.Window.Invoke("SaveFileContent", new string[] {base64 });
            
        }
    }
    catch (Exception e)
    {
        //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        string ValidLocalCopyCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidLocalCopyCheck", "no", "yes", "") as string;
        string ValidNotSavedCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidNotSavedCheck", "no", "yes", "") as string;
        string stepfromJs = string.Empty;
        if (step.Contains('$'))
        {
            step.Replace("$", "");
            stepfromJs = step;
        }
        else
        {
            stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
        }
        ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
        MessageBox.Show(ValidLocalCopyCheck + " " + strForm + " " + ValidNotSavedCheck + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
        // alert(parent.CommonValidations.ValidLocalCopyCheck + " " + strForm2 + " " + parent.CommonValidations.ValidNotSavedCheck + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
        xmlDoc = null;
        xmlElement = null;
        xmlNode = null;
        return "false";
    }

    
	
    //Clean Up    
    try 
    {
        //step = 'close word';
        step = "parent.CommonValidations.ValidCloseWordCheck";
        if(IsWordOpen())
        {	
            try 
            {
                objWord.Quit(0);
            }
            catch(Exception e)
            {

            }
            objWord = null;
        }
        //step = 'delete local copy of document at ' + strForm;

        step = "$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidDeleteLocalCopyCheck", "no", "yes", "").ToString() + " " + strForm;
        if (fso.FileExists(strForm)) //Main doc temp file.
            fso.DeleteFile(strForm);
		
        var headerFile = HtmlPage.Window.Invoke("GetMergeHeaderFileName", null) as string;
        //step = 'delete header file at ' + headerFile;
        step = "$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidDeleteHeaderFileCheck", "no", "yes", "").ToString() + " " + headerFile;
        if (fso.FileExists(headerFile))
            fso.DeleteFile(headerFile);
    }
    catch (Exception e)
    {
        //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        string ValidLocalCopyCleanUpCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidLocalCopyCleanUpCheck", "no", "yes", "") as string;
        string stepfromJs = string.Empty;
        if (step.Contains('$'))
        {
            step.Replace("$", "");
            stepfromJs = step;
        }
        else
        {
            stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
        }
        ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
        MessageBox.Show(ValidLocalCopyCleanUpCheck + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
        //alert(parent.CommonValidations.ValidLocalCopyCleanUpCheck + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
    }
	
    //post back
    try
    {
        //step="post back to server";
        step = "parent.CommonValidations.ValidPostBackServerCheck";
	
    }
    catch (Exception e)
    {
        //alert("The word document could not be sent back to Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        string ValidErrorCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorCheck", "no", "yes", "") as string;
        string stepfromJs = string.Empty;
        if (step.Contains('$'))
        {
            step.Replace("$", "");
            stepfromJs = step;
        }
        else
        {
            stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
        }

        ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
        MessageBox.Show(ValidErrorCheck + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
       // alert(parent.CommonValidations.ValidErrorCheck + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
        return "false";
    }
	
    return "true";
        }

        [ScriptableMember]
        public string StartEditor(string AX,string strFile,bool saveToDisk)
   {
      string headerFileName = string.Empty;  
      string dataSourceFileName=string.Empty;

      if (AX == "") {
            //tkr 8/2007 HACK.
            //could not figure out how to get the error page to show from here.
            //the xpl logic is incredibly complex and fragile, and throws
            //confusing error message when clicked twice
          GetDocumentPath(strFile,"","");  
        }
        else {
            return "false";
        }

        var adTypeBinary = 1;
        var adSaveCreateOverWrite = 2;
        var adTypeText = 2;
        //var step = 'check IsWordOpen';
        var step = "parent.CommonValidations.CheckWordStatus";

        if (IsWordOpen()) {
            //step = 'activate Word';
            step = "parent.CommonValidations.MergeStartEditorStep1";
            objWord.Activate();
            return "false";
        }

        try {//save template and datasource local
            //use DomDocument to convert base64 string to bytes	
            //step = 'get template base64 text';
            step = "parent.CommonValidations.ValidCreateTemplateStep2";
            //var template = (document.getElementById('hdnRTFContent').value);
            //var template= HtmlPage.Window.Invoke("getAndSetValues", new string[] {""+"'"+"(document.getElementById('hdnRTFContent').value)"+"'"+"" }); 
            var template= HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnRTFContent')","no","yes","") as string; 
            if (template == null || template == "")
                throw new Exception(HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.MergeTemplateError", "no", "yes", "").ToString());

            //step = 'get data source base64';
            step = "parent.CommonValidations.GetDataSourceBase64";
            //var dataSource = (document.getElementById('hdnDataSource').value);
            var dataSource = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnDataSource')", "no", "yes", "") as string;
            
            if (dataSource == null || dataSource == "")
                throw new Exception(HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.MergeDataSourceError", "no", "yes", "").ToString());
            
            //step = 'get header file base64';
            step = "parent.CommonValidations.GetHeaderFileBase64";
            //var headerContent = (document.getElementById('hdnHeaderContent').value);
            var headerContent = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnHeaderContent')","no","yes","") as string;
             
            if (dataSource == null || headerContent == "")
                throw new Exception(HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.MergeHeaderError", "no", "yes", "").ToString());// 

            //step = 'instantiate xml document for template';
            step = "parent.CommonValidations.InstantiateXMLDoc";
            var xmlDoc =AutomationFactory.CreateObject("MSXML2.DOMDocument"); 
            //step = 'create b64 element for template';
            step = "parent.CommonValidations.Createb64Element";
            var e = xmlDoc.createElement("b64");
            //step = 'set element.dataType to bin.base64 for template';
            step = "parent.CommonValidations.SetElementDataType";
            e.dataType = "bin.base64";

            //step = 'set element.text to template base64 text';
            step = "parent.CommonValidations.ValidCreateTemplateStep6";
            e.text = template;
            //step = 'get bytes from template element';
            step = "parent.CommonValidations.ValidCreateTemplateStep7";
            var templateBytes = e.nodeTypedValue;

            //use ADODB.Stream to save template bytes to disc
            //step = 'instantiate Stream object for template';
            step = "parent.CommonValidations.ValidCreateTemplateStep8";
            var stm =  AutomationFactory.CreateObject("ADODB.Stream");
            //step = 'set template stm.Type';
            step = "parent.CommonValidations.ValidCreateTemplateStep9";
            stm.Type = adTypeBinary;
            //step = 'open template stream';
            step = "parent.CommonValidations.ValidCreateTemplateStep10";
            stm.Open();
            //step = 'write bytes to template stream';
            step = "parent.CommonValidations.ValidCreateTemplateStep11";
            stm.Write(templateBytes);
            //step = 'save template stream to file ' + strForm;
            step ="$" + Convert.ToString(HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidCreateTemplateStep12", "no", "yes", "")) + " " + strForm;
            stm.SaveToFile(strForm, adSaveCreateOverWrite);
            //step = 'close stream';
            step = "parent.CommonValidations.ValidCreateTemplateStep13";
            stm.Close();
            stm = null;

            //step = 'instantiate xml document for datasource';
            step = "parent.CommonValidations.InstantiateXMLDocDataSource";
            xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
            //step = 'create b64 element for datasource';
            step = "parent.CommonValidations.Createb64ElementDataSource";
            e = xmlDoc.createElement("b64");
            //step = 'set element.dataType to bin.base64 for datasource';
            step = "parent.CommonValidations.SetElementDataTypeDataSource";
            e.dataType = "bin.base64";

            //step = 'set element.text to datasource base64 text';
            step = "parent.CommonValidations.SetElementText";
            e.text = dataSource;
            //step = 'get bytes from datasource element';
            step = "parent.CommonValidations.GetBytesDataSource";
            var dataSourceBytes = e.nodeTypedValue;

            //use ADODB.Stream to save datasource to disk
             dataSourceFileName = HtmlPage.Window.Invoke("GetDataSourceFileName", null) as string;
            //step = 'instantiate Stream object for data source';
            step = "parent.CommonValidations.InstantiateStreamObj";
            stm = AutomationFactory.CreateObject("ADODB.Stream");
            //step = 'set datasource stm.Type';
            step = "parent.CommonValidations.SetDataSourceSTMType";
            stm.Type = adTypeBinary;
            //step = 'open datasource stream';
            step = "parent.CommonValidations.OpenDataSourceStream";
            stm.Open();
            //step = 'write bytes to datasource stream';
            step = "parent.CommonValidations.WriteDataSourceStream";
            stm.Write(dataSourceBytes);
            //step = 'save datasource stream to file ' + dataSourceFileName;
            step = "$"+ Convert.ToString(HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.SaveDataSourceStream", "no", "yes", "")) + " " + dataSourceFileName;
            stm.SaveToFile(dataSourceFileName, adSaveCreateOverWrite);
            //step = 'close stream';
            step = "parent.CommonValidations.ValidCloseStreamCheck";
            stm.Close();
            stm = null;

            //step = 'instantiate xml document for header';
            step = "parent.CommonValidations.InstantiateXMLDocHeader";
            xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
            //step = 'create b64 element for header';
            step = "parent.CommonValidations.Createb64ElementHeader";
            e = xmlDoc.createElement("b64");
            //step = 'set element.dataType to bin.base64 for header';
            step = "parent.CommonValidations.SetElementDataTypeHeader";
            e.dataType = "bin.base64";

            //step = 'set element.text to header base64 text';
            step = "parent.CommonValidations.SetElementTextHeader";
            e.text = headerContent;
            //step = 'get bytes from header element';
            step = "parent.CommonValidations.GetBytesHeader";
            var headerBytes = e.nodeTypedValue;

            //use ADODB.Stream to save header to disk
            
            if (strForm.Length > 4) {
                headerFileName = strForm.Substring(0, strForm.Length - 4) + ".hdr";
                //step = 'instantiate Stream object for data source';
                step = "parent.CommonValidations.InstantiateStreamObj";
                stm = AutomationFactory.CreateObject("ADODB.Stream");
                //step = 'set datasource stm.Type';
                step = "parent.CommonValidations.SetDataSourceSTMType";
                stm.Type = adTypeBinary;
                //step = 'open datasource stream';
                step = "parent.CommonValidations.OpenDataSourceStream";
                stm.Open();
                //step = 'write bytes to datasource stream';
                step = "parent.CommonValidations.WriteDataSourceStream";
                stm.Write(headerBytes);
                //step = 'save datasource stream to file ' + headerFileName;
                step = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.SaveDataSourceStream", "no", "yes", "").ToString() + " " + headerFileName;
                stm.SaveToFile(headerFileName, adSaveCreateOverWrite);
                //step = 'close stream';
                step = "parent.CommonValidations.ValidCloseStreamCheck";
                stm.Close();
                stm = null;
            }
            e = null;
            xmlDoc = null;
        }
        catch (Exception e) {
            //return DisplayError("The word template and data source could not be saved to this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
            string WordMergeTempError = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTempError", "no", "yes", "") as string;
            string stepfromJs = string.Empty;
            if (step.Contains('$'))
            {
                step.Replace("$", "");
                stepfromJs = step;
            }
            else
            {
                stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
            }
            ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
            return HtmlPage.Window.Invoke("DisplayErrorfromSilver", WordMergeTempError + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs) as string;

          
        }

        try {
            //In the future if Office 2003 support is not required, should call WordMerge4Vesion11Lower12Higher
            //and change data source content.
            //Function call Changed by pawan for mits 10983 reverted back to routine used by Tom since few properties worked well well with it.
            
          
            WordMerge4Vesion12Higher(strForm, headerFileName, dataSourceFileName, saveToDisk);
            //objWord.FileSaveAs(strForm);
        }
        catch (Exception e) {
            //return DisplayError("The local Microsft Word application required to complete the wizard could not be loaded because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
            string WordApplicationError = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordApplicationError", "no", "yes", "") as string;
            string stepfromJs = string.Empty;
            if (step.Contains('$'))
            {
                step.Replace("$", "");
                stepfromJs = step;
            }
            else
            {
                stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
            }
            ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
            return HtmlPage.Window.Invoke("DisplayErrorfromSilver", WordApplicationError + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs) as string;
           // return DisplayError(parent.CommonValidations.WordApplicationError + " " + e.InnerException + "," + e.Message + "'\r\n'" + "parent.CommonValidations.ValidErrorStepCheck" + " " + step);
        }

         Editor_Launched = 1;
         HtmlPage.Window.Invoke("getAndSetValues", Editor_Launched,"no","no","");
         return "false";
        }

        [ScriptableMember]
        public bool OpenWord()
  {
      var step = "";
        try {//open template
                //step = 'open word document: ' + strForm;

            step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.OpenWordDocument", "no", "yes", "").ToString() + " " + strForm;
                objWord = AutomationFactory.CreateObject("Word.Application");
                wordDoc = objWord.Documents.Open(strForm);

                //Try to save the document to make MSWord's SaveDate function work
                objWord.ActiveDocument.Saveas(strForm);
            }
            catch (Exception e) {
                //DisplayError("The word template at " + strForm + " could not be opened by Microsft Word because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string WordMergeTemplateErr1 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr1", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
                HtmlPage.Window.Invoke("DisplayErrorfromSilver", WordMergeTemplateErr1 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
              //  DisplayError(parent.CommonValidations.WordMergeTemplateErr1 + " " + strForm + " " + parent.CommonValidations.WordMergeTemplateErr2 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
                if (objWord != null) {
                    objWord.Quit(0);
                    objWord = null;
                    return false;
                }
            }
        return true;
  }

        [ScriptableMember]
        public string FinishOpenMerge(bool saveToTemp,int Editor_Launched,string strFile)
  {

     dynamic objWordTemp, xmlDoc,xmlElement ,xmlNode;
      dynamic wordTempDoc="";
       
        var step = "";
            //var wordTempDoc="";
            var isProtected=false;//added by mudabbir
//            var objWordTemp = null;
            string dataSourceFileName =string.Empty;
            GetDocumentPath(strFile, "", "");
            if (Editor_Launched == 0) {
                //alert(parent.CommonValidations.MustClickLaunchBtn);//mudabbir commented
                string MustClickLaunchBtn = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidLocalCopyCheck", "no", "yes", "") as string;
                MessageBox.Show(MustClickLaunchBtn);
                //alert("You must click the 'Launch Merge' button and complete the merge before finalizing.");
                return "false";
            }
            if (saveToTemp) {
                
                 objWordTemp = AutomationFactory.CreateObject("Word.Application");
                try
                {//open template
                    //step = 'open word document: ' + strForm;
                    step = "$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.OpenWordDocument", "no", "yes", "").ToString() + ' ' + strForm;
                   
                   wordTempDoc = objWordTemp.Documents.Open(strForm);

                    //Try to save the document to make MSWord's SaveDate function work
                    objWordTemp.ActiveDocument.Saveas(strForm);
                }
                catch (Exception e) {
                    //DisplayError(parent.CommonValidations.WordMergeTemplateErr1 + " " + sFileName + " " + parent.CommonValidations.WordMergeTemplateErr2 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);  //mudabbir commented
                    string WordMergeTemplateErr1 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr1", "no", "yes", "") as string;
                    string stepfromJs = string.Empty;
                    if (step.Contains('$'))
                    {
                        step.Replace("$", "");
                        stepfromJs = step;
                    }
                    else
                    {
                        stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                    }
                    ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
                    HtmlPage.Window.Invoke("DisplayErrorfromSilver", WordMergeTemplateErr1 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
                    //DisplayError("The word template at " + strForm + " could not be opened by Microsft Word because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                    if (objWordTemp != null) {
                        objWordTemp.Quit(0);
                        objWordTemp = null;
                        return "false";
                    }
                }

                try {//Verify we have full edit rights.	Isprotected added by pawan for 10983								   
                    if (objWordTemp.ActiveDocument.ProtectionType != -1) //if not totally unlocked then
                    {
                        objWordTemp.ActiveDocument.UnProtect("");
                        isProtected = true;
                    }
                }
                catch (Exception e) {
                    //alert("The word template at " + strForm + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
                    string WordMergeTemplateErr1 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr1", "no", "yes", "") as string;
                    string WordMergeTemplateErr3 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr3", "no", "yes", "") as string;
                    MessageBox.Show(WordMergeTemplateErr1 + " " + strForm + " " + WordMergeTemplateErr3.Split('\n'));
                    //alert(parent.CommonValidations.WordMergeTemplateErr1 + " " + sFileName + " " + (parent.CommonValidations.WordMergeTemplateErr3).split('\\n').join('\n'));   //Swati 31/01/2013
                    objWordTemp.visible = false;
                    objWordTemp.Activate();
                }

                try {
                    //step = 'activate Word';
                    step = "parent.CommonValidations.ValidActivateWordStepCheck";
                    objWordTemp.visible = true;
                    objWordTemp.Activate();
                    wordTempDoc.Select();  //this call instantiates MailMerge object
                    //step = 'open header';
                    step = "parent.CommonValidations.OpenHeader";
                   string headerFileName = strForm.Substring(0, strForm.Length - 4) + ".hdr";
                    if (headerFileName.Length > 0) {
                        wordTempDoc.MailMerge.OpenHeaderSource(headerFileName);
                    }
                    //step = 'open data source';
                    step = "parent.CommonValidations.OpenDataSource"; 
                    dataSourceFileName = HtmlPage.Window.Invoke("GetDataSourceFileName", null) as string;
                    wordTempDoc.MailMerge.OpenDataSource(dataSourceFileName);
                    wordTempDoc.MailMerge.DataSource.FirstRecord = 1;

                    //Get number of records to be merged
                    wordTempDoc.MailMerge.DataSource.LastRecord = 1;
                    //var objRecordNumber = document.getElementById("hdnNumRecords");
                    var objRecordNumber=  HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnNumRecords')","no","yes","") as string;
                    
                    if (objRecordNumber != null) {
                        var sRecordNumber = objRecordNumber;
                        //if (!isNaN(sRecordNumber)) { //mudabbir commented
                            wordTempDoc.MailMerge.DataSource.LastRecord = int.Parse(sRecordNumber);
                        //}
                    }
                    wordTempDoc.MailMerge.Destination = 0;

                    //step = 'do mail merge';
                    step = "parent.CommonValidations.DoMailMerge";
                    wordTempDoc.MailMerge.Execute(false);
                    //step = 'select merged document';
                    step = "parent.CommonValidations.SelectMergedDoc";
                       //var mergedDoc = objWord.Documents.Item(1);


                    //step = 'close the original';
                    step = "parent.CommonValidations.CloseOriginal";
                    wordTempDoc.Close();

                    //step = 'select merged document';
                    step = "parent.CommonValidations.SelectMergedDoc";

                    objWordTemp.ActiveDocument.Select();

                    //step = 'save the merged document over the original at ' + strForm;
                    step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.SaveMergedDoc", "no", "yes", "").ToString() + ' ' + strForm; 
                    objWordTemp.ActiveDocument.Saveas(strForm);
                    objWordTemp.Quit(0);
                    objWordTemp = null;
                    HtmlPage.Window.Invoke("sleep",2000);
//                    sleep(2000);//replaced this with above line

                }
                catch (Exception e) {
                   // DisplayError(parent.CommonValidations.MSWordError + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step); // mudabbir commed
                    string MSWordError = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.MSWordError", "no", "yes", "") as string;
                    string stepfromJs = string.Empty;
                    if (step.Contains('$'))
                    {
                        step.Replace("$", "");
                        stepfromJs = step;
                    }
                    else
                    {
                        stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                    }
                    ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
                    HtmlPage.Window.Invoke("DisplayErrorfromSilver", MSWordError + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
                    //DisplayError("Microsoft Word could not accomplish the merge because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                    objWordTemp.Quit(0);
                    objWordTemp = null;
                    return "false";
                }
            }

            try {

                if (IsWordOpen() && objWord.Documents.Count == 1) {
                    //step = 'activate Word';
                    step = "MergeClaimLetterValidations.FinishOpenStep1";
                    objWord.Activate();

                    //step = 'get pointer to merged document';
                    step = "MergeClaimLetterValidations.FinishOpenStep2";
                    var wordDoc = objWord.Documents.Item(1);

                    //step = 'select, save and close the document';
                    step = "MergeClaimLetterValidations.FinishOpenStep3";
                    wordDoc.Select();
                    wordDoc.Save();
                    wordDoc.Close();
                }

                var adTypeBinary = 1;
                //step = 'instantiate Stream object';
                step = "MergeClaimLetterValidations.FinishOpenStep4";
                dynamic stm = AutomationFactory.CreateObject("ADODB.Stream");
                //step = 'set stm.Type';
                step = "MergeClaimLetterValidations.FinishOpenStep5";
                stm.Type = adTypeBinary;
                //step = 'open stream';
                step = "MergeClaimLetterValidations.FinishOpenStep6";
                stm.Open();
                //step = 'read file ' + strForm + ' into stream';
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.FinishOpenStep7", "no", "yes", "").ToString() + ' ' + strForm + ' ' + HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.FinishOpenStep8", "no", "yes", "").ToString();
                stm.LoadFromFile(strForm);
                //step = 'read bytes from stream';
                step = "MergeClaimLetterValidations.FinishOpenStep9";
                var bytes = stm.Read();
                //step = 'close stream';
                step = "MergeClaimLetterValidations.FinishOpenStep10";
                stm.Close();
                stm = null;

                //step = 'instantiate xml document';
                step = "parent.CommonValidations.ValidCreateTemplateStep3";
                 xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
                xmlDoc.async = false;
                //step = 'create root element';
                step = "parent.CommonValidations.ValidRootElementCheck";
                 xmlElement = xmlDoc.createElement("root");
                xmlDoc.documentElement = xmlElement;
                xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
                //step = 'create element to hold word document';
                step = "MergeClaimLetterValidations.FinishOpenStep11";
                 xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
                xmlNode.dataType = "bin.base64";
                //step = 'read stream into element';
                step = "MergeClaimLetterValidations.FinishOpenStep12";
                xmlNode.nodeTypedValue = bytes;
                //step = 'read base64 text from element';
                step = "MergeClaimLetterValidations.FinishOpenStep13";
                var base64 = xmlNode.text;

                //step = 'cleanup';
                step = "MergeClaimLetterValidations.FinishOpenStep14";
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;

                //document.all.hdnNewFileName.value = strForm;
                 HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnNewFileName","yes","no",strForm);
                //document.all.hdnRTFContent.value = base64;
                HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnRTFContent","yes","no",base64);
                //document.all.firstTime.value = "";
                HtmlPage.Window.Invoke("getAndSetValues", "document.all.firstTime","yes","no","");

            }
            catch (Exception e) {
               // alert(MergeClaimLetterValidations.FinishOpenStepError1 + " " + strForm + " " + MergeClaimLetterValidations.FinishOpenStepError2 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step); // Swati 31/01/2013 // Mudabbir commented
                string FinishOpenStepError1 = HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.FinishOpenStepError1", "no", "yes", "") as string;
                string FinishOpenStepError2 = HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.FinishOpenStepError2", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                 {
                      step.Replace("$", "");
                      stepfromJs = step;
                 }
               else
               {
                stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
               }
                ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
                MessageBox.Show(FinishOpenStepError1 + " " + strForm + " " + FinishOpenStepError2 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
                //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
                return "false";
            }

            //Clean Up    
            try {
                //step = 'close word';
                step = "MergeClaimLetterValidations.FinishOpenStep15";
                if (IsWordOpen()) {
                    objWord.Quit(0);
                    objWord = null;
                }

                //step = 'delete local copy of document at ' + strForm;
                step = "$"+ HtmlPage.Window.Invoke("getAndSetValues",  "parent.CommonValidations.ValidDeleteLocalCopyCheck", "no", "yes", "").ToString() + ' ' + strForm;
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);

                //dataSourceFileName = GetDataSourceFileName();
                dataSourceFileName = HtmlPage.Window.Invoke("GetDataSourceFileName", null) as string;
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.FinishOpenStep16", "no", "yes", "").ToString() + ' ' + dataSourceFileName;
                //step = 'delete data source file at ' + dataSourceFileName;
                if (fso.FileExists(dataSourceFileName))
                    fso.DeleteFile(dataSourceFileName);
            }
            catch (Exception e) {
               // alert(parent.CommonValidations.NoLocalCopyClean + " " + e.name + ',' + e.message + '\r\n' + MergeClaimLetterValidations.FinishOpenStep17 + " " + step); // Swati 30/01/2013
                string NoLocalCopyClean = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.NoLocalCopyClean", "no", "yes", "") as string;
                string FinishOpenStep17 = HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.FinishOpenStep17", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                MessageBox.Show(NoLocalCopyClean + " " + e.GetType() + ',' + e.Message + "\r\n" + FinishOpenStep17 + " " + stepfromJs);
                //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
            }

            //document.getElementById('hdnaction').value="AttachAndFinish";
            return "true";
   
  }

        [ScriptableMember]
        public string StartClmEditor(string strFile)
  {
          GetDocumentPath(strFile,"","");
            var adTypeBinary = 1;
            var adSaveCreateOverWrite = 2;
            var adTypeText = 2;
            dynamic SplitData1=null, SplitData2=null, SplitData3=null, SplitData4=null, SplitData5=null, SplitData6=null, SplitData7=null, SplitData8=null, sSecFileName=null, headerFileName=null,password=null;
            //var step = 'check IsWordOpen';
            var step = "MergeClaimLetterValidations.ValidateClaimLetterStep1";

            if (IsWordOpen()) {
                //step = 'activate Word';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep34";
                objWord.Activate();
                return "false";
            }

            try {//save template and datasource local
                //use DomDocument to convert base64 string to bytes	
                //step = 'get template base64 text';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep2";
                var template = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnRTFContent')","no","yes","") as string; 
                
                
                if (template == null || template == "")
                    throw new Exception(HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep3", "no", "yes", "").ToString());// 
                //step = 'get data source base64';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep4";
                var dataSource = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnDataSource')","no","yes","") as string; 
                
                if (dataSource == null || dataSource == "")
                    throw new Exception(HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep5", "no", "yes", "").ToString());// 
                //step = 'get header file base64';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep6";
                var headerContent =  HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnHeaderContent')","no","yes","") as string; 
               
                if (dataSource == null || headerContent == "")
                    throw new Exception(HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep7", "no", "yes", "").ToString());
                //document.all.hdnLetterGenerated.value = -1;
                HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnLetterGenerated","yes","no",-1);

                //var password = (document.getElementById('hdnPassword').value);
                password= HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnPassword')","no","yes","") as string; 

                //var sSecFileName = (document.getElementById('hdnSecFileName').value);
                 sSecFileName  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnSecFileName')","no","yes","") as string; 
               // var numrecords = (document.getElementById('hdnNumRecords').value);
                var numrecords  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnNumRecords')","no","yes","");

                //var SplitData1 = (document.getElementById('hdnData4Lv1').value);
                SplitData1  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnData4Lv1')","no","yes","") as string; 
                //var SplitData2 = (document.getElementById('hdnData4Lv2').value);
                SplitData2  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnData4Lv2')","no","yes","") as string; 
                //var SplitData3 = (document.getElementById('hdnData4Lv3').value);
                 SplitData3  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnData4Lv3')","no","yes","") as string; 
                //var SplitData4 = (document.getElementById('hdnData4Lv4').value);
                 SplitData4  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnData4Lv4')","no","yes","") as string; 
                //var SplitData5 = (document.getElementById('hdnData4Lv5').value);
                 SplitData5  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnData4Lv5')","no","yes","") as string; 
                //var SplitData6 = (document.getElementById('hdnData4Lv6').value);
                 SplitData6  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnData4Lv6')","no","yes","") as string; 
                //var SplitData7 = (document.getElementById('hdnData4Lv7').value);
                 SplitData7  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnData4Lv7')","no","yes","") as string; 
                //var SplitData8 = (document.getElementById('hdnData4Lv8').value);
                 SplitData8  = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnData4Lv8')","no","yes","") as string; 

                if ((SplitData1 == "") && (SplitData2 == "") && (SplitData3 == "") && (SplitData4 == "") && (SplitData5 == "") && (SplitData6 == "") && (SplitData7 == "") && (SplitData8 == ""))
                    //throw new Error("The word merge data source was not passed to the browser, please check the RMX web errors server error log for an error message.  The most likely cause of this error is that the word merge header file (*.hdr file) could not be found in document storage, or the application could not interpret a merge field name.");
                   // throw new Error(MergeClaimLetterValidations.ValidateClaimLetterStep8); //mudabbir commented
                    throw new Exception(HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep8", "no", "yes", "").ToString());
                if (password == "")
                    //throw new Error("Email Claim Letter Password is not set at any level of Organization Hierarchy. No Claim Letters generated.")
                    //throw new Error(MergeClaimLetterValidations.ValidateClaimLetterStep9); //mudabbir commented
                    throw new Exception(HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep9", "no", "yes", "").ToString());
                //step = 'instantiate xml document for template';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep10";
                var xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
                //step = 'create b64 element for template';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep11";
                var e = xmlDoc.createElement("b64");
                //step = 'set element.dataType to bin.base64 for template';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep12";
                e.dataType = "bin.base64";

                //step = 'set element.text to template base64 text';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep13";
                e.text = template;
                //step = 'get bytes from template element';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep14";
                var templateBytes = e.nodeTypedValue;

                //use ADODB.Stream to save template bytes to disc
                //step = 'instantiate Stream object for template';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep15";
                var stm = AutomationFactory.CreateObject("ADODB.Stream");
                //step = 'set template stm.Type';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep16";
                stm.Type = adTypeBinary;
                //step = 'open template stream';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep17";
                stm.Open();
                //step = 'write bytes to template stream';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep18";
                stm.Write(templateBytes);
                //step = 'save template stream to file ' + strForm;
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep19", "no", "yes", "").ToString() + " " + strForm;
                stm.SaveToFile(strForm, adSaveCreateOverWrite);
                //step = 'close stream';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep20";
                stm.Close();
                stm = null;



                //step = 'instantiate xml document for header';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep21";
                xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
                //step = 'create b64 element for header';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep22";
                e = xmlDoc.createElement("b64");
                //step = 'set element.dataType to bin.base64 for header';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep23";
                e.dataType = "bin.base64";

                //step = 'set element.text to header base64 text';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep24";
                e.text = headerContent;
                //step = 'get bytes from header element';
                step = "MergeClaimLetterValidations.ValidateClaimLetterStep25";
                var headerBytes = e.nodeTypedValue;

                //use ADODB.Stream to save header to disk
                if (strForm.Length > 4) {
                    headerFileName = strForm.Substring(0, strForm.Length - 4) + ".hdr";
                    //step = 'instantiate Stream object for data source';
                    step = "MergeClaimLetterValidations.ValidateClaimLetterStep26";
                    stm = AutomationFactory.CreateObject("ADODB.Stream");
                    //step = 'set datasource stm.Type';
                    step = "MergeClaimLetterValidations.ValidateClaimLetterStep27";
                    stm.Type = adTypeBinary;
                    //step = 'open datasource stream';
                    step = "MergeClaimLetterValidations.ValidateClaimLetterStep28";
                    stm.Open();
                    //step = 'write bytes to datasource stream';
                    step = "MergeClaimLetterValidations.ValidateClaimLetterStep29";
                    stm.Write(headerBytes);
                    //step = 'save datasource stream to file ' + headerFileName;
                    step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep30", "no", "yes", "").ToString() + " " + headerFileName;
                    stm.SaveToFile(headerFileName, adSaveCreateOverWrite);
                    //step = 'close stream';
                    step = "MergeClaimLetterValidations.ValidateClaimLetterStep20";
                    stm.Close();
                    stm = null;
                }
                e = null;
                xmlDoc = null;
            }
            catch (Exception e) {
                //document.all.hdnLetterGenerated.value = 0;
                HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnLetterGenerated","yes","no",0);
                //return DisplayError("The word template and data source could not be saved to this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string ValidateClaimLetterStep31 = HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep31", "no", "yes", "") as string;
                if(string.Compare(ValidateClaimLetterStep32,"")==0)
                ValidateClaimLetterStep32 = HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep32", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                return HtmlPage.Window.Invoke("DisplayErrorfromSilver", ValidateClaimLetterStep31 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidateClaimLetterStep32 + " " + stepfromJs) as string;
                //return DisplayError(MergeClaimLetterValidations.ValidateClaimLetterStep31 + " " + e.name + ',' + e.message + '\r\n' + MergeClaimLetterValidations.ValidateClaimLetterStep32 + " " + step);//mudabbir commted
            }

            try {
                //added by navdeep    
                var dataFileName = "";
                var secFile = "";
                var dataSourceFileName = "";
                //function PrepareDataSource(dataSource, iNum)
                //if( numrecords > 1)
                //{
                for (var iCtr = 1; iCtr <= 8; iCtr = iCtr + 1) {
                    if ((SplitData1 != "") && iCtr == 1) {
                       // dataFileName = GetDataFileName(sSecFileName, 1);
                        dataFileName  = HtmlPage.Window.Invoke("GetDataFileName", sSecFileName,1) as string;
                       // secFile = GetSecFileName(sSecFileName, 1);
                        secFile  = HtmlPage.Window.Invoke("GetSecFileName", sSecFileName,1) as string;
                        dataSourceFileName = PrepareDataSource(SplitData1, 1);
                        
                        WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName, secFile, password);
                        //document.all.hdnTitle1.value = "Department"
                        //if (fso.FileExists(dataFileName))                     
        

                          // document.all.hdnRTFContent1.value = ConvertLetterToStream(dataFileName,false);
       HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnRTFContent1')", "yes", "no", ConvertLetterToStream(dataFileName, false));

   
                        //if (fso.FileExists(secFile))
                       // document.all.hdnSecureRTFContent1.value = ConvertLetterToStream(secFile,false);
       HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnSecureRTFContent1')", "yes", "no", ConvertLetterToStream(secFile, false));
                    }
                    if ((SplitData2 != "") && iCtr == 2) {
                        
                       // dataFileName = GetDataFileName(sSecFileName, 2);
                        dataFileName  = HtmlPage.Window.Invoke("GetDataFileName", sSecFileName,2) as string;
                       // secFile = GetSecFileName(sSecFileName, 2);
                        secFile  = HtmlPage.Window.Invoke("GetSecFileName", sSecFileName,2) as string;
                        //dataSourceFileName = PrepareDataSource(SplitData2, 2, Editor_Launched, xmlDoc);//mudabbir commeted and add below line
                        dataSourceFileName = PrepareDataSource(SplitData2, 2);//
                        WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName, secFile, password);
                        //document.all.hdnTitle2.value = "Facility"
                        //document.all.hdnRTFContent2.value = ConvertLetterToStream(dataFileName,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnRTFContent2')", "yes", "no", ConvertLetterToStream(dataFileName, false));
                        //document.all.hdnSecureRTFContent2.value = ConvertLetterToStream(secFile,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnSecureRTFContent2')", "yes", "no", ConvertLetterToStream(secFile, false));
                    }

                    if ((SplitData3 != "") && iCtr == 3) {
                        
                       // dataFileName = GetDataFileName(sSecFileName, 3);
                        dataFileName  = HtmlPage.Window.Invoke("GetDataFileName", sSecFileName,3) as string;
                       // secFile = GetSecFileName(sSecFileName, 3);
                        secFile  = HtmlPage.Window.Invoke("GetSecFileName", sSecFileName,3) as string;
                        dataSourceFileName = PrepareDataSource(SplitData3, 3);
                        WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName, secFile, password);
                        //document.all.hdnTitle3.value = "Location"
                       // document.all.hdnRTFContent3.value = ConvertLetterToStream(dataFileName,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnRTFContent3')", "yes", "no", ConvertLetterToStream(dataFileName, false));
                       // document.all.hdnSecureRTFContent3.value = ConvertLetterToStream(secFile,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnSecureRTFContent3')", "yes", "no", ConvertLetterToStream(secFile, false));
                    }
                    if ((SplitData4 != "") && iCtr == 4) {

                        // dataFileName = GetDataFileName(sSecFileName, 4);
                        dataFileName = HtmlPage.Window.Invoke("GetDataFileName", sSecFileName, 4) as string;
                        // secFile = GetSecFileName(sSecFileName, 4);
                        secFile = HtmlPage.Window.Invoke("GetSecFileName", sSecFileName, 4) as string;
                        dataSourceFileName = PrepareDataSource(SplitData4, 4);
                        WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName, secFile, password);
                        //document.all.hdnTitle4.value = "Division"
                       // document.all.hdnRTFContent4.value = ConvertLetterToStream(dataFileName,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnRTFContent4')", "yes", "no", ConvertLetterToStream(dataFileName, false));
                       // document.all.hdnSecureRTFContent4.value = ConvertLetterToStream(secFile,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnSecureRTFContent4')", "yes", "no", ConvertLetterToStream(secFile, false));
                    }
                    if ((SplitData5 != "") && iCtr == 5) {

                        // dataFileName = GetDataFileName(sSecFileName, 5);
                        dataFileName = HtmlPage.Window.Invoke("GetDataFileName", sSecFileName, 5) as string;
                        // secFile = GetSecFileName(sSecFileName, 5);
                        secFile = HtmlPage.Window.Invoke("GetSecFileName", sSecFileName, 5) as string;
                        dataSourceFileName = PrepareDataSource(SplitData5, 5);
                        WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName, secFile, password);
                        //document.all.hdnTitle5.value = "Region"
                       // document.all.hdnRTFContent5.value = ConvertLetterToStream(dataFileName,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnRTFContent5')", "yes", "no", ConvertLetterToStream(dataFileName, false));
                       // document.all.hdnSecureRTFContent5.value = ConvertLetterToStream(secFile,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnSecureRTFContent5')", "yes", "no", ConvertLetterToStream(secFile, false));
                    }
                    if ((SplitData6 != "") && iCtr == 6) {

                        // dataFileName = GetDataFileName(sSecFileName, 6);
                        dataFileName = HtmlPage.Window.Invoke("GetDataFileName", sSecFileName, 6) as string;
                        // secFile = GetSecFileName(sSecFileName, 6);
                        secFile = HtmlPage.Window.Invoke("GetSecFileName", sSecFileName, 6) as string;
                        dataSourceFileName = PrepareDataSource(SplitData6, 6);
                        WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName, secFile, password);
                        //  document.all.hdnTitle6.value = "Operation"
                        //document.all.hdnRTFContent6.value = ConvertLetterToStream(dataFileName,false);
                        HtmlPage.Window.Invoke("getAndSetValues","document.getElementById('hdnRTFContent6')", "yes", "no", ConvertLetterToStream(dataFileName, false));
                      //  document.all.hdnSecureRTFContent6.value = ConvertLetterToStream(secFile,false);
                        HtmlPage.Window.Invoke("getAndSetValues","document.getElementById('hdnSecureRTFContent6')", "yes", "no", ConvertLetterToStream(secFile, false));
                    }
                    if ((SplitData7 != "") && iCtr == 7) {

                        // dataFileName = GetDataFileName(sSecFileName, 7);
                        dataFileName = HtmlPage.Window.Invoke("GetDataFileName", sSecFileName, 7) as string;
                        // secFile = GetSecFileName(sSecFileName, 7);
                        secFile = HtmlPage.Window.Invoke("GetSecFileName", sSecFileName, 7) as string;
                        dataSourceFileName = PrepareDataSource(SplitData7, 7);
                        WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName, secFile, password);
                        //  document.all.hdnTitle7.value = "Company"
                        //document.all.hdnRTFContent7.value = ConvertLetterToStream(dataFileName,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnRTFContent7')", "yes", "no", ConvertLetterToStream(dataFileName, false));
                       // document.all.hdnSecureRTFContent7.value = ConvertLetterToStream(secFile,false);
                        HtmlPage.Window.Invoke("getAndSetValues","document.getElementById('hdnSecureRTFContent7')", "yes", "no", ConvertLetterToStream(secFile, false));
                    }
                    if ((SplitData8 != "") && iCtr == 8) {

                        // dataFileName = GetDataFileName(sSecFileName, 8);
                        dataFileName = HtmlPage.Window.Invoke("GetDataFileName", sSecFileName, 8) as string;
                        // secFile = GetSecFileName(sSecFileName, 8);
                        secFile = HtmlPage.Window.Invoke("GetSecFileName", sSecFileName, 8) as string;
                        dataSourceFileName = PrepareDataSource(SplitData8, 8);
                        WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName, secFile, password);
                        // document.all.hdnTitle8.value = "Client"
                        //document.all.hdnRTFContent8.value = ConvertLetterToStream(dataFileName,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnRTFContent8')", "yes", "no", ConvertLetterToStream(dataFileName, false));
                      //  document.all.hdnSecureRTFContent8.value = ConvertLetterToStream(secFile,false);
                        HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnSecureRTFContent8')", "yes", "no", ConvertLetterToStream(secFile, false));
                    }
                    if (fso.FileExists(dataSourceFileName))
                        fso.DeleteFile(dataSourceFileName);
                    if (fso.FileExists(dataFileName))
                        fso.DeleteFile(dataFileName);
                    if (fso.FileExists(secFile))
                        fso.DeleteFile(secFile);
                }

                if (fso.FileExists(strForm))
                    fso.DeleteFile(strForm);
                if (fso.FileExists(headerFileName))
                    fso.DeleteFile(headerFileName);
                //}

            }
            catch (Exception e) {
                //document.all.hdnLetterGenerated.value = 0;
                HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnLetterGenerated", "yes", "no", 0);
                //return DisplayError("The local Microsft Word application required to complete the wizard could not be loaded because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string ValidateClaimLetterStep33 = HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep33", "no", "yes", "") as string;
                string ValidateClaimLetterStep32 = HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep32", "no", "yes", "") as string;
                ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                return HtmlPage.Window.Invoke("DisplayErrorfromSilver", ValidateClaimLetterStep33 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs) as string;
               // return DisplayError(MergeClaimLetterValidations.ValidateClaimLetterStep33 + " " + e.name + ',' + e.message + "'\r\n'"+ MergeClaimLetterValidations.ValidateClaimLetterStep32 + " " + step); //mudabbir commented
            }

            Editor_Launched = 1;
            //document.all.hdnLetterGenerated.value = -1;
            return "false";
  }

        [ScriptableMember]
        public string PrepareDataSource(string dataSource,int iNum) 

  {
        var step = "";
        dynamic xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
        var e = xmlDoc.createElement("b64");
        var adTypeBinary = 1;
        var adSaveCreateOverWrite = 2;
        //var adTypeText = 2;

        e.dataType = "bin.base64";
        //step = 'instantiate xml document for datasource';
        step = "MergeClaimLetterValidations.ValidatePrepareDataSource1";
        xmlDoc =AutomationFactory.CreateObject("MSXML2.DOMDocument");
        //step = 'create b64 element for datasource';
        step = "MergeClaimLetterValidations.ValidatePrepareDataSource2";
        e = xmlDoc.createElement("b64");
        //step = 'set element.dataType to bin.base64 for datasource';
        step = "MergeClaimLetterValidations.ValidatePrepareDataSource3";
        e.dataType = "bin.base64";

        //step = 'set element.text to datasource base64 text';
        step = "MergeClaimLetterValidations.ValidatePrepareDataSource4";
        e.text = dataSource;
        //step = 'get bytes from datasource element';
        step = "MergeClaimLetterValidations.ValidatePrepareDataSource5";
        var dataSourceBytes = e.nodeTypedValue;

        //use ADODB.Stream to save datasource to disk
        //var dataSourceFileName = GetDataFileContent(iNum);
        var dataSourceFileName = HtmlPage.Window.Invoke("GetDataFileContent", iNum) as string;
        //step = 'instantiate Stream object for data source';
        step = "MergeClaimLetterValidations.ValidateClaimLetterStep26";
        dynamic stm = AutomationFactory.CreateObject("ADODB.Stream");
        //step = 'set datasource stm.Type';
        step = "MergeClaimLetterValidations.ValidateClaimLetterStep27";
        stm.Type = adTypeBinary;
        //step = 'open datasource stream';
        step = "MergeClaimLetterValidations.ValidateClaimLetterStep28";
        stm.Open();
        //step = 'write bytes to datasource stream';
        step = "MergeClaimLetterValidations.ValidateClaimLetterStep29";
        stm.Write(dataSourceBytes);
        //step = 'save datasource stream to file ' + dataSourceFileName;
        step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateClaimLetterStep30", "no", "yes", "").ToString() + " " + dataSourceFileName;
        stm.SaveToFile(dataSourceFileName, adSaveCreateOverWrite);
        //step = 'close stream';
        step = "MergeClaimLetterValidations.ValidateClaimLetterStep20";
        stm.Close();
        stm = null;
        return dataSourceFileName;
    }

        [ScriptableMember]
        public string Finish(int editorL)
    {
        var step="";
            dynamic stm,xmlDoc,xmlElement,xmlNode;
            try {
                Editor_Launched = editorL;
                if (Editor_Launched == 0) {
                    //alert("You must click the 'Launch Merge' button and complete the merge before finalizing.");
                    string ValidCompleteMerge = HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidCompleteMerge", "no", "yes", "") as string;
                    MessageBox.Show(ValidCompleteMerge);
                   // alert(MergeEditResultValidations.ValidCompleteMerge); //Aman ML Changes //mudabbir commented

                    return "false";
                }

                if (IsWordOpen() && objWord.Documents.Count == 1) {
                    //step = 'activate Word';
                    step = "MergeEditResultValidations.ValidateFinishStep1"; //Aman ML Change
                    objWord.Activate();

                    //step='get pointer to merged document';
                    step = "MergeEditResultValidations.ValidateFinishStep2";
                    var wordDoc = objWord.Documents.Item(1);

                    //step='select, save and close the document';
                    step = "MergeEditResultValidations.ValidateFinishStep3";
                    wordDoc.Select();
                    wordDoc.Save();
                    wordDoc.Close();
                }

                var adTypeBinary = 1;
                //step = 'instantiate Stream object';
                step = "MergeEditResultValidations.ValidateFinishStep4";
                stm = AutomationFactory.CreateObject("ADODB.Stream");
                //step = 'set stm.Type';
                step = "MergeEditResultValidations.ValidateFinishStep5";
                stm.Type = adTypeBinary;
                //step = 'open stream';
                step = "MergeEditResultValidations.ValidateFinishStep6";
                stm.Open();
                //step = 'read file ' + strForm + ' into stream';
                step = "$" + HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidateFinishStep7", "no", "yes", "").ToString() + " " + strForm + " " + HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidateFinishStep8", "no", "yes", "").ToString();

                stm.LoadFromFile(strForm);
                //step = 'read bytes from stream';
                step = "MergeEditResultValidations.ValidateFinishStep9";
                var bytes = stm.Read();
                //step = 'close stream';
                step = "MergeEditResultValidations.ValidateFinishStep10";
                stm.Close();
                stm = null;

                //step = 'instantiate xml document';
                step = "MergeEditResultValidations.ValidateFinishStep11";
                xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
                xmlDoc.async = false;
                //step = 'create root element';
                step = "MergeEditResultValidations.ValidateFinishStep12";
                xmlElement = xmlDoc.createElement("root");
                xmlDoc.documentElement = xmlElement;
                xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
                //step = 'create element to hold word document';
                step = "MergeEditResultValidations.ValidateFinishStep13";
                xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
                xmlNode.dataType = "bin.base64";
                //step = 'read stream into element';
                step = "MergeEditResultValidations.ValidateFinishStep14";
                xmlNode.nodeTypedValue = bytes;
                //step = 'read base64 text from element';
                step = "MergeEditResultValidations.ValidateFinishStep15";
                var base64 = xmlNode.text;

                //step='cleanup';
                step = "MergeEditResultValidations.ValidateFinishStep16";
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;

                //document.all.hdnNewFileName.value = strForm;
                HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnNewFileName", "yes", "no", strForm);
                //document.all.hdnRTFContent.value = base64;
                HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnRTFContent", "yes", "no", base64);
            }
            catch (Exception e) {
                //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string ValidateFinishStep17 = HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidateFinishStep17", "no", "yes", "") as string;
                string ValidateFinishStep18 = HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidateFinishStep18", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                string ValidateFinishStep23 = HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidateFinishStep23", "no", "yes", "") as string;
                MessageBox.Show(ValidateFinishStep17 + " " + strForm + " " + ValidateFinishStep18 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidateFinishStep23 + " " + stepfromJs);
                //alert(MergeEditResultValidations.ValidateFinishStep17 + " " + strForm + " " + MergeEditResultValidations.ValidateFinishStep18 + " " + e.name + ',' + e.message + '\r\n' + MergeEditResultValidations.ValidateFinishStep23 + " " + step); //mudabbir commentd
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
                return "false";
            }

            //Clean Up    
            try {
                //step = 'close word';
                step = "MergeEditResultValidations.ValidateFinishStep19";
                if (IsWordOpen()) {
                    objWord.Quit(0);
                    objWord = null;

                }
                //step = 'delete local copy of document at ' + strForm;
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidateFinishStep20", "no", "yes", "").ToString() + " " + strForm;
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);

                //var dataSourceFileName = GetDataSourceFileName();
               var dataSourceFileName = HtmlPage.Window.Invoke("GetDataSourceFileName", null) as string;
                //step = 'delete data source file at ' + dataSourceFileName;
               step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidateFinishStep21", "no", "yes", "").ToString() + " " + dataSourceFileName;
                if (fso.FileExists(dataSourceFileName))
                    fso.DeleteFile(dataSourceFileName);
            }
            catch (Exception e) {
                //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string ValidateFinishStep21 = HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidateFinishStep21", "no", "yes", "") as string;
                string ValidateFinishStep23 = HtmlPage.Window.Invoke("getAndSetValues", "MergeEditResultValidations.ValidateFinishStep23", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                MessageBox.Show(ValidateFinishStep21 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidateFinishStep23 + " " + stepfromJs);
               // alert(MergeEditResultValidations.ValidateFinishStep21 + " " + e.name + ',' + e.message + '\r\n' + MergeEditResultValidations.ValidateFinishStep23 + " " + step);//mudabbir commeted
            }

            //document.getElementById('hdnaction').value="AttachAndFinish";
            return "true";
}

        [ScriptableMember]
        public bool WordMerge(string sFileName,string sHeaderFileName,string sDataFileName,string dataFileName,string secFile,string password) 
    {
         dynamic wordDoc="";
        var step = "";
        var isProtected = false;

        try {//open template
            //step = 'open word document: ' + sFileName;
            step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.OpenWordDocument", "no", "yes", "").ToString() + " " + sFileName;
            objWord = AutomationFactory.CreateObject("Word.Application");
            wordDoc = objWord.Documents.Open(sFileName);

            //Try to save the document to make MSWord's SaveDate function work
            objWord.ActiveDocument.Saveas(sFileName);
        }
        catch (Exception e) {
            //document.all.hdnLetterGenerated.value = 0;
            HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnLetterGenerated", "yes", "no", 0);
            //DisplayError("The word template at " + sFileName + " could not be opened by Microsft Word because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
            string WordMergeTemplateErr1 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr1", "no", "yes", "") as string;
            string WordMergeTemplateErr2 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr1", "no", "yes", "") as string;
            string stepfromJs = string.Empty;
            if (step.Contains('$'))
            {
                step.Replace("$", "");
                stepfromJs = step;
            }
            else
            {
                stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
            }
            ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
            HtmlPage.Window.Invoke("DisplayErrorfromSilver", WordMergeTemplateErr1 + " " + sFileName + " " + WordMergeTemplateErr2 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
           // DisplayError(parent.CommonValidations.WordMergeTemplateErr1 + " " + sFileName + " " + parent.CommonValidations.WordMergeTemplateErr2 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);//mudabbir commented
            if (objWord != null) {
                objWord.Quit(0);
                objWord = null;
                return false;
            }
        }

        try {//Verify we have full edit rights.	Isprotected added by pawan for 10983								   
            if (objWord.ActiveDocument.ProtectionType != -1) //if not totally unlocked then
            {
                objWord.ActiveDocument.UnProtect("");
                isProtected = true;
            }
        }
        catch (Exception e) {
            //document.all.hdnLetterGenerated.value = 0;
            HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnLetterGenerated", "yes", "no", 0);
            //alert("The word template at " + sFileName + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
            string WordMergeTemplateErr1 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr1", "no", "yes", "") as string;
            string WordMergeTemplateErr3 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr3", "no", "yes", "") as string;
            MessageBox.Show(WordMergeTemplateErr1 + " " + strForm + " " + WordMergeTemplateErr3.Split('\n'));
          //  alert(parent.CommonValidations.WordMergeTemplateErr1 + " " + sFileName + " " + (parent.CommonValidations.WordMergeTemplateErr3).split('\\n').join('\n'));//mudabbir commented
            objWord.visible = true;
            objWord.Activate();
        }

        try {   //step='activate Word';
            step = "parent.CommonValidations.ValidActivateWordStepCheck";
            //	    objWord.visible = false;
            //	    objWord.Activate();	
            wordDoc.Select();  //this call instantiates MailMerge object
            //step='open header';
            step = "parent.CommonValidations.OpenHeader";
            if (sHeaderFileName.Length > 0) {
                wordDoc.MailMerge.OpenHeaderSource(sHeaderFileName);
            }
            //step='open data source';
            step = "parent.CommonValidations.OpenDataSource";
            wordDoc.MailMerge.OpenDataSource(sDataFileName);
            wordDoc.MailMerge.DataSource.FirstRecord = 1;

            //Get number of records to be merged
            wordDoc.MailMerge.DataSource.LastRecord = 1;
            //var objRecordNumber = document.getElementById("hdnNumRecords");
            var objRecordNumber=  HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnNumRecords')","no","yes","") as string;
            if (objRecordNumber != null) {
                var sRecordNumber = objRecordNumber;
//                if ( (sRecordNumber)) {
                    wordDoc.MailMerge.DataSource.LastRecord = int.Parse(sRecordNumber);
  //              }
            }
            wordDoc.MailMerge.Destination = 0;

            //step='do mail merge';
            step = "parent.CommonValidations.DoMailMerge";
            wordDoc.MailMerge.Execute(false);
            //step='select merged document';
            step = "parent.CommonValidations.SelectMergedDoc";
            //var mergedDoc = objWord.Documents.Item(1);


            //objWord.ActiveDocument.Saveas(secFile,0,0,"Navdeep"); 		

            //step='close the original';
            step = "parent.CommonValidations.CloseOriginal";
            //wordDoc.Saveas(strForm)
            //wordDoc.Select();   
            wordDoc.Close();

            //step='select merged document';
            step = "parent.CommonValidations.SelectMergedDoc";
            //Not need rt now doesn't suffice clients requirement pawan changed for mits 10983 enabling protection if already protected
            //if (isProtected == true)
            //	objWord.ActiveDocument.Protect(2,true,"");

            //change end

            objWord.ActiveDocument.Select();
            //step='save the merged document over the original at ' + dataFileName;
            step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.SaveMergedDoc", "no", "yes", "").ToString() + " " + dataFileName;
            objWord.ActiveDocument.Saveas(dataFileName);

            //step='save the Secured Document over the original at ' + secFile;    
            step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.SaveSecuredDoc", "no", "yes", "").ToString() + " " + secFile;
            objWord.ActiveDocument.Saveas(secFile, 0, 0, password);

            objWord.Quit(0);
            objWord = null;

            for (var iCtr = 1; iCtr <= 5; iCtr = iCtr + 1) {
                //setTimeout("FileMerge",1000);        
                //wait(1000);
                HtmlPage.Window.Invoke("wait",1000);
                if (fso.FileExists(secFile))
                    break;
            }
            return true;
        } //
        catch (Exception e) {
            //document.all.hdnLetterGenerated.value = 0;
            HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnLetterGenerated", "yes", "no", 0);
            //DisplayError("Microsoft Word could not accomplish the merge because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
            string MSWordError = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.MSWordError", "no", "yes", "") as string;
            string stepfromJs = string.Empty;
            if (step.Contains('$'))
            {
                step.Replace("$", "");
                stepfromJs = step;
            }
            else
            {
                stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
            }
            ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
            HtmlPage.Window.Invoke("DisplayErrorfromSilver", MSWordError + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
            //DisplayError(parent.CommonValidations.MSWordError + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);//mudabbir commented
            objWord.Quit(0);
            objWord = null;
            return false;
        }
        //Done
}

        [ScriptableMember]
        public string ConvertLetterToStream(string sFile, bool isSecure)
    {
         var step = "";
        var base64 = "";
            dynamic stm,xmlDoc,xmlElement,xmlNode;
        //Convert Back to Stream
        try {
            var adTypeBinary = 1;
            //step = 'instantiate Stream object';
            step = "MergeClaimLetterValidations.ValidateConvertLetterToStream13";
             stm = AutomationFactory.CreateObject("ADODB.Stream");
            //step = 'set stm.Type';
            step = "MergeClaimLetterValidations.ValidateConvertLetterToStream1";
            stm.Type = adTypeBinary;
            //step = 'open stream';
            step = "MergeClaimLetterValidations.ValidateConvertLetterToStream2";
            stm.Open();
            //step = 'read file ' + sFile + ' into stream';
            step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateConvertLetterToStream3", "no", "yes", "").ToString() + " " + sFile + " " + HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateConvertLetterToStream4", "no", "yes", "").ToString();
            stm.LoadFromFile(sFile);
            //step = 'read bytes from stream';
            step = "MergeClaimLetterValidations.ValidateConvertLetterToStream5";
            var bytes = stm.Read();
            //step = 'close stream';
            step = "MergeClaimLetterValidations.ValidateClaimLetterStep20";
            stm.Close();
            stm = null;

            //step = 'instantiate xml document';
            step = "MergeClaimLetterValidations.ValidateConvertLetterToStream14";
             xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
            xmlDoc.async = false;
            //step = 'create root element';
            step = "MergeClaimLetterValidations.ValidateConvertLetterToStream6";
             xmlElement = xmlDoc.createElement("root");
            xmlDoc.documentElement = xmlElement;
            xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
            //step = 'create element to hold word document';
            step = "MergeClaimLetterValidations.ValidateConvertLetterToStream7";
             xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
             xmlNode.dataType = "bin.base64";
            //step = 'read stream into element';
             step = "MergeClaimLetterValidations.ValidateConvertLetterToStream8";
            xmlNode.nodeTypedValue = bytes;
            //step = 'read base64 text from element';
            step = "MergeClaimLetterValidations.ValidateConvertLetterToStream9";
            base64 = xmlNode.text;

            //step='cleanup';
            step = "MergeClaimLetterValidations.ValidateConvertLetterToStream10";
            xmlDoc = null;
            xmlElement = null;
            xmlNode = null;
        }
        catch (Exception e) {
            //document.all.hdnLetterGenerated.value = 0;
            HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnLetterGenerated", "yes", "no", 0);
            //alert("The local copy of the document at " + sFile + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
            string ValidateConvertLetterToStream11 = HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateConvertLetterToStream11", "no", "yes", "") as string;
            string ValidateConvertLetterToStream12 = HtmlPage.Window.Invoke("getAndSetValues", "MergeClaimLetterValidations.ValidateConvertLetterToStream12", "no", "yes", "") as string;
            string stepfromJs = string.Empty;
            if (step.Contains('$'))
            {
                step.Replace("$", "");
                stepfromJs = step;
            }
            else
            {
                stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
            }
            ValidateClaimLetterStep32 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidateClaimLetterStep32", "no", "yes", "") as string;
            MessageBox.Show(ValidateConvertLetterToStream11 + " " + sFile + " " + ValidateConvertLetterToStream12 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidateClaimLetterStep32 + " " + stepfromJs);
           // alert(MergeClaimLetterValidations.ValidateConvertLetterToStream11 + " " + sFile + MergeClaimLetterValidations.ValidateConvertLetterToStream12 + " " + e.name + ',' + e.message + '\r\n' + MergeClaimLetterValidations.ValidateClaimLetterStep32 + " " + step); //mudabbir commenetd
            xmlDoc = null;
            xmlElement = null;
            xmlNode = null;
            return base64;
        }
        return base64;
}

        [ScriptableMember]
        public bool WordMerge4Vesion12Higher(string sFileName,string sHeaderFileName,string sDataFileName,bool saveToDisk) {
        dynamic wordDoc = null;
        var step = "";
        var isProtected = false;
        
            try {//open template
                //step = 'open word document: ' + strForm;

                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.OpenWordDocument", "no", "yes", "").ToString() + " " + strForm;
                objWord = AutomationFactory.CreateObject("Word.Application");
                wordDoc = objWord.Documents.Open(strForm);

                //Try to save the document to make MSWord's SaveDate function work
                objWord.ActiveDocument.Saveas(strForm);
            }
            catch (Exception e) {
                //DisplayError("The word template at " + strForm + " could not be opened by Microsft Word because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string WordMergeTemplateErr1 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr1", "no", "yes", "") as string;
                string WordMergeTemplateErr2 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr2", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
                HtmlPage.Window.Invoke("DisplayErrorfromSilver", WordMergeTemplateErr1 + " " + strForm + " " + WordMergeTemplateErr2 + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
                //DisplayError(parent.CommonValidations.WordMergeTemplateErr1 + " " + strForm + " " + parent.CommonValidations.WordMergeTemplateErr2 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);//mudabbir commented
                if (objWord != null) {
                    objWord.Quit(0);
                    objWord = null;
                    return false;
                }
            }
        

        try {//Verify we have full edit rights.	Isprotected added by pawan for 10983								   
            if (objWord.ActiveDocument.ProtectionType != -1) //if not totally unlocked then
            {
                objWord.ActiveDocument.UnProtect("");
                isProtected = true;
            }
        }
        catch (Exception e) {
            //alert("The word template at " + strForm + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
            string WordMergeTemplateErr1 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr1", "no", "yes", "") as string;
            string WordMergeTemplateErr3 = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.WordMergeTemplateErr3", "no", "yes", "") as string;
            string stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
            ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
            MessageBox.Show(WordMergeTemplateErr1 + " " + strForm + " " + WordMergeTemplateErr3.Split('\n'));
           // alert(parent.CommonValidations.WordMergeTemplateErr1 + " " + strForm + " " + (parent.CommonValidations.WordMergeTemplateErr3).split('\\n').join('\n'));//mudabbir commented
            objWord.visible = true;
            objWord.Activate();
        }

        try {
            //step='activate Word';
            step = "parent.CommonValidations.ValidActivateWordStepCheck";
            objWord.visible = true;
            objWord.Activate();
            wordDoc.Select();  //this call instantiates MailMerge object
            //step='open header';
            step = "parent.CommonValidations.OpenHeader";
            if (sHeaderFileName.Length > 0) {
                wordDoc.MailMerge.OpenHeaderSource(sHeaderFileName);
            }
            //step='open data source';
            step = "parent.CommonValidations.OpenDataSource";
            wordDoc.MailMerge.OpenDataSource(sDataFileName);
            wordDoc.MailMerge.DataSource.FirstRecord = 1;

            //Get number of records to be merged
            wordDoc.MailMerge.DataSource.LastRecord = 1;
            //var objRecordNumber = document.getElementById("hdnNumRecords");
            var objRecordNumber = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnNumRecords')", "no", "yes", "") as string;
            if (objRecordNumber != null) {
                var sRecordNumber = objRecordNumber;
                //if (!isNaN(sRecordNumber)) {
                    wordDoc.MailMerge.DataSource.LastRecord = int.Parse(sRecordNumber);
               // }
            }
            wordDoc.MailMerge.Destination = 0;

            //step='do mail merge';
            step = "parent.CommonValidations.DoMailMerge";
            wordDoc.MailMerge.Execute(false);
            //step='select merged document';
            step = "parent.CommonValidations.SelectMergedDoc";
            //var mergedDoc = objWord.Documents.Item(1);


            //step='close the original';
            step = "parent.CommonValidations.CloseOriginal";
            //wordDoc.Saveas(strForm)
            //wordDoc.Select();   
            wordDoc.Close();

            //step='select merged document';
            step = "parent.CommonValidations.SelectMergedDoc";
            //Not need rt now doesn't suffice clients requirement pawan changed for mits 10983 enabling protection if already protected
            //if (isProtected == true)
            //	objWord.ActiveDocument.Protect(2,true,"");

            //change end
            //Deb : MITS 32759 
            objWord.visible = true;
            objWord.Activate();
            //Deb : MITS 32759 
            objWord.ActiveDocument.Select();

            if (saveToDisk) {
                //step='save the merged document over the original at ' + strForm;
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.SaveMergedDoc", "no", "yes", "").ToString() + " " + strForm;
                objWord.ActiveDocument.Saveas(strForm);
            }

        }
        catch (Exception e) {
            // DisplayError("Microsoft Word could not accomplish the merge because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
            string MSWordError = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.MSWordError", "no", "yes", "") as string;
            string stepfromJs = string.Empty;
            if (step.Contains('$'))
            {
                step.Replace("$", "");
                stepfromJs = step;
            }
            else
            {
                stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
            }
            ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "parent.CommonValidations.ValidErrorStepCheck", "no", "yes", "") as string;
            HtmlPage.Window.Invoke("DisplayErrorfromSilver", MSWordError + " " + e.GetType() + ',' + e.Message + "\r\n" + ValidErrorStepCheck + " " + stepfromJs);
            //DisplayError(parent.CommonValidations.MSWordError + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);//mudabbir commneted
            objWord.Quit(0);
            objWord = null;
            return false;
        }
        return true;
        //Done
    }

        [ScriptableMember]
        public void CleanAndCancel()
 {
     if (IsWordOpen())
     {
         objWord.Quit(0);
         objWord = null;
     }

     //Clean Up.
     if (fso.FileExists(strForm)) //Main doc temp file.
         fso.DeleteFile(strForm);
 }
  
        [ScriptableMember]
        public void CleanAndClose()
 {
     if (fso.FileExists(strForm)) //Main doc temp file.
         fso.DeleteFile(strForm);
 }
        [ScriptableMember]
        public string FinishEditPPT(string strFile,int editorL)
        {
            GetDocumentPath(strFile, "", "");
            Editor_Launched = editorL;
            var step = "";
            dynamic xmlDoc,xmlNode,xmlElement;
            try
            {
                if (Editor_Launched == 0)
                {
                    return "true";
                }
                //pleaseWait.Show();//commented by Mudabbir
                HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "yes","no","no");
                if (IsPPTOpen() && objWord.Presentations.Count == 1)
                {
                    //step = 'activate PPT';
                    step = "EditDocValidations.jsActivatePPT";
                    //objWord.Activate();

                    //step = 'get pointer to document';
                    step = "EditDocValidations.jsFinishedEditStep1";
                    var wordDoc = objWord.Presentations.Item(1);

                    //step = 'select, save and close the document';
                    step = "EditDocValidations.jsFinishedEditStep2";
                    // wordDoc.Select();
                    wordDoc.Save();
                    wordDoc.Close();
                }
                
                var adTypeBinary = 1;
                //step = 'instantiate Stream object';
                step = "EditDocValidations.jsFinishedEditStep3";
                var stm = AutomationFactory.CreateObject("ADODB.Stream");
                //step = 'set stm.Type';
                step = "EditDocValidations.jsFinishedEditStep4";
                stm.Type = adTypeBinary;
                //step = 'open stream';
                step = "EditDocValidations.jsFinishedEditStep5";
                stm.Open();
                //step = 'read file ' + strForm + ' into stream';
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep6", "no", "yes", "").ToString() + ' ' + strForm + ' ' + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep7", "no", "yes", "").ToString();
                stm.LoadFromFile(strForm);
                //step = 'read bytes from stream';
                step = "EditDocValidations.jsFinishedEditStep8";
                var bytes = stm.Read();
                //step = 'close stream';
                step = "EditDocValidations.jsFinishedEditStep9";
                stm.Close();
                stm = null;

                //step = 'instantiate xml document';
                step = "EditDocValidations.jsFinishedEditStep10";
                xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
                xmlDoc.async = false;
                //step = 'create root element';
                step = "EditDocValidations.jsFinishedEditStep11";
                xmlElement = xmlDoc.createElement("root");
                xmlDoc.documentElement = xmlElement;
                xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
                //step = 'create element to hold document';
                step = "EditDocValidations.jsFinishedEditStep12";
                xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
                xmlNode.dataType = "bin.base64";
                //step = 'read stream into element';
                step = "EditDocValidations.jsFinishedEditStep13";
                xmlNode.nodeTypedValue = bytes;
                //step = 'read base64 text from element';
                step = "EditDocValidations.jsFinishedEditStep14";
                var base64 = xmlNode.text;

                try
                {
                    //step = 'close PPT';
                    step = "EditDocValidations.jsClosePPT";
                    if (objWord!=null)
                    {
                        objWord.Quit();
                        objWord = null;
                        //idTmr = window.setInterval("Cleanup();", 1);
                        HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "yes", "no","no","no");
                    }

                    //step = 'delete local copy of document at ' + strForm;
                    step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep17", "no", "yes", "").ToString() + ' ' + strForm;
                    if (fso.FileExists(strForm)) //Main doc temp file.
                        fso.DeleteFile(strForm);


                }
                catch (Exception e)
                {
                    //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                   string jsFinishedEdit3Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit3Error", "no", "yes", "") as string;
                   string stepfromJs = string.Empty;
                   if (step.Contains('$'))
                   {
                    step.Replace("$", "");
                    stepfromJs = step;
                   }
                  else
                   {
                     stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                   }

                   string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                   MessageBox.Show(jsFinishedEdit3Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
                    //alert(EditDocValidations.jsFinishedEdit3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                  }
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;

                //document.all.hdnRTFContent.value = base64;
                HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnRTFContent", "yes", "no", base64);
            }
            catch (Exception e)
            {
                //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string jsFinishedEdit1Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit1Error", "no", "yes", "") as string;
                string jsFinishedEdit2Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit2Error", "no", "yes", "") as string;
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }

                MessageBox.Show(jsFinishedEdit1Error + " " + strForm + " " + " " + jsFinishedEdit2Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
               // alert(EditDocValidations.jsFinishedEdit1Error + ' ' + strForm + ' ' + EditDocValidations.jsFinishedEdit2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
                objWord = null;
                //idTmr = window.setInterval("Cleanup();", 1);
                HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "yes", "no","no","no");
                return "false";
            }

            //Clean Up    
            try
            {
                //step = 'close word';
                step = "EditDocValidations.jsFinishedEditStep16";
                if (IsPPTOpen())
                {
                    objWord.Quit();
                    objWord = null;
                    //idTmr = window.setInterval("Cleanup();", 1);
                    HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "yes", "no","no","no");

                }

                //step = 'delete local copy of document at ' + strForm;
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep17", "no", "yes", "").ToString() + ' ' + strForm;
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);


            }
            catch (Exception e)
            {
                //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        string jsFinishedEdit3Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit3Error", "no", "yes", "") as string;
        string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
        string stepfromJs = string.Empty;
        if (step.Contains('$'))
        {
            step.Replace("$", "");
            stepfromJs = step;
        }
        else
        {
            stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
        }
        MessageBox.Show(jsFinishedEdit3Error  + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
                //alert(EditDocValidations.jsFinishedEdit3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

            //document.getElementById('hdnaction').value="AttachAndFinish";
           // document.frmData.submit();
          HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "no","yes","no");
          return "true";
        }

        [ScriptableMember]
        public string FinishWordEdit(string strFile,int editorL) 
{
            GetDocumentPath(strFile, "", "");
            Editor_Launched = editorL;
            var step = "";
            dynamic xmlDoc,xmlElement,xmlNode,stm;
            try {
                if (Editor_Launched == 0) {
                      return "true";
                }
               // pleaseWait.Show();
                HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "yes","no","no");
                if (IsWordOpen() && objWord.Documents.Count == 1) {
                    //step = 'activate Word';
                    step = "EditDocValidations.jsActivateWord";
                    objWord.Activate();

                    //step = 'get pointer to document';
                    step = "EditDocValidations.jsFinishedEditStep1";
                    var wordDoc = objWord.Documents.Item(1);

                    //step = 'select, save and close the document';
                    step = "EditDocValidations.jsFinishedEditStep2";
                    wordDoc.Select();
                    wordDoc.Save();
                    wordDoc.Close();
                }

                var adTypeBinary = 1;
                //step = 'instantiate Stream object';
                step = "EditDocValidations.jsFinishedEditStep3";
                stm = AutomationFactory.CreateObject("ADODB.Stream");
                //step = 'set stm.Type';
                step = "EditDocValidations.jsFinishedEditStep4";
                stm.Type = adTypeBinary;
                //step = 'open stream';
                step = "EditDocValidations.jsFinishedEditStep5";
                stm.Open();
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep6", "no", "yes", "").ToString() + "," + this.strForm + ", " + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep7", "no", "yes", "").ToString();
                stm.LoadFromFile(strForm);
                //step = 'read bytes from stream';
                step = "EditDocValidations.jsFinishedEditStep8";
                var bytes = stm.Read();
                //step = 'close stream';
                step = "EditDocValidations.jsFinishedEditStep9";
                stm.Close();
                stm = null;

                //step = 'instantiate xml document';
                step = "EditDocValidations.jsFinishedEditStep10";
                xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
                xmlDoc.async = false;
                //step = 'create root element';
                step = "EditDocValidations.jsFinishedEditStep11";
                xmlElement = xmlDoc.createElement("root");
                xmlDoc.documentElement = xmlElement;
                xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
                //step = 'create element to hold document';
                step = "EditDocValidations.jsFinishedEditStep12";
                xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
                xmlNode.dataType = "bin.base64";
                //step = 'read stream into element';
                step = "EditDocValidations.jsFinishedEditStep13";
                xmlNode.nodeTypedValue = bytes;
                //step = 'read base64 text from element';
                step = "EditDocValidations.jsFinishedEditStep14";
                var base64 = xmlNode.text;

                //step = 'cleanup';
                step = "EditDocValidations.jsFinishedEditStep15";
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
               
                //document.all.hdnRTFContent.value = base64;
                HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnRTFContent", "yes", "no", base64);
            }
            catch (Exception e)
            {
                //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
               string jsFinishedEdit1Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit1Error", "no", "yes", "") as string;
               string jsFinishedEdit2Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit2Error", "no", "yes", "") as string;
               string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
               string stepfromJs = string.Empty;
               if (step.Contains('$'))
               {
                step.Replace("$", "");
                stepfromJs = step;
               }
               else
               {
                stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
               }
               MessageBox.Show(jsFinishedEdit1Error + " " + strForm + " "  + jsFinishedEdit2Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
                //alert(EditDocValidations.jsFinishedEdit1Error + ' ' + strForm + ' ' + EditDocValidations.jsFinishedEdit2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
                return "false";
            }

            //Clean Up    
            try {
                //step = 'close word';
                step = "EditDocValidations.jsFinishedEditStep16";
                if (IsWordOpen()) {
                    objWord.Quit(0);
                    objWord = null;
                }

                //step = 'delete local copy of document at ' + strForm;
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep17", "no", "yes", "").ToString() + ' ' + strForm;
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);


            }
            catch (Exception e)
            {
                //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                  string jsFinishedEdit1Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit3Error", "no", "yes", "") as string;
                  string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                  string stepfromJs = string.Empty;
                  if (step.Contains('$'))
                  {
                   step.Replace("$", "");
                   stepfromJs = step;
                  }
                  else
                   {
                     stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                   }
        
                MessageBox.Show(jsFinishedEdit1Error  + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
                //alert(EditDocValidations.jsFinishedEdit3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

           
            //document.frmData.submit();
            HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "no","yes","no");
            return "true";
        }

        [ScriptableMember]
        public string FinishEditExcel(string strFile, int editorL)
        {
            GetDocumentPath(strFile, "", "");
            Editor_Launched = editorL;
            var step = "";
            dynamic xmlDoc,xmlElement,xmlNode; 
            try
            {
                if (Editor_Launched == 0)
                {
                    return "true";
                }
                try
                {
                    //testing the below condition to check if excel is opened / not
                    
                    if (objWord.Workbooks)
                    {
                    }
                }
                catch (Exception )
                {
                    //alert("You must 'Save' and 'Close' edited excel document.");
                    string jsSaveAndCloseExcel= HtmlPage.Window.Invoke("getAndSetValues","EditDocValidations.jsSaveAndCloseExcel","no","yes","") as string;
                    MessageBox.Show(jsSaveAndCloseExcel);
                    //alert(EditDocValidations.jsSaveAndCloseExcel);
                    return "false";
                }
               // pleaseWait.Show();
                HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "yes","no","no");
                if (IsWordOpen() && objWord.Workbooks.Count == 1)
                {
                    //step = 'activate Excel';
                    step = "EditDocValidations.jsActivateExcel";
                    //objWord.Activate();

                    //step = 'get pointer to document';
                    step = "EditDocValidations.jsFinishedEditStep1";
                    var wordDoc = objWord.Workbooks.Item(1);

                    //step = 'select, save and close the document';
                    step = "EditDocValidations.jsFinishedEditStep2";
                    // wordDoc.Select();
                    wordDoc.Save();
                    wordDoc.Close(0);
                 
                }
                
                var adTypeBinary = 1;
                //step = 'instantiate Stream object';
                step = "EditDocValidations.jsFinishedEditStep3";
                var stm = AutomationFactory.CreateObject("ADODB.Stream");
                //step = 'set stm.Type';
                step = "EditDocValidations.jsFinishedEditStep4";
                stm.Type = adTypeBinary;
                //step = 'open stream';
                step = "EditDocValidations.jsFinishedEditStep5";
                stm.Open();
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep6", "no", "yes", "").ToString() + ' ' + strForm + ' ' + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep7", "no", "yes", "").ToString();
                stm.LoadFromFile(strForm);
                //step = 'read bytes from stream';
                step = "EditDocValidations.jsFinishedEditStep8";
                var bytes = stm.Read();
                //step = 'close stream';
                step = "EditDocValidations.jsFinishedEditStep9";
                stm.Close();
                stm = null;

                //step = 'instantiate xml document';
                step = "EditDocValidations.jsFinishedEditStep10";
                xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
                xmlDoc.async = false;
                //step = 'create root element';
                step = "EditDocValidations.jsFinishedEditStep11";
                xmlElement = xmlDoc.createElement("root");
                xmlDoc.documentElement = xmlElement;
                xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
                //step = 'create element to hold document';
                step = "EditDocValidations.jsFinishedEditStep12";
                xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
                xmlNode.dataType = "bin.base64";
                //step = 'read stream into element';
                step = "EditDocValidations.jsFinishedEditStep13";
                xmlNode.nodeTypedValue = bytes;
                //step = 'read base64 text from element';
                step = "EditDocValidations.jsFinishedEditStep14";
                var base64 = xmlNode.text;

                //step = 'cleanup';
                step = "EditDocValidations.jsFinishedEditStep15";
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;

                //document.all.hdnRTFContent.value = base64;
                 HtmlPage.Window.Invoke("getAndSetValues", "document.all.hdnRTFContent", "yes", "no", base64);
            }
            catch (Exception e)
            {
                //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
             string jsFinishedEdit1Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit1Error", "no", "yes", "") as string;
             string jsFinishedEdit2Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit2Error", "no", "yes", "") as string;
             string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
             string stepfromJs = string.Empty;
             if (step.Contains('$'))
             {
                 step.Replace("$", "");
                 stepfromJs = step;
             }
             else
             {
                 stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
             }
             MessageBox.Show(jsFinishedEdit1Error + " " + strForm + " "  + jsFinishedEdit2Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
                //alert(EditDocValidations.jsFinishedEdit1Error + ' ' + strForm + '' + EditDocValidations.jsFinishedEdit2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                xmlDoc = null;
                xmlElement = null;
                xmlNode = null;
                objWord = null;
                //idTmr = window.setInterval("Cleanup();", 1);
                HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "yes", "no","no","no");
                return "false";
            }

            //Clean Up    
            try
            {
                //step = 'close word';
                step = "EditDocValidations.jsFinishedEditStep16";
                if (IsWordOpen())
                {
                    objWord.Quit();
                    objWord = null;
                    //idTmr = window.setInterval("Cleanup();", 1);
                    HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "yes", "no","no","no");

                }

                //step = 'delete local copy of document at ' + strForm;
                step ="$"+ HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEditStep17", "no", "yes", "").ToString() + ' ' + strForm;
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);


            }
            catch (Exception e)
            {
                //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string jsFinishedEdit3Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsFinishedEdit3Error", "no", "yes", "") as string;
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                MessageBox.Show(jsFinishedEdit3Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
              //  alert(EditDocValidations.jsFinishedEdit3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

            //document.getElementById('hdnaction').value="AttachAndFinish";
            
            //document.frmData.submit();
            HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "no","yes","no");
            return "true";
        }

        [ScriptableMember]
        public bool IsPPTOpen()
{
    try
    {
        if (objWord != null)
        {
            if (objWord.Presentations == null || objWord.Presentations.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {

            return false;
        }
    }
    catch (Exception e)
    {
        return false;
    }

}

        [ScriptableMember]
        public string OpenExcelEditor(string sFileName,bool saveToDisk,string strFile)
        {
            dynamic wordDoc = null;
            var step = "";
            var isProtected = false;
            GetDocumentPath(strFile, "", "");
            try
            {//open template
                //step = 'open excel document: ' + strForm;
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsOpenExcel", "no", "yes", "").ToString() + ' ' + strForm;
                objWord = AutomationFactory.CreateObject("Excel.Application");
                wordDoc = objWord.Workbooks.Open(strForm);

                //Try to save the document to make MSWord's SaveDate function work
                // objWord.ActiveWorkbook.Saveas(strForm)
            }
            catch (Exception e)
            {
                //DisplayError("The document at " + strForm + " could not be opened by Microsft Excel because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string jsExcel1Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsExcel1Error", "no", "yes", "") as string;
                string jsExcel2Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsExcel2Error", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                HtmlPage.Window.Invoke("DisplayErrorfromSilver", jsExcel1Error + " " + strForm + " " + jsExcel2Error +  " " + e.GetType() + "," + e.Message + "\r\n" + jsStep + " " + stepfromJs);

                //DisplayError(EditDocValidations.jsExcel1Error + ' ' + strForm + ' ' + EditDocValidations.jsExcel2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                if (objWord != null)
                {
                    objWord.Quit(0);
                    objWord = null;
                    return "false";
                }
            }

            try
            {//Verify we have full edit rights.	Isprotected added by pawan for 10983
                if (objWord.ActiveWorkbook.ProtectionType != -1) //if not totally unlocked then
                {
                    objWord.ActiveWorkbook.UnProtect("");
                    isProtected = true;
                }
            }
            catch (Exception e)
            {
                //alert("The word template at " + strForm + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
                objWord.visible = true;
                objWord.Activate();
            }

            try
            {
                //step = 'activate excel';
                step = "EditDocValidations.jsActivateExcel";
                objWord.visible = true;
                //objWord.Activate();

                // objWord.ActiveWorkbook.Select();

                //                if (saveToDisk) {
                //                    step = 'save the merged document over the original at ' + strForm;
                //                    objWord.ActiveWorkbook.Saveas(strForm);
                //                }

            }
            catch (Exception e)
            {
                //DisplayError("Microsoft Excel could not accomplish the editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string jsExcel3Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsExcel3Error", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                HtmlPage.Window.Invoke("DisplayErrorfromSilver", jsExcel3Error  + " " + e.GetType() + "," + e.Message + "\r\n" + jsStep + " " + stepfromJs);
              //  DisplayError(EditDocValidations.jsExcel3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + step);
                objWord.Quit(0);
                objWord = null;
                return "false";
            }
            //Done
            return "true";
        }

        [ScriptableMember]
        public string StartEditorFromAttachments(string AX,bool saveToDisk,string strFile)

        {
            if (AX == "") {
            //tkr 8/2007 HACK.
            //could not figure out how to get the error page to show from here.
            //the xpl logic is incredibly complex and fragile, and throws
            //confusing error message when clicked twice
          GetDocumentPath(strFile,"","");  
        }
        else {
            return "false";
        }
          
            //setDataChanged(true);
            HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "no", "no","yes");
                
            //strForm = strForm + "x";
            var adTypeBinary = 1;
            var adSaveCreateOverWrite = 2;
            var adTypeText = 2;
            //var step = 'check IsWordOpen';
            var step = "EditDocValidations.jsEditorStep1";

            if (IsWordOpen()) {
                //step = 'activate Word';
                step = "EditDocValidations.jsEditorStep2";
                objWord.Activate();
                return "false";
            }

            try {//save template and datasource local
                //use DomDocument to convert base64 string to bytes
                //step = 'get document base64 text';
                step = "EditDocValidations.jsEditorStep3";
                
                //var template = (document.getElementById('hdnContent').value);
                var template = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById('hdnContent')", "no", "yes", "") as string;
                if (template == null || template == "")
                    throw new Exception(HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsEditor1Error", "no", "yes", "").ToString());

                //step = 'instantiate xml document for document';
                step = "EditDocValidations.jsEditorStep4";
                var xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
                //step = 'create b64 element for template';
                step = "EditDocValidations.jsEditorStep5";
                var e = xmlDoc.createElement("b64");
                //step = 'set element.dataType to bin.base64 for document';
                step = "EditDocValidations.jsEditorStep6";
                e.dataType = "bin.base64";

                //step = 'set element.text to document base64 text';
                step = "EditDocValidations.jsEditorStep7";
                e.text = template;
                //step = 'get bytes from document element';
                step = "EditDocValidations.jsEditorStep8";
                var templateBytes = e.nodeTypedValue;

                //use ADODB.Stream to save template bytes to disc
                // step = 'instantiate Stream object for document';
                step = "EditDocValidations.jsEditorStep9";
                var stm = AutomationFactory.CreateObject("ADODB.Stream");
                //step = 'set template stm.Type';
                step = "EditDocValidations.jsEditorStep10";
                stm.Type = adTypeBinary;
                //step = 'open document stream';
                step = "EditDocValidations.jsEditorStep11";
                stm.Open();
                //step = 'write bytes to document stream';
                step = "EditDocValidations.jsEditorStep12";
                stm.Write(templateBytes);
                //step = 'save document stream to file ' + strForm;
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsEditorStep13", "no", "yes", "").ToString() + ' ' + strForm;
                stm.SaveToFile(strForm, adSaveCreateOverWrite);

                //step = 'close document';
                step = "EditDocValidations.jsEditorStep14";
                stm.Close();
                stm = null;


                e = null;
                xmlDoc = null;
            }
            catch (Exception e) {
                //return DisplayError("The document could not be saved to this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string jsEditor2Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsEditor2Error", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                return HtmlPage.Window.Invoke("DisplayErrorfromSilver", jsEditor2Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs) as string;
                //return DisplayError(EditDocValidations.jsEditor2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

            try {
                //In the future if Office 2003 support is not required, should call WordMerge4Vesion11Lower12Higher
                //and change data source content.
                //Function call Changed by pawan for mits 10983 reverted back to routine used by Tom since few properties worked well well with it.
                //CheckEditorToCall(strForm, saveToDisk);
                HtmlPage.Window.Invoke("CheckEditorToCall", strForm, saveToDisk);
                //objWord.FileSaveAs(strForm);
            }
            catch (Exception e) {
                //return DisplayError("The local Microsft application required to complete the editing could not be loaded because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string jsEditor3Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsEditor3Error", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                return HtmlPage.Window.Invoke("DisplayErrorfromSilver", jsEditor3Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs) as string;
                //return DisplayError(EditDocValidations.jsEditor3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
            }

            Editor_Launched = 1;
            HtmlPage.Window.Invoke("getAndSetValues", Editor_Launched, "no", "no", "");
            return "false";

        }

        [ScriptableMember]
        public string OpenPPTEditor(string sFileName,bool saveToDisk,string strFile)
        {
            dynamic wordDoc =null;
            var step = "";
            var isProtected = false;
            GetDocumentPath(strFile, "", "");
            try
            {//open template
                //step = 'open ppt document: ' + strForm;
                step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsOpenPPTDoc", "no", "yes", "").ToString() + ' ' + strForm;
                objWord = AutomationFactory.CreateObject("PowerPoint.Application");
                objWord.visible = true;
                wordDoc = objWord.Presentations.Open(strForm);

                //Try to save the document to make MSWord's SaveDate function work
                // objWord.ActiveWorkbook.Saveas(strForm)
            }
            catch (Exception e)
            {
                //DisplayError("The document at " + strForm + " could not be opened by Microsft PowerPoint because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);

                string jsPPT1Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsPPT1Error", "no", "yes", "") as string;
                string jsPPT2Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsPPT2Error", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                HtmlPage.Window.Invoke("DisplayErrorfromSilver", jsPPT1Error + " " + strForm + " " + jsPPT2Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);

                //DisplayError(EditDocValidations.jsPPT1Error + ' ' + strForm + ' ' + EditDocValidations.jsPPT2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                if (objWord != null)
                {
                    objWord.Quit(0);
                    objWord = null;
                    return "false";
                }
            }

            try
            {//Verify we have full edit rights.	Isprotected added by pawan for 10983
                if (objWord.ActivePresentation.ProtectionType != -1) //if not totally unlocked then
                {
                    //objWord.ActivePresentation.UnProtect("");
                    isProtected = true;
                }
            }
            catch (Exception e)
            {
                //alert("The word template at " + strForm + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
                objWord.visible = true;
                //objWord.Activate();
            }

            try
            {
                //step = 'activate ppt';
                step = "EditDocValidations.jsActivatePPT";
                objWord.visible = true;
                //objWord.Activate();

                // objWord.ActiveWorkbook.Select();

                //                if (saveToDisk) {
                //                    step = 'save the merged document over the original at ' + strForm;
                //                    objWord.ActiveWorkbook.Saveas(strForm);
                //                }

            }
            catch (Exception e)
            {
                //DisplayError("Microsoft PowerPoint could not accomplish the editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string jsPPT3Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsPPT3Error", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                HtmlPage.Window.Invoke("DisplayErrorfromSilver", jsPPT3Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
                //DisplayError(EditDocValidations.jsPPT3Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                objWord.Quit(0);
                objWord = null;
                return "false";
            }
            //Done
            return "true";
        }

        [ScriptableMember]
        public string OpenWordEditor(string sFileName,bool saveToDisk,string strFile) 
{
            dynamic wordDoc = null;
            var step = "";
            var isProtected = false;
            GetDocumentPath(strFile, "", "");
            try {//open template
                //step = 'open word document: ' + strForm;
                step = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsOpenWordDoc", "no", "yes", "").ToString() + ' ' + strForm;
                objWord = AutomationFactory.CreateObject("Word.Application");
                wordDoc = objWord.Documents.Open(strForm);

                //Try to save the document to make MSWord's SaveDate function work
                objWord.ActiveDocument.Saveas(strForm);
            }
            catch (Exception e) {
                //DisplayError("The word document at " + strForm + " could not be opened by Microsft Word because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string jsWordDoc1Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsWordDoc1Error", "no", "yes", "") as string;
                string jsWordDoc2Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsWordDoc2Error", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }

                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                HtmlPage.Window.Invoke("DisplayErrorfromSilver", jsWordDoc1Error + " " + strForm + " " + jsWordDoc2Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
                //DisplayError(EditDocValidations.jsWordDoc1Error + ' ' + strForm + ' ' + EditDocValidations.jsWordDoc2Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                
                if (objWord != null) {
                    objWord.Quit(0);
                    objWord = null;
                    return "false";
                }
            }

            try {//Verify we have full edit rights.	Isprotected added by pawan for 10983								   
                if (objWord.ActiveDocument.ProtectionType != -1) //if not totally unlocked then
                {
                    objWord.ActiveDocument.UnProtect("");
                    isProtected = true;
                }
            }
            catch (Exception e) {
                //alert("The word document at " + strForm + " is protected from changes.\nPlease unlock it and click the file name again. ");
                //alert(EditDocValidations.jsWordDoc1Error + ' ' + strForm + ' ' + EditDocValidations.jsWordDoc3Error.split('\\n').join('\n'));
                string jsWordDoc1Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsWordDoc1Error", "no", "yes", "") as string;
                string jsWordDoc3Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsWordDoc3Error", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                MessageBox.Show(jsWordDoc1Error + " " + strForm + " " + jsWordDoc3Error.Split('\n'));
                objWord.visible = true;
                objWord.Activate();
            }

            try {
                //step = 'activate Word';
                step = "EditDocValidations.jsActivateWord";
                objWord.visible = true;
                objWord.Activate();

                objWord.ActiveDocument.Select();

                if (saveToDisk) {
                    //step = 'save the document over the original at ' + strForm;
                    step ="$" + HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsDocumentSaving", "no", "yes", "").ToString() + ' ' + strForm;
                    objWord.ActiveDocument.Saveas(strForm);
                }

            }
            catch (Exception e) {
                //DisplayError("Microsoft Word could not accomplish the editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
                string jsWordDoc4Error = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsWordDoc4Error", "no", "yes", "") as string;
                string stepfromJs = string.Empty;
                if (step.Contains('$'))
                {
                    step.Replace("$", "");
                    stepfromJs = step;
                }
                else
                {
                    stepfromJs = HtmlPage.Window.Invoke("getAndSetValues", step, "no", "yes", "") as string;
                }
                string jsStep = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsStep", "no", "yes", "") as string;
                HtmlPage.Window.Invoke("DisplayErrorfromSilver", jsWordDoc4Error + " " + e.GetType() + ',' + e.Message + "\r\n" + jsStep + " " + stepfromJs);
                //DisplayError(EditDocValidations.jsWordDoc4Error + ' ' + e.name + ',' + e.message + '\r\n' + EditDocValidations.jsStep + ' ' + step);
                objWord.Quit(0);
                objWord = null;
                return "false";
            }
            //Done
            return "true";
        }

        [ScriptableMember]
        public string CleanAndCancelWord()
{
    try
    {
        if (objWord) { }
    }
    catch (Exception e)
    {
        return "false";
    }

    try
    {
        //pleaseWait.Show();
        HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "yes","no","no");
        if (IsWordOpen())
        {
            objWord.Quit(0);
            objWord = null;
        }

        //Clean Up.
        if (fso.FileExists(strForm)) //Main doc temp file.
            fso.DeleteFile(strForm);
    }
    catch (Exception e)
    { ; }
    //Shruti for 7337

    return "true";
}
        
        [ScriptableMember]
        public string CleanAndCancelPPT()
{

    try
    {
        if (objWord) { }
    }
    catch (Exception e)
    {
        return "false";
    }
    try
    {
        //pleaseWait.Show();
        HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "yes","no","no");
        if (IsPPTOpen())
        {
            //  alert("You must 'Close' the opened power point document.");
            //return false;
            if (objWord.ActivePresentation)
            {
                objWord.ActivePresentation.Close();
            }
        }
        objWord.Quit();
        objWord = null;
        //idTmr = window.setInterval("Cleanup();", 1);
        HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "yes", "no","no","no");

        //Clean Up.
        if (fso.FileExists(strForm)) //Main doc temp file.
            fso.DeleteFile(strForm);
    }
    catch (Exception e)
    { ; }
    //Shruti for 7337

    return "true";
}
        
        [ScriptableMember]
        public string CleanAndCancelXls()
{
    try
    {
        if (objWord) { }
        if (objWord == null) { return "false"; }
    }
    catch (Exception e)
    {
        return "true";
    }

    if (Editor_Launched == 0)
    {
        return "true";
    }
    try
    {


        try
        {
            //testing the below condition to check if excel is opened / not
            if (objWord.Workbooks)
            {
            }
        }
        catch (Exception e)
        {
            //alert("You must 'Close' the opened excel document.");
            string jsCloseExcel = HtmlPage.Window.Invoke("getAndSetValues", "EditDocValidations.jsCloseExcel", "no", "yes", "") as string;
            MessageBox.Show(jsCloseExcel);
            //alert(EditDocValidations.jsCloseExcel);
            return "false";
        }

        //pleaseWait.Show();
        HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "no", "yes","no","no");
        if (objWord.ActiveWorkbook)
        {
            objWord.ActiveWorkbook.Close(0);
        }

        objWord.Quit();
        objWord = null;
        //idTmr = window.setInterval("Cleanup();", 1);
        HtmlPage.Window.Invoke("pleasewaitAndsetInterval", "yes", "no","no","no");


        //Clean Up.
        if (fso.FileExists(strForm)) //Main doc temp file.
            fso.DeleteFile(strForm);
    }
    catch (Exception e)
    { ; }
    //Shruti for 7337

    return "true";
}
        
        [ScriptableMember]
        public void StreamAndSave(string template,string strForm) 
        {
    var step = "";
    var adTypeBinary = 1;
    var adSaveCreateOverWrite = 2;
    if (template.Length > 0) {
        //use xml object to convert base64 into array of byte   s
        step = "instantiate xml document";
        var xmlDoc = AutomationFactory.CreateObject("MSXML2.DOMDocument");
        step = "create b64 element";
        var e = xmlDoc.createElement("b64");
        step = "set element.dataType to bin.base64";
        e.dataType = "bin.base64";
        step = "set element.text to template base64 text";
        e.text = template;
        step = "get bytes from template element";
        var templateBytes = e.nodeTypedValue;

        //use ADODB.Stream to save template bytes to disc
        step = "instantiate Stream object for template";
        var stm =AutomationFactory.CreateObject("ADODB.Stream");
        step = "set template stm.Type";
        stm.Type = adTypeBinary;
        step = "open template stream";
        stm.Open();
        step = "write bytes to template stream";
        stm.Write(templateBytes);
        step ="$" + "save template stream to file " + this.strForm;
        stm.SaveToFile(this.strForm, adSaveCreateOverWrite);
        step = "close stream";
        stm.Close();
        stm = null;
        
    }
        }

        [ScriptableMember]
        public string SaveFileAndOpenOutlook(string strFile1)
        {
            fso = AutomationFactory.CreateObject("Scripting.FileSystemObject");
            if (string.IsNullOrEmpty(strFile1))
            {
                strForm = fso.GetSpecialFolder(2).Path + "\\" + "ExecutiveSummaryReport.pdf";
            }
            else
                strForm = fso.GetSpecialFolder(2).Path + "\\" + strFile1;
            return strForm;
        }

        [ScriptableMember]
        public void SendAttach(string emailidto,string emailidcc,string body,string subject,string attach1)
        {
             try {
                 var theApp = AutomationFactory.CreateObject("Outlook.Application");

            var theMailItem = theApp.CreateItem(0);
            theMailItem.to = (emailidto);
            theMailItem.cc = (emailidcc);
            theMailItem.Body = (body);
            theMailItem.Subject = (subject);
            if (attach1 != "") {
                theMailItem.Attachments.Add(attach1);
            }

            //Show the mail 
            theMailItem.display();
        }
        catch (Exception e) {
            MessageBox.Show("The following may have cause this error: \n" +
             "1. The Outlook 2007 is not installed on the machine.\n" +
             "2. The msoutl.olb is not availabe at the location " +
             "C:\\Program Files\\Microsoft Office\\OFFICE11\\msoutl.old on client's machine " +
             "due to bad installation of the office 2007." +
             "Re-Install office 2007 with default settings.\n" +
             "3. The Initialize and Scripts ActiveX controls not marked as safe is not set to enable.");

        }
        }

        [ScriptableMember]
         public string SaveAndOpenOutlook(dynamic docIds) {

            dynamic theApp,theMailItem;
            try {
                theApp = AutomationFactory.CreateObject("Outlook.Application");

                theMailItem = theApp.CreateItem(0);  // value 0 = MailItem
            }
            catch (Exception e) {
                /*alert("The following may have cause this error: \n" +
     "1. The Outlook express 2007 is not installed on the machine.\n" +
     "2. The msoutl.olb is not availabe at the location " +
     "C:\\Program Files\\Microsoft Office\\OFFICE11\\msoutl.old on client's machine " +
     "due to bad installation of the office 2007." +
     "Re-Install office2007 with default settings.\n" +
     "3. The Initialize and Scripts ActiveX controls not marked as safe is not set to enable." +
     "4. Outlook is not runnning on the machine.")*/
                //alert(DocumentListValidations.ValidCommonError.split('\\n').join('\n'));
                ValidErrorStepCheck = HtmlPage.Window.Invoke("getAndSetValues", "DocumentListValidations.ValidCommonError", "no", "yes", "") as string;
                ValidErrorStepCheck = ValidErrorStepCheck.Split('\n').ToString();
                MessageBox.Show(ValidErrorStepCheck);
                return "false";
            }



            docIds = docIds.split(',');
            for (int i = 0; i < docIds.Length; i++) {
                if (docIds[i] != null && docIds[i] != "") {
                    var tempid = "hdnFileContent" + i.ToString();
                    var id = HtmlPage.Window.Invoke("getAndSetValues", tempid, "no", "yes", "").ToString();
                    //var template = (document.getElementById(id).value);
                    var template = HtmlPage.Window.Invoke("getAndSetValues", "document.getElementById("+id+")", "no", "yes", "").ToString();
                    fso = AutomationFactory.CreateObject("Scripting.FileSystemObject");
                    var sFileName = docIds[i].split('~');
                    var strFile = sFileName[1];
                    strForm = fso.GetSpecialFolder(2).Path + "\\" + strFile;
                    StreamAndSave(template, strForm);

                }
            }

            theMailItem.display();
            return strForm;
        }
    }
}
