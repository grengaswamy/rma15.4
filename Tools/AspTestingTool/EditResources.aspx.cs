using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Riskmaster.Db;
using Riskmaster.Common;
using System.Reflection;
using System.Reflection.Emit;
using System.Resources;
using System.Globalization;
using System.Threading;

namespace AspTestingTool
{
	/// <summary>
	/// Summary description for EditResources.
	/// </summary>
	public class EditResources : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink lnkMain;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.DropDownList selResources;
		protected System.Web.UI.WebControls.Label lblSelResource;
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DropDownList selCultures;
		private Functions objFunctions;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidFormAction;
		private string m_connectionString;
		protected System.Web.UI.WebControls.Panel pnlText;
		protected System.Web.UI.WebControls.DataGrid dgText;
		protected System.Web.UI.WebControls.Panel pnlImages;
		protected System.Web.UI.WebControls.DataGrid dgImages;
		protected System.Web.UI.WebControls.Button btnCompile;
		private DataGrid dg=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			this.m_connectionString=this.Application["WebFarmSession"].ToString();
			objFunctions = new Functions(this.m_connectionString.ToString());

			//check if this is callback from ReplaceImage.aspx
			string s=Request["hidFormAction"]+"";
			if(s.Length>0)
			{
				string[] arr = s.Split("&".ToCharArray());
				string sAction = arr[0].Replace("action=","");
				string sId=arr[1].Replace("id=","");
				string sFile=arr[2].Replace("file=","");
				string sNewImageFile=String.Empty;
				string sValue=String.Empty;
				string sDefaultValue=String.Empty;

				string sSQL="Select Value,Default_Value From Resource_Dictionary Where Resource_Dictionary_Id="+sId;
				using(DbReader rdr = DbFactory.GetDbReader(Application["WebFarmSession"].ToString(),sSQL))
				{
					if(rdr.Read())
					{
						sValue=rdr.GetValue(0).ToString();
						sDefaultValue=rdr.GetValue(1).ToString();
					}
				}

				switch(sAction.ToLower())
				{
					case "change":	  //over-write the image with file the user chooses with file picker
						sNewImageFile=sFile;
						break;
					case "default":  //over-write the existing image with the default
						sNewImageFile=sDefaultValue;
						break;
					default:
						break;
				}

				//copy the new over the old
				try
				{
					System.IO.File.SetAttributes(sValue,System.IO.FileAttributes.Normal);
					System.IO.File.Copy(sNewImageFile,sValue,true);
				}
				catch(Exception ex)
				{
					throw new Exception(String.Format(Globalization.GetString("AspTestingTool.UnableToCopyFileError"),sNewImageFile,sValue),ex);
				}

				//refill the grid (view_state will not refill it properly)
				getResources(selResources.SelectedIndex.ToString());
			}

			if(!Page.IsPostBack)
			{
				Functions.getCultures(this.selCultures);
				objFunctions.getResourceSets(this.selResources,this.selCultures.SelectedValue.ToString());
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.selResources.SelectedIndexChanged += new System.EventHandler(this.selResources_SelectedIndexChanged);
			this.selCultures.SelectedIndexChanged += new System.EventHandler(this.selCultures_SelectedIndexChanged);
			this.dgText.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_CancelCommand);
			this.dgText.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_EditCommand);
			this.dgText.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_UpdateCommand);
			this.btnCompile.Click += new System.EventHandler(this.btnCompile_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void dg_EditCommand(object sender, DataGridCommandEventArgs e)
		{
			//each editable cell in this row is rendered as a text input
			dgText.EditItemIndex=e.Item.ItemIndex;
			string sType=ViewState["type"].ToString();
			getResources(selResources.SelectedIndex.ToString(),sType);

		}
		private void dg_CancelCommand(object sender, DataGridCommandEventArgs e)
		{
			dgText.EditItemIndex=-1;
			getResources(selResources.SelectedIndex.ToString(),ViewState["type"].ToString());
		}
		private void dg_UpdateCommand(object sender, DataGridCommandEventArgs e)
		{
			// Retrieve the updated value
			TextBox txtValue = (TextBox)e.Item.Cells[5].Controls[0];
			int groupID = Convert.ToInt32(e.Item.Cells[1].Text.ToString());
			string Value = txtValue.Text;

			//update database
			string sSQL="UPDATE RESOURCE_DICTIONARY SET VALUE='"+Value.ToString()+"' WHERE RESOURCE_DICTIONARY_ID=" + groupID;
			using(DbConnection DbConn = DbFactory.GetDbConnection(this.m_connectionString))
			{
				DbConn.Open();			
				DbCommand DbCmd  = DbConn.CreateCommand();
				DbCmd.CommandText=sSQL.ToString();
				DbCmd.ExecuteNonQuery();
			}

			//tell datagrid editing is over
			dgText.EditItemIndex=-1;
			getResources(selResources.SelectedIndex.ToString(),ViewState["type"].ToString());
		}
		private void selResources_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string sGroupId=selResources.SelectedIndex.ToString();
			if(sGroupId!="0")
			{
				getResources(sGroupId);
			}
			else
			{
				this.pnlImages.Visible=false;
				this.pnlText.Visible=false;
			}
		}

		private void selCultures_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.selResources.Items.Clear();
			objFunctions.getResourceSets(this.selResources,this.selCultures.SelectedValue.ToString());
		}

		/// <summary>
		/// Fetch all resources for the selected culture and type of resource.
		/// </summary>
		private void getResources(string sGroupId)
		{
			getResources(sGroupId,String.Empty);
		}

		private void btnCompile_Click(object sender, System.EventArgs e)
		{
			string sGroupId=selResources.SelectedIndex.ToString();
			if(sGroupId!="0")
				compileNewAssembly(sGroupId);

		}
		/// <summary>
		/// Fetch all resources for the selected culture and type of resource.
		/// </summary>
		private void getResources(string sGroupId,string sType)
		{	
			using(DbConnection DbConn = DbFactory.GetDbConnection(m_connectionString))
			{
				DbConn.Open();
				string sSQL=String.Empty;
				DbReader rdr=null;
				if(sType==String.Empty)
				{
					sSQL="Select Type From Resource_Dictionary_Groups Where Group_Id = "+sGroupId;
					using(rdr = DbConn.ExecuteReader(sSQL))
					{	
						if(rdr.Read())
							sType=rdr.GetValue(0).ToString();
					}
				}
				this.ViewState["type"]=sType;   //store in viewstate so not need to fetch from database when editing dgText
				switch(sType.ToLower())
				{
					case "images":
						this.pnlText.Visible=false;
						this.pnlImages.Visible=true;
						sSQL="Select RS.Resource_Dictionary_ID,RS.Description,RS.Default_Value,RS.Value,RS.Name From Resource_Dictionary RS,Resource_Dictionary_Groups RSG Where RS.Group_ID=RSG.Group_ID And RSG.Group_ID="+sGroupId+" Order By RS.Name, RS.Value";
						dg=this.dgImages;
						dg.Columns[1].Visible=false;
						dg.Columns[2].Visible=false;
						dg.Columns[3].Visible=false;
						btnCompile.Visible=false;
						break;
					case "resources":
						this.pnlImages.Visible=false;
						this.pnlText.Visible=true;
						dg=this.dgText;
						sSQL="Select RS.Resource_Dictionary_ID,RS.Name,RS.Type,RS.Default_Value,RS.Value From Resource_Dictionary RS,Resource_Dictionary_Groups RSG Where RS.Group_ID=RSG.Group_ID And RSG.Group_ID="+sGroupId+" Order By RS.Name, RS.Value";
						//do not need TYPE.  (note column order comes from aspx page, not the sql statement)
						dg.Columns[2].Visible=true;
						dg.Columns[3].Visible=false;
						btnCompile.Visible=true;
						break;
					case "page":
						this.pnlImages.Visible=false;
						this.pnlText.Visible=true;
						dg=this.dgText;
						sSQL="Select RS.Resource_Dictionary_ID,RS.Name,RS.Type,RS.Default_Value,RS.Value From Resource_Dictionary RS,Resource_Dictionary_Groups RSG Where RS.Group_ID=RSG.Group_ID And Default_Value Is Not Null and Default_Value != '' And RSG.Group_ID=" + sGroupId + " Order By RS.Type,RS.Value";
						//do not need NAME
						dg.Columns[2].Visible=false;
						dg.Columns[3].Visible=true;
						btnCompile.Visible=false;
						break;
					default:
						this.pnlImages.Visible=false;
						this.pnlText.Visible=true;
						dg=this.dgText;
						sSQL="SELECT RS.* From Resource_Dictionary RS,Resource_Dictionary_Groups RSG Where RS.Group_ID=RSG.Group_ID And RSG.Group_ID="+sGroupId+" Order By RS.Name, RS.Value";
						btnCompile.Visible=true;
						break;
				}
				dg.Visible=true;
			
				using(rdr = DbConn.ExecuteReader(sSQL))
				{
					dg.DataSource=rdr;
					dg.DataBind();
				}
			}
		}


		/// <summary>
		/// Creates a new satellite assembly for the given culture and group id.
		/// </summary>
		/// <param name="sGroupId">Resource_Dictionary.Group_Id</param>
		/// <param name="sCulture">Resource_Dictionary_Group.Culture</param>
		private void compileNewAssembly(string sGroupId)
		{
			using(DbConnection DbConn = DbFactory.GetDbConnection(this.m_connectionString))
			{
				string sResx=String.Empty;
				string sCulture=String.Empty;
				DbConn.Open();	
				DbReader rdr=null;

				string sSQL="Select [Name],Culture From Resource_Dictionary_Groups Where Group_Id="+sGroupId;
				using(rdr=DbConn.ExecuteReader(sSQL))
				{
					if(rdr.Read())
					{
						sResx=rdr.GetString(0);
						sCulture=rdr.GetString(1);
					}
				}

				if(sResx.Length==0 || sCulture.Length==0)
					throw new Exception("Error fetching name and culture from Resource_Dictionary, please notify the system administrator.");

				//.resx file will be in 3 parts, [AssemblyName].Global.resx
				string[] arr = sResx.Split(".".ToCharArray());
				if(arr.Length!=3)
					throw new Exception("Resource name is not in expected format, please notify the system administrator.");

				string sAssemblyName=arr[0];
				string sFileName=arr[1];  //always "Global"?
				string sPath=Server.MapPath("").ToString()+"\\bin\\" + sCulture;

				#region Traces
				Trace.Write("compileNewAssembly","Assembly Name="+sAssemblyName);
				Trace.Write("compileNewAssembly","File Name="+sFileName);
				Trace.Write("compileNewAssembly","Path="+sPath);
				#endregion

				AssemblyBuilder ab=null;
				IResourceWriter rw=null;
				try
				{
					AssemblyName an = new AssemblyName();
					an.Name = String.Concat(sAssemblyName,".resources");
					an.CultureInfo = new CultureInfo(sCulture);
					an.CodeBase = String.Concat("file:///",sPath.ToString());

					Trace.Write("compileNewAssembly","AssemblyName object created.");

					AppDomain domain = Thread.GetDomain();
					ab = domain.DefineDynamicAssembly(an,AssemblyBuilderAccess.RunAndSave,sPath.ToString());
					ModuleBuilder mb = ab.DefineDynamicModule(sAssemblyName+".resources.dll",sAssemblyName+".resources.dll",true);
					rw = mb.DefineResource(sAssemblyName+"." + sFileName + "."+sCulture+".resources",sFileName+"."+sCulture+".resources",ResourceAttributes.Public);

					Trace.Write("compileNewAssembly","AssemblyBuilder,ModuleBuilder,ResourceWriter objects created.");

					//Resource_Dictionary.Name is key
					sSQL="Select Name,Value From Resource_Dictionary Where Group_Id="+sGroupId;
					using(rdr=DbConn.ExecuteReader(sSQL))
					{
						while(rdr.Read())
						{
							rw.AddResource(rdr.GetString(0),rdr.GetString(1));
						}
					}
					//note: do not dispose of writer until AFTER save
					ab.Save(sAssemblyName+".resources.dll");
				}
				catch(Exception ex)
				{
					throw new Exception("Error compiling new resource dll",ex);
				}
				finally
				{
					if(rw!=null )
						rw.Dispose();
				}
			}

		}
	}
}
