<%@ Page language="c#" Codebehind="ResourceReader.aspx.cs" AutoEventWireup="false" Inherits="AspTestingTool.ResourceReader" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ResourceReader</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:label id="Label1" style="Z-INDEX: 102; LEFT: 26px; POSITION: absolute; TOP: 133px" runat="server"
				Width="161px" Height="16px">  Select culture</asp:label><asp:hyperlink id="lnkMain" style="Z-INDEX: 105; LEFT: 311px; POSITION: absolute; TOP: 69px" runat="server"
				Width="186px" Height="26px" NavigateUrl="Main.aspx">Return to Main</asp:hyperlink><asp:image id="Image1" style="Z-INDEX: 103; LEFT: 21px; POSITION: absolute; TOP: 11px" runat="server"
				Width="479px" Height="44px" ImageUrl="Resource.ashx?img=top1.gif"></asp:image><asp:label id="lblTitle" style="Z-INDEX: 101; LEFT: 24px; POSITION: absolute; TOP: 61px" runat="server"
				Width="249px" Height="33px" Font-Size="X-Large">Read Resources</asp:label>
			<asp:button id="btnReadResources" style="Z-INDEX: 104; LEFT: 29px; POSITION: absolute; TOP: 187px"
				runat="server" Width="110px" Height="32px" Text="Read From DLL"></asp:button><asp:table id="tblResources" style="Z-INDEX: 106; LEFT: 32px; POSITION: absolute; TOP: 232px"
				runat="server" Width="725px" Height="202px" GridLines="Both" BorderStyle="Solid" Visible="False"></asp:table><asp:button id="btnReadResx" style="Z-INDEX: 107; LEFT: 256px; POSITION: absolute; TOP: 178px"
				runat="server" Width="123px" Height="39px" Text="Read From .Resx"></asp:button>
			<asp:DropDownList id="selCultures" style="Z-INDEX: 108; LEFT: 30px; POSITION: absolute; TOP: 158px"
				runat="server" Height="24px" Width="172px"></asp:DropDownList>
			<asp:Label id="Label2" style="Z-INDEX: 109; LEFT: 219px; POSITION: absolute; TOP: 134px" runat="server"
				Height="20px" Width="33px">Or</asp:Label>
			<asp:Label id="Label3" style="Z-INDEX: 110; LEFT: 259px; POSITION: absolute; TOP: 128px" runat="server"
				Height="46px" Width="75px">Select .Resx file</asp:Label><INPUT style="Z-INDEX: 111; LEFT: 344px; WIDTH: 426px; POSITION: absolute; TOP: 150px; HEIGHT: 26px"
				type="file" size="51" id="fileResx" runat="server">
		</form>
	</body>
</HTML>
