using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Resources;
using Riskmaster.Common;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace AspTestingTool
{
	/// <summary>
	/// Summary description for ReplaceTokens.
	/// </summary>
	public class ReplaceTokens : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnReplaceTokens;
		protected System.Web.UI.WebControls.Label lblResults;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.HyperLink lnkMain;
		protected System.Web.UI.WebControls.DropDownList selCultures;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			//fetch all subfolders under bin, put in resource picker dropdown
			if(!Page.IsPostBack)
				Functions.getCultures(this.selCultures);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnReplaceTokens.Click += new System.EventHandler(this.btnReplaceTokens_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	/// <summary>
	/// 1.  Find the master javascript files.
	/// 2.  Fetch all tokens inside each file and replace the token with the appropriate string from the resource dll.
	/// 3.  Create a new javascript file in the appropriate language folder.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
		private void btnReplaceTokens_Click(object sender, System.EventArgs e)
		{
			StringBuilder sResults=new StringBuilder(String.Empty);
			string sLang=this.selCultures.SelectedValue;
			string sMasterFolder = HttpContext.Current.Server.MapPath("").ToString()+"\\master\\js";
			string sLangFolder = HttpContext.Current.Server.MapPath("").ToString()+"\\bin\\"+sLang;
			if(!Directory.Exists(sMasterFolder))
			{
				//raise error, this must exist or serious problem has occured
				//Response.Write("Error, the folder "+sMasterFolder+"does not exist, please notify the system administrator.");
				Response.Write(String.Format(Globalization.GetString("AspTestingTool.MissingFolderError"),sMasterFolder));
				Response.End();
			}
			if(!Directory.Exists(sLangFolder))
			{
				//raise error, this must exist or serious problem has occured
				//Response.Write("Error, the folder "+sLangFolder+"does not exist, please notify the system administrator.");
				Response.Write(String.Format(Globalization.GetString("AspTestingTool.MissingFolderError"),sLangFolder));
				Response.End();
			}

			//sResults.Append("The following files were successfully generated:");
			sResults.Append(Globalization.GetString("AspTestingTool.ReplaceTokens.btnReplaceTokens_Click.Success"));
			string sFile;
			string sNewFile;
			DirectoryInfo di = new DirectoryInfo(sMasterFolder);
			FileInfo[] arrFI = di.GetFiles();
			foreach (FileInfo fi in arrFI)
			{
				sFile=fi.Name.ToString();
				//StreamReader sr = new StreamReader(sMasterFolder+"\\"+sFile);
				StreamReader sr = fi.OpenText();
				string sFileContents = sr.ReadToEnd();
				sr.Close();

				sNewFile=sLangFolder+"\\"+sFile;

				//cannot use ResourceManager if want to force-load the parent neutral culture when the default specific culture exists
				//(eg, if language is "en" and not want resources from "en-US"; "en-US" is the default specific culture for the neutral culture "en", resource manager will always choose the default specific culture
				CultureInfo ci = new CultureInfo(sLang,false);
				ResourceManager rm = Globalization.ResourceManager;
				ResourceSet rs = rm.GetResourceSet(ci,true,true);
				Regex reg = new Regex("<~(.|\n)+?~>",RegexOptions.IgnoreCase|RegexOptions.Compiled);
				Match m;
				string sTag;
				string sText;
				string sIdentifier;
				string sNewText;
				for (m = reg.Match(sFileContents.ToString()); m.Success; m = m.NextMatch()) 
				{
					sTag=m.Value;
					sIdentifier=sTag.Substring(2,sTag.IndexOf("\"")-2);
					sText=sTag.Remove(0,sTag.IndexOf("\""));
					sText=sText.Replace("~>","");
					sNewText="\""+rs.GetString(sIdentifier)+"\"";
					sFileContents=sFileContents.Replace(sTag,sNewText);
				}
				StreamWriter sw=new StreamWriter(sNewFile.ToString(),false);
				sw.Write(sFileContents);
				sw.Close();
				sResults.Append("\r\n"+sNewFile.ToString());
			}
			this.lblResults.Text=sResults.ToString();
		}
	}
}
