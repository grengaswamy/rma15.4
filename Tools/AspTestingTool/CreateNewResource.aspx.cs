using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Resources;
using System.IO;
using System.Globalization;
using Riskmaster.Common;
using System.Threading;
using System.Reflection;
using System.Reflection.Emit;


namespace AspTestingTool
{
	/// <summary>
	/// Summary description for CreateNewResource.
	/// </summary>
	public class CreateNewResource : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink lnkMain;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Image Image1;
		TableRow tRow;
		TableCell tCell;
		protected System.Web.UI.WebControls.Table tblResources;
		protected System.Web.UI.WebControls.Button btnCompile;
		protected System.Web.UI.WebControls.DropDownList selCultures;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			Functions.getCultures(this.selCultures);
			char [] delimiter = "|".ToCharArray();
			string sFormAction=this.Request["hidFormAction"]+"";
			string[] s=sFormAction.Split(delimiter);
			if(s.Length>1)
			{
				switch(s[0].ToLower().ToString())
				{
					case "addresource":
						AddResource(s);
						break;
					default:
						break;
				}
			}
		}
		/// <summary>
		/// Add image resources to a .resource file
		/// </summary>
		/// <param name="s"></param>
		private void AddResource(string[] s)
		{
			string sKey=s[1].ToString();
			string sFile=s[2].ToString();
			string sAllOrOne=s[3].ToString();

			string sLang=this.selCultures.SelectedValue;

			/* tkr there seems to be no means to edit a .resx OR a .resources file.  
				* if use ResXResourceReader, they are overwritten when open from disk, or corrupted if re-open from stream
				* if use ResourceReader are overwritten in either case
				* instead will (over)write and compile .resources file
			*/

			string sResource=Server.MapPath("").ToString()+"\\bin\\" + sLang + "\\Global."+sLang+".resources";
			ResourceWriter rsw = new ResourceWriter(sResource);  //sResource is overwritten once .AddResource is called
			System.Drawing.Image img;

			if(sAllOrOne.ToLower()=="all")
			{
				//user wants to add all files in the directory to resource file
				sFile=sFile.Substring(0,sFile.LastIndexOf("\\")).ToString();
				DirectoryInfo di = new DirectoryInfo(sFile);
				FileInfo[] arrFI = di.GetFiles();
				foreach (FileInfo fi in arrFI)
				{
					try
					{
						img = System.Drawing.Image.FromFile(fi.FullName.ToString());
						if(img!=null)
							rsw.AddResource(fi.Name,img);
					}
					catch{}
				}
			}
			else
			{
				img = System.Drawing.Image.FromFile(sFile);
				rsw.AddResource(sKey,img);
			}

			//now pull all items from existing resource set and add those (.AddResource overwrites any existing .resource file with a new .resource file)
			CultureInfo ci = new CultureInfo(sLang,false);
			ResourceManager rm = Globalization.ResourceManager;
			ResourceSet rs = rm.GetResourceSet(ci,true,true);
			IDictionaryEnumerator en = rs.GetEnumerator();
			string sType=String.Empty;
			while (en.MoveNext())
			{
				sType=en.Value.GetType().ToString();
				switch(sType.ToLower())
				{
					case "system.string":
						rsw.AddResource(en.Key.ToString(),en.Value.ToString());
						break;
					case "system.drawing.bitmap":
						img=(System.Drawing.Image)rs.GetObject(en.Key.ToString());
						rsw.AddResource(en.Key.ToString(),img);
						break;
					default:
						break;
				}
			}

			//close the resource writer (this saves .resources file to disk and closes any open streams as well)
			rsw.Close();

			//enumerate the new .resource file
			System.Resources.ResourceReader rsr = new System.Resources.ResourceReader(sResource);
			en = rsr.GetEnumerator();
			enumDictItems(en);
			rsr.Close();

			tblResources.Visible=true;
			/*
			//select the currently-selected folder when page reloads
			for(int i=0;i<this.selResourceFolders.Items.Count;i++)
			{
				if(selResourceFolders.Items[i].Text.ToString()==sLang)
				{
					selResourceFolders.SelectedIndex=i;
					break;
				}
			}
			*/
		}

		private void CompileNewAssembly()
		{
			string sLang=this.selCultures.SelectedValue;
			string sResource=Server.MapPath("").ToString()+"\\bin\\" + sLang + "\\Global."+sLang+".resources";
			string sPath=Server.MapPath("").ToString()+"\\bin\\" + sLang;

			/* this commented section works.  removed to avoid need to use Process.Start(), 
			 * which neccessitates changing machine.config
			//note: see below link for /template argument, causes
			//satellite assembly to inherit metadata from a strong-named parent dll.
			//http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpguide/html/cpconresourcesinasppages.asp
			string sCommand="al /out:AspTestingTool.resources.dll /v:1.0.0.0 /c:de /embed:Global."+sLang+".resources,AspTestingTool.Global."+sLang+".resources";
			CompileSource.DoCompile(sCommand,Server.MapPath("").ToString()+"\\bin\\" + sLang);
			*/
			AssemblyName an = new AssemblyName();
			an.Name = "AspTestingTool.resources"; 
			an.CultureInfo = new CultureInfo(sLang);
			an.CodeBase = String.Concat("file:///",sPath.ToString());

			AppDomain domain = Thread.GetDomain();
			AssemblyBuilder ab = domain.DefineDynamicAssembly(an,AssemblyBuilderAccess.RunAndSave,sPath.ToString());
			ModuleBuilder mb = ab.DefineDynamicModule("AspTestingTool.resources.dll","AspTestingTool.resources.dll",true);

			IResourceWriter rw = mb.DefineResource("AspTestingTool.Global."+sLang+".resources","Global."+sLang+".resources",ResourceAttributes.Public);
			
			//there is no way to load an entire .resource file into the module.  must iterate each resource and add it individually
			//fill resource reader with the .resource file
			System.Resources.ResourceReader rsr = new System.Resources.ResourceReader(sResource);
			
			//need resourceset object for its GetObject function, to return images
			//this only need to retrieve images; if just getting strings could use en.Value.ToString()
			ResourceSet rs = new ResourceSet(rsr);

			System.Drawing.Image img;
			string sType=String.Empty;
			IDictionaryEnumerator en = rsr.GetEnumerator();
			while(en.MoveNext())
			{
				sType=en.Value.GetType().ToString();
				switch(sType.ToLower())
				{
					case "system.string":
						rw.AddResource(en.Key.ToString(),en.Value.ToString());
						break;
					case "system.drawing.bitmap":
						img=(System.Drawing.Image)rs.GetObject(en.Key.ToString());
						rw.AddResource(en.Key.ToString(),img);
						break;
					default:
						break;
				}
				
			}
			rsr.Close();
		
			ab.Save("AspTestingTool.resources.dll");   

		}
		private void enumDictItems(IDictionaryEnumerator en)
		{
			string sType;
			while (en.MoveNext())
			{
				sType=en.Value.GetType().ToString();

				tRow = new TableRow();
				tblResources.Rows.Add(tRow);

				tCell = new TableCell();
				tCell.Text = en.Key.ToString();
				tRow.Cells.Add(tCell);

				tCell = new TableCell();
				tCell.Text = sType;
				tRow.Cells.Add(tCell);

				tCell = new TableCell();
				switch(sType.ToLower())
				{
					case "system.string":
						tCell.Text = en.Value.ToString();
						break;
					default:
						break;
				}
				tRow.Cells.Add(tCell);
				
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCompile.Click += new System.EventHandler(this.btnCompile_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnCompile_Click(object sender, System.EventArgs e)
		{
			CompileNewAssembly();
		}
	}
}
