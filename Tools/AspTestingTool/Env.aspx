<%@ Page language="c#" Codebehind="Env.aspx.cs" AutoEventWireup="false" Inherits="AspTestingTool.Env" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Env</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:Image id="Image1" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"
				ImageUrl="Resource.ashx?img=top1.gif" Height="44px" Width="479px"></asp:Image>
			<asp:table id="tblResources" style="Z-INDEX: 107; LEFT: 24px; POSITION: absolute; TOP: 111px"
				runat="server" Height="202px" Width="647px" GridLines="Both" BorderStyle="Solid"></asp:table>
			<asp:Label id="lblTitle" style="Z-INDEX: 101; LEFT: 21px; POSITION: absolute; TOP: 61px" runat="server"
				Height="33px" Width="331px" Font-Size="X-Large"> Environment Variables </asp:Label>
			<asp:HyperLink id="lnkMain" style="Z-INDEX: 102; LEFT: 384px; POSITION: absolute; TOP: 71px" runat="server"
				Height="26px" Width="106px" NavigateUrl="Main.aspx">Return to Main</asp:HyperLink>
		</form>
	</body>
</HTML>
