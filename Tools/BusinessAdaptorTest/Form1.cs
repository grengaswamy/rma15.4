using System;
using System.Drawing;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Reflection;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;

namespace BusinessAdaptorTest
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtAssemblyName;
		private System.Windows.Forms.TextBox txtClassName;
		private System.Windows.Forms.TextBox txtMethodName;
		private System.Windows.Forms.Label labInputXML;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button btnCall;
		private System.Windows.Forms.TextBox txtXMLIn;
		private System.Windows.Forms.TextBox txtXMLOut;

		private Login oLogin = new Login();
		private UserLogin oUserLogin = null;
		private System.Windows.Forms.TextBox txtErrXML;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtAssemblyName = new System.Windows.Forms.TextBox();
			this.txtClassName = new System.Windows.Forms.TextBox();
			this.txtMethodName = new System.Windows.Forms.TextBox();
			this.labInputXML = new System.Windows.Forms.Label();
			this.txtXMLIn = new System.Windows.Forms.TextBox();
			this.txtXMLOut = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtErrXML = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.btnCall = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Assembly Name:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 16);
			this.label2.TabIndex = 1;
			this.label2.Text = "Class Name:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 72);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(96, 16);
			this.label3.TabIndex = 2;
			this.label3.Text = "Method Name:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// txtAssemblyName
			// 
			this.txtAssemblyName.Location = new System.Drawing.Point(112, 24);
			this.txtAssemblyName.Name = "txtAssemblyName";
			this.txtAssemblyName.Size = new System.Drawing.Size(408, 20);
			this.txtAssemblyName.TabIndex = 3;
			this.txtAssemblyName.Text = "";
			// 
			// txtClassName
			// 
			this.txtClassName.Location = new System.Drawing.Point(112, 48);
			this.txtClassName.Name = "txtClassName";
			this.txtClassName.Size = new System.Drawing.Size(408, 20);
			this.txtClassName.TabIndex = 4;
			this.txtClassName.Text = "";
			// 
			// txtMethodName
			// 
			this.txtMethodName.Location = new System.Drawing.Point(112, 72);
			this.txtMethodName.Name = "txtMethodName";
			this.txtMethodName.Size = new System.Drawing.Size(408, 20);
			this.txtMethodName.TabIndex = 5;
			this.txtMethodName.Text = "";
			// 
			// labInputXML
			// 
			this.labInputXML.Location = new System.Drawing.Point(16, 104);
			this.labInputXML.Name = "labInputXML";
			this.labInputXML.Size = new System.Drawing.Size(96, 16);
			this.labInputXML.TabIndex = 6;
			this.labInputXML.Text = "Input XML Doc:";
			this.labInputXML.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.labInputXML.Click += new System.EventHandler(this.label4_Click);
			// 
			// txtXMLIn
			// 
			this.txtXMLIn.Location = new System.Drawing.Point(112, 104);
			this.txtXMLIn.Multiline = true;
			this.txtXMLIn.Name = "txtXMLIn";
			this.txtXMLIn.Size = new System.Drawing.Size(408, 128);
			this.txtXMLIn.TabIndex = 7;
			this.txtXMLIn.Text = "";
			// 
			// txtXMLOut
			// 
			this.txtXMLOut.BackColor = System.Drawing.SystemColors.InactiveBorder;
			this.txtXMLOut.Location = new System.Drawing.Point(112, 248);
			this.txtXMLOut.Multiline = true;
			this.txtXMLOut.Name = "txtXMLOut";
			this.txtXMLOut.Size = new System.Drawing.Size(408, 128);
			this.txtXMLOut.TabIndex = 9;
			this.txtXMLOut.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(16, 248);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 16);
			this.label4.TabIndex = 8;
			this.label4.Text = "Output XML Doc:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// txtErrXML
			// 
			this.txtErrXML.BackColor = System.Drawing.SystemColors.InactiveBorder;
			this.txtErrXML.Location = new System.Drawing.Point(112, 384);
			this.txtErrXML.Multiline = true;
			this.txtErrXML.Name = "txtErrXML";
			this.txtErrXML.Size = new System.Drawing.Size(408, 128);
			this.txtErrXML.TabIndex = 11;
			this.txtErrXML.Text = "";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 384);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(96, 16);
			this.label5.TabIndex = 10;
			this.label5.Text = "Errors:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// btnCall
			// 
			this.btnCall.Location = new System.Drawing.Point(224, 520);
			this.btnCall.Name = "btnCall";
			this.btnCall.Size = new System.Drawing.Size(120, 24);
			this.btnCall.TabIndex = 12;
			this.btnCall.Text = "Call";
			this.btnCall.Click += new System.EventHandler(this.btnCall_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(536, 558);
			this.Controls.Add(this.btnCall);
			this.Controls.Add(this.txtErrXML);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.txtXMLOut);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtXMLIn);
			this.Controls.Add(this.labInputXML);
			this.Controls.Add(this.txtMethodName);
			this.Controls.Add(this.txtClassName);
			this.Controls.Add(this.txtAssemblyName);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Business Adaptor Test Utility";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void label1_Click(object sender, System.EventArgs e)
		{
		
		}

		private void label4_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnCall_Click(object sender, System.EventArgs e)
		{
			XmlDocument xmlIn = new XmlDocument();
			XmlDocument xmlOut = null;
			BusinessAdaptorErrors errOut = null;

			// Load up XML inbound document
			xmlIn.LoadXml(txtXMLIn.Text);

			// Load the assembly
			Assembly a = Assembly.LoadFrom(txtAssemblyName.Text);

			// Get the type of the class
			Type t = a.GetType(txtClassName.Text);

			// Create an instance of the class
			Object obj = Activator.CreateInstance(t);

			// Call the IBusinessAdaptor interface to initialize security information
			IBusinessAdaptor pBusAdaptor = (IBusinessAdaptor) obj;
			pBusAdaptor.SetSecurityInfo(oUserLogin);

			// Create a delegate and bind it to the method we want to call
			BusinessAdaptorMethod methodDelegate = 
				(BusinessAdaptorMethod) BusinessAdaptorMethod.CreateDelegate(typeof(BusinessAdaptorMethod), obj, txtMethodName.Text, true);

			// Call the method
			bool bResult = methodDelegate(xmlIn, ref xmlOut, ref errOut);

			// Commented code below is solution before delegate method was introduced
			// Lookup method in type
			// MethodInfo m = t.GetMethod(txtMethodName.Text);

			// Invoke the method
			// Object[] pArr = new Object[]{xmlIn, xmlOut, errOut};
			// Object oRet = t.InvokeMember(txtMethodName.Text, 
			//							BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.Public,
			//							 null, obj, pArr);
			// bResult = (bool) oRet;
			// xmlOut = (XmlDocument) pArr[1];
			// errOut = (BusinessAdaptorErrors) pArr[2];

			// Dump output to form
			txtXMLOut.Text = xmlOut.OuterXml;

			// Format and dump errors to XML
			XmlElement xmlElement;  // generic reusable element var
			XmlElement xmlMsgsRoot;
			const string outNamespace = "http://csc.com/Riskmaster/BusinessAdaptor/Common";

			XmlDocument xmlErr = new XmlDocument();
			XmlElement xmlErrRoot = xmlErr.CreateElement("MsgStatus", outNamespace);
			xmlErr.AppendChild(xmlErrRoot);

			// ... add overall result status
			xmlElement = xmlErr.CreateElement("MsgStatusCd", outNamespace);
			if (bResult)
				xmlElement.InnerText = "Success";
			else
				xmlElement.InnerText = "Error";

			xmlErrRoot.AppendChild(xmlElement);

			// ... add individual error messages/warnings (if no errors, there will still be an empty ExtendedStatus element)
			xmlMsgsRoot = xmlErr.CreateElement("ExtendedStatus", outNamespace);
			xmlErrRoot.AppendChild(xmlMsgsRoot);

			if (errOut != null)
			{
				foreach (BusinessAdaptorError err in errOut)
				{
					// ... add error code/number
					xmlElement = xmlErr.CreateElement("ExtendedStatusCd", outNamespace);
					xmlElement.InnerText = err.ErrorCode;
					xmlMsgsRoot.AppendChild(xmlElement);

					// ... add error description
					xmlElement = xmlErr.CreateElement("ExtendedStatusDesc", outNamespace);
					xmlElement.InnerText = err.ErrorDescription;
					xmlMsgsRoot.AppendChild(xmlElement);

					// ...add error type
					xmlElement = xmlErr.CreateElement("ExtendedMsgType", outNamespace);
					switch(err.ErrorType)
					{
						case BusinessAdaptorErrorType.SystemError:
							xmlElement.InnerText = "SystemError";
							break;
						case BusinessAdaptorErrorType.Error:
							xmlElement.InnerText = "Error";
							break;
						case BusinessAdaptorErrorType.Warning:
							xmlElement.InnerText = "Warning";
							break;
						case BusinessAdaptorErrorType.Message:
							xmlElement.InnerText = "Message";
							break;
						case BusinessAdaptorErrorType.PopupMessage:
							xmlElement.InnerText = "PopupMessage";
							break;
						default:
							// TODO   - What to do if not a standard error code?
							break;
					};
					xmlMsgsRoot.AppendChild(xmlElement);

				}
			}

			// Display error xml
			txtErrXML.Text = xmlErr.OuterXml;
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			if (!oLogin.RegisterApplication(0, 0, ref oUserLogin))
				Application.Exit();
		}
	}
}
