﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-4042         | ajohari2   | Underwriters - EFT Payments Part 2
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Security;
using System.Configuration;
using Riskmaster.Db;
using System.Collections;
using Riskmaster.Application.PrintChecks;
using Amyuni.PDFCreator;
using System.IO;
using System.Windows.Forms;
using C1.C1Zip;
using System.Data;
namespace BatchCheckPrint
{
    public class Program
    {
        static string m_JobId = string.Empty;
        private static List<string> m_lstCheckFileNames = null;
        private static List<string> m_lstPreCheckFileNames = null;
        private static List<string> m_lstPostCheckFileNames = null;
        private static List<string> m_lstEOBCheckFileNames = null;
        private static List<string> m_lstHoldPaymentFileNames = null;
        // akaushik5 Added for MITS 38045 Starts
        private static UserLogin oUserLogin = null;
        // akaushik5 Added for MITS 38045 Ends
        static int iClientId = 0;//dvatsa-cloud
        public static void Main(string[] args)
        {
           // args = new string[6];
           // args[0] = "rmACloud";
           // args[1] = "cloud";
           //args[2] = "fa6db697515ef18b";
           //args[3] = "10";
            // (a)      No 4th argument as ClientID won't come in base and other is optional
            // (b)      args[4] = "1";// clientID 
            // or 
            // (c)      args[4] = "9-4-0-(None)-true-false-detail"; //optional params
            // or 
            // (d)    //args[4] = "0"; //clientID
                      //args[5] = "9-4-0-(None)-true-false-detail"; //optional params

            // akaushik5 Commented for MITS 38045 Starts
            //UserLogin oUserLogin = null;
            // akaushik5 Commented for MITS 38045 Ends
            string sPath = string.Empty;
            bool blnSuccess = false;//dvatsa-cloud
            // akaushik5 Added for MITS 38045 Starts
            int startIndex = 4;
            // akaushik5 Added for MITS 38045 Ends
            try
            {
                Console.WriteLine("0 ^*^*^ Check print batch job started.");

                //dvatsa-cloud
                if (args != null && args.Length > 4 && (string.IsNullOrEmpty(args[4]) == false))
                {
                    try
                    {// if 4th argument is clientID
                        iClientId = int.Parse(args[4]);
                        // akaushik5 Added for MITS 38045 Starts
                        startIndex = 5;
                        // akaushik5 Added for MITS 38045 Ends
                    }
                    catch
                    {// if 4th argument is not clientID
                        iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                    }
                }
                //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
                //By this we can run Tool through exe for different client by change ClientId in appSetting.config.
                else
                {
                    iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                }
                //dvatsa-cloud
                if (args.Length > 0)
                {
                    oUserLogin = new UserLogin(args[1], args[0], iClientId);//dvatsa-cloud
                    if (oUserLogin.DatabaseId <= 0)
                    {
                        Console.WriteLine("0 ^*^*^ Authentication failure.");
                        throw new Exception("Authentication failure.");
                    }

                    m_JobId = args[3];

                    m_lstPreCheckFileNames = new List<string>();
                    m_lstPostCheckFileNames = new List<string>();
                    m_lstCheckFileNames = new List<string>();
                    m_lstEOBCheckFileNames = new List<string>();
                    m_lstHoldPaymentFileNames = new List<string>();
                    // akaushik5 Changed for MITS 38045 Starts
                    //for (int i = args.Length - 1; i < args.Length; i++)
                    for (int i = startIndex; i < args.Length; i++)
                    // akaushik5 Changed for MITS 38045 Ends
                    {
                        if (args[i] != "-a")
                        {
                            PrintBatch(oUserLogin, args[i]);
                        }
                    }
                    sPath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PrintChecks");
                    if (m_lstPreCheckFileNames.Count > 0) AppendFile("PreCheckRegisterFile_", m_lstPreCheckFileNames.ToArray());
                    if (m_lstPostCheckFileNames.Count > 0) AppendFile("PostCheckRegisterFile_", m_lstPostCheckFileNames.ToArray());
                    if (m_lstCheckFileNames.Count > 0) AppendFile("PrintCheck_", m_lstCheckFileNames.ToArray());
                    if (m_lstEOBCheckFileNames.Count > 0) AppendFile("PrintCheck_", m_lstEOBCheckFileNames.ToArray());
                    if (m_lstHoldPaymentFileNames.Count > 0) AppendFile("PrintCheck_", m_lstHoldPaymentFileNames.ToArray());
                    string sZipFileName = Conversion.ToDbDateTime(DateTime.Now) + ".zip";
                    CreateZip(sZipFileName);
                    FileToDatabase(Path.Combine(sPath, "Check_" + m_JobId + "_" + sZipFileName), "Check_" + m_JobId + "_" + sZipFileName);
                }
                else
                {
                    Console.WriteLine("0 ^*^*^ Command line argument are missing.");
                }
                Console.WriteLine("0 ^*^*^ Job completed successfully.");

            }
            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                bool rethrow = ExceptionPolicy.HandleException(exc, "Logging Policy");
            }
            finally
            {
                oUserLogin = null;
                m_lstCheckFileNames = null;
                m_lstPostCheckFileNames = null;
                m_lstPreCheckFileNames = null;
            }
        }

        private static void PrintBatch(UserLogin objUserLogin, string sConfig)
        {
            //string MessageTemplate = "<Message><Document><PrintChecks><CheckId /><PrintMode></PrintMode><TransNo></TransNo><RollUpId></RollUpId><BatchNumber></BatchNumber><AccountId></AccountId><IncludeAutoPayment></IncludeAutoPayment><IncludeCombinedPayment></IncludeCombinedPayment><UsingSelection>false</UsingSelection><CheckStockId></CheckStockId><FirstCheckNum></FirstCheckNum><OrderBy></OrderBy><CheckDate></CheckDate><NumAutoChecksToPrint></NumAutoChecksToPrint><IncludeClaimantInPayee /></PrintChecks></Document></Message>";
            //string PreCheckmsgTemplate = "<Message><Document><PrintChecks><FromDateFlag>false</FromDateFlag><ToDateFlag>True</ToDateFlag><AttachedClaimOnly></AttachedClaimOnly><IncludeAutoPayment>True</IncludeAutoPayment><IncludeCombinedPayment>False</IncludeCombinedPayment><FromDate></FromDate><ToDate></ToDate><AccountId></AccountId><OrghierarchyPre></OrghierarchyPre><OrghierarchyLevelPre>1012</OrghierarchyLevelPre><UsingSelection>false</UsingSelection><TransIds></TransIds><AutoTransIds></AutoTransIds><OrderBy></OrderBy></PrintChecks></Document></Message>";
            //string PostCheckmsgTemplate = "<Message><Document><PrintChecks><AccountId></AccountId><BatchNumber></BatchNumber><PostcheckDate></PostcheckDate><OrderBy></OrderBy></PrintChecks></Document></Message>";
            //string AccountChangedXml = "<Message><Document><PrintChecks><FromDateFlag>false</FromDateFlag><ToDateFlag>True</ToDateFlag><AttachedClaimOnly></AttachedClaimOnly><IncludeAutoPayment>True</IncludeAutoPayment><IncludeCombinedPayment>False</IncludeCombinedPayment><FromDate></FromDate><ToDate></ToDate><AccountId>40</AccountId><Orghierarchy></Orghierarchy><OrghierarchyLevel></OrghierarchyLevel></PrintChecks></Document></Message>";
            //JIRA:4042 START: ajohari2
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
			// The changes in Print Check will cause the same changes in exe as well
            //string MessageTemplate = "<Message><Document><PrintChecks><CheckId /><PrintMode></PrintMode><TransNo></TransNo><RollUpId></RollUpId><BatchNumber></BatchNumber><AccountId></AccountId><IncludeAutoPayment></IncludeAutoPayment><IncludeCombinedPayment></IncludeCombinedPayment><PrintEFTPayment></PrintEFTPayment><UsingSelection>false</UsingSelection><CheckStockId></CheckStockId><FirstCheckNum></FirstCheckNum><OrderBy></OrderBy><CheckDate></CheckDate><NumAutoChecksToPrint></NumAutoChecksToPrint><IncludeClaimantInPayee /></PrintChecks></Document></Message>";
            //string PreCheckmsgTemplate = "<Message><Document><PrintChecks><FromDateFlag>false</FromDateFlag><ToDateFlag>True</ToDateFlag><AttachedClaimOnly></AttachedClaimOnly><IncludeAutoPayment>True</IncludeAutoPayment><IncludeCombinedPayment>False</IncludeCombinedPayment><PrintEFTPayment>False</PrintEFTPayment><FromDate></FromDate><ToDate></ToDate><AccountId></AccountId><OrghierarchyPre></OrghierarchyPre><OrghierarchyLevelPre>1012</OrghierarchyLevelPre><UsingSelection>false</UsingSelection><TransIds></TransIds><AutoTransIds></AutoTransIds><OrderBy></OrderBy></PrintChecks></Document></Message>";
			//string PostCheckmsgTemplate = "<Message><Document><PrintChecks><AccountId></AccountId><BatchNumber></BatchNumber><PostcheckDate></PostcheckDate><OrderBy></OrderBy></PrintChecks></Document></Message>";
            //string AccountChangedXml = "<Message><Document><PrintChecks><FromDateFlag>false</FromDateFlag><ToDateFlag>True</ToDateFlag><AttachedClaimOnly></AttachedClaimOnly><IncludeAutoPayment>True</IncludeAutoPayment><IncludeCombinedPayment>False</IncludeCombinedPayment><PrintEFTPayment>False</PrintEFTPayment><FromDate></FromDate><ToDate></ToDate><AccountId>40</AccountId><Orghierarchy></Orghierarchy><OrghierarchyLevel></OrghierarchyLevel></PrintChecks></Document></Message>";
			
            string MessageTemplate = "<Message><Document><PrintChecks><CheckId /><PrintMode></PrintMode><TransNo></TransNo><RollUpId></RollUpId><BatchNumber></BatchNumber><AccountId></AccountId><IncludeAutoPayment></IncludeAutoPayment><IncludeCombinedPayment></IncludeCombinedPayment><DistributionType></DistributionType><UsingSelection>false</UsingSelection><CheckStockId></CheckStockId><FirstCheckNum></FirstCheckNum><OrderBy></OrderBy><CheckDate></CheckDate><NumAutoChecksToPrint></NumAutoChecksToPrint><IncludeClaimantInPayee /></PrintChecks></Document></Message>";
            string PreCheckmsgTemplate = "<Message><Document><PrintChecks><FromDateFlag>false</FromDateFlag><ToDateFlag>True</ToDateFlag><AttachedClaimOnly></AttachedClaimOnly><IncludeAutoPayment>True</IncludeAutoPayment><IncludeCombinedPayment>False</IncludeCombinedPayment><DistributionType></DistributionType><FromDate></FromDate><ToDate></ToDate><AccountId></AccountId><OrghierarchyPre></OrghierarchyPre><OrghierarchyLevelPre>1012</OrghierarchyLevelPre><UsingSelection>false</UsingSelection><TransIds></TransIds><AutoTransIds></AutoTransIds><OrderBy></OrderBy></PrintChecks></Document></Message>";
            string PostCheckmsgTemplate = "<Message><Document><PrintChecks><AccountId></AccountId><BatchNumber></BatchNumber><PostcheckDate></PostcheckDate><OrderBy></OrderBy><DistributionType></DistributionType></PrintChecks></Document></Message>";
            string AccountChangedXml = "<Message><Document><PrintChecks><FromDateFlag>false</FromDateFlag><ToDateFlag>True</ToDateFlag><AttachedClaimOnly></AttachedClaimOnly><IncludeAutoPayment>True</IncludeAutoPayment><IncludeCombinedPayment>False</IncludeCombinedPayment><DistributionType></DistributionType><FromDate></FromDate><ToDate></ToDate><AccountId>40</AccountId><Orghierarchy></Orghierarchy><OrghierarchyLevel></OrghierarchyLevel></PrintChecks></Document></Message>";
			// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            //JIRA:4042 End: 
            bool bResult = false;
            string sPreFileName = string.Empty;
            string sFilePath = string.Empty;
            XmlDocument objXmlIn = null;
            XmlDocument objXmlOut = null;
            XmlDocument objXmlMaster = null;
            BusinessAdaptorErrors objErr = null;
            PrintChecksAdaptor adp = null;
            XmlNode oTempNode = null;
            string sAccountId = string.Empty;
            string sStockId = string.Empty;
            string sOrgId = string.Empty;
            string sOrder = string.Empty;
            string sCombinedFlag = string.Empty;
            string sAutoFlag = string.Empty;
            string sRptType = string.Empty;
			// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //string sPrintEFTPayment = string.Empty;//JIRA:4042 : ajohari2
            string sDistributionType = string.Empty;
			// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            bool bInSuccess = false;
            try
            {
                objErr = new BusinessAdaptorErrors(objUserLogin, iClientId);//dvatsa-cloud
                adp = new PrintChecksAdaptor();
                adp.SetSecurityInfo(objUserLogin, iClientId);//dvatsa-cloud
                objXmlIn = new XmlDocument();
                objXmlOut = new XmlDocument();
                string[] arrConfig = null;
                arrConfig = sConfig.Split('-');
                sAccountId = arrConfig[0];
                sStockId = arrConfig[1];
                sOrgId = arrConfig[2];
                if (sOrgId == "0") sOrgId = "";
                sOrder = arrConfig[3].Replace('_', ' ');
                sCombinedFlag = arrConfig[4];
                sAutoFlag = arrConfig[5];
                sRptType = arrConfig[6];
                //JIRA:4042 START: ajohari2   -- merged again as overidden by a previous change Jira 7530
                //if (arrConfig.Length == 8)
                //{
                   // sPrintEFTPayment = arrConfig[7];
                //}
                //JIRA:4042 End: 
				// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
				if (arrConfig.Length == 8)
				{
	                sDistributionType = arrConfig[7];
				}
				// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                objXmlMaster = new XmlDocument();
                objXmlIn.LoadXml(AccountChangedXml);
                //set values from Input
                objXmlIn.SelectSingleNode("//ToDate").InnerText = DateTime.Now.ToString("d");
                objXmlIn.SelectSingleNode("//AccountId").InnerText = sAccountId;
                objXmlIn.SelectSingleNode("//IncludeCombinedPayment").InnerText = sCombinedFlag;
                objXmlIn.SelectSingleNode("//IncludeAutoPayment").InnerText = sAutoFlag;
                objXmlIn.SelectSingleNode("//Orghierarchy").InnerText = sOrgId;
				// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:4042 START: ajohari2
                //objXmlIn.SelectSingleNode("//PrintEFTPayment").InnerText = sPrintEFTPayment;
                //JIRA:4042 End: 
                objXmlIn.SelectSingleNode("//DistributionType").InnerText = sDistributionType;
				// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment

                Console.WriteLine("0 ^*^*^ Fetching Account id: " + sAccountId + " details");
                bResult = adp.AccountChanged(objXmlIn, ref objXmlMaster, ref objErr);
                if (Conversion.CastToType<int>(objXmlMaster.SelectSingleNode("//CheckSetChanged/NumberOfPrechecks").InnerText, out bInSuccess) > 0)
                {
                    if (bResult)
                    {
                        //preCheck Register
                        objXmlIn.LoadXml(PreCheckmsgTemplate);
                        //set the values from input
                        objXmlIn.SelectSingleNode("//ToDate").InnerText = DateTime.Now.ToString("d");
                        objXmlIn.SelectSingleNode("//AccountId").InnerText = sAccountId;
                        objXmlIn.SelectSingleNode("//IncludeCombinedPayment").InnerText = sCombinedFlag;
                        objXmlIn.SelectSingleNode("//IncludeAutoPayment").InnerText = sAutoFlag;
                        objXmlIn.SelectSingleNode("//OrghierarchyPre").InnerText = sOrgId;
                        objXmlIn.SelectSingleNode("//OrderBy").InnerText = sOrder;
						// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                        //JIRA:4042 START: ajohari2
                        //objXmlIn.SelectSingleNode("//PrintEFTPayment").InnerText = sPrintEFTPayment;
                        //JIRA:4042 End:
                        objXmlIn.SelectSingleNode("//DistributionType").InnerText = sDistributionType;
						// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                        bResult = adp.PreCheckDetail(objXmlIn, ref objXmlOut, ref objErr);
                        if (objErr.Count > 0)
                        {
                            //throw objErr[0].oException;
                            Console.WriteLine("0 ^*^*^ Error occurred in fetching PreCheckDetails: " + objErr[0].oException.Message + " " + objErr[0].oException.InnerException);
                        }
                        if (bResult)
                        {
                            sPreFileName = objXmlOut.SelectSingleNode("//File").Attributes["FileName"].Value;
                            m_lstPreCheckFileNames.Add(sPreFileName);
                            objXmlOut = null;
                            bResult = false;
                            oTempNode = objXmlIn.CreateElement("OrgHierarchyLevel");
                            oTempNode.InnerText = objXmlIn.SelectSingleNode("//OrghierarchyLevelPre").InnerText;
                            objXmlIn.SelectSingleNode("//PrintChecks").AppendChild(oTempNode);
                            //Save precheck register
                            bResult = adp.SavePreCheckDetail(objXmlIn, ref objXmlOut, ref objErr);
                            if (objErr.Count > 0)
                            {
                                //throw objErr[0].oException;
                                Console.WriteLine("0 ^*^*^ Error occurred in saving PreCheckDetails: " + objErr[0].oException.Message + " " + objErr[0].oException.InnerException);
                            }

                            //Print Checks
                            if (bResult)
                            {
                                objXmlIn.LoadXml(MessageTemplate);
                                objXmlIn.SelectSingleNode("//RollUpId").InnerText = "0";
                                // akaushik5 Changed for MITS 38045 Starts
                                //objXmlIn.SelectSingleNode("//BatchNumber").InnerText = ((Conversion.CastToType<Int32>(objXmlMaster.SelectSingleNode("//BatchNumber").InnerText, out bInSuccess)) + 1).ToString();
                                objXmlIn.SelectSingleNode("//BatchNumber").InnerText = objXmlOut.SelectSingleNode("//CheckBatch").InnerText;
                                // akaushik5 Changed for MITS 38045 Ends
                                objXmlIn.SelectSingleNode("//AccountId").InnerText = sAccountId;
                                objXmlIn.SelectSingleNode("//FirstCheckNum").InnerText = objXmlMaster.SelectSingleNode("//FirstCheckNum").InnerText;
                                objXmlIn.SelectSingleNode("//IncludeAutoPayment").InnerText = objXmlMaster.SelectSingleNode("//IncludeAutoPaymentsFlag").InnerText;
                                objXmlIn.SelectSingleNode("//IncludeCombinedPayment").InnerText = objXmlMaster.SelectSingleNode("//IncludeCombinedPaymentsFlag").InnerText;
                                objXmlIn.SelectSingleNode("//CheckDate").InnerText = DateTime.Now.ToString("d");
                                objXmlIn.SelectSingleNode("//OrderBy").InnerText = sOrder;
                                objXmlIn.SelectSingleNode("//CheckStockId").InnerText = sStockId;
								// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                                //JIRA:4042 START: ajohari2
                                //objXmlIn.SelectSingleNode("//PrintEFTPayment").InnerText = sPrintEFTPayment;
                                //JIRA:4042 End: 
                                objXmlIn.SelectSingleNode("//DistributionType").InnerText = sDistributionType;
								// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                                //Add below node to verify call is coming from task manager in adaptor 
                                oTempNode = null;
                                oTempNode = objXmlIn.CreateElement("FromTaskManager");
                                oTempNode.InnerText = "1";
                                objXmlIn.SelectSingleNode("//PrintChecks").AppendChild(oTempNode);

                                bResult = adp.PrintChecksBatch(objXmlIn, ref objXmlOut, ref objErr);
                                if (objErr.Count > 0)
                                {
                                    //throw objErr[0].oException;
                                    Console.WriteLine("0 ^*^*^ Error occurred in fetching PrintCheckBatch: " + objErr[0].oException.Message + " " + objErr[0].oException.InnerException);
                                }
                                if (bResult)
                                {
                                    SavePrintCheckFiles(objXmlOut);

                                    oTempNode = null;
                                    oTempNode = objXmlOut.CreateElement("BatchNumber");
                                    oTempNode.InnerText = objXmlOut.SelectSingleNode("//BatchId").InnerText;
                                    objXmlOut.SelectSingleNode("//PrintCheckDetails").AppendChild(oTempNode);
                                    if (objXmlOut.SelectSingleNode("//FilesNameAndType") != null)
                                        objXmlOut.RemoveChild(objXmlOut.SelectSingleNode("//FilesNameAndType"));
                                    // npadhy JIRA 6418 Starts UpdateStatusForPrintedChecks requires Distribution Type. So Create a node with corresponding value
                                    oTempNode = objXmlOut.CreateElement("DistributionType");
                                    oTempNode.InnerText = sDistributionType;
                                    objXmlOut.SelectSingleNode("//PrintCheckDetails").AppendChild(oTempNode);
                                    // npadhy JIRA 6418 Ends
                                    bResult = adp.UpdateStatusForPrintedChecks(objXmlOut, ref objXmlOut, ref objErr);
                                    if (objErr.Count > 0)
                                    {
                                        //throw objErr[0].oException;
                                        Console.WriteLine("0 ^*^*^ Error occurred in updating status for printed checks: " + objErr[0].oException.Message + " " + objErr[0].oException.InnerException);
                                    }

                                    //PostCheck Register
                                    if (bResult)
                                    {
                                        objXmlIn.LoadXml(PostCheckmsgTemplate);
                                        // akaushik5 Changed for MITS 38045 Starts
                                        //objXmlIn.SelectSingleNode("//BatchNumber").InnerText = ((Conversion.CastToType<Int32>(objXmlMaster.SelectSingleNode("//BatchNumber").InnerText, out bInSuccess)) + 1).ToString();
                                        objXmlIn.SelectSingleNode("//BatchNumber").InnerText = objXmlOut.SelectSingleNode("//CheckBatch").InnerText;
                                        // akaushik5 Changed for MITS 38045 Ends
                                        objXmlIn.SelectSingleNode("//AccountId").InnerText = sAccountId;
                                        objXmlIn.SelectSingleNode("//PostcheckDate").InnerText = DateTime.Now.ToString("d");
                                        // npadhy JIRA 6418 - Starts Copying the value of the Distribution Type and passing it along
                                        objXmlIn.SelectSingleNode("//DistributionType").InnerText = sDistributionType;
                                        // npadhy JIRA 6418 - Ends Copying the value of the Distribution Type and passing it along
                                        bResult = false;
                                        objXmlOut = null;

                                        if (sRptType == "detail")
                                            bResult = adp.PostCheckDetail(objXmlIn, ref objXmlOut, ref objErr);
                                        else if (sRptType == "summary")
                                            bResult = adp.PostCheckSummary(objXmlIn, ref objXmlOut, ref objErr);
                                        else
                                            bResult = adp.PostCheckSubAccount(objXmlIn, ref objXmlOut, ref objErr);

                                        if (objErr.Count > 0)
                                        {
                                            //throw objErr[0].oException;
                                            Console.WriteLine("0 ^*^*^ Error occurred in fetching Post Check Report: " + objErr[0].oException.Message + " " + objErr[0].oException.InnerException);
                                        }
                                        if (bResult)
                                        {
                                            sPreFileName = objXmlOut.SelectSingleNode("//File").Attributes["FileName"].Value;
                                            m_lstPostCheckFileNames.Add(sPreFileName);
                                            Console.WriteLine("0 ^*^*^ Print Check completed for Account: " + sAccountId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("0 ^*^*^ No check exists for selected criteria.");
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("0 ^*^*^ Error occurred in check printing");
                throw exc;
            }
            finally
            {
                objXmlIn = null;
                objXmlOut = null;
                objXmlMaster = null;
                objErr = null;
                adp = null;
                oTempNode = null;
            }

        }

        private static void AppendFile(string sPrintStep, string[] arrFileNames)
        {
            IacDocument objSourceDoc = null;
            IacDocument objDestDoc = null;
            string sFileName = string.Empty;
            bool bAppendFile = true;
            string sFinalPath = string.Empty;
            string sFileType = string.Empty;
            string sBasePath = string.Empty;
            FileStream fsDestFile = null;
            FileStream fsSourceFile = null;
            string sDestFilePath = string.Empty;
            string sSourceFilePath = string.Empty;
            string sCheckFileType = string.Empty;
            try
            {
                sBasePath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PrintChecks");
                sFinalPath = Path.Combine(sBasePath, "Check_" + m_JobId);
                if (!Directory.Exists(sFinalPath))
                {
                    Directory.CreateDirectory(sFinalPath);
                }
                acPDFCreatorLib.Initialize();
                acPDFCreatorLib.SetLicenseKey("CSC Financial Services Group", "07EFCDAB01000100C9D1AB2346F36D68B637892D59E2B2D416C18F0397811F5604016685DBC54D7928B23578A87E09BFEB3D9FC4FE9F");
                foreach (string sName in arrFileNames)
                {
                    bAppendFile = true;
                    sFileName = sName;
                    if (sPrintStep == "PrintCheck_")
                    {
                        sFileName = sName.Split('~')[0];
                        sCheckFileType = sName.Split('~')[1];
                    }
                    //JIRA:4042 START: ajohari2
                    else if (sPrintStep == "PrintCheckcsv_")
                    {
                        if (arrFileNames.Length > 1)
                        {
                            sCheckFileType = arrFileNames[1];
                        }
                    }
                    //JIRA:4042 End: 
                    sSourceFilePath = Path.Combine(sBasePath, sFileName);
                    sFileType = sFileName.Substring(sFileName.LastIndexOf('.') + 1, 3);
                    if(sFileType == "csv")
                    {
                        sDestFilePath = Path.Combine(sFinalPath, sPrintStep + sFileName);
                    }
                    else if(sFileType == "PDF")
                    {
                        sDestFilePath = Path.Combine(sFinalPath, sPrintStep + Conversion.ToDbDate(DateTime.Now) + ".pdf");
                    }
                    if (sCheckFileType == "eft")
                    {
                        sSourceFilePath = Path.Combine(RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PrintChecks\\EFT"), sFileName);
                        sDestFilePath = Path.Combine(sFinalPath, "PrintCheckeft_" +  Conversion.ToDbDate(DateTime.Now) + ".csv");
                    }
                    else if (sCheckFileType == "holdPayments")
                    {
                        sDestFilePath = Path.Combine(sFinalPath, "HoldPayments_" + Conversion.ToDbDate(DateTime.Now) + ".pdf");
                    }
                    else if ((sCheckFileType != "") && (sCheckFileType != "check") && (sCheckFileType != "holdPayments"))
                    {
                        sSourceFilePath = Path.Combine(RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "EOB"), sFileName);
                        sDestFilePath = Path.Combine(sFinalPath, "PrintCheckeob_" + Conversion.ToDbDate(DateTime.Now) + ".pdf");
                    }
                    if (!File.Exists(sDestFilePath))
                    {
                        File.Copy(sSourceFilePath, sDestFilePath);
                        bAppendFile = false;
                    }
                    if (bAppendFile && (sFileType == "PDF")) 
                    {
                        if (fsDestFile == null)
                        {
                            fsDestFile = new FileStream(sDestFilePath, FileMode.Open, FileAccess.ReadWrite);
                            objDestDoc = new IacDocument(null);
                            // akaushik5 Changed for MITS 38045 Starts
                            //objDestDoc.Open(fsDestFile, "");
                            objDestDoc.Open(fsDestFile, sPrintStep.Equals("PrintCheck_") && sCheckFileType.Equals("check") ? oUserLogin.Password : string.Empty);
                            // akaushik5 Changed for MITS 38045 Ends
                        }
                        fsSourceFile = new FileStream(sSourceFilePath, FileMode.Open, FileAccess.Read);
                        objSourceDoc = new IacDocument(null);
                        // akaushik5 Changed for MITS 38045 Starts
                        //objSourceDoc.Open(fsSourceFile, "");
                        objSourceDoc.Open(fsSourceFile, sPrintStep.Equals("PrintCheck_") && sCheckFileType.Equals("check") ? oUserLogin.Password : string.Empty);
                        // akaushik5 Changed for MITS 38045 Ends
                        bool bAppend = objDestDoc.Append(objSourceDoc);
                        fsSourceFile.Flush();
                        fsSourceFile.Close();
                        fsSourceFile.Dispose();
                        objSourceDoc.Dispose();
                        objSourceDoc = null;
                        if (!bAppend)
                        {
                            Console.WriteLine("0 ^*^*^ Not able to append the document. File Name: " + sSourceFilePath + " with " + sDestFilePath);
                            //throw new Exception("Not able to append the document. File Name: " + sSourceFilePath);
                        }
                    }
                    else if (bAppendFile && sFileType == "csv")
                    {
                        File.AppendAllText(sDestFilePath, File.ReadAllText(sSourceFilePath));
                    }
                }
                if (objDestDoc != null)
                {
                    bool bSave = objDestDoc.Save(fsDestFile);
                    objDestDoc.Dispose();
                    objDestDoc = null;
                    fsDestFile.Close();
                    fsDestFile.Dispose();
                    if (!bSave)
                    {
                        Console.WriteLine("0 ^*^*^ Not able to save destination document after append. File Name: " + sDestFilePath);
                        //throw new Exception("Not able to save destination document after apeending files. File Name:" + sDestFilePath);
                    }
                    Console.WriteLine("0 ^*^*^ File appending completed successfully.");
                }
                acPDFCreatorLib.Terminate();
            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Error occured in appending print check files");
                throw e;
            }
            finally
            {
                if (objSourceDoc != null)
                {
                    objSourceDoc.Dispose();
                    objSourceDoc = null;
                }
                if (fsSourceFile != null)
                {
                    fsSourceFile.Dispose();
                }
                if (fsDestFile != null)
                {
                    fsDestFile.Dispose();
                }
                if (objDestDoc != null)
                {
                    objDestDoc.Dispose();
                    objDestDoc = null;
                }
                acPDFCreatorLib.Terminate();
            }

        }
        private static void SavePrintCheckFiles(XmlDocument p_objXmlIn)
        {
            string[] arrFileNameAndType = null;
            string[] arrSingleFileNameAndType = null;
            
           
            if(p_objXmlIn.SelectSingleNode("//PrintChecks").Attributes["FileNameAndType"] != null)
            {
                if (p_objXmlIn.SelectSingleNode("//PrintChecks").Attributes["FileNameAndType"].Value != "")
                {
                    arrFileNameAndType = p_objXmlIn.SelectSingleNode("//PrintChecks").Attributes["FileNameAndType"].Value.Split(",".ToCharArray()[0]);
                }
            }
            else if (p_objXmlIn.SelectSingleNode("//FilesNameAndType") != null)
            {
                if (p_objXmlIn.SelectSingleNode("//FilesNameAndType").InnerText != null && p_objXmlIn.SelectSingleNode("//FilesNameAndType").InnerText != "")
                {
                    arrFileNameAndType = p_objXmlIn.SelectSingleNode("//FilesNameAndType").InnerText.Split(",".ToCharArray()[0]);
                }
            }
            if (arrFileNameAndType != null)
            {
                foreach (string sTmpFileNameAndType in arrFileNameAndType)
                {
                    arrSingleFileNameAndType = sTmpFileNameAndType.Split("~".ToCharArray()[0]);
                    if (arrSingleFileNameAndType != null)
                    {
                        if ((arrSingleFileNameAndType[0].Substring(arrSingleFileNameAndType[0].LastIndexOf('.') + 1, 3) == "csv") && (arrSingleFileNameAndType[0] != "eft"))
                        {
                            AppendFile("PrintCheckcsv_", arrSingleFileNameAndType);
                        }
                        // akaushik5 Changed for MITS 38045 Starts
                        //else if(arrSingleFileNameAndType[0] == "check")
                        else if (arrSingleFileNameAndType[1].Equals("check"))
                        // akaushik5 Changed for MITS 38045 Ends
                        {
                            m_lstCheckFileNames.Add(sTmpFileNameAndType);
                        }
                        else if (arrSingleFileNameAndType[1].Equals("holdPayments"))
                        {
                            m_lstHoldPaymentFileNames.Add(sTmpFileNameAndType);
                        }
                        else
                        {
                            m_lstEOBCheckFileNames.Add(sTmpFileNameAndType);
                        }
                    }
                }
            }
        
        }

        /// <summary>
        /// Create the zipfile with name dictated by caller.  Zipfiles created in this way will not be added to the database.
        /// </summary>
        /// <param name="filename">Name (only) of the zipfile to create</param>
        public static void CreateZip(string filename)
        {
            string sPath = Path.Combine(RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PrintChecks"), "Check_" + m_JobId);
            C1ZipFile ZipFile = null;
            try
            {
                int fileCount = 0;
                if (Directory.Exists(sPath))
                {
                    Console.WriteLine("0 ^*^*^ Creating zip file for printed checks");
                    ZipFile = new C1ZipFile(sPath + "_" + filename);
                    foreach (string file in Directory.GetFiles(sPath))
                    {
                        if (!file.EndsWith(".zip"))
                        {
                            fileCount++;
                            ZipFile.Entries.Add(file);
                        }
                    }
                    ZipFile.Close();
                    Console.WriteLine("0 ^*^*^ Zip file created successfully for " + fileCount + " files.");
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine("0 ^*^*^ Error occured in creating zip file");
                throw e;
            }
            finally
            {
                if (ZipFile != null)
                {
                    ZipFile.Close();
                    ZipFile = null;
                }
            }
        }

        /// <summary>
        /// Add a file to the task manager database TM_JOBS_DOCUMENT table.
        /// </summary>
        /// <param name="filePath">The full path of the file to insert into the DB.</param>
        public static void FileToDatabase(string filePath, string fileName)
        {
            FileStream stream = null;
            byte[] fileData = null;
            if (File.Exists(filePath))
            {
                try
                {
                    stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    fileData = new byte[stream.Length];
                    stream.Read(fileData, 0, (int)stream.Length);
                    stream.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("0 ^*^*^ Error occured in reading zip file.");
                    throw e;
                }
                finally
                {
                    stream.Close();
                    stream.Dispose();
                }
                try
                {
                    Console.WriteLine("0 ^*^*^ Attach file to database.");
                    using (DbConnection dbConnection = DbFactory.GetDbConnection(RMConfigurationManager.GetConnectionString("TaskManagerDataSource", iClientId)))//dvatsa-cloud
                    {
                        dbConnection.Open();
                        DbCommand cmdInsert = dbConnection.CreateCommand();
                        DbParameter paramFileId = cmdInsert.CreateParameter();
                        DbParameter paramJobId = cmdInsert.CreateParameter();
                        DbParameter paramFileName = cmdInsert.CreateParameter();
                        DbParameter paramFileData = cmdInsert.CreateParameter();
                        DbParameter paramContentType = cmdInsert.CreateParameter();
                        DbParameter paramLength = cmdInsert.CreateParameter();

                        cmdInsert.CommandText = "INSERT INTO TM_JOBS_DOCUMENT " +
                            "(TM_FILE_ID, JOB_ID, FILE_NAME, FILE_DATA, CONTENT_TYPE, CONTENT_LENGTH) VALUES " +
                            "(~FileId~, ~JobId~, ~FileName~, ~FileData~, ~ContentType~, ~ContentLength~)";

                        paramFileId.Direction = ParameterDirection.Input;
                        paramFileId.Value = int.Parse(m_JobId);
                        //paramFileId.Value = JobId;
                        paramFileId.ParameterName = "FileId";
                        paramFileId.SourceColumn = "TM_FILE_ID";
                        cmdInsert.Parameters.Add(paramFileId);

                        paramJobId.Direction = ParameterDirection.Input;
                        paramJobId.Value = int.Parse(m_JobId);
                        //paramJobId.Value = JobId;
                        paramJobId.ParameterName = "JobId";
                        paramJobId.SourceColumn = "JOB_ID";
                        cmdInsert.Parameters.Add(paramJobId);

                        paramFileName.Direction = ParameterDirection.Input;
                        paramFileName.Value = fileName;
                        paramFileName.ParameterName = "FileName";
                        paramFileName.SourceColumn = "FILE_NAME";
                        cmdInsert.Parameters.Add(paramFileName);

                        paramFileData.Direction = ParameterDirection.Input;
                        paramFileData.Value = fileData;
                        paramFileData.ParameterName = "FileData";
                        paramFileData.SourceColumn = "FILE_DATA";
                        cmdInsert.Parameters.Add(paramFileData);

                        paramContentType.Direction = ParameterDirection.Input;
                        paramContentType.Value = "";
                        paramContentType.ParameterName = "ContentType";
                        paramContentType.SourceColumn = "CONTENT_TYPE";
                        cmdInsert.Parameters.Add(paramContentType);

                        paramLength.Direction = ParameterDirection.Input;
                        paramLength.Value = fileData.Length;
                        paramLength.ParameterName = "ContentLength";
                        paramLength.SourceColumn = "CONTENT_LENGTH";
                        cmdInsert.Parameters.Add(paramLength);

                        cmdInsert.ExecuteNonQuery();

                    }
                    Console.WriteLine("0 ^*^*^ File attached to database successfully.");
                }
                catch (System.Exception e)
                {
                    Console.WriteLine("0 ^*^*^ Error occured in saving zip file to database.");
                    throw e;
                }

                finally
                {
                    if (fileData != null)
                    {
                        fileData = null;
                    }
                }
            }
        }
    }
}
