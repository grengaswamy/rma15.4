DevNotes

BSB 10/29/2007

It is possible to establish a "trusted session" with the InfoView component of Business Objects XIr2.

The basic steps currently are:
 1.) Deploy  Files to 
BusinessObjects Enterprise 11.5\Web Content\Enterprise115\InfoView\

DTGCrypt.dll
RMBIS.dll
rmxTransfer.aspx
rmxLogin.aspx

2.) Register the two dll files using regsvr32

3.) Go into Central Management Console (CMC)
	Under Authentication\Enterprise find the option at the bottom called "Trusted Authentication". 
	Place a checkmark in the box.
	Enter a password for the "Shared Secret" text box. (Remember this value you'll need it later.)
	Leave the time-out at zero.
	Press Update
	Log out of CMC
4.) Go to the following folder on your web server:
	BusinessObjects Enterprise 11.5\win32_x86\plugins\auth\secEnterprise

5.) Create a text file called "TrustedPrincipal.conf"
	Add only the following line to this file and save:
	SecretPassword=<mySecretPassword from Step 3>

(You may need to restart the services using the CMS tools)

6.) In RMX, go to the Riskmaster.config file.
	Find and Modify the BISURL tag url to point to the new rmxTransfer.aspx
	Example: http://170.30.224.151/businessobjects/Enterprise115/InfoView/rmxTransfer.aspx
	
7.) Either of the following must be true on the client browser for cookies to be exchanged successfully enabling this scenario:
	a.) RMX and BIS hosts are added to the IE trusted zone. (Preferred)
	OR
	b.) Privacy settings are "low" allowing third party cookies to be set/read. 
