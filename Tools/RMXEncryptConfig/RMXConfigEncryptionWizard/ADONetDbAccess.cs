﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Collections.Generic;

using Riskmaster.Db;

namespace RMXdBUpgradeWizard
{
    class ADONetDbAccess
    {
        private static int m_intDatabaseMake;

        public int DatabaseMake
        {
            get
            {
                return m_intDatabaseMake;
            }
            set
            {
                m_intDatabaseMake = value;
            }
        }

        /// <summary>
        /// Execute SQL against a database and return a DataReader
        /// </summary>
        /// <param name="strConnString">connection string</param>
        /// <param name="strSQL">SQL query</param>
        /// <returns>DataReader</returns>
        public static DbReader ExecuteReader(string strConnString, string strSQL)
        {
            var idr = DbFactory.ExecuteReader(strConnString, strSQL);
            //var idr = DbFactory.GetDbReader(strConnString, strSQL, CommandBehavior.CloseConnection);
            return idr;
        }

        /// <summary>
        /// Executes SQL against the database and returns a scalar string value
        /// </summary>
        /// <param name="strConnString">connection string</param>
        /// <param name="strSQL">SQL query</param>
        /// <returns>an int containing the return value</returns>
        public static int ExecuteScalar(string strConnString, string strSQL)
        {
            return Convert.ToInt32(DbFactory.ExecuteScalar(strConnString, strSQL));
        }

        /// <summary>
        /// Executes SQL against the database and returns a scalar string value
        /// </summary>
        /// <param name="strConnString">connection string</param>
        /// <param name="strSQL">SQL query</param>
        /// <returns>a string containing the return value</returns>
        public static string ExecuteString(string strConnString, string strSQL)
        {
            string sRetValue = "0";
            object obj = DbFactory.ExecuteScalar(strConnString, strSQL);
            if(obj != null)
                sRetValue = obj.ToString();

            return sRetValue;
        }

        /// <summary>
        /// Executes SQL against the database and does not return a value
        /// </summary>
        /// <param name="strConnString">connection string</param>
        /// <param name="strSQL">SQL query</param>
        /// <returns>returns an integer representing pass / fail</returns>
        public static int ExecuteNonQuery(string strConnString, string strSQL)
        {
            return DbFactory.ExecuteNonQuery(strConnString, strSQL, new Dictionary<string, string>());
        }

        /// <summary>
        /// Executes a stored procedure against the database and does not return a value
        /// </summary>
        /// <param name="strConnString">connection string</param>
        /// <param name="strStoredProcedure">name of the stored procedure</param>
        /// <returns>returns an integer representing pass / fail</returns>
        public static int ExecuteStoredProcedure(string strConnString, string strStoredProcedure)
        {
            return DbFactory.ExecuteNonQuery(strConnString, strStoredProcedure, new Dictionary<string, string>());
        }

        /// <summary>
        /// Executes SQL against the database that does not return a value
        /// </summary>
        /// <param name="strConnString">connection string</param>
        /// <param name="strSQL">SQL query</param>
        /// <param name="nvColl">parameter collection</param>
        /// <returns></returns>
        public static int ExecuteNonQuery(string strConnString, string strSQL, NameValueCollection nvColl)
        {
            var dbConn = DbFactory.GetDbConnection(strConnString);
            dbConn.Open();

            var dbCmd = dbConn.CreateCommand();

            dbCmd.CommandText = strSQL;
            dbCmd.CommandType = CommandType.Text;

            for (var i = 0; i < nvColl.Count; i++)
            {
                var dbParam = dbCmd.CreateParameter();

                dbParam.ParameterName = nvColl.GetKey(i);
                dbParam.Value = nvColl.Get(i);

                dbCmd.Parameters.Add(dbParam);
            }

            //execute the SQL against the database
            var intRecordsAffected = dbCmd.ExecuteNonQuery();

            //clean up
            dbConn.Close();
            dbConn.Dispose();

            return intRecordsAffected;
        }

        /// <summary>
        /// Returns the database type after connecting to the database
        /// </summary>
        /// <param name="strConnString">connection string</param>
        /// <returns>returns an integer representing the DatabaseType</returns>
        public static int DbType(string strConnString)
        {
            // DBMS_IS_ACCESS   = 0,
            // DBMS_IS_SQLSRVR  = 1,
            // DBMS_IS_SYBASE   = 2,
            // DBMS_IS_INFORMIX = 3,
            // DBMS_IS_ORACLE   = 4,
            // DBMS_IS_ODBC     = 5,
            // DBMS_IS_DB2      = 6,

            var dbConn = DbFactory.GetDbConnection(strConnString);

            dbConn.Open();

            var DB_MAKE = dbConn.DatabaseType;

            dbConn.Close();
            dbConn.Dispose();

            return Convert.ToInt32(DB_MAKE);
        }

        /// <summary>
        /// Executes SQL against the database and returns a scalar DataSet object
        /// </summary>
        /// <param name="strConnString">connection string</param>
        /// <param name="strSQL">SQL query</param>
        /// <returns>DataSet</returns>
        public static DataSet GetDataSet(string strConnString, string strSQL)
        {
            return DbFactory.GetDataSet(strConnString, strSQL);
        }
    }
}
