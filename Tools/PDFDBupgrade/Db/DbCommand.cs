using System;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.Db
{
	
     /// <summary>Riskmaster.Db.DbCommand wraps the Native .Net provider command objects  for ODBC, SQL Server, Oracle and DB2.</summary>
	public class DbCommand : IDbCommand
	{
		private bool m_ExclusiveConnection; //=false;
		//aanandpraka2:Change MITS 22728
        //IDbCommand m_anyCommand;  //The star of the show...
        public IDbCommand m_anyCommand;
        //aanandpraka2:end changes

		//Hold on to wrapped object instances where possible?
		DbConnection  m_connection;
		DbTransaction  m_txn;
		DbParameterCollection m_parameters;

		internal DbCommand(IDbCommand anyCommand)
		{
			m_anyCommand = anyCommand;
			m_anyCommand.CommandTimeout =0; //BSB Fix - Force all queries to infinite timeout.
			m_parameters= new DbParameterCollection(m_anyCommand.Parameters);
		}

		//Ctor - Hold on to internal interface on strongly typed provider command object.
		
		/// <summary>
		/// Used internally only by the DbFactory to produce new instances of this class.
		/// </summary>
		/// <param name="anyCommand">Is the IDbCommand interface of the native provider Command to wrap inside of this object.</param>
        /// <param name="exclusiveConnection">indicates whether or not the connection should be exclusive</param>
		internal DbCommand(IDbCommand anyCommand,bool exclusiveConnection)
		{
			m_ExclusiveConnection = exclusiveConnection;

			m_anyCommand = anyCommand;
			m_anyCommand.CommandTimeout =0; //BSB Fix - Force all queries to infinite timeout.
			m_parameters= new DbParameterCollection(m_anyCommand.Parameters);
		}

		/****
		 * IMPLEMENT THE REQUIRED PROPERTIES.
		 ****/
		
		/// <summary>Riskmaster.Db.DbCommand.CommandText provides the place to specify the SQL to execute.  
		/// Within this property set we are changing out our proprietary SQL parameter markers (Example: "CLAIM_ID=~myvalue~") 
		/// and replacing them with provider specific markers.  (Example in SQL Server: "CLAIM_ID=@myvalue")</summary>
		/// <value>The command string to be executed.</value>
		/// <remarks>The parameter marker replacement happens on the Set of this property and any "Get" against this 
		/// property returns provider specific SQL. Therefore, this property is no longer "reflexive"... Will that be a problem?</remarks>
		/// <example>No example available.</example>
		public  string CommandText  
		{
			get { return m_anyCommand.CommandText;  }
			set  { m_anyCommand.CommandText = ApplyProviderSpecificParamTokens(value);  }
		}

		
		/// <summary>Wraps this property from the Native provider.  See ADO.Net documentation for details.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>none</remarks>
		/// <example>No example available.</example>
		public  int CommandTimeout
		{
			get  { return m_anyCommand.CommandTimeout; }
			set  { m_anyCommand.CommandTimeout = value; }
		}

		
		/// <summary>Wraps this property from the Native provider.  See ADO.Net documentation for details.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>none</remarks>
		/// <example>No example available.</example>
		public  CommandType CommandType
		{
			get { return m_anyCommand.CommandType; }
			set { m_anyCommand.CommandType = value; }
		}

		/// <summary>Wraps this property from the Native provider.  Present only to satisfy the interface mapping requirements.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>It is valid for this guy to throw an exception if the value being assigned is not a DbConnection.</remarks>
		/// <example>No example available.</example>
		IDbConnection IDbCommand.Connection
		{
			get{return this.Connection;}
			set
			{
				DbConnection cn = value as DbConnection;
				if(cn==null) 
					//throw new RMAppException(String.Format(Riskmaster.Common.Globalization.GetString("IDbCommand.Connection.InvalidTypeMessage"),value.GetType().FullName));
                    throw new RMAppException(String.Format("IDbCommand.Connection.InvalidTypeMessage", value.GetType().FullName));
				//throw new RMAppException(String.Format("DbCommand recieved a {0} type connection where a DbConnection type is required.",value.GetType().FullName));
				this.Connection = cn;
			}
		}
		
		/// <summary>Wraps this property from the Native provider.  
		/// The command will attempt to use this connection when it is executed.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>It is valid for this guy to throw an exception if the value being assigned is not a DbConnection.</remarks>
		/// <example>No example available.</example>
		public  DbConnection Connection
		{
			get { return m_connection;}
			set
			{	//Set both an internal book keeping ref to the DbConnection obj
				// AND 
				// Set the full Native Provider connection property of the Command.
				m_connection = value as DbConnection;
				if (m_connection == null)
					throw new System.Exception(String.Format("DbCommand recieved a {0} type connection where a DbConnection type is required.",value.GetType().FullName));
				m_anyCommand.Connection =  m_connection.AsNative();
			}
		}

		
		/// <summary>Wraps this property from the Native provider.  See ADO.Net documentation for details.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>Read Only</remarks>
		/// <example>No example available.</example>
		public  DbParameterCollection Parameters
		{
			get  { return m_parameters; }
		}

		
		/// <summary>Wraps this property from the Native provider.  
		/// Present only to satisfy the interface mapping requirements.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>Read Only</remarks>
		/// <example>No example available.</example>
		IDataParameterCollection IDbCommand.Parameters
		{
			get  { return this.Parameters; }
		}

		/*
		* Set the transaction. Consider additional steps to ensure that the transaction
		* is compatible with the connection, because the two are usually linked.
		*/
		
		/// <summary>Wraps this property from the Native provider.  Present only to satisfy the interface mapping requirements.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>It is valid for this guy to throw an exception if the value being assigned is not a DbTransaction.</remarks>
		/// <example>No example available.</example>
		IDbTransaction IDbCommand.Transaction
		{
			get{return this.Transaction;}
			set
			{
				DbTransaction txn = value as DbTransaction;
				if(txn == null)
					throw new System.Exception(String.Format("DbCommand recieved a {0} type transaction  where a DbTransaction  type is required.",value.GetType().FullName));
				this.Transaction = txn;
			}
		}

		
		/// <summary>Wraps this property from the Native provider.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>Get\Set the transaction. Consider additional steps to ensure that the transaction 
		///  is compatible with the connection, because the two are usually linked.</remarks>
		/// <example>No example available.</example>
		public  DbTransaction Transaction
		{
			get { return m_txn; }
			set
			{ 
				//Set both an internal book keeping ref to the DbTransaction obj
				// AND 
				// Set the full Native Provider transaction  property of the Command.
				m_txn = (DbTransaction)value; 
				m_anyCommand.Transaction = value.AsNative();
			}
		}

		
		/// <summary>Wraps this property from the Native provider.  See ADO.Net documentation for details.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>Read\Write</remarks>
		/// <example>No example available.</example>
		public  UpdateRowSource UpdatedRowSource
		{
			get { return m_anyCommand.UpdatedRowSource;  }
			set { m_anyCommand.UpdatedRowSource = value; }
		}

		/// <summary>Wraps this method from the Native provider.  See ADO.Net documentation for details.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>none</remarks>
		/// <example>No example available.</example>
		public  void Cancel()
		{
			try{m_anyCommand.Cancel();}
			catch{;}
		}

		
		/// <summary>Wraps this method from the Native provider.  Present only to satisfy the interface mapping requirements.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>Really returns a DbParameter object</remarks>
		/// <example>No example available.</example>
		IDbDataParameter IDbCommand.CreateParameter(){return this.CreateParameter();}	
	     		
		/// <summary>Wraps this method from the Native provider.</summary>
		/// <value>See ADO.Net documentation..</value>
		/// <remarks>none</remarks>
		/// <example>No example available.</example>
		public  DbParameter CreateParameter()
		{
			return new DbParameter(m_anyCommand.CreateParameter());
		}

		
		/// <summary>
		/// ExecuteNonQuery is intended for commands that do not return results, instead returning only the number
		///  of records affected.  Please see the ADO.Net documentation for more detail on this item.
		/// </summary>
		/// <remarks> Before calling this method you must set the Connection property to a valid closed DbConnection object.
		/// This function can take parameterized SQL statements in the via the CommandText property 
		/// only if the Parameters collection has been correctly populated.</remarks>
		public  int ExecuteNonQuery()
		{
    
			// There must be a valid and open connection.
			if (m_connection == null || m_connection.State != ConnectionState.Open)
				throw new InvalidOperationException("Connection must be valid and open.");
			DbFactory.Dbg(m_connection.m_InstanceId + " NONQUERY " + "COUNT:" + DbConnection.g_OpenCount + "DETAIL:" +  this.CommandText,DbTrace.Execute);
			return m_anyCommand.ExecuteNonQuery();
		}
	
		/// <summary>Wraps this method from the Native provider.  Present only to satisfy the interface mapping requirements.</summary>
		IDataReader IDbCommand.ExecuteReader()
		{
			DbFactory.Dbg(m_connection.m_InstanceId + " READER " + "COUNT:" + DbConnection.g_OpenCount + "DETAIL:" +  this.CommandText,DbTrace.Execute);
			return this.ExecuteReader();
		}
		
		/// <summary>
		/// ExecuteReader will retrieve results from the data source  and return a DataReader that allows the user to process 
		///  the results. </summary>
		/// <remarks> Before calling this method you must set the Connection property to a valid closed DbConnection object.
		/// This function can take parameterized SQL statements in the via the CommandText property 
		/// only if the Parameters collection has been correctly populated.</remarks>
		public  DbReader ExecuteReader()
		{
			DbFactory.Dbg(m_connection.m_InstanceId + " READER " + "COUNT:" + DbConnection.g_OpenCount + "DETAIL:" +  this.CommandText,DbTrace.Execute);
			return this.ExecuteReader(eLockType.None);
		}
		        
		/// <summary>
		/// ExecuteReader will retrieve results from the data source  and return a DataReader that allows the user to process 
		///  the results. </summary>
		/// <param name="optLock">Used to determine what locking option the DbReader is expected to use.</param>
		/// <returns>Not specified.</returns>
		/// <remarks>If you plan to use a DbWriter against the result set in an optimistic locking mode you must specify that here.</remarks>
		/// <example>No example available.</example>
		public  DbReader ExecuteReader(eLockType optLock)
		{
			// There must be a valid and open connection.
			if (m_connection == null || m_connection.State != ConnectionState.Open)
				throw new InvalidOperationException("Connection must be valid and open");
			DbFactory.Dbg(m_connection.m_InstanceId + " READER " + "COUNT:" + DbConnection.g_OpenCount + "DETAIL:" +  this.CommandText,DbTrace.Execute);

			return new DbReader(m_anyCommand.ExecuteReader(), this, optLock);
		}

		/// <summary>
		/// This function is used internally to achieve proper interface mapping..
		/// </summary>
		IDataReader IDbCommand.ExecuteReader(CommandBehavior behavior)
		{
			return this.ExecuteReader(behavior);
		}

		
		/// <summary>
		/// ExecuteReader will retrieve results from the data source  and return a DataReader that allows the user to process 
		///  the results. </summary>
		/// <returns>A fully populated DbDataReader.</returns>
		/// <remarks>See the ADO.Net documentation for help understanding the CommandBehavior options.</remarks>
		/// <example>No example available.</example>
		/// <param name="behavior">Alters the way the Reader handles it's connection.</param>
		public  DbReader ExecuteReader(CommandBehavior behavior)	
		{
			DbFactory.Dbg(m_connection.m_InstanceId + " READER " + "COUNT:" + DbConnection.g_OpenCount + "DETAIL:" +  this.CommandText,DbTrace.Execute);
			return this.ExecuteReader(behavior,eLockType.None);
		}
		
		/// <summary>
		/// ExecuteReader will retrieve results from the data source  and return a DataReader that allows the user to process 
		///  the results.  This overload allows client code to specify both the Rikmaster locking type and an ADO.Net behavior.
		/// </summary>
		/// <param name="behavior">See: ADO.Net CommandBehavior</param>
		/// <param name="optLock">Riskmaster locking type.</param>
		public  DbReader ExecuteReader(CommandBehavior behavior, eLockType optLock )
		{
			// There must be a valid and open connection.
			if (m_connection == null || m_connection.State != ConnectionState.Open)
				throw new InvalidOperationException("Connection must valid and open");
			 
			DbFactory.Dbg(m_connection.m_InstanceId + " READER " + "COUNT:" + DbConnection.g_OpenCount + "DETAIL:" +  this.CommandText,DbTrace.Execute);

			return new DbReader(m_anyCommand.ExecuteReader(behavior), this, optLock);
		}
		
		/// <summary>
		///   ExecuteScalar assumes that the command will return a single
		///   row with a single column, or if more rows/columns are returned
		///   it will return only the value in the first column of the first row.
		///  </summary>
		public  object ExecuteScalar()
		{
		
			// There must be a valid and open connection.
			if (m_connection == null || m_connection.State != ConnectionState.Open)
				throw new InvalidOperationException("Connection must valid and open");

			// Execute the command.
			DbFactory.Dbg(m_connection.m_InstanceId + " SCALAR " + "COUNT:" + DbConnection.g_OpenCount + "DETAIL:" +  this.CommandText,DbTrace.Execute);

			return m_anyCommand.ExecuteScalar();
		}

		
		/// <summary>
		/// Please see the ADO.Net help for this item.
		/// </summary>
		public  void Prepare()
		{
			m_anyCommand.Prepare();
		}

		 
		/// <summary>
		/// Please see the ADO.Net help for this item.
		/// </summary>
		void IDisposable.Dispose() 
		{
			this.Dispose(true);
			System.GC.SuppressFinalize(this);
		}

		
		/// <summary>
		/// Please see the ADO.Net help for this item.
		/// </summary>
		/// <param name="disposing"></param>
		internal void Dispose(bool disposing) 
		{
			m_anyCommand= null;  //The star of the show...
			
			if(m_ExclusiveConnection) //only allow connection to stay open if we didn't open it..
				m_connection.Close();
			
			m_connection= null; 
			m_txn = null;
			m_parameters = null;
		}

		
		/// <summary>
		/// Provides our internal provider classes access to the ACTUAL original provider object.
		///  NOT to be shared outside this assembly...</summary>
		 internal IDbCommand AsNative(){return m_anyCommand;}
		

		/// <summary>
		/// Utility function strips out our DbProvider Param Tokens and replaces them appropriately for our selected Native Provider.
		/// </summary>
		/// <param name="value">Query string to be made provider specific.</param>
		/// <returns>Provider specific query string.</returns>
		string ApplyProviderSpecificParamTokens(string value)
		{
            if (value==null || !value.Contains(DbParameter.NameDelimiter))
                return value;

			char cProviderDelimiter = '?';
 
			// Determine paramter delimiter
			bool bPositional = false;
			if (m_anyCommand is	SqlCommand)
				cProviderDelimiter ='@';
			else if (m_anyCommand is OracleCommand)	
				cProviderDelimiter =':';
			else if (m_anyCommand is OdbcCommand)
			{
				bPositional = true;
				cProviderDelimiter ='?';
			}

			// Create a stringbuilder and walk through the string replacing parameter markers
			System.Text.StringBuilder sbTmp = new System.Text.StringBuilder(value);
			
			int i = 0;
			int j;
			int len = sbTmp.Length;

			while (i < len)
			{
				if (sbTmp[i] != DbParameter.cNameDelimiter)
					i++;
				else
				{
					// look for closing ~
					j = i;
					for (j = i + 1; j < len && sbTmp[j] != DbParameter.cNameDelimiter; j++)
						;

					if (j < len) // if j = len, then we found no matching ~ and will do nothing
					{
						// here i = first ~ and j = second ~

						// ODBC case to do
						if (bPositional)
						{
							sbTmp[i] = cProviderDelimiter;
							sbTmp.Remove(i + 1, j - i);
							len -= (j - i);
							j = i + 1;
						}
						else
						{
							// normal case
							sbTmp[i] = cProviderDelimiter;  // first ~ becomes provider delimiter
							sbTmp.Remove(j, 1); // second ~ gets removed
							len--;
						}
					}
					i = j;
				}
			}

			return sbTmp.ToString();
		}
    }
}
