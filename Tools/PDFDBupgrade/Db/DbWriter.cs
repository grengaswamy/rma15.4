using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

namespace Riskmaster.Db
{
    #region DbWriter Class
    /// <summary>
    /// This class is designed to take a DataReader  and it's currently selected DataRow and create an insert or 
    /// update statement bound with parameters as appropriate.
    /// This is used instead of the standard commandbuilder with each adapter because we can avoid the overhead 
    /// of making the schema query over again and it gives us finer control over the concurrency checking in the where clause.
    /// 
    /// Note: This class does not have to be retrieved from
    /// </summary>
    public class DbWriter : IDisposable
    {
        private bool isDisposed = false;
        private DbFieldList m_Fields = new DbFieldList();
        private ArrayList m_Tables = new ArrayList(3);
        private ArrayList m_Where = new ArrayList();
        private ArrayList m_Params = new ArrayList();
        private DbConnection m_DbConn;//=null;
        private DbTransaction m_DbTrans;// = null;

        private string m_connectionString;
        private eLockType m_optLock = eLockType.None;
        private string m_timeStamp;
        private string m_userStamp;
        private string m_sourceTimeStamp;
        private bool bSkipAddedByUser = false;


        /// <summary>Internal constructor Riskmaster.Db.DbWriter.DbWriter provided for use by DbFactory.
        /// This overload allows a DbConnection to be passed in for transaction support. Locking is assumed to be
        /// on using this constructor since the Lock information is also passed in.  </summary>
        /// <param name="objConn">Connection on which to execute the writer commands.</param>
        /// <param name="timeStamp"></param>
        /// <param name="userStamp"></param>
        /// <param name="sourceTimeStamp"></param>
        /// <remarks>Used only when RMOptimistic locking is desired.</remarks>
        internal DbWriter(DbConnection objConn, string timeStamp, string userStamp, string sourceTimeStamp)
        {
            m_optLock = eLockType.RMOptimistic;
            m_timeStamp = timeStamp;
            m_userStamp = userStamp;
            m_sourceTimeStamp = sourceTimeStamp;
            m_DbConn = objConn;
            m_connectionString = objConn.ConnectionString;
        }

        /// <summary>Internal constructor Riskmaster.Db.DbWriter.DbWriter provided for use by DbFactory.
        /// This overload allows a DbConnection to be passed in for transaction support. Locking is assumed to be
        /// on using this constructor since the Lock information is also passed in.  </summary>
        /// <param name="objConn">Connection on which to execute the writer commands.</param>
        /// <remarks>Used only when No locking is desired.</remarks>
        internal DbWriter(DbConnection objConn)
        {
            m_optLock = eLockType.None;
            m_DbConn = objConn;
            m_connectionString = objConn.ConnectionString;
        }

        /// <summary>Internal constructor Riskmaster.Db.DbWriter.DbWriter provided for use by DbFactory.
        /// This overload allows a DbReader to be passed in for schema and the initializeValues parameter determines
        ///  whether the current reader row values are placed into the writer.  The optLock parameter allows a lock type to be specified.</summary>
        /// <param name="reader">DbReader with the schema and optionally value data to incorporate in this DbWriter.</param>
        /// <param name="initializeValues">Bool specifies whether the DbReader current row values should be added to this DbWriter.</param>
        /// <param name="optLock">eLockType specifying what lock type to use.  (Currently None or RMOptimistic)</param>
        /// <remarks>none</remarks>
        internal DbWriter(DbReader reader, bool initializeValues, eLockType optLock)
        {
            m_optLock = optLock;
            if (optLock == eLockType.RMOptimistic)
            {
                m_timeStamp = reader.TimeStamp;
                m_userStamp = reader.UserStamp;
                m_sourceTimeStamp = reader.SourceTimeStamp;
            }
            m_connectionString = reader.m_DbCommand.Connection.ConnectionString;
            InitFromReader(reader, initializeValues);
        }


        /// <summary>Internal constructor Riskmaster.Db.DbWriter.DbWriter provided for use by DbFactory.
        /// This overload allows a clean DbWriter to be created against a connection string.  
        /// The Table, DataFields, Values and Where clause must be specified explicitly before Execute() is called.</summary>
        /// <param name="connectionString">Connection string providing access to the target Database.</param>
        /// <remarks>A connection will be created and destroyed internally as necessary for Execute()</remarks>
        internal DbWriter(string connectionString)
        {
            m_connectionString = connectionString;
        }

        /// <summary>Internal constructor Riskmaster.Db.DbWriter.DbWriter provided for use by DbFactory.
        /// This overload allows a DbReader to be passed in for schema and the initializeValues parameter determines
        ///  whether the current reader row values are placed into the writer.  Lock type defaults to None.</summary>
        /// <param name="reader">DbReader with the schema and optionally value data to incorporate in this DbWriter.</param>
        /// <param name="initializeValues">Bool specifies whether the DbReader current row values should be added to this DbWriter.</param>
        /// <remarks>none</remarks>
        internal DbWriter(DbReader reader, bool initializeValues)
        {
            m_connectionString = reader.m_DbCommand.Connection.ConnectionString;
            InitFromReader(reader, initializeValues);
        }

        /// <summary>Internal method Riskmaster.Db.DbWriter.InitFromReader is shared by the various constructors.  
        /// It implements the "schema scrape" and optionally the "value scrape" of Database schema and value information from
        /// the DbReader parameter into the current DbWriter.</summary>
        /// <param name="reader">DbReader with the schema and optionally value data to incorporate in this DbWriter.</param>
        /// <param name="initializeValues">Bool specifies whether the DbReader current row values should be added to this DbWriter.</param>
        /// <remarks>none</remarks>
        internal void InitFromReader(DbReader reader, bool initializeValues)
        {
            if (reader == null || reader.m_DbCommand == null)
                throw new ArgumentException("DbReader contains no data.", "reader");

            //Note: Params are expected only in the where clause... 
            //This is because otherwise the ordering could get screwed up in ODBC with non-named "?" params.
            if (reader.m_DbCommand.Connection.ConnectionType == eConnectionType.Odbc)
                if (reader.m_DbCommand.Parameters.Count > 0)
                    if (reader.m_DbCommand.CommandText.IndexOf("?") < reader.m_DbCommand.CommandText.ToUpper().IndexOf(" WHERE "))
                        throw new ArgumentException("DbWriter could not be initialized from existing DbReader.  DbReader is using an ODBC provider and contains parameters before the 'WHERE' clause.", "reader");

            ParseSelectClause(reader.m_DbCommand.CommandText);
            ParseFromClause(reader.m_DbCommand.CommandText);
            ParseWhereClause(reader.m_DbCommand.CommandText);
            ParseFieldList(reader, initializeValues);
            m_Params = new ArrayList(reader.m_DbCommand.Parameters);
        }

        /// <summary>Riskmaster.Db.DbWriter.ParseSelectClause attempts rudimentary validation
        ///  on the SQL statement passed via the SQL parameter.</summary>
        /// <param name="SQL">String containing the SQL Select statement to validate.</param>
        /// <remarks>Checks only the number of occurences of the SELECT keyword expecting exactly one.</remarks>
        private static void ParseSelectClause(string SQL)
        {
            int SelectCount = Regex.Matches(SQL, @"SELECT ", RegexOptions.Multiline | RegexOptions.IgnoreCase).Count;
            if (SelectCount == 0)
                throw new ArgumentException("DbReader command is not a select statement.  Could not construct writer.", "reader");
            if (SelectCount != 1)
                throw new ArgumentException("DbReader command contains nested select statements.  Could not construct writer.", "reader");
        }

        /// <summary>Riskmaster.Db.DbWriter.ParseFromClause attempts to pick up a single 
        /// table name from the SQL statement passed in the SQL parameter.</summary>
        /// <param name="SQL">String containing the SQL Select statement to examine.</param>
        /// <remarks>Picks up only the FIRST table in the From clause since the DbWriter can only work with a single table at a time.</remarks>
        private void ParseFromClause(string SQL)
        {
            try
            {
                //Try to guess the table name from the query...
                //Pick out only the first table name in the table list 
                // nnithiyanand -  FROM[\s]*([\S]*) this expression fails if the column name has FROM string ex: BT_COUNT_FROM_CODE 
                // so a space is added after FROM in expression.
                Match m = Regex.Match(SQL, @"FROM [\s]*([\S]*)", RegexOptions.Multiline | RegexOptions.IgnoreCase); 
                this.Tables.Add(m.Groups[1].Value);
            }
            catch (Exception e) { throw new Exception(String.Format("Could not choose a target table for writing based on the provided DbReader.\n{0}", e.Message), e); }
        }

        /// <summary>Riskmaster.Db.DbWriter.ParseWhereClause splits the SQL query at the 
        /// where clause adding everything after the WHERE keyword as a single entry in the Where collection property.</summary>
        /// <param name="SQL">String containing the SQL Select statement to examine.</param>
        /// <remarks>We generally treat the full where clause as 1 big string not each individual condition.
        /// The where clause can only contain parameters IF the clause is coming from a DbReader.  
        /// This is becuase there is currently no way to specifiy the desired Parameter values to this class other than 
        /// the reading from the DbReader.Command.Parameters collection during the call to InitFromReader().</remarks>
        private void ParseWhereClause(string SQL)
        {
            try
            {
                //Try to guess the where clause from the query...
                Match m = Regex.Match(SQL, @"WHERE((?<order>.*)ORDER BY|(?<group>.*)GROUP BY|(?<default>.*)\z)", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                int Len = SQL.Length;
                foreach (Group g in m.Groups)
                    if (!String.IsNullOrEmpty(g.Value) & g.Value.Length < Len)
                    {
                        this.Where.Clear();
                        this.Where.Add(g.Value);
                        Len = g.Value.Length;
                    }
            }
            catch (Exception e) { throw new Exception(String.Format("Could not parse the where clause for writing based on the provided DbReader.\n{0}", e.Message), e); }
        }


        /// <summary>Riskmaster.Db.DbWriter.ParseFieldList populates the fields collection 
        /// from the columns and optionally the values of the supplied DbReader.</summary>
        /// <param name="reader">DbReader containing the column collection and current values.</param>
        /// <param name="initializeValues">Specifies whether to gather the values from the current row of DbReader as well as the schema.</param>
        /// <remarks>none</remarks>
        private void ParseFieldList(DbReader reader, bool initializeValues)
        {
            //We're going to try and read values - make sure the reader is initialized and moved
            // onto the first record.
            if (initializeValues)
            {
                try { reader.IsDBNull(0); }
                catch (System.InvalidOperationException e) // Reader wasn't on a valid record.
                {
                    if (!reader.Read()) //Can't put reader on a valid record.
                        throw new ArgumentException(String.Format("DbWriter could not be initialized from existing DbReader.  Current record was invalid.{0}", e.Message), e);
                }

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    if (reader.IsDBNull(i))
                    {
                        Type type = reader.GetFieldType(i);
                        if (IsNumeric(type))
                            m_Fields.Add(reader.GetName(i), 0);
                        else
                            m_Fields.Add(reader.GetName(i), "");
                    }
                    else
                        m_Fields.Add(reader.GetName(i), reader.GetValue(i));
                }
            }
            else
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    Type type = reader.GetFieldType(i);
                    if (type.FullName == "System.String")
                        m_Fields.Add(reader.GetName(i), "");
                    else if (IsNumeric(type))
                        m_Fields.Add(reader.GetName(i), 0);
                    else
                        m_Fields.Add(reader.GetName(i), type.Assembly.CreateInstance(type.Name));
                }
            }
        }


        /// <summary>Riskmaster.Db.DbWriter.Reset places the DbWriter back to an uninitialized state.
        /// Clears all collections, Resets the lock type to None and clears any time stamp information.
        ///   Optionally closes the internal connection if one is open based on the PreserveConnection parameter.</summary>
        /// <param name="preserveConnection">Boolean specifies whether to close the DbWriter's internal connection if one is 
        /// open.</param>
        /// <remarks>none</remarks>
        public void Reset(bool preserveConnection)
        {
            m_Fields = new DbFieldList();
            m_Tables = new ArrayList(3);
            m_Where = new ArrayList();
            m_Params = new ArrayList();
            m_optLock = eLockType.None;
            m_timeStamp = "";
            m_userStamp = "";

            if (!preserveConnection)
                if (m_DbConn != null)
                {
                    m_DbConn.Close();
                    m_DbConn = null;
                }
        }


        /// <summary>Private utility function Riskmaster.Db.DbWriter.IsNumeric tests if type represents any known numeric type.</summary>
        /// <param name="type">The type to test.</param>
        /// <returns>Boolean true if the type is numeric.</returns>
        /// <remarks>none</remarks>
        private static bool IsNumeric(Type type)
        {
            if (type == typeof(int) || type == typeof(long) || type == typeof(byte) ||
                type == typeof(short) || type == typeof(float) || type == typeof(double))
                return true;
            return false;
        }


        /// <summary>Riskmaster.Db.DbWriter.Connection property allows full set\get access 
        /// to the Connection that this DbWriter will use to execute updates and inserts.</summary>
        /// <remarks>If this connection is in use when the DbWriter tries to Execute, the writer will clone the 
        /// connection and use a fresh copy to accomplish the execute.</remarks>
        public DbConnection Connection
        {
            get
            {
                return m_DbConn;
            }
            set
            {
                m_DbConn = value;
            }
        }


        /// <summary>Riskmaster.Db.DbWriter.Fields allows access to the FieldList of this DbWriter.</summary>
        /// <remarks>These fields will be parameterized into the Insert\Update statement and command 
        ///  used inside of Execute().</remarks>
        /// <example>No example available.</example>
        public DbFieldList Fields
        {
            get { return m_Fields; }
        }


        /// <summary>Riskmaster.Db.DbWriter.Tables allows access to the table which is to be 
        /// the target for all updates\inserts.</summary>
        /// <remarks>There is typically only a single table.  We have left this as a possible area for future enhancement.</remarks>
        public ArrayList Tables
        {
            get
            {
                return m_Tables;
            }
        }


        /// <summary>Riskmaster.Db.DbWriter.Where allows access to the Where clause(es) to be
        /// applied to all updates\inserts..</summary>
        /// <remarks>If the where clause is parsed from a DbReader then it will be a single entry in this collection.
        /// It is also possible to explicitly add where clauses individually. Individual where clauses will be joined with an " AND ".  
        /// Therefore for individual clauses to function as expected they should be "stackable" using the "AND" keyword.</remarks>
        /// <example>Example: <code>
        /// writer.where.add("CLAIM_ID = 2");
        /// writer.where.add("EVENT_ID=12");
        /// writer.where.add("(PI_ROW_ID IS NULL OR PI_ROW_ID IN (1,2,3))")
        /// </code>
        /// In this example, the writer would actually use:
        /// <code> "WHERE CLAIM_ID=2 AND EVENT_ID=12 AND (PI_ROW_ID IS NULL OR PI_ROW_ID IN (1,2,3))"</code></example>
        public ArrayList Where
        {
            get
            {
                return m_Where;
            }
        }


        /// <summary>Riskmaster.Db.DbWriter.GetTables allows access to the table which is to be 
        /// the target for all updates\inserts.</summary>
        /// <remarks>The same list of tables as contained in the Tables ArrayList property with the difference that they 
        /// are returned in a strongly typed string array.</remarks>
        /// <example>No example available.</example>
        public string[] GetTables
        {
            get { return (string[])m_Tables.ToArray(typeof(string)); }
        }

        /// <summary>
        /// Executes the specified SQL statement as a transaction
        /// </summary>
        /// <param name="objTrans">Database transaction</param>
        /// <returns>integer indicating success or failure for the database execution</returns>
        public int Execute(DbTransaction objTrans)
        {
            try
            {
                m_DbTrans = objTrans;
                return this.Execute();
            }
            finally
            {
                m_DbTrans = null;
            }
        }

        /// <summary>Riskmaster.Db.DbWriter.Execute takes all supplied information and 
        /// builds parameterized update\insert SQL and populates a Parameters 
        /// collection accordingly.  The end result is update\insert command execution 
        /// at the database using the lock type specified.</summary>
        /// <returns>The integer returned by ExecuteNonQuery()</returns>
        /// <remarks>Will create a temporary connection if necessary.  
        /// This function also enforces the Legacy RMOptimistic lock type if specified.
        /// This function is very similar to that of a Dataset and a "CommandBuilder" but 
        /// is more "legacy friendly" in it's usage pattern and the ability to use RMOptimistic locking instead
        /// of the default for ADO.Net which checks every single field and expects consistent key schema information 
        /// from all datasource types.</remarks>
        /// <example>No example available.</example>
        public int Execute()
        {
            bool bForceCleanUp = false;
            int rowCount = 0;

            if (this.Tables.Count == 0)
                //throw new Riskmaster.ExceptionTypes.RMAppException(Common.Globalization.GetString("DbWriter.Execute.Exception.UnknownTable."));
                throw new Riskmaster.ExceptionTypes.RMAppException("DbWriter.Execute.Exception.UnknownTable.");

            //Try to get a valid connection
            if (m_DbConn == null)  //No connection provided to us.
            {
                m_DbConn = DbFactory.GetDbConnection(m_connectionString);
                m_DbConn.Open();
                bForceCleanUp = true;
            }
            else if (m_DbConn.State != ConnectionState.Open)  //Connection provided to us is not available.
            {
                m_DbConn = DbFactory.GetDbConnection(m_connectionString);
                m_DbConn.Open();
                bForceCleanUp = true;
            }

            if (m_DbConn.State != ConnectionState.Open)
                throw new Exception("Database connection unavailable for writing.");
            try
            {
                //Use that connection to create the command object.
                DbCommand cmd = m_DbConn.CreateCommand();

                //Attempt to use provided transaction context if any.
                if (this.m_DbTrans != null)
                    cmd.Transaction = m_DbTrans;

                //BSB	 Bug fix. This must come before params are populated since
                // Optimistic Locking fields are still being added/removed at this point.
                cmd.CommandText = this.Sql;

                DbParameter p = null;

                //Populate Params Collection as required by the field list.
                for (int i = 0; i < this.Fields.Count; i++)
                {
                    p = cmd.CreateParameter();
                    p.Value = this.Fields[i].Value;
                    p.ParameterName = this.Fields[i].ParameterName;
                    cmd.Parameters.Add(p);
                }

                //Populate Params Collection with any parameters from the original query (can happen when initialized from a reader).
                for (int i = 0; i < this.m_Params.Count; i++)
                {
                    p = cmd.CreateParameter();
                    p.Value = (this.m_Params[i] as DbParameter).Value;
                    p.ParameterName = (this.m_Params[i] as DbParameter).ParameterName;
                    cmd.Parameters.Add(p);
                }

                //Execute the parameterized query
                rowCount = cmd.ExecuteNonQuery();
                if (m_optLock == eLockType.RMOptimistic)
                    if (rowCount < 1)
                        throw new Exception(String.Format("Optimistic Locking Error.  Data has been modified by {0} since the original version from {1} was read.  ", FetchTimeStamp(cmd), m_sourceTimeStamp));
            }
            catch (Exception e)
            {
                //throw new Riskmaster.ExceptionTypes.RMAppException(Common.Globalization.GetString("DbWriter.Execute.Exception.DatabaseError."), e);
                throw new Riskmaster.ExceptionTypes.RMAppException(e.Message);
            }
            finally
            {
                if (bForceCleanUp)
                {
                    m_DbConn.Close();
                    m_DbConn = null;
                }
            }
            return rowCount;
        }


        /// <summary>
        /// Add by kuladeep for Jira-527
        /// It is overload function(Execute) for Output parameter.
        /// </summary>
        /// <param name="currentMode"></param>
        /// <returns></returns>
        public Dictionary<string, object> Execute(ExecuteMode currentMode)
        {
            Dictionary<string, object> returnParameter = null;
            if (currentMode == ExecuteMode.OutParam)
            {
                bool bForceCleanUp = false;
                int rowCount = 0;
                returnParameter = new Dictionary<string, object>();

                if (this.Tables.Count == 0)
                    //throw new Riskmaster.ExceptionTypes.RMAppException(Common.Globalization.GetString("DbWriter.Execute.Exception.UnknownTable."));
                    throw new Riskmaster.ExceptionTypes.RMAppException("DbWriter.Execute.Exception.UnknownTable.");

                //Try to get a valid connection
                if (m_DbConn == null)  //No connection provided to us.
                {
                    m_DbConn = DbFactory.GetDbConnection(m_connectionString);
                    m_DbConn.Open();
                    bForceCleanUp = true;
                }
                else if (m_DbConn.State != ConnectionState.Open)  //Connection provided to us is not available.
                {
                    m_DbConn = DbFactory.GetDbConnection(m_connectionString);
                    m_DbConn.Open();
                    bForceCleanUp = true;
                }

                if (m_DbConn.State != ConnectionState.Open)
                    throw new Exception("Database connection unavailable for writing.");
                try
                {
                    //Use that connection to create the command object.
                    DbCommand cmd = m_DbConn.CreateCommand();

                    //Attempt to use provided transaction context if any.
                    if (this.m_DbTrans != null)
                        cmd.Transaction = m_DbTrans;

                    //BSB	 Bug fix. This must come before params are populated since
                    // Optimistic Locking fields are still being added/removed at this point.
                    cmd.CommandText = this.Sql;

                    DbParameter p = null;

                    //Populate Params Collection as required by the field list.
                    for (int i = 0; i < this.Fields.Count; i++)
                    {
                        p = cmd.CreateParameter();
                        p.Value = this.Fields[i].Value;
                        if (this.Fields[i].Direction == ParameterDirection.Output)
                            p.Direction = ParameterDirection.Output;
                        p.ParameterName = this.Fields[i].ParameterName;
                        cmd.Parameters.Add(p);
                    }

                    //Populate Params Collection with any parameters from the original query (can happen when initialized from a reader).
                    for (int i = 0; i < this.m_Params.Count; i++)
                    {
                        p = cmd.CreateParameter();
                        p.Value = (this.m_Params[i] as DbParameter).Value;
                        p.ParameterName = (this.m_Params[i] as DbParameter).ParameterName;
                        cmd.Parameters.Add(p);
                    }

                    //Execute the parameterized query
                    rowCount = cmd.ExecuteNonQuery();

                    for (int i = 0; i < cmd.Parameters.Count; i++)
                    {

                        p = (DbParameter)cmd.Parameters[i];

                        if (p.Direction == ParameterDirection.Output)
                        {
                            returnParameter.Add(((DbParameter)cmd.Parameters[i]).ParameterName, ((DbParameter)cmd.Parameters[i]).Value);
                        }
                    }

                    if (m_optLock == eLockType.RMOptimistic)
                        if (rowCount < 1)
                            throw new Exception(String.Format("Optimistic Locking Error.  Data has been modified by {0} since the original version from {1} was read.  ", FetchTimeStamp(cmd), m_sourceTimeStamp));
                }
                catch (Exception e)
                {
                    //throw new Riskmaster.ExceptionTypes.RMAppException(Common.Globalization.GetString("DbWriter.Execute.Exception.DatabaseError."), e);
                    throw new Riskmaster.ExceptionTypes.RMAppException(e.Message);
                }
                finally
                {
                    if (bForceCleanUp)
                    {
                        m_DbConn.Close();
                        m_DbConn = null;
                    }
                }
            }
            else
                this.Execute();
            return returnParameter;
        }
        /// <summary>
        /// Add by kuladeep for Jira-527
        /// Check for If Output/Input parameter case.
        /// </summary>
        public enum ExecuteMode
        {
            OutParam,
            InParam
        };

        /// <summary>Riskmaster.Db.DbWriter.Sql allows read only access to the SQL 
        /// which the DbWriter would generate and use if an Execute call were to be called at this point.</summary>
        /// <remarks>Simply calls the private BuildSQL method for implementation.</remarks>
        /// <example>No example available.</example>
        public string Sql { get { return BuildSql(); } }

        public bool DefaultDateTimeAndUser
        {
            get
            {
                return bDefaultDateTimeAndUser;
            }
            set
            {
                bDefaultDateTimeAndUser = value;
            }
        }

        private bool bDefaultDateTimeAndUser = true;

        /// <summary>
        /// Add by kuladeep for Jira-527
        /// </summary>
        public string JoinSQL { get; set; }


        /// <summary>Private utiltiy function Riskmaster.Db.DbWriter.FetchTimeStamp returns the User and Time stamp currently
        /// on the record which DbCommand object tried to update.</summary>
        /// <param name="cmd">DbCommand previously used to process an Execute() call which failed 
        /// to complete because of an RMOptimistic lock violation.</param>
        /// <remarks>This function is used to provide more information to the user about who was working on the same data and
        /// when that person saved changes.</remarks>
        private string FetchTimeStamp(DbCommand cmd)
        {
            DbReader reader = null;
            try
            {
                System.Text.StringBuilder sbFromClause = new System.Text.StringBuilder();
                if (cmd.CommandText.IndexOf("SELECT ") >= 0)
                    sbFromClause.Append(cmd.CommandText.Substring(cmd.CommandText.ToUpper().IndexOf(" FROM")));
                else if (cmd.CommandText.IndexOf("UPDATE ") >= 0 || cmd.CommandText.IndexOf("INSERT ") >= 0)
                {
                    sbFromClause.AppendFormat("FROM {0} WHERE ", this.Tables[0]);

                    for (int i = 0; i < m_Where.Count; i++)
                    {
                        sbFromClause.Append(m_Where[i].ToString());
                        sbFromClause.Append(" ");
                    }
                }

                // BSB 03.03.2006 This causes a problem if we're writing to a table inside an ongoing
                // transaction - that means we HAVE to use the same connection in order to be able to read the
                // stamp information.
                //	reader = DbFactory.GetDbReader(cmd.Connection.ConnectionString,"SELECT UPDATED_BY_USER, DTTM_RCD_LAST_UPD " + fromClause);
                cmd.Parameters.Clear();
                cmd.CommandText = String.Format("SELECT UPDATED_BY_USER, DTTM_RCD_LAST_UPD {0}", sbFromClause);
                reader = cmd.ExecuteReader(Riskmaster.Db.eLockType.None);

                if (reader.Read())
                {
                    string s = String.Format("{0} at {1}", reader.GetString(0), reader.GetString(1));
                    reader.Close();
                    return s;
                }
                else
                {
                    reader.Close();
                    return "";
                }
            }
            catch (Exception e)
            {
                if (reader != null)
                    reader.Close();
                //throw new Riskmaster.ExceptionTypes.RMAppException(Common.Globalization.GetString("DbWriter.FetchTimeStamp.Exception."), e);
                throw new Riskmaster.ExceptionTypes.RMAppException("DbWriter.FetchTimeStamp.Exception.", e);
            }
        }

        /// <summary>Riskmaster.Db.DbWriter.BuildSql implements the building of a SQL Update or Insert statement 
        /// taking into account parameterization and the current locking type in force..</summary>
        /// <returns>String containing the SQL statement to be executed.</returns>
        /// <remarks>Time stamp and User stamp are expected to be specified by the client if 
        /// RMOptimistic locking is in force.  These are the values that must be identical in the DB in order 
        /// to ensure no intermediate changes are being over-written.</remarks>
        private string BuildSql()
        {
            System.Text.StringBuilder sbSql = new System.Text.StringBuilder();
            //sql = "";

            while (m_Where.Contains(""))
                m_Where.Remove("");

            //Add timestamp fields
            if (m_optLock == eLockType.RMOptimistic && bDefaultDateTimeAndUser)
            {
                try { m_Fields.Remove("DTTM_RCD_LAST_UPD"); }
                catch { ;}
                try { m_Fields.Remove("UPDATED_BY_USER"); }
                catch { ;}
                try { m_Fields.Remove("DTTM_RCD_ADDED"); }
                catch { ;}

                try
                {
                    //For Funds and Funds Trans Split we do not want to replace the Added by user if it already contains a value. 
                    //In cases where we print autochecks we need to retain the value of the Added by User
                    if ((m_Tables.Contains("FUNDS")) || (m_Tables.Contains("FUNDS_TRANS_SPLIT")))
                    {
                        bSkipAddedByUser = true;
                    }
                    else
                    {
                        bSkipAddedByUser = false;
                    }
                }
                catch
                {
                    bSkipAddedByUser = true;
                }
                if (bSkipAddedByUser == true)
                {
                    //For Funds and Funds Trans Split we do not want to replace the Added by user if it already contains a value. 
                    //In cases where we print autochecks we need to retain the value of the Added by User
                    if (m_Fields["ADDED_BY_USER"].Value == string.Empty)
                    {
                        try { m_Fields.Remove("ADDED_BY_USER"); }
                        catch { ;}
                        bSkipAddedByUser = false;
                    }
                }
                else
                {
                    try { m_Fields.Remove("ADDED_BY_USER"); }
                    catch { ;}
                }



                //Always fill the updated fields.				
                m_Fields.Add("DTTM_RCD_LAST_UPD", m_timeStamp);
                // JP 01.09.2007   m_Fields.Add("UPDATED_BY_USER", m_userStamp);
                // xhu MITS 19811 It's better to let the user know the error than truncate the data
                //Otherwise it could take the user long time to find out the issue
                //if (m_userStamp.Length > 8)
                //	m_Fields.Add("UPDATED_BY_USER", m_userStamp.Substring(0, 8));
                //else
                m_Fields.Add("UPDATED_BY_USER", m_userStamp);

                //Only fill the added fields if the record already exists.
                if (m_Where.Count == 0)
                {
                    m_Fields.Add("DTTM_RCD_ADDED", m_timeStamp);
                    // JP 01.09.2007   m_Fields.Add("ADDED_BY_USER", m_userStamp);
                    // xhu MITS 19811 It's better to let the user know the error than truncate the data
                    //Otherwise it could take the user long time to find out the issue
                    //if (m_userStamp.Length > 8)
                    //	m_Fields.Add("ADDED_BY_USER", m_userStamp.Substring(0, 8));
                    //else
                    if (bSkipAddedByUser == false)
                    {
                        m_Fields.Add("ADDED_BY_USER", m_userStamp);
                    }
                }
            }

            if (m_Where.Count == 0)
            {
                sbSql.Append("INSERT INTO ");
                System.Text.StringBuilder sbFields = new System.Text.StringBuilder();
                System.Text.StringBuilder sbValues = new System.Text.StringBuilder("VALUES(");
                //string fields="";
                //string values="VALUES(";
                for (int i = 0; i < m_Tables.Count; i++)
                {
                    sbSql.Append(m_Tables[i].ToString());
                    if (i < m_Tables.Count - 1)
                        sbSql.Append(",");
                }
                sbSql.Append(" ");
                // Build Insert statement
                for (int i = 0; i < m_Fields.Count; i++)
                {
                    //Create a Parameter in the Query,
                    DbField field = m_Fields[i];
                    if (field.Direction != ParameterDirection.Output)//Add case by kuladeep for Jira-527-Check for Output Parameter.
                    {
                        sbFields.Append(field.Name);
                        sbValues.Append(field.ParameterName);
                    }
                    //values+=field.SqlFormat;

                    if (i < m_Fields.Count - 1 && field.Direction != ParameterDirection.Output)
                    {
                        sbFields.Append(",");
                        sbValues.Append(",");
                    }
                }

                //Add case by kuladeep for Jira-527 Start
                if (sbValues.ToString().EndsWith(","))
                {
                    string values = sbValues.ToString();
                    sbValues.Clear();
                    values = values.Substring(0, values.Length - 1);
                    sbValues.Append(values);
                }

                if (sbFields.ToString().EndsWith(","))
                {
                    string fields = sbFields.ToString();
                    sbFields.Clear();
                    fields = fields.Substring(0, fields.Length - 1);
                    sbFields.Append(fields);
                }
                //Add case by kuladeep for Jira-527 End

                sbFields.Insert(0, "(");
                sbFields.Append(")");
                sbValues.Append(")");
                sbSql.AppendFormat("{0} {1}", sbFields, sbValues);

                if (!string.IsNullOrEmpty(this.JoinSQL))//Add condition by kuladeep for Jira-527
                {
                    sbSql.Append(";");
                    sbSql.Append(JoinSQL);
                }
            }
            else
            {
                // Build Update Statement
                sbSql.Append("UPDATE ");
                for (int i = 0; i < m_Tables.Count; i++)
                {
                    sbSql.Append(m_Tables[i].ToString());
                    if (i < m_Tables.Count - 1)
                        sbSql.Append(",");
                }
                sbSql.Append(" SET ");
                for (int i = 0; i < m_Fields.Count; i++)
                {
                    DbField field = m_Fields[i];
                    sbSql.Append((String.Format(" {0}={1}", field.Name, field.ParameterName)));
                    if (i < m_Fields.Count - 1)
                        sbSql.Append(",");
                }
                sbSql.Append(" WHERE ");
                for (int i = 0; i < m_Where.Count; i++)
                {
                    sbSql.Append(m_Where[i].ToString());
                    sbSql.Append(" ");
                }

                //Add where clause
                if (m_optLock == eLockType.RMOptimistic)
                {
                    if (m_Where.Count > 0)
                        sbSql.Append(" AND ");
                    if (String.IsNullOrEmpty(m_sourceTimeStamp))
                        sbSql.AppendFormat("(DTTM_RCD_LAST_UPD = '{0}' OR DTTM_RCD_LAST_UPD IS NULL)", m_sourceTimeStamp);
                    else
                        sbSql.Append(String.Format("DTTM_RCD_LAST_UPD = '{0}'", m_sourceTimeStamp));
                }
            }
            return sbSql.ToString();
        }


        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }//method: Dispose()

        #endregion

        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    //Cleanup managed objects by calling their 
                    //Dispose() methods

                    //Close the underlying connection object if open
                    if (m_DbConn.State == ConnectionState.Open)
                    {
                        m_DbConn.Close();
                    }//if

                    m_DbConn.Dispose();
                    if (m_DbTrans != null)
                        m_DbTrans.Dispose();

                    m_DbConn = null;
                    m_DbTrans = null;
                }//if
                //Cleanup unmanaged objects
            }//if
            isDisposed = true;
        }//method: Dispose()

        /// <summary>
        /// Class destructor
        /// </summary>
        ~DbWriter()
        {
            Dispose(false);
        }//destructor


    }//class 
    #endregion


    #region DbField Class
    /// <summary>Riskmaster.Db.DbField is a name\value class 
    /// with methods to render the name as a SQL parameter and the value as a string valid with inline SQL.</summary>
    /// <remarks>none</remarks>
    public class DbField
    {
        /// <summary>The name of this field in the database.</summary>
        public string Name; //="";
        /// <summary>The value to store in this database field.</summary>
        public object Value;//=null;

        // Add by kuladeep for Jira-527
        /// <summary>
        /// variable declared for ParameterDirection Direction
        /// </summary>
        public ParameterDirection Direction;
        
        /// <summary>This overload of the Riskmaster.Db.DbField.DbField applies the supplied name during creation.</summary>
        /// <param name="name">The field name.</param>
        /// <remarks>none</remarks>
        public DbField(string name)
        {
            this.Name = name;
            //This is added so that existing functionality is not broken
            this.Direction = ParameterDirection.Input; //Add by kuladeep for Jira-527
        }

        /// <summary>This overload of the Riskmaster.Db.DbField.DbField applies 
        /// the supplied name and value during creation.</summary>
        /// <param name="name">The field name.</param>
        /// <param name="value">The field value.</param>
        /// <remarks>none</remarks>
        public DbField(string name, object value)
        {
            this.Name = name;
            this.Value = value;
            //This is added so that existing functionality is not broken
            this.Direction = ParameterDirection.Input; //Add by kuladeep for Jira-527
        }
        /// <summary>
        /// Add by kuladeep for Jira-527
        /// Support Output parameter with Input parameter.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="direction"></param>
        public DbField(string name, object value, ParameterDirection direction):this(name, value)
        {
            this.Direction = direction;
        }

        /// <summary>Riskmaster.Db.DbField.SqlFormat returns the value formatted appropriately for use in 
        /// inline SQL.</summary>
        /// <returns>String containing inline SQL of the value.</returns>
        /// <remarks>The preferred method for this is via a fully parameterized query.</remarks>
        public string SqlFormat
        {
            get
            {
                if (Value is string)
                    return String.Format("'{0}'", Value.ToString().Replace("'", "''"));
                else if (Value == null)
                    return "NULL";
                return Value.ToString();
            }
        }


        /// <summary>Riskmaster.Db.DbField.ParameterName supports DBWriter using fully parameterized queries 
        /// by providing an appropriate Parameter name for this field based on the field name.</summary>
        /// <returns>A provider independant query parameter string.</returns>
        /// <remarks>none</remarks>
        public string ParameterName { get { return DbParameter.NameDelimiter + this.Name + DbParameter.NameDelimiter; } }
    }//class 
    #endregion
}//namespace
