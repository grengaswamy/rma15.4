using System;
using System.Data;
using Oracle.DataAccess.Client;

namespace Riskmaster.Db
{
	
     /// <summary>Riskmaster.Db.DbParameter wraps the native data 
     /// provider DataParameter object. It also provides automatic provider 
     /// specific parameter naming.  Therefore parameter names when specified to 
     /// this object may be completely undecorated. </summary>
	/// <remarks>For example @param1 for SQL server can 
	/// and should be named simply "param1"</remarks>
     public class DbParameter : IDbDataParameter
	{
         /// <summary>The NameDelimiter is the string that must be present surrounding a paramater name in a SQL Query run by DbCommand.  Currently: "~" </summary>
		 public const string NameDelimiter ="~"; //"\x31"; //this is a pain to type using an editor
         /// <summary>The NameDelimiter is the string that must be present surrounding a paramater name in a SQL Query run by DbCommand.  Currently: "~" </summary>
		 public const char cNameDelimiter ='~'; //"\x31"; //this is a pain to type using an editor
		 private IDbDataParameter m_anyParameter	; //Star of the show

 
		
         /// <summary>Riskmaster.Db.DbParameter.DbParameter constructor must take 
         /// a reference to the native provider parameter being wrapped.</summary>
         /// <param name="anyParameter">IDbDataParameter interface of the native parameter object to be wrapped.</param>
         /// <remarks>The fact that we must have the IDbDataParameter interface of an 
         /// externally created parameter means that we cannot provide several other 
         /// convenient constructor overloads.  This is because we have no real knowledge of 
         /// what native provider is being used so we cannot decide which native parameter type to 
         /// create for ourselves.  As a future enhancement we could add the ConnectionType as a 
         /// parameter to the overloaded constructors and created the specific native parameter ourselves.</remarks>
         internal DbParameter(IDbDataParameter anyParameter)
		{
			m_anyParameter = anyParameter;
		}
		
         /// <summary>Riskmaster.Db.DbParameter.AsNative provides our internal provider classes
		 ///  access to the ACTUAL original provider object.
		 ///  NOT to be shared outside this assembly...</summary>
		 /// <returns>Generic Interface on the Native provider parameter object. </returns>
		 /// <remarks>none</remarks>
		  internal IDbDataParameter AsNative(){return m_anyParameter;}
		
		 /// <summary>Riskmaster.Db.DbParameter.DbType wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
		  public DbType DbType 
		{
			get  { return m_anyParameter.DbType; }
			set  { m_anyParameter.DbType= value;  }
		}

		
		 /// <summary>Riskmaster.Db.DbParameter.Direction wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
          public ParameterDirection Direction 
		{
			get { return m_anyParameter.Direction; }
			set { m_anyParameter.Direction= value; }
		}

		
		 /// <summary>Riskmaster.Db.DbParameter.Size wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
		  public int Size 
		{
			get { return m_anyParameter.Size; }
			set { m_anyParameter.Size= value; }
		}
		
		 /// <summary>Riskmaster.Db.DbParameter.Precision wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
          public byte Precision 
		{
			get { return m_anyParameter.Precision; }
			set { m_anyParameter.Precision= value; }
		}
		
		 /// <summary>Riskmaster.Db.DbParameter.Scale wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
          public byte Scale 
		{
			get { return m_anyParameter.Scale; }
			set { m_anyParameter.Scale= value; }
		}

		
		 /// <summary>Riskmaster.Db.DbParameter.IsNullable wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
		  public Boolean IsNullable 
		{
			get { return m_anyParameter.IsNullable; }
		}


		
		 /// <summary>Riskmaster.Db.DbParameter.ParameterName wraps the Native property of the same name.</summary>
		 /// <value>Returns a string reprsenting the Provider specific name.</value>
		 /// <remarks>This function is modified from the ADO.net implementation to accept any 
		 /// decorated or un-decorated parameter name and set the decoration appropriately for 
		 /// the native provider currently in use before applying the change to the native parameter object..</remarks>
		 /// <example>No example available.</example>
		  public String ParameterName 
		{
			get { return ToGenericParameterName(m_anyParameter.ParameterName); }
			set { m_anyParameter.ParameterName = this.ToNativeParameterName(value); }
		}

		
		 /// <summary>Riskmaster.Db.DbParameter.SourceColumn  wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
		  public String SourceColumn 
		{
			get { return m_anyParameter.SourceColumn; }
			set { m_anyParameter.SourceColumn= value; }
		}

		
		 /// <summary>Riskmaster.Db.DbParameter.SourceVersion wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
		  public DataRowVersion SourceVersion 
		{
			get { return m_anyParameter.SourceVersion; }
			set { m_anyParameter.SourceVersion= value; }
		}

		
		 /// <summary>Riskmaster.Db.DbParameter.Value wraps the Native property of the same name.</summary>
		 /// <remarks>See ADO.NET documentation for details.</remarks>
		 /// <example>No example available.</example>
		  public object Value 
		{
			get
			{
				return m_anyParameter.Value;
			}
			set
			{
				if(value==null)
					m_anyParameter.Value = System.DBNull.Value;
				// 07.06.06 BSB HACK Perpetuated because as noted by JP, SQL server will 
				// insert a blank space instead of an empty string becuase it has 
				// no concept of "" being different from NULL.  Legacy
				// Rocket used to do this swap for null so we will go ahead and do it as well.
				else if(value is string && String.IsNullOrEmpty(value.ToString()))
					m_anyParameter.Value = System.DBNull.Value;
				else if(value is bool) // && m_anyParameter is Oracle.DataAccess.Client.OracleParameter)
					m_anyParameter.Value= (bool)value  ? -1:0;
				else
					m_anyParameter.Value= value;
			}
		}

		

         /// <summary>Private utility function  Riskmaster.Db.DbParameter.ToNativeParameterName 
         /// cleans and re-decorates the parameter name in sName appropriately 
         /// for the native provider in use.</summary>
         /// <param name="sName">String value of the Parameter name to convert.</param>
         /// <returns>Not specified.</returns>
         /// <remarks>none</remarks>
         private string ToNativeParameterName(string sName)
		{
			string sTemp  = 	ToGenericParameterName(sName);
			if (m_anyParameter is System.Data.SqlClient.SqlParameter)
				return String.Format("@{0}",sTemp);
			if (m_anyParameter is System.Data.Odbc.OdbcParameter)
				return "?";
            if (m_anyParameter is OracleParameter)
                return String.Format(":{0}", sTemp);

			
			throw new SystemException("Unknown Native Provider type.");

		}
		
         /// <summary>Private utility function Riskmaster.Db.DbParameter.ToDbParameterName
         /// cleans the parameter in sName and adds the appropriate decoration for the Riskmaster provider 
         /// using DbParameter.NameDelimiter.</summary>
         /// <param name="sName">String value to re-decorate.</param>
         /// <returns>String redecorated with the base name surrounded by DbParameter.NameDelimiter tokens..</returns>
         private static string ToDbParameterName(string sName)
		{
			string sTemp  = ToGenericParameterName(sName);
			return String.Format("{0}{1}{2}",DbParameter.NameDelimiter, sTemp,DbParameter.NameDelimiter);
		}
		
         /// <summary>Private utility function Riskmaster.Db.DbParameter.ToGenericParameterName
         /// strips all known parameter name decoration from sName.</summary>
         /// <param name="sName">String parameter name to be un-decorated.</param>
         /// <returns>Un-decorated version of sName string.</returns>
         /// <remarks>none</remarks>
         private static string ToGenericParameterName(string sName)
		{	string sTemp=sName;
			sTemp = sTemp.Replace(DbParameter.NameDelimiter,""); //Wrapper Tokens
			sTemp = sTemp.Replace("@",""); //SQL Server Native Tokens
			sTemp = sTemp.Replace("?",""); //ODBC Token
			sTemp = sTemp.Replace(":",""); //Oracle Server Native Tokens
			sTemp = sTemp.Replace("@",""); //DB2 Server Native Token
			return sTemp;
		}

	}
}
