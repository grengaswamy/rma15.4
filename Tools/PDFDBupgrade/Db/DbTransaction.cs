// Author: Brian Battah 
using System;
using System.Data;
namespace Riskmaster.Db
{
	/// <summary>
	/// DbTransaction wraps the native ADO.Net transaction object.  Note that transactions only make sense 
	/// when data is used in a  "connected" fashion holding a Connection and using Commands.
	/// </summary>
	public class DbTransaction:IDbTransaction, IDisposable
	{
		private IDbTransaction m_anyTransaction=null;


		
         /// <summary>
         /// Allows for a DbTransaction to be created internally (by DbConnection) wrapping the native transaction object provided in the "trans" argument.
         /// </summary>
         /// <param name="trans">The native transaction object to wrap.</param>
         internal DbTransaction(IDbTransaction trans)
		{
			m_anyTransaction=trans;
		}
		
		/// <summary>
		/// Release the resources used by component.
		/// </summary>
		 public void Dispose()
		{
			if(m_anyTransaction!=null)
			{
				((IDisposable)m_anyTransaction).Dispose();
				m_anyTransaction=null;
			}
		}

		/// <summary>
		/// Deprecated: Returns  the DbConnection object currently associated with this transaction.
		/// </summary>
		 /// <remarks>Avoid using this method if possible.  It causes DbConnection class to try to wrap an 
		 /// existing native connection in it's constructor which it was never intended to do. 
		 /// Getting the DbConnection object through the Connection property of the DbTransaction
		/// object has two main issues: first, the connection string credentials will probably be lost. 
		/// (They are munged for security reasons by most native providers instead of being fully returned in the connection string.)
		/// Second, the DatabaseType and ConnectionType properties may not be properly initialized if the connection
		/// was using ODBC and relied upon testing performed in the DbConnection classes "Open" method.</remarks>
		 IDbConnection IDbTransaction.Connection
		{
			get{return this.Connection;}
		}

		/// <summary>
		/// Deprecated: Returns  the DbConnection object currently associated with this transaction.
		/// </summary>
		/// <remarks>Avoid using this method if possible.  It causes DbConnection class to try to wrap an existing native connection in it's constructor 
		/// which it was never intended to do.  Getting the DbConnection object through the Connection property of the DbTransaction
		/// object has two main issues: first, the connection string credentials will probably be lost. 
		/// (They are munged for security reasons by most native providers instead of being fully returned in the connection string.)
		/// Second, the DatabaseType and ConnectionType properties may not be properly initialized if the connection
		/// was using ODBC and relied upon testing performed in the DbConnection classes "Open" method.</remarks>
		 public DbConnection Connection
		{
			get
			{
				if(m_anyTransaction!=null)
				{
					return  new DbConnection(m_anyTransaction.Connection);
				}
				throw new InvalidOperationException("No transaction set.");
			}
		}


			/// <summary>
			/// Returns the IsolationLevel for this transaction.
			/// </summary>
			 public IsolationLevel IsolationLevel
		{
			get
			{
				if(m_anyTransaction!=null)
				{
					return m_anyTransaction.IsolationLevel;
				}
				else
					throw new InvalidOperationException("No transaction set.");
			}
		}

		/// <summary>
		/// Commits the database transaction.
		/// </summary>
		 public void Commit()
		{
			if(m_anyTransaction!=null)
			{
				m_anyTransaction.Commit();
			}
			else
				throw new InvalidOperationException("No transaction set.");
		}

		/// <summary>
		/// Rolls back a transaction from a pending state.
		/// </summary>
		 public void Rollback()
		{
			if(m_anyTransaction!=null)
			{
				m_anyTransaction.Rollback();
			}
			else
				throw new InvalidOperationException("No transaction set.");
		}
		
		/// <summary>
		/// Provides our internal provider classes access to the ACTUAL original provider object.
		///  NOT to be shared outside this assembly...</summary>
          internal  IDbTransaction  AsNative(){return m_anyTransaction; }
	}
}
