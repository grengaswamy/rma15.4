﻿using System;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data.Oracle;
using System.Text.RegularExpressions;
using Oracle.DataAccess.Client;
using System.Data.SqlClient;
using System.Text;

namespace Riskmaster.Db
{
	/// <summary>
	/// Provides a class by which ADO.Net Database providers 
	/// can be instantiated, but ensures that the Factory 
	/// remains clean and is devoid of any instance-based data or methods
	/// </summary>
	public static class ADODbFactory
	{
		#region Enterprise Library CreateDatabase methods
		/// <summary>
		/// Creates an instance of a Database
		/// using the Enterprise Library DatabaseFactory
		/// based on whether or not the default connection string
		/// has been overridden
		/// </summary>
		/// <param name="strConnString">string containing the database connection string</param>
		/// <param name="strFactoryType">string containing the name of the specific database provider</param>
		/// <returns>Database object containing a handle to a specific Database Provider</returns>
		/// <example>
		/// SQL Server:
		/// Database db = ADODbFactory.CreateDatabase("Data Source=myServerAddress;Initial Catalog=myDataBase;User Id=myUsername;Password=myPassword;", "System.Data.SqlClient");
		/// Oracle:
		/// Database db = ADODbFactory.CreateDatabase("Data Source=TORCL;User Id=myUsername;Password=myPassword;", "Oracle.DataAccess.Client");
		/// OleDb Microsoft Access:
		/// Database db = ADODbFactory.CreateDatabase("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\myFolder\myAccess2007file.accdb;Persist Security Info=False;", "System.Data.OleDb");
		/// Odbc: 
		/// Database db = ADODbFactory.CreateDatabase("Driver={SQL Server};Server=(local);Database=myDataBase;UID=myUsername;Password=myPassword;", "System.Data.Odbc");
		/// </example>
		/// <seealso cref="http://www.connectionstrings.com"/>
		/// <remarks>The name of the database provider type string IS Case-Sensitive</remarks>
		public static Database CreateDatabase(string strConnString, string strFactoryType)
		{
			Database db = null;

			switch (strFactoryType)
			{
				case DbProviderTypes.SqlClient:
					SqlDatabase sqlDb = new SqlDatabase(strConnString);
					db = sqlDb;
					break;
				case DbProviderTypes.MSOracleClient:
					Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase oracleDb = new Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase(strConnString);
					db = oracleDb;
					break;
				case DbProviderTypes.OracleClient:
					EntLibContrib.Data.OdpNet.OracleDatabase odpNetDb = new EntLibContrib.Data.OdpNet.OracleDatabase(strConnString);
					db = odpNetDb;
					break;
				case DbProviderTypes.OleDb:
				case DbProviderTypes.Odbc:
					DbProviderFactory factory = DbProviderFactories.GetFactory(strFactoryType);
					GenericDatabase genDb = new GenericDatabase(strConnString, factory);
					db = genDb;
					break;
				default:
					SqlDatabase defaultDb = new SqlDatabase(strConnString);
					db = defaultDb;
					break;
			}//switch

			return db;
		}//method: CreateDatabase

		/// <summary>
		/// Creates an instance of a Database
		/// using the Enterprise Library DatabaseFactory
		/// based on a specific database connection string name
		/// stored in the configuration file
		/// </summary>
		/// <param name="strRMConnString">string containing the RISKMASTER-formatted database connection string</param>
		/// <returns>Abstract Database object containing a handle to the current Database Provider</returns>
		public static Database CreateDatabase(string strRMConnString)
		{
			string strProviderConnString = string.Empty;

			string strADONetProvider = GetADONetProvider(strRMConnString, out strProviderConnString);

			return CreateDatabase(strProviderConnString, strADONetProvider);

		} // method: CreateDatabase

		/// <summary>
		/// Translates the various types of currently support RISKMASTER 
		/// Database connection strings into their appropriately mapped ADO.Net Data Provider Types
		/// </summary>
		/// <param name="strConnectionString">string containing the original RISKMASTER Database connection string</param>
		/// <param name="strProviderConnectionString">string containing the modified Provider-specific Database connection string</param>
		/// <returns>string containing the name of the mapped ADO.Net Data Provider</returns>
		public static string GetADONetProvider(string strConnectionString, out string strProviderConnectionString)
		{
			string strADONetProvider = string.Empty;

			//Initialize the out parameter to an empty string
			strProviderConnectionString = string.Empty;

			const string SQLSERVER_REGEX = @"(Driver={(?<DRIVER_NAME>SQL\ Server)}|Driver={(?<DRIVER_NAME>SQL\ Server\ Native\ Client\ .+)}|Driver={(?<DRIVER_NAME>SQL\ Native\ Client)});Server=(?<SERVER_NAME>.+);Database=(?<DATABASE_NAME>.+);UID=(?<UID>.+);PWD=(?<PWD>.+);";
			const string SQLSERVER_REPLACE_REGEX = @"Server=${SERVER_NAME};Database=${DATABASE_NAME};User Id=${UID};Password=${PWD};";
			const string ORACLE_REGEX = @"(Driver={(?<DRIVER_NAME>Oracle\ in\ .+)};Server=(?<TNS_NAME>[^;]+);DBQ=(?<TNS_NAME>[^;]+);UID=(?<UID>[^;]+);PWD=(?<PWD>[^;]+);)|(Driver={(?<DRIVER_NAME>Oracle\sin\s.+)};DBQ=(?<TNS_NAME>[^;]+);UID=(?<UID>[^;]+);PWD=(?<PWD>[^;]+);)";
			string ORACLE_REPLACE_REGEX = "Data Source=${TNS_NAME};User Id=${UID};Password=${PWD};";
			const string DSN_REGEX = @"(?<DSN_NAME>DSN=.+)";
			const string PROVIDER_REGEX = @"(?<PROVIDER_NAME>Provider=.+)";
			string strConnectionStringPattern = string.Format("{0}|{1}|{2}|{3}", SQLSERVER_REGEX, ORACLE_REGEX, PROVIDER_REGEX, DSN_REGEX);

			Regex regExpConnString = new Regex(strConnectionStringPattern, RegexOptions.IgnoreCase);

			Match match = regExpConnString.Match(strConnectionString);

			if (match.Success)
			{
				if (match.Groups["DRIVER_NAME"].Value.Contains("SQL Server") || match.Groups["DRIVER_NAME"].Value.Contains("SQL Native"))
				{
					strADONetProvider = DbProviderTypes.SqlClient;
					strProviderConnectionString = Regex.Replace(strConnectionString, SQLSERVER_REGEX, SQLSERVER_REPLACE_REGEX);
				}//if
				else if (match.Groups["DRIVER_NAME"].Value.Contains("Oracle"))
				{
					strADONetProvider = DbProviderTypes.OracleClient;
					strProviderConnectionString = Regex.Replace(strConnectionString, ORACLE_REGEX, ORACLE_REPLACE_REGEX);
				}//else if
				else if (match.Groups["PROVIDER_NAME"].Value.Contains("Provider"))
				{
					strADONetProvider = DbProviderTypes.OleDb;
					strProviderConnectionString = strConnectionString;
				}//else if
				else if (match.Groups["DSN_NAME"].Value.Contains("DSN"))
				{
					strADONetProvider = DbProviderTypes.Odbc;
					strProviderConnectionString = strConnectionString;
				}//else if
			}//if


			return strADONetProvider;
		}//method: GetADONetProvider();

		/// <summary>
		/// Creates an instance of a Database
		/// using the Enterprise Library DatabaseFactory
		/// based on the default database connection string name
		/// stored in the configuration file
		/// </summary>
		public static Database CreateDatabase()
		{
			return DatabaseFactory.CreateDatabase();
		} // method: CreateDatabase 
		#endregion

		#region Wraps the ADO.Net DbProviderFactory functionality
		/// <summary>
		/// Gets the DatabaseProviderFactory based on the ADO.Net Provider Name
		/// </summary>
		/// <param name="strProviderName">string containing the name of the specific ADO.Net Data Provider</param>
		/// <returns>instance of a DbProviderFactory</returns>
		/// <remarks>Possible Providers are System.Data.SqlClient and Oracle.DataAccess.Client</remarks>
		public static DbProviderFactory GetFactory(string strProviderName)
		{
			DbProviderFactory dbFactory = DbProviderFactories.GetFactory(strProviderName);

			return dbFactory;
		}//method: GetFactory 
		#endregion

		#region Wrapped Enterprise Library DatabaseFactory methods
		/// <summary>
		/// Creates a IDataReader object based on a SQL statement to be executed
		/// </summary>
		/// <param name="strRMConnString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed</param>
		/// <returns>IDataReader containing the DataReader to execute</returns>
		/// <exception cref="DataException">DataException that occurs with database connection problems etc.</exception>
		public static IDataReader ExecuteReader(string strRMConnString, string strSQLStmt)
		{
			try
			{
				return CreateDatabase(strRMConnString).ExecuteReader(CommandType.Text, strSQLStmt);
			}
			catch (System.Data.Common.DbException ex)
			{

				throw ex;
			}
		} // method: ExecuteReader 

		/// <summary>
		/// Creates a DataSet object based on a SQL statement to be executed
		/// </summary>
		/// <param name="strRMConnString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed</param>
		/// <returns>DataSet containing the results of the query</returns>
		/// <exception cref="DataException">DataException that occurs with database connection problems etc.</exception>
		public static DataSet ExecuteDataSet(string strRMConnString, string strSQLStmt)
		{
			try
			{
				return CreateDatabase(strRMConnString).ExecuteDataSet(CommandType.Text, strSQLStmt);
			}
			catch (System.Data.Common.DbException ex)
			{

				throw ex;
			}
		} // method: ExecuteDataSet 

		/// <summary>
		/// Executes a query against the database with no expected result set
		/// </summary>
		/// <param name="strRMConnString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed</param>
		/// <returns>integer containing the number of the records affected by the query</returns>
		/// <exception cref="DataException">DataException that occurs with database connection problems etc.</exception>
		public static int ExecuteNonQuery(string strRMConnString, string strSQLStmt)
		{
			try
			{
				return CreateDatabase(strRMConnString).ExecuteNonQuery(CommandType.Text, strSQLStmt);
			}
			catch (System.Data.Common.DbException ex)
			{

				throw ex;
			}
		} // method: ExecuteNonQuery 

		/// <summary>
		/// Executes a query against the database with a single expected scalar return value
		/// </summary>
		/// <param name="strRMConnString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed</param>
		/// <returns>object containing the result of the query</returns>
		/// <exception cref="DataException">DataException that occurs with database connection problems etc.</exception>
		public static object ExecuteScalar(string strRMConnString, string strSQLStmt)
		{
			try
			{
				return CreateDatabase(strRMConnString).ExecuteScalar(CommandType.Text, strSQLStmt);
			}
			catch (System.Data.Common.DbException ex)
			{

				throw ex;
			}
		} // method: ExecuteScalar 
		#endregion

		/// <summary>
		/// Builds a Database Connection String that is compliant with the respective database platform
		/// </summary>
		/// <param name="strDriverName">string containing the name of the ODBC Driver</param>
		/// <param name="strServerName">string containing the name of the Server</param>
		/// <param name="strDatabaseName">string containing the name of the Database</param>
		/// <param name="strUserName">string containing the name of the User ID</param>
		/// <param name="strUserPassword">string containing the name of the Password</param>
		/// <returns>string containing the database connection string for the respective database platform</returns>
		public static string BuildRMConnectionString(string strDriverName, string strServerName,
			string strDatabaseName, string strUserName, string strUserPassword)
		{
            StringBuilder connBldr = new StringBuilder();
            string strConnectionString = string.Empty;

			if (strDriverName.ToLower().IndexOf("oracle") > -1)
			{


                connBldr.AppendFormat("Driver={{{0}}};", strDriverName);
                connBldr.AppendFormat("Server={0};", strServerName);
                connBldr.AppendFormat("DBQ={0};", strServerName);
                connBldr.AppendFormat("UID={0};", strUserName);
                connBldr.AppendFormat("PWD={0};", strUserPassword);

                strConnectionString = connBldr.ToString();
			}//if
			else
			{
                connBldr.AppendFormat("Driver={{{0}}};", strDriverName);
                connBldr.AppendFormat("Server={0};", strServerName);
                connBldr.AppendFormat("Database={0};", strDatabaseName);
                connBldr.AppendFormat("UID={0};", strUserName);
                connBldr.AppendFormat("PWD={0};", strUserPassword);

                strConnectionString = connBldr.ToString();
			}//else
		   
			return strConnectionString;
		}//method: BuildRMConnectionString

		/// <summary>
		/// Gets the appropriate parameter marker format based on the specific
		/// database provider/vendor
		/// </summary>
		/// <param name="strProviderName">string indicating the specific ADO.Net provider
		/// for the database vendor</param>
		/// <returns>string indicating the appropriate parameter marker to utilize
		/// specific to an ADO.Net Provider</returns>
		/// <remarks>This method should be used to create database-agnostic parameterized 
		/// SQL Queries</remarks>
		/// <example>SELECT CompanyName FROM Customers WHERE CustomerID=
		/// string.Format("{0}{1}", ADODBFactory.GetParameterMarkerFormat("System.Data.SqlClient"), "CustomerID");</example>
		public static string GetParameterMarkerFormat(string strProviderName)
		{
			string strParameterFormat = string.Empty;
			const string SQL_PARAMETER_FORMAT = "@";
			const string ORACLE_PARAMETER_FORMAT = ":";
			const string ODBC_PARAMETER_FORMAT = "?";

			switch (strProviderName)
			{
				case "System.Data.SqlClient":
					strParameterFormat = SQL_PARAMETER_FORMAT;
					break;
				case "Oracle.DataAccess.Client":
					strParameterFormat = ORACLE_PARAMETER_FORMAT;
					break;
				case "System.Data.OracleClient":
					strParameterFormat = ORACLE_PARAMETER_FORMAT;
					break;
				case "System.Data.OleDb":
					strParameterFormat = ODBC_PARAMETER_FORMAT;
					break;
				case "System.Data.Odbc":
					strParameterFormat = ODBC_PARAMETER_FORMAT;
					break;
				default:
					//Set the default parameter format as SQL Server
					strParameterFormat = SQL_PARAMETER_FORMAT;
					break;
			}//switch

			return strParameterFormat;
		}//method: GetParameterMarkerFormat

        /// <summary>
        /// Performs a Batch/Bulk Copy to a SQL Server database table
        /// </summary>
        /// <param name="srcConnectionString">string containing the source database connection string</param>
        /// <param name="targetConnectionString">string containing the target database connection string 
        /// for a SQL Server instance</param>
        /// <param name="strSQL">string containing the name of the data to retrieve from the source database</param>
        /// <param name="strDestTableName">string containing the name of the destination database table</param>
        /// <remarks>The SQLBulkCopy object is used internally to perform a very high-speed batch copy operation.  However,
        /// the target database MUST BE A SQL Server otherwise this method will not work.
        /// In addition, the current implementation requires that the Table Mappings be EXACTLY the SAME.  If they are not,
        /// the implementation will require manually specified ColumnMappings which means that this method cannot be used.</remarks>
        public static void SQLBatchCopy(string srcConnectionString, string targetConnectionString, string strSQL, string strDestTableName)
        {
            
            string strADONetTargetSQLConnString = string.Empty, strADONetProvider = string.Empty;

            //Make the target connection string SQL Server compliant
            strADONetProvider = GetADONetProvider(targetConnectionString, out strADONetTargetSQLConnString);

            using (SqlBulkCopy objSQLBulkCopy = new SqlBulkCopy(strADONetTargetSQLConnString, SqlBulkCopyOptions.Default))
            {
                DataSet dstSourceData = ExecuteDataSet(srcConnectionString, strSQL);

                objSQLBulkCopy.DestinationTableName = strDestTableName;

                objSQLBulkCopy.WriteToServer(dstSourceData.Tables[0]);

                //Clean up
                dstSourceData.Dispose();
                dstSourceData = null;

                //Close the SQLBulkCopy object
                objSQLBulkCopy.Close();
            } // using
        } // method: SQLBatchCopy


        /// <summary>
        /// Performs a Batch/Bulk Copy to a Oracle database table
        /// </summary>
        /// <param name="srcConnectionString">string containing the source database connection string</param>
        /// <param name="targetConnectionString">string containing the target database connection string 
        /// for a SQL Server instance</param>
        /// <param name="strSQL">string containing the name of the data to retrieve from the source database</param>
        /// <param name="strDestTableName">string containing the name of the destination database table</param>
        /// <remarks>The OracleBulkCopy object is used internally to perform a very high-speed batch copy operation.  However,
        /// the target database MUST BE an Oracle database otherwise this method will not work.
        /// In addition, the current implementation requires that the Table Mappings be EXACTLY the SAME.  If they are not,
        /// the implementation will require manually specified ColumnMappings which means that this method cannot be used.</remarks>
        public static void OracleBatchCopy(string srcConnectionString, string targetConnectionString, string strSQL, string strDestTableName)
        {
            
            string strADONetOracleConnString = string.Empty, strADONetProvider = string.Empty;

            //Make the target connection string Oracle compliant
            strADONetProvider = GetADONetProvider(targetConnectionString, out strADONetOracleConnString);

            using (OracleBulkCopy objOracleBulkCopy = new OracleBulkCopy(strADONetOracleConnString, OracleBulkCopyOptions.Default))
            {
                DataSet dstSourceData = ExecuteDataSet(srcConnectionString, strSQL);

                objOracleBulkCopy.DestinationTableName = strDestTableName;

                objOracleBulkCopy.WriteToServer(dstSourceData.Tables[0]);

                //Clean up
                dstSourceData.Dispose();
                dstSourceData = null;

                //Close the OracleBulkCopy object
                objOracleBulkCopy.Close();
            } // using
        } // method: OracleBatchCopy

	}//class

	/// <summary>
	/// Provides the list of currently supported Database Provider Types
	/// </summary>
	public struct DbProviderTypes
	{

		public const string Odbc = "System.Data.Odbc";
		public const string OleDb = "System.Data.OleDb";
		public const string SqlClient = "System.Data.SqlClient";
		public const string OracleClient = "Oracle.DataAccess.Client";
		public const string MSOracleClient = "System.Data.OracleClient";
	} // struct

}
