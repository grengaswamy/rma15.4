using System;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.Application.FileStorage;
using System.Reflection;
using System.Text;
using System.IO;
using System.Collections.Generic;
using Riskmaster.Application.MailMerge;
using Riskmaster.Common;
using System.Collections;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Tools.RtfToDoc
{
    /// <summary>
    /// tkr 10/2007. Converts all rtf word merge templates to word docs, and then deletes the
    /// rtf document from document storage.  It only attempts to
    /// convert merge template documents with an "*.rtf" extension.  It ignores merge template
    /// documents with a "*.doc" extensions.  It ignores any other file in document storage.
    /// You will of course want to backup your Riskmaster Database and document storage files/database 
    /// before running this.
    /// It is safe to run this multiple times on a Riskmaster installation--each time it will 
    /// search out merge templates with an rtf extension and convert them, ignoring all
    /// other documents.
    /// </summary>
    class Merge_RtfToDoc
    {
        private class rmTemplate
        {
            public rmTemplate(int formId, string rtfFormFileName, string rtfWorkingFilePath)
            {
                Form_Id = formId;
                Rtf_Form_File_Name = rtfFormFileName;
                Rtf_Working_File_Path = rtfWorkingFilePath;
                Doc_Form_File_Name = Rtf_Form_File_Name.Replace(".rtf", ".doc");
                Doc_Working_File_Path = Rtf_Working_File_Path.Replace(".rtf", ".doc");
            }
            public int Form_Id;
            public string Rtf_Form_File_Name;
            public string Doc_Form_File_Name;
            public string Rtf_Working_File_Path;
            public string Doc_Working_File_Path;
            public bool Converted = false;
            public bool Stored = false;
        }
        static string m_sUserID = string.Empty;
        static string m_sPassword = string.Empty;
        static string m_sDSN = string.Empty;
        static string m_DocPath = "";
        static string m_WorkPath = AppDomain.CurrentDomain.BaseDirectory + "work\\";
        static string m_sConnectionString = "";
        static UserLogin m_objUserLogin = null;
        static bool m_bDbDocType = false;
        static string m_SecDsn = string.Empty;
        static string _logPath = Environment.CurrentDirectory + "\\Merge_RtfToDoc.log";
        static List<rmTemplate> _templates = new List<rmTemplate>();
        static int _warningCount = 0;
        static int _errorCount = 0;

        static void log(string msg, Exception ex)
        {
            _errorCount++;
            System.IO.File.AppendAllText(_logPath, Environment.NewLine + "Error: " + msg + Environment.NewLine + ex.ToString());
            Console.WriteLine("Error: " + msg + Environment.NewLine + ex.ToString());
        }
        private enum MessageType { Info, Warning, Error };

        static void log(string msg)
        {
            log(msg, MessageType.Info);
        }
        static void log(string msg, MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.Error:
                    _errorCount++;
                    break;
                case MessageType.Warning:
                    _warningCount++;
                    break;
            }
            System.IO.File.AppendAllText(_logPath, Environment.NewLine + messageType.ToString() + ": " + msg);
            Console.WriteLine(messageType.ToString() + ": " + msg);
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (!CheckParams(args))
            {
                EndApp();
                return;
            }

            if (!UserLogin())
            {
                EndApp();
                return;
            }

            if (!m_bDbDocType && !System.IO.Directory.Exists(m_DocPath))
            {
                Console.WriteLine(String.Format("Invalid Document Path {0} - No templates processed", m_DocPath));
                EndApp();
                return;
            }
            else if (m_bDbDocType)
            {
                string errMsg = string.Empty;

                using (DbConnection cn = DbFactory.GetDbConnection(m_DocPath))
                {
                    try
                    {
                        cn.Open();
                        cn.Close();
                    }
                    catch (Exception ex)
                    {
                        errMsg = ex.ToString();
                    }
                    if (errMsg.Length > 0)
                    {
                        Console.WriteLine("Error opening connection to document database " + m_DocPath + ", no templates processed. " + Environment.NewLine + errMsg);
                        EndApp();
                        return;
                    }
                }
            }

            if (File.Exists(_logPath))
            {
                string logFile = _logPath;
                int i = 0;
                while (File.Exists(logFile))
                {
                    i++;
                    logFile = _logPath + i.ToString();
                }
                File.Copy(_logPath, logFile);
                File.Delete(_logPath);
            }

            if (!GetMergeTemplates())
            {
                EndApp();
                return;
            }

            if (!ConvertRtfToDoc())
            {
                EndApp();
                return;
            }

            //put converted merge templates in RM doc storage, replacing the rtfs
            if (!StoreConvertedTemplatesInRiskmaster())
            {
                EndApp();
                return;
            }

            if (!UpdateRMMergeTable())
            {
                EndApp();
                return;
            }

            EndApp();
        }

        /// <summary>
        /// Login to the system
        /// </summary>
        static bool UserLogin()
        {
            frmLogin objForm = new frmLogin();
            bool bLoginSuccess = false;
            string sErrorMessage = string.Empty;
            try
            {
                while (!bLoginSuccess)
                {
                    objForm.ShowDialog();
                    if (!objForm.LoginSuccess)
                    {
                        objForm.Dispose();
                        objForm = null;
                        return false;
                    }

                    m_sUserID = objForm.UserID;
                    m_sPassword = objForm.Password;
                    m_sDSN = objForm.DSN;
                    try
                    {
                        m_SecDsn = Riskmaster.Security.SecurityDatabase.Dsn;
                        Login objLogin = new Login(m_SecDsn);
                        bool bLogin = objLogin.AuthenticateUser(m_sDSN, m_sUserID, m_sPassword, out m_objUserLogin);
                        if (bLogin)
                        {
                            m_sConnectionString = m_objUserLogin.objRiskmasterDatabase.ConnectionString;
                            m_DocPath = m_objUserLogin.objRiskmasterDatabase.GlobalDocPath;
                            m_bDbDocType = (m_objUserLogin.objRiskmasterDatabase.DocPathType != 0);
                            bLoginSuccess = true;
                        }
                        else
                        {
                            sErrorMessage = "Login failed. Please try again.";
                            objForm.LoginMessage = sErrorMessage;
                            Console.WriteLine(sErrorMessage);
                        }
                    }
                    catch (OrgSecAccessViolationException ex)
                    {
                        sErrorMessage = "BES is enabled. Please use a BES admin group member account to upgrade PowerViews";
                        objForm.LoginMessage = ex.Message;
                        Console.WriteLine(sErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        sErrorMessage = ex.Message + " Please try again.";
                        objForm.LoginMessage = sErrorMessage;
                        Console.WriteLine(sErrorMessage );
                    }
                }
                objForm.Dispose();
                objForm = null;
            }
            catch (Exception ex) 
            { 
                Console.WriteLine(ex.Message);
                return false;
            };
            return true;
        }


        static void EndApp()
        {
            string msg = "Completed";
            if (_errorCount > 0)
                msg += " with " + _errorCount.ToString() + " Errors";
            if (_warningCount > 0)
                msg += " with " + _warningCount.ToString() + " Warnings";
            msg += ", please read log file at " + _logPath;
            msg += Environment.NewLine + "Press any key to close...";
            Console.WriteLine(msg);

            Console.ReadLine();
        }

        static bool GetMergeTemplates()
        {
            string sTrash = "";

            FileStorageManager objFileManager = null;

            if (m_bDbDocType)
            {
                objFileManager = new FileStorageManager();
                objFileManager.FileStorageType = StorageType.DatabaseStorage;
                objFileManager.DestinationStoragePath = m_DocPath;
            }

            int counter = 0;

            using (DbConnection cn = DbFactory.GetDbConnection(m_sConnectionString))
            {
                cn.Open();

                //Clear out and ensure working folder exists.
                if (System.IO.Directory.Exists(m_WorkPath))
                    System.IO.Directory.Delete(m_WorkPath, true);
                System.IO.Directory.CreateDirectory(m_WorkPath);

                using (DbReader rdr = cn.ExecuteReader("SELECT FORM_ID, FORM_FILE_NAME FROM MERGE_FORM"))
                {
                    while (rdr.Read())
                    {
                        string formFileName = rdr.GetString("FORM_FILE_NAME");
                        int formId = rdr.GetInt("FORM_ID");
                        if (formFileName.EndsWith(".rtf"))
                        {
                            counter++;
                            if (m_bDbDocType) //Pull from DocStorage
                            {
                                try
                                {
                                    objFileManager.RetrieveFile(formFileName, m_WorkPath + formFileName, out sTrash);
                                    CreateHeaderFile(formId, m_WorkPath, formFileName);
                                    _templates.Add(new rmTemplate(formId, formFileName, m_WorkPath + formFileName));
                                }
                                catch (Exception ex)
                                {
                                    log("unable to pull from database " + formFileName + " into " + m_WorkPath + formFileName, ex);
                                    continue;
                                }
                                log("successfully pulled from database " + formFileName + " into " + m_WorkPath + formFileName);
                            }
                            else  //Pull from DocPath
                            {
                                try
                                {
                                    System.IO.File.Copy(m_DocPath + "\\" + formFileName, m_WorkPath + formFileName, true);
                                    CreateHeaderFile(formId, m_WorkPath, formFileName);
                                    _templates.Add(new rmTemplate(formId, formFileName, m_WorkPath + "\\" + formFileName));
                                }
                                catch (Exception ex)
                                {
                                    log("unable to copy " + m_DocPath + "\\" + formFileName + " into " + m_WorkPath + formFileName, ex);
                                    continue;
                                }
                                log("successfully copied " + m_DocPath + "\\" + formFileName + " into " + m_WorkPath + formFileName);
                            }
                        }
                    }
                }
            }
            if (counter == 0)
            {
                log("zero merge templates with '*.rtf' extension exist in your document storage, there is nothing to process.");
                return false;
            }
            else if (counter > _templates.Count)
            {
                log(counter.ToString() + " merge templates with '*.rtf' extension exist in your document storage but only " + _templates.Count + " will be processed.",MessageType.Warning);
            }
            else
            {
                log(counter.ToString() + " merge templates with '*.rtf' extension exist in your document storage and " + _templates.Count + " will be processed.");
            }

            return true;
        }

        static void CreateHeaderFile(long iFormID, string sWorkPath, string rtfFileName)
        {
            IDictionaryEnumerator objEnum = null;
            TemplateField objField = null;
            TemplateField objExtraField = null;
            string sLine = string.Empty;
            string sHeaderFileName = rtfFileName.Replace(".rtf", ".HDR");
            Template oTemplate = new Template(m_objUserLogin, m_bDbDocType, m_DocPath);
            
            oTemplate.SecureDSN = SecurityDatabase.Dsn;
            oTemplate.Load(iFormID);
            objEnum = oTemplate.Fields.GetEnumerator();
            while (objEnum.MoveNext())
            {
                objField = (TemplateField)objEnum.Value;
                if (sLine != "")
                {
                    sLine = sLine + Convert.ToChar(9).ToString();
                }
                sLine = sLine + Convert.ToChar(34).ToString() + objField.WordFieldName + Convert.ToChar(34).ToString();
                
                //For single field, it need to end with ascii 9
                if (oTemplate.Fields.Count == 1)
                {
                    sLine = sLine + Convert.ToChar(9).ToString();
                }
            }

            //write the header file to the work folder
            sHeaderFileName = sWorkPath + sHeaderFileName;
            StreamWriter objWriter = new StreamWriter(sHeaderFileName, false);
            objWriter.Write(sLine + "\r\n" + sLine + "\r\n");
            objWriter.Flush();
            objWriter.Close();
        }


        static bool StoreConvertedTemplatesInRiskmaster()
        {
            FileStorageManager objFileManager = null;
            if (m_bDbDocType)
            {
                objFileManager = new FileStorageManager();
                objFileManager.FileStorageType = StorageType.DatabaseStorage;
                objFileManager.DestinationStoragePath = m_DocPath;
            }

            foreach (rmTemplate template in _templates)
            {
                //get the header file name
                string sHeaderFileName = template.Doc_Form_File_Name;
                sHeaderFileName = sHeaderFileName.Replace(sHeaderFileName.Substring(sHeaderFileName.Length - 4, 4), ".HDR");
                string sHeaderWorkingFilePath = template.Doc_Working_File_Path;
                sHeaderWorkingFilePath = sHeaderWorkingFilePath.Replace(sHeaderWorkingFilePath.Substring(sHeaderWorkingFilePath.Length - 4, 4), ".HDR");

                if (template.Converted)
                {
                    if (m_bDbDocType) //Push into DocStorage
                    {
                        try
                        {
                            objFileManager.StoreFile(template.Doc_Working_File_Path, template.Doc_Form_File_Name);
                            objFileManager.StoreFile(sHeaderWorkingFilePath, sHeaderFileName);
                            template.Stored = true;
                        }
                        catch (Exception ex)
                        {
                            log("while placing in datatabase document storage: " + template.Doc_Working_File_Path, ex);
                            continue;
                        }
                        //delete the rtf
                        try
                        {
                            objFileManager.DeleteFile(template.Rtf_Form_File_Name);
                            log("successfully deleted rtf file from document storage: " + template.Rtf_Form_File_Name);
                        }
                        catch (Exception ex)
                        {
                            log("template was successfully placed in doc storage, but unable to delete rtf: " + template.Rtf_Form_File_Name, ex);
                        }
                    }
                    else  //Push into DocPath
                    {
                        try
                        {
                            File.Copy(template.Doc_Working_File_Path, m_DocPath + "\\" + template.Doc_Form_File_Name, true);
                            File.Copy(sHeaderWorkingFilePath, m_DocPath + "\\" + sHeaderFileName, true);
                            template.Stored = true;
                        }
                        catch (Exception ex)
                        {
                            log("while copying into document storage: " + template.Doc_Working_File_Path + " to " + m_DocPath + "\\" + template.Doc_Form_File_Name, ex);
                            continue;
                        }
                        //delete the rtf
                        try
                        {
                            File.Delete(m_DocPath + "\\" + template.Rtf_Form_File_Name);
                            log("successfully deleted rtf file from document storage: " + m_DocPath + "\\" + template.Rtf_Form_File_Name);
                        }
                        catch (Exception ex)
                        {
                            log("Warning: template was successfully placed in doc storage, but unable to delete rtf: " + m_DocPath + "\\" + template.Rtf_Form_File_Name, ex);
                        }
                    }
                }
            }
            return true;
        }

        static bool UpdateRMMergeTable()
        {
            string sql = "";
            try
            {
                using (DbConnection cnn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    cnn.Open();
                    foreach (rmTemplate template in _templates)
                    {
                        if (template.Stored)
                        {
                            sql = "UPDATE MERGE_FORM SET FORM_FILE_NAME = '" + template.Doc_Form_File_Name + "' WHERE FORM_ID = " + template.Form_Id.ToString();
                            int i = cnn.ExecuteNonQuery(sql);
                            if (i == 1)
                                log("successfully updated MERGE_FORM record for FORM_ID " + template.Form_Id.ToString());
                            else
                            {
                                log("update statement " + sql + " updated " + i.ToString() + " records, it should have updated 1 record",MessageType.Warning);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log("updating MERGE_FORM table while executing sql statement: " + sql, ex);
                return false;
            }

            return true;
        }

        static bool ConvertRtfToDoc()
        {
            Type tApp = null;
            Object objApp = null;
            const int wdFormatDocument = 0;
            try
            {
                // Use reflection to create instance of Word...avoids version compatibility issue
                tApp = System.Type.GetTypeFromProgID("Word.Application");
                objApp = Activator.CreateInstance(tApp);
                foreach (rmTemplate template in _templates)
                {
                    log("converting " + template.Rtf_Working_File_Path + " to " + template.Doc_Working_File_Path);
                    object oDocs = tApp.InvokeMember("Documents", BindingFlags.GetProperty, null, objApp, null);
                    object aDoc = oDocs.GetType().InvokeMember("Open", BindingFlags.InvokeMethod, null, oDocs, new object[] { template.Rtf_Working_File_Path });
                    aDoc.GetType().InvokeMember("Activate", BindingFlags.InvokeMethod, null, aDoc, null);
                    object oActiveDoc = tApp.InvokeMember("ActiveDocument", BindingFlags.GetProperty, null, objApp, null);
                    oActiveDoc.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, oActiveDoc, new object[] { template.Doc_Working_File_Path, wdFormatDocument });
                    aDoc.GetType().InvokeMember("Close", BindingFlags.InvokeMethod, null, aDoc, new object[] { false, null, null });
                    log("successfully converted " + template.Rtf_Working_File_Path + " to " + template.Doc_Working_File_Path);
                    template.Converted = true;
                }
            }
            finally
            {
                //Closing the application
                if (objApp != null)
                    objApp.GetType().InvokeMember("Quit", BindingFlags.InvokeMethod, null, objApp, new object[] { false, null, null });
            }
            return true;
        }

        static bool CheckParams(string[] args)
        {
            if (args.Length > 0)
            {
                Console.WriteLine("Merge_RftToDoc.exe does not use any command line parameters.");
                Console.WriteLine("Please execute this application again without command line arguments.");
                Console.WriteLine("You will select the target Riskmaster database using the standard Riskmaster Security UI.");
                return false;
            }
            return true;
        }
    }
}