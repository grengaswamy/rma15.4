using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using NUnit.Framework; //Open Source Unit Test Harness Code
using Riskmaster.DataModel;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Serialization.Formatters.Binary;

namespace Riskmaster.Tools.UnitTest
{
	
	
	public class BaseComponentTests : DMFTestBase
	{

		public BaseComponentTests(){}

		
        //public void AccessConnection()
        //{
        //    DbConnection cn = DbFactory.GetDbConnection(String.Format("Driver={{Microsoft Access Driver (*.mdb)}};Dbq={0};Uid=;Pwd=;",base.UnitTestSettings["AccessMdbPath"]));
        //    cn.Open();
        //    cn.Close();
        //}		
		
		
		public void RegisterApplication()
		{
			DbConnection cn = DbFactory.GetDbConnection(Riskmaster.Security.SecurityDatabase.Dsn);
			
			cn.Open();
			DbReader rdr = cn.ExecuteReader("SELECT SECURE_DSN_LOGIN, SMTP_SERVER, ADMIN_EMAIL_ADDR  FROM SETTINGS");
			rdr.Read();
			Trace.WriteLine( (rdr.GetInt16("SECURE_DSN_LOGIN")!=0));		
			cn.Close();
		}		
		
		public void GetSystemViewData()
		{
			Trace.WriteLine(dmf.Context.DbConnLookup.ExecuteString("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE FORM_NAME='personinvolvedlist.xml' AND VIEW_ID=0"));
		}	
		
		public void ClearSystemViewData()
		{
			dmf.Context.DbConnLookup.ExecuteNonQuery("DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID=0");
		}
		
		public void InsertSystemViewDataScreens()
		{
			string sTargetPath = base.UnitTestSettings["FDMViewPath"];
			foreach(string sFileName in System.IO.Directory.GetFiles(sTargetPath,"*.xml"))
				InsertSystemViewDataScreen(System.IO.Path.GetFileName(sFileName));
		}	
		public void InsertSystemViewDataScreen(string sFormName)
		{
			string sTargetPath = base.UnitTestSettings["AutoBuildDeployment.FDMViewPath"];
			string sFormTitle = "";
			string sFileName = sTargetPath + "\\" + sFormName; 
			XmlDocument dom = new XmlDocument();
			try{dom.Load(sFileName);}
			catch{Trace.WriteLine("Skipped Parser Error on : " + sFileName);}
			try{sFormTitle = dom.SelectSingleNode("//form/@title").Value;}
			catch{sFormTitle="Unspecified";}
			DbCommand cmd = dmf.Context.DbConnLookup.CreateCommand();
			DbParameter p = cmd.CreateParameter();
			p.Value = dom.OuterXml;
			p.ParameterName = "XML";
			cmd.Parameters.Add(p);
			DbParameter p2 = cmd.CreateParameter();
			p2.Value = sFormTitle;
			p2.ParameterName = "CAPTION";
			cmd.Parameters.Add(p2);
			cmd.CommandText = String.Format("INSERT INTO NET_VIEW_FORMS VALUES(0,'{0}',1,~CAPTION~,~XML~,NULL)",System.IO.Path.GetFileName(sFileName));
			cmd.ExecuteNonQuery();
		}

		
		public void ResetSystemViewDataScreen()
		{
			string sFormName="piwitness.xml";
			dmf.Context.DbConnLookup.ExecuteNonQuery(String.Format("DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID=0 AND FORM_NAME='{0}'",sFormName));
			InsertSystemViewDataScreen(sFormName);
			Trace.WriteLine("Reset Database form: " + sFormName + " from disk path:" + base.UnitTestSettings["AutoBuildDeployment.FDMViewPath"]); 
		}
		
		public void BinarySerializeUserLogin()
		{
			Riskmaster.Security.UserLogin objUserLogin = new UserLogin(base.DataSourceUserId,base.DataSourcePassword,base.DataSourceTarget);
			System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			//System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(objUserLogin.GetType());
			//System.Runtime.Serialization.Formatters.Soap.SoapFormatter formatter = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
			FileStream fs = new FileStream("c:\\BinaryUserLogin.xml", FileMode.Create);
			formatter.Serialize(fs, objUserLogin);
			fs.Close();
		}		

		
		public void LoadandTestSecurityDatabaseFuncIdCache()
		{
			Trace.WriteLine(System.DateTime.Now);
			Trace.WriteLine("Parent of 1200: " + Riskmaster.Security.SecurityDatabase.GetParentFuncId(1200));
			Trace.WriteLine(System.DateTime.Now);
			
		}		
		
		public void BlobPropertyEdit()
		{
			CheckStockForms frm = (CheckStockForms) dmf.GetDataModelObject("CheckStockForms",false);
			frm.MoveFirst();
			string temp = frm.FormSource;
			Trace.WriteLine(temp);
			frm.FormSource = "Hello:" + temp;
			frm.Save();
			frm.FormSource = temp;
			frm.Save();
		}

		
		public void SimpleListEdit()
		{
			Event ev = (Event) dmf.GetDataModelObject("Event",false);
			ev.MoveFirst();
			DataSimpleList lst = ev.EventXOutcome;
			lst.Add(1);
			lst.Add(2);
			lst.Add(3);
			lst.Add(4);
			lst.Add(5);
			ev.Save();
			Trace.WriteLine("SimpleList Contents:" + ev.EventXOutcome.ToString());
			ev = (Event) dmf.GetDataModelObject("Event",false);
			ev.MoveFirst();
			Trace.WriteLine("SimpleList Contents:" + ev.EventXOutcome.ToString());
			lst = ev.EventXOutcome;
			lst.Remove(1);
			lst.Remove(2);
			lst.Remove(3);
			lst.Remove(4);
			lst.Remove(5);
			ev.Save();
			Trace.WriteLine("SimpleList Contents:" + ev.EventXOutcome.ToString());
		}
		
		public void SoapSerializeUserLogin()
		{
			Riskmaster.Security.UserLogin objUserLogin = new UserLogin(base.DataSourceUserId,base.DataSourcePassword,base.DataSourceTarget);
			//			System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			//System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(objUserLogin.GetType());
			System.Runtime.Serialization.Formatters.Soap.SoapFormatter formatter = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
			FileStream fs = new FileStream("c:\\SoapUserLogin.xml", FileMode.Create);
			formatter.Serialize(fs, objUserLogin);
			fs.Close();
		}		
		
		
		public void SoapDeSerializeUserLogin()
		{

			// Open the file containing the data that you want to deserialize.
			FileStream fs = new FileStream("c:\\SoapUserLogin.xml", FileMode.Open);
			SoapFormatter formatter = new SoapFormatter();
			Riskmaster.Security.UserLogin objUserLogin = (UserLogin) formatter.Deserialize(fs);
			Trace.WriteLine(objUserLogin.LoginName);
		}		
		
		
		public void BinaryDeSerializeUserLogin()
		{
			// Open the file containing the data that you want to deserialize.
			FileStream fs = new FileStream("c:\\BinaryUserLogin.xml", FileMode.Open);
			BinaryFormatter formatter = new BinaryFormatter();
			Riskmaster.Security.UserLogin objUserLogin = (UserLogin) formatter.Deserialize(fs);
			Trace.WriteLine(objUserLogin.LoginName);
		}

		//		
		//		public void SecurityFunctionListSize()
		//		{
		//			int before; int after;
		//			Process objProc = System.Diagnostics.Process.GetCurrentProcess();
		//			before = objProc.PrivateMemorySize;
		//			Riskmaster.Security.UserLogin objUserLogin = new UserLogin(base.DataSourceUserId,base.DataSourcePassword,base.DataSourceTarget);
		//			after = objProc.PrivateMemorySize;
		//
		//			Trace.WriteLine(" Before: " + before);
		//			Trace.WriteLine("After: " + after);
		//			Trace.WriteLine("Diff: " + (after - before));
		//
		//
		//			//			Trace.WriteLine("Populated UserLogin: " + System.Diagnostics.PerformanceCounterInstanceData..InteropServices.Marshal.SizeOf());
		//			//PerformanceCounter PC = new System.Diagnostics.PerformanceCounter( "Process","Virtual Bytes","Struzzo");
		//			//Trace.WriteLine("PerfCounter Process\\PrivateBytes\\Struzzo:" + PC.NextValue());
		//
		////			Trace.WriteLine("UserLogin Type: " + System.Runtime.InteropServices.Marshal.SizeOf(objUserLogin.GetType()));
		////			Trace.WriteLine("New UserLogin: " + System.Runtime.InteropServices.Marshal.SizeOf(new UserLogin()));
		//		}

		
		public void GenerateRequestedSerialization()
		{
			GenerateRequestedSerialization("TestRequest-Instance.xml");
		}
		public void GenerateRequestedSerialization(string sResultFile)
		{
			//Trace.WriteLine( "From UnitTest.cs" + System.AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
			//System.IO.File.Copy(System.AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, "c:\\testconfig.txt",true);
			XmlDocument dom = new XmlDocument();
			//dom.LoadXml("<Event><EventXDatedTextList/><ReporterEntity/><EventQM/><EventMedwatch/><EventXAction/><EventXOutcome/><EventSentinel/><Supplementals/></Event>");
			dom.LoadXml(@"<Claim>
			<PrimaryPiEmployee>
				<PiEntity></PiEntity>
			</PrimaryPiEmployee>
			<Supplementals />
			<Jurisdictionals/>
		</Claim>");
			
			StreamWriter stm = File.CreateText(WorkPath + "TestRequest-Instance.xml");
			StreamWriter stm2 = File.CreateText(WorkPath + "TestRequest-JurisView.xml");
			Claim ev= dmf.GetDataModelObject("Claim",false) as Claim;
			ev.MoveTo(7);
			while(ev.FilingStateId==0)
				ev.MoveNext();
			stm.Write(ev.SerializeObject(dom));
			stm.Flush();
			stm.Close();
			stm2.Write(ev.Jurisdictionals.ViewXml.OuterXml);
			stm2.Flush();
			stm2.Close();
		}
		
		public void LoadLastPiPhysician()
		{
			XmlDocument dom = new XmlDocument();
			//dom.LoadXml("<Event><EventXDatedTextList/><ReporterEntity/><EventQM/><EventMedwatch/><EventXAction/><EventXOutcome/><EventSentinel/><Supplementals/></Event>");
			dom.LoadXml(@"<PiPhysician>
			<Physician>
				<PhysEntity></PhysEntity>
			</Physician>
			<Supplementals />
			<Jurisdictionals/>
		</PiPhysician>");

			PiPhysician ev= dmf.GetDataModelObject("PiPhysician",false) as PiPhysician;
			ev.MoveLast();
			Trace.WriteLine(ev.SerializeObject(dom));
		}		
		
		
		public void LoadLastPersonInvolved()
		{
			XmlDocument dom = new XmlDocument();
			//dom.LoadXml("<Event><EventXDatedTextList/><ReporterEntity/><EventQM/><EventMedwatch/><EventXAction/><EventXOutcome/><EventSentinel/><Supplementals/></Event>");
			dom.LoadXml(@"<PersonInvolved>
			<PiEntity>
			</PiEntity>
			<Supplementals />
			<Jurisdictionals/>
		</PersonInvolved>");
			PersonInvolved pi = null;
			PersonInvolved ev= dmf.GetDataModelObject("PersonInvolved",false) as PersonInvolved;
			ev.MoveLast();
			Trace.WriteLine((ev as PersonInvolved).SerializeObject(dom));
			pi = (ev as PersonInvolved);
			Trace.WriteLine(pi.SerializeObject(dom));
		}
		
		public void LoadFirstPatient()
		{
			XmlDocument dom = new XmlDocument();
			//dom.LoadXml("<Event><EventXDatedTextList/><ReporterEntity/><EventQM/><EventMedwatch/><EventXAction/><EventXOutcome/><EventSentinel/><Supplementals/></Event>");
			dom.LoadXml(@"<Patient>
			<Supplementals />
			<PatientEntity/>
			</Patient>");
			Patient ev= dmf.GetDataModelObject("Patient",false) as Patient;
			ev.MoveFirst();
			ev.EmergencyContact="Nobody "+ DateTime.Now.Ticks.ToString();
			ev.Save();
//			Trace.WriteLine(ev.SerializeObject(dom));
		}
		
		public void LoadPhysician()
		{
			XmlDocument dom = new XmlDocument();
			dom.LoadXml(@"<Physician><Supplementals /></Physician>");
			Physician ev= dmf.GetDataModelObject("Physician",false) as Physician;
			ev.MoveTo(2395);
			Trace.WriteLine(ev.SerializeObject(dom));
			
		}		
		
		
		public void SerializeEvent()
		{
			XmlDocument dom = new XmlDocument();
			dom.LoadXml(@"<Event><Supplementals /></Event>");
			Event ev= dmf.GetDataModelObject("Event",false) as Event;
			ev.MoveLast();
			Trace.WriteLine(ev.SerializeObject(dom));
		}
		public void LoadDependentSupp()
		{
			XmlDocument dom = new XmlDocument();
			dom.LoadXml(@"<EmpXDependent><Supplementals /></EmpXDependent>");
			EmpXDependent ev= dmf.GetDataModelObject("EmpXDependent",false) as EmpXDependent;
			ev.MoveLast();
			Trace.WriteLine(ev.SerializeObject(dom));
			ev.HealthPlanFlag = !ev.HealthPlanFlag;
			ev.Supplementals["TEXT_MEMO"].Value="test ticks" + DateTime.Now.Ticks;
			ev.Save();
				Trace.WriteLine(ev.SerializeObject(dom));
			
		}
		
		public void LoadClaim()
		{
			XmlDocument dom = new XmlDocument();
			dom.LoadXml(@" <Claim><CurrentClaimant/><PrimaryPiEmployee><PiEntity/></PrimaryPiEmployee><Supplementals /><Jurisdictionals /></Claim>");
			Claim ev= dmf.GetDataModelObject("Claim",false) as Claim;
			ev.LineOfBusCode = ev.Context.LocalCache.GetCodeId("VA", "LINE_OF_BUSINESS");
			ev.NavType = ClaimNavType.ClaimNavLOB;
			ev.MoveTo(301);
//			Trace.WriteLine(ev.ClaimStatusHistList.SerializeObject());
			ev.Delete();
			//ev.MoveFirst();
			//Trace.WriteLine((ev.Parent as DataObject).SerializeObject());
			Trace.WriteLine("Deletion Done");
			
		}
		
		public void LoadEntity()
		{
			XmlDocument domLoadFrom  = new XmlDocument();

			//Load Last Serialized Copy of Record
			domLoadFrom.Load(WorkPath + "TestRequest-Instance.xml");

			XmlDocument dom = new XmlDocument();
			dom.LoadXml(@"<Entity />");
			Entity ev= dmf.GetDataModelObject("Entity",false) as Entity;
			ev.MoveTo(229);
			Trace.WriteLine(ev.SerializeObject(dom));
		}
		
		public void LoadClaimCaseMgmt()
		{
			XmlDocument dom = new XmlDocument();
			dom.LoadXml(@"<Claim><CaseManagement /></Claim>");
			Claim ev= dmf.GetDataModelObject("Claim",false) as Claim;
			ev.MoveTo(908);
			Trace.WriteLine(ev.SerializeObject(dom));
		}
		
		public void LoadClaimClaimAWW()
		{
			XmlDocument dom = new XmlDocument();
			dom.LoadXml(@"<Claim><ClaimAWW /></Claim>");
			Claim ev= dmf.GetDataModelObject("Claim",false) as Claim;
			ev.MoveTo(908);
			Trace.WriteLine(ev.SerializeObject(dom));
		}
		
		public void RePopulateRequestedSerialization()
		{
			XmlDocument domLoadFrom  = new XmlDocument();
			XmlDocument domStoreTo = new XmlDocument();
			XmlDocument domFetchPattern = new XmlDocument();

			//Load Last Serialized Copy of Record
			domLoadFrom.Load(WorkPath + "TestRequest-Instance.xml");
			Patient ev= dmf.GetDataModelObject("Patient",false) as Patient;
			ev.PopulateObject(domLoadFrom);

			//Attempt to save the changed Record details
			ev.Save();

			//Make a Back-up of Last Serialized Copy of Record
			try{System.IO.File.Copy(WorkPath + "TestRequest-Instance.xml",WorkPath + "TestRequest-Instance-orig.xml",false);}
			catch{}
	
			//Generate the Record Request Pattern
			domFetchPattern.LoadXml("<Event><EventXDatedTextList/><ReporterEntity/><EventQM/><EventMedwatch/><EventXAction/><EventXOutcome/><Supplementals/></Event>");
				
			//Request Re-Serialization and Store to disk
			domStoreTo.LoadXml(ev.SerializeObject(domFetchPattern));
			domStoreTo.Save(WorkPath + "TestRequest-Instance.xml");
		}
		
		public void TestInvokeProperty()
		{
			FundsDeposit ev = dmf.GetDataModelObject("FundsDeposit",false) as FundsDeposit;
			ev.SetProperty("DepositType",ev.DepositType.ToString()); //short
			ev.SetProperty("Amount",ev.Amount.ToString()); //double
			ev.SetProperty("SubAccId",ev.SubAccId.ToString()); //int
			ev.SetProperty("VoidFlag",ev.VoidFlag.ToString()); //bool
		}
		
		public void TestInvokeParent()
		{
			UnitXClaim o = dmf.GetDataModelObject("UnitXClaim",false) as UnitXClaim;
			o.MoveFirst();
			if(o.Parent==null)
				Trace.WriteLine("Parent Not Yet Loaded.");
			Trace.WriteLine("LoadParent ResultCode: " + o.LoadParent());
			Trace.WriteLine("Parent Claim Number was:" + (o.Parent as Claim).ClaimNumber);
		}
		
		public void TestSerialization()
		{
			StreamWriter stm;
			System.Reflection.Assembly a = System.Reflection.Assembly.GetAssembly(typeof(Riskmaster.DataModel.Event));
			Type[] types = a.GetTypes();
			foreach(Type t in types)
			{
				if(!t.IsClass)
					continue;
				if(t.BaseType.Name != "DataCollection" && t.BaseType.Name != "DataObject" && t.BaseType.Name!="PersonInvolved")
					continue;
				Trace.WriteLine( "Generating: " + t.Name);
				stm = File.CreateText(WorkPath + t.Name + "-Instance.xml") ;
				DataRoot o = dmf.GetDataModelObject(t.Name,false);
				if(o as  DataObject !=null)
				{
					(o as INavigation).MoveFirst();
					stm.Write((o as  DataObject).SerializeObject());
				}
				if(o as  DataCollection !=null)
				{
					stm.Write((o as  DataCollection).SerializeObject());
				}

				stm.Flush();
				stm.Close();
			}
		}

		//		
		//		public void ParseValue()
		//		{
		//			//bool targetValue;
		//			object targetValue = 0;
		//			//if(targetValue is bool)
		//			//	bChangedValue = (targetValue.ToString() != Conversion.ConvertObjToBool(this.m_Fields[fieldName.ToUpper()]).ToString());
		//
		//			//For Numerics, allow 0 to test equivalent to ""...
		//			if(targetValue is int || targetValue is double || targetValue is float)
		//				if(targetValue.ToString() != "0" || this.m_Fields[fieldName.ToUpper()].ToString()!="")
		//					bChangedValue = (targetValue.ToString() != this.m_Fields[fieldName.ToUpper()].ToString());
		//
		//
		//		}
		
		public void EntitySaveTest()
		{
			Entity o = dmf.GetDataModelObject("Entity",false) as Entity;
			o.MoveFirst();
			string prevEmail = o.EmailAddress;
			o.EmailAddress = "me@here.com";
			o.Save();
			o.EmailAddress = prevEmail;
			o.Save();
		}
		
		public void AddNewEntity()
		{
			Entity o = dmf.GetDataModelObject("Entity",false) as Entity;
			o.FirstName="BSB Test Entity";
			o.LastName="BSBTEST3";
			o.EmailAddress = "me@here.com";
			o.Save();
		}
		
		public void ParseGlobalDocPath()
		{
			int DsnId = Int32.Parse(base.UnitTestSettings["DataSourceId"]);
			Riskmaster.Security.RiskmasterDatabase rmDB = new RiskmasterDatabase(DsnId);
			string trash = rmDB.GlobalDocPath;
		}

		
		public void ValidateSingleFile()
		{
			string sFileName = WorkPath + "TestRequest-Instance.xml";
			XmlValidatingReader reader;
			XmlDocument dom = new XmlDocument();
			dom.Load(sFileName);

			Trace.WriteLine("Validating: " + sFileName);

			reader = new XmlValidatingReader(new XmlTextReader("file://" + sFileName));
			reader.ValidationType = System.Xml.ValidationType.Schema;
			while (reader.Read()) 
			{;}
			reader.Close();
		}
		
		public void ValidateSerializedData()
		{
			XmlTextWriter writer;
			XmlValidatingReader reader;
			XmlDocument dom;
			foreach(string sFileName in System.IO.Directory.GetFiles(WorkPath,"*-Instance.xml"))
			{
				dom = new XmlDocument();
				dom.Load(sFileName);

				Trace.WriteLine("Validating: " + sFileName);

				reader = new XmlValidatingReader(new XmlTextReader("file://" + sFileName));
				reader.ValidationType = System.Xml.ValidationType.Schema;
				while (reader.Read()) 
				{;}
				reader.Close();
				
				writer = new XmlTextWriter(sFileName,System.Text.Encoding.UTF8);
				writer.Formatting = System.Xml.Formatting.Indented;
				
				dom.WriteTo(writer);
				writer.Close();
				dom = null;
			}

		}
		 
		public void LoadEventData()
		{
			object ret;
			Trace.WriteLine("Hello NUnit");
			Event myEvent = (Event)dmf.GetDataModelObject("Event",false);
			myEvent.Dump();
			Assert.AreEqual(0,myEvent.EventId);
			myEvent.MoveFirst();
			object junk = myEvent.PiList.GetPrimaryPi();
			Assert.IsFalse(myEvent.EventId ==0);

			//Access some properties looking for an exception to be thrown...
			ret = myEvent.EventXDatedTextList.Count;
			ret = myEvent.PiList.Count;
			foreach(PersonInvolved objPI in myEvent.PiList)
				Assert.IsTrue(objPI.PiEid == objPI.PiEntity.EntityId);

			myEvent.Dump();
				
			//Move to Next Event Sequentially
			myEvent.MoveNext();
			myEvent.Dump();
			ret = myEvent.EventId;
			ret = myEvent.EventXAction;

			//Move to Last Event
			myEvent.MoveLast();
			ret = myEvent.EventXDatedTextList.Count;
			ret = myEvent.EventOsha.HowAccOccurred;
			myEvent.Dump();
			myEvent.EventId.ToString();
			return;
		}

		//Unit Test Deletion functionality
		[NUnit.Framework.Test]
		public  void DeleteEventDatedText()
		{
			Event myEvent = (Event)dmf.GetDataModelObject("Event",false);
			myEvent.MoveLast();
			object ret = myEvent.Dump();
			ret = myEvent.EventId;
			ret  = "Number of Dated Text Entries: " + myEvent.EventXDatedTextList.Count;
			string s ="";
			foreach(EventXDatedText x in myEvent.EventXDatedTextList)
				s += x.EvDtRowId + ",";
			ret  = "Dated Text Entry Ids before delete: " + s;

			int i =0;
			foreach(EventXDatedText x in myEvent.EventXDatedTextList)
			{
				if(i==0)
					i = x.KeyFieldValue;
			}
			myEvent.EventXDatedTextList.Delete(i);

			s ="";
			foreach(EventXDatedText x in myEvent.EventXDatedTextList)
				s += x.EvDtRowId + ",";
			ret = "Dated Text Entry Ids after delete: " +s;

			myEvent.Refresh();
			ret = "Number of Dated Text Entries after delete: " + myEvent.EventXDatedTextList.Count;
			ret = myEvent.Dump();
			return;
		}
		
		public void Path()
		{
			Trace.WriteLine( "From UnitTest.cs" + System.AppDomain.CurrentDomain.BaseDirectory);
		}	

		
		public void TestSuppSaveLoss()
		{
			XmlDocument dom = new XmlDocument();
			dom.LoadXml("<Event><Supplementals /></Event>");

			Event o = dmf.GetDataModelObject("Event",false) as Event;
			Event o2 = dmf.GetDataModelObject("Event",false) as Event;
			o.MoveFirst();
			string sUnique=DateTime.Now.Ticks.ToString();
			o.LocationAreaDesc = sUnique;
			o.Supplementals["TEST_ADDNEW_TEXT"].Value = sUnique;

			string sXml = o.SerializeObject(dom);
			dom.LoadXml(sXml);
			o2.PopulateObject(dom);
			o2.Save();
			Trace.WriteLine("Save Completed");
		}

        
        public void UpdateEntity()
        {
            Entity e = dmf.GetDataModelObject("Entity", false) as Entity;

            e.MoveLast();
            String sPrevUpdateStamp = e.DttmRcdLastUpd;
            e.MiddleName = "Mod";
            e.Save();
            String sNewUpdateStamp = e.DttmRcdLastUpd;
            Trace.WriteLine(sPrevUpdateStamp);
            Trace.WriteLine(sNewUpdateStamp);
            Trace.WriteLine(sNewUpdateStamp == sPrevUpdateStamp);

        }

        
        public void DeleteEntity()
        {
            Entity e = dmf.GetDataModelObject("Entity", false) as Entity;

            e.MoveLast();
            Trace.WriteLine("Entity Id:" + e.EntityId);

            String sPrevUpdateStamp = e.DttmRcdLastUpd;
            e.Delete();
            String sNewUpdateStamp = e.DttmRcdLastUpd;
            Trace.WriteLine(sPrevUpdateStamp);
            Trace.WriteLine(sNewUpdateStamp);
            Trace.WriteLine(sNewUpdateStamp == sPrevUpdateStamp);

        }
		
		public void BeginTrans()
		{
			dmf.Context.TransStart();
		}
		//Test binding connections in SQL Server.
		public void SQLConnectionBinding()
		{
			string sBindToken="";
			DbConnection m_objConnLookup;
			DbTransaction m_objTrans;
			DbConnection m_objConn = Db.DbFactory.GetDbConnection(base.UnitTestSettings["ConnStr"]);
			m_objConn.Open();
			if(m_objConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
			{
				m_objConnLookup = Db.DbFactory.GetDbConnection(m_objConn.ConnectionString);
				m_objConnLookup.Open();
				m_objTrans = m_objConn.BeginTransaction();	
				m_objConn.ExecuteNonQuery("SELECT SHORT_CODE FROM CODES WHERE CODE_ID=241",m_objTrans);
				DbCommand cmd = m_objConn.CreateCommand();

				cmd.CommandType= System.Data.CommandType.StoredProcedure;
				if(m_objConn.ConnectionType== eConnectionType.Odbc)
					cmd.CommandText="{? = CALL sp_getbindtoken (?)}";
				else
					cmd.CommandText="sp_getbindtoken";
				DbParameter p = cmd.CreateParameter();
				p.Direction = System.Data.ParameterDirection.ReturnValue;
				p.Size=255;
				cmd.Parameters.Add(p);
				p = cmd.CreateParameter();
				p.ParameterName= "out_token";
				p.Direction = System.Data.ParameterDirection.Output;
				p.DbType = System.Data.DbType.AnsiString;
				p.Size = 255;
				cmd.Parameters.Add(p);
				cmd.Transaction = m_objTrans;
				cmd.ExecuteNonQuery();
				sBindToken = (cmd.Parameters["out_token"] as DbParameter).Value.ToString();
				Trace.WriteLine( (cmd.Parameters[1] as DbParameter).Value);

				cmd = m_objConnLookup.CreateCommand();
				cmd.CommandType= System.Data.CommandType.StoredProcedure;
				if(m_objConn.ConnectionType== eConnectionType.Odbc)
					cmd.CommandText="{? = CALL sp_bindsession (?)}";
				else
					cmd.CommandText="sp_bindsession";
				
				p = cmd.CreateParameter();
				p.Direction = System.Data.ParameterDirection.ReturnValue;
				p.Size=4;
				cmd.Parameters.Add(p);

				p = cmd.CreateParameter();				
				p.ParameterName= "bind_token";
				p.Direction = System.Data.ParameterDirection.Input;
				if(m_objConn.ConnectionType== eConnectionType.Odbc)
					p.DbType = (System.Data.DbType) System.Data.Odbc.OdbcType.VarChar;	
				else
					p.DbType = System.Data.DbType.AnsiString;
				
				p.Size = 8000;
				p.Value = sBindToken;
				cmd.Parameters.Add(p);
				cmd.ExecuteNonQuery();
				Trace.WriteLine( (cmd.Parameters[0] as DbParameter).Value);
			}
//			m_objConnLookup.Open();

		}
	}

}
