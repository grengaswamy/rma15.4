﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Riskmaster.Security;  
using Riskmaster.Common;
using Riskmaster.Db;
using System.Collections;
using System.Configuration;

namespace Riskmaster.Tools.DocToDocx
{
    public partial class frmLogin : Form
    {
        private Login m_objLogin = null;
        private UserLogin m_objUser = null;
        private string m_sDSN = string.Empty;
        private string m_sUserID = string.Empty;
        private string m_sPassword = string.Empty;
        private string m_ConnectionString = string.Empty;
        private bool m_bLogin = false;
        private bool m_bLoaded = false;
        private Label lblMessage;
        private static bool blnSuccess = false;
        private eDatabaseType m_DatabaseType = 0;
        private int m_iClientId = 0;//sonali for jira -RMACLOUD 2944
        internal string m_sPathShared = string.Empty;//nkaranam2 - To Support Shared Path

        public frmLogin(int p_iClientId)
        {
            m_iClientId = p_iClientId;
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            string[] arrDb;
            if (!m_bLoaded)
            {
                Login objLogin = new Login(m_iClientId);//sonali JIRA RMACLOUD-2944
                arrDb = objLogin.GetDatabases();
                cboDSN.DataSource = arrDb;
                objLogin.Dispose();
                objLogin = null;
                m_bLoaded = true;
                rbtnNo.Select();
                lblSharedPath.Enabled = false;
                txtSharedPath.Enabled = false;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = string.Empty;
                m_sDSN = cboDSN.Text;

                //Check for entered values
                m_sUserID = txtUserName.Text.Trim();
                m_sPassword = txtPassword.Text.Trim();
                //nkaranam2 - To Support Shared Path
                if (rbtnYes.Checked)
                    m_sPathShared = txtSharedPath.Text.Trim();
                if (m_sUserID.Length == 0)
                {
                    MessageBox.Show("Please enter a valid user name.");
                    txtUserName.Focus();
                    return;
                }
                if (m_sPassword.Length == 0)
                {
                    MessageBox.Show("Please enter a valid password.");
                    txtPassword.Focus();
                    return;
                }

                m_bLogin = true;
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            m_bLogin = false;
            this.Close();
        }

        public string UserID
        {
            get { return m_sUserID; }
        }

        public string Password
        {
            get { return m_sPassword; }
        }

        public string DSN
        {
            get { return m_sDSN; }
        }

        public bool LoginSuccess
        {
            get { return m_bLogin; }
        }

        public string LoginMessage
        {
            set { lblMessage.Text = value; }
        }

        private void cboDSN_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        public string sPathShared
        {
            get { return m_sPathShared; }
        }

        private void rbtnNo_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnYes.Checked)
            {
                lblSharedPath.Enabled = true;
                txtSharedPath.Enabled = true;
            }
            else if (rbtnNo.Checked)
            {
                lblSharedPath.Enabled = false;
                txtSharedPath.Enabled = false;
                txtSharedPath.Clear();
            }
        }

        private void rbtnYes_CheckedChanged(object sender, EventArgs e)
        {

        }

    }
}
