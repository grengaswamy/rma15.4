namespace SecDBSetUtility
{
    partial class frmSecDBSetUtility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdSecDBSet = new System.Windows.Forms.Button();
            this.txtDBUserID = new System.Windows.Forms.TextBox();
            this.txtDBUserPassword = new System.Windows.Forms.TextBox();
            this.lblDBUserName = new System.Windows.Forms.Label();
            this.lblDBPwd = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmdSecDBSet
            // 
            this.cmdSecDBSet.Location = new System.Drawing.Point(134, 158);
            this.cmdSecDBSet.Name = "cmdSecDBSet";
            this.cmdSecDBSet.Size = new System.Drawing.Size(193, 23);
            this.cmdSecDBSet.TabIndex = 0;
            this.cmdSecDBSet.Text = "Set Riskmaster Database Login";
            this.cmdSecDBSet.UseVisualStyleBackColor = true;
            this.cmdSecDBSet.Click += new System.EventHandler(this.cmdSecDBSet_Click);
            // 
            // txtDBUserID
            // 
            this.txtDBUserID.Location = new System.Drawing.Point(227, 33);
            this.txtDBUserID.Name = "txtDBUserID";
            this.txtDBUserID.Size = new System.Drawing.Size(100, 20);
            this.txtDBUserID.TabIndex = 1;
            // 
            // txtDBUserPassword
            // 
            this.txtDBUserPassword.Location = new System.Drawing.Point(227, 85);
            this.txtDBUserPassword.Name = "txtDBUserPassword";
            this.txtDBUserPassword.PasswordChar = '*';
            this.txtDBUserPassword.Size = new System.Drawing.Size(100, 20);
            this.txtDBUserPassword.TabIndex = 2;
            this.txtDBUserPassword.UseSystemPasswordChar = true;
            // 
            // lblDBUserName
            // 
            this.lblDBUserName.AutoSize = true;
            this.lblDBUserName.Location = new System.Drawing.Point(33, 40);
            this.lblDBUserName.Name = "lblDBUserName";
            this.lblDBUserName.Size = new System.Drawing.Size(167, 13);
            this.lblDBUserName.TabIndex = 3;
            this.lblDBUserName.Text = "Riskmaster Database User Name:";
            // 
            // lblDBPwd
            // 
            this.lblDBPwd.AutoSize = true;
            this.lblDBPwd.Location = new System.Drawing.Point(33, 92);
            this.lblDBPwd.Name = "lblDBPwd";
            this.lblDBPwd.Size = new System.Drawing.Size(185, 13);
            this.lblDBPwd.TabIndex = 4;
            this.lblDBPwd.Text = "Riskmaster Database User Password:";
            // 
            // frmSecDBSetUtility
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 266);
            this.Controls.Add(this.lblDBPwd);
            this.Controls.Add(this.lblDBUserName);
            this.Controls.Add(this.txtDBUserPassword);
            this.Controls.Add(this.txtDBUserID);
            this.Controls.Add(this.cmdSecDBSet);
            this.Name = "frmSecDBSetUtility";
            this.Text = "Riskmaster Security Database Login Utility";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdSecDBSet;
        private System.Windows.Forms.TextBox txtDBUserID;
        private System.Windows.Forms.TextBox txtDBUserPassword;
        private System.Windows.Forms.Label lblDBUserName;
        private System.Windows.Forms.Label lblDBPwd;
    }
}

