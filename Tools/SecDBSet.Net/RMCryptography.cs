using Riskmaster.Security.Encryption;
using System;

/// <summary>
/// Summary description for RMCryptography
/// </summary>
public class RMCryptography
{
	public RMCryptography()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// Decrypts the Riskmaster password
    /// </summary>
    /// <param name="strEncPassword">string containing the encrypted
    /// Riskmaster password</param>
    /// <returns></returns>
    public static string DecryptPassword(string strEncPassword, string strRMAuthCode)
    {
        string strUnencPassword = string.Empty;
        int intIndex = 0;

        DTGCrypt32 objCrypt = new DTGCrypt32();

        try
        {
            //Decrypt the password
            strUnencPassword = objCrypt.DecryptString(strEncPassword, strRMAuthCode);

            //Get the index of the colon character within the string
            intIndex = strUnencPassword.IndexOf(":");

            //Obtain the actual decrypted password
            strUnencPassword = strUnencPassword.Substring(intIndex + 1);
        }//try
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }//catch
        finally
        {
            //Clean up
            objCrypt = null;
        }//finally


        //return the decrypted password
        return strUnencPassword;
    }//method: DecryptPassword()


    /// <summary>
    /// Encrypts the Riskmaster password
    /// </summary>
    /// <param name="strEncPassword">string containing the clear text
    /// Riskmaster password</param>
    /// <returns></returns>
    public static string EncryptPassword(string strPassword, string strRMAuthCode)
    {
        string strEncPassword = string.Empty;

        DTGCrypt32 objCrypt = new DTGCrypt32();

        try
        {
            //Encrypt the password
            strEncPassword = objCrypt.EncryptString(strPassword, strRMAuthCode);

        }//try
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }//catch
        finally
        {
            //Clean up
            objCrypt = null;
        }//finally


        //return the encrypted password
        return strEncPassword;
    }//method: EncryptPassword()
}
