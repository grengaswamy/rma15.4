using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Security.Encryption;

namespace SecDBSetUtility
{
    public partial class frmSecDBSetUtility : Form
    {

        public frmSecDBSetUtility()
        {
            InitializeComponent();
            string strDBUserID = string.Empty;
            string strDBUserPassword = string.Empty;

            RegistryManipulation.ReadSecurityKey(out strDBUserID, out strDBUserPassword);

            //Determines if the Registry information has already been populated
            if (!strDBUserID.Equals(string.Empty))
            {
                txtDBUserID.Text = RMCryptography.DecryptString(strDBUserID);
                txtDBUserPassword.Text = RMCryptography.DecryptString(strDBUserPassword);
            } // if
        }

        

        /// <summary>
        /// Handles the OnClick event of the Set button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSecDBSet_Click(object sender, EventArgs e)
        {
            try
            {
                SetDatabaseCredentials();
                MessageBox.Show("Registry Key set successfully.", "Success", MessageBoxButtons.OK);
            }
            catch (ApplicationException ex)
            {
                MessageBox.Show(ex.Message, "Failure", MessageBoxButtons.OK);
            }//catch

        }


        /// <summary>
        /// Stores the database credentials in the Registry
        /// </summary>
        private void SetDatabaseCredentials()
        {
            try
            {

                string strDBUserID = string.Empty;
                string strDBUserPassword = string.Empty;
                string strEncDBUserID, strEncDBUserPassword;

                strDBUserID = txtDBUserID.Text.Trim();
                strDBUserPassword = txtDBUserPassword.Text.Trim();

                strEncDBUserID = RMCryptography.EncryptString(strDBUserID);
                strEncDBUserPassword = RMCryptography.EncryptString(strDBUserPassword);

                RegistryManipulation.WriteSecurityKey(strEncDBUserID, strEncDBUserPassword);
            }//try
            catch (Exception ex)
            {
                throw new ApplicationException("Registry key could not be modified", ex);
            }//
        }//method: SetDatabaseCredentials()
    }
}