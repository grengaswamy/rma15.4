﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions; //RegEX
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor;
using System.Net.Mail; 
//using Riskmaster.Application.FileStorage;
//using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Settings;
using Riskmaster.Application.MailMerge;
//using Riskmaster.Application.DocumentManagement;
using Amyuni.PDFCreator;
using Riskmaster.BusinessAdaptor.AutoFroiAcord;
using System.Xml;
using System.IO;
using Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Threading;

namespace SendMail4AutoAcord
{
    public partial class frmSendMail4AutoAcknowledgement : Form
    {

        private bool m_bDeleteTempFiles;
        private int m_iDocPathType;
        private int m_iUserId;
        private DataModelFactory m_objDataModelFactory;
        private UserLogin m_oUserLogin;
        private string m_sConnectionString;
        private string m_sDocPath;
        private string m_sSecurityConnStrig;

        public frmSendMail4AutoAcknowledgement()
        {
            InitializeComponent();
        }

        public frmSendMail4AutoAcknowledgement(UserLogin oUserLogin, DataModelFactory p_DMFactory)
        {
            InitializeComponent();

            this.m_oUserLogin = null;
            this.m_iDocPathType = 0;
            this.m_sDocPath = string.Empty;
            this.m_sConnectionString = string.Empty;
            this.m_iUserId = 0;
            this.m_sSecurityConnStrig = string.Empty;
            this.m_objDataModelFactory = null;
            this.m_bDeleteTempFiles = true;
            this.m_oUserLogin = oUserLogin;
            if (this.m_oUserLogin.DocumentPath.Length > 0)
            {
                this.m_iDocPathType = 0;
                this.m_sDocPath = this.m_oUserLogin.DocumentPath;
            }
            else
            {
                this.m_iDocPathType = this.m_oUserLogin.objRiskmasterDatabase.DocPathType;
                this.m_sDocPath = this.m_oUserLogin.objRiskmasterDatabase.GlobalDocPath;
            }
            this.m_sConnectionString = this.m_oUserLogin.objRiskmasterDatabase.ConnectionString;
            this.m_iUserId = this.m_oUserLogin.UserId;
            this.m_sSecurityConnStrig = RMConfigurationManager.GetConfigConnectionString("RMXSecurity");
            this.m_objDataModelFactory = p_DMFactory;
            if ((RMConfigurationManager.GetAppSetting("DeleteTempFiles") != null) && (RMConfigurationManager.GetAppSetting("DeleteTempFiles").ToUpper() == "FALSE"))
            {
                this.m_bDeleteTempFiles = false;
            }
        }


        public bool AttachAndMailLetter(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Riskmaster.Application.ClaimLetter.ClaimLetter objClmLtr = null;
            XmlNode objNode = null;
            MemoryStream objMemory = null;
            bool bResult;
            int iCtr = 1;
            string sNotes = "";
            string sFileNameSuffix = "";
            string sClass = "";
            string sCategory = "";
            string sType = "";
            int iRecordId = 0;
            string sClaimNo = "";
            string sTemp = "";
            string sFormName = "";
            string[] sTitle = new string[] { "", "", "", "", "", "", "", "" };
            int iPsid = 0;
            bool bMark = false;
            try
            {
                if (this.m_oUserLogin.DocumentPath.Length > 0)
                {
                    this.m_iDocPathType = 0;
                    this.m_sDocPath = this.m_oUserLogin.DocumentPath;
                }
                else
                {
                    this.m_iDocPathType = this.m_oUserLogin.objRiskmasterDatabase.DocPathType;
                    this.m_sDocPath = this.m_oUserLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objNode = p_objXmlIn.SelectSingleNode("/Template/psid");
                if (objNode.InnerText != null)
                {
                    iPsid = Conversion.ConvertStrToInteger(objNode.InnerText);
                }
                if (p_objXmlIn.SelectSingleNode("/Template/LetterType").InnerText == "ACK")
                {
                    sFileNameSuffix = "AckLetter";
                }
                else
                {
                    sFileNameSuffix = "ClosingLetter";
                }
                sNotes = p_objXmlIn.SelectSingleNode("/Template/Notes").InnerText;
                sClass = p_objXmlIn.SelectSingleNode("/Template/Class").InnerText;
                sCategory = p_objXmlIn.SelectSingleNode("/Template/Category").InnerText;
                sType = p_objXmlIn.SelectSingleNode("/Template/Type").InnerText;
                iRecordId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("/Template/RecordId").InnerText);
                sClaimNo = p_objXmlIn.SelectSingleNode("/Template/ClaimNumber").InnerText;
                sFormName = p_objXmlIn.SelectSingleNode("/Template/FormName").InnerText;
                sTitle[0] = p_objXmlIn.SelectSingleNode("/Template/Title1").InnerText;
                sTitle[1] = p_objXmlIn.SelectSingleNode("/Template/Title2").InnerText;
                sTitle[2] = p_objXmlIn.SelectSingleNode("/Template/Title3").InnerText;
                sTitle[3] = p_objXmlIn.SelectSingleNode("/Template/Title4").InnerText;
                sTitle[4] = p_objXmlIn.SelectSingleNode("/Template/Title5").InnerText;
                sTitle[5] = p_objXmlIn.SelectSingleNode("/Template/Title6").InnerText;
                sTitle[6] = p_objXmlIn.SelectSingleNode("/Template/Title7").InnerText;
                sTitle[7] = p_objXmlIn.SelectSingleNode("/Template/Title8").InnerText;
                foreach (string sTmp in sTitle)
                {
                    sTemp = sTitle[iCtr - 1];
                    if (sTemp != "")
                    {
                        bMark = true;
                    }
                    iCtr++;
                }
                objClmLtr = new Riskmaster.Application.ClaimLetter.ClaimLetter(this.m_oUserLogin);
                objClmLtr.DSN = this.m_sConnectionString;
                objClmLtr.SecureConnString = this.m_sSecurityConnStrig;
                objClmLtr.RecordId = iRecordId;
                objClmLtr.Password = p_objXmlIn.SelectSingleNode("/Template/Password").InnerText;
                objClmLtr.FileSuffix = sFileNameSuffix;
                if (bMark)
                {
                    iCtr = 1;
                    foreach (string sTmp in sTitle)
                    {
                        sTemp = sTitle[iCtr - 1];
                        if (sTemp != "")
                        {
                            this.MailClaimLetters(sTemp, p_objXmlIn.SelectSingleNode("/Template/L" + iCtr + "SecFile").InnerText, iCtr, iRecordId, sFileNameSuffix, p_objXmlIn.SelectSingleNode("/Template/Password").InnerText);
                        }
                        iCtr++;
                    }
                }
                bResult = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, p_objException.Message.ToString(), BusinessAdaptorErrorType.Error);
                bResult = false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bResult = false;
            }
            finally
            {
                objNode = null;
                objClmLtr = null;
                objMemory = null;
            }
            return bResult;
        }


        private void btnSendMail_Click(object sender, EventArgs e)
        {
            if (!(this.rdbAcknowledgement.Checked || this.rdbClosingClaim.Checked))
            {
                MessageBox.Show("Please Select the type of the Letter to be Sent", "Send Ack/Closing Letter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                string[] ClaimIds = null;
                int iClaimId = 0;
                string sResult = string.Empty;
                Claim objClaim = null;
                this.rtbOutput.Text = string.Empty;
                bool bFirstTimeCheck = true;
                if (this.txtClaimIds.Text.Trim() != string.Empty)
                {
                    ClaimIds = this.txtClaimIds.Text.Split(new char[] { ',' });
                    this.Cursor = Cursors.WaitCursor;
                    for (int iClaimCount = 0; iClaimCount < ClaimIds.Length; iClaimCount++)
                    {
                        if (!int.TryParse(ClaimIds[iClaimCount].Trim(), out iClaimId))
                        {
                            sResult = "Invalid Claim Id " + ClaimIds[iClaimCount].Trim();
                        }
                        else
                        {
                            objClaim = (Claim)this.m_objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(new int[] { iClaimId });
                            if (bFirstTimeCheck)
                            {
                                bFirstTimeCheck = false;
                                if (this.rdbAcknowledgement.Checked)
                                {
                                    if (Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT ACKLTR_TMPL_ID FROM SYS_PARMS_CLM_LTR")) == 0)
                                    {
                                        MessageBox.Show("No Template Provided for Acknowledgement", "Send Ack/Closing Letter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                        return;
                                    }
                                }
                                else if (Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT ACKLTR_TMPL_ID FROM SYS_PARMS_CLM_LTR")) == 0)
                                {
                                    MessageBox.Show("No Template Provided for Closed Claim", "Send Ack/Closing Letter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                    return;
                                }
                            }
                            sResult = this.ProcessClaim(iClaimId, this.rdbAcknowledgement.Checked ? "ACK" : "CL", objClaim.ClaimNumber);
                            this.UpdateStatus(sResult);
                        }
                    }
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Process Completed", "Send Ack/Closing Letter", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("Please Enter atleast one Claim ID", "Send Ack/Closing Letter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }



        public static string ChangeMessageValue(string xml, string nodetochange, string nodevalue)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            doc.SelectSingleNode(nodetochange).InnerText = nodevalue;
            return doc.OuterXml;
        }

 
        public bool GetClmLetter(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bResult;
            string sFormName = "";
            string sLtrType = "";
            int lRecordId = 0;
            XmlNode objNode = null;
            Riskmaster.Application.ClaimLetter.ClaimLetter objClmLtr = null;
            MergeManager objMergeManager = null;
            StringBuilder sbRowid = null;
            int iTemplateID = 0;
            XmlDocument p_objXmlTemp = null;
            XmlElement objElement = null;
            string sPassword = "";
            try
            {
                sFormName = p_objXmlIn.SelectSingleNode("/ClaimLetter/FormName").InnerText;
                lRecordId = Convert.ToInt32(p_objXmlIn.SelectSingleNode("/ClaimLetter/RecordId").InnerText);
                sLtrType = p_objXmlIn.SelectSingleNode("/ClaimLetter/LetterType").InnerText;
                objClmLtr = new Riskmaster.Application.ClaimLetter.ClaimLetter(this.m_oUserLogin);
                objClmLtr.DSN = this.m_sConnectionString;
                sPassword = objClmLtr.GetPassword(lRecordId);
                if (sPassword == "")
                {
                    return false;
                }
                iTemplateID = objClmLtr.GetTemplateID(sLtrType);
                p_objXmlTemp = new XmlDocument();
                objMergeManager = new MergeManager(this.m_oUserLogin, this.m_sSecurityConnStrig, this.m_iUserId.ToString());
                objMergeManager.DSN = this.m_sConnectionString;
                objMergeManager.UID = this.m_iUserId.ToString();
                if (this.m_sDocPath != "")
                {
                    objMergeManager.TemplatePath = this.m_sDocPath;
                }
                objMergeManager.DocStorageDSN = this.m_sDocPath;
                objMergeManager.DocPathType = this.m_iDocPathType;
                objMergeManager.SecurityDSN = this.m_sSecurityConnStrig;
                objMergeManager.MoveTo(Conversion.ConvertStrToLong(iTemplateID.ToString()));
                objNode = p_objXmlIn.SelectSingleNode("/ClaimLetter/RecordId");
                if (objNode.InnerText != null)
                {
                    objMergeManager.CurMerge.RecordId = Conversion.ConvertStrToLong(objNode.InnerText);
                }
                objMergeManager.CurMerge.FetchData();
                int icount = objMergeManager.CurMerge.GetRecordCount();
                if (icount > 1)
                {
                    objNode = p_objXmlIn.SelectSingleNode("/ClaimLetter/RowIds");
                    for (int i = 1; i <= icount; i++)
                    {
                        objElement = p_objXmlIn.CreateElement("RowId");
                        objElement.InnerText = i.ToString();
                        objNode.AppendChild(objElement);
                    }
                }
                sbRowid = new StringBuilder();
                foreach (XmlNode objTemp in objNode.SelectNodes("//RowIds/RowId"))
                {
                    sbRowid.Append(objTemp.InnerText);
                    sbRowid.Append(",");
                }
                if (sbRowid.ToString().Length > 0)
                {
                    sbRowid.Remove(sbRowid.Length - 1, 1);
                }
                if (sbRowid.Length == 0)
                {
                    sbRowid.Append("1");
                }
                objNode = p_objXmlOut.CreateElement("Template");
                p_objXmlOut.AppendChild(objNode);
                XmlElement e = p_objXmlOut.CreateElement("File");
                e.SetAttribute("Filename", GetServerUniqueFileName("mmt", "doc"));
                e.InnerText = objMergeManager.CurMerge.Template.FormFileContent;
                objNode.AppendChild(e);
                if (!objMergeManager.CurMerge.PreFab)
                {
                    e = p_objXmlOut.CreateElement("DataSource");
                    e.InnerText = objMergeManager.CurMerge.GetMergeDataSource(sbRowid.ToString());
                    objNode.AppendChild(e);
                    objClmLtr.PrepareSplitXML(e.InnerText, iTemplateID, sbRowid.ToString().Split(new char[] { ',' }).Length, lRecordId, objNode, ref p_objXmlOut);
                }
                if (!objMergeManager.CurMerge.PreFab)
                {
                    e = p_objXmlOut.CreateElement("HeaderContent");
                    e.InnerText = objMergeManager.CurMerge.Template.HeaderFileContent;
                    objNode.AppendChild(e);
                }
                if (!objMergeManager.CurMerge.PreFab)
                {
                    e = p_objXmlOut.CreateElement("NumRecords");
                    e.InnerText = p_objXmlOut.SelectSingleNode("//SplitDataNum").InnerText;
                    objNode.AppendChild(e);
                }
                e = p_objXmlOut.CreateElement("Password");
                e.InnerText = sPassword;
                objNode.AppendChild(e);
                e = p_objXmlOut.CreateElement("SecFileName");
                switch (sLtrType)
                {
                    case "ACK":
                        e.InnerText = sFormName + "_AckLetter";
                        break;

                    case "CL":
                        e.InnerText = sFormName + "_ClosedLetter";
                        break;
                }
                objNode.AppendChild(e);
                bResult = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bResult = false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bResult = false;
            }
            finally
            {
                objNode = null;
                objClmLtr = null;
            }
            return bResult;
        }

 

        public static string GetServerUniqueFileName(string sPrefix, string sExtesion)
        {
            string sUniqueFileName;
            try
            {
                Random rand = new Random();
                string sFileName = string.Empty;
                string sRand = string.Empty;
                for (int i = 0; i < 10; i++)
                {
                    sRand = rand.Next().ToString().Substring(0, 1);
                    sFileName = sFileName + sRand;
                }
                sUniqueFileName = sPrefix + sFileName + "." + sExtesion;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(p_objException.Message);
            }
            return sUniqueFileName;
        }


        public bool MailClaimLetters(string p_sTitle, string p_sLetterContent, int p_iCtr, int p_iRecordId, string p_sAttachFileSuffix, string p_sPassword)
        {
            StringBuilder sbSql = null;
            StringBuilder sbEmailIdsTo = null;
            StringBuilder sbMessage = null;
            StringBuilder sbSubject = null;
            Claim objClaim = null;
            Event objEvent = null;
            Entity objEntity = null;
            Riskmaster.Application.EmailDocuments.EmailDocuments objEmailDocuments = null;
            int iEntityId = 0;
            string sLevel = "";
            string sClmtName = "";
            string sEmailIdsTo = "";
            string sEmailFrom = "";
            bool bReturnValue = false;
            bool blnSuccess = false;
            try
            {
                objClaim = (Claim) this.m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(new int[] { p_iRecordId });
                objEvent = (Event) this.m_objDataModelFactory.GetDataModelObject("Event", false);
                objEvent.MoveTo(new int[] { objClaim.EventId });
                switch (p_iCtr)
                {
                    case 1:
                        sLevel = "DEPARTMENT_EID";
                        break;

                    case 2:
                        sLevel = "FACILITY_EID";
                        break;

                    case 3:
                        sLevel = "LOCATION_EID";
                        break;

                    case 4:
                        sLevel = "DIVISION_EID";
                        break;

                    case 5:
                        sLevel = "REGION_EID";
                        break;

                    case 6:
                        sLevel = "OPERATION_EID";
                        break;

                    case 7:
                        sLevel = "COMPANY_EID";
                        break;

                    case 8:
                        sLevel = "CLIENT_EID";
                        break;
                }
                if (sLevel != "DEPARTMENT_EID")
                {
                    sbSql = new StringBuilder();
                    sbSql.Append(string.Concat(new object[] { "SELECT ", sLevel, " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = ", objEvent.DeptEid }));
                    using (DbReader objReader = DbFactory.GetDbReader(this.m_sConnectionString, sbSql.ToString()))
                    {
                        if (objReader.Read())
                        {
                            iEntityId = objReader.GetInt(sLevel);
                        }
                    }
                }
                else
                {
                    iEntityId = objEvent.DeptEid;
                }
                sbSql = new StringBuilder();
                sbSql.Append(string.Concat(new object[] { "SELECT EMAIL_ADDRESS FROM ENT_X_CONTACTINFO EXC,CONTACT_LOB  WHERE EXC.EMAIL_ACK = -1 AND  EMAIL_ADDRESS IS NOT NULL AND EMAIL_ADDRESS <> ' ' AND EXC.ENTITY_ID = ", iEntityId, " AND EXC.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = ", objClaim.LineOfBusCode }));
                using (DbReader objReader = DbFactory.GetDbReader(this.m_sConnectionString, sbSql.ToString()))
                {
                    sbEmailIdsTo = new StringBuilder();
                    while (objReader.Read())
                    {
                        sbEmailIdsTo.Append(objReader.GetString("EMAIL_ADDRESS") + ",");
                    }
                }
                if (sbEmailIdsTo.ToString() != "")
                {
                    sEmailIdsTo = sbEmailIdsTo.ToString().Substring(0, sbEmailIdsTo.ToString().Length - 1);
                }
                if (sEmailIdsTo != "")
                {
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + objClaim.ClaimId + " ORDER BY PRIMARY_CLMNT_FLAG ASC");
                    iEntityId = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sbSql.ToString()));
                    if (iEntityId != 0)
                    {
                        objEntity = (Entity) this.m_objDataModelFactory.GetDataModelObject("Entity", false);
                        objEntity.MoveTo(new int[] { iEntityId });
                        if (objEntity.LastName != "")
                        {
                            sClmtName = (objEntity.LastName + ", " + objEntity.FirstName).Trim();
                        }
                        else
                        {
                            sClmtName = objEntity.FirstName.Trim();
                        }
                    }
                    this.WriteFile(string.Concat(new object[] { Path.GetTempPath(), objClaim.ClaimNumber, p_sAttachFileSuffix, "_", p_iCtr, ".doc" }), p_sLetterContent);
                    objEmailDocuments = new Riskmaster.Application.EmailDocuments.EmailDocuments(this.m_oUserLogin.objRiskmasterDatabase.DataSourceName, this.m_oUserLogin.LoginName, this.m_oUserLogin.Password, this.m_oUserLogin.UserId, this.m_oUserLogin.GroupId);
                    sbSubject = new StringBuilder();
                    sbMessage = new StringBuilder();
                    sbSubject.Append(objClaim.ClaimNumber);
                    sbMessage.Append("Injured party : " + sClmtName + "\n");
                    sbMessage.Append("Date of Event : " + Conversion.ToDate(objEvent.DateOfEvent).ToString("MM/dd/yyyy") + "\n\n\n");
                    sbMessage.Append("Attached you will find document related to the above captioned claim.\n\n");
                    sbMessage.Append("The attached file has been encrypted and password protected for your data security.\n\n");
                    sbMessage.Append("Password has been sent in a separate email.\n\n");
                    using (DbReader objReader = DbFactory.GetDbReader(this.m_sSecurityConnStrig, "SELECT ADMIN_EMAIL_ADDR FROM SETTINGS"))
                    {
                        if (objReader.Read())
                        {
                            sEmailFrom = objReader.GetString("ADMIN_EMAIL_ADDR");
                        }
                    }
                    if (objEmailDocuments.SendEmail(sEmailIdsTo, sbSubject.ToString(), sbMessage.ToString(), string.Concat(new object[] { Path.GetTempPath(), objClaim.ClaimNumber, p_sAttachFileSuffix, "_", p_iCtr, ".doc" }), sEmailFrom))
                    {
                        sbSubject = new StringBuilder();
                        sbMessage = new StringBuilder();
                        sbSubject.Append(objClaim.ClaimNumber + ", Password");
                        sbMessage.Append("Password of document for Claim " + objClaim.ClaimNumber + " is " + p_sPassword + ".");
                        bReturnValue = objEmailDocuments.SendEmail(sEmailIdsTo, sbSubject.ToString(), sbMessage.ToString(), "", sEmailFrom);
                    }
                    if (this.m_bDeleteTempFiles && File.Exists(string.Concat(new object[] { Path.GetTempPath(), objClaim.ClaimNumber, p_sAttachFileSuffix, "_", p_iCtr, ".doc" })))
                    {
                        File.Delete(string.Concat(new object[] { Path.GetTempPath(), objClaim.ClaimNumber, p_sAttachFileSuffix, "_", p_iCtr, ".doc" }));
                    }
                }
                blnSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (sbSql != null)
                {
                    sbSql = null;
                }
                if (sbMessage != null)
                {
                    sbMessage = null;
                }
                if (sbSubject != null)
                {
                    sbSubject = null;
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                }
                if (objEmailDocuments != null)
                {
                    objEmailDocuments = null;
                }
            }
            return blnSuccess;
        }



        private string ProcessClaim(int p_iClaimId, string p_sClmLtrType, string p_sClaimNo)
        {
            string sRTFContent = "";
            string sHeaderContent = "";
            string sDataSource = "";
            string sSecFileName = "";
            string sTempFileName = "";
            string sDataFileName = "";
            string sSecFile = "";
            string sDataSourceFileName = "";
            string sheaderFileName = "";
            string sPassword = "";
            string strform = "";
            string sSplitData1 = string.Empty;
            string sSplitData2 = string.Empty;
            string sSplitData3 = string.Empty;
            string sSplitData4 = string.Empty;
            string sSplitData5 = string.Empty;
            string sSplitData6 = string.Empty;
            string sSplitData7 = string.Empty;
            string sSplitData8 = string.Empty;
            string sSecureRTFContent1 = string.Empty;
            string sSecureRTFContent2 = string.Empty;
            string sSecureRTFContent3 = string.Empty;
            string sSecureRTFContent4 = string.Empty;
            string sSecureRTFContent5 = string.Empty;
            string sSecureRTFContent6 = string.Empty;
            string sSecureRTFContent7 = string.Empty;
            string sSecureRTFContent8 = string.Empty;
            bool bReturn = false;
            bool bLetterGenerated = false;
            XmlDocument objAckXmlIn = new XmlDocument();
            XmlDocument objAckXmlOut = new XmlDocument();
            XmlDocument objErrorOut = new XmlDocument();
            BusinessAdaptorErrors p_objErrOut = new BusinessAdaptorErrors();
            ClaimLetterAdaptor objClaimLetter = new ClaimLetterAdaptor();
            string sResult = string.Empty;
            try
            {
                string MergeMessageTemplate = "<ClaimLetter><TemplateId /> <Name /> <FormName /> <RecordId>" + p_iClaimId.ToString() + "</RecordId> <LetterType>" + p_sClmLtrType + "</LetterType> <RowIds /> </ClaimLetter>";
                objAckXmlIn.LoadXml(MergeMessageTemplate);
                bReturn = this.GetClmLetter(objAckXmlIn, ref objAckXmlOut, ref p_objErrOut);
                string sOrgEntryExist = objAckXmlOut.SelectSingleNode("//OrgEntryExist").InnerText;
                string sTitle1 = objAckXmlOut.SelectSingleNode("//Title0").InnerText;
                string sTitle2 = objAckXmlOut.SelectSingleNode("//Title1").InnerText;
                string sTitle3 = objAckXmlOut.SelectSingleNode("//Title2").InnerText;
                string sTitle4 = objAckXmlOut.SelectSingleNode("//Title3").InnerText;
                string sTitle5 = objAckXmlOut.SelectSingleNode("//Title4").InnerText;
                string sTitle6 = objAckXmlOut.SelectSingleNode("//Title5").InnerText;
                string sTitle7 = objAckXmlOut.SelectSingleNode("//Title6").InnerText;
                string sTitle8 = objAckXmlOut.SelectSingleNode("//Title7").InnerText;
                if (bReturn && !string.IsNullOrEmpty(objAckXmlOut.SelectSingleNode("//Password").InnerText))
                {
                    int iCtr;
                    sTempFileName = objAckXmlOut.SelectSingleNode("//File").Attributes["Filename"].Value;
                    sRTFContent = objAckXmlOut.SelectSingleNode("//File").InnerText;
                    sHeaderContent = objAckXmlOut.SelectSingleNode("//HeaderContent").InnerText;
                    sDataSource = objAckXmlOut.SelectSingleNode("//DataSource").InnerText;
                    sSecFileName = objAckXmlOut.SelectSingleNode("//SecFileName").InnerText;
                    string sNumRecords = objAckXmlOut.SelectSingleNode("//NumRecords").InnerText;
                    sNumRecords = Convert.ToString(1);
                    if (objAckXmlOut.SelectSingleNode("//SplitData0") != null)
                    {
                        sSplitData1 = objAckXmlOut.SelectSingleNode("//SplitData0").InnerText;
                    }
                    if (objAckXmlOut.SelectSingleNode("//SplitData1") != null)
                    {
                        sSplitData2 = objAckXmlOut.SelectSingleNode("//SplitData1").InnerText;
                    }
                    if (objAckXmlOut.SelectSingleNode("//SplitData2") != null)
                    {
                        sSplitData3 = objAckXmlOut.SelectSingleNode("//SplitData2").InnerText;
                    }
                    if (objAckXmlOut.SelectSingleNode("//SplitData3") != null)
                    {
                        sSplitData4 = objAckXmlOut.SelectSingleNode("//SplitData3").InnerText;
                    }
                    if (objAckXmlOut.SelectSingleNode("//SplitData4") != null)
                    {
                        sSplitData5 = objAckXmlOut.SelectSingleNode("//SplitData4").InnerText;
                    }
                    if (objAckXmlOut.SelectSingleNode("//SplitData5") != null)
                    {
                        sSplitData6 = objAckXmlOut.SelectSingleNode("//SplitData5").InnerText;
                    }
                    if (objAckXmlOut.SelectSingleNode("//SplitData6") != null)
                    {
                        sSplitData7 = objAckXmlOut.SelectSingleNode("//SplitData6").InnerText;
                    }
                    if (objAckXmlOut.SelectSingleNode("//SplitData7") != null)
                    {
                        sSplitData8 = objAckXmlOut.SelectSingleNode("//SplitData7").InnerText;
                    }
                    sPassword = objAckXmlOut.SelectSingleNode("//Password").InnerText;
                    if (sPassword == string.Empty)
                    {
                        return "Email Claim Letter Password is not set at any level of Organization Hierarchy. No Claim Letters generated.";
                    }
                    strform = Path.GetTempPath() + sTempFileName;
                    this.WriteFile(strform, sRTFContent);
                    sheaderFileName = strform.Substring(0, strform.Length - 4) + ".hdr";
                    this.WriteFile(sheaderFileName, sHeaderContent);
                    for (iCtr = 1; iCtr <= 8; iCtr++)
                    {
                        sDataFileName = string.Concat(new object[] { Path.GetTempPath(), sTempFileName.Replace(".doc", ""), iCtr, ".doc" });
                        sSecFile = string.Concat(new object[] { Path.GetTempPath(), sTempFileName.Replace(".doc", ""), iCtr, "Sec.doc" });
                        sDataSourceFileName = string.Concat(new object[] { Path.GetTempPath(), "DataSource", iCtr, sTempFileName });
                        if ((sSplitData1 != "") && (iCtr == 1))
                        {
                            this.WriteFile(sDataSourceFileName, sSplitData1);
                            this.WordMerge(strform, sheaderFileName, sDataSourceFileName, sDataFileName, sSecFile, sPassword);
                        }
                        if ((sSplitData2 != "") && (iCtr == 2))
                        {
                            this.WriteFile(sDataSourceFileName, sSplitData2);
                            this.WordMerge(strform, sheaderFileName, sDataSourceFileName, sDataFileName, sSecFile, sPassword);
                        }
                        if ((sSplitData3 != "") && (iCtr == 3))
                        {
                            this.WriteFile(sDataSourceFileName, sSplitData3);
                            this.WordMerge(strform, sheaderFileName, sDataSourceFileName, sDataFileName, sSecFile, sPassword);
                        }
                        if ((sSplitData4 != "") && (iCtr == 4))
                        {
                            this.WriteFile(sDataSourceFileName, sSplitData4);
                            this.WordMerge(strform, sheaderFileName, sDataSourceFileName, sDataFileName, sSecFile, sPassword);
                        }
                        if ((sSplitData5 != "") && (iCtr == 5))
                        {
                            this.WriteFile(sDataSourceFileName, sSplitData5);
                            this.WordMerge(strform, sheaderFileName, sDataSourceFileName, sDataFileName, sSecFile, sPassword);
                        }
                        if ((sSplitData6 != "") && (iCtr == 6))
                        {
                            this.WriteFile(sDataSourceFileName, sSplitData6);
                            this.WordMerge(strform, sheaderFileName, sDataSourceFileName, sDataFileName, sSecFile, sPassword);
                        }
                        if ((sSplitData7 != "") && (iCtr == 7))
                        {
                            this.WriteFile(sDataSourceFileName, sSplitData7);
                            this.WordMerge(strform, sheaderFileName, sDataSourceFileName, sDataFileName, sSecFile, sPassword);
                        }
                        if ((sSplitData8 != "") && (iCtr == 8))
                        {
                            this.WriteFile(sDataSourceFileName, sSplitData8);
                            this.WordMerge(strform, sheaderFileName, sDataSourceFileName, sDataFileName, sSecFile, sPassword);
                        }
                    }
                    Thread.Sleep(200);
                    for (iCtr = 1; iCtr <= 8; iCtr++)
                    {
                        sDataFileName = string.Concat(new object[] { Path.GetTempPath(), sTempFileName.Replace(".doc", ""), iCtr, ".doc" });
                        sSecFile = string.Concat(new object[] { Path.GetTempPath(), sTempFileName.Replace(".doc", ""), iCtr, "Sec.doc" });
                        sDataSourceFileName = string.Concat(new object[] { Path.GetTempPath(), "DataSource", iCtr, sTempFileName });
                        if ((sSplitData1 != "") && (iCtr == 1))
                        {
                            sSecureRTFContent1 = this.ReadWordFile(sSecFile, sPassword);
                            bLetterGenerated = true;
                        }
                        if ((sSplitData2 != "") && (iCtr == 2))
                        {
                            sSecureRTFContent2 = this.ReadWordFile(sSecFile, sPassword);
                            bLetterGenerated = true;
                        }
                        if ((sSplitData3 != "") && (iCtr == 3))
                        {
                            sSecureRTFContent3 = this.ReadWordFile(sSecFile, sPassword);
                            bLetterGenerated = true;
                        }
                        if ((sSplitData4 != "") && (iCtr == 4))
                        {
                            sSecureRTFContent4 = this.ReadWordFile(sSecFile, sPassword);
                            bLetterGenerated = true;
                        }
                        if ((sSplitData5 != "") && (iCtr == 5))
                        {
                            sSecureRTFContent5 = this.ReadWordFile(sSecFile, sPassword);
                            bLetterGenerated = true;
                        }
                        if ((sSplitData6 != "") && (iCtr == 6))
                        {
                            sSecureRTFContent6 = this.ReadWordFile(sSecFile, sPassword);
                            bLetterGenerated = true;
                        }
                        if ((sSplitData7 != "") && (iCtr == 7))
                        {
                            sSecureRTFContent7 = this.ReadWordFile(sSecFile, sPassword);
                            bLetterGenerated = true;
                        }
                        if ((sSplitData8 != "") && (iCtr == 8))
                        {
                            sSecureRTFContent8 = this.ReadWordFile(sSecFile, sPassword);
                            bLetterGenerated = true;
                        }
                        if (this.m_bDeleteTempFiles)
                        {
                            if (File.Exists(sDataSourceFileName))
                            {
                                File.Delete(sDataSourceFileName);
                            }
                            if (File.Exists(sDataFileName))
                            {
                                File.Delete(sDataFileName);
                            }
                            if (File.Exists(sSecFile))
                            {
                                File.Delete(sSecFile);
                            }
                        }
                    }
                    if (this.m_bDeleteTempFiles)
                    {
                        if (File.Exists(strform))
                        {
                            File.Delete(strform);
                        }
                        if (File.Exists(sheaderFileName))
                        {
                            File.Delete(sheaderFileName);
                        }
                    }
                }
                if (bLetterGenerated)
                {
                    if (sOrgEntryExist.ToUpper() == "FALSE")
                    {
                        sSecureRTFContent8 = sSecureRTFContent7 = sSecureRTFContent6 = sSecureRTFContent5 = sSecureRTFContent4 = sSecureRTFContent3 = sSecureRTFContent2 = sSecureRTFContent1;
                    }
                    string MergeMessageSaveTemplate = "<Template><L1File></L1File><L1SecFile></L1SecFile><L2File></L2File><L2SecFile></L2SecFile><L3File></L3File><L3SecFile></L3SecFile><L4File></L4File><L4SecFile></L4SecFile><L5File></L5File><L5SecFile></L5SecFile><L6File></L6File><L6SecFile></L6SecFile><L7File></L7File><L7SecFile></L7SecFile><L8File></L8File><L8SecFile></L8SecFile><LetterType/><ClaimNumber></ClaimNumber><Title1/><Title2/><Title3/><Title4/><Title5/><Title6/><Title7/><Title8/><Notes/><Class/><Type/><Comments/><RecordId/><Subject/><FormName/><psid/><Password/><Category/></Template>";
                    MergeMessageSaveTemplate = ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(ChangeMessageValue(MergeMessageSaveTemplate, "//RecordId", p_iClaimId.ToString()), "//L1File", sSecureRTFContent1), "//L2File", sSecureRTFContent2), "//L3File", sSecureRTFContent3), "//L4File", sSecureRTFContent4), "//L5File", sSecureRTFContent5), "//L6File", sSecureRTFContent6), "//L7File", sSecureRTFContent7), "//L8File", sSecureRTFContent8), "//Title1", sTitle1), "//Title2", sTitle2), "//Title3", sTitle3), "//Title4", sTitle4), "//Title5", sTitle5), "//Title6", sTitle6), "//Title7", sTitle7), "//Title8", sTitle8), "//L1SecFile", sSecureRTFContent1), "//L2SecFile", sSecureRTFContent2), "//L3SecFile", sSecureRTFContent3), "//L4SecFile", sSecureRTFContent4), "//L5SecFile", sSecureRTFContent5), "//L6SecFile", sSecureRTFContent6), "//L7SecFile", sSecureRTFContent7), "//L8SecFile", sSecureRTFContent8), "//LetterType", p_sClmLtrType), "//Notes", ""), "//Class", "0"), "//Category", "0"), "//Type", "0"), "//Comments", ""), "//RecordId", p_iClaimId.ToString()), "//Subject", ""), "//ClaimNumber", p_sClaimNo), "//FormName", "CLAIM"), "//Password", sPassword), "//psid", "0");
                    XmlDocument objXmlSaveAckIn = new XmlDocument();
                    XmlDocument objXmlSaveAckOut = new XmlDocument();
                    objXmlSaveAckIn.LoadXml(MergeMessageSaveTemplate);
                    bReturn = this.AttachAndMailLetter(objXmlSaveAckIn, ref objXmlSaveAckOut, ref p_objErrOut);
                    sResult = ((p_sClmLtrType == "ACK") ? "Acknowledgment" : "Closing Claim") + " Letter Successfully Sent for Claim Id " + p_iClaimId.ToString();
                }
                else
                {
                    sResult = "No Letter Generated, No data exists for any of the level of the Org Hierarchy";
                }
                return sResult;
            }
            catch (Exception ex)
            {
                return (("Error Occured While Generating/Sending Letter for Claim Id " + p_iClaimId.ToString()) + "<b> Execption is " + ex.Message + "</b>");
            }
        }



        private string ReadWordFile(string path, string sPassword)
        {
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] fileData = new byte[fs.Length];
            fs.Read(fileData, 0, Convert.ToInt32(fs.Length));
            fs.Close();
            return Convert.ToBase64String(fileData);
        }


        private void UpdateStatus(string sMessage)
        {
            this.rtbOutput.Text = this.rtbOutput.Text + sMessage + Environment.NewLine;
        }


        public void WordMerge(string strform, string sheaderFileName, string sDataSourceFileName, string sDataFileName, string sSecFile, string sPassword)
        {
            object fileName = strform;
            object objdataFileName = sDataFileName;
            object pause = false;
            object objsSecFile = sSecFile;
            object objPassword = sPassword;
            object misValue = Missing.Value;
            Microsoft.Office.Interop.Word.Application objWordApp = new ApplicationClass();
            Document objWordDoc = null;
            objWordDoc = objWordApp.Documents.Open(ref fileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
            objWordApp.ActiveDocument.SaveAs(ref fileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
            objWordDoc.Select();
            objWordDoc.MailMerge.OpenHeaderSource(sheaderFileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
            objWordDoc.MailMerge.OpenDataSource(sDataSourceFileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
            objWordDoc.MailMerge.DataSource.FirstRecord = 1;
            objWordDoc.MailMerge.DataSource.LastRecord = 1;
            objWordDoc.MailMerge.Destination = WdMailMergeDestination.wdSendToNewDocument;
            objWordDoc.MailMerge.Execute(ref pause);
            objWordDoc.Close(ref misValue, ref misValue, ref misValue);
            objWordApp.ActiveDocument.Select();
            objWordApp.ActiveDocument.SaveAs(ref objdataFileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
            objWordApp.ActiveDocument.SaveAs(ref objsSecFile, ref misValue, ref misValue, ref objPassword, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
            objWordApp.Quit(ref misValue, ref misValue, ref misValue);
            objWordDoc = null;
            objWordApp = null;
        }


        private void WriteFile(string FilePath, string FileContent)
        {
            byte[] bytes;
            bytes = bytes = Convert.FromBase64String(FileContent);
            using (FileStream fs = new FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (new BinaryWriter(fs))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
        }

    }
    
}
