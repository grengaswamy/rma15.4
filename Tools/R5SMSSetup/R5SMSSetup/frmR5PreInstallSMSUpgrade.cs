﻿using System;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Tools.ChecksumManager;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Configuration;

namespace R5SMSSetup
{

    /// <summary>
    /// This utility is a pre-requisite for setting up SMS for a new R5 client. It will generate Insert scripts 
    /// for updation of client security database. This can also be used for generating checksum's for the 3 security
    /// tables USER_TABLE , DATA_SOURCE_TABLE and USER_DETAILS_TABLE
    /// After running these SQL scripts R5 session can be established hence R5 SMS User Interface would start working fine
    /// </summary>
    /// <remarks>
    /// Created By: Raman Bhatia
    /// </remarks>
    public partial class frmR5PreInstallSMSUpgrade : Form
    {
        
        const string SQL_SERVER_FILTER = "SQL Server";
        const string SQL_NATIVE_CLIENT_FILTER = "SQL Native Client";
        const string ORACLE_FILTER = "Oracle in";
        const string LICENSE_COUNT = "1";

        #region // PUBLIC PROPERTIES
        enum DATABASE_DRIVERS
        {
            SQL_Server = 1,
            Oracle = 4
        }


        internal int DBTYPE
        {
            get;
            set;
        }//property: STATUS

        #endregion // PUBLIC PROPERTIES

        public frmR5PreInstallSMSUpgrade()
        {
            InitializeComponent();
        }
        private void frmR5PreInstallSMSUpgrade_Load(object sender, EventArgs e)
        {
            this.DBTYPE = (int)DATABASE_DRIVERS.SQL_Server; //Default to SQL Server
            cmbODBCDrivers.DataSource = GetODBCDrivers();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            string sMode = System.Configuration.ConfigurationSettings.AppSettings["r5mode"]; 
                        // add null check here
            bool bMode=false;
            Boolean.TryParse(sMode , out bMode);
            if (bMode)
            {
                // rsolanki2: its a R5 databases with all audit fields. 
                // generate the  SQL statements with the DT stamp and audit details included
                this.Text = "R5 SMS setup (R5 db Mode)";
                if (ribbonTabUser.Checked)
                {
                    txtCheckSum.Text = CalculateChecksums.CalculateUserCheckSum(txtLastName.Text, txtFirstName.Text, txtUserId1.Text);

                    lblInsertQuery.Text = SMSRepository.InsertUser(txtUserId1.Text, txtLastName.Text, txtFirstName.Text, txtCheckSum.Text);
                    lblUpdateQuery.Text = SMSRepository.UpdateUser(txtUserId1.Text, txtLastName.Text, txtFirstName.Text, txtCheckSum.Text);
                }
                else if (ribbonTabDSN.Checked) //Data Source Table CheckSum needs to be generated
                {

                    txtCheckSum.Text = CalculateChecksums.CalculateDataSourceCheckSum(txtDbUserId.Text, txtDbPwd.Text,
                        txtDSN.Text, txtDSNId.Text, DBTYPE);
                    txtCRC.Text = CalculateChecksums.CalculateCRC(txtDSNId.Text, LICENSE_COUNT);

                    //Insert Query
                    string sConnectionString = GetDatabaseConnectionString();

                    lblInsertQuery.Text = SMSRepository.InsertDSN(txtDSN.Text, txtDSNId.Text,
                    txtDbUserId.Text, txtDbPwd.Text, DBTYPE.ToString(), sConnectionString, txtCheckSum.Text, txtCRC.Text);
                    lblUpdateQuery.Text = SMSRepository.UpdateDSN(txtDSN.Text, txtDSNId.Text,
                    txtDbUserId.Text, txtDbPwd.Text, DBTYPE.ToString(), sConnectionString, txtCheckSum.Text, txtCRC.Text);
                }
                else if (ribbonTabUserDetails.Checked) //User Details Table Checksum needs to be generated
                {
                    txtCheckSum.Text = CalculateChecksums.CalculateUserDetailsCheckSum(txtUDLoginName.Text,
                        txtUDPwd.Text, txtUDUserID.Text, txtUDDSNId.Text);


                    lblInsertQuery.Text = SMSRepository.InsertUserDetails(txtUDDSNId.Text, txtUDUserID.Text,
                        txtUDLoginName.Text, txtUDPwd.Text, txtCheckSum.Text);
                    lblUpdateQuery.Text = SMSRepository.UpdateUserDetails(txtUDDSNId.Text, txtUDUserID.Text,
                        txtUDLoginName.Text, txtUDPwd.Text, txtCheckSum.Text);
                }
            }
            else
            {
                // rsolanki2: its a Rmworld/RMX r3 databases without audit fields. 
                // generate the  SQL statements with the DT stamp and audit details excluded
                this.Text = "R5 SMS setup (RmWorld db Mode)";
                if (ribbonTabUser.Checked)
                {

                    txtCheckSum.Text = CalculateChecksums.CalculateUserCheckSum(txtLastName.Text, txtFirstName.Text, txtUserId1.Text);

                    lblInsertQuery.Text = SMSRepository.InsertUserLegacy(txtUserId1.Text, txtLastName.Text, txtFirstName.Text, txtCheckSum.Text);
                    lblUpdateQuery.Text = SMSRepository.UpdateUserLegacy(txtUserId1.Text, txtLastName.Text, txtFirstName.Text, txtCheckSum.Text);
                }
                else if (ribbonTabDSN.Checked) //Data Source Table CheckSum needs to be generated
                {


                    txtCheckSum.Text = CalculateChecksums.CalculateDataSourceCheckSum(txtDbUserId.Text, txtDbPwd.Text,
                        txtDSN.Text, txtDSNId.Text, DBTYPE);
                    txtCRC.Text = CalculateChecksums.CalculateCRC(txtDSNId.Text, LICENSE_COUNT);

                    //Insert Query
                    string sConnectionString = GetDatabaseConnectionString();

                    lblInsertQuery.Text = SMSRepository.InsertDSNLegacy(txtDSN.Text, txtDSNId.Text,
                    txtDbUserId.Text, txtDbPwd.Text, DBTYPE.ToString(), sConnectionString, txtCheckSum.Text, txtCRC.Text);
                    lblUpdateQuery.Text = SMSRepository.UpdateDSNLegacy(txtDSN.Text, txtDSNId.Text,
                    txtDbUserId.Text, txtDbPwd.Text, DBTYPE.ToString(), sConnectionString, txtCheckSum.Text, txtCRC.Text);


                }
                else if (ribbonTabUserDetails.Checked) //User Details Table Checksum needs to be generated
                {


                    txtCheckSum.Text = CalculateChecksums.CalculateUserDetailsCheckSum(txtUDLoginName.Text,
                        txtUDPwd.Text, txtUDUserID.Text, txtUDDSNId.Text);


                    lblInsertQuery.Text = SMSRepository.InsertUserDetailsLegacy(txtUDDSNId.Text, txtUDUserID.Text,
                        txtUDLoginName.Text, txtUDPwd.Text, txtCheckSum.Text);
                    lblUpdateQuery.Text = SMSRepository.UpdateUserDetailsLegacy(txtUDDSNId.Text, txtUDUserID.Text,
                        txtUDLoginName.Text, txtUDPwd.Text, txtCheckSum.Text);
                }
            }

            //MessageBox.Show(sMode);

            
        }

        private string GetDatabaseConnectionString()
        {
            string sConnectionString = string.Empty;
            if (DBTYPE == (int)DATABASE_DRIVERS.SQL_Server)
            {
                sConnectionString = String.Format("Driver={{{0}}};Server={1};Database={2};", cmbODBCDrivers.SelectedValue, txtDBServer.Text, txtDbName.Text);
            }
            else
            {
                sConnectionString = String.Format("Driver={{{0}}};Server={1};DBQ={1};", cmbODBCDrivers.SelectedValue.ToString(), txtDBServer.Text);
            }
            return sConnectionString;
        }


        /// <summary>
        /// Gets a list of the ODBC Drivers on the system
        /// for SQL Server and Oracle
        /// </summary>
        /// <returns>string array containing all of the currently installed SQL Server and Oracle drivers</returns>
        private string[] GetODBCDrivers()
        {
        
            List<string> arrFilteredDrivers = new List<string>();
            const string ODBC_DRIVERS = @"SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers";
            

            RegistryKey regODBCKey = Registry.LocalMachine.OpenSubKey(ODBC_DRIVERS);
            string[] arrODBCDrivers = regODBCKey.GetValueNames();

            foreach (string strODBCDriver in arrODBCDrivers)
            {
                if (strODBCDriver.Contains(SQL_SERVER_FILTER) || strODBCDriver.Contains(SQL_NATIVE_CLIENT_FILTER) || strODBCDriver.Contains(ORACLE_FILTER))
                {
                    arrFilteredDrivers.Add(strODBCDriver);
                } // if

            }//foreach

            return arrFilteredDrivers.ToArray();
        }//method: GetODBCDrivers()

        /// <summary>
        /// Gets the specified database type based on the selected Database Driver
        /// </summary>
        /// <param name="strSelectedDbDriver">string containing the driver name</param>
        /// <returns>integer containing the required database type value</returns>
        private int GetDatabaseType(string strSelectedDbDriver)
        {
            int DB_TYPE = 0;

            if (strSelectedDbDriver.Contains(SQL_SERVER_FILTER) || strSelectedDbDriver.Contains(SQL_NATIVE_CLIENT_FILTER))
            {
                DB_TYPE = (int)DATABASE_DRIVERS.SQL_Server;
            } // if
            else if (strSelectedDbDriver.Contains(ORACLE_FILTER))
            {
                DB_TYPE = (int)DATABASE_DRIVERS.Oracle;
            } // else if


            return DB_TYPE;
        }

        private void cmbODBCDrivers_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCheckSum.Text = string.Empty;
            txtDbUserId.Text = string.Empty;
            txtDbPwd.Text = string.Empty;
            txtDbUserId.ReadOnly = false;
            txtDbPwd.ReadOnly = false;
            lblDBUserID.Text = "Enter Db User Id:";
            lblDBPwd.Text = "Enter Db Password:";
            txtUDPwd.ReadOnly = false;
            if (GetDatabaseType(cmbODBCDrivers.SelectedValue.ToString()) == (int)DATABASE_DRIVERS.SQL_Server)
            {
                this.DBTYPE = (int)DATABASE_DRIVERS.SQL_Server;
                lblDBServerName.Text = "Db Server Name/IP";
                lblDbName.Visible = true;
                txtDbName.Visible = true;
            }
            else
            {
                this.DBTYPE = (int)DATABASE_DRIVERS.Oracle;
                lblDbName.Visible = false;
                txtDbName.Visible = false;
                lblDBServerName.Text = "TNS Service Name";
            }
        }

        /// <summary>
        /// Handles copying all of the data in the Insert textbox
        /// to the Clipboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCopyToClipboardInsert_Click(object sender, EventArgs e)
        {
            lblInsertQuery.SelectAll();
            lblInsertQuery.Copy();
        }

        /// <summary>
        /// Handles copying all of the data in the Update textbox
        /// to the Clipboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCopyToClipboardUpdate_Click(object sender, EventArgs e)
        {
            lblUpdateQuery.SelectAll();
            lblUpdateQuery.Copy();
        }

       

        


    }

}

