﻿using System;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Tools.ChecksumManager
{
    public class CalculateChecksums
    {
        internal static ushort STATUS
        {
            get
            {
                return 0;
            }
        }//property: STATUS

   
        public static int LANGUAGE_CODE
        {
            get
            {
                return 1033;
            }
        }//property: LANGUAGE_CODE

        public static int MANAGER_ID
        {
            get
            {
                return 0;
            }
        }//property: MANAGER_ID

        #region // CONVERSIONMETHODS

        public static string ToDbDate(DateTime date)
        {
            if (date == DateTime.MinValue)
                return string.Empty;
            return date.ToString("yyyyMMdd");
        }


        public static byte GetAscii(string s)
        {
            if (s == "" || s == null)
                return 0;
            return System.Text.Encoding.ASCII.GetBytes(s.ToCharArray(0, 1), 0, 1)[0];
        }


        public static byte[] GetByteArr(string s)
        {
            Byte[] ret = { 0 };
            if (s == "" || s == null)
                return ret;
            return System.Text.Encoding.ASCII.GetBytes(s);
        }


        public static byte[] GetByteArr(int iSrc)
        {
            byte[] byteArr = new byte[4];
            ushort[] wordArr = new ushort[2];

            GetHILOWord(iSrc, out wordArr[0], out wordArr[1]);
            GetHILOByte(wordArr[0], out byteArr[0], out byteArr[1]);
            GetHILOByte(wordArr[1], out byteArr[2], out byteArr[3]);
            return byteArr;
        }


        public static void GetHILOWord(int Src, out ushort iLO, out ushort iHI)
        {
            unchecked
            {
                iLO = (ushort)(Src & 0x0000FFFF);
                iHI = (ushort)((Src >> 16) & 0x0000FFFF);
            }
        }


        public static void GetHILOByte(ushort Src, out byte iLO, out byte iHI)
        {
            unchecked
            {
                iLO = (byte)(Src & 0x00FF);
                iHI = (byte)((Src >> 8) & 0x00FF);
            }
        }


        public static string ToDbDateTime(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "00000000000000";
            return date.ToString("yyyyMMddHHmmss");
        }


        #endregion // CONVERSIONMETHODS

        /// <summary>
        /// Checksum method specifically required for InstallShield
        /// since Deferred and Commit Execution custom actions
        /// can only receive a single paramter of [CustomActionData]
        /// </summary>
        /// <param name="strUserId"></param>
        /// <param name="strUserPwd"></param>
        /// <param name="strDSN"></param>
        /// <param name="strDSNId"></param>
        /// <param name="intDBType"></param>
        /// <returns></returns>
        /// <remarks>InstallShield does not support overloaded methods in Managed Code Custom Action assemblies</remarks>
        public static string CalculateISDataSourceCheckSum(string strCustomActionData)
        {
            string[] strParams = strCustomActionData.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            return CalculateDataSourceCheckSum(strParams[0], strParams[1], strParams[2], strParams[3], Convert.ToInt32(strParams[4]));
        } // method: CalculateISDataSourceCheckSum

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string CalculateUserCheckSum(string strLastName, string strFirstName, string strUserId)
        {
            byte[] byteArr = new byte[24];
            ushort iHI, iLO;

            byteArr[0] = GetAscii(strLastName);
            byteArr[1] = GetAscii(strFirstName);
            byteArr[2] = GetAscii("");
            byteArr[3] = GetAscii("");
            byteArr[4] = GetAscii("");
            byteArr[5] = GetAscii("");
            byteArr[6] = GetAscii("");
            byteArr[7] = GetAscii("");
            byteArr[8] = GetAscii("");
            byteArr[9] = GetAscii("");
            byteArr[10] = GetAscii("");
            byteArr[11] = GetAscii("");

            // Get Manager ID
            GetHILOWord(MANAGER_ID, out iLO, out iHI);
            GetHILOByte(iLO, out byteArr[12], out byteArr[13]);
            GetHILOByte(iHI, out byteArr[14], out byteArr[15]);

            // Get USER ID
            GetHILOWord(Convert.ToInt32(strUserId), out iLO, out iHI);
            GetHILOByte(iLO, out byteArr[16], out byteArr[17]);
            GetHILOByte(iHI, out byteArr[18], out byteArr[19]);

            // And NLS Code
            GetHILOWord(LANGUAGE_CODE, out iLO, out iHI);
            GetHILOByte(iLO, out byteArr[20], out byteArr[21]);
            GetHILOByte(iHI, out byteArr[22], out byteArr[23]);

            return RMCryptography.ComputeHashAsString(byteArr);            
        } // method: CalculateUserCheckSum

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string CalculateUserDetailsCheckSum(string strLoginName, string strLoginPwd,
            string strUserId, string strDSNId)
        {
            byte[] byteArr = null;
            byte[] tmpArr = null;
            int cBytes = 0;
            string sStringBuff = "";
            ushort iHI, iLO;

            // Fill er up
            sStringBuff = strLoginName + ToDbDate(System.DateTime.MinValue);

            if (ToDbDate(System.DateTime.MinValue).Length == 0)
                sStringBuff += "";
            else
                sStringBuff += "000000";

            sStringBuff = sStringBuff +
                strLoginPwd +
                ToDbDate(System.DateTime.MinValue) +
                "" +
                "" +
                "" +
                "" +
                "" +
                "" +
                "" +
                "" +
                "" +
                "" +
                "" +
                "" +
                "" +
                "";
            tmpArr = GetByteArr(sStringBuff);
            cBytes = tmpArr.Length;
            byteArr = new byte[tmpArr.Length + 8];
            tmpArr.CopyTo(byteArr, 0);

            GetHILOWord(Convert.ToInt32(strDSNId), out iLO, out iHI);
            GetHILOByte(iLO, out byteArr[cBytes], out byteArr[cBytes + 1]);
            GetHILOByte(iHI, out byteArr[cBytes + 2], out byteArr[cBytes + 3]);

            GetHILOWord(Convert.ToInt32(strUserId), out iLO, out iHI);
            GetHILOByte(iLO, out byteArr[cBytes + 4], out byteArr[cBytes + 5]);
            GetHILOByte(iHI, out byteArr[cBytes + 6], out byteArr[cBytes + 7]);

            return RMCryptography.ComputeHashAsString(byteArr);
        } // method: CalculateUserDetailsCheckSum

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string CalculateDataSourceCheckSum(string strUserId, string strUserPwd,
            string strDSN, string strDSNId, int intDBType)
        {
            byte[] byteArr = new byte[1 + strUserId.Length + strUserPwd.Length + 10];
            int cBytes = 1;

            byteArr[0] = GetAscii(strDSN);
            GetByteArr(strUserId + strUserPwd).CopyTo(byteArr, 1);
            cBytes = cBytes + strUserId.Length + strUserPwd.Length;

            GetByteArr(Convert.ToInt32(strDSNId)).CopyTo(byteArr, cBytes);
            GetByteArr(intDBType).CopyTo(byteArr, cBytes + 4);
            GetHILOByte(Convert.ToUInt16(STATUS), out byteArr[cBytes + 8], out byteArr[cBytes + 9]);

            return RMCryptography.ComputeHashAsString(byteArr);
        } // method: CalculateDataSourceCheckSum

        /// <summary>
        /// Gets the CRC for a specific DSN ID
        /// </summary>
        /// <param name="strDSNId">string containing the DSN ID</param>
        /// <returns>string containing the computed CRC for the DSN ID</returns>
        public static string CalculateCRC(string strDSNId)
        {
            const string LICENSE_COUNT = "-1";

            return CalculateCRC(strDSNId, LICENSE_COUNT);
        } // method: CalculateCRC
            

        /// <summary>
        /// Gets the CRC for a specific DSN ID and a specified number of licenses
        /// </summary>
        /// <param name="strDSNId">string containing the DSN ID</param>
        /// <param name="strNumLicenses">string containing the required number of licenses for the DSN ID</param>
        /// <returns>string containing the computed CRC for the DSN ID</returns>
        public static string CalculateCRC(string strDSNId, string strNumLicenses)
        {

            //CRC check
            byte[] buff = null;
            string sLicUpdDate = CalculateChecksums.ToDbDate(DateTime.Now);

            buff = GetByteArr(strDSNId + strNumLicenses + sLicUpdDate);
            return RMCryptography.ComputeHashAsString(buff);
        } // method: CalculateCRC

        /// <summary>
        /// Gets an encrypted string
        /// </summary>
        /// <param name="strUnencrypted">string containing unencrypted information</param>
        /// <returns>encrypted string</returns>
        public static string GetEncryptedString(string strUnencrypted)
        {
            return RMCryptography.EncryptString(strUnencrypted);
        } // method: GetEncryptedString
    }
}
