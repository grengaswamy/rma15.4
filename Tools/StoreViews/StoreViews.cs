using System;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class App
	{
		static string m_ViewPath="";
		static string m_ConnStr="";
		static UserLogin m_objUserLogin=null;
		static int m_LoadCounter=0;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if(!CheckParams(args))
				return;
			
			//Get UserLogin
			if(m_ConnStr==null || m_ConnStr=="")
			{
				Riskmaster.Security.Login objLogin = new  Login(SecurityDatabase.Dsn);
				try
				{
					objLogin.RegisterApplication(0,0,ref m_objUserLogin);
				}
				catch(Exception e)
				{
					Console.WriteLine("Login Exception:");
					Console.WriteLine(e.InnerException.Message);
				}
				if(m_objUserLogin==null)
				{
					Console.WriteLine("Authentication Failed - No forms processed");
					return;
				}else
					m_ConnStr = m_objUserLogin.objRiskmasterDatabase.ConnectionString;
			}

			//Iterate over Files
			foreach(string sFileName in System.IO.Directory.GetFiles(m_ViewPath,"*.xml"))
				InsertSystemViewDataScreen(System.IO.Path.GetFileName(sFileName));

			//Give Results
			Console.WriteLine(String.Format("StoreViews {0} files loaded.",m_LoadCounter));
		}
		static public void InsertSystemViewDataScreen(string sFormName)
		{
			string sTargetPath = m_ViewPath;
			string sFormTitle = "";
			string sFileName = sTargetPath + "\\" + sFormName; 
			XmlDocument dom = new XmlDocument();
			try{dom.Load(sFileName);}
			catch{Console.WriteLine("Skipped Parser Error on : " + sFileName);}
			try{sFormTitle = dom.SelectSingleNode("//form/@title").Value;}
			catch{sFormTitle="Unspecified";}
			using(DbConnection objConn = Riskmaster.Db.DbFactory.GetDbConnection(m_ConnStr))
			{
				objConn.Open();
				objConn.ExecuteNonQuery(String.Format("DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID=0 AND FORM_NAME='{0}'",sFormName));
				DbCommand cmd = objConn.CreateCommand();
				DbParameter p = cmd.CreateParameter();
				p.Value = dom.OuterXml;
				p.ParameterName = "XML";
				cmd.Parameters.Add(p);
				DbParameter p2 = cmd.CreateParameter();
				p2.Value = sFormTitle;
				p2.ParameterName = "CAPTION";
				cmd.Parameters.Add(p2);
				cmd.CommandText = String.Format("INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML) VALUES(0,'{0}',1,~CAPTION~,~XML~)",System.IO.Path.GetFileName(sFileName));
				cmd.ExecuteNonQuery();
				m_LoadCounter++;
				objConn.Close();
			}
			Console.WriteLine("Updated DB Version of {0}",sFormName);
		}
		static bool CheckParams(string[] args)
		{
			if(args.Length >2)
			{
				Console.WriteLine("[ERROR] Incorrect usage, please specify the full path to the folder containing view xml files to be stored.");
				Console.WriteLine("Example: storeviews c:\\home\\views");
				Console.WriteLine("Example: storeviews c:\\home\\views 'DSN=Riskmaster;UserId=sa;Pwd=;'");
				return false;
			}
			if (args.Length<1)
				m_ViewPath = "./views";
			else
				m_ViewPath = args[0];
			if(!System.IO.Directory.Exists(m_ViewPath))
			{
				Console.WriteLine("[ERROR] Invalid Path specified, please specify the full path to the folder containing view xml files to be stored.");
				Console.WriteLine("Example: storeviews c:\\home\\views");
				Console.WriteLine("Example: storeviews c:\\home\\views 'DSN=Riskmaster;UserId=sa;Pwd=;'");
				return false;
			}
			
			if(args.Length<=1)
				return true;

			m_ConnStr= args[1];
			DbConnection cn = DbFactory.GetDbConnection(m_ConnStr);
			try{cn.Open();cn.Close();}
			catch{cn = null;}
			if(cn==null)
			{
				Console.WriteLine("[ERROR] Invalid Connection String specified.  If you do not have db connection information, you may optionally leave off this parameter and you will be prompted for a known riskmaster login instead.  Otherwise, please specify the complete connection string to the database where the view xml files are to be stored.");
				Console.WriteLine("Example: storeviews c:\\home\\views");
				Console.WriteLine("Example: storeviews c:\\home\\views 'DSN=Riskmaster;UserId=sa;Pwd=;'");
				return false;
			}
			
			return true;
		}
	}
