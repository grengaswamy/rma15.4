VERSION 5.00
Begin VB.Form frmLicenseLogin 
   Caption         =   "Riskmaster X Database Upgrade"
   ClientHeight    =   4890
   ClientLeft      =   5370
   ClientTop       =   3180
   ClientWidth     =   4905
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4890
   ScaleWidth      =   4905
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnCancel 
      Caption         =   "Exit"
      Height          =   455
      Left            =   2985
      TabIndex        =   2
      Top             =   3795
      Width           =   1810
   End
   Begin VB.CommandButton btnUpgrade 
      Caption         =   "Upgrade"
      Default         =   -1  'True
      Height          =   455
      Left            =   135
      TabIndex        =   1
      Top             =   3795
      Width           =   1810
   End
   Begin VB.ListBox List1 
      Height          =   3375
      ItemData        =   "license-login.frx":0000
      Left            =   90
      List            =   "license-login.frx":0002
      MultiSelect     =   1  'Simple
      TabIndex        =   0
      Top             =   285
      Width           =   4740
   End
   Begin VB.Label Label1 
      Caption         =   "Select one or more databases to upgrade:"
      Height          =   225
      Left            =   165
      TabIndex        =   3
      Top             =   15
      Width           =   3615
   End
End
Attribute VB_Name = "frmLicenseLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public boolOK As Boolean

Private Sub btnCancel_Click()
    boolOK = False
    Me.Hide
End Sub


'---------------------------------------------------------------------------------------
' Procedure : btnUpgrade_Click
' DateTime  : 8/15/2007 20:11
' Author    : Administrator
' Purpose   : Handles the OnClick event of the Upgrade button
'---------------------------------------------------------------------------------------
'
Private Sub btnUpgrade_Click()
On Error GoTo hError

    Dim db As Integer
    Dim rs As Integer
    Dim sTmp As String
    Dim lTmp As Long
    Dim crypt As New DTGCrypt
    Dim iPos As Integer
    Dim iPos2 As Integer
    Dim sTmp2 As String
    Dim sDate As String
    Dim sDSN As String
    Dim lLicNum As Long
    Dim byteArr() As Byte
    
    'Check if the ListBox is empty
    If List1.ListCount < 1 Then
        'Display the error message
        DirectedMsgBox "No Riskmaster databases have been configured in Security Management System.  Please configure a Riskmaster database before running this upgrade.", vbCritical, "Riskmaster databases not configured."
        Exit Sub
    End If
    
    
    Call DatabaseSelections
    If garrSelectedItems.Count < 1 Then
        DirectedMsgBox "No Riskmaster databases have been selected in the list.  Please select at least one database in the list.", vbCritical, "Riskmaster databases not configured."
        Exit Sub
    End If

    Me.Hide

    Exit Sub

hError:
    Dim EResult As Integer
    EResult = iGeneralError(Err, Error$, "frmLicLog.btnUpgrade")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Sub
    End Select
End Sub

'Allow multiple databases to be selected from the ListBox
Private Sub DatabaseSelections()
    Dim intCounter As Integer
        
    'Check to verify that at least 1 item has been selected from the Listbox
    If List1.ListIndex = -1 Then
        DirectedMsgBox "Please select a database to upgrade first.", vbInformation, "Upgrade"
        Exit Sub
    End If
    
    'Loop through and determine all of the selected items in the ListBox
    For intCounter = List1.ListCount - 1 To 0 Step -1
        If List1.Selected(intCounter) = True Then
            garrSelectedItems.Add (CLng(List1.ItemData(intCounter)))
        End If
    Next intCounter
End Sub


Private Sub Form_Load()
On Error GoTo hError

    Dim db As Integer
    Dim rs As Integer
    Dim sTmp As String
    Dim lTmp As Long
    Const DB_UPGRADE_CAPTION As String = "Riskmaster X Database Upgrade for "
    
    'Set the caption for the Riskmaster X Database upgrade form
    Me.Caption = DB_UPGRADE_CAPTION & gsDBTargetVer
    
    
    db = DB_OpenDatabase(g_hEnv, g_sSecurityDBConnectionString, 0)
    
    rs = DB_CreateRecordset(db, "SELECT * FROM DATA_SOURCE_TABLE ORDER BY DSN", DB_FORWARD_ONLY, 0)
    While Not DB_EOF(rs)
        List1.AddItem vDB_GetData(rs, "DSN")
        List1.ItemData(List1.NewIndex) = lAnyVarToLong(vDB_GetData(rs, "DSNID"))
    
        DB_MoveNext rs
    Wend
    
    DB_CloseRecordset rs, DB_DROP
    
hExit:
    DB_CloseDatabase db
    Exit Sub
hError:
    Dim EResult As Integer
    EResult = iGeneralError(Err, Error$, "Display RM/World Databases")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Resume hExit
    End Select

End Sub
