;**********************************************************************************
;NON-OCCUPATIONAL CLAIMS SECURITY DATABASE CHANGES
;Contains rm security database changes for LTD enhancements to Non-Occ (formerly STD) module
;Created: 06/15/2005 KDB
;**********************************************************************************

; XHU 05/12/2005 Change function name from "Short Term Disability" to "Non-Occupational Claims"
[FORGIVE] UPDATE FUNCTION_LIST SET FUNCTION_NAME='Non-Occupational Claims' WHERE FUNC_ID=60000