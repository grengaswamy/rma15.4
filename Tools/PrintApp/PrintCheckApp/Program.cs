﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Configuration;
namespace Riskmaster.PrintCheck
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string sJobId="-1";
            string sDSN = "";
       
            if (args.Length > 0)
            {

                if (ConfigurationManager.AppSettings["Pwd"] != null && (ConfigurationManager.AppSettings["Pwd"].Trim().Equals("") || ConfigurationManager.AppSettings["Uid"].Trim().Equals("")))
                {
                    Application.Run(new frmChangeSettings());
                }
                GetArgs(args, ref sJobId, ref sDSN);
                Application.Run(new frmPrintCheck(sJobId, sDSN));
                
                
            }
           
            else

                Application.Run(new frmChangeSettings());
            
           
           
        }
        //Get Print JOB argument
        static void GetArgs(string[] args, ref string sJobId, ref string sDSN)
        {
            string sJob = args[0].Substring("alert:".Length);
            int iJobId = sJob.IndexOf("CSCDSNID=");
            sJobId = sJob.Substring(0, iJobId);
            sJobId = sJobId.Replace("CSCJOBID=", "");
            sDSN = sJob.Substring(iJobId+9);
            
        }
    }
}
