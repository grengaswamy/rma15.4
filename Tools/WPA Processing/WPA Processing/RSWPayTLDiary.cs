﻿using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System;
using System.Runtime.InteropServices;
using System.Xml;
using System.IO;
using System.Windows.Forms;
namespace WPAProcessing
{
    public class RSWPayTLDiary
    {
        //Connection Info 
        //private static string m_sConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource");
        //private static string m_sConnectionSecurity = RMConfigurationManager.GetConfigConnectionString("RMXSecurity");
        //revove after getting correct connection string
        private static string m_sConnectionRiskmaster = WPAForm.Globalvar.m_ConStrRiskmaster;

        //for Supervisory Approval flag of reserve worksheet
        public static bool m_bUseSupAppReserves;
        //for time limit in days for supervisory approval of reserve worksheet
        public static int m_iDaysAppReserves;
        //for time limit in hours for supervisory approval of reserve worksheet
        public static int m_iHoursAppReserves;
        //for specifying stepwise or jump functionality
        public static bool m_bNotifySupReserves;
        //to specify to use current adjuster supervisory approval
        public static bool m_bUseCurrAdjReserves;
        //for Supervisory Approval level
        public static short m_iLevel;
        //for claim Id
        public static int m_lClaim_ID;
        //for line of business code of the claim
        public static int m_iLOBCode;
        //for claim type of claim
        public static int m_iClmTypeCode;
        //for sending diary in case payment is placed on hold
        public static int m_iHoldSendDiary;
        //for time limit in days for supervisory approval of payments
        public static int m_iDaysForApproval;
        //for time limit in hours for supervisory approval of payments
        public static int m_iHoursForApproval;
        //store last updated date of RserveWorksheet on 12Feb2010
        public static string m_RSWLastUpdatedDate;
        //store the flag to send the email notification to supervisior for reserve worksheet'
        public static bool m_bDisableEmailNotifyForSuperVsr;
        //store the flag to send the dairy notification to supervisior for reserve worksheet'
        public static bool m_bDisableDiaryNotifyForSuperVsr;
        //rsushilaggar MITS 19624 30-Mar-2010'
        //store the flag to send the email notification to supervisior for payment'
        public static bool m_bDisableEmailNotifyForPayment;
        private static eDatabaseType m_stDataBaseType;
        // public static int g_dbMake;
        public static int lDSNID;
        //RMA-4300,RMA-6131, : systemErrors(static memeber) is used in static methods.Should not be instantiate in constactor[RSWPayTLDiary()].
        static BusinessAdaptorErrors systemErrors = new BusinessAdaptorErrors(WPAForm.Globalvar.m_iClientId);
        public WPAForm objForm = null;
        public RSWPayTLDiary()
        {
            objForm = new WPAForm();
        }
        public struct structReserves
        {
            public int iReserveTypeCode;
            public double dTotalPaid;
            public double dTotalCollected;
            public double dTotalIncurred;
            public double dBalance;
            public string sReserveDesc;
            public string sStatusCode;
        }
        public struct structResFinal
        {
            public short iReserveTypeCode;
            public double dBalAmount;
            public double dNewBalAmount;
            public double dNewReserves;
        }

        public static structReserves[] m_arrstructReserves;
        public static structResFinal[] m_arrstructResFinal;
        static int m_iArrSize;
        public static string sGetUserName(string lUserName = "", int lUserId = 0)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            DbConnection objConn = null;
            string sSenderLoginName = string.Empty;
            int userID = 0;
            if (lUserId != 0)
                userID = lUserId;
            if (!string.IsNullOrEmpty(lUserName))
                sSenderLoginName = lUserName;
            try
            {
                if (!string.IsNullOrEmpty(lUserName))
                {
                    sSQL = "SELECT LAST_NAME, FIRST_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID";
                    sSQL = sSQL + " AND USER_DETAILS_TABLE.LOGIN_NAME = '" + sSenderLoginName + "'";
                }
                else if (lUserId != 0)
                    sSQL = "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE  USER_ID = " + userID;
                //remove after getting correct connection string

                objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_sConnectionSecurity);
                objConn.Open();
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        if (!string.IsNullOrEmpty(lUserName))
                            sSenderLoginName = objReader.GetString("FIRST_NAME") + " " + objReader.GetString("LAST_NAME");
                        else
                            sSenderLoginName = objReader.GetString("LOGIN_NAME");
                    }

                }

            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("GetUserName", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn.Close();
                }
            }

            return sSenderLoginName;
        }

        public static void GenerateTimeLapseDiaries(int DSNID)
        {
            lDSNID = DSNID;
            //Check Payment Utilities setting and send diaries accordingly
            CheckPaymentTLDiaries();
            //Check Reserve Worksheet Utilities setting and send diaries accordingly
            CheckResWorksheetTLDiaries();
        }
        public static void CheckPaymentTLDiaries()
        {
            ReadCheckOptionsForPayments();
            PayAppTimeLapseDiaries();
        }

        public static void CheckResWorksheetTLDiaries()
        {

            ReadCheckOptionsForResWorksheet();
            ResWorksheetTimeLapseDiaries();
        }
        public static void ReadCheckOptionsForPayments()
        {
            m_iHoldSendDiary = 0;
            m_iDaysForApproval = 0;
            m_iHoursForApproval = 0;
            m_bDisableEmailNotifyForPayment = false;
            //sai
            UpdateStatus("Getting Utility settings for Payments: Start ");

            string sSQL = "SELECT * FROM CHECK_OPTIONS";
            DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
            objConn.Open();
            try
            {
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        m_iHoldSendDiary = objReader.GetInt("SH_DIARY");
                        m_iDaysForApproval = objReader.GetInt("DAYS_FOR_APPROVAL");
                        m_iHoursForApproval = objReader.GetInt("HOURS_FOR_APPROVAL");
                        m_bDisableEmailNotifyForPayment = objReader.GetBoolean("DISABLE_EMAIL_NOTIFY_FOR_PYMT");
                        m_bDisableEmailNotifyForSuperVsr = objReader.GetBoolean("DISABLE_EMAIL_NOTIFY_FOR_SUPV");
                        m_bDisableDiaryNotifyForSuperVsr = objReader.GetBoolean("DISABLE_DIARY_NOTIFY_FOR_SUPV");
                        m_bUseCurrAdjReserves = objReader.GetBoolean("USE_CUR_ADJ_RESERVES");
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("ReadCheckOptionsForPayments", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn.Close();
                }
            }
            UpdateStatus("Getting Utility settings for Payments: Complete ");
        }

        public static void ReadCheckOptionsForResWorksheet()
        {

            m_bUseSupAppReserves = false;
            m_iDaysAppReserves = 0;
            m_iHoursAppReserves = 0;
            m_bNotifySupReserves = false;
            m_bUseCurrAdjReserves = false;
            m_bDisableDiaryNotifyForSuperVsr = false;
            m_bDisableEmailNotifyForSuperVsr = false;
            UpdateStatus("Getting Utility settings for Reserve Worksheet: Start ");
            string sSQL = "SELECT * FROM CHECK_OPTIONS";
            DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
            objConn.Open();
            try
            {
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        m_bUseSupAppReserves = objReader.GetBoolean("USE_SUP_APP_RESERVES");
                        m_iDaysAppReserves = objReader.GetInt("DAYS_APP_RESERVES");
                        m_iHoursAppReserves = objReader.GetInt("HOURS_APP_RESERVES");
                        m_bNotifySupReserves = objReader.GetBoolean("NOTIFY_SUP_RESERVES");
                        m_bUseCurrAdjReserves = objReader.GetBoolean("USE_CUR_ADJ_RESERVES");
                        m_bDisableEmailNotifyForSuperVsr = objReader.GetBoolean("DISABLE_EMAIL_NOTIFY_FOR_SUPV");
                        m_bDisableDiaryNotifyForSuperVsr = objReader.GetBoolean("DISABLE_DIARY_NOTIFY_FOR_SUPV");
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("ReadCheckOptionsForResWorksheet", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn.Close();
                }
            }
            UpdateStatus("Getting Utility settings for Reserve Worksheet: Complete ");
        }
        public static void ResWorksheetTimeLapseDiaries()
        {

            if ((m_bUseSupAppReserves))
            {
                if (m_iDaysAppReserves > 0 || m_iHoursAppReserves > 0)
                {
                    UpdateStatus("Generating Diaries for Reserve Worksheet: Start ");
                    AssignReserveWorksheet();
                    UpdateStatus("Generating Diaries for Reserve Worksheet: Complete ");
                }
                else
                {
                    return;
                }
            }
        }
        public static void PayAppTimeLapseDiaries()
        {
            string sSQL = null;
            int iCount = 0;
            int lFundsHoldStatusCode = 0;
            System.DateTime dAssDateTime = default(System.DateTime);
            System.DateTime dTempDateTime = default(System.DateTime);
            string sInterval = null;
            int iNumber = 0;
            int iStep = 0;
            int lLobLevelUserId = 0;
            int lTopLevelUserId = 0;

            int lTransId = 0;
            string sCtlNumber = null;
            int lApproverId = 0;
            string sDiaryAssigningUser = null;
            string sDiaryAssignedUser = null;

            int lRecipientUserId = 0;
            string sRecipientLoginName = null;
            int lSenderUserId = 0;
            string sSenderLoginName = null;
            string sRegarding = null;
            string sAttachTable = null;
            int lAttachRecordID = 0;
            string sEntryName = null;
            string sDiaryNotes = null;

            string sUserName = null;
            string sFromEmail = null;
            string sToEmail = null;
            string sSubject = null;
            string sBody = null;
            bool bSendMailStatus = false;
            bool bIsMailEntry = false;
            UpdateStatus("Generating Diaries for Payments: Start ");
            //sai
            //db = ROCKETCOMLibDTGRocket_definst.DB_OpenDatabase(hEnv, sTrimNull((objLogin.SecurityDSN)), 0);
            LocalCache objCache = new LocalCache(m_sConnectionRiskmaster, WPAForm.Globalvar.m_iClientId);//psharma206
            long ltableid = objCache.GetTableId("CHECK_STATUS");
            if (ltableid > 0)
                lFundsHoldStatusCode = objCache.GetCodeId("H", "CHECK_STATUS");
            //lGetCodeIDWithShort(ROCKETCOMLibDTGRocket_definst.DB_GetHdbc(dbLookup), "H", lGetTableID("CHECK_STATUS"));

            sSQL = "SELECT * FROM PMT_APPROVAL_HIST WHERE PAH_ROW_ID IN(";
            sSQL = sSQL + " SELECT MAX(PAH_ROW_ID) FROM PMT_APPROVAL_HIST";
            sSQL = sSQL + " WHERE TRANS_ID IN (SELECT FUNDS.TRANS_ID FROM FUNDS";
            sSQL = sSQL + " WHERE FUNDS.PAYMENT_FLAG <> 0";
            sSQL = sSQL + " AND FUNDS.STATUS_CODE = " + lFundsHoldStatusCode + " AND FUNDS.VOID_FLAG = 0)";
            sSQL = sSQL + " GROUP BY TRANS_ID) ";
            sSQL = sSQL + " ORDER BY  PAH_ROW_ID DESC ";
            try
            {
                DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                objConn.Open();
                m_stDataBaseType = objConn.DatabaseType;
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {

                        lTransId = objReader.GetInt("TRANS_ID");
                        lApproverId = objReader.GetInt("APPROVER_ID");
                        string sdate = objReader.GetString("DTTM_APPROVAL_CHGD");
                        dAssDateTime = Convert.ToDateTime(sdate.Substring(0, 4) + "/" + sdate.Substring(4, 2) + "/" + sdate.Substring(6, 2) + " " + sdate.Substring(8, 2) + ":" + sdate.Substring(10, 2) + ":" + sdate.Substring(12, 2));
                        //*******************************************************************************
                        //Get the Last Diary Assigned User                        
                        if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                        {
                            sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                            sSQL = sSQL + " WHERE ATTACH_TABLE = 'FUNDS' AND ENTRY_NOTES LIKE 'The time for payment approval lapsed for%'";
                            sSQL = sSQL + " AND ATTACH_RECORDID = " + lTransId;
                            sSQL = sSQL + " ORDER BY ENTRY_ID DESC";
                        }
                        else if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            sSQL = "SELECT ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                            sSQL = sSQL + " WHERE ATTACH_TABLE = 'FUNDS' AND ENTRY_NOTES LIKE 'The time for payment approval lapsed for%'";
                            sSQL = sSQL + " And ROWNUM <= 1 AND ATTACH_RECORDID = " + lTransId;
                            sSQL = sSQL + " ORDER BY ENTRY_ID DESC";
                        }
                        else
                        {
                            sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                            sSQL = sSQL + " WHERE ATTACH_TABLE = 'FUNDS' AND ENTRY_NOTES LIKE 'The time for payment approval lapsed for%'";
                            sSQL = sSQL + " AND ATTACH_RECORDID = " + lTransId;
                            sSQL = sSQL + " ORDER BY ENTRY_ID DESC";
                        }

                        using (DbReader objReader2 = objConn.ExecuteReader(sSQL))
                        {
                            while (objReader2.Read())
                            {
                                if (objReader2.Read())
                                {
                                    sDiaryAssigningUser = objReader2.GetString("ASSIGNING_USER") + "";
                                    sDiaryAssignedUser = objReader2.GetString("ASSIGNED_USER") + "";
                                }
                                else
                                {
                                    sDiaryAssigningUser = "";
                                    sDiaryAssignedUser = "";
                                }
                            }
                        }
                        if ((m_iDaysForApproval > 0))
                        {
                            iNumber = m_iDaysForApproval;
                            sInterval = "d";
                        }
                        else
                        {
                            iNumber = m_iHoursForApproval;
                            sInterval = "h";
                        }

                        dTempDateTime = dAssDateTime;

                        if ((iNumber > 0))
                        {
                            iCount = 0;
                            while ((dTempDateTime < DateTime.Now))
                            {

                                iCount = iCount + 1;

                                //dTempDateTime = DateTime.ad DateAdd(sInterval, iNumber, dTempDateTime);
                                if (sInterval == "h")
                                    dTempDateTime = dTempDateTime.AddHours(iNumber);
                                else if (sInterval == "d")
                                    dTempDateTime = dTempDateTime.AddDays(iNumber);

                            }
                        }

                        lRecipientUserId = lApproverId;
                        lSenderUserId = lApproverId;

                        for (iStep = 1; iStep <= iCount - 1; iStep++)
                        {

                            if ((lRecipientUserId == 0))
                            {
                                break;
                            }
                            else
                            {
                                lSenderUserId = lRecipientUserId;
                            }

                            lRecipientUserId = lGetManagerID(lRecipientUserId);

                        }
                        if ((lRecipientUserId == 0))
                        {

                            sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL LSA, CLAIM C, FUNDS F";
                            sSQL = sSQL + " WHERE LSA.LOB_CODE = C.LINE_OF_BUS_CODE And C.CLAIM_ID = F.CLAIM_ID";
                            sSQL = sSQL + " AND F.TRANS_ID = " + lTransId;
                            try
                            {
                                using (DbReader objReaderUser = objConn.ExecuteReader(sSQL))
                                {
                                    if (objReaderUser.Read())
                                    {
                                        lLobLevelUserId = objReaderUser.GetInt("USER_ID");
                                        //For getting LOB level User
                                    }
                                    else
                                    {
                                        lLobLevelUserId = 0;
                                    }

                                    sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1";
                                    using (DbReader objReader2 = objConn.ExecuteReader(sSQL))
                                    {
                                        if (objReader2.Read())
                                        {
                                            lTopLevelUserId = objReader2.GetInt("USER_ID");
                                            //For getting Top level User
                                        }
                                        else
                                        {
                                            lTopLevelUserId = 0;
                                        }
                                    }
                                    if ((iStep == iCount && lLobLevelUserId != 0))
                                    {
                                        lRecipientUserId = lLobLevelUserId;
                                    }
                                    else
                                    {
                                        //Sender - LOB Level User , if exist else the Highest level manager
                                        if ((lLobLevelUserId != 0))
                                        {
                                            lSenderUserId = lLobLevelUserId;
                                            iStep = iStep + 1;
                                        }
                                        lRecipientUserId = lTopLevelUserId;
                                    }
                                }
                            }
                            catch (Exception RMA_Exc)
                            {
                            }

                        }

                        sSenderLoginName = Global.sGetUser((lDSNID), lSenderUserId);
                        sRecipientLoginName = Global.sGetUser((lDSNID), lRecipientUserId);

                        if ((lSenderUserId != lRecipientUserId && lSenderUserId != lTopLevelUserId))
                        {

                            sSQL = "SELECT CTL_NUMBER FROM FUNDS WHERE TRANS_ID = " + lTransId;
                            using (DbReader objReader3 = objConn.ExecuteReader(sSQL))
                            {
                                if (objReader3.Read())
                                {
                                    sCtlNumber = objReader3.GetString("CTL_NUMBER") + "";
                                }
                                else
                                {
                                    sCtlNumber = "";
                                }
                            }
                            if (!string.IsNullOrEmpty(sSenderLoginName))
                                sUserName = sGetUserName(sSenderLoginName);
                            //Get Sender First Name and Last Name

                            //Create Diary                            
                            if ((m_iHoldSendDiary > 0))
                            {
                                if ((sDiaryAssigningUser != sSenderLoginName || sDiaryAssignedUser != sRecipientLoginName))
                                {

                                    sRegarding = "Payments: " + sCtlNumber;
                                    sAttachTable = "FUNDS";
                                    lAttachRecordID = lTransId;
                                    sEntryName = "Check On Hold";
                                    sDiaryNotes = "The time for payment approval lapsed for " + sUserName + ". ";
                                    sDiaryNotes = sDiaryNotes + "The payment has been submitted for your approval.";
                                    //Mits 33843
                                    int parent = WPAForm.ParentRecord(sAttachTable, lAttachRecordID, WPAForm.Globalvar.m_iClientId);
                                    GenerateDiary(sSenderLoginName, sRecipientLoginName, sRegarding, sAttachTable, parent, lAttachRecordID, sEntryName, sDiaryNotes);

                                }
                            }
                            //***************************************************************************************************

                            //Send Email
                            //***************************************************************************************************
                            //sai
                            sFromEmail = MAPIVB.sGetEmailByLoginName(sSenderLoginName);
                            sToEmail = MAPIVB.sGetEmailByLoginName(sRecipientLoginName);



                            if ((!string.IsNullOrEmpty(sFromEmail) && !string.IsNullOrEmpty(sToEmail) && m_bDisableEmailNotifyForPayment != true))
                            {

                                // dAssDateTime = DateTime.a DateAdd(sInterval, iNumber * (iStep - 1), dAssDateTime);
                                sSQL = "SELECT ENTRY_ID FROM WPA_EMAIL_ENTRY";
                                sSQL = sSQL + " WHERE ATTACH_REC_ID = '" + sCtlNumber + "'";
                                sSQL = sSQL + " AND MAIL_REGARDING = 'Approval Request for Payment: " + sCtlNumber + "'";
                                sSQL = sSQL + " AND SENDER_ID = '" + sSenderLoginName + "'";
                                sSQL = sSQL + " AND RECEIVER_ID = '" + sRecipientLoginName + "'";
                                using (DbReader objReader4 = objConn.ExecuteReader(sSQL))
                                {
                                    if (objReader4.Read())
                                    {
                                        bIsMailEntry = false;
                                    }
                                    else
                                        bIsMailEntry = true;
                                }


                                //if (bIsMailEntry)
                                //    dAssDateTime = DateTime.Now.Add(sInterval, iNumber * (iStep - 1), dAssDateTime);

                                sSubject = "Approval Request for Payment: " + sCtlNumber;

                                sBody = "A Payment: " + sCtlNumber + " has been submitted for your approval on " + dAssDateTime + " by " + sUserName + "." + Constants.VBCrLf;
                                sBody = sBody + "Please enter the RISKMASTER system and navigate to Approve Funds Transactions.";
                                sBody = sBody + " There you will find this and other Requests awaiting your approval." + Constants.VBCrLf + Constants.VBCrLf;
                                sBody = sBody + "Reason: The time for payment approval lapsed for " + sUserName + ". " + Constants.VBCrLf + Constants.VBCrLf;
                                sBody = sBody + "Thank you," + Constants.VBCrLf + "RISKMASTER X";

                                //bSendEmail(sUserName, sFromEmail, sToEmail, sSubject, sBody);
                                bSendMailStatus = bSendEmail(sUserName, sFromEmail, sToEmail, sSubject, sBody);
                                bool IsEntryMade = false;
                                if (bSendMailStatus)
                                    IsEntryMade = EmailEntry(sCtlNumber, sSubject, sSenderLoginName, sRecipientLoginName, WPAForm.Globalvar.m_iClientId);

                            }
                        }
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("ReadCheckOptionsForResWorksheet", null, false, systemErrors);
            }
            finally
            {

            }
            UpdateStatus("Generating Diaries for Payments: Complete ");
        }
        public static void GenerateDiary(string sSenderLoginName, string sRecipientLoginName, string sRegarding, string sAttachTable, int sParentRecord, int lAttachRecordID, string sEntryName, string sDiaryNotes)
        {
            string sSQL = null;
            DbConnection objConn = null;
            try
            {

                if ((!string.IsNullOrEmpty(sRecipientLoginName)))
                {
                    //Mits 33843 - Adding a new parameter att_parent_code
                    sSQL = "INSERT INTO WPA_DIARY_ENTRY (ENTRY_ID,ENTRY_NAME,ENTRY_NOTES,CREATE_DATE,PRIORITY,";
                    sSQL = sSQL + " STATUS_OPEN,AUTO_CONFIRM,ASSIGNED_USER, ASSIGNING_USER, ASSIGNED_GROUP,IS_ATTACHED,ATTACH_TABLE,ATT_PARENT_CODE,";
                    sSQL = sSQL + " ATT_FORM_CODE,ATTACH_RECORDID,REGARDING,ATT_SEC_REC_ID,COMPLETE_TIME,COMPLETE_DATE,";
                    sSQL = sSQL + " DIARY_VOID, DIARY_DELETED,NOTIFY_FLAG,ROUTE_FLAG,ESTIMATE_TIME,AUTO_ID,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,ADDED_BY_USER,UPDATED_BY_USER) "; //dvatsa - added columns for JIRA 12160
                    //sSQL = sSQL + " VALUES(" + lGetNextUID("WPA_DIARY_ENTRY") + ",'" + sEntryName.Replace( "'", "''") + "','";               
                    sSQL = sSQL + " VALUES(" + Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_DIARY_ENTRY", WPAForm.Globalvar.m_iClientId) + ",'" + sEntryName.Replace("'", "''") + "','";
                    sSQL = sSQL + " VALUES(" + ",'" + sEntryName.Replace("'", "''") + "','";
                    sSQL = sSQL + sDiaryNotes.Replace("'", "''") + "','" + DateTime.Now.ToString("yyyyMMddHHmmss") + "',1,1,0,'" + sRecipientLoginName.Replace("'", "''") + "','";

                    sSQL = sSQL + sSenderLoginName.Replace("'", "''") + "','',1,'" + sAttachTable + "," + sParentRecord + "',3192,'" + lAttachRecordID + "','" + sRegarding.Replace("'", "''");
                    sSQL = sSQL + "',0,'235959','" + DateTime.Now.ToString("yyyyMMdd") + "',0,0,0,0,0,0,'" + DateTime.Now.ToString("yyyyMMddHHmmss") + "','" + DateTime.Now.ToString("yyyyMMddHHmmss") + "' , '" + sRecipientLoginName.Replace("'", "''") + "', '" + sRecipientLoginName.Replace("'", "''") + "')"; //dvatsa- added column values for JIRA 12160
                    objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);

                    objConn.Open();
                    objConn.ExecuteNonQuery(sSQL);

                }
            }
            catch (Exception p_objEx)
            {

            }
            finally
            {
                if (objConn != null)
                    objConn.Dispose();
            }
        }


        public static void AssignReserveWorksheet()
        {


            int lPendingApprovalCode = 0;

            string dtLastUpdatedRecord = null;
            LocalCache objCache = new LocalCache(m_sConnectionRiskmaster, WPAForm.Globalvar.m_iClientId); //psharma206
            int TableID = objCache.GetTableId("RSW_STATUS");
            if (TableID > 0)
                lPendingApprovalCode = objCache.GetCodeId("PA", "RSW_STATUS");
            //Check if supervisory approval for reserve worksheet is enabled

            string sDate = null;
            if ((m_bUseSupAppReserves))
            {
                //Check if there is any valid data in "Days for Approval",
                //if the data is not valid then this function is not required
                if ((m_iDaysAppReserves > 0 | m_iHoursAppReserves > 0))
                {
                    DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                    objConn.Open();
                    string sSQL = "SELECT * FROM RSW_WORKSHEETS WHERE RSW_STATUS_CODE = " + Convert.ToString(lPendingApprovalCode);
                    using (DbReader objReader = objConn.ExecuteReader(sSQL))
                    {
                        while (objReader.Read())
                        {
                            //Assign each reserve worksheets to proper user
                            m_iLevel = 1;
                            m_lClaim_ID = objReader.GetInt("CLAIM_ID");
                            DbReader objReader2 = objConn.ExecuteReader("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + Convert.ToString(m_lClaim_ID));
                            if (objReader2.Read())
                            {
                                m_iLOBCode = objReader2.GetInt("LINE_OF_BUS_CODE");
                                m_iClmTypeCode = objReader2.GetInt("CLAIM_TYPE_CODE");
                            }
                            sDate = objReader.GetString("DTTM_RCD_LAST_UPD");
                            dtLastUpdatedRecord = sDate.Substring(0, 4) + "-" + sDate.Substring(4, 2) + "-" + sDate.Substring(6, 2) + " " + sDate.Substring(8, 2) + ":" + sDate.Substring(10, 2) + ":" + sDate.Substring(12, 2);
                            GetResCategories(m_iLOBCode, m_iClmTypeCode);
                            GetNewReserveAmts(m_iLOBCode, objReader.GetString("RSW_XML").ToString());

                            if ((m_bNotifySupReserves))
                            {
                                StepWiseUpdateRSWForUser(objReader.GetInt("RSW_ROW_ID"), objReader.GetString("SUBMITTED_BY"), objReader.GetString("SUBMITTED_TO"), dtLastUpdatedRecord, m_iLevel, objReader.GetString("RSW_XML"));//psharma206
                            }
                            else
                            {
                                JumpUpdateRSWForUser(objReader.GetInt("RSW_ROW_ID"), objReader.GetString("SUBMITTED_BY"), objReader.GetString("SUBMITTED_TO"), dtLastUpdatedRecord, m_iLevel, objReader.GetString("RSW_XML"));//psharma206
                            }
                        }
                    }
                }
                else
                {
                    return;
                }
            }
        }
        public static void GetNewReserveAmts(int p_iLOBCode, string p_sXMLDoc)
        {


            int iCnt = 0;
            int iReserveTypeCode = 0;
            iReserveTypeCode = 0;
            string sReserveDesc = string.Empty;
            string sGPName = string.Empty;
            string sLOB = string.Empty;
            string sCtrlMName = string.Empty;
            //MSXML2.DOMDocument60 objDOMDocument = default(MSXML2.DOMDocument60);
            XmlDocument objDOMDocument = new XmlDocument();
            Riskmaster.Settings.CCacheFunctions ObjShortcode = new Riskmaster.Settings.CCacheFunctions(m_sConnectionRiskmaster, WPAForm.Globalvar.m_iClientId);//cloud change
            //replaced sGetShortCode method with GetShortCode
            sLOB = ObjShortcode.GetShortCode(Convert.ToInt32(p_iLOBCode));

            m_arrstructResFinal = new structResFinal[m_iArrSize + 1];
            objDOMDocument.LoadXml(p_sXMLDoc);

            for (iCnt = 0; iCnt <= (m_iArrSize - 1); iCnt++)
            {
                iReserveTypeCode = m_arrstructReserves[iCnt].iReserveTypeCode;
                sReserveDesc = m_arrstructReserves[iCnt].sReserveDesc.Trim();
                sGPName = "//group[@name='" + sReserveDesc + sLOB + "']";

                if ((objDOMDocument.SelectNodes(sGPName).Count > 0))
                {
                    if ((objDOMDocument.SelectSingleNode(sGPName).HasChildNodes))
                    {
                        sCtrlMName = sReserveDesc.Substring(1, 3) + sLOB;

                        m_arrstructResFinal[iCnt].iReserveTypeCode = Convert.ToInt16(Convert.ToString(iReserveTypeCode));

                        if ((!string.IsNullOrEmpty(objDOMDocument.SelectSingleNode("//control[@name='BalAmt" + sCtrlMName + "']").InnerText)))
                        {
                            m_arrstructResFinal[iCnt].dBalAmount = Convert.ToDouble(objDOMDocument.SelectSingleNode("//control[@name='BalAmt" + sCtrlMName + "']").InnerText);
                        }

                        if ((!string.IsNullOrEmpty(objDOMDocument.SelectSingleNode("//control[@name='NewBalAmt" + sCtrlMName + "']").InnerText)))
                        {
                            m_arrstructResFinal[iCnt].dNewBalAmount = Convert.ToDouble(objDOMDocument.SelectSingleNode("//control[@name='NewBalAmt" + sCtrlMName + "']").InnerText);
                        }

                        if ((!string.IsNullOrEmpty(objDOMDocument.SelectSingleNode("//control[@name='hdnNewRes" + sCtrlMName + "']").InnerText)))
                        {
                            m_arrstructResFinal[iCnt].dNewReserves = Convert.ToDouble(objDOMDocument.SelectSingleNode("//control[@name='hdnNewRes" + sCtrlMName + "']").InnerText);
                        }

                    }
                }
            }
        }
        public static void JumpUpdateRSWForUser(int p_iRSWID, string p_sSubmittedBy, string p_sSubmittedTo, string p_dSubmittedOn, int p_iLevel, string p_sXMLDoc)
        {
            string sDs = string.Empty;
            string sDSN = string.Empty;
            int iInterval = 1;
            string sSQL = string.Empty;
            string sSupervisor = string.Empty;
            int iTopID = 0;
            int iLOBManagerID = 0;
            XmlDocument objDOMDocument = new XmlDocument();
            string sRegarding = string.Empty;
            string sPCName = string.Empty;
            string sClaimNum = string.Empty;
            string sRSWType = string.Empty;
            string sSenderLoginName = string.Empty;
            string sRecipientLoginName = string.Empty;
            string sFromEmail = string.Empty;
            string sToEmail = string.Empty;
            string sAttachTable = string.Empty;
            int lAttachRecordID = 0;
            string sEntryName = string.Empty;
            string sDiaryAssigningUser = string.Empty;
            string sDiaryAssignedUser = string.Empty;
            string sUserName = string.Empty;
            string sDiaryNotes = string.Empty;

            int iAdjID = 0;
            DbReader objReader = null;
            DbConnection objConn = null;
            //sDS.Value = objLogin.SecurityDSN;
            ////Security database DSN
            //sDSN = sTrimNull(sDS.Value);
            //db = ROCKETCOMLibDTGRocket_definst.DB_OpenDatabase(hEnv, sDSN, 0);

            //iDays = p_iLevel * m_iDaysAppReserves
            if ((m_iDaysAppReserves > 0))
            {
                iInterval = p_iLevel * m_iDaysAppReserves;
            }
            else if ((m_iHoursAppReserves > 0))
            {
                iInterval = p_iLevel * m_iHoursAppReserves;
            }
            else
            {
                iInterval = 0;
            }


            if ((Convert.ToDateTime(AddIntervalToSubmitDate(p_dSubmittedOn, iInterval)) < DateTime.Now))
            {
                //Supervisor within reserve limits
                iLOBManagerID = GetLOBManagerID(m_iLOBCode);

                sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1";
                objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_sConnectionSecurity);
                objConn.Open();
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        iTopID = objReader.GetInt("USER_ID");
                    }
                }
                // rs1 = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(dbGlobalConnect2, sSQL, ROCKETCOMLib.DTGDBGeneral.DB_READ_ONLY, 0);
                //if (!ROCKETCOMLibDTGRocket_definst.DB_EOF(rs1))
                //{
                //    //UPGRADE_WARNING: Couldn't resolve default property of object vDB_GetData(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                //    iTopID = vDB_GetData(rs1, "USER_ID");
                //}
                //result = ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs1, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP);

                // sSupervisor = Convert.ToString(lGetManagerID( Convert.ToInt32(p_sSubmittedTo)));
                sSupervisor = Convert.ToString(lGetManagerID(Convert.ToInt32(p_sSubmittedTo)));

                sSupervisor = Convert.ToString(GetApprovalID(p_sXMLDoc, Convert.ToInt32(sSupervisor), m_lClaim_ID, Convert.ToInt32(p_sSubmittedBy), iGetGroupID(Convert.ToInt32(sSupervisor))));

                if ((Convert.ToInt32(sSupervisor) == Convert.ToInt32(p_sSubmittedTo)))
                {
                    if (((Convert.ToInt32(p_sSubmittedTo) == iLOBManagerID) && (Convert.ToInt32(p_sSubmittedTo) != iTopID)))
                    {
                        p_sSubmittedTo = Convert.ToString(iTopID);
                        p_sSubmittedBy = Convert.ToString(iLOBManagerID);

                        sSenderLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedBy));
                        sRecipientLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedTo));
                    }
                    else if (((Convert.ToInt16(p_sSubmittedTo) == iLOBManagerID) && (Convert.ToInt16(p_sSubmittedTo) == iTopID)))
                    {
                        return;
                    }
                }
                else
                {
                    if ((sSupervisor != "0"))
                    {
                        p_sSubmittedBy = p_sSubmittedTo;
                        JumpUpdateRSWForUser(p_iRSWID, p_sSubmittedBy, sSupervisor, p_dSubmittedOn, p_iLevel, p_sXMLDoc);
                    }
                }
                //worksheet has not lapsed for this user: assign it to this user
            }
            else
            {
                //Send Diary/Mail to p_sSubmittedTo from p_sSubmittedBy
                sSenderLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedBy));
                sRecipientLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedTo));
            }

            string sDiaryAdj = string.Empty;
            string sAdjLoginName = string.Empty;
            string sAdjEmail = string.Empty;
            if ((Convert.ToInt32(p_sSubmittedBy) != Convert.ToInt32(p_sSubmittedTo)))
            {
                if ((!string.IsNullOrEmpty(sSenderLoginName) && !string.IsNullOrEmpty(sRecipientLoginName)))
                {
                    objDOMDocument.LoadXml(p_sXMLDoc);

                    if ((!string.IsNullOrEmpty(objDOMDocument.SelectSingleNode("//control[@name='hdnPCName']").InnerText)))
                    {
                        sPCName = objDOMDocument.SelectSingleNode("//control[@name='hdnPCName']").InnerText;
                    }

                    if ((!string.IsNullOrEmpty(objDOMDocument.SelectSingleNode("//control[@name='hdnRSWType']").InnerText)))
                    {
                        sRSWType = objDOMDocument.SelectSingleNode("//control[@name='hdnRSWType']").InnerText;
                    }

                    if ((!string.IsNullOrEmpty(objDOMDocument.SelectSingleNode("//control[@name='hdnClaimNumber']").InnerText)))
                    {
                        sClaimNum = objDOMDocument.SelectSingleNode("//control[@name='hdnClaimNumber']").InnerText;
                    }

                    sRegarding = sClaimNum + " * " + sPCName;
                    sAttachTable = "CLAIM";
                    lAttachRecordID = Convert.ToInt32(m_lClaim_ID);
                    sUserName = RSWPayTLDiary.sGetUserName(sSenderLoginName);
                    //Get Sender First Name and Last Name
                    sEntryName = "Approval request for Claim " + sClaimNum + " – " + sPCName + " - Time Lapsed for " + sUserName;
                    sDiaryNotes = GetEmailFormat("RSWSheet", m_lClaim_ID);
                    sDiaryAdj = sDiaryNotes;

                    if (Convert.ToDouble(sRSWType) == 0)
                    {
                        sRSWType = "";
                    }
                    sDiaryAdj = sDiaryAdj.Replace("{RSW_TYPE}", sRSWType);
                    sDiaryAdj = sDiaryAdj.Replace("{CLAIM_NUMBER}", sClaimNum);
                    sDiaryAdj = sDiaryAdj.Replace("{USER_NAME}", sPCName);
                    sDiaryAdj = sDiaryAdj.Replace("{REASON}", "The time for Reserve Worksheet approval has lapsed for  & sGetUserName(sSenderLoginName) & ");
                    sDiaryAdj = sDiaryAdj.Replace("{SUBMITTED_TO}", RSWPayTLDiary.sGetUserName(sRecipientLoginName));
                    sDiaryAdj = sDiaryAdj.Replace("{SUBMITTED_BY}", RSWPayTLDiary.sGetUserName(sSenderLoginName));
                    sDiaryNotes = sDiaryNotes.Replace("{RSW_TYPE}", sRSWType);
                    sDiaryNotes = sDiaryNotes.Replace("{CLAIM_NUMBER}", sClaimNum);
                    sDiaryNotes = sDiaryNotes.Replace("{USER_NAME}", sPCName);
                    sDiaryNotes = sDiaryNotes.Replace("{REASON}", "The time for Reserve Worksheet approval has lapsed for & sUserName & ");

                    sDiaryNotes = sDiaryNotes.Replace("{SUBMITTED_TO}", sGetUserName(sRecipientLoginName));

                    sDiaryNotes = sDiaryNotes.Replace("{SUBMITTED_BY}", sGetUserName(sSenderLoginName));

                    //Get the Last Diary Assigned User


                    if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                        sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + sDiaryNotes.Trim() + " %'";
                        sSQL = sSQL + " AND ATTACH_RECORDID = " + Convert.ToInt32(m_lClaim_ID);
                        sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + Convert.ToString(p_iRSWID) + ")";
                        sSQL = sSQL + " AND ASSIGNED_USER = '" + sRecipientLoginName.Trim() + "' AND ASSIGNING_USER = '" + sSenderLoginName.Trim() + "'";
                        sSQL = sSQL + " ORDER BY ENTRY_ID DESC";
                    }
                    else if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                    {
                        sSQL = "SELECT ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                        sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + sDiaryNotes.Trim() + " %'";
                        sSQL = sSQL + " AND ATTACH_RECORDID = " + Convert.ToInt32(m_lClaim_ID);
                        sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + Convert.ToString(p_iRSWID) + ")";
                        sSQL = sSQL + " AND ASSIGNED_USER = '" + sRecipientLoginName.Trim() + "' AND ASSIGNING_USER = '" + sSenderLoginName.Trim() + "'";
                        sSQL = sSQL + " AND ROWNUM <= 1 ORDER BY ENTRY_ID DESC";
                    }
                    else
                    {
                        sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                        sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + sDiaryNotes.Trim() + " %'";
                        sSQL = sSQL + " AND ATTACH_RECORDID = " + Convert.ToInt32(m_lClaim_ID);
                        sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + Convert.ToString(p_iRSWID) + ")";
                        sSQL = sSQL + " AND ASSIGNED_USER = '" + sRecipientLoginName.Trim() + "' AND ASSIGNING_USER = '" + sSenderLoginName.Trim() + "'";
                        sSQL = sSQL + " ORDER BY ENTRY_ID DESC";
                    }

                    using (objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (objReader.Read())
                        {
                            sDiaryAssigningUser = objReader.GetString("ASSIGNING_USER") + "";
                            sDiaryAssignedUser = objReader.GetString("ASSIGNED_USER") + "";
                        }
                        else
                        {
                            sDiaryAssigningUser = "";
                            sDiaryAssignedUser = "";
                        }
                    }
                    // result = ROCKETCOMLibDTGRocket_definst.DB_CloseRecordset(rs1, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP);
                    if (objReader != null)
                        objReader.Dispose();
                    sDiaryNotes = "A " + sRSWType + " for Claim " + sClaimNum + " – " + sPCName + " has been submitted for your approval." + Constants.VBCrLf;
                    sDiaryNotes = sDiaryNotes + Constants.VBCrLf + "Reason: The time for Reserve Worksheet approval has lapsed for " + sUserName + ". " + Constants.VBCrLf;
                    sDiaryNotes = sDiaryNotes + Constants.VBCrLf + "Thank you," + Constants.VBCrLf + "RISKMASTER X";

                    sFromEmail = MAPIVB.sGetEmailByLoginName(sSenderLoginName);
                    sToEmail = MAPIVB.sGetEmailByLoginName(sRecipientLoginName);

                    if ((sDiaryAssigningUser != sSenderLoginName | sDiaryAssignedUser != sRecipientLoginName))
                    {
                        //rsushilaggar MITS 19624 30-Mar-2010'
                        if ((m_bDisableDiaryNotifyForSuperVsr != true))
                        {
                            int parent = WPAForm.ParentRecord(sAttachTable, lAttachRecordID, WPAForm.Globalvar.m_iClientId);
                            GenerateDiary(sSenderLoginName, sRecipientLoginName, sRegarding, sAttachTable, parent, lAttachRecordID, sEntryName, sDiaryNotes);
                        }
                        if ((!string.IsNullOrEmpty(sFromEmail.Trim()) && !string.IsNullOrEmpty(sToEmail.Trim()) && m_bDisableEmailNotifyForSuperVsr != true))
                        {
                            bSendEmail(sUserName, sFromEmail, sToEmail, sEntryName, sDiaryNotes);
                        }
                        //End If Comment by kuladeep for mits:28665

                        //Sending Diary and Email to Adjuster
                        iAdjID = iGetAdjusterEID(m_lClaim_ID);
                        if (((iAdjID > 0) && (Convert.ToInt16(p_sSubmittedTo) != iAdjID)))
                        {
                            sAdjLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(iAdjID));
                            sAdjEmail = MAPIVB.sGetEmailByLoginName(sAdjLoginName);

                            sDiaryAdj = "A " + sRSWType + " for Claim " + sClaimNum + " – " + sPCName + " has been submitted to " + sGetUserName(sRecipientLoginName) + " for approval by " + sGetUserName(sSenderLoginName) + Constants.VBCrLf;
                            sDiaryAdj = sDiaryAdj + Constants.VBCrLf + "Please enter the RISKMASTER system and navigate to the Claim in order to see this reserve worksheet" + Constants.VBCrLf;
                            sDiaryAdj = sDiaryAdj + Constants.VBCrLf + "Reason: The time for Reserve Worksheet approval has lapsed for " + sGetUserName(sSenderLoginName) + ". " + Constants.VBCrLf;
                            sDiaryAdj = sDiaryAdj + Constants.VBCrLf + "Thank you," + Constants.VBCrLf + Constants.VBCrLf + "RISKMASTER X";

                            if ((m_bDisableDiaryNotifyForSuperVsr != true))
                            {
                                int parent = WPAForm.ParentRecord(sAttachTable, lAttachRecordID, WPAForm.Globalvar.m_iClientId);
                                GenerateDiary(sSenderLoginName, sAdjLoginName, sRegarding, sAttachTable, parent, lAttachRecordID, sEntryName, sDiaryAdj);
                            }
                            if ((!string.IsNullOrEmpty(sFromEmail.Trim()) && !string.IsNullOrEmpty(sAdjEmail.Trim()) && m_bDisableEmailNotifyForSuperVsr != true))
                            {
                                bSendEmail(sUserName, sFromEmail, sAdjEmail, sEntryName, sDiaryAdj);
                            }
                        }
                    }
                }
            }
        }
        //public static string sGetEmailByLoginName(string sUser)
        //{
        //    DbReader objReader = null;
        //    DbConnection objConn = null;          
        //    string sTmp = string.Empty;
        //    string sEmailAddr = null;                      
        //    string sSQL = null;
        //     objConn = DbFactory.GetDbConnection(m_sConnectionSecurity);
        //        objConn.Open();
        //        sSQL = "SELECT DISTINCT EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID =USER_TABLE.USER_ID AND  USER_DETAILS_TABLE.LOGIN_NAME = '" + sUser + "'";
        //        try
        //        {
        //            using (objReader = objConn.ExecuteReader(sSQL))
        //            {
        //                if (objReader.Read())
        //                {
        //                    sEmailAddr = objReader.GetString(0) + "";
        //                }
        //                else
        //                {
        //                    sEmailAddr = "";
        //                }
        //            }
        //        }
        //        catch (Exception p_objEx)
        //        {                    
        //            systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
        //            WPAProcessing.logErrors("GetEmailByLoginName", null, false, systemErrors);
        //        }
        //        finally
        //        {
        //            if (objConn != null)
        //            {
        //                if (objConn.State == System.Data.ConnectionState.Open)
        //                {
        //                    objConn.Close();
        //                    objConn.Dispose();
        //                }
        //                if (objReader != null)
        //                {
        //                    objReader.Close();
        //                    objReader.Dispose();
        //                }
        //            }
        //        }
        //    // Query       
        //    return sEmailAddr;       
        //}
        public static bool bSendEmail(string sSenderName, string sSenderEmail, string sRecipientTo, string sSubject, string sBody)
        {

            bool functionReturnValue = false;
            System.Net.Mail.SmtpClient objSMTP = null;
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();

            int lReturn = 0;
            functionReturnValue = false;
            objSMTP = new System.Net.Mail.SmtpClient();
            // mail = new System.Net.Mail.MailMessage();
            string sErrString = string.Empty;
            bool bValidated = true;

            try
            {
                //UPGRADE_WARNING: Couldn't resolve default property of object objSMTP.AddRecipient. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                if ((!string.IsNullOrEmpty(sRecipientTo)))
                    mail.To.Add(sRecipientTo);
                if ((string.IsNullOrEmpty(sRecipientTo)))
                {
                    // subHourGlass(false);
                    // MsgBox("Recipient does not have a valid Email Address.", MsgBoxStyle.Information);
                    // subHourGlass(true);
                    sErrString = "-> User does not have a valid Email Address.";
                    bValidated = false;
                }

                // Connect to security DB for fetching SMTP server.
                //db = ROCKETCOMLibDTGRocket_definst.DB_OpenDatabase(hEnv, sTrimNull((objLogin.SecurityDSN)), 0);
                //rs = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(db, "SELECT SMTP_SERVER FROM SETTINGS WHERE ID = 1", ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0);
                //UPGRADE_WARNING: Couldn't resolve default property of object objSMTP.MailServer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                //UPGRADE_WARNING: Couldn't resolve default property of object sAnyVarToString(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                //changes done by pk
                //objSMTP.MailServer = Trim(sAnyVarToString(vDB_GetData(rs, "SMTP_SERVER")))
                DbConnection objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_sConnectionSecurity);
                objConn.Open();
                string sSQL = "SELECT SMTP_SERVER FROM SETTINGS WHERE ID = 1";
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                        objSMTP.Host = objReader.GetString("SMTP_SERVER").Trim();

                }
                objConn.Close();

                if ((string.IsNullOrEmpty(objSMTP.Host)))
                {
                    // subHourGlass(false);
                    // Interaction.MsgBox("Please specify Mail Server using Security Management System.", MsgBoxStyle.Information);
                    //subHourGlass(true);
                    sErrString = sErrString + " -> Please specify valid Admin Email Address using Security Management System.";
                    bValidated = false;

                }
                if (!bValidated)
                {
                    sErrString = "Export To Email Failed. " + sErrString;
                    return bValidated;
                }
                //UPGRADE_WARNING: Couldn't resolve default property of object objSMTP.From. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'

                //changes done by pk
                //mailAdd = New System.Net.Mail.MailAddress
                //UPGRADE_WARNING: Couldn't resolve default property of object objSMTP.FromAddr. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                // objSMTP. = sSenderEmail
                mail.From = new System.Net.Mail.MailAddress(sSenderEmail);

                //UPGRADE_WARNING: Couldn't resolve default property of object objSMTP.Subject. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                //objSMTP.Subject = sSubject
                mail.Subject = sSubject;
                //UPGRADE_WARNING: Couldn't resolve default property of object objSMTP.BodyText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                //objSMTP.BodyText = sBody
                mail.Body = sBody;
                //UPGRADE_WARNING: Couldn't resolve default property of object objSMTP.LicenseKey. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                //objSMTP.LicenseKey = "Dorn Technology Group, Inc./30034206149719039930"
                //UPGRADE_WARNING: Couldn't resolve default property of object objSMTP.Send. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                //lReturn = objSMTP.Send 'If return value is 0 that means successful
                objSMTP.Send(mail);

                if (lReturn != 0)
                {
                    // Interaction.MsgBox("Error in Sending Mail.", MsgBoxStyle.Information);
                    sErrString = "Error in Sending Mail.";
                }
                else
                {
                    functionReturnValue = true;
                }
            }
            //catch (RMAppException p_objEx)
            //{
            //    systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
            //    WPAProcessing.logErrors("RSWPayTLDiary\bSendMail", null, false, systemErrors);
            //}
            catch (Exception p_objEx)
            {
                //  throw new RMAppException(Globalization.GetString("RSWPayTLDiary.bSendEmail"),
                //p_objException);
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("RSWPayTLDiary\bSendMail", null, false, systemErrors);
            }

            return functionReturnValue;
        }
        public static string GetEmailFormat(string p_sModuleName, int p_iClaimID)
        {

            string sSQL = string.Empty;
            string sAppPath = string.Empty;
            string sTextFormat = string.Empty;
            string[] X = null;
            int i = 0;
            string sTempPath = string.Empty;
            string[] sep = { "/", "/" };
            DbConnection objConn = null;
            DbReader objReader = null;
            // sAppPath = Replace(App.Path, "legacybin", "appfiles\")
            //  sAppPath = My.Application.Info.DirectoryPath;
            //UPGRADE_WARNING: Couldn't resolve default property of object x. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            X = sAppPath.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            for (i = 0; i <= X.GetUpperBound(i); i++)
            {
                //UPGRADE_WARNING: Couldn't resolve default property of object x(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTempPath = X[i];
                if (i == X.GetUpperBound(i))
                {
                    //  sAppPath = My.Application.Info.DirectoryPath, sTempPath, "appfiles\\");
                }
            }
            sAppPath = sAppPath + p_sModuleName + "\\";

            string sAdd = string.Empty;
            string sClaimantName = string.Empty;
            try
            {
                if ((p_sModuleName == "RSWSheet"))
                {
                    sAppPath = sAppPath + "RSWSUB_EMAIL.txt";
                    //sTextFormat = FileText(sAppPath);
                    if (p_iClaimID > 0)
                    {

                        sSQL = "SELECT ENTITY.LAST_NAME  LN,ENTITY.FIRST_NAME  FN,CLAIM_NUMBER,CLAIM.EVENT_NUMBER  EN,CLAIM.FILE_NUMBER  FNO,CLAIM.DATE_OF_CLAIM  DOC,CLAIM.TIME_OF_CLAIM  TOC,";
                        sSQL = sSQL + " CLAIM.SERVICE_CODE SC ,EVENT.COUNTY_OF_INJURY  COJ,EVENT.ADDR1  ADD1,EVENT.ADDR2  ADD2,EVENT.ADDR3  ADD3,EVENT.ADDR4  ADD4, EVENT.CITY  CITY,EVENT.LOCATION_AREA_DESC  LAD,";
                        sSQL = sSQL + " EVENT.DATE_OF_EVENT   DOE ,EVENT.TIME_OF_EVENT  TOE,EVENT.DATE_REPORTED   DR,";
                        sSQL = sSQL + " EVENT.INJURY_FROM_DATE   IFD,EVENT.INJURY_TO_DATE  ITD,";
                        sSQL = sSQL + " LOB.CODE_DESC  LIOFB,CT.CODE_DESC  CLAIMTYPE,ET.CODE_DESC  EVENTTYPE";
                        sSQL = sSQL + " from CLAIM inner join EVENT on claim.event_id=event.event_id";
                        sSQL = sSQL + " INNER JOIN CLAIMANT on CLAIM.CLAIM_ID=CLAIMANT.CLAIM_ID";
                        sSQL = sSQL + " inner join ENTITY on CLAIMANT.CLAIMANT_EID=ENTITY.ENTITY_ID";
                        sSQL = sSQL + " INNER JOIN CODES_TEXT  LOB ON LOB.CODE_ID=CLAIM.LINE_OF_BUS_CODE";
                        sSQL = sSQL + " left outer join CODES_TEXT   CT on CLAIM.CLAIM_TYPE_CODE =CT.CODE_ID";
                        sSQL = sSQL + " left outer join CODES_TEXT  ET on EVENT.EVENT_TYPE_CODE=ET.CODE_ID where CLAIM.CLAIM_ID=" + p_iClaimID;

                        //  rs = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(dbGlobalConnect2, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0);
                        objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                        objConn.Open();
                        using (objReader = objConn.ExecuteReader(sSQL))
                        {
                            if (objReader.Read())
                            {
                                //LOB = CStr(vDB_GetData(rs, "LIOFB"))
                                // sAdd = sAnyVarToString(vDB_GetData(rs, "ADD1")) + "   " + sAnyVarToString(vDB_GetData(rs, "ADD2")) + "   " + sAnyVarToString(vDB_GetData(rs, "CITY"));
                                sAdd = objReader.GetString("ADD1") + "   " + objReader.GetString("ADD2") + "   " + objReader.GetString("ADD3") + "   " + objReader.GetString("ADD4") + "   " + objReader.GetString("CITY");
                                sClaimantName = objReader.GetString("LN") + "   " + objReader.GetString("FN");
                                sTextFormat = sTextFormat.Replace("{CLAIMANT_NAME}", sClaimantName);
                                sTextFormat = sTextFormat.Replace("{CLAIM_NUMBER}", objReader.GetString("CLAIM_NUMBER"));
                                sTextFormat = sTextFormat.Replace("{EVENT_NUMBER}", objReader.GetString("EN"));
                                sTextFormat = sTextFormat.Replace("{DATE_OF_CLAIM}", objReader.GetString("DOC"));
                                sTextFormat = sTextFormat.Replace("{TIME_OF_CLAIM}", objReader.GetString("TOC"));
                                sTextFormat = sTextFormat.Replace("{FILE_NO}", objReader.GetString("FNO"));
                                sTextFormat = sTextFormat.Replace("{SERVICE_CODE}", objReader.GetString("SC"));
                                sTextFormat = sTextFormat.Replace("{COUNTY_OF_INJURY}", objReader.GetString("COJ"));
                                sTextFormat = sTextFormat.Replace("{ADDRESS}", sAdd);
                                sTextFormat = sTextFormat.Replace("{DATE_OF_EVENT}", objReader.GetString("DOE"));
                                sTextFormat = sTextFormat.Replace("{TIME_OF_EVENT}", objReader.GetString("TOE"));
                                sTextFormat = sTextFormat.Replace("{DATE_REPORTED}", objReader.GetString("DR"));
                                sTextFormat = sTextFormat.Replace("{INJURY_FROM_DATE}", objReader.GetString("IFD"));
                                sTextFormat = sTextFormat.Replace("{INJURY_TO_DATE}", objReader.GetString("ITD"));
                                sTextFormat = sTextFormat.Replace("{EVENT_LOCATION}", objReader.GetString("LAD"));
                                sTextFormat = sTextFormat.Replace("{LINE_OF_BUSINESS}", objReader.GetString("LIOFB"));
                                sTextFormat = sTextFormat.Replace("{CLAIM_TYPE}", objReader.GetString("CLAIMTYPE"));
                                sTextFormat = sTextFormat.Replace("{EVENT_TYPE}", objReader.GetString("EVENTTYPE"));
                                objReader.Close();
                            }
                            else
                            {
                                objReader.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("RSWPayTLDiary/GetEmailFormat", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
            }
            return sTextFormat;
        }
        public static string AddIntervalToSubmitDate(string p_dSubmittedOn, int p_iInterval)
        {
            string sFinalSubmitDate = null;

            if ((p_iInterval > 0))
            {
                if ((m_iDaysAppReserves > 0))
                {
                    sFinalSubmitDate = Convert.ToString(Convert.ToDateTime(p_dSubmittedOn).AddDays(Convert.ToDouble(p_iInterval)));
                }
                else if ((m_iHoursAppReserves > 0))
                {
                    sFinalSubmitDate = Convert.ToString(Convert.ToDateTime(p_dSubmittedOn).AddHours(Convert.ToDouble(p_iInterval)));
                }
                else
                {
                    sFinalSubmitDate = p_dSubmittedOn;
                }
            }
            else
            {
                if ((m_iDaysAppReserves > 0))
                {
                    sFinalSubmitDate = Convert.ToString(Convert.ToDateTime(p_dSubmittedOn).AddDays(Convert.ToDouble(m_iDaysAppReserves)));
                }
                else if ((m_iHoursAppReserves > 0))
                {
                    sFinalSubmitDate = Convert.ToString(Convert.ToDateTime(p_dSubmittedOn).AddHours(Convert.ToDouble(m_iHoursAppReserves)));
                }
                else
                {
                    sFinalSubmitDate = p_dSubmittedOn;
                }
            }
            return sFinalSubmitDate;

        }
        public static int GetLOBManagerID(int p_iLOB)
        {
            string sSQL = null;
            sSQL = "";
            int lMaxAmount = 0;
            lMaxAmount = 0;
            int iApproverID = 0;
            short iCount = 0;
            DbConnection objConn = null;
            try
            {
                sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + Convert.ToString(p_iLOB);

                objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                objConn.Open();
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        iApproverID = objReader.GetInt("USER_ID");
                        lMaxAmount = objReader.GetInt("RESERVE_MAX");

                        for (iCount = 0; iCount <= (m_iArrSize - 1); iCount++)
                        {
                            if ((lMaxAmount < m_arrstructResFinal[iCount].dNewReserves))
                            {
                                iApproverID = 0;
                            }
                        }

                        if ((iApproverID > 0))
                        {
                            return iApproverID;
                        }
                    }
                    else
                    {
                        if (objReader != null)
                            objReader.Close();
                    }
                }
                //Get Top level user
                sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1";
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        iApproverID = objReader.GetInt("USER_ID");
                        return iApproverID;
                    }
                    else
                    {
                        if (objReader != null)
                            objReader.Close();
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("RSWPayTLDiary/GetLOBManagerID", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
            return iApproverID;
        }
        public static void GetResCategories(int p_iLOBCode, int p_iClmTypeCode)
        {
            string sSQL = null;
            bool bResByClaimType = false;
            int iCnt = 0;
            sSQL = "SELECT RES_BY_CLM_TYPE FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + Convert.ToString(p_iLOBCode);
            DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
            objConn.Open();
            using (DbReader ObjReader = objConn.ExecuteReader(sSQL))
            {
                if (ObjReader.Read())
                {
                    bResByClaimType = ObjReader.GetBoolean("RES_BY_CLM_TYPE");
                }
            }
            //Get reserve buckets based on claim type
            sSQL = "SELECT Count(*) as cnt";
            if ((bResByClaimType))
            {
                sSQL = sSQL + " FROM SYS_CLM_TYPE_RES, CODES_TEXT ";
                sSQL = sSQL + " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ";
                sSQL = sSQL + " LINE_OF_BUS_CODE = " + Convert.ToString(p_iLOBCode);
                sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + Convert.ToString(p_iClmTypeCode);
                //Get all reserve buckets that are available
            }
            else
            {
                sSQL = sSQL + " FROM SYS_LOB_RESERVES, CODES_TEXT ";
                sSQL = sSQL + " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ";
                sSQL = sSQL + " LINE_OF_BUS_CODE = " + Convert.ToString(p_iLOBCode);
            }
            using (DbReader ObjReader = objConn.ExecuteReader(sSQL))
            {
                // if (ObjReader.NextResult())
                if (ObjReader.Read())
                {
                    m_iArrSize = ObjReader.GetInt("cnt");
                }
            }
            //resizing array
            m_arrstructReserves = new structReserves[m_iArrSize + 1];

            //Get reserve buckets based on claim type
            sSQL = "SELECT RESERVE_TYPE_CODE, CODE_DESC";
            if ((bResByClaimType))
            {
                sSQL = sSQL + " FROM SYS_CLM_TYPE_RES, CODES_TEXT ";
                sSQL = sSQL + " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ";
                sSQL = sSQL + " LINE_OF_BUS_CODE = " + Convert.ToString(p_iLOBCode);
                sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + Convert.ToString(p_iClmTypeCode);
                sSQL = sSQL + " ORDER BY CODE_DESC ";
                //Get all reserve buckets that are available
            }
            else
            {
                sSQL = sSQL + " FROM SYS_LOB_RESERVES, CODES_TEXT ";
                sSQL = sSQL + " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ";
                sSQL = sSQL + " LINE_OF_BUS_CODE = " + Convert.ToString(p_iLOBCode);
                sSQL = sSQL + " ORDER BY CODE_DESC ";
            }
            using (DbReader ObjReader = objConn.ExecuteReader(sSQL))
            {
                iCnt = 0;
                while (ObjReader.Read())
                {
                    m_arrstructReserves[iCnt].iReserveTypeCode = ObjReader.GetInt("RESERVE_TYPE_CODE");
                    m_arrstructReserves[iCnt].sReserveDesc = ObjReader.GetString("CODE_DESC") + "";
                    iCnt++;

                }
            }
            return;
        }
        public static object StepWiseUpdateRSWForUser(int p_iRSWID, string p_sSubmittedBy, string p_sSubmittedTo, string p_dSubmittedOn, int p_iLevel, string p_sXMLDoc)
        {
            object functionReturnValue = null;
            string sDSN = string.Empty;
            int iInterval = 1;

            string sSQL = string.Empty;
            string sSubmittedTo = null;
            sSubmittedTo = "0";
            XmlDataDocument objDOMDocument = new XmlDataDocument();

            string sRegarding = null;
            string sPCName = null;
            string sClaimNum = null;
            string sRSWType = null;
            string sSenderLoginName = null;
            string sRecipientLoginName = null;
            string sFromEmail = null;
            string sToEmail = null;
            string sAttachTable = null;
            int lAttachRecordID = 0;
            string sEntryName = null;
            string sUserName = null;
            string sDiaryNotes = null;
            string sDiaryAssigningUser = null;
            string sDiaryAssignedUser = null;

            int iAdjID = 0;

            //sDS.Value = objLogin.SecurityDSN;
            ////Security database DSN
            //sDSN = sTrimNull(sDS.Value);
            //db = ROCKETCOMLibDTGRocket_definst.DB_OpenDatabase(hEnv, sDSN, 0);

            //iDays = p_iLevel * m_iDaysAppReserves
            if ((m_iDaysAppReserves > 0))
            {
                iInterval = p_iLevel * m_iDaysAppReserves;
            }
            else if ((m_iHoursAppReserves > 0))
            {
                iInterval = p_iLevel * m_iHoursAppReserves;
            }
            else
            {
                iInterval = 0;
            }
            DbConnection objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_sConnectionSecurity);
            //If worsksheet has lapsed for the user, find its supervisor
            //If (DateAdd("d", CDbl(iDays), p_dSubmittedOn) < Now) Then

            int iLOBManagerID = 0;
            int iTopID = 0;
            if ((Convert.ToDateTime(AddIntervalToSubmitDate(p_dSubmittedOn, iInterval)) < DateTime.Now))
            {
                sSQL = "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + p_sSubmittedTo;
                // rs1 = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(db, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0);
                // DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                objConn.Open();
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        sSubmittedTo = Convert.ToString(objReader.GetInt("MANAGER_ID"));
                    }
                }

                //If supervisor exists for this user, determine if the worksheet has lapsed for it too
                //or not otherwise assign worksheet to supervisor
                if ((sSubmittedTo != "0"))
                {
                    p_dSubmittedOn = AddIntervalToSubmitDate(p_dSubmittedOn, 0);
                    StepWiseUpdateRSWForUser(p_iRSWID, p_sSubmittedTo, sSubmittedTo, p_dSubmittedOn, p_iLevel, p_sXMLDoc);
                    //No supervisor for this user
                }
                else
                {
                    if ((m_iDaysAppReserves > 0))
                    {
                        iInterval = p_iLevel * m_iDaysAppReserves;
                    }
                    else if ((m_iHoursAppReserves > 0))
                    {
                        iInterval = p_iLevel * m_iHoursAppReserves;
                    }
                    else
                    {
                        iInterval = 0;
                    }


                    iLOBManagerID = 0;
                    iLOBManagerID = GetLOBManagerID(m_iLOBCode);
                    sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1";
                    DbConnection objCon = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                    objCon.Open();
                    using (DbReader objReader = objCon.ExecuteReader(sSQL))
                    {
                        if (objReader.Read())
                        {
                            iTopID = objReader.GetInt("USER_ID");
                        }
                        objCon.Close();
                    }

                    if ((iLOBManagerID == 0))
                    {
                        //Do Nothing
                    }
                    else if ((iTopID == iLOBManagerID))
                    {
                        //Send Diary/Mail to iLOBManagerID from p_sSubmittedTo
                        p_sSubmittedBy = p_sSubmittedTo;
                        p_sSubmittedTo = Convert.ToString(iLOBManagerID);
                        sSenderLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedBy));
                        sRecipientLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedTo));
                    }
                    else
                    {
                        //Check if time lapsed for LOB Manager
                        //If (DateAdd("d", CDbl(iDays), p_dSubmittedOn) < Now) Then
                        if ((Convert.ToDateTime(AddIntervalToSubmitDate(p_dSubmittedOn, iInterval)) < DateTime.Now))
                        {
                            //Send Diary/Mail to iTopID from p_sSubmittedTo
                            p_sSubmittedBy = Convert.ToString(iLOBManagerID);
                            p_sSubmittedTo = Convert.ToString(iTopID);
                            sSenderLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedBy));
                            sRecipientLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedTo));
                        }
                        else
                        {
                            //Send Diary/Mail to iLOBManagerID from p_sSubmittedTo
                            p_sSubmittedBy = p_sSubmittedTo;
                            p_sSubmittedTo = Convert.ToString(iLOBManagerID);
                            sSenderLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedBy));
                            sRecipientLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedTo));
                        }
                    }
                }
            }
            else
            {
                //Send Diary/Mail to p_sSubmittedTo from p_sSubmittedBy                
                sSenderLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedBy));
                sRecipientLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(p_sSubmittedTo));
            }

            string sDiaryAdj = string.Empty;
            string sAdjLoginName = string.Empty;
            string sAdjEmail = string.Empty;
            if ((Convert.ToInt32(p_sSubmittedBy) != Convert.ToInt32(p_sSubmittedTo)))
            {
                if ((!string.IsNullOrEmpty(sSenderLoginName) && !string.IsNullOrEmpty(sRecipientLoginName)))
                {
                    objDOMDocument.Load(p_sXMLDoc);

                    if ((!string.IsNullOrEmpty(objDOMDocument.SelectSingleNode("//control[@name='hdnPCName']").InnerText)))
                    {
                        sPCName = objDOMDocument.SelectSingleNode("//control[@name='hdnPCName']").InnerText;
                    }

                    if ((!string.IsNullOrEmpty(objDOMDocument.SelectSingleNode("//control[@name='hdnRSWType']").InnerText)))
                    {
                        sRSWType = objDOMDocument.SelectSingleNode("//control[@name='hdnRSWType']").InnerText;
                    }

                    if ((!string.IsNullOrEmpty(objDOMDocument.SelectSingleNode("//control[@name='hdnClaimNumber']").InnerText)))
                    {
                        sClaimNum = objDOMDocument.SelectSingleNode("//control[@name='hdnClaimNumber']").InnerText;
                    }

                    sRegarding = sClaimNum + " * " + sPCName;
                    sAttachTable = "CLAIM";
                    lAttachRecordID = Convert.ToInt32(m_lClaim_ID);
                    sUserName = sGetUserName(sSenderLoginName);
                    //Get Sender First Name and Last Name
                    sEntryName = "Approval request for Claim " + sClaimNum + " – " + sPCName + " - Time Lapsed for " + sUserName;
                    //Get the Last Diary Assigned User
                    sDiaryNotes = GetEmailFormat("RSWSheet", m_lClaim_ID);
                    sDiaryAdj = sDiaryNotes;
                    if (Convert.ToDouble(sRSWType) == 0)
                    {
                        sRSWType = "";
                    }
                    sDiaryAdj = sDiaryAdj.Replace("{RSW_TYPE}", (sRSWType));
                    sDiaryAdj = sDiaryAdj.Replace("{CLAIM_NUMBER}", sClaimNum);
                    sDiaryAdj = sDiaryAdj.Replace("{USER_NAME}", sPCName);
                    sDiaryAdj = sDiaryAdj.Replace("{REASON}", "The time for Reserve Worksheet approval has lapsed for " + sGetUserName(sSenderLoginName));
                    sDiaryAdj = sDiaryAdj.Replace("{SUBMITTED_TO}", sGetUserName(sRecipientLoginName));
                    sDiaryAdj = sDiaryAdj.Replace("{SUBMITTED_BY}", sGetUserName(sSenderLoginName));
                    sDiaryNotes = sDiaryNotes.Replace("{RSW_TYPE}", sRSWType);
                    sDiaryNotes = sDiaryNotes.Replace("{CLAIM_NUMBER}", sClaimNum);
                    sDiaryNotes = sDiaryNotes.Replace("{USER_NAME}", sPCName);
                    sDiaryNotes = sDiaryNotes.Replace("{REASON}", "The time for Reserve Worksheet approval has lapsed for " + sUserName);
                    sDiaryNotes = sDiaryNotes.Replace("{SUBMITTED_TO}", sGetUserName(sRecipientLoginName));
                    sDiaryNotes = sDiaryNotes.Replace("{SUBMITTED_BY}", sGetUserName(sSenderLoginName));

                    //Get the Last Diary Assigned User
                    m_stDataBaseType = objConn.DatabaseType;
                    if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                        sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + sDiaryNotes.Trim() + " %'";
                        sSQL = sSQL + " AND ATTACH_RECORDID = " + Convert.ToInt32(m_lClaim_ID);
                        sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + Convert.ToString(p_iRSWID) + ")";
                        sSQL = sSQL + " AND ASSIGNED_USER = '" + sRecipientLoginName.Trim() + "' AND ASSIGNING_USER = '" + sSenderLoginName.Trim() + "'";
                        sSQL = sSQL + " ORDER BY ENTRY_ID DESC";
                    }

                    else if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sSQL = "SELECT ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                        sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + sDiaryNotes.Trim() + " %'";
                        sSQL = sSQL + " AND ATTACH_RECORDID = " + Convert.ToInt32(m_lClaim_ID);
                        sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + Convert.ToString(p_iRSWID) + ")";
                        sSQL = sSQL + " AND ASSIGNED_USER = '" + sRecipientLoginName.Trim() + "' AND ASSIGNING_USER = '" + sSenderLoginName.Trim() + "'";
                        sSQL = sSQL + " AND ROWNUM <= 1 ORDER BY ENTRY_ID DESC";
                    }

                    else
                    {
                        sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY ";
                        sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + sDiaryNotes.Trim() + " %'";
                        sSQL = sSQL + " AND ATTACH_RECORDID = " + Convert.ToInt32(m_lClaim_ID);
                        sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + Convert.ToString(p_iRSWID) + ")";
                        sSQL = sSQL + " ORDER BY ENTRY_ID DESC";
                    }
                    objConn.Open();
                    using (DbReader objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (objReader.Read())
                        {

                            sDiaryAssigningUser = objReader.GetString("ASSIGNING_USER") + "";
                            sDiaryAssignedUser = objReader.GetString("ASSIGNED_USER") + "";
                        }
                        else
                        {
                            sDiaryAssigningUser = "";
                            sDiaryAssignedUser = "";
                        }
                    }
                    sFromEmail = MAPIVB.sGetEmailByLoginName(sSenderLoginName);
                    sToEmail = MAPIVB.sGetEmailByLoginName(sRecipientLoginName);

                    if ((sDiaryAssigningUser != sSenderLoginName | sDiaryAssignedUser != sRecipientLoginName))
                    {
                        //rsushilaggar MITS 19624 30-Mar-2010'
                        if ((m_bDisableDiaryNotifyForSuperVsr != true))
                        {
                            int parent = WPAForm.ParentRecord(sAttachTable, lAttachRecordID, WPAForm.Globalvar.m_iClientId);
                            GenerateDiary(sSenderLoginName, sRecipientLoginName, sRegarding, sAttachTable, parent, lAttachRecordID, sEntryName, sDiaryNotes);
                        }
                        if ((!string.IsNullOrEmpty(sFromEmail.Trim()) && !string.IsNullOrEmpty(sToEmail.Trim()) && m_bDisableEmailNotifyForSuperVsr != true))
                        {

                            bSendEmail(sUserName, sFromEmail, sToEmail, sEntryName, sDiaryNotes);
                        }
                        //End If comment by kuladeep for mits:28665

                        //Sending Diary and Email to Adjuster
                        iAdjID = iGetAdjusterEID(m_lClaim_ID);
                        if (((iAdjID > 0) && (Convert.ToInt16(p_sSubmittedTo) != iAdjID)))
                        {


                            // sAdjLoginName = Global.sGetUser(db, (objUser.DSNID), Convert.ToInt32(iAdjID));
                            sAdjLoginName = Global.sGetUser(lDSNID, Convert.ToInt32(iAdjID));
                            sAdjEmail = MAPIVB.sGetEmailByLoginName(sAdjLoginName);


                            sDiaryAdj = "A " + sRSWType + " for Claim " + sClaimNum + " – " + sPCName + " has been submitted to " + sGetUserName(sRecipientLoginName) + " for approval by " + sGetUserName(sSenderLoginName) + Constants.VBCrLf;
                            sDiaryAdj = sDiaryAdj + Constants.VBCrLf + "Please enter the RISKMASTER system and navigate to the Claim in order to see this reserve worksheet" + Constants.VBCrLf;
                            sDiaryAdj = sDiaryAdj + Constants.VBCrLf + "Reason: The time for Reserve Worksheet approval has lapsed for " + sGetUserName(sSenderLoginName) + ". " + Constants.VBCrLf;
                            sDiaryAdj = sDiaryAdj + Constants.VBCrLf + "Thank you," + Constants.VBCrLf + Constants.VBCrLf + "RISKMASTER X";

                            //rsushilaggar MITS 19624 30-Mar-2010'
                            if ((m_bDisableDiaryNotifyForSuperVsr != true))
                            {
                                int parent = WPAForm.ParentRecord(sAttachTable, lAttachRecordID, WPAForm.Globalvar.m_iClientId);
                                GenerateDiary(sSenderLoginName, sAdjLoginName, sRegarding, sAttachTable, parent, lAttachRecordID, sEntryName, sDiaryAdj);
                            }
                            if ((!string.IsNullOrEmpty(sFromEmail.Trim()) && !string.IsNullOrEmpty(sAdjEmail.Trim()) && m_bDisableEmailNotifyForSuperVsr != true))
                            {

                                bSendEmail(sUserName, sFromEmail, sAdjEmail, sEntryName, sDiaryAdj);
                            }

                        }

                    }
                    //Add/change location by kuladeep for mits:28665

                }
            }
            return functionReturnValue;

        }
        public static int iGetAdjusterEID(int p_lClaimId)
        {
            int functionReturnValue = 0;
            string sSQL = string.Empty;
            int lCurrentAdjusterID = 0;
            int iRMUserID = 0;
            DbConnection objConn = null;
            sSQL = "SELECT ADJUSTER_EID FROM CLAIM_ADJUSTER WHERE CLAIM_ID = " + Convert.ToString(p_lClaimId) + " AND CURRENT_ADJ_FLAG <> 0";
            // rs1 = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(dbGlobalConnect2, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0);
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                objConn.Open();
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        lCurrentAdjusterID = objReader.GetInt("ADJUSTER_EID");
                    }
                    objConn.Close();
                }

                if ((lCurrentAdjusterID > 0))
                {
                    sSQL = "SELECT RM_USER_ID FROM ENTITY WHERE ENTITY_ID = " + Convert.ToString(lCurrentAdjusterID);
                    //  rs1 = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(dbGlobalConnect2, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0);
                    objConn.Open();
                    using (DbReader objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (objReader.Read())
                        {
                            iRMUserID = objReader.GetInt("RM_USER_ID");
                            functionReturnValue = iRMUserID;
                        }
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("iGetAdjusterEID", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
            return functionReturnValue;
        }
        public static int lGetTableID(string sTableName)
        {
            int lTableID = 0;
            bool result = false;
            if (string.IsNullOrEmpty(sTableName.Trim()))
            {
                return lTableID;
            }

            // lTableID = lGetTableIDCache(sTableName);  //vb dll method
            DbConnection objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_sConnectionSecurity);
            objConn.Open();
            try
            {
                if (lTableID == 0)
                {
                    using (DbReader objReader = objConn.ExecuteReader("SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + sTableName + "'"))
                    {
                        if (objReader.Read())
                        {
                            lTableID = objReader.GetInt(1);

                        }
                        objConn.Close();
                    }

                    result = Util_GLS.bInitGlossary(false, true);
                }
            }

            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("lGetTableID", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
            return lTableID;

        }
        public static int lGetManagerID(int iRMId)
        {
            int iManagerID = 0;
            string sSQL = null;
            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(WPAForm.Globalvar.m_sConnectionSecurity);
                objConn.Open();
                sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.MANAGER_ID ";
                sSQL = sSQL + " FROM USER_DETAILS_TABLE,USER_TABLE";
                sSQL = sSQL + " WHERE USER_DETAILS_TABLE.DSNID =  " + lDSNID;
                sSQL = sSQL + " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";
                sSQL = sSQL + " AND USER_TABLE.USER_ID = " + iRMId;

                //   rs = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(db, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0);
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        iManagerID = objReader.GetInt("MANAGER_ID");

                    }
                    objConn.Close();
                }

            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("lGetManagerID", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
            return iManagerID;

        }
        public static int GetApprovalID(string p_sXMLDoc, int p_iManagerID, int p_lClaimId, int p_iUserID, int p_iGroupId)
        {
            int iThisManagerID = 0;
            int iApproverID = 0;
            bool bUnderLimit = false;

            iApproverID = p_iManagerID;
            iThisManagerID = p_iManagerID;

            if ((iThisManagerID == 0))
            {
                //No Supervisor Assigned
                iApproverID = GetLOBManagerID(m_iLOBCode);
            }
            else
            {
                bUnderLimit = UnderUserLimit(p_sXMLDoc, iThisManagerID, p_iGroupId, p_lClaimId);

                if ((bUnderLimit))
                {
                    iApproverID = iThisManagerID;
                }
                else
                {
                    //Need to check next manager

                    iApproverID = iThisManagerID;
                    iThisManagerID = lGetManagerID(Convert.ToInt32(iThisManagerID));

                    //No Supervisor Assigned
                    //If there is no manager assigned
                    if ((iThisManagerID == 0))
                    {
                        iThisManagerID = GetLOBManagerID(m_iLOBCode);
                    }
                    else
                    {
                        //Not to notify immediate manager
                        if (!(m_bNotifySupReserves))
                        {
                            iThisManagerID = GetApprovalID(p_sXMLDoc, iThisManagerID, p_lClaimId, p_iUserID, p_iGroupId);
                        }
                    }
                    iApproverID = iThisManagerID;
                }
            }
            return iApproverID;
        }
        public static bool UnderUserLimit(string p_sXMLDoc, int p_iUserID, int p_iGroupId, int p_lClaimId)
        {
            bool functionReturnValue = false;
            string sSQL = string.Empty;

            bool bUnderUserLimit = false;

            for (int iCnt = 0; iCnt <= (m_iArrSize - 1); iCnt++)
            {
                //Get Reserve Limits and Validate If Reserves fall within that limit
                for (int iCount = 0; iCount <= (m_iArrSize - 1); iCount++)
                {
                    //Query to find reserve limit for all Reserves
                    if ((m_arrstructResFinal[iCount].iReserveTypeCode == m_arrstructReserves[iCnt].iReserveTypeCode))
                    {
                        if ((m_arrstructResFinal[iCount].dNewReserves > 0))
                        {

                            bUnderUserLimit = CheckReserveLimits(p_lClaimId, m_arrstructReserves[iCnt].iReserveTypeCode, m_arrstructResFinal[iCount].dNewReserves, p_iUserID, p_iGroupId);
                            if ((bUnderUserLimit))
                            {
                                functionReturnValue = false;
                                return functionReturnValue;
                            }
                        }
                    }
                }
            }
            functionReturnValue = true;
            return functionReturnValue;


        }
        public static bool CheckReserveLimits(int p_lClaimId, int p_iReserveTypeCode, double p_dAmount, int p_iUserID, int p_iGroupId)
        {
            double dMaxAmount = 0;
            string sSQL = string.Empty;

            bool bReturnValue = false;

            sSQL = "SELECT MAX(MAX_AMOUNT)  MAX_AMT FROM RESERVE_LIMITS,CLAIM ";
            sSQL = sSQL + " WHERE (USER_ID = " + Convert.ToString(p_iUserID);
            if ((p_iGroupId != 0))
            {
                sSQL = sSQL + " OR GROUP_ID = " + Convert.ToString(p_iGroupId);
            }
            sSQL = sSQL + " ) AND CLAIM.CLAIM_ID = " + Convert.ToString(p_lClaimId);
            sSQL = sSQL + " AND CLAIM.LINE_OF_BUS_CODE = RESERVE_LIMITS.LINE_OF_BUS_CODE";
            sSQL = sSQL + " AND RESERVE_TYPE_CODE = " + Convert.ToString(p_iReserveTypeCode);

            //  rs1 = ROCKETCOMLibDTGRocket_definst.DB_CreateRecordset(dbGlobalConnect2, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0);
            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                objConn.Open();
                // m_stDataBaseType = objConn.DatabaseType;
                using (DbReader objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        if (objReader.GetBoolean("MAX_AMT"))
                        {
                            bReturnValue = false;
                        }
                        if ((bReturnValue))
                        {
                            //UPGRADE_WARNING: Couldn't resolve default property of object vDB_GetData(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dMaxAmount = objReader.GetDouble("MAX_AMT");

                            if ((p_dAmount > dMaxAmount))
                            {
                                bReturnValue = true;
                            }
                            else
                            {
                                bReturnValue = false;
                            }
                        }
                        else
                        {
                            bReturnValue = false;
                        }
                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("RSWPayTLDiary.CheckReserveLimits", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
            return bReturnValue;


        }
        public static int iGetGroupID(int p_iUserID)
        {
            int iGroupID = 0;
            DbConnection objConn = null;
            try
            {
                if (p_iUserID > 0)
                {
                    objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                    objConn.Open();
                    using (DbReader objReader = objConn.ExecuteReader("SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + Convert.ToString(p_iUserID)))
                    {
                        if (objReader.Read())
                        {
                            iGroupID = objReader.GetInt(0);
                        }

                    }
                }
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("RSWPayTLDiary.iGetGroupID", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
            return iGroupID;
        }

        public static bool EmailEntry(string sCtlNumber, string sSubject, string sSenderLoginName, string sRecipientLoginName, int iClientId)
        {
            bool bSendEmail = false;
            string sSQL = string.Empty;
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionRiskmaster);
                objConn.Open();
                sSQL = "INSERT INTO WPA_EMAIL_ENTRY (ENTRY_ID, ATTACH_REC_ID, MAIL_REGARDING, SENDER_ID, RECEIVER_ID, SENDING_DATE)";
                sSQL = sSQL + "VALUES(" + Utilities.GetNextUID(m_sConnectionRiskmaster, "WPA_EMAIL_ENTRY", iClientId) + ", '" + sCtlNumber + "', '" + sSubject + "', '" + sSenderLoginName + "', '" + sRecipientLoginName + "','" + DateTime.Now.ToString("yyyyMMdd") + "') ";
                objConn.ExecuteNonQuery(sSQL);
                bSendEmail = true;
                return bSendEmail;
            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("RSWPayTLDiary.EmailEntry", null, false, systemErrors);
            }
            finally
            {
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }

            }
            return bSendEmail;
        }

        public static void UpdateStatus(string sMessage)
        {
            if (WPAForm.IsCallFromTM)
            {
                // RMA-4300 :WPA processing exe tries to write the log in Application.StartupPath
                systemErrors.Add("WPAProcessing", sMessage, BusinessAdaptorErrorType.Message);
                WPAProcessing.logErrors("UpdateStatus", null, true, systemErrors);
                //string path = Application.StartupPath + @"\wpaproc.log";
                //File.AppendAllText(path, sMessage + Environment.NewLine);
                WPAForm.WriteToStdOut(0, sMessage);
            }

        }

    }
}

