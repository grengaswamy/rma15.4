﻿using Riskmaster.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.Collections.Specialized;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.IO;
using Riskmaster.Security.Encryption;
using System.Configuration;
namespace WPAProcessing
{
  public  static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
    public   static void Main(string[] args)
        {
            BusinessAdaptorErrors systemErrors = null;
            
            try
            {

            bool IsCallFromTM = false;

            //Change & add by kuladeep for Cloud-Start
            int iClientId = 0;
            string sTempClientId = string.Empty;
            if (args != null && args.Length > 5 && (args[5] != null))
            {
                sTempClientId = args[5] ?? string.Empty;
                iClientId = Convert.ToInt32(sTempClientId);
            }
            //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
            //By this we can run WPA through exe for different client by change ClientId in appSetting.config.
            else
            {
                bool blnSuccess = false;
                iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
            }

            systemErrors = new BusinessAdaptorErrors(iClientId); 
            //Login m_objLogin = new Login();
            Login m_objLogin = new Login(iClientId);
            //UserLogin oUserLogin = new UserLogin();
            UserLogin oUserLogin = null;
            //Change & add by kuladeep for Cloud-End

            Boolean bCon = false;
            if (args != null && args.Length > 0)
            {
                #region Log
                //  string path = @"D:\Sample\WPA\WPA\bin\Debug\wpaproc.log";
                //File.Open(path, FileMode.Open);
               // File.WriteAllText(path, args.Length.ToString());
                
                //File.AppendAllText(path, "before Login attempt:" + DateTime.Now.ToString());
                //string[] userInfo = args[0].Split(' ');
               // File.AppendAllText(path, "before Login attempt:" + DateTime.Now.ToString() + args[1] + args[2] + args[0]);
                // string sPwd = RMCryptography.DecryptString(args[2]);
                #endregion Log
                IsCallFromTM = true;
                string sPwd = args[2] ?? string.Empty;
                string sUname = args[1] ?? string.Empty;
                string sDSN = args[0] ?? string.Empty;
                //Deb : MITS 34031 
                //oUserLogin = m_objLogin.AuthenticateUser(sUname, sPwd, sDSN) as UserLogin;

                //Add & change by kuladeep for Cloud--Start
                //oUserLogin = new UserLogin(sUname, sDSN);
                oUserLogin = new UserLogin(sUname, sDSN, iClientId);
                //Add & change by kuladeep for Cloud--End

                if (oUserLogin.DatabaseId <= 0)
                {
                    throw new Exception("Authentication failure.");
                }
                //Deb : MITS 34031 
                WPAForm objform = new WPAForm(oUserLogin, iClientId);
                objform.GetAutoDiaryCount(true);
            }
            else
            {
                //int a = 10;
                //int b = 0;
                //a = a / b;
                IsCallFromTM = false;
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                oUserLogin = new UserLogin(iClientId);//psharma206              

                bCon = m_objLogin.RegisterApplication(0, 0, ref oUserLogin,iClientId);
                if (bCon)
                {
                    Application.Run(new WPAForm(oUserLogin,iClientId));

                    
                }
                else
                {
                    MessageBox.Show("Login has Failed... "+"\n"+"End of Auto Diary Processing");

                }
            }
                #region old
                // BusinessAdaptorErrors systemErrors = new BusinessAdaptorErrors();
           
            //if (args != null && args.Length > 0)
            //{
            //    string path  = Application.StartupPath +@"\wpaproc.log" ;
            //   // File.Open(path, FileMode.Open);
            //    File.WriteAllText(path,args.ToString());
            //}
           
           
                  
                //UserLogin oUserLogin = new UserLogin();
                //Boolean bCon = false;
                //string path = @"D:\Sample\WPA\WPA\bin\Debug\wpaproc.log";

                //File.AppendAllText(path, "before Login attempt:" + DateTime.Now.ToString());
                //bCon = m_objLogin.RegisterApplication(0, 0, ref oUserLogin);
                //File.AppendAllText(path, "After Login attempt:" + DateTime.Now.ToString());
               
                //if (bCon)
                //{
                //    Application.Run(new WPAForm(oUserLogin));
                //    WPAForm objform = new WPAForm();
                //    objform.GetAutoDiaryCount();
                //}
                #endregion Log

            }
            catch (Exception p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                WPAProcessing.logErrors("Main Method", null, false, systemErrors);               
            }
        }
    }
}
