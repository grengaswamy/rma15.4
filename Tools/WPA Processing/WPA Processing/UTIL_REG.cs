﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPAProcessing
{
    public class UTIL_REG
    {
        // Declare all Registration Database API's

        //---------------------------------------------------------
        // Registration database constants
        //----------------------------------------------------------
        public const string REG_ROOT_KEY = "SOFTWARE\\DTG";
        public const short REG_SZ = 1;
        public const short KEY_LENGTH = 256;
        public const short REG_SUCCESS = 0;
        public const short REG_BADKEY = 2;
        public const int HKEY_LOCAL_MACHINE = 80000002;
       
        public static extern int RegQueryValue(int hKey, string lpSubKey, string lpValue, ref int lpcbValue);

        //*****************************************************************
        //*
        //*  bRegDBGetValue (sKey As String, sValue As String) As Integer
        //*
        //*  <Enter Function Description Here>
        //*
        //*  Inputs: <Enter Input Values Here>
        //*
        //*  Returns: <Enter Return Values Here>
        //*
        //*  Date Written:             By:
        //*
        //*  Revisions:
        //*
        //*  Date          Who     Description
        //*  --------      ---     ------------------------------------
        //*
        //*****************************************************************
        public static bool bRegDBGetValue(string sKey, string sValue)
        {
            bool functionReturnValue = false;

            // VB6.FixedLengthString sItem = new VB6.FixedLengthString(KEY_LENGTH);
            string sItem = string.Empty;
            int lrtn = 0;
            functionReturnValue = false;
            string sRoot = null;
            string sTmp = null;
            //  sRoot = REG_ROOT_KEY + "\\" + String.Trim(My.Application.Info.Title);
            if (!string.IsNullOrEmpty(sKey) & sKey.Substring(1) != "\\")
            {
                sTmp = sRoot + "\\" + sKey;
            }
            else
            {
                sTmp = sRoot + sKey;
            }
            int keylen = KEY_LENGTH;
            lrtn = RegQueryValue(HKEY_LOCAL_MACHINE, sTmp, sItem, ref keylen);

            if (lrtn == REG_SUCCESS)
            {
                functionReturnValue = true;
                // sValue = sTrimNull(sItem.Value);
                sValue = sItem.Trim();
            }
            else
            {
                functionReturnValue = false;
            }
            return functionReturnValue;
            //ERROR_TRAP_bRegDBGetValue:


            //    if (iGeneralErrorExt(Err().Number, Conversion.ErrorToString(), "UTIL_REG.BAS\\bRegDBGetValue") == IDRETRY)
            //    {	 // ERROR: Not supported in C#: ResumeStatement
            //    }
            //    else
            //        functionReturnValue = false;
            //    return functionReturnValue;
            //    return functionReturnValue;

        }
    }
}
