Attribute VB_Name = "WinAPI_Functions"
Option Explicit



Public Type SHITEMID 'mkid
       cb As Long
       abID As Byte
End Type

Public Type ITEMIDLIST 'idl
   mkid As SHITEMID
End Type

Public Type BROWSEINFO 'bi
   hOwner As Long
   pidlRoot As Long
   pszDisplayName As String
   lpszTitle As String
   ulFlags As Long
   lpfn As Long
   lParam As Long
   iImage As Long
End Type
Public Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias "SHGetPathFromIDListA" _
       (ByVal pidl As Long, ByVal pszPath As String) As Long
Public Declare Function SHBrowseForFolder Lib "shell32.dll" Alias "SHBrowseForFolderA" _
       (lpBrowseInfo As BROWSEINFO) As Long
'Only returns file system directories. If the user selects folders
'that are not part of the file system, the OK button is grayed.
Public Const BIF_RETURNONLYFSDIRS = &H1
'Does not include network folders below the domain level in the tree
'view control. For starting the Find Computer.
Public Const BIF_DONTGOBELOWDOMAIN = &H2
'Includes a status area in the dialog box. The callback function can
'set the status text by sending messages to the dialog box.
Public Const BIF_STATUSTEXT = &H4
'Only returns file system ancestors. If the user selects anything other
'than a file system ancestor, the OK button is grayed.
Public Const BIF_RETURNFSANCESTORS = &H8
'Only returns computers. If the user selects anything other than a computer,
'the OK button is grayed.
Public Const BIF_BROWSEFORCOMPUTER = &H1000
'Only returns (network) printers. If the user selects anything other than
'a printer, the OK button is grayed.
Public Const BIF_BROWSEFORPRINTER = &H2000

Public Const BIF_BROWSEINCLUDEFILES = &H4000 ' Browsing for Everything

Public Declare Sub CoTaskMemFree Lib "ole32.dll" (ByVal pv As Long)

Public g_PrevWndProc As Long

Public Function WinBrowseForFolder(lBrowseType As Long, hWndOwner As Long, Optional sBrowseTitle As String = "") As String
Dim bi As BROWSEINFO
Dim IDL As ITEMIDLIST
Dim pidl As Long
Dim r As Long
Dim pos As Integer
Dim sPath As String
'Fill the BROWSEINFO structure with the needed data.
bi.hOwner = hWndOwner
'Pointer to the item identifier list specifying the
'location of the "root" folder to browse from.
' If NULL, the desktop folder is used.
bi.pidlRoot = 0&
' message to be displayed in the Browse dialog.
bi.lpszTitle = sBrowseTitle
' the type of folder to return.
bi.ulFlags = lBrowseType
' Show the browse folder dialog
pidl = SHBrowseForFolder(bi)
' Tthe dialog has closed, so parse & display the user's
' returned folder selection contained in pidl&.
sPath = Space$(512)
r = SHGetPathFromIDList(ByVal pidl, ByVal sPath$)
If r Then
   pos = InStr(sPath, Chr$(0))
   WinBrowseForFolder = Left(sPath, pos - 1)
Else
   WinBrowseForFolder = ""
End If
Call CoTaskMemFree(pidl)
End Function


