VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Write to PDF Files"
      Height          =   615
      Left            =   360
      TabIndex        =   2
      Top             =   480
      Width           =   2295
   End
   Begin VB.Frame fraDocument 
      Caption         =   "Select the path for the pdf-forms folder"
      Height          =   885
      Left            =   240
      TabIndex        =   0
      Top             =   1800
      Width           =   3975
      Begin VB.CommandButton Command2 
         Caption         =   "..."
         Height          =   375
         Left            =   3240
         TabIndex        =   3
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox txtPath 
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   3015
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public g_iFormsChanged As Integer
Public g_iCount As Integer

Sub WriteToPDF(ByVal s_AcrAVDoc As String)
    Dim acr As CAcroApp
    Dim acrAVDoc As CAcroAVDoc
    Dim acrPDDoc As CAcroPDDoc
    Dim acroForm As AFORMAUTLib.AFormApp
    Dim myField As AFORMAUTLib.Field
    Dim FrmFields As AFORMAUTLib.Fields
    Dim iRet As Long, i As Long
    Dim sJavaScript As String
    Dim bIsButton As Boolean


    On Error GoTo hError
    Set acr = CreateObject("AcroExch.App")
    
 
    Set acrAVDoc = CreateObject("Acroexch.AVDoc")

    If acrAVDoc.Open(s_AcrAVDoc, "") Then
        Set acrPDDoc = acrAVDoc.GetPDDoc
    Else
        Exit Sub
    End If
    
    
    Set acroForm = CreateObject("AFormAut.App")
    
    
    Set FrmFields = acroForm.Fields

    
    For Each myField In FrmFields
        If (myField.Type = "button") Then
        
            'Get the field name
            If LCase(myField.Name) = "btnprintsave" Then
                bIsButton = True
                g_iFormsChanged = g_iFormsChanged + 1
                '-- Print & Save Script,Created: BSB 04.26.2006
                
                Dim sFileName As String
                sFileName = CurDir$ & "\PrintSave.txt"
                sJavaScript = FileLoad(sFileName)
                
                '-- Execute the following JavaScript for "Print & Save"
                myField.SetJavaScriptAction "up", sJavaScript
                
                '-- Save the changes & exit
                iRet = acrPDDoc.Save(1, s_AcrAVDoc)
                'Exit Sub
            ElseIf LCase(myField.Name) = "btnsaveonly" Then
                bIsButton = True
                
                sFileName = CurDir$ & "\SaveOnly.txt"
                sJavaScript = FileLoad(sFileName)
                '-- Execute the following JavaScript for "Save Only"
                myField.SetJavaScriptAction "up", sJavaScript
                
                '-- Save the changes & exit
                iRet = acrPDDoc.Save(1, s_AcrAVDoc)

                GoTo hExit
            End If
        
        End If
    Next myField

'-- Used to get the list of all the forms which are not having "Save Only" and
'-- "Print & Save" buttons.
    If Not bIsButton Then
        g_iCount = g_iCount + 1
'''        WriteToFile gCount & " : " & s_AcrAVDoc
    End If

hExit:
acrPDDoc.Close
acrAVDoc.Close 1

acr.Exit

Set acr = Nothing
Set acrAVDoc = Nothing
Set acroForm = Nothing
Set FrmFields = Nothing
Exit Sub

hError:
    LogError "WriteToPdf", 0, Err.Number, Err.Source, s_AcrAVDoc & "  " & Err.Description
    GoTo hExit
'    Resume
End Sub


Private Sub Command1_Click()
    Dim objFSO As New Scripting.FileSystemObject
    Dim fld As Scripting.Folder
    Dim objFile As Scripting.File
    Dim sPdfHome As String
    Dim sFolders() As String
    Dim sMessage As String
    
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    bExit = False

    sPdfHome = txtPath.Text & "\"
    
    'This will look for all the .pdf files under the state abbrv. folders.
    For Each fld In objFSO.GetFolder(sPdfHome).SubFolders
        For Each objFile In fld.Files
            If Len(Trim(fld.Name)) = "2" Then
                If LCase(Right(objFile.Name, 3)) = "pdf" Then
                    WriteToPDF sPdfHome & fld.Name & "\" & objFile.Name
                End If
            End If
        Next
    Next

    sMessage = "Number of forms changed : " & g_iFormsChanged & Chr(13)
    sMessage = sMessage & "Number of forms not having save button : " & g_iCount
    
    
    MsgBox sMessage
    
hExit:
    Set objFSO = Nothing
    Exit Sub
hError:
    LogError "Command", 0, Err.Number, Err.Source, "Error From Command"
    
    GoTo hExit
    Resume

End Sub

Private Sub Command2_Click()
    Dim sDir As String
    sDir = WinBrowseForFolder(BIF_RETURNONLYFSDIRS, hWnd, "Select Documents Folder.")
    
    If sDir <> "" Then txtPath.Text = sDir

End Sub
'Purpose     :  Returns the contents of a file as a single continuous string
'Inputs      :  sFileName               The path and file name of the file to open and read
'Outputs     :  The contents of the specified file
'Notes       :  Usually used for text files, but will load any file type.
'Revisions   :

Function FileLoad(ByVal sFileName As String) As String
    Dim iFileNum As Integer, lFileLen As Long

    On Error GoTo ErrFinish
    'Open File
    iFileNum = FreeFile
    'Read file
    Open sFileName For Binary Access Read As #iFileNum
    lFileLen = LOF(iFileNum)
    'Create output buffer
    FileLoad = String(lFileLen, " ")
    'Read contents of file
    Get iFileNum, 1, FileLoad

ErrFinish:
    Close #iFileNum
    On Error GoTo 0
End Function
