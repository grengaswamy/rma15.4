﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;
using Microsoft.VisualBasic;
using Riskmaster.Db;

namespace RMXJurRulesDBUpgradeWizard
{
    public class CJRCalcBenefitOffsets
    {
        private const string sClassName = "CJRCalcBenefitOffsets";
        private List<CJRCalcBenefitOffset> mCol = new List<CJRCalcBenefitOffset>();
      
        public CJRCalcBenefitOffset Add( string FriendlyName,  int TableRowID,  int TableRowIDFriendlyName,  int TableRowIDParent,  int useCode,  int sKey)
        {
            CJRCalcBenefitOffset functionReturnValue = new CJRCalcBenefitOffset();
            try
            {
                //create a new object
                CJRCalcBenefitOffset objNewMember = new CJRCalcBenefitOffset();
                objNewMember = new CJRCalcBenefitOffset();

                //set the properties passed into the method
                objNewMember.TableRowID = TableRowID;
                objNewMember.useCode = useCode;
                objNewMember.TableRowIDParent = TableRowIDParent;
                objNewMember.TableRowIDFriendlyName = TableRowIDFriendlyName;
                objNewMember.FriendlyName = FriendlyName;

                mCol.Add(objNewMember);

                //return the object created
                functionReturnValue = objNewMember;

                objNewMember = null;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg:  - [CJRCalcBenefitOffsets.Add]" + ex.Message);
            }
            return functionReturnValue;
        }

        public int DLLFetchParentTableRowID(int ParentTableRowID)
        { 
            string sSQL;
            int DLLFetchParentTableRowID = 0;
            try
            {
                sSQL = GetSQLParentTableRowID(ref ParentTableRowID);
                DLLFetchParentTableRowID = FetchDataCollection(sSQL);
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg:  - [CJRCalcBenefitOffsets.DLLFetchParentTableRowID]" + ex.Message);
            }
            return DLLFetchParentTableRowID;
        }

        public int DLLFetchDataByTableRowIDParent(int ParentTableRowID)
        {
            string sSQL;
            int DLLFetchParentTableRowID = 0;
            try
            {
                sSQL = GetSQLParentTableRowID(ref ParentTableRowID);
                DLLFetchParentTableRowID = FetchDataCollection(sSQL);
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg:  - [CJRCalcBenefitOffsets.DLLFetchDataByTableRowIDParent]" + ex.Message);
            }
            return DLLFetchParentTableRowID;
        }

        public int FetchDataCollection(string sSQL)
        {
            int lTest;
            string FRIENDLY_NAME=string.Empty;
            int FN_TABLE_ROW_ID;
            int PT_TABLE_ROW_ID;
            int USE_CODE;

            int FetchDataCollection = 0;
            DbReader objReader = default(DbReader);

            try
            {
			//JIRA RMA-1052 
                using (objReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                {
                    if (objReader.Read())
                    {
                        while (objReader.Read())
                        {

                            lTest = objReader.GetInt32("TABLE_ROW_ID");
                            FRIENDLY_NAME = objReader.GetString("FRIENDLY_NAME");
                            FN_TABLE_ROW_ID = objReader.GetInt32("FN_TABLE_ROW_ID");
                            PT_TABLE_ROW_ID = objReader.GetInt32("PT_TABLE_ROW_ID");
                            USE_CODE = objReader.GetInt32("USE_CODE");

                            Add(FRIENDLY_NAME, lTest, FN_TABLE_ROW_ID, PT_TABLE_ROW_ID, USE_CODE, lTest);
                        }
                    }
                }
                FetchDataCollection = -1;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg:  - [CJRCalcBenefitOffsets.FetchDataCollection]" + ex.Message);
            }
            return FetchDataCollection;
        }

        public int SaveData()
        {
            foreach(var val in mCol)
            {
                return val.SaveData();
            }
            return 0;
        }

        public CJRCalcBenefitOffset Item(int vntIndexKey)
        {
            return mCol[vntIndexKey];
        }


        public int Count
        {
            get { return mCol.Count; }
        }
        public string ConnectionString
        {
            get { return DisplayDBUpgrade.g_sConnectString; }
            set { DisplayDBUpgrade.g_sConnectString = value; }
        }
        public void Remove(ref CJRCalcBenefitOffset vntIndexKey)
        {
            mCol.Remove(vntIndexKey);
        }
        private string GetBaseSQL()
        {
            string functionReturnValue = string.Empty;
            string sSQL = string.Empty;

            functionReturnValue = "";

            sSQL = "";
            sSQL = sSQL + "SELECT";
            sSQL = sSQL + " FN_TABLE_ROW_ID";
            sSQL = sSQL + ", PT_TABLE_ROW_ID";
            sSQL = sSQL + ", WCP_FRIENDLY_NAME.TABLE_ROW_ID";
            sSQL = sSQL + ", USE_CODE";
            sSQL = sSQL + ", FRIENDLY_NAME";
            sSQL = sSQL + " FROM WCP_BEN_SWCH, WCP_BEN_X_OFF, WCP_FRIENDLY_NAME";

            functionReturnValue = sSQL;
            return functionReturnValue;
        }
        private string GetSQLParentTableRowID(ref int lParentTableRowID)
        {
            string functionReturnValue = string.Empty;
            string sSQL = string.Empty;

            functionReturnValue = "";
            sSQL = "";
            sSQL = sSQL + "SELECT";
            sSQL = sSQL + " FN_TABLE_ROW_ID";
            sSQL = sSQL + ", PT_TABLE_ROW_ID";
            sSQL = sSQL + ", WCP_FRIENDLY_NAME.TABLE_ROW_ID";
            sSQL = sSQL + ", USE_CODE";
            sSQL = sSQL + ", FRIENDLY_NAME";
            sSQL = sSQL + " FROM WCP_BEN_X_SWCH,WCP_FRIENDLY_NAME";
            sSQL = sSQL + " WHERE WCP_BEN_X_SWCH.PT_TABLE_ROW_ID = " + lParentTableRowID;
            sSQL = sSQL + " AND WCP_BEN_X_SWCH.FN_TABLE_ROW_ID = WCP_FRIENDLY_NAME.TABLE_ROW_ID";

            functionReturnValue = sSQL;
            return functionReturnValue;
        }
    }
}
