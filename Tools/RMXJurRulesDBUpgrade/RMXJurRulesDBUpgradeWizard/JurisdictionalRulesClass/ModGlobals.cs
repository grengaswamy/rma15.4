﻿using System;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Riskmaster.Db;

namespace RMXJurRulesDBUpgradeWizard
{
    static class modGlobals
    {
        public enum DisBenCalcMask
        {
            dbcSuccess = 0,
            dbcNoWeeklyWage = 1,
            dbcNoEventDate = 2,
            dbcNoBankAccounts = 4,
            dbcNoTransMapping = 8,
            dbcNoRecord = 16,
            dbcNoPaymentDue = 32,
            dbcNoPPDRecord = 64,
            dbcSplitPayment = 128,
            dbcNoRateChange = 256,
            dbcRateChanged = 512,
            dbcPreCATwoYear = 1024,
            dbcNoEarnings = 2048,
            dbcNoEmployee = 4096,
            dbcNoJurisdictionalRule = 8192,
            dbcNoImpairment = 16384
        }

        //Deemed Code //JIRA RMA-1052 
        public static int GetNextUID(string sTableName)
        {
            int functionReturnValue = 0;
            int lTableID = 0;
            int lNextUID = 0;
            int lOrigUID = 0;
            int lRows = 0;
            int lCollisionRetryCount = 0;
            string sSQL = string.Empty;
            DbReader objReader = null;
            DbConnection objDbConnection = null;
            DbTransaction objDbTransaction = null;
            DbCommand objDbCommand = null;
            try
            {
                sTableName = sTableName.ToString().Trim();
                objReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, "SELECT TABLE_ID,NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + sTableName + "'");

                if (objReader.Read())
                {
                    lTableID = objReader.GetInt32(0);
                    lOrigUID = lNextUID = objReader.GetInt32(1);

                    lNextUID = lNextUID + 1;

                    // try to reserve id (searched update)
                    sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + lNextUID + " WHERE TABLE_ID = " + lTableID;

                    // only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
                    if (lOrigUID == 0)
                    {
                        sSQL = sSQL + " AND NEXT_UNIQUE_ID = " + lOrigUID;
                    }

                    // Try update

                    using (objDbConnection = DbFactory.GetDbConnection(DisplayDBUpgrade.g_sConnectString))
                    {
                        objDbConnection.Open();

                        objDbCommand = objDbConnection.CreateCommand();

                        objDbTransaction = objDbConnection.BeginTransaction();
                        objDbCommand.Connection = objDbConnection;
                        objDbCommand.Transaction = objDbTransaction;
                        objDbCommand.CommandText = sSQL;

                        objDbCommand.ExecuteNonQuery();

                        objDbTransaction.Commit();
                    }
                    // if success, return
                    if (lRows == 1)
                    {
                        functionReturnValue = lNextUID - 1;
                        return functionReturnValue;
                        // success
                    }
                    else
                    {
                        lCollisionRetryCount = lCollisionRetryCount + 1;
                        // collided with another user - try again (up to 1000 times)
                    }
                }

                functionReturnValue = lNextUID;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: - [ModGlobals.IGetNextUID]" + ex.Message);
                return 0;
            }
            finally
            {
                //objDbConnection.Close();
                //objDbCommand = null;
                //objDbTransaction = null;
                //objReader = null;
            }
            return functionReturnValue;
        }
		//Deemed Code //JIRA RMA-1052 
        public static int GetNextUID_new(string p_sTableName)
        {
            int functionReturnValue = 0;
            string sSQL = string.Empty;
            int iTableID = 0;
            int iNextUID = 0;
            int iOrigUID = 0;
            int iRows = 0;
            int iCollisionRetryCount = 0;
            int iErrRetryCount = 0;
            int iErrConenctionOpenRetryCount = 0;
            DbConnection p_oDBConnection = null;
            DbReader objDbReader = null;
            DbTransaction p_objTrans = null;
            try
            {
                p_oDBConnection = DbFactory.GetDbConnection(DisplayDBUpgrade.g_sConnectString);

                do
                {
                    if (p_oDBConnection.State != System.Data.ConnectionState.Open)
                    {
                        do
                        {
                            try
                            {
                                p_oDBConnection.Open();
                                break;
                            }
                            catch (Exception e)
                            {
                                iErrConenctionOpenRetryCount++;
                            }
                        }
                        while (iErrConenctionOpenRetryCount < 20);

                        if (iErrConenctionOpenRetryCount >= 20)
                        {
                            throw new Exception("Context.GetNextUID.Exception.Error On Connection Open");
                        }
                    }

                    //Get Current Id.
                    objDbReader = p_oDBConnection.ExecuteReader("SELECT TABLE_ID, NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sTableName + "'", p_objTrans);
                  
                    if (!objDbReader.Read())
                    {
                        objDbReader.Close();
                        objDbReader = null;
                        throw new Exception("GetNextUID.Exception.NoSuchTable");
                    }

                    iTableID = objDbReader.GetInt("TABLE_ID");
                    iNextUID = objDbReader.GetInt("NEXT_UNIQUE_ID");
                    objDbReader.Close();
                    objDbReader = null;

                    // Compute next id
                    iOrigUID = iNextUID;
                    if (iOrigUID != 0)
                        iNextUID++;
                    else
                        iNextUID = 2;
                    
                    // try to reserve id (searched update)
                    sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + iNextUID
                        + " WHERE TABLE_ID = " + iTableID;

                    // only add searched clause if no chance of a null originally
                    // in row (only if no records ever saved against table)   JP 12/7/96
                    if (iOrigUID != 0)
                        sSQL += " AND NEXT_UNIQUE_ID = " + iOrigUID;


                    // Try update
                    try
                    {
                        iRows = p_oDBConnection.ExecuteNonQuery(sSQL, p_objTrans);
                    }
                    catch (Exception e)
                    {
                        iErrRetryCount++;
                        if (iErrRetryCount >= 5)
                        {
                            throw new Exception("Context.GetNextUID.Exception.ErrorModifyingDB");
                        }
                    }
                    // if success, return
                    if (iRows == 1)
                    {
                        return iNextUID - 1;
                    }
                    else
                    {
                        // collided with another user - try again (up to 1000 times)
                        iCollisionRetryCount++;
                        // Close the connection to be reopened
                        p_oDBConnection.Close();
                    }
                }
                while ((iErrRetryCount < 5) && (iCollisionRetryCount < 1000));

                if (iCollisionRetryCount >= 1000)
                {
                    throw new Exception("Utilities.GetNextUID.Exception.CollisionTimeout");
                }

                functionReturnValue = iNextUID;
            }
            catch (Exception p_oException)
            {
                throw new Exception("Utilities.GetNextUID.Exception" + "  " + p_oException.Message);
            }
            finally
            {
                p_oDBConnection.Close();
                p_objTrans = null;
                objDbReader = null;
            }
            //shouldn't get here under normal conditions.
            return functionReturnValue;
        }
    }
}
