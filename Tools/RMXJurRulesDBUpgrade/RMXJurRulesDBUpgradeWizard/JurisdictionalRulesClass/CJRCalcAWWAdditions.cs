﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Diagnostics;
using Riskmaster.Db;
using Riskmaster.Common; //JIRA RMA-1052 

namespace RMXJurRulesDBUpgradeWizard
{
    public class CJRCalcAWWAdditions
    {
        private const string sClassName = "CJRCalcAWWAdditions";
        private List<CJRCalcAWWAddition> mCol  = new List<CJRCalcAWWAddition>();
        //---------------------------------------------------------------------------------------
        // Procedure : DLLFetchData
        // DateTime  : 7/15/2004 14:47
        // Author    : jtodd22
        // Purpose   : This is an internal Data Fetch of the DLL.
        //...........: There is no need to connect to the database.
        //---------------------------------------------------------------------------------------
        //
        public int DLLFetchData(ref int lTableRowIDParent)
        {
            int functionReturnValue = 0;
            string sSQL = null;

            try
            {
                functionReturnValue = 0;
                sSQL = GetSQLTableRowIDParent(ref lTableRowIDParent);
                FetchDataCollection(ref sSQL);
                functionReturnValue = -1;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg: " + "- [CJRBenefitRuleTTD.DLLFetchData]  " + ex.Message);
                return 0;
            }
            return functionReturnValue;

        }
        public int SaveData()
        {
            int functionReturnValue = 0;
            short i = 0;
            int lTest = 0;
            string sSQL = null;
            DbReader objReader = default(DbReader);
            DbWriter objWriter = default(DbWriter);

            functionReturnValue = 0;

            foreach (var entry in mCol)
            {
                sSQL = "";
                sSQL = sSQL + "SELECT";
                sSQL = sSQL + " FN_TABLE_ROW_ID";
                sSQL = sSQL + ", PT_TABLE_ROW_ID";
                sSQL = sSQL + ", TABLE_ROW_ID";
                sSQL = sSQL + ", USE_CODE";
                sSQL = sSQL + " FROM WCP_AWW_X_SWCH";
                sSQL = sSQL + " WHERE TABLE_ROW_ID = " + entry.TableRowID;
				//JIRA RMA-1052 
                using (objReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                {
                    if ((objReader.Read()))
                    {
                        objWriter = DbFactory.GetDbWriter(objReader, true);
                        objWriter.Fields["FN_TABLE_ROW_ID"].Value = entry.TableRowIDFriendlyName;
                        objWriter.Fields["PT_TABLE_ROW_ID"].Value = entry.TableRowIDParent;
                        objWriter.Fields["USE_CODE"].Value = entry.useCode;
                        objWriter.Execute();
                        objWriter = null; //JIRA RMA-1052 
                    }
                    else
                    {
                        objWriter = DbFactory.GetDbWriter(DisplayDBUpgrade.g_sConnectString);
                        //lTest = modGlobals.GetNextUID("WCP_AWW_X_SWCH");
                        lTest = Utilities.GetNextUID(DisplayDBUpgrade.g_sConnectString, "WCP_AWW_X_SWCH"); //JIRA RMA-1052 
                        objWriter.Tables.Add("WCP_AWW_X_SWCH");
                        objWriter.Fields.Add("TABLE_ROW_ID", lTest);
                        objWriter.Fields.Add("FN_TABLE_ROW_ID", entry.TableRowIDFriendlyName);
                        objWriter.Fields.Add("PT_TABLE_ROW_ID", entry.TableRowIDParent);
                        objWriter.Fields.Add("USE_CODE", entry.useCode);
                        objWriter.Execute();
                        objWriter = null; //JIRA RMA-1052 
                    }
                }
            }
            return functionReturnValue;

        }
        //---------------------------------------------------------------------------------------
        // Procedure : LoadData
        // DateTime  : 7/15/2004 13:52
        // Author    : jtodd22
        // Purpose   :
        // Notes     : This LoadData function was called from inside the DLL and from the
        //...........: AWW Calculator in the EXE.  This presented an issue with database connections.
        //...........: The issue was solved by splitting the original function.
        //---------------------------------------------------------------------------------------
        //
        public int LoadData(ref int lTableRowIDParent)
        {
            int functionReturnValue = 0;
            string sSQL = null;

            try
            {
                functionReturnValue = 0;
                sSQL = GetSQLTableRowIDParent(ref lTableRowIDParent);
                functionReturnValue = FetchDataCollection(ref sSQL);
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg:  - [CJRCalcAWWAdditions.LoadData]" + ex.Message);
            }
           
            return functionReturnValue;
        }

        public CJRCalcAWWAddition Add(string FriendlyName, int TableRowID, int TableRowIDFriendlyName, int TableRowIDParent, int useCode)
        {

            CJRCalcAWWAddition functionReturnValue = new CJRCalcAWWAddition();
            //create a new object
            try
            {
                CJRCalcAWWAddition objNewMember = new CJRCalcAWWAddition();
                objNewMember = new CJRCalcAWWAddition();
                //set the properties passed into the method
                objNewMember.TableRowID = TableRowID;
                objNewMember.useCode = useCode;
                objNewMember.TableRowIDParent = TableRowIDParent;
                objNewMember.TableRowIDFriendlyName = TableRowIDFriendlyName;
                objNewMember.FriendlyName = FriendlyName;

                mCol.Add(objNewMember);

                functionReturnValue = objNewMember;
                objNewMember = null;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg:  - [CJRCalcAWWAdditions.Add]" + ex.Message);
            }
            return functionReturnValue;
        }

        public string ConnectionString
        {
            get { return DisplayDBUpgrade.g_sConnectString; }
            set { DisplayDBUpgrade.g_sConnectString = value; }
        }

        public CJRCalcAWWAddition Item(int vntIndexKey) 
        {
             return mCol[vntIndexKey]; 
        }

        public int Count
        {
            get 
            {
                if (mCol==null)
                {
                    return 0;
                }
                else
                {
                    return mCol.Count; 
                }
            }
        }

        public void Remove(CJRCalcAWWAddition vntIndexKey)
        {
            mCol.Remove(vntIndexKey);
        }

      
        private string GetBaseSQL()
        {
            string functionReturnValue = null;
            string sSQL = null;
            functionReturnValue = "";
            sSQL = "";
            sSQL = sSQL + "SELECT";
            sSQL = sSQL + " FN_TABLE_ROW_ID";
            sSQL = sSQL + ", PT_TABLE_ROW_ID";
            sSQL = sSQL + ", TABLE_ROW_ID";
            sSQL = sSQL + ", USE_CODE";
            functionReturnValue = sSQL;
            return functionReturnValue;
        }

        private string GetSQLTableRowIDParent(ref int lTableRowIDParent)
        {
            string functionReturnValue = string.Empty;
            string sSQL = string.Empty; ;

            functionReturnValue = "";

            sSQL = "";
            sSQL = sSQL + "SELECT";
            sSQL = sSQL + " FN_TABLE_ROW_ID";
            sSQL = sSQL + ", PT_TABLE_ROW_ID";
            sSQL = sSQL + ", WCP_FRIENDLY_NAME.TABLE_ROW_ID";
            sSQL = sSQL + ", USE_CODE";
            sSQL = sSQL + ", FRIENDLY_NAME";
            sSQL = sSQL + " FROM WCP_AWW_X_SWCH,WCP_FRIENDLY_NAME";
            sSQL = sSQL + " WHERE WCP_AWW_X_SWCH.PT_TABLE_ROW_ID = " + lTableRowIDParent;
            sSQL = sSQL + " AND WCP_AWW_X_SWCH.FN_TABLE_ROW_ID = WCP_FRIENDLY_NAME.TABLE_ROW_ID";

            functionReturnValue = sSQL;
            return functionReturnValue;
        }
        public int FetchDataCollection(ref string sSQL)
        {
            int FetchDataCollection = 0;
            DbReader objReader = null;
            int lTest = 0;
            string strKey = string.Empty;

            try
            {
			//JIRA RMA-1052 
                using (objReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                {
                    strKey = lTest.ToString();

                    while (objReader.Read())
                    {
                        lTest = objReader.GetInt32("TABLE_ROW_ID");

                        Add(objReader.GetString("FRIENDLY_NAME"), lTest, objReader.GetInt32("FN_TABLE_ROW_ID"), objReader.GetInt32("PT_TABLE_ROW_ID"), objReader.GetInt32("USE_CODE"));
                    }
                }
                FetchDataCollection = -1;
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "--- Error Msg:  - [CJRCalcAWWAdditions.FetchDataCollection]" + ex.Message);
            }
            return FetchDataCollection;
        }

    }
}
