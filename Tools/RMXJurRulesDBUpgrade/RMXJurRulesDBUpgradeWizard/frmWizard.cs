﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using Microsoft.Data.ConnectionUI;
using Microsoft.Win32;

using DevComponents.DotNetBar;
using Riskmaster.Db;

namespace RMXJurRulesDBUpgradeWizard
{
    public partial class frmWizard : Office2007Form
    {
        #region global variables
        private bool _dragging;
        private bool _isCustom;
        public static bool _isHPUpgrade=false; //igupta3

        public static bool _sDbchkMangeIndex = false; //Govind
        public static bool _sDbchkUpgradeDatabase = true; //Govind


        private Point _offset;
        private string _secConnString;
        private string _secProvider;
        private string _errorCache;

        const string SQL_SERVER_FILTER = "SQL Server";
        #endregion

        public frmWizard()
        {
            InitializeComponent();
            Activate();
        }

        #region wizard page events
        /// <summary>
        /// wizard page changing function - all navigation processing and call originate from here
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_WizardPageChanging(object sender, WizardCancelPageChangeEventArgs e)
        {
            chkUpgradeDatabase.Visible = false; //Govind

            Cursor = Cursors.WaitCursor;
            //rsolanki2:  updates for enabling more detailed logging in dev environment
            Boolean.TryParse(System.Configuration.ConfigurationManager.AppSettings["DebugLogging"], out UpdateFunctions._bDebugModeLogging);

            //make sure the error page is skipped through normal navigation
            if (e.NewPage == wpErrorPage && e.PageChangeSource == eWizardPageChangeSource.NextButton )
            {
                e.NewPage = wpFinish;
            }

            //rsolanki2 : adding the back button to the finish page.
            if (e.OldPage == wpFinish && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                e.NewPage = wpDataSources;
                
                //reselecting the previously selected DSN's 
                if (!string.IsNullOrEmpty(LblDsnList.Text))
                {
                    string[] arrDsnList = LblDsnList.Text.Split(',');
                    foreach (string sDsnEntry in arrDsnList)
                    {
                        foreach (ListViewItem lvi in lvDatabases.Items)
                        {
                            if (lvi.Text.Equals(sDsnEntry))
                            {
                                lvi.Selected = true;
                                break;
                            }
                        }
                    }

                    LblDsnList.Visible = true;
                    LblDsnListHeader.Visible = true;
                }
                return;
            }
            //leaving disclaimer page and check connectivity to security database
            if (e.OldPage == wpDisclaimer && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                string sDbUpgradeCustom = ConfigurationManager.AppSettings["DBUPGRADE_CUSTOM"].ToString().ToUpper();
                _isCustom = (sDbUpgradeCustom == bool.TrueString.ToUpper()) ? true : false;
                

                //Validate all of the configured database connections
                var arrFailedConnections = DisplayDBUpgrade.ValidateConnections();

                //If any connection strings failed, do not proceed with the installation
                if (arrFailedConnections.Count > 0)
                {
                    System.Text.StringBuilder strErrBldr = new System.Text.StringBuilder();

                    strErrBldr.AppendFormat("One or more connection strings were invalid:{0}", "<br /><br />");

                    foreach (var connSetting in arrFailedConnections)
                    {
                        strErrBldr.AppendFormat("{0}<br />", connSetting.Name);
                    }//foreach

                    ErrorDisplay(strErrBldr.ToString(), "error", eWizardButtonState.False, String.Empty);
                    e.Cancel = true;
                    Cursor = Cursors.Arrow;
                    return;
                }//if

                //process validations if appropriate
                if (!DisplayDBUpgrade.ValidateStartup(_isCustom))
                {
                    string sErrorMessage = "The Database Upgrade Program cannot be started. Likely causes " + 
                                           "include an incorrect folder structure or missing dependencies.<br /><br />" +
                                           "Please check <font color='#000000'>JurRules_dBUpgrade.log</font> file for errors and " + 
                                           "contact RISKMASTER Support.<br /><br />Press Cancel to close program.";
                    ErrorDisplay(sErrorMessage, "error", eWizardButtonState.False, String.Empty);
                    e.Cancel = true;
                    Cursor = Cursors.Arrow;
                    return;
                }

                DisplayDBUpgrade.g_SessionConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSession);

                //load the database list if validation passes
                if (LoadDSNs(String.Empty))
                {
                    Cursor = Cursors.Arrow;
                    e.NewPage = wpDataSources;
                    Application.DoEvents();
                }
                else //else load up the build connection screen
                {
                    //e.Cancel = true;
                    Cursor = Cursors.Arrow;
                    //wizard1.SelectedPage = wpBuildConnect;
                    Application.DoEvents();
                }
            }

            if (e.NewPage == wpBuildConnect)
            {
                btnSaveConfig.Enabled = false;
                wpBuildConnect.BackButtonVisible = eWizardButtonState.False;
                wpBuildConnect.CancelButtonVisible = eWizardButtonState.True;
                wpBuildConnect.NextButtonVisible = eWizardButtonState.True;
                wpBuildConnect.NextButtonEnabled = eWizardButtonState.False;

                try
                {
                    _secConnString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSecurity);
                    ParseConnString(_secConnString, String.Empty);

                    if (TestConnection(_secConnString))
                    {
                        wpBuildConnect.NextButtonEnabled = eWizardButtonState.True;
                        btnSaveConfig.Enabled = true;
                    }
                }
                catch (Exception)
                {
                    lblStatus.Text = _errorCache;
                    lblStatus.Refresh();
                    Application.DoEvents();
                }
            }

            if (e.OldPage == wpBuildConnect && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                //load the database list if validation passes
                if (LoadDSNs(_secConnString))
                {
                    Cursor = Cursors.Arrow;
                    e.NewPage = wpDataSources;
                    Application.DoEvents();
                }
                else //else load up the build connection screen
                {
                    e.Cancel = true;
                    Cursor = Cursors.Arrow;
                    //e.NewPage = wpBuildConnect;
                }
            }

            //leaving database selection page and validating connection for those selected
            if (e.OldPage == wpDataSources && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                if (lvDatabases.SelectedItems.Count == 0)
                {
                    lblWarningWPDS.Visible = true;
                    e.Cancel = true;
                }
                else
                {   
                    DatabaseSelections();
                }

            }

            //leaving database selection page's validating connection for those selected and Select the Type of DB processs
            if (e.OldPage == wpValidateConnections && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {

                chkUpgradeDatabase.Visible = true;
                lblSelectDesiredProcess.Visible = true;
                labelX18.Visible = true;
                lblSelectProcessNext.Visible = true;
            }

            //rsolanki2: mits 21693
            if (e.OldPage == wpDataSources && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                LblDsnList.Visible = false;
                LblDsnListHeader.Visible = false;
            }
            //make sure that any selected items in the dB list are de-selected during navigation to
            //ensure selection before proceeding
            if (e.NewPage == wpDataSources)
            {
                lvDatabases.SelectedItems.Clear();
            }

            if (e.NewPage == wpSelectProcess)
            {
                chkUpgradeDatabase.Visible = true;
                labelX18.Visible = true;
                lblSelectDesiredProcess.Visible = true;

               
            }
            if (e.OldPage == wpSelectProcess && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                string sDbchkUpgradeDatabase = chkUpgradeDatabase.Checked.ToString().ToUpper();
                _sDbchkUpgradeDatabase = (sDbchkUpgradeDatabase == bool.TrueString.ToUpper()) ? true : false;
                //Govind checkbox changes ends

            }

            //if the error wasn't serious we'll allow starting over
            if (e.OldPage == wpErrorPage && e.PageChangeSource == eWizardPageChangeSource.BackButton)
            {
                e.NewPage = wpDisclaimer;
            }
        }

        /// <summary>
        /// after validation page opens test connections to selected data sources
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpValidateConnections_AfterPageDisplayed(object sender, WizardPageChangeEventArgs e)
        {
            int iFailed = 0;
            string sErrorMsg = String.Empty;

            wpValidateConnections.CancelButtonVisible = eWizardButtonState.False;
            wpValidateConnections.NextButtonVisible = eWizardButtonState.True;

            //reset lists before proceeding
            DisplayDBUpgrade.dictFailedItems.Clear();
            DisplayDBUpgrade.dictPassedItems.Clear();
            lvFailed.Items.Clear();
            lvPassed.Items.Clear();

            lblDBConnResults.Text = String.Empty;
            lblTestingDB.Text = String.Empty;

            //make sure images are displayed and page is fully loaded before starting
            pbConnectionTest.Value = 0;
            pbConnectionTest.Maximum = DisplayDBUpgrade.dictSelectedItems.Count;
            lblTestingDB.Text = String.Empty;
            Application.DoEvents();

            //test each selected connection for connectivity
            foreach (var kvp in DisplayDBUpgrade.dictSelectedItems)
            {
                lblTestingDB.Text = "Checking connectivity for: <font color='#A0522D'>" + kvp.Key + "</font>";
                Application.DoEvents();

                //try connection
                DisplayDBUpgrade.bAborted = DisplayDBUpgrade.SelectDatabase(kvp.Value, ref sErrorMsg);

                //handle failed connection
                if (!DisplayDBUpgrade.bAborted)
                {
                    DisplayDBUpgrade.dictFailedItems.Add(kvp.Key,kvp.Value);
                    iFailed++;
                    pbConnectionTest.Value++;
                    lvFailed.Items.Add(kvp.Key, sErrorMsg);
                    Application.DoEvents(); 
                    continue;
                }

                //handle passed connections
                DisplayDBUpgrade.dictPassedItems.Add(kvp.Key,kvp.Value);
                pbConnectionTest.Value++;
                lvPassed.Items.Add(kvp.Key);
                Application.DoEvents();
            }

            //display approriate navigation message
            if (iFailed == DisplayDBUpgrade.dictSelectedItems.Count)
            {
                wpValidateConnections.NextButtonVisible = eWizardButtonState.False;
                lblDBConnResults.Text = "<font color='red'>Connections to all of your selections have failed, press <b>Back</b> to retry, or <b>Cancel</b> to exit.</font>";
            }
            else if (iFailed > 0 && iFailed != DisplayDBUpgrade.dictSelectedItems.Count)
            {
                lblDBConnResults.Text = "<font color='red'>Connections to some of your selections have failed, press <b>Back</b> to retry, <b>Next</b> to continue with valididated selections (processing will begin immediately), or <b>Cancel</b> to exit.</font>";
            }
            else
            {
                lblDBConnResults.Text = "Connections to all of your selections have been verified, press <b>Next</b> to continue (processing will begin immediately), or <b>Cancel</b> to exit";
            }
            
            wpValidateConnections.CancelButtonVisible = eWizardButtonState.True;

            lblTestingDB.Text = "Finished....";
        }

        /// <summary>
        /// wizard processing page all the upgrades will be done here with progress visible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpProcess_AfterPageDisplayed(object sender, WizardPageChangeEventArgs e)
        {
            wpProcess.NextButtonVisible = eWizardButtonState.False;
            wpProcess.BackButtonVisible = eWizardButtonState.False;
            Application.DoEvents();

            //leave wizard to process the database upgrades
            UpgradeScripts.UpgradeRMDatabase(_isCustom, _isHPUpgrade, false);

            wpProcess.NextButtonEnabled = eWizardButtonState.True;

            if (!DisplayDBUpgrade.bStop)
            {
                wizard1.SelectedPage = wpFinish;
            }
            Application.DoEvents();
        }

        private void wpSelectProcess_AfterPageDisplayed(object sender, DevComponents.DotNetBar.WizardPageChangeEventArgs e)
        {
            //MessageBox.Show("Event Hit");

            wpSelectProcess.NextButtonVisible = eWizardButtonState.False;
            wpSelectProcess.BackButtonVisible = eWizardButtonState.False;
            Application.DoEvents();

            //leave wizard to process the database upgrades
            //UpgradeScripts.UpgradeRMDatabase(_isCustom, _isHPUpgrade, false);

            wpSelectProcess.NextButtonEnabled = eWizardButtonState.True;
            wpSelectProcess.CancelButtonEnabled = eWizardButtonState.True;
            wpSelectProcess.NextButtonVisible = eWizardButtonState.True;
            wpSelectProcess.BackButtonVisible = eWizardButtonState.True;
                    
            Application.DoEvents();


        }

        /// <summary>
        /// display error messages in the wizard rather than in a pop-up message box
        /// </summary>
        /// <param name="strErrorMsg"></param>
        /// <param name="strIcon"></param>
        /// <param name="bDisplayButton"></param>
        public static void ErrorDisplay(string sError, string sIcon, eWizardButtonState bDisplayButton, string sException)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                PictureBox myPB = myForm.wpErrorPage.Controls["picturebox1"] as PictureBox;

                switch (sIcon.ToUpper().Trim())
                {
                    case "ERROR":
                        myPB.Image = SystemIcons.Error.ToBitmap();
                        break;
                    case "INFO":
                        myPB.Image = SystemIcons.Information.ToBitmap();
                        break;
                    case "WARNING":
                        myPB.Image = SystemIcons.Warning.ToBitmap();
                        break;
                    default:
                        break;
                }

                myForm.wpErrorPage.BackButtonVisible = bDisplayButton;
                myForm.wpErrorPage.NextButtonVisible = eWizardButtonState.False;
                myForm.wpErrorPage.Controls["gbError"].Controls["lblError"].Text = sError;

                if (!String.IsNullOrEmpty(sException))
                {
                    myForm.wpErrorPage.Controls["gbTechDesc"].Visible = true;
                    myForm.wpErrorPage.Controls["gbTechDesc"].Text = "Technical Description";
                    myForm.wpErrorPage.Controls["gbTechDesc"].Controls["lblException"].Text = sException;
                }
                else
                {
                    myForm.wpErrorPage.Controls["gbTechDesc"].Visible = false;
                }

                myForm.wizard1.SelectedPage = myForm.wpErrorPage;
            }
        }
        #endregion

        #region database functions
        /// <summary>
        /// load the database list
        /// </summary>
        private bool LoadDSNs(string sConnString)
        {
            DataTable dtListItems = new DataTable();

            dtListItems.Columns.Add(DisplayDBUpgrade.strDSNColumn);
            dtListItems.Columns.Add(DisplayDBUpgrade.strDSNIDColumn);

            //clear list box of any previous items
            lvDatabases.Items.Clear();

            try
            {
                if (String.IsNullOrEmpty(sConnString))
                {
                    DisplayDBUpgrade.g_SecConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSecurity);
                }
                else
                {
                    DisplayDBUpgrade.g_SecConnectString = sConnString;
                }

                //DataTable dtDSN = null;
                //dtDSN = Riskmaster.Security.RiskmasterDatabase.GetRiskmasterDatabases();

                using (var drDSNs = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, RISKMASTERScripts.GetOnlyDSNs()))
                {
                    while (drDSNs.Read())
                    {
                        string strKey = drDSNs[DisplayDBUpgrade.strDSNColumn].ToString();
                        string strValue = drDSNs[DisplayDBUpgrade.strDSNIDColumn].ToString();
                        string[] str = new string[2];
                        str[0] = strKey;
                        str[1] = strValue;
                        ListViewItem lvi = new ListViewItem(str);
                        lvDatabases.Items.Add(lvi);
                    }
                }

                if (lvDatabases.Items.Count <= 0)
                {
                    _errorCache = "An error has occurred during population of DSNs." + 
                                  Environment.NewLine + Environment.NewLine + 
                                  "There are no datasources configured in the specified security database. " + 
                                  "Please correct and try again later.";
                    ErrorDisplay(_errorCache, "error", eWizardButtonState.True, String.Empty);

                    return false;
                }

                lvDatabases.SelectedItems.Clear();

                return true;
            }
            catch (Exception ex)
            {
                _errorCache = "An error has occurred during population of DSNs." +
                              Environment.NewLine + Environment.NewLine + 
                              "This is usually caused by not being able to connect to the security database, " + 
                              "please double check connection information before trying again.";
                string sNull = ex.Message;
                ErrorDisplay(_errorCache, "error", eWizardButtonState.True, ex.Message);

                return false;
            }
        }

        /// <summary>
        /// fill public list with database selections
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, int> DatabaseSelections()
        {
            //clear out any previously added datasources
            DisplayDBUpgrade.dictSelectedItems.Clear();

            LblDsnList.Text = string.Empty;

            foreach (ListViewItem lvi in lvDatabases.SelectedItems)
            {
                DisplayDBUpgrade.dictSelectedItems.Add(lvi.Text.Trim(), Convert.ToInt32(lvi.SubItems[1].Text));

                //rsolanki2: mits 21693
                LblDsnList.Text = string.Concat(LblDsnList.Text , lvi.Text , ", ");
            }

            //rsolanki2: mits 21693
            LblDsnList.Text = LblDsnList.Text.Substring(0, LblDsnList.Text.Length - 2);
            LblDsnList.Visible = true;
            LblDsnListHeader.Visible = true;

            return DisplayDBUpgrade.dictSelectedItems;
        }

        /// <summary>
        /// disable warning if an item is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvDatabases_Click(object sender, EventArgs e)
        {
            if (lvDatabases.SelectedItems.Count != 0)
            {
                lblWarningWPDS.Visible = false;
            }
        }

        /// <summary>
        /// lvFailed_SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvFailed_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sCaption = String.Empty;

            ListView.SelectedListViewItemCollection objItems = this.lvFailed.SelectedItems;
            int iCnt = objItems.Count;

            if (iCnt > 0)
            {
                ListViewItem lvi = objItems[0];
                sCaption = String.Format("Selected Database: {0}", lvi.SubItems[0].Text);
                MessageBox.Show(lvi.ImageKey, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region progress bar and display text functions
        /// <summary>
        /// set maximum properties on overall progress bars
        /// </summary>
        /// <param name="iOverallMaximum"></param>
        public static void SetOverallProgressBarProperties(int iOverallMaximum)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.pbOverall.Maximum = iOverallMaximum;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// set maximum properties on current progress bars
        /// </summary>
        /// <param name="iCurrentMaximum"></param>
        public static void SetCurrentProgressBarProperties(int iCurrentMaximum)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.pbCurrentFile.Maximum = iCurrentMaximum;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// update the overall progress bar on process page
        /// </summary>
        /// <param name="iOverall"></param>
        public static void UpdateOverallProgressBar(int iOverall)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.pbOverall.Value = iOverall;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// update the current progress bar on process page
        /// </summary>
        /// <param name="iCurrentFile"></param>
        public static void UpdateCurrentProgressBar(int iCurrentFile)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.pbCurrentFile.Value = iCurrentFile;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// updates the name of the script file being processed
        /// </summary>
        /// <param name="sScriptName"></param>
        public static void UpdateScriptName(string sScriptName)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                if (sScriptName.Length > 33)
                {
                    myForm.lblProcessingScript.Text = "Processing script: <font size='-4' color='#000000'>" + sScriptName + " </font>";
                }
                else if (sScriptName.Length > 30)
                {
                    myForm.lblProcessingScript.Text = "Processing script: <font size='-3' color='#000000'>" + sScriptName + " </font>";
                }
                else if (sScriptName.Length > 25)
                {
                    myForm.lblProcessingScript.Text = "Processing script: <font size='-2' color='#000000'>" + sScriptName + " </font>";
                }
                else
                {
                    myForm.lblProcessingScript.Text = "Processing script: <font color='#000000'>" + sScriptName + " </font>";
                }

                Application.DoEvents();
            }
        }

        /// <summary>
        /// updates the name of the database being processed
        /// </summary>
        /// <param name="sDatabaseName"></param>
        public static void UpdateDBName(string sDatabaseName)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            switch (sDatabaseName)
            {
                case "SECURITY":
                    DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_SecConnectString);
                    break;
                case "VIEW":
                    DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_ViewConnectString);
                    break;
                case "HISTORY":
                    DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_HistTrackConnectString);
                    break;
                default:
                    DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);
                    break;
            }

            if (myForm != null)
            {
                switch (DisplayDBUpgrade.g_dbMake)
                {
                    case 1:                 // DBMS_IS_SQLSRVR  = 1
                        myForm.pictureBox8.Image = RMXJurRulesDBUpgradeWizard.Properties.Resources.mssql_icon;
                        break;
                    case 4:                 // DBMS_IS_ORACLE   = 4
                        myForm.pictureBox8.Image = RMXJurRulesDBUpgradeWizard.Properties.Resources.logo_oracle;
                        break;
                    default:
                                            // DBMS_IS_ACCESS   = 0
                                            // DBMS_IS_SYBASE   = 2
                                            // DBMS_IS_INFORMIX = 3
                                            // DBMS_IS_ODBC     = 5
                                            // DBMS_IS_DB2      = 6
                        myForm.pictureBox8.Image = RMXJurRulesDBUpgradeWizard.Properties.Resources.blank;
                        break;
                }
            
                myForm.lblProcessingDB.Text = String.Format("Processing dB: <font color='#A0522D'>{0}</font>", sDatabaseName);
            }

            Application.DoEvents();
        }

        /// <summary>
        /// update the executing text box with current SQL
        /// </summary>
        /// <param name="sSQL"></param>
        public static void UpdateExecutingText(string sSQL)
        {
            frmWizard myForm = Application.OpenForms["frmWizard"] as frmWizard;

            if (myForm != null)
            {
                myForm.tbExecuting.Text = sSQL;
                Application.DoEvents();
            }
        }
        #endregion

        #region wizard mouse events
        /// <summary>
        /// frmWizard_MouseDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmWizard_MouseDown(object sender, MouseEventArgs e)
        {
            _offset.X = e.X;
            _offset.Y = e.Y;
            _dragging = true;
        }

        /// <summary>
        /// frmWizard_MouseUp
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmWizard_MouseUp(object sender, MouseEventArgs e)
        {
            _dragging = false;
        }
        
        /// <summary>
        /// frmWizard_MouseMove
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmWizard_MouseMove(object sender, MouseEventArgs e)
        {
            if (_dragging)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new Point(currentScreenPos.X - _offset.X, currentScreenPos.Y - _offset.Y);
            }
        }
        #endregion

        #region wizard cancel/finish events
        /// <summary>
        /// finish button clicked close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_FinishButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// on disclaimer page - cancel button clicked, close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpDisclaimer_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// on build connection page - cancel button clicked, close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpBuildConnect_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// on data sources page - cancel button clicked, close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpDataSources_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// cancel was selected on validation page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpValidateConnections_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// cancel button pressed on error page - close application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpErrorPage_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close wizard
            Environment.Exit(Environment.ExitCode);
        }

        /// <summary>
        /// cancel was selected on Select process page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wpSelectProcess_CancelButtonClick(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //close Select Process wizard
            Environment.Exit(Environment.ExitCode);

        }
        #endregion

        #region wpBuildConnect button events
        /// <summary>
        /// btnSecConn_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSecConn_Click(object sender, EventArgs e)
        {
            BuildConnectionString();
        }

        /// <summary>
        /// btnSaveConfig_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            if (!String.IsNullOrEmpty(_secConnString))
            {
                try
                {
                    config.ConnectionStrings.ConnectionStrings["RMXSecurity"].ConnectionString = _secConnString;
                    config.ConnectionStrings.ConnectionStrings["RMXSecurity"].ProviderName = _secProvider;
                }
                catch (Exception)
                {
                    ConnectionStringsSection csSection = config.ConnectionStrings;
                    csSection.ConnectionStrings.Add(new ConnectionStringSettings("RMXSecurity", _secConnString, _secProvider));
                }
            }

            try
            {

                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("connectionStrings");

                lblStatus.Text = "<font color='#000000'>connectionStrings.config</font> " + 
                                 "<font color='#009900'>file was saved.  Press <b>Next</b> to continue...</font>";
                lblStatus.Refresh();
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                string sErrorMessage = "The Database Upgrade Program has encountered an error. Likely cause is the " +
                                       "directory/file is not writable...<br /><br />Exception was: " + ex.Message + 
                                       "<br /><br />You may want to contact RISKMASTER Support.<br /><br />" +
                                       "Press Cancel to close program.";
                ErrorDisplay(sErrorMessage, "error", eWizardButtonState.False, String.Empty);
                DisplayDBUpgrade.bStop = true;
            }
        }

        /// <summary>
        /// pictureBox7_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox7_Click(object sender, EventArgs e)
        {
            MessageBox.Show(String.Format("Sec String: {0}\n", _secConnString), "DEBUG ONLY!");
        }
        #endregion

        #region wpBuildConnect functions
        /// <summary>
        /// BuildConnectionString
        /// </summary>
        private void BuildConnectionString()
        {
            string sShortName = String.Empty;
            string sConnString = String.Empty;
            string sTestConnString = String.Empty;
            string sProviderName = String.Empty;

            DataConnectionDialog dcdObject = new DataConnectionDialog();
            DataSource.AddStandardDataSources(dcdObject);
            dcdObject.DataSources.Remove(DataSource.AccessDataSource);
            dcdObject.DataSources.Remove(DataSource.OracleDataSource);
            dcdObject.DataSources.Remove(DataSource.SqlDataSource);
            dcdObject.DataSources.Remove(DataSource.SqlFileDataSource);
            dcdObject.DataSources.Add(DataSource.OdbcDataSource);

            if (DataConnectionDialog.Show(dcdObject) == DialogResult.OK)
            {
                sTestConnString = dcdObject.ConnectionString;
                sShortName = dcdObject.SelectedDataProvider.ShortDisplayName;
                sProviderName = dcdObject.SelectedDataProvider.Name;

                _secProvider = sProviderName;
                sConnString = ParseConnString(sTestConnString, sShortName);
                _secConnString = sConnString;

                if (TestConnection(sConnString))
                {
                    wpBuildConnect.NextButtonEnabled = eWizardButtonState.True;
                    btnSaveConfig.Enabled = true;
                }
            }
            else
            {
                if (TestConnection(_secConnString))
                {
                    wpBuildConnect.NextButtonEnabled = eWizardButtonState.True;
                    btnSaveConfig.Enabled = true;
                }
            }

            dcdObject.Dispose();
        }

        /// <summary>
        /// ParseConnString
        /// </summary>
        /// <param name="sConnString"></param>
        /// <param name="sDBType"></param>
        /// <returns></returns>
        private string ParseConnString(string sConnString, string sDBType)
        {
            #region string variable initializations
            string sDbKey = String.Empty;
            string sDbVal = String.Empty;
            string sDsnKey = String.Empty;
            string sDsnVal = String.Empty;
            string sUidKey = String.Empty;
            string sUidVal = String.Empty;
            string sPwdKey = String.Empty;
            string sPwdVal = String.Empty;
            string sServerKey = String.Empty;
            string sServerVal = String.Empty;
            string sOracleDriver = String.Empty;
            #endregion

            string[] arrDrivers = UpgradeScripts.GetODBCDrivers();

            foreach (string driver in arrDrivers)
            {
                if (driver.Contains("Oracle"))
                {
                    sOracleDriver = driver;
                    break;
                }
            }

            Regex regex = new Regex(@"^(?<dsn>(?<dsnKey>DSN|Provider|Driver)=(?<dsnVal>[^;]+);?)?" + 
                          @"(?<server>(?<serverKey>Server|Data\sSource|wsid)=(?<serverVal>[^;]+);?)?" + 
                             @"(?<db>(?<dbKey>Database|DBQ|Initial\sCatalog)=(?<dbVal>[^;]+);?)?" + 
                               @"(?<info>(?<infoKey>Persist\sSecurity\sInfo)=(?<infoVal>[^;]+);?)?" + 
                                            @"(?<uid>(?<uidKey>UID|User\sID)=(?<uidVal>[^;]+);?)?" + 
                                            @"(?<pwd>(?<pwdKey>PWD|Password)=(?<pwdVal>[^;]+);?)?" + 
                                                    @"(?<misc>(?<miscKey>.+)=(?<miscVal>[^;]+);?)?$", RegexOptions.IgnoreCase);
            Match match = regex.Match(sConnString);

            if (match.Success)
            {
                sDsnKey = match.Groups["dsnKey"].Value;
                sDsnVal = match.Groups["dsnVal"].Value;
                sServerKey = match.Groups["serverKey"].Value;
                sServerVal = match.Groups["serverVal"].Value;
                sDbKey = match.Groups["dbKey"].Value;
                sDbVal = match.Groups["dbVal"].Value;
                sUidKey = match.Groups["uidKey"].Value;
                sUidVal = match.Groups["uidVal"].Value;
                sPwdKey = match.Groups["pwdKey"].Value;
                sPwdVal = match.Groups["pwdVal"].Value;
            }

            lblSecDSNorDriver.Visible = false;
            tbSecDSN.Visible = false;

            lblSecServer.Visible = false;
            tbSecServer.Visible = false;

            lblSecDB.Visible = false;
            tbSecDB.Visible = false;

            if (match.Success)
            {
                if (!String.IsNullOrEmpty(sDsnVal))
                {
                    lblSecDSNorDriver.Visible = true;
                    tbSecDSN.Visible = true;
                    lblSecDSNorDriver.Text = sDsnKey;
                    tbSecDSN.Text = sDsnVal;
                }

                if (!String.IsNullOrEmpty(sServerVal))
                {
                    lblSecServer.Visible = true;
                    tbSecServer.Visible = true;
                    lblSecServer.Text = sServerKey;
                    tbSecServer.Text = sServerVal;
                }

                if (!String.IsNullOrEmpty(sDbVal))
                {
                    lblSecDB.Visible = true;
                    tbSecDB.Visible = true;
                    lblSecDB.Text = sDbKey;
                    tbSecDB.Text = sDbVal;
                }

                lblSecUID.Text = sUidKey;
                tbSecUID.Text = sUidVal;

                lblSecPWD.Text = sPwdKey;
                tbSecPWD.Text = sPwdVal;
            }

            switch (sDBType)
            {
                case "ODBC":
                    //Dsn=DTG Security32;uid=sa;app=Microsoft (R) Visual Studio (R) 2008;wsid=MPALINSKI-1;database=RMXR5_Views;pwd=riskmaster.2k8
                    if (String.IsNullOrEmpty(sDbVal))
                    {
                        sConnString = String.Format("DSN={0};UID={1};PWD={2};", sDsnVal, sUidVal, sPwdVal);
                    }
                    else
                    {
                        sConnString = String.Format("DSN={0};database={1};UID={2};PWD={3};", sDsnVal, sDbVal, sUidVal, sPwdVal);
                    }
                    break;
                case "OracleClient":
                    //Data Source=ORCL;User ID=arp;Password=arp;Unicode=True
                    //Driver={Oracle in OraClient11g_home1};Server=orcl2;DBQ=orcl2;UID=rmx_sec;PWD=rmx_sec;
                    sConnString = String.Format("Driver={{{0}}};Server={1};DBQ={1};UID={2};PWD={3};", sOracleDriver, sServerVal, sUidVal, sPwdVal);
                    break;
                case "SqlClient":
                    //Data Source=cscdbssfl002;Initial Catalog=RMXR5_Views;User ID=sa;Password=riskmaster.2k8
                    //Data Source=MCOJOCARI-VDBS;Initial Catalog=RMXSecurity;Persist Security Info=True;User ID=sa;Password=riskmaster
                    if (String.IsNullOrEmpty(sUidVal) || String.IsNullOrEmpty(sPwdVal))
                    {
                        sConnString = String.Format("Driver={{{0}}};Server={1};Database={2};", SQL_SERVER_FILTER, sServerVal, sDbVal);
                    }
                    else
                    {
                        sConnString = String.Format("Driver={{{0}}};Server={1};Database={2};UID={3};PWD={4};", SQL_SERVER_FILTER, sServerVal, sDbVal, sUidVal, sPwdVal);
                    }
                    break;
                default:
                    break;
            }

            return sConnString;
        }

        /// <summary>
        /// TestConnection
        /// </summary>
        /// <param name="sConnString"></param>
        /// <returns></returns>
        private bool TestConnection(string sConnString)
        {
            int iFieldCount = 0;
            bool bStatus = true;
            string[] arrErrorMsgs = { };
            DbReader drDSNs = null;

            try
            {
                using (drDSNs = ADONetDbAccess.ExecuteReader(sConnString, RISKMASTERScripts.GetDSNs()))
                {
                    iFieldCount = drDSNs.FieldCount;
                }

                lblStatus.Text = "<font color='#009900'>Connection was successful.  Press <b>Next</b> to continue...</font>";
                lblStatus.Refresh();
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), ex.Message);

                _errorCache = ex.Message.Replace("\n", String.Empty);
                arrErrorMsgs = _errorCache.Split('\r');

                if (arrErrorMsgs.Length > 1)
                {
                    if (arrErrorMsgs[0] == arrErrorMsgs[1])
                    {
                        _errorCache = arrErrorMsgs[0];
                    }
                }

                Regex regex = new Regex(@"^(.*)(DATA_SOURCE_TABLE|table\sor\sview\sdoes\snot\sexist)(.*)$", RegexOptions.IgnoreCase);
                Match match = regex.Match(_errorCache);

                if (match.Success)
                {
                    _errorCache = "Connection failed.<br />Likely reason: The database is not a RISKMASTER Security DB.";
                }
                else
                {
                    _errorCache = "Connection failed.";
                }

                lblStatus.Text = _errorCache;
                lblStatus.Refresh();
                Application.DoEvents();

                bStatus = false;
            }

            return bStatus;
        }
        #endregion

        private void frmWizard_Load(object sender, EventArgs e)
        {

        }
        
    }
}
