;####################################################################################################################
;01 JurisRowID As Long
;02 UseSAWWMaximumCode As Long
;03 UseSAWWMinimumCode As Long
;04 PayFloorAmount As Long
;05 DollarForDollar As Long
;06 PayConCurrentPPD As Long
;07 EffectiveDateDTG As String
;08 EndingDateDTG As String
;09 FloorAmount As Double
;10 JurisDefinedWorkWeek As Long
;11 MaxCompRate As Double
;12 PayFixedAmount As Double
;13 MaxPercentSAWW As Double
;14 HighRate As Double
;15 LowRate As Double
;16 TotalAmount As Double
;17 TotalWeeks As Long
;18 ClaimantsAWWPoint as double
;Idaho
[ASSIGN %%1=ADD_JURISDICTION(ID,Idaho,0,0,0,0,0,0)]
[ASSIGN %%2=ADD_CODE(Y,1033,Yes,YES_NO,0)]
[ASSIGN %%3=ADD_CODE(N,1033,No,YES_NO,0)]
;                          1   2   3   4   5   6   7        8 9     10 11 12     13 14    15    16 17 18
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%2,%%3,%%3,19990101, ,68.40,00,00,205.20,00,90.00,67.00,00,00,306.28)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%2,%%3,%%3,20000101, ,70.65,00,00,211.95,00,90.00,67.00,00,00,316.35)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%2,%%3,%%3,20010101, ,74.25,00,00,222.75,00,90.00,67.00,00,00,332.46)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%2,%%3,%%3,20020101, ,78.90,00,00,236.70,00,90.00,67.00,00,00,353.29)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%2,%%3,%%3,20030101, ,79.05,00,00,237.15,00,90.00,67.00,00,00,353.97)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%2,%%3,%%3,20040101, ,80.10,00,00,240.30,00,90.00,67.00,00,00,358.66)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%2,%%3,%%3,20050101, ,81.45,00,00,244.35,00,90.00,67.00,00,00,364.71)]
;
[ASSIGN %%1=ADD_JURISDICTION(PA,Pennsylvania,0,0,0,0,0,0)]
;                          1   2   3   4   5   6   7        8 9  10 11     12     13 14    15    16 17 18
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%3,%%3,%%3,20050101, ,00,00,716.00,358.00,00,90.00,66.67,00,00,537.00)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%3,%%3,%%3,20040101, ,00,00,690.00,345.00,00,90.00,66.67,00,00,517.23)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%3,%%3,%%3,20030101, ,00,00,675.00,337.50,00,90.00,66.67,00,00,506.22)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%3,%%3,%%3,20020101, ,00,00,662.00,331.00,00,90.00,66.67,00,00,496.48)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%3,%%3,%%3,20010101, ,00,00,644.00,322.00,00,90.00,66.67,00,00,483.00)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%3,%%3,%%3,20000101, ,00,00,611.00,305.50,00,90.00,66.67,00,00,458.25)]
[ASSIGN %%4=ADD_WCP_TTD_ID(%%1,%%2,%%3,%%3,%%3,%%3,19990101, ,00,00,588.00,294.00,00,90.00,66.67,00,00,441.00)]
;
[ASSIGN %%1=ADD_JURISDICTION(IL,Illinois,0,0,0,0,0,0)]
;
;01JurisRowID As Long, _
;02EffectiveDateDTG as String
;03Child1 As Double
;04Child2 As Double
;05Child3 As Double
;06Child4 As Double
;07ClaimantsAWWCode As Long
;08MarriedValue As Double
;09MaxCompRate As Double
;10MaxPercentOfSAWW As Double
;11PayConCurrentPPD As Long
;12PrimeRate As Double
;13SingleValue As Double
;14TotalAmount As Double
;15TotalWeeks As Long
;16UseSAWWMaximumCode As Long
;17UseSAWWMinimumCode As Long
;                          1   2        3      4      5      6      7   8      9     10 
[ASSIGN %%4=ADD_WCP_TTD_IL(%%1,20000115,108.30,113.40,117.40,124.30,%%2,105.50,00.00,00.00,%%3,66.67,100.90,00.00,00,%%2,%%2)]
