;Function Layout, ADD_WCP_BODY_MEMBER
;iStateRowID = Val(stmpArray(1)), _
;EffectiveDateDTG = Trim(stmpArray(2)), _
;sBodyMemberDesc = Trim(stmpArray(3)), _
;dMaxAmount = danyvartodoulbe(stmpArray(4)), _
;dWeeks = dAnyvarTodouble(stmpArray(5)), _
;dMonths = dAnyvarToDouble(stmpArray(6)), _
;dAmputationPercentRate = dAnyvartoDouble(stmpArray(7))
;#################################################################################
;#################################################################################
;*********************************************************************************
;Arkansas
[ASSIGN %%1=ADD_JURISDICTION(AR,Arkansas,0,0,0,0,0,0)]
;11-9-521. Compensation for disability--Scheduled permanent injuries.
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,An employee who sustains a permanent compensable injury scheduled in this
;subsection shall receive in addition to compensation for temporary total and temporary partial benefits during
; the healing period or until the employee returns to work, whichever occurs first,
; weekly benefits in the amount of the permanent partial disability rate attributable to the injury,
; for that period of time set out in the following schedule 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Arm (total loss of use or) amputated at the elbow-or between the elbow and shoulder,0,244,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Arm (total loss of use or) amputated between the elbow and wrist,0,183,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Leg (total loss of use or) amputated at the knee-or between the knee and the hip,0,184,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Leg (total loss of use or) amputated between the knee and the ankle,0, 131,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Hand (total loss of use or) amputated,0, 183,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Thumb (total loss of use or) amputated,0,73,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Thumb (total loss of use or) amputation of the first phalange,0,36.5,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Thumb (total loss of use or) amputation of the second or more phalange,0,73,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,First finger (total loss of use or) amputated,0,43,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,First finger (total loss of use or) amputation of the first phalange,0,21.5,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,First finger (total loss of use or) amputation of the second or more phalange,0,43,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Second finger (total loss of use or) amputated,0,37,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Second finger (total loss of use or) amputation of the first phalange,0,18.5,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Second finger (total loss of use or) amputation of the second or more phalange,0,37,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Third finger (total loss of use or) amputated,0, 24,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Third finger (total loss of use or) amputation of the first phalange,0, 12,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Third finger (total loss of use or) amputation of the second or more phalange,0, 24,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Fourth finger (total loss of use or) amputated,0,19,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Fourth finger (total loss of use or) amputation of the first phalange,0,8.5,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Fourth finger (total loss of use or) amputation of the second or more phalange,0,19,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Foot (total loss of use or) amputated,0,131,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Great toe (total loss of use or) amputated,0,32,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Toe other than great toe (total loss of use or) amputated,0,11,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Eye enucleated- in which there was useful vision,0, 105,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Loss of hearing of one ear,0,42,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Loss of hearing of both ears,0,158,0,0)] 
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Loss of one testicle,0,53,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Loss of both testicles,0, 158,0,0)] 
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,permanent loss of eighty percent (80%) or more of the vision of an eye,0, 105,0,0)] 
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,
;Compensation for amputation or loss of use of two (2) or more digits or one (1) or more phalanges of two (2) or more digits of a hand or a foot may be proportioned to the total loss of use of the hand or the foot occasioned thereby but shall not exceed the compensation for total loss of a hand or a foot 
;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,
;Compensation for permanent partial loss or loss of use of a member shall be for the proportionate loss or loss of use of the member 
; set forth above except as otherwise provided in a section 11-9-519(b) 

