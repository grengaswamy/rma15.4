;Function Layout, ADD_WCP_BODY_MEMBER
;iStateRowID = Val(stmpArray(1)), _
;EffectiveDateDTG = Trim(stmpArray(2)), _
;sBodyMemberDesc = Trim(stmpArray(3)), _
;dMaxAmount = danyvartodoulbe(stmpArray(4)), _
;dWeeks = dAnyvarTodouble(stmpArray(5)), _
;dMonths = dAnyvarToDouble(stmpArray(6)), _
;dAmputationPercentRate = dAnyvartoDouble(stmpArray(7))
;#################################################################################
;#################################################################################
;*********************************************************************************
;Alabama
;Code of Alabama; Section 25-5-57
[ASSIGN %%1=ADD_JURISDICTION(AL,Alabama,0,0,0,0,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a thumb,0,62,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of the first phalange of the thumb,0,31,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two or more phalanges of a thumb,0,62,0,0)]
;
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a first finger-commonly called the index finger,0,43,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of the first phalange of a first finger-commonly called the index finger,0,21.5,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two or more phalanges of a first finger-commonly called the index finger,0,43,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a second finger,0,31,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of the first phalange of a second finger,0,15.5,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two or more phalanges of a second finger,0,31,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a third finger,0,22,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of the first phalange of a third finger,0,11,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two or more phalanges of a third finger,0,22,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a fourth finger-commonly called the little finger,0,16,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of the first phalange of a fourth finger-commonly called the little finger,0,8,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two or more phalanges of a fourth finger-commonly called the little finger,0,16,0,0)]
;
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a great toe,0,32,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of the first phalange of a great toe,0,16,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two or more phalanges of a great toe,0,32,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of any of the toes other than the great toe,0,11,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of the first phalange of any toes other than the great toe,0,5.5,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two or more phalanges of any toes other than the great toe,0,11,0,0)]
;
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a hand,0,170,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,Amputation between the elbow and wrist is loss of a hand,0,170,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of an arm,0,222,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a foot,0,139,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,amputation between the knee and ankle is loss of a foot,0,139,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of a leg,0,200,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of an eye,0,124,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,complete and permanent loss of hearing in both ears,0,163,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,complete and permanent loss of hearing in one ear,0,53,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of an eye and a leg,0,350,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of an eye and one arm,0,350,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of an eye and a hand,0,325,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of an eye and a foot,0,300,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two arms-other than at the shoulder,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two hands,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two legs,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of two feet,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of one arm and the other hand,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of one hand and one foot,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of one leg and the other foot,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of one hand and one leg,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of one arm and one foot,0,400,0,0)]
[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,20000101,loss of one arm and one leg,0,400,0,0)]

;[ASSIGN %%2=ADD_WCP_BODY_MEMBER(%%1,serious disfigurement,
; not resulting from the loss of a member or other injury specifically compensated,
; materially affecting the employability of the injured person in the employment 
; in which he or she was injured or other employment for which he or she is then qualified,
; 66 2/3 percent of the average weekly earnings for the period as the court may determine,
; but not exceeding 100 weeks.
