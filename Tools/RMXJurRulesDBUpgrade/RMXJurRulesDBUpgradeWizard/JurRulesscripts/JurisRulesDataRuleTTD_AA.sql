;##################################################################################
;WCP_RULE_TDD
[ASSIGN %%1= QUERY(SELECT MAX(TABLE_ROW_ID) + 1 FROM WCP_RULE_TTD)]
[FORGIVE] UPDATE GLOSSARY SET NEXT_UNIQUE_ID = %%1 WHERE SYSTEM_TABLE_NAME = 'WCP_RULE_TTD'
[ASSIGN %%2=ADD_CODE(Y,1033,Yes,YES_NO,0)]
[ASSIGN %%3=ADD_CODE(N,1033,No,YES_NO,0)]
;.DollarForDollar = Val(stmpArray(1))
;.EffectiveDateDTG = Trim$(stmpArray(2))
;.EndingDateDTG = Trim$(stmpArray(3))
;.FloorAmount = CDbl(stmpArray(4))
;.JurisRowID = Val(stmpArray(5))
;.MaxAWW = CDbl(stmpArray(6))
;.MaxCompRate = cdbl(stmpArray(7))
;.PayConCurrentPPD = Val(stmpArray(8)) %%3(No in California)
;.PayCurrentRateAfterTwoYears = Val(stmpArray(9)) %%2(Yes in California)
;.PayFloorAmount = Val(stmpArray(10)) %%2(Yes in California
;.PercentSAWW = CDbl(stmpArray(11))
;.PrimeRate = CDbl(stmpArray(12))
;.PrimeRateMaxAmount = CDbl(stmpArray(13))
;.PrimeRateMaxWeeks = CDbl(stmpArray(14))
;.SecondRate = CDbl(stmpArray(15))
;.SecondRateMaxAmount = CDbl(stmpArray(16))
;.SecondRateMaxWeeks = CDbl(stmpArray(17))
;.TotalAmount = CDbl(stmpArray(18))
;.TotalWeeks = CDbl(stmpArray(19))
;.UseSAWWMax = CLng(stmpArray(20))
;.UseSAWWMin = CLng(stmpArray(21))
;                                                                                   1   1    1  1   1 1 1  1  1 1 1
;                               1   2        3        4     5  6      7     8   9   0   1    2      3 4 5  6  7 8 9
;*********************************************************************************
