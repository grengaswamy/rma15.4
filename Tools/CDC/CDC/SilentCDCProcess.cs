﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Security;

namespace Riskmaster.Tools.CDC
{
    class SilentCDCProcess
    {
        public static bool Validation(string[] args)
        {
            string strAdminDBUId = string.Empty;
            string strAdminDBPWD = string.Empty;

            InitializeParameters(args);

            //MITS 22996 Ignore password when called from TaskManager
            AppGlobal.UserLogin = new UserLogin(AppGlobal.User, AppGlobal.DSN,AppGlobal.ClientId);//Add clientId by kuladeep for Cloud Jira-1124
            if (AppGlobal.UserLogin.DatabaseId <= 0)
            {
                throw new Exception(Message.AuthentiCationFailureMsg);
            }
            else
            {
                AppGlobal.RMDBConnstring = AppGlobal.UserLogin.objRiskmasterDatabase.ConnectionString;                
                AppGlobal.DatabaseType = AppGlobal.UserLogin.objRiskmasterDatabase.DbType;
                Common.GetDatabaseNameAndID();
                
                if (!string.IsNullOrEmpty(AppGlobal.CDCAdminUID) && !string.IsNullOrEmpty(AppGlobal.CDCAdminPwd))
                {

                    Common.CreateAdminConnStr(AppGlobal.CDCAdminUID, AppGlobal.CDCAdminPwd);
                }
                else
                {
                    AppGlobal.RMAdminConnstring = AppGlobal.RMDBConnstring;
                    Common.GetUIdPwdFromConString(AppGlobal.RMAdminConnstring, ref strAdminDBUId, ref strAdminDBPWD);
                    AppGlobal.CDCAdminUID = strAdminDBUId;
                    AppGlobal.CDCAdminPwd = strAdminDBPWD;

                }
                if (AppGlobal.DatabaseType == Riskmaster.Db.eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    Common.SetRMDBO();
                }
                else
                {
                    AppGlobal.DBOUserId = AppGlobal.UserLogin.objRiskmasterDatabase.RMUserId;
                }
            }

            if (AppGlobal.SubTaskValue == "1")
            {
                CDCManager.PopulateAuditTables();
                CDCManager.ProcessCDCSetup();
            }

            if (AppGlobal.SubTaskValue == "2")
            {
                CDCManager.PurgeDataFromAuditTables();
            }

            if (AppGlobal.SubTaskValue == "3" || AppGlobal.SubTaskValue == "")
            {
                CDCManager.PopulateAuditTables();
                CDCManager.ProcessCDCSetup();
                CDCManager.PurgeDataFromAuditTables();
            }
            
            
            
            Common.FinishCDCProcess();
            if (AppGlobal.IsError == true)
                Common.DisplayMessage(Message.CDCManagerProcessAborted, Mode.Error);
            return true;
        }

        private static void InitializeParameters(string[] p_args)
        {//intialize all the appglobal parmeters with
            //command line arguments.
            int iLength = 0;
            string sPrefix = string.Empty;
            string sRebuildAll = string.Empty;
            iLength = p_args.Length;

            for (int i = 0; i < iLength; i++)
            {

                sPrefix = p_args[i].Trim();

                if (sPrefix.Length > 3)
                {
                    sPrefix = sPrefix.Substring(0, 3);
                }
                switch (sPrefix.ToLower())
                {
                    case "-ds":
                        AppGlobal.DSN = p_args[i].Trim().Substring(3);
                        break;
                    case "-ru":
                        AppGlobal.User = p_args[i].Trim().Substring(3);
                        break;
                    case "-rp":
                        AppGlobal.Password = p_args[i].Trim().Substring(3);
                        break;
                    case "-au":
                        AppGlobal.CDCAdminUID = p_args[i].Trim().Substring(3);
                        break;
                    case "-ap":
                        AppGlobal.CDCAdminPwd = p_args[i].Trim().Substring(3);
                        break;
                    case "-st":
                        AppGlobal.SubTaskValue = p_args[i].Trim().Substring(3);
                        break;
                    case "-ci"://Add clientId by kuladeep for Cloud Jira-1124
                        AppGlobal.ClientId = Convert.ToInt32(p_args[i].Trim().Substring(3));
                        break;
                        

                }
            }


        }

       
       
    }
}
