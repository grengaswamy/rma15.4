﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Xsl;

using DevComponents.DotNetBar;

using Riskmaster.Application.RMUtilities;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Models;



namespace Riskmaster.Tools.CDC
{
    public partial class frmCDC : Office2007Form
    {
        #region variables

        //Code comment by kuladeep for Cloud Jira-1124 Start
        private static string[] sDataSeparator = { "|^^|" };
        //RMAuthentication.AuthenticationServiceClient authService = null;
        //Code comment by kuladeep for Cloud Jira-1124 End
        private string m_sWarningMessage = String.Empty;
        
        
        private static string m_DSN = string.Empty;
        
        private static string sLogXml = string.Empty;
        
        private string m_strUser = string.Empty;
        private string m_strPassword = string.Empty;
        private string m_RMXSecConnString = string.Empty;
      
        #endregion

        /// <summary>
        /// frmCDCWizard
        /// </summary>
        /// <param name="strSecConnString"></param>
        
        public frmCDC(string strSecConnString)
        {
            m_RMXSecConnString = strSecConnString;
            InitializeComponent();
        }

        /// <summary>
        /// InitializeComponent
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.wizard1 = new DevComponents.DotNetBar.Wizard();
            this.wpConfirmation = new DevComponents.DotNetBar.WizardPage();
            this.lblError = new DevComponents.DotNetBar.LabelX();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.wpDSNSelection = new DevComponents.DotNetBar.WizardPage();
            this.lblWarningWPDS = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.lvDatabases = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.dsn = new System.Windows.Forms.ColumnHeader();
            this.id = new System.Windows.Forms.ColumnHeader();
            this.lstDsnList = new System.Windows.Forms.ListBox();
            this.wizardAdminCredentials = new DevComponents.DotNetBar.WizardPage();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.txtAdminDBId = new System.Windows.Forms.TextBox();
            this.txtAdminDBPwd = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.wpStatus = new DevComponents.DotNetBar.WizardPage();
            this.labelSubStatus = new DevComponents.DotNetBar.LabelX();
            this.labelX18 = new DevComponents.DotNetBar.LabelX();
            this.lblStatus = new DevComponents.DotNetBar.LabelX();
            this.labelX16 = new DevComponents.DotNetBar.LabelX();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.progressBarCDC = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.timerCDC = new System.Windows.Forms.Timer(this.components);
            this.wizard1.SuspendLayout();
            this.wpConfirmation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.wpDSNSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.wizardAdminCredentials.SuspendLayout();
            this.wpStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // wizard1
            // 
            this.wizard1.BackColor = System.Drawing.Color.White;
            this.wizard1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.wizard1.ButtonStyle = DevComponents.DotNetBar.eWizardStyle.Office2007;
            this.wizard1.Cursor = System.Windows.Forms.Cursors.Default;
            this.wizard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizard1.FinishButtonTabIndex = 3;
            // 
            // 
            // 
            this.wizard1.FooterStyle.BackColor = System.Drawing.Color.White;
            this.wizard1.FooterStyle.BackColorGradientAngle = 90;
            this.wizard1.FooterStyle.BorderBottomWidth = 1;
            this.wizard1.FooterStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.wizard1.FooterStyle.BorderLeftWidth = 1;
            this.wizard1.FooterStyle.BorderRightWidth = 1;
            this.wizard1.FooterStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Etched;
            this.wizard1.FooterStyle.BorderTopColor = System.Drawing.SystemColors.Control;
            this.wizard1.FooterStyle.BorderTopWidth = 1;
            this.wizard1.FooterStyle.Class = "";
            this.wizard1.FooterStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.wizard1.FooterStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.wizard1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(57)))), ((int)(((byte)(129)))));
            this.wizard1.HeaderCaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.wizard1.HeaderDescriptionVisible = false;
            this.wizard1.HeaderHeight = 90;
            this.wizard1.HeaderImage = global::Riskmaster.Tools.CDC.Properties.Resources.WizardBanner4;
            this.wizard1.HeaderImageAlignment = DevComponents.DotNetBar.eWizardTitleImageAlignment.Left;
            this.wizard1.HeaderImageSize = new System.Drawing.Size(688, 52);
            // 
            // 
            // 
            this.wizard1.HeaderStyle.BackColor = System.Drawing.Color.Transparent;
            this.wizard1.HeaderStyle.BackColorGradientAngle = 90;
            this.wizard1.HeaderStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Etched;
            this.wizard1.HeaderStyle.BorderBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(157)))), ((int)(((byte)(182)))));
            this.wizard1.HeaderStyle.BorderBottomWidth = 1;
            this.wizard1.HeaderStyle.BorderColor = System.Drawing.SystemColors.Control;
            this.wizard1.HeaderStyle.BorderLeftWidth = 1;
            this.wizard1.HeaderStyle.BorderRightWidth = 1;
            this.wizard1.HeaderStyle.BorderTopWidth = 1;
            this.wizard1.HeaderStyle.Class = "";
            this.wizard1.HeaderStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.wizard1.HeaderStyle.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.wizard1.HeaderTitleIndent = 702;
            this.wizard1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F";
            this.wizard1.Location = new System.Drawing.Point(0, 0);
            this.wizard1.Name = "wizard1";
            this.wizard1.Size = new System.Drawing.Size(700, 550);
            this.wizard1.TabIndex = 0;
            this.wizard1.WizardPages.AddRange(new DevComponents.DotNetBar.WizardPage[] {
            this.wpConfirmation,
            this.wpDSNSelection,
            this.wizardAdminCredentials,
            this.wpStatus});
            this.wizard1.CancelButtonClick += new System.ComponentModel.CancelEventHandler(this.wizard1_CancelButtonClick);
            this.wizard1.WizardPageChanging += new DevComponents.DotNetBar.WizardCancelPageChangeEventHandler(this.wizard1_WizardPageChanging);
            this.wizard1.FinishButtonClick += new System.ComponentModel.CancelEventHandler(this.wizard1_FinishButtonClick);
            // 
            // wpConfirmation
            // 
            this.wpConfirmation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpConfirmation.BackColor = System.Drawing.Color.Transparent;
            this.wpConfirmation.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpConfirmation.Controls.Add(this.lblError);
            this.wpConfirmation.Controls.Add(this.txtUserName);
            this.wpConfirmation.Controls.Add(this.txtPassword);
            this.wpConfirmation.Controls.Add(this.lblUserName);
            this.wpConfirmation.Controls.Add(this.lblPassword);
            this.wpConfirmation.Controls.Add(this.labelX8);
            this.wpConfirmation.Controls.Add(this.pictureBox1);
            this.wpConfirmation.Controls.Add(this.labelX1);
            this.wpConfirmation.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpConfirmation.Location = new System.Drawing.Point(7, 102);
            this.wpConfirmation.Name = "wpConfirmation";
            this.wpConfirmation.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpConfirmation.Style.BackColor = System.Drawing.Color.White;
            this.wpConfirmation.Style.BackgroundImagePosition = DevComponents.DotNetBar.eStyleBackgroundImage.TopLeft;
            this.wpConfirmation.Style.Class = "";
            // 
            // 
            // 
            this.wpConfirmation.StyleMouseDown.Class = "";
            // 
            // 
            // 
            this.wpConfirmation.StyleMouseOver.Class = "";
            this.wpConfirmation.TabIndex = 0;
            // 
            // lblError
            // 
            // 
            // 
            // 
            this.lblError.BackgroundStyle.Class = "";
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(62, 292);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(455, 23);
            this.lblError.TabIndex = 23;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(153, 229);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 20);
            this.txtUserName.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(153, 255);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(100, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblUserName.Location = new System.Drawing.Point(58, 229);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(89, 20);
            this.lblUserName.TabIndex = 1;
            this.lblUserName.Text = "User Name";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblPassword.Location = new System.Drawing.Point(58, 255);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(78, 20);
            this.lblPassword.TabIndex = 2;
            this.lblPassword.Text = "Password";
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.Class = "";
            this.labelX8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.ForeColor = System.Drawing.Color.Silver;
            this.labelX8.Location = new System.Drawing.Point(493, 0);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(99, 23);
            this.labelX8.TabIndex = 5;
            this.labelX8.Text = "Confirmation";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Riskmaster.Tools.CDC.Properties.Resources.Database;
            this.pictureBox1.Location = new System.Drawing.Point(528, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(61, 2);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(445, 198);
            this.labelX1.TabIndex = 3;
            this.labelX1.Text = "This wizard will guide you through tracking History of Changes  in RISKMASTER Tab" +
                "les.<br/><br/>Computer Sciences Corporation cannot be held responsible for unaut" +
                "horized use of this program.<br/><br/>";
            this.labelX1.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.labelX1.WordWrap = true;
            // 
            // wpDSNSelection
            // 
            this.wpDSNSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpDSNSelection.AntiAlias = false;
            this.wpDSNSelection.BackColor = System.Drawing.Color.Transparent;
            this.wpDSNSelection.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.wpDSNSelection.Controls.Add(this.lblWarningWPDS);
            this.wpDSNSelection.Controls.Add(this.labelX3);
            this.wpDSNSelection.Controls.Add(this.labelX9);
            this.wpDSNSelection.Controls.Add(this.pictureBox2);
            this.wpDSNSelection.Controls.Add(this.labelX2);
            this.wpDSNSelection.Controls.Add(this.lvDatabases);
            this.wpDSNSelection.Controls.Add(this.lstDsnList);
            this.wpDSNSelection.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpDSNSelection.Location = new System.Drawing.Point(7, 102);
            this.wpDSNSelection.Name = "wpDSNSelection";
            this.wpDSNSelection.PageDescription = "Select a Database to Upgrade";
            this.wpDSNSelection.PageTitle = "Database Selection Screen";
            this.wpDSNSelection.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpDSNSelection.Style.Class = "";
            // 
            // 
            // 
            this.wpDSNSelection.StyleMouseDown.Class = "";
            // 
            // 
            // 
            this.wpDSNSelection.StyleMouseOver.Class = "";
            this.wpDSNSelection.TabIndex = 7;
            // 
            // lblWarningWPDS
            // 
            // 
            // 
            // 
            this.lblWarningWPDS.BackgroundStyle.Class = "";
            this.lblWarningWPDS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarningWPDS.Location = new System.Drawing.Point(90, 360);
            this.lblWarningWPDS.Name = "lblWarningWPDS";
            this.lblWarningWPDS.Size = new System.Drawing.Size(531, 23);
            this.lblWarningWPDS.TabIndex = 16;
            this.lblWarningWPDS.Text = "<font color=\"#BA1419\">You must select a database to continue.....</font>";
            this.lblWarningWPDS.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblWarningWPDS.Visible = false;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.Class = "";
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.Location = new System.Drawing.Point(90, 338);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(531, 23);
            this.labelX3.TabIndex = 15;
            this.labelX3.Text = "Press <b>Next</b> to continue....";
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.Class = "";
            this.labelX9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX9.ForeColor = System.Drawing.Color.Silver;
            this.labelX9.Location = new System.Drawing.Point(438, 3);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(154, 23);
            this.labelX9.TabIndex = 14;
            this.labelX9.Text = "Database Selection";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Riskmaster.Tools.CDC.Properties.Resources.DatabaseSearch;
            this.pictureBox2.Location = new System.Drawing.Point(528, 29);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(64, 64);
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.Location = new System.Drawing.Point(90, 19);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(393, 23);
            this.labelX2.TabIndex = 12;
            this.labelX2.Text = "Select dB to <b>Track History of Changes</b>";
            // 
            // lvDatabases
            // 
            // 
            // 
            // 
            this.lvDatabases.Border.BorderColor = System.Drawing.Color.LightGray;
            this.lvDatabases.Border.Class = "ListViewBorder";
            this.lvDatabases.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dsn,
            this.id});
            this.lvDatabases.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvDatabases.ForeColor = System.Drawing.Color.Sienna;
            this.lvDatabases.FullRowSelect = true;
            this.lvDatabases.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvDatabases.Location = new System.Drawing.Point(90, 48);
            this.lvDatabases.MultiSelect = false;
            this.lvDatabases.Name = "lvDatabases";
            this.lvDatabases.Size = new System.Drawing.Size(405, 284);
            this.lvDatabases.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvDatabases.TabIndex = 11;
            this.lvDatabases.UseCompatibleStateImageBehavior = false;
            this.lvDatabases.View = System.Windows.Forms.View.Details;
            this.lvDatabases.Click += new System.EventHandler(this.lvDatabases_Click);
            // 
            // dsn
            // 
            this.dsn.Width = 405;
            // 
            // id
            // 
            this.id.Width = 0;
            // 
            // lstDsnList
            // 
            this.lstDsnList.Location = new System.Drawing.Point(90, 103);
            this.lstDsnList.Name = "lstDsnList";
            this.lstDsnList.ScrollAlwaysVisible = true;
            this.lstDsnList.Size = new System.Drawing.Size(405, 199);
            this.lstDsnList.Sorted = true;
            this.lstDsnList.TabIndex = 10;
            this.lstDsnList.Visible = false;
            // 
            // wizardAdminCredentials
            // 
            this.wizardAdminCredentials.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wizardAdminCredentials.BackColor = System.Drawing.Color.White;
            this.wizardAdminCredentials.Controls.Add(this.labelX4);
            this.wizardAdminCredentials.Controls.Add(this.txtAdminDBId);
            this.wizardAdminCredentials.Controls.Add(this.txtAdminDBPwd);
            this.wizardAdminCredentials.Controls.Add(this.label1);
            this.wizardAdminCredentials.Controls.Add(this.label2);
            this.wizardAdminCredentials.Location = new System.Drawing.Point(7, 102);
            this.wizardAdminCredentials.Name = "wizardAdminCredentials";
            this.wizardAdminCredentials.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wizardAdminCredentials.Style.Class = "";
            // 
            // 
            // 
            this.wizardAdminCredentials.StyleMouseDown.Class = "";
            // 
            // 
            // 
            this.wizardAdminCredentials.StyleMouseOver.Class = "";
            this.wizardAdminCredentials.TabIndex = 13;
            this.wizardAdminCredentials.Text = "wizardADminCredentials";
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.Class = "";
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.Location = new System.Drawing.Point(169, 64);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(393, 23);
            this.labelX4.TabIndex = 13;
            this.labelX4.Text = "Please enter ADMIN dB credentials.";
            // 
            // txtAdminDBId
            // 
            this.txtAdminDBId.Location = new System.Drawing.Point(295, 148);
            this.txtAdminDBId.Name = "txtAdminDBId";
            this.txtAdminDBId.Size = new System.Drawing.Size(100, 20);
            this.txtAdminDBId.TabIndex = 4;
            // 
            // txtAdminDBPwd
            // 
            this.txtAdminDBPwd.Location = new System.Drawing.Point(295, 174);
            this.txtAdminDBPwd.Name = "txtAdminDBPwd";
            this.txtAdminDBPwd.PasswordChar = '*';
            this.txtAdminDBPwd.Size = new System.Drawing.Size(100, 20);
            this.txtAdminDBPwd.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(165, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "DB User Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(165, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "DB Password";
            // 
            // wpStatus
            // 
            this.wpStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.wpStatus.BackColor = System.Drawing.Color.White;
            this.wpStatus.CancelButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpStatus.Controls.Add(this.labelSubStatus);
            this.wpStatus.Controls.Add(this.labelX18);
            this.wpStatus.Controls.Add(this.lblStatus);
            this.wpStatus.Controls.Add(this.labelX16);
            this.wpStatus.Controls.Add(this.pictureBox6);
            this.wpStatus.Controls.Add(this.progressBarCDC);
            this.wpStatus.HelpButtonVisible = DevComponents.DotNetBar.eWizardButtonState.False;
            this.wpStatus.Location = new System.Drawing.Point(7, 102);
            this.wpStatus.Name = "wpStatus";
            this.wpStatus.Size = new System.Drawing.Size(682, 386);
            // 
            // 
            // 
            this.wpStatus.Style.Class = "";
            // 
            // 
            // 
            this.wpStatus.StyleMouseDown.Class = "";
            // 
            // 
            // 
            this.wpStatus.StyleMouseOver.Class = "";
            this.wpStatus.TabIndex = 12;
            this.wpStatus.Text = "wizardPage1";
            // 
            // labelSubStatus
            // 
            // 
            // 
            // 
            this.labelSubStatus.BackgroundStyle.Class = "";
            this.labelSubStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubStatus.Location = new System.Drawing.Point(90, 157);
            this.labelSubStatus.Name = "labelSubStatus";
            this.labelSubStatus.Size = new System.Drawing.Size(455, 23);
            this.labelSubStatus.TabIndex = 24;
            // 
            // labelX18
            // 
            // 
            // 
            // 
            this.labelX18.BackgroundStyle.Class = "";
            this.labelX18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX18.Location = new System.Drawing.Point(90, 231);
            this.labelX18.Name = "labelX18";
            this.labelX18.Size = new System.Drawing.Size(455, 121);
            this.labelX18.TabIndex = 23;
            this.labelX18.Text = "When complete press <b>Back </b>to return to <i>Database Selection</i> screen, or" +
                " <b>Finish </b>to close application.";
            // 
            // lblStatus
            // 
            // 
            // 
            // 
            this.lblStatus.BackgroundStyle.Class = "";
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(90, 114);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(455, 23);
            this.lblStatus.TabIndex = 22;
            // 
            // labelX16
            // 
            // 
            // 
            // 
            this.labelX16.BackgroundStyle.Class = "";
            this.labelX16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX16.ForeColor = System.Drawing.Color.Silver;
            this.labelX16.Location = new System.Drawing.Point(439, 0);
            this.labelX16.Name = "labelX16";
            this.labelX16.Size = new System.Drawing.Size(154, 23);
            this.labelX16.TabIndex = 21;
            this.labelX16.Text = "Processing";
            this.labelX16.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Riskmaster.Tools.CDC.Properties.Resources.DatabaseEdit;
            this.pictureBox6.Location = new System.Drawing.Point(529, 29);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(64, 64);
            this.pictureBox6.TabIndex = 20;
            this.pictureBox6.TabStop = false;
            // 
            // progressBarCDC
            // 
            this.progressBarCDC.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.progressBarCDC.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.progressBarCDC.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.progressBarCDC.BackgroundStyle.BorderBottomWidth = 1;
            this.progressBarCDC.BackgroundStyle.BorderColor = System.Drawing.Color.LightGray;
            this.progressBarCDC.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.progressBarCDC.BackgroundStyle.BorderLeftWidth = 1;
            this.progressBarCDC.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.progressBarCDC.BackgroundStyle.BorderRightWidth = 1;
            this.progressBarCDC.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.progressBarCDC.BackgroundStyle.BorderTopWidth = 1;
            this.progressBarCDC.BackgroundStyle.Class = "";
            this.progressBarCDC.ChunkColor = System.Drawing.Color.CornflowerBlue;
            this.progressBarCDC.ChunkColor2 = System.Drawing.Color.White;
            this.progressBarCDC.ChunkGradientAngle = 90;
            this.progressBarCDC.Location = new System.Drawing.Point(90, 186);
            this.progressBarCDC.Name = "progressBarCDC";
            this.progressBarCDC.Size = new System.Drawing.Size(508, 28);
            this.progressBarCDC.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.progressBarCDC.TabIndex = 19;
            // 
            // timerCDC
            // 
            this.timerCDC.Tick += new System.EventHandler(this.timerCDC_Tick);
            // 
            // frmCDC
            // 
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(700, 550);
            this.Controls.Add(this.wizard1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCDC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CDC Wizard";
            this.Load += new System.EventHandler(this.frmBESRebuild_Load);
            this.wizard1.ResumeLayout(false);
            this.wpConfirmation.ResumeLayout(false);
            this.wpConfirmation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.wpDSNSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.wizardAdminCredentials.ResumeLayout(false);
            this.wizardAdminCredentials.PerformLayout();
            this.wpStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);

        }

        /// <summary>
        /// Loads the contents of the PowerView upgrade form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmBESRebuild_Load(object sender, EventArgs e)
        {
            //Code comment by kuladeep for Cloud Jira-1124 Start
            //authService = new RMAuthentication.AuthenticationServiceClient();
            //Code comment by kuladeep for Cloud Jira-1124 end
        }


        /// <summary>
        /// Event Handler for when the Finish button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_FinishButtonClick(object sender, CancelEventArgs e)
        {
            //Close the form
            Close();

            //Remove the application from memory
            System.Windows.Forms.Application.Exit();
        }

        /// <summary>
        /// Event Handled for when the Cancel button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_CancelButtonClick(object sender, CancelEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        /// <summary>
        /// handles page changing events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_WizardPageChanging(object sender, WizardCancelPageChangeEventArgs e)
        {
            //shut off(reset) warning labels while navigating
            Login objLogin = null;
            
            bool blnIsAuthenticated = false;
            bool bCDCAdimn = false;
            lblError.Text = string.Empty;
            string sSql = string.Empty;
            string sAdminDBId = string.Empty;
            string sAdminDBPwd = string.Empty;
            //Change by kuladeep for Cloud-Jira-1124 Start
            AuthData oAuthData = null;
            string sOutResult = string.Empty;
            //Change by kuladeep for Cloud-Jira-1124 End

            if (e.OldPage == wpConfirmation && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                try
                {
                    AppGlobal.User = txtUserName.Text.Trim();
                    AppGlobal.Password = txtPassword.Text.Trim();
                    if (AppGlobal.User== string.Empty)
                    {
                        e.Cancel = true;
                        Common.UpdateStatus("User Name can not be blank. Please try again. ",Mode.Error);
                        txtUserName.Focus();
                    }
                    else if (AppGlobal.Password == string.Empty)
                    {
                        e.Cancel = true;
                        Common.UpdateStatus("Password can not be blank. Please try again. ", Mode.Error);
                        txtPassword.Focus();
                    }
                    else
                    {

                        oAuthData = new AuthData();
                        oAuthData.UserName = AppGlobal.User;
                        oAuthData.Password = AppGlobal.Password;
                        oAuthData.ClientId = AppGlobal.ClientId;

                        //Change by kuladeep for Cloud-Jira-1124 Start
                        //blnIsAuthenticated = authService.AuthenticateUser(AppGlobal.User, AppGlobal.Password, out m_sWarningMessage);
                        sOutResult = AppGlobal.GetResponse<string>("RMService/Authenticate/login", "POST", "application/json", oAuthData);
                        blnIsAuthenticated = Convert.ToBoolean(sOutResult.Split(sDataSeparator, StringSplitOptions.None)[0]);
                        //Change by kuladeep for Cloud-Jira-1124 End

                        if (!blnIsAuthenticated)
                        {
                            e.Cancel = true;
                            Common.UpdateStatus("Your login attempt was not successful. Please try again. ", Mode.Error);
                            txtPassword.Text = string.Empty;
                        }

                        else
                        {
                            //Change by kuladeep for Cloud-Jira-1124 Start   
                            //GetBindingCollection(authService.GetUserDSNs(AppGlobal.User));
                            GetBindingCollection(AppGlobal.GetResponse<Dictionary<string,string>>("RMService/Authenticate/Dsn","POST", "application/json", oAuthData));
                            //Change by kuladeep for Cloud-Jira-1124 End
                        }
                    }
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message, ex);
                }
                
            }

            if (e.OldPage == wpDSNSelection && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                try
                {
                    //show warning if nothing is selected and stop navigation
                    if (lvDatabases.SelectedItems.Count == 0)
                    {
                        lblWarningWPDS.Visible = true;
                        e.Cancel = true;
                    }

                    else
                    {
                        objLogin = new Login(m_RMXSecConnString, AppGlobal.ClientId); //Add clientId by kuladeep for Cloud Jira-1124
                        blnIsAuthenticated = objLogin.AuthenticateUser(lvDatabases.SelectedItems[0].Text, AppGlobal.User, AppGlobal.Password, out AppGlobal.UserLogin, AppGlobal.ClientId);//Add clientId by kuladeep for Cloud Jira-1124

                        if (blnIsAuthenticated)
                        {
                            AppGlobal.DSN = AppGlobal.UserLogin.objRiskmasterDatabase.DataSourceName;
                            AppGlobal.RMDBConnstring = AppGlobal.UserLogin.objRiskmasterDatabase.ConnectionString;
                            AppGlobal.DBOUserId = AppGlobal.UserLogin.objRiskmasterDatabase.RMUserId;
                            AppGlobal.DatabaseType = AppGlobal.UserLogin.objRiskmasterDatabase.DbType;
                            Common.GetDatabaseNameAndID();
                            bCDCAdimn = ValidateandGetCDCAdminCredential(AppGlobal.RMDBConnstring);
                            if (bCDCAdimn)
                            {
                                AppGlobal.RMAdminConnstring = AppGlobal.RMDBConnstring;
                                if (AppGlobal.DatabaseType == Riskmaster.Db.eDatabaseType.DBMS_IS_SQLSRVR)
                                {
                                    Common.SetRMDBO();
                                }
                                else
                                {
                                    AppGlobal.DBOUserId = AppGlobal.UserLogin.objRiskmasterDatabase.RMUserId;
                                }
                                Common.GetUIdPwdFromConString(AppGlobal.RMAdminConnstring, ref sAdminDBId, ref sAdminDBPwd);
                                AppGlobal.CDCAdminUID = sAdminDBId;
                                wizard1.SelectedPage = wpStatus;

                                this.Cursor = Cursors.WaitCursor;
                                Common.StartAutoProgreeBar();
                                CDCManager.PopulateAuditTables();
                                CDCManager.ProcessCDCSetup();
                                CDCManager.PurgeDataFromAuditTables();
                                Common.StopAutoProgreeBar();
                                Common.FinishCDCProcess();
                                if(AppGlobal.IsError ==true)
                                    Common.DisplayMessage(Message.CDCManagerProcessAborted, Mode.Error);
                                e.Cancel = true;
                                this.Cursor = Cursors.Default;
                            }
                            else
                            {
                                wizard1.SelectedPage = wizardAdminCredentials;
                                e.Cancel = true;
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    Common.StopAutoProgreeBar();
                    Common.FinishCDCProcess();
                    Common.DisplayMessage(ex.Message, Mode.Error);
                    e.Cancel = true;
                }

            }

            if (e.OldPage == wizardAdminCredentials && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                try
                {
                    sAdminDBId = txtAdminDBId.Text.Trim();
                    sAdminDBPwd = txtAdminDBPwd.Text.Trim();
                    Common.CreateAdminConnStr(sAdminDBId, sAdminDBPwd);
                    bCDCAdimn = ValidateandGetCDCAdminCredential(AppGlobal.RMAdminConnstring);
                    if (bCDCAdimn)
                    {
                        AppGlobal.CDCAdminUID = sAdminDBId;
                        if (AppGlobal.DatabaseType == Riskmaster.Db.eDatabaseType.DBMS_IS_SQLSRVR)
                        {
                            Common.SetRMDBO();
                        }
                        else
                        {
                            AppGlobal.DBOUserId = AppGlobal.UserLogin.objRiskmasterDatabase.RMUserId;
                        }
                        wizard1.SelectedPage = wpStatus;
                        this.Cursor = Cursors.WaitCursor;
                        Common.StartAutoProgreeBar();
                        CDCManager.PopulateAuditTables();
                        CDCManager.ProcessCDCSetup();
                        CDCManager.PurgeDataFromAuditTables();
                        Common.StopAutoProgreeBar();
                        Common.FinishCDCProcess();
                        if (AppGlobal.IsError == true)
                        {
                            Common.DisplayMessage(Message.CDCManagerProcessAborted, Mode.Error);
                        }
                        this.Cursor = Cursors.Default;
                        
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                
                catch (Exception ex)
                {
                    Common.StopAutoProgreeBar();
                    Common.FinishCDCProcess();
                    Common.DisplayMessage(ex.Message, Mode.Error);
                    e.Cancel = true;
                }
            }



        }

       

        /// <summary>
        /// lvDatabases_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvDatabases_Click(object sender, EventArgs e)
        {
            //shut of warning label if a selection is made
            if (lblWarningWPDS.Visible)
            {
                lblWarningWPDS.Visible = false;
            }
        }
        private void GetBindingCollection(Dictionary<string, string> objDictValues)
        {
            //Loop through each item in the Dictionary collection
            lvDatabases.Items.Clear();
            foreach (string strKey in objDictValues.Keys)
            {
                ListViewItem lvItem = new ListViewItem { Text = strKey };

                lvItem.SubItems.Add(objDictValues[strKey]);

                lvDatabases.Items.Add(lvItem);
            } 
        } 



        
       


      

        private bool ValidateandGetCDCAdminCredential(string strDbConnString)
        {
           
            
            bool bIsCDCAdmin = false;
            const string DB_OWNER_ROLE = "db_owner";
            const string SYSADMIN_ROLE = "sysadmin";
            string sSQL = string.Empty;

            try
            {
                if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    
                    //Check if the user is a member of the db_owner role
                    if (Common.IsSQLRoleMember(strDbConnString, DB_OWNER_ROLE))
                    {
                        bIsCDCAdmin = true;
                        
                    }//if
                    else
                    {
                        bIsCDCAdmin = false;
                    }//else

                    //To check if the user is having  sysadmin permission
                    if (bIsCDCAdmin)
                    {
                        if (Common.IsSQLServerRoleMember(strDbConnString, SYSADMIN_ROLE))
                        {
                            bIsCDCAdmin = true;
                        }
                        else
                        {
                            bIsCDCAdmin = false;
                        }
                    }
                    //end
             
                }//if
                else if (AppGlobal.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    string strUserName = string.Empty;
                    string strPassword = string.Empty;
                    //Parses out the User ID and Pwd from the database connection string
                    Common.GetUIdPwdFromConString(strDbConnString, ref strUserName, ref strPassword);
                    sSQL = "SELECT USERNAME,GRANTED_ROLE FROM USER_ROLE_PRIVS WHERE GRANTED_ROLE = 'DBA' AND USERNAME ='" + strUserName.ToUpper()+"'";
                    using (DbReader objDbReader = DbFactory.GetDbReader(strDbConnString, sSQL))
                    {
                        if (objDbReader.Read())
                        {

                            bIsCDCAdmin = true;
                            
                        }
                        else
                            bIsCDCAdmin = false;
                    }

                }




                
            }
            catch (Exception p_objExp)
            {
                
            }
            finally
            {
               
            }
            return bIsCDCAdmin;
        }

        public void timerCDC_Tick(object sender, EventArgs e)
        {
             this.progressBarCDC.PerformStep();
             if (this.progressBarCDC.Value == this.progressBarCDC.Maximum)
             { this.progressBarCDC.Value = 0; } 
        }




      




    }

    
}