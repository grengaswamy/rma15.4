﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Common;
using System.Net;
using System.ServiceModel;
using System.IO;
using Riskmaster.Models;
using System.Runtime.Serialization.Json;

namespace Riskmaster.Tools.CDC
{
    public class AppGlobal
    {

        private static string m_sUser = string.Empty; //user login
        private static string m_sPwd = string.Empty; //password
        private static string m_sDSN = string.Empty; //database name
        private static string m_sDBOUserId = string.Empty; // database owner
        private static string m_CDCAdminUID = string.Empty;//cdc admin db user ID
        private static string m_CDCAdminPwd = string.Empty;//cdc admin db password
        private static eDatabaseType m_sDatabaseType = 0;
        private static string m_sDbConnstring = string.Empty;
        private static string m_sDbAdminConnstring = string.Empty;
        private static string m_sAuditDbConnstring = string.Empty;
        private static string m_sDatabaseName = string.Empty;
        private static bool m_bSilentMode = false;
        private static int m_iRMDbId = 0;
        private static bool m_bError = false;
        private static string m_sSubtaskValue = string.Empty;
        private static int m_iClientId = 0;


        public static string User
        {
            get
            {
                return m_sUser;
            }
            set
            {
                m_sUser = value;
            }
        }

        public static string Password
        {
            get
            {
                return m_sPwd;
            }
            set
            {
                m_sPwd = value;
            }
        }

        public static string DSN
        {
            get
            {
                return m_sDSN;
            }
            set
            {
                m_sDSN = value;
            }
        }

        public static string DBOUserId
        {
            get
            {
                return m_sDBOUserId;
            }
            set
            {
                m_sDBOUserId = value;
            }
        }
        

        public static string CDCAdminUID
        {
            get
            {
                return m_CDCAdminUID;
            }
            set
            {
                m_CDCAdminUID = value;
            }
        }

        public static string CDCAdminPwd
        {
            get
            {
                return m_CDCAdminPwd;
            }
            set
            {
                m_CDCAdminPwd = value;
            }
        }
        public static string RMDBConnstring
        {
            get
            {
                return m_sDbConnstring;
            }
            set
            {
                m_sDbConnstring = value;
            }
        }

        public static string RMAdminConnstring
        {
            get
            {
                return m_sDbAdminConnstring;
            }
            set
            {
                m_sDbAdminConnstring = value;
            }
        }

        /// <summary>
        /// Gets an ADO.Net based Admin Tracking connection string
        /// </summary>
        public static string RMAdminADONetConnString
        {
            get
            {
                return Common.TranslateConnectionStringToNative(m_sDbAdminConnstring);
            }//get
        }//property

        /// <summary>
        /// Gets an ADO.Net based for History Tracking connection string
        /// </summary>
        public static string RMHistADONetConnString
        {
            get
            {
                return Common.TranslateConnectionStringToNative(HistoryTrackingDSN(AppGlobal.ClientId));//Add clientId by kuladeep for Cloud Jira-1124
            }//get
        }//property

        public static string RMDatabaseName
        {
            get
            {
                return m_sDatabaseName;
            }
            set
            {
                m_sDatabaseName = value;
            }
        }
        

        public static eDatabaseType DatabaseType
        {
            get
            {
                return m_sDatabaseType;
            }
            set
            {
                m_sDatabaseType = value;
            }
        }

        public static bool IsSilentMode
        {
            get
            {
                return m_bSilentMode;
            }
            set
            {
                m_bSilentMode = value;
            }
        }

        public static bool IsError
        {
            get
            {
                return m_bError;
            }
            set
            {
                m_bError = value;
            }
        }

        public static int RMDatabaseId
        {
            get
            {
                return m_iRMDbId;
            }
            set
            {
                m_iRMDbId = value;
            }
        }

        public static string SubTaskValue
        {
            get
            {
                return m_sSubtaskValue;
            }
            set
            {
                m_sSubtaskValue = value;
            }
        }

        public static int ClientId
        {
            get
            {
                return m_iClientId;
            }
            set
            {
                m_iClientId = value;
            }
        }
        public static UserLogin UserLogin = null;


        //Add & change by kuladeep for Cloud-1124 Start

        //public static string SecurityDSN
        //{
        //    get
        //    {
        //        return ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
        //    }//get
        //}//property: SecurityDSN
        //public static string HistoryTrackingDSN
        //{
        //    get
        //    {
        //        return ConfigurationManager.ConnectionStrings["HistoryDataSource"].ConnectionString;
        //    }//get
        //}//property: HistoryTrackingDSN
        public static string SecurityDSN(int p_iClientId)
        {
            return RMConfigurationManager.GetConnectionString("RMXSecurity", p_iClientId);
        }
        public static string HistoryTrackingDSN(int p_iClientId)
        {
            return RMConfigurationManager.GetConnectionString("HistoryDataSource", p_iClientId);                
        }
        public static T GetResponse<T>(string uri, string oMethod, string ContentType, object oData)
        {
            WebClient client = null;
            string data = string.Empty;
            string responseString = string.Empty;
            try
            {
                client = new WebClient();
                uri = uri.Trim('/');
                uri = System.Configuration.ConfigurationManager.AppSettings["BaseServiceUrl"].ToString() + uri;
                client.Encoding = Encoding.UTF8;
                client.Headers[HttpRequestHeader.ContentType] = ContentType; // working
                if (oMethod == "GET")
                {
                    responseString = client.DownloadString(uri);
                }
                else
                {
                    data = "{\"o" + oData.GetType().Name + "\": " + JSONSerializeDeserialize.Serialize(oData) + "}";
                    responseString = client.UploadString(uri, oMethod.ToString(), data);
                }
                return JSONSerializeDeserialize.DeserializeJSon<T>(responseString);
            }
            catch (WebException wex)
            {
                string exMessage = wex.Message;
                if (wex.Response != null)
                {
                    using (StreamReader r = new StreamReader(wex.Response.GetResponseStream()))
                    {
                        exMessage = r.ReadToEnd();
                    }
                    // the fault xml 
                    RMException theFault = new RMException();
                    throw new FaultException<RMException>(theFault, new FaultReason(wex.Message), new FaultCode("Sender"));
                }
                return default(T);
            }
            catch (Exception ex)
            {
                return default(T);
            }
            finally
            {
                if (client != null)
                {
                    client = null;
                }
            }
        }
        //Add & change by kuladeep for Cloud-1124 End
    }
}

/// <summary>
/// Created by Deb for JSON implementation
/// </summary>
public class JSONSerializeDeserialize
{
    /// <summary>
    /// Serialize the object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string Serialize<T>(T obj)
    {
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
        MemoryStream ms = new MemoryStream();
        serializer.WriteObject(ms, obj);
        string retVal = Encoding.UTF8.GetString(ms.ToArray());
        ms.Close();
        return retVal;
    }
    /// <summary>
    /// Deserialize the object with property exposed
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="json"></param>
    /// <returns></returns>
    public static T Deserialize<T>(string json)
    {
        T obj1 = Activator.CreateInstance<T>();
        MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj1.GetType());
        if (json.Length > 0 && json.Substring(0, 1) == "{")
        {
            obj1 = (T)serializer.ReadObject(ms);
        }
        ms.Close();
        return obj1;
    }
    /// <summary>
    /// Deserialize the return json
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="jsonString"></param>
    /// <returns></returns>
    public static T DeserializeJSon<T>(string jsonString)
    {
        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
        MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
        if (stream.Length == 0) return default(T);
        T obj = (T)ser.ReadObject(stream);
        stream.Close();
        return obj;
    }
}
