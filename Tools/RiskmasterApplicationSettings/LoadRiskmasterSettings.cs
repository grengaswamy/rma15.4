using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Data;
using Riskmaster.Application.RiskmasterSettings;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Application.ApplicationSettings
{
    /// <summary>
    /// Loads all of the required Riskmaster Settings
    /// </summary>
    public class LoadRiskmasterSettings: ISecuritySettings, ISMServerSettings, ISessionSettings, ISMTPServerSettings,
                                        IRMConfigSettings
    {
        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <remarks>Hidden (i.e. private) so that users cannot 
        /// instantiate the object using the default constructor</remarks>
        private LoadRiskmasterSettings()
        {
 
        } // class constructor

        /// <summary>
        /// OVERLOADED: Class constructor
        /// </summary>
        /// <param name="strGlobalASAPath"></param>
        /// <param name="strRMConfigPath"></param>
        public LoadRiskmasterSettings(string strGlobalASAPath, string strRMConfigPath)
        {
            RiskmasterSettingsManager.GlobalASAPath = strGlobalASAPath;
            RiskmasterSettingsManager.RiskmasterConfigPath = strRMConfigPath;
        } // constructor



        #region Load Riskmaster Settings Methods
        /// <summary>
        /// Loads the DTG Security32 and SMServer information
        /// </summary>
        /// <permission cref="">Requires read access to the Registry</permission>
        public void LoadRegistrySettings()
        {

            NameValueCollection nvSecSettings = new NameValueCollection();
            
            //Load the Security User Name
            nvSecSettings.Add("EncDBUserID", RiskmasterRegistryKeys.DTGSecurity32UserKey);
            nvSecSettings.Add("EncDBUserValue", string.Empty);

            this.SecurityUID = RMCryptography.DecryptString(RiskmasterSettingsManager.GetRegistrySetting(nvSecSettings));

            nvSecSettings.Clear();

            //Load the Security Password
            nvSecSettings.Add("EncDBUserPwd", RiskmasterRegistryKeys.DTGSecurity32PasswordKey);
            nvSecSettings.Add("EncDBPwdValue", string.Empty);

            this.SecurityPwd = RMCryptography.DecryptString(RiskmasterSettingsManager.GetRegistrySetting(nvSecSettings));

            nvSecSettings.Clear();

            //Load the Sortmaster DSN
            nvSecSettings.Add("SMServerKey", RiskmasterRegistryKeys.SMServerKey);
            nvSecSettings.Add("SMServerDSN", RiskmasterRegistryKeys.SMServerDSNValue);

            string strSMServerDSN = RiskmasterSettingsManager.GetRegistrySetting(nvSecSettings);
            ConnectionStringManager objConnMgr = new ConnectionStringManager(strSMServerDSN);
            this.SMServerUID = objConnMgr.UID;
            this.SMServerPwd = objConnMgr.PWD;

            //Load the SMTP Server
            nvSecSettings.Clear();

            nvSecSettings.Add("SMServerKey", RiskmasterRegistryKeys.SMServerKey);
            nvSecSettings.Add("SMTPServer", RiskmasterRegistryKeys.SMTPServerValue);

            m_strSMTPServer = RiskmasterSettingsManager.GetRegistrySetting(nvSecSettings);

            //Clean up
            nvSecSettings = null;
        }

        /// <summary>
        /// Loads all of the relevant Sortmaster information
        /// </summary>
        /// <permission cref="">Requires read access to the File System</permission>
        public void LoadGlobalASASettings()
        {
            string strSecurityDSN = RiskmasterSettingsManager.GetGlobalASASetting("DTGSecurityAppSetting", GlobalASAAppSettings.DTGSecurityAppSetting);
            string strSessionDSN = RiskmasterSettingsManager.GetGlobalASASetting("SessionAppSetting", GlobalASAAppSettings.WebFarmSessionAppSetting);
            string strSMServerDSN = RiskmasterSettingsManager.GetGlobalASASetting("SMServerAppSetting", GlobalASAAppSettings.SMServerAppSetting);
            ConnectionStringManager objConnMgr = new ConnectionStringManager(strSecurityDSN);
            
            this.SecurityUID = objConnMgr.UID;
            this.SecurityPwd = objConnMgr.PWD;
            this.SecurityConnectionString = strSecurityDSN;

            this.SessionConnectionString = strSessionDSN;
            
            //Re-initialize teh value with the Sortmaster connection string
            objConnMgr.ConnectionString = strSMServerDSN;

            this.SMServerUID = objConnMgr.UID;
            this.SMServerPwd = objConnMgr.PWD;
            
            m_strSMTPServer = RiskmasterSettingsManager.GetGlobalASASetting("SMTPServerAppSetting", GlobalASAAppSettings.SMTPServerAppSetting);

            //Clean up
            objConnMgr = null;

        }

        /// <summary>
        /// Loads the Session DSN and SMTP Server settings
        /// </summary>
        /// <permission cref="">Requires read access to the File System</permission>
        public void LoadRiskmasterConfigSettings()
        {
            //Read the Session DSN information
            string strSessionDSN = RiskmasterSettingsManager.GetRMConfigSetting("SessionDSN", RiskmasterConfigSettingElements.RMSession);

            //Read the Business Objects Server information
            m_strBusObjectsServer = RiskmasterSettingsManager.GetRMConfigSetting("BusObjectsServer", RiskmasterConfigSettingElements.BISXPath);

            //Read the Sortmaster.Net Server information
            m_strSMNetServer = RiskmasterSettingsManager.GetRMConfigSetting("SMNetServer", RiskmasterConfigSettingElements.SMNetServer);

            //Read the AppServer information
            m_strAppServer = RiskmasterSettingsManager.GetRMConfigSetting("AppServer", RiskmasterConfigSettingElements.AppServer);

            //Read the IP Address information
            m_dstIPAddresses = RiskmasterSettingsManager.GetRMConfigSettingDataSet("CommonWebServiceHandler/TrustedIPPool");            
        }//method: LoadRiskmasterConfigSettings()

        #endregion


        #region ISMServerSettings Members

        public string SMServerUID
        {
            get;
            set;
        }

        public string SMServerPwd
        {
            get;
            set;
        }

        #endregion

        #region ISMTPServerSettings Members

        private string m_strSMTPServer;

        public string SMTPServer
        {
            get
            {
                return m_strSMTPServer;
            }
            set
            {
                m_strSMTPServer = value;
            }
        }

        #endregion

        #region IRMConfigSettings Members

        private string m_strBusObjectsServer;
        private string m_strAppServer;
        private string m_strSMNetServer;
        private DataSet m_dstIPAddresses;

        public string BusObjectsServer
        {
            get
            {
                return m_strBusObjectsServer;
            }
            set
            {
                m_strBusObjectsServer = value;
            }
        }

        public string AppServer
        {
            get{return m_strAppServer;}
            set{m_strAppServer = value;}
        } // property AppServer


        public string SMNetServer
        {
            get{return m_strSMNetServer;}
            set{m_strSMNetServer = value;}
        } // property SMNetServer

        public DataSet IPAddresses
        {
            get{return m_dstIPAddresses;}
            set{m_dstIPAddresses = value;}
        } // property IPAddresses


        #endregion

        #region ISecuritySettings Members

        public string SecurityUID
        {
            get;
            set;
        }

        public string SecurityPwd
        {
            get;
            set;
        }

        public string SecurityConnectionString
        {
            get;
            set;
        }

        #endregion

        #region ISessionSettings Members

        public string SessionConnectionString
        {
            get;
            set;
        
        }

        #endregion
    }
}
