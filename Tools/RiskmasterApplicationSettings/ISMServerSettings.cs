using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.ApplicationSettings
{
    interface ISMServerSettings
    {
        string SMServerUID
        {
            get;
            set;
        }

        string SMServerPwd
        {
            get;
            set;
        }
    }
}
