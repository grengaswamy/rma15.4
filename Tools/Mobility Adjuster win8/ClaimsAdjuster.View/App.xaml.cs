﻿using ClaimsAdjuster.View.Helper;
using ClaimsAdjuster.View.Pages;
using ClaimsAdjuster.ViewModel;
using ClaimsAdjuster.ViewModel.Helpers;
using IdentityMine.BaseUI;
using IdentityMine.Common;
using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Data.Xml.Dom;
using Windows.Storage;
using Windows.System.Threading;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using ClaimsAdjuster.ViewModel.Repository;
using Windows.UI.Notifications;
// The Grid App template is documented at http://go.microsoft.com/fwlink/?LinkId=234226

namespace ClaimsAdjuster.View
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application, INavigationApp
    {
        private bool isLaunchedFromToast;
        private Button focusButton;
        /// <summary>
        /// Initializes the singleton Application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        /// 

        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
            this.UnhandledException += App_UnhandledException;

            ThreadPool.RunAsync((s) =>
            {
                LoadDataToViewMapping();
            }, WorkItemPriority.High);

        }

        void RootFrame_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MainViewModel.Instance.CurrentView = SnapHelper.DetermineDefaultVisualState();
        }

        private static async void App_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            e.Handled = true;
        }

        DataToViewCollection DataToViewMappings;

        public Frame RootFrame { get; private set; }

        Dictionary<string, string> _dict = new Dictionary<string, string>();

        public async void LoadDataToViewMapping()
        {
            var folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Resources");
            var file = await folder.GetFileAsync("DataToViewMapping.xml");

            var xmlDoc = await XmlDocument.LoadFromFileAsync(file);

            DataToViewMappings = new DataToViewCollection();

            foreach (IXmlNode xmlNode in xmlDoc.SelectNodes("/DataToViewCollection/DataToViewMapping"))
            {
                DataToViewMappings.Add(new DataToViewMapping() { ResultID = xmlNode.Attributes.GetNamedItem("ResultID").NodeValue.ToString(), PageType = Type.GetType(xmlNode.Attributes.GetNamedItem("PageType").NodeValue.ToString()) });
            }
        }


        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            isLaunchedFromToast = (args.Arguments == Constants.ToastSelectedArgument) ? true : false;    // Activated from toast notification

            // Do not repeat app initialization when already running, just ensure that
            // the window is active
            TileUpdateManager.CreateTileUpdaterForApplication().Clear();
            if (args.PreviousExecutionState == ApplicationExecutionState.Running || args.PreviousExecutionState == ApplicationExecutionState.Suspended)
            {
                bool isUser = await GetUserDetails();
                if (MainViewModel.Instance.IsAuthenticated && isUser)
                {

                    if (isLaunchedFromToast)
                    {
                        MainViewModel.Instance.NavigateToTasksCommand.Execute(null);
                    }
                    else if (args.TileId != "App" && !string.IsNullOrEmpty(args.TileId))    //if the app is already running, and the user tries to open a pinned (tile)
                    {
                        MainViewModel.Instance.TileParams = args.Arguments;
                        InitializeAppUsingTileInfo(args);
                        MainViewModel.Instance.NavigateToHomeHubCommand.Execute(null);
                        return;
                    }
                    Window.Current.Activate();
                    return;
                }
            }
            await LoadInitialData(args);
        }

        private void InitializeAppUsingTileInfo(LaunchActivatedEventArgs args)
        {
            MainViewModel.Instance.NavigateToTileParam();
        }

        private async Task<bool> GetUserDetails()
        {
            User user = await MainViewModel.Instance.CscRepository.GetUserDetails(MainViewModel.Instance.AppSettings.LastLoggedUserId);
            if (user == null)
                return false;
            else
                return true;
        }

        private async Task LoadInitialData(IActivatedEventArgs args)
        {
            //// Create a Frame to act as the navigation context and associate it with
            //// a SuspensionManager key


            var rootFrame = new Frame();
            rootFrame.Style = Resources["ClaimsAdjusterRootFrameStyle"] as Style;
            SuspensionManager.RegisterFrame(rootFrame, "AppFrame");
            MainViewModel.Instance.AppInstance = this;
            MainViewModel.Instance.Dispatcher = rootFrame.Dispatcher;
            SettingsHelper.Instance.Load();
            if (MainVMBase.InstanceBase.Settings == null)
                MainVMBase.InstanceBase.Settings = await MainViewModel.Instance.FromStorageFile<AppSettings>(AppSettings.IdString); //Load from IsolatedStorage

            await OnLanguageChanged(MainViewModel.Instance.AppSettings.CurrentLanguage);

            await MainViewModel.Instance.Activate(args.PreviousExecutionState == ApplicationExecutionState.Terminated);

            var mainVMResx = new KeyValuePair<object, object>("MainVM", MainViewModel.Instance);
            if (!this.Resources.Contains(mainVMResx))
                this.Resources.Add(mainVMResx);

            //Windows.ApplicationModel.Search.SearchPane.GetForCurrentView().SuggestionsRequested += App_SuggestionsRequested;

            RootFrame = rootFrame;
            // MainViewModel.Instance.InitialFrameNavigationState = rootFrame.GetNavigationState(); // we can use this  'navigation state' string to clear frame navigation back stack
            MainVMBase.InstanceBase.AppInstance = this;
            if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
            {
                // Restore the saved session state only when appropriate
                await SuspensionManager.RestoreAsync();
            }
            bool isUser = await GetUserDetails();
            Window.Current.Content = rootFrame;
            rootFrame.Loaded += RootFrame_Loaded;
            Window.Current.SizeChanged += Current_SizeChanged;
            Window.Current.VisibilityChanged += Current_VisibilityChanged;
            RootFrame.SizeChanged += RootFrame_SizeChanged;
            RootFrame.Navigated += RootFrame_Navigated;
            if (rootFrame.Content == null || rootFrame.BackStack.Count > 0)
            {
                if (args is LaunchActivatedEventArgs)
                {
                    var launchArgs = (LaunchActivatedEventArgs)args;
                    if (launchArgs.TileId != "App" && !string.IsNullOrEmpty(launchArgs.TileId))//If the app is opened from a pinned tile
                    {
                        //TODO:Navigate from Tile
                        MainViewModel.Instance.TileParams = launchArgs.Arguments;

                        if (MainViewModel.Instance.IsAuthenticated && isUser != null)
                        {
                            rootFrame.Navigate(typeof(HomePage), launchArgs.TileId);
                            SettingsHelper.Instance.Load();

                        }
                        else
                            rootFrame.Navigate(typeof(SignInPage), launchArgs.TileId);
                    }
                    if (rootFrame.Content == null || rootFrame.BackStack.Count > 0)
                    {
                        MainViewModel.Instance.TileParams = string.Empty;
                        if (MainViewModel.Instance.IsAuthenticated && isUser)
                        {
                            rootFrame.Navigate(typeof(HomePage));
                            if (isLaunchedFromToast)
                                MainViewModel.Instance.NavigateToTasksCommand.Execute(null);
                            SettingsHelper.Instance.Load();
                        }
                        else
                            rootFrame.Navigate(typeof(SignInPage));
                    }
                }
            }

            Window.Current.Activate();  // Ensure the current window is active
        }

        private void RootFrame_Loaded(object sender, RoutedEventArgs e)
        {
            focusButton = GenericHelper.FindVisualChildByName<Button>(RootFrame, "FocusButton");
        }

        void Current_VisibilityChanged(object sender, Windows.UI.Core.VisibilityChangedEventArgs e)
        {
            ApplicationData.Current.LocalSettings.Values[Constants.AppVisibilityKey] = e.Visible;

            if (MainViewModel.Instance.IsAuthenticated)
                MainViewModel.Instance.GenerateToastData();
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            MainViewModel.Instance.CurrentView = SnapHelper.DetermineDefaultVisualState();
        }

        private void RootFrame_Navigated(object sender, Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            if (focusButton != null)//HACK: to avoid focus being set to SearchBox control on navigation.
                focusButton.Focus(FocusState.Programmatic);
            if (e.Parameter != null)
            {
                IServiceRequest serviceRequest = MainVMBase.InstanceBase.ServiceFactory.GetService(e.Parameter.ToString());
                if (serviceRequest != null)
                {
                    (RootFrame.Content as Page).DataContext = serviceRequest.Result;
                    MainViewModel.Instance.CurrentService = serviceRequest;
                }
            }


        }

        // To show SearchTerm suggestions in the search charm.
        private void App_SuggestionsRequested(Windows.ApplicationModel.Search.SearchPane sender, Windows.ApplicationModel.Search.SearchPaneSuggestionsRequestedEventArgs args)
        {

        }

        protected async override void OnSearchActivated(Windows.ApplicationModel.Activation.SearchActivatedEventArgs args)
        {
            if (Window.Current.Content == null) //true if app was not running when search was activated
            {
                // load intial data  .             
                await LoadInitialData(args);
            }
            //MainViewModel.Instance.SearchTerm = args.QueryText;
            //MainViewModel.Instance.SearchCommand.Execute(null);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await SuspensionManager.SaveAsync();
            await Task.Run(() => MainViewModel.Instance.DeActivate(true));
            deferral.Complete();
        }

        public void OnThemeChanged(string theme)
        {
            throw new NotImplementedException();
        }

        public string AppName
        {
            get { return MainViewModel.AppName; }
        }

        public string GetResourceString(string key)
        {
            return _dict[key];
        }

        public void Navigate(IServiceRequest request)
        {
            //Temp code to Navigate back home
            if (request != null)
            {
                if (request.ClassId != "TaskDetailsRequest") //bug #20560 fix, to keep the bottom app bar in task details page open while opening throught a secondary task details tile
                    MainViewModel.Instance.DismissAppBar(false);
                //Result to Page mapping can be done here..
                var type = DataToViewMappings.FirstOrDefault(s => s.ResultID == request.Result.ClassID).PageType;
                ((Frame)Window.Current.Content).Navigate(type, request.Id);
            }
            else
            {
                RootFrame.Navigate(typeof(HomePage));
            }
            Window.Current.Activate();
        }

        public async Task OnLanguageChanged(string language)
        {
            _dict.Clear();
            if (String.IsNullOrEmpty(language))
            {
                language = "en-US";
            }
            //Checking localization files exists

            StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(Constants.LocalizationFolderPath);
            IStorageItem file = await folder.TryGetItemAsync(string.Format("AppResources.{0}.resx", language.Replace("-", "_")));
            if (file == null)
            {
                file = await folder.TryGetItemAsync("AppResources.resx");
            }

            Stream sr = await (file as StorageFile).OpenStreamForReadAsync();
            XDocument xmlDoc = XDocument.Load(sr);
            List<XElement> dataItems = xmlDoc.Descendants("data").ToList();
            ResourceDictionary resourceDictionary = new ResourceDictionary();
            foreach (var item in dataItems)
            {
                string key = item.Attribute("name").Value;
                string value = item.Descendants("value").FirstOrDefault().Value;
                //TODO: Use ResourceLoader
                _dict.Add(key, value);
                resourceDictionary.Add(key, value);//Inserting items to ResourceDictionary
            }

            ////Merging localized strings
            this.Resources.MergedDictionaries.Add(resourceDictionary);
        }

    }
}
