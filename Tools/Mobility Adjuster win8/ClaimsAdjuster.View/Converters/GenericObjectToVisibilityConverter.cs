﻿using System;
using System.Globalization;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace ClaimsAdjuster.View.Converters
{
    public class GenericObjectToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (parameter != null)
            {
                if (value == null)
                    return Visibility.Collapsed;
                else
                {
                    if (value.ToString().Equals(parameter))
                        return Visibility.Visible;

                    return Visibility.Collapsed;
                }
            }

            if (value is bool)
            {
                if ((bool)value) return Visibility.Visible;
                else return Visibility.Collapsed;
            }

            if (value is string)
            {
                if (string.IsNullOrEmpty(value.ToString()))
                    return Visibility.Collapsed;
                else
                    return Visibility.Visible;

            }

            if (value == null)
                return Visibility.Collapsed;
            else
                return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
