﻿using ClaimsAdjuster.ViewModel;
using IdentityMine.Behaviors;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace ClaimsAdjuster.View.Behaviors
{
    public class MinimizedButtonBehavior : Behavior<Button>
    {

        #region Methods

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.Click += AssociatedObject_Click;
            MainViewModel.Instance.CurrentFrame.Navigated -= CurrentFrame_Navigated;
            MainViewModel.Instance.CurrentFrame.Navigated += CurrentFrame_Navigated;
            ToggleMinimizedButtonVisibility();//Called ToggleMinimizedButtonVisibility As Navigated Event does not fire for the first Navigation i.e. (HomePage/Sign In)
        }

        /// <summary>
        /// Toggles the visibility of MinimizedButton if BottomAppBar Exists
        /// </summary>
        private void ToggleMinimizedButtonVisibility()
        {
            Page currentPage = MainViewModel.Instance.CurrentFrame.Content as Page;
            if (currentPage.BottomAppBar == null)
                AssociatedObject.Visibility = Visibility.Collapsed;
            else
                AssociatedObject.Visibility = Visibility.Visible;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.Click -= AssociatedObject_Click;
            MainViewModel.Instance.CurrentFrame.Navigated -= CurrentFrame_Navigated;
        }

        #endregion

        #region Events

        private void CurrentFrame_Navigated(object sender, Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            ToggleMinimizedButtonVisibility();
        }

        private void AssociatedObject_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Page currentPage = MainViewModel.Instance.CurrentFrame.Content as Page;
            if (currentPage.BottomAppBar != null)
                currentPage.BottomAppBar.IsOpen = true;
            if (currentPage.TopAppBar != null)
                currentPage.TopAppBar.IsOpen = true;
        }

        #endregion
    }
}
