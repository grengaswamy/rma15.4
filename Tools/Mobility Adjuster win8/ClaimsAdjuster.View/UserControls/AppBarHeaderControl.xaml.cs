﻿using IdentityMine.Controls;
using Windows.UI.Xaml;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ClaimsAdjuster.View.UserControls
{
    public sealed partial class AppBarHeaderControl : LayoutAwareUserControl
    {
        public AppBarHeaderControl()
        {
            this.InitializeComponent();
        }

        #region Events

        /// <summary>
        /// Navigate to home page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HomeAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement logic
        }

        /// <summary>
        /// Navigate to tasks page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TasksAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement logic
        }

        /// <summary>
        /// Navigate to claims page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClaimsAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement logic
        }

        /// <summary>
        /// Navigate to events page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventsAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement logic
        }

        #endregion
    }
}
