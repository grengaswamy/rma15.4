﻿using ClaimsAdjuster.ViewModel;
using IdentityMine.Controls;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Animation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ClaimsAdjuster.View.UserControls
{
    public sealed partial class AppHeaderControl : LayoutAwareUserControl
    {
        private double landscapeSearchControlAndMarginWidth = 690;
        private double portraitSearchControlAndMarginWidth = 291;
        private double windowSize;
        public AppHeaderControl()
        {
            this.InitializeComponent();
            this.Loaded += AppHeaderControl_Loaded;
            Window.Current.SizeChanged += Current_SizeChanged;
            windowSize = Window.Current.Bounds.Width;
        }

        void AppHeaderControl_Loaded(object sender, RoutedEventArgs e)
        {
            FocusButton.Focus(FocusState.Programmatic);
            DetermineState();
        }

        void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            DetermineState();
        }

        void DetermineState()
        {
            if (MainViewModel.Instance.CurrentView == ViewState.FullScreenLandscape)
            {
                VisualStateManager.GoToState(this, "Normal", false);
                MainViewModel.Instance.AvailableHeaderWidth = Window.Current.Bounds.Width - landscapeSearchControlAndMarginWidth;
            }
            else if (MainViewModel.Instance.CurrentView == ViewState.Snapped)
            {
                VisualStateManager.GoToState(this, "Snapped", false);
            }
            else if (MainViewModel.Instance.CurrentView == ViewState.FullScreenPortrait || MainViewModel.Instance.CurrentView == ViewState.Filled)
            {
                VisualStateManager.GoToState(this, "Portrait", false);
             }
            if (Window.Current.Bounds.Width > Convert.ToDouble(App.Current.Resources["SnapPixelWidth"]) && MainViewModel.Instance.CurrentView != ViewState.FullScreenLandscape)
            {
                //Checking for filled view
                MainViewModel.Instance.AvailableHeaderWidth = Window.Current.Bounds.Width - portraitSearchControlAndMarginWidth;         
            }
        }

    }
}
