﻿using IdentityMine.Controls;
using Windows.UI.Xaml;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ClaimsAdjuster.View.UserControls
{
    public sealed partial class EventDetailsControl : LayoutAwareUserControl
    {
        #region Fields
        
        private Visibility showEventHeader = Visibility.Collapsed;

        public Visibility ShowEventHeader
        {
            get
            {
                return showEventHeader;
            }
            set
            {
                showEventHeader = value;
            }
        } 

        #endregion

        #region Constructors
        
        public EventDetailsControl()
        {
            this.InitializeComponent();
        } 

        #endregion
    }
}
