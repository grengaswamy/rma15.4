﻿using IdentityMine.Controls;
using Windows.UI.Xaml;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace ClaimsAdjuster.View.UserControls
{
    public sealed partial class ClaimDetailsControl : LayoutAwareUserControl
    {
        #region Fields

        private Visibility claimExtraDetailsVisibility;

        #endregion

        public ClaimDetailsControl()
        {
            this.InitializeComponent();
        }

        #region Properties

        public Visibility ClaimExtraDetailsVisibility
        {
            get
            {
                return claimExtraDetailsVisibility;
            }
            set
            {
                claimExtraDetailsVisibility = value;
            }
        }

        #endregion

    }
}
