﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NotesPage : Page
    {
        #region Fields

        NoteCollection dataContext;

        #endregion

        #region Constructor
        public NotesPage()
        {
            this.InitializeComponent();
        }

        #endregion

        #region EventHandlers

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            dataContext = this.DataContext as NoteCollection;
            if (dataContext != null)
            {
                dataContext.PropertyChanged -= dataContext_PropertyChanged;
                if (e.NavigationMode == NavigationMode.Back && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
                    dataContext.UnLoad();
            }

            base.OnNavigatingFrom(e);
        }

        void dataContext_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (mainContent != null && e.PropertyName == "Count")
            {
                this.mainContent.ScrollToPosition(0);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            dataContext = this.DataContext as NoteCollection;
            if (dataContext != null)
                dataContext.PropertyChanged += dataContext_PropertyChanged;
            base.OnNavigatedTo(e);
        }

        #endregion

    }
}
