﻿using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ClaimsPage : Page
    {
        #region Fields

        ClaimCollection dataContext;

        #endregion

        #region Constructors

        public ClaimsPage()
        {
            this.InitializeComponent();
        }

        #endregion

        #region EventHandlers

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            dataContext = this.DataContext as ClaimCollection;
            if (dataContext != null)
                dataContext.PropertyChanged += dataContext_PropertyChanged;
            base.OnNavigatedTo(e);
        }

        void dataContext_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (mainContent != null && e.PropertyName == "Count")
            {
                this.mainContent.ScrollToPosition(0);
            }
        }

        protected override void OnNavigatedFrom(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            dataContext = this.DataContext as ClaimCollection;
            if (dataContext != null)
            {
                dataContext.PropertyChanged -= dataContext_PropertyChanged;
                if (e.NavigationMode == NavigationMode.Back && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
                    dataContext.UnLoad();
            }

            base.OnNavigatedFrom(e);
        }

        void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender != null)
                (sender as ScrollViewer).ChangeView(0, null, null);
        }

        #endregion
    }
}
