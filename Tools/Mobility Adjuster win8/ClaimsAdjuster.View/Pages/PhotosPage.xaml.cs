﻿using ClaimsAdjuster.View.Helper;
using ClaimsAdjuster.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PhotosPage : Page
    {
        #region Fields

        MediaCollection dataContext;
        ListView snappedGridView;
        GridView normalGridView;

        #endregion

        #region Constructor
        public PhotosPage()
        {
            this.InitializeComponent();
            this.Loaded += PhotosPage_Loaded;     
        }

        #endregion

        #region EventHandlers

        private void ClaimGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!(e.ClickedItem is MediaViewModel)) return;
            var selectedItem = (e.ClickedItem as MediaViewModel);
            MainViewModel.Instance.OpenPhotoCommand.Execute(selectedItem.FileLocation);
        }

        private void GridView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                MediaViewModel mediaItemAdded = (e.AddedItems[0] as MediaViewModel);
                mediaItemAdded.IsSelected = true;
                if (snappedGridView != null && !snappedGridView.SelectedItems.Contains(mediaItemAdded))
                    snappedGridView.SelectedItems.Add(mediaItemAdded);
                if (normalGridView != null && !normalGridView.SelectedItems.Contains(mediaItemAdded))
                    normalGridView.SelectedItems.Add(mediaItemAdded);
            }
            else if (e.RemovedItems.Count > 0)
            {
                MediaViewModel mediaItemRemoved = (e.RemovedItems[0] as MediaViewModel);
                mediaItemRemoved.IsSelected = false;
                if (snappedGridView != null && snappedGridView.SelectedItems.Contains(mediaItemRemoved))
                    snappedGridView.SelectedItems.Remove(mediaItemRemoved);
                if (normalGridView != null && normalGridView.SelectedItems.Contains(mediaItemRemoved))
                    normalGridView.SelectedItems.Remove(mediaItemRemoved);

            }

            OpeningBottomAppBar();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            dataContext = this.DataContext as MediaCollection;
            if (e.NavigationMode == NavigationMode.Back && dataContext != null && !MainViewModel.Instance.SamePageExists(dataContext.ServiceId))
            {
                dataContext.Title = string.Empty;
                dataContext.UnLoad();
            }
            base.OnNavigatedFrom(e);
        }

        void PhotosPage_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            OpeningBottomAppBar();
        }

        private void SnappedGridView_Loaded(object sender, RoutedEventArgs e)
        {
            snappedGridView = GenericHelper.FindNameInTree(AutoAlignControl.SnappedViewContent as FrameworkElement, "SnappedGridView") as ListView;
            if (normalGridView != null && normalGridView.SelectedItems.Count > 0)
            {
                foreach (var item in normalGridView.SelectedItems)
                {
                    if (!snappedGridView.SelectedItems.Contains(item))
                        snappedGridView.SelectedItems.Add(item);
                }
            }
        }

        private void NormalGridView_Loaded(object sender, RoutedEventArgs e)
        {
            normalGridView = GenericHelper.FindNameInTree(AutoAlignControl.Content as FrameworkElement, "NormalGridView") as GridView;
            if (snappedGridView != null && snappedGridView.SelectedItems.Count > 0)
            {
                foreach (var item in snappedGridView.SelectedItems)
                {
                    if (!normalGridView.SelectedItems.Contains(item))
                        normalGridView.SelectedItems.Add(item);
                }
            }
        }

        private void OpeningBottomAppBar()
        {

            if (dataContext == null)
            {
                dataContext = this.DataContext as MediaCollection;
            }
            dataContext.RaisePropertyChanged("HasAnySelectedItem");
            if (this.BottomAppBar.IsOpen != dataContext.HasAnySelectedItem)
                this.BottomAppBar.IsOpen = dataContext.HasAnySelectedItem;
        }

        #endregion


    }
}
