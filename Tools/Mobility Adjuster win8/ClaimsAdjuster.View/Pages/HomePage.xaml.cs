﻿using ClaimsAdjuster.ViewModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HomePage : Page
    {
        #region Constructors

        public HomePage()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        protected override void OnNavigatedTo(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            this.DataContext = MainViewModel.Instance.HomePageVM;

            MainViewModel.Instance.IsHomePage = true;
            if (e.NavigationMode == Windows.UI.Xaml.Navigation.NavigationMode.Back)
            {
                MainViewModel.Instance.IsLoadHome = true;
                MainViewModel.Instance.ClosedTaskList.Clear();
            }
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            MainViewModel.Instance.IsHomePage = false;
            base.OnNavigatedFrom(e);
        }

        #endregion

    }
}
