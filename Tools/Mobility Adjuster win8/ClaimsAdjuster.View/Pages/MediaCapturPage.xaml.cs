﻿using ClaimsAdjuster.ViewModel;
using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ClaimsAdjuster.View.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MediaCapturPage : Page
    {
        private MediaCaptureViewModel dataContext;
        private DispatcherTimer audioTimer;
        private int audioDuration = 0;

        public MediaCapturPage()
        {
            this.InitializeComponent();
            this.Loaded += MediaCapturPage_Loaded;
        }

        void MediaCapturPage_Loaded(object sender, RoutedEventArgs e)
        {
            dataContext = this.DataContext as MediaCaptureViewModel;
            dataContext.RecordAudioStarted += dataContext_RecordAudioStarted;
            dataContext.RecordAudioStopped += dataContext_RecordAudioStopped;
            audioTimer = new DispatcherTimer();
            audioTimer.Interval = new TimeSpan(0, 0, 1);
            audioTimer.Tick += audioTimer_Tick;

            dataContext.InitializeAudioCapture();

            //dataContext.StartAudioDeviceReady();
        }

        void dataContext_RecordAudioStarted(object sender, EventArgs e)
        {
            audioDuration = 0;
            dataContext.RecordingTimeText = SecondsToString(audioDuration);
            audioTimer.Start();
        }

        void dataContext_RecordAudioStopped(object sender, EventArgs e)
        {
            audioDuration = 0;
            audioTimer.Stop();
        }

        void audioTimer_Tick(object sender, object e)
        {
            audioDuration += 1;

            dataContext.RecordingTimeText = SecondsToString(audioDuration);
        }

        private string SecondsToString(int intSeconds)
        {
            TimeSpan t = TimeSpan.FromSeconds(intSeconds);

            string answer = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    t.Hours,
                                    t.Minutes,
                                    t.Seconds);

            return answer;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            MainViewModel.Instance.RaiseFrameItemProperties();
            if (e.NavigationMode != NavigationMode.Forward && dataContext != null)
            {
                dataContext.CloseAudioCapture();
                dataContext.RecordAudioStarted -= dataContext_RecordAudioStarted;
                dataContext.RecordAudioStopped -= dataContext_RecordAudioStopped;
                if (audioTimer.IsEnabled)
                {
                    audioTimer.Stop();
                    audioTimer = null;
                }

                this.DataContext = null;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            MainViewModel.Instance.RaiseFrameItemProperties();
            base.OnNavigatedTo(e);
        }
    }

}
