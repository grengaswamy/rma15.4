﻿using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Core;

namespace IdentityMine.Controls
{
    public sealed class MoreButtonItemsControlCustom : ContentControl
    {
        #region Constructors

        public MoreButtonItemsControlCustom()
        {

            DefaultStyleKey = typeof(MoreButtonItemsControlCustom);
        }

        #endregion

        #region Properties

        public double ItemMargin
        {
            get { return (double)GetValue(ItemMarginProperty); }
            set { SetValue(ItemMarginProperty, value); }
        }


        // Using a DependencyProperty as the backing store for ItemMargin.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemMarginProperty =
            DependencyProperty.Register("ItemMargin", typeof(double), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(10.0));


        public double UsedHeight
        {
            get { return (double)GetValue(UsedHeightProperty); }
            set { SetValue(UsedHeightProperty, value); }
        }
        // Using a DependencyProperty as the backing store for UsedHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UsedHeightProperty =
            DependencyProperty.Register("UsedHeight", typeof(double), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(0.0));

        public double AppBarHeight
        {
            get { return (double)GetValue(AppBarHeightProperty); }
            set { SetValue(AppBarHeightProperty, value); }
        }
        // Using a DependencyProperty as the backing store for UsedHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AppBarHeightProperty =
            DependencyProperty.Register("AppBarHeight", typeof(double), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(0.0));
        // Using a DependencyProperty as the backing store for FirstItemRowSpan.  This enables animation, styling, binding, etc...

        public int MaxItemCount
        {
            get { return (int)GetValue(MaxItemCountProperty); }
            set { SetValue(MaxItemCountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaxItemCount.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxItemCountProperty =
            DependencyProperty.Register("MaxItemCount", typeof(int), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(6));



        public bool AutoMaxItemCount
        {
            get { return (bool)GetValue(AutoMaxItemCountProperty); }
            set { SetValue(AutoMaxItemCountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AutoMaxItemCount.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutoMaxItemCountProperty =
            DependencyProperty.Register("AutoMaxItemCount", typeof(bool), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(true));


        public int MaxColumn
        {
            get { return (int)GetValue(MaxColumnProperty); }
            set { SetValue(MaxColumnProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaxColumn.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxColumnProperty =
            DependencyProperty.Register("MaxColumn", typeof(int), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(3));


        public bool ShowMoreButton
        {
            get { return (bool)GetValue(ShowMoreButtonProperty); }
            set { SetValue(ShowMoreButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowMoreButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowMoreButtonProperty =
            DependencyProperty.Register("ShowMoreButton", typeof(bool), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(true));


        public double ItemWidth
        {
            get { return (double)GetValue(ItemWidthProperty); }
            set { SetValue(ItemWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ItemWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemWidthProperty =
            DependencyProperty.Register("ItemWidth", typeof(double), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(100.0));


        public double ItemHeight
        {
            get { return (double)GetValue(ItemHeightProperty); }
            set { SetValue(ItemHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ItemHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemHeightProperty =
            DependencyProperty.Register("ItemHeight", typeof(double), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(100.0));


        public int FirstItemRowSpan
        {
            get { return (int)GetValue(FirstItemRowSpanProperty); }
            set { SetValue(FirstItemRowSpanProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FirstItemRowSpan.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FirstItemRowSpanProperty =
            DependencyProperty.Register("FirstItemRowSpan", typeof(int), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(1));



        public int FirstItemColumnSpan
        {
            get { return (int)GetValue(FirstItemColumnSpanProperty); }
            set { SetValue(FirstItemColumnSpanProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FirstItemColumnSpan.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FirstItemColumnSpanProperty =
            DependencyProperty.Register("FirstItemColumnSpan", typeof(int), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(1));


        public DataTemplate MoreButtonTemplate
        {
            get { return (DataTemplate)GetValue(MoreButtonTemplateProperty); }
            set { SetValue(MoreButtonTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MoreButtonTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MoreButtonTemplateProperty =
            DependencyProperty.Register("MoreButtonTemplate", typeof(DataTemplate), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(null));


        public DataTemplate ItemsTemplate
        {
            get { return (DataTemplate)GetValue(ItemsTemplateProperty); }
            set { SetValue(ItemsTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ItemsTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsTemplateProperty =
            DependencyProperty.Register("ItemsTemplate", typeof(DataTemplate), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(null));


        public object ItemsSource
        {
            get { return (object)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(object), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(null, new PropertyChangedCallback(OnItemsSourceChanged)));

        private static void OnItemsSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            MoreButtonItemsControlCustom control = sender as MoreButtonItemsControlCustom;
            if (control != null)
            {
                control.UpdateItemsSource(e.NewValue, e.OldValue);
            }
        }

        public double ActualItemWidth
        {
            get
            {
                return this.ItemWidth + this.ItemMargin;
            }
        }

        public double ActualItemHeight
        {
            get
            {
                return this.ItemHeight + this.ItemMargin;
            }
        }
        public bool HasItems
        {
            get { return (bool)GetValue(HasItemsProperty); }
            set { SetValue(HasItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasItemsProperty =
            DependencyProperty.Register("HasItems", typeof(bool), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(false));



        public DataTemplate FirstItemTemplate
        {
            get { return (DataTemplate)GetValue(FirstItemTemplateProperty); }
            set { SetValue(FirstItemTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FirstItemTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FirstItemTemplateProperty =
            DependencyProperty.Register("FirstItemTemplate", typeof(DataTemplate), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(null));


        public bool ShowAllContents
        {
            get { return (bool)GetValue(ShowAllContentsProperty); }
            set { SetValue(ShowAllContentsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowAllContents.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowAllContentsProperty =
            DependencyProperty.Register("ShowAllContents", typeof(bool), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(false));

        public bool ShowTransitionAnimations
        {
            get { return (bool)GetValue(ShowTransitionAnimationsProperty); }
            set { SetValue(ShowTransitionAnimationsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowTransitionAnimations.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowTransitionAnimationsProperty =
            DependencyProperty.Register("ShowTransitionAnimations", typeof(bool), typeof(MoreButtonItemsControlCustom), new PropertyMetadata(true));
        #endregion

        #region Methods

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            PART_ItemsControl = this.GetTemplateChild("PART_ItemsControl") as VariableSizedWrapGrid;
            if (PART_ItemsControl != null)
            {
                if (ShowAllContents)
                {
                    PART_ItemsControl.Orientation = Orientation.Vertical;
                }
                if (ShowTransitionAnimations)
                {
                    PART_ItemsControl.ChildrenTransitions = new TransitionCollection();
                    PART_ItemsControl.ChildrenTransitions.Add(new AddDeleteThemeTransition());
                    PART_ItemsControl.ChildrenTransitions.Add(new RepositionThemeTransition());
                }
            }
            UpdateItemsSource(ItemsSource, null, !AutoMaxItemCount);
            this.SizeChanged += MoreButtonItemsControl_SizeChanged;
            this.Loaded += MoreButtonItemsControl_Loaded;

        }

        void MoreButtonItemsControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        void MoreButtonItemsControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (AutoMaxItemCount)
            {
                MaxItemCount = CalculateMaxItemCount(new Size(this.ActualWidth, this.ActualHeight));
                UpdateItemCollection();
            }
        }

        private int CalculateMaxItemCount(Size currentSize)
        {
            int columnCount = MaxColumn;
            //if (!double.IsNaN(this.Width) && (int)MaxColumn * ItemWidth > this.Width)
            //{
            //    columnCount = (int)(currentSize.Width / ItemWidth);
            //}
            //int rowCount = (int)(currentSize.Height / ItemHeight);
            if (!double.IsNaN(Width) && MaxColumn * this.ActualItemWidth > Width)
            {
                columnCount = (int)(currentSize.Width / this.ActualItemWidth);
            }
            double windowSize =System.Math.Floor(CoreWindow.GetForCurrentThread().Bounds.Height);
            if (AppBarHeight == 0.0)
                AppBarHeight = 200;
            var rowCount = (int)((windowSize - (AppBarHeight)) / this.ActualItemHeight);

            int count = columnCount * rowCount;
            if (FirstItemRowSpan > 1 || FirstItemColumnSpan > 1)
            {
                count -= FirstItemColumnSpan * FirstItemRowSpan;
                if (count > 0)
                {
                    return count + 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return count;
            }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            return base.MeasureOverride(availableSize);
        }

        private void UpdateItemsSource(object newSource, object oldSource, bool generateItems = true)
        {
            if (oldSource != null && oldSource is INotifyCollectionChanged)
            {
                (oldSource as INotifyCollectionChanged).CollectionChanged -= MoreButtonItemsControl_CollectionChanged;
            }
            if (PART_ItemsControl != null)
            {
                if (generateItems)
                {
                    GenerateItems();
                }
                if (newSource != null && newSource is INotifyCollectionChanged)
                {
                    (newSource as INotifyCollectionChanged).CollectionChanged += MoreButtonItemsControl_CollectionChanged;
                }
            }
            SetHasItems();
        }

        void MoreButtonItemsControl_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (PART_ItemsControl != null)
            {
                if (sender != null && (sender as IList).Count == 0 && e.Action == NotifyCollectionChangedAction.Reset)
                {
                    RemoveAllItems();
                }
                else
                {
                    if (e.NewItems != null)
                    {
                        AddItemCollection(e.NewItems);
                    }
                    if (e.OldItems != null)
                    {
                        foreach (var item in e.OldItems)
                        {
                            DeleteItem(item);
                        }
                    }
                }

                UpdateWidth();
            }
            SetHasItems();
        }

        private void RemoveAllItems()
        {
            lock (this)
            {
                int childCount = PART_ItemsControl.Children.Count;
                if (childCount > 0)
                {
                    for (int i = childCount - 1; i >= 0; --i)
                    {
                        PART_ItemsControl.Children.Remove(PART_ItemsControl.Children[i]);
                    }
                }
                _isMoreButtonVisible = false;
            }
        }

        private void SetHasItems()
        {
            HasItems = ItemsSource != null && ItemsSource is IList && (ItemsSource as IList).Count > 0;
        }

        private void AddItemCollection(ICollection items)
        {
            if (items != null)
            {
                foreach (var item in items)
                {
                    if (!AddItem(item))
                    {
                        break;
                    }
                }
            }
        }

        private ContentControl GetContent(object obj, DataTemplate template)
        {
            ContentControl cont = new ContentControl();
            cont.Content = obj;
            cont.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            cont.VerticalContentAlignment = VerticalAlignment.Stretch;
            cont.VerticalAlignment = VerticalAlignment.Stretch;
            cont.HorizontalAlignment = HorizontalAlignment.Stretch;
            cont.ContentTemplate = template;
            return cont;
        }

        private bool AddItem(object item)
        {
            lock (this)
            {
                int index = 0;
                if ((ItemsSource is IList))
                {
                    index = (ItemsSource as IList).IndexOf(item);
                }
                if (!ShowAllContents)
                {
                    if (PART_ItemsControl.Children.Count < MaxItemCount
                            || (PART_ItemsControl.Children.Count == MaxItemCount && !ShowMoreButton)
                                || index < MaxItemCount - 1)
                    {

                        AddContent(item, index);
                        if (PART_ItemsControl.Children.Count > MaxItemCount)
                        {
                            if (_isMoreButtonVisible)
                            {
                                PART_ItemsControl.Children.RemoveAt(MaxItemCount - 1);
                            }
                            else
                            {
                                PART_ItemsControl.Children.RemoveAt(MaxItemCount);
                            }
                        }
                        return true;
                    }
                    else if (PART_ItemsControl.Children.Count == MaxItemCount && !_isMoreButtonVisible)
                    {
                        AddMoreButton();
                    }
                    return false;
                }
                else
                {
                    AddContent(item, index);
                    return true;
                }
            }
        }

        private void AddContent(object item, int index)
        {
            ContentControl cont;
            if (index == 0 && FirstItemTemplate != null)
            {
                cont = GetContent(item, this.FirstItemTemplate);
            }
            else
            {
                cont = GetContent(item, this.ItemsTemplate);
            }
            if (index == 0)
            {
                cont.SetValue(VariableSizedWrapGrid.RowSpanProperty, FirstItemRowSpan);
                cont.SetValue(VariableSizedWrapGrid.ColumnSpanProperty, FirstItemColumnSpan);
                if (PART_ItemsControl.Children.Count > 0)
                {
                    ContentControl currentFirstItem = PART_ItemsControl.Children[0] as ContentControl;
                    currentFirstItem.SetValue(VariableSizedWrapGrid.RowSpanProperty, 1);
                    currentFirstItem.SetValue(VariableSizedWrapGrid.ColumnSpanProperty, 1);
                    currentFirstItem.ContentTemplate = ItemsTemplate;
                }
            }
            PART_ItemsControl.Children.Insert(index, cont);
        }

        private void AddMoreButton()
        {
            if (ShowMoreButton)
            {
                PART_ItemsControl.Children.RemoveAt(PART_ItemsControl.Children.Count - 1);
                ContentControl cont = GetContent(null, this.MoreButtonTemplate);
                PART_ItemsControl.Children.Add(cont);
                _isMoreButtonVisible = true;
            }
        }

        private void DeleteItem(object item)
        {
            lock (this)
            {
                ContentControl control = PART_ItemsControl.Children.FirstOrDefault(i =>
                    (i is ContentControl) && item.Equals((i as ContentControl).Content)) as ContentControl;
                if (control != null)
                {
                    if (PART_ItemsControl.Children.IndexOf(control) == 0 && PART_ItemsControl.Children.Count > 1)
                    {
                        ContentControl nextControl = PART_ItemsControl.Children[1] as ContentControl;
                        nextControl.SetValue(VariableSizedWrapGrid.RowSpanProperty, FirstItemRowSpan);
                        nextControl.SetValue(VariableSizedWrapGrid.ColumnSpanProperty, FirstItemColumnSpan);
                    }
                    PART_ItemsControl.Children.Remove(control);
                    if ((ItemsSource as IList).Count >= MaxItemCount && !ShowAllContents)
                    {
                        int index = MaxItemCount - 1;
                        if (ShowMoreButton)
                        {
                            index = MaxItemCount - 2;
                        }
                        AddItem((ItemsSource as IList)[index]);
                    }
                }
                if ((ItemsSource as IList).Count == MaxItemCount && _isMoreButtonVisible)
                {
                    DeleteMoreButton();
                    AddItem((ItemsSource as IList)[MaxItemCount - 1]);
                }
            }
        }

        private void DeleteMoreButton()
        {
            if (ShowMoreButton)
            {
                PART_ItemsControl.Children.RemoveAt(PART_ItemsControl.Children.Count - 1);
                _isMoreButtonVisible = false;
            }
        }

        private void GenerateItems()
        {
            if (PART_ItemsControl != null)
            {
                PART_ItemsControl.Children.Clear();
                _isMoreButtonVisible = false;
                AddItemCollection(ItemsSource as ICollection);
                UpdateWidth();
            }
        }

        private void UpdateItemCollection()
        {
            if (ItemsSource != null && PART_ItemsControl.Children.Count < MaxItemCount && PART_ItemsControl.Children.Count < (ItemsSource as IList).Count)
            {
                if (_isMoreButtonVisible)
                {
                    DeleteMoreButton();
                }
                int index = PART_ItemsControl.Children.Count - 1;
                if (index < 0)
                {
                    index = 0;
                }
                for (int i = index; i < (ItemsSource as IList).Count; i++)
                {
                    if (!AddItem((ItemsSource as IList)[i]))
                    {
                        break;
                    }
                }
            }
            else if (PART_ItemsControl.Children.Count > MaxItemCount + 1)
            {
                PART_ItemsControl.Children.RemoveAt(PART_ItemsControl.Children.Count - 1);
                UpdateItemCollection();
            }
            else if (PART_ItemsControl.Children.Count > MaxItemCount)
            {
                PART_ItemsControl.Children.RemoveAt(PART_ItemsControl.Children.Count - 1);
                AddMoreButton();
            }

            UpdateWidth();
        }

        private void UpdateWidth()
        {
            if (PART_ItemsControl != null)
            {
                double newWidth;
                if (MaxColumn > PART_ItemsControl.Children.Count)
                {
                    //newWidth = (PART_ItemsControl.Children.Count * ItemWidth) + (FirstItemColumnSpan - 1) * ItemWidth;
                    newWidth = (PART_ItemsControl.Children.Count * this.ActualItemWidth) + ((FirstItemColumnSpan - 1) * this.ActualItemWidth) - ItemMargin;
                }
                else
                {
                    // newWidth = MaxColumn * ItemWidth;
                    newWidth = MaxColumn * this.ActualItemWidth;
                }
                if (newWidth > 0)
                {
                    PART_ItemsControl.Width = newWidth;
                }
                else
                {
                    PART_ItemsControl.Width = 0;
                }


                //No. of item space used
                int usedCellCount = PART_ItemsControl.Children.Count + (this.FirstItemRowSpan * this.FirstItemColumnSpan) - 1;
                if (MaxColumn >= usedCellCount)
                {
                    UsedHeight = ItemHeight;
                }
                else
                {
                    double newUsedHeight = (usedCellCount / this.MaxColumn) * this.ActualItemHeight;
                    if (usedCellCount % this.MaxColumn > 0)
                    {
                        newUsedHeight += ItemHeight;
                    }
                    else
                    {
                        newUsedHeight -= ItemMargin;
                    }
                    UsedHeight = newUsedHeight;
                }
            }
        }

        #endregion

        #region Fields

        VariableSizedWrapGrid PART_ItemsControl;
        private bool _isMoreButtonVisible;

        #endregion
    }
}
