﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace IdentityMine.Controls
{
    [TemplateVisualState(GroupName = "ContentState", Name = "MainContent")]
    [TemplateVisualState(GroupName = "ContentState", Name = "TemporaryContent")]

    public class TemporaryContentControl : ContentControl
    {
        #region Constructors

        public TemporaryContentControl()
        {
            this.DefaultStyleKey = typeof(TemporaryContentControl);
            this.Loaded += new RoutedEventHandler(TemporaryContentControl_Loaded);
        }
        
        #endregion

        #region Properties

        public bool ShowTemporaryContent
        {
            get { return (bool)GetValue(ShowTemporaryContentProperty); }
            set { SetValue(ShowTemporaryContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowTemporaryContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowTemporaryContentProperty =
            DependencyProperty.Register("ShowTemporaryContent", typeof(bool), typeof(TemporaryContentControl), new PropertyMetadata(false, new PropertyChangedCallback(OnShowTemporaryContentChanged)));

        private static void OnShowTemporaryContentChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as TemporaryContentControl).UpdateSource();
        }

        public DataTemplate TemporaryContentTemplate
        {
            get { return (DataTemplate)GetValue(TemporaryContentTemplateProperty); }
            set { SetValue(TemporaryContentTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TemporaryContentTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TemporaryContentTemplateProperty =
            DependencyProperty.Register("TemporaryContentTemplate", typeof(DataTemplate), typeof(TemporaryContentControl), new PropertyMetadata(null));

        public bool SkipTemporaryContentAnimation
        {
            get { return (bool)GetValue(SkipTemporaryContentAnimationProperty); }
            set { SetValue(SkipTemporaryContentAnimationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SkipTemporaryContentAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SkipTemporaryContentAnimationProperty =
            DependencyProperty.Register("SkipTemporaryContentAnimation", typeof(bool), typeof(TemporaryContentControl), new PropertyMetadata(true));
        
        #endregion

        #region Methods

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            PART_MainContentBorder=this.GetTemplateChild("PART_MainContentBorder") as Border;
            PART_TemporaryContent=this.GetTemplateChild("PART_TemporaryContent") as Border;
            SetContentPositions();
        }

        private void UpdateSource()
        {            
            if (ShowTemporaryContent)
            {
                if (SkipTemporaryContentAnimation)
                {
                    VisualStateManager.GoToState(this, "ImmediateTemporaryContent", true);
                }
                else
                {
                    VisualStateManager.GoToState(this, "TemporaryContent", true);
                }
            }
            else
            {
                VisualStateManager.GoToState(this, "MainContent", true);
            }
        }

        void TemporaryContentControl_Loaded(object sender, RoutedEventArgs e)
        {
            SetContentPositions();
        }

        private void SetContentPositions()
        {
            if (ShowTemporaryContent)
            {
                if (PART_MainContentBorder != null && PART_MainContentBorder.RenderTransform is CompositeTransform)
                {
                    PART_MainContentBorder.RenderTransform = new CompositeTransform { TranslateX = 1000 };
                    PART_MainContentBorder.Opacity = 0;
                }
                if (PART_TemporaryContent != null && PART_TemporaryContent.RenderTransform is CompositeTransform)
                {
                    PART_TemporaryContent.RenderTransform = new CompositeTransform { TranslateX = 0 };
                    PART_TemporaryContent.Opacity = 1;
                }
            }
            else
            {
                if (PART_MainContentBorder != null && PART_MainContentBorder.RenderTransform is CompositeTransform)
                {
                    PART_MainContentBorder.RenderTransform = new CompositeTransform { TranslateX = 0 };
                    PART_MainContentBorder.Opacity = 1;
                }
                if (PART_TemporaryContent != null && PART_TemporaryContent.RenderTransform is CompositeTransform)
                {
                    PART_TemporaryContent.RenderTransform = new CompositeTransform { TranslateX = 1000 };
                    PART_TemporaryContent.Opacity = 0;
                }
            }
        }


        #endregion

        #region Fields

        Border PART_MainContentBorder, PART_TemporaryContent;

        #endregion
    }
}
