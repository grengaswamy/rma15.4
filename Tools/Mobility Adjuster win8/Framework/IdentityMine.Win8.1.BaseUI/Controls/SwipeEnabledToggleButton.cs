﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;


namespace IdentityMine.Controls
{
    public sealed class SwipeEnabledToggleButton : ToggleButton
    {
        #region Fields

        private Grid PART_ContentGrid;
        private CompositeTransform _contentGridTransform;
        private Grid PART_SelectedCheckGrid;
        private Rectangle PART_UnSelectedDragHighlight;
        private double lastDelta;

        #endregion

        #region Constructors

        public SwipeEnabledToggleButton()
        {
            this.DefaultStyleKey = typeof(SwipeEnabledToggleButton);
            this.ManipulationMode = ManipulationModes.All;
            this.IsRightTapEnabled = true;
        }

        #endregion

        #region Properties



        public Orientation SwipeOrientation
        {
            get { return (Orientation)GetValue(SwipeOrientationProperty); }
            set { SetValue(SwipeOrientationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SwipeOrientation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SwipeOrientationProperty =
            DependencyProperty.Register("SwipeOrientation", typeof(Orientation), typeof(SwipeEnabledToggleButton), new PropertyMetadata(Orientation.Vertical));

        public double MaximumSwipeDelta
        {
            get { return (double)GetValue(MaximumSwipeDeltaProperty); }
            set { SetValue(MaximumSwipeDeltaProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaximumSwipeDelta.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaximumSwipeDeltaProperty =
            DependencyProperty.Register("MaximumSwipeDelta", typeof(double), typeof(SwipeEnabledToggleButton), new PropertyMetadata(15.0));

        public Brush CheckmarkBrush
        {
            get { return (Brush)GetValue(CheckmarkBrushProperty); }
            set { SetValue(CheckmarkBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CheckmarkBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CheckmarkBrushProperty =
            DependencyProperty.Register("CheckmarkBrush", typeof(Brush), typeof(SwipeEnabledToggleButton), new PropertyMetadata(new SolidColorBrush(Colors.Blue)));

        #endregion

        #region Methods
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            PART_ContentGrid = this.GetTemplateChild("PART_ContentGrid") as Grid;
            if (PART_ContentGrid != null)
            {
                _contentGridTransform = PART_ContentGrid.RenderTransform as CompositeTransform;
                if (_contentGridTransform == null)
                {
                    _contentGridTransform = new CompositeTransform();
                    PART_ContentGrid.RenderTransform = _contentGridTransform;
                }
            }
            PART_SelectedCheckGrid = this.GetTemplateChild("PART_SelectedCheckGrid") as Grid;
            PART_UnSelectedDragHighlight = this.GetTemplateChild("PART_UnSelectedDragHighlight") as Rectangle;
        }

        protected override void OnRightTapped(RightTappedRoutedEventArgs e)
        {
            base.OnRightTapped(e);
            this.IsChecked = !this.IsChecked;
            e.Handled = true;
        }

        protected override void OnManipulationDelta(ManipulationDeltaRoutedEventArgs e)
        {
            base.OnManipulationDelta(e);
            if (_contentGridTransform != null)
            {
                if (SwipeOrientation == Orientation.Vertical && e.Cumulative.Translation.Y != 0)
                {
                    lastDelta += e.Delta.Translation.Y;
                    _contentGridTransform.TranslateY = GetTranslateValue(lastDelta);
                }
                else if (SwipeOrientation == Orientation.Horizontal && e.Cumulative.Translation.X != 0)
                {
                    lastDelta += e.Delta.Translation.X;
                    _contentGridTransform.TranslateX = GetTranslateValue(lastDelta);
                }
            }
        }

        private double GetTranslateValue(double change)
        {
            if (change >= MaximumSwipeDelta || change <= -MaximumSwipeDelta)
            {
                if (this.IsChecked == true)
                {
                    PART_SelectedCheckGrid.Opacity = 0;
                }
                else
                {
                    PART_UnSelectedDragHighlight.Opacity = 1;
                }

                return change > 0 ? MaximumSwipeDelta : -MaximumSwipeDelta;
            }
            else
            {
                double opacity = MaximumSwipeDelta / change;
                if (change < 0)
                    opacity = -opacity;
                if (this.IsChecked == true)
                {
                    PART_SelectedCheckGrid.Opacity = opacity;
                }
                else
                {
                    PART_UnSelectedDragHighlight.Opacity = opacity;
                }
                return change;
            }
        }

        protected override void OnManipulationCompleted(ManipulationCompletedRoutedEventArgs e)
        {
            base.OnManipulationCompleted(e);
            if (lastDelta != 0)
            {
                if (lastDelta >= MaximumSwipeDelta || lastDelta <= -MaximumSwipeDelta)
                {
                    if (this.IsChecked == true)
                    {
                        PART_SelectedCheckGrid.Opacity = 1;
                    }
                    else
                    {
                        PART_UnSelectedDragHighlight.Opacity = 0;
                    }
                    this.IsChecked = !this.IsChecked;
                }
            }
            _contentGridTransform.TranslateY = 0;
            _contentGridTransform.TranslateX = 0;
            lastDelta = 0;
        }

        #endregion
    }
}
