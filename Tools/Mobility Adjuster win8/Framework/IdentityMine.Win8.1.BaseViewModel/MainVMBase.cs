﻿using System;
using System.Net;


#if NETFX_CORE
using Windows.Storage;
using System.Net.NetworkInformation;
#else
using System.Windows.Threading;
using Microsoft.Phone.Info;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Net.NetworkInformation;
#endif

using System.Diagnostics;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Globalization;



#if LAKEVIEW
using Microsoft.Xna.Framework.Storage;
#endif

namespace IdentityMine.ViewModel
{

    #region Enumerations

    /// <summary>
    /// Specifies how app is lauched
    /// </summary>
    public enum AppLaunchMode
    {
        /// <summary>
        /// Launched through fast app switching
        /// </summary>
        FAS,

        /// <summary>
        /// Launched through Tombstone
        /// </summary>
        Tombstone,

        /// <summary>
        /// Launch though app start or app resume
        /// </summary>
        Default
    }

    #endregion

    public abstract class MainVMBase : ResultItem
    {
        private bool isNetworkOnline;
        public bool isInternetAvailable;

#if WP7
        private readonly Version version78 = new Version(7, 10, 8858);
#endif

        #region Static Members

        [Obsolete("Use property: AppLaunchMode instead.")]
        public static bool IsFromTombStoning { get; set; }

        [Obsolete("Use property: AppLaunchMode instead.")]
        public static bool IsAppInstancePreserved { get; set; }

        /// <summary>
        /// Gets the app launch mode
        /// </summary>
        public static AppLaunchMode AppLaunchMode
        {
            get
            {
                if (IsAppInstancePreserved && IsFromTombStoning)
                    return AppLaunchMode.FAS;
                else if (!IsAppInstancePreserved && IsFromTombStoning)
                    return AppLaunchMode.Tombstone;
                else
                    return AppLaunchMode.Default;
            }
        }

        protected static MainVMBase _instance;

        public static MainVMBase InstanceBase
        {
            get
            {
                return _instance;
            }
        }

        #endregion

        public MainVMBase()
        {
#if LAKEVIEW
            _getCacheRes ult = CacheContainer.BeginOpenCacheContainer((state) =>
            {
                IsolatedStorage = CacheContainer.EndOpenCacheContainer(_getCacheResult);

                _getCacheResult = null;
            }, null);

            while (_getCacheResult != null)
                Thread.Sleep(50);

            _getCacheResult = null;
#elif NETFX_CORE
            IsolatedStorage = Windows.Storage.ApplicationData.Current.LocalFolder;
            LocalSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
#endif
        }

        #region Properties



        public virtual string BingMapKey
        {
            get
            {
                return "";
            }
        }

#if WINDOWS_PHONE
        public System.IO.IsolatedStorage.IsolatedStorageFile IsolatedStorage { get; set; }
#elif LAKEVIEW
        public Microsoft.Xna.Framework.Storage.StorageContainer IsolatedStorage { get; set; }
#elif NETFX_CORE
        public StorageFolder IsolatedStorage { get; set; }

        public ApplicationDataContainer LocalSettings { get; set; }
#endif


#if NETFX_CORE
        public Windows.UI.Core.CoreDispatcher Dispatcher { get; set; }
#else
        public System.Windows.Threading.Dispatcher Dispatcher { get; set; }
#endif
        public abstract INavigationApp AppInstance { get; set; }

        /// <summary>
        /// Framework require to create a Page at the UI project level and assign the Uri path to this property
        /// </summary>
        public static string PhonePage;

        public abstract IServiceRequest CurrentService { get; set; }

#if WINDOWS_PHONE
        public virtual string DeviceID
        {
            get
            {
                return Settings.DeviceID;
            }
        }
#endif


        public CookieContainer Cookie;

        public SettingsBase Settings { get; set; }

        public abstract ServiceRequestFactory ServiceFactory { get; }

#if NETFX_CORE

#else

        public Dictionary<string, string> UrlMapper
        {
            get
            {
                Dictionary<string, string> urlMapper;
                if (IsolatedStorageSettings.ApplicationSettings.Contains("UrlMapper"))
                {
                    urlMapper = IsolatedStorageSettings.ApplicationSettings["UrlMapper"] as Dictionary<string, string>;
                }
                else
                {
                    urlMapper = new Dictionary<string, string>();
                }
                return urlMapper;
            }
            set
            {
                IsolatedStorageSettings.ApplicationSettings["UrlMapper"] = value;
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }


        public Dictionary<string, string> VolatileUrlMapper
        {
            get;
            set;
        }

#endif
        public event EventHandler NetworkChanged;

        public bool IsNetworkOnline
        {
            get
            {
                return isNetworkOnline;
            }
            set
            {
                isNetworkOnline = value;
#if !NETFX_CORE
                if (Dispatcher.CheckAccess())
                {
                   RaisePropertyChanged("IsNetworkOnline");
                }
                else
                {
                   Dispatcher.BeginInvoke(() => RaisePropertyChanged("IsNetworkOnline"));                  
                }
#else
                RaisePropertyChanged("IsNetworkOnline");
#endif
            }
        }

        public event EventHandler InternetAvailabilityChanged;

        public bool IsInternetAvailable
        {
            get
            {
                return isInternetAvailable;
            }
            set
            {
                if (isInternetAvailable != value)
                {
                    isInternetAvailable = value;
                    RaisePropertyChanged("IsInternetAvailable");
                    if (InternetAvailabilityChanged != null)
                        InternetAvailabilityChanged(this, null);
                }
            }
        }

#if WP7
        public bool IsVersion78 
        { 
            get { return Environment.OSVersion.Version >= version78; } 
        }
#endif



        #endregion

        #region Methods

        public abstract new string GetStringResource(string key);

        /// <summary>
        /// Checking the response is valid
        /// This is done to check the result in the Authenticated Networks like Starbucks
        /// </summary>
        /// <param name="responseString"></param>
        /// <returns></returns>
        public virtual bool CheckForFaultResult(string responseString, string ServiceID = null)
        {
            return false;
        }

        public virtual void HandleError(string customError, string actualError = null, Action action = null, bool isClosableOnlyOnAction = false)
        {
        }

        public virtual void HandleMessage(string message)
        {
        }

        //Get the Placeholder Icon for a Specific StateInfo Object
        public virtual Uri GetPlaceHolder(StateInfo stateInfo)
        {
            return null;
        }

        public void Navigate(ResultItem resultItem)
        {
            IServiceRequest sr = ServiceFactory.GetService(resultItem); //JJBUG or GetService can manage the Push Queue Instead of calling Load() below.

            if (sr == null) return;

            CurrentService = sr;
            try
            {
                sr.ExecuteAsync();
            }
            catch (TaskCanceledException)
            {
                return;
            }
#if NETFX_CORE
            AppInstance.Navigate(sr);
#else
            AppInstance.Navigate(new Uri(PhonePage + "?id=" + sr.Id, UriKind.Relative));
#endif

        }

        public void AddServiceAndNavigate(IServiceRequest service)
        {
            ServiceFactory.AddService(service);
            Navigate(service.Id);
        }

        public void Navigate(string serviceId)
        {
            IServiceRequest sr = ServiceFactory.GetService(serviceId); //JJBUG or GetService can manage the Push Queue Instead of calling Load() below.

            CurrentService = sr;
            try
            {
                sr.ExecuteAsync();
            }
            catch (TaskCanceledException)
            {
                return;
            }
            
#if NETFX_CORE
            AppInstance.Navigate(sr);
#else
            AppInstance.Navigate(new Uri(PhonePage + "?id=" + serviceId, UriKind.Relative));
#endif
        }

        public virtual void StoreExpiryTime(string id, string result)
        {

        }

        public virtual void UpdateCookie(WebResponse response)
        {
        }

        //ELBUG: I'm not sure if Lakeview needs an equivalent to IsCacheExpired. The system will forcibly delete cached files anyway (though not while the app is runnning)
#if WINDOWS_PHONE
        public virtual bool IsCacheExpired(string fileName)
        {
            try
            {
                if (IsolatedStorageSettings.ApplicationSettings != null
                    && IsolatedStorageSettings.ApplicationSettings.Contains(fileName)
                    && IsolatedStorageSettings.ApplicationSettings[fileName] != null)
                {
                    DateTime expiryTime = DateTime.Parse(IsolatedStorageSettings.ApplicationSettings[fileName].ToString(), CultureInfo.InvariantCulture);

                    if (expiryTime < DateTime.Now.ToUniversalTime())
                        return true;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message); //JJBUG #PERF need to avoid this exception at begining.
            }

            return false;
        }
#elif NETFX_CORE
        public virtual bool IsCacheExpired(string fileName)
        {
            try
            {
                if (LocalSettings.Values.ContainsKey(fileName)
                    && LocalSettings.Values[fileName] != null)
                {
                    DateTime expiryTime = DateTime.Parse(LocalSettings.Values[fileName].ToString());

                    return (expiryTime < DateTime.Now.ToUniversalTime());
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message); //JJBUG #PERF need to avoid this exception at begining.
            }

            return true;
        }
#endif

        public virtual void DeActivate(bool isTombStoning = false)
        {
            if (!isTombStoning)
            {

#if NETFX_CORE
#else
                //Delete all the tempfiles used for the tombstoning, since this is a proper exit
                foreach (string fileName in IsolatedStorage.GetFileNames("*.tmpfile"))
                {
                    if (fileName.EndsWith(".tmpfile"))
                    {
                        //adding try catch to avoid WP8 specific crashes on delete even if file exists
                        try
                        {
                            IsolatedStorage.DeleteFile(fileName);
                        }
                        catch { }
                    }
                }

#endif
            }

        }

        public virtual Task Activate(bool isTombStoning = false, bool isAppInstancePreserved = false)
        {
            IsFromTombStoning = isTombStoning;
            IsAppInstancePreserved = isAppInstancePreserved;
            // initial value of the online variable


#if WINDOWS_PHONE
            IsNetworkOnline = DeviceNetworkInformation.IsNetworkAvailable;
#if DEBUG
            DispatcherTimer timer;
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 5);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
#endif
#endif

            if (!isAppInstancePreserved)
            {
                SetupNetworkChange();
            }
#if WP7
            return TaskEx.FromResult(false);
#else
            return Task.FromResult(false);
#endif

        }


#if WINDOWS_PHONE
#if DEBUG
        void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                var pmUsage = (long)DeviceStatus.ApplicationPeakMemoryUsage; //(long)DeviceExtendedProperties.GetValue("ApplicationPeakMemoryUsage");
                var maxMemUsage = (long)DeviceStatus.ApplicationMemoryUsageLimit; //(long)DeviceExtendedProperties.GetValue("DeviceTotalMemory");
                var currentMemUsage = (long)DeviceStatus.ApplicationCurrentMemoryUsage;
                pmUsage /= 1024 * 1024;
                maxMemUsage /= 1024 * 1024;
                currentMemUsage /= 1024 * 1024;
                Debug.WriteLine(String.Format("Mem : {0} / {1} MB, Current: {2} MB", pmUsage, maxMemUsage, currentMemUsage));
            }
            catch
            {

            }
        }
#endif
#endif
        #endregion


        public async Task<IServiceRequest> FindServiceRequestFromQueryString(IDictionary<string, string> queryString)
        {
            IServiceRequest serviceRequest = null;
            StateInfo stateInfo = null;
            string queryValue;
            if (queryString.TryGetValue("StateInfo", out queryValue))
            {
                stateInfo = new StateInfo(queryValue);
                serviceRequest = await MainVMBase.InstanceBase.ServiceFactory.SetServiceByClassName(stateInfo);
            }
            if (serviceRequest != null)
                return serviceRequest;
            else
                return OnProcessQueryStringForServiceRequest(queryString);
        }

        protected virtual IServiceRequest OnProcessQueryStringForServiceRequest(IDictionary<string, string> queryString)
        {
            return null;
        }

#if WINDOWS_PHONE

        private void SetupNetworkChange()
        {
            // Get current network availalability and store the 
            DeviceNetworkInformation.NetworkAvailabilityChanged += (s, e) =>
            {
                IsNetworkOnline = DeviceNetworkInformation.IsNetworkAvailable;
                if (NetworkChanged != null)
                    NetworkChanged(this, null);
            };
        }

#elif NETFX_CORE

        private void SetupNetworkChange()
        {
            // Get current network availalability and store the 
            // initial value of the online variable

            IsNetworkOnline = NetworkInterface.GetIsNetworkAvailable();

            // Now add a network change event handler to indicate
            // network availability 
            NetworkChange.NetworkAddressChanged +=
                new NetworkAddressChangedEventHandler(OnNetworkChange);
        }

        private void OnNetworkChange(object sender, EventArgs e)
        {
            IsNetworkOnline = NetworkInterface.GetIsNetworkAvailable();
            if (NetworkChanged != null)
                NetworkChanged(this, null);
        }
#endif
    }
}
