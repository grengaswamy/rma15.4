﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace IdentityMine.Helper
{
    public static class BindingHelper
    {
        public static Binding GetBinding(object source, string path, BindingMode mode = BindingMode.TwoWay)
        {
            Binding binding = new Binding();
            binding.Source = source;
            binding.Path = new PropertyPath(path);
            binding.Mode = mode;
            return binding;
        }
    }
}
