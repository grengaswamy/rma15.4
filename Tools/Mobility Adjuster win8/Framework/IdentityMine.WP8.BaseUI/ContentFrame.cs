﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using IdentityMine.ViewModel;
using System.Windows.Navigation; 
using System.Diagnostics;
using System.Collections.ObjectModel;

#if WINDOWS_PHONE
using Microsoft.Phone.Controls;

#endif

namespace IdentityMine.BaseUI
{
    public class ContentFrame : Grid
    {

        #region Fields

        TemplateType type = null;
        bool isBack;

        #if WINDOWS_PHONE
                PhoneApplicationPage _page;
        #else
                FrameworkElement _page;
        #endif

        #endregion

        #region Properties

        public TemplateTypeCollection TemplateTypes
        {
            get { return (TemplateTypeCollection)GetValue(TemplateTypes1Property); }
            set { SetValue(TemplateTypes1Property, value); }
        }

        // Using a DependencyProperty as the backing store for TemplateTypes1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TemplateTypes1Property =
            DependencyProperty.Register("TemplateTypes", typeof(TemplateTypeCollection), typeof(ContentFrame), null);

        public DataTemplate DefaultTemplate
        {
            get;
            set;
        }

        #endregion

        #region Methods


#if WINDOWS_PHONE
        public void LoadContent(PhoneApplicationPage page, IResult result, bool isBack)
#else
        public void LoadContent(FrameworkElement page, IResult result, bool isBack)
#endif
        {
            _page = page;
            this.isBack = isBack;

            if (result == null) return;

            type = FindTemplateType(result.ClassID); 

            if (type != null)
            {

                if (type.IsSingleTon)
                {

                    if (type.Element == null)
                    {
                        DataTemplate template = FindTemplate(type);
                        type.Element = template.LoadContent() as BaseUserControl;
                    }

                }
                else
                {
                    DataTemplate template = FindTemplate(type);
                    type.Element = template.LoadContent() as BaseUserControl;
                }     
                
                type.Element.DataContext = result;
                
                if (!Children.Contains(type.Element))
                {
                    Children.Add(type.Element);
                }

#if WINDOWS_PHONE

                if (page != null)
                {
                    page.ApplicationBar = type.Element.ApplicationBar;
                    try
                    {
                        type.Element.State = page.State;
                    }
                    catch
                    {
                        Debug.WriteLine("Exception on ContentFrame while setting State");
                    }
                }
#endif

              LoadCompleted(result, page);
                
            }
        }

        void result_ResultLoaded(object sender, EventArgs e)
        {
            if (type != null)
            {
                type.Element.ResultAndControlLoaded(_page, isBack);
            }

            ((IResult)sender).ResultLoaded -= new EventHandler(result_ResultLoaded); 
        }


#if WINDOWS_PHONE
        public void LoadCompleted(IResult result, PhoneApplicationPage page)
#else
        public void LoadCompleted(IResult result, FrameworkElement page)
#endif

        {
            _page = page;
            if (result.IsLoaded)
            {
                if (type != null)
                {
                    type.Element.ResultAndControlLoaded(_page, isBack);
                }
            }
            else
                result.ResultLoaded += new EventHandler(result_ResultLoaded);
        }

        public void UnLoadContent(NavigationEventArgs e, bool isBack)
        {
            if (type != null && type.Element != null && type.IsSingleTon)
            {
                type.Element.NavigatedFrom(e,isBack);
                Children.Clear();
            }
            if (MainVMBase.IsAppInstancePreserved)
                MainVMBase.IsAppInstancePreserved = false;
            if (MainVMBase.IsFromTombStoning)
                MainVMBase.IsFromTombStoning = false;
        }

        public void OnBackNavigation(System.ComponentModel.CancelEventArgs e)
        {
            if (type != null && type.Element != null)
            {
                type.Element.BackNavigated(e);
            }
        }

        public TemplateType FindTemplateType(string typeName)
        {
            return TemplateTypes.SingleOrDefault(e => e.DataTypeName == typeName);
        }

        public DataTemplate FindTemplate(TemplateType template)
        {
            return template != null ? template.Template : DefaultTemplate;
        }
        #endregion

       
    }

    public class TemplateTypeCollection : ObservableCollection<TemplateType>
    { }

    public class TemplateType
    {
        public TemplateType()
        {
            IsSingleTon = true;
        }

        public string DataTypeName
        {
            get;
            set;
        }

        public DataTemplate Template
        {
            get;
            set;
        }

        public bool IsSingleTon
        {
            get;
            set;
        }

        public BaseUserControl Element { get; set; }
    }
}
