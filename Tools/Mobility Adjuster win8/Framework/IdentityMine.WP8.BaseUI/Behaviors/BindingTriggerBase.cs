﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace IdentityMine.BaseUI.Behaviors
{
    public class BindingTriggerBase : TriggerBase<FrameworkElement>
    {
        #region Overrides

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Loaded += new RoutedEventHandler(AssociatedObject_Loaded);
            AssociatedObject.Unloaded += new RoutedEventHandler(AssociatedObject_Unloaded);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.Loaded -= new RoutedEventHandler(AssociatedObject_Loaded);
            AssociatedObject.Unloaded -= new RoutedEventHandler(AssociatedObject_Unloaded);

            Uninitialize();
        }

        #endregion

        #region Binding

        Binding _binding;
        public Binding Binding
        {
            get { return _binding; }
            set
            {
                if (_binding != value)
                {
                    _binding = value;

                    if (_target != null)
                    {
                        if (_binding != null)
                        {
                            _target.SetBinding(BindingTarget.BindingValueProperty, Binding);
                        }
                        else
                        {
                            _target.ClearValue(BindingTarget.BindingValueProperty);
                        }
                    }
                }
            }
        }

        #endregion

        #region BindingValue

        protected object BindingValue
        {
            get { return _target != null ? _target.BindingValue : null; }
        }

        #endregion

        #region Virtual Methods

        protected virtual void OnBindingValueChanged()
        {
        }

        #endregion

        #region Private Methods

        void Initialize()
        {
            _target = new BindingTarget();
            _target.BindingValueChanged += new EventHandler(_target_BindingValueChanged);

            _dataContextChangedCallback = new Action(OnDataContextChanged);
            DataContextChangedHelper.Attach(AssociatedObject, _dataContextChangedCallback);

            if (Binding != null)
            {
                _target.SetBinding(BindingTarget.BindingValueProperty, Binding);
            }
        }

        void Uninitialize()
        {
            DataContextChangedHelper.Detach(AssociatedObject, _dataContextChangedCallback);

            if (_target != null)
            {
                _target.ClearValue(BindingTarget.BindingValueProperty);
                _target = null;
            }
        }

        void AssociatedObject_Unloaded(object sender, RoutedEventArgs e)
        {
            Uninitialize();
        }

        void AssociatedObject_Loaded(object sender, RoutedEventArgs e)
        {
            Initialize();
        }

        void OnDataContextChanged()
        {
            if (_target != null)
            {
                _target.DataContext = AssociatedObject.DataContext;
            }
        }

        void _target_BindingValueChanged(object sender, EventArgs e)
        {
            OnBindingValueChanged();
        }

        #endregion
        Action _dataContextChangedCallback;
        BindingTarget _target;
        #region class BindingTarget

        class BindingTarget : FrameworkElement
        {
            public FrameworkElement Element
            {
                get;
                set;
            }

            public object BindingValue
            {
                get { return (object)GetValue(BindingValueProperty); }
                set { SetValue(BindingValueProperty, value); }
            }

            // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
            public static readonly DependencyProperty BindingValueProperty =
                DependencyProperty.Register("BindingValue", typeof(object), typeof(BindingTarget),
                new PropertyMetadata(null, new PropertyChangedCallback(OnBindingValueChanged)));

            static void OnBindingValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
            {
                ((BindingTarget)d).OnBindingValueChanged(e.OldValue, e.NewValue);
            }

            public event EventHandler BindingValueChanged;

            void OnBindingValueChanged(object oldValue, object newValue)
            {
                if (BindingValueChanged != null)
                {
                    BindingValueChanged(this, EventArgs.Empty);
                }
            }
        }

        #endregion


    }
}
