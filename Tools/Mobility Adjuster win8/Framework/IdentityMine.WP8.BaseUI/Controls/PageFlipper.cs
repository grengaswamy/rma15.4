﻿using Microsoft.Phone.Controls;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace IdentityMine.BaseUI
{
    public class PageFlipper : Control
    {

        #region Fields

        ContentControl PART_Content1, PART_Content2, PART_Content3, PART_ScaleContent;
        ScrollViewer PART_ScrollViewer;
        Border PART_Border;
        ContentControl _selectedContentControl;
        TranslateTransform _translateTransform1, _translateTransform2, _translateTransform3, _selectedTranslateTransform;
        ScaleTransform _scaleTransform1, _scaleTransform2, _scaleTransform3, _selectedScaleTransform;
        //GestureListener _gestureService;
        double _itemMargin = 10;
        double _swipeThreshold = 100;
        double _maxScale = 3;
        double initialScale;
        bool _isLoaded;
        bool _animateTransition;
        double _dragHorizontal;
        Point _scalTransformOrigin = new Point(0.5, 0.5);
        bool animationStarted;
        bool _isTranslating;
        #endregion

        public PageFlipper()
        {
            this.DefaultStyleKey = typeof(PageFlipper);
            Loaded += new RoutedEventHandler(PageFlipper_Loaded);
        }


        public EventHandler<EventArgs> SelectionChanged;
        public EventHandler<EventArgs> OnSartScaling;
        #region Properties

        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register("SelectedIndex", typeof(int), typeof(PageFlipper), new PropertyMetadata(-1, new PropertyChangedCallback(OnSelectedIndexChanged)));

        private void SetSelectedIndexFromItem(object p)
        {
            if (Collection != null && Collection.Count > 0)
            {
                SelectedIndex = Collection.IndexOf(p);
                SetContents();
            }
        }


        // Using a DependencyProperty as the backing store for Content.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Content2Property =
            DependencyProperty.Register("Content2", typeof(object), typeof(PageFlipper), new PropertyMetadata(null));


        public IList Collection
        {
            get { return (IList)GetValue(CollectionProperty); }
            set { SetValue(CollectionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Collection.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CollectionProperty =
            DependencyProperty.Register("Collection", typeof(IList), typeof(PageFlipper), new PropertyMetadata(null, new PropertyChangedCallback(OnCollectionChanged)));


        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ItemTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemTemplateProperty =
            DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(PageFlipper), new PropertyMetadata(null));



        public DataTemplate ScaleItemTemplate
        {
            get { return (DataTemplate)GetValue(ScaleItemTemplateProperty); }
            set { SetValue(ScaleItemTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleItemTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleItemTemplateProperty =
            DependencyProperty.Register("ScaleItemTemplate", typeof(DataTemplate), typeof(PageFlipper), new PropertyMetadata(null));



        public bool ShowNextItemPreview
        {
            get { return (bool)GetValue(ShowNextItemPreviewProperty); }
            set { SetValue(ShowNextItemPreviewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowNextItemPreview.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowNextItemPreviewProperty =
            DependencyProperty.Register("ShowNextItemPreview", typeof(bool), typeof(PageFlipper), new PropertyMetadata(true));

        public bool EnableScaling
        {
            get { return (bool)GetValue(EnableScalingProperty); }
            set { SetValue(EnableScalingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EnableScaling.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableScalingProperty =
            DependencyProperty.Register("EnableScaling", typeof(bool), typeof(PageFlipper), new PropertyMetadata(false));

        public bool EnableTapScaling
        {
            get { return (bool)GetValue(EnableTapScalingProperty); }
            set { SetValue(EnableTapScalingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EnableTapScaling.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableTapScalingProperty =
            DependencyProperty.Register("EnableTapScaling", typeof(bool), typeof(PageFlipper), new PropertyMetadata(true));


        public bool IsScaled
        {
            get { return (bool)GetValue(IsScaledProperty); }
            set
            {
                if (!IsScaled && OnSartScaling != null)
                    OnSartScaling(null, null);
                SetValue(IsScaledProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for IsScaled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsScaledProperty =
            DependencyProperty.Register("IsScaled", typeof(bool), typeof(PageFlipper), new PropertyMetadata(false));


        public bool EnableRepeating
        {
            get { return (bool)GetValue(EnableRepeatingProperty); }
            set { SetValue(EnableRepeatingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EnableRepeating.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableRepeatingProperty =
            DependencyProperty.Register("EnableRepeating", typeof(bool), typeof(PageFlipper), new PropertyMetadata(false));

        private double _leftMaxTranslateX;
        private void UpdateLeftMaxTranslateX()
        {
            if (PART_ScaleContent != null && _selectedScaleTransform != null)
            {
                if (PART_ScaleContent.ActualWidth * _selectedScaleTransform.ScaleX >= PART_Border.ActualWidth)
                {
                    _leftMaxTranslateX = (PART_ScaleContent.ActualWidth * (1 - PART_ScaleContent.RenderTransformOrigin.X) * (_selectedScaleTransform.ScaleX - 1)) - (PART_Border.ActualWidth - PART_ScaleContent.ActualWidth) / 2;
                }
                else
                {
                    _leftMaxTranslateX = (PART_ScaleContent.ActualWidth * (1 - PART_ScaleContent.RenderTransformOrigin.X) * (_selectedScaleTransform.ScaleX - 1)) - ((PART_ScaleContent.ActualWidth * _selectedScaleTransform.ScaleX) - PART_ScaleContent.ActualWidth) / 2;
                }

            }
            else
            {
                _leftMaxTranslateX = 0;
            }
        }

        private double _topMaxTranslateY;
        private void UpdateTopMaxTranslateY()
        {
            if (PART_ScaleContent != null && _selectedScaleTransform != null)
            {
                if (PART_ScaleContent.ActualHeight * _selectedScaleTransform.ScaleY >= PART_Border.ActualHeight)
                {
                    _topMaxTranslateY = (PART_ScaleContent.ActualHeight * (1 - PART_ScaleContent.RenderTransformOrigin.Y) * (_selectedScaleTransform.ScaleY - 1)) - (PART_Border.ActualHeight - PART_ScaleContent.ActualHeight) / 2;
                }
                else
                {
                    _topMaxTranslateY = (PART_ScaleContent.ActualHeight * (1 - PART_ScaleContent.RenderTransformOrigin.Y) * (_selectedScaleTransform.ScaleY - 1)) - ((PART_ScaleContent.ActualHeight * _selectedScaleTransform.ScaleY) - PART_ScaleContent.ActualHeight) / 2;
                }
            }
            else
            {
                _topMaxTranslateY = 0;
            }
        }

        private double _rightMaxTranslateX;
        private void UpdateRightMaxTranslateX()
        {
            if (PART_ScaleContent != null && _selectedScaleTransform != null)
            {
                if (PART_ScaleContent.ActualWidth * _selectedScaleTransform.ScaleX >= PART_Border.ActualWidth)
                {
                    _rightMaxTranslateX = (PART_ScaleContent.RenderTransformOrigin.X * (_selectedScaleTransform.ScaleX - 1) * PART_ScaleContent.ActualWidth) - (PART_Border.ActualWidth - PART_ScaleContent.ActualWidth) / 2; ;
                }
                else
                {
                    _rightMaxTranslateX = (PART_ScaleContent.RenderTransformOrigin.X * (_selectedScaleTransform.ScaleX - 1) * PART_ScaleContent.ActualWidth) - ((PART_ScaleContent.ActualWidth * _selectedScaleTransform.ScaleX) - PART_ScaleContent.ActualWidth) / 2;
                }

            }
            else
            {
                _rightMaxTranslateX = 0;
            }
        }

        private double _bottomMaxTranslateY;
        private void UpdateBottomMaxTranslateY()
        {
            if (PART_ScaleContent != null && _selectedScaleTransform != null)
            {
                if (PART_ScaleContent.ActualHeight * _selectedScaleTransform.ScaleY >= PART_Border.ActualHeight)
                {
                    _bottomMaxTranslateY = (PART_ScaleContent.RenderTransformOrigin.Y * (_selectedScaleTransform.ScaleY - 1) * PART_ScaleContent.ActualHeight) - (PART_Border.ActualHeight - PART_ScaleContent.ActualHeight) / 2;
                }
                else
                {
                    _bottomMaxTranslateY = (PART_ScaleContent.RenderTransformOrigin.Y * (_selectedScaleTransform.ScaleY - 1) * PART_ScaleContent.ActualHeight) - ((PART_ScaleContent.ActualHeight * _selectedScaleTransform.ScaleY) - PART_ScaleContent.ActualHeight) / 2;
                }

            }
            else
            {
                _bottomMaxTranslateY = 0;
            }
        }

        #endregion

        #region Methods

        public override void OnApplyTemplate()
        {
            PART_Content1 = GetTemplateChild("PART_Content1") as ContentControl;
            PART_Content2 = GetTemplateChild("PART_Content2") as ContentControl;
            PART_Content3 = GetTemplateChild("PART_Content3") as ContentControl;
            PART_ScaleContent = GetTemplateChild("PART_ScaleContent") as ContentControl;
            PART_ScrollViewer = GetTemplateChild("PART_ScrollViewer") as ScrollViewer;
            PART_Border = GetTemplateChild("PART_Border") as Border;

            _translateTransform1 = new TranslateTransform();
            _translateTransform2 = new TranslateTransform();
            _translateTransform3 = new TranslateTransform();
            _scaleTransform1 = new ScaleTransform();
            _scaleTransform2 = new ScaleTransform();
            _scaleTransform3 = new ScaleTransform();
            _selectedScaleTransform = new ScaleTransform();
            _selectedTranslateTransform = new TranslateTransform();

            if (PART_Content1 != null)
            {
                TransformGroup transform1 = new TransformGroup();
                transform1.Children.Add(_translateTransform1);
                transform1.Children.Add(_scaleTransform1);
                PART_Content1.RenderTransform = transform1;
            }
            if (PART_Content2 != null)
            {
                TransformGroup transform2 = new TransformGroup();
                transform2.Children.Add(_translateTransform2);
                transform2.Children.Add(_scaleTransform2);
                PART_Content2.RenderTransform = transform2;
            }
            if (PART_Content3 != null)
            {
                TransformGroup transform3 = new TransformGroup();
                transform3.Children.Add(_translateTransform3);
                transform3.Children.Add(_scaleTransform3);
                PART_Content3.RenderTransform = transform3;
            }

            if (PART_ScaleContent != null)
            {
                TransformGroup transform4 = new TransformGroup();
                transform4.Children.Add(_selectedScaleTransform);
                transform4.Children.Add(_selectedTranslateTransform);
                PART_ScaleContent.RenderTransform = transform4;
                PART_ScaleContent.RenderTransformOrigin = new Point(0.5, 0.5);
            }

            SetContents();
            //_gestureService = GestureService.GetGestureListener(this);
            EnableContentScaling();
            this.ManipulationStarted += new EventHandler<ManipulationStartedEventArgs>(PageFlipper_ManipulationStarted);
            //_gestureService.DragDelta += new EventHandler<DragDeltaGestureEventArgs>(DragDeltaHandler);
            //_gestureService.DragCompleted += new EventHandler<DragCompletedGestureEventArgs>(DragCompletedHandler);
            //_gestureService.DragStarted += new EventHandler<DragStartedGestureEventArgs>(DragStartedHandler);

            base.OnApplyTemplate();
        }

        void PageFlipper_ManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            if (this.ActualWidth == 0 && (this.Collection == null || this.Collection.Count == 0))
            {
                e.Complete();
            }
        }

        private void EnableContentScaling()
        {
            //if (_gestureService != null && EnableScaling)
            //{
            //    if (EnableTapScaling)
            //    {
            //        _gestureService.Tap += new EventHandler<Microsoft.Phone.Controls.GestureEventArgs>(_gestureService_Tap);
            //        _gestureService.DoubleTap += new EventHandler<Microsoft.Phone.Controls.GestureEventArgs>(DoubleTapHandler);
            //    }
            //    _gestureService.PinchStarted += new EventHandler<PinchStartedGestureEventArgs>(PinchStartedHandler);
            //    _gestureService.PinchDelta += new EventHandler<PinchGestureEventArgs>(PinchDeltaHandler);
            //    _gestureService.PinchCompleted += new EventHandler<PinchGestureEventArgs>(PinchCompletedHandler);
            //}
        }

        void _gestureService_Tap(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            if (!_isTranslating && !IsScaled && !animationStarted && EnableTapScaling)
            {
                StartScaling(new Point(0.5, 0.5));
            }
        }

        private void DissableContentScaling()
        {
            //if (_gestureService != null && !EnableScaling)
            //{
            //    _gestureService.DoubleTap -= new EventHandler<Microsoft.Phone.Controls.GestureEventArgs>(DoubleTapHandler);
            //    _gestureService.PinchStarted -= new EventHandler<PinchStartedGestureEventArgs>(PinchStartedHandler);
            //    _gestureService.PinchDelta -= new EventHandler<PinchGestureEventArgs>(PinchDeltaHandler);
            //    _gestureService.PinchCompleted -= new EventHandler<PinchGestureEventArgs>(PinchCompletedHandler);
            //}
        }

        void PageFlipper_Loaded(object sender, RoutedEventArgs e)
        {
            _isLoaded = true;
            SetContents();
            this.SizeChanged += new SizeChangedEventHandler(PageFlipper_SizeChanged);
            if (PART_ScrollViewer != null && PART_ScaleContent != null)
            {
                PART_ScaleContent.Height = PART_ScrollViewer.ActualHeight;
                PART_ScaleContent.Width = PART_ScrollViewer.ActualWidth;
            }

            this.Unloaded += new RoutedEventHandler(PageFlipper_Unloaded);
        }

        void PageFlipper_Unloaded(object sender, RoutedEventArgs e)
        {
            this.Unloaded -= new RoutedEventHandler(PageFlipper_Unloaded);
            _isLoaded = false;
            ResetContent();
        }

        void PageFlipper_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _animateTransition = false;
            if (_selectedContentControl == null)
            {
                SetContents();
            }
            else
            {
                ResetTranslateValue();
            }
            if (IsScaled)
            {
                ResetScalingAnimation(new Duration(TimeSpan.FromMilliseconds(300)), 1, false);
            }
        }

        void DragStartedHandler(object sender, DragStartedGestureEventArgs e)
        {
            _dragHorizontal = 0;
            _isTranslating = true;
            UpdateTopMaxTranslateY();
            UpdateBottomMaxTranslateY();
            UpdateRightMaxTranslateX();
            UpdateLeftMaxTranslateX();
        }

        void DragCompletedHandler(object sender, DragCompletedGestureEventArgs e)
        {
            _isTranslating = false;
            if (animationStarted)
            {
                return;
            }
            if (_selectedScaleTransform != null && IsScaled)
            {
                //double newTranslateX = 0;
                //double newTranslateY = 0;
                //if (_selectedTranslateTransform.X < -leftMaxTranslateX)
                //{
                //    newTranslateX = -leftMaxTranslateX;
                //}
                //else if (_selectedTranslateTransform.X > rightMaxTranslateX)
                //{
                //    newTranslateX = rightMaxTranslateX;
                //}
                //else
                //{
                //    newTranslateX = _selectedTranslateTransform.X;
                //}

                //if (_selectedTranslateTransform.Y < -topMaxTranslateY)
                //{
                //    newTranslateY = -topMaxTranslateY;
                //}
                //else if (_selectedTranslateTransform.Y > bottomMaxTranslateY)
                //{
                //    newTranslateY = bottomMaxTranslateY;
                //}
                //else
                //{
                //    newTranslateY = _selectedTranslateTransform.Y;
                //}
                ResetScalingAnimation(new Duration(TimeSpan.FromMilliseconds(300)), _selectedScaleTransform.ScaleX, false);
                //double toX;
                //double toY;

                //double timeX;
                //double timeY;

                //inertiaStoryboard = new Storyboard();
                //DoubleAnimation animationX = new DoubleAnimation();
                //Storyboard.SetTarget(animationX, _selectedTranslateTransform);
                //Storyboard.SetTargetProperty(animationX, new PropertyPath(TranslateTransform.XProperty));
                //animationX.To = toX;
                //animationX.Duration = new Duration(TimeSpan.FromSeconds(timeX));
                //inertiaStoryboard.Children.Add(animationX);

                //DoubleAnimation animationY = new DoubleAnimation();
                //Storyboard.SetTarget(animationY, _selectedTranslateTransform);
                //Storyboard.SetTargetProperty(animationY, new PropertyPath(TranslateTransform.YProperty));
                //animationY.To = toY;
                //animationY.Duration = new Duration(TimeSpan.FromSeconds(timeY));
                //inertiaStoryboard.Children.Add(animationY);

                //inertiaStoryboard.Begin();
            }
            else if (e.Direction == System.Windows.Controls.Orientation.Horizontal)
            {
                if (_dragHorizontal >= _swipeThreshold || e.HorizontalVelocity > ActualWidth)
                {
                    int index = GetPreviousIndex(SelectedIndex);
                    if (EnableRepeating || index < SelectedIndex)
                    {
                        SelectedIndex = index;
                    }
                    else
                    {
                        ResetTranslateValue();
                    }
                }
                else if (_dragHorizontal <= -_swipeThreshold || e.HorizontalVelocity < -ActualWidth)
                {
                    int index = GetNextIndex(SelectedIndex);
                    if (EnableRepeating || index > SelectedIndex)
                    {
                        SelectedIndex = GetNextIndex(SelectedIndex);
                    }
                    else
                    {
                        ResetTranslateValue();
                    }
                }
                else
                {
                    ResetTranslateValue();
                }
                e.Handled = true;
            }
        }

        void DragDeltaHandler(object sender, DragDeltaGestureEventArgs e)
        {
            if (animationStarted)
            {
                return;
            }
            if (_selectedScaleTransform != null && IsScaled)
            {
                double extraMargin = 100;
                if (!(_selectedTranslateTransform.X + e.HorizontalChange < -_leftMaxTranslateX - extraMargin)
                    && !(_selectedTranslateTransform.X + e.HorizontalChange > _rightMaxTranslateX + extraMargin))
                {
                    _selectedTranslateTransform.X += e.HorizontalChange;
                }

                if (!(_selectedTranslateTransform.Y + e.VerticalChange < -_topMaxTranslateY - extraMargin)
                 && !(_selectedTranslateTransform.Y + e.VerticalChange > _bottomMaxTranslateY + extraMargin))
                {
                    _selectedTranslateTransform.Y += e.VerticalChange;
                }

            }
            else if (e.Direction == System.Windows.Controls.Orientation.Horizontal)
            {
                _dragHorizontal += e.HorizontalChange;
                double newChange = e.HorizontalChange;
                if ((Math.Abs(_dragHorizontal) > this.ActualWidth / 4
                        && (SelectedIndex == Collection.Count - 1 || SelectedIndex == 0))
                    || EnableRepeating)
                {
                    newChange /= 4;
                }
                if (ShowNextItemPreview)
                {
                    _translateTransform1.X += newChange;
                    _translateTransform2.X += newChange;
                    _translateTransform3.X += newChange;
                }
                else
                {
                    _selectedTranslateTransform.X += e.HorizontalChange;
                }
            }

        }

        void PinchStartedHandler(object sender, PinchStartedGestureEventArgs e)
        {
            if (_isTranslating)
            {
                e.Handled = true;
                return;
            }
            Point point0 = e.GetPosition(this, 0);
            Point point1 = e.GetPosition(this, 1);
            Point midpoint = new Point((point0.X + point1.X) / 2, (point0.Y + point1.Y) / 2);
            midpoint = StartScaling(new Point(midpoint.X / this.ActualWidth, midpoint.Y / this.ActualHeight));
        }

        private Point StartScaling(Point transformOrigin)
        {
            PART_ScaleContent.RenderTransformOrigin = transformOrigin;
            initialScale = _selectedScaleTransform.ScaleX;
            Storyboard sb = new Storyboard();

            DoubleAnimation opacityValue = new DoubleAnimation();
            Storyboard.SetTarget(opacityValue, PART_ScaleContent);
            Storyboard.SetTargetProperty(opacityValue, new PropertyPath(FrameworkElement.OpacityProperty));
            opacityValue.To = 1;
            opacityValue.Duration = new Duration(TimeSpan.FromMilliseconds(100));
            sb.Children.Add(opacityValue);

            DoubleAnimation opacityValue2 = new DoubleAnimation();
            Storyboard.SetTarget(opacityValue2, _selectedContentControl);
            Storyboard.SetTargetProperty(opacityValue2, new PropertyPath(FrameworkElement.OpacityProperty));
            opacityValue2.To = 0;
            opacityValue2.Duration = new Duration(TimeSpan.FromMilliseconds(100));
            sb.Children.Add(opacityValue2);

            sb.Begin();
            IsScaled = true;
            return transformOrigin;
        }

        void PinchDeltaHandler(object sender, PinchGestureEventArgs e)
        {
            if (initialScale * e.DistanceRatio > 1.0 && initialScale * e.DistanceRatio <= _maxScale)
            {
                _selectedScaleTransform.ScaleX = _selectedScaleTransform.ScaleY = initialScale * e.DistanceRatio;
            }
            else if (initialScale * e.DistanceRatio > 0.5 && initialScale * e.DistanceRatio <= _maxScale + 1)
            {
                double diff = initialScale * e.DistanceRatio - _selectedScaleTransform.ScaleX;
                _selectedScaleTransform.ScaleX = _selectedScaleTransform.ScaleY = _selectedScaleTransform.ScaleX + diff / 10;
            }
        }

        void PinchCompletedHandler(object sender, PinchGestureEventArgs e)
        {
            if (_selectedScaleTransform.ScaleX < 1.05 && !EnableTapScaling)
            {
                ResetScalingAnimation(new Duration(TimeSpan.FromMilliseconds(100)));
            }
            else if (_selectedScaleTransform.ScaleX >= _maxScale)
            {
                ResetScalingAnimation(new Duration(TimeSpan.FromMilliseconds(300)), _maxScale, false);
            }
            else if (_selectedScaleTransform.ScaleX <= 1)
            {
                ResetScalingAnimation(new Duration(TimeSpan.FromMilliseconds(300)), 1, false);
            }
            else
            {
                ResetScalingAnimation(new Duration(TimeSpan.FromMilliseconds(300)), _selectedScaleTransform.ScaleX, false);
            }
        }

        void DoubleTapHandler(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            if (EnableScaling && EnableTapScaling)
            {
                ResetScalingAnimation(new Duration(TimeSpan.FromMilliseconds(300)));
            }
        }

        public void ResetScalingAnimation(Duration duration, double scaleValue = 1, bool hideScaleTemplate = true)
        {
            Storyboard sb = new Storyboard();

            double newTranslateX = 0;
            double newTranslateY = 0;

            UpdateLeftMaxTranslateX();
            UpdateRightMaxTranslateX();
            if (!hideScaleTemplate)
            {
                if (_selectedScaleTransform.ScaleX <= 1.05)
                {
                    newTranslateX = 0;
                }
                else if (_selectedTranslateTransform.X <= -_leftMaxTranslateX)
                {
                    newTranslateX = -_leftMaxTranslateX;
                }
                else if (_selectedTranslateTransform.X >= _rightMaxTranslateX)
                {
                    newTranslateX = _rightMaxTranslateX;
                }
                else
                {
                    newTranslateX = _selectedTranslateTransform.X;
                }

                UpdateTopMaxTranslateY();
                UpdateBottomMaxTranslateY();
                if (_selectedScaleTransform.ScaleY <= 1.05)
                {
                    newTranslateY = 0;
                }
                else if (_selectedTranslateTransform.Y <= -_topMaxTranslateY)
                {
                    newTranslateY = -_topMaxTranslateY;
                }
                else if (_selectedTranslateTransform.Y >= _bottomMaxTranslateY)
                {
                    newTranslateY = _bottomMaxTranslateY;
                }
                else
                {
                    newTranslateY = _selectedTranslateTransform.Y;
                }

                //if (_selectedScaleTransform.ScaleX >= _maxScale)
                {
                    duration = duration.Add(new Duration(TimeSpan.FromMilliseconds(300)));
                }
                //else if (_selectedScaleTransform.ScaleX >= _maxScale / 2)
                //{
                //    duration = duration.Add(new Duration(TimeSpan.FromMilliseconds(200)));
                //}
                //else if (_selectedScaleTransform.ScaleX >= _maxScale / 3)
                //{
                //    duration = duration.Add(new Duration(TimeSpan.FromMilliseconds(100)));
                //}
            }

            DoubleAnimation animationScaleX = new DoubleAnimation();
            Storyboard.SetTarget(animationScaleX, _selectedScaleTransform);
            Storyboard.SetTargetProperty(animationScaleX, new PropertyPath(ScaleTransform.ScaleXProperty));
            animationScaleX.To = scaleValue;
            animationScaleX.Duration = duration;
            animationScaleX.EasingFunction = GetEasingToAnimation();
            sb.Children.Add(animationScaleX);

            DoubleAnimation animationScaleY = new DoubleAnimation();
            Storyboard.SetTarget(animationScaleY, _selectedScaleTransform);
            Storyboard.SetTargetProperty(animationScaleY, new PropertyPath(ScaleTransform.ScaleYProperty));
            animationScaleY.To = scaleValue;
            animationScaleY.Duration = duration;
            animationScaleY.EasingFunction = GetEasingToAnimation();
            sb.Children.Add(animationScaleY);

            DoubleAnimation animationX = new DoubleAnimation();
            Storyboard.SetTarget(animationX, _selectedTranslateTransform);
            Storyboard.SetTargetProperty(animationX, new PropertyPath(TranslateTransform.XProperty));
            animationX.To = newTranslateX;
            animationX.Duration = duration;
            animationX.EasingFunction = GetEasingToAnimation();
            sb.Children.Add(animationX);

            DoubleAnimation animationY = new DoubleAnimation();
            Storyboard.SetTarget(animationY, _selectedTranslateTransform);
            Storyboard.SetTargetProperty(animationY, new PropertyPath(TranslateTransform.YProperty));
            animationY.To = newTranslateY;
            animationY.Duration = duration;
            animationY.EasingFunction = GetEasingToAnimation();
            sb.Children.Add(animationY);

            if (hideScaleTemplate)
            {
                DoubleAnimation opacityValue = new DoubleAnimation();
                Storyboard.SetTarget(opacityValue, PART_ScaleContent);
                Storyboard.SetTargetProperty(opacityValue, new PropertyPath(FrameworkElement.OpacityProperty));
                opacityValue.To = 0;
                opacityValue.BeginTime = duration.TimeSpan;
                opacityValue.Duration = new Duration(TimeSpan.FromMilliseconds(100));
                sb.Children.Add(opacityValue);

                DoubleAnimation opacityValue2 = new DoubleAnimation();
                Storyboard.SetTarget(opacityValue2, _selectedContentControl);
                Storyboard.SetTargetProperty(opacityValue2, new PropertyPath(FrameworkElement.OpacityProperty));
                opacityValue2.To = 1;
                opacityValue.BeginTime = duration.TimeSpan;
                opacityValue2.Duration = new Duration(TimeSpan.FromMilliseconds(100));
                sb.Children.Add(opacityValue2);

                IsScaled = false;
            }
            sb.Begin();
        }

        private IEasingFunction GetEasingToAnimation()
        {
            ExponentialEase ease = new ExponentialEase();
            ease.EasingMode = EasingMode.EaseOut;
            ease.Exponent = 5;

            return ease;
        }

        void SelectNextItem(int selectedIndex)
        {
            Storyboard sb = new Storyboard();
            ContentControl control = null;
            if (_selectedContentControl == PART_Content2)
            {
                _selectedContentControl = PART_Content3;
                //_selectedScaleTransform = _scaleTransform3;
                //_selectedTranslateTransform = _translateTransform3;
                // SetNextItem(PART_Content1, selectedIndex);
                control = PART_Content1;
                TranslateItem(_translateTransform2, -1, sb);
                TranslateItem(_translateTransform3, 0, sb);
                TranslateItem(_translateTransform1, 1, sb, false);
            }
            else if (_selectedContentControl == PART_Content3)
            {
                _selectedContentControl = PART_Content1;
                //_selectedScaleTransform = _scaleTransform1;
                //_selectedTranslateTransform = _translateTransform1;
                //SetNextItem(PART_Content2, selectedIndex);
                control = PART_Content2;
                TranslateItem(_translateTransform3, -1, sb);
                TranslateItem(_translateTransform1, 0, sb);
                TranslateItem(_translateTransform2, 1, sb, false);
            }
            else if (_selectedContentControl == PART_Content1)
            {
                _selectedContentControl = PART_Content2;
                // _selectedScaleTransform = _scaleTransform2;
                //_selectedTranslateTransform = _translateTransform2;
                //SetNextItem(PART_Content3, selectedIndex);
                control = PART_Content3;
                TranslateItem(_translateTransform1, -1, sb);
                TranslateItem(_translateTransform2, 0, sb);
                TranslateItem(_translateTransform3, 1, sb, false);
            }
            if (control != null)
            {
                sb.Begin();
                animationStarted = true;
                sb.Completed += (s, e) =>
                {
                    SetNextItem(control, selectedIndex);
                    PART_ScaleContent.Content = Collection[SelectedIndex];
                    animationStarted = false;
                    if (SelectionChanged != null)
                        SelectionChanged(null, null);

                };
            }
        }

        void SelectPrevItem(int selectedIndex)
        {
            Storyboard sb = new Storyboard();
            ContentControl control = null;
            if (_selectedContentControl == PART_Content2)
            {
                _selectedContentControl = PART_Content1;
                //_selectedScaleTransform = _scaleTransform1;
                //_selectedTranslateTransform = _translateTransform1;
                //SetPreviousItem(PART_Content3, selectedIndex);
                control = PART_Content3;
                TranslateItem(_translateTransform3, -1, sb, false);
                TranslateItem(_translateTransform1, 0, sb);
                TranslateItem(_translateTransform2, 1, sb);
            }
            else if (_selectedContentControl == PART_Content1)
            {
                _selectedContentControl = PART_Content3;
                //_selectedScaleTransform = _scaleTransform3;
                //_selectedTranslateTransform = _translateTransform3;
                //SetPreviousItem(PART_Content2, selectedIndex);
                control = PART_Content2;
                TranslateItem(_translateTransform2, -1, sb, false);
                TranslateItem(_translateTransform3, 0, sb);
                TranslateItem(_translateTransform1, 1, sb);
            }
            else if (_selectedContentControl == PART_Content3)
            {
                _selectedContentControl = PART_Content2;
                // _selectedScaleTransform = _scaleTransform2;
                // _selectedTranslateTransform = _translateTransform2;
                //SetPreviousItem(PART_Content1, selectedIndex);
                control = PART_Content1;
                TranslateItem(_translateTransform1, -1, sb, false);
                TranslateItem(_translateTransform2, 0, sb);
                TranslateItem(_translateTransform3, 1, sb);
            }

            sb.Begin();
            animationStarted = true;
            sb.Completed += (s, e) =>
            {
                SetPreviousItem(control, selectedIndex);
                animationStarted = false;
                PART_ScaleContent.Content = Collection[SelectedIndex];
                if (SelectionChanged != null)
                    SelectionChanged(null, null);

            };
        }

        void ResetTranslateValue()
        {
            Storyboard sb = new Storyboard();

            if (_selectedContentControl == PART_Content2)
            {
                TranslateItem(_translateTransform1, -1, sb);
                TranslateItem(_translateTransform2, 0, sb);
                TranslateItem(_translateTransform3, 1, sb);
            }
            else if (_selectedContentControl == PART_Content3)
            {
                TranslateItem(_translateTransform2, -1, sb);
                TranslateItem(_translateTransform3, 0, sb);
                TranslateItem(_translateTransform1, 1, sb);
            }
            else if (_selectedContentControl == PART_Content1)
            {
                TranslateItem(_translateTransform3, -1, sb);
                TranslateItem(_translateTransform1, 0, sb);
                TranslateItem(_translateTransform2, 1, sb);
            }

            sb.Begin();
            animationStarted = true;
            sb.Completed += (s, e) =>
            {
                animationStarted = false;
            };
        }

        void TranslateItem(TranslateTransform cTransform, int position, Storyboard sb, bool animate = true)
        {

            if (cTransform != null)
            {
                double to = 0;
                switch (position)
                {
                    case -1:
                        to = -this.ActualWidth - _itemMargin;
                        break;
                    case 0:
                        to = 0;
                        break;
                    case 1:
                        to = this.ActualWidth + _itemMargin;
                        break;
                }
                if (!animate || !_animateTransition)
                {
                    cTransform.X = to;
                }
                else if (sb != null)
                {
                    DoubleAnimation animation = new DoubleAnimation();
                    Storyboard.SetTarget(animation, cTransform);
                    Storyboard.SetTargetProperty(animation, new PropertyPath(TranslateTransform.XProperty));
                    animation.To = to;
                    animation.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300));
                    sb.Children.Add(animation);
                }
            }
        }

        private int GetNextIndex(int selectedIndex)
        {
            if (selectedIndex < Collection.Count - 1)
            {
                return selectedIndex + 1;
            }
            return 0;
        }

        private int GetPreviousIndex(int selectedIndex)
        {
            if (selectedIndex > 0)
            {
                return selectedIndex - 1;
            }
            return Collection.Count - 1;
        }

        private void SetNextItem(ContentControl control, int selectedIndex)
        {

            if (control != null)
            {
                int index = GetNextIndex(selectedIndex);
                if (EnableRepeating || index > selectedIndex)
                {
                    control.Opacity = 1;
                    if (index > Collection.Count - 1)
                    {
                        index = 0;
                    }
                    control.Content = Collection[index];
                }
                else
                {
                    control.Content = null;
                    control.Opacity = 0;
                }
            }
        }

        private void SetPreviousItem(ContentControl control, int selectedIndex)
        {
            if (control != null)
            {
                int index = GetPreviousIndex(selectedIndex);
                if (EnableRepeating || index < selectedIndex)
                {
                    control.Opacity = 1;
                    if (index > Collection.Count - 1)
                    {
                        index = 0;
                    }
                    control.Content = Collection[index];
                }
                else
                {
                    if (control != null)
                    {
                        control.Content = null;
                        control.Opacity = 0;
                    }

                }
            }
        }

        private void SetCurrentItem(ContentControl control, int selectedIndex)
        {
            Opacity = 1;
            if (selectedIndex > Collection.Count - 1)
            {
                selectedIndex = 0;
            }
            if (control != null)
            {
                control.Content = Collection[selectedIndex];
                control.Opacity = 1;
            }
            PART_ScaleContent.Content = Collection[selectedIndex];
        }

        private void SetContents()
        {
            if (_isLoaded && Collection != null && Collection.Count > 0 && PART_Content1 != null && PART_Content2 != null && PART_Content3 != null && this.ActualWidth > 0)
            {
                _selectedContentControl = PART_Content2;
                //_selectedScaleTransform = _scaleTransform2;
                //_selectedTranslateTransform = _translateTransform2;
                if (SelectedIndex < 0)
                {
                    SelectedIndex = 0;
                }

                SetPreviousItem(PART_Content1, SelectedIndex);
                SetCurrentItem(PART_Content2, SelectedIndex);
                SetNextItem(PART_Content3, SelectedIndex);
                ResetTranslateValue();
            }
        }

        private static void OnCollectionChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PageFlipper control = sender as PageFlipper;
            if (control != null)
            {
                control.SetContents();
            }
        }

        private static void OnSelectedIndexChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PageFlipper control = sender as PageFlipper;
            if (control != null)
            {
                if (control.SelectedIndex < 0)
                {
                    control.ResetContent();
                }
                else
                {
                    int diff = (int)e.NewValue - (int)e.OldValue;
                    if ((int)e.OldValue == -1)
                        diff = 0;
                    if (diff == -1)
                    {
                        control.SelectPrevItem((int)e.NewValue);
                    }
                    else if (diff == 1)
                    {
                        control.SelectNextItem((int)e.NewValue);
                    }
                    else
                    {
                        control.SetContents();
                    }
                    control._animateTransition = true;
                }
            }
        }

        private void ResetContent()
        {
            _animateTransition = false;
            _selectedContentControl = null;
            if (PART_Content1 != null)
            {
                PART_Content1.Content = null;
            }
            if (PART_Content2 != null)
            {
                PART_Content2.Content = null;
            }
            if (PART_Content3 != null)
            {
                PART_Content3.Content = null;
            }
        }

        private static void OnEnableScalingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PageFlipper control = sender as PageFlipper;
            if (control != null)
            {
                if (control.EnableScaling)
                {
                    control.EnableContentScaling();
                }
                else
                {
                    control.DissableContentScaling();
                }
            }
        }

        #endregion
    }
}
