﻿
namespace IdentityMine.BaseUI
{
    public interface IPageTransitionEvents
    {
        void PageTransitionOutStarting(string transitionName);
        void PageTransitionInStarting(string transitionName);
        void PageTransitionInCompleted(string transitionName);
    }
}
