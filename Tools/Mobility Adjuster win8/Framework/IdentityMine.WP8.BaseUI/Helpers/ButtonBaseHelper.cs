﻿using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace IdentityMine.BaseUI.Helpers
{
    public class BtnHelper
    {
        #region Command

        public static ICommand GetCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(CommandProperty);
        }

        public static void SetCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(CommandProperty, value);
        }

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.RegisterAttached("Command", typeof(ICommand), typeof(BtnHelper), new PropertyMetadata(null, new PropertyChangedCallback(OnCommandChanged)));

        static void OnCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ButtonBase btn = (ButtonBase)d;

            if (e.OldValue != null && e.NewValue == null)
            {
                btn.Click -= new RoutedEventHandler(btn_Click);
            }

            if (e.NewValue != null && e.OldValue == null)
            {
                btn.Click += new RoutedEventHandler(btn_Click);
            }
        }

        #endregion

        #region CommandParameter

        public static object GetCommandParameter(DependencyObject obj)
        {
            return (object)obj.GetValue(CommandParameterProperty);
        }

        public static void SetCommandParameter(DependencyObject obj, object value)
        {
            obj.SetValue(CommandParameterProperty, value);
        }

        // Using a DependencyProperty as the backing store for CommandParameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.RegisterAttached("CommandParameter", typeof(object), typeof(BtnHelper), new PropertyMetadata(null));

        #endregion

        #region IconPath
        public static string GetIconPath(DependencyObject obj)
        {
            return (string)obj.GetValue(IconPathProperty);
        }

        public static void SetIconPath(DependencyObject obj, string value)
        {
            obj.SetValue(IconPathProperty, value);
        }

        // Using a DependencyProperty as the backing store for IconPath.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconPathProperty =
            DependencyProperty.RegisterAttached("IconPath", typeof(string), typeof(BtnHelper), new PropertyMetadata(string.Empty));
        #endregion

        static void btn_Click(object sender, RoutedEventArgs e)
        {
            ButtonBase btn = (ButtonBase)sender;
            ICommand cmd = GetCommand((ButtonBase)sender);

            if (cmd != null)
            {
                cmd.Execute(GetCommandParameter(btn));
            }
        }
    }
}
