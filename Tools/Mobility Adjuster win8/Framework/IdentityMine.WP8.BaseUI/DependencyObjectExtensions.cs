﻿using System.Windows;
using System.Windows.Media;

namespace IdentityMine.BaseUI
{
    public static class DependencyObjectExtensions
    {
        public static T GetParent<T>(this DependencyObject item) where T : DependencyObject
        {
            if (item == null)
            {
                return null;
            }
            else if (item is T)
            {
                return (T)item;
            }
            else
            {
                DependencyObject parent = null;

                if (item is FrameworkElement)
                {
                    parent = ((FrameworkElement)item).Parent;
                }

                if (parent == null)
                {
                    parent = VisualTreeHelper.GetParent(item);
                }

                return GetParent<T>(parent);
            }
        }

        public static T FindChild<T>(this DependencyObject parent, string name) where T : FrameworkElement
        {
            if (parent == null)
            {
                return null;
            }
            else if (parent is T && ((FrameworkElement)parent).Name == name)
            {
                return (T)parent;
            }
            else
            {
                int count = VisualTreeHelper.GetChildrenCount(parent);

                for (int i = 0; i < count; i++)
                {
                    T item = FindChild<T>(VisualTreeHelper.GetChild(parent, i), name);

                    if (item != null)
                    {
                        return item;
                    }
                }

                return null;
            }
        }

        public static T FindChild<T>(this DependencyObject parent) where T : FrameworkElement
        {
            if (parent == null)
            {
                return null;
            }
            else if (parent is T)
            {
                return (T)parent;
            }
            else
            {
                int count = VisualTreeHelper.GetChildrenCount(parent);

                for (int i = 0; i < count; i++)
                {
                    T item = FindChild<T>(VisualTreeHelper.GetChild(parent, i));

                    if (item != null)
                    {
                        return item;
                    }
                }

                return null;
            }
        }

        public static T FindChildWithPropertyValue<T>(this DependencyObject parent, DependencyProperty property, object value) where T : FrameworkElement
        {
            if (parent == null)
            {
                return null;
            }
            else if (parent is T && parent.GetValue(property).Equals(value))
            {
                return (T)parent;
            }
            else
            {
                int count = VisualTreeHelper.GetChildrenCount(parent);

                for (int i = 0; i < count; i++)
                {
                    T item = FindChildWithPropertyValue<T>(VisualTreeHelper.GetChild(parent, i), property, value);

                    if (item != null)
                    {
                        return item;
                    }
                }

                return null;
            }
        }
    }
}
