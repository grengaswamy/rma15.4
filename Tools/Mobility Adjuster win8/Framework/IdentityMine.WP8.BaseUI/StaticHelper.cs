﻿using System.Collections.Generic;

namespace IdentityMine.BaseUI
{
    public static class Helper
    {

        public static void AddDictionaryValue(this IDictionary<string, object> dictionary, string key, object value)
        {
            if (dictionary == null) return;

            if (dictionary.ContainsKey(key))
                dictionary[key] = value;
            else
                dictionary.Add(key, value);
        }
    }
}
