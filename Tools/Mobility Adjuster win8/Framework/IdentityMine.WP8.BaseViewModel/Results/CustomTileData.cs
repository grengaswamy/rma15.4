﻿using IdentityMine.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityMine.ViewModel
{
#if WP7
    public class CustomTileData : CustomStandardTileData
#else
    public class CustomTileData : CustomFlipTileData
#endif
    {
    }
}
