
namespace IdentityMine.ViewModel
{
    public interface IResults
    {
        int Count { get;}

        string GroupName { get; set; }

        void LoadIsoImages(int startIndex, int count);

        void UnLoadImages();
    }
}
