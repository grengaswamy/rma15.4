﻿using System.Collections.Generic;
using System.Linq;

namespace IdentityMine.ViewModel
{
    public class StringResourceManager
    {
        public StringResourceManager(string [] languages)
        {
            Resources = new List<LocalizedResource>();

            foreach (string lang in languages)
            {
                Resources.Add(new LocalizedResource(lang));
            }
        }

        public List<LocalizedResource> Resources { get; private set; }

        public LocalizedResource SelectedResource { get; set; }

        public string this[string key]
        {
            get
            {
                if(SelectedResource.Sessions.ContainsKey(key))
                    return SelectedResource[key];
                return null;
            }
        }

        public void SetResource(string lang)
        {
            SelectedResource = Resources.FirstOrDefault(s=>s.Lang == lang);
            SelectedResource.Sessions.Clear();
        }

        public void Add(string key, string value)
        {
            SelectedResource[key] = value;
        }
    }

}
