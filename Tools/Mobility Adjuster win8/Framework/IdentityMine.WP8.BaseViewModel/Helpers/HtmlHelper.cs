﻿using System.Net;
using System.Text.RegularExpressions;

namespace IdentityMine.BaseViewModel.Helpers
{
   public static class HtmlHelper
   {
      private static readonly Regex TagRegex = new Regex("<.+?>", RegexOptions.Compiled);

      public static string StripHtml(string html)
      {
         // Decode HTML string 
         html = HttpUtility.HtmlDecode(html);

         // There's a bug in HtmlDecode which does not replace &apos; so we do it here
         html = html.Replace("&apos;", "'");

         // Finally remove all HTML tags
         html = TagRegex.Replace(html, string.Empty);

         return html;
      }
   }
}
