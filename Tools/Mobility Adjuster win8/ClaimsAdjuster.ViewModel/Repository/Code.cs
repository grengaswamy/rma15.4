﻿using SQLite;

namespace ClaimsAdjuster.ViewModel.Repository
{
    public class Code
    {
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        public string ShortCode { get; set; }
        public string Description { get; set; }
        public string CodeId { get; set; }
        public int UserId { get; set; }
        public CodeType CodeType { get; set; }
    }

    public enum CodeType
    {
        None,
        STATES,
        /// <summary>
        /// Type of Note
        /// </summary>
        NOTE_TYPE_CODE,
        /// <summary>
        /// Type of Task
        /// </summary>
        WPA_ACTIVITIES,
        /// <summary>
        /// Type of parties involved
        /// </summary>
        PERSON_INV_TYPE,
        /// <summary>
        /// TVarious status of claim
        /// </summary>
        CLAIM_STATUS,

        OTHER_PEOPLE
    }
}
