﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using System.Xml.Serialization;
using IdentityMine.ViewModel;
using System.ComponentModel.DataAnnotations;

namespace ClaimsAdjuster.ViewModel.Repository
{
    [Table("User")]
    public class User : ViewModelBase
    {
        #region Fields

        private string userName;
        private string dsnName;
        private string webserverUrl;
        private string password;

        #endregion

        #region Properties
        
        [XmlIgnore]
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
                RaisePropertyChanged("UserName");
            }
        }

        [Required(AllowEmptyStrings = false)]
        public string Password 
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                RaisePropertyChanged("Password");
            }
        }

        [Required(AllowEmptyStrings = false)]
        [XmlIgnore]
        public string WebServerUrl 
        {
            get
            {
                return webserverUrl;
            }
            set
            {
                webserverUrl = value;
                RaisePropertyChanged("WebServerUrl");
            }
        }

        [Required(AllowEmptyStrings = false)]
        public string DSNName 
        {
            get
            {
                return dsnName;
            }
            set
            {
                dsnName = value;
                RaisePropertyChanged("DSNName");
            }
        }

        [XmlIgnore]
        public bool IsHttps { get; set; }

        #endregion

        public override string ToString()
        {
            return string.Format("{0}, {1}", UserName, Password);
        }
    }
}
