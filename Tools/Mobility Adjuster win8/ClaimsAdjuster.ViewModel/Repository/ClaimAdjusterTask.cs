﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using IdentityMine.ViewModel;
using System.ComponentModel.DataAnnotations;
using ClaimsAdjuster.ViewModel.Validation;

namespace ClaimsAdjuster.ViewModel.Repository
{
    [Table("Task")]
    public class ClaimAdjusterTask : ViewModelBase
    {

        #region Fields

        private string taskName;
        private string taskType;
        private Code selectedTaskCodeType;
        private string claimNumber;
        private DateTimeOffset taskDateTimeOffset;
        private TimeSpan taskTimeSpan;
        private string eventNumber;

        #endregion

        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        public int ServerEntryId { get; set; }
        public string TaskType { get; set; }
        public int EventId { get; set; }

        [RequiredIf("TaskParentType", ParentType.Claim, "Claim number cannot be empty")]
        public string ClaimNumber
        {
            get
            {
                return claimNumber;
            }
            set
            {
                claimNumber = value;
                RaisePropertyChanged("ClaimNumber");
            }
        }

        public string Location { get; set; }
        public string Address { get; set; }
        public DateTime TaskDateTime { get; set; }

        [Ignore]
        public DateTimeOffset SelectedDateTimeOffset
        {
            get
            {
                if (this.taskDateTimeOffset.Date == DateTime.MinValue)
                    taskDateTimeOffset = DateTime.Now;
                return taskDateTimeOffset;
            }
            set
            {
                taskDateTimeOffset = value;
            }
        }

        [Ignore]
        public TimeSpan SelectedTimeSpan
        {
            get
            {
                if (taskTimeSpan == new TimeSpan(0))
                    taskTimeSpan = DateTime.Now.TimeOfDay;
                return taskTimeSpan;
            }
            set
            {
                taskTimeSpan = value;
            }
        }

        public string TaskDescription { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string TaskName
        {
            get
            {
                return taskName;
            }
            set
            {
                taskName = value;
                RaisePropertyChanged("TaskName");
            }
        }
        public int Status { get; set; }
        public int UserId { get; set; }
        public DateTime LastSyncDate { get; set; }
        public string TaskTypeId { get; set; }
        public ParentType TaskParentType { get; set; }
		 public int ParentViewId { get; set; } //mbahl3 Mits 36145

        [RequiredIf("TaskParentType", ParentType.Event, "Event number cannot be empty")]
        public string EventNumber
        {
            get
            {
                return eventNumber;
            }
            set
            {
                eventNumber = value;
                RaisePropertyChanged("EventNumber");
            }
        }

        [Required(AllowEmptyStrings = false)]
        [Ignore]
        public Code SelectedTaskCodeType
        {
            get
            {
                return selectedTaskCodeType;
            }
            set
            {
                selectedTaskCodeType = value;
                RaisePropertyChanged("SelectedTaskCodeType");
            }
        }


        /// <summary>
        /// Gets the date time for display purpose
        /// </summary>
        [Ignore]
        public string DateTimeText
        {
            get
            {
                return TaskDateTime.ToString("hh:mm tt - MM/dd/yyyy");
            }
        }

        public string TaskTypeDisplayText
        {
            get
            {
                return TaskType.Substring(TaskType.IndexOf(" ") + 1);
            }
        }
        public override string ToString()
        {
            return string.Format("{0}", TaskDescription);
        }

        public bool HasVaidTaskDateTime
        {
            get
            {
                return TaskDateTime != DateTime.MinValue;
            }
        }

        [Ignore]
        public string Title
        {
            get
            {
                if (TaskParentType == ParentType.Event)
                    return string.Format("{0}{1}", MainViewModel.Instance.GetStringResource("EventHashText"), EventNumber);
                else
                    return string.Format("{0}{1}", MainViewModel.Instance.GetStringResource("ClaimHashText"), ClaimNumber);
            }
        }
    }
}