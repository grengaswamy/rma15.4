﻿using SQLite;
using System;
using System.Text;

namespace ClaimsAdjuster.ViewModel.Repository
{
    [Table("PartyInvolved")]
    public class PartyInvolved
    {
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        public string ClaimNumber { get; set; }
        public int ServerEntityID { get; set; }
        public string EntityType { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County { get; set; }
        public string ZipCode { get; set; }
        public string EmailAddress { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhone { get; set; }
        public int Status { get; set; }
        public int UserId { get; set; }

        public string LossDescription { get; set; }

        public DateTime LossDate { get; set; }

        /// <summary>
        /// Gets the full address of the claim.
        /// </summary>
        [Ignore]
        public string FullAddress
        {
            get
            {
                StringBuilder formattedAddress = new StringBuilder();
                formattedAddress.Append(this.Address1);
                if (!string.IsNullOrWhiteSpace(this.Address2))
                    formattedAddress.Append(" ").Append(this.Address2);
                if (!string.IsNullOrWhiteSpace(this.City))
                    formattedAddress.Append(", ").Append(this.City);
                if (!string.IsNullOrWhiteSpace(this.State))
                    formattedAddress.Append(", ").Append(this.State);
                if (!string.IsNullOrWhiteSpace(this.ZipCode))
                    formattedAddress.Append(", ").Append(this.ZipCode);

                return formattedAddress.ToString();
            }
        }

        public string EntityTypeFormated
        {
            get
            {
                return this.EntityType.Insert(this.EntityType.IndexOf(" "), " -") ;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}", Name, EntityType);
        }
    }
}



