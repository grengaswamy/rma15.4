﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using IdentityMine.ViewModel;

namespace ClaimsAdjuster.ViewModel.Repository
{
    [Table("Claim")]
    public class Claim : ViewModelBase
    {
        private string eventAddress;
        private string secondaryTitle;

        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        public int EventId { get; set; }
        public string EventNumber { get; set; }

        public string EventAddress
        {
            get
            {
                return eventAddress;
            }
            set
            {
                eventAddress = value;
                RaisePropertyChanged("EventAddress");
            }
        }

        public string EventLocation { get; set; }

        public string ClaimNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyName { get; set; }
        public string PolicyDescriptioin { get; set; }
        public string ClaimStatus { get; set; }
        public int ClaimantServerEntityID { get; set; }
        public string ClaimantFirstName { get; set; }
        public string ClaimantLasttName { get; set; }
        public string ClaimantAddress1 { get; set; }
        public string ClaimantAddress2 { get; set; }
        public string ClaimantCity { get; set; }
        public string ClaimantState { get; set; }
        public string ClaimantCounty { get; set; }
        public string ClaimantZipCode { get; set; }
        public string ClaimantEmailAddress { get; set; }
        public string ClaimantHomePhone { get; set; }
        public string ClaimantOfficePhone { get; set; }
        public double NetTotal { get; set; }
        public double TotalCollection { get; set; }
        public double TotalPayments { get; set; }
        public double TotalVoids { get; set; }
        public double PaymentsLessVoids { get; set; }
        public double TotalPartiesInvolved { get; set; }
        public int Status { get; set; }
        public int UserId { get; set; }
        public DateTime LastSyncDate { get; set; }
        public string LossDescription { get; set; }
        public bool IsDetailDownloaded { get; set; }
        public DateTime LossDate { get; set; }
        public DateTime ClaimDate { get; set; }
        /// <summary>
        /// Gets the full address of the claim.
        /// </summary>
        [Ignore]
        public string FullAddress
        {
            get
            {
                StringBuilder formattedAddress = new StringBuilder();
                formattedAddress.Append(this.ClaimantAddress1);
                if (!string.IsNullOrWhiteSpace(this.ClaimantAddress2))
                    formattedAddress.Append(" ").Append(this.ClaimantAddress2);
                if (!string.IsNullOrWhiteSpace(this.ClaimantCity))
                    formattedAddress.Append(", ").Append(this.ClaimantCity);
                if (!string.IsNullOrWhiteSpace(this.ClaimantState))
                    formattedAddress.Append(", ").Append(this.ClaimantState);
                if (!string.IsNullOrWhiteSpace(this.ClaimantZipCode))
                    formattedAddress.Append(", ").Append(this.ClaimantZipCode);

                //mbahl3 chaange made to display claimant phone number on screen 
                if (!string.IsNullOrWhiteSpace(this.ClaimantOfficePhone))
                {
                    formattedAddress.Append("\n").Append(this.ClaimantOfficePhone);
               
                }
                else if (!string.IsNullOrWhiteSpace(this.ClaimantHomePhone))
                    formattedAddress.Append("\n").Append(this.ClaimantHomePhone);

                //mbahl3
                return formattedAddress.ToString();
            }
        }
        [Ignore]
        public string LossDateFormated
        {
            get
            {
                return this.LossDate.ToString("MM/dd/yyyy");
            }
        }

        /// <summary>
        /// Gets whether the claim has any parties involved
        /// </summary>
        [Ignore]
        public bool HasPartiesInvolved
        {
            get
            {
                return this.TotalPartiesInvolved > 0;
            }
        }

        public bool HasPayment
        {
            get;
            set;
        }
        public override string ToString()
        {
            return string.Format("{0}, {1}", PolicyNumber, ClaimNumber);
        }

        public bool HasValidLossDate
        {
            get
            {
                return LossDate != DateTime.MinValue;
            }
        }

        [Ignore]
        public string SecondaryTitle
        {
            get
            {
                if (string.IsNullOrWhiteSpace(secondaryTitle))
                {
                    if (!string.IsNullOrWhiteSpace(EventLocation))
                        secondaryTitle = string.Format("{0} {1}, {2}, {3} {4}", MainViewModel.Instance.GetStringResource("EventHashText"), EventNumber, EventLocation, MainViewModel.Instance.GetStringResource("LossText"), LossDateFormated);
                    else
                        secondaryTitle = string.Format("{0} {1}, {2} {3}", MainViewModel.Instance.GetStringResource("EventHashText"), EventNumber, MainViewModel.Instance.GetStringResource("LossText"), LossDateFormated);
                }
                return secondaryTitle;
            }
        }

        [Ignore]
        public string CreatedDateText
        {
            get
            {
                return string.Format(MainViewModel.Instance.GetStringResource("CreatedText"), ClaimDate.ToString("MM/dd/yyyy"));
            }
        }

        [Ignore]
        public bool HasPolicyInfo
        {
            get
            {
                return !string.IsNullOrWhiteSpace(PolicyName) || !(string.IsNullOrWhiteSpace(ClaimantFirstName) && string.IsNullOrWhiteSpace(ClaimantLasttName));
            }
        }

    }
}

