﻿using IdentityMine.ViewModel;
using System;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class ServiceRequestConcreteFactory : ServiceRequestFactory
    {
        protected override async Task<IServiceRequest> GetServiceByClassName(StateInfo sessionString)
        {
            //Instantiate object of class with Name=className
            Type serviceType = Type.GetType(sessionString["Type"]);
            IServiceRequest sr = GetService(sessionString.Id);
            if (sr == null)
            {
                sr = (IServiceRequest)Activator.CreateInstance(serviceType);
            }
            await sr.ReadFromState(sessionString);
            return sr;
        }
    }
}

