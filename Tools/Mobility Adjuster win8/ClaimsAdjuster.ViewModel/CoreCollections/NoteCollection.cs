﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityMine.ViewModel;
using ClaimsAdjuster.ViewModel.Repository;
using System.Collections.ObjectModel;
using Windows.Foundation;

namespace ClaimsAdjuster.ViewModel
{
    public class NoteCollection : ResultItems<NoteViewModel>
    {
        #region Feilds
        
        private string selectedSortingField;
        private ObservableCollection<string> sortingFieldList;
       
        #endregion

        #region Propertes

        public ObservableCollection<string> SortingFieldList
        {
            get
            {
                if (sortingFieldList == null)
                {
                    sortingFieldList = new ObservableCollection<string>()
                    {   
                        MainViewModel.Instance.GetStringResource("NoteTypeText"),
                        MainViewModel.Instance.GetStringResource("DateText"),
                    };
                }
                return sortingFieldList;

            }
        }

        public string SelectedSortingField
        {
            get
            {
                return selectedSortingField;
            }
            set
            {
                if (value == null)
                    return;
                selectedSortingField = value;
                RefreshData();
            }
        }

        #endregion

        #region Constructors

        public NoteCollection()
        {

        }

        #endregion

        #region Methods

        public void InitializeVaules(List<Notes> notes, int claimViewid = 0) // mbahl3 Mits 36145 
        {
            selectedSortingField = MainViewModel.Instance.GetStringResource("NoteTypeText");
            LoadNotes(notes, claimViewid); // mbahl3 Mits 36145 
        }
        public async void LoadNotes(List<Notes> notes, int claimViewid = 0)  //mbahl3 Mits 36145
        {
            await MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {

                Collection.Clear();
                foreach (var item in notes)
                {
                    NoteViewModel cscNote = new NoteViewModel();
                    cscNote.CurrentNote = item;
                    cscNote.ClaimViewId = claimViewid; //mbahl3 Mits 36145
                    Collection.Add(cscNote);
                }

                RaisePropertyChanged("Count");
                RaisePropertyChanged("SelectedSortingField");
            });
        }
        public void RefreshData()
        {
            if (selectedSortingField == null)
                return;
            List<Notes> sortedNotes = null;
            IAsyncAction asyncAction = MainVMBase.InstanceBase.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {

                if (selectedSortingField == MainViewModel.Instance.GetStringResource("NoteTypeText"))
                    sortedNotes = Collection.OrderBy(note => note.CurrentNote.Heading).Select(noteVM => noteVM.CurrentNote).ToList();
                else if (selectedSortingField == MainViewModel.Instance.GetStringResource("DateText"))
                    sortedNotes = Collection.OrderBy(note => note.CurrentNote.DateCreated).Select(noteVM => noteVM.CurrentNote).ToList(); ;
                if (sortedNotes!=null)
                    LoadNotes(sortedNotes);
            });
        }

        #endregion
    }
}
