﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using System.Diagnostics;
using SQLite;
using System.Diagnostics.CodeAnalysis;

namespace ClaimsAdjuster.ViewModel.Sync
{
    public enum DeleteMode
    {
        Soft = 0,
        Hard = 1
    }

    public interface ISyncFolder
    {
        /// <summary>
        ///     The name of the physical folder in Local and Remote Storage
        /// </summary>
        string FolderName { get; }

        /// <summary>
        ///     Returns all local files in the LocalStorageFolder
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<ISyncFile>> ReadAsync();

        /// <summary>
        ///     Returns a single or null file based on an Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ISyncFile> LookupAsync(int id);

        /// <summary>
        ///     Returns a single or null file based on it's file name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<ISyncFile> LookupAsync(string name);

        /// <summary>
        ///     Pushes all locally modified items in the Folder to the associated
        ///     remote Folder.
        /// </summary>
        /// <returns>
        ///     A task that completes when the push operation has finished.
        /// </returns>
        Task PushAsync();

        /// <summary>
        ///     Pulls all remotely modified items from the associated
        ///     remote Folder.
        /// </summary>
        /// <returns>
        ///     A task that completes when the pull operation has finished.
        /// </returns>
        Task PullAsync();

        /// <summary>
        ///     Removes all items from the local Folder.
        /// </summary>
        /// <returns>
        ///     A task that completes when the truncate operation has finished.
        /// </returns>
        Task TruncateAsync();

        /// <summary>
        ///     Creates a 'undefined' (not inserted) local file based on a user defined name
        ///     If the file is found then the status of the file is still marked as undefined but its original status is stored.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        Task<ISyncFile> CreateFileAsync(string fileName);

        /// <summary>
        ///     Opens a readable stream containing the specified instance of the file
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        Task<IRandomAccessStream> OpenReadAsync(ISyncFile instance);

        Task<IRandomAccessStream> OpenWriteAsync(ISyncFile instance);

        Task DeleteAsync(ISyncFile instance);

        /// <summary>
        ///     Insert a file and mark ready to sync
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        Task InsertAndCommitAsync(ISyncFile instance, IRandomAccessStream source);

        /// <summary>
        ///     Update a file and mark it ready to sync
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        Task UpdateAndCommitAsync(ISyncFile instance, IRandomAccessStream source);

        /// <summary>
        ///     Marks all files which are undefined as ready to Sync (inserted or updated)
        /// </summary>
        /// <returns></returns>
        Task FlushAsync();

        /// <summary>
        ///     Undo all work in progress files - this only marks the state of the files back to their previous state
        /// </summary>
        /// <returns></returns>
        Task CancelAsync();

        /// <summary>
        ///     Undo changes to a file - this only marks the state of the file back to it's previous state
        /// </summary>
        /// <returns></returns>
        Task UndoAsync(ISyncFile instance);

        /// <summary>
        ///     Marks the file as ready to sync (inserted or updated based on previous state)
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        Task CommitAsync(ISyncFile instance);

        /// <summary>
        ///     Occurs when the pull has started.
        /// </summary>
        event EventHandler<SyncStartedEventArgs> PullStarted;

        /// <summary>
        ///     Occurs every time pull has a progress on one file.
        /// </summary>
        event EventHandler<SyncProgressEventArgs> PullProgressChanged;

        /// <summary>
        ///     Occurs when the pull is completed.
        /// </summary>
        event EventHandler<SyncCompletedEventArgs> PullCompleted;

        /// <summary>
        ///     Occurs when the push has started.
        /// </summary>
        event EventHandler<SyncStartedEventArgs> PushStarted;

        /// <summary>
        ///     Occurs every time push has a progress on one file.
        /// </summary>
        event EventHandler<SyncProgressEventArgs> PushProgressChanged;

        /// <summary>
        ///     Occurs when the push is completed.
        /// </summary>
        event EventHandler<SyncCompletedEventArgs> PushCompleted;

        /// <summary>
        ///     Occurs when list of files to be synchronized has been built.
        /// </summary>
        event EventHandler<SyncListBuiltEventArgs> SyncListBuilt;
    }

    public interface ISyncFileStorage
    {
        Task<ISyncFile> CreateFileAsync(string name, ISyncFolder folder);
        Task<IRandomAccessStream> OpenReadAsync(ISyncFile instance);
        Task<IRandomAccessStream> OpenWriteAsync(ISyncFile instance);
        Task DeleteAsync(ISyncFile instance);
        Task InsertAndCommitAsync(ISyncFile instance, IRandomAccessStream source);
        Task UpdateAndCommitAsync(ISyncFile instance, IRandomAccessStream source);
        Task<IEnumerable<ISyncFile>> ReadAsync(ISyncFolder folder);
        Task<ISyncFile> LookupAsync(ISyncFolder folder, int id);
        Task<ISyncFile> LookupAsync(ISyncFolder folder, string name);
        Task TruncateAsync(ISyncFolder folder);
        Task CommitAsync(ISyncFile instance);
        Task FlushAsync(ISyncFolder folder);
        Task CancelAsync(ISyncFolder folder);
        Task UndoAsync(ISyncFile instance);
        Task SyncedAsync(ISyncFile instance, SyncFileStatus status);
    }

    public interface ISyncFileStateStorage
    {
        Task<ISyncFileState> CreateFileAsync();
        Task<IEnumerable<ISyncFileState>> ReadAsync(string folderName);
        Task<IEnumerable<ISyncFileState>> ReadAsync(string folderName, SyncFileStatus status);
        Task OpenWriteAsync(ISyncFileState instance);
        Task<ISyncFileState> LookupAsync(int id);
        Task<ISyncFileState> LookupAsync(string folderName, string name);
        Task InsertAsync(ISyncFileState instance);
        Task UpdateAsync(ISyncFileState instance);
        Task UpdateAsync(ISyncFileState instance, SyncFileStatus status);
        Task DeleteAsync(ISyncFileState instance, DeleteMode mode = DeleteMode.Soft);
        Task TruncateAsync(string folderName);
        Task CancelAsync(string folderName);
        Task UndoAsync(ISyncFileState instance);
    }

    public class DefaultSyncFileStorage : ISyncFileStorage
    {
        private readonly ISyncFileStateStorage _stateStorage;

        public DefaultSyncFileStorage()
            : this(new DefaultSyncFileStateStorage(string.Empty))
        {
        }

        public DefaultSyncFileStorage(ISyncFileStateStorage stateStorage)
        {
            _stateStorage = stateStorage;
        }

        public async Task<IRandomAccessStream> OpenReadAsync(ISyncFile instance)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            IStorageFolder localFolder = await GetLocalStorageFolderAsync(instance.FolderName);

            StorageFile file =
                await localFolder.CreateFileAsync(instance.FileName, CreationCollisionOption.OpenIfExists);

            // Give you the stream
            return await file.OpenReadAsync();
        }


        public async Task<IRandomAccessStream> OpenWriteAsync(ISyncFile instance)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            StorageFile file = null;

            IStorageFolder localFolder = await GetLocalStorageFolderAsync(instance.FolderName);

            file = await localFolder.CreateFileAsync(instance.FileName, CreationCollisionOption.ReplaceExisting);

            // need to mark the state of the file as undefined. This will ensure the file is not picked up in any Push routines
            ISyncFileState state = await _stateStorage.LookupAsync(instance.FolderName, instance.FileName);

            if (state == null)
            {
                state = await ConvertToSyncStateFile(instance);
                await _stateStorage.InsertAsync(state);
            }
            else
            {
                state.ApplyPropertiesFrom(instance);
                await _stateStorage.UpdateAsync(state, SyncFileStatus.Undefined);
            }

            instance.Status = SyncFileStatus.Undefined;

            // Give you the stream
            return await file.OpenAsync(FileAccessMode.ReadWrite);
        }

        public async Task DeleteAsync(ISyncFile instance)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            IStorageFolder localFolder = await GetLocalStorageFolderAsync(instance.FolderName);

            try
            {
                // need to mark the state of the file as undefined. This will ensure the file is not picked up in any Push routines
                ISyncFileState state = await _stateStorage.LookupAsync(instance.FolderName, instance.FileName);

                if (state == null)
                {
                    state = await ConvertToSyncStateFile(instance);
                }
                else
                {
                    state.ApplyPropertiesFrom(instance);
                    await _stateStorage.UpdateAsync(state, SyncFileStatus.Undefined);
                }

                // mark the file as deleted - do not remove the record physically from the database
                await _stateStorage.DeleteAsync(state);
            }
            catch (FileNotFoundException)
            {
                // local data storage is out of sync to its file state
                // ignore the exception
            }

            // Give you the actual storage file
            StorageFile file = await localFolder.GetFileAsync(instance.FileName);

            if (file != null)
                await file.DeleteAsync();

            instance.Status = SyncFileStatus.Deleted;
        }

        /// <summary>
        ///     Update a file and mark it ready to sync
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public async Task InsertAndCommitAsync(ISyncFile instance, IRandomAccessStream source)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            if (source == null)
                throw new ArgumentNullException("source");

            IStorageFolder localFolder = await GetLocalStorageFolderAsync(instance.FolderName);

            // the user has specifically asked to insert, so fail if the file actually exists
            StorageFile file =
                await localFolder.CreateFileAsync(instance.FileName, CreationCollisionOption.FailIfExists);

            // Give you the stream
            IRandomAccessStream target = await file.OpenAsync(FileAccessMode.ReadWrite);

            await RandomAccessStream.CopyAndCloseAsync(source, target);

            ISyncFileState state = await ConvertToSyncStateFile(instance);

            bool stateInsertFailed = false;

            try
            {
                await _stateStorage.InsertAsync(state);
            }
            catch (ArgumentException)
            {
                // the sync state is out of date so ignore
                stateInsertFailed = true;
            }

            if (stateInsertFailed)
                await _stateStorage.UpdateAsync(state, SyncFileStatus.Created);
        }

        /// <summary>
        ///     Marks all files which are undefined as ready to Sync (inserted or updated)
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public async Task UpdateAndCommitAsync(ISyncFile instance, IRandomAccessStream source)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            if (source == null)
                throw new ArgumentNullException("source");

            IStorageFolder localFolder = await GetLocalStorageFolderAsync(instance.FolderName);

            // the user has specifically asked to insert, so fail if the file actually exists
            StorageFile file =
                await localFolder.CreateFileAsync(instance.FileName, CreationCollisionOption.OpenIfExists);

            // Give you the stream
            IRandomAccessStream target = await file.OpenAsync(FileAccessMode.ReadWrite);

            await RandomAccessStream.CopyAndCloseAsync(source, target);

            // find the state
            ISyncFileState state = await _stateStorage.LookupAsync(instance.FolderName, instance.FileName);

            if (state == null)
                state = await ConvertToSyncStateFile(instance);
            else
            {
                state.ApplyPropertiesFrom(instance);
                state.Status = instance.Status;
            }

            bool stateUpdateFailed = false;

            try
            {
                await _stateStorage.UpdateAsync(state);
            }
            catch (FileNotFoundException)
            {
                // the sync state is out of date so ignore
                stateUpdateFailed = true;
            }

            if (stateUpdateFailed)
                await _stateStorage.InsertAsync(state);
        }


        public async Task<IEnumerable<ISyncFile>> ReadAsync(ISyncFolder folder)
        {
            if (folder == null)
                throw new ArgumentNullException("folder");

            IEnumerable<ISyncFileState> stateFiles = await _stateStorage.ReadAsync(folder.FolderName);

            var localFiles = new List<ISyncFile>();

            foreach (ISyncFileState s in stateFiles)
            {
                localFiles.Add(ConvertFromSyncStateFile(s, folder));
            }

            return localFiles;
        }

        public async Task<ISyncFile> LookupAsync(ISyncFolder folder, int id)
        {
            if (folder == null)
                throw new ArgumentNullException("folder");

            ISyncFile file = null;

            ISyncFileState state = await _stateStorage.LookupAsync(id);

            if (state != null)
                file = ConvertFromSyncStateFile(await _stateStorage.LookupAsync(id), folder);

            return file;
        }

        public async Task<ISyncFile> LookupAsync(ISyncFolder folder, string name)
        {
            if (folder == null)
                throw new ArgumentNullException("folder");

            ISyncFile file = null;

            ISyncFileState state = await _stateStorage.LookupAsync(folder.FolderName, name);

            if (state != null)
                file = ConvertFromSyncStateFile(state, folder);

            return file;
        }

        /// <summary>
        ///     Remove all files (sync and state)
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public async Task TruncateAsync(ISyncFolder folder)
        {
            if (folder == null)
                throw new ArgumentNullException("folder");

            await _stateStorage.TruncateAsync(folder.FolderName);

            IStorageFolder localFolder = await GetLocalStorageFolderAsync(folder.FolderName);

            await localFolder.DeleteAsync(StorageDeleteOption.PermanentDelete);
        }

        public async Task CommitAsync(ISyncFile instance)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            ISyncFileState state = await _stateStorage.LookupAsync(instance.FolderName, instance.FileName);

            if (state != null)
            {
                state.ApplyPropertiesFrom(instance);
                state.Status = instance.Status;
            }

            await CommitStateAsync(instance, state);
        }

        public async Task SyncedAsync(ISyncFile instance, SyncFileStatus status)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            ISyncFileState state = await _stateStorage.LookupAsync(instance.FolderName, instance.FileName);

            if (state != null)
            {
                state.ApplyPropertiesFrom(instance);

                if (status == SyncFileStatus.Deleted)
                {
                    try
                    {
                        await _stateStorage.DeleteAsync(state, DeleteMode.Hard);
                    }
                    catch (FileNotFoundException)
                    {
                        // continue as what looks like has happened is the underlying state is not matched to the sync folder
                    }
                }
                else
                {
                    await _stateStorage.UpdateAsync(state, status);
                }
            }

            instance.Status = SyncFileStatus.AsServer;
        }

        public async Task FlushAsync(ISyncFolder folder)
        {
            if (folder == null)
                throw new ArgumentNullException("folder");

            IEnumerable<ISyncFileState> results =
                await _stateStorage.ReadAsync(folder.FolderName, SyncFileStatus.Undefined);

            foreach (ISyncFileState state in results)
            {
                ISyncFile instance = ConvertFromSyncStateFile(state, folder);
                await CommitStateAsync(instance, state);
            }
        }

        /// <summary>
        ///     Creates a new file in the folder
        /// </summary>
        /// <param name="name"></param>
        /// <param name="folder"></param>
        /// <returns></returns>
        public async Task<ISyncFile> CreateFileAsync(string name, ISyncFolder folder)
        {
            if (folder == null)
                throw new ArgumentNullException("folder");

            ISyncFile file = null;

            // first of all determine if the file already exists by looking at the state database
            ISyncFileState state = await _stateStorage.LookupAsync(folder.FolderName, name);

            if (state != null)
            {
                // existing entry therefore the status should be set to updated
                if (state.Status != SyncFileStatus.Undefined)
                {
                    string message = "A file with a similar name already exists in the sync container.";

                    if (state.Status == SyncFileStatus.Deleted)
                    {
                        // a delete has happened but the push has not
                        // the user must decide if the data info should be removed 
                        // or a push should happen first
                        message = "A file has been marked as deleted but a push to the server has not happened yet.";
                    }

                    throw new Exception(message);
                }

                // new file so create new state
                file = new SyncFile(folder) { FileName = name };
                file.ApplyPropertiesFrom(state);
            }
            else
            {
                // new file so create new state
                file = new SyncFile(folder) { FileName = name };

                state = await _stateStorage.CreateFileAsync();
                state.ApplyPropertiesFrom(file);
            }

            // finally set the status of the file to 
            file.Status = SyncFileStatus.Undefined;
            await _stateStorage.OpenWriteAsync(state);

            return file;
        }

        public async Task CancelAsync(ISyncFolder folder)
        {
            if (folder == null)
                throw new ArgumentNullException("folder");

            await _stateStorage.CancelAsync(folder.FolderName);
        }

        public async Task UndoAsync(ISyncFile instance)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            ISyncFileState state = await _stateStorage.LookupAsync(instance.FolderName, instance.FileName);

            if (state.Status == SyncFileStatus.Undefined)
            {
                if (state != null)
                    await _stateStorage.UndoAsync(state);

                state = await _stateStorage.LookupAsync(instance.FolderName, instance.FileName);

                if (state != null)
                    instance.Status = state.Status;
            }
        }

        private async Task CommitStateAsync(ISyncFile instance, ISyncFileState state)
        {
            // check to see if the file exists in the sync folder.
            // if it doesn't then the file has been deleted and therefore the state file should be deleted as well

            IStorageFile file = await FetchFileAsync(instance, instance.FolderName);

            if (file == null)
            {
                // if the previous state was undefined then the user has called CreateFile and then Committed but has not
                // saved a disk to file. In this case we should assume that it is an error on the users behalf and 
                // inform them appropriately.

                if (state != null)
                {
                    // if the previous state is undefined throw error
                    if (state.PreviousStatus == SyncFileStatus.Undefined)
                        throw new SyncException(string.Format("You must save file {0} to disk before saving, or undo",
                            instance.FileName));
                    // the file no longer exists so we can assume it has been deleted
                }
                else
                {
                    // assume the file has been removed from disk intentionally
                    await _stateStorage.DeleteAsync(state);
                }
            }
            else
            {
                // the file exists so its state needs to updated to Created or Updated
                if (state == null)
                {
                    state = await ConvertToSyncStateFile(instance);
                    await _stateStorage.InsertAsync(state);
                    instance.Status = SyncFileStatus.Created;
                }
                else
                {
                    instance.Status = state.PreviousStatus == SyncFileStatus.AsServer
                        ? SyncFileStatus.Updated
                        : SyncFileStatus.Created;

                    // the file exists therefore its state needs to be set to Updated
                    await _stateStorage.UpdateAsync(state, instance.Status);
                }
            }
        }

        private async Task<IStorageFile> FetchFileAsync(ISyncFile instance, string folderName)
        {
            IStorageFolder localFolder = await GetLocalStorageFolderAsync(folderName);

            try
            {
                StorageFile file = await localFolder.GetFileAsync(instance.FileName);
                return file;
            }
            catch (FileNotFoundException)
            {
                return null;
            }
        }

        private async Task<IStorageFolder> GetLocalStorageFolderAsync(string folderName)
        {
            return
                await
                    ApplicationData.Current.LocalFolder.CreateFolderAsync(folderName,
                        CreationCollisionOption.OpenIfExists);
        }

        private async Task<ISyncFileState> ConvertToSyncStateFile(ISyncFile file)
        {
            ISyncFileState state = await _stateStorage.CreateFileAsync();

            state.ApplyPropertiesFrom(file);

            return state;
        }

        private ISyncFile ConvertFromSyncStateFile(ISyncFileState state, ISyncFolder folder)
        {
            var file = new SyncFile(folder);

            file.ApplyPropertiesFrom(state);

            return file;
        }
    }

    public class DefaultSyncFileState : ISyncFileState
    {
        public string ServerUrl { get; set; }

        public DateTime? TimeStampDb
        {
            get
            {
                if (TimeStamp.HasValue)
                    return TimeStamp.Value.DateTime;
                return null;
            }
            set
            {
                if (value != null)
                    TimeStamp = new DateTimeOffset(value.Value);
            }
        }

        public bool SyncFlag { get; set; }
        public string DeviceID { get; set; }

        [AutoIncrement]
        [PrimaryKey]
        public int Id { get; set; }

        public string FolderName { get; set; }

        [Ignore]
        public DateTimeOffset? TimeStamp { get; set; }

        public string ChangedByUser { get; set; }

        public string FileName { get; set; }
        public string ETag { get; set; }
        public int StatusAsInt { get; internal set; }
        public int PreviousStatusAsInt { get; internal set; }

        [Ignore]
        public SyncFileStatus Status
        {
            get { return (SyncFileStatus)StatusAsInt; }
            set { StatusAsInt = (int)value; }
        }

        [Ignore]
        public SyncFileStatus PreviousStatus
        {
            get { return (SyncFileStatus)PreviousStatusAsInt; }
            set { PreviousStatusAsInt = (int)value; }
        }

        public void ApplyPropertiesFrom(ISyncFile file)
        {
            
            FileName = file.FileName;
            FolderName = file.FolderName;
            TimeStamp = file.LastModified;
        }
    }

    public class DefaultSyncFileStateStorage : ISyncFileStateStorage
    {
        private const bool SQLTRACE = false;
        private const int MAX_RETRIES = 1000;

        private const string BUSY = "busy";
        private const string DATABASE_LOCKED = "database is locked";
        private static string _dbPath = string.Empty;
        private readonly TimeSpan BUSY_TIMEOUT_WAIT = new TimeSpan(0, 0, 0, 0, 1000);

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public DefaultSyncFileStateStorage(string name)
        {
            _dbPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, name);

            // Initialize the database if necessary
            lock (this)
            {
                using (var connection = new SQLiteConnection(_dbPath))
                {
                    connection.Trace = SQLTRACE;
                    // Create the tables if they don't exist
                    try
                    {
                        connection.CreateTable<DefaultSyncFileState>();
                    }
                    catch (SQLiteException)
                    {
                        // Continue if they do exist
                    }

                    connection.Close();
                }
            }
        }

        public Task<ISyncFileState> CreateFileAsync()
        {
            return Task.Factory.StartNew(() => new DefaultSyncFileState() as ISyncFileState);
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task<IEnumerable<ISyncFileState>> ReadAsync(string folderName)
        {
            return Task.Factory.StartNew(() =>
            {
                var results = new List<ISyncFileState>();

                bool retry = true;
                int retries = 0;

                while (retry)
                {
                    retry = false;

                    // Open SQL Lite Connection
                    lock (this)
                    {
                        using (var connection = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                connection.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                connection.Trace = SQLTRACE;
                                // Get all local files

                                results =
                                    connection.Table<DefaultSyncFileState>()
                                        .Where(b => b.FolderName == folderName)
                                        .OrderBy(b => b.FileName)
                                        .ToList<ISyncFileState>();
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    connection.Close();
                                    throw;
                                }
                            }

                            connection.Close();
                        }
                    }
                }

                return (IEnumerable<ISyncFileState>)results;
            });
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task<IEnumerable<ISyncFileState>> ReadAsync(string folderName, SyncFileStatus status)
        {
            return Task.Factory.StartNew(() =>
            {
                var results = new List<ISyncFileState>();
                int retries = 0;
                bool retry = true;

                while (retry)
                {
                    retry = false;

                    // Open SQL Lite Connection
                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // Get all local files

                                results =
                                    conn.Table<DefaultSyncFileState>()
                                        .Where(b => b.FolderName == folderName && b.StatusAsInt == (int)status)
                                        .OrderBy(b => b.FileName)
                                        .ToList<ISyncFileState>();
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }

                return (IEnumerable<ISyncFileState>)results;
            });
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task<ISyncFileState> LookupAsync(int id)
        {
            return Task.Factory.StartNew(() =>
            {
                ISyncFileState existingFile = null;
                bool retry = true;
                int retries = 0;

                while (retry)
                {
                    retry = false;
                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // search for file in database
                                existingFile = (conn.Table<DefaultSyncFileState>()
                                    .SingleOrDefault(b => b.Id == id));
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }

                return existingFile;
            });
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task<ISyncFileState> LookupAsync(string folderName, string name)
        {
            return Task.Factory.StartNew(() =>
            {
                bool retry = true;
                ISyncFileState existingFile = null;
                int retries = 0;

                while (retry)
                {
                    retry = false;

                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // search for file in database
                                existingFile = (conn.Table<DefaultSyncFileState>()
                                    .SingleOrDefault(b => b.FileName == name && b.FolderName == folderName));
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }

                return existingFile;
            });
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task InsertAsync(ISyncFileState instance)
        {
            Debug.WriteLine("Inserting file {0}", instance.FileName);
            if (instance == null)
                throw new ArgumentNullException("instance");

            return Task.Factory.StartNew(() =>
            {
                instance.Status = SyncFileStatus.Undefined;
                instance.PreviousStatus = SyncFileStatus.Undefined;
                bool retry = true;
                int retries = 0;

                while (retry)
                {
                    retry = false;

                    // Open SQL Lite Connection
                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // Check whether the file is already in the DB
                                DefaultSyncFileState persistedFile = (conn.Table<DefaultSyncFileState>()
                                    .SingleOrDefault(b => b.FileName == instance.FileName));

                                // If file exists
                                if (persistedFile != null)
                                {
                                    // Update it, by copying over the values
                                    throw new ArgumentException("File already exists", "instance");
                                }

                                // the local file does not exist so create it
                                conn.Insert(instance);
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }
            });
        }

        /// <summary>
        ///     If the instance does not exist then a FileNotFoundException will be thrown
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public Task UpdateAsync(ISyncFileState instance)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            return Save(instance, SyncFileStatus.Updated);
        }

        /// <summary>
        ///     If the instance does not exist then a FileNotFoundException will be thrown
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public Task UpdateAsync(ISyncFileState instance, SyncFileStatus status)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            return Save(instance, status);
        }


        /// <summary>
        ///     Marks the file as deleted allowing a push to recognize when files need to be removed from the server
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task DeleteAsync(ISyncFileState instance, DeleteMode mode = DeleteMode.Soft)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            return Task.Factory.StartNew(() =>
            {
                bool retry = true;
                int retries = 0;

                while (retry)
                {
                    retry = false;

                    // Open SQL Lite Connection
                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // Get the local file from the database
                                DefaultSyncFileState persistedFile = conn.Table<DefaultSyncFileState>()
                                    .SingleOrDefault(b => b.Id == instance.Id);

                                // If it doesn't exist throw exception
                                if (persistedFile == null)
                                    throw new FileNotFoundException();

                                persistedFile.Status = persistedFile.Status;
                                persistedFile.Status = SyncFileStatus.Deleted;

                                if (mode == DeleteMode.Soft)
                                {
                                    // Try updating it
                                    if (conn.Update(persistedFile) < 0)
                                        throw new InvalidOperationException("Soft Delete failed");
                                }
                                else
                                {
                                    // Try deleting it
                                    if (conn.Delete(persistedFile) < 0)
                                        throw new InvalidOperationException("Hard Delete failed");
                                }
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }
            });
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task TruncateAsync(string folderName)
        {
            return Task.Factory.StartNew(() =>
            {
                bool retry = true;
                int retries = 0;

                while (retry)
                {
                    retry = false;

                    // Open SQL Lite Connection
                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // Get the local file from the database
                                TableQuery<DefaultSyncFileState> persistedFiles = conn.Table<DefaultSyncFileState>()
                                    .Where(b => b.FolderName == folderName);

                                // not doing a DeleteAll as that removes all records regardless of filter
                                foreach (DefaultSyncFileState p in persistedFiles)
                                    conn.Delete(p);
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }
            });
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task CancelAsync(string folderName)
        {
            return Task.Factory.StartNew(() =>
            {
                bool retry = true;
                int retries = 0;

                while (retry)
                {
                    retry = false;

                    // Open SQL Lite Connection
                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // Get the local file from the database
                                List<ISyncFileState> persistedFiles = conn.Table<DefaultSyncFileState>()
                                    .Where(b => b.StatusAsInt == (int)SyncFileStatus.Undefined).ToList<ISyncFileState>();

                                // roll the status back 
                                foreach (ISyncFileState p in persistedFiles)
                                {
                                    if (p.PreviousStatus == SyncFileStatus.Undefined)
                                        conn.Delete(p);
                                    else
                                    {
                                        p.Status = p.PreviousStatus;
                                        conn.Update(p);
                                    }
                                }
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }
            });
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task UndoAsync(ISyncFileState instance)
        {
            return Task.Factory.StartNew(() =>
            {
                bool retry = true;

                while (retry)
                {
                    retry = false;
                    int retries = 0;

                    // Open SQL Lite Connection
                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // Get the local file from the database
                                DefaultSyncFileState persistedFile =
                                    conn.Table<DefaultSyncFileState>().SingleOrDefault(b => b.Id == instance.Id);

                                if (persistedFile != null)
                                {
                                    persistedFile.Status = persistedFile.PreviousStatus;

                                    if (persistedFile.Status == SyncFileStatus.Undefined)
                                    {
                                        if (conn.Delete(persistedFile) < 0)
                                            throw new InvalidOperationException("Undo failed");
                                    }
                                    else
                                    {
                                        if (conn.Update(persistedFile) < 0)
                                            throw new InvalidOperationException("Undo failed");
                                    }
                                }
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }
            });
        }

        /// <summary>
        ///     Simply marks the file as work in progress, i.e. marks it as undefined
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public Task OpenWriteAsync(ISyncFileState instance)
        {
            return Task.Factory.StartNew(() =>
            {
                bool retry = true;

                while (retry)
                {
                    retry = false;
                    int retries = 0;

                    // Open SQL Lite Connection
                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // Get the local file from the database
                                DefaultSyncFileState persistedFile =
                                    conn.Table<DefaultSyncFileState>().SingleOrDefault(b => b.Id == instance.Id);

                                if (persistedFile != null)
                                {
                                    persistedFile.PreviousStatus = persistedFile.Status;
                                    persistedFile.Status = SyncFileStatus.Undefined;

                                    if (conn.Update(persistedFile) < 0)
                                        throw new InvalidOperationException("Open Write failed");
                                }
                                else
                                {
                                    instance.Status = instance.PreviousStatus = SyncFileStatus.Undefined;

                                    if (conn.Insert(instance) < 0)
                                        throw new InvalidOperationException("Open Write failed");
                                }
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }
            });
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        private Task Save(ISyncFileState instance, SyncFileStatus status)
        {
            instance.PreviousStatus = (status == SyncFileStatus.Created)
                ? SyncFileStatus.Undefined
                : instance.Status;
            instance.Status = status;

            return Task.Factory.StartNew(() =>
            {
                bool retry = true;
                int retries = 0;

                while (retry)
                {
                    retry = false;

                    // Open SQL Lite Connection
                    lock (this)
                    {
                        using (var conn = new SQLiteConnection(_dbPath))
                        {
                            try
                            {
                                conn.BusyTimeout = BUSY_TIMEOUT_WAIT;
                                conn.Trace = SQLTRACE;
                                // Check whether the file is already in the DB
                                DefaultSyncFileState persistedFile = (conn.Table<DefaultSyncFileState>()
                                    .SingleOrDefault(b => b.FileName == instance.FileName));

                                // If file exists
                                if (persistedFile == null)
                                {
                                    // Update it, by copying over the values
                                    throw new FileNotFoundException(string.Format("Instance {0} does not exist",
                                        instance.FileName));
                                }
                                persistedFile.ChangedByUser = MainViewModel.Instance.CurrentUserInfo.UserName;
                                persistedFile.PreviousStatus = instance.PreviousStatus;
                                persistedFile.Status = instance.Status;
                                persistedFile.TimeStamp = instance.TimeStamp;

                                // the local file exists so update it
                                conn.Update(persistedFile);
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message.Equals(BUSY, StringComparison.CurrentCultureIgnoreCase) ||
                                    ex.Message.Equals(DATABASE_LOCKED, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (retries < MAX_RETRIES)
                                    {
                                        Task.Delay(BUSY_TIMEOUT_WAIT);
                                        retry = true;
                                    }

                                    retries++;
                                }
                                else
                                {
                                    conn.Close();
                                    throw;
                                }
                            }

                            conn.Close();
                        }
                    }
                }
            });
        }
    }
}
