﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel.Sync
{

    public class SyncStartedEventArgs : EventArgs
    {
        /// <summary>
        ///     Indicates sync started
        /// </summary>
        /// <param name="syncFolder"></param>
        public SyncStartedEventArgs(string tableName)
        {
            SyncTableName = tableName;
        }

        public string SyncTableName { get; private set; }
    }

    public class SyncCompletedEventArgs : EventArgs
    {
        /// <summary>
        ///     Fires when sync operation is completed
        /// </summary>
        /// <param name="syncFolder">syncID</param>
        public SyncCompletedEventArgs(string syncID)
        {
            SyncIdentifier = syncID;
        }
        public string SyncIdentifier { get; set; }
      
    }

    public class SyncListBuiltEventArgs : EventArgs
    {
        public SyncListBuiltEventArgs(IEnumerable<string> syncRecords)
        {
            SyncRecords = syncRecords;
        }

       
        public IEnumerable<string> SyncRecords { get; set; }
    }

    public class SyncProgressEventArgs : EventArgs
    {

           /// <summary>
        ///     Indicates Sync Progress
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="serviceXML"></param>
        /// <param name="localXML"></param>
        /// <param name="result"></param>
        /// <param name="error"></param>
        public SyncProgressEventArgs(string tableName, string serviceXML, string localXML,
            SyncFileResult result, string error = "")
        {
            TableName = tableName;
            ServiceXML = serviceXML;
            LocalXML = localXML;
            Result = result;
            Error = error;
        }

        public SyncProgressEventArgs(string tableName, string serviceXML, string localXML) :
            this(tableName, serviceXML, localXML, SyncFileResult.Success)
        {
        }

        public string TableName { get; private set; }
        public string ServiceXML { get; private set; }
        public string LocalXML { get; private set; }
        public SyncFileResult Result { get; private set; }
        public string Error { get; private set; }
    }

    public enum SyncFileResult
    {
        Success,
        Failure
    }
    
}
