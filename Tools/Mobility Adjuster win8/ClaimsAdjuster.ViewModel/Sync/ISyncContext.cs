﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClaimsAdjuster.ViewModel.Services;
using ClaimsAdjuster.ViewModel.Extensions;

namespace ClaimsAdjuster.ViewModel.Sync
{

    public interface ISyncContext
    {
       
        bool IsInitialized { get; }
        Task InitializeAsync();
        Task<string> GetSyncDataAsync(string xml);
    }


    public class SyncContext : ISyncContext
    {
        private SyncEnabledStorage _syncEnabledStorage;
        public SyncContext()
        {
           
            IsInitialized = false;
        }
        public bool IsInitialized { get; private set; }
        public Task InitializeAsync()
        {
            UIDispatcher.Initialize();
            return Task.Factory.StartNew(() =>
            {
                IsInitialized = true;
                _syncEnabledStorage = new SyncEnabledStorage();
            });
        }

        public Task<string> GetSyncDataAsync(string tableName)
        {
            return Task.Factory.StartNew<string>(() =>
            {
                EnsureInitialized();

                return string.Empty;
            });
        }


        private void EnsureInitialized()
        {
            if (!IsInitialized)
            {
                // TODO: Better exception message
                throw new InvalidOperationException(
                    "The Sync Context must be initialized before sync instances can be created.");
            }
        }

    }

    internal class SyncEnabledStorage
    {
        public SyncEnabledStorage()
        {
        }
    }
}
