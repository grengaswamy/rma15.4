﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel.Logger
{
    public sealed class MetroEventSource : EventSource
    {
        public static MetroEventSource Log = new MetroEventSource();

        [Event(1, Level = EventLevel.Informational)]
        public void Info(string message)
        {
            this.WriteEvent(1, message);
        }

        [Event(2, Level = EventLevel.Critical)]
        public void Critical(string message)
        {
            this.WriteEvent(2, message);
        }
    }
}
