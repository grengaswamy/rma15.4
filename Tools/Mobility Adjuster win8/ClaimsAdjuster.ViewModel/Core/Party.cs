﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;

namespace ClaimsAdjuster.ViewModel
{
    public class Party : ClaimsAdjuster
    {


        #region Properties

        public PartyInvolved CscPartyInvolved { get; set; }

        public override bool HasFullAddress
        {
            get
            {
                return CscPartyInvolved != null && !string.IsNullOrWhiteSpace(CscPartyInvolved.FullAddress);
            }
        }

        public override bool HasPhoneNumber
        {
            get
            {
                return CscPartyInvolved != null && (!string.IsNullOrWhiteSpace(CscPartyInvolved.HomePhone) || !string.IsNullOrWhiteSpace(CscPartyInvolved.OfficePhone));
            }
        }

        public override bool HasEmailId
        {
            get
            {
                return CscPartyInvolved != null && !string.IsNullOrWhiteSpace(CscPartyInvolved.EmailAddress);
            }
        }

        protected override void OnLaunchMapCommand()
        {
            LaunchHelper.LaunchMap(CscPartyInvolved.FullAddress);
        }
        //mbahl3 11/04/2014 changed for skype claimant number display 
        protected override void OnLaunchSkypeCommand()
        {
            if (!string.IsNullOrWhiteSpace(this.CscPartyInvolved.OfficePhone))
            {
                LaunchHelper.LaunchSkype(CscPartyInvolved.OfficePhone);

            }
            else if(!string.IsNullOrWhiteSpace(this.CscPartyInvolved.HomePhone))
            {
                LaunchHelper.LaunchSkype(CscPartyInvolved.HomePhone);
            }
            
                       
        }
        //mbahl3 11/04/2014 changed for skype claimant number display 

        protected override void OnLaunchMailCommand()
        {
            LaunchHelper.LaunchMail(CscPartyInvolved.EmailAddress);
        }


        #endregion

        #region Methods



        #endregion


    }
}
