﻿using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace ClaimsAdjuster.ViewModel
{
    public class SearchViewModel : ClaimsAdjuster
    {
        #region Fields

        private string searchTerm;
        private ClaimCollection claims;
        private TaskCollection tasks;
        private EventCollection events;
        private ICommand loadResultsCommand;
        private bool claimsButtonIsChecked;
        private bool eventsButtonIsChecked;
        private bool tasksButtonIsChecked;

        #endregion

        #region Constructors

        public SearchViewModel()
        {
            HasNoRecords = false;
        }

        #endregion

        #region Properties

        public string SearchTerm
        {
            get
            {
                return searchTerm;
            }
            set
            {
                searchTerm = value;
                RaisePropertyChanged("SearchTerm");
                RaisePropertyChanged("SearchPageHeader");
            }
        }

        public ClaimCollection Claims
        {
            get
            {
                if (claims == null)
                    claims = new ClaimCollection();
                return claims;
            }
        }

        public TaskCollection Tasks
        {
            get
            {
                if (tasks == null)
                    tasks = new TaskCollection();
                return tasks;
            }
        }

        public EventCollection Events
        {
            get
            {
                if (events == null)
                    events = new EventCollection();
                return events;
            }
        }

        public bool HasClaims
        {
            get
            {
                return Claims.Count > 0;
            }
        }

        public bool HasTasks
        {
            get
            {
                return Tasks.Count > 0;
            }
        }

        public bool HasEvents
        {
            get
            {
                return Events.Count > 0;
            }
        }

        public string SearchPageHeader
        {
            get
            {
                return string.Format(MainViewModel.Instance.GetStringResource("SearchHeaderText"), SearchTerm);
            }
        }

        public string TasksText
        {
            get
            {
                return string.Format(MainViewModel.Instance.GetStringResource("TasksCountText"), Tasks.Count);
            }
        }

        public string ClaimsText
        {
            get
            {
                return string.Format(MainViewModel.Instance.GetStringResource("ClaimsCountText"), Claims.Count);
            }
        }

        public string EventsText
        {
            get
            {
                return string.Format(MainViewModel.Instance.GetStringResource("EventsCountText"), Events.Count);
            }
        }

        public SearchType CurrentSelectedType { get; set; }

        public bool ClaimsButtonIsChecked
        {
            get
            {
                return claimsButtonIsChecked;
            }
            set
            {
                claimsButtonIsChecked = value;
                RaisePropertyChanged("ClaimsButtonIsChecked");
            }
        }

        public bool EventsButtonIsChecked
        {
            get
            {
                return eventsButtonIsChecked;
            }
            set
            {
                eventsButtonIsChecked = value;
                RaisePropertyChanged("EventsButtonIsChecked");
            }
        }

        public bool TasksButtonIsChecked
        {
            get
            {
                return tasksButtonIsChecked;
            }
            set
            {
                tasksButtonIsChecked = value;
                RaisePropertyChanged("TasksButtonIsChecked");
            }
        }

        public bool HasNoRecords { get; set; }

        #endregion

        #region Commands and Members
        public ICommand LoadResultsCommand
        {
            get
            {
                if (loadResultsCommand == null)
                    loadResultsCommand = new RelayCommand(OnLoadResultsCommand);
                return loadResultsCommand;
            }
        }

        private async void OnLoadResultsCommand()
        {
            List<Claim> claimsList = new List<Claim>();
            List<ClaimAdjusterTask> tasksList = new List<ClaimAdjusterTask>();
            List<Event> eventsList = new List<Event>();
            MainViewModel.Instance.SetAppBusy(Constants.State_SearchLoading, true);

            Claims.Collection.Clear();
            Events.Collection.Clear();
            Tasks.Collection.Clear();

            string searchTermLowerCaps = SearchTerm.ToLower();
            var claims = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();
            claimsList = claims.Where(p => (searchTermLowerCaps.Contains(p.ClaimNumber.ToLower())
                                        || p.ClaimNumber.ToLower().Contains(searchTerm.ToLower())
                                        || (((p.ClaimantFirstName + " " + p.ClaimantLasttName).ToLowerInvariant()).Contains(searchTermLowerCaps)))).ToList();

            var tasks = await MainViewModel.Instance.CscRepository.GetObjectsAsync<ClaimAdjusterTask>();
            var events = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Event>();
            foreach (var item in claimsList)
            {
                List<ClaimAdjusterTask> tasksAgainstClaim = tasks.Where(p => p.TaskParentType == ParentType.Claim &&
                                                                             p.ClaimNumber.Equals(item.ClaimNumber) &&
                                                                             p.Status != (int)DataStatus.ClosedOffline).ToList(); //ensures that closed tasks are not listed in the search results
                List<Event> eventsAgainstClaim = events.Where(p => p.EventNumber.Equals(item.EventNumber)).ToList();

                tasksList.AddRange(tasksAgainstClaim);
                eventsList.AddRange(eventsAgainstClaim);
            }

            await Claims.AddToCollection(claimsList);
            await Events.AddToCollection(eventsList);
            await Tasks.AddToCollection(tasksList);

            RaisePropertyChanged("HasClaims");
            RaisePropertyChanged("HasEvents");
            RaisePropertyChanged("HasTasks");

            RaisePropertyChanged("ClaimsText");
            RaisePropertyChanged("EventsText");
            RaisePropertyChanged("TasksText");

            HasNoRecords = Claims.Count == 0;
            RaisePropertyChanged("HasNoRecords");

            MainViewModel.Instance.SetAppBusy(Constants.State_SearchLoading, false);
        }

        #endregion

        #region Methods

        public void ClearCollections()
        {
            Tasks.UnLoad();
            Events.UnLoad();
            Claims.UnLoad();
        }

        /// <summary>
        /// Removes the closed tasks while navigating back to the search results page
        /// </summary>
        public void RemoveClosedTasks()
        {
            var taskIds = Tasks.Collection.Select(p => p.CscTask.Id).ToList();
            foreach (var item in taskIds)
            {
                if (MainViewModel.Instance.ClosedTaskList.Contains(item))
                    Tasks.Collection.Remove(Tasks.Collection.FirstOrDefault(p => p.CscTask.Id == item));
            }
            RaisePropertyChanged("HasTasks");
            RaisePropertyChanged("TasksText");
        }

        #endregion
    }

    public enum SearchType
    {
        Claims,
        Events,
        Tasks
    }
}
