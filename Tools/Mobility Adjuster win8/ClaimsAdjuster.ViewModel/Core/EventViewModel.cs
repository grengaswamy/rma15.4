﻿using ClaimsAdjuster.ViewModel.Helpers;
using ClaimsAdjuster.ViewModel.Repository;
using IdentityMine.ViewModel;
using System.Windows.Input;

namespace ClaimsAdjuster.ViewModel
{
    public class EventViewModel : ClaimsAdjuster
    {
        #region Fields

        private TaskCollection tasks;
        private ClaimCollection claims;
        private ICommand navigateToClaimsCommand;
        private Event cscEvent;

        #endregion

        #region Properties

        public Event CscEvent
        {
            get
            {
                return cscEvent;
            }
            set
            {
                cscEvent = value;
                RaisePropertyChanged("CscEvent");
                RaisePropertyChanged("HasFullAddress");
            }
        }

        public bool IsNew
        {
            get
            {
                return CscEvent != null && CscEvent.Status == (int)DataStatus.NewlyAddedFromServer;
            }
        }

        public TaskCollection Tasks
        {
            get
            {
                if (tasks == null)
                    tasks = new TaskCollection();
                return tasks;
            }
        }

        public ClaimCollection Claims
        {
            get
            {
                if (claims == null)
                    claims = new ClaimCollection();
                return claims;
            }
        }

        /// <summary>
        /// Gets whether event has any tasks.
        /// </summary>
        public bool HasTasks
        {
            get
            {
                return Tasks.Count > 0;
            }
        }

        /// <summary>
        /// Gets whether event has any tasks.
        /// </summary>
        public bool HasClaims
        {
            get
            {
                return Claims.Count > 0;
            }
        }

        public override bool HasFullAddress
        {
            get
            {
                return CscEvent != null && !string.IsNullOrWhiteSpace(this.CscEvent.FullAddress);
            }
        }

        #endregion

        #region Command

        /// <summary>
        /// Command for navigating to claim listing page
        /// </summary>
        public ICommand NavigateToClaimsCommand
        {
            get
            {
                if (navigateToClaimsCommand == null)
                    navigateToClaimsCommand = new RelayCommand(OnNavigateToClaimsCommand);
                return navigateToClaimsCommand;
            }
        }

        #endregion

        #region Methods

        public override IServiceRequest CreateServiceRequest()
        {
            string serviceId = string.Format(ServiceRequestIds.EventDetailsRequest, CscEvent.EventNumber);
            EventDetailsRequest eventDetailsRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as EventDetailsRequest;
            if (eventDetailsRequest == null)
                eventDetailsRequest = new EventDetailsRequest(serviceId);
            eventDetailsRequest.Result = this;
            return eventDetailsRequest;
        }

        /// <summary>
        /// Loads Respective Claims and Tasks
        /// </summary>
        public void InitializeDetails()
        {
            LoadTasks();
            LoadClaims();
        }

        private async void LoadClaims()
        {
            var claimItems = await MainViewModel.Instance.CscRepository.GetClaimsByEventAsync(CscEvent.EventNumber);
            await Claims.AddToCollection(claimItems);
            RaisePropertyChanged("HasClaims");
        }

        public async void LoadTasks()
        {
            var taskItems = await MainViewModel.Instance.CscRepository.GetTaskByEventAsync(CscEvent.EventNumber);
            await Tasks.AddToCollection(taskItems);
            RaisePropertyChanged("HasTasks");
        }

        public void ClearCollection()
        {
            Tasks.Collection.Clear();
            Claims.Collection.Clear();
            Pictures.Clear();
            Videos.Clear();
        }

        public override void OnNavigateToTasksCommand()
        {
            string serviceId = string.Format(ServiceRequestIds.CscTasksRequest, CscEvent.EventNumber);

            TasksRequest cscTasksRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as TasksRequest;
            if (cscTasksRequest == null)
                cscTasksRequest = new TasksRequest(serviceId);
            cscTasksRequest.ParentNumber = CscEvent.EventNumber;
            cscTasksRequest.ParentType = ParentType.Event;
            MainViewModel.Instance.Navigate(serviceId);
        }

        private void OnNavigateToClaimsCommand()
        {
            string serviceId = string.Format(ServiceRequestIds.CscClaimsRequest, CscEvent.EventNumber);
            ClaimsRequest cscClaimsRequest = MainViewModel.Instance.ServiceFactory.GetService(serviceId) as ClaimsRequest;
            if (cscClaimsRequest == null)
                cscClaimsRequest = new ClaimsRequest(serviceId);
            cscClaimsRequest.EventNumber = CscEvent.EventNumber;
            MainViewModel.Instance.Navigate(serviceId);
        }

        protected override void OnLaunchMapCommand()
        {
            LaunchHelper.LaunchMap(this.CscEvent.FullAddress);
        }

        #endregion
    }
}
