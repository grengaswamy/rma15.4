﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Runtime.Serialization;
using IdentityMine.ViewModel;


namespace ClaimsAdjuster.ViewModel
{
    /// <summary>
    /// This class carries all the Persistant data , saved and loaded from isolated storage.
    /// </summary>
    [DataContract]
    public class AppSettings : LocationSettingsBase
    {
        #region Fields

        public static string IdString = Constants.DefaultAppSettingsId;

        #endregion

        #region Constructors

        public AppSettings():base()
        {
            Id = IdString;
            CurrentLanguage = "en-US";
        }

        static AppSettings()
        {
            IdString = Constants.DefaultAppSettingsId;
        }

        #endregion

        #region Properties

        [DataMember]
        public string CurrentLanguage { get; set; }

        [DataMember]
        public int LastLoggedUserId { get; set; }

        [DataMember]
        public DateTime SyncDate { get; set; }

        #endregion

    }
}