﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace ClaimsAdjuster.ViewModel.Helpers
{
    public static class LogHelper
    {
        public async static void Log(string message)
        {
            try
            {
                StorageFolder local = ApplicationData.Current.LocalFolder;
                StorageFile logFile = await local.CreateFileAsync("log.txt", CreationCollisionOption.OpenIfExists);

                List<string> lines = new List<string>();
                lines.Add(string.Format("{0}: {1}", DateTime.Now.ToString(), message));

                await FileIO.AppendLinesAsync(logFile, lines);
            }
            catch (Exception)
            {

            }
        }
    }
}
