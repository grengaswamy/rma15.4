﻿using ClaimsAdjuster.ViewModel.Repository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ClaimsAdjuster.ViewModel.Helpers
{
    public static class ParsingHelper
    {

        #region Methods

        /// <summary>
        /// To parse the  response of claim List API
        /// </summary>
        /// <param name="claimNumber"></param>
        /// <param name="xmlResult"></param>
        /// <returns></returns>
        public static List<Claim> ParseClaimList(List<Claim> offlineClaims, string xmlResult)
        {
            List<Claim> claims = null;
            XDocument xDoc;
            try
            {
                xDoc = XDocument.Parse(xmlResult);
            }
            catch
            {
                return null;
            }
            if (xDoc.Descendants("MsgStatusCd").First().Value == StatusCodes.Success.ToString())
            {
                var document = xDoc.Descendants("Document").First();
                var colums = document.Descendants("columns").First();
                var columnList = document.Descendants("column").ToList();

                var results = document.Descendants("results").First();
                var datalist = results.Descendants("data").First();
                var claimList = datalist.Descendants("row").ToList();
                if (claimList != null && claimList.Count > 0)
                {
                    Claim claim = null;
                    foreach (var claimRow in claimList)
                    {
                        int index = 0;
                        var claimRowFields = claimRow.Descendants("field").ToList();

                        index = columnList.FindIndex(column => column.Value.Equals("Claim Number"));
                        var claimNumber = claimRowFields[index].Value;
                        if (string.IsNullOrEmpty(claimNumber))
                            claimNumber = "Claim";
                        claim = new Claim();

                        if (!offlineClaims.Any(offlineClaim => offlineClaim.ClaimNumber.Trim().Equals(claimNumber)))
                        {
                            claim.Status = (int)DataStatus.NewlyAddedFromServer;

                        }
                        else
                        {
                            claim.Status = (int)DataStatus.PreviouslyAddedFromServer;
                        }

                        claim.ClaimNumber = claimNumber;

                        index = columnList.FindIndex(column => column.Value.Equals("Policy Name"));
                        claim.PolicyName = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Claimant First Name"));
                        claim.ClaimantFirstName = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Claimant Last Name"));
                        claim.ClaimantLasttName = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Claimant Address 1"));
                        claim.ClaimantAddress1 = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Claimant Address 2"));
                        claim.ClaimantAddress2 = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Claimant City"));
                        claim.ClaimantCity = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Claimant State"));
                        claim.ClaimantState = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Claimant ZIP Code"));
                        claim.ClaimantZipCode = claimRowFields[index].Value;

                        //mbahl3 Mits 36145
                        index = columnList.FindIndex(column => column.Value.Equals("Claimant Office Phone"));
                        claim.ClaimantOfficePhone = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Claimant Home Phone"));
                        claim.ClaimantHomePhone = claimRowFields[index].Value;


                        //mbahl3 Mits 36145
                        index = columnList.FindIndex(column => column.Value.Equals("Event Number"));
                        if (index >= 0)
                            claim.EventNumber = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Claimant Email"));
                        if (index >= 0)
                            claim.ClaimantEmailAddress = claimRowFields[index].Value;

                        index = columnList.FindIndex(column => column.Value.Equals("Event Description"));
                        if (index >= 0)
                            claim.LossDescription = claimRowFields[index].Value;


                        index = columnList.FindIndex(column => column.Value.Equals("Event Date"));
                        if (index >= 0)
                            claim.LossDate = DateTime.Parse(claimRowFields[index].Attribute("formatedtext").Value, CultureInfo.InvariantCulture);

                        index = columnList.FindIndex(column => column.Value.Equals("Claim Status"));
                        if (index >= 0)
                        {
                            var claimStatus = claimRowFields[index].Value;
                            claim.ClaimStatus = string.IsNullOrWhiteSpace(claimStatus) || !claimStatus.Contains("-") ? claimStatus : claimStatus.Substring(claimStatus.IndexOf("-") + 1).Trim();
                        }
                        index = columnList.FindIndex(column => column.Value.Equals("Claim Date"));
                        if (index >= 0)
                            claim.ClaimDate = DateTime.Parse(claimRowFields[index].Attribute("formatedtext").Value, CultureInfo.InvariantCulture);

                        claim.UserId = MainViewModel.Instance.CurrentUserInfo.Id;
                        claim.LastSyncDate = DateTime.Now;
                        claim.IsDetailDownloaded = false;
                        if (claims == null) claims = new List<Claim>();
                        claims.Add(claim);

                        // mbahl3 Mits  36145  hack to map correct claimant details to claim details
                        claim.ClaimantServerEntityID = claims.Count(c => c.ClaimNumber.Equals(claimNumber));
                    }

                }
            }

            return claims;
        }

        /// <summary>
        /// To parse the  response of task list API
        /// </summary>
        /// <param name="claimNumber"></param>
        /// <param name="xmlResult"></param>
        /// <returns></returns>
        public static List<ClaimAdjusterTask> ParseTaskList(List<ClaimAdjusterTask> offlineTasks, string xmlResult)
        {
            List<ClaimAdjusterTask> tasks = null;
            XDocument xDoc;
            try
            {
                xDoc = XDocument.Parse(xmlResult);
            }
            catch
            {
                return null;
            }
            if (xDoc.Descendants("MsgStatusCd").First().Value == StatusCodes.Success.ToString())
            {
                var document = xDoc.Descendants("Document").First();
                var diaries = document.Descendants("diaries").First();
                var diaryList = diaries.Descendants("diary").ToList();
                if (diaryList != null && diaryList.Count > 0)
                {

                    var downloadedTaskNumbers = new List<int>();
                    foreach (var diary in diaryList)
                    {
                        try
                        {
                            string attachPrompt = diary.Attribute("attachprompt").Value;
                            if (!string.IsNullOrWhiteSpace(attachPrompt) && attachPrompt != "No ")
                            {
                                int serverEntryId = Convert.ToInt32(diary.Attributes("entry_id").First().Value);
                                ClaimAdjusterTask task = offlineTasks.FirstOrDefault(offlinetask => offlinetask.ServerEntryId == serverEntryId && offlinetask.UserId == MainViewModel.Instance.CurrentUserInfo.Id);
                                if (task == null)
                                {
                                    task = new ClaimAdjusterTask()
                                    {
                                        UserId = MainViewModel.Instance.CurrentUserInfo.Id
                                    };
                                    task.Status = (int)DataStatus.NewlyAddedFromServer;
                                }
                                else
                                {
                                    if (task.Status == (int)DataStatus.ClosedOffline)
                                        continue;
                                    task.Status = (int)DataStatus.PreviouslyAddedFromServer;
                                }
                                task.ServerEntryId = serverEntryId;
                                task.TaskType = diary.Attribute("work_activity").Value;
                                task.EventId = 0;
                                var attachPromptItems = attachPrompt.Split();
                                if (attachPromptItems.Count() > 0)
                                {
                                    if (attachPromptItems[0] == "CLAIM")
                                    {
                                        task.TaskParentType = ParentType.Claim;
                                        task.ClaimNumber = attachPromptItems[1];
                                    }
                                    else if (attachPromptItems[0] == "EVENT")
                                    {
                                        task.TaskParentType = ParentType.Event;
                                        task.EventNumber = attachPromptItems[1];
                                    }
                                }
                                task.TaskDateTime = DateTime.Parse(string.Format("{0} {1}", diary.Attribute("complete_date").Value, diary.Attribute("CompleteTime").Value), CultureInfo.InvariantCulture);
                                var attriEnteryNotes = diary.Attribute("EntryNotes");
                                if (attriEnteryNotes != null)
                                {
                                    //Changed based on new API to EntryNotes
                                    task.TaskDescription = attriEnteryNotes.Value;
                                }
                                task.TaskName = diary.Attribute("tasksubject").Value;
                                if (string.IsNullOrEmpty(task.TaskName))
                                    task.TaskName = "Task";
                                task.LastSyncDate = DateTime.Now;
                                if (tasks == null)
                                    tasks = new List<ClaimAdjusterTask>();
                                tasks.Add(task);
                            }
                        }
                        catch
                        {

                        }

                    }

                }

            }

            return tasks;
        }


        /// <summary>
        /// To parse the  response of claim detail API
        /// </summary>
        /// <param name="claimNumber"></param>
        /// <param name="xmlResult"></param>
        /// <returns></returns>
        public static async Task<bool> ParseClaimDetail(string xmlResult, Claim currentClaim = null, string claimNumber = "")
        {
            bool success = false;
            try
            {
                XDocument xDoc = XDocument.Parse(xmlResult);
                if (xDoc.Descendants("MsgStatusCd").First().Value == StatusCodes.Success.ToString())
                {
                    var offlineClaims = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();

                    Claim claim = null;
                    if (currentClaim != null)
                    {
                        claim = currentClaim;
                        claimNumber = currentClaim.ClaimNumber;
                    }
                    else
                    {
                        claim = offlineClaims.FirstOrDefault(offlineClaim => offlineClaim.ClaimNumber.Trim().Equals(claimNumber));
                    }
                    if (claim == null)
                        claim = new Claim();

                    var document = xDoc.Descendants("Document").First();
                    var executiveSummaryReport = document.Descendants("ExecutiveSummaryReport").First();
                    var file = executiveSummaryReport.Descendants("File").First();
                    var claimSummary = file.Descendants("ClaimSummary").First();



                    var claimInfo = claimSummary.Descendants("ClaimInfo").First();
                    if (claimInfo != null)
                    {
                        claim.LossDate = claim.LossDate != DateTime.MinValue ? claim.LossDate : DateTime.Parse(claimInfo.Descendants("EventDateRptd").First().Value, CultureInfo.InvariantCulture);
                        claim.LossDescription = string.IsNullOrWhiteSpace(claim.LossDescription) ? claimInfo.Descendants("LossDescription").First().Value : claim.LossDescription;
                        claim.PolicyName = string.IsNullOrWhiteSpace(claim.PolicyName) ? claimInfo.Descendants("PolicyName").First().Value : claim.PolicyName;
                        claim.ClaimStatus = claimInfo.Descendants("ClaimStatus").First().Value;
                        // TO DO : Data to be removed
                        //claim.PolicyNumber = "AS45867512658";
                        //claim.PolicyDescriptioin = "Policy description lorem ipsum is simply dummy textof the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";

                        var enhancedNotes = claimInfo.Descendants("EnhancedNotes").Count() > 0 ? claimInfo.Descendants("EnhancedNotes").First() : null;
                        if (enhancedNotes != null)
                        {

                            var notes = enhancedNotes.Descendants("Note").ToList();
                            if (notes != null)
                            {
                                var existingNotes = await MainViewModel.Instance.CscRepository.GetNotesByClaimAsync(claimNumber);
                                List<Notes> newNotes = new List<Notes>();

                                foreach (var onlineNote in notes)
                                {

                                    Notes newNote = new Notes()
                                    {

                                        NoteServerId = onlineNote.Descendants("NoteId").First().Value,
                                        ParentType = (int)ParentType.Claim,
                                        ParentId = claimNumber,
                                        Heading = onlineNote.Descendants("NoteType").First().Value,
                                        Content = onlineNote.Descendants("NoteText").First().Value,
                                        DateCreated = StringHelpers.GetParsedDateTime(onlineNote.Descendants("DateCreated").First().Value, onlineNote.Descendants("TimeCreated").First().Value),
                                        Status = (int)DataStatus.NewlyAddedFromServer,
                                        UserId = MainViewModel.Instance.CurrentUserInfo.Id,
                                    };
                                    if (existingNotes.Any(note => note.NoteServerId == newNote.NoteServerId))
                                    {
                                        newNote.Status = (int)DataStatus.PreviouslyAddedFromServer;
                                    }
                                    else
                                    {
                                        newNote.Status = (int)DataStatus.NewlyAddedFromServer;
                                    }
                                    newNotes.Add(newNote);
                                }
                                await MainViewModel.Instance.CscRepository.DeleteItemListAsync<Notes>(existingNotes);
                                await MainViewModel.Instance.CscRepository.InsertItemsAsync(newNotes);
                            }
                        }

                    }

                    var eventInfo = claimSummary.Descendants("EventDetail").First();
                    if (eventInfo != null)
                    {
                        var eventNumber = claimInfo.Descendants("EventNumber").First().Value;
                        var offlineEvents = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Event>();
                        Event claimEvent = offlineEvents.FirstOrDefault(offlineEvent => offlineEvent.EventNumber.Equals(eventNumber));
                        if (claimEvent != null)
                        {


                            claimEvent.EventNumber = eventNumber;
                            //claimEvent.EventName = eventInfo.Descendants("EventNumber").First().Value;
                            claimEvent.EventDescription = eventInfo.Descendants("EventDescription").First().Value;
                            claimEvent.EventDate = DateTime.Parse(claimInfo.Descendants("DateOfEvent").First().Value + " " + claimInfo.Descendants("TimeOfEvent").First().Value, CultureInfo.InvariantCulture);
                            claimEvent.EventAddress1 = eventInfo.Descendants("LocationAddress").First().Value;
                            claimEvent.City = eventInfo.Descendants("City").First().Value;
                            claimEvent.State = eventInfo.Descendants("State").First().Value;
                            claimEvent.PostalCode = eventInfo.Descendants("Zipcode").First().Value;
                            claimEvent.Country = eventInfo.Descendants("CountyOfInjury").First().Value;
                            //claimEvent.Status = (int)DataStatus.PreviouslyAddedFromServer;
                            claimEvent.UserId = MainViewModel.Instance.CurrentUserInfo.Id;
                            claimEvent.LastSyncDate = DateTime.Now;
                            await MainViewModel.Instance.CscRepository.SaveEventAsync(claimEvent);
                            MainViewModel.Instance.HomePageVM.AddToHomeEvents(claimEvent);//Inserts item to HomePage events
                            claim.EventNumber = eventNumber;
                            claim.EventId = claimEvent.Id;
                            claim.EventAddress = claimEvent.FullAddress;
                            claim.EventLocation = !string.IsNullOrWhiteSpace(claimEvent.State) ? claimEvent.State : (!string.IsNullOrWhiteSpace(claimEvent.City) ? claimEvent.City : (!string.IsNullOrWhiteSpace(claimEvent.EventAddress1) ? claimEvent.EventAddress1 : string.Empty));
                        }
                    }
                    var claimantsInformationDescendants = claimSummary.Descendants("ClaimantsInformation");
                    if (claimantsInformationDescendants != null && claimantsInformationDescendants.Count() > 0)
                    {
                        var claimantsInformation = claimantsInformationDescendants.First();
                        var claimants = claimantsInformation.Descendants("Claimants").First();
                        //mbahl3 Mits   36145
						XElement claimant1 = default(XElement);
                        if (claimants.Descendants(string.Format("Claimant{0}", claim.ClaimantServerEntityID)).First() != null)
                        {
                            claimant1 = claimants.Descendants(string.Format("Claimant{0}", claim.ClaimantServerEntityID)).First();
                        }
						//var claimant1 = claimants.Descendants("Claimant1").First();
                        if (claimant1 != null)
                        {
                            claim.ClaimantFirstName = string.IsNullOrWhiteSpace(claim.ClaimantFirstName) ? claimant1.Descendants("ClaimantName").First().Value : claim.ClaimantFirstName;
                            claim.ClaimantAddress1 = claimant1.Descendants("Address").First().Value;
                            claim.ClaimantCity = claimant1.Descendants("City").First().Value;
                            claim.ClaimantState = claimant1.Descendants("State").First().Value;
                            claim.ClaimantZipCode = claimant1.Descendants("ZipCode").First().Value;
                            var countryDescendants = claimant1.Descendants("Country");
                            claim.ClaimantCounty = countryDescendants != null && countryDescendants.Count() > 0 ? countryDescendants.First().Value : string.Empty;
                            claim.ClaimantHomePhone = claimant1.Descendants("HomePhone").First().Value;
                            claim.ClaimantOfficePhone = claimant1.Descendants("OfficePhone").First().Value;
                        }
                    }
                    var diaryEntryDetailDescendants = claimSummary.Descendants("DiaryEntryDetail");
                    if (diaryEntryDetailDescendants != null && diaryEntryDetailDescendants.Count() > 0)
                    {
                        var diaryEntryDetail = diaryEntryDetailDescendants.First();
                        var diaryList = diaryEntryDetail.Descendants("Diary").ToList();
                        if (diaryList != null && diaryList.Count > 0)
                        {
                            var offlineTasks = await MainViewModel.Instance.CscRepository.GetObjectsAsync<ClaimAdjusterTask>();
                            foreach (var diary in diaryList)
                            {
                                int serverEntryId = Convert.ToInt32(diary.Descendants("EntryID").First().Value);
                                var task = offlineTasks.Where(offlinetask => offlinetask.ServerEntryId == serverEntryId && offlinetask.UserId == MainViewModel.Instance.CurrentUserInfo.Id).FirstOrDefault();
                                if (task != null)
                                {
                                    task.ServerEntryId = serverEntryId;
                                    task.TaskType = diary.Descendants("WorkActivity").First().Value;
                                    task.EventId = 0;
                                    task.ClaimNumber = claimNumber;
                                    task.TaskDateTime = task.TaskDateTime == DateTime.MinValue ? DateTime.Parse(diary.Descendants("DateDue").First().Value, CultureInfo.InvariantCulture) : task.TaskDateTime;
                                    task.TaskDescription = string.IsNullOrWhiteSpace(task.TaskDescription) ? diary.Descendants("EntryNotes").First().Value : task.TaskDescription;
                                    task.TaskName = string.IsNullOrWhiteSpace(task.TaskName) ? diary.Descendants("TaskName").First().Value : task.TaskName;
                                    //task.Status = (int)DataStatus.NewlyAddedFromServer;
                                    task.UserId = MainViewModel.Instance.CurrentUserInfo.Id;

                                    //if (task.TaskDateTime >= MainViewModel.Instance.TaskFilterFromDate && task.TaskDateTime <= MainViewModel.Instance.TaskFilterToDate)
                                    await MainViewModel.Instance.CscRepository.SaveClaimAdjusterTaskAsync(task);
                                }

                            }
                        }

                    }
                    var paymentHistoryDescendants = claimSummary.Descendants("PaymentHistory");
                    if (paymentHistoryDescendants != null && paymentHistoryDescendants.Count() > 0)
                    {
                        await MainViewModel.Instance.CscRepository.DeletePaymentByClaimAsync(claimNumber);
                        var paymentHistory = paymentHistoryDescendants.First();
                        claim.NetTotal = StringHelpers.ParseAsDouble(paymentHistory.Descendants("TotalAll").First().Value);
                        claim.TotalCollection = StringHelpers.ParseAsDouble(paymentHistory.Descendants("TotalCollect").First().Value);
                        claim.TotalPayments = StringHelpers.ParseAsDouble(paymentHistory.Descendants("TotalPay").First().Value);
                        claim.TotalVoids = StringHelpers.ParseAsDouble(paymentHistory.Descendants("TotalVoid").First().Value);
                        claim.PaymentsLessVoids = StringHelpers.ParseAsDouble(paymentHistory.Descendants("PaymentsLessVoids").First().Value);

                        var paymentList = paymentHistory.Descendants("Payment").ToList();
                        if (paymentList != null && paymentList.Count > 0)
                        {
                            claim.HasPayment = true;
                        }
                    }
                    var personsInvolvedDescendants = claimSummary.Descendants("PersonsInvolved");
                    if (personsInvolvedDescendants != null && personsInvolvedDescendants.Count() > 0)
                    {
                        await MainViewModel.Instance.CscRepository.DeletePartyInvolvedByClaimAsync(claimNumber);
                        List<PartyInvolved> partiesInvolved = new List<PartyInvolved>();

                        var personsInvolved = personsInvolvedDescendants.First();
                        foreach (var partyType in personsInvolvedDescendants)
                        {
                            var partyGroups = partyType.Elements();
                            if (partyGroups.Count() > 0)
                            {
                                foreach (var partyGroup in partyGroups)
                                {
                                    var parties = partyGroup.Elements();
                                    if (parties.Count() > 0)
                                    {
                                        foreach (var party in parties)
                                        {
                                            var newPartyInvolved = ParsePartyInvolved((party as XElement), claimNumber);
                                            if (newPartyInvolved != null)
                                                partiesInvolved.Add(newPartyInvolved);

                                        }
                                    }
                                }
                            }

                        }
                        if (partiesInvolved.Count > 0)
                        {
                            // [MS : ]  :   Not required as this is not the main type. Not sure why we decided to add this
                            //claim.TotalPartiesInvolved = partiesInvolved.Count + 1;
                            claim.TotalPartiesInvolved = partiesInvolved.Count;
                            IEnumerable<PartyInvolved> partiesInvolvedToSave = partiesInvolved;
                            await MainViewModel.Instance.CscRepository.InsertItemsAsync(partiesInvolvedToSave);
                        }

                    }
                    claim.IsDetailDownloaded = true;
                    await MainViewModel.Instance.CscRepository.SaveClaimAsync(claim);
                    success = true;
                }
            }
            catch
            {
                success = false;
            }

            return success;
        }

        public static async Task<bool> ParsePersonsList(string xmlResult, Claim currentClaim = null, string claimNumber = "")
        {
            bool success = false;
            XDocument xDoc = null;
            try
            {
                xDoc = XDocument.Parse(xmlResult);
            }
            catch
            {
                return false;
            }
            return success;
        }

        public static async Task<bool> ParsePaymentHistory(string xmlResult, Claim currentClaim = null, string claimNumber = "")
        {
            bool success = false;
            XDocument xDoc = null;
            try
            {
                xDoc = XDocument.Parse(xmlResult);
            }
            catch
            {
                return false;
            }
            if (xDoc.Descendants("MsgStatusCd").First().Value == StatusCodes.Success.ToString())
            {
                if (currentClaim != null)
                    claimNumber = currentClaim.ClaimNumber;
                var paymentHistoryDescendants = xDoc.Descendants("PaymentHistory");
                if (paymentHistoryDescendants != null && paymentHistoryDescendants.Count() > 0)
                {
                    var paymentHistory = paymentHistoryDescendants.First();

                    currentClaim.NetTotal = StringHelpers.ParseAsDouble(paymentHistory.Descendants("TotalAll").First().Value);
                    currentClaim.TotalCollection = StringHelpers.ParseAsDouble(paymentHistory.Descendants("TotalCollect").First().Value);
                    currentClaim.TotalPayments = StringHelpers.ParseAsDouble(paymentHistory.Descendants("TotalPay").First().Value);
                    currentClaim.TotalVoids = StringHelpers.ParseAsDouble(paymentHistory.Descendants("TotalVoid").First().Value);
                    currentClaim.PaymentsLessVoids = StringHelpers.ParseAsDouble(paymentHistory.Descendants("PaymentsLessVoids").First().Value);


                    var paymentList = paymentHistory.Descendants("row").ToList();
                    if (paymentList != null && paymentList.Count > 0)
                    {

                        await MainViewModel.Instance.CscRepository.DeletePaymentHistoryByClaimAsync(claimNumber);
                        List<PaymentHistory> payList = new List<PaymentHistory>();
                        foreach (var paymentHistoryData in paymentList)
                        {

                            payList.Add(new PaymentHistory()
                            {
                                ClaimNumber = claimNumber,
                                PaymentDate = DateTime.Parse(paymentHistoryData.Descendants("CheckDate").First().Value, CultureInfo.InvariantCulture),
                                PaymentType = paymentHistoryData.Descendants("PaymentTypes").First().Value,
                                Payee = ParsePayee(paymentHistoryData.Descendants("Name")),
                                PaymentStatus = paymentHistoryData.Descendants("CheckStatus").First().Value.ToUpper().Contains("RELEASED") ? Constants.PaymentClearedText : Constants.PaymentNotClearedText,
                                Amount = StringHelpers.ParseAsDouble((paymentHistoryData.Descendants("Amount").First().Value)),
                                Status = (int)DataStatus.NewlyAddedFromServer,
                                UserId = MainViewModel.Instance.CurrentUserInfo.Id,
                                PaymentDescription = paymentHistoryData.Descendants("PaymentDescription").First().Value,
                                PayeeAddress = paymentHistoryData.Descendants("Address").First().Value,
                            }
                           );

                        }
                        if (payList.Count > 0)
                        {

                            await MainViewModel.Instance.CscRepository.InsertItemsAsync(payList);
                        }
                    }
                }

            }
            return success;
        }

        /// <summary>
        /// To parse the response of PartyInvolved
        /// </summary>
        /// <param name="xmlParty"></param>
        /// <param name="claimNumber"></param>
        /// <returns></returns>
        /// 

        private static string ParsePayee(IEnumerable<XElement> payee)
        {
            StringBuilder strb = new StringBuilder();
            int count = 0;
            foreach (var payeeNames in payee.ToList())
            {
                if (count != 0)
                    strb.Append(" , ");
                strb.Append(payeeNames.Value);
                count++;
            }

            return strb.ToString();
        }
        private static PartyInvolved ParsePartyInvolved(XElement xmlParty, string claimNumber)
        {
            PartyInvolved newPartyInvolved = null;
            try
            {
                var homePhoneDescendants = xmlParty.Descendants("HomePhone");
                var officePhoneDescendants = xmlParty.Descendants("OfficePhone");
                var genderDescendants = xmlParty.Descendants("Sex");
                var entityTypeDescendants = xmlParty.Descendants("EntityType");
                var typeDescendants = xmlParty.Descendants("Type");
                newPartyInvolved = new PartyInvolved()
                {
                    ClaimNumber = claimNumber,
                    ServerEntityID = Convert.ToInt32(xmlParty.Descendants("EntityID").First().Value),
                    EntityType = entityTypeDescendants != null && entityTypeDescendants.Count() > 0 ? entityTypeDescendants.First().Value : (typeDescendants != null && typeDescendants.Count() > 0 ? typeDescendants.First().Value : string.Empty),
                    Name = xmlParty.Descendants("Name").First().Value,
                    Gender = genderDescendants != null && genderDescendants.Count() > 0 ? genderDescendants.First().Value : string.Empty,
                    HomePhone = homePhoneDescendants != null && homePhoneDescendants.Count() > 0 ? homePhoneDescendants.First().Value : string.Empty,
                    OfficePhone = officePhoneDescendants != null && officePhoneDescendants.Count() > 0 ? officePhoneDescendants.First().Value : string.Empty,
                    Status = (int)DataStatus.NewlyAddedFromServer,
                    UserId = MainViewModel.Instance.CurrentUserInfo.Id,
                };
                var objAddressInformation = xmlParty.Descendants("AddressInformation");
                if (objAddressInformation != null && objAddressInformation.Count() > 0)
                {
                    var addressInformation = objAddressInformation.First();
                    var address1 = addressInformation.Descendants("Address1").First();
                    newPartyInvolved.Address1 = address1.Descendants("Addr1").First().Value;
                    newPartyInvolved.Address2 = address1.Descendants("Addr2").First().Value;
                    newPartyInvolved.City = address1.Descendants("City").First().Value;
                    newPartyInvolved.State = address1.Descendants("County").First().Value;
                    newPartyInvolved.County = address1.Descendants("State").First().Value;
                    newPartyInvolved.ZipCode = address1.Descendants("ZipCode").First().Value;
                    newPartyInvolved.EmailAddress = address1.Descendants("EmailAddress").First().Value;
                }
            }
            catch
            {

            }

            return newPartyInvolved;
        }


        /// <summary>
        /// To parse the response of Note Created
        /// </summary>
        /// <param name="xmlParty"></param>
        /// <param name="claimNumber"></param>
        /// <returns></returns>
        public static bool ParseNoteCreatResponse(string xmlResult)
        {
            XDocument response = XDocument.Parse(xmlResult);
            var msgStatusDes = response.Descendants("MsgStatus");
            var claimProgressNoteDes = response.Descendants("ClaimProgressNoteId");
            return claimProgressNoteDes != null && claimProgressNoteDes.Count() > 0 && Convert.ToInt32(claimProgressNoteDes.First().Value) > 0;
        }

        public static List<string> ParseAttachmentCreatResponse(string xmlResult)
        {
            List<string> strReturn = null;
            XDocument response = XDocument.Parse(xmlResult);
            var msgStatusDes = response.Descendants("MsgStatus");
            var msgStatus = msgStatusDes.Descendants("MsgStatusCd").FirstOrDefault();
            var files = msgStatusDes.Descendants("File").ToList();
            if ((msgStatus != null && msgStatus.Value.ToLower() == "success"))
            {
                if (files.Count > 0)
                    strReturn = new List<string>();
                foreach (var results in files)
                {
                    strReturn.Add(results.Value);
                }
            }
            return strReturn;

        }

        /// <summary>
        /// Parse the success message and retures true/false
        /// </summary>
        /// <param name="uploadResult"></param>
        /// <returns></returns>
        public static bool UploadResultParse(string uploadResult)
        {
            XDocument xDoc = null;
            try
            {
                xDoc = XDocument.Parse(uploadResult);
            }
            catch (Exception)
            {
                return false;
            }
            var msgStatus = xDoc.Descendants("MsgStatus").FirstOrDefault();
            if (msgStatus != null)
            {
                var msgCode = msgStatus.Descendants("MsgStatusCd").FirstOrDefault();
                if (msgCode != null)
                {
                    string msg = msgCode.Value;
                    if (msg == "Success")
                    {
                        return true;
                    }
                    else if (msg == "Error")
                    {
                        var resultDocument = xDoc.Descendants("Document").FirstOrDefault();
                        if (resultDocument != null)
                        {
                            var document = resultDocument.Descendants("Document").FirstOrDefault();
                            if (document != null)
                            {
                                var diaryStatus = document.Descendants("DiaryStatus").FirstOrDefault();
                                if (diaryStatus != null)
                                {
                                    var status = diaryStatus.Descendants("Status").FirstOrDefault();
                                    if (status != null && status.Value == "Closed")
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Parses the result and returns the list of entry ids
        /// </summary>
        public static List<int> ParseNewTasksUpload(string newTasksResponse)
        {
            XDocument xDoc = null;
            try
            {
                xDoc = XDocument.Parse(newTasksResponse);
            }
            catch
            {
                return null;
            }
            List<int> entryIds = new List<int>();
            if (xDoc != null)
            {
                IEnumerable saveDiaries = xDoc.Descendants("SaveDiaryResponse").ToList();
                foreach (XElement item in saveDiaries)
                {
                    var resultMessage = item.Descendants("ResultMessage").FirstOrDefault();
                    if (resultMessage != null)
                    {
                        var msgStatus = resultMessage.Descendants("MsgStatus").FirstOrDefault();
                        if (msgStatus != null)
                        {
                            var msgCode = msgStatus.Descendants("MsgStatusCd").FirstOrDefault();
                            if (msgCode != null)
                            {
                                if (msgCode.Value == "Success")
                                {
                                    var document = resultMessage.Descendants("Document").FirstOrDefault();
                                    if (document != null)
                                    {
                                        var diary = document.Descendants("Diary").FirstOrDefault();
                                        if (diary != null)
                                        {
                                            entryIds.Add(Convert.ToInt32(diary.Attribute("EntryId").Value));
                                        }
                                        else
                                        {
                                            entryIds.Add(-1);//To differentiate between valid entry id
                                        }
                                    }
                                }
                                else
                                {
                                    entryIds.Add(-1);
                                }
                            }
                        }
                    }
                }
            }
            return entryIds;
        }

        public async static Task<List<Event>> ParseEventList(string eventsResult)
        {
            XDocument xDoc = null;
            try
            {
                xDoc = XDocument.Parse(eventsResult);
            }
            catch
            {
                return null;
            }
            List<Event> events = new List<Event>();
            if (xDoc != null)
            {
                XElement msgStatus = xDoc.Descendants("MsgStatus").FirstOrDefault();
                if (msgStatus != null)
                {
                    XElement msgStatusCode = msgStatus.Descendants("MsgStatusCd").FirstOrDefault();
                    if (msgStatusCode != null && msgStatusCode.Value == "Success")
                    {
                        XElement documet = xDoc.Descendants("Document").FirstOrDefault();
                        if (documet != null)
                        {
                            XElement results = documet.Descendants("results").FirstOrDefault();
                            if (results != null)
                            {
                                XElement columns = results.Descendants("columns").FirstOrDefault();
                                if (columns != null)
                                {
                                    var columnList = columns.Descendants("column").ToList();
                                    var data = results.Descendants("data").FirstOrDefault();
                                    if (data != null)
                                    {
                                        var rows = data.Descendants("row").ToList();
                                        if (rows != null && rows.Count > 0)
                                        {
                                            var offlineEvents = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Event>();
                                            foreach (var row in rows)
                                            {
                                                var fields = row.Descendants("field").ToList();
                                                int index = columnList.FindIndex(c => c.Value == "Event Number");
                                                string eventNumber = fields[index].Value;
                                                if (eventNumber.Length > 3)
                                                {
                                                    Event eventItem = new Event();
                                                    eventItem.EventNumber = eventNumber;
                                                    if (offlineEvents.Any(s => s.EventNumber == eventItem.EventNumber && s.UserId == MainViewModel.Instance.CurrentUserInfo.Id))
                                                        eventItem.Status = (int)DataStatus.PreviouslyAddedFromServer;
                                                    else
                                                        eventItem.Status = (int)DataStatus.NewlyAddedFromServer;
                                                    index = columnList.FindIndex(c => c.Value == "Event Date");
                                                    var date = fields[index].Attributes("formatedtext").FirstOrDefault();
                                                    if (date != null)
                                                    {
                                                        string dateText = date.Value;
                                                        if (!string.IsNullOrWhiteSpace(dateText))
                                                            eventItem.EventDate = DateTime.Parse(dateText, CultureInfo.InvariantCulture);
                                                    }
                                                    index = columnList.FindIndex(c => c.Value == "Event Description");
                                                    eventItem.EventDescription = fields[index].Value;
                                                    index = columnList.FindIndex(c => c.Value == "Loc.: Address 1");
                                                    eventItem.EventAddress1 = fields[index].Value;
                                                    index = columnList.FindIndex(c => c.Value == "Loc.: Address 2");
                                                    eventItem.EventAddress2 = fields[index].Value;
                                                    index = columnList.FindIndex(c => c.Value == "Event Title");
                                                    eventItem.EventName = fields[index].Value;
                                                    index = columnList.FindIndex(c => c.Value == "Number of Tasks");
                                                    string tasksCount = fields[index].Value;
                                                    if (!string.IsNullOrEmpty(tasksCount))
                                                        eventItem.TasksCount = Convert.ToInt32(tasksCount);
                                                    index = columnList.FindIndex(c => c.Value == "Number of Claims");
                                                    string claimsCount = fields[index].Value;
                                                    if (!string.IsNullOrEmpty(claimsCount))
                                                        eventItem.ClaimsCount = Convert.ToInt32(claimsCount);
                                                    eventItem.UserId = MainViewModel.Instance.CurrentUserInfo.Id;
                                                    index = columnList.FindIndex(c => c.Value == "Loc.: City");
                                                    eventItem.City = fields[index].Value;
                                                    index = columnList.FindIndex(c => c.Value == "Loc.: Country");
                                                    string countryCode = fields[index].Value;
                                                    if (!string.IsNullOrWhiteSpace(countryCode))
                                                    {
                                                        var countryCodes = countryCode.Split('-');
                                                        if (countryCodes.Count() > 1)
                                                            eventItem.Country = countryCodes[1].Trim();
                                                    }
                                                    index = columnList.FindIndex(c => c.Value == "Loc.: State");
                                                    string stateCode = fields[index].Value;
                                                    if (!string.IsNullOrWhiteSpace(stateCode))
                                                    {
                                                        var stateCodes = stateCode.Split('-');
                                                        if (stateCodes.Count() > 1)
                                                            eventItem.State = stateCodes[1].Trim();
                                                    }
                                                    index = columnList.FindIndex(c => c.Value == "Loc.: ZIP/Postal Code");
                                                    eventItem.PostalCode = fields[index].Value;
                                                    events.Add(eventItem);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return events;
        }

        public static List<StatusCodes> ParseUploadNewNotes(string notesUploadResponse)
        {
            XDocument xDoc = null;
            try
            {
                xDoc = XDocument.Parse(notesUploadResponse);
            }
            catch
            {
                return null;
            }
            List<StatusCodes> statusCodes = new List<StatusCodes>();
            var saveProgressNotes = xDoc.Descendants("SaveProgressNotesResponse").ToList();
            foreach (var item in saveProgressNotes)
            {
                var progressNote = item.Descendants("ProgressNote").FirstOrDefault();
                if (progressNote != null)
                {
                    var msgStatusDes = progressNote.Descendants("MsgStatus").FirstOrDefault();
                    var claimProgressNoteDes = progressNote.Descendants("ClaimProgressNoteId").FirstOrDefault();
                    if (msgStatusDes != null && msgStatusDes.Value == "Success" && claimProgressNoteDes != null && Convert.ToInt32(claimProgressNoteDes.Value) > 0)
                        statusCodes.Add(StatusCodes.Success);
                    else
                        statusCodes.Add(StatusCodes.Error);
                }

            }
            return statusCodes;
        }

        #endregion
    }
}
