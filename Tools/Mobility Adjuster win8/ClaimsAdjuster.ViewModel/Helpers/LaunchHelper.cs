﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Appointments;
using Windows.Storage;
using Windows.System;

namespace ClaimsAdjuster.ViewModel.Helpers
{

    /// <summary>
    /// Defines the helper class for launching external apps
    /// </summary>
    public class LaunchHelper
    {
        /// <summary>
        /// Launches map application with specified address.
        /// </summary>
        /// <remarks>
        /// For additional map parameter support, include an enumeration and extend the method.
        /// Reference: http://msdn.microsoft.com/en-us/library/windows/apps/jj635237.aspx
        /// </remarks>
        /// <param name="content"></param>
        public static async void LaunchMap(string content)
        {
            string addressFormat = string.Format("bingmaps://?where={0}", content);

            var uri = new Uri(addressFormat);
            await Launcher.LaunchUriAsync(uri);
        }

        /// <summary>
        /// Launches Skype app
        /// </summary>
        /// <remarks>
        /// Reference: http://vdcruijsen.net/2013/04/launching-skype-from-your-own-windows-store-app/
        /// </remarks>
        /// //mbahl3 11/04/2014 changed for skype claimant number display 
        public static async void LaunchSkype(string PhoneNumber = "")
        {
           
            string addressFormat = "skype:";
          
         //mbahl3 chaange made to display claimant phone number on skype trimmed extn  
            if (PhoneNumber.Contains("Ext"))
            {
                int index = PhoneNumber.IndexOf("Ext");
                string claimantNumber = PhoneNumber.Substring(0, index);
                PhoneNumber = claimantNumber;

            }
          //mini
            var uri = new Uri((addressFormat) + PhoneNumber);
            await Launcher.LaunchUriAsync(uri);
        }

        /// <summary>
        /// Launches Mail app
        /// </summary>
        public static async void LaunchMail(string emailID = "")
        {
            string addressFormat = string.Format("mailto:{0}", emailID);

            var uri = new Uri(addressFormat);
            await Launcher.LaunchUriAsync(uri);
        }

        /// <summary>
        /// Launches calendar appointment
        /// </summary>
        public static async void LaunchAppointment()
        {
            await AppointmentManager.ShowTimeFrameAsync(DateTime.Now, new TimeSpan(1, 0, 0));
        }

        public static async void LaunchPhoto(string imageName)
        {
            StorageFile image = await StorageFile.GetFileFromApplicationUriAsync(new Uri(imageName, UriKind.Absolute));
            await Launcher.LaunchFileAsync(image);
        }

        public static async Task LaunchBrowser(string uri)
        {
            LauncherOptions options = new LauncherOptions();
            options.DesiredRemainingView = Windows.UI.ViewManagement.ViewSizePreference.UseHalf;
            await Launcher.LaunchUriAsync(new Uri(uri));
        }
    }
}
