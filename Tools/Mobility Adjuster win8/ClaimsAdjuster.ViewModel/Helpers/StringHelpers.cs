﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;

namespace ClaimsAdjuster.ViewModel.Helpers
{
    public static class StringHelpers
    {
        public static  List<string> imageFileTypes = new List<string>() {"png","jpg","bmp","jpeg","hdp","wdp","tif","tiff","raw","gif"};
        public static  List<string> videoFileTypes = new List<string>() { "wm", "wmv",  "asf","m2ts",   "m2t","mov","qt","avi","wtv","dvr-ms","mp4", "mov","m4v","mpeg", "mpg", "mpe", "m1v", "mp2", "mpv2", "mod",   "vob","m1v","avi","mov"};
        public static  List<string> audioFileTypes = new List<string>() { "asx","wm","wma","wmx","wav","mp3","m3u","aac"};
        public static string SerializeCustom<T>(T value)
        {

            if (value == null)
            {
                return null;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = new UnicodeEncoding(false, false);
            settings.Indent = false;
            settings.OmitXmlDeclaration = true;
           

            using (Utf8StringWriter textWriter = new Utf8StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value);
                }
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// Deserialize string to T object
        /// </summary>
        public static T DeserializeCustom<T>(string result)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlReader reader = XmlReader.Create(new StringReader(result));
            T deserializeObject = (T)(serializer.Deserialize(reader));
            return deserializeObject;
        }

        public sealed class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding { get { return Encoding.Unicode; } }
        }

        public static DateTime GetParsedDateTime(string datePart,string timePart)
        {

           return DateTime.Parse(string.Format("{0} {1}", datePart, timePart), CultureInfo.InvariantCulture);

        }
        public static string GetFileType(string fileName)
        {
            string fileType=string.Empty;
            if(imageFileTypes.Contains(fileName.ToLower()))
                return FileType.BITMAP.ToString();
            else if (audioFileTypes.Contains(fileName.ToLower()))
                return FileType.AUDIO.ToString();
            else if (videoFileTypes.Contains(fileName.ToLower()))
                return FileType.VIDEO.ToString();
            return FileType.UNKNOWN.ToString();
        }

        public static PinType GetPinType(string param)
        {
           return  (PinType)System.Enum.Parse(typeof(PinType), param);
        }

        public static double ParseAsDouble(string strAmount)
        {
            try
            {
                if (!string.IsNullOrEmpty(strAmount))
                {

                    return Double.Parse(strAmount, 
                        NumberStyles.Number | NumberStyles.AllowCurrencySymbol|NumberStyles.AllowDecimalPoint|
                        NumberStyles.AllowParentheses);
                   
                }
            }
            catch( Exception ex)
            {
              
            }
            return 0.0;
        }

        public static Dictionary<string, string> ParseTileArgument()
        {
            string[] tileParams = MainViewModel.Instance.TileParams.Split('&');
            Dictionary<string, string> tile = new Dictionary<string, string>();
            foreach (string strParams in tileParams)
            {
                string[] values = strParams.Split('=');
                tile.Add(values[0], values[1]);
            }
            return tile;
        }
        
        /// <summary>
        /// Check for any invalid characters from Constants.InvalidCharacters
        /// </summary>
        public static bool CheckValidity(string content)
        {
            return content.Any(s => Constants.InvalidCharacters.Contains(s));
        }
    }
}
