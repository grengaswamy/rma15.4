﻿using ClaimsAdjuster.ViewModel.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class ClaimsRequest : ServiceRequestBaseEx
    {

        #region Constructors

        public ClaimsRequest()
        {
        }

        public ClaimsRequest(string id) :
            base(id)
        {
            Result = new ClaimCollection();
        }

        #endregion

        #region Properties

        public string EventNumber { get; set; }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
            var claimsVM = Result as ClaimCollection;
            List<Claim> claims = null;
            claimsVM.Collection.Clear();
            await claimsVM.InitializeFilterValues();
            if (!string.IsNullOrWhiteSpace(EventNumber))//Get's claim's with respective to a event
                claims = await MainViewModel.Instance.CscRepository.GetClaimsByEventAsync(EventNumber);
            else
                claims = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Claim>();
            await claimsVM.LoadData(claims);
        }

        #endregion

    }
}
