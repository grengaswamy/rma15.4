﻿using ClaimsAdjuster.ViewModel.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace ClaimsAdjuster.ViewModel
{
    public class PhotoPageRequest : ServiceRequestBaseEx
    {

         #region Constructors

        public PhotoPageRequest()
        {
        }

        public PhotoPageRequest(string id) :
            base(id)
        {
            Result = new MediaCollection();
        }

        #endregion

        #region Properties

        public string ParentId { get; set; }

        public ParentType ParentType { get; set; }

        public MediaCaptureType MediaCaptureType { get; set; }
        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
            var mediaCollection = (Result as MediaCollection);
            if (MediaCaptureType == MediaCaptureType.Photo)
                mediaCollection.Title = MainViewModel.Instance.GetStringResource("PhotosText");
            else if (MediaCaptureType == MediaCaptureType.Video)
                mediaCollection.Title = MainViewModel.Instance.GetStringResource("VideosText");
            else if (MediaCaptureType == MediaCaptureType.Audio)
                mediaCollection.Title = MainViewModel.Instance.GetStringResource("AudiosText");
            mediaCollection.Collection.Clear();
            mediaCollection.RaisePropertyChanged("Count");
            await mediaCollection.LoadData(this.ParentId, this.ParentType, this.MediaCaptureType);
        }

        #endregion
    }
}
