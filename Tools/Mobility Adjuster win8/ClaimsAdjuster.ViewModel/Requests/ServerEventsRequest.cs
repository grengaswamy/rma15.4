﻿using ClaimsAdjuster.ViewModel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class ServerEventsRequest : ServiceRequestBaseEx
    {
        #region Constructors

        public ServerEventsRequest()
        {
        }

        public ServerEventsRequest(string id) :
            base(id)
        {
            Result = new EventCollection();
        }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
            string eventsResult = string.Empty;
            if (MainViewModel.Instance.IsInternetAvailable)
            {
                string requestXml = string.Format(MobileCommonService.GetEventsList, MainViewModel.Instance.AuthenticationToken);
                eventsResult = await MobileCommonService.ProcessRequestAsync(requestXml);
            }
            await (Result as EventCollection).SetServerEvents(eventsResult);
        }

        #endregion

    }
}
