﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class NoteDetailsRequest : ServiceRequestBaseEx
    {
        #region Constructors

        public NoteDetailsRequest()
        {
        }

        public NoteDetailsRequest(string id) :
            base(id)
        {
        }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
            NoteViewModel noteVM = (Result as NoteViewModel);
            noteVM.DisplayName = MainViewModel.Instance.GetStringResource("NoteDetailsText");
            await noteVM.LoadData(noteVM.CurrentNote.ParentId);
        }

        #endregion
    }
}
