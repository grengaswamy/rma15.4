﻿
using ClaimsAdjuster.ViewModel.Repository;
using System.Threading.Tasks;
namespace ClaimsAdjuster.ViewModel
{
    public class EventDetailsRequest : ServiceRequestBaseEx
    {

        #region Constructors

        public EventDetailsRequest()
        {
        }

        public EventDetailsRequest(string id) :
            base(id)
        {
        }

        #endregion

        #region Methods

        protected override async System.Threading.Tasks.Task OnLoad()
        {
            CheckTile();
            (Result as EventViewModel).InitializeDetails();
            await base.OnLoad();
        }

        private void CheckTile()
        {
            EventViewModel datacontext = Result as EventViewModel;
            MainViewModel.Instance.TileExists(datacontext.CscEvent.EventNumber);
        }

        #endregion

    }
}
