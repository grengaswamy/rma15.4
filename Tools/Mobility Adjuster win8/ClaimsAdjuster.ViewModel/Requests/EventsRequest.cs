﻿using ClaimsAdjuster.ViewModel.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class EventsRequest : ServiceRequestBaseEx
    {

        #region Constructors

        public EventsRequest()
        {
        }

        public EventsRequest(string id) :
            base(id)
        {
            Result = new EventCollection();
        }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
            EventCollection eventCollection = (Result as EventCollection);
            eventCollection.InitializeFilterValues();
            List<Event> events = await MainViewModel.Instance.CscRepository.GetObjectsAsync<Event>();
            eventCollection.LoadData(events.OrderByDescending(s => s.EventDate).ToList());
        }

        #endregion

    }
}
