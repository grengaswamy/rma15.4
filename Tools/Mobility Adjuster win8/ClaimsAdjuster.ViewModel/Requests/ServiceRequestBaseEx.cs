﻿using IdentityMine.ViewModel;
using System;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class ServiceRequestBaseEx : ServiceRequestBase
    {
        private int maxResult;

        public int MaxResult
        {
            get
            {
                if (maxResult == 0)
                    maxResult = MainViewModel.Instance.MaxResult;
                return maxResult;
            }
            set
            {
                maxResult = value;
            }
        }

        public int ResultCount { get; set; }

        public int StartIndex { get; set; }

        public ServiceRequestBaseEx()
            : base()
        {
        }

        public ServiceRequestBaseEx(string id, bool clearResult = false)
            : base(id, id, false, TimeSpan.MinValue, clearResult)
        {
        }

        public ServiceRequestBaseEx(string title, string id, bool clearResult = false)
            : base(title, id, false, TimeSpan.MinValue, clearResult)
        {
        }

        public ServiceRequestBaseEx(string title, string id, bool isCacheEnabled, TimeSpan cacheLifetime, bool clearResult = false)
            : base(title, id, isCacheEnabled, cacheLifetime, clearResult)
        {
        }

        public override async Task ExecuteAsync()
        {
            MainViewModel.Instance.SetAppBusy(Constants.State_PageNavigation, true);
            this.DownloadFinished += ServiceRequestBaseEx_DownloadFinished;
            await base.ExecuteAsync();
        }

        private void ServiceRequestBaseEx_DownloadFinished(object sender, EventArgs e)
        {
            this.DownloadFinished -= ServiceRequestBaseEx_DownloadFinished;
            MainViewModel.Instance.SetAppBusy(Constants.State_PageNavigation, false);
        }

        protected override void PrepareRequestString()
        {
        }

    }
}
