﻿using ClaimsAdjuster.ViewModel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaimsAdjuster.ViewModel
{
    public class ServerTasksRequest : ServiceRequestBaseEx
    {
        #region Constructors

        public ServerTasksRequest()
        {
        }

        public ServerTasksRequest(string id) :
            base(id)
        {
            Result = new TaskCollection();
        }

        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
                string tasksResult = string.Empty;
                if (MainViewModel.Instance.IsInternetAvailable)
                {
                    string requestXml = string.Format(MobileCommonService.GetTaskRequest, MainViewModel.Instance.AuthenticationToken, MainViewModel.Instance.CurrentUserInfo.UserName, string.Empty, string.Empty);
                    tasksResult = await MobileCommonService.ProcessRequestAsync(requestXml);
                }
                await (Result as TaskCollection).SetServerTasks(tasksResult);
        }

        #endregion
    }
}
