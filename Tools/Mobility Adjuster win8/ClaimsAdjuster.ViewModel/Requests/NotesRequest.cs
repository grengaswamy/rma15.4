﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityMine.ViewModel;

namespace ClaimsAdjuster.ViewModel
{
    public class NotesRequest :ServiceRequestBaseEx
    {
        #region Constructors
        
        public NotesRequest()
        {

        }

        public NotesRequest(string id)
            :base(id)
        {
            Result = new NoteCollection();
        }

        #endregion

        #region Properties

        public string ClaimNumber { get; set; }
        public int ClaimViewId { get; set; } //mbahl3 Mits 36145
        #endregion

        #region Methods

        protected override async Task OnLoad()
        {
            await ExecuteRequest();
            await base.OnLoad();
        }

        private async Task ExecuteRequest()
        {
            var notesCollection = this.Result as NoteCollection;
            var notes = await MainViewModel.Instance.CscRepository.GetNotesByClaimAsync(ClaimNumber);

            notesCollection.InitializeVaules(notes.OrderBy(note => note.Heading).ToList(), ClaimViewId); //mbahl3 Mits 36145
        }

        #endregion
    }
}
