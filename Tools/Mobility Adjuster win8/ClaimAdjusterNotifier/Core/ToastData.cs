﻿using System.Xml.Serialization;

namespace ClaimAdjuster.Notifier
{

    [XmlRoot("ToastData")]
    public sealed class ToastData
    {
        public DueTask[] DueTasks { get; set; }

        [XmlAttribute("TotalDueTasks")]
        public int TotalDueTasks { get; set; }
    }

    [XmlRoot("DueTask")]
    public sealed class DueTask
    {
        [XmlAttribute("DueDate")]
        public string DueDate { get; set; }

        [XmlAttribute("TaskName")]
        public string TaskName { get; set; }

        [XmlAttribute("Type")]
        public string TaskTypeText { get; set; }
    }
}
