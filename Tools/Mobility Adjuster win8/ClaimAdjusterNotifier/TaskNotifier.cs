﻿using ClaimAdjuster.Notifier.TileContent;
using System;
using System.Globalization;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Storage;
using Windows.UI.Notifications;

namespace ClaimAdjuster.Notifier
{
    public sealed class TaskNotifier : IBackgroundTask
    {
        private const string ToastDataFilename = "ToastData.xml";
        private const string AppVisibilityKey = "IsAppVisibleForeground";
        private const int MediumAndWideFields = 4;
        private const int LargeFields = 3;
        private const int MaxQueue = 5;
        private ToastData lstTasks;

        private ToastData TasksList
        {
            get;
            set;

        }
        /// <summary>
        /// Toast template for due task in next 1 hour
        /// </summary>
        /// <remarks>
        /// Template: One string of bold text wrapped across the first two lines, one string of regular text on the third line.
        /// Parameter: {0} = time interval, {1} = Task Type, {2} = Due task name, {3} = 'x' upcoming task (in 24 hours)
        /// </remarks>
        private const string DueTaskToastTemplate = "<toast launch='toast_goto_tasks'><visual><binding template='ToastText03'><text id='1'>Due in {0} minutes\n{1} : {2}</text><text id='2'>{3} upcoming tasks</text></binding></visual></toast>";

        public void Run(IBackgroundTaskInstance taskInstance)
        {
            CheckDueTasks();
        }

        private async void CheckDueTasks()
        {
            try
            {
                var appForegroundValue = ApplicationData.Current.LocalSettings.Values[AppVisibilityKey];

                if (appForegroundValue != null && (bool)appForegroundValue) // If app is foreground, don't show notification.
                    return;

                StorageFile toastdataFile = await ApplicationData.Current.LocalFolder.GetFileAsync(ToastDataFilename);
                if (toastdataFile != null)
                {
                    string toastdataContent = await FileIO.ReadTextAsync(toastdataFile);

                    if (!string.IsNullOrEmpty(toastdataContent))
                    {
                        ToastData toastdata = StringHelpers.DeserializeCustom(toastdataContent);
                      
                        // Prepare toast notification template
                        int duetaskCount = 0;
                        if (toastdata != null && toastdata.DueTasks != null && (duetaskCount = toastdata.DueTasks.Length) > 0)
                        {
                            TasksList = toastdata;
                            // Check for due task in next 1 hour
                            DueTask dueTask = toastdata.DueTasks[0];
                            DateTime duedate = DateTime.Parse(dueTask.DueDate, CultureInfo.InvariantCulture);
                            int remainingMinutes = (int)(duedate - DateTime.Now).TotalMinutes;

                            if (remainingMinutes > 0 && remainingMinutes < 60)
                            {
                                string toastTemplate = string.Format(DueTaskToastTemplate, remainingMinutes, dueTask.TaskTypeText, dueTask.TaskName, toastdata.TotalDueTasks);
                                XmlDocument toastDOM = new XmlDocument();
                                toastDOM.LoadXml(toastTemplate);

                                ToastNotification toast = new ToastNotification(toastDOM);
                                ToastNotificationManager.CreateToastNotifier().Show(toast);
                            }
                            UpdateAllTiles(toastdata);
                        }

                    }
                }
            }
            catch (Exception exp)
            {

            }
        }

        void taskInstance_Canceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {

        }
        private static void UpdateAllTiles(ToastData tasks)
        {
            TileUpdateManager.CreateTileUpdaterForApplication().Clear();
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.EnableNotificationQueue(true);
            updater.Clear();
           
            
            //For every 3 large tiles create a new medium/wide tile
           
            for (int i = 0; i < tasks.DueTasks.Length; i = i + 3)
            {
                if (i > 5)
                    break;
                ITileSquare310x310TextList01 largeTile = TileContentFactory.CreateTileSquare310x310TextList01();
                ITileWide310x150Text03 wideTile = TileContentFactory.CreateTileWide310x150Text03();
                ITileSquare150x150Text04 mediumTile = TileContentFactory.CreateTileSquare150x150Text04();
                bool second =false , third=false;
                for (int j = 0; (j+i) < tasks.DueTasks.Length && j<3; j++)
                {
                    if (j % 3 == 0)
                    {
                        largeTile.TextHeading1.Text = tasks.DueTasks[j + i].TaskName;
                        largeTile.TextBodyGroup1Field1.Text = "Task type : " + tasks.DueTasks[j + i].TaskTypeText;
                        largeTile.TextBodyGroup1Field2.Text = tasks.DueTasks[j + i].DueDate;
                        wideTile.TextHeadingWrap.Text = largeTile.TextHeading1.Text + " - " + largeTile.TextBodyGroup1Field2.Text;
                        mediumTile.TextBodyWrap.Text = wideTile.TextHeadingWrap.Text;
                        wideTile.Square150x150Content = mediumTile;
                        largeTile.Wide310x150Content = wideTile;


                    }
                    else if (j % 3 == 1)
                    {
                        largeTile.TextHeading2.Text = tasks.DueTasks[j + i].TaskName;
                        largeTile.TextBodyGroup2Field1.Text = "Task type : " + tasks.DueTasks[j + i].TaskTypeText;
                        largeTile.TextBodyGroup2Field2.Text = tasks.DueTasks[j + i].DueDate;
                        second =true;

                    }
                    else if (j % 3 == 2)
                    {
                        largeTile.TextHeading3.Text = tasks.DueTasks[j + i].TaskName;
                        largeTile.TextBodyGroup3Field1.Text = "Task type : " + tasks.DueTasks[j + i].TaskTypeText;
                        largeTile.TextBodyGroup3Field2.Text = tasks.DueTasks[j + i].DueDate;
                        third =true;
                    }

                }

                largeTile.ContentId = "Main" + i.ToString();
                TileNotification tileNotification = largeTile.CreateNotification();
                tileNotification.Tag = i.ToString();
                updater.Update(tileNotification);
                if(second)
                {
                    ITileWide310x150Text03 wideTile2 = TileContentFactory.CreateTileWide310x150Text03();
                    ITileSquare150x150Text04 mediumTile2 = TileContentFactory.CreateTileSquare150x150Text04();

                    wideTile2.TextHeadingWrap.Text = largeTile.TextHeading2.Text + " - " + largeTile.TextBodyGroup2Field2.Text;

                    mediumTile2.TextBodyWrap.Text = wideTile2.TextHeadingWrap.Text;

                    wideTile2.Square150x150Content = mediumTile2;
                    largeTile.Wide310x150Content = wideTile2;


                    TileNotification notification2 = largeTile.CreateNotification();
                    notification2.Tag = "Cycle 2" + i.ToString();
                    notification2.ExpirationTime = DateTimeOffset.MaxValue;

                    updater.Update(notification2);
                }
                if(third)
                {
                  
                    ITileWide310x150Text03 wideTile3 = TileContentFactory.CreateTileWide310x150Text03();
                    ITileSquare150x150Text04 mediumTile3 = TileContentFactory.CreateTileSquare150x150Text04();


                    wideTile3.TextHeadingWrap.Text = largeTile.TextHeading3.Text + " - " + largeTile.TextBodyGroup3Field2.Text;
                    mediumTile3.TextBodyWrap.Text = wideTile3.TextHeadingWrap.Text;

                    wideTile3.Square150x150Content = mediumTile3;
                    largeTile.Wide310x150Content = wideTile3;

                    TileNotification notification3 = largeTile.CreateNotification();
                    notification3.Tag = "Cycle 3" + i.ToString();
                    notification3.ExpirationTime = DateTimeOffset.MaxValue;

                    updater.Update(notification3);
                }

            }
        }


        private static ITileSquare310x310TextList01 CreateLargeTile(int start)
        {
            ITileSquare310x310TextList01 largeTile = TileContentFactory.CreateTileSquare310x310TextList01();
            largeTile.Wide310x150Content = CreateWideTile(start);

            return largeTile;


        }

        private static ITileWide310x150BlockAndText01 CreateWideTile(int start)
        {
            ITileWide310x150BlockAndText01 wideTile = TileContentFactory.CreateTileWide310x150BlockAndText01();
            wideTile.TextBody1.Text = "Wide";
            wideTile.Square150x150Content = CreateMediumTile(start);
            return wideTile;


        }

        private static ITileSquare150x150Text03 CreateMediumTile(int start)
        {
            ITileSquare150x150Text03 mediumTile = TileContentFactory.CreateTileSquare150x150Text03();
            mediumTile.TextBody1.Text = "Medium";
            return mediumTile;
        }
    }
}
