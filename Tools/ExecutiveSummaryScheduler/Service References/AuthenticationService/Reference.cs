﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.235
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Riskmaster.Tools.ExecutiveSummaryScheduler.AuthenticationService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="AuthenticationService.IAuthenticationService")]
    public interface IAuthenticationService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/AuthenticateUser", ReplyAction="http://tempuri.org/IAuthenticationService/AuthenticateUserResponse")]
        bool AuthenticateUser(out string strMessage, string strUserName, string strPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/AuthenticateSMSUser", ReplyAction="http://tempuri.org/IAuthenticationService/AuthenticateSMSUserResponse")]
        bool AuthenticateSMSUser(out string strMessage, string strUserName, string strPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/SingleSignOnUser", ReplyAction="http://tempuri.org/IAuthenticationService/SingleSignOnUserResponse")]
        bool SingleSignOnUser(string strUserName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/ChangePassword", ReplyAction="http://tempuri.org/IAuthenticationService/ChangePasswordResponse")]
        bool ChangePassword(string p_sLoginName, string p_sOldPassword, string p_sNewPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/GetAuthenticationProviders", ReplyAction="http://tempuri.org/IAuthenticationService/GetAuthenticationProvidersResponse")]
        System.Collections.Generic.Dictionary<string, string> GetAuthenticationProviders();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/GetUserDSNs", ReplyAction="http://tempuri.org/IAuthenticationService/GetUserDSNsResponse")]
        System.Collections.Generic.Dictionary<string, string> GetUserDSNs(string strUserName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/GetUserViews", ReplyAction="http://tempuri.org/IAuthenticationService/GetUserViewsResponse")]
        System.Collections.Generic.Dictionary<string, string> GetUserViews(string strUserName, string strDatabaseName, string strSelectedDsnId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/GetCustomViews", ReplyAction="http://tempuri.org/IAuthenticationService/GetCustomViewsResponse")]
        string GetCustomViews(string strSelectedDsnId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/GetUserSessionID", ReplyAction="http://tempuri.org/IAuthenticationService/GetUserSessionIDResponse")]
        string GetUserSessionID(string strUserName, string strDSNName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAuthenticationService/LogOutUser", ReplyAction="http://tempuri.org/IAuthenticationService/LogOutUserResponse")]
        void LogOutUser(string strSessionID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IAuthenticationServiceChannel : ExecutiveSummaryScheduler.AuthenticationService.IAuthenticationService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AuthenticationServiceClient : System.ServiceModel.ClientBase<ExecutiveSummaryScheduler.AuthenticationService.IAuthenticationService>, ExecutiveSummaryScheduler.AuthenticationService.IAuthenticationService {
        
        public AuthenticationServiceClient() {
        }
        
        public AuthenticationServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public AuthenticationServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AuthenticationServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AuthenticationServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool AuthenticateUser(out string strMessage, string strUserName, string strPassword) {
            return base.Channel.AuthenticateUser(out strMessage, strUserName, strPassword);
        }
        
        public bool AuthenticateSMSUser(out string strMessage, string strUserName, string strPassword) {
            return base.Channel.AuthenticateSMSUser(out strMessage, strUserName, strPassword);
        }
        
        public bool SingleSignOnUser(string strUserName) {
            return base.Channel.SingleSignOnUser(strUserName);
        }
        
        public bool ChangePassword(string p_sLoginName, string p_sOldPassword, string p_sNewPassword) {
            return base.Channel.ChangePassword(p_sLoginName, p_sOldPassword, p_sNewPassword);
        }
        
        public System.Collections.Generic.Dictionary<string, string> GetAuthenticationProviders() {
            return base.Channel.GetAuthenticationProviders();
        }
        
        public System.Collections.Generic.Dictionary<string, string> GetUserDSNs(string strUserName) {
            return base.Channel.GetUserDSNs(strUserName);
        }
        
        public System.Collections.Generic.Dictionary<string, string> GetUserViews(string strUserName, string strDatabaseName, string strSelectedDsnId) {
            return base.Channel.GetUserViews(strUserName, strDatabaseName, strSelectedDsnId);
        }
        
        public string GetCustomViews(string strSelectedDsnId) {
            return base.Channel.GetCustomViews(strSelectedDsnId);
        }
        
        public string GetUserSessionID(string strUserName, string strDSNName) {
            return base.Channel.GetUserSessionID(strUserName, strDSNName);
        }
        
        public void LogOutUser(string strSessionID) {
            base.Channel.LogOutUser(strSessionID);
        }
    }
}
