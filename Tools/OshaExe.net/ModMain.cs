using System;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.FileStorage;
namespace Osha
{
    public static class ModMain
    {
        public static Int64 g_ZIPFileSize = 0;
        public static string SMTPServer = String.Empty;
        public static string RptFolder = String.Empty;
        public static string DSN = String.Empty;
        public static string MainDSN = String.Empty;
        public static bool IsReportProcessed = false;
        public const string DTG_REG_SUBKEY = @"Software\DTG\SORTMASTER Server";
        private const string DIR_SEPERATOR_CHAR = @"\";
        
        public static void Main(string[] args)
        {
            Console.WriteLine("execution started ->main");
            CWorker objWorker;
            string s = String.Empty;
            int iPtr = 0;
            string sTemp = String.Empty;
            bool blnSuccess = false;//dvatsa-cloud
            int m_iClientId = 0;//dvatsa-cloud
            try
            {

            // args = new string[5];//dvatsa-cloud
            // args[0] = "relay.csc.com|^|";
           //args[1] = "D:\\Main RMX Source\\RiskmasterCDRCloudSprint\\UI\\riskmaster\\userdata|^|";
            //  args[2] = "Driver={SQL Server};Server=20.198.58.89;Database=rmACloudTMNew;UID=sa;PWD=csc123@CSC;|^|";//DSN
             // args[3] = "Driver={SQL Server};Server=20.198.58.89;Database=rmACloudRM;UID=sa;PWD=csc123@CSC;|^|";//Main DSN
              // args[4] = "1|^|";

                //Console.WriteLine("args : " + args[4].ToString());
                objWorker = new CWorker();
                string sArgs = string.Empty;
                if (args.Length > 0)
                {

                    for (int i = 0; i < args.Length; i++)
                    {
                        sArgs += args[i] + " ";
                    }
                    //ErrorLog.LogError("", 0, 0, "", "Args :" + sArgs);

                    //sTemp = args[0].Trim();
                    sTemp = sArgs;

                    // get SMTP Server
                    iPtr = sTemp.IndexOf("|^|");
                    SMTPServer = (sTemp.Substring(0, iPtr)).Trim();
                    sTemp = (sTemp.Substring(iPtr + 3)).Trim();
                    //ErrorLog.LogError("", 0, 0, "", "SMTPServer :" + SMTPServer);
                    // get Report folder
                    iPtr = sTemp.IndexOf("|^|");
                    RptFolder = (sTemp.Substring(0, iPtr)).Trim();
                    sTemp = (sTemp.Substring(iPtr + 3)).Trim();
                    //ErrorLog.LogError("", 0, 0, "", "RptFolder :" + RptFolder);
                    // get TaskManager DSN
                    iPtr = sTemp.IndexOf("|^|");
                    DSN = (sTemp.Substring(0, iPtr)).Trim();
                    sTemp = (sTemp.Substring(iPtr + 3)).Trim();
                    //ErrorLog.LogError("", 0, 0, "", "DSN :" + DSN);
                    // get Main DSN
                    iPtr = sTemp.IndexOf("|^|");
                    MainDSN = (sTemp.Substring(0, iPtr)).Trim();
                    sTemp = (sTemp.Substring(iPtr + 3)).Trim();
                    //ErrorLog.LogError("", 0, 0, "", "MainDSN :" + MainDSN);

                    //For ClientId Confirmation---Otherwise it will work with 0-ClientId as per base function.
                    iPtr = sTemp.IndexOf("|^|");
                    if (iPtr > 0)
                    {
                        m_iClientId = Conversion.CastToType<int>((sTemp.Substring(0, iPtr)).Trim(), out blnSuccess);
                        sTemp = (sTemp.Substring(iPtr + 3)).Trim();
                    }
                    //Console.WriteLine("SMTPServer :" + SMTPServer + "-" + "RptFolder :" + RptFolder + "-" + "DSN :" + DSN + "-" + "MainDSN :" + MainDSN);
                    objWorker.SMTPServer = SMTPServer;
                    objWorker.ReportFolder = RptFolder;
                    objWorker.DSN = DSN;
                    objWorker.ClientId = m_iClientId;//dvatsa-cloud


                    g_ZIPFileSize = 1073741;
                    s = RMConfigurationManager.GetAppSetting("ZipFileSize");

                    if (s != "" && Conversion.IsNumeric(s))
                    {
                        g_ZIPFileSize = Convert.ToInt64(s);
                    }
                    objWorker.Init();
                    Console.WriteLine("objWorker.Init(); executed->main");
                    IsReportProcessed = false;
                    do
                    {
                        Thread.Sleep(1000);
                        //Console.WriteLine("objWorker.Main(); After Sleep");
                        objWorker.ScheduleReports();
                        Console.WriteLine("objWorker.Main(); executed->After Schedule");
                        objWorker.LookForWork();
                        Console.WriteLine("objWorker.Main(); executed->After Lookforwork");
                        Thread.Sleep(500);

                        if (IsReportProcessed)
                        {
                            break;
                        }

                    } while (objWorker.__Continue);

                }
                objWorker.Shutdown();
                objWorker = null;
            }
            catch (Exception exc)
            {
                ErrorLog.LogError(exc,m_iClientId);//dvatsa-cloud
            }
        }

        public static string GetMachineName(int p_iClientId)
        {
            string sGetMachineName = String.Empty;
            try
            {
                sGetMachineName = System.Environment.MachineName;
            }
            catch (Exception ex)
            {
                ErrorLog.LogError(ex, p_iClientId);//dvatsa-cloud
            }
            return sGetMachineName;
        }

        public static string GetZipFileName(string sFileName)
        {
            string GetZipFileName = String.Empty;
            int i = 0;
            if (sFileName == "")
            {
                return GetZipFileName;
            }


            i = sFileName.LastIndexOf(".");
			//rkulavil : RMA-16623 starts
            //if (i == 0)
            //{
            //    GetZipFileName = sFileName + ".zip";
            //}
            //else
            //{
            //    GetZipFileName = sFileName.Substring(0, i) + "zip";
            //}
            GetZipFileName = string.Format("{0}.zip", i == -1 ? sFileName : sFileName.Substring(0, i));
			//rkulavil : RMA-16623 ends

            return GetZipFileName;
        }

        public static Int32 lGetNextUID(string sTableName, string dbConnection)
        {
            Int32 iGetNextUID = 0;
            DbReader objReader = null;
            string sSQL = String.Empty;
            Int32 lNextUID = 0;
            Int32 lOrigUID = 0;
            Int32 lRows = 0;
            Int32 lCollisionRetryCount = 0;
            Int32 lErrRetryCount = 0;
            string sSaveError = String.Empty;
            bool bSaveError = false;
            try
            {
                do
                {
                    objReader = DbFactory.GetDbReader(dbConnection, "SELECT NEXT_ID FROM TM_REP_IDS WHERE TABLE_NAME = '" + sTableName + "'");
                    if (objReader.Read())
                    {
                        lNextUID = objReader.GetInt32(0);
                    }
                    else
                    {
                        throw new RMAppException("Specified table does not exist in glossary.");
                    }

                    // Compute next id
                    lOrigUID = lNextUID;

                    if (Convert.ToBoolean(lOrigUID))
                    {
                        lNextUID = lNextUID + 1;
                    }
                    else
                    {
                        lNextUID = 2;
                    }

                    // try to reserve id (searched update)
                    sSQL = "UPDATE TM_REP_IDS SET NEXT_ID = " + lNextUID + " WHERE TABLE_NAME = '" + sTableName + "'";

                    // only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
                    if (Convert.ToBoolean(lOrigUID))
                    {
                        sSQL = sSQL + " AND NEXT_ID = " + lOrigUID;
                    }

                    // Try update
                    try
                    {
                        lRows = DbFactory.ExecuteNonQuery(dbConnection, sSQL);
                    }
                    catch (Exception ex)
                    {
                        sSaveError = ex.Message;
                        lErrRetryCount = lErrRetryCount + 1;
                        if (lErrRetryCount >= 5)
                        {
                            bSaveError = true;
                            throw new RMAppException(sSaveError);

                        }
                    }

                    if (bSaveError)
                    {
                        bSaveError = false;
                    }
                    else
                    {
                        // if success, return
                        if (lRows == 1)
                        {
                            iGetNextUID = Convert.ToInt32(lNextUID - 1);
                            return iGetNextUID; // success
                        }
                        else
                        {
                            lCollisionRetryCount = lCollisionRetryCount + 1; // collided with another user - try again (up to 1000 times)
                        }
                    }

                } while ((lErrRetryCount < 5) && (lCollisionRetryCount < 1000));

                if (lCollisionRetryCount >= 1000)
                {
                    throw new RMAppException("Collision timeout. Server load too high. Please wait and try again.");
                }

                iGetNextUID = 0;
                return iGetNextUID;
            }
            catch (Exception ex)
            {
                throw new RMAppException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        public static Int32 lTMGetNextUID(string sTableName, string dbConnection)
        {
            Int32 iTMGetNextUID = 0;
            DbReader objReader = null;
            string sSQL = String.Empty;
            Int32 lNextUID = 0;
            Int32 lOrigUID = 0;
            Int32 lRows = 0;
            Int32 lCollisionRetryCount = 0;
            Int32 lErrRetryCount = 0;
            string sSaveError = String.Empty;
            bool bSaveError = false;

            try
            {
                do
                {
                    objReader = DbFactory.GetDbReader(dbConnection, "SELECT NEXT_ID FROM TM_IDS WHERE TABLE_NAME = '" + sTableName + "'");
                    if (objReader.Read())
                    {
                        lNextUID = objReader.GetInt32(0);
                    }
                    else
                    {
                        throw new RMAppException("Specified table does not exist in glossary.");
                    }

                    // Compute next id
                    lOrigUID = lNextUID;
                    if (Convert.ToBoolean(lOrigUID))
                        lNextUID = lNextUID + 1;

                    else
                        lNextUID = 2;

                    // try to reserve id (searched update)
                    sSQL = "UPDATE TM_IDS SET NEXT_ID = " + lNextUID + " WHERE TABLE_NAME = '" + sTableName + "'";

                    // only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
                    if (Convert.ToBoolean(lOrigUID))
                        sSQL = sSQL + " AND NEXT_ID = " + lOrigUID;


                    // Try update
                    try
                    {
                        lRows = DbFactory.ExecuteNonQuery(dbConnection, sSQL);
                    }
                    catch (Exception ex)
                    {
                        sSaveError = ex.Message;
                        lErrRetryCount = lErrRetryCount + 1;
                        if (lErrRetryCount >= 5)
                        {
                            bSaveError = true;
                            throw new RMAppException(sSaveError);
                        }
                    }

                    if (bSaveError)
                        bSaveError = false;

                    else
                    {
                        // if success, return
                        if (lRows == 1)
                        {
                            iTMGetNextUID = lNextUID - 1;
                            return iTMGetNextUID; // success
                        }
                        else
                            lCollisionRetryCount = lCollisionRetryCount + 1; // collided with another user - try again (up to 1000 times)
                    }
                } while ((lErrRetryCount < 5) && (lCollisionRetryCount < 1000));

                if (lCollisionRetryCount >= 1000)
                    throw new RMAppException("Collision timeout. Server load too high. Please wait and try again.");
                iTMGetNextUID = 0;
                return iTMGetNextUID;
            }
            catch (Exception ex)
            {
                throw new RMAppException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        public static string ApplyMirror(string sMirror, string sDSN)
        {
            string sTmp = String.Empty;
            sTmp = sDSN.Substring(sDSN.ToUpper().IndexOf("SERVER=") + 7);
            sTmp = sTmp.Substring(0, sTmp.IndexOf(";"));
            return sDSN.Replace(sTmp, sMirror);
        }

        public static string getUniqueFileName(string sBasedOn, string sFolder, bool bCreateFile = false)
        {
            string sgetUniqueFileName = String.Empty;
            Int32 l = 0;
            string s = String.Empty;
            string sDir = String.Empty;
            string sExtension = String.Empty;
            string sName = String.Empty;
            Console.WriteLine("Entered getUniqueFileName");
            if (sBasedOn != "")
            {
                // Separate file name and path
                l = sBasedOn.LastIndexOf(DIR_SEPERATOR_CHAR);
                if (l > 0)
                    sName = sBasedOn.Substring((int)(l + 1));
                else
                    sName = sBasedOn;

                // Remove extension if any
                l = sName.LastIndexOf(".");
                if (l > 0)
                {
                    sExtension = sName.Substring((int)(l));
                    sName = sName.Substring(0, (int)(l));
                }

                if (sName == "")
                    sName = "doc";

                sDir = sFolder;
                if (!sDir.EndsWith(DIR_SEPERATOR_CHAR))
                    sDir = sDir + DIR_SEPERATOR_CHAR;

                if (!File.Exists(sDir + sName + sExtension))
                {
                    sgetUniqueFileName = sDir + sName + sExtension;
                    return sgetUniqueFileName;
                }
                else
                {
                    // Try to get file name nicely
                    l = 1;
                    do
                    {
                        s = sDir + sName + "-" + l + sExtension;
                        l = l + 1;
                    } while (File.Exists(s) && l < 2000);
                    if (l < 2000)
                    {
                        sgetUniqueFileName = s;
                        return sgetUniqueFileName;
                    }
                }
            }
            sgetUniqueFileName = "smo" + Path.GetTempFileName();
            if (!bCreateFile)
                File.Delete(sgetUniqueFileName);

            Console.WriteLine("Successfully exited getUniqueFileName");
            return sgetUniqueFileName;
        }

        // Note: Return 1 for Alpha, 2 for Numeric, 3 for acceptable symbol, 0 only for BAD filename characters.
        public static int IsAlphaNum(int intChrCode)
        {
            //rkulavil:RMA-15957 starts
            //for (int i = 35; i <= 122; i++)
            //{
            //    if ((i >= 65 && i <= 90) || (i >= 97 && i <= 122))
            //        return 1;
            //    if (i >= 48 && i <= 57)
            //        return 2;
            //    if (i == 36 || i == 95)
            //        return 3;

            //}
            //return 0;
            if ((intChrCode >= 65 && intChrCode <= 90) || (intChrCode >= 97 && intChrCode <= 122))
                return 1;
            else if (intChrCode >= 48 && intChrCode <= 57)
                return 2;
            else if (intChrCode == 36 || intChrCode == 95)
                return 3;
            else
                return 0;
            //rkulavil:RMA-15957 ends
        }
                
    }

}
