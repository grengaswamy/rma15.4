﻿using System;

namespace RMXdBUpgradeWizard
{
    public class RiskmasterDBTypes
    {
        #region declare constant variables
        public const int DBMS_IS_ACCESS = 0;
        public const int DBMS_IS_DB2 = 6;
        public const int DBMS_IS_INFORMIX = 3;
        public const int DBMS_IS_ODBC = 5;
        public const int DBMS_IS_ORACLE = 4;
        public const int DBMS_IS_SQLSRVR = 1;
        public const int DBMS_IS_SYBASE = 2;

        public const int ODBC_BIT = -7;
        public const int ODBC_DOUBLE = 8;
        public const int ODBC_INTEGER = -15;
        public const int ODBC_LONG = -16;
        public const int ODBC_SINGLE = 7;
        public const int ODBC_MEMO = -1;
        public const int ODBC_TEXT = 1;
        #endregion

        /// <summary>
        /// ConvertDBParameters
        /// </summary>
        /// <param name="idBMake"></param>
        /// <returns></returns>
        public static string ConvertDBParameters(int idBMake)
        {
            string strParm = String.Empty;

            switch (idBMake)
            {
                case RiskmasterDBTypes.DBMS_IS_SQLSRVR:
                    {
                        strParm = "@";
                        break;
                    }
                case RiskmasterDBTypes.DBMS_IS_ORACLE:
                    {
                        strParm = ":";
                        break;
                    }
            }

            return strParm;
        }
    }
}
