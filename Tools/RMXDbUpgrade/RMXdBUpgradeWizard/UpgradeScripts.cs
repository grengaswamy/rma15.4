﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Data;
using Microsoft.Win32;
using Riskmaster.Security.Encryption;
using Riskmaster.Db;

namespace RMXdBUpgradeWizard
{
    class UpgradeScripts
    {
        #region variables
        public static bool _isSilent;
        const string SQL_SERVER_FILTER = "SQL Server";
        const string SQL_NATIVE_CLIENT_FILTER = "SQL Native Client";
        const string SQL_SERVER_NATIVE_CLIENT_FILTER = "SQL Server Native Client";
        const string ORACLE_FILTER = "Oracle in";
        private static int m_iClientId = 0;
        #endregion

        /// <summary>
        /// returns a string array returning the names of the SQL Script files
        /// </summary>
        /// <param name="strSQLScriptFiles"></param>
        /// <param name="strDelimiter"></param>
        /// <returns></returns>
        public static string[] GetSQLScriptFiles(string strSQLScriptFiles, string strDelimiter)
        {
            List<string> arrListSQLScriptFiles = new List<string>();

            if ((!String.IsNullOrEmpty(strSQLScriptFiles)) && (strSQLScriptFiles.Length > 0))
            {
                return strSQLScriptFiles.Split(strDelimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            }

            return arrListSQLScriptFiles.ToArray();
        }

        /// <summary>
        /// Gets a list of the ODBC Drivers on the system for SQL Server and Oracle
        /// </summary>
        /// <returns>string array containing all of the currently installed SQL Server and Oracle drivers</returns>
        public static string[] GetODBCDrivers()
        {
            List<string> arrFilteredDrivers = new List<string>();
            const string ODBC_DRIVERS = @"SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers";

            RegistryKey regODBCKey = Registry.LocalMachine.OpenSubKey(ODBC_DRIVERS);
            string[] arrODBCDrivers = regODBCKey.GetValueNames();

            foreach (string strODBCDriver in arrODBCDrivers)
            {
                if (strODBCDriver.Contains(SQL_SERVER_FILTER) || strODBCDriver.Contains(SQL_NATIVE_CLIENT_FILTER) 
                    || strODBCDriver.Contains(SQL_SERVER_NATIVE_CLIENT_FILTER) || strODBCDriver.Contains(ORACLE_FILTER))
                {
                    arrFilteredDrivers.Add(strODBCDriver);
                } // if
            }//foreach

            return arrFilteredDrivers.ToArray();
        }
        
        #region  OshoMigration // Added by abhinav - Mits-33063

        public static void OshoMigration()  // try
        {
           
            String strTaskManagerSource = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMTaskManager);

            String strSortMaster = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSortmaster);
            int iOverall = 0;
            const string STRING_DELIMITER = ",";

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }

            string[] arrSQLFiles = null;


            //get the list of SQL files for the RISKMASTER security database
            arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.OSHOMIGSCRIPTS, STRING_DELIMITER);

            //gagnihotri Audit changes MITS 17425, existing SQL triggers are to be modified.
            //Triggers are need to be modified prior to db-upgrade.
            //In future script could be changed to create SQL audit triggers also
            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_SecConnectString);

            if (DisplayDBUpgrade.g_dbMake == 5)
            {
                throw new ApplicationException("DSN connectivity is not allowed for the Security DB.<br /><br />Please change the 'RMXSecurity' connectionString to a MSSQL or Oracle provider.");
            }

            if (DisplayDBUpgrade.g_dbMake == 1 || DisplayDBUpgrade.g_dbMake == 4)// DBMS_IS_SQLSRVR  = 1
            {
                string[] sourceArrSQLFile = arrSQLFiles;
            }

            if (arrSQLFiles.Length > 0)
            {
                if (!_isSilent)
                {
                    frmWizard.UpdateDBName("TASK MANAGER");
                    frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                }

                foreach (var strSQLFile in arrSQLFiles)
                {
                    //display name of script on the update form
                    frmWizard.UpdateScriptName(strSQLFile);
                    //MJP - get the security database make
                    // JIRA 1466 Starts
                    int tempResult = UpdateFunctions.result;  
                    UpdateFunctions.result = 1;
                    UpdateFunctions.ExecuteSQLScriptExtended(strTaskManagerSource, strSQLFile, String.Empty, _isSilent);
                    // JIRA 1446 Ends

                    if (UpdateFunctions.result == 1)
                    {
                        MigrateOshoData(strTaskManagerSource, strSortMaster);
                    }
                    // JIRA 1446 Starts
                    UpdateFunctions.result = tempResult;
                    // JIRA 1466 Ends
                    if (!_isSilent)
                    {
                        frmWizard.UpdateOverallProgressBar(iOverall);
                    }
                }
            }
        }

        #endregion

        #region  MigrateOshoData // Added by abhinav - Mits-33063
        private static void MigrateOshoData(String strTaskManagerSource, String strSortMaster)
        {
            #region INSERTION CODE FOR SQL SERVER or ORACLE

            if (DisplayDBUpgrade.g_dbMake == 4 || DisplayDBUpgrade.g_dbMake == 1)
            {
                #region For Oracle & Sql
                StringBuilder sSql = new StringBuilder();
                DbReader myReader = null;
                DbWriter objDbWriter = null;

                               
                    sSql.Append(string.Format(@"SELECT * FROM SM_IDS"));                    //first table
                    myReader = DbFactory.ExecuteReader(strSortMaster, sSql.ToString());
                    while (myReader.Read())
                    {
                        //logic to process data for the first result.
                        objDbWriter = DbFactory.GetDbWriter(strTaskManagerSource);
                        objDbWriter.Tables.Add("TM_REP_IDS");
                        objDbWriter.Fields.Add("TABLE_NAME", myReader["TABLE_NAME"]);
                        objDbWriter.Fields.Add("NEXT_ID", myReader["NEXT_ID"]);
                        objDbWriter.Execute();
                        objDbWriter = null;
                    }
                                                
                sSql.Clear();
                myReader = null;


                sSql.Append(string.Format(@"SELECT * FROM SM_JOB_LOG"));                // Second Table
                myReader = DbFactory.ExecuteReader(strSortMaster, sSql.ToString());
                while (myReader.Read())
                {
                    //logic to process data for the Second table.

                    objDbWriter = DbFactory.GetDbWriter(strTaskManagerSource);
                    objDbWriter.Tables.Add("TM_REP_JOB_LOG");
                    objDbWriter.Fields.Add("LOG_ID", myReader["LOG_ID"]);
                    objDbWriter.Fields.Add("JOB_ID", myReader["JOB_ID"]);
                    objDbWriter.Fields.Add("MSG_TYPE", myReader["MSG_TYPE"]);
                    objDbWriter.Fields.Add("MSG", myReader["MSG"]);
                    objDbWriter.Fields.Add("DTTM_LOGGED", myReader["DTTM_LOGGED"]);

                    objDbWriter.Execute();
                    objDbWriter = null;
                }

                sSql.Clear();
                myReader = null;


                sSql.Append(string.Format(@"SELECT * FROM SM_JOBS"));                // Third Table
                myReader = DbFactory.ExecuteReader(strSortMaster, sSql.ToString());
                while (myReader.Read())
                {
                    //logic to process data for the third table.

                    objDbWriter = DbFactory.GetDbWriter(strTaskManagerSource);
                    objDbWriter.Tables.Add("TM_REP_JOBS");
                    objDbWriter.Fields.Add("JOB_ID", myReader["JOB_ID"]);
                    objDbWriter.Fields.Add("JOB_PRIORITY", myReader["JOB_PRIORITY"]);
                    objDbWriter.Fields.Add("JOB_NAME", myReader["JOB_NAME"]);
                    objDbWriter.Fields.Add("JOB_DESC", myReader["JOB_DESC"]);
                    objDbWriter.Fields.Add("DSN", myReader["DSN"]);
                    objDbWriter.Fields.Add("OUTPUT_TYPE", myReader["OUTPUT_TYPE"]);
                    objDbWriter.Fields.Add("OUTPUT_PATH", myReader["OUTPUT_PATH"]);
                    objDbWriter.Fields.Add("OUTPUT_PATH_URL", myReader["OUTPUT_PATH_URL"]);
                    objDbWriter.Fields.Add("OUTPUT_OPTIONS", myReader["OUTPUT_OPTIONS"]);
                    objDbWriter.Fields.Add("REPORT_XML", myReader["REPORT_XML"]);
                    objDbWriter.Fields.Add("START_DTTM", myReader["START_DTTM"]);
                    objDbWriter.Fields.Add("NOTIFICATION_TYPE", myReader["NOTIFICATION_TYPE"]);
                    objDbWriter.Fields.Add("NOTIFY_EMAIL", myReader["NOTIFY_EMAIL"]);
                    objDbWriter.Fields.Add("NOTIFY_MSG", myReader["NOTIFY_MSG"]);
                    objDbWriter.Fields.Add("ASSIGNED", myReader["ASSIGNED"]);
                    objDbWriter.Fields.Add("ASSIGNED_TO", myReader["ASSIGNED_TO"]);
                    objDbWriter.Fields.Add("ASSIGNED_DTTM", myReader["ASSIGNED_DTTM"]);
                    objDbWriter.Fields.Add("COMPLETE", myReader["COMPLETE"]);
                    objDbWriter.Fields.Add("COMPLETE_DTTM", myReader["COMPLETE_DTTM"]);
                    objDbWriter.Fields.Add("USER_ID", myReader["USER_ID"]);
                    objDbWriter.Fields.Add("ARCHIVED", myReader["ARCHIVED"]);
                    objDbWriter.Fields.Add("ERROR_FLAG", myReader["ERROR_FLAG"]);

                    objDbWriter.Execute();
                    objDbWriter = null;
                }


                sSql.Clear();
                myReader = null;

                sSql.Append(string.Format(@"SELECT * FROM SM_REPORTS"));                // Fourth Table
                myReader = DbFactory.ExecuteReader(strSortMaster, sSql.ToString());
                while (myReader.Read())
                {
                    //logic to process data for the Fourth table.

                    objDbWriter = DbFactory.GetDbWriter(strTaskManagerSource);
                    objDbWriter.Tables.Add("TM_REPORTS");
                    objDbWriter.Fields.Add("REPORT_ID", myReader["REPORT_ID"]);
                    objDbWriter.Fields.Add("REPORT_NAME", myReader["REPORT_NAME"]);
                    objDbWriter.Fields.Add("REPORT_DESC", myReader["REPORT_DESC"]);
                    objDbWriter.Fields.Add("REPORT_XML", myReader["REPORT_XML"]);
                    objDbWriter.Fields.Add("PRIVACY_CASE", myReader["PRIVACY_CASE"]);

                    objDbWriter.Execute();
                    objDbWriter = null;

                }
                sSql.Clear();
                myReader = null;

                sSql.Append(string.Format(@"SELECT * FROM SM_REPORTS_PERM"));                // Fifth Table
                myReader = DbFactory.ExecuteReader(strSortMaster, sSql.ToString());
                while (myReader.Read())
                {
                    //logic to process data for the Fifth table.

                    objDbWriter = DbFactory.GetDbWriter(strTaskManagerSource);
                    objDbWriter.Tables.Add("TM_REPORTS_PERM");
                    objDbWriter.Fields.Add("REPORT_ID", myReader["REPORT_ID"]);
                    objDbWriter.Fields.Add("USER_ID", myReader["USER_ID"]);

                    objDbWriter.Execute();
                    objDbWriter = null;
                }

                sSql.Clear();
                myReader = null;


                sSql.Append(string.Format(@"SELECT * FROM SM_SCHEDULE"));                // Sixth Table
                myReader = DbFactory.ExecuteReader(strSortMaster, sSql.ToString());
                while (myReader.Read())
                {
                    //logic to process data for the Sixth table.


                    objDbWriter = DbFactory.GetDbWriter(strTaskManagerSource);
                    objDbWriter.Tables.Add("TM_REP_SCHEDULE");
                    objDbWriter.Fields.Add("SCHEDULE_ID", myReader["SCHEDULE_ID"]);
                    objDbWriter.Fields.Add("REPORT_ID", myReader["REPORT_ID"]);

                    objDbWriter.Fields.Add("LAST_RUN_DTTM", myReader["LAST_RUN_DTTM"]);
                    objDbWriter.Fields.Add("NEXT_RUN_DATE", myReader["NEXT_RUN_DATE"]);
                    objDbWriter.Fields.Add("SCHEDULE_TYPE", myReader["SCHEDULE_TYPE"]);

                    objDbWriter.Fields.Add("START_TIME", myReader["START_TIME"]);
                    objDbWriter.Fields.Add("START_DATE", myReader["START_DATE"]);
                    objDbWriter.Fields.Add("MON_RUN", myReader["MON_RUN"]);

                    objDbWriter.Fields.Add("TUE_RUN", myReader["TUE_RUN"]);
                    objDbWriter.Fields.Add("WED_RUN", myReader["WED_RUN"]);
                    objDbWriter.Fields.Add("THU_RUN", myReader["THU_RUN"]);

                    objDbWriter.Fields.Add("FRI_RUN", myReader["FRI_RUN"]);
                    objDbWriter.Fields.Add("SAT_RUN", myReader["SAT_RUN"]);
                    objDbWriter.Fields.Add("SUN_RUN", myReader["SUN_RUN"]);

                    objDbWriter.Fields.Add("DAYOFMONTH_RUN", myReader["DAYOFMONTH_RUN"]);
                    objDbWriter.Fields.Add("JAN_RUN", myReader["JAN_RUN"]);
                    objDbWriter.Fields.Add("FEB_RUN", myReader["FEB_RUN"]);

                    objDbWriter.Fields.Add("MAR_RUN", myReader["MAR_RUN"]);
                    objDbWriter.Fields.Add("APR_RUN", myReader["APR_RUN"]);
                    objDbWriter.Fields.Add("MAY_RUN", myReader["MAY_RUN"]);

                    objDbWriter.Fields.Add("JUN_RUN", myReader["JUN_RUN"]);
                    objDbWriter.Fields.Add("JUL_RUN", myReader["JUL_RUN"]);
                    objDbWriter.Fields.Add("AUG_RUN", myReader["AUG_RUN"]);

                    objDbWriter.Fields.Add("SEP_RUN", myReader["SEP_RUN"]);
                    objDbWriter.Fields.Add("OCT_RUN", myReader["OCT_RUN"]);
                    objDbWriter.Fields.Add("NOV_RUN", myReader["NOV_RUN"]);

                    objDbWriter.Fields.Add("DEC_RUN", myReader["DEC_RUN"]);
                    objDbWriter.Fields.Add("JOB_NAME", myReader["JOB_NAME"]);
                    objDbWriter.Fields.Add("JOB_DESC", myReader["JOB_DESC"]);

                    objDbWriter.Fields.Add("DSN", myReader["DSN"]);
                    objDbWriter.Fields.Add("OUTPUT_TYPE", myReader["OUTPUT_TYPE"]);
                    objDbWriter.Fields.Add("OUTPUT_PATH", myReader["OUTPUT_PATH"]);

                    objDbWriter.Fields.Add("OUTPUT_PATH_URL", myReader["OUTPUT_PATH_URL"]);
                    objDbWriter.Fields.Add("OUTPUT_OPTIONS", myReader["OUTPUT_OPTIONS"]);
                    objDbWriter.Fields.Add("REPORT_XML", myReader["REPORT_XML"]);

                    objDbWriter.Fields.Add("NOTIFICATION_TYPE", myReader["NOTIFICATION_TYPE"]);
                    objDbWriter.Fields.Add("NOTIFY_EMAIL", myReader["NOTIFY_EMAIL"]);
                    objDbWriter.Fields.Add("NOTIFY_MSG", myReader["NOTIFY_MSG"]);

                    objDbWriter.Fields.Add("USER_ID", myReader["USER_ID"]);

                    objDbWriter.Execute();
                    objDbWriter = null;
                }


                sSql.Clear();
                myReader = null;

                sSql.Append(string.Format(@"SELECT * FROM SM_WORKER_COMMANDS"));                // Seventh Table
                myReader = DbFactory.ExecuteReader(strSortMaster, sSql.ToString());
                while (myReader.Read())
                {
                    //logic to process data for the Seventh table.
                    objDbWriter.Tables.Add("TM_WORKER_COMMANDS");
                    objDbWriter.Fields.Add("COMMAND_ID", myReader["COMMAND_ID"]);
                    objDbWriter.Fields.Add("WORKER_PROCESS", myReader["WORKER_PROCESS"]);
                    objDbWriter.Fields.Add("ISSUED_DTTM", myReader["ISSUED_DTTM"]);

                    objDbWriter.Execute();
                    objDbWriter = null;
                }

                // Insert for hardcoded Entries in script for TM_SCHEDULE_TYPE , TM_TASK_TYPE                  
                //logic to Insert for hardcoded Entries in script


                //objDbWriter = DbFactory.GetDbWriter(strTaskManagerSource);
                //objDbWriter.Tables.Add("TM_SCHEDULE_TYPE");
                //objDbWriter.Fields.Add("SCHEDULE_TYPE_ID", DisplayDBUpgrade.GetNextUID(strTaskManagerSource,"TM_SCHEDULE_TYPE"));
                //objDbWriter.Fields.Add("SCHEDULE_TYPE", "Daily");                 
                //objDbWriter.Execute();
                //objDbWriter = null;

                ////<Task Name="OSHA Reports" cmdline="yes"><Path>Osha.exe</Path></Task>

                //StringBuilder config = new StringBuilder();
                //config.Append("<Task Name=");
                //config.Append('"'+"OSHA Reports"+'"');
                //config.Append(" cmdline=");
                //config.Append('"' + "yes" + '"');
                //config.Append("><Path>Osha.exe</Path></Task>");


                //objDbWriter = DbFactory.GetDbWriter(strTaskManagerSource);
                //objDbWriter.Tables.Add("TM_TASK_TYPE");
                //objDbWriter.Fields.Add("TASK_TYPE_ID", 5);
                //objDbWriter.Fields.Add("NAME", "OSHA Reports");
                //objDbWriter.Fields.Add("DESCRIPTION", "OSHA Reports");
                //objDbWriter.Fields.Add("CONFIG", config.ToString());
                //objDbWriter.Fields.Add("DISABLE_DETAILED_STATUS", 0 );
                //objDbWriter.Fields.Add("SYSTEM_MODULE_NAME", "OSHAReports");

                //objDbWriter.Execute();
                //objDbWriter = null;

                #endregion
            }
            #endregion
        }
        #endregion

        
        /// <summary>
        /// upgrades the RISKMASTER security database
        /// </summary>
        public static void UpgradeRMSecurity(bool bIsHPUpgrade)
        {
            int iOverall = 0;
            const string STRING_DELIMITER = ",";

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }

            string[] arrSQLFiles = null;

            //Raman 5/6/2013 : We do not want to run all scripts for CHP's
            if (bIsHPUpgrade)
            {
                arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.RMHPSECURITYDBSCRIPTS, STRING_DELIMITER);
            }
            else
            {
                //get the list of SQL files for the RISKMASTER security database
                arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.SECURITYSCRIPTS, STRING_DELIMITER);
            }

            //gagnihotri Audit changes MITS 17425, existing SQL triggers are to be modified.
            //Triggers are need to be modified prior to db-upgrade.
            //In future script could be changed to create SQL audit triggers also
            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_SecConnectString);

            if (DisplayDBUpgrade.g_dbMake == 5)
            {
                throw new ApplicationException("DSN connectivity is not allowed for the Security DB.<br /><br />Please change the 'RMXSecurity' connectionString to a MSSQL or Oracle provider.");
            }

            if (DisplayDBUpgrade.g_dbMake == 1)// DBMS_IS_SQLSRVR  = 1
            {
                string[] sourceArrSQLFile = arrSQLFiles;
                Array.Resize(ref arrSQLFiles, arrSQLFiles.Length + 1);//add +1 to the existing array
                Array.Copy(sourceArrSQLFile, 0, arrSQLFiles, 1, sourceArrSQLFile.Length);//move entire array to the end
                //Run the security audit trigger
                Array.Copy(new object[] {DisplayDBUpgrade.TRIGGERS[1] }, 0, arrSQLFiles, 0, 1);//insert at 0 index
            }
            //if (DisplayDBUpgrade.g_dbMake == 4)// DBMS_IS_ORACLE  = 4
            //{
            //    string[] sourceArrSQLFile = arrSQLFiles;
            //    Array.Resize(ref arrSQLFiles, arrSQLFiles.Length + 1);//add +1 to the existing array
            //    Array.Copy(sourceArrSQLFile, 0, arrSQLFiles, 1, sourceArrSQLFile.Length);//move entire array to the end
            //    //Run the security audit trigger
            //    Array.Copy(new object[] { DisplayDBUpgrade.ORACLE_TRIGGERS[1] }, 0, arrSQLFiles, 0, 1);//insert at 0 index
            //}
            if (arrSQLFiles.Length > 0)
            {
                DisplayDBUpgrade.sODBCName = "SECURITY";

                //create the necessary log file
                CreateLogFile();

                if (!_isSilent)
                {
                    frmWizard.UpdateDBName("SECURITY");
                    frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                }

                foreach (var strSQLFile in arrSQLFiles)
                {
                    iOverall++;

                    if (!_isSilent)
                    {
                        //display name of script on the update form
                        frmWizard.UpdateScriptName(strSQLFile);
                        frmWizard.UpdateCurrentProgressBar(0);
                    }

                    //MJP - get the security database make
                    DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_SecConnectString);
                    if (DisplayDBUpgrade.g_dbMake == 4 && strSQLFile == "Oracle-secdb-final.sql")
                    {
                        bool bExecute = false;
                        bExecute = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_SecConnectString, strSQLFile, DisplayDBUpgrade.g_dbMake, _isSilent);
                    }
                    else
                    {
                    //execute each individual SQL file against the database
                    UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_SecConnectString, strSQLFile, String.Empty, _isSilent);
                    }
                    if (!_isSilent)
                    {
                        frmWizard.UpdateOverallProgressBar(iOverall);
                    }
                }
            }
        }

        /// <summary>
        /// upgrade the selected RISKMASTER base database(s)
        /// </summary>
        public static void UpgradeRMBase(bool bIsHPUpgrade, bool bInsertViewsOnly)
        {
            const string STRING_DELIMITER = ",";

            //run dBUpgrade on selected datasources
            foreach (KeyValuePair<string,int> kvp in DisplayDBUpgrade.dictPassedItems)
            {
                int iOverall = 0;

                if (!_isSilent)
                {
                    //reset the progressbars on wizard processing page for each selected dB
                    frmWizard.SetOverallProgressBarProperties(0);
                    frmWizard.SetCurrentProgressBarProperties(0);
                    frmWizard.UpdateCurrentProgressBar(0);
                    frmWizard.UpdateOverallProgressBar(0);
                }

                string[] arrSQLFiles = null;
                string[] arrSQLPriorFiles = null;
                //get the list of SQL files for the RISKMASTER databases
                //Raman 5/6/2013 : We do not want to run all scripts for CHP's
                if (bIsHPUpgrade)
                {
                    arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.RMHPMAINDBSCRIPTS, STRING_DELIMITER);
                }
                else
                {
                    arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.BASESCRIPTS, STRING_DELIMITER);
                    arrSQLPriorFiles = GetSQLScriptFiles(DisplayDBUpgrade.BASEPRIORSCRIPTS, STRING_DELIMITER);
                }

                //MITS 36013 Added by Swati Agarwal for adding staging tables file WWIG
                string sDbUpgradeStaging = ConfigurationManager.AppSettings["DBUPGRADE_STAGING"].ToString().ToUpper();
                if (sDbUpgradeStaging == "TRUE")
                {
                    Array.Resize(ref arrSQLFiles, arrSQLFiles.Length + 1);//add +1 to the existing array
                    arrSQLFiles[arrSQLFiles.Length - 1] = GetSQLScriptFiles(DisplayDBUpgrade.RMASTAGINGSCRIPTS, STRING_DELIMITER)[0].ToString();
                }
                //change end here by swati

                //gagnihotri Audit changes MITS 17425, existing SQL triggers are to be modified.
                //Triggers are need to be modified prior to db-upgrade.
                //In future script could be changed to create SQL audit triggers also
                DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);
                if (DisplayDBUpgrade.g_dbMake == 1)// DBMS_IS_SQLSRVR  = 1
                {
                    string[] sourceArrSQLFile = arrSQLFiles;
                    Array.Resize(ref arrSQLFiles, arrSQLFiles.Length + 1);//add +1 to the existing array
                    Array.Copy(sourceArrSQLFile, 0, arrSQLFiles, 1, sourceArrSQLFile.Length);//move entire array to the end
                    //Run the standard modify audit trigger
                    Array.Copy(new object[] { DisplayDBUpgrade.TRIGGERS[0]}, 0, arrSQLFiles, 0, 1);//insert at 0 index
                }
                //if (DisplayDBUpgrade.g_dbMake == 4)// DBMS_IS_SQLSRVR  = 1
                //{
                //    string[] sourceArrSQLFile = arrSQLFiles;
                //    Array.Resize(ref arrSQLFiles, arrSQLFiles.Length + 1);//add +1 to the existing array
                //    Array.Copy(sourceArrSQLFile, 0, arrSQLFiles, 1, sourceArrSQLFile.Length);//move entire array to the end
                //    //Run the standard modify audit trigger
                //    Array.Copy(new object[] { DisplayDBUpgrade.ORACLE_TRIGGERS[0] }, 0, arrSQLFiles, 0, 1);//insert at 0 index
                //}
                if (arrSQLFiles.Length > 0)
                {
                    DisplayDBUpgrade.sODBCName = kvp.Key;
                    DisplayDBUpgrade.g_iDSNID = kvp.Value;

                    if (!bInsertViewsOnly)
                    {
                        using (var dbDSNRdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, RISKMASTERScripts.GetSelectedDSNInfo(DisplayDBUpgrade.g_iDSNID)))
                        {
                            while (dbDSNRdr.Read())
                            {
                                //var blnDBExists = true;

                                //form a connection string so we can connect and do rest of the upgrade
                                DisplayDBUpgrade.g_sConnectString = RISKMASTERScripts.BuildConnectionString(dbDSNRdr["CONNECTION_STRING"].ToString(), RMCryptography.DecryptString(dbDSNRdr["RM_USERID"].ToString()), RMCryptography.DecryptString(dbDSNRdr["RM_PASSWORD"].ToString()));
                                DisplayDBUpgrade.CheckForOracleString(ref DisplayDBUpgrade.g_sConnectString);

                                //deal with Document DB (if it exists in the security db)
                                string sDocDB = String.Empty;
                                string sDocUserId = String.Empty;

                                string sDocConnString = dbDSNRdr["GLOBAL_DOC_PATH"].ToString();

                                if (!String.IsNullOrEmpty(sDocConnString))
                                {
                                    if (sDocConnString.Contains("Driver"))
                                    {
                                        DisplayDBUpgrade.g_DocumentConnectString = RISKMASTERScripts.DecryptDocumentConnectionString(sDocConnString, ref sDocUserId, ref sDocDB);
                                
                                        DisplayDBUpgrade.g_DocUserId = sDocUserId;
                                        DisplayDBUpgrade.g_DocDB = sDocDB;
                                        DisplayDBUpgrade.CheckForOracleString(ref DisplayDBUpgrade.g_DocumentConnectString);

                                        UpgradeRMDocument(); //process document scripts
                                    }
                                }

                                //get the database make
                                DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);

                                //get the database release and version information
                                DisplayDBUpgrade.gsDBRelease = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT RELEASE_NUMBER FROM SYS_PARMS");
                                DisplayDBUpgrade.gsDBVersion = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT DTG_VERSION FROM SYS_PARMS");

                                if (dbDSNRdr["ORGSEC_FLAG"].ToString() != "0")
                                {
                                    DisplayDBUpgrade.g_bOrgSecOn = true;
                                }
                            }
                        }

                        //create the necessary log file
                        CreateLogFile();

                        //MJP - this hides the password so it doesn't record it into the logfile...
                        string sPasswordHiddenConnString = String.Empty;
                        string[] result = DisplayDBUpgrade.g_sConnectString.Split(new string[] { "PWD" }, StringSplitOptions.None);
                        sPasswordHiddenConnString = String.Format(@"{0}PWD=*** NOT SHOWN ***;", result[0]);

                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "DSN ID: " + DisplayDBUpgrade.g_iDSNID + "\r\n");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Connection String: " + sPasswordHiddenConnString + "\r\n");

                       
                        if (!_isSilent)
                        {
                            frmWizard.UpdateDBName(kvp.Key);
                           // frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);

                            if (bIsHPUpgrade)
                            {
                                frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                            }
                            else
                            {
                                //rsolanki2: commenting the updates for updating the full name in enhc notes 
                                frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length + arrSQLPriorFiles.Length);
                                //frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length + arrSQLPriorFiles.Length + 1);
                            }
                        }

                        //some of the scripts should be run prior to base scripts
                        if (arrSQLPriorFiles != null && arrSQLPriorFiles.Length > 0)
                        {
                            ExecuteSqlScripts(ref iOverall, arrSQLPriorFiles);
                        }

                        UpdateFunctions.ConnectedDB = "RMX Base DB";
                        //Base scripts
                        if (arrSQLFiles.Length > 0)
                        {
                            //GOVIND - START
                            string sDbchkUpgradeDatabase = frmWizard._sDbchkUpgradeDatabase.ToString().ToUpper();
                            //_sDbchkUpgradeDatabase = (sDbchkUpgradeDatabase == bool.TrueString.ToUpper()) ? true : false;

                            if (sDbchkUpgradeDatabase.Equals("TRUE"))
                                {
                                        ExecuteSqlScripts(ref iOverall, arrSQLFiles);
                                } 
                            //GOVIND - End                            
                        }
                    
                        //UpdateUserInEnhanceNotes(ref iOverall);

                        //Raman 5/6/2013 : We do not want to run these stored procedures in CHP's
                        if (!bIsHPUpgrade)
                        {
                            UpgradeRMStoreProcedures(kvp.Key);
                        }
                    }
                    
                    //process other dB specific functions
                    UpdateFunctions.UpdateFDMViews(_isSilent);
                    UpdateFunctions.UpdateWPADiary(_isSilent);

                    //refresh BES views for SQL Server
                    if (DisplayDBUpgrade.g_dbMake == RiskmasterDBTypes.DBMS_IS_SQLSRVR && DisplayDBUpgrade.g_bOrgSecOn)
                    {
                        UpdateFunctions.RefreshSQLServerBESViews(_isSilent);
                    }
                }
            }
            EnableBRS();
        }
        private static void UpdateUserInEnhanceNotes(ref int iOverall)
        {
            
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sUserName = string.Empty;
            string sSql = string.Empty;
            int iUserId = 0;
            int iSqlCount = 0;

            DataSet objDataSet = null;


            //log the name of the file
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating User name in Enhance Notes" + " Started - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            sSql = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME ='PRGNOTES_UPDATED' ";
            int iPrgNotesUpdated = Convert.ToInt32(DbFactory.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSql));
            if (iPrgNotesUpdated != -1)
            {
                try
                {
                    //fetch the user id of all the distict user from CLAIM_PRG_NOTE
                    sSql = "SELECT DISTINCT ENTERED_BY FROM CLAIM_PRG_NOTE";
                    objDataSet = DbFactory.GetDataSet(DisplayDBUpgrade.g_sConnectString, sSql, m_iClientId);

                    // number of update queries to be executed.
                    iSqlCount = objDataSet.Tables[0].Rows.Count;
                    if (!_isSilent)
                    {
                        //display name of proces on the update form
                        frmWizard.UpdateScriptName("Updating User name in Enhance Notes");
                        frmWizard.UpdateCurrentProgressBar(0);
                        frmWizard.SetCurrentProgressBarProperties(iSqlCount);

                    }

                    foreach (DataRow drObject in objDataSet.Tables[0].Rows)
                    {
                        sSql = string.Empty;
                        sFirstName = string.Empty;
                        sLastName = string.Empty;
                        sUserName = string.Empty;
                        try
                        {
                            iUserId = Convert.ToInt32(drObject[0].ToString());
                            //query to fetch User Name
                            sSql = "SELECT FIRST_NAME, LAST_NAME from USER_TABLE WHERE USER_ID = " + iUserId;

                            using (DbReader objRdr = DbFactory.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, sSql))
                            {
                                if (objRdr.Read())
                                {
                                    sFirstName = objRdr["FIRST_NAME"].ToString();

                                    sLastName = objRdr["LAST_NAME"].ToString();

                                    //User Name which will replace login name
                                    sUserName = sFirstName + " " + sLastName;

                                }

                            }

                            //update query which will replace login name by user name(First Name + Last Name)
                            if (!string.IsNullOrEmpty(sUserName))
                            {
                                sSql = "UPDATE CLAIM_PRG_NOTE SET ENTERED_BY_NAME ='" + sUserName + "'  WHERE ENTERED_BY =" + iUserId;
                                frmWizard.UpdateExecutingText(sSql);
                                DbFactory.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSql);
                            }
                        }
                        catch (Exception Ex)
                        {
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), Ex.Message);
                        }


                    }
                    objDataSet.Dispose();

                    sSql = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = -1 WHERE PARM_NAME = 'PRGNOTES_UPDATED'";

                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSql);

                }
                catch (Exception objEx)
                {
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), objEx.Message);
                }
            }

            
            iOverall++;
            if (!_isSilent)
            {
                frmWizard.UpdateOverallProgressBar(iOverall);
            }
        }

        private static void ExecuteSqlScripts(ref int  iOverall, string[] arrSQLFiles)
        {
            foreach (string strSQLFile in arrSQLFiles)
            {
                iOverall++;

                // Enhancement Mits id:  -- Govind - Start 

                if(strSQLFile.Equals("RM_X_base_Tables.sql"))
                {
                    //set sBaseLogType to Tables
                    DisplayDBUpgrade.sBaseLogType = "Tables";
                    //Create seprate Logfile here for Tables
                    CreateLogFile();

                }

                if (strSQLFile.Equals("RM_X_base_Indexes.sql"))
                {
                    //set sBaseLogType to Tables
                    DisplayDBUpgrade.sBaseLogType = "Indexes";
                    //Create seprate Logfile here for Tables
                    CreateLogFile();

                }

                // Enhancement Mits id:  -- Govind - End
 

                if (!_isSilent)
                {
                    //display name of script on the update form
                    frmWizard.UpdateScriptName(strSQLFile);
                    frmWizard.UpdateCurrentProgressBar(0);
                }
                else
                {
                    //Console.WriteLine("");
                    //Console.WriteLine("Script: " + strSQLFile);
                }

                if (DisplayDBUpgrade.g_dbMake == 4 && strSQLFile == "oracle-maindb-final.sql")
                {
                    bool bExecute = false;
                    bExecute = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_sConnectString, strSQLFile, DisplayDBUpgrade.g_dbMake, _isSilent);
                }
                else
                {
                //execute each individual SQL file against the database
                UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_sConnectString, strSQLFile, String.Empty, _isSilent);
                }
                if (!_isSilent)
                {
                    frmWizard.UpdateOverallProgressBar(iOverall);
                }
            }
            
        }

        /// <summary>
        /// upgrades the RISKMASTER session database
        /// </summary>
        public static void UpgradeRMSession()
        {
            int iOverall = 0;
            const string STRING_DELIMITER = ",";

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }

            //get the list of SQL files for the RISKMASTER security database
            string[] arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.SESSIONSCRIPTS, STRING_DELIMITER);

            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_SessionConnectString);
            
            if (arrSQLFiles.Length > 0)
            {
                DisplayDBUpgrade.sODBCName = "SESSION";

                //create the necessary log file
                CreateLogFile();

                if (!_isSilent)
                {
                    frmWizard.UpdateDBName("SESSION");
                    frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                }

                foreach (var strSQLFile in arrSQLFiles)
                {
                    iOverall++;

                    if (!_isSilent)
                    {
                        //display name of script on the update form
                        frmWizard.UpdateScriptName(strSQLFile);
                        frmWizard.UpdateCurrentProgressBar(0);
                    }

                    //MJP - get the security database make
                    DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_SessionConnectString);

                    //execute each individual SQL file against the database
                    UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_SessionConnectString, strSQLFile, String.Empty, _isSilent);

                    if (!_isSilent)
                    {
                        frmWizard.UpdateOverallProgressBar(iOverall);
                    }
                }
                UpdateFunctions.UpdateSessionCustomizeTable();//asharma326 JIRA 6422 func to insert HTML configurations 
                
            }
        }


        //Mgaba2: MITS 23998: Getting the information whether DBUpgrade has been run on Document Storage DB to remove "\" from file name
        private static int GetDocStoreRemoveSlashFlag()
        {
            string sSQL = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'DOC_STORE_REM_SLASH'";
            return (ADONetDbAccess.ExecuteScalar(DisplayDBUpgrade.g_sConnectString, sSQL));
        }

        //Mgaba2: MITS 23998 :setting the information that DBUpgrade has been run on Document Storage DB to remove "\" from file name
        private static void SetDocStoreRemoveSlashFlag()
        {
            string sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = -1 WHERE PARM_NAME = 'DOC_STORE_REM_SLASH'";
            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
        }

        /// <summary>
        /// upgrades the RISKMASTER document database
        /// </summary>
        public static void UpgradeRMDocument()
        {
            int iDB_Make = -1;
            string sSQL = String.Empty;
            string strDataType = String.Empty;
            string strMaxLength = String.Empty;

            const string DOC_TABLE  = "DOCUMENT_STORAGE";
            const string DOC_COLUMN = "DOC_BLOB";
            const string DOC_TYPE   = "VARBINARY(MAX)";
            bool bInsertStoredProc = false; //Mgaba2: MITS 23998 
            int iDocStoreRemoveSlashFlag = 0; //Mgaba2: MITS 23998 

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }

            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_DocumentConnectString);
            iDB_Make = DisplayDBUpgrade.g_dbMake;

            string strQuery = RISKMASTERScripts.GetDataTypeAndLength(DOC_TABLE, DOC_COLUMN, DisplayDBUpgrade.g_DocumentConnectString, DisplayDBUpgrade.g_DocUserId);

            System.Data.DataSet dsObject = ADONetDbAccess.GetDataSet(DisplayDBUpgrade.g_DocumentConnectString, strQuery);
            foreach (System.Data.DataRow drObject in dsObject.Tables[0].Rows)
            {
                strDataType = drObject["c_datatype"].ToString();
            }
            dsObject.Dispose();

            //Mgaba2: MITS 23998 
            iDocStoreRemoveSlashFlag = GetDocStoreRemoveSlashFlag();         

            switch (iDB_Make)
            {
                case 1:                 // DBMS_IS_SQLSRVR  = 1
                    switch (strDataType)
                    {
                        case "varbinary":
                            break;
                        default:
                            sSQL = String.Format("ALTER TABLE {0} ALTER COLUMN {1} {2}", DOC_TABLE, DOC_COLUMN, DOC_TYPE);
                            ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_DocumentConnectString, sSQL);
                            break;
                    }
                    //Mgaba2: MITS 23998 :Start
                    if (iDocStoreRemoveSlashFlag == 0)
                    {
                        try
                        {
                            //execute each individual SQL file against the database
                            bInsertStoredProc = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_DocumentConnectString, "USP_REM_SLASH_FILENAME_SQL.sql", iDB_Make, _isSilent);
                            if (bInsertStoredProc)
                            {
                                ADONetDbAccess.ExecuteStoredProcedure(DisplayDBUpgrade.g_DocumentConnectString, "USP_REM_SLASH_FILENAME");
                                SetDocStoreRemoveSlashFlag();
                            }
                        }
                        catch (Exception ex)
                        {
                            string strLogFilePath = String.Format(@"{0}\\dBUpgrade.log", Application.StartupPath);
                            DisplayDBUpgrade.LogErrors(strLogFilePath, ex.ToString());
                        }
                    }
                    //Mgaba2: MITS 23998 :End
                    break;
                case 4:                 // DBMS_IS_ORACLE   = 4
                    switch (strDataType)
                    {
                        case "blob":
                            break;
                        default:
                            try
                            {
                                sSQL = String.Format("ALTER TABLE {0} ADD {1} {2}", DOC_TABLE, "DOC_BLOB_TEMP", "BLOB");
                                ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_DocumentConnectString, sSQL);
                            }
                            catch (Exception)
                            {
                            }

                            //execute SQL file against the database
                            string sSQLFile = "USP_CLOB_BLOB_Conversion_Oracle.SQL";
                            bool bExecuteSP = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_DocumentConnectString, sSQLFile, iDB_Make, _isSilent);

                            //execute stored procedure
                            //if (bExecuteSP)
                            //{
                                try
                                {
                                    string strExecuteSP = String.Format("BEGIN {0}; COMMIT; END;", "USP_CLOB_BLOB_CONVERSION");
                                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_DocumentConnectString, strExecuteSP);
                                }//try
                                catch (Exception ex)
                                {
                                    string strLogFilePath = String.Format(@"{0}\\dBUpgrade.log", Application.StartupPath);
                                    DisplayDBUpgrade.LogErrors(strLogFilePath, ex.ToString());
                                }
                            //}//if
                            break;
                    }
                    //Mgaba2: MITS 23998 :Start
                    if (iDocStoreRemoveSlashFlag == 0)
                    {                        
                        try
                        {
                            //execute each individual SQL file against the database
                            bInsertStoredProc = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_DocumentConnectString, "USP_REM_SLASH_FILENAME_ORACLE.sql", iDB_Make, _isSilent);
                            if (bInsertStoredProc)
                            {                                
                                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_DocumentConnectString, string.Format("BEGIN USP_REM_SLASH_FILENAME('{0}'); COMMIT; END;", DisplayDBUpgrade.g_DocUserId.ToUpper()));
                                SetDocStoreRemoveSlashFlag();
                            }
                        }
                        catch (Exception ex)
                        {
                            string strLogFilePath = String.Format(@"{0}\\dBUpgrade.log", Application.StartupPath);
                            DisplayDBUpgrade.LogErrors(strLogFilePath, ex.ToString());
                        }
                    }
                    //Mgaba2: MITS 23998 :End
                    break;
                default:
                    break;
            }
        }

     //nadim added for rebuilding indexes-END

       //nadim added for rebuilding indexes-start
        /// <summary>
        /// upgrades the RISKMASTER  database Indexes
        /// </summary>
        public static void UpgradeIndexes()
        {
            int iOverall = 0;
            int iDB_Make = -1;
            const string STRING_DELIMITER = ",";
            string sScriptPath = String.Empty;
            int iDebugCount = -1;
            String sLine = String.Empty;
            bool bPrimaryKeySetting = GetprimarykeySetting();


            if (!bPrimaryKeySetting)
            {
                if (!_isSilent)
                {
                    //reset the progressbars on wizard processing page
                    frmWizard.SetOverallProgressBarProperties(0);
                    frmWizard.SetCurrentProgressBarProperties(0);
                    frmWizard.UpdateCurrentProgressBar(0);
                    frmWizard.UpdateOverallProgressBar(0);
                }


                DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);

                string[] arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.STOREDPROCS4PRIMARYKEYS, STRING_DELIMITER);

                if (arrSQLFiles.Length > 0)
                {
                    DisplayDBUpgrade.sODBCName = "PRIMARY_KEYS TASK";

                    //create the necessary log file
                    CreateLogFile();

                    if (!_isSilent)
                    {
                        frmWizard.UpdateDBName("PRIMARY_KEYS TASK");
                        frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                    }

                    iDB_Make = DisplayDBUpgrade.g_dbMake;

                    foreach (string strSQLFile in arrSQLFiles)
                    {
                        iOverall++;
                        switch (strSQLFile)
                        {

                            case "USP_CREATE_NEW_STUFF":
                                frmWizard.UpdateExecutingText("Creating New Tables required for primary key task");
                                break;
                            case "USP_GETALLINDEXINFO":
                                frmWizard.UpdateExecutingText("Fetching all Constraints and Indexes available in the  database");
                                break;
                            case "USP_EXISTING_INDICES_TEMP":
                                frmWizard.UpdateExecutingText("Storing all Constraints and Indexes vailable in the database and storing them into  a temporary table.");
                                break;
                            case "USP_DROPEXISTINGINDEXES":
                                frmWizard.UpdateExecutingText("Droping  all Constraints and Indexes present in the Database");
                                break;
                            case "USP_OPTIMUM_INDICES":
                                frmWizard.UpdateExecutingText("Storing all RISKMASTER standard Constraints and Indexes in a temporary table");
                                break;
                            case "USP_FINAL_INDICES":
                                frmWizard.UpdateExecutingText("Merging all RISKMASTER standard Constraints and Indexes with others Constraints and Indexes ");
                                break;
                            case "USP_CREATE_INDEXES":
                                frmWizard.UpdateExecutingText("Creating new Constraints and Indexes as per RISKMASTER standard ");
                                break;
                            default:
                                break;
                        }

                        //get the path for the scripts to be executed
                        sScriptPath = UpdateFunctions.GetScriptPath(UpdateFunctions.GetSystemPath(), strSQLFile);

                        switch (iDB_Make)
                        {
                            case 1:
                                sScriptPath = sScriptPath + "_SQL.SQL";
                                break;
                            case 4:
                                sScriptPath = sScriptPath + "_ORACLE.SQL";
                                break;
                            default:
                                break;
                        }

                        if (!(UpdateFunctions.CheckIfScriptExists(strSQLFile, _isSilent, ref sScriptPath)))
                        {
                            return;
                        }

                        StreamReader sr = File.OpenText(sScriptPath);

                        do
                        {
                            iDebugCount++;
                        } while ((sr.ReadLine()) != null);

                        sr.Close();

                        if (!_isSilent)
                        {
                            frmWizard.UpdateScriptName(strSQLFile);//display name of script on the update form
                            frmWizard.SetCurrentProgressBarProperties(iDebugCount);
                        }

                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating Primary Keys Task(" + sScriptPath + ") Started - " + DateTime.Now);
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");

                        switch (iDB_Make)
                        {
                            case 1:
                                try
                                {
                                    ADONetDbAccess.ExecuteStoredProcedure(DisplayDBUpgrade.g_sConnectString, strSQLFile);
                                }
                                catch (Exception ex)
                                {
                                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating Primary Keys Task(" + sScriptPath + ") - " + ex.Message);
                                }
                                break;
                            case 4:
                                String strSQLFile1 = strSQLFile + "('" + DisplayDBUpgrade.g_UserId.ToUpper() + "')";
                                string strExecuteSP = String.Format("BEGIN {0}; COMMIT; END;", strSQLFile1);
                                try
                                {
                                    ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strExecuteSP);
                                }
                                catch (Exception ex)
                                {
                                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating Primary Keys Task(" + sScriptPath + ") - " + ex.Message);
                                }
                                break;
                            default:
                                break;
                        }


                        if (!_isSilent)
                        {
                            frmWizard.UpdateCurrentProgressBar(iDebugCount);
                            frmWizard.UpdateOverallProgressBar(iOverall);
                        }
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating Primary Keys Task(" + sScriptPath + ") Finished - " + DateTime.Now);
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
                    }

                }
                UpdateprimarykeySetting();
            }
        }

        //nadim added for rebuilding indexes-end

        //nadim added for running stored proc with params-start
        /// <summary>
        /// upgrades the RISKMASTER  database stored procs with params
        /// </summary>
        public static void RunStoredProcWithParams(string sProc)
        {
            
            int iDB_Make = -1;
            string sScriptPath = String.Empty;
            int iDebugCount = -1;
            String sLine = String.Empty;
            string strExecuteSP = string.Empty;
            string strSQLFile = string.Empty;

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }
            if (sProc.Contains("("))
            {
                strSQLFile = sProc.Remove(sProc.IndexOf('('));
            }
            else if (sProc.Contains(" "))
            {
                strSQLFile = sProc.Remove(sProc.IndexOf(' '));
            }
            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);


            DisplayDBUpgrade.sODBCName = "RUN STORED PROCEDURES WITH PARAMS";

            //create the necessary log file
            CreateLogFile();

            if (!_isSilent)
            {
                frmWizard.UpdateDBName("RUN STORED PROCEDURES WITH PARAMS");               
                frmWizard.SetOverallProgressBarProperties(10);
            }

            iDB_Make = DisplayDBUpgrade.g_dbMake;
            frmWizard.UpdateExecutingText(sProc);
            //get the path for the scripts to be executed
            sScriptPath = UpdateFunctions.GetScriptPath(UpdateFunctions.GetSystemPath(), strSQLFile);
            switch (iDB_Make)
            {
                case 1:
                    strSQLFile += "_SQL.SQL";
                    sScriptPath = sScriptPath + "_SQL.SQL";
                    break;
                case 4:
                    strSQLFile += "_ORACLE.SQL";
                    sScriptPath = sScriptPath + "_ORACLE.SQL";
                    break;
                default:
                    break;
            }

            if (!(UpdateFunctions.CheckIfScriptExists(strSQLFile, _isSilent, ref sScriptPath)))
            {
                return;
            }

            StreamReader sr = File.OpenText(sScriptPath);

            do
            {
                iDebugCount++;
            } while ((sr.ReadLine()) != null);

            sr.Close();

            if (!_isSilent)
            {
                frmWizard.UpdateScriptName(strSQLFile);//display name of script on the update form
                frmWizard.SetCurrentProgressBarProperties(iDebugCount);
            }

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating stored proc with params(" + strSQLFile + ") Started - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");

            switch (iDB_Make)
            {
                case 1:
                    try
                    {
                        ADONetDbAccess.ExecuteStoredProcedure(DisplayDBUpgrade.g_sConnectString, sProc);

                    }
                    catch (Exception ex)
                    {
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating stored proc with params(" + strSQLFile + ") - " + ex.Message);
                    }
                    break;
                case 4:

                    try
                    {
                        strExecuteSP = "BEGIN  " + sProc + " ;COMMIT; END; ";
                        ADONetDbAccess.ExecuteStoredProcedure(DisplayDBUpgrade.g_sConnectString, strExecuteSP);

                    }
                    catch (Exception ex)
                    {
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating stored proc with params(" + strSQLFile + ") - " + ex.Message);
                    }
                    break;
                default:
                    break;
            }


            if (!_isSilent)
            {
                frmWizard.UpdateCurrentProgressBar(iDebugCount);               
                frmWizard.UpdateOverallProgressBar(100);
            }
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating stored proc with params(" + strSQLFile + ") Finished - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
        }
        //nadim added for running stored proc with params-end
        //nadim added for updating stored proc with params-start
        /// <summary>
        /// upgrades the RISKMASTER  database stored procs with params
        /// </summary>
        public static void UpgradeStoredProcWithParams()
        {
            string sProcName = string.Empty;            
            int iDB_Make = -1;            
            string sScriptPath = String.Empty;           
            String sLine = String.Empty;
            string strExecuteSP = string.Empty;
            int iConfFlag = 0;
            string sOutParam = string.Empty;
            try
            {

                iDB_Make = DisplayDBUpgrade.g_dbMake;

                switch (iDB_Make)

                {
                    case 1://SQL server                        
                        iConfFlag = GetConfFlagSetting();
                        sProcName = "USP_CREATE_TRIGGERS " + "'" + DisplayDBUpgrade.g_UserId + "' , " + iConfFlag + ", '" + sOutParam+"'";                        
                        RunStoredProcWithParams(sProcName);

                        
                       //Add your stored proc here with param in case of sql.
                        //sProcName = "Your Stored Proc name " + "'" + your parameters+"'"; 
                        //RunStoredProcWithParams(sProcName); 





                        break;
                    case 4://Oracle Server
                         iConfFlag = GetConfFlagSetting();
                        sProcName = "USP_CREATE_TRIGGERS" + "('" + DisplayDBUpgrade.g_UserId.ToUpper() + "'," + iConfFlag + ")";
                        RunStoredProcWithParams(sProcName);

                      
                        //Add your stored proc here with param in case of oracle.
                        //sProcName = "Your Stored Proc name " + "'" + your parameters+"'"; 
                        //RunStoredProcWithParams(sProcName); 


                        break;
                    default:
                        break;
                }
                GrantPrivilegesOnTable();

            }
            catch(Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating stored proc with params - " + ex.Message);                
            }   
  
        }

        //nadim added for granting privileges on Table to User-start
        /// <summary>
        /// Grant privilege on RISKMASTER table to BES user.
        /// </summary>
        public static void GrantPrivilegesOnTable()
        {
            string sProcName = string.Empty;
            int iDB_Make = -1;
            string sScriptPath = String.Empty;
            String sLine = String.Empty;
            string strExecuteSP = string.Empty;            
            string sOutParam = string.Empty;
            string sBesRole = string.Empty;
            int iDbId = 0;
            int iConfFlag = 0;
            string sNewUserName = string.Empty;
            string sBesUsers = string.Empty;
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            int iEnhBes = 0;
            int iDeff = 0;
            int iBes = 0;
            string sStoredproc = string.Empty;//deb:mits 24404
            try
            {

                iDB_Make = DisplayDBUpgrade.g_dbMake;

                //int iBES = GetBesFlagSetting();
                GetBesFlagSetting(ref iBes, ref iDeff);

                if ((iBes == -1) && (iDeff==-1))//if BES is Enabled
                {
                    iDbId=GetDBId();

                    sBesRole = "BESROLE" + iDbId.ToString();

                    switch (iDB_Make)
                    {
                        case 1://SQL server  
                            sStoredproc = ConfigurationManager.AppSettings["StoredProcExecutePermission4SQL"].Trim();//deb:mits 24404
                            sProcName = "USP_GRANT_PRIVILEGES " + "'" + sBesRole + "','" + sStoredproc + "'";  
                            RunStoredProcWithParams(sProcName);
                            break;
                        case 4://Oracle Server
                            iConfFlag = GetConfFlagSetting();

                            iEnhBes = GetEnhBesFlagSetting();
                            sStoredproc = ConfigurationManager.AppSettings["StoredProcExecutePermission4Oracle"].Trim();//deb:mits 24404
                            if (iEnhBes == -1)
                            {
                                sProcName = "USP_GRANT_PRIVILEGES" + "('" + sBesRole + "','" + DisplayDBUpgrade.g_UserId.ToUpper() + "'," + iConfFlag + "," + iEnhBes + ",'" + sNewUserName + "','" + sStoredproc + "')";

                                RunStoredProcWithParams(sProcName);

                            }
                            else
                            {
                                sBesUsers = "BES" + string.Format("{0:0000}", iDbId) + string.Format("{0:0000}", DisplayDBUpgrade.g_DsnId);

                                sSQL = "SELECT USERNAME FROM ALL_USERS WHERE USERNAME LIKE '%" + sBesUsers + "%'";

                                using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                                {
                                    if (objDbReader != null)
                                    {
                                        while (objDbReader.Read())
                                        {

                                            sNewUserName = objDbReader.GetString("USERNAME");

                                            sProcName = "USP_GRANT_PRIVILEGES" + "('" + sBesRole + "','" + DisplayDBUpgrade.g_UserId.ToUpper() + "'," + iConfFlag + "," + iEnhBes + ",'" + sNewUserName + "','" + sStoredproc + "')";

                                            RunStoredProcWithParams(sProcName);
                                        }
                                    }
                                }
                            }                           
                            
                            break;
                        default:
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Grant privileges on Tables to BES User - " + ex.Message);
            }

        }

        //nadim added for getting BES flag setting -start
        /// <summary>
        /// getting BES flag setting
        /// </summary>

        public static void GetBesFlagSetting(ref int iBesFlag,ref int iDeffFlag)
        {
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            

            try
            {
                sSQL = "SELECT ORGSEC_FLAG,DEF_BES_FLAG FROM DATA_SOURCE_TABLE WHERE DSNID = " + DisplayDBUpgrade.g_DsnId;
                using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_SecConnectString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            iBesFlag = objDbReader.GetInt32("ORGSEC_FLAG");
                            iDeffFlag = objDbReader.GetInt32("DEF_BES_FLAG");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Getting BES Flag - " + ex.Message);
            }

           
        }
        //nadim added for getting BES flag setting -end

        //nadim added for getting ENH BES  flag setting -start
        /// <summary>
        /// getting ENH BES flag setting
        /// </summary>

        public static int GetEnhBesFlagSetting()
        {
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            int iEnhBesFlag = 0;

            try
            {
                sSQL = "SELECT ENH_BES FROM SYS_PARMS";
                using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            iEnhBesFlag = objDbReader.GetInt("ENH_BES");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Getting enh BES Flag - " + ex.Message);
            }

            return iEnhBesFlag;
        }
        //nadim added for getting ENH BES setting -end

        /// <summary>
        /// Get Database ID
        /// </summary>
        /// <returns>DatabaseId</returns>
        private static int GetDBId()
        {
            string sMainDbName = string.Empty;
            string sSql = string.Empty;
            int iDbId = 0;
            int iPos1;
            int iPos2;
            int iDB_Make = -1;
            string sConnstringTemp = string.Empty;
            DbReader objDbReader = null;

            try
            {
                 iDB_Make = DisplayDBUpgrade.g_dbMake;
                 sConnstringTemp = DisplayDBUpgrade.g_sConnectString;
                 switch (iDB_Make)
                 {
                     case 1://SQL server                        

                         iPos1 = sConnstringTemp.IndexOf("Database=");
                         iPos2 = sConnstringTemp.IndexOf("UID=");
                         sMainDbName = sConnstringTemp.Substring(iPos1 + 9, iPos2 - iPos1 - 10);
                         sSql = "SELECT DATABASE_ID FROM SYS.DATABASES WHERE NAME='" + sMainDbName + "'";
                         break;
                     case 4://Oracle Server

                         iPos1 = sConnstringTemp.IndexOf("UID=");
                         iPos2 = sConnstringTemp.IndexOf("PWD=");
                         sMainDbName = sConnstringTemp.Substring(iPos1 + 4, iPos2 - iPos1 - 5);
                          sSql = "SELECT USER_ID FROM ALL_USERS WHERE USERNAME='" + sMainDbName.ToUpper() + "'";
                         break;
                     default:
                         break;
                 }

                 using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sSql))
                 {

                     if (objDbReader != null)
                     {
                         while (objDbReader.Read())
                         {
                             if (iDB_Make ==1)
                             {
                                 iDbId = objDbReader.GetInt32("DATABASE_ID");
                             }
                             else
                             {
                                 iDbId = objDbReader.GetInt32("USER_ID");
                             }
                         }

                     }
                 }

                

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Getting DB Id - " + ex.Message);
            }

            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();

            }

            return iDbId;
        }

        //nadim added for updating stored proc with params-start
        public static bool  GetprimarykeySetting()
        {
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            //int iPrimaryKeysetting = 0;
            bool bSetting = false;
           
            try
            {
                sSQL = "SELECT PRIMARY_KEY_TASK FROM DATA_SOURCE_TABLE WHERE DSNID = "+DisplayDBUpgrade.g_DsnId;
                using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_SecConnectString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                           
                            bSetting = Convert.ToBoolean(objDbReader.GetInt("PRIMARY_KEY_TASK"));
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating Primary Keys Task - " + ex.Message);
            }
           
            return bSetting;
        }

        //nadim added for getting confidential flag setting -start
        /// <summary>
        /// etting confidential flag setting
        /// </summary>

        public static int GetConfFlagSetting()
        {
            DbReader objDbReader = null;
            string sSQL = string.Empty;            
            int iConfRec = 0;

            try
            {
                sSQL = "SELECT CONF_FLAG FROM DATA_SOURCE_TABLE WHERE DSNID = " + DisplayDBUpgrade.g_DsnId;
                using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_SecConnectString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                          
                             iConfRec = objDbReader.GetInt32("CONF_FLAG");
                            
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating triggers - " + ex.Message);
            }

            return iConfRec;
        }
        //nadim added for getting confidential flag setting -end

        public static void UpdateprimarykeySetting()
        {
            
            string sSQL = string.Empty;


            sSQL = "UPDATE DATA_SOURCE_TABLE SET PRIMARY_KEY_TASK =-1 WHERE DSNID= " + DisplayDBUpgrade.g_DsnId;
            try
            {
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_SecConnectString, sSQL);
            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating Primary Keys Task - " + ex.Message);
            }

           
        }
        //nadim added for rebuilding indexes-end

        private static void UpgradePKInHD()
        {
            StringBuilder sbSql = null;
            DbReader objDbReader = null;

            DisplayDBUpgrade.sODBCName = "PK_FLAG_UPDATE_TASK";
            CreateLogFile();

            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);

            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating Primary Key Flag in HIST_TRACK_DICTIONARY table Task Started - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            try
            {
                sbSql = new StringBuilder();
                if (DisplayDBUpgrade.g_dbMake == 4)//Oracle
                {
                    sbSql.Length = 0;
                    sbSql.Append("SELECT UC.TABLE_NAME,UCC.COLUMN_NAME,UCC.CONSTRAINT_NAME,HT.TABLE_ID ");
                    sbSql.Append(" FROM USER_CONS_COLUMNS UCC INNER JOIN USER_CONSTRAINTS UC ON UCC.CONSTRAINT_NAME = UC.CONSTRAINT_NAME");
                    sbSql.Append(" INNER JOIN HIST_TRACK_TABLES HT ON HT.TABLE_NAME=UC.TABLE_NAME ");
                    sbSql.Append(" WHERE UC.CONSTRAINT_TYPE = 'P'");
                }
                else//Sql
                {
                    sbSql.Length = 0;
                    sbSql.Append("SELECT TC.TABLE_NAME,CC.COLUMN_NAME,TC.CONSTRAINT_NAME,HT.TABLE_ID ");
                    sbSql.Append(" FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE CC");
                    sbSql.Append(" INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC ON TC.CONSTRAINT_NAME = CC.CONSTRAINT_NAME ");
                    sbSql.Append(" INNER JOIN HIST_TRACK_TABLES HT ON HT.TABLE_NAME = TC.TABLE_NAME");
                    sbSql.Append(" WHERE TC.CONSTRAINT_TYPE = 'PRIMARY KEY' ");
                }

                using (objDbReader = DbFactory.GetDbReader(DisplayDBUpgrade.g_sConnectString, sbSql.ToString()))
                {
                    while (objDbReader.Read())
                    {
                        sbSql.Length = 0;
                        sbSql.Append(" UPDATE HIST_TRACK_DICTIONARY SET PRIMARY_KEY_FLAG = -1 ");
                        sbSql.AppendFormat(" WHERE TABLE_ID ={0}  AND COLUMN_NAME = '{1}'", objDbReader.GetInt32("TABLE_ID"), objDbReader.GetString("COLUMN_NAME"));
                        try
                        {
                            ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sbSql.ToString());
                        }
                        catch (Exception ex)
                        {
                            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error:Updating Primary Key Flag in HIST_TRACK_DICTIONARY table Task - " + ex.Message);
                        }

                    }
                }




            }
            catch (Exception ex)
            {
                DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "-Error: Updating Primary Key Flag in HIST_TRACK_DICTIONARY table Task - " + ex.Message);
            }


            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "===================================================================================");
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Updating Primary Key Flag in HIST_TRACK_DICTIONARY table Task Finished - " + DateTime.Now);
            DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "\r\n");

        }



        /// <summary>
        /// upgrades the RISKMASTER TaskManager database
        /// </summary>
        public static void UpgradeRMTaskManager()
        {
            int iOverall = 0;
            const string STRING_DELIMITER = ",";

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }

            DisplayDBUpgrade.g_TaskManagerConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMTaskManager);
            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_TaskManagerConnectString);

            //get the list of SQL files for the TaskManager database
            string[] arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.TASKMANAGERSCRIPTS, STRING_DELIMITER);

            

            if (arrSQLFiles.Length > 0)
            {
                DisplayDBUpgrade.sODBCName = "TASK MANAGER";

                //create the necessary log file
                CreateLogFile();

                if (!_isSilent)
                {
                    frmWizard.UpdateDBName("TASK MANAGER");
                    frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                }


                foreach (string strSQLFile in arrSQLFiles)
                {
                    iOverall++;

                    if (!_isSilent)
                    {
                        frmWizard.UpdateScriptName(strSQLFile);//display name of script on the update form
                        frmWizard.UpdateCurrentProgressBar(0);
                    }

                    //execute each individual SQL file against the database
                    UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_TaskManagerConnectString, strSQLFile, String.Empty, _isSilent);

                    if (!_isSilent)
                    {
                        frmWizard.UpdateOverallProgressBar(iOverall);
                    }
                }
 
            }
        }

        /// <summary>
        /// UpgradeRMStoreProcedures
        /// </summary>
        public static void UpgradeRMStoreProcedures(string sDbName)
        {
            bool bExecuteSP = false;
            int iOverall = 0;
            int iDB_Make = -1;
            const string STRING_DELIMITER = ",";
            string sSQLFile = String.Empty;
            string sStoredProc = String.Empty;
            // akaushik5 Changed for MITS 33420 Starts
            string specificStoredProc = string.Empty; 
            StringBuilder sqlFiles = new StringBuilder();
            iDB_Make = DisplayDBUpgrade.g_dbMake;

            switch(iDB_Make)
            {
                case 1:
                    specificStoredProc = DisplayDBUpgrade.SQLSTOREDPROCS;
                    break;
                case 4:
                    specificStoredProc = DisplayDBUpgrade.ORACLESTOREDPROCS;
                    break;
                default:
                    break;
            }

            // Specific Stored procs need to be added first 
            if (!string.IsNullOrEmpty(specificStoredProc))
            {
                sqlFiles.AppendFormat("{0}{1}", specificStoredProc, STRING_DELIMITER);
            }

            sqlFiles.Append(DisplayDBUpgrade.STOREDPROCS);

            //get list of all SQL files for RISKMASTER stored procedures
            string[] arrSQLFiles = GetSQLScriptFiles(sqlFiles.ToString(), STRING_DELIMITER);
            // akaushik5 Changed for MITS 33420 Ends

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
                frmWizard.UpdateDBName(sDbName);
                frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
            }

            foreach (string strSQLFile in arrSQLFiles)
            {
                iOverall++;
                sStoredProc = strSQLFile;

                switch (iDB_Make)
                {
                    case 1:
                        sSQLFile = String.Format("{0}_SQL.sql", sStoredProc);
                        break;
                    case 4:
                        sSQLFile = String.Format("{0}_ORACLE.sql", sStoredProc);
                        break;
                    default:
                        break;
                }

                if (!_isSilent)
                {
                    //display name of stored procedure on the update form
                    frmWizard.UpdateScriptName(sStoredProc);
                    frmWizard.UpdateCurrentProgressBar(0);
                }

                //execute each individual SQL file against the database
                bExecuteSP = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_sConnectString, sSQLFile, iDB_Make, _isSilent);

                //execute stored procedure
                if (bExecuteSP)
                {
                    try
                    {
                        switch (iDB_Make)
                        {
                            case 1:
                                ADONetDbAccess.ExecuteStoredProcedure(DisplayDBUpgrade.g_sConnectString, sStoredProc);
                                break;
                            case 4:
                                string strExecuteSP = String.Format("BEGIN {0}; COMMIT; END;", sStoredProc);
                                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, strExecuteSP);
                                break;
                            default:
                                break;
                        }
                    }//try
                    catch (Exception ex)
                    {
                        string strLogFilePath = String.Format(@"{0}\\dBUpgrade.log", Application.StartupPath);
                        DisplayDBUpgrade.LogErrors(strLogFilePath, ex.ToString());
                    }
                }//if

                if (!_isSilent)
                {
                    frmWizard.UpdateOverallProgressBar(iOverall);
                }
            }
        }
        
        /// <summary>
        /// start upgrade process
        /// </summary>
        public static void UpgradeRMDatabase(bool bIsCustom, bool bIsHPUpgrade, bool bIsSilent)
        {
            bool bIsError = false;
            string strSQLFile = String.Empty;
            string strException = String.Empty;
            string strStackTrace = String.Empty;
            string strRunningScript = String.Empty;
            string strErrConnType = String.Empty;
            string strErrConnString = String.Empty;
            StringBuilder strErrorMessage = new StringBuilder();

            bool bInsertViewsOnly = (DisplayDBUpgrade.INSERTVIEWSONLY.ToUpper() == bool.TrueString.ToUpper()) ? true : false;
            //nadim
            bool bRunPrimarykeys = (DisplayDBUpgrade.RunPrimarykey.ToUpper() == bool.TrueString.ToUpper()) ? true : false;
            _isSilent = bIsSilent;

            try
            {
                if (bIsCustom)
                {
                    strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeCustom";
                    //Console.WriteLine(strRunningScript);
                    UpgradeCustom(); //process customized scripts
                }
                else
                {
                    
                    // Added by abhinav- Mits 33063  (Start)
                    OshoMigration();

                    // Ended


                    DisplayDBUpgrade.sBaseLogType = ""; //Govind


                    strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeRMSecurity";
                    //Console.WriteLine(strRunningScript);

                    if (!bInsertViewsOnly)
                    {
                        strErrConnType = "RMXSecurity";
                        strErrConnString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSecurity);
                        UpgradeRMSecurity(bIsHPUpgrade); //process security scripts
                    }

                    strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeRMView";
                    //Console.WriteLine(strRunningScript);

                    strErrConnType = "ViewDataSource";
                    strErrConnString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMViews);
                    UpgradeRMView(); //process view scripts

                    strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeRMBase";
                    //Console.WriteLine(strRunningScript);

                    strErrConnType = "RMX Base DB";
                    DisplayDBUpgrade.sBaseLogType = "Default"; //Govind

                    //UpdateFunctions.ConnectedDB = "RMX Base DB";
                    strErrConnString = DisplayDBUpgrade.g_sConnectString;
                    //UpgradeRMBase(bInsertViewsOnly); //process all other scripts that are dB specific for all selected dB's
                    UpgradeRMBase(bIsHPUpgrade, bInsertViewsOnly); //process all other scripts that are dB specific for all selected dB's
                    UpdateFunctions.ConnectedDB = string.Empty;
                    strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeRMSession";
                    //Console.WriteLine(strRunningScript);

                    strErrConnType = "SessionDataSource";
                    DisplayDBUpgrade.sBaseLogType = ""; //Govind

                    strErrConnString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSession);
                    UpgradeRMSession(); //process session scripts

                    strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeRMTaskManager";

                    strErrConnType = "TaskManagerDataSource";
                    strErrConnString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMTaskManager);
                    UpgradeRMTaskManager(); //process TaskManager scripts

                    //this is old functionality for upgrading views in older db's to newer db's
                    DisplayDBUpgrade.g_ViewConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMViews);
                    strRunningScript = "UpdateFunctions.UpdatePowerViews";
                    //Console.WriteLine(strRunningScript);
                    UpdateFunctions.UpdatePowerViews(_isSilent);

                    ////MGaba2:R7:Upgrading the Audit Database:Start
                    //strErrConnType = "HistoryDataSource";
                    //strErrConnString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMHistory);
                    //strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeRMHistory";                    
                    //UpgradeRMHistory();
                    ////MGaba2:R7:Upgrading the Audit Database:End

                    strErrConnType = "ReportServer";
                    strErrConnString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSortmaster);
                    strRunningScript = "UpgradeScripts.UpgradeRMDatabase.UpgradeSortMaster";

                     UpgradeSortMaster();

                    string sDbchkMangeIndex = frmWizard._sDbchkMangeIndex.ToString().ToUpper();                    
                    //_sDbchkMangeIndex = (sDbchkMangeIndex == bool.TrueString.ToUpper()) ? true : false;


                    //Raman 5/6/2013 : Following tasks would not be run for CHP's

                    //if(!bIsHPUpgrade)
                    if (sDbchkMangeIndex.Equals("TRUE"))
                    {
                        //nadim for rebuilding indexes
                        if (bRunPrimarykeys)
                        {
                            UpgradeIndexes();
                        }
                        //This function will upgrade 
                        UpgradePKInHD();
                        //nadim for running parameterised stored proc
                        UpgradeStoredProcWithParams();
                        //nadim for running parameterised stored proc

                    }                    
                    
                }
            }
            catch (System.Data.Odbc.OdbcException exODBC)
            {
                bIsError = true;
                strException = exODBC.Message;
                strStackTrace = exODBC.StackTrace;
            }
            catch (Exception ex)
            {
                bIsError = true;

                //Format of the initialization string does not conform to specification starting at index 0.
                Regex regex = new Regex(@"^(.*)(Format\sof\sthe\sinitialization\sstring)(.*)$", RegexOptions.IgnoreCase);
                Match match = regex.Match(ex.Message);

                if (match.Success)
                {
                    strException = String.Format("Connection failed.<br />Likely reason: connectionStrings.config has an invalid {0} setting: {1}", strErrConnType, strErrConnString);
                }
                else
                {
                    strException = ex.Message;
                }
                
                strStackTrace = ex.StackTrace;
            }

            string strLogFilePath = String.Format(@"{0}\\dBUpgrade.log", Application.StartupPath);

            if (bIsError)
            {
                string sErrorMessage = "Please check <font color='#000000'>dBUpgrade.log</font> file for errors and " + 
                                       "contact RISKMASTER Support.<br /><br />Press Cancel to close program.";

                strErrorMessage.AppendFormat("Exception Error/StackTrace: {0}", strStackTrace);
                strErrorMessage.AppendLine();
                strErrorMessage.AppendLine();

                if (_isSilent)
                {
                    Console.Error.WriteLine("Error: " + strException);
                }
                else
                {
                    frmWizard.ErrorDisplay(sErrorMessage, "error", DevComponents.DotNetBar.eWizardButtonState.False, strException);
                }

                DisplayDBUpgrade.bStop = true;
            }

            strErrorMessage.AppendLine("dBUgrade Finished: " + DateTime.Now);
            strErrorMessage.AppendLine();
            DisplayDBUpgrade.LogErrors(strLogFilePath, strErrorMessage.ToString());
        }

        private static void EnableBRS()
        {
            bool bIsBRSEnabled = (DisplayDBUpgrade.EnableBRS.ToUpper() == bool.TrueString.ToUpper()) ? true : false;
            string sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = -1 WHERE PARM_NAME = 'USE_BRS'";
            if (bIsBRSEnabled)
            {
                ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_sConnectString, sSQL);
            }
        }

        /// <summary>
        /// creates the error log file (logs all errors that occur on [FORGIVE] lines)
        /// </summary>
        public static void CreateLogFile()
        {
            //get the path to the Error Log file
            string strLogFilePath = DisplayDBUpgrade.GetLogFilePath();

            //append the required text to the Error Log file
            File.AppendAllText(strLogFilePath, "Update " + DisplayDBUpgrade.sODBCName + " Database to RISKMASTER " + DisplayDBUpgrade.DBTARGETVER + " " + DateTime.Now.ToShortDateString());

            //insert a blank line for readability
            File.AppendAllText(strLogFilePath, Environment.NewLine);
        }

        /// <summary>
        /// upgrades the RISKMASTER view database
        /// </summary>
        public static void UpgradeRMView()
        {
            int iOverall = 0;
            const string STRING_DELIMITER = ",";

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }
            
            DisplayDBUpgrade.g_ViewConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMViews);

            //MJP - get the view database make
            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_ViewConnectString);

            //get the list of SQL files for the RISKMASTER view database
            string[] arrViewFiles = GetSQLScriptFiles(DisplayDBUpgrade.VIEWSCRIPTS, STRING_DELIMITER);

            if (arrViewFiles.Length > 0)
            {
                DisplayDBUpgrade.sODBCName = "VIEW";

                //create the necessary log file
                CreateLogFile();

                if (!_isSilent)
                {
                    frmWizard.UpdateDBName("VIEW");
                    frmWizard.SetOverallProgressBarProperties(arrViewFiles.Length);
                }

                foreach (string strSQLFile in arrViewFiles)
                {
                    iOverall++;

                    if (!_isSilent)
                    {
                        frmWizard.UpdateScriptName(strSQLFile);//display name of script on the update form
                        frmWizard.UpdateCurrentProgressBar(0);
                    }

                    //execute each individual SQL file against the database
                    UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_ViewConnectString, strSQLFile, String.Empty, _isSilent);

                    if (!_isSilent)
                    {
                        frmWizard.UpdateOverallProgressBar(iOverall);
                    }
                }
            }
        }

        /// <summary>
        /// upgrades the RISKMASTER view database
        /// </summary>
        public static void UpgradeCustomScriptForView()
        {
            int iOverall = 0;
            const string STRING_DELIMITER = ",";

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }

            DisplayDBUpgrade.g_ViewConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMViews);

            //MJP - get the view database make
            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_ViewConnectString);

            //get the list of SQL files for the RISKMASTER view database
            string[] arrViewFiles = GetSQLScriptFiles(DisplayDBUpgrade.VIEWSCRIPTS, STRING_DELIMITER);

            if (arrViewFiles.Length > 0)
            {
                DisplayDBUpgrade.sODBCName = "VIEW";

                //create the necessary log file
                CreateLogFile();

                if (!_isSilent)
                {
                    frmWizard.UpdateDBName("VIEW");
                    frmWizard.SetOverallProgressBarProperties(arrViewFiles.Length);
                }

                foreach (string strSQLFile in arrViewFiles)
                {
                    iOverall++;

                    if (!_isSilent)
                    {
                        frmWizard.UpdateScriptName(strSQLFile);//display name of script on the update form
                        frmWizard.UpdateCurrentProgressBar(0);
                    }

                    //execute each individual SQL file against the database
                    UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_ViewConnectString, strSQLFile, String.Empty, _isSilent);

                    if (!_isSilent)
                    {
                        frmWizard.UpdateOverallProgressBar(iOverall);
                    }
                }
            }
        }

        /// <summary>
        /// upgrades the RISKMASTER Security database
        /// </summary>
        public static void UpgradeCustomScriptForSecurity()
        {
            int iOverall = 0;
            const string STRING_DELIMITER = ",";

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }

            DisplayDBUpgrade.g_SecConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSecurity);

            //MJP - get the view database make
            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_SecConnectString);

            //get the list of SQL files for the RISKMASTER view database
            string[] arrViewFiles = GetSQLScriptFiles(DisplayDBUpgrade.SECURITYSCRIPTS, STRING_DELIMITER);

            if (arrViewFiles.Length > 0)
            {
                DisplayDBUpgrade.sODBCName = "SECURITY";

                //create the necessary log file
                CreateLogFile();

                if (!_isSilent)
                {
                    frmWizard.UpdateDBName("SECURITY");
                    frmWizard.SetOverallProgressBarProperties(arrViewFiles.Length);
                }

                foreach (string strSQLFile in arrViewFiles)
                {
                    iOverall++;

                    if (!_isSilent)
                    {
                        frmWizard.UpdateScriptName(strSQLFile);//display name of script on the update form
                        frmWizard.UpdateCurrentProgressBar(0);
                    }

                    //execute each individual SQL file against the database
                    UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_SecConnectString, strSQLFile, String.Empty, _isSilent);

                    if (!_isSilent)
                    {
                        frmWizard.UpdateOverallProgressBar(iOverall);
                    }
                }
            }
        }

        /// <summary>
        /// MGaba2:R7:upgrades the RISKMASTER History Database
        /// </summary>
        //public static void UpgradeRMHistory()
        //{
        //    int iOverall = 0;
        //    const string STRING_DELIMITER = ",";

        //    if (!_isSilent)
        //    {
        //        //reset the progressbars on wizard processing page
        //        frmWizard.SetOverallProgressBarProperties(0);
        //        frmWizard.SetCurrentProgressBarProperties(0);
        //        frmWizard.UpdateCurrentProgressBar(0);
        //        frmWizard.UpdateOverallProgressBar(0);
        //    }

        //    DisplayDBUpgrade.g_HistTrackConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMHistory);

        //    DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_HistTrackConnectString);

        //    //get the list of SQL files for the RISKMASTER view database
        //    string[] arrScriptFiles = GetSQLScriptFiles(DisplayDBUpgrade.HISTORYTRACKINGSCRIPTS, STRING_DELIMITER);

        //    if (arrScriptFiles.Length > 0)
        //    {
        //        DisplayDBUpgrade.sODBCName = "HISTORY";

        //        //create the necessary log file
        //        CreateLogFile();

        //        if (!_isSilent)
        //        {
        //            frmWizard.UpdateDBName("HISTORY");
        //            frmWizard.SetOverallProgressBarProperties(arrScriptFiles.Length+1);
        //        }
        //        if (!_isSilent)
        //        {
        //            frmWizard.UpdateScriptName("HistTrackSync");//display name of script on the update form
        //            frmWizard.UpdateCurrentProgressBar(0);
        //        }
        //        //Execute each sql statement of HIST_TRACK_SYNC table on History Tracking database.
        //        UpdateFunctions.ExecuteHistTrackSyncSQL(DisplayDBUpgrade.g_HistTrackConnectString, _isSilent);

        //        foreach (string strSQLFile in arrScriptFiles)
        //        {
        //            iOverall++;

        //            if (!_isSilent)
        //            {
        //                frmWizard.UpdateScriptName(strSQLFile);//display name of script on the update form
        //                frmWizard.UpdateCurrentProgressBar(0);
        //            }

        //            //execute each individual SQL file against the database
        //            UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_HistTrackConnectString, strSQLFile, String.Empty, _isSilent);

        //            if (!_isSilent)
        //            {
        //                frmWizard.UpdateOverallProgressBar(iOverall);
        //            }
        //        }                
        //    }
        //    UpgradeRMHistoryStoreProcedures();
        //}
        /* FUTURE IMPLEMENTATION
        public static void UpgradeHP()
        {
            const string STRING_DELIMITER = ",";

            int i = DisplayDBUpgrade.dictPassedItems.Count;

            if (_isSilent)
            {
                Console.WriteLine("# of items in dictPassedItems: " + i.ToString());
            }
             //run dBUpgrade on selected datasources
            foreach (KeyValuePair<string, int> kvp in DisplayDBUpgrade.dictPassedItems)
            {
                if (!_isSilent)
                {
                    //reset the progressbars on wizard processing page for each selected dB
                    frmWizard.SetOverallProgressBarProperties(0);
                    frmWizard.SetCurrentProgressBarProperties(0);
                    frmWizard.UpdateCurrentProgressBar(0);
                    frmWizard.UpdateOverallProgressBar(0);
                }
                else
                {
                    Console.WriteLine("key: " + kvp.Key + " | value: " + kvp.Value);
                }

                //get the list of HP SQL files
                string[] arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.QUICKHPUPGRADESCRIPTS, STRING_DELIMITER);

                int iOverall = 0;

                if (arrSQLFiles.Length > 0)
                {
                    DisplayDBUpgrade.sODBCName = kvp.Key;
                    DisplayDBUpgrade.g_iDSNID = kvp.Value;

                    Console.WriteLine("g_SecConnectString: " + DisplayDBUpgrade.g_SecConnectString);

                    using (var dbDSNRdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, RISKMASTERScripts.GetSelectedDSNInfo(DisplayDBUpgrade.g_iDSNID)))
                    {
                        while (dbDSNRdr.Read())
                        {
                            //form a connection string so we can connect and do rest of upgrade
                            DisplayDBUpgrade.g_sConnectString = RISKMASTERScripts.BuildConnectionString(dbDSNRdr["CONNECTION_STRING"].ToString(), RMCryptography.DecryptString(dbDSNRdr["RM_USERID"].ToString()), RMCryptography.DecryptString(dbDSNRdr["RM_PASSWORD"].ToString()));

                            DisplayDBUpgrade.CheckForOracleString(ref DisplayDBUpgrade.g_sConnectString);

                            Console.WriteLine("g_sConnectString: " + DisplayDBUpgrade.g_sConnectString);

                            //get the database make
                            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);

                            //get the database release and version information
                            DisplayDBUpgrade.gsDBRelease = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT RELEASE_NUMBER FROM SYS_PARMS");
                            DisplayDBUpgrade.gsDBVersion = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT DTG_VERSION FROM SYS_PARMS");

                            if (dbDSNRdr["ORGSEC_FLAG"].ToString() != "0")
                            {
                                DisplayDBUpgrade.g_bOrgSecOn = true;
                            }
                        }
                    }

                    //create the necessary log file
                    CreateLogFile();

                    //MJP - this hides the password so it doesn't record into the logfile...
                    string sPasswordHiddenConnString = String.Empty;
                    string[] result = DisplayDBUpgrade.g_sConnectString.Split(new string[] { "PWD" }, StringSplitOptions.None);
                    sPasswordHiddenConnString = String.Format(@"{0}PWD=*** NOT SHOWN ***;", result[0]);

                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "DSN ID: " + DisplayDBUpgrade.g_iDSNID + "\r\n");
                    DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Connection String: " + sPasswordHiddenConnString + "\r\n");
                    if (!_isSilent)
                    {
                        frmWizard.UpdateDBName(kvp.Key);
                        frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                    }

                    foreach (string strSQLFile in arrSQLFiles)
                    {
                        iOverall++;

                        if (!_isSilent)
                        {
                            //display name of script on the update form
                            frmWizard.UpdateScriptName(strSQLFile);
                            frmWizard.UpdateCurrentProgressBar(0);
                        }

                        //execute each individual SQL file against the database
                        UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_sConnectString, strSQLFile, String.Empty, _isSilent);

                        if (!_isSilent)
                        {
                            frmWizard.UpdateOverallProgressBar(iOverall);
                        }
                    }
                }
                else
                {
                    if (!_isSilent)
                    {
                        string sErrorMessage = "HP scripts were not found.<br /><br />Contact RISKMASTER for further support.<br /><br />Press Cancel to close program.";
                        frmWizard.ErrorDisplay(sErrorMessage, "error", DevComponents.DotNetBar.eWizardButtonState.False, String.Empty);
                    }
                    else
                    {
                        string sErrorMessage = "HP scripts were not found.\n\nContact RISKMASTER for further support.\n";
                        Console.Error.WriteLine(sErrorMessage);
                    }

                    DisplayDBUpgrade.bStop = true;
                    break;
                }
            }
        
        }
        */

        
        /// <summary>
        /// upgrades RISKMASTER database(s) with customized scripts
        /// </summary>
        public static void UpgradeCustom()
        {
            const string STRING_DELIMITER = ",";

            int i = DisplayDBUpgrade.dictPassedItems.Count;

            if (_isSilent)
            {
                Console.WriteLine("# of items in dictPassedItems: " + i.ToString());
            }

            if (DisplayDBUpgrade.CUSTOMVIEWONLY == "true")
            {
                UpgradeCustomScriptForView();
            }
            else if (DisplayDBUpgrade.CUSTOMSECURITYONLY == "true")
            {
                UpgradeCustomScriptForSecurity();
            }
            else
            {
                //run dBUpgrade on selected datasources
                foreach (KeyValuePair<string, int> kvp in DisplayDBUpgrade.dictPassedItems)
                {
                    if (!_isSilent)
                    {
                        //reset the progressbars on wizard processing page for each selected dB
                        frmWizard.SetOverallProgressBarProperties(0);
                        frmWizard.SetCurrentProgressBarProperties(0);
                        frmWizard.UpdateCurrentProgressBar(0);
                        frmWizard.UpdateOverallProgressBar(0);
                    }
                    else
                    {
                        Console.WriteLine("key: " + kvp.Key + " | value: " + kvp.Value);
                    }

                    //get the list of custom SQL files
                    string[] arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.CUSTOMSCRIPTS, STRING_DELIMITER);

                    int iOverall = 0;

                    if (arrSQLFiles.Length > 0)
                    {
                        DisplayDBUpgrade.sODBCName = kvp.Key;
                        DisplayDBUpgrade.g_iDSNID = kvp.Value;

                        Console.WriteLine("g_SecConnectString: " + DisplayDBUpgrade.g_SecConnectString);

                        using (var dbDSNRdr = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, RISKMASTERScripts.GetSelectedDSNInfo(DisplayDBUpgrade.g_iDSNID)))
                        {
                            while (dbDSNRdr.Read())
                            {
                                //form a connection string so we can connect and do rest of upgrade
                                DisplayDBUpgrade.g_sConnectString = RISKMASTERScripts.BuildConnectionString(dbDSNRdr["CONNECTION_STRING"].ToString(), RMCryptography.DecryptString(dbDSNRdr["RM_USERID"].ToString()), RMCryptography.DecryptString(dbDSNRdr["RM_PASSWORD"].ToString()));

                                DisplayDBUpgrade.CheckForOracleString(ref DisplayDBUpgrade.g_sConnectString);

                                Console.WriteLine("g_sConnectString: " + DisplayDBUpgrade.g_sConnectString);

                                //get the database make
                                DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_sConnectString);

                                //get the database release and version information
                                DisplayDBUpgrade.gsDBRelease = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT RELEASE_NUMBER FROM SYS_PARMS");
                                DisplayDBUpgrade.gsDBVersion = ADONetDbAccess.ExecuteString(DisplayDBUpgrade.g_sConnectString, "SELECT DTG_VERSION FROM SYS_PARMS");

                                if (dbDSNRdr["ORGSEC_FLAG"].ToString() != "0")
                                {
                                    DisplayDBUpgrade.g_bOrgSecOn = true;
                                }
                            }
                        }

                        //create the necessary log file
                        CreateLogFile();

                        //MJP - this hides the password so it doesn't record into the logfile...
                        string sPasswordHiddenConnString = String.Empty;
                        string[] result = DisplayDBUpgrade.g_sConnectString.Split(new string[] { "PWD" }, StringSplitOptions.None);
                        sPasswordHiddenConnString = String.Format(@"{0}PWD=*** NOT SHOWN ***;", result[0]);

                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "DSN ID: " + DisplayDBUpgrade.g_iDSNID + "\r\n");
                        DisplayDBUpgrade.LogErrors(DisplayDBUpgrade.GetLogFilePath(), "Connection String: " + sPasswordHiddenConnString + "\r\n");
                        if (!_isSilent)
                        {
                            frmWizard.UpdateDBName(kvp.Key);
                            frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
                        }

                        foreach (string strSQLFile in arrSQLFiles)
                        {
                            iOverall++;

                            if (!_isSilent)
                            {
                                //display name of script on the update form
                                frmWizard.UpdateScriptName(strSQLFile);
                                frmWizard.UpdateCurrentProgressBar(0);
                            }

                            //execute each individual SQL file against the database
                            UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_sConnectString, strSQLFile, String.Empty, _isSilent);

                            if (!_isSilent)
                            {
                                frmWizard.UpdateOverallProgressBar(iOverall);
                            }
                        }
                    }
                    else
                    {
                        if (!_isSilent)
                        {
                            string sErrorMessage = "Custom scripts were not found.<br /><br />Contact RISKMASTER for further support.<br /><br />Press Cancel to close program.";
                            frmWizard.ErrorDisplay(sErrorMessage, "error", DevComponents.DotNetBar.eWizardButtonState.False, String.Empty);
                        }
                        else
                        {
                            string sErrorMessage = "Custom scripts were not found.\n\nContact RISKMASTER for further support.\n";
                            Console.Error.WriteLine(sErrorMessage);
                        }

                        DisplayDBUpgrade.bStop = true;
                        break;
                    }
                }
            }
        }

        /// <summary>
        ///MGaba2:R7:This function will insert and execute stored procedures on History Database
        /// </summary>
        //public static void UpgradeRMHistoryStoreProcedures()
        //{
        //    bool bExecuteSP = false;
        //    int iOverall = 0;
        //    int iDB_Make = -1;
        //    const string STRING_DELIMITER = ",";
        //    string sSQLFile = String.Empty;
        //    string sStoredProc = String.Empty;

        //    //get list of all SQL files for RISKMASTER History stored procedures
        //    string[] arrSQLFiles = GetSQLScriptFiles(DisplayDBUpgrade.HISTORYSTOREDPROCS, STRING_DELIMITER);

        //    if (!_isSilent)
        //    {
        //        //reset the progressbars on wizard processing page
        //        frmWizard.SetOverallProgressBarProperties(0);
        //        frmWizard.SetCurrentProgressBarProperties(0);
        //        frmWizard.UpdateCurrentProgressBar(0);
        //        frmWizard.UpdateOverallProgressBar(0);
        //        frmWizard.UpdateDBName("HISTORY");
        //        frmWizard.SetOverallProgressBarProperties(arrSQLFiles.Length);
        //    }

        //    iDB_Make = DisplayDBUpgrade.g_dbMake;

        //    foreach (string strSQLFile in arrSQLFiles)
        //    {
        //        iOverall++;
        //        sStoredProc = strSQLFile;

        //        switch (iDB_Make)
        //        {
        //            case 1:
        //                sSQLFile = String.Format("{0}_SQL.sql", sStoredProc);
        //                break;
        //            case 4:
        //                sSQLFile = String.Format("{0}_ORACLE.sql", sStoredProc);
        //                break;
        //            default:
        //                break;
        //        }

        //        if (!_isSilent)
        //        {
        //            //display name of stored procedure on the update form
        //            frmWizard.UpdateScriptName(sStoredProc);
        //            frmWizard.UpdateCurrentProgressBar(0);
        //        }

        //        //execute each individual SQL file against the database
        //        bExecuteSP = UpdateFunctions.InsertStoredProcedure(DisplayDBUpgrade.g_HistTrackConnectString, sSQLFile, iDB_Make, _isSilent);

        //        //execute stored procedure
        //        if (bExecuteSP)
        //        {
        //            try
        //            {
        //                switch (iDB_Make)
        //                {
        //                    case 1:
        //                        ADONetDbAccess.ExecuteStoredProcedure(DisplayDBUpgrade.g_HistTrackConnectString, sStoredProc);
        //                        break;
        //                    case 4:
        //                        string strExecuteSP = String.Format("BEGIN {0}; COMMIT; END;", sStoredProc);
        //                        ADONetDbAccess.ExecuteNonQuery(DisplayDBUpgrade.g_HistTrackConnectString, strExecuteSP);
        //                        break;
        //                    default:
        //                        break;
        //                }
        //            }//try
        //            catch (Exception ex)
        //            {
        //                string strLogFilePath = String.Format(@"{0}\\dBUpgrade.log", Application.StartupPath);
        //                DisplayDBUpgrade.LogErrors(strLogFilePath, ex.ToString());
        //            }
        //        }//if

        //        if (!_isSilent)
        //        {
        //            frmWizard.UpdateOverallProgressBar(iOverall);
        //        }
        //    }
        //}

        public static void UpgradeSortMaster()
        {
            int iOverall = 0;
            const string STRING_DELIMITER = ",";

            if (!_isSilent)
            {
                //reset the progressbars on wizard processing page
                frmWizard.SetOverallProgressBarProperties(0);
                frmWizard.SetCurrentProgressBarProperties(0);
                frmWizard.UpdateCurrentProgressBar(0);
                frmWizard.UpdateOverallProgressBar(0);
            }

            DisplayDBUpgrade.g_ReportServer_ConnectionString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSortmaster);

            DisplayDBUpgrade.g_dbMake = DisplayDBUpgrade.GetDatabaseMake(DisplayDBUpgrade.g_ReportServer_ConnectionString);

            //get the list of SQL files for the RISKMASTER view database
            string[] arrScriptFiles = GetSQLScriptFiles(DisplayDBUpgrade.RMSORTMASTERSCRIPTS, STRING_DELIMITER);

            if (arrScriptFiles.Length > 0)
            {
                DisplayDBUpgrade.sODBCName = "SORTMASTER";

                //create the necessary log file
                CreateLogFile();

                if (!_isSilent)
                {
                    frmWizard.UpdateDBName("SORTMASTER");
                    frmWizard.SetOverallProgressBarProperties(arrScriptFiles.Length + 1);
                }
                if (!_isSilent)
                {
                    frmWizard.UpdateScriptName("SortMaster");//display name of script on the update form
                    frmWizard.UpdateCurrentProgressBar(0);
                }
                //Execute each sql statement of HIST_TRACK_SYNC table on History Tracking database.
                //UpdateFunctions.ExecuteHistTrackSyncSQL(DisplayDBUpgrade.g_ReportServer_ConnectionString, _isSilent);

                foreach (string strSQLFile in arrScriptFiles)
                {
                    iOverall++;

                    if (!_isSilent)
                    {
                        frmWizard.UpdateScriptName(strSQLFile);//display name of script on the update form
                        frmWizard.UpdateCurrentProgressBar(0);
                    }

                    //execute each individual SQL file against the database
                    UpdateFunctions.ExecuteSQLScriptExtended(DisplayDBUpgrade.g_ReportServer_ConnectionString, strSQLFile, String.Empty, _isSilent);

                    if (!_isSilent)
                    {
                        frmWizard.UpdateOverallProgressBar(iOverall);
                    }
                }
            }
        }

    }
}

