[GO_DELIMITER]
CREATE OR REPLACE
PROCEDURE USP_REBUILD_ORG_HIERARCHY (sRM_USERID VARCHAR2)
 AUTHID CURRENT_USER IS  
 BEGIN  
    DECLARE  
    iTableCount INTEGER; 
    GroupId VARCHAR2(100);  
    GroupEntities VARCHAR2(4000);   
    sSql VARCHAR2(4000);  
    GROUPIDCURSOR SYS_REFCURSOR;  
    sSqlCursor VARCHAR2(2000); 
    
    BEGIN
        
        -- Drop the Table if already exists 
        iTableCount:=0;
        SELECT COUNT(*) INTO iTableCount FROM ALL_OBJECTS WHERE OBJECT_TYPE = 'TABLE' AND OBJECT_NAME='OH_MAP_TMP' AND OWNER=UPPER(sRM_USERID); 
        
        IF (iTableCount=1) THEN 
            sSQL:='DROP TABLE '||sRM_USERID||'.OH_MAP_TMP'; 
            EXECUTE IMMEDIATE sSQL; 
        END IF;  
        
        iTableCount:=0;
        SELECT COUNT(*) INTO iTableCount FROM ALL_OBJECTS WHERE OBJECT_TYPE = 'TABLE' AND OBJECT_NAME='ENTITY_MAP_TEMP' AND OWNER=UPPER(sRM_USERID); 
        
        IF (iTableCount=1) THEN 
            sSQL:='DROP TABLE '||sRM_USERID||'.ENTITY_MAP_TEMP'; 
            EXECUTE IMMEDIATE sSQL; 
        END IF;
        
        -- Create the Temporary table for storing the Org Hierarchy details
        sSql:=  'CREATE TABLE '||sRM_USERID||'.OH_MAP_TMP NOLOGGING AS 
                  SELECT DISTINCT  DEPARTMENT.ENTITY_ID AS DEPARTMENT_EID,
                        FACILITY.ENTITY_ID AS FACILITY_EID, 
                        LOCATION.ENTITY_ID AS LOCATION_EID,
                        DIVISION.ENTITY_ID AS DIVISION_EID, 
                        REGION.ENTITY_ID AS REGION_EID,
                        OPERATION.ENTITY_ID AS OPERATION_EID, 
                        COMPANY.ENTITY_ID AS COMPANY_EID, 
                        CLIENT.ENTITY_ID AS CLIENT_EID 
                  FROM  '||sRM_USERID||'.ENTITY CLIENT LEFT OUTER JOIN '||sRM_USERID||'.ENTITY COMPANY
                        ON COMPANY.PARENT_EID = CLIENT.ENTITY_ID
                        LEFT OUTER JOIN '||sRM_USERID||'.ENTITY OPERATION
                        ON OPERATION.PARENT_EID = COMPANY.ENTITY_ID
                        LEFT OUTER JOIN '||sRM_USERID||'.ENTITY REGION
                        ON REGION.PARENT_EID = OPERATION.ENTITY_ID
                        LEFT OUTER JOIN '||sRM_USERID||'.ENTITY DIVISION
                        ON DIVISION.PARENT_EID = REGION.ENTITY_ID
                        LEFT OUTER JOIN '||sRM_USERID||'.ENTITY LOCATION
                        ON LOCATION.PARENT_EID = DIVISION.ENTITY_ID
                        LEFT OUTER JOIN '||sRM_USERID||'.ENTITY FACILITY
                        ON FACILITY.PARENT_EID = LOCATION.ENTITY_ID
                        LEFT OUTER JOIN '||sRM_USERID||'.ENTITY DEPARTMENT
                        ON DEPARTMENT.PARENT_EID = FACILITY.ENTITY_ID 
                  WHERE 
                        CLIENT.ENTITY_TABLE_ID = 1005';
       
        EXECUTE IMMEDIATE sSql;
        -- Create Indexes for Fast Retrival                
        sSql:= 'CREATE INDEX OHT_DEPT_IDX ON '||sRM_USERID||'.OH_MAP_TMP(DEPARTMENT_EID)';
        EXECUTE IMMEDIATE sSql;
        sSql:= 'CREATE INDEX OHT_FAC_IDX ON '||sRM_USERID||'.OH_MAP_TMP(FACILITY_EID)';
        EXECUTE IMMEDIATE sSql;
        sSql:= 'CREATE INDEX OHT_LOC_IDX ON '||sRM_USERID||'.OH_MAP_TMP(LOCATION_EID)';
        EXECUTE IMMEDIATE sSql;
        sSql:= 'CREATE INDEX OHT_DIV_IDX ON '||sRM_USERID||'.OH_MAP_TMP(DIVISION_EID)';
        EXECUTE IMMEDIATE sSql;
        sSql:= 'CREATE INDEX OHT_REG_IDX ON '||sRM_USERID||'.OH_MAP_TMP(REGION_EID)';
        EXECUTE IMMEDIATE sSql;
        sSql:= 'CREATE INDEX OHT_OP_IDX ON '||sRM_USERID||'.OH_MAP_TMP(OPERATION_EID)';
        EXECUTE IMMEDIATE sSql;
        sSql:= 'CREATE INDEX OHT_COMP_IDX ON '||sRM_USERID||'.OH_MAP_TMP(COMPANY_EID)';
        EXECUTE IMMEDIATE sSql;
        sSql:= 'CREATE INDEX OHT_CLIENT_IDX ON '||sRM_USERID||'.OH_MAP_TMP(CLIENT_EID)'; 
        EXECUTE IMMEDIATE sSql;
        
        sSql:=  'CREATE TABLE '||sRM_USERID||'.ENTITY_MAP_TEMP (ENTITY_ID INT NULL, GROUP_ID INT NULL)';
        EXECUTE IMMEDIATE sSql;
        

        -- Loop through all the Groups and Process it while adding data to Entity Map
        
        sSqlCursor := 'SELECT '||sRM_USERID||'.ORG_SECURITY.GROUP_ID, GROUP_ENTITIES 
                        FROM '||sRM_USERID||'.ORG_SECURITY LEFT OUTER JOIN '||sRM_USERID||'.ORG_SECURITY_USER
                        ON '||sRM_USERID||'.ORG_SECURITY.GROUP_ID = '||sRM_USERID||'.ORG_SECURITY_USER.GROUP_ID
                        WHERE GROUP_ENTITIES <> ''<ALL>''';
        BEGIN  
            OPEN GROUPIDCURSOR FOR sSqlCursor;  
                LOOP  
                    FETCH GROUPIDCURSOR INTO GroupId, GroupEntities;  
                    EXIT WHEN GROUPIDCURSOR%NOTFOUND;  
                
                    -- Process the Departments in Entity Map for the same Group Id
                    sSql :=   'INSERT INTO '||sRM_USERID||'.ENTITY_MAP_TEMP(GROUP_ID,ENTITY_ID) 
                          SELECT DISTINCT ' || GroupId || ',DEPARTMENT_EID FROM '||sRM_USERID||'.OH_MAP_TMP
                          WHERE DEPARTMENT_EID IN (' || GroupEntities || ')
                                OR FACILITY_EID IN (' || GroupEntities || ')
                                OR LOCATION_EID IN (' || GroupEntities || ')
                                OR DIVISION_EID IN (' || GroupEntities || ')
                                OR REGION_EID IN (' || GroupEntities || ')
                                OR OPERATION_EID IN (' || GroupEntities || ')
                                OR COMPANY_EID IN (' || GroupEntities || ')
                                OR CLIENT_EID IN (' || GroupEntities || ') ';
                                
                    EXECUTE IMMEDIATE sSql;  
                
                    sSql :=   'INSERT INTO '||sRM_USERID||'.ENTITY_MAP_TEMP(GROUP_ID,ENTITY_ID) 
                          SELECT DISTINCT ' || GroupId || ',FACILITY_EID FROM '||sRM_USERID||'.OH_MAP_TMP
                          WHERE DEPARTMENT_EID IN (' || GroupEntities || ')
                                OR FACILITY_EID IN (' || GroupEntities || ')
                                OR LOCATION_EID IN (' || GroupEntities || ')
                                OR DIVISION_EID IN (' || GroupEntities || ')
                                OR REGION_EID IN (' || GroupEntities || ')
                                OR OPERATION_EID IN (' || GroupEntities || ')
                                OR COMPANY_EID IN (' || GroupEntities || ')
                                OR CLIENT_EID IN (' || GroupEntities || ') ';
                
                    EXECUTE IMMEDIATE sSql;
                
                    sSql :=   'INSERT INTO '||sRM_USERID||'.ENTITY_MAP_TEMP(GROUP_ID,ENTITY_ID) 
                          SELECT DISTINCT ' || GroupId || ',LOCATION_EID FROM '||sRM_USERID||'.OH_MAP_TMP
                          WHERE DEPARTMENT_EID IN (' || GroupEntities || ')
                                OR FACILITY_EID IN (' || GroupEntities || ')
                                OR LOCATION_EID IN (' || GroupEntities || ')
                                OR DIVISION_EID IN (' || GroupEntities || ')
                                OR REGION_EID IN (' || GroupEntities || ')
                                OR OPERATION_EID IN (' || GroupEntities || ')
                                OR COMPANY_EID IN (' || GroupEntities || ')
                                OR CLIENT_EID IN (' || GroupEntities || ') ';
                
                    EXECUTE IMMEDIATE sSql;                    
                
                    sSql :=   'INSERT INTO '||sRM_USERID||'.ENTITY_MAP_TEMP(GROUP_ID,ENTITY_ID) 
                          SELECT DISTINCT ' || GroupId || ',DIVISION_EID FROM '||sRM_USERID||'.OH_MAP_TMP
                          WHERE DEPARTMENT_EID IN (' || GroupEntities || ')
                                OR FACILITY_EID IN (' || GroupEntities || ')
                                OR LOCATION_EID IN (' || GroupEntities || ')
                                OR DIVISION_EID IN (' || GroupEntities || ')
                                OR REGION_EID IN (' || GroupEntities || ')
                                OR OPERATION_EID IN (' || GroupEntities || ')
                                OR COMPANY_EID IN (' || GroupEntities || ')
                                OR CLIENT_EID IN (' || GroupEntities || ') ';
                
                    EXECUTE IMMEDIATE sSql;
                                    
                    sSql :=   'INSERT INTO '||sRM_USERID||'.ENTITY_MAP_TEMP(GROUP_ID,ENTITY_ID) 
                          SELECT DISTINCT ' || GroupId || ',REGION_EID FROM '||sRM_USERID||'.OH_MAP_TMP
                          WHERE DEPARTMENT_EID IN (' || GroupEntities || ')
                                OR FACILITY_EID IN (' || GroupEntities || ')
                                OR LOCATION_EID IN (' || GroupEntities || ')
                                OR DIVISION_EID IN (' || GroupEntities || ')
                                OR REGION_EID IN (' || GroupEntities || ')
                                OR OPERATION_EID IN (' || GroupEntities || ')
                                OR COMPANY_EID IN (' || GroupEntities || ')
                                OR CLIENT_EID IN (' || GroupEntities || ') ';                                        
                
                    EXECUTE IMMEDIATE sSql;                                     
                
                    sSql :=   'INSERT INTO '||sRM_USERID||'.ENTITY_MAP_TEMP(GROUP_ID,ENTITY_ID) 
                          SELECT DISTINCT ' || GroupId || ',OPERATION_EID FROM '||sRM_USERID||'.OH_MAP_TMP
                          WHERE DEPARTMENT_EID IN (' || GroupEntities || ')
                                OR FACILITY_EID IN (' || GroupEntities || ')
                                OR LOCATION_EID IN (' || GroupEntities || ')
                                OR DIVISION_EID IN (' || GroupEntities || ')
                                OR REGION_EID IN (' || GroupEntities || ')
                                OR OPERATION_EID IN (' || GroupEntities || ')
                                OR COMPANY_EID IN (' || GroupEntities || ')
                                OR CLIENT_EID IN (' || GroupEntities || ') ';  
               
                    EXECUTE IMMEDIATE sSql;                                     
                
                    sSql :=   'INSERT INTO '||sRM_USERID||'.ENTITY_MAP_TEMP(GROUP_ID,ENTITY_ID) 
                          SELECT DISTINCT ' || GroupId || ',COMPANY_EID FROM '||sRM_USERID||'.OH_MAP_TMP
                          WHERE DEPARTMENT_EID IN (' || GroupEntities || ')
                                OR FACILITY_EID IN (' || GroupEntities || ')
                                OR LOCATION_EID IN (' || GroupEntities || ')
                                OR DIVISION_EID IN (' || GroupEntities || ')
                                OR REGION_EID IN (' || GroupEntities || ')
                                OR OPERATION_EID IN (' || GroupEntities || ')
                                OR COMPANY_EID IN (' || GroupEntities || ')
                                OR CLIENT_EID IN (' || GroupEntities || ') ';  

                    EXECUTE IMMEDIATE sSql;
                
                    sSql :=   'INSERT INTO '||sRM_USERID||'.ENTITY_MAP_TEMP(GROUP_ID,ENTITY_ID) 
                          SELECT DISTINCT ' || GroupId || ',CLIENT_EID FROM '||sRM_USERID||'.OH_MAP_TMP
                          WHERE DEPARTMENT_EID IN (' || GroupEntities || ')
                                OR FACILITY_EID IN (' || GroupEntities || ')
                                OR LOCATION_EID IN (' || GroupEntities || ')
                                OR DIVISION_EID IN (' || GroupEntities || ')
                                OR REGION_EID IN (' || GroupEntities || ')
                                OR OPERATION_EID IN (' || GroupEntities || ')
                                OR COMPANY_EID IN (' || GroupEntities || ')
                                OR CLIENT_EID IN (' || GroupEntities || ') ';  
                
                    EXECUTE IMMEDIATE sSql;
                    
                    COMMIT;  
                    
                    -- Handled the case when Sec_Dept_eid is Null or 0. For Example when a Payment is made directly
					-- in That case Sec_Dept_eid in Funds will be 0.
                    sSql :=  'INSERT INTO '||sRM_USERID||'.ENTITY_MAP_TEMP (GROUP_ID, ENTITY_ID) 
									VALUES (' || GroupId || ',0)';
					EXECUTE IMMEDIATE sSql;
					
                END LOOP;  
            CLOSE GROUPIDCURSOR;  
        END;  
             
        -- Drop the Table
        sSql := 'DROP TABLE '||sRM_USERID||'.ENTITY_MAP';
        EXECUTE IMMEDIATE sSql;
         
        sSql :=  'CREATE TABLE '||sRM_USERID||'.ENTITY_MAP NOLOGGING AS SELECT DISTINCT ENTITY_ID,GROUP_ID FROM '||sRM_USERID||'.ENTITY_MAP_TEMP WHERE ENTITY_ID IS NOT NULL';
        EXECUTE IMMEDIATE sSql;
        
        sSql :=  'ALTER TABLE '||sRM_USERID||'.ENTITY_MAP MODIFY ENTITY_ID INT NOT NULL';
        EXECUTE IMMEDIATE sSql;
		sSql :=  'ALTER TABLE '||sRM_USERID||'.ENTITY_MAP MODIFY GROUP_ID INT NOT NULL';
		EXECUTE IMMEDIATE sSql;
		sSql :=  'CREATE UNIQUE INDEX PK_ENTITY_MAP ON '||sRM_USERID||'.ENTITY_MAP(ENTITY_ID,GROUP_ID)';
		EXECUTE IMMEDIATE sSql;
        
        sSql := 'UPDATE '||sRM_USERID||'.ORG_SECURITY SET UPDATED_FLAG = -1 WHERE GROUP_ENTITIES <> ''<ALL>''';
        EXECUTE IMMEDIATE sSql;
    END;
END USP_REBUILD_ORG_HIERARCHY;  
GO
