[GO_DELIMITER]
create or replace
procedure USP_MOVE_ENTITY_ADDRESSES   
 AS   
 BEGIN   
     DECLARE   
          ADDRESSID INTEGER;  
          MAXADDRESS_ID INTEGER;  
          PHONEID INTEGER;  
          MAXPHONE_ID INTEGER;  
          OFFICE_CODEID INTEGER;  
          HOME_CODEID INTEGER;
          ADDRESS_COUNT INTEGER;
	  PHONE_COUNT INTEGER;
          
           sSQL VARCHAR2(2000);         	 
           iMinID INTEGER;
           iMaxID INTEGER;
           iLoopCount INTEGER;
           iFLAG INTEGER;
             
     BEGIN 
               SELECT COUNT(ADDRESS_ID) INTO ADDRESS_COUNT FROM ENTITY_X_ADDRESSES;
               SELECT COUNT(PHONE_ID) INTO PHONE_COUNT FROM ADDRESS_X_PHONEINFO;
               SELECT CODE_ID INTO  OFFICE_CODEID FROM CODES WHERE TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='PHONES_CODES') AND SHORT_CODE='O';  
               SELECT CODE_ID INTO  HOME_CODEID FROM CODES WHERE TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='PHONES_CODES') AND SHORT_CODE='H';  
                   
               IF(ADDRESS_COUNT=0 OR PHONE_COUNT=0) THEN  
                   -- DECLARE    
                    --    CURSOR ENTITY_ADD IS SELECT ENTITY_ID FROM ENTITY ;     
                    BEGIN   
                      BEGIN
				             	sSQL:='SELECT MIN(ENTITY_ID) FROM ENTITY';
				              EXECUTE IMMEDIATE sSQL INTO iMinID;
					
			            		sSQL:='SELECT MAX(ENTITY_ID) FROM ENTITY';
			 	            	EXECUTE IMMEDIATE sSQL INTO iMaxID;
				              END; 
                      
                       -- FOR ADD_COUNT IN ENTITY_ADD     
                        iLoopCount:=iMinID;
					              WHILE iLoopCount<=iMaxID
                        LOOP  
                        sSQL:='SELECT COUNT(*) FROM ENTITY  WHERE ENTITY_ID BETWEEN  '||iLoopCount ||' AND ('||iLoopCount||' + 49999)';
                        EXECUTE IMMEDIATE sSQL INTO iFLAG;
                        
                           IF (ADDRESS_COUNT=0 AND iFLAG > 0) THEN  
                            SELECT NVL(MAX(ADDRESS_ID), 0) INTO ADDRESSID FROM ENTITY_X_ADDRESSES;     
                            INSERT INTO ENTITY_X_ADDRESSES(ENTITY_ID,ADDRESS_ID,ADDR1, ADDR2, CITY,COUNTY,COUNTRY_CODE,STATE_ID,ZIP_CODE,EMAIL_ADDRESS,FAX_NUMBER,PRIMARY_ADD_FLAG )    
                               SELECT ENTITY_ID,ADDRESSID + ROW_NUMBER() over (order by entity_id) AS ADDRESS_ID,ADDR1,ADDR2,CITY,COUNTY,COUNTRY_CODE,STATE_ID,ZIP_CODE,EMAIL_ADDRESS,FAX_NUMBER,-1   
                               FROM ENTITY WHERE(ADDR1 IS NOT NULL OR ADDR2 IS NOT NULL OR CITY IS NOT NULL OR COUNTY IS NOT NULL OR (COUNTRY_CODE <>0 AND COUNTRY_CODE IS NOT NULL) OR (STATE_ID <>0 AND STATE_ID IS NOT NULL) OR ZIP_CODE IS NOT NULL OR EMAIL_ADDRESS IS NOT NULL OR FAX_NUMBER IS NOT NULL)   
                               AND  ENTITY.ENTITY_ID >= iLoopCount AND ENTITY.ENTITY_ID <= (iLoopCount + 49999);  
                           END IF;  
                            
                           IF (PHONE_COUNT=0 AND iFLAG > 0) THEN  
                              SELECT NVL(MAX(PHONE_ID), 0) INTO PHONEID FROM ADDRESS_X_PHONEINFO;  
                               INSERT INTO ADDRESS_X_PHONEINFO(ENTITY_ID,PHONE_ID,PHONE_CODE, PHONE_NO)    
                                   SELECT ENTITY_ID,PHONEID + ROW_NUMBER() over (order by entity_id) AS PHONE_ID,OFFICE_CODEID,PHONE1  
                                   FROM ENTITY WHERE (length(PHONE1)> 0) 
                                   AND  ENTITY.ENTITY_ID >= iLoopCount AND ENTITY.ENTITY_ID <= (iLoopCount + 49999);
                              
                              SELECT NVL(MAX(PHONE_ID), 0) INTO PHONEID FROM ADDRESS_X_PHONEINFO;   
                               INSERT INTO ADDRESS_X_PHONEINFO(ENTITY_ID,PHONE_ID,PHONE_CODE, PHONE_NO)    
                                   SELECT ENTITY_ID,PHONEID + ROW_NUMBER() over (order by entity_id) AS PHONE_ID,HOME_CODEID,PHONE2  
                                   FROM ENTITY WHERE (length(PHONE2)> 0) 
                                   AND  ENTITY.ENTITY_ID >= iLoopCount AND ENTITY.ENTITY_ID <= (iLoopCount + 49999);
                           END IF;  
                           iLoopCount:=iLoopCount + 50000;
                        END LOOP;    
                    END;       
                    BEGIN   
                     IF ADDRESS_COUNT=0 THEN  
                        SELECT MAX(ADDRESS_ID) INTO MAXADDRESS_ID FROM ENTITY_X_ADDRESSES;    
                        UPDATE GLOSSARY SET NEXT_UNIQUE_ID = (MAXADDRESS_ID + 1) WHERE SYSTEM_TABLE_NAME = 'ENTITY_X_ADDRESSES';   
                     END IF;  
                       
                     IF PHONE_COUNT=0 THEN  
                       SELECT MAX(PHONE_ID) INTO MAXPHONE_ID FROM ADDRESS_X_PHONEINFO;    
                        UPDATE GLOSSARY SET NEXT_UNIQUE_ID = (MAXPHONE_ID + 1) WHERE SYSTEM_TABLE_NAME = 'ADDRESS_X_PHONEINFO';   
                     END IF;  
                    END;    
                    COMMIT;   
              END IF;   
           END;      
  END;    
  
[EXECUTE_SP]
GO