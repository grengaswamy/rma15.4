[GO_DELIMITER]
--<--AUTHOR-NADIM ZAFAR-->
--<--NAME-SP_CREATE_BES_ROLE-->
--<--DATE CREATED-11/02/2009-->
DECLARE @spSQL AS nVARCHAR(max) 
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'USP_CREATE_BES_ROLE')
BEGIN
	DROP PROCEDURE USP_CREATE_BES_ROLE
END
	SET @spSQL = 'CREATE PROCEDURE USP_CREATE_BES_ROLE @sRM_userID varchar(100),@sBesRole varchar(50),@out_err varchar(500) OUTPUT,@sStoredproc varchar(5000)
				  AS
					Declare @nvSql as nVarchar(4000)
begin try
				  	IF NOT EXISTS (SELECT NAME FROM SYS.DATABASE_PRINCIPALS WHERE NAME=@sBesRole AND TYPE = ''R'')
					BEGIN
						set	@nvSql =''CREATE ROLE ''+ @sBesRole 
						Exec sp_executesql @nvSql 
					END
						EXEC USP_REFRESH_BES_ROLE @sBesRole,@sStoredproc
end try
begin catch
	SELECT	@out_err = error_message()
end catch
	'
	EXECUTE sp_executesql @spSQL 
GO