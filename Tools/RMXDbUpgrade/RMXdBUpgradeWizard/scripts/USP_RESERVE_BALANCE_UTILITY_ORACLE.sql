[GO_DELIMITER]
create or replace PROCEDURE USP_RESERVE_BALANCE_UTILITY(psnglclaims NUMBER, pClaimNumber VARCHAR2 DEFAULT null, psFrom  varchar2 DEFAULT null, psTo varchar2 DEFAULT null,p_sqlerrm out varchar2)
AUTHID CURRENT_USER IS
pClaimID NUMBER(10);
v_maxclid NUMBER;
v_minclid NUMBER;
v_cntrc NUMBER;
v_sFrom varchar2(10);
v_sTo varchar2(10);
v_tempminclid NUMBER;
--v_count number
V_SAMOUNT FLOAT;
V_CLAIM_ID NUMBER;
v_SQL varchar2(4000);
v_SQL1 varchar2(4000);
nextClaimId NUMBER;
IsCarrierOn varchar2(50);
FTSRCRowID varchar2(50);
V_RC_ROW_ID NUMBER;
v_sqlerrm varchar2(2000);
p_sqlerrm_date varchar2(20);
v_col_exist         number;
IsMultiCurr   BOOLEAN:= false;
v_BALANCEAMOUNT double precision;
v_CHANGEAMOUNT double precision;
v_CLM_CUR_RES_AMT double precision;
v_BASETOCLAIMCURRATE double precision;
v_RESERVE double precision;
v_CLM_CUR_COL_TOT double precision;
v_COLLECTION double precision;
v_CLM_CUR_PAID_TOT double precision;
v_PAID double precision;
V_CLM_CUR_BAL_AMT double precision;
V_CLAIMANT_EID number;
V_UNIT_ID number;
V_RESERVE_TYPE_CODE number;
V_INCURRED double precision;
V_DEPTEID number;
V_CRC number;
V_RESSTATUSCODE number;
V_CLAIMCURRCODE number;
V_POLCVG_LOSS_ROW_ID number;
V_ISFIRSTFINAL number;
V_POLICY_CVG_SEQNO number;
V_DATE_ENTERED varchar2(8);
V_DTTM_RCD_ADDED varchar2(14);
V_CLM_CUR_INC_AMT double precision;
v_RSV_ROW_ID NUMBER;
v_CLOSED_FLAG Number;
v_AUTO_ADJ_FLAG Number;
v_REASON VARCHAR2(30);
v_ENTERED_BY_USER varchar2(50);
v_CLAIM_CURRENCY_PAID_TOTAL DOUBLE PRECISION;
V_CLAIM_CURR_COLLECTION_TOTAL DOUBLE PRECISION;
TYPE rcCURSOR IS REF CURSOR;
rc_CURSOR rcCURSOR;
RESBAL_RH_INSERT rcCURSOR;
BEGIN

select to_char(systimestamp, 'YYYYmmddhhmmss')
    into p_sqlerrm_date
    from dual;

  SELECT str_parm_value into IsCarrierOn  FROM Parms_Name_Value where parm_name='MULTI_COVG_CLM';

  IF IsCarrierOn <> '0' THEN
    FTSRCRowID := ', FTS.RC_ROW_ID ';
  ELSE
    FTSRCRowID := '';
  END IF;

  IF psnglclaims = 1 THEN
   BEGIN
    SELECT CLAIM_ID into pClaimID FROM Claim where CLAIM_NUMBER = pClaimNumber;  
    EXCEPTION 
      WHEN NO_DATA_FOUND THEN  
        p_sqlerrm:= p_sqlerrm_date || ':' ||  'Provided claim number does not exist';  
        return;
      WHEN OTHERS THEN  
       ROLLBACK;  
        v_sqlerrm:=substr(SQLERRM,1,2000);  
        p_sqlerrm:=p_sqlerrm_date || ':' ||  v_sqlerrm;
        RETURN;
        
   UPDATE RESERVE_CURRENT SET PAID_TOTAL = 0, COLLECTION_TOTAL = 0 WHERE CLAIM_ID = pClaimID;  
   END;
  ELSE
    IF psFrom IS NOT NULL AND psTo IS NOT NULL THEN
      v_sFrom := to_char(to_date(psFrom,'yyyymmdd'),'yyyymmdd');
      v_sTo := to_char(to_date(psto,'yyyymmdd'),'yyyymmdd');

      BEGIN
        SELECT MAX(RESERVE_CURRENT.CLAIM_ID), MIN(RESERVE_CURRENT.CLAIM_ID)
        into v_maxclid,v_minclid
        FROM RESERVE_CURRENT , CLAIM WHERE CLAIM.CLAIM_ID = RESERVE_CURRENT.CLAIM_ID
        AND DATE_OF_CLAIM >= v_sFrom AND DATE_OF_CLAIM <= v_sTo;

        SELECT COUNT(1) into v_cntrc FROM RESERVE_CURRENT, CLAIM WHERE CLAIM.CLAIM_ID = RESERVE_CURRENT.CLAIM_ID
        AND DATE_OF_CLAIM >= v_sFrom AND DATE_OF_CLAIM <= v_sTo;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_sqlerrm:=substr(SQLERRM,1,2000);
           p_sqlerrm:=p_sqlerrm_date || ':' ||  v_sqlerrm;  
          RETURN;
        WHEN TOO_MANY_ROWS THEN
          v_sqlerrm:=substr(SQLERRM,1,2000);
           p_sqlerrm:=p_sqlerrm_date || ':' ||  v_sqlerrm;  
          RETURN;
        WHEN OTHERS THEN
          v_sqlerrm:=substr(SQLERRM,1,2000);
           p_sqlerrm:=p_sqlerrm_date || ':' ||  v_sqlerrm;  
          RETURN;
      END;

      IF v_cntrc = 0 THEN
         v_sqlerrm:= p_sqlerrm_date || ':' ||  'Reserves does not exists in the provided criteria';  
        p_sqlerrm:=v_sqlerrm;
        RETURN;
      END IF;
    ELSE
      SELECT MAX(CLAIM_ID), MIN(CLAIM_ID)
      into v_maxclid,v_minclid
      FROM RESERVE_CURRENT;
    END IF;

    v_SQL := 'UPDATE RESERVE_CURRENT SET PAID_TOTAL = 0.0, COLLECTION_TOTAL = 0.0
            WHERE CLAIM_ID BETWEEN :1 AND :2';

    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE V_SQL USING v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE V_SQL USING v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;

  END IF; -- else of IF psnglclaims = 1

--BEGIN----------------------------UPDATE PAID_TOTAL STARTS------------------------------------------------
  p_sqlerrm := p_sqlerrm_date || ':' ||
               ' UPDATE PAID_TOTAL STARTS' || '|';
  V_SQL := 'SELECT SUM(FTS.AMOUNT), F.CLAIM_ID, F.CLAIMANT_EID, FTS.RESERVE_TYPE_CODE, F.UNIT_ID '||' '|| FTSRCRowID ||' '||
        'FROM FUNDS F, FUNDS_TRANS_SPLIT FTS, CLAIM C WHERE F.CLAIM_ID = C.CLAIM_ID AND F.TRANS_ID = FTS.TRANS_ID';

  IF psnglclaims = 1  THEN
    V_SQL := V_SQL || ' AND F.CLAIM_ID = :1';
    ELSE
        IF psFrom IS NOT NULL AND psTo IS NOT NULL THEN
          V_SQL := V_SQL || ' AND C.DATE_OF_CLAIM >= :1 AND C.DATE_OF_CLAIM <= :2';
        END IF;
  END IF;

  V_SQL := V_SQL || ' AND F.VOID_FLAG = 0 AND F.PAYMENT_FLAG <> 0 GROUP BY F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE ' || FTSRCRowID ||' ORDER BY F.CLAIM_ID';

IF psnglclaims = 1 THEN
  OPEN rc_CURSOR FOR v_SQL using pClaimID;
ELSE
  If psFrom IS NOT NULL AND psTo IS NOT NULL THEN
    OPEN rc_CURSOR FOR v_SQL using v_sFrom,v_sTo;
  ELSE
    OPEN rc_CURSOR FOR v_SQL;
  END IF;
END IF;

  LOOP
    IF (IsCarrierOn <> '0') THEN
      FETCH rc_CURSOR
        INTO V_SAMOUNT, V_CLAIM_ID, V_CLAIMANT_EID, V_RESERVE_TYPE_CODE, V_UNIT_ID, V_RC_ROW_ID;
        EXIT WHEN rc_CURSOR%NOTFOUND;

        UPDATE RESERVE_CURRENT SET PAID_TOTAL = ROUND(V_SAMOUNT,2)
        WHERE CLAIM_ID = V_CLAIM_ID
        AND RC_ROW_ID = V_RC_ROW_ID;
    ELSE
      FETCH rc_CURSOR
        INTO V_SAMOUNT, V_CLAIM_ID, V_CLAIMANT_EID, V_RESERVE_TYPE_CODE, V_UNIT_ID;
        EXIT WHEN rc_CURSOR%NOTFOUND;
            UPDATE RESERVE_CURRENT SET PAID_TOTAL = ROUND(V_SAMOUNT,2)
        WHERE CLAIM_ID = V_CLAIM_ID
        AND CLAIMANT_EID = V_CLAIMANT_EID
        AND RESERVE_TYPE_CODE = V_RESERVE_TYPE_CODE
        AND UNIT_ID = V_UNIT_ID;
    END IF;
    END LOOP;
CLOSE rc_CURSOR;
p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||
                 ' UPDATE PAID_TOTAL ENDS' || '|';
--END----------------------------UPDATE PAID_TOTAL ENDS------------------------------------------------
--BEGIN----------------------------UPDATE COLLECTION_TOTAL STARTS----------------------------------------
p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||
                 ' UPDATE COLLECTION_TOTAL STARTS' || '|';

  V_SQL := 'SELECT SUM(FTS.AMOUNT), F.CLAIM_ID, F.CLAIMANT_EID, FTS.RESERVE_TYPE_CODE, F.UNIT_ID ' || FTSRCRowID ||
      ' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS, CLAIM C WHERE F.CLAIM_ID = C.CLAIM_ID AND F.TRANS_ID = FTS.TRANS_ID';

  IF psnglclaims = 1  THEN
    V_SQL := V_SQL || ' AND F.CLAIM_ID = :1';
    ELSE
    IF psFrom IS NOT NULL And psTo IS NOT NULL THEN
        V_SQL := V_SQL || ' AND C.DATE_OF_CLAIM >= :1 AND C.DATE_OF_CLAIM <= :2';
    END IF;
  END IF;

  V_SQL := V_SQL || ' AND F.VOID_FLAG = 0 AND F.PAYMENT_FLAG = 0 GROUP BY F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE' || FTSRCRowID || ' ORDER BY F.CLAIM_ID';

IF psnglclaims = 1 THEN
  OPEN rc_CURSOR FOR v_SQL USING pClaimID;
ELSE
  If psFrom IS NOT NULL AND psTo IS NOT NULL THEN
    OPEN rc_CURSOR FOR v_SQL USING v_sFrom,v_sTo;
  ELSE
    OPEN rc_CURSOR FOR v_SQL;
  END IF;
END IF;

  LOOP
    IF (IsCarrierOn <> '0') THEN
      FETCH rc_CURSOR
        INTO V_SAMOUNT, V_CLAIM_ID, V_CLAIMANT_EID, V_RESERVE_TYPE_CODE, V_UNIT_ID, V_RC_ROW_ID;
        EXIT WHEN rc_CURSOR%NOTFOUND;

        UPDATE RESERVE_CURRENT SET COLLECTION_TOTAL = ROUND(V_SAMOUNT,2)
        WHERE CLAIM_ID = V_CLAIM_ID
        AND RC_ROW_ID = V_RC_ROW_ID;
    ELSE
      FETCH rc_CURSOR
        INTO V_SAMOUNT, V_CLAIM_ID, V_CLAIMANT_EID, V_RESERVE_TYPE_CODE, V_UNIT_ID;
        EXIT WHEN rc_CURSOR%NOTFOUND;
            UPDATE RESERVE_CURRENT SET COLLECTION_TOTAL = ROUND(V_SAMOUNT,2)
        WHERE CLAIM_ID = V_CLAIM_ID
        AND CLAIMANT_EID = V_CLAIMANT_EID
        AND RESERVE_TYPE_CODE = V_RESERVE_TYPE_CODE
        AND UNIT_ID = V_UNIT_ID;
    END IF;
    END LOOP;
CLOSE rc_CURSOR;
p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||
                 ' UPDATE COLLECTION_TOTAL ENDS' || '|';

--END----------------------------UPDATE COLLECTION_TOTAL ENDS----------------------------------------
--BEGIN----------------------------UPDATE BALANCE_AMOUNT STARTS----------------------------------------
p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||
                 ' UPDATE BALANCE_AMOUNT STARTS' || '|';
				 -- case 2 ->> if SYS_PARMS_LOB.COLL_IN_RSV_BAL='N' EXCLUDING RECOVERY RESERVES
V_SQL1 := 'UPDATE RESERVE_CURRENT
    SET RESERVE_CURRENT.BALANCE_AMOUNT =  (RESERVE_CURRENT.RESERVE_AMOUNT-RESERVE_CURRENT.PAID_TOTAL)
WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
 AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
 where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE and ((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0))))
 and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
 WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
 and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';

  IF psnglclaims = 1 THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000  THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;

-- case 1 ->> if SYS_PARMS_LOB_COLL_IN_RSV_BAL='Y' EXCLUDING RECOVERY RESERVES
V_SQL1 := 'UPDATE RESERVE_CURRENT
    SET RESERVE_CURRENT.BALANCE_AMOUNT = (RESERVE_CURRENT.RESERVE_AMOUNT-(RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL))
WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
 AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
 where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE and ((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 )))))
 and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
 WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
 and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';
  IF psnglclaims = 1  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    while  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000  THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;

-- Case 3 ->> Closed Claims
  BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE TEMPRCCHANGEAMOUNT';
  EXCEPTION
  WHEN OTHERS THEN
         NULL;
  END;

  BEGIN
    V_SQL1 := 'CREATE TABLE TEMPRCCHANGEAMOUNT (RC_ROW_ID NUMBER(10) , CHANGE_AMOUNT NUMBER)';
    EXECUTE IMMEDIATE V_SQL1;
  END;
--CASE A -> For Closed Claim Set Reserve Amount to Zero is true and RESERVE_CURRENT.RESERVE_AMOUNT <> 0
V_SQL1 := 'INSERT INTO TEMPRCCHANGEAMOUNT
  ( SELECT RC_ROW_ID,  RESERVE_CURRENT.RESERVE_AMOUNT
    FROM RESERVE_CURRENT
           JOIN CLAIM
            ON RESERVE_CURRENT.CLAIM_ID = CLAIM.CLAIM_ID
           JOIN CODES CSTAT
            ON CLAIM.CLAIM_STATUS_CODE = CSTAT.CODE_ID
           JOIN CODES ABSSTAT
            ON CSTAT.RELATED_CODE_ID = ABSSTAT.CODE_ID
           JOIN SYS_PARMS_LOB
            ON SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE
       WHERE ABSSTAT.SHORT_CODE = ''C''
               AND SYS_PARMS_LOB.SET_TO_ZERO <> 0
               AND RESERVE_CURRENT.RESERVE_AMOUNT <> 0
               AND ( SELECT SHORT_CODE
                     FROM CODES
                        WHERE CODE_ID = ( SELECT RELATED_CODE_ID
                                          FROM CODES
                                             WHERE CODE_ID = RESERVE_CURRENT.RESERVE_TYPE_CODE ) ) <> :0 ';
  IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1 )';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2 )';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;

 V_SQL1 := 'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.BALANCE_AMOUNT = RESERVE_CURRENT.BALANCE_AMOUNT - RESERVE_CURRENT.RESERVE_AMOUNT
, RESERVE_CURRENT.RESERVE_AMOUNT = 0
WHERE RESERVE_CURRENT.RESERVE_AMOUNT <> 0
AND RESERVE_CURRENT.CLAIM_ID =
(
  SELECT CLAIM.CLAIM_ID
  FROM CLAIM
  JOIN CODES CSTAT ON CLAIM.CLAIM_STATUS_CODE=CSTAT.CODE_ID
  JOIN CODES ABSSTAT ON CSTAT.RELATED_CODE_ID=ABSSTAT.CODE_ID
  JOIN SYS_PARMS_LOB ON SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE
  WHERE CLAIM.CLAIM_ID = RESERVE_CURRENT.CLAIM_ID
  AND ABSSTAT.SHORT_CODE=''C''
  AND SYS_PARMS_LOB.SET_TO_ZERO <> 0
)
AND RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES A
WHERE A.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
AND A.RELATED_CODE_ID =(SELECT B.CODE_ID FROM CODES B WHERE B.CODE_ID=A.RELATED_CODE_ID AND  B.SHORT_CODE <> :0))';

  IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;

  --CASE B -> For Closed Claim Set Reserve Balance to Zero is true and RESERVE_CURRENT.RESERVE_BALANCE <> 0
  -- OR For Closed Claim Set negative Reserve Balance to Zero is true and RESERVE_CURRENT.RESERVE_BALANCE < 0
V_SQL1 := 'INSERT INTO TEMPRCCHANGEAMOUNT
  ( SELECT RC_ROW_ID,
           -1 * RESERVE_CURRENT.BALANCE_AMOUNT
    FROM RESERVE_CURRENT
           JOIN CLAIM
            ON RESERVE_CURRENT.CLAIM_ID = CLAIM.CLAIM_ID
           JOIN CODES CSTAT
            ON CLAIM.CLAIM_STATUS_CODE = CSTAT.CODE_ID
           JOIN CODES ABSSTAT
            ON CSTAT.RELATED_CODE_ID = ABSSTAT.CODE_ID
           JOIN SYS_PARMS_LOB
            ON SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE
       WHERE ABSSTAT.SHORT_CODE = ''C''
               AND ( ( SYS_PARMS_LOB.BAL_TO_ZERO <> 0
               AND RESERVE_CURRENT.BALANCE_AMOUNT <> 0 )
               OR ( SYS_PARMS_LOB.NEG_BAL_TO_ZERO <> 0
               AND RESERVE_CURRENT.BALANCE_AMOUNT < 0 ) )
               AND ( SELECT SHORT_CODE
                     FROM CODES
                        WHERE CODE_ID = ( SELECT RELATED_CODE_ID
                                          FROM CODES
                                             WHERE CODE_ID = RESERVE_CURRENT.RESERVE_TYPE_CODE ) ) <> :0 ';
  IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1)';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2)';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;

  V_SQL1 := 'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.RESERVE_AMOUNT = RESERVE_CURRENT.RESERVE_AMOUNT - RESERVE_CURRENT.BALANCE_AMOUNT
  , RESERVE_CURRENT.BALANCE_AMOUNT = 0
WHERE RESERVE_CURRENT.CLAIM_ID =
(
  SELECT CLAIM.CLAIM_ID
  FROM CLAIM
  JOIN CODES CSTAT ON CLAIM.CLAIM_STATUS_CODE=CSTAT.CODE_ID
  JOIN CODES ABSSTAT ON CSTAT.RELATED_CODE_ID=ABSSTAT.CODE_ID
  JOIN SYS_PARMS_LOB ON SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE
  WHERE CLAIM.CLAIM_ID = RESERVE_CURRENT.CLAIM_ID
  AND ABSSTAT.SHORT_CODE=''C''
  AND
  (  (SYS_PARMS_LOB.BAL_TO_ZERO <> 0 AND RESERVE_CURRENT.BALANCE_AMOUNT <> 0)
    OR (SYS_PARMS_LOB.NEG_BAL_TO_ZERO <> 0 AND RESERVE_CURRENT.BALANCE_AMOUNT < 0)
  )
)
AND RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES A
WHERE A.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
AND A.RELATED_CODE_ID =(SELECT B.CODE_ID FROM CODES B WHERE B.CODE_ID=A.RELATED_CODE_ID AND  B.SHORT_CODE <> :0))';

  IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;

-- Case 5: Recovery Reserve
V_SQL1 := 'UPDATE RESERVE_CURRENT
    SET RESERVE_CURRENT.BALANCE_AMOUNT = RESERVE_CURRENT.RESERVE_AMOUNT - RESERVE_CURRENT.COLLECTION_TOTAL
  WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
  where claim.CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
  AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
  where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE ))
  and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
  WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
  and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE = :0))';--'||''''||'R'||''''||'))';

  IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;
p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||
                 ' UPDATE BALANCE_AMOUNT ENDS' || '|';
--END----------------------------UPDATE BALANCE_AMOUNT ENDS----------------------------------------
--BEGIN----------------------------UPDATE INCURRED_AMOUNT STARTS----------------------------------------
p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||
                 ' UPDATE INCURRED_AMOUNT STARTS' || '|';

-- Case 1: Recovery Reserve
V_SQL1 := 'UPDATE RESERVE_CURRENT
  SET RESERVE_CURRENT.INCURRED_AMOUNT =  (CASE WHEN RESERVE_CURRENT.BALANCE_AMOUNT < 0 THEN RESERVE_CURRENT.COLLECTION_TOTAL
                        ELSE RESERVE_CURRENT.BALANCE_AMOUNT + RESERVE_CURRENT.COLLECTION_TOTAL  END)
   WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
   where claim.CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
   AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
   where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE ))
   and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
   WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
   and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE = :0))';

 IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;

-- CASE 2: None Recovery Reserve
-- Case E: Include Collection in Reserve Balance = 0 Include Collection in Incurred = 0 and Balance Amount >= 0
V_SQL1 := 'UPDATE RESERVE_CURRENT
  SET RESERVE_CURRENT.INCURRED_AMOUNT = RESERVE_CURRENT.BALANCE_AMOUNT+RESERVE_CURRENT.PAID_TOTAL
WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
 AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
 where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE
 AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0)) AND ((SYS_PARMS_LOB.COLL_IN_INCUR_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL = 0))))
 AND RESERVE_CURRENT.BALANCE_AMOUNT >= 0.0
 and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
 WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
 and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';--'||''''||'R'||''''||'))';

  IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN  :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;

--Case F: Include Collection in Reserve Balance = 0 Include Collection in Incurred = 0 and Balance Amount < 0
V_SQL1 := 'UPDATE RESERVE_CURRENT
   SET RESERVE_CURRENT.INCURRED_AMOUNT = RESERVE_CURRENT.PAID_TOTAL
WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
 AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
 where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE
 AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0))  AND ((SYS_PARMS_LOB.COLL_IN_INCUR_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL = 0))))
 AND RESERVE_CURRENT.BALANCE_AMOUNT < 0.0
 and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
 WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
 and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';--'||''''||'R'||''''||'))';

  IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;
-- Case A: Include Collection in Reserve Balance <> 0 and Balance Amount >= 0 and Paid_Total - Collection_Total >= 0
--     : Include Collection in Reserve Balance = 0 and Collection in INCURRED <> 0 and Balance Amount >= 0

--V_SQL1 := 'UPDATE RESERVE_CURRENT
--    SET RESERVE_CURRENT.INCURRED_AMOUNT =  RESERVE_CURRENT.BALANCE_AMOUNT + (RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL)
--WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
--where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
-- AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
-- where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE
-- AND (
--      (((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
--											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
--											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 ))) AND RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL >= 0.0)
--      OR
--      (((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0 )) AND 
--			((SYS_PARMS_LOB.COLL_IN_INCUR_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL <> 0 
--											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
--											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = 0))))
--    )
--    ))
-- AND RESERVE_CURRENT.BALANCE_AMOUNT >= 0.0
-- and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
-- WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
-- and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';--'||''''||'R'||''''||'))';
--
--IF psnglclaims = 1
--  THEN
--    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
--    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
--  ELSE
--    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
--    v_tempminclid := v_minclid;
--    WHILE  v_tempminclid <= v_maxclid
--    LOOP
--      IF (v_maxclid - v_tempminclid) > 1000
--      THEN
--        nextClaimId := v_tempminclid + 1000;
--        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
--      ELSE
--        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
--      END IF;
--      v_tempminclid := v_tempminclid + 1001;
--    END LOOP;
--  END IF;
V_SQL1 := 'UPDATE RESERVE_CURRENT
    SET RESERVE_CURRENT.INCURRED_AMOUNT =  RESERVE_CURRENT.BALANCE_AMOUNT + (RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL)
WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
 AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
 where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE
 AND (
      (((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 ))))
      OR
      (((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0 )) AND 
			((SYS_PARMS_LOB.COLL_IN_INCUR_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL <> 0 
											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = 0))))
    )
    ))
 AND RESERVE_CURRENT.BALANCE_AMOUNT >= 0.0
 and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
 WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
 and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';--'||''''||'R'||''''||'))';

IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;

-- Case B: Include Collection in Reserve Balance <> 0 and Balance Amount < 0 and Paid_Total - Collection_Total >= 0
--     : Include Collection in Reserve Balance = 0 and Collection in INCURRED <> 0 and Balance Amount < 0
--V_SQL1 := 'UPDATE RESERVE_CURRENT
--    SET RESERVE_CURRENT.INCURRED_AMOUNT =
--    (RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL)
--WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
--where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
-- AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
-- where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE
-- AND (
--      (((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
--											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
--											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 ))) AND RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL >= 0.0)
--      OR
--      ((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0 )) AND ((SYS_PARMS_LOB.COLL_IN_INCUR_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL <> 0 
--											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
--											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = 0)))
--    )
--    ))
-- AND RESERVE_CURRENT.BALANCE_AMOUNT < 0.0
-- and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
-- WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
-- and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';--'||''''||'R'||''''||'))';
--
--    IF psnglclaims = 1
--  THEN
--    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
--    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
--  ELSE
--    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
--    v_tempminclid := v_minclid;
--    WHILE  v_tempminclid <= v_maxclid
--    LOOP
--      IF (v_maxclid - v_tempminclid) > 1000
--      THEN
--        nextClaimId := v_tempminclid + 1000;
--        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
--      ELSE
--        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
--      END IF;
--      v_tempminclid := v_tempminclid + 1001;
--    END LOOP;
--  END IF;
V_SQL1 := 'UPDATE RESERVE_CURRENT
    SET RESERVE_CURRENT.INCURRED_AMOUNT =
    (RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL)
WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
 AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
 where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE
 AND (
      (((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 ))))
      OR
      ((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0 )) AND ((SYS_PARMS_LOB.COLL_IN_INCUR_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL <> 0 
											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = 0)))
    )
    ))
 AND RESERVE_CURRENT.BALANCE_AMOUNT < 0.0
 and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
 WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
 and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';--'||''''||'R'||''''||'))';

    IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;
-- Case C: Include Collection in Reserve Balance <> 0 and Balance Amount >= 0 and Paid_Total - Collection_Total < 0
--V_SQL1 := 'UPDATE RESERVE_CURRENT
--    SET RESERVE_CURRENT.INCURRED_AMOUNT =
--    RESERVE_CURRENT.BALANCE_AMOUNT
--WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
--where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
-- AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
-- where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE   AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0) )))
-- AND RESERVE_CURRENT.BALANCE_AMOUNT >= 0.0 AND RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL < 0.0
-- and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
-- WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
-- and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';--'||''''||'R'||''''||'))';
--
--  IF psnglclaims = 1
--  THEN
--    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
--    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
--  ELSE
--    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
--    v_tempminclid := v_minclid;
--    WHILE  v_tempminclid <= v_maxclid
--    LOOP
--      IF (v_maxclid - v_tempminclid) > 1000
--      THEN
--        nextClaimId := v_tempminclid + 1000;
--        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
--      ELSE
--        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
--      END IF;
--      v_tempminclid := v_tempminclid + 1001;
--    END LOOP;
--  END IF;

-- Case D: Include Collection in Reserve Balance <> 0 and Balance Amount < 0 and Paid_Total - Collection_Total < 0
-- V_SQL1 := 'UPDATE RESERVE_CURRENT
--   SET RESERVE_CURRENT.INCURRED_AMOUNT = 0
--WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
--where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID
-- AND CLAIM.LINE_OF_BUS_CODE=(select SYS_PARMS_LOB.LINE_OF_BUS_CODE from  SYS_PARMS_LOB
-- where SYS_PARMS_LOB.Line_Of_Bus_Code=CLAIM.LINE_OF_BUS_CODE  AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
--											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
--											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 )))))
-- AND RESERVE_CURRENT.BALANCE_AMOUNT < 0.0 AND RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL < 0.0
-- and RESERVE_CURRENT.RESERVE_TYPE_CODE = (SELECT CODE_ID FROM CODES a
-- WHERE a.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE
-- and a.RELATED_CODE_ID =(SELECT b.code_id FROM CODES b where b.code_id=a.RELATED_CODE_ID and  b.SHORT_CODE <> :0))';--'||''''||'R'||''''||'))';
--
--  IF psnglclaims = 1
--  THEN
--    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
--    EXECUTE IMMEDIATE v_SQL USING 'R',pClaimID;
--  ELSE
--    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
--    v_tempminclid := v_minclid;
--    WHILE  v_tempminclid <= v_maxclid
--    LOOP
--      IF (v_maxclid - v_tempminclid) > 1000
--      THEN
--        nextClaimId := v_tempminclid + 1000;
--        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, nextClaimId ;
--      ELSE
--        EXECUTE IMMEDIATE v_SQL USING 'R',v_tempminclid, v_maxclid ;
--      END IF;
--      v_tempminclid := v_tempminclid + 1001;
--    END LOOP;
--  END IF;


  COMMIT;
  p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||
                 ' UPDATE INCURRED_AMOUNT ENDS' || '|';
----------------------------UPDATE INCURRED_AMOUNT ENDS----------------------------------------
----------------------------UPDATE MULTICURRENCY COLUMNS STARTS---------------------------------------
select count(1) into v_col_exist from user_tab_columns
         where table_name = 'RESERVE_CURRENT'
           and column_name = 'CLAIM_CURR_CODE';

  IF v_col_exist = 0 THEN
    IsMultiCurr := FALSE;
  ELSE
    IsMultiCurr := TRUE;
  END IF;
  IF IsMultiCurr = true THEN
  V_SQL1 := 'UPDATE RESERVE_CURRENT
   SET CLAIM_CURRENCY_RESERVE_AMOUNT = RESERVE_CURRENT.RESERVE_AMOUNT * NVL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE, 0), 1),
   CLAIM_CURRENCY_INCURRED_AMOUNT = RESERVE_CURRENT.INCURRED_AMOUNT * NVL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE, 0), 1),
   CLAIM_CURR_COLLECTION_TOTAL = RESERVE_CURRENT.COLLECTION_TOTAL * NVL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE, 0), 1),
   CLAIM_CURRENCY_PAID_TOTAL = RESERVE_CURRENT.PAID_TOTAL * NVL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE, 0), 1),
   CLAIM_CURRENCY_BALANCE_AMOUNT = RESERVE_CURRENT.BALANCE_AMOUNT * NVL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE, 0), 1)
WHERE RESERVE_CURRENT.CLAIM_ID =(select CLAIM.CLAIM_ID from claim
where claim .CLAIM_ID=RESERVE_CURRENT.CLAIM_ID)';

  IF psnglclaims = 1
  THEN
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID = :1';
    EXECUTE IMMEDIATE v_SQL USING pClaimID;
  ELSE
    V_SQL := V_SQL1 || ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN :1 AND :2';
    v_tempminclid := v_minclid;
    WHILE  v_tempminclid <= v_maxclid
    LOOP
      IF (v_maxclid - v_tempminclid) > 1000
      THEN
        nextClaimId := v_tempminclid + 1000;
        EXECUTE IMMEDIATE v_SQL USING v_tempminclid, nextClaimId ;
      ELSE
        EXECUTE IMMEDIATE v_SQL USING v_tempminclid, v_maxclid ;
      END IF;
      v_tempminclid := v_tempminclid + 1001;
    END LOOP;
  END IF;
  COMMIT;
  END IF;
----------------------------UPDATE MULTICURRENCY COLUMNS ENDS---------------------------------------
----------------------------UPDATE RESERVE_HISTORY STARTS--------------------------------------


  IF IsMultiCurr = false THEN
    V_SQL1 := 'SELECT RESERVE_CURRENT.BALANCE_AMOUNT, TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT
  , RESERVE_CURRENT.CLAIM_ID, RESERVE_CURRENT.CLAIMANT_EID, RESERVE_CURRENT.UNIT_ID, RESERVE_CURRENT.RESERVE_TYPE_CODE
  , RESERVE_CURRENT.RESERVE_AMOUNT, RESERVE_CURRENT.COLLECTION_TOTAL, RESERVE_CURRENT.INCURRED_AMOUNT, RESERVE_CURRENT.PAID_TOTAL
  , RESERVE_CURRENT.SEC_DEPT_EID, RESERVE_CURRENT.CRC
  , RESERVE_CURRENT.RES_STATUS_CODE FROM RESERVE_CURRENT
  JOIN TEMPRCCHANGEAMOUNT ON TEMPRCCHANGEAMOUNT.RC_ROW_ID = RESERVE_CURRENT.RC_ROW_ID
  WHERE ABS(TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT) > 0';
  ELSE
  IF IsCarrierOn <> '0' THEN
    V_SQL1 := 'SELECT RESERVE_CURRENT.BALANCE_AMOUNT, TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT, RESERVE_CURRENT.CLAIM_ID, RESERVE_CURRENT.CLAIMANT_EID
  , RESERVE_CURRENT.UNIT_ID, RESERVE_CURRENT.RESERVE_TYPE_CODE, RESERVE_CURRENT.RESERVE_AMOUNT, RESERVE_CURRENT.COLLECTION_TOTAL
  , RESERVE_CURRENT.INCURRED_AMOUNT, RESERVE_CURRENT.PAID_TOTAL
  , RESERVE_CURRENT.SEC_DEPT_EID, RESERVE_CURRENT.CRC, RESERVE_CURRENT.RES_STATUS_CODE
  , RESERVE_CURRENT.CLAIM_CURR_CODE , RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE
  , RESERVE_CURRENT.POLCVG_LOSS_ROW_ID, RESERVE_CURRENT.IS_FIRST_FINAL, RESERVE_CURRENT.POLICY_CVG_SEQNO
  FROM RESERVE_CURRENT
  JOIN TEMPRCCHANGEAMOUNT ON TEMPRCCHANGEAMOUNT.RC_ROW_ID = RESERVE_CURRENT.RC_ROW_ID
  WHERE ABS(TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT) > 0';
  ELSE
    V_SQL1 := 'SELECT RESERVE_CURRENT.BALANCE_AMOUNT, TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT, RESERVE_CURRENT.CLAIM_ID, RESERVE_CURRENT.CLAIMANT_EID
  , RESERVE_CURRENT.UNIT_ID, RESERVE_CURRENT.RESERVE_TYPE_CODE, RESERVE_CURRENT.RESERVE_AMOUNT, RESERVE_CURRENT.COLLECTION_TOTAL
  , RESERVE_CURRENT.INCURRED_AMOUNT, RESERVE_CURRENT.PAID_TOTAL
  , RESERVE_CURRENT.SEC_DEPT_EID, RESERVE_CURRENT.CRC, RESERVE_CURRENT.RES_STATUS_CODE
  , RESERVE_CURRENT.CLAIM_CURR_CODE , RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE
  FROM RESERVE_CURRENT
  JOIN TEMPRCCHANGEAMOUNT ON TEMPRCCHANGEAMOUNT.RC_ROW_ID = RESERVE_CURRENT.RC_ROW_ID
  WHERE ABS(TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT) > 0';
    END IF;
  END IF;

  OPEN RESBAL_RH_INSERT FOR V_SQL1;
  LOOP
    IF IsMultiCurr = false THEN
      FETCH RESBAL_RH_INSERT INTO v_BALANCEAMOUNT,v_CHANGEAMOUNT,v_CLAIM_ID,v_CLAIMANT_EID,v_UNIT_ID,v_RESERVE_TYPE_CODE,v_RESERVE,v_COLLECTION,v_INCURRED,v_PAID,v_DEPTEID,v_CRC,v_RESSTATUSCODE;
    ELSE
      IF IsCarrierOn <> '0' THEN
        FETCH RESBAL_RH_INSERT INTO v_BALANCEAMOUNT,v_CHANGEAMOUNT,v_CLAIM_ID,v_CLAIMANT_EID,v_UNIT_ID,v_RESERVE_TYPE_CODE,v_RESERVE,v_COLLECTION,v_INCURRED,v_PAID,v_DEPTEID,v_CRC,v_RESSTATUSCODE,v_CLAIMCURRCODE,v_BASETOCLAIMCURRATE,v_POLCVG_LOSS_ROW_ID,v_ISFIRSTFINAL,v_POLICY_CVG_SEQNO;
      ELSE
        FETCH RESBAL_RH_INSERT INTO v_BALANCEAMOUNT,v_CHANGEAMOUNT,v_CLAIM_ID,v_CLAIMANT_EID,v_UNIT_ID,v_RESERVE_TYPE_CODE,v_RESERVE,v_COLLECTION,v_INCURRED,v_PAID,v_DEPTEID,v_CRC,v_RESSTATUSCODE,v_CLAIMCURRCODE,v_BASETOCLAIMCURRATE;
      END IF;
    END IF;
    EXIT WHEN RESBAL_RH_INSERT%NOTFOUND;
    SELECT NEXT_UNIQUE_ID  INTO v_RSV_ROW_ID  FROM GLOSSARY   WHERE SYSTEM_TABLE_NAME = 'RESERVE_HISTORY';
    v_DATE_ENTERED := to_char(sysdate, 'YYYYMMDD');
    v_DTTM_RCD_ADDED := to_char(sysdate, 'YYYYMMDDHHMISS');
    v_CLOSED_FLAG := -1;
    v_AUTO_ADJ_FLAG := 0;
    IF IsMultiCurr = false THEN
      V_SQL := 'INSERT INTO RESERVE_HISTORY(
              BALANCE_AMOUNT
            , CHANGE_AMOUNT
            , RSV_ROW_ID
            , CLAIM_ID
            , CLAIMANT_EID
            , UNIT_ID
            , RESERVE_TYPE_CODE
            , RESERVE_AMOUNT
            , COLLECTION_TOTAL
            , INCURRED_AMOUNT
            , PAID_TOTAL
            , DATE_ENTERED
            , ENTERED_BY_USER
            , REASON
            , UPDATED_BY_USER
            , DTTM_RCD_ADDED
            , DTTM_RCD_LAST_UPD
            , ADDED_BY_USER
            , CRC
            , SEC_DEPT_EID
            , RES_STATUS_CODE
            , CLOSED_FLAG
            , AUTO_ADJ_FLAG
            )
            VALUES(
            :1
            , :2
            , :3
            , :4
            , :5
            , :6
            , :7
            , :8
            , :9
            , :10
            , :11
            , :12
            , :13
            , :14
            , :15
            , :16
            , :17
            , :18
            , :19
            , :20
            , :21
            , :22
            , :23)';

          execute immediate V_SQL
            using  v_BALANCEAMOUNT
          , v_CHANGEAMOUNT
          , v_RSV_ROW_ID
          , v_CLAIM_ID
          , v_CLAIMANT_EID
          , v_UNIT_ID
          , v_RESERVE_TYPE_CODE
          , v_RESERVE
          , v_COLLECTION
          , v_INCURRED
          , v_PAID
          , v_DATE_ENTERED
          , v_ENTERED_BY_USER
          , v_REASON
          , v_DTTM_RCD_ADDED
          , v_CRC
          , v_DEPTEID
          , v_RESSTATUSCODE
          , v_CLOSED_FLAG
          , v_AUTO_ADJ_FLAG;

    ELSE
      v_BASETOCLAIMCURRATE := NVL(NULLIF(v_BASETOCLAIMCURRATE, 0), 1);
      v_CLM_CUR_RES_AMT := v_BASETOCLAIMCURRATE * v_RESERVE;
      v_CLM_CUR_INC_AMT := v_BASETOCLAIMCURRATE * v_INCURRED;
      v_CLM_CUR_COL_TOT := v_BASETOCLAIMCURRATE * v_COLLECTION;
      v_CLM_CUR_PAID_TOT := v_BASETOCLAIMCURRATE * v_PAID;
      v_CLM_CUR_BAL_AMT := v_BASETOCLAIMCURRATE * v_BALANCEAMOUNT;

      IF IsCarrierOn <> '0' THEN
        V_SQL := 'INSERT INTO RESERVE_HISTORY(
              BALANCE_AMOUNT
            , CHANGE_AMOUNT
            , RSV_ROW_ID
            , CLAIM_ID
            , CLAIMANT_EID
            , UNIT_ID
            , RESERVE_TYPE_CODE
            , RESERVE_AMOUNT
            , COLLECTION_TOTAL
            , INCURRED_AMOUNT
            , PAID_TOTAL
            , DATE_ENTERED
            , ENTERED_BY_USER
            , REASON
            , UPDATED_BY_USER
            , DTTM_RCD_ADDED
            , DTTM_RCD_LAST_UPD
            , ADDED_BY_USER
            , CRC
            , SEC_DEPT_EID
            , RES_STATUS_CODE
            , CLOSED_FLAG
            , AUTO_ADJ_FLAG
            , CLAIM_CURR_CODE
            , CLAIM_TO_BASE_CUR_RATE
            , CLAIM_CURRENCY_RESERVE_AMOUNT
            , CLAIM_CURRENCY_INCURRED_AMOUNT
            , CLAIM_CURR_COLLECTION_TOTAL
            , CLAIM_CURRENCY_PAID_TOTAL
            , CLAIM_CURRENCY_BALANCE_AMOUNT
            , BASE_TO_CLAIM_CUR_RATE
            , POLICY_CVG_SEQNO
            , POLCVG_LOSS_ROW_ID
            , IS_FIRST_FINAL
            )
            VALUES(
            :1
            , :2
            , :3
            , :4
            , :5
            , :6
            , :7
            , :8
            , :9
            , :10
            , :11
            , :12
            , :13
            , :14
            , :15
            , :16
            , :17
            , :18
            , :19
            , :20
            , :21
            , :22
            , :23
            , :24
            , :25
            , :26
            , :27
            , :28
            , :29
            , :30
            , :31
            , :32
            , :33
            , :34)';
            execute immediate V_SQL
          using  v_BALANCEAMOUNT
          , v_CHANGEAMOUNT
          , v_RSV_ROW_ID
          , v_CLAIM_ID
          , v_CLAIMANT_EID
          , v_UNIT_ID
          , v_RESERVE_TYPE_CODE
          , v_RESERVE
          , v_COLLECTION
          , v_INCURRED
          , v_PAID
          , v_DATE_ENTERED
          , v_ENTERED_BY_USER
          , v_REASON
          , v_DTTM_RCD_ADDED
          , v_CRC
          , v_DEPTEID
          , v_RESSTATUSCODE
          , v_CLOSED_FLAG
          , v_AUTO_ADJ_FLAG
          , v_CLAIMCURRCODE
          , v_BASETOCLAIMCURRATE
          , v_CLM_CUR_RES_AMT
          , v_CLM_CUR_INC_AMT
          , v_CLAIM_CURR_COLLECTION_TOTAL
          , v_CLAIM_CURRENCY_PAID_TOTAL
          , v_CLM_CUR_BAL_AMT
          , v_POLICY_CVG_SEQNO
          , v_POLCVG_LOSS_ROW_ID
          , v_ISFIRSTFINAL;
      ELSE
          V_SQL := 'INSERT INTO RESERVE_HISTORY(
              BALANCE_AMOUNT
            , CHANGE_AMOUNT
            , RSV_ROW_ID
            , CLAIM_ID
            , CLAIMANT_EID
            , UNIT_ID
            , RESERVE_TYPE_CODE
            , RESERVE_AMOUNT
            , COLLECTION_TOTAL
            , INCURRED_AMOUNT
            , PAID_TOTAL
            , DATE_ENTERED
            , ENTERED_BY_USER
            , REASON
            , UPDATED_BY_USER
            , DTTM_RCD_ADDED
            , DTTM_RCD_LAST_UPD
            , ADDED_BY_USER
            , CRC
            , SEC_DEPT_EID
            , RES_STATUS_CODE
            , CLOSED_FLAG
            , AUTO_ADJ_FLAG
            , CLAIM_CURR_CODE
            , CLAIM_TO_BASE_CUR_RATE
            , CLAIM_CURRENCY_RESERVE_AMOUNT
            , CLAIM_CURRENCY_INCURRED_AMOUNT
            , CLAIM_CURR_COLLECTION_TOTAL
            , CLAIM_CURRENCY_PAID_TOTAL
            , CLAIM_CURRENCY_BALANCE_AMOUNT
            , BASE_TO_CLAIM_CUR_RATE
            )
            VALUES(
            :1
            , :2
            , :3
            , :4
            , :5
            , :6
            , :7
            , :8
            , :9
            , :10
            , :11
            , :12
            , :13
            , :14
            , :15
            , :16
            , :17
            , :18
            , :19
            , :20
            , :21
            , :22
            , :23
            , :24
            , :25
            , :26
            , :27
            , :28
            , :29
            , :30
            , :31
            )';
            execute immediate V_SQL
          using  v_BALANCEAMOUNT
          , v_CHANGEAMOUNT
          , v_RSV_ROW_ID
          , v_CLAIM_ID
          , v_CLAIMANT_EID
          , v_UNIT_ID
          , v_RESERVE_TYPE_CODE
          , v_RESERVE
          , v_COLLECTION
          , v_INCURRED
          , v_PAID
          , v_DATE_ENTERED
          , v_ENTERED_BY_USER
          , v_REASON
          , v_DTTM_RCD_ADDED
          , v_CRC
          , v_DEPTEID
          , v_RESSTATUSCODE
          , v_CLOSED_FLAG
          , v_AUTO_ADJ_FLAG
          , v_CLAIMCURRCODE
          , v_BASETOCLAIMCURRATE
          , v_CLM_CUR_RES_AMT
          , v_CLM_CUR_INC_AMT
          , v_CLAIM_CURR_COLLECTION_TOTAL
          , v_CLAIM_CURRENCY_PAID_TOTAL
          , v_CLM_CUR_BAL_AMT;
      END IF;
    END IF;
    UPDATE GLOSSARY SET NEXT_UNIQUE_ID = v_RSV_ROW_ID + 1 WHERE SYSTEM_TABLE_NAME = 'RESERVE_HISTORY';
  END LOOP;
  CLOSE RESBAL_RH_INSERT;

----------------------------UPDATE RESERVE_HISTORY ENDS--------------------------------------
commit;
p_sqlerrm := p_sqlerrm  || p_sqlerrm_date || ':' ||
                 ' SUCCESS' || '|';
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    ROLLBACK;
    v_sqlerrm:=substr(SQLERRM,1,2000);
     p_sqlerrm:=p_sqlerrm_date || ':' ||  v_sqlerrm;  
  WHEN TOO_MANY_ROWS THEN
      ROLLBACK;
    v_sqlerrm:=substr(SQLERRM,1,2000);
     p_sqlerrm:=p_sqlerrm_date || ':' ||  v_sqlerrm;  
  WHEN OTHERS THEN
      ROLLBACK;
    v_sqlerrm:=substr(SQLERRM,1,2000);
     p_sqlerrm:=p_sqlerrm_date || ':' ||  v_sqlerrm;  
END;
GO