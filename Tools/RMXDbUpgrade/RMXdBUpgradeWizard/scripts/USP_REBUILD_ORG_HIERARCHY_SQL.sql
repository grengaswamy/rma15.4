[GO_DELIMITER]
DECLARE @spSQL AS nVARCHAR(max) 

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'USP_REBUILD_ORG_HIERARCHY') 
DROP PROCEDURE USP_REBUILD_ORG_HIERARCHY

SET @spSQL = 'CREATE PROCEDURE USP_REBUILD_ORG_HIERARCHY @sRM_USERID VARCHAR(20), @out_err varchar(500) OUTPUT
 AS  
    DECLARE @GroupId VARCHAR(100)  
    DECLARE @GroupEntities VARCHAR(max)
    DECLARE @sSql nVARCHAR(max)

	begin try
	IF EXISTS (SELECT * FROM sys.objects WHERE type = ''U'' AND name = ''OH_MAP_TMP'') 
	DROP TABLE OH_MAP_TMP

	IF EXISTS (SELECT * FROM sys.objects WHERE type = ''U'' AND name = ''ENTITY_MAP_TEMP'') 
	DROP TABLE ENTITY_MAP_TEMP
    -- Create the Temporary table for storing the Org Hierarchy details
     SET @sSql= ''SELECT DISTINCT DEPARTMENT.ENTITY_ID DEPARTMENT_EID,
                        FACILITY.ENTITY_ID FACILITY_EID, 
                        LOCATION.ENTITY_ID LOCATION_EID,
                        DIVISION.ENTITY_ID DIVISION_EID, 
                        REGION.ENTITY_ID REGION_EID,
                        OPERATION.ENTITY_ID OPERATION_EID, 
                        COMPANY.ENTITY_ID COMPANY_EID, 
                        CLIENT.ENTITY_ID CLIENT_EID 
				  INTO OH_MAP_TMP
                  FROM  ENTITY CLIENT LEFT OUTER JOIN ENTITY COMPANY
                        ON COMPANY.PARENT_EID = CLIENT.ENTITY_ID
                        LEFT OUTER JOIN ENTITY OPERATION
                        ON OPERATION.PARENT_EID = COMPANY.ENTITY_ID
                        LEFT OUTER JOIN ENTITY REGION
                        ON REGION.PARENT_EID = OPERATION.ENTITY_ID
                        LEFT OUTER JOIN ENTITY DIVISION
                        ON DIVISION.PARENT_EID = REGION.ENTITY_ID
                        LEFT OUTER JOIN ENTITY LOCATION
                        ON LOCATION.PARENT_EID = DIVISION.ENTITY_ID
                        LEFT OUTER JOIN ENTITY FACILITY
                        ON FACILITY.PARENT_EID = LOCATION.ENTITY_ID
                        LEFT OUTER JOIN ENTITY DEPARTMENT
                        ON DEPARTMENT.PARENT_EID = FACILITY.ENTITY_ID 
                  WHERE 
                         CLIENT.ENTITY_TABLE_ID = 1005''
		exec sp_executesql @sSql			
        -- Create Indexes for Fast Retrival
        SET @sSql = ''CREATE INDEX OHT_DEPT_IDX ON OH_MAP_TMP(DEPARTMENT_EID)''
        exec sp_executesql @sSql
        SET @sSql = ''CREATE INDEX OHT_FAC_IDX ON OH_MAP_TMP(FACILITY_EID)''
        exec sp_executesql @sSql
        SET @sSql = ''CREATE INDEX OHT_LOC_IDX ON OH_MAP_TMP(LOCATION_EID)''
        exec sp_executesql @sSql
        SET @sSql = ''CREATE INDEX OHT_DIV_IDX ON OH_MAP_TMP(DIVISION_EID)''
        exec sp_executesql @sSql
        SET @sSql = ''CREATE INDEX OHT_REG_IDX ON OH_MAP_TMP(REGION_EID)''
        exec sp_executesql @sSql
        SET @sSql = ''CREATE INDEX OHT_OP_IDX ON OH_MAP_TMP(OPERATION_EID)''
        exec sp_executesql @sSql
        SET @sSql = ''CREATE INDEX OHT_COMP_IDX ON OH_MAP_TMP(COMPANY_EID)''
        exec sp_executesql @sSql
        SET @sSql = ''CREATE INDEX OHT_CLIENT_IDX ON OH_MAP_TMP(CLIENT_EID)''
        exec sp_executesql @sSql
		
		SET @sSql = ''CREATE TABLE ENTITY_MAP_TEMP (ENTITY_ID INT NULL, GROUP_ID INT NULL)''
        exec sp_executesql @sSql

        -- Loop through all the Groups and Process it while adding data to Entity Map
        DECLARE GROUPIDCURSOR CURSOR FOR  
        SELECT ORG_SECURITY.GROUP_ID, GROUP_ENTITIES 
        FROM ORG_SECURITY INNER JOIN ORG_SECURITY_USER
        ON ORG_SECURITY.GROUP_ID = ORG_SECURITY_USER.GROUP_ID
        WHERE GROUP_ENTITIES <> ''<ALL>''

            OPEN GROUPIDCURSOR  
               FETCH GROUPIDCURSOR INTO @GroupId, @GroupEntities 
					
				 WHILE @@FETCH_STATUS = 0
				 BEGIN
                    -- Process the Departments in Entity Map for the same Group Id
                    SET @sSql =  ''INSERT INTO ENTITY_MAP_TEMP(GROUP_ID,ENTITY_ID) 
                              SELECT DISTINCT '' + @GroupId + '',DEPARTMENT_EID FROM OH_MAP_TMP
                              WHERE DEPARTMENT_EID IN ('' + @GroupEntities + '')
                                    OR FACILITY_EID IN ('' + @GroupEntities + '')
                                    OR LOCATION_EID IN ('' + @GroupEntities + '')
                                    OR DIVISION_EID IN ('' + @GroupEntities + '')
                                    OR REGION_EID IN ('' + @GroupEntities + '')
                                    OR OPERATION_EID IN ('' + @GroupEntities + '')
                                    OR COMPANY_EID IN ('' + @GroupEntities + '')
                                    OR CLIENT_EID IN ('' + @GroupEntities + '')''

                    exec sp_executesql @sSql  

                    SET @sSql = replace(@sSql, '',DEPARTMENT_EID'', '',FACILITY_EID'')
                    exec sp_executesql @sSql 

                    SET @sSql = replace(@sSql, '',FACILITY_EID'', '',LOCATION_EID'')
                    exec sp_executesql @sSql  


                    SET @sSql = replace(@sSql, '',LOCATION_EID'', '',DIVISION_EID'')
                    exec sp_executesql @sSql  


                    SET @sSql = replace(@sSql, '',DIVISION_EID'', '',REGION_EID'')
                    exec sp_executesql @sSql  


                    SET @sSql = replace(@sSql, '',REGION_EID'', '',OPERATION_EID'')
                    exec sp_executesql @sSql  


                    SET @sSql = replace(@sSql, '',OPERATION_EID'', '',COMPANY_EID'')
                    exec sp_executesql @sSql 


                    SET @sSql = replace(@sSql, '',COMPANY_EID'', '',CLIENT_EID'')
                    exec sp_executesql @sSql
		
					-- Handled the case when Sec_Dept_eid is Null or 0. For Example when a Payment is made directly
					-- in That case Sec_Dept_eid in Funds will be 0.
					SET @sSql =  ''INSERT INTO ENTITY_MAP_TEMP (GROUP_ID, ENTITY_ID) 
									VALUES ('' + @GroupId + '',0)''
					exec sp_executesql @sSql

					FETCH GROUPIDCURSOR INTO @GroupId, @GroupEntities 
			     END  
            CLOSE GROUPIDCURSOR  
			DEALLOCATE GROUPIDCURSOR
            
			-- Drop the Table
            SET @sSql = ''DROP TABLE ENTITY_MAP''
            exec sp_executesql @sSql

			SET @sSql = ''SELECT DISTINCT ENTITY_ID,GROUP_ID  INTO ENTITY_MAP
								FROM ENTITY_MAP_TEMP 
								WHERE ENTITY_ID IS NOT NULL''
			exec sp_executesql @sSql
			
			ALTER TABLE ENTITY_MAP ALTER COLUMN ENTITY_ID INT NOT NULL
			ALTER TABLE ENTITY_MAP ALTER COLUMN GROUP_ID INT NOT NULL
			ALTER TABLE ENTITY_MAP ADD CONSTRAINT PK_ENTITY_MAP PRIMARY KEY (ENTITY_ID,GROUP_ID) 

			SET @sSql = ''UPDATE ORG_SECURITY SET UPDATED_FLAG = -1 WHERE GROUP_ENTITIES <> ''''<ALL>''''''
			exec sp_executesql @sSql
			
			end try
		begin catch
			SELECT	@out_err = error_message()
		end catch
'
			

EXECUTE sp_executesql @spSQL
GO

