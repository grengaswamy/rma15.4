[GO_DELIMITER]
create or replace
PROCEDURE USP_CREATE_TRIGGERS (sRm_USERID varchar2,iConfRec INTEGER)    
  AUTHID CURRENT_USER IS    
  BEGIN    
  	DECLARE    
  		iFLAG INTEGER;    
  		iTriggerCount INTEGER;    
  		sSQL VARCHAR2(5000);    
      
  	BEGIN    
 		sSQL:='SELECT COUNT(*) FROM ALL_TRIGGERS WHERE TABLE_NAME=''EVENT'' AND STATUS=''ENABLED'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.EVENT DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
      
 		sSQL:='SELECT COUNT(*) FROM ALL_TRIGGERS WHERE TABLE_NAME=''PERSON_INVOLVED'' AND STATUS=''ENABLED'' AND  OWNER='''||UPPER(sRM_USERID)||'''';  
                  EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.PERSON_INVOLVED DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;    
      
 		sSQL:='SELECT COUNT(*) FROM ALL_TRIGGERS WHERE TABLE_NAME=''ENTITY'' AND STATUS=''ENABLED'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                  EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.ENTITY DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
      
 		sSQL:='SELECT COUNT(*)   FROM ALL_TRIGGERS WHERE TABLE_NAME=''EMPLOYEE'' AND STATUS=''ENABLED'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.EMPLOYEE DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
      
 		sSQL:='SELECT COUNT(*)   FROM ALL_TRIGGERS WHERE TABLE_NAME=''CLAIM'' AND STATUS=''ENABLED'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.CLAIM DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
      
 		sSQL:='SELECT COUNT(*)   FROM ALL_TRIGGERS WHERE TABLE_NAME=''CLAIMANT'' AND STATUS=''ENABLED'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.CLAIMANT DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;    
      
 		sSQL:='SELECT COUNT(*)   FROM ALL_TRIGGERS WHERE TABLE_NAME=''FUNDS'' AND STATUS=''ENABLED'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.FUNDS DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;     
      
 		sSQL:='SELECT COUNT(*)   FROM ALL_TRIGGERS WHERE TABLE_NAME=''RESERVE_CURRENT'' AND STATUS=''ENABLED'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.RESERVE_CURRENT DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;    
      
 		sSQL:='SELECT COUNT(*)   FROM ALL_TRIGGERS WHERE TABLE_NAME=''RESERVE_HISTORY'' AND STATUS=''ENABLED'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.RESERVE_HISTORY DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
      
  	IF(iConfRec = -1) THEN    
  		sSQL:='SELECT COUNT(*)   FROM ALL_TRIGGERS WHERE TABLE_NAME=''FUNDS_AUTO'' AND STATUS=''ENABLED'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iFLAG;  
 		IF iFLAG >0 THEN  
 			sSQL:='ALTER TABLE '||sRM_USERID||'.FUNDS_AUTO DISABLE ALL TRIGGERS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
 	END IF;    
      
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''EVENT_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.EVENT_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;  
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*) FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''EVENT_UPD'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.EVENT_UPD';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*) FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''CLAIM_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.CLAIM_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;  
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)   FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''CLAIM_UPD'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.CLAIM_UPD';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''PERSON_INVOLVED_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.PERSON_INVOLVED_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;  
   
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''PERSON_INVOLVED_UPD'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.PERSON_INVOLVED_UPD';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;     
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)   FROM ALL_OBJECTS WHERE OBJECT_TYPE =''TRIGGER'' AND OBJECT_NAME=''CLAIMANT_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 			IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.CLAIMANT_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;    
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''FUNDS_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.FUNDS_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;  
 		  
  		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''FUNDS_AUTO_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.FUNDS_AUTO_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;    
    
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''RESERVE_HISTORY_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.RESERVE_HISTORY_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;  
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*) FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''RESERVE_CURRENT_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.RESERVE_CURRENT_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''ENTITY_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.ENTITY_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;  
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''EMPLOYEE_INS'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.EMPLOYEE_INS';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;  
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''EMPLOYEE_UPD'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.EMPLOYEE_UPD';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;  
  	    
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''ENTITY_TO_ENTITY_MAP'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.ENTITY_TO_ENTITY_MAP';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;  
      
 		iTriggerCount:=0;  
 		sSQL:='SELECT COUNT(*)  FROM ALL_OBJECTS WHERE OBJECT_TYPE = ''TRIGGER'' AND OBJECT_NAME=''ORG_SECURITY_UPD'' AND OWNER='''||UPPER(sRM_USERID)||'''';  
                 EXECUTE IMMEDIATE sSQL INTO iTriggerCount;  
 		IF (iTriggerCount=1) THEN     
 			sSQL:='DROP TRIGGER '||sRM_USERID||'.ORG_SECURITY_UPD';  
 			EXECUTE IMMEDIATE sSQL;  
 		END IF;   
      
 		sSQL:='CREATE OR REPLACE TRIGGER '||sRM_USERID||'.EVENT_INS BEFORE INSERT ON '||sRM_USERID||'.EVENT   
  				FOR EACH ROW     
  				BEGIN    
  					:new.SEC_DEPT_EID := :new.dept_eid;       
  				END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
 		sSQL:='CREATE OR REPLACE TRIGGER '||sRM_USERID||'.EVENT_UPD BEFORE UPDATE OF DEPT_EID ON '||sRM_USERID||'.EVENT   
  				FOR EACH ROW    
  				BEGIN    
  					IF :old.DEPT_EID != :new.DEPT_EID THEN    
  						:new.SEC_DEPT_EID := :new.DEPT_EID;    
  						UPDATE PERSON_INVOLVED SET SEC_DEPT_EID = :new.dept_eid WHERE EVENT_ID = :new.event_id;    
  						UPDATE CLAIM SET SEC_DEPT_EID = :new.dept_eid WHERE EVENT_ID = :new.event_id;    
  					END IF;    
  					IF :old.CONF_EVENT_ID != :new.CONF_EVENT_ID THEN    
  						UPDATE PERSON_INVOLVED SET CONF_EVENT_ID = :new.CONF_EVENT_ID WHERE EVENT_ID = :new.EVENT_ID;    
  						UPDATE ENTITY SET CONF_EVENT_ID=1 WHERE ENTITY_ID=:new.DEPT_EID;     
  						UPDATE ENTITY SET CONF_EVENT_ID=1 WHERE ENTITY_ID IN (SELECT FACILITY_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=:new.DEPT_EID );    
  						UPDATE ENTITY SET CONF_EVENT_ID=1 WHERE ENTITY_ID IN (SELECT LOCATION_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=:new.DEPT_EID );    
  						UPDATE ENTITY SET CONF_EVENT_ID=1 WHERE ENTITY_ID IN (SELECT DIVISION_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=:new.DEPT_EID );    
  						UPDATE ENTITY SET CONF_EVENT_ID=1 WHERE ENTITY_ID IN (SELECT REGION_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=:new.DEPT_EID );    
  						UPDATE ENTITY SET CONF_EVENT_ID=1 WHERE ENTITY_ID IN (SELECT OPERATION_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=:new.DEPT_EID );    
  						UPDATE ENTITY SET CONF_EVENT_ID=1 WHERE ENTITY_ID IN (SELECT COMPANY_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=:new.DEPT_EID );    
  						UPDATE ENTITY SET CONF_EVENT_ID=1 WHERE ENTITY_ID IN (SELECT CLIENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=:new.DEPT_EID );    
  						UPDATE CLAIM SET CONF_EVENT_ID = :new.CONF_EVENT_ID WHERE EVENT_ID=:new.EVENT_ID;    
  					END IF;    
  				END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
 		sSQL:='CREATE OR REPLACE TRIGGER '||sRM_USERID||'.CLAIM_INS BEFORE INSERT ON '||sRM_USERID||'.CLAIM   
  				FOR EACH ROW    
  				DECLARE  
                                   HOLD_SEC_DEPT_EID NUMBER;    
                                   HOLD_CONF_EVENT_ID NUMBER;    
                                   HOLD_CONF_FLAG NUMBER;    
  				BEGIN    
  					SELECT EVENT.DEPT_EID, EVENT.CONF_FLAG, EVENT.CONF_EVENT_ID INTO HOLD_SEC_DEPT_EID, HOLD_CONF_FLAG, HOLD_CONF_EVENT_ID FROM EVENT WHERE EVENT.EVENT_ID = :new.EVENT_ID;    
  					:new.SEC_DEPT_EID := HOLD_SEC_DEPT_EID;';    
  				IF(iConfRec = -1) THEN    
  					sSQL:=sSQL || ' :new.CONF_FLAG := HOLD_CONF_FLAG;     
  									:new.CONF_EVENT_ID := HOLD_CONF_EVENT_ID;';    
  				END IF;    
  		sSQL:= sSQL || ' END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
 		sSQL:=' CREATE OR REPLACE TRIGGER '||sRM_USERID||'.CLAIM_UPD BEFORE UPDATE OF SEC_DEPT_EID ON '||sRM_USERID||'.CLAIM  
  				FOR EACH ROW    
  				BEGIN    
  					IF :old.SEC_DEPT_EID != :new.SEC_DEPT_EID THEN    
  						UPDATE CLAIMANT SET SEC_DEPT_EID = :new.sec_dept_eid WHERE CLAIM_ID = :new.claim_id;    
  						UPDATE FUNDS SET SEC_DEPT_EID = :new.sec_dept_eid WHERE CLAIM_ID = :new.claim_id;    
  						UPDATE RESERVE_CURRENT SET SEC_DEPT_EID = :new.sec_dept_eid WHERE CLAIM_ID = :new.claim_id;    
  						UPDATE RESERVE_HISTORY SET SEC_DEPT_EID = :new.sec_dept_eid WHERE CLAIM_ID = :new.claim_id;    
  					END IF;    
  					IF :old.CONF_EVENT_ID != :new.CONF_EVENT_ID THEN    
  						UPDATE CLAIMANT SET CONF_EVENT_ID = :new.CONF_EVENT_ID WHERE CLAIM_ID = :new.CLAIM_ID;   
  						UPDATE FUNDS SET CONF_EVENT_ID = :new.CONF_EVENT_ID WHERE CLAIM_ID = :new.CLAIM_ID;    
  						UPDATE FUNDS_AUTO SET CONF_EVENT_ID = :new.CONF_EVENT_ID WHERE CLAIM_ID=:new.CLAIM_ID;    
  						UPDATE RESERVE_CURRENT SET CONF_EVENT_ID = :new.CONF_EVENT_ID WHERE CLAIM_ID = :new.CLAIM_ID;    
  						UPDATE RESERVE_HISTORY SET CONF_EVENT_ID = :new.CONF_EVENT_ID WHERE CLAIM_ID = :new.CLAIM_ID;    
  					END IF;    
  					IF :old.CONF_FLAG != :new.CONF_FLAG THEN    
  						UPDATE CLAIMANT SET CONF_FLAG = :new.CONF_FLAG WHERE CLAIM_ID = :new.CLAIM_ID;   
  						UPDATE FUNDS SET CONF_FLAG = :new.CONF_FLAG WHERE CLAIM_ID = :new.CLAIM_ID;    
  						UPDATE FUNDS_AUTO SET CONF_FLAG = :new.CONF_FLAG WHERE CLAIM_ID=:new.CLAIM_ID;    
  						UPDATE RESERVE_CURRENT SET CONF_FLAG = :new.CONF_FLAG WHERE CLAIM_ID = :new.CLAIM_ID;    
  						UPDATE RESERVE_HISTORY SET CONF_FLAG = :new.CONF_FLAG WHERE CLAIM_ID = :new.CLAIM_ID;    
  					END IF;    
  				END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
 		sSQL:=' CREATE OR REPLACE TRIGGER '||sRM_USERID||'.PERSON_INVOLVED_INS BEFORE INSERT ON '||sRM_USERID||'.PERSON_INVOLVED   
  				FOR EACH ROW    
  				DECLARE   
                                   HOLD_SEC_DEPT_EID NUMBER;    
                                   HOLD_CONF_EVENT_ID NUMBER;    
                                   HOLD_CONF_FLAG NUMBER;    
  				BEGIN    
  					SELECT EVENT.DEPT_EID, EVENT.CONF_FLAG, EVENT.CONF_EVENT_ID INTO HOLD_SEC_DEPT_EID, HOLD_CONF_FLAG, HOLD_CONF_EVENT_ID FROM EVENT WHERE EVENT.EVENT_ID = :new.EVENT_ID;            
  					:new.SEC_DEPT_EID := HOLD_SEC_DEPT_EID;';    
  				IF(iConfRec = -1) THEN    
  					sSQL:=sSQL || ' :new.CONF_FLAG := HOLD_CONF_FLAG;     
  									:new.CONF_EVENT_ID := HOLD_CONF_EVENT_ID;';    
  				END IF;    
  		sSQL:= sSQL || ' END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
      
  		IF(iConfRec = -1) THEN    
  		sSQL:='CREATE OR REPLACE TRIGGER '||sRM_USERID||'.PERSON_INVOLVED_UPD BEFORE UPDATE OF CONF_EVENT_ID ON '||sRM_USERID||'.PERSON_INVOLVED    
  				FOR EACH ROW    
  				BEGIN    
  					IF :old.CONF_EVENT_ID != :new.CONF_EVENT_ID THEN    
  						UPDATE ENTITY SET CONF_EVENT_ID=1 WHERE ENTITY_ID=:new.PI_EID AND ENTITY_TABLE_ID = 1060;    
  					END IF;    
  				END;';    
  	    EXECUTE IMMEDIATE sSQL;    
  	   END IF;    
      
  		sSQL:=' CREATE OR REPLACE TRIGGER '||sRM_USERID||'.CLAIMANT_INS BEFORE INSERT ON '||sRM_USERID||'.CLAIMANT     
  				FOR EACH ROW    
  				DECLARE  
                                   HOLD_SEC_DEPT_EID NUMBER;    
                                   HOLD_CONF_EVENT_ID NUMBER;    
                                   HOLD_CONF_FLAG NUMBER;    
  				BEGIN    
      				SELECT CLAIM.SEC_DEPT_EID, CLAIM.CONF_FLAG, CLAIM.CONF_EVENT_ID INTO HOLD_SEC_DEPT_EID, HOLD_CONF_FLAG, HOLD_CONF_EVENT_ID FROM CLAIM WHERE CLAIM.CLAIM_ID = :new.CLAIM_ID;          
  					:new.SEC_DEPT_EID := HOLD_SEC_DEPT_EID;';    
  				IF(iConfRec = -1) THEN    
  					sSQL:=sSQL || ' :new.CONF_FLAG := HOLD_CONF_FLAG;     
  									:new.CONF_EVENT_ID := HOLD_CONF_EVENT_ID;';   
  				END IF;    
  		sSQL:= sSQL || ' END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
 		sSQL:='CREATE OR REPLACE TRIGGER '||sRM_USERID||'.FUNDS_INS BEFORE INSERT ON '||sRM_USERID||'.FUNDS   
  				FOR EACH ROW    
  				DECLARE  
                                   HOLD_SEC_DEPT_EID NUMBER;    
                                   HOLD_CONF_EVENT_ID NUMBER;    
                                   HOLD_CONF_FLAG NUMBER;    
  				BEGIN    
 				  IF :new.CLAIM_ID <> 0 THEN   
  					SELECT CLAIM.SEC_DEPT_EID, CLAIM.CONF_FLAG, CLAIM.CONF_EVENT_ID INTO HOLD_SEC_DEPT_EID, HOLD_CONF_FLAG, HOLD_CONF_EVENT_ID FROM CLAIM WHERE CLAIM.CLAIM_ID = :new.CLAIM_ID;          
  					:new.SEC_DEPT_EID := HOLD_SEC_DEPT_EID;';    
  				IF(iConfRec = -1) THEN    
  					sSQL:=sSQL || ' :new.CONF_FLAG := HOLD_CONF_FLAG;     
  									:new.CONF_EVENT_ID := HOLD_CONF_EVENT_ID;';    
  				END IF;    
  		sSQL:= sSQL || 'END IF;  
 		 END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
 		sSQL:='CREATE OR REPLACE TRIGGER '||sRM_USERID||'.RESERVE_HISTORY_INS BEFORE INSERT ON '||sRM_USERID||'.RESERVE_HISTORY   
  				FOR EACH ROW    
  				DECLARE   
                                   HOLD_SEC_DEPT_EID NUMBER;    
                                   HOLD_CONF_EVENT_ID NUMBER;    
                                   HOLD_CONF_FLAG NUMBER;    
  				BEGIN    
  					SELECT CLAIM.SEC_DEPT_EID, CLAIM.CONF_FLAG, CLAIM.CONF_EVENT_ID INTO HOLD_SEC_DEPT_EID, HOLD_CONF_FLAG, HOLD_CONF_EVENT_ID FROM CLAIM WHERE CLAIM.CLAIM_ID = :new.CLAIM_ID;    
  					:new.SEC_DEPT_EID := HOLD_SEC_DEPT_EID;';    
  				IF(iConfRec = -1) THEN    
  					sSQL:=sSQL || ' :new.CONF_FLAG := HOLD_CONF_FLAG;     
  									:new.CONF_EVENT_ID := HOLD_CONF_EVENT_ID;';    
  				END IF;    
  		sSQL:= sSQL || ' END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
 		sSQL:=' CREATE OR REPLACE TRIGGER '||sRM_USERID||'.RESERVE_CURRENT_INS BEFORE INSERT ON '||sRM_USERID||'.RESERVE_CURRENT   
  				FOR EACH ROW    
  				DECLARE  
                                   HOLD_SEC_DEPT_EID NUMBER;    
                                   HOLD_CONF_EVENT_ID NUMBER;    
                                   HOLD_CONF_FLAG NUMBER;    
  				BEGIN    
  					SELECT CLAIM.SEC_DEPT_EID, CLAIM.CONF_FLAG, CLAIM.CONF_EVENT_ID INTO HOLD_SEC_DEPT_EID, HOLD_CONF_FLAG, HOLD_CONF_EVENT_ID FROM CLAIM WHERE CLAIM.CLAIM_ID = :new.CLAIM_ID;    
  					:new.SEC_DEPT_EID := HOLD_SEC_DEPT_EID;';    
  				IF(iConfRec = -1) THEN    
  					sSQL:=sSQL || ' :new.CONF_FLAG := HOLD_CONF_FLAG;     
  									:new.CONF_EVENT_ID := HOLD_CONF_EVENT_ID;';    
  				END IF;    
  		sSQL:= sSQL || ' END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
  		IF(iConfRec = -1) THEN    
  		sSQL:=' CREATE OR REPLACE TRIGGER '||sRM_USERID||'.FUNDS_AUTO_INS BEFORE INSERT ON '||sRM_USERID||'.FUNDS_AUTO     
  			    FOR EACH ROW    
  				DECLARE  
                                   HOLD_CONF_EVENT_ID NUMBER;    
                                   HOLD_CONF_FLAG NUMBER;	    
  				BEGIN    
  					SELECT CLAIM.CONF_FLAG, CLAIM.CONF_EVENT_ID INTO HOLD_CONF_FLAG, HOLD_CONF_EVENT_ID FROM CLAIM WHERE CLAIM.CLAIM_ID = :new.CLAIM_ID;    
  					:new.CONF_FLAG := HOLD_CONF_FLAG;    
  					:new.CONF_EVENT_ID := HOLD_CONF_EVENT_ID;    
  				END;';    
  		EXECUTE IMMEDIATE sSQL;    
  		END IF;    
      
 		sSQL:='CREATE OR REPLACE TRIGGER '||sRM_USERID||'.ENTITY_INS BEFORE INSERT ON '||sRM_USERID||'.ENTITY   
  				FOR EACH ROW    
  				DECLARE  
                                   HOLD_SEC_DEPT_EID NUMBER;    
  				BEGIN    
  					if :new.ENTITY_TABLE_ID = 1060 then    
  						SELECT EMPLOYEE.SEC_DEPT_EID INTO HOLD_SEC_DEPT_EID FROM EMPLOYEE WHERE EMPLOYEE.EMPLOYEE_EID = :new.ENTITY_ID;    
  						:new.SEC_DEPT_EID := HOLD_SEC_DEPT_EID;    
  					elsif :new.ENTITY_TABLE_ID BETWEEN 1005 and 1012 then    
  						:new.SEC_DEPT_EID := :new.ENTITY_ID;    
  					else    
  						:new.SEC_DEPT_EID := 0;    
  					end if;	    
  					exception    
  					when others then    
  						:new.SEC_DEPT_EID := 0;    
  				END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
 		sSQL:='CREATE OR REPLACE TRIGGER '||sRM_USERID||'.EMPLOYEE_INS BEFORE INSERT ON '||sRM_USERID||'.EMPLOYEE   
  				FOR EACH ROW     
  				BEGIN    
  					:new.SEC_DEPT_EID := :new.DEPT_ASSIGNED_EID;    
  					UPDATE ENTITY SET SEC_DEPT_EID = :new.SEC_DEPT_EID WHERE ENTITY.ENTITY_ID = :new.EMPLOYEE_EID;    
  				END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
 		sSQL:='CREATE OR REPLACE TRIGGER '||sRM_USERID||'.EMPLOYEE_UPD BEFORE UPDATE OF DEPT_ASSIGNED_EID ON '||sRM_USERID||'.EMPLOYEE   
  				FOR EACH ROW     
  				BEGIN    
  					IF :new.DEPT_ASSIGNED_EID != :old.DEPT_ASSIGNED_EID  THEN    
  						:new.SEC_DEPT_EID := :new.DEPT_ASSIGNED_EID;    
  						UPDATE ENTITY SET SEC_DEPT_EID = :new.SEC_DEPT_EID WHERE ENTITY.ENTITY_ID = :new.EMPLOYEE_EID;    
  					END IF;    
  				END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
      
  		sSQL:='CREATE OR REPLACE    
 				TRIGGER '||sRM_USERID||'.ENTITY_TO_ENTITY_MAP BEFORE INSERT on '||sRM_USERID||'.ENTITY FOR EACH ROW  
      
  				BEGIN    
  				          
  					DECLARE  
                                         EntityId VARCHAR2(100);    
  					Inserted_Eid VARCHAR2(100);    
  					Entity_Tableid VARCHAR2(100);    
  					InsertedEntity_Tableid VARCHAR2(100);    
  					ParentEid VARCHAR2(100);    
  					InsertedParent_Eid VARCHAR2(100);    
  					GroupId VARCHAR2(100);    
  					GroupEntities VARCHAR2(4000);    
  					nvsql varchar2(4000);    
  					iCount int;    
  						BEGIN    
  						  IF (:new.ENTITY_TABLE_ID >= 1005 AND :new.ENTITY_TABLE_ID <= 1012) THEN    
  							BEGIN    
  							  EntityId := :new.ENTITY_ID;    
  							  Entity_Tableid := :new.ENTITY_TABLE_ID;    
  							  ParentEid := :new.PARENT_EID;    
  							End;      
  						  End if;      
  				           
  						  IF (EntityId IS NOT NULL) THEN    
  							BEGIN    
  									Inserted_Eid := EntityId;    
  									InsertedEntity_Tableid := Entity_Tableid;    
  									InsertedParent_Eid := ParentEid;    
  				                        
  									    
  									DECLARE CURSOR GROUPIDCURSOR IS     
  									SELECT	GROUP_ID, GROUP_ENTITIES    
  									FROM	ORG_SECURITY WHERE GROUP_ENTITIES <> ''<ALL>'';    
  									BEGIN    
  											OPEN GROUPIDCURSOR;    
  				                                
  											LOOP     
  													FETCH GROUPIDCURSOR INTO GroupId,GroupEntities;    
  													EXIT WHEN GROUPIDCURSOR%NOTFOUND;    
  				        
  													IF( GroupId IS NOT NULL ) THEN    
  												  BEGIN    
  														  WHILE Entity_Tableid >1005    
  														  LOOP    
  																  EntityId := ParentEid;			    
  																  iCount := 0;    
  																  Select Group_entities INTO GroupEntities from Org_security  Where Group_id = GroupId;    
  				                                                      
  																  nvsql := ''SELECT  COUNT(*)  FROM ORG_SECURITY INNER JOIN ENTITY_MAP    
  																						  ON ORG_SECURITY.GROUP_ID = ENTITY_MAP.GROUP_ID WHERE ENTITY_MAP.ENTITY_ID  IN ('' || GroupEntities || '')  AND ORG_SECURITY.GROUP_ID = ''|| GroupId ||    
  																						  '' AND ENTITY_MAP.ENTITY_ID = '' || EntityId;    
  				                                                      
  																  EXECUTE IMMEDIATE nvsql INTO iCount;    
      
  																  IF iCount >0 THEN    
  																  BEGIN    
  																		  INSERT INTO ENTITY_MAP(ENTITY_ID,GROUP_ID) VALUES (Inserted_Eid, GroupId);    
  																		  EXIT;    
  																  END;    
  																  END IF;    
  				                                                      
  																  SELECT ENTITY_TABLE_ID INTO Entity_Tableid FROM ENTITY WHERE ENTITY.ENTITY_ID = EntityId;    
  																  SELECT PARENT_EID INTO ParentEid FROM ENTITY WHERE ENTITY.ENTITY_ID = EntityId;    
  														  END LOOP;    
  													END;    
  												  END IF;    
  												EntityId := Inserted_Eid;    
  												Entity_Tableid := InsertedEntity_Tableid;    
  												ParentEid := InsertedParent_Eid;    
  											 END LOOP;    
  											CLOSE GROUPIDCURSOR;    
  									END;    
  							END;    
  						  END IF;    
  						END;      
  				END;';    
      
  		EXECUTE IMMEDIATE sSQL;    
      
      
  	sSQL:='create or replace    
 					TRIGGER '||sRM_USERID||'.ORG_SECURITY_UPD BEFORE UPDATE ON '||sRM_USERID||'.ORG_SECURITY   
  			FOR EACH ROW    
  			BEGIN    
  				IF (:old.GROUP_ADJ_TEXT_TYPES != :new.GROUP_ADJ_TEXT_TYPES)     
  					OR (:old.GROUP_ENH_NOTES_TYPES != :new.GROUP_ENH_NOTES_TYPES)    
  					OR (:old.GROUP_INSURERS != :new.GROUP_INSURERS )    
  					OR (:old.GROUP_BROKERS != :new.GROUP_BROKERS )    
  					OR (:old.GROUP_ADJ_TEXT_TYPES IS NULL AND :new.GROUP_ADJ_TEXT_TYPES IS NOT NULL)    
  					OR (:old.GROUP_ADJ_TEXT_TYPES IS NOT NULL AND :new.GROUP_ADJ_TEXT_TYPES IS NULL)    
  					OR (:old.GROUP_ENH_NOTES_TYPES IS NULL AND :new.GROUP_ENH_NOTES_TYPES IS NOT NULL)    
  					OR (:old.GROUP_ENH_NOTES_TYPES IS NOT NULL AND :new.GROUP_ENH_NOTES_TYPES IS NULL)    
  			    	OR (:old.GROUP_INSURERS IS NULL AND :new.GROUP_INSURERS IS NOT NULL)    
  					OR (:old.GROUP_INSURERS IS NOT NULL AND :new.GROUP_INSURERS IS NULL)    
  					OR (:old.GROUP_BROKERS IS NULL AND :new.GROUP_BROKERS IS NOT NULL)    
  					OR (:old.GROUP_BROKERS IS NOT NULL AND :new.GROUP_BROKERS IS NULL)    
  				THEN    
 					UPDATE  '||sRM_USERID||'.GROUP_MAP SET UPDATED_FLAG =-1,STATUS_FLAG =0 WHERE SUPER_GROUP_ID = :new.GROUP_ID ;  
  					  
  				END IF;    
  			END;';    
  		EXECUTE IMMEDIATE sSQL;    
      
  		sSQL:='ALTER TABLE '||sRM_USERID||'.CLAIM ENABLE ALL TRIGGERS';    
  		EXECUTE IMMEDIATE sSQL;    
        
  		sSQL:='ALTER TABLE '||sRM_USERID||'.CLAIMANT ENABLE ALL TRIGGERS';    
  		EXECUTE IMMEDIATE sSQL;    
       
  		sSQL:='ALTER TABLE '||sRM_USERID||'.EMPLOYEE ENABLE ALL TRIGGERS';    
  		EXECUTE IMMEDIATE sSQL;    
       
  		sSQL:='ALTER TABLE '||sRM_USERID||'.ENTITY ENABLE ALL TRIGGERS';    
  		EXECUTE IMMEDIATE sSQL;    
       
  		sSQL:='ALTER TABLE '||sRM_USERID||'.EVENT ENABLE ALL TRIGGERS';    
  		EXECUTE IMMEDIATE sSQL;    
       
  		sSQL:='ALTER TABLE '||sRM_USERID||'.FUNDS ENABLE ALL TRIGGERS';    
  		EXECUTE IMMEDIATE sSQL;    
       
  		sSQL:='ALTER TABLE '||sRM_USERID||'.PERSON_INVOLVED ENABLE ALL TRIGGERS';    
  		EXECUTE IMMEDIATE sSQL;    
       
  		sSQL:='ALTER TABLE '||sRM_USERID||'.RESERVE_CURRENT ENABLE ALL TRIGGERS';    
  		EXECUTE IMMEDIATE sSQL;    
      
  		sSQL:='ALTER TABLE '||sRM_USERID||'.RESERVE_HISTORY ENABLE ALL TRIGGERS';    
  		EXECUTE IMMEDIATE sSQL;    
  	    
  		IF(iConfRec = -1) THEN   
  			sSQL:='ALTER TABLE '||sRM_USERID||'.FUNDS_AUTO ENABLE ALL TRIGGERS';   		   
 			EXECUTE IMMEDIATE sSQL;    
  		END IF;    
  	END;    
  END;    
GO