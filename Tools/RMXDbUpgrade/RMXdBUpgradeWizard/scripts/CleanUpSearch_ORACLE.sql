[GO_DELIMITER]
create or replace
PROCEDURE CleanUpSearch (p_Table_Name  varchar2,p_sUserName varchar2)    
   AUTHID CURRENT_USER AS     
 BEGIN    
   DECLARE  
   --it_exists INTEGER;      
   sSQL VARCHAR2(2000);  
   --sObjtype varchar2(100);  
   --eExcptn EXCEPTION;       
   BEGIN     
     --sSQL:='Select count(1) from all_objects where owner ='''|| UPPER(p_sUserName)||''' and object_name='''||UPPER(p_Table_Name)||'''';     
     --EXECUTE IMMEDIATE sSQL INTO it_exists;  
     --IF (it_exists >= 1) THEN  
     --BEGIN  
        --sSQL:='Select object_type from all_objects where owner ='''|| UPPER(p_sUserName)||''' and object_name='''||UPPER(p_Table_Name)||'''';     
        --EXECUTE IMMEDIATE sSQL INTO sObjtype;       
        --IF (sObjtype = 'SYNONYM') THEN   
         --BEGIN   
          --sSQL := 'drop synonym '|| UPPER(p_sUserName) || '.'|| UPPER(p_Table_Name);   
          --EXECUTE IMMEDIATE sSQL;        
         --END;   
        --ELSIF (sObjtype = 'TABLE') THEN  
         --BEGIN   
          --sSQL := 'drop table '|| UPPER(p_sUserName) || '.'|| UPPER(p_Table_Name);   
          --EXECUTE IMMEDIATE sSQL;        
         --END;  
        --END IF;   
     --END;  
     --END IF;  
     sSQL := 'drop table ' || UPPER(p_sUserName) || '.'|| UPPER(p_Table_Name); 
     EXECUTE IMMEDIATE sSQL; 
     EXCEPTION
      WHEN OTHERS THEN
       IF (SQLCODE = -942) THEN
        BEGIN
         sSQL := 'drop synonym '|| UPPER(p_sUserName) || '.'|| UPPER(p_Table_Name);     
         EXECUTE IMMEDIATE sSQL;
         EXCEPTION
         WHEN OTHERS THEN
         null;
        END; 
       ELSE
        null; 
      END IF;  
   END;    
  END;  
Go
