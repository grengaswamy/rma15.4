[GO_DELIMITER]
IF OBJECT_ID('USP_FINANCIAL_HISTORY_REBUILD', 'P') IS NOT NULL
    DROP PROCEDURE USP_FINANCIAL_HISTORY_REBUILD;
GO
CREATE PROCEDURE USP_FINANCIAL_HISTORY_REBUILD
@p_jobID	varchar(50) ='direct',
@p_mode		varchar(20),
@p_claimid	varchar(100) = null ,
@p_sqlerrm	varchar(MAX) OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @CurrentPaid			decimal(20,2);
	DECLARE @CurrentCollection		decimal(20,2);
	DECLARE @CurrentReserve			decimal(20,2);
	DECLARE @CurrentIncurred		decimal(20,2);
	DECLARE @CurrentBalance			decimal(20,2);
	DECLARE @PreviousPaid			decimal(20,2);
	DECLARE @PreviousCollection		decimal(20,2);
	DECLARE @PreviousReserve		decimal(20,2);
	DECLARE @PreviousIncurred		decimal(20,2);
	DECLARE @PreviousBalance		decimal(20,2);
	Declare @m_bZeroBasedFinHist	BIT;
	Declare @m_bFullBuild			BIT;
	Declare @isEventBased			INT;
	Declare @m_DeptAbbr				VARCHAR(100);
	Declare @p_retval				VARCHAR(20);

	-- Declare type of GC LOB
	DECLARE @GCLOBParmCollectionInRsv	SMALLINT;
	DECLARE @GCLOBParmCollectionInIncurred	SMALLINT;
	DECLARE @GCLOBParmPerRsvCollectionInRsv	SMALLINT;
	DECLARE @GCLOBParmPerRsvCollectionInIncurred	SMALLINT;
	DECLARE @GCLOBParmReserveTracking	INT;
	DECLARE @GCLOBParmAdjRsv AS SMALLINT;
	DECLARE @GCLOBParmLineOfBusCode AS INT;
	DECLARE @GCLOBParmBalToZero AS SMALLINT;
	DECLARE @GCLOBParmNegBalToZero AS SMALLINT;
	DECLARE @GCLOBParmSetToZero AS SMALLINT;
	DECLARE @GCLOBParmEstNumofClaims AS INT;
	DECLARE @GCLOBParmDurationCode AS INT;

	-- Declare type of WC LOB
	DECLARE @WCLOBParmCollectionInRsv as SMALLINT;
	DECLARE @WCLOBParmCollectionInIncurred as SMALLINT;
	DECLARE @WCLOBParmPerRsvCollectionInRsv as SMALLINT;
	DECLARE @WCLOBParmPerRsvCollectionInIncurred as SMALLINT;
	DECLARE @WCLOBParmReserveTracking AS INT;
	DECLARE @WCLOBParmAdjRsv AS SMALLINT;
	DECLARE @WCLOBParmLineOfBusCode AS INT;
	DECLARE @WCLOBParmBalToZero AS SMALLINT;
	DECLARE @WCLOBParmNegBalToZero AS SMALLINT;
	DECLARE @WCLOBParmSetToZero AS SMALLINT;
	DECLARE @WCLOBParmEstNumofClaims AS INT;
	DECLARE @WCLOBParmDurationCode AS INT;

	-- Declate type of VA LOB
	DECLARE @VALOBParmCollectionInRsv as SMALLINT;
	DECLARE @VALOBParmCollectionInIncurred as SMALLINT;
	DECLARE @VALOBParmPerRsvCollectionInRsv as SMALLINT;
	DECLARE @VALOBParmPerRsvCollectionInIncurred as SMALLINT;
	DECLARE @VALOBParmReserveTracking AS INT;
	DECLARE @VALOBParmAdjRsv AS SMALLINT;
	DECLARE @VALOBParmLineOfBusCode AS INT;
	DECLARE @VALOBParmBalToZero AS SMALLINT;
	DECLARE @VALOBParmNegBalToZero AS SMALLINT;
	DECLARE @VALOBParmSetToZero AS SMALLINT;
	DECLARE @VALOBParmEstNumofClaims AS INT;
	DECLARE @VALOBParmDurationCode AS INT;

	-- Declare type NONOCC LOB
	DECLARE @NONOCCLOBParmCollectionInRsv as SMALLINT;
	DECLARE @NONOCCLOBParmCollectionInIncurred as SMALLINT;
	DECLARE @NONOCCLOBParmPerRsvCollectionInRsv as SMALLINT;
	DECLARE @NONOCCLOBParmPerRsvCollectionInIncurred as SMALLINT;
	DECLARE @NONOCCLOBParmReserveTracking AS INT;
	DECLARE @NONOCCLOBParmAdjRsv AS SMALLINT;
	DECLARE @NONOCCLOBParmLineOfBusCode AS INT;
	DECLARE @NONOCCLOBParmBalToZero AS SMALLINT;
	DECLARE @NONOCCLOBParmNegBalToZero AS SMALLINT;
	DECLARE @NONOCCLOBParmSetToZero AS SMALLINT;
	DECLARE @NONOCCLOBParmEstNumofClaims AS INT;
	DECLARE @NONOCCLOBParmDurationCode AS INT;

	-- SYS Parameters
	DECLARE @m_ESSP  AS VARCHAR(100);
	DECLARE @m_useTranDate AS BIT;
	DECLARE @m_includeVoids AS BIT;
	DECLARE @m_includeOnlyPrintedChecks AS BIT;

	--- Initlob Internal Variables----
	DECLARE @collectionInRsv  AS SMALLINT;
	DECLARE @collectionInIncurred  AS SMALLINT;
	DECLARE @perrsvcollectionInRsv  AS SMALLINT;
	DECLARE @perrsvcollectionInIncurred  AS SMALLINT;
	DECLARE @reserveTracking AS INTEGER;
	DECLARE @adjRsv AS SMALLINT;
	DECLARE @lineOfBusCode AS INT;
	DECLARE @balToZero AS SMALLINT;
	DECLARE @negBalToZero AS SMALLINT;
	DECLARE @setToZero AS SMALLINT;
	DECLARE @estNumofClaims AS INT;
	DECLARE @durationCode AS INT;
	DECLARE @cbDontCare varchar(200);
	DECLARE @bIncludeVoids varchar(200)= 0;
	DECLARE @bPrintedOnly varchar(200)= 0;
	DECLARE @sSQL nvarchar(1000);
	DECLARE @v_check_error bit=1;
	DECLARE @v_FH_VOIDS_INCLUDE_c bit;
	DECLARE @v_FH_VOIDS_INCLUDE SMALLINT;
	DECLARE @v_FH_ONLY_PRINTED bit;
	DECLARE @FIN_HIST_EVAL AS Smallint;
	DECLARE @ESSP As INT;
	DECLARE @sTime AS varchar(1000)='00000000';
	DECLARE @lastdate varchar(1000) ='00000000';
	DECLARE @cb varchar(1000)=1;
	DECLARE @lnextuniqueid INT;
	DECLARE @retVal bit;
	DECLARE @v_sqlerrm varchar(2000);
	DECLARE @bUpdateOnly bit= 0;
	
	DECLARE @v_retval varchar(100);
	DECLARE @oldString varchar(2000);
	DECLARE @sSQL2  varchar(2000);
	DECLARE @sSQL3  varchar(2000);
	DECLARE @scratchString varchar(2000);
	DECLARE @tempTime VARCHAR(100);
	
	DECLARE @counter INT=0;
	DECLARE @totalCount INT;
	DECLARE @v_count INT;
	DECLARE @sTemp varchar(2000);
	DECLARE @sTemp2 varchar(2000);
	DECLARE @iCounter INT = 0; 
	DECLARE @lClaimID varchar(1000)= -1;
	DECLARE @lClaimantEID varchar(1000)= 0;
	DECLARE @lUnitID varchar(1000)= 0;
	DECLARE @lReserveTypeCode varchar(1000)= 0;
	DECLARE @lscratch varchar(1000);
	DECLARE @cEvalDate varchar(10);
	DECLARE @err INT;
	DECLARE @sSQL_Insert varchar(2000);

	DECLARE @outstandingBalance decimal(20,2);
	DECLARE @incurred decimal(20,2);
	DECLARE @CLAIMANT_EID INT;
	DECLARE @UNIT_ID INT;
	DECLARE @RESERVE_TYPE_CODE INT;
	DECLARE @LOB INT;
	DECLARE @FH_ROW_ID INT;
	DECLARE @EVAL_DATE varchar(8); -- need to check this variable
	DECLARE @PAID decimal(20,2);
	DECLARE @COLLECTED decimal(20,2);
	DECLARE @RESERVE decimal(20,2);
	DECLARE @sBatchSQL varchar(2000);
	DECLARE @bClosed bit= 0;
	DECLARE @bCollInRsvBal bit;
	DECLARE @bCollInIncurred bit;
	DECLARE @bPerRsvCollInRsvBal bit;
	DECLARE @bPerRsvCollInIncurred bit;
	DECLARE @bBalToZero bit;
	DECLARE @tempStr varchar(100); -- need to check this variable

	DECLARE @paidBalance decimal(20,2);
	DECLARE @v_prevValues AS Table(id decimal(20,2));
	DECLARE @v_runningSum AS TABLE (id decimal(20,2));

	DECLARE @incurredDelta decimal(20,2);
	DECLARE @balanceDelta decimal(20,2);
	DECLARE @bIgnoreRow bit= 0;
	DECLARE @BALANCE INT; 
	DECLARE @CLAIM_ID INT=0;
	
	-- CArrier Claims Settings
	DECLARE @m_str_parm_value as VARCHAR(50);
	--DECLARE @sRCRowId as VARCHAR(50);
	--DECLARE @sRCRowIdJoin as VARCHAR(50);
	--DECLARE @sPolCovLossId as VARCHAR(50);
	--DECLARE @sInsertRCRowId as VARCHAR(max);
	--DECLARE @sInsertPolCovLossId as VARCHAR(max);
	DECLARE @RC_ROW_ID as INT;
	DECLARE @polcvg_loss_row_id AS INT;
	DECLARE @lRC_ROW_ID as INT;
	DECLARE @lpolcvg_loss_row_id AS INT;
	DECLARE @bIsRecoveryBucket AS INT;
	
	DECLARE @bZeroBasedFinHist AS VARCHAR(100);
	DECLARE @szZeroBasedCrit AS VARCHAR(100);
	Declare @bFullBuild     AS VARCHAR(100);
	DECLARE @temp AS decimal(20,2);;
	
	-- Recreate Financial History 
	IF @p_mode='1' OR @p_mode='3' OR @p_mode='5' 
	BEGIN
		UPDATE GLOSSARY SET DTTM_LAST_UPDATE= '00000000',  NEXT_UNIQUE_ID =  1 WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST'
		SET @m_bFullBuild=1;
	END
	ELSE
	BEGIN
		SET @m_bFullBuild=0;
	END
	
	-- Create Zero-Based Financial History   
	IF @p_mode='2' OR @p_mode='3' OR @p_mode='4' OR @p_mode='5'
		SET @m_bZeroBasedFinHist = 1;
	
	IF @p_mode='2' OR @p_mode='3' -- Create Zero-Based Financial History Based On Date Of Claim 
		SET @isEventBased=0;
	ELSE IF @p_mode='4' OR @p_mode='5' -- Create Zero-Based Financial History Based On Date Of Event
		SET @isEventBased=1;
		
	Begin TRY
		EXEC USP_FINANCIAL_HISTORY_QUERY_GLOSSARY 'QUERY',@sTime OUTPUT, @lnextuniqueid OUTPUT, @retval, @v_sqlerrm ; --need to check the input parameters what value we need to pass in it
	END TRY
	BEGIN CATCH
		insert into fin_hist_err_log ( JOB_ID,Obj_name,err_statement,loc,err_msg,create_date) 
		values (@p_jobID,'UpdateFinHistTable Procedure','Calling of QueryGlossary procedure Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
	END CATCH
	
	IF @sTime = null or @sTime='' or @sTime='0'
		SET @sTime = '00000000';

-- Populate SYS_PARAMS_LOB for GC, WC, VA and NONOCC
	DECLARE v_sysParmsLOB_cur CURSOR FOR 
	SELECT RESERVE_TRACKING, LINE_OF_BUS_CODE, ADJ_RSV, COLL_IN_RSV_BAL, BAL_TO_ZERO, 
	NEG_BAL_TO_ZERO, SET_TO_ZERO, EST_NUM_OF_CLAIMS, DURATION_CODE,COLL_IN_INCUR_BAL,PER_RSV_COLL_IN_RSV_BAL,PER_RSV_COLL_IN_INCR_BAL
			FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE IN (241, 242, 243, 844) ORDER BY LINE_OF_BUS_CODE;
	OPEN v_sysParmsLOB_cur;
    
    FETCH NEXT FROM v_sysParmsLOB_cur
      INTO @reserveTracking, @lineOfBusCode, @adjrsv, @collectioninrsv, @baltozero, @negbaltozero, @settozero, @estnumofclaims, @durationcode, @collectioninIncurred,@perrsvcollectionInRsv,@perrsvcollectionInIncurred;
	WHILE @@FETCH_STATUS <> -1
	BEGIN
		IF @lineOfBusCode = 241 
		BEGIN
			SET @GCLOBParmCollectionInRsv = @collectioninrsv;
			SET @GCLOBParmCollectionInIncurred = @collectioninIncurred;
			SET @GCLOBParmPerRsvCollectionInRsv=@perrsvcollectionInRsv;
			SET @GCLOBParmPerRsvCollectionInIncurred=@perrsvcollectionInIncurred;
			SET @GCLOBParmreserveTracking = @reserveTracking;
			SET @GCLOBParmadjRsv = @adjrsv;
			SET @GCLOBParmlineOfBusCode = @lineOfBusCode;
			SET @GCLOBParmbalToZero = @baltozero;
			SET @GCLOBParmnegBalToZero = @negbaltozero;
			SET @GCLOBParmsetToZero = @settozero;
			SET @GCLOBParmestNumofClaims = @estnumofclaims;
			SET @GCLOBParmdurationCode = @durationcode;
		END
        
        IF @lineOfBusCode = 243
        BEGIN
			SET @WCLOBParmcollectionInRsv = @collectioninrsv;
			SET @WCLOBParmCollectionInIncurred = @collectioninIncurred;
			SET @WCLOBParmPerRsvCollectionInRsv=@perrsvcollectionInRsv;
			SET @WCLOBParmPerRsvCollectionInIncurred=@perrsvcollectionInIncurred;
			SET @WCLOBParmreserveTracking = @reserveTracking;
			SET @WCLOBParmadjRsv = @adjrsv;
			SET @WCLOBParmlineOfBusCode = @lineOfBusCode;
			SET @WCLOBParmBalToZero = @baltozero;
			SET @WCLOBParmnegBalToZero = @negbaltozero;
			SET @WCLOBParmsetToZero =  @settozero;
			SET @WCLOBParmestNumofClaims = @estnumofclaims;
			SET @WCLOBParmdurationCode = @durationcode;
		END
		
		IF @lineOfBusCode = 242
		BEGIN
			SET @VALOBParmcollectionInRsv = @collectioninrsv;
			SET @VALOBParmCollectionInIncurred = @collectioninIncurred;
			SET @VALOBParmPerRsvCollectionInRsv=@perrsvcollectionInRsv;
			SET @VALOBParmPerRsvCollectionInIncurred=@perrsvcollectionInIncurred;
			SET @VALOBParmreserveTracking = @reserveTracking;
			SET @VALOBParmadjRsv = @adjrsv;
			SET @VALOBParmlineOfBusCode = @lineOfBusCode;
			SET @VALOBParmBalToZero = @baltozero;
			SET @VALOBParmnegBalToZero = @negbaltozero;
			SET @VALOBParmsetToZero =  @settozero;
			SET @VALOBParmestNumofClaims = @estnumofclaims;
			SET @VALOBParmdurationCode = @durationcode;
		END
		
		IF @lineOfBusCode = 844
		BEGIN
			SET @NONOCCLOBParmcollectionInRsv = @collectioninrsv;
			SET @NONOCCLOBParmCollectionInIncurred = @collectioninIncurred;
			SET @NONOCCLOBParmPerRsvCollectionInRsv=@perrsvcollectionInRsv;
			SET @NONOCCLOBParmPerRsvCollectionInIncurred=@perrsvcollectionInIncurred;
			SET @NONOCCLOBParmreserveTracking = @reserveTracking;
			SET @NONOCCLOBParmadjRsv = @adjrsv;
			SET @NONOCCLOBParmlineOfBusCode = @lineOfBusCode;
			SET @NONOCCLOBParmBalToZero = @baltozero;
			SET @NONOCCLOBParmnegBalToZero = @negbaltozero;
			SET @NONOCCLOBParmsetToZero =  @settozero;
			SET @NONOCCLOBParmestNumofClaims = @estnumofclaims;
			SET @NONOCCLOBParmdurationCode = @durationcode;
		END
		
		FETCH NEXT FROM v_sysParmsLOB_cur
			INTO @reserveTracking, @lineOfBusCode, @adjrsv, @collectioninrsv, @baltozero, @negbaltozero, @settozero, @estnumofclaims, @durationcode, @collectioninIncurred,@perrsvcollectionInRsv,@perrsvcollectionInIncurred;
	END;
	CLOSE v_sysParmsLOB_cur;
	DEALLOCATE v_sysParmsLOB_cur;

-- GET SYS PARAMETERS STARTS
	SET @FIN_HIST_EVAL = (Select FIN_HIST_EVAL from SYS_PARMS);
	SET @ESSP = (Select ESSP from SYS_PARMS);
	
	SET @m_str_parm_value = (SELECT str_parm_value FROM Parms_Name_Value  where parm_name='MULTI_COVG_CLM');
	
	--IF (@m_str_parm_value <> '0')
	--BEGIN
	--	SET @sRCRowId = ', RC_ROW_ID';
	--	SET @sRCRowIdJoin=', RC.RC_ROW_ID'
	--	SET @sInsertRCRowId = ',ISNULL((SELECT DISTINCT RC_ROW_ID from RESERVE_CURRENT where RESERVE_CURRENT.CLAIM_ID = RESERVE_HISTORY.CLAIM_ID AND RESERVE_CURRENT.CLAIMANT_EID = RESERVE_HISTORY.CLAIMANT_EID AND RESERVE_CURRENT.UNIT_ID = RESERVE_HISTORY.UNIT_ID AND RESERVE_CURRENT.RESERVE_TYPE_CODE = RESERVE_HISTORY.RESERVE_TYPE_CODE AND RESERVE_CURRENT.POLCVG_LOSS_ROW_ID = RESERVE_HISTORY.POLCVG_LOSS_ROW_ID),0) RC_ROW_ID';
	--	SET @sPolCovLossId = ', POLCVG_LOSS_ROW_ID';
	--	SET @sInsertPolCovLossId = ',ISNULL((SELECT POLCVG_LOSS_ROW_ID from RESERVE_CURRENT where RESERVE_CURRENT.RC_ROW_ID = FTS.RC_ROW_ID),0) POLCVG_LOSS_ROW_ID';
	--END
	--ELSE
	--BEGIN
	--	SET @sRCRowId = '';
	--	SET @sPolCovLossId = '';
	--	SET @sInsertRCRowId = '';
	--	SET @sInsertPolCovLossId = '';
	--END
	
	--it is negation of value i.e. if fin_hist_eval is false then it is true or vice versa
	IF @FIN_HIST_EVAL=0 
		SET @m_useTranDate =1;
	ELSE 
		SET @m_useTranDate = 0;

	--Assign Client Name from the sys_parms table, add more cases for other clients as required.
	IF @ESSP=1740 
		SET @m_ESSP = 'CRAWFORD';
	ELSE
		SET @m_ESSP = '';

	SET @m_includeVoids = 0;

	Begin TRY
		SET @sSQL = 'SELECT @dbObjects = ISNULL(FH_VOIDS_INCLUDE, 0) FROM SYS_PARMS'
		EXEC sp_ExecuteSQL @sSQL,
		 N'@dbObjects smallint output',
		 @dbObjects = @v_FH_VOIDS_INCLUDE OUTPUT;
		
		IF @v_FH_VOIDS_INCLUDE = 0
			SET  @v_FH_VOIDS_INCLUDE_c=0;
		ELSE
			SET @v_FH_VOIDS_INCLUDE_c=1;
    END TRY
    BEGIN CATCH
		SET @v_check_error=0;
		SET @m_includeVoids = 0;
	END CATCH;
	  
    IF @v_check_error = 1 and @v_FH_VOIDS_INCLUDE_c <> 0
		SET @m_includeVoids = 1;
	
	SET @m_includeOnlyPrintedChecks = 0;
	Begin TRY
		SET @v_check_error = 1;
		SET @v_FH_ONLY_PRINTED = 0;
		SET @sSQL = 'SELECT @dbObjects = ISNULL(FH_ONLY_PRINTED, 0) FROM SYS_PARMS'
		EXEC sp_ExecuteSQL @sSQL,
		 N'@dbObjects smallint output',
		 @dbObjects = @v_FH_ONLY_PRINTED OUTPUT;
    END TRY
    BEGIN CATCH
		SET @m_includeOnlyPrintedChecks = 0;
		SET @v_check_error=0;
	END CATCH;
			
    IF @v_check_error = 1
    BEGIN
		IF (@v_FH_ONLY_PRINTED <> 0)
    		SET @m_includeOnlyPrintedChecks = 1; --'0' is used for false
		ELSE
    	    SET @m_includeOnlyPrintedChecks = 0; --'0' is used for false
    END

	SET @lastdate = @sTime;

	BEGIN TRY
		EXEC USP_GET_CLAIMS_WITH_FUNDS_RES_UPD @p_jobID,@lastdate,@p_claimid,@m_str_parm_value,@v_sqlerrm
	END TRY
	BEGIN CATCH
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
		insert into fin_hist_err_log (job_ID,Obj_name,err_statement,loc,err_msg,create_date) 
		values (@p_jobID,'UpdateFinHistTable Procedure','Calling of GetClaimswithFundsResUPD procedure Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
	END CATCH

	
	BEGIN -- drop temp tables
		BEGIN TRY
			SET @p_sqlerrm=	dbo.FormatMessage('DROP TABLE FH1');
			SET @sSQL = 'DROP TABLE FH1';
			EXEC (@sSQL);
		END TRY
		BEGIN CATCH
			SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
			insert into fin_hist_err_log (job_id, Obj_name,err_statement,loc,err_msg,create_date) 
			values (@p_jobID,'constructFinHistTable Procedure','DROP TABLE FH1 Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
		END CATCH

		BEGIN TRY
			SET @p_sqlerrm=	 @p_sqlerrm + dbo.FormatMessage('DROP TABLE FH2');
			SET @sSQL2 = 'DROP TABLE FH2';
			EXEC (@sSQL2);
		END TRY
		BEGIN CATCH
			SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
			insert into fin_hist_err_log (job_id, Obj_name,err_statement,loc,err_msg,create_date) 
			values (@p_jobID,'constructFinHistTable Procedure','DROP TABLE FH2 Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
		END CATCH

		BEGIN TRY
			SET @p_sqlerrm=	 @p_sqlerrm + dbo.FormatMessage('DROP TABLE FH3');
			SET @sSQL3 = 'DROP TABLE FH3';
			EXEC (@sSQL3);
		END TRY
		BEGIN CATCH
			SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
			insert into fin_hist_err_log (job_id, Obj_name,err_statement,loc,err_msg,create_date) 
			values (@p_jobID,'constructFinHistTable Procedure','DROP TABLE FH3 Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
		END CATCH
	END -- drop temp tables
	
	IF (@lastdate = '00000000') 
	BEGIN
		BEGIN TRY
			SET @sSQL = 'SELECT @dbObjects = count(1) from FINANCIAL_HIST'
			EXEC sp_ExecuteSQL @sSQL,
			 N'@dbObjects int output',
			 @dbObjects = @v_count OUTPUT;
		END TRY
		BEGIN CATCH
			insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
			values (@p_jobID,'constructFinHistTable Procedure','Calling of checkTable procedure from constructFinHistTable got Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
		END CATCH
		
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('DROP FINANCIAL_HIST - START');
		
		IF @v_count >= 0 
		BEGIN
			BEGIN TRY
				DROP TABLE FINANCIAL_HIST;
			END TRY
			BEGIN CATCH
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
				insert into fin_hist_err_log (job_id, Obj_name,err_statement,loc,err_msg,create_date) 
				values (@p_jobID,'constructFinHistTable Procedure','TRUNCATE TABLE FINANCIAL_HIST got Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
			END CATCH
		END
		
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('DROP FINANCIAL_HIST - END, CREATION START');    
		
		BEGIN
			BEGIN TRY
			IF (@m_str_parm_value = '0')
				SET @sSQL = 'CREATE TABLE FINANCIAL_HIST (FH_ROW_ID INT, EVAL_DATE varchar(8) NOT NULL, CLAIM_ID INT NOT NULL, CLAIMANT_EID INT NOT NULL, UNIT_ID INT NOT NULL, RESERVE_TYPE_CODE INT NOT NULL, PAID FLOAT, COLLECTED FLOAT, RESERVE FLOAT, BALANCE FLOAT
				, INCURRED FLOAT )';
			ELSE
				SET @sSQL = 'CREATE TABLE FINANCIAL_HIST (FH_ROW_ID INT, EVAL_DATE varchar(8) NOT NULL, CLAIM_ID INT NOT NULL, PAID FLOAT, COLLECTED FLOAT, RESERVE FLOAT, BALANCE FLOAT, INCURRED FLOAT,RC_ROW_ID INT NOT NULL)';
			EXECUTE (@sSQL);
			
			IF (@m_str_parm_value = '0')
				SET @sSQL = 'ALTER TABLE FINANCIAL_HIST ADD CONSTRAINT PK_FINANCIAL_HIST PRIMARY KEY (EVAL_DATE, CLAIM_ID, CLAIMANT_EID, UNIT_ID, RESERVE_TYPE_CODE)';
			ELSE
				SET @sSQL = 'ALTER TABLE FINANCIAL_HIST ADD CONSTRAINT PK_FINANCIAL_HIST PRIMARY KEY (EVAL_DATE, CLAIM_ID,RC_ROW_ID)';
			EXECUTE (@sSQL);
			
			END TRY
			BEGIN CATCH 
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
				insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
				values (@p_jobID,'constructFinHistTable Procedure','CREATE TABLE FINANCIAL_HIST got Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
			END CATCH;  
		END
	END
	
	SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('DROP FINANCIAL_HIST - END, CREATION END');  
	SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('CREATE FH1 START');  
	
	BEGIN
		BEGIN TRY
			IF (@m_str_parm_value = '0')
				SET @sSQL = 'CREATE TABLE FH1 (EVAL_DATE varchar(8), CLAIM_ID INT, CLAIMANT_EID INT, UNIT_ID INT
				,RESERVE_TYPE_CODE INT, LOB INT, PAID FLOAT, COLLECTED FLOAT, BALANCE FLOAT, RESERVE FLOAT, INCURRED FLOAT) ';
			ELSE
				SET @sSQL = 'CREATE TABLE FH1 (EVAL_DATE varchar(8), CLAIM_ID INT,
				 LOB INT, PAID FLOAT, COLLECTED FLOAT, BALANCE FLOAT, RESERVE FLOAT, INCURRED FLOAT, RC_ROW_ID INT,
				 ) ';
			EXECUTE (@sSQL);
			SET @p_retval='TRUE';
		END TRY
		BEGIN CATCH
			SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
			
			insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
			values (@p_jobID,'InsertDataFromFunds Procedure','Create Table FH1',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
		End CATCH;
		-- FH1 Ends           
		
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('CREATE FH1 END');    
        
		BEGIN --INSERT INTO FH1 STARTS
			SET @sTemp = ',TEMP_FINHIST';
			SET @sTemp2 = ' AND TEMP_FINHIST.CLAIM_ID = F.CLAIM_ID ';
			
			BEGIN --GET PAYMENTS STARTS
				SET @sSQL = 'INSERT INTO FH1 SELECT ';
				
				IF @m_useTranDate = 1
					SET @sSQL = @sSQL + ' F.TRANS_DATE ';
				ELSE
					SET @sSQL = @sSQL + ' F.DATE_OF_CHECK ';
              
--financial key will be selected from reserve current table for carrier claims
              
              IF (@m_str_parm_value <> '0')
						BEGIN
					SET @sSQL = @sSQL + ' EVAL_DATE  ,F.CLAIM_ID
										, 0 LOB, SUM(FTS.AMOUNT) PAID, 0.0 COLLECTED,
										0.0 BALANCE, 0.0 RESERVE, 0.0 INCURRED ,FTS.RC_ROW_ID
										FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' + @sTemp + ' 
										WHERE F.TRANS_ID = FTS.TRANS_ID ' + @sTemp2 ;
										
							
						END
						ELSE
						BEGIN
							SET @sSQL = @sSQL + ' EVAL_DATE, F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, 
										FTS.RESERVE_TYPE_CODE, 0 LOB, SUM(FTS.AMOUNT) PAID, 0.0 COLLECTED,
										0.0 BALANCE, 0.0 RESERVE, 0.0 INCURRED 
										FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' + @sTemp + ' 
										WHERE F.TRANS_ID = FTS.TRANS_ID ' + @sTemp2;
						END
              
              
              
              
				

		   		IF @m_includeVoids = 0
					SET @sSQL = @sSQL + ' AND F.VOID_FLAG = 0';

				-- Option added to allow for only printed checks to be included in FH
				IF (@m_includeOnlyPrintedChecks = 1)
					SET @sSQL = @sSQL+' AND F.STATUS_CODE = 1054';    
				
				SET @sSQL = @sSQL + ' AND F.PAYMENT_FLAG <> 0 GROUP BY ';

				IF @m_useTranDate = 1 
					SET @sSQL = @sSQL + ' F.TRANS_DATE ';
				ELSE 
					SET @sSQL = @sSQL + ' F.DATE_OF_CHECK ';
--financial key will be selected from reserve current table for carrier claims

   IF (@m_str_parm_value <> '0')
   BEGIN
				SET @sSQL = @sSQL + ',F.CLAIM_ID,FTS.RC_ROW_ID';
				END
ELSE
BEGIN
				SET @sSQL = @sSQL + ', F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE ' ;
	END			
				Begin TRY
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 GET PAYMENTS START');  
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(@sSQL);
					EXECUTE (@sSQL);
					SET @p_retval='TRUE';
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 GET PAYMENTS END');  	
				END TRY
				BEGIN CATCH
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
					insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
					values (@p_jobID,'InsertDataFromFunds Procedure','INSERT INTO FH1 - Get payments transactions:'+@sSQL,ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
				End CATCH;
			END --GET PAYMENTS ENDS

			BEGIN -- GET COLLECTION STARTS
				SET @sSQL = 'INSERT INTO FH1 SELECT ';

				IF @m_useTranDate = 1 
					SET @sSQL = @sSQL + ' F.TRANS_DATE ';
				ELSE 
					SET @sSQL = @sSQL + ' F.DATE_OF_CHECK ';
--financial key will be selected from reserve current table for carrier claims
					
									
									IF (@m_str_parm_value <> '0')
					BEGIN
						SET @sSQL = @sSQL + ' EVAL_DATE, F.CLAIM_ID, 0 LOB, 0.0 PAID, SUM(FTS.AMOUNT) COLLECTED,
								0.0 BALANCE, 0.0 RESERVE, 0.0 INCURRED ,FTS.RC_ROW_ID
								FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' + @sTemp + ' WHERE F.TRANS_ID = FTS.TRANS_ID ' + @sTemp2;
								 
					END
					ELSE
					BEGIN
						SET @sSQL = @sSQL + ' EVAL_DATE, F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, 
								FTS.RESERVE_TYPE_CODE, 0 LOB, 0.0 PAID, SUM(FTS.AMOUNT) COLLECTED,
								0.0 BALANCE, 0.0 RESERVE, 0.0 INCURRED 
								FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' + @sTemp + ' WHERE F.TRANS_ID = FTS.TRANS_ID ' + @sTemp2;
					END
					
				if (@m_includeVoids = 0)
					SET @sSQL = @sSQL + ' AND F.VOID_FLAG = 0';    
				
				SET @sSQL = @sSQL + ' AND F.PAYMENT_FLAG = 0 GROUP BY ';
				
				IF @m_useTranDate = 1
					SET @sSQL = @sSQL + ' F.TRANS_DATE ';
				else 
					SET @sSQL = @sSQL + ' F.DATE_OF_CHECK ';
--financial key will be selected from reserve current table for carrier claims

		IF (@m_str_parm_value <> '0')
				SET @sSQL = @sSQL + ',F.CLAIM_ID, FTS.RC_ROW_ID';
ELSE
			SET @sSQL = @sSQL + ', F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE ' ;
			
			print @sSQL;
				 SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(@sSQL);	
				
				Begin TRY
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 GET COLLECTION START');  
					EXECUTE (@sSQL);
					SET @p_retval='TRUE';
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 GET COLLECTION END');  	
				END TRY
				BEGIN CATCH
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
					insert into fin_hist_err_log (job_id, Obj_name,err_statement,loc,err_msg,create_date) 
					values (@p_jobID,'InsertDataFromFunds Procedure','INSERT INTO FH1 - 2 - Get collection transactions:'+@sSQL,ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
				End CATCH
			END -- GET COLLECTION END
		
			IF (@m_includeVoids <> 0) 
			BEGIN -- ... handle voided payments STARTS
--financial key will be selected from reserve current table for carrier claims
				
				IF (@m_str_parm_value <> '0')
	BEGIN
	SET @sSQL = 'INSERT INTO FH1 SELECT F.VOID_DATE EVAL_DATE,F.CLAIM_ID, 0 LOB,(-1.0 * SUM(FTS.AMOUNT)) PAID, 0.0 COLLECTED,
				0.0 BALANCE, 0.0  RESERVE,0.0 INCURRED ,FTS.RC_ROW_ID
				FROM FUNDS F, FUNDS_TRANS_SPLIT FTS' + @sTemp + ' 
				WHERE F.TRANS_ID = FTS.TRANS_ID '+ @sTemp2 + ' AND F.VOID_FLAG <> 0 ';
				
				
	END
	ELSE
	BEGIN
	SET @sSQL = 'INSERT INTO FH1 SELECT F.VOID_DATE EVAL_DATE,F.CLAIM_ID,F.CLAIMANT_EID,F.UNIT_ID,
				FTS.RESERVE_TYPE_CODE, 0 LOB,(-1.0 * SUM(FTS.AMOUNT)) PAID, 0.0 COLLECTED,
				0.0 BALANCE, 0.0  RESERVE,0.0 INCURRED 
				FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' + @sTemp + ' 
				WHERE F.TRANS_ID = FTS.TRANS_ID '+ @sTemp2 + ' AND F.VOID_FLAG <> 0';
	END
				

				if @m_includeOnlyPrintedChecks = 1
					SET @sSQL = @sSQL + ' AND F.STATUS_CODE = 1054 ';
				
				SET @sSQL = @sSQL + ' AND F.PAYMENT_FLAG <> 0 GROUP BY ';
--financial key will be selected from reserve current table for carrier claims

	IF (@m_str_parm_value <> '0')
				SET @sSQL = @sSQL + ' F.VOID_DATE,F.CLAIM_ID,FTS.RC_ROW_ID';
ELSE
				SET @sSQL = @sSQL + ' F.VOID_DATE, F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE ' ;
				
				
				
				print @sSQL;
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(@sSQL);	
				Begin  TRY 
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 HANDLE VOIDED PAYMENT START');  	 
					EXECUTE (@sSQL);
					SET @p_retval='TRUE';
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 HANDLE VOIDED PAYMENT END');  	 	
				END TRY
				BEGIN CATCH
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
					insert into fin_hist_err_log (job_id, Obj_name,err_statement,loc,err_msg,create_date) 
					values (@p_jobID,'InsertDataFromFunds Procedure','INSERT INTO FH1 - 3 - Handle voided payments......'+ @sSQL,ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
				End CATCH
				
				-- Handle voided collections
--financial key will be selected from reserve current table for carrier claims
				
				IF (@m_str_parm_value <> '0')
	BEGIN
	SET @sSQL = 'INSERT INTO FH1 SELECT F.VOID_DATE EVAL_DATE,F.CLAIM_ID, 0 LOB, 0.0 PAID,(-1.0 * SUM(FTS.AMOUNT)) COLLECTED,
				0.0 BALANCE, 0.0 RESERVE, 0.0 INCURRED ,FTS.RC_ROW_ID
				FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' + @sTemp + ' 
				WHERE F.TRANS_ID = FTS.TRANS_ID ' + @sTemp2 + ' AND F.VOID_FLAG <> 0 ' ;
	END
	ELSE
	BEGIN
	SET @sSQL = 'INSERT INTO FH1 SELECT F.VOID_DATE EVAL_DATE, F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID,
				FTS.RESERVE_TYPE_CODE, 0 LOB, 0.0 PAID,(-1.0 * SUM(FTS.AMOUNT)) COLLECTED,
				0.0 BALANCE, 0.0 RESERVE, 0.0 INCURRED 
				FROM FUNDS F, FUNDS_TRANS_SPLIT FTS ' + @sTemp + ' 
				WHERE F.TRANS_ID = FTS.TRANS_ID ' + @sTemp2 + ' AND F.VOID_FLAG <> 0 ';
	END
				
				

				SET @sSQL = @sSQL+' AND F.PAYMENT_FLAG = 0 GROUP BY ';
--financial key will be selected from reserve current table for carrier claims

IF (@m_str_parm_value <> '0')
				SET @sSQL = @sSQL + 'F.VOID_DATE,F.CLAIM_ID, FTS.RC_ROW_ID';
ELSE
	SET @sSQL = @sSQL + 'F.VOID_DATE, F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE' ;
				Begin TRY    
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 HANDLE VOIDED COLLECTION START');  	 	
					EXECUTE (@sSQL);
					SET @p_retval='TRUE';
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 HANDLE VOIDED COLLECTION END');  	 		
				END TRY
				BEGIN CATCH
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
					insert into fin_hist_err_log (job_id, Obj_name,err_statement,loc,err_msg,create_date) 
					values (@p_jobID,'InsertDataFromFunds Procedure','INSERT INTO FH1 - 4 - Handle voided collections......'+@sSQL,ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
				End CATCH
			END -- Handle voided payments END

-- Process Stop Pay transactions as negative transactions
			BEGIN TRY 
				SET @sTemp = ',TEMP_FINHIST WHERE TEMP_FINHIST.CLAIM_ID = RESERVE_HISTORY.CLAIM_ID';
				IF (@m_str_parm_value <> '0')
				BEGIN
				select 'block 6'
				SET @sSQL = 'INSERT INTO FH1 SELECT RESERVE_HISTORY.DATE_ENTERED EVAL_DATE,RC.CLAIM_ID,0 LOB,0.0 PAID,0.0 COLLECTED,0.0 BALANCE,
				 SUM(RESERVE_HISTORY.CHANGE_AMOUNT) RESERVE,0.0 INCURRED ,RC.RC_ROW_ID FROM RESERVE_CURRENT RC,RESERVE_HISTORY'+ @sTemp +'
				 AND RC.CLAIM_ID = RESERVE_HISTORY.CLAIM_ID AND RC.CLAIMANT_EID = RESERVE_HISTORY.CLAIMANT_EID 
				 AND RC.UNIT_ID = RESERVE_HISTORY.UNIT_ID AND RC.RESERVE_TYPE_CODE = RESERVE_HISTORY.RESERVE_TYPE_CODE
				  AND RC.POLCVG_LOSS_ROW_ID = RESERVE_HISTORY.POLCVG_LOSS_ROW_ID
				   
				  GROUP BY  RESERVE_HISTORY.DATE_ENTERED,RC.RC_ROW_ID,RC.CLAIM_ID';
				 
				 print @sSQL;
				END
				ELSE
				BEGIN
				select 'block 7';
				
				SET @sSQL = 'INSERT INTO FH1 SELECT DATE_ENTERED EVAL_DATE,RESERVE_HISTORY.CLAIM_ID,CLAIMANT_EID,
				UNIT_ID,RESERVE_TYPE_CODE,0 LOB,0.0 PAID,0.0 COLLECTED,0.0 BALANCE,
				 SUM(CHANGE_AMOUNT) RESERVE,0.0 INCURRED  FROM RESERVE_HISTORY '+ @sTemp + 
				 ' GROUP BY  DATE_ENTERED,RESERVE_HISTORY.CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE';
				END
				select @sSQL;
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(@sSQL);	
				
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 START');  	 	
				EXECUTE (@sSQL);
				SET @p_retval= 'TRUE'; 
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH1 END');  	 
			END TRY
			BEGIN CATCH
			SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
				INSERT INTO FIN_HIST_ERR_LOG (job_id, Obj_name,err_statement,loc,err_msg,create_date) 
				VALUES (@p_jobID,'INSERT FROM RESERVE_HISTORY TABLER', @sSQL, ERROR_LINE(), ERROR_MESSAGE(), SYSDATETIME());
			END CATCH
	END -- INSERT INTO FH1 ENDS
    
-- MERGE
 --- Code start here for MergeFHData
	BEGIN TRY
		SET @sSQL = 'CREATE INDEX FH1_CL_IDX ON FH1(CLAIM_ID)';
		EXECUTE (@sSQL);
		SET @p_retval='TRUE';
	END TRY
	BEGIN CATCH
		insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
		values (@p_jobID,'MergeFHData Procedure','CREATE INDEX FH1_CL_IDX',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
	end CATCH;  

	BEGIN TRY
	IF (@m_str_parm_value <> '0')
				BEGIN
				SET @sSQL = 'CREATE INDEX FH1_IDX ON FH1(EVAL_DATE,CLAIM_ID,RC_ROW_ID)';
				END
				ELSE
				BEGIN
				
				SET @sSQL = 'CREATE INDEX FH1_IDX ON FH1(EVAL_DATE,CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE)';
				END
				
		
		EXECUTE (@sSQL);
		SET @p_retval='TRUE';
	END TRY
	BEGIN CATCH
	SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
		insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
		values (@p_jobID,'MergeFHData Procedure','CREATE INDEX FH1_IDX',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
	END CATCH

   -- Merge entries together
   IF (@m_str_parm_value <> '0')
				BEGIN
				select 'Block 4';
	SET @sSQL = 'SELECT EVAL_DATE,FH1.CLAIM_ID,CLAIM.LINE_OF_BUS_CODE  LOB,SUM(FH1.PAID) PAID,';
	SET @sSQL = @sSQL + ' SUM(FH1.COLLECTED) COLLECTED,SUM(FH1.BALANCE) BALANCE,SUM(FH1.RESERVE) RESERVE,SUM(FH1.INCURRED) INCURRED,FH1.RC_ROW_ID ';
	
	SET @sSQL = @sSQL + ' INTO FH3 FROM FH1,CLAIM WHERE FH1.CLAIM_ID = CLAIM.CLAIM_ID AND FH1.CLAIM_ID <> 0 
	GROUP BY EVAL_DATE,FH1.CLAIM_ID,CLAIM.LINE_OF_BUS_CODE,FH1.RC_ROW_ID ';
	
				END
				ELSE
				BEGIN
				select 'Block 5';
				
	SET @sSQL = 'SELECT EVAL_DATE,FH1.CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,CLAIM.LINE_OF_BUS_CODE  LOB,SUM(FH1.PAID) PAID,';
	SET @sSQL = @sSQL + ' SUM(FH1.COLLECTED) COLLECTED,SUM(FH1.BALANCE) BALANCE,SUM(FH1.RESERVE) RESERVE,SUM(FH1.INCURRED) INCURRED ';
	
	SET @sSQL = @sSQL + ' INTO FH3 FROM FH1,CLAIM WHERE FH1.CLAIM_ID = CLAIM.CLAIM_ID AND FH1.CLAIM_ID <> 0 
	GROUP BY EVAL_DATE,FH1.CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,CLAIM.LINE_OF_BUS_CODE ';
	
	
				END
   
   
   
   
	begin TRY
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('CREATE FH3 START');  	 
		EXECUTE (@sSQL);
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('CREATE FH3 END');  	 
		SET @p_retval='TRUE';
		SET @retval=1;
    end try
    begin catch
		SET @p_retval='FALSE';
		SET @retval=0;
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
		insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
		values (@p_jobID,'MergeFHData Procedure','CREATE TABLE FH3',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
	end catch 
	

	if (@retVal <> 0)  -- IF FH3 Created Successfully then create FH2
	BEGIN
	IF (@m_str_parm_value <> '0')
				BEGIN
				SET @sSQL = 'SELECT EVAL_DATE,CLAIM_ID,
		LOB,PAID,COLLECTED,BALANCE,RESERVE,INCURRED,RC_ROW_ID  INTO FH2 FROM FH3';
				END
				ELSE
				BEGIN
				
				SET @sSQL = 'SELECT EVAL_DATE,CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE, 
		LOB,PAID,COLLECTED,BALANCE,RESERVE,INCURRED  INTO FH2 FROM FH3';
				END
		
	END
	BEGIN try
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('CREATE FH2 START');  	 
		EXECUTE (@sSQL);
		SET @p_retval='TRUE';
		SET @retval=1;
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('CREATE FH2 END');  	 
	END TRY
    BEGIN CATCH
		SET @p_retval='FALSE';
		SET @retval=0;
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
	
		insert into fin_hist_err_log (job_id,Obj_name,err_statement,loc,err_msg,create_date) 
        values (@p_jobID,'MergeFHData Procedure','Create TABLE FH2',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
    END CATCH; 
    
	IF @retVal = 1 AND @m_bZeroBasedFinHist = 1
	BEGIN --Process Zero based only if flag is set
		IF @isEventBased = 0
		BEGIN  --Claim based
		
		IF (@m_str_parm_value <> '0')
				BEGIN
				
		
		
		SET @sSQL = 'INSERT INTO FH2 SELECT CLAIM.DATE_OF_CLAIM, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0';
				END
				ELSE
				BEGIN
				
				SET @sSQL = 'INSERT INTO FH2 SELECT CLAIM.DATE_OF_CLAIM, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0.00,0.00,0.00'
				END
		
			
				
			--IF (@m_str_parm_value <> '0')
			--	SET @sSQL =  @sSQL+ ',0,0'; -- Kapil
			
			IF @m_bFullBuild = 1 
			BEGIN
				SET @sSQL = @sSQL+ ' FROM CLAIM WHERE CLAIM.CLAIM_ID <> 0 AND CLAIM.DATE_OF_CLAIM < ';
				SET @sSQL = @sSQL + ' (SELECT MIN(EVAL_DATE) FROM FH3 WHERE FH3.CLAIM_ID=CLAIM.CLAIM_ID)';
			END
			ELSE
			BEGIN
				SET @sSQL = @sSQL+ ' FROM CLAIM WHERE CLAIM.CLAIM_ID <> 0 AND CLAIM.DATE_OF_CLAIM < ';
				SET @sSQL = @sSQL + ' (SELECT MIN(EVAL_DATE) FROM FINANCIAL_HIST WHERE FINANCIAL_HIST.CLAIM_ID=CLAIM.CLAIM_ID)';
			END
			 
			BEGIN TRY
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH2 START');
				EXECUTE (@sSQL);
				SET @p_retval='TRUE';
				SET @retval=1;
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH2 END'); 
			END try
			begin catch
				SET @p_retval='FALSE';
				SET @retval=0;
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
				insert into fin_hist_err_log ( Obj_name,err_statement,loc,err_msg,create_date) 
				values ('MergeFHData Procedure','INSERT INTO FH2 - 1 ',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
			end catch 
      
			if @retVal = 1
			BEGIN  --Claims that does not have record in FH3, enter a record in FH2 for them
				begin try -- kapil added 0,0 for rcrowdid and polcvg
				IF (@m_str_parm_value <> '0')
				BEGIN
		
		SET @sSQL = 'INSERT INTO FH2 SELECT CLAIM.DATE_OF_CLAIM, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0';
				END
				ELSE
				BEGIN
				
				SET @sSQL = 'INSERT INTO FH2 SELECT CLAIM.DATE_OF_CLAIM, CLAIM.CLAIM_ID,
					  0,0,0,0,0.00,0.00,0.00,0.00,0.00'
				END
				
				
					
					
					--IF (@m_str_parm_value <> '0')
					--	SET @sSQL = @sSQL + ',0,0 '
					 
					IF @m_bFullBuild = 1 BEGIN
						SET @sSQL = @sSQL + ' FROM CLAIM LEFT JOIN FH3 ON CLAIM.CLAIM_ID=FH3.CLAIM_ID
						WHERE CLAIM.CLAIM_ID<>0  AND FH3.CLAIM_ID IS NULL';
					END
					ELSE
					BEGIN
					SET @sSQL = @sSQL + ' FROM CLAIM LEFT JOIN FINANCIAL_HIST ON CLAIM.CLAIM_ID=FINANCIAL_HIST.CLAIM_ID
						WHERE CLAIM.CLAIM_ID<>0  AND FINANCIAL_HIST.CLAIM_ID IS NULL';
					END
					
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH2 CLAIM ZEROBASED START');	
					EXECUTE (@sSQL);
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH2 CLAIM ZEROBASED END');	
					SET @p_retval='TRUE';
					SET @retval=1;
				END TRY
				BEGIN CATCH
					SET @p_retval='FALSE';
					SET @retval=0;
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
					insert into fin_hist_err_log ( Obj_name,err_statement,loc,err_msg,create_date) 
					values ('MergeFHData Procedure','INSERT INTO FH2 - 2 ',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
				END CATCH
			END
		END
		ELSE --Event based
		BEGIN
		--//Check if any claim does not have record in FH3 on it's date_of_event. If so, enter a record in FH2
		IF (@m_str_parm_value <> '0')
				BEGIN
				SET @sSQL = 'INSERT INTO FH2 SELECT EVENT.DATE_OF_EVENT, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0';
				
				END
				ELSE
				BEGIN
				
					SET @sSQL = 'INSERT INTO FH2 SELECT EVENT.DATE_OF_EVENT, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0.00,0.00,0.00';
				END
				
		
				
			--IF (@m_str_parm_value <> '0')
			--	SET @sSQL = @sSQL + ',0,0 ';
				
			IF @m_bFullBuild = 1 
			BEGIN
				SET @sSQL = @sSQL + 'FROM CLAIM, EVENT WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID 
				AND CLAIM.CLAIM_ID <> 0 
				AND EVENT.DATE_OF_EVENT < (SELECT MIN(EVAL_DATE) FROM FH3 WHERE FH3.CLAIM_ID=CLAIM.CLAIM_ID)';
			END
			ELSE
			BEGIN
				SET @sSQL = @sSQL + 'FROM CLAIM, EVENT WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID 
				AND CLAIM.CLAIM_ID <> 0 
				AND EVENT.DATE_OF_EVENT < (SELECT MIN(EVAL_DATE) FROM FINANCIAL_HIST WHERE FINANCIAL_HIST.CLAIM_ID=CLAIM.CLAIM_ID)'; -- kapil added 0,0 for rcrowdid and polcvg
			END
			
			BEGIN TRY
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH2 EVENT START');
				EXECUTE (@sSQL);
				SET @p_retval='TRUE';
				SET @retval=1;
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH2 EVENT END');
			END TRY
			BEGIN CATCH
				SET @p_retval='FALSE';
				SET @retVal=0;
				SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
				insert into fin_hist_err_log ( Obj_name,err_statement,loc,err_msg,create_date) 
				 values ('MergeFHData Procedure','INSERT INTO FH2 - 3',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
			END CATCH

			IF @retVal =1
			BEGIN
			 --//Claims that does not have record in FH3, enter a record in FH2 for them
				BEGIN TRY
				IF (@m_str_parm_value <> '0')
				BEGIN
				 SET @sSQL = 'INSERT INTO FH2 SELECT EVENT.DATE_OF_EVENT, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0';
				 
				
				END
				ELSE
				BEGIN
				 SET @sSQL = 'INSERT INTO FH2 
				   SELECT EVENT.DATE_OF_EVENT, CLAIM.CLAIM_ID, 0,0,0,0,0.00,0.00,0.00,0.00,0.00'
				
				END
				
				  
				   
					--IF (@m_str_parm_value <> '0')
					--	SET @sSQL = @sSQL + ',0,0';
				   
					IF @m_bFullBuild = 1
					BEGIN
						SET @sSQL = @sSQL + 'FROM CLAIM JOIN EVENT ON CLAIM.EVENT_ID=EVENT.EVENT_ID
						LEFT JOIN FH3
						ON  CLAIM.CLAIM_ID=FH3.CLAIM_ID 
						WHERE CLAIM.CLAIM_ID<>0 
						AND FH3.CLAIM_ID IS NULL';
					END
					ELSE
					BEGIN
						SET @sSQL = @sSQL + 'FROM CLAIM JOIN EVENT ON CLAIM.EVENT_ID=EVENT.EVENT_ID
						LEFT JOIN FINANCIAL_HIST
						ON  CLAIM.CLAIM_ID=FINANCIAL_HIST.CLAIM_ID 
						WHERE CLAIM.CLAIM_ID<>0 
						AND FINANCIAL_HIST.CLAIM_ID IS NULL';
					END
					
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH2 EVENT ZEROBASED START');
				
					EXECUTE (@sSQL);
					SET @p_retval='TRUE';
					SET @retval=1;
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('INSERT INTO FH2 EVENT ZEROBASED END');
				END TRY
				BEGIN CATCH
					SET @p_retval='FALSE';
					SET @retval=0;
					SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
					insert into fin_hist_err_log ( Obj_name,err_statement,loc,err_msg,create_date) 
					values ('MergeFHData Procedure','INSERT INTO FH2 - 4',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
				END CATCH
			END
		END -- EVENT BASED END
	END --Process Zero based only if flag is set END
-- UPDATE FINHIST
--Code start here for updatefinhist
	SET @totalCount = @v_count;
	SET @FH_ROW_ID = @lnextuniqueid; -- need to check from where lNextUniqueID value is coming
	begin TRY
	IF (@m_str_parm_value <> '0')
		SET @sSQL = 'CREATE INDEX FH2_IDX ON FH2(CLAIM_ID,RC_ROW_ID,EVAL_DATE) ';
	ELSE
		SET @sSQL = 'CREATE INDEX FH2_IDX ON FH2(CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,EVAL_DATE) ';
	EXECUTE (@sSQL);
    END TRY
    BEGIN CATCH
		insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
		values (@p_jobID,'UpdateFinHist Procedure','Index FH@_IDX failed........',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
	end CATCH;
	SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('CURSOR FH2 + INSERT INTO FINANCIAL_HIST START');
	if(@FH_ROW_ID IS NUll OR @FH_ROW_ID = 0)
		SET @FH_ROW_ID = 1;
	SET @CLAIM_ID = 0; 
	SET @CLAIMANT_EID = 0; 
	SET @UNIT_ID = 0; 
	SET @RESERVE_TYPE_CODE = 0;
	SET @EVAL_DATE = 0;
	SET @PAID = 0.0; 
	SET @COLLECTED = 0.00; 
	SET @RESERVE = 0.0;
	SET @cEvalDate = 0;
	IF (@m_str_parm_value <> '0')
		SET @sSQL = 'DECLARE c_FH2 CURSOR FOR SELECT CLAIM_ID, EVAL_DATE,PAID,COLLECTED,RESERVE,LOB 
		, RC_ROW_ID FROM FH2 ORDER BY CLAIM_ID,RC_ROW_ID,EVAL_DATE';
	ELSE
		SET @sSQL = 'DECLARE c_FH2 CURSOR FOR SELECT CLAIM_ID, CLAIMANT_EID, UNIT_ID, RESERVE_TYPE_CODE,EVAL_DATE,PAID,COLLECTED,RESERVE,LOB 
		FROM FH2 ORDER BY CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,EVAL_DATE';
		
		exec sp_executesql @sSQL;
		OPEN c_FH2
		IF (@m_str_parm_value <> '0')
			FETCH NEXT FROM c_FH2 INTO @CLAIM_ID,  @EVAL_DATE, @PAID, @COLLECTED, @RESERVE , @LOB, @RC_ROW_ID;
		ELSE
			FETCH NEXT FROM c_FH2 INTO @CLAIM_ID, @CLAIMANT_EID, @UNIT_ID, @RESERVE_TYPE_CODE, @EVAL_DATE, @PAID, @COLLECTED, @RESERVE , @LOB;
		BEGIN TRAN-- CURSOR c_FH2
		WHILE(@@FETCH_STATUS <> -1)
		BEGIN
			SET @bIgnoreRow = 0;
			SET @counter= @counter+1;
			
			IF(@LOB = 241)
			BEGIN
				SET @bCollInRsvBal = @GCLOBParmCollectionInRsv;
				SET @bBalToZero = @GCLOBParmBalToZero;
				SET @bCollInIncurred = @GCLOBParmCollectionInIncurred;
				SET @bPerRsvCollInRsvBal=@GCLOBParmPerRsvCollectionInRsv;
				SET @bPerRsvCollInIncurred=@GCLOBParmPerRsvCollectionInIncurred;
			END

			IF(@LOB = 242)
			BEGIN
				SET @bCollInRsvBal = @VALOBParmCollectionInRsv;
				SET @bBalToZero = @VALOBParmBalToZero;
				SET @bCollInIncurred = @VALOBParmCollectionInIncurred;
				SET @bPerRsvCollInRsvBal=@VALOBParmPerRsvCollectionInRsv;
				SET @bPerRsvCollInIncurred=@VALOBParmPerRsvCollectionInIncurred;
			END

			IF(@LOB = 243)
			BEGIN
				SET @bCollInRsvBal = @WCLOBParmCollectionInRsv;
				SET @bBalToZero = @WCLOBParmBalToZero;
				SET @bCollInIncurred = @WCLOBParmCollectionInIncurred;
				SET @bPerRsvCollInRsvBal=@WCLOBParmPerRsvCollectionInRsv;
				SET @bPerRsvCollInIncurred=@WCLOBParmPerRsvCollectionInIncurred;
			END

			IF(@LOB = 844)
			BEGIN
				SET @bCollInRsvBal = @NONOCCLOBParmCollectionInRsv;
				SET @bBalToZero = @NONOCCLOBParmBalToZero;
				SET @bCollInIncurred = @NONOCCLOBParmCollectionInIncurred;
				SET @bPerRsvCollInRsvBal=@NONOCCLOBParmPerRsvCollectionInRsv;
				SET @bPerRsvCollInIncurred=@NONOCCLOBParmPerRsvCollectionInIncurred;
			END
			
			IF (@m_str_parm_value <> '0')
			BEGIN	
				if (@lClaimID <> @CLAIM_ID  OR 	@lRC_ROW_ID<>@RC_ROW_ID)
				BEGIN
					SET @PreviousPaid = 0.0;
					SET @PreviousCollection = 0.00;
					SET @PreviousReserve = 0.0;
					SET @PreviousIncurred = 0.0;
					SET @PreviousBalance = 0.0;
					SET @CurrentPaid = 0.0;
					SET @CurrentCollection = 0.00;
					SET @CurrentReserve = 0.0;
					SET @CurrentIncurred = 0.0;
					SET @CurrentBalance = 0.0;
				END
			END
			ELSE if (@lClaimID <> @CLAIM_ID or @lClaimantEID <> @CLAIMANT_EID or @lUnitID <> @UNIT_ID or @lReserveTypeCode <> @RESERVE_TYPE_CODE)
			BEGIN
				SET @PreviousPaid = 0.0;
				SET @PreviousCollection = 0.00;
				SET @PreviousReserve = 0.0;
				SET @PreviousIncurred = 0.0;
				SET @PreviousBalance = 0.0;
				SET @CurrentPaid = 0.0;
				SET @CurrentCollection = 0.00;
				SET @CurrentReserve = 0.0;
				SET @CurrentIncurred = 0.0;
				SET @CurrentBalance = 0.0;
			END
			
			
			
			SET @CurrentPaid = @CurrentPaid + @PAID;
			SET @CurrentCollection = @CurrentCollection + @COLLECTED;
			SET @CurrentReserve = @CurrentReserve + @RESERVE;
			
			
			if(@bPerRsvCollInRsvBal<>0)
			BEGIN
				IF NOT EXISTS(SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL WHERE LINE_OF_BUS_CODE=@LOB AND RES_TYPE_CODE=@RESERVE_TYPE_CODE AND COLL_IN_RSV_BAL=-1)
					BEGIN
					    --PRINT 'No matching row exists'
					    SET @bPerRsvCollInRsvBal=0;
					END
			END
			
			if (@bCollInRsvBal <> 0 or @bPerRsvCollInRsvBal <> 0) 
			begin
				SET @paidBalance = @CurrentPaid - @CurrentCollection;
				select '@paidBalance block 1' ;
				select @paidBalance;
				end
			else 
			begin
				SET @paidBalance = @CurrentPaid;
				select '@paidBalance block 2' ;
				select   @paidBalance;
			end
			SET @bIsRecoveryBucket = ISNULL((select CODE_ID from CODES where CODE_ID=@RESERVE_TYPE_CODE and 
									RELATED_CODE_ID  =(select CODE_ID from CODES where TABLE_ID in (
									select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME='MASTER_RESERVE')
									and SHORT_CODE='R')),0);

			IF @bIsRecoveryBucket <> 0
			BEGIN	
				SET @outstandingBalance = @CurrentReserve - @CurrentCollection;
				--akaushik5 Changed for MITS 36271 Starts
				--SET @incurred = -@CurrentCollection;
				IF @outstandingBalance < 0
				BEGIN
					SET @incurred = @CurrentCollection;
				END
				ELSE
				BEGIN
					SET @incurred = @outstandingBalance + @CurrentCollection;
				END
				--akaushik5 Changed for MITS 36271 Ends
			END
			ELSE	
				BEGIN
				if (@bClosed <> 0 and @bBalToZero <> 0)
					SET @outstandingBalance = 0.0;
				else
				BEGIN
					SET @outstandingBalance = @CurrentReserve - @paidBalance;  -- need to check its assignment
					if (@outstandingBalance < 0.0) 
						SET @outstandingBalance = 0.0;
				END  
				
				SET @temp =0.0;
				IF(@bCollInRsvBal <> 0 or @bPerRsvCollInRsvBal <> 0)
				BEGIN
					SET @temp = @CurrentPaid - @CurrentCollection;

					--IF(@temp < 0.0)
					--BEGIN
					--	SET @temp = 0.0;
					--END

					IF(@outstandingBalance < 0.0)
					BEGIN
						SET @incurred = @temp;
					END
					ELSE
					BEGIN
						SET @incurred = @outstandingBalance + @temp;
					END
				END
				ELSE 
				BEGIN
					IF(@outstandingBalance < 0.0)
					BEGIN
						SET @incurred = @CurrentPaid;
					END
					ELSE
					BEGIN
						SET @incurred = @outstandingBalance + @CurrentPaid;
					END
				END
				
				
				if(@bPerRsvCollInIncurred<>0)
					BEGIN
						IF NOT EXISTS(SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL WHERE LINE_OF_BUS_CODE=@LOB AND RES_TYPE_CODE=@RESERVE_TYPE_CODE AND COLL_IN_RSV_BAL=0)
							BEGIN
								--PRINT 'No matching row exists'
								SET @bPerRsvCollInIncurred=0;
							END
					END
				IF(@bCollInIncurred <> 0 or @bPerRsvCollInIncurred <> 0)
				BEGIN
					SET @incurred = @incurred - @CurrentCollection;
				END

				--akaushik5 Commented for MITS 37788 Starts
				--IF(@incurred < 0.0)
				--BEGIN
				--	SET @incurred = 0.0;
				--END
				--akaushik5 Commented for MITS 37788 Ends
			END
			
			SET @CurrentBalance = @outstandingBalance;
			SET @CurrentIncurred = @incurred;
			
			-- // find incurred
			--// find differentials
			SET @incurredDelta = @CurrentIncurred - @PreviousIncurred;

			if (abs(@incurredDelta) < 0.009)
				SET @incurredDelta = 0.0;

			SET @balanceDelta = @CurrentBalance - @PreviousBalance;

			if (abs(@balanceDelta) < 0.009)
				SET @balanceDelta = 0.0;
								
			if (@bIgnoreRow = 0) 
			BEGIN
				Begin TRY
					IF (@m_str_parm_value <> '0')
						EXEC(' INSERT INTO FINANCIAL_HIST(CLAIM_ID,EVAL_DATE, PAID,COLLECTED,RESERVE,INCURRED,BALANCE,FH_ROW_ID, RC_ROW_ID)
								VALUES( ' + @claim_id+ ',' + @eval_date + ','+ @paid + ','+ @collected + ','+ @reserve + ','+ @incurredDelta + ','+ @balanceDelta + ','+ @fh_row_id + ','+ @RC_ROW_ID + ')' )  
					ELSE
						EXEC(' INSERT INTO FINANCIAL_HIST(CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,EVAL_DATE, PAID,COLLECTED,RESERVE,INCURRED,BALANCE,FH_ROW_ID)
								VALUES( ' + @claim_id+ ',' + @claimant_eid + ',' + @unit_id +',' + @reserve_type_code + ','+ @eval_date + ','+ @paid + ','+ @collected + ','+ @reserve + ','+ @incurredDelta + ','+ @balanceDelta + ','+ @fh_row_id + ')' )
				END TRY
				BEGIN CATCH
					SET @err = 1;
					insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
					values (@p_jobID,'UpdateFinHist Procedure','Insert into Financial_hist table failed........',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
				END CATCH;    

				if @retVal = 1 
					SET @p_retval= 'TRUE';
				else 
				BEGIN
					SET @p_retval='FALSE';
					return;
				END

				SET @PreviousPaid = @CurrentPaid;
				SET @PreviousCollection = @CurrentCollection;
				SET @PreviousReserve = @CurrentReserve;
				SET @PreviousIncurred = @CurrentIncurred;
				SET @PreviousBalance = @CurrentBalance;

			--// Get next record
				SET @lClaimID = @CLAIM_ID;
				SET @lClaimantEID = @CLAIMANT_EID;
				SET @lUnitID = @UNIT_ID;
				SET @lReserveTypeCode = @RESERVE_TYPE_CODE;
				SET @cEvalDate= @EVAL_DATE;
				SET @FH_ROW_ID= @FH_ROW_ID + 1;
				
					
				IF(@m_str_parm_value <>0)
				BEGIN
					SET @lRC_ROW_ID=@RC_ROW_ID;
				END
			end

			SET @CLAIM_ID = 0;
			SET @CLAIMANT_EID = 0; 
			SET @UNIT_ID = 0; 
			SET @RESERVE_TYPE_CODE = 0;
			SET @EVAL_DATE = 0; 
			SET @PAID = 0.0; 
			SET @COLLECTED = 0.00; 
			SET @RESERVE = 0.0;
			-- akaushik5 Fixed Performance Issue Starts
				IF @fh_row_id % 5000 = 0
				BEGIN
					COMMIT TRAN
					BEGIN TRAN
				END				
			-- akaushik5 Fixed Performance Issue Ends	
			IF (@m_str_parm_value <> '0')
				FETCH NEXT FROM c_FH2 INTO @CLAIM_ID, @EVAL_DATE, @PAID, @COLLECTED, @RESERVE , @LOB, @RC_ROW_ID;
			ELSE
				FETCH NEXT FROM c_FH2 INTO @CLAIM_ID, @CLAIMANT_EID, @UNIT_ID, @RESERVE_TYPE_CODE, @EVAL_DATE, @PAID, @COLLECTED, @RESERVE , @LOB;
		END
		CLOSE c_FH2;
		DEALLOCATE  c_FH2;
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('CURSOR FH2 + INSERT INTO FINANCIAL_HIST END');
		COMMIT TRAN
	SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('UPDATE GLOSSARY START');
	IF @p_claimid IS NULL
	BEGIN
		BEGIN TRY
			UPDATE GLOSSARY SET NEXT_UNIQUE_ID = @FH_ROW_ID, DTTM_LAST_UPDATE = replace(replace(replace(convert(varchar(19), getdate(), 126),'-',''),'T',''),':','')
			WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST';
		END TRY
		BEGIN CATCH
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
			insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
			values (@p_jobID,'CreateFinHistFromScratch Procedure','Calling of QueryGlossary procedure Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
		END CATCH;
	END
	ELSE IF @p_claimid IS NOT NULL
		BEGIN
				BEGIN TRY
			UPDATE GLOSSARY SET NEXT_UNIQUE_ID = @FH_ROW_ID
			WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST';
		END TRY
		BEGIN CATCH
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
		
			insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
			values (@p_jobID,'CreateFinHistFromScratch Procedure','Calling of QueryGlossary procedure Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
		END CATCH;
		END
	SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('UPDATE GLOSSARY END');	
	SET @p_retval='TRUE';
      Begin TRY
			IF((SELECT COUNT(*) FROM sys.indexes WHERE name='FH_IDX1' AND object_id = OBJECT_ID('FINANCIAL_HIST')) = 0)
			EXEC ('CREATE UNIQUE INDEX FH_IDX1 ON FINANCIAL_HIST (FH_ROW_ID)');
      END TRY
      BEGIN CATCH
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());      
        insert into fin_hist_err_log (JOB_ID, Obj_name,err_statement,loc,err_msg,create_date) 
		values (@p_jobID, 'Create Index FH_IDX1','Create Index FH_IDX1 Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
      END CATCH;
      
      Begin TRY
		IF((SELECT COUNT(*) FROM sys.indexes WHERE name='FH_IDX2' AND object_id = OBJECT_ID('FINANCIAL_HIST')) = 0)
		BEGIN	
			IF (@m_str_parm_value <> '0')
				EXEC ('CREATE UNIQUE INDEX FH_IDX2 ON FINANCIAL_HIST (CLAIM_ID, EVAL_DATE, RC_ROW_ID)');
			ELSE
				EXEC ('CREATE UNIQUE INDEX FH_IDX2 ON FINANCIAL_HIST (CLAIM_ID, CLAIMANT_EID, UNIT_ID, RESERVE_TYPE_CODE, EVAL_DATE)');
		END	
      END TRY
      BEGIN CATCH 
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage(ERROR_MESSAGE());
        insert into fin_hist_err_log ( job_id,Obj_name,err_statement,loc,err_msg,create_date) 
		values (@p_jobID,'Create Index FH_IDX2','Create Index FH_IDX2 Failed..',ERROR_LINE(),ERROR_MESSAGE(),SYSDATETIME());
      end CATCH
	
		SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('SUCCESS');	
	END
SET NOCOUNT OFF
END 
GO