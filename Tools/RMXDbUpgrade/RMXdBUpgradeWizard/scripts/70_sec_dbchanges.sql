;Deb FUNCTION_LIST Constraint added
[FORGIVE_ALTER] ALTER TABLE FUNCTION_LIST ADD CONSTRAINT PK_FUNCTION_LIST PRIMARY KEY (FUNC_ID)

; XHU 04/22/2005 change function id for create/edit under short term disability's non-occ payments.
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE,CHECKSUM) values (62502,'View',62500,1,NULL)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE,CHECKSUM) values (62503,'Update',62500,1,NULL)


;kdb - 09/12/2005 - MITS 5653 Security Database changes for Case Management Project for Non-Occ Claims (DI)

[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623100,'Case Manager History',60000,0)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623101,'View',623100,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623102,'Update',623100,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623103,'Create',623100,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623104,'Delete',623100,2)

[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(633100,'Case Manager Notes',623100,0)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(633101,'View',633100,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(633102,'Update',633100,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(633103,'Create',633100,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(633104,'Delete',633100,2)

[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623200,'Treatment Plan',60000,0)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623201,'View',623200,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623202,'Update',623200,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623203,'Create',623200,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623204,'Delete',623200,2)

[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623300,'Medical Management Savings',60000,0)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623301,'View',623300,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623302,'Update',623300,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623303,'Create',623300,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623304,'Delete',623300,2)

[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623400,'Accommodations',60000,0)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623401,'View',623400,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623402,'Update',623400,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623403,'Create',623400,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623404,'Delete',623400,2)

[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623500,'Vocational Rehabilitation',60000,0)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623501,'View',623500,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623502,'Update',623500,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623503,'Create',623500,2)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623504,'Delete',623500,2)

[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(623600,'Disability Guidelines',60000,0)

;kdb End MITS 5653

;VD MITS 6428 01/04/2006
[SQL Server] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_FAILURE (USER_LOGIN varchar(50) NULL,USER_PASSWORD varchar(50) NULL,IP_ADDRESS varchar(50) NULL,BROWSER varchar(100) NULL,DTTM_FAILED varchar(50) NULL)
[Sybase] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_FAILURE (USER_LOGIN varchar(50) NULL,USER_PASSWORD varchar(50) NULL,IP_ADDRESS varchar(50) NULL,BROWSER varchar(100) NULL,DTTM_FAILED varchar(50) NULL)
[Oracle] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_FAILURE (USER_LOGIN varchar2(50) NULL,USER_PASSWORD varchar2(50) NULL,IP_ADDRESS varchar2(50) NULL,BROWSER varchar2(100) NULL,DTTM_FAILED varchar2(50) NULL)
[Informix] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_FAILURE (USER_LOGIN varchar(50),USER_PASSWORD varchar(50),IP_ADDRESS varchar(50),BROWSER varchar(100),DTTM_FAILED varchar(50))
[DB2] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_FAILURE (USER_LOGIN varchar(50),USER_PASSWORD varchar(50),IP_ADDRESS varchar(50),BROWSER varchar(100),DTTM_FAILED varchar(50))
[Access] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_FAILURE (USER_LOGIN text(50),USER_PASSWORD text(50),IP_ADDRESS text(50),BROWSER text(100),DTTM_FAILED text(50))

[SQL Server] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_TRACK (USER_ID INTEGER NULL,USER_LOGIN varchar(50) NULL,IP_ADDRESS varchar(50) NULL,HOST_NAME varchar(100) NULL,BROWSER varchar(100) NULL,DTTM_LOGIN varchar(50) NULL)
[Sybase] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_TRACK (USER_ID INTEGER NULL,USER_LOGIN varchar(50) NULL,IP_ADDRESS varchar(50) NULL,HOST_NAME varchar(100) NULL,BROWSER varchar(100) NULL,DTTM_LOGIN varchar(50) NULL)
[Oracle] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_TRACK (USER_ID NUMBER(10) NULL,USER_LOGIN varchar2(50) NULL,IP_ADDRESS varchar2(50) NULL,HOST_NAME varchar2(100) NULL,BROWSER varchar2(100) NULL,DTTM_LOGIN varchar2(50) NULL)
[Informix] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_TRACK (USER_ID INTEGER,USER_LOGIN varchar(50),IP_ADDRESS varchar(50),HOST_NAME varchar(100),BROWSER varchar(100),DTTM_LOGIN varchar(50))
[DB2] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_TRACK (USER_ID INTEGER,USER_LOGIN varchar(50),IP_ADDRESS varchar(50),HOST_NAME varchar(100),BROWSER varchar(100),DTTM_LOGIN varchar(50))
[Access] [FORGIVE_CREATETABLE] CREATE TABLE USER_LOGIN_TRACK (USER_ID LONG,USER_LOGIN text(50),IP_ADDRESS text(50),HOST_NAME text(100),BROWSER text(100),DTTM_LOGIN text(50))
;

'MWC Add security for voiding checks on closed claims
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (9692,'Allow Voids on Closed Claims',9650,2)

'SSV 06/16/2006 for RMNet 7.06 
'Update the DOMAIN_LOGIN_ENABLED field to 0 for all clients that currently have NULL values
[SQL Server][FORGIVE] UPDATE SETTINGS SET DOMAIN_LOGIN_ENABLED = 0 WHERE DOMAIN_LOGIN_ENABLED IS NULL
[Oracle][FORGIVE] UPDATE SETTINGS SET DOMAIN_LOGIN_ENABLED = 0 WHERE DOMAIN_LOGIN_ENABLED IS NULL;

;************************************************************************************
; gagnihotri MITS 11182 02/05/2008
; Adding Columns UPDATED_BY_USER and DTTM_RCD_LAST_UPD

;DATA_SOURCE_TABLE

[Access]     [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD UPDATED_BY_USER TEXT(25)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD UPDATED_BY_USER VARCHAR(25) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD UPDATED_BY_USER VARCHAR(25) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD UPDATED_BY_USER VARCHAR(25) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD UPDATED_BY_USER VARCHAR(25)
[DB2]        [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD UPDATED_BY_USER VARCHAR(25)

[Access]     [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD DTTM_RCD_LAST_UPD TEXT(14)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) 
[DB2]        [FORGIVE_ALTER] ALTER TABLE DATA_SOURCE_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14)

;DOMAIN

[Access]     [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD UPDATED_BY_USER TEXT(25)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD UPDATED_BY_USER VARCHAR(25) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD UPDATED_BY_USER VARCHAR(25) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD UPDATED_BY_USER VARCHAR(25) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD UPDATED_BY_USER VARCHAR(25)
[DB2]        [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD UPDATED_BY_USER VARCHAR(25)

[Access]     [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD DTTM_RCD_LAST_UPD TEXT(14)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD DTTM_RCD_LAST_UPD VARCHAR(14) 
[DB2]        [FORGIVE_ALTER] ALTER TABLE DOMAIN ADD DTTM_RCD_LAST_UPD VARCHAR(14)

;SETTINGS

[Access]     [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD UPDATED_BY_USER TEXT(25)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD UPDATED_BY_USER VARCHAR(25) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD UPDATED_BY_USER VARCHAR(25) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD UPDATED_BY_USER VARCHAR(25) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD UPDATED_BY_USER VARCHAR(25)
[DB2]        [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD UPDATED_BY_USER VARCHAR(25)

[Access]     [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD DTTM_RCD_LAST_UPD TEXT(14)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD DTTM_RCD_LAST_UPD VARCHAR(14) 
[DB2]        [FORGIVE_ALTER] ALTER TABLE SETTINGS ADD DTTM_RCD_LAST_UPD VARCHAR(14)

;USER_DETAILS_TABLE

[Access]     [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD UPDATED_BY_USER TEXT(25)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD UPDATED_BY_USER VARCHAR(25) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD UPDATED_BY_USER VARCHAR(25) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD UPDATED_BY_USER VARCHAR(25) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD UPDATED_BY_USER VARCHAR(25)
[DB2]        [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD UPDATED_BY_USER VARCHAR(25)

[Access]     [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD DTTM_RCD_LAST_UPD TEXT(14)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) 
[DB2]        [FORGIVE_ALTER] ALTER TABLE USER_DETAILS_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14)

;USER_ORGSEC

[Access]     [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD UPDATED_BY_USER TEXT(25)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD UPDATED_BY_USER VARCHAR(25) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD UPDATED_BY_USER VARCHAR(25) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD UPDATED_BY_USER VARCHAR(25) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD UPDATED_BY_USER VARCHAR(25)
[DB2]        [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD UPDATED_BY_USER VARCHAR(25)

[Access]     [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD DTTM_RCD_LAST_UPD TEXT(14)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD DTTM_RCD_LAST_UPD VARCHAR(14) 
[DB2]        [FORGIVE_ALTER] ALTER TABLE USER_ORGSEC ADD DTTM_RCD_LAST_UPD VARCHAR(14)

;USER_TABLE

[Access]     [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD UPDATED_BY_USER TEXT(25)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD UPDATED_BY_USER VARCHAR(25) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD UPDATED_BY_USER VARCHAR(25) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD UPDATED_BY_USER VARCHAR(25) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD UPDATED_BY_USER VARCHAR(25)
[DB2]        [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD UPDATED_BY_USER VARCHAR(25)

[Access]     [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD DTTM_RCD_LAST_UPD TEXT(14)
[SQL Server] [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Sybase]     [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Oracle]     [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) NULL
[Informix]   [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14) 
[DB2]        [FORGIVE_ALTER] ALTER TABLE USER_TABLE ADD DTTM_RCD_LAST_UPD VARCHAR(14)

; gagnihotri MITS 11182 02/05/2008 End
;************************************************************************************

;************************************************************************************
; Ankur MITS 11058 02/27/2008
; Adding fields for Case Manager History table

[FORGIVE] UPDATE FUNCTION_LIST SET FUNCTION_NAME = 'CM_X_CMgr_Reffered_To_First Name' where FUNC_ID=1080100011
[FORGIVE] UPDATE FUNCTION_LIST SET FUNCTION_NAME = 'CM_X_CMgr_Reffered_To_Last Name' where FUNC_ID=1080100012
[FORGIVE] UPDATE FUNCTION_LIST SET FUNC_ID=1080100017 where FUNC_ID=1080100013 
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) values (1080100013,'CM_X_CMgr_Reffered_To_Full Name',50801,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) values (1080100014,'CM_X_CMgr_Manager_First Name',50801,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) values (1080100015,'CM_X_CMgr_Manager_Last Name',50801,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) values (1080100016,'CM_X_CMgr_Manager_Full Name',50801,0)

; Ankur MITS 11058 02/27/2008 End
;************************************************************************************

;************************************************************************************
; Ankur MITS 11139 02/27/2008
; Adding fields for BRS Invoice detail table

[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400044,'BRS I Det_Table_Code_2',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400045,'BRS I Det_Fee_Amt1',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400046,'BRS I Det_Fee_Amt2',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400047,'BRS I Det_Table ID',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400048,'BRS I Det_Prescription Number ',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400049,'BRS I Det_Prescription Date ',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400050,'BRS I Det_Drug Name',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400051,'BRS I Det_Medication Quantity ',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400052,'BRS I Det_Med. Days Supply ',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400053,'BRS I Det_Supply Charge ',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400054,'BRS I Det_Pharmacy Usual Charge',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400055,'BRS I Det_Prescription Indicator (Code)',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400056,'BRS I Det_Prescription Indicator (Desc)',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400057,'BRS I Det_Purchase\Rental Ind. (Code)',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400058,'BRS I Det_Purchase\Rental Ind. (Desc)',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400059,'BRS I Det_Dispensed as Written (Code)',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400060,'BRS I Det_Dispensed as Written (Desc)',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400061,'BRS I Det_Revenue Code ',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400062,'BRS I Det_HCPCS Level II Code',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400063,'BRS I Det_Pharmacy Phy. NDC ',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400064,'BRS I Det_Pharmacy Phy. NDC Desc',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400065,'BRS I Det_Ref Number',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400066,'BRS I Det_FL License #',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400067,'BRS I Det_Pres. Certification (Code)',50044,0)
[FORGIVE] INSERT INTO FUNCTION_LIST ( FUNC_ID, FUNCTION_NAME, PARENT_ID, ENTRY_TYPE ) VALUES (1004400068,'BRS I Det_Pres. Certification (Desc)',50044,0)

; Ankur MITS 11139 02/27/2008 End
;************************************************************************************


;************************************************************************************
; Ankur MITS 10490 02/27/2008
; Adding fields for Contact Info table

[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (52005, 'Contact Info', 50000, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500001, 'Contact_Name', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500002, 'Contact_Title', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500003, 'Contact_Initials', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500004, 'Contact_Addr. Line 1', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500005, 'Contact_Addr. Line 2', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500006, 'Contact_City', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500007, 'Contact_State (Short)', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500008, 'Contact_State (Long)', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500009, 'Contact_Zip', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500010, 'Contact_Phone', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500011, 'Contact_Fax', 52005, 0)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (1200500013, 'Contact_Email', 52005, 0)

; Ankur MITS 10490 02/27/2008 End
;************************************************************************************

;DF MITS 12768 7/29/2008
;make the attachments button enabled when going to the claimant screen from GC and VA
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES(1806,'Attachments',1800,1)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID,FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(1807,'View/Play/Print',1800,1)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID,FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(1808,'Attach To',1800,1)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID,FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(1809,'Detach From',1800,1)

[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES(7656,'Attachments',7650,1)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID,FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(7657,'View/Play/Print',7650,1)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID,FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(7658,'Attach To',7650,1)
[FORGIVE] INSERT INTO FUNCTION_LIST (FUNC_ID,FUNCTION_NAME, PARENT_ID, ENTRY_TYPE) VALUES(7659,'Detach From',7650,1)

;VD MITS 15805
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (60051,'View',60050,1)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (60052,'Update',60050,1)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (60053,'Create New',60050,1)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (60054,'Delete',60050,1)

;DF MITS 17960
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (60951,'View',60950,1)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (60952,'Update',60950,1)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (60953,'Create New',60950,1)
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (60954,'Delete',60950,1)

;DF MITS 17987
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (4860,'Allow Access To WC PDF Forms',4800,2)

;OFAC CHECK - PAYEE CHECK UTILITY
[FORGIVE] INSERT INTO FUNCTION_LIST(FUNC_ID,FUNCTION_NAME,PARENT_ID,ENTRY_TYPE) VALUES (10900 ,'Allow Access to Payee Check Review',9500,0)

