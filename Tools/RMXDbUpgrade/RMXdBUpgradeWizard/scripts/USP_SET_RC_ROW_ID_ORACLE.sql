[GO_DELIMITER]
create or replace
PROCEDURE USP_SET_RC_ROW_ID(splitTableName VARCHAR2,fundsTableName VARCHAR2,splitPrimaryKey VARCHAR2,fundsPrimaryKey VARCHAR2)
AS
BEGIN
DECLARE iMinID INT;
							iMaxID INT;
							iLoopCount INT;							
							iRC_ROW_ID INT;
							iSplit_Row_id INT;
							sSQL VARCHAR2(2000);
              sSqlCursor VARCHAR2(4000);
              RCROWIDCURSOR SYS_REFCURSOR;
              iCount INT;              
              BEGIN             
                              
                 sSQL:= 'SELECT COUNT(*) FROM ' || splitTableName || ' WHERE RC_ROW_ID IS NOT NULL' ;                
                 EXECUTE IMMEDIATE sSQL INTO iCount;  
                 IF iCount > 1 THEN
                   RETURN;
                 END IF;
              
                  sSQL:='SELECT MIN(' ||  splitPrimaryKey || ' ) FROM ' || splitTableName;                  
                  EXECUTE IMMEDIATE sSQL INTO iMinID;
                                    
                  sSQL:='SELECT MAX(' ||  splitPrimaryKey || ' ) FROM ' || splitTableName;
                  EXECUTE IMMEDIATE sSQL INTO iMaxID;                 
                                  
                  iLoopCount :=iMinID;
                  WHILE iLoopCount <= iMaxID
                  LOOP                    
                      sSqlCursor := 'SELECT  FTS.'  || splitPrimaryKey || ',RC.RC_ROW_ID FROM RESERVE_CURRENT RC INNER JOIN ' 
                                    || fundsTableName || ' F ON RC.CLAIM_ID = F.CLAIM_ID AND RC.CLAIMANT_EID = F.CLAIMANT_EID 
                                    AND RC.UNIT_ID = F.UNIT_ID INNER JOIN ' || splitTableName || ' FTS ON  F.' 
                                    || fundsPrimaryKey || '= FTS.'|| fundsPrimaryKey || ' AND 
                                    RC.RESERVE_TYPE_CODE = FTS.RESERVE_TYPE_CODE WHERE FTS.'||  splitPrimaryKey || ' >= ' || iLoopCount
                                    || ' AND FTS.'||  splitPrimaryKey || '<=' || ( iLoopCount + 4999) ; 								
                        BEGIN      --inner cursor start
                            OPEN RCROWIDCURSOR FOR sSqlCursor;
                            LOOP
                               FETCH RCROWIDCURSOR INTO iSplit_Row_id, iRC_ROW_ID;		
                               EXIT WHEN RCROWIDCURSOR%NOTFOUND;                            
                               sSQL := 'UPDATE '||  splitTableName || ' SET RC_ROW_ID = ' || iRC_ROW_ID || ' WHERE '||  splitPrimaryKey || ' = ' || iSplit_Row_id;								
                               EXECUTE IMMEDIATE sSQL;                            
                             END LOOP;                                                    
                             CLOSE RCROWIDCURSOR;
                        END;
                     COMMIT;
                     iLoopCount := iLoopCount + 5000;
                   END LOOP;                     
                      
                   
              END;
END USP_SET_RC_ROW_ID;
GO




