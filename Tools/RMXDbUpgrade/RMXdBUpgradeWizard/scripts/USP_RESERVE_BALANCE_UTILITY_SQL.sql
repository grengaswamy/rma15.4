[GO_DELIMITER]
IF OBJECT_ID('USP_RESERVE_BALANCE_UTILITY', 'P') IS NOT NULL
    DROP PROCEDURE USP_RESERVE_BALANCE_UTILITY
GO
	
CREATE PROCEDURE USP_RESERVE_BALANCE_UTILITY(@psnglclaims int, @pClaimNumber varchar(25) = '', @psFrom  VARCHAR(10) = null, @psTo VARCHAR(10) = null, @p_sqlerrm	varchar(MAX) OUTPUT)
AS
BEGIN
DECLARE @pClaimID int
declare @v_maxclid int
declare @v_minclid int
declare @v_cntrc int
declare @v_sFrom varchar(10)
declare @v_sTo varchar(10)
declare @v_tempminclid int
declare @V_SAMOUNT FLOAT
declare @V_CLAIM_ID int
DECLARE @CLAIMANT_EID		AS INT;
DECLARE @UNIT_ID			AS INT;
DECLARE @RESERVE_TYPE_CODE	AS INT;
declare @V_RC_ROW_ID int
declare @v_SQL as nvarchar(max);
declare @v_SQL1 as nvarchar(max);
declare @V_sTempSQL varchar(4000)
declare @V_CLID int
declare @V_sRsvParentShortCode int
DECLARE @ErrorMessage NVARCHAR(4000)
DECLARE @V_BAMOUNT int 
DECLARE @V_RAMOUNT int
DECLARE @V_PDTOT int
DECLARE @V_CTOT int
DECLARE @V_RTCODE varchar(40) 
DECLARE @V_LBUSCD varchar(40)
DECLARE @V_CLSTCD varchar(40)
DECLARE @shCode varchar(25)
DECLARE @nextClaimId AS INT;
DECLARE @IsCarrierOn as VARCHAR(50);
DECLARE @FTSRCRowID as VARCHAR(50);
DECLARE @RC_ROW_ID AS INT;
DECLARE @IsMultiCurr as INT;
DECLARE @BALANCEAMOUNT AS FLOAT;
DECLARE @CHANGEAMOUNT AS FLOAT;
DECLARE @CLAIM_CURRENCY_RESERVE_AMOUNT AS FLOAT;
DECLARE @CLAIM_CURRENCY_INCURRED_AMOUNT AS FLOAT;
DECLARE @CLAIM_CURR_COLLECTION_TOTAL AS FLOAT;
DECLARE @CLAIM_CURRENCY_PAID_TOTAL AS FLOAT;
DECLARE @CLAIM_CURRENCY_BALANCE_AMOUNT AS FLOAT;
DECLARE @RSV_ROW_ID AS INT;
DECLARE @DATE_ENTERED AS VARCHAR(8); 
DECLARE @DTTM_RCD_ADDED AS VARCHAR(14); 
DECLARE @CLOSED_FLAG AS INT = -1;
DECLARE @AUTO_ADJ_FLAG AS INT = 0;
DECLARE @BASETOCLAIMCURRATE AS FLOAT;
DECLARE @RESERVE AS FLOAT;
DECLARE @INCURRED AS FLOAT;
DECLARE @COLLECTION AS FLOAT
DECLARE @PAID AS FLOAT;
DECLARE @CLAIM_ID AS INT;
DECLARE @DEPTEID AS INT;
DECLARE @CRC AS INT;
DECLARE @RESSTATUSCODE AS INT;
DECLARE @CLAIMCURRCODE AS INT;
DECLARE @POLCVG_LOSS_ROW_ID as INT;
DECLARE @ISFIRSTFINAL as INT;
DECLARE @POLICY_CVG_SEQNO as INT;
DECLARE @REASON AS VARCHAR(30) = 'AUTO ADJUST';
DECLARE @ENTERED_BY_USER AS VARCHAR(30) = 'RESBAL';

	SELECT @IsCarrierOn  = str_parm_value FROM Parms_Name_Value where parm_name='MULTI_COVG_CLM';
	
	IF @IsCarrierOn <> '0'
		SET @FTSRCRowID = ', FTS.RC_ROW_ID ';
	ELSE
		SET @FTSRCRowID = '';
	
	IF @psnglclaims = 1 
	BEGIN
		SET @pClaimID = (Select CLAIM_ID from CLAIM where CLAIM_NUMBER = @pClaimNumber);
		IF @pClaimID IS NULL
		BEGIN
			SET @p_sqlerrm=	dbo.FormatMessage('Provided claim number does not exist');
			RETURN;
		END
		UPDATE RESERVE_CURRENT SET PAID_TOTAL = 0, COLLECTION_TOTAL = 0 WHERE CLAIM_ID = @pClaimID
	END
	ELSE
	BEGIN
		IF @psFrom IS NOT NULL AND @psTo IS NOT NULL
		BEGIN
			select @v_sFrom = convert(int, convert(varchar(10), @psFrom, 112))
			select @v_sTo = convert(int, convert(varchar(10), @psto, 112))
			BEGIN TRY
				SELECT @v_maxclid = MAX(RESERVE_CURRENT.CLAIM_ID), @v_minclid=MIN(RESERVE_CURRENT.CLAIM_ID)
				FROM RESERVE_CURRENT , CLAIM WHERE CLAIM.CLAIM_ID = RESERVE_CURRENT.CLAIM_ID 
				AND DATE_OF_CLAIM >= @v_sFrom AND DATE_OF_CLAIM <= @v_sTo;

				SELECT @v_cntrc = COUNT(*) FROM RESERVE_CURRENT, CLAIM WHERE CLAIM.CLAIM_ID = RESERVE_CURRENT.CLAIM_ID 
				AND DATE_OF_CLAIM >= @v_sFrom AND DATE_OF_CLAIM <= @v_sTo;
			END TRY
			BEGIN CATCH
				SELECT @ErrorMessage = ERROR_MESSAGE()
				RAISERROR (@ErrorMessage, 16, 1)
				RETURN
			END CATCH

			IF @v_cntrc = 0
			BEGIN
				--RAISERROR ('There are no reserve current entries',16,1)
				SET @p_sqlerrm=	dbo.FormatMessage('Reserves does not exists in the provided criteria');
				RETURN
			END
		END
		ELSE
		BEGIN
			SELECT @v_maxclid=MAX(CLAIM_ID), @v_minclid=MIN(CLAIM_ID) FROM RESERVE_CURRENT
			SELECT @v_cntrc = COUNT(*) FROM RESERVE_CURRENT
		END
		set @v_SQL = 'UPDATE RESERVE_CURRENT SET PAID_TOTAL = 0.0, COLLECTION_TOTAL = 0.0 
						WHERE CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
						
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
BEGIN----------------------------UPDATE PAID_TOTAL STARTS------------------------------------------------
	SET @p_sqlerrm=	dbo.FormatMessage('UPDATE PAID_TOTAL STARTS');
	SET @V_SQL = 'DECLARE rcCursor cursor for SELECT SUM(FTS.AMOUNT), F.CLAIM_ID, F.CLAIMANT_EID, FTS.RESERVE_TYPE_CODE, F.UNIT_ID '+ @FTSRCRowID+
				'FROM FUNDS F, FUNDS_TRANS_SPLIT FTS, CLAIM C WHERE F.CLAIM_ID = C.CLAIM_ID AND F.TRANS_ID = FTS.TRANS_ID';
	
	if @psnglclaims = 1  
		set @V_SQL = @V_SQL + ' AND F.CLAIM_ID = @pClaimID';
    Else If @psFrom is not null And @psTo is not null  
		begin    
            set @V_SQL = @V_SQL +' AND C.DATE_OF_CLAIM >= @v_sFrom AND C.DATE_OF_CLAIM <= @v_sTo';
        end
  
	SET @V_SQL = @V_SQL +' AND F.VOID_FLAG = 0 AND F.PAYMENT_FLAG <> 0 GROUP BY F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE '+@FTSRCRowID+' ORDER BY F.CLAIM_ID';
	
	IF @psnglclaims = 1
	BEGIN
		exec sp_executesql @v_SQL
			,N' @pClaimID INT'
			,@pClaimID;
	END
	ELSE
	BEGIN 
		If @psFrom is not null And @psTo is not null
		BEGIN
			exec sp_executesql @v_SQL
			,N'@v_sFrom VARCHAR(10), @v_sTo VARCHAR(10)'
			,@v_sFrom, @v_sTo;
		END
		ELSE
		BEGIN
			exec sp_executesql @v_SQL
		END
	END
	
	open rcCursor
		IF @IsCarrierOn <> '0'
		BEGIN
			fetch NEXT from rcCursor into @V_SAMOUNT, @V_CLAIM_ID, @CLAIMANT_EID, @RESERVE_TYPE_CODE, @UNIT_ID, @RC_ROW_ID
			while @@fetch_status = 0
			begin
				UPDATE RESERVE_CURRENT SET PAID_TOTAL = ROUND(@V_SAMOUNT,2) 
				WHERE CLAIM_ID = @V_CLAIM_ID  
				AND RC_ROW_ID = @RC_ROW_ID
				
				fetch NEXT from rcCursor into @V_SAMOUNT, @V_CLAIM_ID, @CLAIMANT_EID, @RESERVE_TYPE_CODE, @UNIT_ID, @RC_ROW_ID
			end
		END
		ELSE	
		BEGIN
			fetch NEXT from rcCursor into @V_SAMOUNT, @V_CLAIM_ID, @CLAIMANT_EID, @RESERVE_TYPE_CODE, @UNIT_ID
		
			while @@fetch_status = 0
			begin
				
				UPDATE RESERVE_CURRENT SET PAID_TOTAL = ROUND(@V_SAMOUNT,2) 
				WHERE CLAIM_ID = @V_CLAIM_ID  
				AND CLAIMANT_EID = @CLAIMANT_EID
				AND RESERVE_TYPE_CODE = @RESERVE_TYPE_CODE
				AND UNIT_ID = @UNIT_ID;
				
				fetch next from rcCursor into @V_SAMOUNT, @V_CLAIM_ID, @CLAIMANT_EID, @RESERVE_TYPE_CODE, @UNIT_ID
			end
		END
	close rcCursor
	deallocate rcCursor
	SET @p_sqlerrm=	@p_sqlerrm + dbo.FormatMessage('UPDATE PAID_TOTAL ENDS');
END----------------------------UPDATE PAID_TOTAL ENDS------------------------------------------------
BEGIN----------------------------UPDATE COLLECTION_TOTAL STARTS----------------------------------------
	SET @p_sqlerrm=	@p_sqlerrm + dbo.FormatMessage('UPDATE COLLECTION_TOTAL STARTS');
	
	SET @V_SQL = 'DECLARE rcCursor cursor for SELECT SUM(FTS.AMOUNT), F.CLAIM_ID, F.CLAIMANT_EID, FTS.RESERVE_TYPE_CODE, F.UNIT_ID '+ @FTSRCRowID +
				' FROM FUNDS F, FUNDS_TRANS_SPLIT FTS, CLAIM C WHERE F.CLAIM_ID = C.CLAIM_ID AND F.TRANS_ID = FTS.TRANS_ID';

	if @psnglclaims = 1  
		set @V_SQL = @V_SQL + ' AND F.CLAIM_ID = @pClaimID';
    Else If @psFrom is not null And @psTo is not null  
		begin    
            set @V_SQL = @V_SQL +' AND C.DATE_OF_CLAIM >= @v_sFrom AND C.DATE_OF_CLAIM <= @v_sTo';
        end
  
	SET @V_SQL = @V_SQL +' AND F.VOID_FLAG = 0 AND F.PAYMENT_FLAG = 0 GROUP BY F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, FTS.RESERVE_TYPE_CODE'+ @FTSRCRowID+' ORDER BY F.CLAIM_ID';

	IF @psnglclaims = 1
	BEGIN
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END
	ELSE
	BEGIN 
		If @psFrom is not null And @psTo is not null
		BEGIN
			exec sp_executesql @v_SQL
			,N'@v_sFrom VARCHAR(10), @v_sTo VARCHAR(10)'
			,@v_sFrom, @v_sTo;
		END
		ELSE
		BEGIN
			exec sp_executesql @v_SQL
		END
	END
	
	open rcCursor
	IF @IsCarrierOn <> '0'	
	BEGIN
		fetch NEXT from rcCursor into @V_SAMOUNT, @V_CLAIM_ID, @CLAIMANT_EID, @RESERVE_TYPE_CODE, @UNIT_ID, @RC_ROW_ID
			while @@fetch_status = 0
			begin
				UPDATE RESERVE_CURRENT SET COLLECTION_TOTAL = ROUND(@V_SAMOUNT,2)
				WHERE CLAIM_ID = @V_CLAIM_ID  
				AND RC_ROW_ID = @RC_ROW_ID;
			
			fetch NEXT from rcCursor into @V_SAMOUNT, @V_CLAIM_ID, @CLAIMANT_EID, @RESERVE_TYPE_CODE, @UNIT_ID, @RC_ROW_ID
		end
	END
	ELSE
	BEGIN
		fetch NEXT from rcCursor into @V_SAMOUNT, @V_CLAIM_ID, @CLAIMANT_EID, @RESERVE_TYPE_CODE, @UNIT_ID
			while @@fetch_status = 0
			begin
				UPDATE RESERVE_CURRENT SET COLLECTION_TOTAL = ROUND(@V_SAMOUNT,2)
				WHERE CLAIM_ID = @V_CLAIM_ID  
				AND CLAIMANT_EID = @CLAIMANT_EID
				AND RESERVE_TYPE_CODE = @RESERVE_TYPE_CODE
				AND UNIT_ID = @UNIT_ID;
			
			fetch NEXT from rcCursor into @V_SAMOUNT, @V_CLAIM_ID, @CLAIMANT_EID, @RESERVE_TYPE_CODE, @UNIT_ID
		end
	END
	close rcCursor
	deallocate rcCursor
	SET @p_sqlerrm=	@p_sqlerrm + dbo.FormatMessage('UPDATE COLLECTION_TOTAL ENDS');
END----------------------------UPDATE COLLECTION_TOTAL ENDS----------------------------------------
BEGIN----------------------------UPDATE BALANCE_AMOUNT STARTS----------------------------------------
	SET @p_sqlerrm=	@p_sqlerrm + dbo.FormatMessage('UPDATE BALANCE_AMOUNT STARTS');
	-- case 2 ->> if SYS_PARMS_LOB.COLL_IN_RSV_BAL='N' EXCLUDING RECOVERY RESERVES
	SET @V_SQL1 = 'UPDATE RESERVE_CURRENT
	SET RESERVE_CURRENT.BALANCE_AMOUNT =  (RESERVE_CURRENT.RESERVE_AMOUNT-RESERVE_CURRENT.PAID_TOTAL)
	FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
	WHERE (RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID) AND (CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE) 
	AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0))
	AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
	
	if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
-- case 1 ->> if SYS_PARMS_LOB_COLL_IN_RSV_BAL='Y' EXCLUDING RECOVERY RESERVES
	SET @V_SQL1 = 'UPDATE RESERVE_CURRENT
		SET RESERVE_CURRENT.BALANCE_AMOUNT = (RESERVE_CURRENT.RESERVE_AMOUNT-(RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL)) 
		FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
		WHERE (RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID) AND (CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE) 
		AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 )))
		AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
	if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
-- case 3 ->> Closed claim
	IF OBJECT_ID('TEMPRCCHANGEAMOUNT', 'U') IS NOT NULL
	BEGIN
		SET @v_SQL1 = 'DROP TABLE TEMPRCCHANGEAMOUNT' 
		EXECUTE (@v_SQL1);
	END
	
	SET @v_SQL1 = 'CREATE TABLE TEMPRCCHANGEAMOUNT (RC_ROW_ID INT , CHANGE_AMOUNT FLOAT)';
	EXECUTE (@v_SQL1);
	
	--CASE A -> For Closed Claim Set Reserve Amount to Zero is true and RESERVE_CURRENT.RESERVE_AMOUNT <> 0
	SET @v_SQL1 =  'INSERT INTO TEMPRCCHANGEAMOUNT 
	SELECT RC_ROW_ID, - ROUND(RESERVE_CURRENT.RESERVE_AMOUNT,2) AS CHANGE_AMOUNT
	FROM RESERVE_CURRENT 
	JOIN CLAIM ON RESERVE_CURRENT.CLAIM_ID = CLAIM.CLAIM_ID 
	JOIN CODES AS CSTAT ON CLAIM.CLAIM_STATUS_CODE=CSTAT.CODE_ID
	JOIN CODES AS ABSSTAT ON CSTAT.RELATED_CODE_ID=ABSSTAT.CODE_ID
	JOIN SYS_PARMS_LOB ON SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE
	WHERE ABSSTAT.SHORT_CODE=''C''
	AND SYS_PARMS_LOB.SET_TO_ZERO <> 0 
	AND RESERVE_CURRENT.RESERVE_AMOUNT <> 0
	AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
	
	if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
	
	SET @v_SQL1 =  'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.BALANCE_AMOUNT = ROUND(RESERVE_CURRENT.BALANCE_AMOUNT,2) - ROUND(RESERVE_CURRENT.RESERVE_AMOUNT,2)
	, RESERVE_CURRENT.RESERVE_AMOUNT = 0
	FROM RESERVE_CURRENT 
	JOIN CLAIM ON RESERVE_CURRENT.CLAIM_ID = CLAIM.CLAIM_ID 
	JOIN CODES AS CSTAT ON CLAIM.CLAIM_STATUS_CODE=CSTAT.CODE_ID
	JOIN CODES AS ABSSTAT ON CSTAT.RELATED_CODE_ID=ABSSTAT.CODE_ID
	JOIN SYS_PARMS_LOB ON SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE
	WHERE ABSSTAT.SHORT_CODE=''C''
	AND SYS_PARMS_LOB.SET_TO_ZERO <> 0 
	AND RESERVE_CURRENT.RESERVE_AMOUNT <> 0
	AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
	if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END

	--CASE B -> For Closed Claim Set Reserve Balance to Zero is true and RESERVE_CURRENT.RESERVE_BALANCE <> 0
	-- OR For Closed Claim Set negative Reserve Balance to Zero is true and RESERVE_CURRENT.RESERVE_BALANCE < 0
	SET @V_SQL1 =  'INSERT INTO TEMPRCCHANGEAMOUNT SELECT RC_ROW_ID, - ROUND(RESERVE_CURRENT.BALANCE_AMOUNT,2)
	FROM RESERVE_CURRENT 
	JOIN CLAIM ON RESERVE_CURRENT.CLAIM_ID = CLAIM.CLAIM_ID 
	JOIN CODES AS CSTAT ON CLAIM.CLAIM_STATUS_CODE=CSTAT.CODE_ID
	JOIN CODES AS ABSSTAT ON CSTAT.RELATED_CODE_ID=ABSSTAT.CODE_ID
	JOIN SYS_PARMS_LOB ON SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE
	WHERE ABSSTAT.SHORT_CODE=''C''
	AND 
	(	(SYS_PARMS_LOB.BAL_TO_ZERO <> 0 AND RESERVE_CURRENT.BALANCE_AMOUNT <> 0)
		OR (SYS_PARMS_LOB.NEG_BAL_TO_ZERO <> 0 AND RESERVE_CURRENT.BALANCE_AMOUNT < 0)
	)
	AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
	
    if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END

	SET @V_SQL1 =  'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.RESERVE_AMOUNT = ROUND(RESERVE_CURRENT.RESERVE_AMOUNT,2) - ROUND(RESERVE_CURRENT.BALANCE_AMOUNT,2)
	, RESERVE_CURRENT.BALANCE_AMOUNT = 0
	FROM RESERVE_CURRENT 
	JOIN CLAIM ON RESERVE_CURRENT.CLAIM_ID = CLAIM.CLAIM_ID 
	JOIN CODES AS CSTAT ON CLAIM.CLAIM_STATUS_CODE=CSTAT.CODE_ID
	JOIN CODES AS ABSSTAT ON CSTAT.RELATED_CODE_ID=ABSSTAT.CODE_ID
	JOIN SYS_PARMS_LOB ON SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE
	WHERE ABSSTAT.SHORT_CODE=''C''
	AND 
	(	(SYS_PARMS_LOB.BAL_TO_ZERO <> 0 AND RESERVE_CURRENT.BALANCE_AMOUNT <> 0)
		OR (SYS_PARMS_LOB.NEG_BAL_TO_ZERO <> 0 AND RESERVE_CURRENT.BALANCE_AMOUNT < 0)
	)
	AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
	
    if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END

-- Case 5: Recovery Reserve		
		SET @V_SQL1 = 'UPDATE RESERVE_CURRENT
		SET RESERVE_CURRENT.BALANCE_AMOUNT = RESERVE_CURRENT.RESERVE_AMOUNT - RESERVE_CURRENT.COLLECTION_TOTAL
		WHERE ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) = ''R'''
	if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
	SET @p_sqlerrm=	@p_sqlerrm + dbo.FormatMessage('UPDATE BALANCE_AMOUNT ENDS');
END----------------------------UPDATE BALANCE_AMOUNT ENDS----------------------------------------
BEGIN----------------------------UPDATE INCURRED_AMOUNT STARTS----------------------------------------
	SET @p_sqlerrm=	@p_sqlerrm + dbo.FormatMessage('UPDATE INCURRED_AMOUNT STARTS');
-- Case 1: Recovery Reserve 
	SET @V_SQL1 = 'UPDATE RESERVE_CURRENT
	SET RESERVE_CURRENT.INCURRED_AMOUNT =  (CASE WHEN RESERVE_CURRENT.BALANCE_AMOUNT < 0 THEN RESERVE_CURRENT.COLLECTION_TOTAL 
												ELSE RESERVE_CURRENT.BALANCE_AMOUNT + RESERVE_CURRENT.COLLECTION_TOTAL	END)
	FROM RESERVE_CURRENT
	WHERE ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) = ''R'''
	if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
-- CASE 2: None Recovery Reserve 
-- Case E: Include Collection in Reserve Balance = 0 Include Collection in Incurred = 0 and Balance Amount >= 0
	SET @V_SQL1 = 'UPDATE RESERVE_CURRENT 
	SET RESERVE_CURRENT.INCURRED_AMOUNT = RESERVE_CURRENT.BALANCE_AMOUNT+RESERVE_CURRENT.PAID_TOTAL
	FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
    WHERE RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID AND CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE
    AND RESERVE_CURRENT.BALANCE_AMOUNT >= 0.0 
	AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0))  AND ((SYS_PARMS_LOB.COLL_IN_INCUR_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL = 0))
	AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
    if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
--Case F: Include Collection in Reserve Balance = 0 Include Collection in Incurred = 0 and Balance Amount < 0
	SET @V_SQL1 = 'UPDATE RESERVE_CURRENT 
	SET RESERVE_CURRENT.INCURRED_AMOUNT = RESERVE_CURRENT.PAID_TOTAL
	FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
    WHERE RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID AND CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE
    AND RESERVE_CURRENT.BALANCE_AMOUNT < 0.0 
	AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0 )) AND ((SYS_PARMS_LOB.COLL_IN_INCUR_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL = 0))
	AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
    if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
-- Case A: Include Collection in Reserve Balance <> 0 and Balance Amount >= 0 and Paid_Total - Collection_Total >= 0
--		 : Include Collection in Reserve Balance = 0 and Collection in INCURRED <> 0 and Balance Amount >= 0
	--SET @V_SQL1 = 'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.INCURRED_AMOUNT = 
 --   RESERVE_CURRENT.BALANCE_AMOUNT + (RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL)
	--FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
 --   WHERE RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID AND CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE
 --   AND RESERVE_CURRENT.BALANCE_AMOUNT >= 0.0
 --   AND (
	--		(((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
	--										AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
	--										WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 ))) AND RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL >= 0.0)
	--		OR
	--		(((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0 )) AND 
	--		((SYS_PARMS_LOB.COLL_IN_INCUR_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL <> 0 
	--										AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
	--										WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = 0))))
	--	)
	--AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
	SET @V_SQL1 = 'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.INCURRED_AMOUNT = 
    RESERVE_CURRENT.BALANCE_AMOUNT + (RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL)
	FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
    WHERE RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID AND CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE
    AND RESERVE_CURRENT.BALANCE_AMOUNT >= 0.0
    AND (
			(((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 ))))
			OR
			(((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0 )) AND 
			((SYS_PARMS_LOB.COLL_IN_INCUR_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL <> 0 
											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = 0))))
		)
	AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''

    if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
-- Case B: Include Collection in Reserve Balance <> 0 and Balance Amount < 0 and Paid_Total - Collection_Total >= 0
--		 : Include Collection in Reserve Balance = 0 and Collection in INCURRED <> 0 and Balance Amount < 0
	--SET @V_SQL1 = 'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.INCURRED_AMOUNT = 
 --   (RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL)
	--FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
 --   WHERE RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID AND CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE
 --   AND RESERVE_CURRENT.BALANCE_AMOUNT < 0.0
 --   AND (
	--		(((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
	--										AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
	--										WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 ))) AND RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL >= 0.0)
	--		OR
	--		((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0 )) AND ((SYS_PARMS_LOB.COLL_IN_INCUR_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL <> 0 
	--										AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
	--										WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = 0)))
	--	)
	--AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
 --   if @psnglclaims = 1  
	--BEGIN	
	--	SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
	--	exec sp_executesql @v_SQL
	--		,N'@pClaimID INT'
	--		,@pClaimID;
	--END	
	--ELSE
	--BEGIN
	--	SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
	--	set @v_tempminclid = @v_minclid;
	--	while  @v_tempminclid <= @v_maxclid 
	--	BEGIN
	--		IF (@v_maxclid - @v_tempminclid) > 1000  
	--		BEGIN
	--			SET @nextClaimId = @v_tempminclid + 1000;
	--			exec sp_executesql @v_SQL
	--				,N'@minClaimId INT, @maxClaimId INT'
	--				,@v_tempminclid, @nextClaimId ;
	--		END
	--		ELSE
	--		BEGIN
	--			exec sp_executesql @v_SQL
	--				,N'@minClaimId INT, @maxClaimId INT'
	--				,@v_tempminclid, @v_maxclid ;
	--		END
	--		SET @v_tempminclid = @v_tempminclid + 1001
	--	END
	--END
	SET @V_SQL1 = 'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.INCURRED_AMOUNT = 
    (RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL)
	FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
    WHERE RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID AND CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE
    AND RESERVE_CURRENT.BALANCE_AMOUNT < 0.0
    AND (
			(((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 ))))
			OR
			((SYS_PARMS_LOB.COLL_IN_RSV_BAL = 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL = 0 )) AND ((SYS_PARMS_LOB.COLL_IN_INCUR_BAL <> 0) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_INCR_BAL <> 0 
											AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
											WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = 0)))
		)
	AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
    if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
-- Case C: Include Collection in Reserve Balance <> 0 and Balance Amount >= 0 and Paid_Total - Collection_Total < 0
	--SET @V_SQL1 = 'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.INCURRED_AMOUNT = 
 --   RESERVE_CURRENT.BALANCE_AMOUNT 
	--FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
 --   WHERE RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID AND CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE
 --   AND RESERVE_CURRENT.BALANCE_AMOUNT >= 0.0 AND RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL < 0.0
	--AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0 ) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
	--										AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
	--										WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 )))
	--AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
 --   if @psnglclaims = 1  
	--BEGIN	
	--	SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
	--	exec sp_executesql @v_SQL
	--		,N'@pClaimID INT'
	--		,@pClaimID;
	--END	
	--ELSE
	--BEGIN
	--	SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
	--	set @v_tempminclid = @v_minclid;
	--	while  @v_tempminclid <= @v_maxclid 
	--	BEGIN
	--		IF (@v_maxclid - @v_tempminclid) > 1000  
	--		BEGIN
	--			SET @nextClaimId = @v_tempminclid + 1000;
	--			exec sp_executesql @v_SQL
	--				,N'@minClaimId INT, @maxClaimId INT'
	--				,@v_tempminclid, @nextClaimId ;
	--		END
	--		ELSE
	--		BEGIN
	--			exec sp_executesql @v_SQL
	--				,N'@minClaimId INT, @maxClaimId INT'
	--				,@v_tempminclid, @v_maxclid ;
	--		END
	--		SET @v_tempminclid = @v_tempminclid + 1001
	--	END
	--END
-- Case D: Include Collection in Reserve Balance <> 0 and Balance Amount < 0 and Paid_Total - Collection_Total < 0
	--SET @V_SQL1 = 'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.INCURRED_AMOUNT = 0
	--FROM RESERVE_CURRENT, CLAIM, SYS_PARMS_LOB
 --   WHERE RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID AND CLAIM.LINE_OF_BUS_CODE=SYS_PARMS_LOB.LINE_OF_BUS_CODE
 --   AND RESERVE_CURRENT.BALANCE_AMOUNT < 0.0 AND RESERVE_CURRENT.PAID_TOTAL - RESERVE_CURRENT.COLLECTION_TOTAL < 0.0
	--AND ((SYS_PARMS_LOB.COLL_IN_RSV_BAL <> 0 ) OR (SYS_PARMS_LOB.PER_RSV_COLL_IN_RSV_BAL <> 0 
	--										AND RESERVE_CURRENT.RESERVE_TYPE_CODE IN (SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL
	--										WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE=CLAIM.LINE_OF_BUS_CODE AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL = -1 )))
	--AND ( SELECT SHORT_CODE FROM CODES WHERE CODE_ID = (SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID= RESERVE_CURRENT.RESERVE_TYPE_CODE)) <> ''R'''
 --   if @psnglclaims = 1  
	--BEGIN	
	--	SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
	--	exec sp_executesql @v_SQL
	--		,N'@pClaimID INT'
	--		,@pClaimID;
	--END	
	--ELSE
	--BEGIN
	--	SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
	--	set @v_tempminclid = @v_minclid;
	--	while  @v_tempminclid <= @v_maxclid 
	--	BEGIN
	--		IF (@v_maxclid - @v_tempminclid) > 1000  
	--		BEGIN
	--			SET @nextClaimId = @v_tempminclid + 1000;
	--			exec sp_executesql @v_SQL
	--				,N'@minClaimId INT, @maxClaimId INT'
	--				,@v_tempminclid, @nextClaimId ;
	--		END
	--		ELSE
	--		BEGIN
	--			exec sp_executesql @v_SQL
	--				,N'@minClaimId INT, @maxClaimId INT'
	--				,@v_tempminclid, @v_maxclid ;
	--		END
	--		SET @v_tempminclid = @v_tempminclid + 1001
	--	END
	--END
	SET @p_sqlerrm=	@p_sqlerrm + dbo.FormatMessage('UPDATE INCURRED_AMOUNT ENDS');
END
----------------------------UPDATE INCURRED_AMOUNT ENDS----------------------------------------	
----------------------------UPDATE MULTICURRENCY COLUMNS STARTS---------------------------------------
IF EXISTS (SELECT NAME FROM SYS.COLUMNS WHERE NAME = N'CLAIM_CURR_CODE' AND OBJECT_ID = OBJECT_ID(N'RESERVE_CURRENT'))
BEGIN
	SET @IsMultiCurr = 1
END
ELSE
BEGIN
	SET @IsMultiCurr = 0
END
IF @IsMultiCurr = 1
BEGIN
	SET @V_SQL1 = 'UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.CLAIM_CURRENCY_RESERVE_AMOUNT = RESERVE_CURRENT.RESERVE_AMOUNT * ISNULL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE,0), 1)
, RESERVE_CURRENT.CLAIM_CURRENCY_INCURRED_AMOUNT = RESERVE_CURRENT.INCURRED_AMOUNT * ISNULL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE,0), 1)
, RESERVE_CURRENT.CLAIM_CURR_COLLECTION_TOTAL = RESERVE_CURRENT.COLLECTION_TOTAL * ISNULL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE,0), 1)
, RESERVE_CURRENT.CLAIM_CURRENCY_PAID_TOTAL = RESERVE_CURRENT.PAID_TOTAL * ISNULL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE,0), 1)
, RESERVE_CURRENT.CLAIM_CURRENCY_BALANCE_AMOUNT = RESERVE_CURRENT.BALANCE_AMOUNT * ISNULL(NULLIF(RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE,0), 1)
FROM RESERVE_CURRENT JOIN CLAIM ON CLAIM.CLAIM_ID = RESERVE_CURRENT.CLAIM_ID'
    if @psnglclaims = 1  
	BEGIN	
		SET @V_SQL = @V_SQL1 +' AND RESERVE_CURRENT.CLAIM_ID = @pClaimID';
		exec sp_executesql @v_SQL
			,N'@pClaimID INT'
			,@pClaimID;
	END	
	ELSE
	BEGIN
		SET @V_SQL= @V_SQL1 + ' AND RESERVE_CURRENT.CLAIM_ID BETWEEN @minClaimId AND @maxClaimId';
		set @v_tempminclid = @v_minclid;
		while  @v_tempminclid <= @v_maxclid 
		BEGIN
			IF (@v_maxclid - @v_tempminclid) > 1000  
			BEGIN
				SET @nextClaimId = @v_tempminclid + 1000;
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @nextClaimId ;
			END
			ELSE
			BEGIN
				exec sp_executesql @v_SQL
					,N'@minClaimId INT, @maxClaimId INT'
					,@v_tempminclid, @v_maxclid ;
			END
			SET @v_tempminclid = @v_tempminclid + 1001
		END
	END
END
----------------------------UPDATE MULTICURRENCY COLUMNS ENDS---------------------------------------
----------------------------UPDATE RESERVE_HISTORY STARTS--------------------------------------
	IF @IsMultiCurr = 0
	BEGIN 
		SET @v_SQL1 = 'DECLARE RESBAL_RH_INSERT CURSOR FOR SELECT RESERVE_CURRENT.BALANCE_AMOUNT, TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT
	, RESERVE_CURRENT.CLAIM_ID, RESERVE_CURRENT.CLAIMANT_EID, RESERVE_CURRENT.UNIT_ID, RESERVE_CURRENT.RESERVE_TYPE_CODE
	, RESERVE_CURRENT.RESERVE_AMOUNT, RESERVE_CURRENT.COLLECTION_TOTAL, RESERVE_CURRENT.INCURRED_AMOUNT, RESERVE_CURRENT.PAID_TOTAL
	, RESERVE_CURRENT.SEC_DEPT_EID, RESERVE_CURRENT.CRC
	, RESERVE_CURRENT.RES_STATUS_CODE FROM RESERVE_CURRENT
	JOIN TEMPRCCHANGEAMOUNT ON TEMPRCCHANGEAMOUNT.RC_ROW_ID = RESERVE_CURRENT.RC_ROW_ID
	WHERE ABS(TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT) > 0'
	END
	ELSE
	BEGIN 
		IF @IsCarrierOn <> '0' 
		BEGIN
		SET @v_SQL1 = 'DECLARE RESBAL_RH_INSERT CURSOR FOR SELECT RESERVE_CURRENT.BALANCE_AMOUNT, TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT, RESERVE_CURRENT.CLAIM_ID, RESERVE_CURRENT.CLAIMANT_EID
	, RESERVE_CURRENT.UNIT_ID, RESERVE_CURRENT.RESERVE_TYPE_CODE, RESERVE_CURRENT.RESERVE_AMOUNT, RESERVE_CURRENT.COLLECTION_TOTAL
	, RESERVE_CURRENT.INCURRED_AMOUNT, RESERVE_CURRENT.PAID_TOTAL
	, RESERVE_CURRENT.SEC_DEPT_EID, RESERVE_CURRENT.CRC, RESERVE_CURRENT.RES_STATUS_CODE 
	, RESERVE_CURRENT.CLAIM_CURR_CODE , RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE
	, RESERVE_CURRENT.POLCVG_LOSS_ROW_ID, RESERVE_CURRENT.IS_FIRST_FINAL, RESERVE_CURRENT.POLICY_CVG_SEQNO
	FROM RESERVE_CURRENT
	JOIN TEMPRCCHANGEAMOUNT ON TEMPRCCHANGEAMOUNT.RC_ROW_ID = RESERVE_CURRENT.RC_ROW_ID
	WHERE ABS(TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT) > 0'
		END
		ELSE
		BEGIN
		SET @v_SQL1 = 'DECLARE RESBAL_RH_INSERT CURSOR FOR SELECT RESERVE_CURRENT.BALANCE_AMOUNT, TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT, RESERVE_CURRENT.CLAIM_ID, RESERVE_CURRENT.CLAIMANT_EID
	, RESERVE_CURRENT.UNIT_ID, RESERVE_CURRENT.RESERVE_TYPE_CODE, RESERVE_CURRENT.RESERVE_AMOUNT, RESERVE_CURRENT.COLLECTION_TOTAL
	, RESERVE_CURRENT.INCURRED_AMOUNT, RESERVE_CURRENT.PAID_TOTAL
	, RESERVE_CURRENT.SEC_DEPT_EID, RESERVE_CURRENT.CRC, RESERVE_CURRENT.RES_STATUS_CODE 
	, RESERVE_CURRENT.CLAIM_CURR_CODE , RESERVE_CURRENT.BASE_TO_CLAIM_CUR_RATE
	FROM RESERVE_CURRENT
	JOIN TEMPRCCHANGEAMOUNT ON TEMPRCCHANGEAMOUNT.RC_ROW_ID = RESERVE_CURRENT.RC_ROW_ID
	WHERE ABS(TEMPRCCHANGEAMOUNT.CHANGE_AMOUNT) > 0'
		END
	END
	EXEC SP_EXECUTESQL @V_SQL1;
	OPEN RESBAL_RH_INSERT
	
	IF @IsMultiCurr = 0
	BEGIN
		FETCH NEXT FROM RESBAL_RH_INSERT INTO @BALANCEAMOUNT, @CHANGEAMOUNT 
		, @CLAIM_ID, @CLAIMANT_EID, @UNIT_ID, @RESERVE_TYPE_CODE
		, @RESERVE, @COLLECTION, @INCURRED, @PAID, @DEPTEID, @CRC, @RESSTATUSCODE
	END
	ELSE
		BEGIN
		IF @IsCarrierOn <> '0'
			BEGIN	
			FETCH NEXT FROM RESBAL_RH_INSERT INTO @BALANCEAMOUNT, @CHANGEAMOUNT 
			, @CLAIM_ID, @CLAIMANT_EID, @UNIT_ID, @RESERVE_TYPE_CODE
			, @RESERVE, @COLLECTION, @INCURRED, @PAID, @DEPTEID, @CRC, @RESSTATUSCODE
			, @CLAIMCURRCODE , @BASETOCLAIMCURRATE
			, @POLCVG_LOSS_ROW_ID, @ISFIRSTFINAL, @POLICY_CVG_SEQNO
			END
		ELSE
			BEGIN
			FETCH NEXT FROM RESBAL_RH_INSERT INTO @BALANCEAMOUNT, @CHANGEAMOUNT 
			, @CLAIM_ID, @CLAIMANT_EID, @UNIT_ID, @RESERVE_TYPE_CODE
			, @RESERVE, @COLLECTION, @INCURRED, @PAID, @DEPTEID, @CRC, @RESSTATUSCODE
			, @CLAIMCURRCODE , @BASETOCLAIMCURRATE
			END
		END

	WHILE(@@FETCH_STATUS <> -1)
		BEGIN
			SELECT @RSV_ROW_ID = NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'RESERVE_HISTORY';
			SELECT @DATE_ENTERED = CONVERT(VARCHAR(10), GETDATE(), 112);
			SELECT @DTTM_RCD_ADDED = REPLACE(REPLACE( REPLACE(CONVERT(VARCHAR(19), GETDATE(), 126), '-',''),'T',''),':','');
			SET @CLOSED_FLAG = -1;
			SET @AUTO_ADJ_FLAG = 0;
			 
			IF @IsMultiCurr = 0
			BEGIN
				SET @V_SQL = 'INSERT INTO RESERVE_HISTORY(
						  BALANCE_AMOUNT 
						, CHANGE_AMOUNT 
						, RSV_ROW_ID 
						, CLAIM_ID 
						, CLAIMANT_EID 
						, UNIT_ID 
						, RESERVE_TYPE_CODE
						, RESERVE_AMOUNT	
						, COLLECTION_TOTAL 
						, INCURRED_AMOUNT 
						, PAID_TOTAL 
						, DATE_ENTERED 
						, ENTERED_BY_USER 
						, REASON 
						, UPDATED_BY_USER
						, DTTM_RCD_ADDED	
						, DTTM_RCD_LAST_UPD 
						, ADDED_BY_USER 
						, CRC
						, SEC_DEPT_EID
						, RES_STATUS_CODE
						, CLOSED_FLAG
						, AUTO_ADJ_FLAG
						)
						VALUES(
						@BALANCEAMOUNT
						, @CHANGEAMOUNT 
						, @RSV_ROW_ID
						, @CLAIM_ID 
						, @CLAIMANT_EID
						, @UNIT_ID
						, @RESERVE_TYPE_CODE
						, @RESERVE
						, @COLLECTION
						, @INCURRED
						, @PAID
						, @DATE_ENTERED
						, @ENTERED_BY_USER
						, @REASON
						, @ENTERED_BY_USER
						, @DTTM_RCD_ADDED
						, @DTTM_RCD_ADDED
						, @ENTERED_BY_USER
						, @CRC
						, @DEPTEID
						, @RESSTATUSCODE
						, @CLOSED_FLAG
						, @AUTO_ADJ_FLAG)';
				
				exec sp_executesql @v_SQL
					,N'@BALANCEAMOUNT FLOAT
					, @CHANGEAMOUNT FLOAT
					, @RSV_ROW_ID INT
					, @CLAIM_ID INT
					, @CLAIMANT_EID INT
					, @UNIT_ID INT
					, @RESERVE_TYPE_CODE INT
					, @RESERVE FLOAT
					, @COLLECTION FLOAT
					, @INCURRED FLOAT
					, @PAID FLOAT
					, @DATE_ENTERED VARCHAR(8)
					, @ENTERED_BY_USER VARCHAR(50)
					, @REASON VARCHAR(30)
					, @DTTM_RCD_ADDED VARCHAR(14)
					, @CRC INT
					, @DEPTEID INT
					, @RESSTATUSCODE INT
					, @CLOSED_FLAG SMALLINT
					, @AUTO_ADJ_FLAG SMALLINT'
					, @BALANCEAMOUNT
					, @CHANGEAMOUNT
					, @RSV_ROW_ID
					, @CLAIM_ID
					, @CLAIMANT_EID
					, @UNIT_ID
					, @RESERVE_TYPE_CODE
					, @RESERVE
					, @COLLECTION
					, @INCURRED
					, @PAID
					, @DATE_ENTERED
					, @ENTERED_BY_USER
					, @REASON
					, @DTTM_RCD_ADDED
					, @CRC
					, @DEPTEID
					, @RESSTATUSCODE
					, @CLOSED_FLAG
					, @AUTO_ADJ_FLAG;
			END
			ELSE
			BEGIN
				SET @BASETOCLAIMCURRATE = ISNULL(NULLIF(@BASETOCLAIMCURRATE,0), 1);
				SET @CLAIM_CURRENCY_RESERVE_AMOUNT = @BASETOCLAIMCURRATE * @RESERVE;
				SET @CLAIM_CURRENCY_INCURRED_AMOUNT = @BASETOCLAIMCURRATE * @INCURRED;
				SET @CLAIM_CURR_COLLECTION_TOTAL = @BASETOCLAIMCURRATE * @COLLECTION;
				SET @CLAIM_CURRENCY_PAID_TOTAL = @BASETOCLAIMCURRATE * @PAID;
				SET @CLAIM_CURRENCY_BALANCE_AMOUNT = @BASETOCLAIMCURRATE * @BALANCEAMOUNT;
				IF @IsCarrierOn <> '0'
				BEGIN	
					
					SET @V_SQL = 'INSERT INTO RESERVE_HISTORY(
						  BALANCE_AMOUNT 
						, CHANGE_AMOUNT 
						, RSV_ROW_ID 
						, CLAIM_ID 
						, CLAIMANT_EID 
						, UNIT_ID 
						, RESERVE_TYPE_CODE
						, RESERVE_AMOUNT	
						, COLLECTION_TOTAL 
						, INCURRED_AMOUNT 
						, PAID_TOTAL 
						, DATE_ENTERED 
						, ENTERED_BY_USER 
						, REASON 
						, UPDATED_BY_USER
						, DTTM_RCD_ADDED	
						, DTTM_RCD_LAST_UPD 
						, ADDED_BY_USER 
						, CRC
						, SEC_DEPT_EID
						, RES_STATUS_CODE
						, CLOSED_FLAG
						, AUTO_ADJ_FLAG
						, CLAIM_CURR_CODE
						, CLAIM_TO_BASE_CUR_RATE
						, CLAIM_CURRENCY_RESERVE_AMOUNT
						, CLAIM_CURRENCY_INCURRED_AMOUNT
						, CLAIM_CURR_COLLECTION_TOTAL
						, CLAIM_CURRENCY_PAID_TOTAL
						, CLAIM_CURRENCY_BALANCE_AMOUNT
						, BASE_TO_CLAIM_CUR_RATE
						, POLICY_CVG_SEQNO
						, POLCVG_LOSS_ROW_ID
						, IS_FIRST_FINAL
						)VALUES(
						@BALANCEAMOUNT
						, @CHANGEAMOUNT 
						, @RSV_ROW_ID
						, @CLAIM_ID 
						, @CLAIMANT_EID
						, @UNIT_ID
						, @RESERVE_TYPE_CODE
						, @RESERVE
						, @COLLECTION
						, @INCURRED
						, @PAID
						, @DATE_ENTERED
						, @ENTERED_BY_USER
						, @REASON
						, @ENTERED_BY_USER
						, @DTTM_RCD_ADDED
						, @DTTM_RCD_ADDED
						, @ENTERED_BY_USER
						, @CRC
						, @DEPTEID
						, @RESSTATUSCODE
						, @CLOSED_FLAG
						, @AUTO_ADJ_FLAG 
						, @CLAIMCURRCODE
						, @BASETOCLAIMCURRATE
						, @CLAIM_CURRENCY_RESERVE_AMOUNT 
						, @CLAIM_CURRENCY_INCURRED_AMOUNT
						, @CLAIM_CURR_COLLECTION_TOTAL 
						, @CLAIM_CURRENCY_PAID_TOTAL
						, @CLAIM_CURRENCY_BALANCE_AMOUNT
						, @BASETOCLAIMCURRATE
						, @POLICY_CVG_SEQNO
						, @POLCVG_LOSS_ROW_ID
						, @ISFIRSTFINAL
						)';
				
				exec sp_executesql @v_SQL
					,N'@BALANCEAMOUNT FLOAT
					, @CHANGEAMOUNT FLOAT
					, @RSV_ROW_ID INT
					, @CLAIM_ID INT
					, @CLAIMANT_EID INT
					, @UNIT_ID INT
					, @RESERVE_TYPE_CODE INT
					, @RESERVE FLOAT
					, @COLLECTION FLOAT
					, @INCURRED FLOAT
					, @PAID FLOAT
					, @DATE_ENTERED VARCHAR(8)
					, @ENTERED_BY_USER VARCHAR(50)
					, @REASON VARCHAR(30)
					, @DTTM_RCD_ADDED VARCHAR(14)
					, @CRC INT
					, @DEPTEID INT
					, @RESSTATUSCODE INT
					, @CLOSED_FLAG SMALLINT
					, @AUTO_ADJ_FLAG SMALLINT
					, @CLAIMCURRCODE INT
					, @BASETOCLAIMCURRATE FLOAT
					, @CLAIM_CURRENCY_RESERVE_AMOUNT FLOAT
					, @CLAIM_CURRENCY_INCURRED_AMOUNT FLOAT
					, @CLAIM_CURR_COLLECTION_TOTAL FLOAT
					, @CLAIM_CURRENCY_PAID_TOTAL FLOAT
					, @CLAIM_CURRENCY_BALANCE_AMOUNT FLOAT
					, @POLICY_CVG_SEQNO INT
					, @POLCVG_LOSS_ROW_ID INT
					, @ISFIRSTFINAL SMALLINT'
					, @BALANCEAMOUNT
					, @CHANGEAMOUNT
					, @RSV_ROW_ID
					, @CLAIM_ID
					, @CLAIMANT_EID
					, @UNIT_ID
					, @RESERVE_TYPE_CODE
					, @RESERVE
					, @COLLECTION
					, @INCURRED
					, @PAID
					, @DATE_ENTERED
					, @ENTERED_BY_USER
					, @REASON
					, @DTTM_RCD_ADDED
					, @CRC
					, @DEPTEID
					, @RESSTATUSCODE
					, @CLOSED_FLAG
					, @AUTO_ADJ_FLAG
					, @CLAIMCURRCODE
					, @BASETOCLAIMCURRATE
					, @CLAIM_CURRENCY_RESERVE_AMOUNT
					, @CLAIM_CURRENCY_INCURRED_AMOUNT
					, @CLAIM_CURR_COLLECTION_TOTAL
					, @CLAIM_CURRENCY_PAID_TOTAL
					, @CLAIM_CURRENCY_BALANCE_AMOUNT
					, @POLICY_CVG_SEQNO 
					, @POLCVG_LOSS_ROW_ID
					, @ISFIRSTFINAL;
				END
			ELSE
			BEGIN
				SET @V_SQL = 'INSERT INTO RESERVE_HISTORY(
						  BALANCE_AMOUNT 
						, CHANGE_AMOUNT 
						, RSV_ROW_ID 
						, CLAIM_ID 
						, CLAIMANT_EID 
						, UNIT_ID 
						, RESERVE_TYPE_CODE
						, RESERVE_AMOUNT	
						, COLLECTION_TOTAL 
						, INCURRED_AMOUNT 
						, PAID_TOTAL 
						, DATE_ENTERED 
						, ENTERED_BY_USER 
						, REASON 
						, UPDATED_BY_USER
						, DTTM_RCD_ADDED	
						, DTTM_RCD_LAST_UPD 
						, ADDED_BY_USER 
						, CRC
						, SEC_DEPT_EID
						, RES_STATUS_CODE
						, CLOSED_FLAG
						, AUTO_ADJ_FLAG
						, CLAIM_CURR_CODE
						, CLAIM_TO_BASE_CUR_RATE
						, CLAIM_CURRENCY_RESERVE_AMOUNT
						, CLAIM_CURRENCY_INCURRED_AMOUNT
						, CLAIM_CURR_COLLECTION_TOTAL
						, CLAIM_CURRENCY_PAID_TOTAL
						, CLAIM_CURRENCY_BALANCE_AMOUNT
						, BASE_TO_CLAIM_CUR_RATE)
						VALUES(
						@BALANCEAMOUNT
						, @CHANGEAMOUNT 
						, @RSV_ROW_ID
						, @CLAIM_ID 
						, @CLAIMANT_EID
						, @UNIT_ID
						, @RESERVE_TYPE_CODE
						, @RESERVE
						, @COLLECTION
						, @INCURRED
						, @PAID
						, @DATE_ENTERED
						, @ENTERED_BY_USER
						, @REASON
						, @ENTERED_BY_USER
						, @DTTM_RCD_ADDED
						, @DTTM_RCD_ADDED
						, @ENTERED_BY_USER
						, @CRC
						, @DEPTEID
						, @RESSTATUSCODE
						, @CLOSED_FLAG
						, @AUTO_ADJ_FLAG 
						, @CLAIMCURRCODE
						, @BASETOCLAIMCURRATE
						, @CLAIM_CURRENCY_RESERVE_AMOUNT 
						, @CLAIM_CURRENCY_INCURRED_AMOUNT
						, @CLAIM_CURR_COLLECTION_TOTAL 
						, @CLAIM_CURRENCY_PAID_TOTAL
						, @CLAIM_CURRENCY_BALANCE_AMOUNT
						, @BASETOCLAIMCURRATE)';
				
				exec sp_executesql @v_SQL
					,N'@BALANCEAMOUNT FLOAT
					, @CHANGEAMOUNT FLOAT
					, @RSV_ROW_ID INT
					, @CLAIM_ID INT
					, @CLAIMANT_EID INT
					, @UNIT_ID INT
					, @RESERVE_TYPE_CODE INT
					, @RESERVE FLOAT
					, @COLLECTION FLOAT
					, @INCURRED FLOAT
					, @PAID FLOAT
					, @DATE_ENTERED VARCHAR(8)
					, @ENTERED_BY_USER VARCHAR(50)
					, @REASON VARCHAR(30)
					, @DTTM_RCD_ADDED VARCHAR(14)
					, @CRC INT
					, @DEPTEID INT
					, @RESSTATUSCODE INT
					, @CLOSED_FLAG SMALLINT
					, @AUTO_ADJ_FLAG SMALLINT
					, @CLAIMCURRCODE INT
					, @BASETOCLAIMCURRATE FLOAT
					, @CLAIM_CURRENCY_RESERVE_AMOUNT FLOAT
					, @CLAIM_CURRENCY_INCURRED_AMOUNT FLOAT
					, @CLAIM_CURR_COLLECTION_TOTAL FLOAT
					, @CLAIM_CURRENCY_PAID_TOTAL FLOAT
					, @CLAIM_CURRENCY_BALANCE_AMOUNT FLOAT'
					, @BALANCEAMOUNT
					, @CHANGEAMOUNT
					, @RSV_ROW_ID
					, @CLAIM_ID
					, @CLAIMANT_EID
					, @UNIT_ID
					, @RESERVE_TYPE_CODE
					, @RESERVE
					, @COLLECTION
					, @INCURRED
					, @PAID
					, @DATE_ENTERED
					, @ENTERED_BY_USER
					, @REASON
					, @DTTM_RCD_ADDED
					, @CRC
					, @DEPTEID
					, @RESSTATUSCODE
					, @CLOSED_FLAG
					, @AUTO_ADJ_FLAG
					, @CLAIMCURRCODE
					, @BASETOCLAIMCURRATE
					, @CLAIM_CURRENCY_RESERVE_AMOUNT
					, @CLAIM_CURRENCY_INCURRED_AMOUNT
					, @CLAIM_CURR_COLLECTION_TOTAL
					, @CLAIM_CURRENCY_PAID_TOTAL
					, @CLAIM_CURRENCY_BALANCE_AMOUNT;
				
				END
			END
		
		IF @IsMultiCurr = 0
		BEGIN
			FETCH NEXT FROM RESBAL_RH_INSERT INTO @BALANCEAMOUNT, @CHANGEAMOUNT 
			, @CLAIM_ID, @CLAIMANT_EID, @UNIT_ID, @RESERVE_TYPE_CODE
			, @RESERVE, @COLLECTION, @INCURRED, @PAID, @DEPTEID, @CRC, @RESSTATUSCODE
		END
		ELSE
		BEGIN
		IF @IsCarrierOn <> '0'
		BEGIN	
			FETCH NEXT FROM RESBAL_RH_INSERT INTO @BALANCEAMOUNT, @CHANGEAMOUNT 
			, @CLAIM_ID, @CLAIMANT_EID, @UNIT_ID, @RESERVE_TYPE_CODE
			, @RESERVE, @COLLECTION, @INCURRED, @PAID, @DEPTEID, @CRC, @RESSTATUSCODE
			, @CLAIMCURRCODE , @BASETOCLAIMCURRATE
			, @POLCVG_LOSS_ROW_ID, @ISFIRSTFINAL, @POLICY_CVG_SEQNO
		END
			ELSE
			BEGIN
				FETCH NEXT FROM RESBAL_RH_INSERT INTO @BALANCEAMOUNT, @CHANGEAMOUNT 
				, @CLAIM_ID, @CLAIMANT_EID, @UNIT_ID, @RESERVE_TYPE_CODE
				, @RESERVE, @COLLECTION, @INCURRED, @PAID, @DEPTEID, @CRC, @RESSTATUSCODE
				, @CLAIMCURRCODE , @BASETOCLAIMCURRATE
			END
		END
		
		UPDATE GLOSSARY SET NEXT_UNIQUE_ID = @RSV_ROW_ID + 1 WHERE SYSTEM_TABLE_NAME = 'RESERVE_HISTORY';
	END
	CLOSE RESBAL_RH_INSERT;
	DEALLOCATE  RESBAL_RH_INSERT;		
	
	IF OBJECT_ID('TEMPRCCHANGEAMOUNT', 'U') IS NOT NULL
	BEGIN
		SET @v_SQL1 = 'DROP TABLE TEMPRCCHANGEAMOUNT' 
		EXECUTE (@v_SQL1);
	END
----------------------------UPDATE RESERVE_HISTORY ENDS----------------------------------------
	SET @p_sqlerrm= @p_sqlerrm  + dbo.FormatMessage('SUCCESS');	
END
GO