[GO_DELIMITER]
DECLARE @spSQL AS nVARCHAR(max) 
IF EXISTS(SELECT TYPE, NAME FROM sys.objects WHERE type = 'P' AND name = 'USP_CREATE_TRIGGERS')
BEGIN
	DROP PROCEDURE USP_CREATE_TRIGGERS
END
	SET @spSQL = '
CREATE PROCEDURE USP_CREATE_TRIGGERS @sRm_USERID varchar(200),@iConfRec INT,@out_err varchar(500) OUTPUT
AS
DECLARE @nvSql NVARCHAR(4000)
begin try
	ALTER TABLE EVENT DISABLE TRIGGER ALL 
	ALTER TABLE PERSON_INVOLVED DISABLE TRIGGER ALL
	ALTER TABLE ENTITY DISABLE TRIGGER ALL
	ALTER TABLE EMPLOYEE DISABLE TRIGGER ALL
	ALTER TABLE CLAIM DISABLE TRIGGER ALL
	ALTER TABLE CLAIMANT DISABLE TRIGGER ALL
	ALTER TABLE FUNDS DISABLE TRIGGER ALL
	ALTER TABLE RESERVE_CURRENT DISABLE TRIGGER ALL
	ALTER TABLE RESERVE_HISTORY DISABLE TRIGGER ALL
	--pmittal5 Confidential Record
	IF(@iConfRec = -1)
	BEGIN
		ALTER TABLE FUNDS_AUTO DISABLE TRIGGER ALL
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''EVENT_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql =''DROP TRIGGER EVENT_INS''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''EVENT_UPD'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql =''DROP TRIGGER EVENT_UPD''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''CLAIM_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql =''DROP TRIGGER CLAIM_INS''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''CLAIM_UPD'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql =''DROP TRIGGER CLAIM_UPD''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''PERSON_INVOLVED_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER PERSON_INVOLVED_INS''
		Exec sp_executesql @nvSql
	END
	--pmittal5 - confidential record
	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''PERSON_INVOLVED_UPD'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER PERSON_INVOLVED_UPD''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''CLAIMANT_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER CLAIMANT_INS''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''FUNDS_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER FUNDS_INS''
		Exec sp_executesql @nvSql
	END
	--pmittal5 Confidential Record
	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''FUNDS_AUTO_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER FUNDS_AUTO_INS''
		Exec sp_executesql @nvSql
	END
	
	--End - pmittal5
	
	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''RESERVE_HISTORY_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER RESERVE_HISTORY_INS''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''RESERVE_CURRENT_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER RESERVE_CURRENT_INS''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''ENTITY_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER ENTITY_INS''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''EMPLOYEE_INS'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER EMPLOYEE_INS''
		Exec sp_executesql @nvSql
	END

	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''EMPLOYEE_UPD'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER EMPLOYEE_UPD''
		Exec sp_executesql @nvSql
	END
	
	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''ENTITY_TO_ENTITY_MAP'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER ENTITY_TO_ENTITY_MAP''
		Exec sp_executesql @nvSql
	END
	
	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME=''ORG_SECURITY_UPD'' AND XTYPE=''TR'')
	BEGIN
		Set @nvSql=''DROP TRIGGER ORG_SECURITY_UPD''
		Exec sp_executesql @nvSql
	END

	DECLARE @DB_TABLE_OWNER NVARCHAR(50)
	Select @DB_TABLE_OWNER=ss.name  from Sys.objects  so inner join sys.schemas ss On so.Schema_id = ss.Schema_id WHERE   type=''U'' and so.name =''CLAIM'' order by ss.Name

	Set @nvSql = ''CREATE TRIGGER EVENT_INS ON ''+@DB_TABLE_OWNER+''.EVENT FOR INSERT AS 
	IF UPDATE(DEPT_EID) 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.EVENT SET SEC_DEPT_EID = inserted.DEPT_EID FROM ''+@DB_TABLE_OWNER+''.EVENT,inserted WHERE inserted.EVENT_ID = ''+@DB_TABLE_OWNER+''.EVENT.EVENT_ID
	END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER EVENT_UPD ON ''+@DB_TABLE_OWNER+''.EVENT FOR UPDATE AS 
	IF UPDATE(DEPT_EID) 
	BEGIN 
		DECLARE @iCountEvTr  INT
		select @iCountEvTr= count(*) from deleted where deleted.dept_eid not in (select inserted.dept_eid from inserted)	
		if @iCountEvTr >0
		BEGIN 
			UPDATE ''+@DB_TABLE_OWNER+''.EVENT SET SEC_DEPT_EID = inserted.DEPT_EID FROM ''+@DB_TABLE_OWNER+''.EVENT,inserted WHERE ''+@DB_TABLE_OWNER+''.EVENT.EVENT_ID = inserted.EVENT_ID
			UPDATE ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED SET SEC_DEPT_EID = inserted.DEPT_EID FROM ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED,inserted WHERE inserted.EVENT_ID = ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED.EVENT_ID
			UPDATE ''+@DB_TABLE_OWNER+''.CLAIM SET SEC_DEPT_EID = inserted.DEPT_EID FROM ''+@DB_TABLE_OWNER+''.CLAIM,inserted WHERE inserted.EVENT_ID = ''+@DB_TABLE_OWNER+''.CLAIM.EVENT_ID
		END
	END 
--pmittal5 Confidential Record
	IF UPDATE(CONF_EVENT_ID) 
	BEGIN
		UPDATE ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED SET CONF_EVENT_ID = inserted.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED,inserted WHERE inserted.EVENT_ID = ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED.EVENT_ID
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET CONF_EVENT_ID=1 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID=inserted.DEPT_EID 
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET CONF_EVENT_ID=1 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID IN (SELECT FACILITY_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=inserted.DEPT_EID )
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET CONF_EVENT_ID=1 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID IN (SELECT LOCATION_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=inserted.DEPT_EID )
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET CONF_EVENT_ID=1 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID IN (SELECT DIVISION_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=inserted.DEPT_EID )
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET CONF_EVENT_ID=1 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID IN (SELECT REGION_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=inserted.DEPT_EID )
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET CONF_EVENT_ID=1 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID IN (SELECT OPERATION_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=inserted.DEPT_EID )
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET CONF_EVENT_ID=1 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID IN (SELECT COMPANY_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=inserted.DEPT_EID )
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET CONF_EVENT_ID=1 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID IN (SELECT CLIENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=inserted.DEPT_EID )
		UPDATE ''+@DB_TABLE_OWNER+''.CLAIM SET CONF_EVENT_ID = inserted.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.CLAIM,inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.EVENT_ID=inserted.EVENT_ID
	END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER CLAIM_INS ON ''+@DB_TABLE_OWNER+''.CLAIM FOR INSERT AS 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.CLAIM SET SEC_DEPT_EID = ''+@DB_TABLE_OWNER+''.EVENT.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.CLAIM,''+@DB_TABLE_OWNER+''.EVENT, inserted WHERE ''+@DB_TABLE_OWNER+''.EVENT.EVENT_ID = inserted.EVENT_ID AND inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID''
	--pmittal5 Confidential Record
		IF(@iConfRec = -1)
		BEGIN
			set @nvSql= @nvSql + '' UPDATE ''+@DB_TABLE_OWNER+''.CLAIM SET CONF_FLAG = ''+@DB_TABLE_OWNER+''.EVENT.CONF_FLAG,CONF_EVENT_ID = ''+@DB_TABLE_OWNER+''.EVENT.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.CLAIM,''+@DB_TABLE_OWNER+''.Event,inserted WHERE ''+@DB_TABLE_OWNER+''.EVENT.EVENT_ID = inserted.EVENT_ID AND inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID''
		END
	set @nvSql= @nvSql + '' END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER CLAIM_UPD ON ''+@DB_TABLE_OWNER+''.CLAIM FOR UPDATE AS 
	IF UPDATE(SEC_DEPT_EID) 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.CLAIMANT SET SEC_DEPT_EID = inserted.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.CLAIMANT,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.CLAIMANT.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.FUNDS SET SEC_DEPT_EID = inserted.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.FUNDS,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.FUNDS.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT SET SEC_DEPT_EID = inserted.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY SET SEC_DEPT_EID = inserted.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY.CLAIM_ID
	END 
	--pmittal5 Confidential Record
	IF UPDATE(CONF_EVENT_ID) 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.CLAIM SET CONF_EVENT_ID = inserted.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.CLAIM,''+@DB_TABLE_OWNER+''.EVENT,inserted WHERE ''+@DB_TABLE_OWNER+''.EVENT.EVENT_ID = inserted.EVENT_ID AND inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.CLAIMANT SET CONF_EVENT_ID = inserted.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.CLAIMANT,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.CLAIMANT.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.FUNDS SET CONF_EVENT_ID = inserted.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.FUNDS,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.FUNDS.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.FUNDS_AUTO SET CONF_EVENT_ID = inserted.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.FUNDS_AUTO,inserted WHERE ''+@DB_TABLE_OWNER+''.FUNDS_AUTO.CLAIM_ID=inserted.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT SET CONF_EVENT_ID = inserted.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY SET CONF_EVENT_ID = inserted.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY.CLAIM_ID
	END
	IF UPDATE(CONF_FLAG) 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.CLAIM SET CONF_FLAG = inserted.CONF_FLAG FROM ''+@DB_TABLE_OWNER+''.CLAIM,''+@DB_TABLE_OWNER+''.EVENT,inserted WHERE ''+@DB_TABLE_OWNER+''.EVENT.EVENT_ID = inserted.EVENT_ID AND inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.CLAIMANT SET CONF_FLAG = inserted.CONF_FLAG FROM ''+@DB_TABLE_OWNER+''.CLAIMANT,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.CLAIMANT.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.FUNDS SET CONF_FLAG = inserted.CONF_FLAG FROM ''+@DB_TABLE_OWNER+''.FUNDS,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.FUNDS.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.FUNDS_AUTO SET CONF_FLAG = inserted.CONF_FLAG FROM ''+@DB_TABLE_OWNER+''.FUNDS_AUTO,inserted WHERE ''+@DB_TABLE_OWNER+''.FUNDS_AUTO.CLAIM_ID=inserted.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT SET CONF_FLAG = inserted.CONF_FLAG FROM ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT.CLAIM_ID
		UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY SET CONF_FLAG = inserted.CONF_FLAG FROM ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY,inserted WHERE inserted.CLAIM_ID = ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY.CLAIM_ID
	END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER PERSON_INVOLVED_INS ON ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED FOR INSERT AS 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED SET SEC_DEPT_EID = ''+@DB_TABLE_OWNER+''.EVENT.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED,''+@DB_TABLE_OWNER+''.EVENT, inserted WHERE ''+@DB_TABLE_OWNER+''.EVENT.EVENT_ID = inserted.EVENT_ID AND inserted.PI_ROW_ID = ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED.PI_ROW_ID''
--pmittal5 Confidential Record
		IF(@iConfRec = -1)
		BEGIN
			set @nvSql = @nvSql + '' UPDATE ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED SET CONF_FLAG = ''+@DB_TABLE_OWNER+''.EVENT.CONF_FLAG,CONF_EVENT_ID = ''+@DB_TABLE_OWNER+''.EVENT.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED,''+@DB_TABLE_OWNER+''.EVENT, inserted WHERE ''+@DB_TABLE_OWNER+''.EVENT.EVENT_ID = inserted.EVENT_ID AND inserted.PI_ROW_ID = ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED.PI_ROW_ID''
		END
	set @nvSql= @nvSql + '' END ''
	Exec sp_executesql @nvSql

	IF(@iConfRec = -1)
	BEGIN
		set @nvSql=''CREATE TRIGGER PERSON_INVOLVED_UPD ON ''+@DB_TABLE_OWNER+''.PERSON_INVOLVED FOR UPDATE AS 
		IF UPDATE(CONF_EVENT_ID) 
		BEGIN 
			UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET CONF_EVENT_ID=1 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID=inserted.PI_EID AND ENTITY.ENTITY_TABLE_ID = 1060
		END ''
		Exec sp_executesql @nvSql
	END
--End - pmittal5

	set @nvSql=''CREATE TRIGGER CLAIMANT_INS ON ''+@DB_TABLE_OWNER+''.CLAIMANT FOR INSERT AS 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.CLAIMANT SET SEC_DEPT_EID = ''+@DB_TABLE_OWNER+''.CLAIM.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.CLAIMANT,''+@DB_TABLE_OWNER+''.CLAIM, inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID = inserted.CLAIM_ID AND inserted.CLAIMANT_ROW_ID = ''+@DB_TABLE_OWNER+''.CLAIMANT.CLAIMANT_ROW_ID''
--pmittal5 Confidential Record
		IF(@iConfRec = -1)
		BEGIN
			set @nvSql = @nvSql + '' UPDATE ''+@DB_TABLE_OWNER+''.CLAIMANT SET CONF_FLAG = ''+@DB_TABLE_OWNER+''.CLAIM.CONF_FLAG,CONF_EVENT_ID = ''+@DB_TABLE_OWNER+''.CLAIM.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.CLAIMANT,''+@DB_TABLE_OWNER+''.CLAIM, inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID = inserted.CLAIM_ID AND inserted.CLAIMANT_ROW_ID = ''+@DB_TABLE_OWNER+''.CLAIMANT.CLAIMANT_ROW_ID''
		END
	set @nvSql = @nvSql + '' END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER FUNDS_INS ON ''+@DB_TABLE_OWNER+''.FUNDS FOR INSERT AS 
	BEGIN 
	--MGaba2:MITS 22579
		IF (Select Count(*) FROM inserted where INSERTED.CLAIM_ID <> 0) > 0
			UPDATE ''+@DB_TABLE_OWNER+''.FUNDS SET SEC_DEPT_EID = ''+@DB_TABLE_OWNER+''.CLAIM.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.FUNDS,''+@DB_TABLE_OWNER+''.CLAIM, inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID = inserted.CLAIM_ID AND inserted.TRANS_ID = ''+@DB_TABLE_OWNER+''.FUNDS.TRANS_ID''
	--pmittal5 Confidential Record
			IF(@iConfRec = -1)
			BEGIN
				set @nvSql = @nvSql + '' UPDATE ''+@DB_TABLE_OWNER+''.FUNDS SET CONF_FLAG = ''+@DB_TABLE_OWNER+''.CLAIM.CONF_FLAG,CONF_EVENT_ID = ''+@DB_TABLE_OWNER+''.CLAIM.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.FUNDS,''+@DB_TABLE_OWNER+''.CLAIM, inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID = inserted.CLAIM_ID AND inserted.TRANS_ID = ''+@DB_TABLE_OWNER+''.FUNDS.TRANS_ID''
			END
	set @nvSql = @nvSql + '' END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER RESERVE_HISTORY_INS ON ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY FOR INSERT AS 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY SET SEC_DEPT_EID = ''+@DB_TABLE_OWNER+''.CLAIM.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY,''+@DB_TABLE_OWNER+''.CLAIM, inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID = inserted.CLAIM_ID AND inserted.RSV_ROW_ID = ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY.RSV_ROW_ID''
--pmittal5 Confidential Record
		IF(@iConfRec = -1)
		BEGIN
			set @nvSql = @nvSql + '' UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY SET CONF_FLAG = ''+@DB_TABLE_OWNER+''.CLAIM.CONF_FLAG,CONF_EVENT_ID = ''+@DB_TABLE_OWNER+''.CLAIM.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY,''+@DB_TABLE_OWNER+''.CLAIM, inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID = inserted.CLAIM_ID AND inserted.RSV_ROW_ID = ''+@DB_TABLE_OWNER+''.RESERVE_HISTORY.RSV_ROW_ID''
		END
	set @nvSql = @nvSql + '' END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER RESERVE_CURRENT_INS ON ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT FOR INSERT AS 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT SET SEC_DEPT_EID = ''+@DB_TABLE_OWNER+''.CLAIM.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT,''+@DB_TABLE_OWNER+''.CLAIM, inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID = inserted.CLAIM_ID AND inserted.RC_ROW_ID = ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT.RC_ROW_ID''
--pmittal5 Confidential Record
		IF(@iConfRec = -1)
		BEGIN
			set @nvSql = @nvSql + '' UPDATE ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT SET CONF_FLAG = ''+@DB_TABLE_OWNER+''.CLAIM.CONF_FLAG,CONF_EVENT_ID = ''+@DB_TABLE_OWNER+''.CLAIM.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT,''+@DB_TABLE_OWNER+''.CLAIM, inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID = inserted.CLAIM_ID AND inserted.RC_ROW_ID = ''+@DB_TABLE_OWNER+''.RESERVE_CURRENT.RC_ROW_ID''
		END
	set @nvSql = @nvSql + '' END ''
	Exec sp_executesql @nvSql

--pmittal5 Confidential Record
	IF(@iConfRec = -1)
	BEGIN
		set @nvSql=''CREATE TRIGGER FUNDS_AUTO_INS ON ''+@DB_TABLE_OWNER+''.FUNDS_AUTO FOR INSERT AS 
		BEGIN
			UPDATE ''+@DB_TABLE_OWNER+''.FUNDS_AUTO SET CONF_EVENT_ID = ''+@DB_TABLE_OWNER+''.CLAIM.CONF_EVENT_ID FROM ''+@DB_TABLE_OWNER+''.FUNDS_AUTO,''+@DB_TABLE_OWNER+''.CLAIM, inserted WHERE ''+@DB_TABLE_OWNER+''.CLAIM.CLAIM_ID = inserted.CLAIM_ID AND inserted.AUTO_TRANS_ID = ''+@DB_TABLE_OWNER+''.FUNDS_AUTO.AUTO_TRANS_ID
		END ''
		Exec sp_executesql @nvSql
	END

	set @nvSql=''CREATE TRIGGER ENTITY_INS ON ''+@DB_TABLE_OWNER+''.ENTITY FOR INSERT AS 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET SEC_DEPT_EID = ''+@DB_TABLE_OWNER+''.EMPLOYEE.SEC_DEPT_EID FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted,''+@DB_TABLE_OWNER+''.EMPLOYEE WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID = inserted.ENTITY_ID AND ''+@DB_TABLE_OWNER+''.EMPLOYEE.EMPLOYEE_EID = inserted.ENTITY_ID AND inserted.ENTITY_TABLE_ID = 1060
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET SEC_DEPT_EID = inserted.ENTITY_ID FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE inserted.ENTITY_ID = ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID AND inserted.ENTITY_TABLE_ID BETWEEN 1005 AND 1012
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET SEC_DEPT_EID = 0 FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID = inserted.ENTITY_ID AND inserted.ENTITY_TABLE_ID <> 1060 AND inserted.ENTITY_TABLE_ID NOT BETWEEN 1005 AND 1012
	END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER [ENTITY_TO_ENTITY_MAP] on [dbo].[ENTITY] AFTER INSERT 
				AS
				DECLARE @entity_id VARCHAR(100),
				@inserted_eid VARCHAR(100),
				@entity_tableid VARCHAR(100),
				@insertedentity_tableid VARCHAR(100),
				@parent_eid VARCHAR(100),
				@insertedparent_eid VARCHAR(100),
				@group_id VARCHAR(100),
				@Group_Entities VARCHAR(max),
				@nv_sql as nvarchar(max),
				@Count int

				SELECT	@entity_id = ENTITY_ID,
						@entity_tableid = ENTITY_TABLE_ID ,
						@parent_eid = PARENT_EID
				FROM inserted 
				WHERE  inserted.ENTITY_TABLE_ID BETWEEN 1005 AND 1012

				IF (@entity_id IS NOT NULL AND @entity_id <> '''''''')
				BEGIN
					SET @inserted_eid = @entity_id
					SET @insertedentity_tableid = @entity_tableid
					SET @insertedparent_eid = @parent_eid

					-- Declare the Cursor and Loop through All the Groups having entry in the Org_Security 
					DECLARE GROUPIDCURSOR CURSOR FOR 
					SELECT	GROUP_ID , GROUP_ENTITIES
					FROM	ORG_SECURITY WHERE GROUP_ENTITIES <> ''''<ALL>''''


					
					OPEN	GROUPIDCURSOR
					FETCH	NEXT 
					FROM	GROUPIDCURSOR INTO @group_id, @Group_Entities
				 
					WHILE	@@FETCH_STATUS = 0 
					BEGIN
						IF( @group_id IS NOT NULL )
						BEGIN
							
							WHILE(@entity_tableid >1005)
							BEGIN
								SET @entity_id = @parent_eid			
								SET @Count = 0
								Select @Group_Entities = Group_entities from Org_security  Where Group_id = @group_id
								--To check if entity_id is in Group_Entities for particular group_id
								SET @nv_sql = ''''SELECT  @Count = COUNT(*)  FROM ORG_SECURITY INNER JOIN ENTITY_MAP
											ON ORG_SECURITY.GROUP_ID = ENTITY_MAP.GROUP_ID WHERE ENTITY_MAP.ENTITY_ID  IN ('''' + @Group_Entities + '''')  AND ORG_SECURITY.GROUP_ID = ''''+@group_id +
											'''' AND ENTITY_MAP.ENTITY_ID = '''' +@entity_id 

								Exec sp_executesql @nv_sql,N''''@Count INT OUTPUT'''',@Count OUTPUT

								--If entity_id exist in group_entities , insert a new record for this group id 
								IF @Count >0
								BEGIN
									
									INSERT INTO ENTITY_MAP(ENTITY_ID,GROUP_ID) VALUES (@inserted_eid, @group_id)
									BREAK 
								END
								--Othewise fetch the parent_eid of this entity and repeat the process. 
								SELECT @parent_eid = PARENT_EID,@entity_tableid = ENTITY_TABLE_ID
								FROM ENTITY 
								WHERE  ENTITY_ID = @entity_id
							END
						END
					FETCH NEXT FROM GROUPIDCURSOR INTO @group_id, @Group_Entities
					--reset values of entity_id , entity_tableid and parent_eid to initial values
					SET @entity_id = @inserted_eid 
					SET @entity_tableid  = @insertedentity_tableid 
					SET @parent_eid  = @insertedparent_eid
					END
					CLOSE GROUPIDCURSOR
					DEALLOCATE GROUPIDCURSOR
				END''



	Exec sp_executesql @nvSql


	set @nvSql=''CREATE TRIGGER EMPLOYEE_INS ON ''+@DB_TABLE_OWNER+''.EMPLOYEE FOR INSERT AS 
	BEGIN 
		UPDATE ''+@DB_TABLE_OWNER+''.EMPLOYEE SET ''+@DB_TABLE_OWNER+''.EMPLOYEE.SEC_DEPT_EID = inserted.DEPT_ASSIGNED_EID FROM ''+@DB_TABLE_OWNER+''.EMPLOYEE,inserted WHERE ''+@DB_TABLE_OWNER+''.EMPLOYEE.EMPLOYEE_EID = inserted.EMPLOYEE_EID
		UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET SEC_DEPT_EID = inserted.DEPT_ASSIGNED_EID FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE inserted.EMPLOYEE_EID = ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID
	END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER EMPLOYEE_UPD ON ''+@DB_TABLE_OWNER+''.EMPLOYEE FOR UPDATE AS 
	IF UPDATE(DEPT_ASSIGNED_EID) 
	BEGIN
		declare @iCountEmpTr  INT
		select @iCountEmpTr= count(*) from deleted where deleted.DEPT_ASSIGNED_EID not in (select inserted.DEPT_ASSIGNED_EID from inserted)	
		if @iCountEmpTr >0
		BEGIN 
			UPDATE ''+@DB_TABLE_OWNER+''.EMPLOYEE SET SEC_DEPT_EID = inserted.DEPT_ASSIGNED_EID FROM ''+@DB_TABLE_OWNER+''.EMPLOYEE,inserted WHERE ''+@DB_TABLE_OWNER+''.EMPLOYEE.EMPLOYEE_EID = inserted.EMPLOYEE_EID
			UPDATE ''+@DB_TABLE_OWNER+''.ENTITY SET SEC_DEPT_EID = inserted.DEPT_ASSIGNED_EID FROM ''+@DB_TABLE_OWNER+''.ENTITY,inserted WHERE inserted.EMPLOYEE_EID = ''+@DB_TABLE_OWNER+''.ENTITY.ENTITY_ID
		END	
	END ''
	Exec sp_executesql @nvSql

	set @nvSql=''CREATE TRIGGER ORG_SECURITY_UPD ON ''+@DB_TABLE_OWNER+''.ORG_SECURITY FOR UPDATE AS 
		DECLARE @iCountOrgSecTr  INT
				select @iCountOrgSecTr= count(*) 
				from deleted, inserted						
				where deleted.GROUP_ID =inserted.GROUP_ID 
				AND (deleted.GROUP_ADJ_TEXT_TYPES != inserted.GROUP_ADJ_TEXT_TYPES 
				OR deleted.GROUP_ENH_NOTES_TYPES != inserted.GROUP_ENH_NOTES_TYPES
				OR deleted.GROUP_INSURERS != inserted.GROUP_INSURERS
				OR deleted.GROUP_BROKERS != inserted.GROUP_BROKERS	
				OR deleted.GROUP_ADJ_TEXT_TYPES IS NULL AND  inserted.GROUP_ADJ_TEXT_TYPES IS NOT NULL
				OR deleted.GROUP_ADJ_TEXT_TYPES IS NOT NULL AND  inserted.GROUP_ADJ_TEXT_TYPES IS NULL
				OR deleted.GROUP_ENH_NOTES_TYPES IS NULL AND  inserted.GROUP_ENH_NOTES_TYPES IS NOT NULL
				OR deleted.GROUP_ENH_NOTES_TYPES IS NOT NULL AND  inserted.GROUP_ENH_NOTES_TYPES IS NULL
				OR deleted.GROUP_INSURERS IS NULL AND  inserted.GROUP_INSURERS IS NOT NULL
				OR deleted.GROUP_INSURERS IS NOT NULL AND  inserted.GROUP_INSURERS IS NULL
				OR deleted.GROUP_BROKERS IS NULL AND  inserted.GROUP_BROKERS IS NOT NULL
				OR deleted.GROUP_BROKERS IS NOT NULL AND  inserted.GROUP_BROKERS IS NULL)	
		if @iCountOrgSecTr >0
		BEGIN 
			UPDATE ''+@DB_TABLE_OWNER+''.GROUP_MAP SET ''+@DB_TABLE_OWNER+''.GROUP_MAP.UPDATED_FLAG =-1,''+@DB_TABLE_OWNER+''.GROUP_MAP.STATUS_FLAG =0  FROM ''+@DB_TABLE_OWNER+''.GROUP_MAP,inserted WHERE ''+@DB_TABLE_OWNER+''.GROUP_MAP.SUPER_GROUP_ID = inserted.GROUP_ID
		END ''
	Exec sp_executesql @nvSql

	ALTER TABLE CLAIM ENABLE TRIGGER ALL
	ALTER TABLE CLAIMANT ENABLE TRIGGER ALL
	ALTER TABLE EMPLOYEE ENABLE TRIGGER ALL
	ALTER TABLE ENTITY ENABLE TRIGGER ALL
	ALTER TABLE EVENT ENABLE TRIGGER ALL
	ALTER TABLE funds ENABLE TRIGGER ALL
	ALTER TABLE PERSON_INVOLVED ENABLE TRIGGER ALL
	ALTER TABLE RESERVE_CURRENT ENABLE TRIGGER ALL
	ALTER TABLE RESERVE_HISTORY ENABLE TRIGGER ALL
	--pmittal5 Confidential Record
	IF(@iConfRec = -1)
	BEGIN
		ALTER TABLE FUNDS_AUTO ENABLE TRIGGER ALL
	END
end try
begin catch
	SELECT	@out_err = error_message()
end catch
	'
	EXECUTE sp_executesql @spSQL
GO