-- =============================================
-- Author:		CSC
-- Create date: 06/04/2010
-- Description:	Copies Data from Supp_dictionary To Hist_Track_Dictionary
-- =============================================
[GO_DELIMITER]
CREATE OR REPLACE
PROCEDURE USP_SUPP_DICT_TO_HIST_DICT
AS 
BEGIN 
    DECLARE 
         sSQL VARCHAR2(2000);         	 
         iMinID INTEGER;
         iMaxID INTEGER;
         iLoopCount INTEGER;
         iFLAG INTEGER;         
            BEGIN            
				BEGIN
					sSQL:='SELECT MIN(FIELD_ID) FROM SUPP_DICTIONARY';
					 EXECUTE IMMEDIATE sSQL INTO iMinID;
					
					sSQL:='SELECT MAX(FIELD_ID) FROM SUPP_DICTIONARY';
					EXECUTE IMMEDIATE sSQL INTO iMaxID;
				 END;  
				 BEGIN 
					iLoopCount:=iMinID;
					WHILE iLoopCount<=iMaxID
					LOOP
						sSQL:='SELECT COUNT(*) FROM SUPP_DICTIONARY  WHERE FIELD_ID BETWEEN  '||iLoopCount ||' AND ('||iLoopCount||' + 4999)';
								 EXECUTE IMMEDIATE sSQL INTO iFLAG;
						IF iFLAG > 0 THEN 
							sSQL:='INSERT INTO HIST_TRACK_DICTIONARY(COLUMN_ID,TABLE_ID,COLUMN_NAME,USER_PROMPT,RMDATA_TYPE,PRIMARY_KEY_FLAG,SYSTEM_TABLE_NAME,DATATYPE,NUMERIC_PRECISION,COLUMN_SIZE)
								 (SELECT HIST_TRACK_DICTIONARY_SEQ.NEXTVAL, HTT.TABLE_ID,
									UPPER(SD.SYS_FIELD_NAME),
									CASE WHEN SD.USER_PROMPT IS NOT NULL THEN SD.USER_PROMPT
										 ELSE SD.SYS_FIELD_NAME
										 END,
									SD.FIELD_TYPE,
									CASE WHEN FIELD_TYPE=7 THEN -1 
										 ELSE 0 
										 END,
									CASE WHEN SD.CODE_FILE_ID <> 0 THEN G.SYSTEM_TABLE_NAME
										 ELSE NULL 
										 END,
									CASE WHEN FIELD_TYPE IN (0,3,4,10,12,13) THEN  ''VARCHAR2''
										 WHEN FIELD_TYPE IN (1,2,6,7,8,9,14,15,16) THEN  ''NUMBER''	
									END,
									CASE WHEN FIELD_TYPE IN (6,7,8,9,14,15,16) THEN  10
										 ELSE 0
										 END,
									SD.FIELD_SIZE
									FROM SUPP_DICTIONARY SD INNER JOIN HIST_TRACK_TABLES HTT
									 ON HTT.TABLE_NAME = SD.SUPP_TABLE_NAME 
									INNER JOIN GLOSSARY G ON SD.CODE_FILE_ID=G.TABLE_ID
									WHERE SD.FIELD_TYPE NOT IN (5,11,17) 
									 AND SD.DELETE_FLAG=0
									 AND SD.FIELD_ID >= ' || iLoopCount || ' AND SD.FIELD_ID <= (' || iLoopCount || ' + 4999))';
								
							EXECUTE IMMEDIATE sSQL;
						END IF;
						iLoopCount:=iLoopCount+5000;
					END LOOP;
					COMMIT;
                END;  
            END;
END;

[EXECUTE_SP]
GO
