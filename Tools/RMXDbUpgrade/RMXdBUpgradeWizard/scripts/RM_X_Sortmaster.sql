;***********************************************************************************
; DB Upgrade Script for SORTMASTER Database
;***********************************************************************************
;OSHA enhancement - Adding new column in SM_REPORTS table
[SQL Server]	[FORGIVE_ALTER] ALTER TABLE SM_REPORTS ADD PRIVACY_CASE SMALLINT  NULL
[Oracle]	[FORGIVE_ALTER] ALTER TABLE SM_REPORTS ADD PRIVACY_CASE NUMBER(5) NULL