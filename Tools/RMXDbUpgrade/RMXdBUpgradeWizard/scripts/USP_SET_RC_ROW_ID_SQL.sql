[GO_DELIMITER]
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'USP_SET_RC_ROW_ID')
BEGIN
	DROP PROCEDURE USP_SET_RC_ROW_ID
END


DECLARE @spSQL AS nVARCHAR(max) 
SET @spSQL = '
CREATE PROCEDURE USP_SET_RC_ROW_ID  @splitTableName VARCHAR(50),@fundsTableName VARCHAR(50),@splitPrimaryKey VARCHAR(50),@fundsPrimaryKey VARCHAR(50)
				  AS
				  BEGIN
					-- SET NOCOUNT ON added to prevent extra result sets from
					-- interfering with SELECT statements.
					SET NOCOUNT ON;

					
					DECLARE @iMinID INT,
							@iMaxID INT,
							@iLoopCount INT,
							@iFLAG INT,
							@iRC_ROW_ID INT,
							@iSplit_Row_id INT,
							@iCount INT,
							@sQuery nvarchar(1000)
					BEGIN
						
						 						 
						 SET @sQuery = ''SELECT @ret = COUNT(*) FROM '' + @splitTableName + '' WHERE RC_ROW_ID IS NOT NULL''              
						 exec sp_executesql @sQuery, N''@ret int OUTPUT'', @iCount OUTPUT;
						 IF @iCount > 1 
						 BEGIN
						   RETURN;
						 END 					
					
						SET @sQuery = ''SELECT @ret = MIN('' +  @splitPrimaryKey + '' ) FROM ''+ @splitTableName				 
						exec sp_executesql @sQuery, N''@ret int OUTPUT'', @iMinID OUTPUT;
						
						
						SET @sQuery = ''SELECT @ret = MAX('' +  @splitPrimaryKey + '' ) FROM ''+ @splitTableName	
						exec sp_executesql @sQuery, N''@ret int OUTPUT'', @iMaxID OUTPUT;		
						
						
						--creating temp table
					
						
						
						SET @sQuery = ''SELECT  FTS.''  + @splitPrimaryKey + '',RC.RC_ROW_ID INTO  RCROWIDTEMP FROM RESERVE_CURRENT RC INNER JOIN '' + @fundsTableName + '' F
						ON RC.CLAIM_ID = F.CLAIM_ID AND RC.CLAIMANT_EID = F.CLAIMANT_EID AND RC.UNIT_ID = F.UNIT_ID 
						INNER JOIN '' + @splitTableName + ''  FTS
						ON  F.'' + @fundsPrimaryKey + ''= FTS.''+ @fundsPrimaryKey + '' AND RC.RESERVE_TYPE_CODE = FTS.RESERVE_TYPE_CODE''
						
						exec sp_executesql @sQuery
						--Updating chunks of temp table
						SET @iLoopCount = @iMinID
						WHILE @iLoopCount <= @iMaxID
						BEGIN
								
								
							SET @sQuery = ''SELECT  @ret  = COUNT(*)  FROM  RCROWIDTEMP  WHERE '' + @splitPrimaryKey + '' BETWEEN '' + cast(@iLoopCount as varchar(20)) + '' AND '' + cast((@iLoopCount + 4999) as varchar(20))  							 
							exec sp_executesql @sQuery, N''@ret int OUTPUT'', @iFLAG OUTPUT;	
							
							IF (@iFLAG > 0 )
							BEGIN							
								SET @sQuery = ''DECLARE RCROWIDCURSOR CURSOR FOR SELECT '' + @splitPrimaryKey + '',RC_ROW_ID FROM  RCROWIDTEMP WHERE ''+ @splitPrimaryKey + '' >= ''+ cast(@iLoopCount as varchar(20)) + '' AND ''+ @splitPrimaryKey + ''<='' + cast((@iLoopCount + 4999) as varchar(20))  								
								exec sp_executesql @sQuery
								OPEN RCROWIDCURSOR																
								FETCH NEXT FROM RCROWIDCURSOR INTO @iSplit_Row_id, @iRC_ROW_ID								
								WHILE @@FETCH_STATUS = 0
								BEGIN								
									SET @sQuery = ''UPDATE ''+ @splitTableName + '' SET RC_ROW_ID = '' + cast(@iRC_ROW_ID as varchar(20)) + '' WHERE ''+ @splitPrimaryKey + '' = '' + cast(@iSplit_Row_id as varchar(20))									
									exec sp_executesql @sQuery																		
									FETCH NEXT FROM RCROWIDCURSOR INTO @iSplit_Row_id, @iRC_ROW_ID
								END
								CLOSE RCROWIDCURSOR
								DEALLOCATE RCROWIDCURSOR	
							END
							SET @iLoopCount = @iLoopCount + 5000
						END	
		
						IF OBJECT_ID(''RCROWIDTEMP'') IS NOT NULL 
						BEGIN 						
							DROP TABLE RCROWIDTEMP							
						END 	
					END						
				  END	
				  			  
				  '
				  EXECUTE sp_executesql @spSQL
				  GO
				  
				  
				 
				  