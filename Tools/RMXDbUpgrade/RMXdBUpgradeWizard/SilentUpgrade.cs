﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Text;

namespace RMXdBUpgradeWizard
{
    class SilentUpgrade
    {
        private static bool _isCustom;
        private static bool _isHPUprade;

        private static bool _sDbchkMangeInd;
        private static bool _sDbchkUpgradeDB;
        //private static string _secConnString;
        //private static string _secProvider;
        private static string _errorCache;
        private static NameValueConfigurationCollection nvc = new NameValueConfigurationCollection();

        /// <summary>
        /// Validation
        /// </summary>
        /// <returns></returns>
        public static bool Validation()
        {
            string sDbUpgradeCustom = ConfigurationManager.AppSettings["DBUPGRADE_CUSTOM"].ToString().ToUpper();
            _isCustom = (sDbUpgradeCustom == bool.TrueString.ToUpper()) ? true : false;
            //igupta3
            //string sDbUpgradeHP = ConfigurationManager.AppSettings["DBUPGRADE_HP"].ToString().ToUpper();
            string sDbUpgradeHP = frmWizard._isHPUpgrade.ToString().ToUpper();
            _isHPUprade = (sDbUpgradeHP == bool.TrueString.ToUpper()) ? true : false;
            //Govind - start
            string sDbchkMangeInd = frmWizard._sDbchkMangeIndex.ToString().ToUpper();
            _sDbchkMangeInd = (sDbchkMangeInd == bool.TrueString.ToUpper()) ? true : false;

            string sDbchkUpgradeDB = frmWizard._sDbchkMangeIndex.ToString().ToUpper();
            _sDbchkUpgradeDB = (sDbchkUpgradeDB == bool.TrueString.ToUpper()) ? true : false;
            //Govind - End

            //process validations if appropriate
            if (!DisplayDBUpgrade.ValidateStartup(_isCustom))
            {
                string sErrorMessage = "The Database Upgrade Program cannot be started. Likely causes " +
                                       "include an incorrect folder structure or missing dependencies.\n\n" +
                                       "Please check dBUpgrade.log file for errors and " +
                                       "contact RISKMASTER Support.\n\nPress Cancel to close program.";
                Console.Error.WriteLine(sErrorMessage);
                return false;
            }
            else
            {
                //Console.WriteLine("validation passed...");

                DisplayDBUpgrade.g_SessionConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSession);

                if (LoadDSNs(String.Empty))
                {
                    //Console.WriteLine("load dsn's from security db passed...");
                    bool bDSNfound = false;
                    bool bListDSN = false;
                    int iDSNid = 0;
                    string sDSN = String.Empty;
                    string[] args = Environment.GetCommandLineArgs();

                    foreach (string arg in args)
                    {
                        if (bDSNfound)
                        {
                            sDSN = arg;
                            break;
                        }

                        if (arg.ToUpper() == "LIST" || arg.ToUpper() == "-LIST")
                        {
                            bListDSN = true;
                            break;
                        }

                        if (arg.ToUpper() == "DSN" || arg.ToUpper() == "-DSN")
                        {
                            bDSNfound = true;
                        }
                    }

                    if (String.IsNullOrEmpty(sDSN) && !bListDSN)
                    {
                        Console.Error.WriteLine("no DSN was passed as an ARG... (app.exe -dsn DsnName)");
                        return false;
                    }

                    if (!bListDSN)
                    {
                        //Console.WriteLine("DSN: " + sDSN);
                    }

                    DisplayDBUpgrade.dictPassedItems.Clear();

                    foreach (NameValueConfigurationElement item in nvc)
                    {
                        if (bListDSN)
                        {
                            Console.WriteLine(item.Name);
                        }

                        if (sDSN == item.Name)
                        {
                            //Console.WriteLine("DSN found in the security DB...");
                            int.TryParse(item.Value, out iDSNid);
                            DisplayDBUpgrade.dictPassedItems.Add(sDSN, iDSNid);
                            break;
                        }
                    }

                    if (bListDSN)
                    {
                        return false;
                    }

                    //try connection
                    DisplayDBUpgrade.bAborted = DisplayDBUpgrade.SelectDatabase(iDSNid, ref _errorCache);

                    //handle failed connection
                    if (!DisplayDBUpgrade.bAborted)
                    {
                        Console.Error.WriteLine("connection to dsn failed...");
                        return false;
                    }
                    else
                    {
                        //Console.WriteLine("connection to dsn passed...");
                        UpgradeScripts.UpgradeRMDatabase(_isCustom, _isHPUprade, true);
                    }
                }
                else
                {
                    Console.Error.WriteLine(_errorCache);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// load the database list
        /// </summary>
        private static bool LoadDSNs(string sConnString)
        {
            DataTable dtListItems = new DataTable();

            dtListItems.Columns.Add(DisplayDBUpgrade.strDSNColumn);
            dtListItems.Columns.Add(DisplayDBUpgrade.strDSNIDColumn);

            try
            {
                if (String.IsNullOrEmpty(sConnString))
                {
                    DisplayDBUpgrade.g_SecConnectString = DisplayDBUpgrade.GetDbUpgradeConnectionStrings(DbUpgradeConnectionStrings.RMSecurity);
                }
                else
                {
                    DisplayDBUpgrade.g_SecConnectString = sConnString;
                }

                using (var drDSNs = ADONetDbAccess.ExecuteReader(DisplayDBUpgrade.g_SecConnectString, RISKMASTERScripts.GetDSNs()))
                {
                    while (drDSNs.Read())
                    {
                        var strKey = drDSNs[DisplayDBUpgrade.strDSNColumn].ToString();
                        var strValue = drDSNs[DisplayDBUpgrade.strDSNIDColumn].ToString();

                        NameValueConfigurationElement nvcE = new NameValueConfigurationElement(strKey, strValue);
                        nvc.Add(nvcE);
                    }
                }

                if (nvc.Count <= 0)
                {
                    _errorCache = "An error has occurred during population of DSNs.\n\n" +
                                  "There are no datasources configured in the specified security database. " +
                                  "Please correct and try again later.";

                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                _errorCache = "An error has occurred during population of DSNs.\n\n" +
                              "This is usually caused by not being able to connect to the security database, " +
                              "please double check connection information before trying again.";
                string sNull = ex.Message;

                return false;
            }
        }
    }
}
