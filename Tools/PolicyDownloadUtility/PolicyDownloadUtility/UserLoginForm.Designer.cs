﻿namespace PolicyDownloadUtility
{
    partial class frmUserLoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUserNm = new System.Windows.Forms.Label();
            this.txtUserNm = new System.Windows.Forms.TextBox();
            this.cbDSNNm = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblUserNm
            // 
            this.lblUserNm.AutoSize = true;
            this.lblUserNm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserNm.Location = new System.Drawing.Point(108, 71);
            this.lblUserNm.Name = "lblUserNm";
            this.lblUserNm.Size = new System.Drawing.Size(69, 13);
            this.lblUserNm.TabIndex = 0;
            this.lblUserNm.Text = "User Name";
            // 
            // txtUserNm
            // 
            this.txtUserNm.Location = new System.Drawing.Point(191, 70);
            this.txtUserNm.Name = "txtUserNm";
            this.txtUserNm.Size = new System.Drawing.Size(171, 20);
            this.txtUserNm.TabIndex = 1;
            this.txtUserNm.Text = "csc";
            // 
            // cbDSNNm
            // 
            this.cbDSNNm.FormattingEnabled = true;
            this.cbDSNNm.Location = new System.Drawing.Point(190, 117);
            this.cbDSNNm.Name = "cbDSNNm";
            this.cbDSNNm.Size = new System.Drawing.Size(172, 21);
            this.cbDSNNm.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(108, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "DSN Name";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(169, 203);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 6;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // frmUserLoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 359);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbDSNNm);
            this.Controls.Add(this.txtUserNm);
            this.Controls.Add(this.lblUserNm);
            this.Name = "frmUserLoginForm";
            this.Text = "UserLoginForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUserNm;
        private System.Windows.Forms.TextBox txtUserNm;
        private System.Windows.Forms.ComboBox cbDSNNm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnNext;
    }
}