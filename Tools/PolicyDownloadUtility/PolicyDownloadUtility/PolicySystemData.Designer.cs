﻿using System.Windows.Forms;
namespace PolicyDownloadUtility
{
    partial class frmPolicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            Application.Exit(); // aaggarwal29: Added since application was not exiting on closing the second form by "X" button.
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPolicy));
            this.cbPolicySysID = new System.Windows.Forms.ComboBox();
            this.lblPolicySysNm = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.pnlDownload = new System.Windows.Forms.Panel();
            this.rtbStatus = new System.Windows.Forms.RichTextBox();
            this.pnlUserInput = new System.Windows.Forms.Panel();
            this.pnlImage = new System.Windows.Forms.Panel();
            this.lblError = new System.Windows.Forms.Label();
            this.lblHelpMsg = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.txtUpperIndex = new System.Windows.Forms.TextBox();
            this.txtLowerIndex = new System.Windows.Forms.TextBox();
            this.lblRange = new System.Windows.Forms.Label();
            this.btnStartProcessing = new System.Windows.Forms.Button();
            this.lblSrcPath = new System.Windows.Forms.Label();
            this.lbMsg = new System.Windows.Forms.Label();
            this.pbOverall = new System.Windows.Forms.ProgressBar();
            this.pbStatus = new System.Windows.Forms.PictureBox();
            this.pnlDownload.SuspendLayout();
            this.pnlUserInput.SuspendLayout();
            this.pnlImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // cbPolicySysID
            // 
            this.cbPolicySysID.FormattingEnabled = true;
            this.cbPolicySysID.Location = new System.Drawing.Point(130, 12);
            this.cbPolicySysID.Name = "cbPolicySysID";
            this.cbPolicySysID.Size = new System.Drawing.Size(121, 21);
            this.cbPolicySysID.TabIndex = 0;
            // 
            // lblPolicySysNm
            // 
            this.lblPolicySysNm.AutoSize = true;
            this.lblPolicySysNm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPolicySysNm.Location = new System.Drawing.Point(3, 15);
            this.lblPolicySysNm.Name = "lblPolicySysNm";
            this.lblPolicySysNm.Size = new System.Drawing.Size(121, 13);
            this.lblPolicySysNm.TabIndex = 1;
            this.lblPolicySysNm.Text = "Policy System Name";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(104, 42);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // pnlDownload
            // 
            this.pnlDownload.Controls.Add(this.rtbStatus);
            this.pnlDownload.Location = new System.Drawing.Point(12, 279);
            this.pnlDownload.Name = "pnlDownload";
            this.pnlDownload.Size = new System.Drawing.Size(683, 196);
            this.pnlDownload.TabIndex = 4;
            this.pnlDownload.Visible = false;
            // 
            // rtbStatus
            // 
            this.rtbStatus.Location = new System.Drawing.Point(8, 10);
            this.rtbStatus.Name = "rtbStatus";
            this.rtbStatus.ReadOnly = true;
            this.rtbStatus.Size = new System.Drawing.Size(661, 173);
            this.rtbStatus.TabIndex = 2;
            this.rtbStatus.Text = "";
            // 
            // pnlUserInput
            // 
            this.pnlUserInput.Controls.Add(this.btnNext);
            this.pnlUserInput.Controls.Add(this.cbPolicySysID);
            this.pnlUserInput.Controls.Add(this.lblPolicySysNm);
            this.pnlUserInput.Location = new System.Drawing.Point(203, 26);
            this.pnlUserInput.Name = "pnlUserInput";
            this.pnlUserInput.Size = new System.Drawing.Size(273, 72);
            this.pnlUserInput.TabIndex = 5;
            // 
            // pnlImage
            // 
            this.pnlImage.Controls.Add(this.pbOverall);
            this.pnlImage.Controls.Add(this.lblError);
            this.pnlImage.Controls.Add(this.lblHelpMsg);
            this.pnlImage.Controls.Add(this.lblDesc);
            this.pnlImage.Controls.Add(this.lblTo);
            this.pnlImage.Controls.Add(this.lblFrom);
            this.pnlImage.Controls.Add(this.txtUpperIndex);
            this.pnlImage.Controls.Add(this.txtLowerIndex);
            this.pnlImage.Controls.Add(this.lblRange);
            this.pnlImage.Controls.Add(this.btnStartProcessing);
            this.pnlImage.Controls.Add(this.lblSrcPath);
            this.pnlImage.Controls.Add(this.lbMsg);
            this.pnlImage.Controls.Add(this.pbStatus);
            this.pnlImage.Location = new System.Drawing.Point(17, 117);
            this.pnlImage.Name = "pnlImage";
            this.pnlImage.Size = new System.Drawing.Size(678, 145);
            this.pnlImage.TabIndex = 6;
            this.pnlImage.Visible = false;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(488, 60);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 14;
            // 
            // lblHelpMsg
            // 
            this.lblHelpMsg.AutoSize = true;
            this.lblHelpMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelpMsg.Location = new System.Drawing.Point(429, 86);
            this.lblHelpMsg.Name = "lblHelpMsg";
            this.lblHelpMsg.Size = new System.Drawing.Size(79, 12);
            this.lblHelpMsg.TabIndex = 13;
            this.lblHelpMsg.Text = "Eg: From 1 To 100";
            // 
            // lblDesc
            // 
            this.lblDesc.AutoSize = true;
            this.lblDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc.Location = new System.Drawing.Point(277, 86);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(146, 12);
            this.lblDesc.TabIndex = 12;
            this.lblDesc.Text = "Keep empty to process all records.";
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(396, 64);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 11;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(273, 64);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 10;
            this.lblFrom.Text = "From";
            // 
            // txtUpperIndex
            // 
            this.txtUpperIndex.Location = new System.Drawing.Point(422, 58);
            this.txtUpperIndex.Name = "txtUpperIndex";
            this.txtUpperIndex.Size = new System.Drawing.Size(43, 20);
            this.txtUpperIndex.TabIndex = 9;
            // 
            // txtLowerIndex
            // 
            this.txtLowerIndex.Location = new System.Drawing.Point(309, 58);
            this.txtLowerIndex.Name = "txtLowerIndex";
            this.txtLowerIndex.Size = new System.Drawing.Size(43, 20);
            this.txtLowerIndex.TabIndex = 8;
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRange.Location = new System.Drawing.Point(131, 65);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(126, 13);
            this.lblRange.TabIndex = 5;
            this.lblRange.Text = "Records to Process :";
            // 
            // btnStartProcessing
            // 
            this.btnStartProcessing.Location = new System.Drawing.Point(251, 110);
            this.btnStartProcessing.Name = "btnStartProcessing";
            this.btnStartProcessing.Size = new System.Drawing.Size(135, 23);
            this.btnStartProcessing.TabIndex = 4;
            this.btnStartProcessing.Text = "Start Download Process";
            this.btnStartProcessing.UseVisualStyleBackColor = true;
            this.btnStartProcessing.Click += new System.EventHandler(this.btnStartProcessing_Click);
            // 
            // lblSrcPath
            // 
            this.lblSrcPath.AutoSize = true;
            this.lblSrcPath.Location = new System.Drawing.Point(269, 38);
            this.lblSrcPath.Name = "lblSrcPath";
            this.lblSrcPath.Size = new System.Drawing.Size(35, 13);
            this.lblSrcPath.TabIndex = 3;
            this.lblSrcPath.Text = "label2";
            // 
            // lbMsg
            // 
            this.lbMsg.AutoSize = true;
            this.lbMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMsg.Location = new System.Drawing.Point(172, 38);
            this.lbMsg.Name = "lbMsg";
            this.lbMsg.Size = new System.Drawing.Size(85, 13);
            this.lbMsg.TabIndex = 2;
            this.lbMsg.Text = "Source Path :";
            // 
            // pbOverall
            // 
            this.pbOverall.Location = new System.Drawing.Point(279, 7);
            this.pbOverall.Name = "pbOverall";
            this.pbOverall.Size = new System.Drawing.Size(100, 23);
            this.pbOverall.TabIndex = 15;
            // 
            // pbStatus
            // 
            this.pbStatus.Image = ((System.Drawing.Image)(resources.GetObject("pbStatus.Image")));
            this.pbStatus.ImageLocation = "";
            this.pbStatus.Location = new System.Drawing.Point(412, 7);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(25, 27);
            this.pbStatus.TabIndex = 1;
            this.pbStatus.TabStop = false;
            this.pbStatus.Visible = false;
            // 
            // frmPolicy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 487);
            this.Controls.Add(this.pnlImage);
            this.Controls.Add(this.pnlUserInput);
            this.Controls.Add(this.pnlDownload);
            this.Name = "frmPolicy";
            this.Text = "Policy Download Utility";
            this.Load += new System.EventHandler(this.frmPolicy_Load);
            this.pnlDownload.ResumeLayout(false);
            this.pnlUserInput.ResumeLayout(false);
            this.pnlUserInput.PerformLayout();
            this.pnlImage.ResumeLayout(false);
            this.pnlImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbPolicySysID;
        private System.Windows.Forms.Label lblPolicySysNm;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlDownload;
        private System.Windows.Forms.Panel pnlUserInput;
        private RichTextBox rtbStatus;
        private Panel pnlImage;
        private Button btnStartProcessing;
        private Label lblSrcPath;
        private Label lbMsg;
        private Label lblRange;
        private Label lblHelpMsg;
        private Label lblDesc;
        private Label lblTo;
        private Label lblFrom;
        private TextBox txtUpperIndex;
        private TextBox txtLowerIndex;
        private Label lblError;
        private ProgressBar pbOverall;
        private PictureBox pbStatus;
    }
}

