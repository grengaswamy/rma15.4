﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.IO;
using System.Web;
using System.Diagnostics;
using System.Configuration;

namespace RMXOutlook2007AddIn
{
    public partial class ClaimInfo : Form
    {
        Outlook.Selection m_selectedItems = null;
        Outlook.MailItem m_selectedMailItem = null;
        TextWriterTraceListener oTraceListener = null;
        TraceSwitch oTraceSwitch = null;
        
        public ClaimInfo(Outlook.Selection selectedItems)
        {
            InitializeComponent();
            m_selectedItems = selectedItems;
            DefaultValueToMsgAndInitializeTrace();
            
        }
        public ClaimInfo(Outlook.MailItem selectedMailItem)
        {
            InitializeComponent();
            m_selectedMailItem = selectedMailItem;
            DefaultValueToMsgAndInitializeTrace();
            
        }
        private void DefaultValueToMsgAndInitializeTrace()
        {
            try
            {
                checkBox1.Checked = true;
                comboBox1.SelectedIndex = 0;
                CheckAndDisableSendAttachments();
                //Initialize trace switch
                string sLogFile = String.Format("{0}" + ConfigurationManager.AppSettings["LogFileName"], ConfigurationManager.AppSettings["TemplatePath"]);
                oTraceListener = new TextWriterTraceListener(sLogFile);
                Trace.Listeners.Add(oTraceListener);
                Trace.AutoFlush = true;
                oTraceSwitch = new TraceSwitch("mySwitch", "Entire application");
            }
            catch (Exception ex)
            {
                if (oTraceListener != null)
                {
                    oTraceListener.WriteLine(String.Format("{0}. {1}", "TimeStamp:", DateTime.Now.ToString()));
                    oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));
                }

                MessageBox.Show("An error has occurred while opening the Claim Information Form for RMX plug-in.");
            }
            finally
            {
                if (oTraceListener != null)
                    oTraceListener.Close();
            }

        }
        private void CheckAndDisableSendAttachments()
        {
            checkBox1.Checked = true;
            checkBox1.Enabled = false;
        }
        private void UnCheckAndEnableSendAttachments()
        {
            checkBox1.Checked = false;
            checkBox1.Enabled = true;
        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            DocumentAttachWrapper objDocAttach = new DocumentAttachWrapper();
            bool bAllNotEmailItems = true;
            
            try
            {
			//rsharma220 RMA-136 Start
                if (string.IsNullOrEmpty(textBox1.Text) && rdbClaim.Checked)
                {
                    MessageBox.Show("Please enter a claim number");
                    return;
                }
                else if (string.IsNullOrEmpty(textBox1.Text) && rdbEvent.Checked)
                {
                    MessageBox.Show("Please enter an event number");
                    return;
                }

                if (m_selectedItems != null)
                {
                    if (m_selectedItems.Count > 0)
                    {
                        for (int j = 1; j <= m_selectedItems.Count; j++)
                        {
                            Object selObject = m_selectedItems[j];
                            if (selObject is Outlook.MailItem)
                            {
                                Outlook.MailItem eMail = (selObject as Outlook.MailItem);
                                if (rdbClaim.Checked)
                                    objDocAttach.CreateDocDetails(comboBox1.SelectedItem.ToString(), eMail, checkBox1.Checked, textBox1.Text, "Claim Number");
                                else if (rdbEvent.Checked)
                                    objDocAttach.CreateDocDetails(comboBox1.SelectedItem.ToString(), eMail, checkBox1.Checked, textBox1.Text, "Event Number");

                                bAllNotEmailItems = false;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        if (bAllNotEmailItems)//If none of the selected items is an email item.
                        {
                            MessageBox.Show("Only a mail item can be uploaded.");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Selected Email Item(s) Attached To RMX");
                            this.Close();
                        }
                    }
                }
                else if (m_selectedMailItem != null)
                {
                    objDocAttach.CreateDocDetails(comboBox1.SelectedItem.ToString(), m_selectedMailItem, checkBox1.Checked, textBox1.Text, label1.Text);
					//rsharma220 RMA-136 End
                    MessageBox.Show("Email Attached To RMX");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Only a mail item can be uploaded.");
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                if (oTraceListener != null)
                {
                    oTraceListener.WriteLine(String.Format("{0}. {1}", "TimeStamp:", DateTime.Now.ToString()));
                    oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));
                }

                MessageBox.Show("An error has occurred while sending the email item to RMX.Please check whether the claim exists and then try again.");
            }
            finally
            {
                if (oTraceListener != null)
                    oTraceListener.Close();
                
            }

        }
      

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                CheckAndDisableSendAttachments();
            }
            else
            {
                UnCheckAndEnableSendAttachments();
            }
        }

        private void ClaimInfo_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
		//rsharma220 RMA-136 Start
        private void rdbEvent_CheckedChanged(object sender, EventArgs e)
        {
            label1.Text = "Event Number";
        }

        private void rdbClaim_CheckedChanged(object sender, EventArgs e)
        {
            label1.Text = "Claim Number";

        }
		//rsharma220 RMA-136 End
    }
}
