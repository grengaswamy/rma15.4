﻿using System;
using System.Collections.Generic;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Globalization;

namespace RMXOutlook2007AddIn
{
    class DocumentAttachWrapper
    {
            //Formats in which the emails can be uploaded
            private const string MSG = ".msg";
            private const string TXT = ".txt";
            private const string DOC = ".doc";
            private const string HTML = ".html";
        public bool AddDocument(Dictionary<string,string> sDocDetails,FileStream fs, byte[] fileContent, string sUserName, string sDsnName)
        {
            string sRMXUserName = string.Empty;
            string sDSNName = string.Empty;

            
            try
            {
               
                long lFileSize = fileContent.Length;
                double dSizeInMB = (lFileSize/1024f)/1024f;
                if (dSizeInMB > 100.00)
                {
                    //MessageBox.Show("Cannot upload  file " + sDocDetails["FileName"] + ". The size is more than 100 MB");
                    MessageBox.Show(string.Format("Cannot upload  file {0}. The size is more than 100 MB",sDocDetails["FileName"]));
                    return false;
                }

                
                DataStreamingService.DataStreamingServiceClient rmservice = null;
                rmservice = new RMXOutlook2007AddIn.DataStreamingService.DataStreamingServiceClient();
                DataStreamingService.StreamedDocumentType document = null;
                document = new DataStreamingService.StreamedDocumentType();

                
                AuthenticationServiceReference.AuthenticationServiceClient auService = null;
                auService = new AuthenticationServiceReference.AuthenticationServiceClient();
                sRMXUserName = ConfigurationManager.AppSettings["RMXUsername"];
                sDSNName = ConfigurationManager.AppSettings["DsnName"];
                document.Class = new RMXOutlook2007AddIn.DataStreamingService.StreamedCodeType();
                document.Type = new RMXOutlook2007AddIn.DataStreamingService.StreamedCodeType();
                document.Category = new RMXOutlook2007AddIn.DataStreamingService.StreamedCodeType();
                document.Token = auService.GetUserSessionID(sRMXUserName, sDSNName);
                document.Title = sDocDetails["Title"];
				//rsharma220 RMA-136 Start
                if (!sDocDetails.ContainsKey("ClaimNumber"))
                {
                    document.FolderId = sDocDetails["EventNumber"];
                    document.AttachTable = "event";
                    document.AttachRecordId = -20;
                }
                else
                {
                    document.FolderId = sDocDetails["ClaimNumber"];
                    document.AttachTable = "claim";
                    document.AttachRecordId = -19;
                }
				//rsharma220 RMA-136 End
                document.PsId = 3000;
                document.FilePath = string.Empty;
                document.Subject = sDocDetails["Subject"]; 
                document.Notes = string.Empty;
                document.Keywords = string.Empty;
                document.FileName = sDocDetails["FileName"];
                document.fileStream = fs;
                //akaushik5 added filecontent, changes for cloud
                //document.FileContents = fileContent;


                rmservice.CreateDocument(document.AttachRecordId,
                    document.AttachTable,
                    document.Category,
                    //npadhy Claim Number needs to be sent from Mobility
                    string.Empty,
                    document.Class,
                    // Client Id hardcoded as of now.
                    0,
                    document.Copy,
                    document.Create,
                    document.CreateDate,
                    document.Delete,
                    document.DocInternalType,
                    document.DocumentCategory,
                    document.DocumentClass,
                    document.DocumentId,
                    document.DocumentsType,
                    document.Download,
                    document.Edit,
                    document.Email,
                    document.Errors,
                    document.FileContents,
                    document.FileName,
                    document.FilePath,
                    lFileSize.ToString(),
                    document.FolderId,
                    document.FolderName,
                    document.FormName,
                    document.Keywords,
                    document.Move,
                    document.Notes,
                    document.Pid,
                    document.PsId,
                    document.Readonly,
                    document.ScreenFlag,
                    document.Subject,
                    document.Title,
                    document.Token,
                    document.Transfer,
                    document.Type,
                    document.UserId,
                    document.UserName,
                    document.View,
                    document.fileStream);

                
                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {

            }
        }
		//rsharma220 RMA-136
        public void CreateDocDetails(string sFormat, Outlook.MailItem eMail, bool bUploadAttachment, string sNumber, string Labeltext)
        {
            Dictionary<string, string> sDocDetails = new Dictionary<string,string>();
            string sFileName = string.Empty;
            bool bAddDocumentSuccess = false;
            FileStream fs = null;
            
            try
            {


                sDocDetails.Add("Title", "Email Item_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss", DateTimeFormatInfo.InvariantInfo));
                if (Labeltext == "Claim Number")
                    sDocDetails.Add("ClaimNumber", sNumber);
                else if (Labeltext == "Event Number")
                    sDocDetails.Add("EventNumber", sNumber);
                sDocDetails.Add("Subject", eMail.Subject);
                switch (sFormat)
                {
                   
                        
                    case TXT:
                        sFileName = "Email Item_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss",DateTimeFormatInfo.InvariantInfo) + sFormat;
                        sDocDetails.Add("FileName", sFileName);
                        eMail.SaveAs(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, Outlook.OlSaveAsType.olTXT);
                        byte[] fileContentTxt = File.ReadAllBytes(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                        fs = File.Open(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, FileMode.Open);
                        
                        bAddDocumentSuccess = AddDocument(sDocDetails,fs, fileContentTxt, ConfigurationManager.AppSettings["RMXUsername"], ConfigurationManager.AppSettings["DsnName"]);
                        fs.Close();
                        File.Delete(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                        if (bAddDocumentSuccess && bUploadAttachment)
                        {
                            if (eMail.Attachments.Count > 0)
                            {
                                for (int i = 1; i <= eMail.Attachments.Count; i++)
                                {
                                    sFileName = eMail.Attachments[i].FileName;
                                    sDocDetails.Remove("FileName");
                                    sDocDetails.Add("FileName", sFileName);
                                    eMail.Attachments[i].SaveAsFile(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                                    fileContentTxt = File.ReadAllBytes(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                                   
                                    fs = File.Open(ConfigurationManager.AppSettings["TemplatePath"] + sFileName,FileMode.Open);
                                    
                                    AddDocument(sDocDetails,fs, fileContentTxt, ConfigurationManager.AppSettings["RMXUsername"], ConfigurationManager.AppSettings["DsnName"]);
                                    fs.Close();
                                    File.Delete(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                                }
                            }
                           
                        }
                        
                        break;
                    case DOC:

                        sFileName = "Email Item_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss",DateTimeFormatInfo.InvariantInfo) + sFormat;
                        sDocDetails.Add("FileName", sFileName);
                        eMail.SaveAs(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, Outlook.OlSaveAsType.olDoc);
                        byte[] fileContentDoc = File.ReadAllBytes(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                        fs = File.Open(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, FileMode.Open);

                        bAddDocumentSuccess = AddDocument(sDocDetails, fs, fileContentDoc, ConfigurationManager.AppSettings["RMXUsername"], ConfigurationManager.AppSettings["DsnName"]);
                        fs.Close();
                        File.Delete(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                        if (bAddDocumentSuccess && bUploadAttachment)
                        {
                            if (eMail.Attachments.Count > 0)
                            {
                                for (int i = 1; i <= eMail.Attachments.Count; i++)
                                {
                                    sDocDetails.Remove("FileName");
                                    sFileName = eMail.Attachments[i].FileName;
                                    sDocDetails.Add("FileName", sFileName);
                                    eMail.Attachments[i].SaveAsFile(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                                    fileContentDoc = File.ReadAllBytes(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                                    fs = File.Open(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, FileMode.Open);

                                    AddDocument(sDocDetails, fs, fileContentDoc, ConfigurationManager.AppSettings["RMXUsername"], ConfigurationManager.AppSettings["DsnName"]);
                                    fs.Close();
                                    File.Delete(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                                }
                            }
                            
                        }
                        
                        break;
                    case HTML:
                        sFileName = "Email Item_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss",DateTimeFormatInfo.InvariantInfo) + sFormat;
                        sDocDetails.Add("FileName", sFileName);
                        eMail.SaveAs(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, Outlook.OlSaveAsType.olHTML);
                        byte[] fileContentHtml = File.ReadAllBytes(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                        fs = File.Open(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, FileMode.Open);

                        bAddDocumentSuccess = AddDocument(sDocDetails, fs, fileContentHtml, ConfigurationManager.AppSettings["RMXUsername"], ConfigurationManager.AppSettings["DsnName"]);
                        fs.Close();
                        File.Delete(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                        if (bAddDocumentSuccess && bUploadAttachment)
                        {
                            if (eMail.Attachments.Count > 0)
                            {
                                for (int i = 1; i <= eMail.Attachments.Count; i++)
                                {
                                    sDocDetails.Remove("FileName");
                                    sFileName = eMail.Attachments[i].FileName;
                                    sDocDetails.Add("FileName", sFileName);
                                    eMail.Attachments[i].SaveAsFile(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                                    fileContentHtml = File.ReadAllBytes(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                                    fs = File.Open(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, FileMode.Open);

                                    AddDocument(sDocDetails, fs, fileContentHtml, ConfigurationManager.AppSettings["RMXUsername"], ConfigurationManager.AppSettings["DsnName"]);
                                    fs.Close();
                                    File.Delete(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                                }
                            }
                        }
                        
                        break;
                    case MSG:
                    default://send in msg format
                        sFileName = "Email Item_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss",DateTimeFormatInfo.InvariantInfo) + MSG;
                        sDocDetails.Add("FileName", sFileName);
                        eMail.SaveAs(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, Outlook.OlSaveAsType.olMSG);
                        byte[] fileContentMsg = File.ReadAllBytes(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                        fs = File.Open(ConfigurationManager.AppSettings["TemplatePath"] + sFileName, FileMode.Open);

                        bAddDocumentSuccess = AddDocument(sDocDetails, fs, fileContentMsg, ConfigurationManager.AppSettings["RMXUsername"], ConfigurationManager.AppSettings["DsnName"]);
                        fs.Close();
                        File.Delete(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
                        break;
                
                }

                return;
                           
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                fs.Close();
                File.Delete(ConfigurationManager.AppSettings["TemplatePath"] + sFileName);
            }
        }
    }
}
