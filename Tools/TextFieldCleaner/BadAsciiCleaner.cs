using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml;

using Riskmaster.Common;
using Riskmaster.Db;
using System.Collections.Generic;
using System.Text;

namespace CleanTextFields
{
	/// <summary>
	/// Summary description for RemoveBadAscii.
	/// </summary>
	public class BadAsciiCleaner
	{
		public event progressDelegate showProgress;
		public event progressDelegate showComplete;
		public event messageDelegate showMessage;
        public delegate void progressDelegate(string sMessage);
        public delegate void messageDelegate(string sMessage, bool bError);

        private bool m_bPauseScan = false;
        private int m_iClaimId = 0; //pmittal5
        private eDatabaseType m_DatabaseType;
		private string m_sConnectionString = String.Empty;
		private string m_sFilePath = String.Empty;
		private const string m_ConfigXmlFile = "AsciiCleaner.xml";
		private const string m_ResultXmlFile = "BadRecords.xml";

        public BadAsciiCleaner(string sConnectionString)
        {
            this.m_sConnectionString = sConnectionString;
        }
         
        //public BadAsciiCleaner(string sConnectionString, eDatabaseType p_DatabaseType, string sPath)
        public BadAsciiCleaner(string sConnectionString, eDatabaseType p_DatabaseType, string sPath, int iClaimId)
		{
            this.m_bPauseScan = false;
            this.m_sConnectionString = sConnectionString;
            this.m_DatabaseType = p_DatabaseType;
            this.m_sFilePath = sPath;
            this.m_iClaimId = iClaimId;  //pmittal5
        }

        #region unused code
        /// <summary>
		/// scan the table.column defined in the configure file for
		/// bad ascii code ( 0 - 31, except 9, 10, 13)
		/// </summary>
        //public void scanBadAscii()
        //{
        //    XmlDocument oConfigXml = new XmlDocument();
        //    XmlDocument oBadRecordsXml = new XmlDocument();
        //    DbReader oReader = null;
        //    int iBadRecords = 0;
        //    bool bError = false;

        //    try
        //    {
        //        oConfigXml.Load( m_sFilePath + m_ConfigXmlFile);

        //        oBadRecordsXml.LoadXml("<records/>");
        //        XmlNode oParentNode = oBadRecordsXml.SelectSingleNode("/records");

        //        //raise the scanTable event
        //        string sMessage = "Start Scanning for Bad Ascii Codes.";
        //        showProgress(sMessage);

        //        //loop through all the tables which are required to be scanned
        //        XmlNodeList oTableNodeList = oConfigXml.SelectNodes("//tables/table[@scan='yes']");
        //        foreach(XmlNode oTableNode in oTableNodeList)
        //        {
        //            string sTableName = oTableNode.Attributes["name"].Value;
        //            string sIDColumn = oTableNode.Attributes["idcolumn"].Value;
        //            string sTextColumn = oTableNode.Attributes["textcolumn"].Value;
        //            string sHtmlTextColumn = oTableNode.Attributes["htmltextcolumn"].Value;
					
        //            //raise the scanTable event
        //            sMessage = System.Environment.NewLine + System.Environment.NewLine 
        //                + sTableName + "." + sTextColumn + System.Environment.NewLine;
        //            showProgress(sMessage);

        //            //loop through all the ascii code between 0 - 31 (except 9,10,13)
        //            for(int iAsciiCode=0; iAsciiCode<32; iAsciiCode++)
        //            {
        //                //pause scaning
        //                while( m_bPauseScan )
        //                {
        //                    showMessage("The scan process is paused. Click button to continue.", false);
        //                    System.Threading.Thread.Sleep(1000);
        //                }

        //                if( iAsciiCode == 9 || iAsciiCode == 10 || iAsciiCode == 13)
        //                    continue;

        //                showProgress(iAsciiCode.ToString() + ",");

        //                string sSQL = "SELECT " + sIDColumn + " FROM " + sTableName + " WHERE ";
						
        //                switch( m_DatabaseType )
        //                {
        //                    case eDatabaseType.DBMS_IS_SQLSRVR:
        //                        sSQL += "CHARINDEX(CHAR(" + iAsciiCode + "), " + sTextColumn + ")>0";
        //                        break;
        //                    case eDatabaseType.DBMS_IS_ORACLE:
        //                        sSQL += "INSTR(" + sTextColumn + ", CHR(" + iAsciiCode + "))>0";
        //                        break;
        //                    default:
        //                        sSQL += "CHARINDEX(CHAR(" + iAsciiCode + "), " + sTextColumn + ")>0";
        //                        break;
        //                }

        //                oReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                while( oReader.Read() )
        //                {
        //                    int iRecordId = oReader.GetInt(sIDColumn);
        //                    XmlElement oBadRecord = oBadRecordsXml.CreateElement("record");
        //                    oBadRecord.SetAttribute("tablename", sTableName);
        //                    oBadRecord.SetAttribute("idcolumn", sIDColumn);
        //                    oBadRecord.SetAttribute("idvalue", iRecordId.ToString());
        //                    oBadRecord.SetAttribute("textcolumn", sTextColumn);
        //                    oBadRecord.SetAttribute("htmltextcolumn", sHtmlTextColumn);
        //                    oBadRecord.SetAttribute("asciicode", iAsciiCode.ToString());
        //                    oParentNode.AppendChild(oBadRecord);

        //                    iBadRecords++;
        //                    showMessage(iBadRecords.ToString() + " bad records found.", false);
        //                }
        //                oReader.Close();
        //            }
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        showMessage("Error during scan. " + ex.Message, true );
        //        bError = true;
        //    }
        //    finally
        //    {
        //        if( oReader != null )
        //            oReader.Close();
        //    }

        //    //Save the result to file
        //    oBadRecordsXml.Save(m_sFilePath + m_ResultXmlFile);

        //    if( !bError )
        //    {
        //        showMessage("Done scanning. Total " + iBadRecords.ToString() + " bad records found.", false);
        //    }
			
        //    showComplete("Done");
        //}
        #endregion

        public int writeBadTestData()
        {
            int iNumRowsAffected = 0;
            StringBuilder sbQuery = new StringBuilder();

            //@table_name   = 'ADJUST_DATED_TEXT'
		    //@id_col       = 'ADJ_DTTEXT_ROW_ID'   //primary key
		    //@source_col   = 'DATED_TEXT'
		    //@idCol_value  = 0
		    //@htmlcomment  = 'DATED_TEXT_HTMLCOMMENTS'

            //---------------------012345678901234567890123456789--
            string sourceString = "The Inserted Text is here -><-";
            char[] badChars = new char[33];
            for (int x = 0; x < 32; x++)
            {
                badChars[x] = Convert.ToChar(x);
                sourceString = sourceString.Insert(28, Convert.ToString(badChars[x]));
            }

            sbQuery.AppendFormat("UPDATE ADJUST_DATED_TEXT ");
            sbQuery.AppendFormat("   SET DATED_TEXT = '{0}', ", sourceString);
            sbQuery.AppendFormat("       DATED_TEXT_HTMLCOMMENTS = '{0}' ", sourceString);
            sbQuery.AppendFormat(" WHERE ADJ_DTTEXT_ROW_ID = '11'");

            try
            {
                iNumRowsAffected = DbFactory.ExecuteNonQuery(m_sConnectionString, sbQuery.ToString(), new Dictionary<string, string>());
            }
            catch (Exception ex)
            {
                this.showMessage("Error during UPDATE: " + ex.Message, true);
            }

            return iNumRowsAffected;
        }

        public void scanBadAscii()
        {
            XmlDocument oConfigXml = new XmlDocument();
            XmlDocument oBadRecordsXml = new XmlDocument();
            DbReader oReader = null;
            int iBadRecords = 0;
            bool bError = false;
            int iLob = 0;

            try
            {
                oConfigXml.Load(this.m_sFilePath + "AsciiCleaner.xml");

                oBadRecordsXml.LoadXml("<records/>");
                XmlNode oParentNode = oBadRecordsXml.SelectSingleNode("/records");
                string sMessage = "Start Scanning for Bad Ascii Codes.";
                //pmittal5 10/14/08
                XmlNodeList objTableList;
                if (m_iClaimId == 0)
                {
                    objTableList = oConfigXml.SelectNodes("//tables/table[@scan='yes']");
                }
                else
                {
                    objTableList = oConfigXml.SelectNodes("//tables/table[@scan='yes' and @isClaimRelated='yes']");
                    oReader = DbFactory.GetDbReader(this.m_sConnectionString, "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + m_iClaimId);
                    if (oReader.Read())
                        iLob = oReader.GetInt("LINE_OF_BUS_CODE");
                    oReader.Close();
                }

                //foreach (XmlNode oTableNode in oConfigXml.SelectNodes("//tables/table[@scan='yes']"))
                foreach (XmlNode oTableNode in objTableList)
                {//End - pmittal5
                    string sTableName = oTableNode.Attributes["name"].Value;
                    string sIDColumn = oTableNode.Attributes["idcolumn"].Value;
                    string sTextColumn = oTableNode.Attributes["textcolumn"].Value;
                    string sHtmlTextColumn = string.Empty;
                    string sSQL = string.Empty;
                    if(oTableNode.Attributes["htmltextcolumn"] != null)
                         sHtmlTextColumn = oTableNode.Attributes["htmltextcolumn"].Value;
                    sMessage = System.Environment.NewLine + System.Environment.NewLine + sTableName + "." + sTextColumn + System.Environment.NewLine;
                    if(sHtmlTextColumn != "")
                        sMessage += sTableName + "." + sHtmlTextColumn + System.Environment.NewLine;  //pmittal5 10/10/08
                    this.showProgress(sMessage);
                    //pmittal5 10/10/08
                    //int iAsciiCode = 0;
                    //goto Label_02A8;

                    //pmittal5 12/11/08
                    int iIdColValue = 0;
                    if (m_iClaimId != 0)
                    {
                        switch (sTableName)
                        {
                            case "EVENT":
                                oReader = DbFactory.GetDbReader(this.m_sConnectionString, "SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID = " + m_iClaimId);
                                if(oReader.Read())
                                    iIdColValue = oReader.GetInt("EVENT_ID");
                                break;
                            case "ENTITY":
                                if(iLob == 243 || iLob == 844)  //For WC and DI claims
                                {
                                    oReader = DbFactory.GetDbReader(this.m_sConnectionString, "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID=" + m_iClaimId + " AND PRIMARY_CLMNT_FLAG <> 0 AND NOT PRIMARY_CLMNT_FLAG IS NULL");
                                    if (oReader.Read())
                                        iIdColValue = oReader.GetInt("CLAIMANT_EID");
                                }
                                else
                                    continue;
                                break;
                            case "PERSON_INVOLVED":
                            case "PI_X_DIAG_HIST":
                            case "PI_X_MMI_HIST":
                                if (iLob == 243 || iLob == 844)  //For WC and DI claims
                                {
                                    oReader = DbFactory.GetDbReader(this.m_sConnectionString, "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID= (SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID = " + m_iClaimId + ") AND PI_EID = (SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID=" + m_iClaimId + " AND PRIMARY_CLMNT_FLAG <> 0 AND NOT PRIMARY_CLMNT_FLAG IS NULL)");
                                    if (oReader.Read())
                                        iIdColValue = oReader.GetInt("PI_ROW_ID");
                                }
                                else
                                    continue;
                                break;
                            default:
                                iIdColValue = m_iClaimId;
                                break;
                        }
                        if(oReader != null)
                            oReader.Close();
                    }
                    //End 

                    goto Label_0151;
                    //end - pmittal5
                Label_0136:
                    this.showMessage("The scan process is paused. Click button to continue.", false);
                    System.Threading.Thread.Sleep(0x3e8);
                Label_0151:
                    if (this.m_bPauseScan)
                    {
                        goto Label_0136;
                    }
                    //pmittal5 10/10/08 - No need to pass "AsciiCode" to Stored Procedure
                    //switch (iAsciiCode)
                    //{
                    //    case 9:
                    //    case 10:
                    //    case 13:
                    //        break;

                    //default:
                    //{
                    //string sSQL = "EXEC usp_ScanTextFieldNew '" + sTableName + "', '" + sIDColumn + "', '" + sTextColumn + "', '" + iAsciiCode.ToString() + "'";
                    try
                    {
                        if (sHtmlTextColumn == "")
                            sSQL = "EXEC usp_ScanTextFieldNew '" + sTableName + "', '" + sIDColumn + "', '" + sTextColumn + "', " + iIdColValue;
                        else
                            sSQL = "EXEC usp_ScanTextFieldNew '" + sTableName + "', '" + sIDColumn + "', '" + sTextColumn + "', " + iIdColValue + ", '" + sHtmlTextColumn + "'";

                        //End - pmittal5
                        oReader = DbFactory.GetDbReader(this.m_sConnectionString, sSQL);
                    }
                    catch (SqlException sqlex)
                    {
                        if (sqlex.Number == 2812)
                        {
                            MessageBox.Show("Stored Procedure 'usp_ScanTextFieldNew' does not exist in the database.");
                        }
                        else
                        {
                            if (m_iClaimId == 0)
                            {
                                //frmOptions.writeLog(string.Format("ERROR:[Error Scanning Bad Ascii:] {0} Error while running stored procedure for table : {1} having Primary Id : {2}  for Text Column : {3} ", sqlex.Message, sTableName, sIDColumn, sTextColumn));
                                Debug.WriteLine(string.Format("ERROR:[Error Scanning Bad Ascii:] {0} Error while running stored procedure for table : {1} having Primary Id : {2}  for Text Column : {3} ", sqlex.Message, sTableName, sIDColumn, sTextColumn));
                            }
                            else
                            {
                                //frmOptions.writeLog(string.Format("ERROR:[Error Scanning Bad Ascii:] {0} Error while running stored procedure for table : {1} having Primary Id : {2} for Text Column : {3} where Claim Id is : {4}", sqlex.Message, sTableName, sIDColumn, sTextColumn, m_iClaimId));
                                Debug.WriteLine(string.Format("ERROR:[Error Scanning Bad Ascii:] {0} Error while running stored procedure for table : {1} having Primary Id : {2} for Text Column : {3} where Claim Id is : {4}", sqlex.Message, sTableName, sIDColumn, sTextColumn, m_iClaimId));
                            }
                        }
                    }
                    
                    while (oReader.Read())
                    {
                        int iRecordId = oReader.GetInt("ROW_ID");
                        int iAsciiCode = oReader.GetInt("ASCII_CODE"); //pmittal5 MITS 10519 10/10/08
                        XmlElement oBadRecord = oBadRecordsXml.CreateElement("record");
                        oBadRecord.SetAttribute("tablename", sTableName);
                        oBadRecord.SetAttribute("idcolumn", sIDColumn);
                        oBadRecord.SetAttribute("idvalue", iRecordId.ToString());
                        oBadRecord.SetAttribute("textcolumn", sTextColumn);
                        oBadRecord.SetAttribute("htmltextcolumn", sHtmlTextColumn);
                        oBadRecord.SetAttribute("asciicode", iAsciiCode.ToString());
                        oParentNode.AppendChild(oBadRecord);
                        iBadRecords++;
                    }
                    oReader.Close();

                    //break;
                    //}
                    //}

                    //pmittal5 10/10/08 - Execute Stored Procedure only once for a table.  
                    //iAsciiCode++;
                    //Label_02A8:
                    //if (iAsciiCode < 0x20)
                    //{
                    //    goto Label_0151;
                    //}
                    //End - pmittal5
                }
            }
            catch (Exception ex)
            {
                this.showMessage("Error during scan. " + ex.Message, true);
                bError = true;
            }
            finally
            {
                if (oReader != null)
                {
                    oReader.Close();
                }
            }

            oBadRecordsXml.Save(this.m_sFilePath + "BadRecords.xml");
            if (!bError)
            {
                this.showMessage("Done scanning. Total " + iBadRecords.ToString() + " bad records found.", false);
            }
            this.showComplete("Done");
        }

		/// <summary>
		/// Remove the bad ascii characters found in the scan step
		/// </summary>
		public void RemoveBadAscii()
		{
			XmlDocument oBadRecordsXml = new XmlDocument();
			XmlNodeList oRecordNodeList = null;
			string sSQL = string.Empty;
			DbReader oReader = null;
			string sText = string.Empty;
			string sHtmlText = string.Empty;
			DbWriter objDbWriter = null;
			int iRecordsNum = 0;
			bool bError = false;

			//Retrive the Comments & HTMLComments and remove the bad ascii character
			try
			{
				oBadRecordsXml.Load( m_sFilePath + m_ResultXmlFile);
				oRecordNodeList = oBadRecordsXml.SelectNodes("//records/record");
				foreach(XmlNode oRecordNode in oRecordNodeList)
				{
					string sTableName = oRecordNode.Attributes["tablename"].Value;
					string sIDValue = oRecordNode.Attributes["idvalue"].Value;
					string sIDColumn = oRecordNode.Attributes["idcolumn"].Value;
					string sTextColumn = oRecordNode.Attributes["textcolumn"].Value;
					string sHtmlTextColumn = oRecordNode.Attributes["htmltextcolumn"].Value;
					string sAsciiCode = oRecordNode.Attributes["asciicode"].Value;
					string sBadAscii = Convert.ToChar(int.Parse(sAsciiCode)).ToString();
					
					sSQL = "SELECT " + sTextColumn;
					if( sHtmlTextColumn.Length > 0 )
						sSQL += "," + sHtmlTextColumn;
					sSQL += " FROM " + sTableName + " WHERE " + sIDColumn + "=" + sIDValue;
                    
                    try //mgaba2
                    {
                        oReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    }
                    catch (Exception exc)
                    {
                        //frmOptions.writeLog(string.Format("ERROR:[Error Removing Bad Ascii:] {0} While retreiving from XML,it failed for table : {1} with Primary Column : {2} having value {3) for Text Column : {4} with ascii code : {5}", exc.Message, sTableName, sIDColumn, sIDValue, sTextColumn, sBadAscii));
                        Debug.WriteLine(string.Format("ERROR:[Error Removing Bad Ascii:] {0} While retreiving from XML,it failed for table : {1} with Primary Column : {2} having value {3) for Text Column : {4} with ascii code : {5}", exc.Message, sTableName, sIDColumn, sIDValue, sTextColumn, sBadAscii));
                    }
					if( oReader.Read() )
					{
						sText = oReader.GetString(sTextColumn);
						sText = sText.Replace(sBadAscii, string.Empty);
						
						if( sHtmlTextColumn.Length > 0 )
						{
							sHtmlText = oReader.GetString(sHtmlTextColumn);
							sHtmlText = sHtmlText.Replace(sBadAscii, string.Empty);
						}

						//Update the database record
						objDbWriter = DbFactory.GetDbWriter(m_sConnectionString);
						objDbWriter.Tables.Add(sTableName);
						objDbWriter.Fields.Add(sTextColumn,sText);
						if( sHtmlTextColumn.Length > 0 )
						{
							objDbWriter.Fields.Add(sHtmlTextColumn, sHtmlText);
						}
						objDbWriter.Where.Add(sIDColumn + " =" + sIDValue );
                        
                        try//mgaba2
                        {
                            objDbWriter.Execute();
                        }
                        catch (Exception exc)
                        {
                            //frmOptions.writeLog(string.Format("ERROR:[Error Removing Bad Ascii:] {0} While updating database,it failed for table : {1} with Primary Column : {2} having value {3) for Text Column : {4} with ascii code : {5}", exc.Message, sTableName, sIDColumn, sIDValue, sTextColumn, sBadAscii));
                            Debug.WriteLine(string.Format("ERROR:[Error Removing Bad Ascii:] {0} While updating database,it failed for table : {1} with Primary Column : {2} having value {3) for Text Column : {4} with ascii code : {5}", exc.Message, sTableName, sIDColumn, sIDValue, sTextColumn, sBadAscii));
                        }
						iRecordsNum++;
						showProgress("Process for " + sTableName + "." + sIDColumn + "=" + sIDValue + System.Environment.NewLine);
					}
					oReader.Close();
				}
			}
			catch(Exception e)
			{
				showMessage("Error in removing bad ascii. " + e.Message, true);
				bError = true;
			}
			finally
			{
				if( oReader != null )
					oReader.Close();
			}

			if( !bError )
			{
				showMessage("Done removing bad ascii. Total of " + iRecordsNum.ToString() + " records processed.", false);
			}
            this.showComplete("Done");  //pmittal5
		}

		public bool PauseScan
		{
			set{ m_bPauseScan = value; }
		}
	}
}
