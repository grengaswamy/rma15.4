/****** Object:  StoredProcedure [dbo].[usp_ScanTextFieldNew]    Script Date: 04/28/2008 14:42:04 ******/  
IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[usp_ScanTextFieldNew]') AND xtype in (N'P')) 
DROP PROC [usp_ScanTextFieldNew]

Go 

Create PROCEDURE [usp_ScanTextFieldNew]  
 @table_name varchar(20),  
 @id_col varchar(30),  
 @source_col varchar(30),  
 @idCol_value int,  
 @htmlcomment varchar(30) = null  
AS  
Declare @Sql as varchar(8000)  
  
Declare @newline as char(1)  
  
set @newline = char(10)  
  
SELECT @SQL = 'DECLARE @Row_Id Int ' + @newline +  
'DECLARE @position bigint ' + @newline +  
'DECLARE @COL_VALUE varchar(max)    ' + @newline +  
'DECLARE @Col_Length bigint ' + @newline +  
'SET TEXTSIZE 8000 ' + @newline + 
'DECLARE Clean_cursor CURSOR FOR ' + @newline +  
'SELECT ' +  @id_col +  ' FROM ' +  @table_name +  @newline  
  
IF(@idCol_value <> 0) 
	IF (@table_name = 'EVENT')
		SELECT @SQL = @SQL + ' WHERE EVENT_ID = ' + Cast(@idCol_value as varchar) + @newline  
	ELSE IF(@table_name = 'ENTITY')
		SELECT @SQL = @SQL + ' WHERE ENTITY_ID = ' + Cast(@idCol_value as varchar) + @newline  
	ELSE IF(@table_name = 'PERSON_INVOLVED' OR @table_name = 'PI_X_DIAG_HIST' OR @table_name = 'PI_X_MMI_HIST')
		SELECT @SQL = @SQL + ' WHERE PI_ROW_ID = ' + Cast(@idCol_value as varchar) + @newline 
	ELSE IF(@table_name = 'CLAIM')
		SELECT @SQL = @SQL + ' WHERE CLAIM_ID = ' + Cast(@idCol_value as varchar) + @newline  
  
SELECT @SQL = @SQL + ' OPEN Clean_cursor ' + @newline +  
'FETCH NEXT FROM Clean_cursor ' + @newline +  
'INTO @Row_id ' + @newline +  
'IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N''[TEMP_TABLE]'') AND xtype in (N''U'')) DROP TABLE [TEMP_TABLE]  ' + @newline +  
'CREATE TABLE TEMP_TABLE (ROW_ID INT NULL, ASCII_CODE INT NULL) ' + @newline +  
  
'IF @@FETCH_STATUS = 0 ' + @newline +  
'BEGIN ' + @newline +  
'WHILE @@FETCH_STATUS = 0 ' + @newline +  
'BEGIN ' + @newline +  
'SELECT @position = 1 ' + @newline +  

'Select @Col_value = ' + @source_col + ' from ' + @table_name + ' where ' +  @id_col + ' =  Cast(@Row_Id as varchar)' + @newline + 
'SELECT @Col_Length = DATALENGTH(@Col_value)' + @newline +
'WHILE @position <= @Col_Length ' + @newline +  
'BEGIN ' + @newline +  
'IF ASCII(SUBSTRING(@Col_value, @position, 1)) < 32 AND ASCII(SUBSTRING(@Col_value, @position, 1)) != 9 AND ASCII(SUBSTRING(@Col_value, @position, 1)) != 10 AND ASCII(SUBSTRING(@Col_value, @position, 1)) != 13 ' + @newline +  
'BEGIN ' + @newline +  
'INSERT INTO TEMP_TABLE VALUES (@ROW_ID, ASCII(SUBSTRING(@Col_value, @position, 1))) ' + @Newline +  
'END ' + @newline +  
'SET @position = @position + 1 ' + @newline +  
'END ' + @newline

if(@htmlcomment is not null)  
SELECT @SQL = @SQL + 'SELECT @position = 1 ' + @newline +  

'Select @Col_value = ' + @htmlcomment + ' from ' + @table_name + ' where ' +  @id_col + ' =  Cast(@Row_Id as varchar)' + @newline + 
'SELECT @Col_Length = DATALENGTH(@Col_value)' + @newline +
'WHILE @position <= @Col_Length ' + @newline +  
'BEGIN ' + @newline +  
'IF ASCII(SUBSTRING(@Col_value, @position, 1)) < 32 AND ASCII(SUBSTRING(@Col_value, @position, 1)) != 9 AND ASCII(SUBSTRING(@Col_value, @position, 1)) != 10 AND ASCII(SUBSTRING(@Col_value, @position, 1)) != 13 ' + @newline +  
'BEGIN ' + @newline +  
'INSERT INTO TEMP_TABLE VALUES (@ROW_ID, ASCII(SUBSTRING(@Col_value, @position, 1))) ' + @Newline +  
'END ' + @newline +  
'SET @position = @position + 1 ' + @newline +  
'END ' + @newline
  
SELECT @Sql = @sql + 'FETCH NEXT FROM Clean_cursor INTO @Row_id ' + @newline +  
'END ' + @newline +  
'END ' + @newline +  
'ELSE ' + @newline +  
'BEGIN ' + @newline +  
'PRINT ''-------- No Data in --------'' '+ @newline +  
'END ' + @newline +  
'CLOSE Clean_cursor ' + @newline +  
'DEALLOCATE Clean_cursor '  + @newline +  
'SELECT DISTINCT * FROM TEMP_TABLE '  
  
print @sql
Exec(@Sql)

