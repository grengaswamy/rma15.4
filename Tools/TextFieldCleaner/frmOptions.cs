using System;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using System.IO; //mgaba2

namespace CleanTextFields
{
   public class frmOptions : Form
    {
        private static string m_sDSN = string.Empty;
        private string m_sUserID = string.Empty;
        private string m_sPassword = string.Empty;
       private string m_sConnectionString = string.Empty;
       private eDatabaseType m_DatabaseType;
       private int iClaimId = 0;

       private System.Windows.Forms.Button btnOk;
       private RadioButton rdScanWhole;
       private RadioButton rdScanSpecific;
       private Button btnExit;
       private Label lblError;
       private Label lblClaimNumber;
       private TextBox txtClaimNumber;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new frmOptions());
        }
       //public frmOptions(string p_sConnString)
       // {
       //     InitializeComponent();
       //    //mgaba2: Also changed the constructor,removed the connection type and connection string properties
       //     m_sConnectionString = p_sConnString;
       // }
        public frmOptions()
        {
            InitializeComponent();
             
        }
        private void frmOptions_Load(object sender, System.EventArgs e)
        {
            UserLogin();
            //mgaba2-deleting the log file if already exists : start
            string sPath = Application.ExecutablePath;
            int iLength = sPath.LastIndexOf("\\");
            sPath = sPath.Substring(0, iLength + 1);
            sPath = sPath + "TextFieldsCleaner.log";
            if (File.Exists(sPath))
            {
                File.Delete(sPath);
            }
            //mgaba2-end
        }
        
        private void UserLogin()
        {
            //mgaba2-Used the security login component,also excluded its frmLogin :Start
            //frmLogin objForm = new frmLogin();
            Login m_objLogin = new Login();
            UserLogin m_userLogin = null;
            bool bCon = false;
            bCon = m_objLogin.RegisterApplication(this.Handle.ToInt32(), 0, ref m_userLogin);

            try
            {
                
                if (bCon == true)
                {
                    m_sUserID = m_userLogin.LoginName;
                    m_sPassword = m_userLogin.Password;
                    m_sDSN = m_userLogin.objRiskmasterDatabase.DataSourceName;
                    m_sConnectionString = m_userLogin.objRiskmasterDatabase.ConnectionString;
                    this.Visible = true;
                }
                else
                {
                    if (m_objLogin != null)
                        m_objLogin.Dispose();
                    Application.Exit();
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); };

        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.rdScanWhole = new System.Windows.Forms.RadioButton();
            this.rdScanSpecific = new System.Windows.Forms.RadioButton();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.lblClaimNumber = new System.Windows.Forms.Label();
            this.txtClaimNumber = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(119, 100);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // rdScanWhole
            // 
            this.rdScanWhole.AutoSize = true;
            this.rdScanWhole.Checked = true;
            this.rdScanWhole.Location = new System.Drawing.Point(41, 21);
            this.rdScanWhole.Name = "rdScanWhole";
            this.rdScanWhole.Size = new System.Drawing.Size(128, 17);
            this.rdScanWhole.TabIndex = 3;
            this.rdScanWhole.TabStop = true;
            this.rdScanWhole.Text = "Scan whole database";
            this.rdScanWhole.UseVisualStyleBackColor = true;
            this.rdScanWhole.CheckedChanged += new System.EventHandler(this.rdScanWhole_CheckedChanged);
            // 
            // rdScanSpecific
            // 
            this.rdScanSpecific.AutoSize = true;
            this.rdScanSpecific.Location = new System.Drawing.Point(41, 44);
            this.rdScanSpecific.Name = "rdScanSpecific";
            this.rdScanSpecific.Size = new System.Drawing.Size(134, 17);
            this.rdScanSpecific.TabIndex = 4;
            this.rdScanSpecific.Text = "Scan for Specific Claim";
            this.rdScanSpecific.UseVisualStyleBackColor = true;
            this.rdScanSpecific.CheckedChanged += new System.EventHandler(this.rdScanSpecific_CheckedChanged);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(200, 100);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblError
            // 
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(41, 76);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(362, 18);
            this.lblError.TabIndex = 6;
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblClaimNumber
            // 
            this.lblClaimNumber.AutoSize = true;
            this.lblClaimNumber.Location = new System.Drawing.Point(181, 47);
            this.lblClaimNumber.Name = "lblClaimNumber";
            this.lblClaimNumber.Size = new System.Drawing.Size(75, 13);
            this.lblClaimNumber.TabIndex = 1;
            this.lblClaimNumber.Text = "Claim Number:";
            this.lblClaimNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblClaimNumber.Visible = false;
            // 
            // txtClaimNumber
            // 
            this.txtClaimNumber.Location = new System.Drawing.Point(262, 44);
            this.txtClaimNumber.Name = "txtClaimNumber";
            this.txtClaimNumber.Size = new System.Drawing.Size(141, 20);
            this.txtClaimNumber.TabIndex = 2;
            this.txtClaimNumber.Visible = false;
            // 
            // frmOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 141);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.rdScanSpecific);
            this.Controls.Add(this.rdScanWhole);
            this.Controls.Add(this.txtClaimNumber);
            this.Controls.Add(this.lblClaimNumber);
            this.Controls.Add(this.btnOk);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(439, 175);
            this.MinimumSize = new System.Drawing.Size(439, 175);
            this.Name = "frmOptions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Options";
            this.Load += new System.EventHandler(this.frmOptions_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public static void writeLog(string p_strLogText)
        {//mgaba2:Added a function which appends the input message in the TextFieldsCleaner.log file
            string sPath = Application.ExecutablePath;
            int iLength = sPath.LastIndexOf("\\");
            sPath = sPath.Substring(0, iLength + 1);
            sPath = sPath + "TextFieldsCleaner.log";

            string sMessage = string.Format("[{0}] {1}{2}", System.DateTime.Now.ToString(), p_strLogText, Environment.NewLine);
            try
            {
                File.AppendAllText(sPath, sMessage);
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            finally
            {

            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            string sClaimNumber = string.Empty;
            string sSql = string.Empty;
            DbCommand objCommand = null;
            DbConnection objConnection = null;
            try
            {
                if (rdScanSpecific.Checked == true)
                {
                    sClaimNumber = txtClaimNumber.Text;
                    sSql = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber + "'";
                    objConnection = DbFactory.GetDbConnection(m_sConnectionString);
                    objConnection.Open();
                    m_DatabaseType = objConnection.DatabaseType;//mgaba2
                    objCommand = objConnection.CreateCommand();
                    objCommand.CommandText = sSql;
                    object objResult = objCommand.ExecuteScalar();
                    if (objResult != null)
                    {
                        iClaimId = Conversion.ConvertStrToInteger(objResult.ToString());
                        objResult = null;
                        //mgaba2
                        this.Hide();
                        frmMain objMain = new frmMain(m_sConnectionString, iClaimId);
                        objMain.ShowDialog();
                        objMain = null;
                        this.Show();
                    }
                    else
                    {
                        txtClaimNumber.Focus();
                        throw new Exception("The Claim Number specified does not exist in the Database.");
                    }
                    objConnection.Close();
                }
                else
                {
                    this.Hide();
                    iClaimId = 0;
                    frmMain objMain = new frmMain(m_sConnectionString, iClaimId);
                    objMain.ShowDialog();
                    objMain = null;
                    this.Show();
                }
                
             }
            catch(Exception ex)
			{
                lblError.Text = ex.Message;
			}
			finally
			{
                if(objConnection != null)
                    objConnection.Close();
                objCommand = null;
			}
            
        }

       private void rdScanSpecific_CheckedChanged(object sender, EventArgs e)
       {
           lblError.Text = "";
           if (rdScanSpecific.Checked == true)
           {
               lblClaimNumber.Visible = true;
               txtClaimNumber.Visible = true;
           }
           else
           {
               lblClaimNumber.Visible = false;
               txtClaimNumber.Visible = false;
           }
       }

       private void rdScanWhole_CheckedChanged(object sender, EventArgs e)
       {
           lblError.Text = "";
           if (rdScanWhole.Checked == true)
           {
               lblClaimNumber.Visible = false;
               txtClaimNumber.Visible = false;
           }
           else
           {
               lblClaimNumber.Visible = true;
               txtClaimNumber.Visible = true;
           }
       }

       private void btnExit_Click(object sender, EventArgs e)
       {
           Application.Exit();
       }

       public int ClaimId
       {
           get { return iClaimId; }
           set { iClaimId = value; }
       }

    }
}