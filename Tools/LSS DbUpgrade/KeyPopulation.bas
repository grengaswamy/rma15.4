Attribute VB_Name = "KeyPopulation"
' BSB 04.13.2006
' Routines to ensure that new key columns become fully populated
' during DB Upgrade.
'
'Key columns replacements handled here are:
'
' VEHICLE_X_ACC_DATE (UNIT_ID,ACCIDENT_DATE,CLAIM_ID) --> VEH_X_ACC_DATE_ROW_ID
' POLICY_X_MCO_ENH (POLICY_ID,MCO_EID) --> POL_X_MCO_ENH_ROW_ID
' POLICY_X_MCO (POLICY_ID,MCO_EID) --> POL_X_MCO_ROW_ID
' CLIENT_LIMITS (CLIENT_EID,LOB_CODE,RESERVE_TYPE_CODE,PAYMENT_FLAG) --> CLIENT_LIMITS_ROWID
' CHECK_STOCK_FORMS (STOCK_ID,IMAGE_IDX,FORM_TYPE) --> FORM_ROW_ID
Option Explicit

Public Sub PopulateNewRMXTableKeys()
    PopulateKeys "VEHICLE_X_ACC_DATE", "VEH_X_ACC_DATE_ROW_ID", "UNIT_ID|ACCIDENT_DATE|CLAIM_ID"
    PopulateKeys "POLICY_X_MCO_ENH", "POL_X_MCO_ENH_ROW_ID", "POLICY_ID|MCO_EID|MCO_BEGIN_DATE|MCO_END_DATE"
    PopulateKeys "POLICY_X_MCO", "POL_X_MCO_ROW_ID", "POLICY_ID|MCO_EID|MCO_BEGIN_DATE|MCO_END_DATE"
    PopulateKeys "CLIENT_LIMITS", "CLIENT_LIMITS_ROWID", "CLIENT_EID|LOB_CODE|RESERVE_TYPE_CODE|PAYMENT_FLAG"
    PopulateKeys "CHECK_STOCK_FORMS", "FORM_ROW_ID", "STOCK_ID|IMAGE_IDX|FORM_TYPE"
End Sub

'sColumns -- the columns need to be in the where clause
Public Function PopulateKeys(sTableName As String, sNewKeyField As String, sColumns) As Integer
    Dim iRS1 As Integer
    Dim iRS2 As Integer
    Dim i As Integer
    Dim j As Long
    Dim sSQL As String
    Dim sFields As String
    Dim vData As Variant
    Dim colSQL As Collection
    Dim UID As Long
    Dim sValue As String
    Dim dicColumns As Scripting.Dictionary
    Dim vColumns As Variant
    Dim iCount As Long
    Dim sColumnName As String
    Dim iColumnType As Long
    
    'Check if it is necessary. The user may rerun the DBUpgrade multiple times
    'If the sNewKeyField has null value OR has some duplicate values for sNewKeyField
    sSQL = "SELECT COUNT(*) FROM " + sTableName + " WHERE " + sNewKeyField + " IS NULL"
    iRS1 = DB_CreateRecordset(dbODBC2, sSQL, DB_FORWARD_ONLY, 0)
    If Not DB_EOF(iRS1) Then
        DB_GetData iRS1, 1, vData
        DB_CloseRecordset iRS1, DB_DROP
        iCount = CLng("0" + vData)
        If iCount = 0 Then
            'Check for duplicate values from previous run
            sSQL = "SELECT COUNT(*), " & sNewKeyField & " FROM " & sTableName & _
                " GROUP BY " & sNewKeyField & " HAVING COUNT(*) > 1"
            iRS1 = DB_CreateRecordset(dbODBC2, sSQL, DB_FORWARD_ONLY, 0)
            'If no null value and no duplicate value, exit function
            If DB_EOF(iRS1) Then
                GoTo hExit
            End If
            DB_CloseRecordset iRS1, DB_DROP
        End If
    End If
    
    UID = lGetNextUID(sTableName)
    
    Set dicColumns = New Dictionary
    vColumns = Split(sColumns, "|")
    For i = LBound(vColumns) To UBound(vColumns)
        dicColumns.Add vColumns(i), 1
    Next i
    
    Set colSQL = New Collection
    
    sSQL = "SELECT * FROM " + sTableName
    iRS1 = DB_CreateRecordset(dbODBC2, sSQL, DB_FORWARD_ONLY, 0)
    
    While (Not DB_EOF(iRS1))
        sFields = ""
        For i = 1 To DB_ColumnCount(iRS1)
            sColumnName = DB_ColumnName(iRS1, i)
            iColumnType = DB_ColumnType(iRS1, i)
            If dicColumns.Exists(sColumnName) Then
                DB_GetData iRS1, i, vData
                Select Case iColumnType
                Case -16, -15    'For integer, small int
                    j = CLng("0" & vData)
                    If sFields <> "" Then
                        sFields = sFields & " AND "
                    End If
                    sFields = sFields + DB_ColumnName(iRS1, i) & "=" + CStr(j)
                Case 1    'For string
                    sValue = "" & vData
                    sValue = Replace(sValue, "'", "''")
                    If sFields <> "" Then
                        sFields = sFields & " AND "
                    End If
                    sFields = sFields & DB_ColumnName(iRS1, i) & "='" + sValue & "'"
                End Select
            End If
        Next
        sSQL = "UPDATE " & sTableName & " SET " & sNewKeyField & "=" & CStr(UID) & " WHERE " & sFields
        colSQL.Add sSQL
        UID = UID + 1
        DB_MoveNext iRS1
    Wend
    DB_CloseRecordset iRS1, DB_DROP
        
    'Fix Glossary
    DB_SQLExecute dbODBC2, "UPDATE GLOSSARY SET NEXT_UNIQUE_ID=" & CStr(UID) & " WHERE SYSTEM_TABLE_NAME='" & sTableName + "'"
     
    For Each vData In colSQL
        DB_SQLExecute dbODBC2, CStr(vData)
        PopulateKeys = PopulateKeys + 1
    Next
    
hExit:
On Error Resume Next
    DB_CloseRecordset iRS1, DB_DROP
    Exit Function

hError:
    Dim EResult As Integer
    EResult = iGeneralError(Err, Error$, "KeyMigration.PopulateKeys")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Resume hExit
    End Select
End Function
