﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Application.RiskmasterSettings;
using Riskmaster.Application.ApplicationSettings;

namespace RiskmasterSettingsEditor
{
    public partial class frmRiskmasterSettings : Form
    {
        private string m_strRiskmasterFile = string.Empty;
        private bool m_blnRMXConfiguration = true;

        public frmRiskmasterSettings()
        {
            InitializeComponent();
        }

        #region Event Handlers
        /// <summary>
        /// Handles the OnClick event of the Ribbon toolbar buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void utMgrRiskmasterSettings_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
        {
            switch (e.Tool.Key)
            {
                case "Load Registry Settings": //ButtonTool
                    LoadRMWorldSettings();
                  break;
                case "Load Reporting Settings":    // ButtonTool
                    ofdRMGlobalASAPath.Filter = "Asa files (*.asa)|*.asa";
                    ofdRMGlobalASAPath.FileName = "Global.asa";
                    ofdRMGlobalASAPath.InitialDirectory = @"C:\Program Files\CSC\WIZARDSM\";
                    if (ofdRMGlobalASAPath.ShowDialog() == DialogResult.OK)
                    {
                        string strFileName = ofdRMGlobalASAPath.FileName;

                        //Update the class member variable for the file name
                        m_strRiskmasterFile = strFileName;

                        //Indicate that this is an RMNet configuration
                        m_blnRMXConfiguration = false;

                        LoadRMNetSettings(strFileName);
                    }//if
                    break;
                case "Save":    // ButtonTool
                    if (!m_strRiskmasterFile.Equals(string.Empty))
                    {
                        if (m_blnRMXConfiguration)
                        {
                            UpdateRMXSettings(m_strRiskmasterFile);
                        }//if
                        else
                        {
                            UpdateRMNetSettings(m_strRiskmasterFile);
                        }//else

                        MessageBox.Show("Riskmaster Settings updated successfully.", "Riskmaster Settings Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }//if
                    else
                    {
                        MessageBox.Show("Please select a file prior to saving any information.", "Select a File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }//else
                    
                    break;
                case "IIS Management":    // ButtonTool
                    //Stop and re-start IIS
                    ManageProcesses("inetmgr");
                    break;
                case "Services Management":
                    //Open up the Services Management Console
                    ManageProcesses("services.msc");
                    break;
                case "ODBC Management":
                    //Open up the ODBC Data Sources Management Console
                    ManageProcesses("odbcad32");
                    break;
            }//switch

        }//event: utMgrRiskmasterSettings_ToolClick 
        #endregion

        /// <summary>
        /// Manages various external processes to the application
        /// </summary>
        /// <param name="strProcCommand"></param>
        private void ManageProcesses(string strProcCommand)
        {
            Process objProc;

            //Set the Process parameters
            ProcessStartInfo objProcStartInfo = new ProcessStartInfo(strProcCommand);

            //Run the process
            objProc = Process.Start(objProcStartInfo);

            //Clean up
            objProcStartInfo = null;
            objProc = null;
        }//method: ManageProcesses()

        #region Load Settings Methods
        /// <summary>
        /// Loads RISKMASTER X settings
        /// </summary>
        /// <param name="strRMConfigPath">string containing the file path to the Riskmaster.config file</param>
        private void LoadRMXSettings(string strRMConfigPath)
        {
            RMConfigurationSettings objRMConfiguration = new RMConfigurationSettings(strRMConfigPath);

            LoadRiskmasterSettings objLoadRMSettings = new LoadRiskmasterSettings(objRMConfiguration.GlobalASAPath, objRMConfiguration.RMConfigurationPath);

            try
            {
                //Load the individual settings
                objLoadRMSettings.LoadRegistrySettings();
                objLoadRMSettings.LoadGlobalASASettings();

                //Populate the Security credentials
                txtSecUID.Text = objLoadRMSettings.SecurityUID;
                txtSecPwd.Text = objLoadRMSettings.SecurityPwd;

                //Populate the SMServer credentials
                txtSMServerUID.Text = objLoadRMSettings.SMServerUID;
                txtSMServerPwd.Text = objLoadRMSettings.SMServerPwd;

                //Populate the SMTP server value
                txtSMTPServer.Text = objLoadRMSettings.SMTPServer;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error occurred in Application.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //Clean up
                objLoadRMSettings = null;
                objRMConfiguration = null;
            }//finally
        }//method: LoadRMXSettings()

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strGlobalASAPath"></param>
        private void LoadRMNetSettings(string strGlobalASAPath)
        {

            LoadRiskmasterSettings objLoadRMSettings = new LoadRiskmasterSettings(strGlobalASAPath, string.Empty);

            try
            {
                //Load the individual settings
                objLoadRMSettings.LoadGlobalASASettings();


                //Populate the relevant connection strings
                txtRMXSecurityConnString.Text = objLoadRMSettings.SecurityConnectionString;
                txtRMXSessionConnString.Text = objLoadRMSettings.SessionConnectionString;

                ////Populate the Security credentials
                txtSecUID.Text = objLoadRMSettings.SecurityUID;
                txtSecPwd.Text = objLoadRMSettings.SecurityPwd;

                //Populate the SMServer credentials
                txtSMServerUID.Text = objLoadRMSettings.SMServerUID;
                txtSMServerPwd.Text = objLoadRMSettings.SMServerPwd;

                //Populate the SMTP server value
                txtSMTPServer.Text = objLoadRMSettings.SMTPServer;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error occurred in Application.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //Clean up
                objLoadRMSettings = null;
            }//finally
        }//method: LoadRMNetSettings() 

        /// <summary>
        /// Loads the relevant Riskmaster World settings
        /// </summary>
        private void LoadRMWorldSettings()
        {

            LoadRiskmasterSettings objLoadRMSettings = new LoadRiskmasterSettings(string.Empty, string.Empty);

            try
            {
                //Load the individual settings
                objLoadRMSettings.LoadRegistrySettings();

                //Populate the Security credentials
                txtSecUID.Text = objLoadRMSettings.SecurityUID;
                txtSecPwd.Text = objLoadRMSettings.SecurityPwd;

                //Populate the SMServer credentials
                txtSMServerUID.Text = objLoadRMSettings.SMServerUID;
                txtSMServerPwd.Text = objLoadRMSettings.SMServerPwd;

                //Populate the SMTP server value
                txtSMTPServer.Text = objLoadRMSettings.SMTPServer;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error occurred in Application.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //Clean up
                objLoadRMSettings = null;
            }//finally
        }//method: LoadRMWorldSettings() 
        #endregion

        #region Update Settings Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strGlobalASAPath"></param>
        private void UpdateRMNetSettings(string strGlobalASAPath)
        {

            UpdateRiskmasterSettings objUpdateRMSetttings = new UpdateRiskmasterSettings(strGlobalASAPath, string.Empty);

            try
            {
                //Populate the object with the relevant updated credentials
                //Populate the connection strings
                objUpdateRMSetttings.SecurityConnectionString = txtRMXSecurityConnString.Text;
                objUpdateRMSetttings.SessionConnectionString = txtRMXSessionConnString.Text;

                //Populate the Security credentials
                objUpdateRMSetttings.SecurityUID = txtSecUID.Text;
                objUpdateRMSetttings.SecurityPwd = txtSecPwd.Text;

                //Populate the SMServer credentials
                objUpdateRMSetttings.SMServerUID = txtSMServerUID.Text;
                objUpdateRMSetttings.SMServerPwd = txtSMServerPwd.Text;

                //Populate the SMTP server value
                objUpdateRMSetttings.SMTPServer = txtSMTPServer.Text;

                //Update all the required files
                objUpdateRMSetttings.UpdateRegistrySettings();
                objUpdateRMSetttings.UpdateGlobalASASettings();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error occurred in Application.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //Clean up
                objUpdateRMSetttings = null;
            }//finally
        }//method: UpdateRMNetSettings()

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strRMConfigPath"></param>
        private void UpdateRMXSettings(string strRMConfigPath)
        {
            RMConfigurationSettings objRMConfiguration = new RMConfigurationSettings(strRMConfigPath);

            UpdateRiskmasterSettings objUpdateRMSetttings = new UpdateRiskmasterSettings(objRMConfiguration.GlobalASAPath, objRMConfiguration.RMConfigurationPath);

            try
            {
                //Populate the object with the relevant updated credentials

                //Populate the Security credentials
                objUpdateRMSetttings.SecurityUID = txtSecUID.Text;
                objUpdateRMSetttings.SecurityPwd = txtSecPwd.Text;

                //Populate the SMServer credentials
                objUpdateRMSetttings.SMServerUID = txtSMServerUID.Text;
                objUpdateRMSetttings.SMServerPwd = txtSMServerPwd.Text;

                //Populate the SMTP server value
                objUpdateRMSetttings.SMTPServer = txtSMTPServer.Text;

                //Update all the required files
                objUpdateRMSetttings.UpdateRegistrySettings();
                objUpdateRMSetttings.UpdateGlobalASASettings();
                objUpdateRMSetttings.UpdateRiskmasterConfigSettings();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error occurred in Application.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //Clean up
                objUpdateRMSetttings = null;
                objRMConfiguration = null;
            }//finally
        }//method: UpdateRMXSettings 
        #endregion
    }
}
