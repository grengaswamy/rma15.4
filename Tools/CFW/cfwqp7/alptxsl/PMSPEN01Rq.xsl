<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--<xsl:template match="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq" mode ="PMSPEN01">--><!--103409-->
<xsl:template match="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq" mode ="PMSPEN01">	<!--103409-->
	<BUS__OBJ__RECORD>
		<RECORD__NAME__ROW>
			<RECORD__NAME>PMSPEN01</RECORD__NAME>
		</RECORD__NAME__ROW>
		<PMSPEN01__RECORD>
			<xsl:variable name="Ind1" select="ModInfo[ActionCd = 'InsuredNameChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind5" select="ModInfo[ActionCd = 'InsuredMailingAddressChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind6" select="ModInfo[ActionCd = 'ExperienceModificationChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind7" select="ModInfo[ActionCd = 'ProducerNameChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind8" select="ModInfo[ActionCd = 'InsuredWorkPlaceChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind9" select="ModInfo[ActionCd = 'InsuredLegalStatusChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind10" select="ModInfo[ActionCd = 'Item3AStatesChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind11" select="ModInfo[ActionCd = 'Item3BLimitsChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind12" select="ModInfo[ActionCd = 'Item3CStatesChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind13" select="ModInfo[ActionCd = 'Item3DEndorsementNbrChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind14" select="ModInfo[ActionCd = 'Item4OtherChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind15" select="ModInfo[ActionCd = 'InterimAdjPremiumChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind16" select="ModInfo[ActionCd = 'CarrierServicingOfficeChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind17" select="ModInfo[ActionCd = 'RiskIdentifierChgInd']/ChangeDesc"></xsl:variable>
			<xsl:variable name="Ind18" select="ModInfo[ActionCd = 'CarrierNumberChgInd']/ChangeDesc"></xsl:variable>
			<LOC>
				<xsl:value-of select="$LOC"/>
			</LOC>
			<MCO>
				<xsl:value-of select="$MCO"/>
			</MCO>
			<SYMBOL>
				<xsl:value-of select="$SYM"/>
			</SYMBOL>
			<POLICYNO>
				<xsl:value-of select="$POL"/>
			</POLICYNO>
			<MODULE>
				<xsl:value-of select="$MOD"/>
			</MODULE>
			<IND1>
				<xsl:choose>
					<xsl:when test="$Ind1 = 'M'">X</xsl:when>
				</xsl:choose> 
				</IND1>
			<IND5>
				<xsl:choose>
					<xsl:when test="$Ind5 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND5>
			<IND6>
				<xsl:choose>
					<xsl:when test="$Ind6 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND6>
			<IND7>
				<xsl:choose>
					<xsl:when test="$Ind7 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND7>
			<IND8>
				<xsl:choose>
					<xsl:when test="$Ind8 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND8>
			<IND9>
				<xsl:choose>
					<xsl:when test="$Ind9 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND9>
			<IND10>
				<xsl:choose>
					<xsl:when test="$Ind10 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND10>
			<IND11>
				<xsl:choose>
					<xsl:when test="$Ind11 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND11>
			<IND12>
				<xsl:choose>
					<xsl:when test="$Ind12 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND12>
			<IND13>
				<xsl:choose>
					<xsl:when test="$Ind13 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND13>
			<IND14>
				<xsl:choose>
					<xsl:when test="$Ind14 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND14>
			<IND15>
				<xsl:choose>
					<xsl:when test="$Ind15 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND15>
			<IND16>
				<xsl:choose>
					<xsl:when test="$Ind16 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND16>
			<IND17>
				<xsl:choose>
					<xsl:when test="$Ind17 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND17>
			<IND18>
				<xsl:choose>
					<xsl:when test="$Ind18 = 'M'">X</xsl:when>
				</xsl:choose>
			</IND18>
			<FREEFORM>
				<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/RemarkText[@IdRef = 'remarks']"/>-->		<!--103409-->
				<xsl:value-of select="/ACORD/InsuranceSvcRq/WorkCompPolicyModRq/RemarkText"/>				<!--103409-->
			</FREEFORM>
			<DECPRT1>
				<xsl:if test="$Ind1 = 'M' or $Ind5 = 'M' or $Ind6 = 'M' or $Ind7 = 'M' or $Ind9 = 'M' or $Ind10 = 'M' or $Ind11 = 'M' or $Ind12 = 'M' or $Ind16 = 'M' or $Ind17 = 'M' or $Ind18 = 'M'">X</xsl:if>
			</DECPRT1>
			<DECPRT2>
				<xsl:if test="$Ind8 = 'M'">X</xsl:if>
			</DECPRT2>
			<DECPRT3>
				<xsl:if test="$Ind6 = 'M' or $Ind14 = 'M' or $Ind15 = 'M'">X</xsl:if>
			</DECPRT3>
			<DECPRT4>
				<xsl:if test="$Ind13='M'">X</xsl:if>
			</DECPRT4>
		</PMSPEN01__RECORD>
	</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet> 
