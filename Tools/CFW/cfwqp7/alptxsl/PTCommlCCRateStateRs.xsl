<?xml version="1.0"?><!-- Issue 69110 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

    <xsl:template name="CreateCommlCCRateState">
            <xsl:variable name="ASBB" select="//ASBBCPL1__RECORD"/>
             <CommlRateState>
               <StateProvCd><xsl:value-of select="B5BCCD"/></StateProvCd>
               <CommlAutoHiredInfo>
                  <HiredLiabilityCostAmt>
                     <Amt><xsl:value-of select="$ASBB/BBCIVA"/></Amt>
                  </HiredLiabilityCostAmt>
                   <xsl:for-each select="//ASBYCPL1__RECORD[substring(BYAOTX,1,4)='HIRE']">
                    <CommlCoverage>
                     <CoverageCd><xsl:value-of select="BYAOTX"/></CoverageCd>
                     <CoverageDesc/>
                     <xsl:if test="string-length(BYAGVA) > 0">
                     <Limit>
                        <FormatCurrencyAmt>
                           <Amt><xsl:value-of select="BYAGVA"/></Amt>
                        </FormatCurrencyAmt>
                     </Limit>
                     </xsl:if>
                     <xsl:choose> 
					 <xsl:when test="BYAOTX = 'HIRECM'">
					 <Deductible>
                        <FormatCurrencyAmt>
                           <Amt>
						   <xsl:value-of select="//ASBBCPL1__RECORD[BBAGTX = 'CA']/BBPOTX"/>	<!--UNI00042VQ3-->
						   </Amt>
                        </FormatCurrencyAmt>
                        <DeductibleTypeCd>FL</DeductibleTypeCd>
                     </Deductible>
					 </xsl:when>
					 <xsl:when test="BYAOTX = 'HIRECO'">
					 <Deductible>
                        <FormatCurrencyAmt>
                           <Amt>
						   <xsl:value-of select="//ASBBCPL1__RECORD[BBAGTX = 'CA']/BBEGCD"/>	<!--UNI00042VQ3-->
						   </Amt>
                        </FormatCurrencyAmt>
                        <DeductibleTypeCd>FL</DeductibleTypeCd>
                     </Deductible>
					 </xsl:when>
					 <xsl:otherwise>                 
                     <Deductible>
                        <FormatCurrencyAmt>
                           <Amt><xsl:value-of select="BYA9NB"/></Amt>
                        </FormatCurrencyAmt>
                        <DeductibleTypeCd>FL</DeductibleTypeCd>
                     </Deductible>
					 </xsl:otherwise>
                     </xsl:choose>
                     <CurrentTermAmt>
                        <Amt><xsl:value-of select="BYA3VA"/></Amt>
                     </CurrentTermAmt>
                  </CommlCoverage>
                  </xsl:for-each>                     
               </CommlAutoHiredInfo>
               <CommlAutoNonOwnedInfo>
                  <NonOwnedInfo>
                     <NonOwnedGroupTypeCd/>
                     <NumNonOwned><xsl:value-of select="$ASBB/BBBKNB"/></NumNonOwned>
                     <xsl:for-each select="//ASBYCPL1__RECORD[substring(BYAOTX,1,3)='NON']">
                     <CommlCoverage>
                     <CoverageCd><xsl:value-of select="BYAOTX"/></CoverageCd>
                     <CoverageDesc/>
                     <xsl:if test="string-length(BYAGVA) > 0">
                     <Limit>
                        <FormatCurrencyAmt>
                           <Amt><xsl:value-of select="BYAGVA"/></Amt>
                        </FormatCurrencyAmt>
                     </Limit>
                     </xsl:if>
                     <xsl:if test="string-length(BYA9NB) > 0">                    
                     <Deductible>
                        <FormatCurrencyAmt>
                           <Amt><xsl:value-of select="BYA9NB"/></Amt>
                        </FormatCurrencyAmt>
                        <DeductibleTypeCd>FL</DeductibleTypeCd>
                     </Deductible>
                     </xsl:if>
                     <CurrentTermAmt>
                        <Amt><xsl:value-of select="BYA3VA"/></Amt>
                     </CurrentTermAmt>
                  </CommlCoverage>
                  </xsl:for-each>                     
                  </NonOwnedInfo>
               </CommlAutoNonOwnedInfo>
               <CommlAutoDriveOtherCarInfo>
                  <NumIndividualsCovered><xsl:value-of select="$ASBB/BBUSNB3"/></NumIndividualsCovered>
                     <xsl:for-each select="//ASBYCPL1__RECORD[substring(BYAOTX,1,3)='DOC']">
                     <CommlCoverage>
                     <CoverageCd><xsl:value-of select="BYAOTX"/></CoverageCd>
                     <CoverageDesc/>
                     <xsl:if test="string-length(BYAGVA) > 0">
                     <Limit>
                        <FormatCurrencyAmt>
                           <Amt><xsl:value-of select="BYAGVA"/></Amt>
                        </FormatCurrencyAmt>
                     </Limit>
                     </xsl:if>
                     <xsl:if test="string-length(BYA9NB) > 0">                    
                     <Deductible>
                        <FormatCurrencyAmt>
                           <Amt><xsl:value-of select="BYA9NB"/></Amt>
                        </FormatCurrencyAmt>
                        <DeductibleTypeCd>FL</DeductibleTypeCd>
                     </Deductible>
                     </xsl:if>
                     <xsl:if test="BYAOTX='DOC-CM' or substring(BYAOTX,1,5)='DOC-L'">
                     <Option>
                        <OptionTypeCd>YNInd1</OptionTypeCd>
                        <OptionCd/>
                        <OptionValue>Y</OptionValue>
                     </Option>
                     </xsl:if>                     
                     <CurrentTermAmt>
                        <Amt><xsl:value-of select="BYA3VA"/></Amt>
                     </CurrentTermAmt>
                  </CommlCoverage>
                  </xsl:for-each>                                       
               </CommlAutoDriveOtherCarInfo>
		       <!-- Issue 65809 Begin-->
			   <xsl:for-each select="key('StateKey', B5BCCD)">
		                 <xsl:call-template name="CreateCommlVeh">
	        	         </xsl:call-template>
		           </xsl:for-each>
			<!-- Issue 65809 Ends-->             
            </CommlRateState>               
    </xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->