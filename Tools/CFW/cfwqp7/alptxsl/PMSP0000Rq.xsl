<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="ACORD/InsuranceSvcRq/*/*" mode="PMSP0000">
       <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>PMSP0000</RECORD__NAME>
         </RECORD__NAME__ROW>
         <PMSP0000__RECORD>
            <SYMBOL><xsl:value-of select="$SYM"/></SYMBOL>
            <POLICY0NUM><xsl:value-of select="$POL"/></POLICY0NUM>
            <MODULE><xsl:value-of select="$MOD"/></MODULE>
            <MASTER0CO><xsl:value-of select="$MCO"/></MASTER0CO>
            <LOCATION><xsl:value-of select="$LOC"/></LOCATION>
            <!-- 34771 start -->
			<!--<TYPE0ACT>NB</TYPE0ACT>-->
			<TYPE0ACT>
				<!--103409 starts-->
				<!--<xsl:choose>
				<xsl:when test="com.csc_AmendmentMode = 'N'">NB</xsl:when>
				<xsl:when test="com.csc_AmendmentMode = 'A'">EN</xsl:when>
				<xsl:when test="com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
				<xsl:when test="com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
				<xsl:when test="com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
				<xsl:when test="com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
			      <xsl:otherwise>NB</xsl:otherwise>
				</xsl:choose>-->
				
				<xsl:call-template name="ConvertPolicyStatusCd">
					<xsl:with-param name="PolicyStatusCd">
						<xsl:value-of select="//PolicyStatusCd"/>
					</xsl:with-param>
				</xsl:call-template>
				<!--103409 Ends-->
			</TYPE0ACT>
			<!-- 34771 end -->
            <ACT0DT>
			<!-- 50764 start-->
			<xsl:choose> 
				<!--<xsl:when test="com.csc_AmendmentMode = 'R' or com.csc_AmendmentMode = 'NR'">-->	<!--103409-->
					<xsl:when test="PolicyStatusCd = 'R' or PolicyStatusCd = 'NR'">		<!--103409-->
					<xsl:call-template name="ConvertISODateToPTDate">
	                  		<xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/>
	                		</xsl:call-template>
				</xsl:when>		
				<xsl:otherwise>	
			<!-- 50764 end-->
		            	<xsl:call-template name="ConvertISODateToPTDate">
	                  	<!-- 29653 Start -->
	                  	<!--<xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/> -->
	                  	<xsl:with-param name="Value" select="../TransactionEffectiveDt"/>
	                  	<!-- 29653 End   -->
	               		</xsl:call-template>
				</xsl:otherwise>	<!-- 50764 -->
			</xsl:choose>		<!-- 50764 -->
            </ACT0DT>
            <OPER0ID>INTER</OPER0ID>
            <COMPANY0NO>
			<!-- Issue 59878 Start -->
			<xsl:if test="$LOB != 'CPP'">
				<xsl:value-of select="$PCO"/>
			</xsl:if>
			<!-- Issue 59878 Start -->
			</COMPANY0NO>
            <TRANS0STAT>U</TRANS0STAT>
            <ETIME>0000</ETIME>
            <ENT0DT>
				<!-- 39039 start-->
			<xsl:choose> 
				<!--<xsl:when test="com.csc_AmendmentMode = 'RI'">-->		<!--103409-->
					<xsl:when test="PolicyStatusCd = 'RI'">		<!--103409-->
				<xsl:call-template name="ConvertISODateToPTDate">
	                  		<xsl:with-param name="Value" select="../TransactionEffectiveDt"/>
	                	</xsl:call-template>
			</xsl:when>		
			<xsl:otherwise>	
			<!-- 39039 end-->
				<xsl:call-template name="ConvertISODateToPTDate">
					<!-- Case 34771 start
                    			<xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/>-->
                    		        <xsl:with-param name="Value" select="../TransactionRequestDt"/>
                    			<!-- Case 34771 end -->  
				</xsl:call-template>
			</xsl:otherwise>	<!-- 39039 -->
			</xsl:choose>		<!-- 39039 -->
			</ENT0DT>
            <xsl:variable name="EffDt">
				<!--34771 start -->
				<xsl:choose>
				<!--<xsl:when test="com.csc_AmendmentMode = 'N'">--><!--103409-->
				<xsl:when test="PolicyStatusCd = 'N'"><!--103409-->
               <xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/>
               </xsl:call-template>
				</xsl:when>
				<!-- 86926 start-->
				<!--<xsl:when test="com.csc_AmendmentMode = 'B'">--><!--103409-->
				<xsl:when test="PolicyStatusCd = 'B'"><!--103409-->
               <xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/>
               </xsl:call-template>
				</xsl:when>
				<!-- 86926 end-->
				<!-- 50764 start -->
				<!--<xsl:when test="com.csc_AmendmentMode = 'R'">--><!--103409-->
				<xsl:when test="PolicyStatusCd = 'R'"><!--103409-->
                <xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/>
                </xsl:call-template>
				</xsl:when>
				<!--<xsl:when test="com.csc_AmendmentMode = 'NR'">--><!--103409-->
				<xsl:when test="PolicyStatusCd = 'NR'"><!--103409-->
                <xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/>
                </xsl:call-template>
				</xsl:when>
				<!-- 50764 end -->
				<!-- 39039 start -->
				<!--<xsl:when test="com.csc_AmendmentMode = 'CN'">--><!--103409-->
				<xsl:when test="PolicyStatusCd = 'CN'"><!--103409-->
                <xsl:call-template name="ConvertISODateToPTDate">
		  <!-- 63722 Start -->
                  <!--<xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/> -->
		   <xsl:with-param name="Value" select="../TransactionEffectiveDt"/>
		  <!-- 63722 End -->
                </xsl:call-template>
				</xsl:when>
				<!-- 39039 End -->
				<!-- 39039 start -->
				<!--<xsl:when test="com.csc_AmendmentMode = 'RI'">--><!--103409-->
				<xsl:when test="PolicyStatusCd = 'RI'"><!--103409-->
                <xsl:call-template name="ConvertISODateToPTDate">
		  <!-- 63722 Start -->
                  <!--<xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/> -->
                  <xsl:with-param name="Value" select="../TransactionEffectiveDt"/>
		  <!-- 63722 End -->
                </xsl:call-template>
				</xsl:when>
				<!-- 39039 End -->
				<!--<xsl:when test="com.csc_AmendmentMode = 'A'">--><!--103409-->
				<xsl:when test="PolicyStatusCd = 'A'"><!--103409-->
					<xsl:call-template name="ConvertISODateToPTDate">
                 	 <xsl:with-param name="Value" select="../TransactionEffectiveDt"/>
               		</xsl:call-template>
				</xsl:when>
				</xsl:choose>
               <!--34771 start -->
             </xsl:variable>            
            <ERROR014><xsl:value-of select="substring($EffDt,1,4)"/></ERROR014>
            <ERROR015><xsl:value-of select="substring($EffDt,5,3)"/></ERROR015>            
			<!-- 50764 start -->
			 <!-- <ERROR019>QUOT</ERROR019> -->
			<xsl:choose>
				<xsl:when test="$TYPEACT='RB'">
					<ERROR019></ERROR019>
				</xsl:when>
				<xsl:otherwise>
					<ERROR019>QUOT</ERROR019>
				</xsl:otherwise>
			</xsl:choose>
			<!-- 50764 end -->
            <REC0CNT>00</REC0CNT>
         </PMSP0000__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->