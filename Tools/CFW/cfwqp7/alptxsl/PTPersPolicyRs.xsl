<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

    <xsl:template match="/*/PMSP0200__RECORD" mode="PersPolicyPA">
        <PersPolicy id="policy">
            <PolicyNumber>
                  <xsl:value-of select="POLICY0NUM"/>
            </PolicyNumber>
            <PolicyVersion>
                  <xsl:value-of select="MODULE"/>
            </PolicyVersion>
            <CompanyProductCd>
                  <xsl:value-of select="SYMBOL"/>
            </CompanyProductCd>
            <LOBCd>APV</LOBCd>
            <NAICCd>
                  <xsl:value-of select="MASTER0CO"/>
            </NAICCd>
            <ControllingStateProvCd>
	         <xsl:call-template name="ConvertNumericStateCdToAlphaStateCd">
   	  		<xsl:with-param name="Value" select="RISK0STATE"/>
   		   </xsl:call-template>
            </ControllingStateProvCd>
            <ContractTerm> 
                <EffectiveDt>
                    <xsl:call-template name="ConvertPTDateToISODate">
                       <xsl:with-param name="Value"  select="EFF0DT"/>
                    </xsl:call-template>
                </EffectiveDt> 
                <ExpirationDt>
                    <xsl:call-template name="ConvertPTDateToISODate">
                       <xsl:with-param name="Value"  select="EXP0DT"/>
                    </xsl:call-template>
                </ExpirationDt>
                <!-- 55614 begin -->
                <DurationPeriod>
					   <NumUnits><xsl:value-of select="INSTAL0TRM"/></NumUnits>
				    </DurationPeriod>
				    <!-- 55614 end -->
            </ContractTerm>
            <BillingMethodCd/>
            <CurrentTermAmt>
                <Amt>
                         <!-- xsl:value-of select="TOT0AG0PRM"/ -->
		  <xsl:choose>
			<xsl:when test="count(//PMSP0400__RECORD) > 0">
				<xsl:value-of select="/*/PMSP0400__RECORD/NEWWRTPRM"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="TOT0AG0PRM"/>
			</xsl:otherwise>
		  </xsl:choose>             
                </Amt>
            </CurrentTermAmt>
            <OtherInsuranceWithCompanyCd/>
            <xsl:for-each select="/*/ASBECPL1__RECORD">
                <xsl:call-template name="BuildForm"/>
            </xsl:for-each>
            <OtherOrPriorPolicy>
                <PolicyCd/> 
                <PolicyNumber/>
                <LOBCd/> 
                <InsurerName/>
                <ContractTerm> 
                    <EffectiveDt/>
                    <ExpirationDt/>
                </ContractTerm>
                <PolicyAmt>
                    <Amt/>
                </PolicyAmt>
            </OtherOrPriorPolicy>
            <PaymentOption>
                <PaymentPlanCd><xsl:value-of select="PAY0CODE"/><xsl:value-of select="MODE0CODE"/></PaymentPlanCd>
                <MethodPaymentCd/>
                <DepositAmt>
                    <Amt/>
                </DepositAmt>
            </PaymentOption>
					<!--113617 start-->
					<AccountNumberId>
						<xsl:value-of select="CUST0NO"/>
					</AccountNumberId>
					<!--<AccountNumberId/>-->
					<!--113617 end-->
			<xsl:for-each select="/*/ASBMCPL1__RECORD[BMC7ST != 'Y']">	
                    <xsl:call-template name="CreateAccidentViolation"/>                    
           </xsl:for-each>
            <xsl:for-each select="/*/ASBLCPL1__RECORD[BLC7ST != 'Y']">
                    <xsl:call-template name="CreateDriverVeh"/>                    
           </xsl:for-each>
            <com.csc_CompanyPolicyProcessingId>
                   <xsl:value-of select="LOCATION"/>
            </com.csc_CompanyPolicyProcessingId>
            <com.csc_InsuranceLineIssuingCompany>
                  <xsl:value-of select="COMPANY0NO"/>
            </com.csc_InsuranceLineIssuingCompany>
            <com.csc_PolicyTermMonths>
                  <xsl:value-of select="INSTAL0TRM"/>
            </com.csc_PolicyTermMonths>
        </PersPolicy>
    </xsl:template>        
</xsl:stylesheet>

