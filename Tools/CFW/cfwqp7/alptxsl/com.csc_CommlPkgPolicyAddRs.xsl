<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="CommonFuncRs.xsl"/>
	 <xsl:include href="PTErrorRs.xsl"/>
    <xsl:include href="PTSignOnRs.xsl"/>
    <xsl:include href="PTProducerRs.xsl"/>
    <xsl:include href="PTInsuredOrPrincipalRs.xsl"/>
    <xsl:include href="PTCommlPolicyRs.xsl"/>
    <xsl:include href="PTFormsRs.xsl"/>
   
    <xsl:include href="PTAdditionalInterestRs.xsl"/>
    <xsl:include href="PTLocationRs.xsl"/>
    <xsl:include href="PTSubLocationRs.xsl"/>
    <xsl:include href="PTCommlSubLocationRs.xsl"/>
    <xsl:include href="PTCommlPropertyRs.xsl"/>
    <xsl:include href="PTGeneralLiabilityRs.xsl"/>
    <xsl:include href="PTInlandMarineRs.xsl"/>
    <xsl:include href="PTCommlCrimeRs.xsl"/>
	<!-- Issue 59878 - Start -->
	<!--xsl:include href="PTCPPFormsRs.xsl"/-->
	<xsl:include href="PTCommlRateStateRs.xsl"/>
    <xsl:include href="PTCommlCCRateStateRs.xsl"/>		<!-- UNI00948 -->
    <xsl:include href="PTCommlVehRs.xsl"/>
    <xsl:include href="PTCommlCoverageRs.xsl"/>
    <xsl:include href="PTCoveredAutoSymbolRs.xsl"/>
    <xsl:include href="PTTruckersSupplementRs.xsl"/> 
	<!-- Issue 59878 - End -->


    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
	<!-- Issue 59878 - Start -->
	<xsl:key name="StateKey" match="ASB5CPL1__RECORD" use="B5BCCD"/>   <!-- Unit/State Key -->
    <xsl:key name="VehKey" match="ASBYCPL1__RECORD" use="BYAENB"/>   <!-- Coverage/Vehicle Key -->
    <xsl:key name="ILKey" match="ASBYCPL1__RECORD" use="BYAGTX"/>   <!-- Coverage/Insurance Line Key -->
	<!-- Issue 59878 - End -->

    <xsl:template match="/">
        <ACORD>
            <xsl:call-template name="BuildSignOn"/>
            <InsuranceSvcRs>
                <RqUID/>
                <com.csc_CommlPkgPolicyAddRs>
                    <RqUID/>
                    <TransactionResponseDt/>
                    <CurCd>USD</CurCd>
    			    <xsl:call-template name="PointErrorsRs"/>
                    <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Producer"/>
                    <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="Business"/>
                    <xsl:apply-templates select="/*/PMSP0200__RECORD" mode="CommlPolicy"/>
                    <xsl:for-each select="/*/ASBUCPL1__RECORD">
                        <xsl:call-template name="CreateLocation"/>
                    </xsl:for-each>
                    <xsl:for-each select="/*/ASBVCPL1__RECORD">
                            <xsl:call-template name="CreateCommlSubLocation"/>
                    </xsl:for-each>                     
                        <xsl:for-each select="/*/ASBBCPL1__RECORD">
                            <xsl:variable name="InsLine" select="BBAGTX"/>
                            <xsl:choose>
                                <xsl:when test="$InsLine='CF'">
                                    <xsl:call-template name="CreateCommlProperty">
                                    <xsl:with-param name="InsLine">CF</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="$InsLine='GL'">
                                    <xsl:call-template name="CreateGeneralLiability"/>
                                </xsl:when>
                                <xsl:when test="$InsLine='CR'">
                                    <xsl:call-template name="CreateCommlCrime"/>
                                </xsl:when>
                                <xsl:when test="$InsLine='IMC'">
                                    <xsl:call-template name="CreateInlandMarine"/>
                                </xsl:when>
                                <xsl:otherwise/>
                            </xsl:choose>
                        </xsl:for-each>
					<!-- Issue 59878 - Start -->
					<CommlAutoLineBusiness>
                     <LOBCd>CA</LOBCd>
                      	<xsl:apply-templates select="/*/ASCSCPL1__RECORD"/>	
	                     <xsl:choose>
	                      <xsl:when test="com.csc_CommlPkgPolicyAddRs/ASB5CPL1__RECORD/B5AENB > 0">
							<xsl:for-each select="com.csc_CommlPkgPolicyAddRs/ASB5CPL1__RECORD[count(. | key('StateKey', B5BCCD)[1])=1]">
								<xsl:sort select="B5BCCD"/>
								<xsl:call-template name="CreateCommlRateState"/>
        	               </xsl:for-each>
        	              </xsl:when>
        	             <xsl:otherwise>
							<xsl:for-each select="com.csc_CommlPkgPolicyAddRs/ASB5CPL1__RECORD[count(. | key('StateKey', B5BCCD)[1])=1]">
								<xsl:sort select="B5BCCD"/>
 		        	               		<xsl:call-template name="CreateCommlCCRateState"/>
						   	</xsl:for-each>    
        	             </xsl:otherwise>
        	            </xsl:choose>
                           <xsl:for-each select="/*/ASBYCPL1__RECORD[BYAOTX='CTCOMP']">
                           <xsl:call-template name="CreateCommlCoverage"/>
        	               </xsl:for-each>
                           <xsl:for-each select="/*/ASBYCPL1__RECORD[BYAOTX='CTLIAB']">
                           <xsl:call-template name="CreateCommlCoverage"/>
        	               </xsl:for-each>
                           <xsl:for-each select="/*/ASBYCPL1__RECORD[BYAOTX='CAEXP']">
                           		<xsl:call-template name="CreateCommlCoverage"/>
        	               </xsl:for-each>
						   <xsl:for-each select="/*[1]/ASBYCPL1__RECORD[BYAOTX='HIRREN']">
						   <xsl:if test="BYAENB ='0'">
                           <xsl:call-template name="CreateCommlCoverage"/>
						   </xsl:if>
						   </xsl:for-each>
                       	<xsl:call-template name="CreateTrailerInterchange"/>
						<!-- Build Forms -->
						 <!--Issue 102807 commented out starts-->
				<!--xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='CA']">		
  				<xsl:call-template name="BuildCPPForm" />
   			</xsl:for-each-->
   			<!--Issue 102807 commented out ends-->
	            </CommlAutoLineBusiness>
				<!-- Issue 59878 - Start -->
                </com.csc_CommlPkgPolicyAddRs>
            </InsuranceSvcRs>
        </ACORD>
    </xsl:template>
 </xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\Issue_InternalXMLResponse.xml" htmlbaseurl="" outputurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->