<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Add)
-->
  <xsl:template name="CreateISLPPP4">
    <xsl:param name="Action"/>
    <xsl:variable name="TableName">ISLPPP4</xsl:variable>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ISLPPP4</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ISLPPP4__RECORD>
        <RECID>PP4</RECID>
        <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <ACTDATE>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
			<!-- 31594 start -->
			<xsl:when test="$Action = 'Mod'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
			<!-- 31594 end -->
            <xsl:otherwise>
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </ACTDATE>
        <ACTIONSEQ>0000</ACTIONSEQ>
        <UNITNO>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">UNITNO</xsl:with-param>
            <xsl:with-param name="FieldLength">5</xsl:with-param>
            <xsl:with-param name="Value" select="substring(../@LocationRef,2,string-length(../@LocationRef)-1)"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </UNITNO>
        <COVSEQ>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
            <xsl:with-param name="FieldLength">4</xsl:with-param>
			<!-- Case 31594 start -->
            <!--<xsl:with-param name="Value" select="position()"/>-->
			<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier/OtherId"/>
			<!-- Case 31594 end -->
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </COVSEQ>
        <TRANSSEQ>0001</TRANSSEQ>
        <SUBLINE>011</SUBLINE>
        <CLASSNUM>
          <xsl:value-of select="substring(RatingClassificationCd,1,4)"/>
        </CLASSNUM>
        <MAJPERIL>032</MAJPERIL>
        <COVEFFDTE>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
		  <!-- 31594 start -->
            <xsl:when test="$Action = 'Mod'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
		  <!-- 31594 end -->
            <xsl:otherwise>
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </COVEFFDTE>
        <COVEXPDTE>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/ExpirationDt"/>
              </xsl:call-template>
            </xsl:when>
		  <!-- 31594 start -->
            <xsl:when test="$Action = 'Mod'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/ExpirationDt"/>
              </xsl:call-template>
            </xsl:when>
		  <!-- 31594 end -->
            <xsl:otherwise>
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/ContractTerm/ExpirationDt"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </COVEXPDTE>
        <TRANSDTE>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
		  <!-- 31594 start -->
            <xsl:when test="$Action = 'Mod'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
		  <!-- 31594 end -->
            <xsl:otherwise>
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </TRANSDTE>
        <ENTRYDTE>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
		  <!-- 31594 start -->
            <xsl:when test="$Action = 'Mod'">
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:when>
		  <!-- 31594 end -->
            <xsl:otherwise>
              <xsl:call-template name="ConvertISODateToPTDate">
                <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/ContractTerm/EffectiveDt"/>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </ENTRYDTE>
        <ACCTGDTE>00000</ACCTGDTE>
        <STATE>
          <xsl:value-of select="../../StateProvCd"/>
        </STATE>
        <AUDITFREQ>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">
              <xsl:value-of select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/CommlPolicySupplement/AuditFrequencyCd"/>
            </xsl:when>
		  <!-- 31594 start -->
            <xsl:when test="$Action = 'Mod'">
			  <xsl:value-of select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/CommlPolicySupplement/AuditFrequencyCd"/>
            </xsl:when>
		  <!-- 31594 end -->
            <xsl:otherwise>
              <xsl:value-of select="/ACORD/InsuranceSvcRq/WorkCompPolicyAddRq/CommlPolicy/CommlPolicySupplement/AuditFrequencyCd"/>
            </xsl:otherwise>
          </xsl:choose>
        </AUDITFREQ>
        <TRANS>18</TRANS>
        <EXPOSURE>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">EXPOSURE</xsl:with-param>
            <xsl:with-param name="FieldLength">9</xsl:with-param>
            <xsl:with-param name="Value" select="ActualRemunerationAmt/Amt"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </EXPOSURE>
        <LIMITOCCUR>000000000</LIMITOCCUR>
        <LIMITAGGER>000000000</LIMITAGGER>
        <DEDUCTIBLE>0000000</DEDUCTIBLE>
        <COMMISSION>000000</COMMISSION>
        <TOTALORIG>00000000000</TOTALORIG>
        <ORIGINAL>00000000000</ORIGINAL>
        <PREMIUM>00000000000</PREMIUM>
        <TOTALPREM>00000000000</TOTALPREM>
        <TYPEACT>QQ</TYPEACT>
        <DESCSEQ>
          <xsl:value-of select="substring(RatingClassificationCd,5,2)"/>
        </DESCSEQ>
      </ISLPPP4__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->