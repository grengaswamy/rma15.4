<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/Location (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location (Mod) - 31594
-->
   <xsl:template name="CreatePMSPSA06">
      <xsl:variable name="TableName">PMSPSA06</xsl:variable>
      <xsl:variable name="LocationNbr" select="@LocationRef" />
      <xsl:variable name="LocationId" select="@id" />
      <xsl:variable name="LocationPath" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/Addr" />


      <xsl:variable name="LocationNumber" select="substring(@id,2,string-length(@id)-1)" />

      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>PMSPSA06</RECORD__NAME>
         </RECORD__NAME__ROW>

         <PMSPSA06__RECORD>
            <SYMBOL>
               <xsl:value-of select="$SYM" />
            </SYMBOL>

            <POLICYNO>
               <xsl:value-of select="$POL" />
            </POLICYNO>

            <MODULE>
               <xsl:value-of select="$MOD" />
            </MODULE>

            <MASTERCO>
               <xsl:value-of select="$MCO" />
            </MASTERCO>

            <LOCATION>
               <xsl:value-of select="$LOC" />
            </LOCATION>

            <UNITNO>
               <xsl:call-template name="FormatData">
                  <xsl:with-param name="FieldName">SITE</xsl:with-param>

                  <xsl:with-param name="FieldLength">5</xsl:with-param>

                  <xsl:with-param name="Value" select="$LocationNumber" />

                  <xsl:with-param name="FieldType">N</xsl:with-param>
               </xsl:call-template>
            </UNITNO>

<!-- 29653 Start -->
<!--<ADDRESS>
            <xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[substring(@id,2,string-length(@id)-1)=$LocationNumber]/GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/>
        </ADDRESS> 
        <ADDRESS2>
            <xsl:value-of select="Addr/Addr1"/>
        </ADDRESS2>  -->
            <xsl:choose>
                <xsl:when test="Addr/Addr1 != ''">
<!--<ADDRESS__DDS>-->
                  <ADDRESS>
		     <!-- Issue 50396 Begin                     
                     <xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[@id=$LocationNumber]/GeneralPartyInfo/ NameInfo/CommlName/CommercialNamee" / -->
                        <xsl:variable name="NameRef" select="@NameInfoRef" />

                        <xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[@id=$NameRef]/GeneralPartyInfo/ NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName" />
		     <!-- Issue 50396 End -->
                  </ADDRESS>

<!--</ADDRESS__DDS>-->
                  <ADDRESS2>
                     <xsl:value-of select="Addr/Addr1" />
                  </ADDRESS2>
               </xsl:when>

               <xsl:otherwise>
                  <ADDRESS>Generated Optional 1</ADDRESS>

                  <ADDRESS2>Generated Address 1</ADDRESS2>
               </xsl:otherwise>

<!-- 29653 End -->
            </xsl:choose>

            <CITY>
               <xsl:value-of select="Addr/City" />
            </CITY>

            <STATE>
               <xsl:value-of select="Addr/StateProvCd" />
            </STATE>

            <ZIPCODE>
               <xsl:value-of select="Addr/PostalCode" />
            </ZIPCODE>

            <CONTACT>
            </CONTACT>

            <CONTACTPH>
            </CONTACTPH>
         </PMSPSA06__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>

<!--
  The base xpath for this segment:
  /ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Quote)
  /ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Mod) - 31594
  -->
<!-- Case 31594 begin - Modified the template to build one SA06 segment for each location -->
   <xsl:template name="CreatePMSPSA06Defaults">
      <xsl:variable name="TableName">PMSPSA06</xsl:variable>

      <xsl:variable name="LocationNbr" select="@LocationRef" />

      <xsl:variable name="LocationPath" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/Addr" />

	   <!--<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode" />-->		<!--103409-->
	   <xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd" />			<!--103409-->

      <xsl:if test="$TranType != 'A' or $LocationNbr != 'l0'">
         <BUS__OBJ__RECORD>
            <RECORD__NAME__ROW>
               <RECORD__NAME>PMSPSA06</RECORD__NAME>
            </RECORD__NAME__ROW>

            <PMSPSA06__RECORD>
               <SYMBOL>
                  <xsl:value-of select="$SYM" />
               </SYMBOL>

               <POLICYNO>
                  <xsl:value-of select="$POL" />
               </POLICYNO>

               <MODULE>
                  <xsl:value-of select="$MOD" />
               </MODULE>

               <MASTERCO>
                  <xsl:value-of select="$MCO" />
               </MASTERCO>

               <LOCATION>
                  <xsl:value-of select="$LOC" />
               </LOCATION>

               <UNITNO>
                  <xsl:call-template name="FormatData">
                     <xsl:with-param name="FieldName">SITE</xsl:with-param>

                     <xsl:with-param name="FieldLength">5</xsl:with-param>

                     <xsl:with-param name="Value" select="substring(@LocationRef,2,string-length(@LocationRef)-1)" />

                     <xsl:with-param name="FieldType">N</xsl:with-param>
                  </xsl:call-template>
               </UNITNO>

               <xsl:choose>
                  <xsl:when test="$LocationPath/Addr1!=''">
<!--<ADDRESS__DDS>-->
                     <ADDRESS>
                        <xsl:variable name="NameRef" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/@NameInfoRef" />

                        <xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[@id=$NameRef]/GeneralPartyInfo/ NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName" />
                     </ADDRESS>

<!--</ADDRESS__DDS>-->
                     <ADDRESS2>
                        <xsl:value-of select="$LocationPath/Addr1" />
                     </ADDRESS2>
                  </xsl:when>

                  <xsl:otherwise>
                     <ADDRESS>Generated Optional 1</ADDRESS>

                     <ADDRESS2>Generated Address 1</ADDRESS2>
                  </xsl:otherwise>
               </xsl:choose>

               <CITY>
<!--<xsl:value-of select="Addr/City"/>-->
                  <xsl:value-of select="$LocationPath/City" />
               </CITY>

               <STATE>
<!--<xsl:value-of select="Addr/StateProvCd"/>-->
                  <xsl:value-of select="$LocationPath/StateProvCd" />
               </STATE>

               <xsl:choose>
                  <xsl:when test="$LocationPath/Addr1!=''">
                     <ZIPCODE>
                        <xsl:value-of select="$LocationPath/PostalCode" />
                     </ZIPCODE>
                  </xsl:when>

                  <xsl:otherwise>
                     <ZIPCODE>12345</ZIPCODE>
                  </xsl:otherwise>
               </xsl:choose>

               <CONTACT>
               </CONTACT>

               <CONTACTPH>
               </CONTACTPH>
            </PMSPSA06__RECORD>
         </BUS__OBJ__RECORD>
      </xsl:if>
   </xsl:template>

<!-- Case 31594 end -->
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->

