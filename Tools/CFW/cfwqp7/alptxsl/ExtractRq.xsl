<?xml version="1.0"?>
<!-- ** 65494 Added for Immediate Print -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="CommonFuncRq.xsl"/>  <!-- Common Formatting Routines -->
    <xsl:include href="PMSP0000Rq.xsl"/>  <!-- Activity Record -->

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/*[2]/*/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/*[2]/*/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/*[2]/*/com.csc_InsuranceLineIssuingCompany"/>
    <xsl:variable name="SYM" >
		<xsl:choose>
			<xsl:when test="/ACORD/InsuranceSvcRq/*[2]/*/CompanyProductCd='QAP'">CPP</xsl:when>
			<xsl:otherwise><xsl:value-of select="/ACORD/InsuranceSvcRq/*[2]/*/CompanyProductCd"/></xsl:otherwise>
		</xsl:choose>
    </xsl:variable>
    <xsl:variable name="POL">
	  	    <xsl:call-template name="FormatData">
	   	  		<xsl:with-param name="FieldName">$POL</xsl:with-param>      
		     		 <xsl:with-param name="FieldLength">7</xsl:with-param>
		     		 <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*[2]/*/PolicyNumber"/>
		      		<xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template> 
	</xsl:variable>
    <xsl:variable name="MOD">
  	         <xsl:call-template name="FormatData">
	   	  	<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
		      <xsl:with-param name="FieldLength">2</xsl:with-param>
		      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*[2]/*/PolicyVersion"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
    </xsl:variable>    
    <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/*[2]/*/LOBCd"/>   
    <xsl:variable name="TYPEACT">
  		<xsl:choose>
			<!--Issue 103409 Starts-->
			<!--<xsl:when test="/ACORD/InsuranceSvcRq/*/*/com.csc_AmendmentMode='N'">QQ</xsl:when>
			<xsl:when test="/ACORD/InsuranceSvcRq/*/*/com.csc_AmendmentMode='R'">RB</xsl:when>-->

			<xsl:when test="/ACORD/InsuranceSvcRq/*/*/PolicyStatusCd='N'">QQ</xsl:when>
			<xsl:when test="/ACORD/InsuranceSvcRq/*/*/PolicyStatusCd='R'">RB</xsl:when>
			<xsl:otherwise>EN</xsl:otherwise>
			<!--Issue 103409 Ends-->
  		</xsl:choose>
	</xsl:variable> 
	
    <xsl:template match="/">
        <xsl:element name="ExtractRq">
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/ExtractRq/*[8]" mode="PMSP0000"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>