<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/Location/AdditionalInterest (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location/AdditionalInterest (Mod) - 31594
-->
  <xsl:template name="CreateISLPPP2">
    <xsl:variable name="TableName">ISLPPP2</xsl:variable>
	<xsl:variable name="IntNum" select="substring(@id, 2,string-length(@id)-1)"/>
    <xsl:variable name="IntType" select="AdditionalInterestInfo/NatureInterestCd"/>
    <xsl:variable name="IntSeq" select="count(../AdditionalInterest[substring(@id,2,string-length(@id)-1) &lt; $IntNum]/AdditionalInterestInfo[NatureInterestCd = $IntType])"/>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ISLPPP2</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ISLPPP2__RECORD>
        <RECID>PP2</RECID>
        <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICY0NUM>
          <xsl:value-of select="$POL"/>
        </POLICY0NUM>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTER0CO>
          <xsl:value-of select="$MCO"/>
        </MASTER0CO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <ACTDATE>
          <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
          </xsl:call-template>
        </ACTDATE>
        <ACTIONSEQ>0000</ACTIONSEQ>
        <USE0CODE>
          <xsl:value-of select="AdditionalInterestInfo/NatureInterestCd"/>
        </USE0CODE>
		<!-- Start Case 33788 -->
        <!--USE0LOC>00000</USE0LOC-->
        <USE0LOC>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">USE0LOC</xsl:with-param>
            <xsl:with-param name="FieldLength">5</xsl:with-param>
            <xsl:with-param name="Value" select="substring(../@id,2,string-length(../@id)-1)"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
		</USE0LOC>
		<!-- End Case 33788 -->
		<!-- Start Case 31594 -->
        <!-- <DESC0SEQ>0</DESC0SEQ> -->
		<DESC0SEQ>
		<xsl:choose>
		<xsl:when test="$TYPEACT='EN'">
		<xsl:value-of select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='HostId']/OtherId"/> 
		</xsl:when>
		<xsl:otherwise><xsl:value-of select="$IntSeq"/></xsl:otherwise>	
		</xsl:choose>
		</DESC0SEQ>
		<!-- End Case 31594 -->
        <DZIP0CODE>
          <xsl:value-of select="GeneralPartyInfo/Addr/PostalCode"/>
        </DZIP0CODE>
        <DESC0LINE1>
          <xsl:value-of select="GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
        </DESC0LINE1>
        <DESC0LINE2>
          <xsl:value-of select="GeneralPartyInfo/Addr/Addr1"/>
        </DESC0LINE2>
        <DESC0LINE3>
          <xsl:value-of select="GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/>
        </DESC0LINE3>
        <DESC0LINE4>
          <xsl:value-of select="substring(concat(GeneralPartyInfo/Addr/City, '                                                                                                    '), 1, 28)"/>
          <xsl:value-of select="GeneralPartyInfo/Addr/StateProvCd"/>
        </DESC0LINE4>
      </ISLPPP2__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->