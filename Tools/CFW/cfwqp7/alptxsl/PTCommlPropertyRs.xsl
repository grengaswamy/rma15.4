<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template name="CreateCommlProperty">
       <xsl:param name="InsLine"/>
	    <xsl:choose>
	      <xsl:when test="$InsLine='CF'">           
        <com.csc_CommlPropertyLineBusiness>
            <LOBCd>CFIRE</LOBCd>
             <CurrentTermAmt>
                <Amt><xsl:value-of select="BBA3VA"/></Amt>
            </CurrentTermAmt>
<!-- Blanket Limits -->
             <PropertyInfo>
                 <CommlPropertyInfo LocationRef="l0">
                    <SubjectInsuranceCd>BLNKT</SubjectInsuranceCd>
<!-- Building Limit for CF -->
                    <xsl:if test="string-length(BBAGVA)"> 
                       <xsl:call-template name="BuildCoverage">
                           <xsl:with-param name="Name">BLDG</xsl:with-param>
                           <xsl:with-param name="LimitAmt"><xsl:value-of select="BBAGVA"/></xsl:with-param>
                           <xsl:with-param name="CoinsurancePct"><xsl:value-of select="BBAKCD"/></xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
<!-- BLDG/PP Limit CF -->
					<!--Issue 59878 start-->
					<xsl:if test="string-length(BBCZVA)"> 
                       <xsl:call-template name="BuildCoverage">
                           <xsl:with-param name="Name">PERS</xsl:with-param>
                           <xsl:with-param name="LimitAmt"><xsl:value-of select="BBCZVA"/></xsl:with-param>
                           <xsl:with-param name="CoinsurancePct"><xsl:value-of select="BBAKCD"/></xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
					<!--Issue 59878 end-->
                    <xsl:if test="string-length(BBUSVA3)">
                       <xsl:call-template name="BuildCoverage">
                          <xsl:with-param name="Name">BPP</xsl:with-param>
                          <xsl:with-param name="LimitAmt"><xsl:value-of select="BBUSVA3"/></xsl:with-param>
                          <xsl:with-param name="CoinsurancePct"><xsl:value-of select="BBAKCD"/></xsl:with-param>
                       </xsl:call-template>
                    </xsl:if>
<!-- STOCK Limit CF -->
                    <xsl:if test="string-length(BBUSVA4)">
                       <xsl:call-template name="BuildCoverage">
                          <xsl:with-param name="Name">STOCK</xsl:with-param>
                          <xsl:with-param name="LimitAmt"><xsl:value-of select="BBUSVA4"/></xsl:with-param>
                          <xsl:with-param name="CoinsurancePct"><xsl:value-of select="BBAKCD"/></xsl:with-param>
                       </xsl:call-template>
                    </xsl:if>
<!-- BUSINESS INCOME excluding EXTRA EXPENSE Limit CF -->
                    <xsl:if test="string-length(BBUSVA5)">
                       <xsl:call-template name="BuildCoverage">
                          <xsl:with-param name="Name">BUSEE</xsl:with-param>
                          <xsl:with-param name="LimitAmt"><xsl:value-of select="BBUSVA5"/></xsl:with-param>
                          <xsl:with-param name="CoinsurancePct"><xsl:value-of select="BBAKCD"/></xsl:with-param>
                       </xsl:call-template>
                    </xsl:if>
<!-- BUILDERS RISK Limit CF -->
                    <xsl:if test="string-length(BBCYVA)">
                       <xsl:call-template name="BuildCoverage">
                          <xsl:with-param name="Name">BRWOR</xsl:with-param>
                          <xsl:with-param name="LimitAmt"><xsl:value-of select="BBCYVA"/></xsl:with-param>
                          <xsl:with-param name="CoinsurancePct"><xsl:value-of select="BBAKCD"/></xsl:with-param>
                       </xsl:call-template>
                    </xsl:if>
<!-- EXTRA EXPENSE Limit CF -->
                    <xsl:if test="string-length(BBCXVA)">
                       <xsl:call-template name="BuildCoverage">
                          <xsl:with-param name="Name">EE</xsl:with-param>
                          <xsl:with-param name="LimitAmt"><xsl:value-of select="BBCXVA"/></xsl:with-param>
                          <xsl:with-param name="CoinsurancePct"><xsl:value-of select="BBAKCD"/></xsl:with-param>
                       </xsl:call-template>
                    </xsl:if>
                    <com.csc_ReportingInd><xsl:value-of select="BBCZST"/></com.csc_ReportingInd>                    
                 </CommlPropertyInfo>
                 <xsl:for-each select="//ASBYCPL1__RECORD[BYAGTX='CF']"> 
                 <xsl:sort select="BYBRNB"/><!--Issue 102807 -->
   <!-- Ignore Coverage Minimum Premiums. They are generated below. -->
                                <xsl:variable name="IsMinPremCov">
                                    <xsl:call-template name="MinPremLookup">
                                        <xsl:with-param name="InsLine">CF</xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:if test="$IsMinPremCov='N'">
        <xsl:variable name="Location" select="BYBRNB"/>
        <xsl:variable name="SubLocation" select="BYEGNB"/>
        <xsl:variable name="CovCode" select="normalize-space(BYAOTX)"/>
         <xsl:variable name="ACORDCovCode">
                <xsl:call-template name="TranslateCoverageCode">
                    <xsl:with-param name="InsLine">CF</xsl:with-param>
                    <xsl:with-param name="CovCode" select="$CovCode"/>
                </xsl:call-template>
            </xsl:variable> 
            <!--Issue 102807 starts-->
            <xsl:variable name="GetForm">
            <xsl:if test="not(preceding-sibling::ASBYCPL1__RECORD[BYBRNB=$Location and BYAGTX='CF'])">
                   <xsl:text>1</xsl:text>
           </xsl:if>
           </xsl:variable>    
           <!--Issue 102807 ends-->                   
         <CommlPropertyInfo>
             <xsl:attribute name="LocationRef">l<xsl:value-of select="$Location"/></xsl:attribute>
             <xsl:attribute name="SubLocationRef">s<xsl:value-of select="$SubLocation"/></xsl:attribute>
             <xsl:variable name="PMACd" select="//ASB5CPL1__RECORD[B5BRNB=$Location and B5EGNB=$SubLocation and B5AGTX='CF']/B5KWTX"/>
            <SubjectInsuranceCd><xsl:value-of select="$ACORDCovCode"/></SubjectInsuranceCd>
            <ClassCd><xsl:value-of select="BYNDTX"/></ClassCd>
            <CommlCoverage>
               <CoverageCd>
                     <xsl:value-of select="BYAMCD"/>
               </CoverageCd>
               <CoverageDesc/>
           	<xsl:if test="string-length(BYAGVA) &gt; 0">
               <Limit>
                <FormatCurrencyAmt>
                    <Amt><xsl:value-of select="BYAGVA"/></Amt>
                </FormatCurrencyAmt>
                <LimitAppliesToCd/>
               </Limit>
            </xsl:if>
           	<xsl:if test="string-length(BYUSVA4) &gt; 0">
               <Limit>
                <FormatCurrencyAmt>
                    <Amt><xsl:value-of select="BYUSVA4"/></Amt>
                </FormatCurrencyAmt>
                <LimitAppliesToCd>Theft</LimitAppliesToCd>
               </Limit>
            </xsl:if>            
			<xsl:if test="string-length(BYPPTX) &gt; 0">
			   <Deductible>
			     <FormatCurrencyAmt>
				    <Amt><xsl:value-of select="BYPPTX"/></Amt>
			     </FormatCurrencyAmt>
			     <DeductibleTypeCd/>
			   </Deductible>
			</xsl:if>
			<xsl:if test="string-length(BYANCD) &gt; 0 and $CovCode='BR'">
			   <Deductible>
			     <FormatCurrencyAmt>
				    <Amt><xsl:value-of select="BYANCD"/></Amt>
			     </FormatCurrencyAmt>
			     <DeductibleTypeCd/>
			     <DeductibleAppliesToCd>Theft</DeductibleAppliesToCd>
			   </Deductible>
			</xsl:if>			
			<xsl:if test="BYI0TX = 'Y'">
               <Option>
                  <OptionTypeCd>YNInd1</OptionTypeCd> 
                  <OptionCd>EQ</OptionCd> 
                  <OptionValue>Y</OptionValue> 
               </Option>
			</xsl:if>
               <CurrentTermAmt>
                  <Amt><xsl:value-of select="BYA3VA"/></Amt>
                  <com.csc_OverrideInd>
                     <xsl:choose>
                       <xsl:when test="BYD3ST='0'">1</xsl:when>
                       <xsl:otherwise>0</xsl:otherwise>
                     </xsl:choose>                 
                  </com.csc_OverrideInd> 
               </CurrentTermAmt>
            <xsl:if test="string-length(BYAKCD) &gt; 0">
               <CommlCoverageSupplement>
                  <CoinsurancePct><xsl:value-of select="BYAKCD"/></CoinsurancePct> 
               </CommlCoverageSupplement>
          </xsl:if>
           </CommlCoverage>
           
            <xsl:if test="BYUSCD5 &gt; 0">
                <xsl:call-template name="BuildCoverage">
                    <xsl:with-param name="Name">INFL</xsl:with-param>
                </xsl:call-template>
            </xsl:if>
				   <com.csc_TerritoryCd><xsl:value-of select="BYAGNB"/></com.csc_TerritoryCd>
					<com.csc_PMACd><xsl:value-of select="$PMACd"/></com.csc_PMACd>
					<com.csc_ClassCdDesc><xsl:value-of select="BYKKTX"/></com.csc_ClassCdDesc>  
<!-- Generate the Coverage Minimum Premium aggregate if applicable -->
			<xsl:call-template name="GenerateMinPrem">
			    <xsl:with-param name="InsLine">CF</xsl:with-param>
			    <xsl:with-param name="Location" select="$Location"/>
			    <xsl:with-param name="SubLocation" select="$SubLocation"/>
			    <xsl:with-param name="CovCode" select="$CovCode"/>
			</xsl:call-template>
				<!--Issue 102807 starts-->
	     		<xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='CF' and BEBRNB =$Location]">
        	               <xsl:if test="$GetForm = '1'">
								    <xsl:call-template name="BuildCPPForm"/>
	    					 </xsl:if>
              </xsl:for-each>
		     <!--Issue 102807 ends-->
             </CommlPropertyInfo>
            </xsl:if>
           </xsl:for-each>
           <!--Issue 102807 starts-->
	  <xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='CF'and BEBRNB='0']">
	  		<xsl:call-template name="BuildCPPForm" />			
    	</xsl:for-each>	
 <!--Issue 102807 ends-->	
          </PropertyInfo>
                 
<!-- Now generate the Insurance Line Minimum Premium aggregate -->
			<xsl:call-template name="GenerateMinPrem">
			    <xsl:with-param name="InsLine">CF</xsl:with-param>
			    <xsl:with-param name="CovCode">INSLIN</xsl:with-param>
			</xsl:call-template>
<!-- Build Modification Factors -->
            <xsl:for-each select="//ASBCCPL1__RECORD[BCAGTX='CF']">
    		    <com.csc_ModificationFactors>
        			<StateProvCd><xsl:value-of select="BCADCD"/></StateProvCd>
<!-- Multi Loc -->
        			<xsl:call-template name="BuildModFactor">
         			    <xsl:with-param name="Code">com.csc_ML</xsl:with-param>
         			    <xsl:with-param name="FactorField"><xsl:value-of select="BCAEPC"/></xsl:with-param>
        			</xsl:call-template>
<!-- IRPM -->
        			<xsl:call-template name="BuildModFactor">
        			    <xsl:with-param name="Code">IRPM</xsl:with-param>
        			    <xsl:with-param name="FactorField"><xsl:value-of select="BCANPC"/></xsl:with-param>
        			</xsl:call-template>
    		    </com.csc_ModificationFactors>
    		</xsl:for-each>
<!-- Build Forms --><!-- ICH00284B -->
	<!--Issue 102807 commented out startrs-->
	       <!-- <xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='CF']">	-->	<!--UNI00284B-->
			<!--<xsl:call-template name="BuildCPPForm" />-->
		<!--</xsl:for-each>-->
		<!--Issue 102807 commented out end-->
        </com.csc_CommlPropertyLineBusiness>
		</xsl:when>

<!-- BOP start-->
		<xsl:when test="$InsLine='BO'">
			<PropertyInfo>
			<!--<xsl:for-each select="//ASB5CPL1__RECORD"> 34771-->
				<xsl:for-each select="//ASB5CPL1__RECORD[B5C7ST!='Y']"> <!-- 34771-->
			<xsl:variable name="Location" select="B5BRNB"/>
        	<xsl:variable name="SubLocation" select="B5EGNB"/>
			<!-- Build for SubjectInsuranceCd=""-->
			<CommlPropertyInfo>
				<xsl:attribute name="LocationRef">l<xsl:value-of select="$Location"/> 
				</xsl:attribute>
				<xsl:attribute name="SubLocationRef">s<xsl:value-of select="$SubLocation"/>
				</xsl:attribute>
				<SubjectInsuranceCd></SubjectInsuranceCd>
			<ClassCd><xsl:value-of select="B5PTTX"/></ClassCd>													 <!-- 34771-->
				<com.csc_ClassCdDesc><xsl:value-of select="B5KKTX"/></com.csc_ClassCdDesc>							 <!-- 34771-->
				<xsl:for-each select="//ASBYCPL1__RECORD[BYBRNB=$Location and BYEGNB=$SubLocation and BYC7ST!='Y']"> <!-- 34771 -->
					<xsl:variable name="CoverageCode">																 <!-- 34771-->
					<xsl:choose>																					 <!-- 34771-->
                		<xsl:when test="BYAOTX='PRFD&amp;O'">PRFDO</xsl:when>										 <!-- 34771-->
                		<xsl:when test="BYAOTX='PRFE&amp;O'">PRFEO</xsl:when>										 <!-- 34771-->
                		<xsl:when test="BYAOTX='M&amp;SOFF'">MSOFF</xsl:when>										 <!-- 34771-->
                		<xsl:when test="BYAOTX='M&amp;SPRM'">MSPRM</xsl:when>										 <!-- 34771-->			
					<xsl:otherwise><xsl:value-of select="BYAOTX"/></xsl:otherwise>								 <!-- 34771-->
					</xsl:choose>																					 <!-- 34771-->																			 <!-- 34771-->
					</xsl:variable> 																				 <!-- 34771-->
					<xsl:call-template name="BuildCoverage">
						<xsl:with-param name="Name" select="$CoverageCode"></xsl:with-param> 						 <!-- 34771: changed param from BYAOTX to $CoverageCode-->
						<xsl:with-param name="LimitAmt" select="BYAGVA"></xsl:with-param> 
						<xsl:with-param name="DeductibleAmt" select="BYPPTX"></xsl:with-param> 
						<xsl:with-param name="PremiumAmt" select="BYA3VA"></xsl:with-param> 
					</xsl:call-template> 
				</xsl:for-each>
			</CommlPropertyInfo>
			
				<!-- 34771: start -->
			<!-- SubjectInsuranceCd="BLDG" -->
			<CommlPropertyInfo>
				<xsl:attribute name="LocationRef">l<xsl:value-of select="$Location"/> 
				</xsl:attribute>
				<xsl:attribute name="SubLocationRef">s<xsl:value-of select="$SubLocation"/>
				</xsl:attribute>
				<SubjectInsuranceCd>BLDG</SubjectInsuranceCd>
				<ClassCd><xsl:value-of select="B5PTTX"/></ClassCd>	
				<com.csc_ClassCdDesc><xsl:value-of select="B5KKTX"/></com.csc_ClassCdDesc> 
					<Option>
						<OptionCd>com.csc_OHCL</OptionCd>
						<OptionValue><xsl:value-of select="B5USIN14"/></OptionValue> 
					</Option>
					<Option>
						<OptionCd>com.csc_OHPT</OptionCd>
						<OptionValue><xsl:value-of select="B5USIN13"/></OptionValue> 
					</Option>
					<Option>
						<OptionCd>com.csc_OPC</OptionCd>
						<OptionValue><xsl:value-of select="B5I4TX"/></OptionValue> 
					</Option>
					<Option>
						<OptionCd>com.csc_OPP</OptionCd>
						<OptionValue><xsl:value-of select="B5I5TX"/></OptionValue> 
					</Option>
					<Option>
						<OptionCd>com.csc_OPW</OptionCd>
						<OptionValue><xsl:value-of select="B5USIN15"/></OptionValue> 
					</Option>
				<CommlCoverage>
					<CoverageCd>INFL</CoverageCd>
					<Limit>	<FormatCurrencyAmt><Amt>0</Amt></FormatCurrencyAmt></Limit>
					<Option>
					<OptionValue><xsl:value-of select="B5BKNB"/></OptionValue>
					</Option>
					<CurrentTermAmt><Amt>0</Amt></CurrentTermAmt>
				</CommlCoverage>
				<CommlCoverage>
					<CoverageCd>XCBUL</CoverageCd>
					<Limit><FormatCurrencyAmt><Amt>0</Amt></FormatCurrencyAmt></Limit>
					<Option>
					<OptionValue><xsl:value-of select="B5C2ST"/></OptionValue>
					</Option>
					<CurrentTermAmt><Amt>0</Amt></CurrentTermAmt>
				</CommlCoverage>
				<CommlCoverage>
					<CoverageCd></CoverageCd>
					<Limit>
					<FormatCurrencyAmt>
						<Amt><xsl:value-of select="B5AHVA"/></Amt>
					</FormatCurrencyAmt>
					<ValuationCd><xsl:value-of select="B5C0ST"/></ValuationCd>
					</Limit>
					<Deductible>
					<FormatCurrencyAmt>
						<Amt><xsl:value-of select="B5USIN20"/></Amt>
					</FormatCurrencyAmt>
					<DeductibleTypeCd>HURR</DeductibleTypeCd>
					</Deductible>
				</CommlCoverage>
			</CommlPropertyInfo>
			<!-- SubjectInsuranceCd="BLDG" -->
			<CommlPropertyInfo>
				<xsl:attribute name="LocationRef">l<xsl:value-of select="$Location"/> 
				</xsl:attribute>
				<xsl:attribute name="SubLocationRef">s<xsl:value-of select="$SubLocation"/>
				</xsl:attribute>
				<SubjectInsuranceCd>BPP</SubjectInsuranceCd>
				<ClassCd><xsl:value-of select="B5PTTX"/></ClassCd>	
				<com.csc_ClassCdDesc><xsl:value-of select="B5KKTX"/></com.csc_ClassCdDesc> 
				<CommlCoverage>
					<CoverageCd/>
					<Limit>
					<FormatCurrencyAmt>
					<Amt><xsl:value-of select="B5USVA3"/></Amt>
					</FormatCurrencyAmt>
					</Limit>
					<Deductible>
					<FormatCurrencyAmt>
						<Amt><xsl:value-of select="B5ANCD"/></Amt>
					</FormatCurrencyAmt>
					</Deductible>
				</CommlCoverage>
			</CommlPropertyInfo>
			<!-- 34771 end -->
			</xsl:for-each>
            <!--<xsl:for-each select="//ASBYCPL1__RECORD[BYBRNB='0' and BYEGNB='0']"> 34771-->>
			<xsl:for-each select="//ASBYCPL1__RECORD[BYBRNB='0' and BYEGNB='0' and BYC7ST!='Y']"> <!-- 34771 -->
				<xsl:call-template name="BuildCoverage">
					<xsl:with-param name="Name" select="BYAOTX"></xsl:with-param> 
					<xsl:with-param name="LimitAmt" select="BYAGVA"></xsl:with-param> 
					<xsl:with-param name="PremiumAmt" select="BYA3VA"></xsl:with-param> 
				</xsl:call-template> 
			</xsl:for-each>
			</PropertyInfo>
				<!-- 34771 start -->
			<LiabilityInfo>
					<xsl:call-template name="BuildCoverage">
					<xsl:with-param name="Name">EAOCC</xsl:with-param> 
					<xsl:with-param name="LimitAmt" select="//BBC3VA"></xsl:with-param> 
					</xsl:call-template> 
					<xsl:call-template name="BuildCoverage">
					<xsl:with-param name="Name">FLL</xsl:with-param> 
					<xsl:with-param name="LimitAmt" select="//BBC2VA"></xsl:with-param> 
					</xsl:call-template>
					<xsl:call-template name="BuildCoverage">
					<xsl:with-param name="Name">GENAG</xsl:with-param> 
					<xsl:with-param name="LimitAmt" select="//BBUSVA4"></xsl:with-param> 
					</xsl:call-template>
					<xsl:call-template name="BuildCoverage">
					<xsl:with-param name="Name">MEDPM</xsl:with-param> 
					<xsl:with-param name="LimitAmt" select="//BBCYVA"></xsl:with-param> 
					</xsl:call-template>
					<xsl:call-template name="BuildCoverage">
					<xsl:with-param name="Name">PRDCO</xsl:with-param> 
					<xsl:with-param name="LimitAmt" select="//BBUSVA5"></xsl:with-param> 
					</xsl:call-template>
			</LiabilityInfo>
			<!-- 34771 end -->
		</xsl:when>
<!-- BOP End -->

		</xsl:choose>        
    </xsl:template>
    
    <xsl:template name="BuildCoverage">
        <xsl:param name="Name"/>
        <xsl:param name="LimitAmt"/>
		<xsl:param name="DeductibleAmt"/>        
		<xsl:param name="PremiumAmt">0</xsl:param>
        <xsl:param name="CoinsurancePct"/>
        <CommlCoverage>
            <CoverageCd><xsl:value-of select="$Name"/></CoverageCd>
			<IterationNumber><xsl:value-of select="BYC0NB"/></IterationNumber>
            <CoverageDesc/>
             <Limit>
                <FormatCurrencyAmt>
                    <Amt><xsl:value-of select="$LimitAmt"/></Amt>
                </FormatCurrencyAmt>
                <LimitAppliesToCd/>
            </Limit>
            <xsl:if test="string-length($DeductibleAmt) &gt; 0">
			   <Deductible>
				   <FormatCurrencyAmt>
					   <Amt><xsl:value-of select="$DeductibleAmt"/></Amt>
				   </FormatCurrencyAmt>
			   </Deductible>
            </xsl:if> 
            <!-- 34771 start -->
			<xsl:call-template name="BuildCoverageOptions">
					<xsl:with-param name="CoverageCd" select="BYAOTX"></xsl:with-param> 
			</xsl:call-template> 
			<!-- 34771 End -->
            <CurrentTermAmt>
				<!--<Amt><xsl:value-of select="$PremiumAmt"/></Amt> 34771-->
                <!-- 34771 : start -->
                <Amt>
				<xsl:choose>
				<xsl:when test="$PremiumAmt!=''">
					<xsl:value-of select="$PremiumAmt"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				</Amt>
				<com.csc_OverrideInd>
				<xsl:choose>
				<xsl:when test="BYD3ST='0'">Y</xsl:when>
				<xsl:otherwise>N</xsl:otherwise>
				</xsl:choose>
				</com.csc_OverrideInd>
				<!-- 34771 : end -->
            </CurrentTermAmt>
            <xsl:if test="string-length($CoinsurancePct) &gt; 0">
            <CommlCoverageSupplement>
               <CoinsurancePct><xsl:value-of select="$CoinsurancePct"/></CoinsurancePct> 
            </CommlCoverageSupplement>
          </xsl:if>          
            <!--34771 : start -->
		  <xsl:if test="BYBRNB='0' and BYEGNB='0'"> 
		  <com.csc_ItemIdInfo>
			<OtherIdentifier>
				<OtherIdTypeCd>com.csc_NextAvailableCoverageSeqNbr</OtherIdTypeCd>
				<OtherId><xsl:value-of select="BYC0NB +1"/></OtherId>
			</OtherIdentifier>
		  </com.csc_ItemIdInfo>
		  </xsl:if>
		  <!--34771 : end -->          
        </CommlCoverage>
    </xsl:template>
    <!-- 34771 start -->
	<xsl:template name="BuildCoverageOptions">
		<xsl:param name="CoverageCd"/>
		<xsl:choose>
		<xsl:when test="$CoverageCd='EMPDIS'">
			<Option>
			<OptionCd>com.csc_NumEmp</OptionCd>
			<OptionValue><xsl:value-of select="BYBKNB"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_NumLoc</OptionCd>
			<OptionValue><xsl:value-of select="BYBLNB"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='ADDMGR'">
			<Option>
			<OptionCd>com.csc_NumAddlInsured</OptionCd>
			<OptionValue><xsl:value-of select="BYBLNB"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='BACKUP'">
			<Option>
			<OptionCd>com.csc_NumBldg</OptionCd>
			<OptionValue><xsl:value-of select="BYBKNB"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='BOILER'">
			<Option>
			<OptionCd>com.csc_BoilerClassCd</OptionCd>
			<OptionValue><xsl:value-of select="BYAMCD"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='CMPTR'">
			<Option>
			<OptionCd>com.csc_DataMedia</OptionCd>
			<OptionValue><xsl:value-of select="BYAHVA"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='EQBLDG' or $CoverageCd='EQPERS'">
			<Option>
			<OptionCd>com.csc_EQBldgClassCd</OptionCd>
			<OptionValue><xsl:value-of select="BYUSCD5"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_EQDedPct</OptionCd>
			<OptionValue><xsl:value-of select="BYANCD"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_EQZoneInd</OptionCd>
			<OptionValue><xsl:value-of select="BYI2TX"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='LIQEXC'">
			<Option>
			<OptionCd>com.csc_LiqExclInd</OptionCd>
			<OptionValue><xsl:value-of select="BYCZST"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_LiquorScldInd</OptionCd>
			<OptionValue><xsl:value-of select="BYC1ST"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='MECH'">
			<Option>
			<OptionCd>com.csc_ACCoverageInd</OptionCd>
			<OptionValue><xsl:value-of select="BYCZST"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_BoilerPressureInd</OptionCd>
			<OptionValue><xsl:value-of select="BYC0ST"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_PressureInd</OptionCd>
			<OptionValue><xsl:value-of select="BYC1ST"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='MINE'">
			<Option>
			<OptionCd>com.csc_MineCountyCd</OptionCd>
			<OptionValue><xsl:value-of select="BYAKCD"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_MineOccupancyInd</OptionCd>
			<OptionValue><xsl:value-of select="BYI1TX"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_TypeStructureInd</OptionCd>
			<OptionValue><xsl:value-of select="BYCZST"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='PRFBRB' or $CoverageCd='PRFBTY' or
						$CoverageCd='PRFCLR' or $CoverageCd='PRFVET'">
			<Option>
			<OptionCd>com.csc_NumPerson</OptionCd>
			<OptionValue><xsl:value-of select="substring-before(BYAEPC,'.')"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='SPOIL'">
			<Option>
			<OptionCd>com.csc_ClassTypeCd</OptionCd>
			<OptionValue><xsl:value-of select="BYAKCD"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_MaintAgreementInd</OptionCd>
			<OptionValue><xsl:value-of select="BYC0ST"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_TypeCoverageInd</OptionCd>
			<OptionValue><xsl:value-of select="BYCZST"/></OptionValue>
			</Option>
		</xsl:when>
		<xsl:when test="$CoverageCd='TIME'">
			<Option>
			<OptionCd>com.csc_OHCL</OptionCd>
			<OptionValue><xsl:value-of select="BYC1ST"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_OHPT</OptionCd>
			<OptionValue><xsl:value-of select="BYCZST"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_OPC</OptionCd>
			<OptionValue><xsl:value-of select="BYIYTX"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_OPP</OptionCd>
			<OptionValue><xsl:value-of select="BYIZTX"/></OptionValue>
			</Option>
			<Option>
			<OptionCd>com.csc_OPW</OptionCd>
			<OptionValue><xsl:value-of select="BYC2ST"/></OptionValue>
			</Option>
		</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- 34771 end -->
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->