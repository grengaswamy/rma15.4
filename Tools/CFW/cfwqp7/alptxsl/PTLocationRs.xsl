<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

    <!-- 55614 <xsl:template name="CreateLocation" mode="PTLocation"> -->
     <xsl:template name="CreateLocation">
 		    <xsl:variable name="Location" select="BUBRNB"/>
         <Location>
             <xsl:attribute name="id"><xsl:value-of select="concat('l',$Location)"/></xsl:attribute>  
            <ItemIdInfo>
                <InsurerId><xsl:value-of select="$Location"/></InsurerId>
            </ItemIdInfo>
            <Addr>
               <AddrTypeCd></AddrTypeCd>
               <Addr1><xsl:value-of select="BUEFTX"/></Addr1>
               <Addr2><xsl:value-of select="BUEGTX"/></Addr2>
                <City><xsl:value-of select="BUEITX"/></City>
               <StateProvCd><xsl:value-of select="BUEJTX"/></StateProvCd>
               <PostalCode><xsl:value-of select="BUAPNB"/></PostalCode>
               <Country>USA</Country>
               <!--  <County/>   34771-->
               <County><xsl:value-of select="/*/ASBVCPL1__RECORD[BVBRNB=$Location]/BVAPNB"/></County> <!-- 34771 -->
            </Addr>
            <!--<xsl:for-each select="/*/ASBVCPL1__RECORD[BVBRNB=$Location]">-->
            <xsl:for-each select="/*/ASBVCPL1__RECORD[BVBRNB=$Location and BVC7ST!='Y']">			 <!-- 34771 -->
                <xsl:call-template name="CreateSubLocation">
                </xsl:call-template>
            </xsl:for-each>
            <xsl:for-each select="/*/PMSP1200__RECORD[$Location = substring(USE0LOC, string-length(USE0LOC)-string-length($Location)+1, string-length($Location)) and DESC0SEQ='0']">
                  <xsl:call-template name="CreateAdditionalInterest"/>
               </xsl:for-each> 
               	<!-- 34771 start -->
			<com.csc_ItemIdInfo>
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_NextAvailableSubLocationNbr</OtherIdTypeCd>
					<OtherId><xsl:value-of select="count(//ASBVCPL1__RECORD[BVBRNB=$Location])+1"/></OtherId>
				</OtherIdentifier>
			</com.csc_ItemIdInfo>
			<!-- 34771 end -->
         </Location>
    </xsl:template>
    <!-- Issue # 34771 Starts Added CreateWCLocation template-->
    <xsl:template name="CreateWCLocation">
		<xsl:variable name="Site" select="SITE"/>
		<xsl:variable name="ServerSeq" select="position()"/>
		<Location>
         <xsl:attribute name="id"><xsl:value-of select="concat($KEY,'-l',$ServerSeq)"/></xsl:attribute>
			<xsl:attribute name="NameInfoRef"><xsl:value-of select="concat($KEY,'-n',$ServerSeq)"/></xsl:attribute>
			<ItemIdInfo>
				<InsurerId><xsl:value-of select="$ServerSeq"/></InsurerId>
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_HostRowSeq</OtherIdTypeCd>
					<OtherId><xsl:value-of select="$Site"/></OtherId>
				</OtherIdentifier>
			</ItemIdInfo>
			<Addr>
				<AddrTypeCd>StreetAddress</AddrTypeCd>
				<Addr1><xsl:value-of select="ADDRESS2"/></Addr1>
				<Addr2><!--<xsl:value-of select="ADDRESS2"/>--></Addr2>
				<City><xsl:value-of select="substring(CITYST,1,28)"/></City>
                        <!-- 29653 Start -->
				<!--<StateProvCd><xsl:value-of select="substring(CITYST,29,2)"/></StateProvCd> -->
                        <StateProvCd>
                             <xsl:call-template name="ConvertNumericStateCdToAlphaStateCd">
	   	     	                           <xsl:with-param name="Value" select="STATE"/>
		   	            </xsl:call-template> 
                        </StateProvCd> 
                        <!-- 29653 End -->
				<PostalCode><xsl:value-of select="ZIP"/></PostalCode>
				<Country>USA</Country>
				<County/>
			</Addr>
			<!-- 55614 Begin -->
			<!--<xsl:for-each select="/*/PMSP1200__RECORD[$Site=USE0LOC]">
				<xsl:call-template name="CreateWorkCompAdditionalInterest"></xsl:call-template>
			</xsl:for-each>-->
			<!-- 62413 start -->
			<!--<TaxCodeInfo>
				<TaxCd><xsl:value-of select="FEIN"/></TaxCd>
				<TaxTypeCd>FEIN</TaxTypeCd>
				<TaxCd><xsl:value-of select="UNEMPNO"/></TaxCd>
				<TaxTypeCd>UNEMPLOYMENT NUMBER</TaxTypeCd>
			</TaxCodeInfo>-->
			<!-- 62413 end -->
			<TaxCodeInfo>
				<TaxCd><xsl:value-of select="FEIN"/></TaxCd>
				<TaxTypeCd>FEIN</TaxTypeCd>
			</TaxCodeInfo>
			<TaxCodeInfo>	
				<TaxCd><xsl:value-of select="UNEMPNO"/></TaxCd>
				<TaxTypeCd>UNEMPLOYMENT NUMBER</TaxTypeCd>
			</TaxCodeInfo>
			<!--<xsl:for-each select="/*/PMSP1200__RECORD[$Site=USE0LOC]">-->		<!--103409-->
			<xsl:for-each select="/*/PMSP1200__RECORD[DEEMSTAT = '' and $Site=USE0LOC]">	<!--103409-->
				<xsl:call-template name="CreateWorkCompAdditionalInterest"></xsl:call-template>
			</xsl:for-each>
			<!-- 55614 End -->			
		</Location>
    </xsl:template>
	<!-- Issue # 34771 Ends Added CreateWCLocation template-->
	<!-- 55614 begin -->
    <xsl:template name="CreateLocationFrom12LA">
 		    <xsl:variable name="Location" select="LOCATION"/>
         <Location>
            <ItemIdInfo>
                <InsurerId>1</InsurerId>
            </ItemIdInfo>
            <Addr>
               <AddrTypeCd></AddrTypeCd>
               <Addr1><xsl:value-of select="DESC0LINE1"/></Addr1>
               <Addr2><xsl:value-of select="DESC0LINE2"/></Addr2>
               <Addr3><xsl:value-of select="DESC0LINE3"/></Addr3>
               <Addr4><xsl:value-of select="DESC0LINE5"/></Addr4>
                <City><xsl:value-of select="substring(DESC0LINE4,1,28)"/></City>
               <StateProvCd><xsl:value-of select="substring(DESC0LINE4,29,2)"/></StateProvCd>
               <PostalCode><xsl:value-of select="DZIP0CODE"/></PostalCode>
               <Country>USA</Country>
               <County><xsl:value-of select="substring(/*/ASBQCPL1__RECORD/BQBKCD,3,3)"/></County>
            </Addr>
            <CountyTownCd><xsl:value-of select="/*/PMSP0200__RECORD/PROFIT0CTR"/></CountyTownCd>
            <xsl:for-each select="/*/PMSP1200__RECORD[$Location = substring(USE0LOC, string-length(USE0LOC)-string-length($Location)+1, string-length
            ($Location)) and DESC0SEQ='0' and USE0CODE!='LA']">
                  <xsl:call-template name="CreateAdditionalInterest"/>
               </xsl:for-each> 
         </Location>
    </xsl:template>
    <xsl:template name="CreateHOLocation">
 		    <xsl:variable name="Location" select="LOCATION"/>
         <Location>
                  <xsl:attribute name="id"><xsl:value-of select="concat($KEY,'-l1')"/></xsl:attribute>									  
					   <xsl:variable name="add1" select="/*/ASBQCPL1__RECORD/BQDCNB"/>
					  <!-- <xsl:variable name="add2" select="/*/ASBQCPL1__RECORD/BQDDNB"/> 63722 Issue 431-->
					   <xsl:variable name="add2" select="/*/ASBQCPL1__RECORD/BQDENB"/><!--63722 Issue 431-->
					   <xsl:variable name="PostCode" select="/*/ASBQCPL1__RECORD/BQAPNB"/>
					   <xsl:variable name="State" select="/*/ASBQCPL1__RECORD/BQBCCD"/>
					   <xsl:variable name="County" select="/*/ASBQCPL1__RECORD/BQH2TX"/>
					   <xsl:if test="$add1!='' and ($add2!='' or $PostCode!='' or $State!='' or $County!='' )">
					   <ItemIdInfo>
                     <InsurerId>1</InsurerId>
                  </ItemIdInfo>												  			 					 					  
					   <Addr>
						 <Addr1>						
							<xsl:value-of select="/*/ASBQCPL1__RECORD/BQDCNB"/>
						 </Addr1>
						 <Addr2>
						<xsl:if test="/*/ASBQCPL1__RECORD/BQDENB !='' "> 
							<!--<xsl:value-of select="/*/ASBQCPL1__RECORD/BQDDNB"/>63722 Issue 431-->
							<xsl:value-of select="/*/ASBQCPL1__RECORD/BQDENB"/><!--63722 Issue 431-->
						 </xsl:if>
						 </Addr2>
						 <City>							
							<xsl:value-of select="substring(//PMSP0200__RECORD/ADD0LINE04,1,28)"/>
						</City>
						<StateProvCd>								
							<xsl:value-of select="/*/ASBQCPL1__RECORD/BQBCCD"/>	                 	
						</StateProvCd>
						<PostalCode>							
							<xsl:value-of select="/*/ASBQCPL1__RECORD/BQAPNB"/>	
						</PostalCode>
							<County>
									<xsl:value-of select="/*/ASBQCPL1__RECORD/BQH2TX"/>	
							</County>
						</Addr>
						</xsl:if>
						<xsl:if test="$add1='0' or $add2='0' or $PostCode='0' or $State='0' or $County='0' or ($add1='')">
						<Addr>								          																																																				
							<Addr1>
							<xsl:value-of select="/*/PMSP0200__RECORD/ADD0LINE03"/>						
							</Addr1>
							<Addr2>						
							<xsl:value-of select="/*/PMSP0200__RECORD/ADD0LINE02"/>	
							</Addr2>							
							<PostalCode>
								<xsl:value-of select="/*/PMSP0200__RECORD/ZIP0POST"/>						
							</PostalCode>						
							<City>							
							<xsl:value-of select="substring(//PMSP0200__RECORD/ADD0LINE04,1,28)"/>
							</City>
							<StateProvCd>							
								<xsl:value-of select="substring(//PMSP0200__RECORD/ADD0LINE04,29,2)"/>                   	
							</StateProvCd>
							<CountyCd/>
							<County>
									<xsl:value-of select="/*/ASBQCPL1__RECORD/BQH2TX"/>
							</County>	
							</Addr>
					</xsl:if>
			 <!--113387 starts-->
			 <!--xsl:for-each select="//PMSP1200__RECORD">
					<xsl:call-template name="CreateAdditionalInterest"/>	
					</xsl:for-each-->
			 <xsl:for-each select="//PMSP1200__RECORD[USE0CODE!='LA']">
				 <xsl:call-template name="CreateAdditionalInterest"/>
					</xsl:for-each>														 
			 <!--113387 ends-->
				   </Location>
    </xsl:template>
	<!-- 55614 end -->
</xsl:stylesheet>

