<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

    <xsl:template match="/*/PMSP0200__RECORD" mode="Person">
        <InsuredOrPrincipal>
            <GeneralPartyInfo>
                <NameInfo>
	             <PersonName>
					     <Surname/>
                    <GivenName/>
		              <OtherGivenName/>
		              <TitlePrefix/>
		              <NameSuffix/>
                 </PersonName>
                 <com.csc_LongName><xsl:value-of select="translate(ADD0LINE01,';','')"/></com.csc_LongName>		                        
                </NameInfo>                
                <Addr>
                    <AddrTypeCd>StreetAddress</AddrTypeCd>
                    <!-- 33385 Begin -->
                    <!--Addr2><xsl:value-of select="ADD0LINE02"/></Addr2>
                    <Addr3><xsl:value-of select="ADD0LINE03"/></Addr3>
                    <Addr4><xsl:value-of select="ADD0LINE04"/></Addr4-->                    
                    <Addr2><xsl:value-of select="ADD0LINE03"/></Addr2>
		    <!-- 55614 <Addr3><xsl:value-of select="ADD0LINE02"/></Addr3>-->                    
					<!-- 33385 End -->                  
                    <City><xsl:value-of select="substring(ADD0LINE04,1,28)"/></City>
                    <StateProvCd><xsl:value-of select="substring(ADD0LINE04,29,2)"/></StateProvCd>
                    <PostalCode><xsl:value-of select="ZIP0POST"/></PostalCode>
                    <!-- 55614 begin -->
                    <CountryCd>USA</CountryCd>
                    <!-- 55614 end -->
                    <County/>
                    <!-- Case34770 starts Case 36998 Resolution37022-->
				        <!-- 55614 <CountryCd/> -->
                    <!--<CountyCd><xsl:value-of select="/*/ASBQCPL1__RECORD/BQH2TX"/></CountyCd>-->
				 <!--Case34770 Ends Case 36998 Resolution 37022-->
                </Addr>
                <Communications>
                    <PhoneInfo>
                        <PhoneTypeCd/>
                        <CommunicationUseCd/>
                        <PhoneNumber/>
                    </PhoneInfo>
                </Communications>
            </GeneralPartyInfo>
            <InsuredOrPrincipalInfo>
                <InsuredOrPrincipalRoleCd>I</InsuredOrPrincipalRoleCd>
            </InsuredOrPrincipalInfo>
        </InsuredOrPrincipal>  
        	<!-- 33385 Begin -->
	<!-- 	<xsl:if test="string-length(ADD0LINE02) &gt; 0">
        <InsuredOrPrincipal>
            <GeneralPartyInfo>
                <NameInfo>
	             <PersonName>
                       <Surname/>
                       <GivenName/>
		           <OtherGivenName/>
		           <TitlePrefix/>
		           <NameSuffix/>
                   </PersonName>
                   <com.csc_LongName><xsl:value-of select="ADD0LINE02"/></com.csc_LongName>		                        
                </NameInfo>                
            </GeneralPartyInfo>
            <InsuredOrPrincipalInfo>
                <InsuredOrPrincipalRoleCd>AI</InsuredOrPrincipalRoleCd>
            </InsuredOrPrincipalInfo>
        </InsuredOrPrincipal>  
		</xsl:if>	-->
		<!-- 33385 End -->
		<!-- 55614 begin -->
		<xsl:if test="string-length(ADD0LINE02) &gt; 0">
        <InsuredOrPrincipal>
            <GeneralPartyInfo>
                <NameInfo>
	             <PersonName>
                       <Surname/>
                       <GivenName/>
		           <OtherGivenName/>
		           <TitlePrefix/>
		           <NameSuffix/>
                   </PersonName>
                   <com.csc_LongName><xsl:value-of select="ADD0LINE02"/></com.csc_LongName>		                        
                </NameInfo>                
            </GeneralPartyInfo>
            <InsuredOrPrincipalInfo>
                <InsuredOrPrincipalRoleCd>AI</InsuredOrPrincipalRoleCd>
            </InsuredOrPrincipalInfo>
        </InsuredOrPrincipal>  
		</xsl:if>	
		<!-- 55614 End -->
    </xsl:template>
    
    <!-- 29653 Start -->
    <!-- <xsl:template match="/*/ISLPPP1__RECORD" mode="WClnsured"> -->
    <xsl:template match="/*/PMSP0200__RECORD" mode="WClnsured">
    <!-- 29653 End   -->
        <InsuredOrPrincipal>
            <GeneralPartyInfo>
                <NameInfo>
	          <CommlName>
	         <!-- 29653 Start --> 
		     <!--<CommercialName><xsl:value-of select="INS0NAME"/></CommercialName> -->
		     <CommercialName><xsl:value-of select="ADD0LINE01"/></CommercialName>
		     <!-- 29653 End   -->
                     <SupplementaryNameInfo>
                        <SupplementaryNameCd/>
                        <SupplementaryName/>
                     </SupplementaryNameInfo>
	          </CommlName>
	          <!-- 29653 Start -->
	          <!-- <LegalEntityCd><xsl:value-of select="LEGENTTYPE"/></LegalEntityCd> -->
	          <LegalEntityCd><xsl:value-of select="/*/PMSPWC02__RECORD/LEGALID"/></LegalEntityCd>
	          <!-- 29653 End -->
	               <!-- 29653 Start -->
                   <!--<com.csc_LongName><xsl:value-of select="INS0NAME"/></com.csc_LongName> -->	
                   <com.csc_LongName><xsl:value-of select="ADD0LINE01"/></com.csc_LongName>	
                   <!-- 29653 End   -->                        
                </NameInfo>                
                <Addr>
 	               <AddrTypeCd>StreetAddress</AddrTypeCd>
 	                <!-- 29653 Start -->
 	                <!--<Addr1><xsl:value-of select="ADD0LINE01"/></Addr1> -->
 	                <Addr1><xsl:value-of select="ADD0LINE03"/></Addr1>
                    <!-- 29653 End   -->
                    <Addr2><xsl:value-of select="ADD0LINE02"/></Addr2>
                    <!-- 29653 Start -->
                    <!--<City><xsl:value-of select="substring(CITY0STATE,1,28)"/></City>
                    <StateProvCd><xsl:value-of select="substring(CITY0STATE,29,2)"/></StateProvCd> -->
                    <City><xsl:value-of select="ADD0LINE04__RED/INSURED__CITY"/></City>
                    <StateProvCd><xsl:value-of select="ADD0LINE04__RED/INSURED__STATE"/></StateProvCd>
                    <!-- 29653 End  -->
                    <PostalCode><xsl:value-of select="ZIP0POST"/></PostalCode>
                    <CountryCd/>
                    <County/>
                </Addr>
                <Communications>
                    <PhoneInfo>
                        <PhoneTypeCd/>
                        <CommunicationUseCd/>
                        <PhoneNumber/>
                    </PhoneInfo>
                </Communications>
            </GeneralPartyInfo>
            <InsuredOrPrincipalInfo>
                <InsuredOrPrincipalRoleCd>I</InsuredOrPrincipalRoleCd>
            </InsuredOrPrincipalInfo>
        </InsuredOrPrincipal>  
    </xsl:template>

<!-- Case 31594 Begin -->
    <xsl:template match="/*/PMSP0200__RECORD" mode="Business">
    <xsl:variable name="LOB" select="/*/PMSP0200__RECORD/LINE0BUS"/>
    <!--55614 Begin -->
		<!--<InsuredOrPrincipal id="n0">-->
	 <InsuredOrPrincipal>
		<xsl:attribute name="id"><xsl:value-of select="concat($KEY,'-n0')"/></xsl:attribute>
	 <!--55614 End -->
			<GeneralPartyInfo>
				<NameInfo>
					<CommlName>
						<CommercialName><xsl:value-of select="ADD0LINE01"/></CommercialName>
						<SupplementaryNameInfo>
							<SupplementaryNameCd>DBA</SupplementaryNameCd>
							<!--SYSDST Log #38 starts-->
							<!--<SupplementaryName><xsl:value-of select="ADD0LINE02"/></SupplementaryName>-->
							<xsl:choose>
								<xsl:when test="$LOB='WCV' or $LOB='WCA'">
									<SupplementaryName>
									<!--111420 SYS/DST Log #322 start-->
										<!--<xsl:value-of select="/*/PMSP1200__RECORD/DESC0LINE1"/>-->
											<xsl:value-of select="/*/PMSP1200__RECORD[USE0CODE ='DA']/DESC0LINE1"/>
									<!--111420 SYS/DST Log #322 end-->
									</SupplementaryName>
								</xsl:when>
								<xsl:otherwise>
									<SupplementaryName>
										<xsl:value-of select="ADD0LINE02"/>
									</SupplementaryName>
								</xsl:otherwise>
							</xsl:choose>
							<!--SYSDST Log #38 ends-->
							</SupplementaryNameInfo>
					</CommlName>
				<!-- 34771 start -->
					<xsl:choose>								
                  	<xsl:when test="$LOB='WCV' or $LOB='WCA'">
						<LegalEntityCd><xsl:value-of select="/*/PMSPWC02__RECORD/LEGALID"/></LegalEntityCd>
					</xsl:when>
					<xsl:when test="$LOB='BOP'">
						<LegalEntityCd><xsl:value-of select="/*/ASBACPL1__RECORD/BAH5TX"/></LegalEntityCd>
					</xsl:when>
					</xsl:choose>
					<!-- 34771 end -->
					<!-- Case 39370 Begin -->
					<TaxIdentity>
					  <TaxIdTypeCd>FEIN</TaxIdTypeCd>      <!-- 55614 -->
						<TaxId><xsl:value-of select="/*/PMSPWC02__RECORD/FEIN"/></TaxId>
					</TaxIdentity>
					<!-- Case 39370 End -->
					<com.csc_LongName><xsl:value-of select="ADD0LINE01"/></com.csc_LongName>		                        
				</NameInfo>                
				<Addr>
					<AddrTypeCd>StreetAddress</AddrTypeCd>
                    <Addr1><xsl:value-of select="ADD0LINE03"/></Addr1>
                    <Addr2></Addr2>
                    <City><xsl:value-of select="ADD0LINE04__RED/INSURED__CITY"/></City>
                    <StateProvCd><xsl:value-of select="ADD0LINE04__RED/INSURED__STATE"/></StateProvCd>
                    <PostalCode><xsl:value-of select="ZIP0POST"/></PostalCode>
                    <CountryCd/>
                    <County/>
                </Addr>
                <Communications>
                    <PhoneInfo>
						<PhoneTypeCd/>
                        <CommunicationUseCd/>
                        <PhoneNumber/>
                    </PhoneInfo>
                </Communications>
			</GeneralPartyInfo>
            <InsuredOrPrincipalInfo>
                <InsuredOrPrincipalRoleCd>I</InsuredOrPrincipalRoleCd>
		<!-- Case 34768 Begin -->
		<BusinessInfo>
			<com.csc_NCCIIDNumber>
				<xsl:value-of select="/*/PMSPWC02__RECORD/POOLID"/>
			</com.csc_NCCIIDNumber>
		</BusinessInfo>
		<!-- Case 34768 End -->
            </InsuredOrPrincipalInfo>
        </InsuredOrPrincipal>  
    </xsl:template>
    
    <xsl:template name="WCLocationInfo">
		<InsuredOrPrincipal>
			<xsl:variable name="LocNbr" select="format-number(SITE,'0')"/>
			 <!--55614 Begin -->
			<!--<xsl:attribute name="id"><xsl:value-of select="concat('n',$LocNbr)"/></xsl:attribute>-->
			<xsl:attribute name="id"><xsl:value-of select="concat($KEY,'-n',position())"/></xsl:attribute>
			 <!--55614 End -->
			<GeneralPartyInfo>
				<NameInfo>
					<CommlName>
						<CommercialName><xsl:value-of select="NAME"/></CommercialName>
						<!-- 55614 Begin
						<SupplementaryNameInfo>
							<SupplementaryNameCd>DBA</SupplementaryNameCd>
							<SupplementaryName><xsl:value-of select="ADDRESS1"/></SupplementaryName>
						</SupplementaryNameInfo>
						 55614 End -->
					</CommlName>
					<!-- 55614 Begin
					<LegalEntityCd />
               <com.csc_LongName />
               55614 End -->
				</NameInfo>
				<!-- 55614 Begin -->                
				<!--<Addr>
					<AddrTypeCd>StreetAddress</AddrTypeCd>
                    <Addr1><xsl:value-of select="ADDRESS2"/></Addr1>
                    <Addr2></Addr2>
                    <City><xsl:value-of select="substring(CITYST,1,28)"/></City>
			  <StateProvCd><xsl:value-of select="substring(CITYST,29,2)"/></StateProvCd> 
                    <PostalCode><xsl:value-of select="ZIP"/></PostalCode>
                    <CountryCd/>
                    <County/>
                </Addr>
                <Communications>
                    <PhoneInfo>
						<PhoneTypeCd/>
                        <CommunicationUseCd/>
                        <PhoneNumber/>
                    </PhoneInfo>
                </Communications>-->
         <!-- 55624 End -->       
			</GeneralPartyInfo>
            <InsuredOrPrincipalInfo>
                <InsuredOrPrincipalRoleCd>Principal</InsuredOrPrincipalRoleCd>
            </InsuredOrPrincipalInfo>
        </InsuredOrPrincipal>  
    </xsl:template>
<!-- Case 31594 End -->
<!--98500 Begin -->
    <xsl:template name="CreateContactInfo">
		<InsuredOrPrincipal>
			<xsl:attribute name="id"><xsl:value-of select="concat($KEY,'-n98')"/></xsl:attribute>
			<GeneralPartyInfo>
				<NameInfo>
					<PersonName>
                    <Surname/>
                    <GivenName/>
               </PersonName>
					<com.csc_LongName><xsl:value-of select="/*/BASP0200E__RECORD/CONTACT1"/></com.csc_LongName>
				   </NameInfo>
                <Communications>
                    <PhoneInfo>
                        <PhoneNumber><xsl:value-of select="/*/BASP0200E__RECORD/PHONE1"/></PhoneNumber>
                    </PhoneInfo>
                    <EmailInfo>
                       <EmailAddr><xsl:value-of select="/*/BASP0200E__RECORD/EMAILADDR1"/></EmailAddr>
                    </EmailInfo>
                </Communications>
			</GeneralPartyInfo>
            <InsuredOrPrincipalInfo>
                <InsuredOrPrincipalRoleCd>PrimaryContact</InsuredOrPrincipalRoleCd>
            </InsuredOrPrincipalInfo>
        </InsuredOrPrincipal>
		<InsuredOrPrincipal>
			<xsl:attribute name="id"><xsl:value-of select="concat($KEY,'-n99')"/></xsl:attribute>
			<GeneralPartyInfo>
				<NameInfo>
					<PersonName>
						<Surname/>
						<GivenName/>
					</PersonName>
					<com.csc_LongName>
						<xsl:value-of select="/*/BASP0200E__RECORD/CONTACT2"/>
					</com.csc_LongName>
				</NameInfo>
				<Communications>
					<PhoneInfo>
						<PhoneNumber>
							<xsl:value-of select="/*/BASP0200E__RECORD/PHONE2"/>
						</PhoneNumber>
					</PhoneInfo>
					<EmailInfo>
						<EmailAddr>
							<xsl:value-of select="/*/BASP0200E__RECORD/EMAILADDR2"/>
						</EmailAddr>
					</EmailInfo>
				</Communications>
			</GeneralPartyInfo>
			<InsuredOrPrincipalInfo>
				<InsuredOrPrincipalRoleCd>SecondaryContact</InsuredOrPrincipalRoleCd>
			</InsuredOrPrincipalInfo>
		</InsuredOrPrincipal>
	</xsl:template>
    <!--98500 End -->
</xsl:stylesheet>

