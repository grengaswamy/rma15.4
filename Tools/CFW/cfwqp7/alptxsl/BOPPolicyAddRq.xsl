<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="CommonFuncRq.xsl"/>  <!-- Common Formatting Routines -->
    <xsl:include href="PMSP0000Rq.xsl"/>  <!-- Activity Record -->
    <xsl:include href="PMSP0200Rq.xsl"/>  <!-- Basic Contract Record -->
    <xsl:include href="PMSP1200Rq.xsl"/>  <!-- Additional Interest Record -->
    <xsl:include href="PMSP1200ATRq.xsl"/>  <!-- Issue 53911 - one more Additional Interest Record for BOP -->
    <xsl:include href="ASBACPL1Rq.xsl"/>  <!-- Policy Level Record -->
    <xsl:include href="ASBBCPL1Rq.xsl"/>  <!-- Insurance Line Record -->
    <xsl:include href="ASBCCPL1Rq.xsl"/>  <!-- Product Record -->
    <xsl:include href="ASBUCPL1Rq.xsl"/>  <!-- Location Record -->
    <xsl:include href="ASBVCPL1Rq.xsl"/>  <!-- Sublocation Record -->
    <xsl:include href="ASBYCPL1Rq.xsl"/>  <!-- Coverage Record -->
    <xsl:include href="ASB5CPL1Rq.xsl"/>  <!-- Unit Record -->
	<xsl:include href="ALB5CPL1Rq.xsl"/>
	<xsl:include href="ASBZCPL1Rq.xsl"/>  <!-- Coverage Supplement -->
	<xsl:include href="PTInfoRq.xsl"/>    <!-- Case 39568 -->
   <xsl:include href="BASP0200ERq.xsl"/> <!-- 57418 start -->
  
	<!-- Case 27708 Start Jeff Simmons Client file entries -->
	<xsl:include href="BASCLT0100Rq.xsl"/>
	<xsl:include href="BASCLT0300Rq.xsl"/>
	<xsl:include href="BASCLT1400Rq.xsl"/>
 	<xsl:include href="BASCLT1500Rq.xsl"/> 
	<!-- Case 27708 End   Jeff Simmons Client file entries -->

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
    <xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy/CompanyProductCd"/>
    <xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy/PolicyNumber"/>
    <xsl:variable name="MOD">00</xsl:variable>    
    <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy/LOBCd"/>   
    <!-- 29653 start -->
    <xsl:variable name="TYPEACT">
  	<xsl:choose>
		<!--<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode='N'">NB</xsl:when>-->			<!--103409-->
		<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd='N'">NB</xsl:when>						<!--103409-->
		<xsl:otherwise>EN</xsl:otherwise>
  	</xsl:choose>
    </xsl:variable> 
    <!-- 29653 end -->
	<xsl:variable name="InsLine">BOP</xsl:variable>
	   <xsl:template match="/">
        <xsl:element name="BOPPolicyAddRq">
 			 <!-- 39568 Start -->
             <xsl:apply-templates select="ACORD/InsuranceSvcRq/BOPPolicyAddRq" mode="CreatePTInfo"/> 
             <!-- 39568 End   -->
	  <!-- Build BASP0200E record - 57418 start-->
	  <xsl:apply-templates select="ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy" mode="BASP0200E"/>
	  <!-- 57418 end -->             
<!-- Build Policy Activity Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy" mode="PMSP0000"/>
<!-- Build Policy Basic Contract Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/BOPPolicyAddRq" mode="PMSP0200"/>
<!-- Build Policy Level Additional Interest Records -->
            <xsl:for-each select="ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy/AdditionalInterest">
                <xsl:call-template name="CreateAddlInterest"/>
            </xsl:for-each>
            <xsl:for-each select="ACORD/InsuranceSvcRq/BOPPolicyAddRq/Location">
                <xsl:variable name="Location" select="ItemIdInfo/InsurerId"/>
<!-- Build Location Records -->
               <xsl:call-template name="CreateLocation">
                     <xsl:with-param name="Location" select="$Location"/>    
                </xsl:call-template>
<!-- Build SubLocation Records -->
                <xsl:for-each select="SubLocation">
                    <xsl:call-template name="CreateSubLocation">
                        <xsl:with-param name="Location" select="$Location"/>
                    </xsl:call-template>
                </xsl:for-each>
<!-- Build Additional Interests Attached to SubLocations -->
                <xsl:for-each select="SubLocation/AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">                
                      <xsl:call-template name="CreateAddlInterest">
                         <xsl:with-param name="Location" select="$Location"/>
                         <!-- <xsl:with-param name="SubLocation" select="ItemIdInfo/InsurerId"/>  -->
						 <xsl:with-param name="SubLocation" select="../ItemIdInfo/InsurerId"/>    <!-- .NET Correct -->                  
                      </xsl:call-template>
                   </xsl:if>                    
                </xsl:for-each>

<!-- Issue 53911 Build 1200AT Record for each Additional Interests Attached to SubLocations -->
                <xsl:for-each select="SubLocation/AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">                
                      <xsl:call-template name="CreateBOPAddlInterest1200AT">
                         <xsl:with-param name="Location" select="$Location"/>
                         <xsl:with-param name="SubLocation" select="../ItemIdInfo/InsurerId"/>
                      </xsl:call-template>
                   </xsl:if>                    
                </xsl:for-each>
<!-- End Issue 53911 -->

<!-- Build Additional Interests Attached to this Location -->
                <xsl:for-each select="AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                        <xsl:call-template name="CreateAddlInterest">
                            <xsl:with-param name="Location" select="$Location"/>    
                        </xsl:call-template>
                   </xsl:if>                          
                </xsl:for-each>
            </xsl:for-each>
<!-- Build BOP Policy Level Record -->
            <xsl:apply-templates select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq" mode="ASBACPL1"/>
<!-- Build BOP Insurance Line Information Records -->
            <xsl:for-each select="ACORD/InsuranceSvcRq/BOPPolicyAddRq/BOPLineBusiness">
<!-- Build Insurance Line Record -->
                <xsl:call-template name="BuildInsuranceLine">
                    <xsl:with-param name="InsLine" select="$InsLine"/>
                </xsl:call-template>
<!-- Build Product Record -->               
                <xsl:variable name="RateState" select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy/ControllingStateProvCd"/>
<!-- 37036 start -->
                <!--<xsl:for-each select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/Location">
				<xsl:call-template name="BuildProductRecord">
                        <xsl:with-param name="InsLine" select="$InsLine"/>
                        <xsl:with-param name="RateState" select="Addr/StateProvCd"/>
                </xsl:call-template>
				</xsl:for-each>-->
				<xsl:for-each select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/BOPLineBusiness/com.csc_ModificationFactors">
				    <xsl:call-template name="BuildProductRecord">
                        <xsl:with-param name="InsLine" select="$InsLine"/>
                        <xsl:with-param name="RateState" select="StateProvCd"/>
                    </xsl:call-template>
                </xsl:for-each>
				<!-- 37036 end -->
<!--Building Information start-->
 				<xsl:for-each select="/ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlSubLocation">
 				      <xsl:variable name="LocationRef" select="@LocationRef"/>
                      <xsl:variable name="SubLocationRef" select="@SubLocationRef"/>
 				      <xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]"/>
						<xsl:call-template name="BuildUnitRecord">
 							<xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
					 		<xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
					    	<xsl:with-param name="RateState" select="$RateState"/>
					    	<xsl:with-param name="InsLine" select="$InsLine"/>
					    	<xsl:with-param name="Node" select="$Node"/>
        				</xsl:call-template>
        				<xsl:call-template name="BuildALB5CPL1">
               				<xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
               				<xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
               				<xsl:with-param name="InsLine" select="$InsLine"/>
               				<xsl:with-param name="Node" select="$Node"/>
            			</xsl:call-template>                
				</xsl:for-each>
<!-- Build Property Unit and Regular Coverage Records -->
				<xsl:for-each select="PropertyInfo/CommlCoverage">
					<xsl:if test="CoverageCd!=''">
					<xsl:call-template name="BuildCoverage">
                    	<xsl:with-param name="Location" select="0"/>
                    	<xsl:with-param name="SubLocation" select="0"/>
                    	<xsl:with-param name="InsLine" select="$InsLine"/>
                    	<xsl:with-param name="RateState" select="$RateState"/>
               		</xsl:call-template>
					<xsl:for-each select="CommlCoverageSupplement">
						<xsl:if test="(position() mod 4 = '1') and substring-after(com.csc_Description,'_')!=''"> 
							<xsl:call-template name="BuildCoverageSupplement">
                    		<xsl:with-param name="Location" select="0"/>
                    		<xsl:with-param name="SubLocation" select="0"/>
							<xsl:with-param name="Position" select="position()"/>
						</xsl:call-template>
						</xsl:if>
					</xsl:for-each> 
					</xsl:if>
				</xsl:for-each> 
				<xsl:for-each select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='']">
				<xsl:variable name="Location" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
        		<xsl:variable name="SubLocation" select="substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)"/>
					<xsl:for-each select="CommlCoverage"> 
						<xsl:if test="CoverageCd!=''">
        		        <xsl:call-template name="BuildCoverage">
                    	<xsl:with-param name="Location" select="$Location"/>
                    	<xsl:with-param name="SubLocation" select="$SubLocation"/>
                    	<xsl:with-param name="InsLine" select="$InsLine"/>
                    	<xsl:with-param name="RateState" select="$RateState"/>
               			</xsl:call-template>
						<xsl:for-each select="CommlCoverageSupplement">
						<xsl:if test="(position() mod 4 = '1') and substring-after(com.csc_Description,'_')!=''"> 
							<xsl:call-template name="BuildCoverageSupplement">
                    		<xsl:with-param name="Location" select="$Location"/>
                    		<xsl:with-param name="SubLocation" select="$SubLocation"/>
							<xsl:with-param name="Position" select="position()"/>
						</xsl:call-template>
						</xsl:if>
					</xsl:for-each> 
						</xsl:if>
					</xsl:for-each>
				</xsl:for-each>
				<xsl:for-each select="PropertyInfo/GlassSignInfo">
					<xsl:variable name="Location" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
        			<xsl:variable name="SubLocation" select="substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)"/>
					<xsl:for-each select="GlassSignSchedule">
					<xsl:variable name="PrevCoverage">
					<xsl:choose>
               <xsl:when test="count(GlassSignSchedule) > 1">
                <xsl:value-of select="preceding-sibling::GlassSignSchedule[position()=1]/ScheduleTypeCd"/>
               </xsl:when>
               <xsl:otherwise></xsl:otherwise>
               </xsl:choose>
               </xsl:variable>
					<xsl:variable name="CurrCoverage"><xsl:value-of select="ScheduleTypeCd"/>
					</xsl:variable>
					<xsl:if test="$CurrCoverage!=$PrevCoverage"> 
		            <xsl:call-template name="BuildCoverage">
                    	<xsl:with-param name="Location" select="$Location"/>
                    	<xsl:with-param name="SubLocation" select="$SubLocation"/>
                    	<xsl:with-param name="InsLine" select="$InsLine"/>
                    	<xsl:with-param name="RateState" select="$RateState"/>
               		</xsl:call-template>
					</xsl:if>
					</xsl:for-each>
				</xsl:for-each>
             </xsl:for-each> 
             
             <!-- Case 27708 Start Jeff Simmons Client file entries -->
			 <xsl:for-each select="ACORD/InsuranceSvcRq/BOPPolicyAddRq">
                <xsl:call-template name="CreateBASCLT0100"></xsl:call-template>
                <xsl:call-template name="CreateBASCLT0300"></xsl:call-template>
                <xsl:call-template name="CreateBASCLT1400"></xsl:call-template>
			 </xsl:for-each> 
                              
             <xsl:for-each select="ACORD/InsuranceSvcRq/BOPPolicyAddRq/CommlPolicy/AdditionalInterest">
                <xsl:call-template name="CreateBASCLT1500"/>
             </xsl:for-each>
            
             <xsl:for-each select="ACORD/InsuranceSvcRq/BOPPolicyAddRq/Location">
                <xsl:variable name="Location" select="ItemIdInfo/InsurerId"/>
                <xsl:for-each select="SubLocation/AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">                
                      <xsl:call-template name="CreateBASCLT1500">
                         <xsl:with-param name="Location" select="$Location"/>
                         <xsl:with-param name="SubLocation" select="ItemIdInfo/InsurerId"/>                         
                      </xsl:call-template>
                   </xsl:if>                    
                </xsl:for-each>
                <xsl:for-each select="AdditionalInterest">
                   <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                        <xsl:call-template name="CreateBASCLT1500">
                            <xsl:with-param name="Location" select="$Location"/>    
                        </xsl:call-template>
                   </xsl:if>                          
                </xsl:for-each>
             </xsl:for-each>
             <!-- Case 27708 End   Jeff Simmons Client file entries -->
                
        </xsl:element>
    </xsl:template>
   
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="file://d:\NETIUT\WEBSERVER\Log Files\SYS\#108/21601276977309255323BOAddRq.xml" htmlbaseurl="" outputurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->