<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/Location (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location (Mod) - 31594
-->
  <xsl:template name="CreatePMSPSA05">
    <xsl:variable name="TableName">PMSPSA05</xsl:variable>
    <xsl:variable name="LocationId" select="@id"/>
    <xsl:variable name="LocationNumber" select="substring(@id,2,string-length(@id)-1)"/>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPSA05</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPSA05__RECORD>
      		<SYMBOL>
          		<xsl:value-of select="$SYM"/>
        	</SYMBOL>
        	<POLICYNO>
          		<xsl:value-of select="$POL"/>
        	</POLICYNO>
        	<MODULE>
          		<xsl:value-of select="$MOD"/>
        	</MODULE>
        	<MASTERCO>
          		<xsl:value-of select="$MCO"/>
        	</MASTERCO>
        	<LOCATION>
          		<xsl:value-of select="$LOC"/>
        	</LOCATION>
      		<UNITNO>
      			<xsl:call-template name="FormatData">
            		<xsl:with-param name="FieldName">SITE</xsl:with-param>
            		<xsl:with-param name="FieldLength">5</xsl:with-param>
            		<xsl:with-param name="Value" select="$LocationNumber"/>
            		<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template>
          	</UNITNO>
			<ENTRYDTE>
				<!-- 29653 Start -->
				<!--<xsl:call-template name="ConvertISODateToPTDate">
                  	<xsl:with-param name="Value" select="ContractTerm/EffectiveDt"/>
                </xsl:call-template> -->
              	<xsl:call-template name="ConvertISODateToPTDate">
                  	<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
               	</xsl:call-template>
              	<!-- 29653 End --> 
			</ENTRYDTE>
			<NAME>
			    <!-- 29653 Start -->
				<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal[substring(@id,2,string-length(@id)-1)=$LocationNumber]/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/> -->
			   	<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal
        							[substring(@id,2)=$LocationNumber]
        							/GeneralPartyInfo/NameInfo/com.csc_LongName"/>
			    <!-- 29653 End  -->
			</NAME>	
			<DESCUSE1>
				<xsl:value-of select="Addr/Addr1"/>
			</DESCUSE1>
			<DESCUSE2>
				<xsl:value-of select="substring(concat(Addr/City, '                                                                                                    '), 1, 28)"/>
          		<xsl:value-of select="Addr/StateProvCd"/>
			</DESCUSE2>
			<UNITPREM>00000000000</UNITPREM>
			<LOB>
			<!-- WC Rewrite -->
			<xsl:choose>
			 	<xsl:when test="$LOB = 'WC' or $LOB = 'WCV'">
					   WCV
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$SYM"/>
				</xsl:otherwise>
				</xsl:choose>
			<!-- WC Rewrite -->
			</LOB>
			<!-- 50764 start -->
			<xsl:choose>
				<xsl:when test="$TYPEACT='RB'">
					<TYPEACT>RB</TYPEACT>
				</xsl:when>
				<xsl:otherwise>
					<TYPEACT>NB</TYPEACT>
				</xsl:otherwise>
			</xsl:choose>
			<!-- 50764 end --> 
		</PMSPSA05__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>

  <!--
  The base xpath for this segment:
  /ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Quote)
  /ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Mod) - 31594
  -->
  
<!-- Case 31594 begin - Modified the template to build one SA05 segement for each location -->
<xsl:template name="CreatePMSPSA05Defaults">
    <xsl:variable name="TableName">PMSPSA05</xsl:variable>
	<xsl:variable name="LocationNbr" select="@LocationRef"/>
	<xsl:variable name="LocationPath" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/Addr"/>
	<!--<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode"/>-->	<!--103409-->
	<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd"/>	<!--103409-->
	<xsl:if test="$TranType != 'A' or $LocationNbr != 'l0'">
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPSA05</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPSA05__RECORD>
      		<SYMBOL>
          		<xsl:value-of select="$SYM"/>
        	</SYMBOL>
        	<POLICYNO>
          		<xsl:value-of select="$POL"/>
        	</POLICYNO>
        	<MODULE>
          		<xsl:value-of select="$MOD"/>
        	</MODULE>
        	<MASTERCO>
          		<xsl:value-of select="$MCO"/>
        	</MASTERCO>
        	<LOCATION>
          		<xsl:value-of select="$LOC"/>
        	</LOCATION>
      		<UNITNO>
      			 <xsl:call-template name="FormatData">
            			<xsl:with-param name="FieldName">SITE</xsl:with-param>
            			<xsl:with-param name="FieldLength">5</xsl:with-param>
            			<xsl:with-param name="Value" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
            			<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template>
          	</UNITNO>
			<ENTRYDTE>
				<xsl:call-template name="ConvertISODateToPTDate">
                  	<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
               	</xsl:call-template>
			</ENTRYDTE>
			<NAME>
				<xsl:variable name="LocationNumber" select="substring(/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/@NameInfoRef,2)"/>
        			<xsl:value-of select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal
        								[substring(@id,2)=$LocationNumber]
        								/GeneralPartyInfo/NameInfo/com.csc_LongName"/>
        								<!--/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>-->
			</NAME>	
			<DESCUSE1>
				<!--<xsl:value-of select="Addr/Addr1"/>-->
				<xsl:value-of select="$LocationPath/Addr1"/>
			</DESCUSE1>
			<DESCUSE2>
				<!--<xsl:value-of select="substring(concat(Addr/City, '                                                                                                    '), 1, 28)"/>
          		<xsl:value-of select="Addr/StateProvCd"/>-->
				<xsl:value-of select="substring(concat($LocationPath/City, '                                                                                                    '), 1, 28)"/>
          		<xsl:value-of select="$LocationPath/StateProvCd"/>
			</DESCUSE2>
			<UNITPREM>00000000000</UNITPREM>
			<LOB>
			<!-- WC Rewrite -->
			<xsl:choose>
				<xsl:when test="$LOB = 'WC' or $LOB = 'WCV'">
					   WCV
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$SYM"/>
				</xsl:otherwise>
				</xsl:choose>
			</LOB>
			<!-- WC Rewrite -->
			<!-- 50764 start -->
			<xsl:choose>
				<xsl:when test="$TYPEACT='RB'">
					<TYPEACT>RB</TYPEACT>
				</xsl:when>
				<xsl:otherwise>
					<TYPEACT>NB</TYPEACT>
				</xsl:otherwise>
			</xsl:choose>
			<!-- 50764 end --> 
		</PMSPSA05__RECORD>
    </BUS__OBJ__RECORD>
    </xsl:if>
  </xsl:template>
  <!-- Case 31594 end -->


</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->