<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform XML from the iSolutions database
into the ACORD XML Quote business message before sending to the Communications Frameworks   
E-Service case 14734 
***********************************************************************************************
-->
   <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

  <xsl:template name="CreateWorkCompLocInfo">
      <!-- 29653 Start -->
      <xsl:param name="Rate"/>
      <!-- 29653 End -->
    <xsl:variable name="State" select="STATE"/>
    <WorkCompLocInfo>
      <xsl:variable name="Site" select="UNITNO"/>
      <xsl:attribute name="LocationRef">
        <xsl:value-of select="concat('l',$Site)"/>
      </xsl:attribute>
      <WorkCompRateClass>
        <ActualRemunerationAmt>
          <Amt>
		  	<!-- 31594 start -->
            <!--<xsl:value-of select="LIMITAGGER"/>-->
			<xsl:choose>
            <!-- 29653 Start -->
			<!-- <xsl:when  test="/*/ISLPPP1__RECORD/TYPE0ACT = 'EN'"> -->
            <xsl:when  test="/*/PMSPSA15__RECORD/TYPE0ACT = 'EN'">
            <!-- 29653 End   --> 
				<xsl:value-of select="PREMIUM"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="LIMITAGGER"/>
			</xsl:otherwise>
			</xsl:choose>
			<!-- 31594 end -->
          </Amt>
        </ActualRemunerationAmt>
        <NumEmployees>
        </NumEmployees>
        <NumEmployeesFullTime>
        </NumEmployeesFullTime>
        <NumEmployeesPartTime>
        </NumEmployeesPartTime>
        <Rate>
          <!-- 29653 Start --> 
          <!--<xsl:value-of select="FORMATRATE"/> -->
          <xsl:value-of select="$Rate"/>
          <!-- 29653 End -->
        </Rate>
        <RatingClassificationCd>
          <!-- ISsue 78200 modified the field size to 6 byte from 4
          <xsl:value-of select="CLASSNUM"/>-->
		  <xsl:choose>
		  <xsl:when test="string-length(CLASSNUM)='3'">
		  	<xsl:value-of select="concat(CLASSNUM,'$$$')"/>
		  </xsl:when> 
		  <xsl:when test="string-length(CLASSNUM)='4'">
		  	<xsl:value-of select="concat(CLASSNUM,'$$')"/>
		  </xsl:when> 
		  <xsl:when test="string-length(CLASSNUM)='5'">
		  	<xsl:value-of select="concat(CLASSNUM,'$')"/>
		  </xsl:when> 
		  <xsl:otherwise>
			<xsl:value-of select="CLASSNUM"/>
		  </xsl:otherwise>
		  </xsl:choose>

          <!-- 29653 Start -->
          <!--<xsl:value-of select="DESCSEQ"/> -->
          <xsl:variable name="CovSeq" select="COVSEQ"/>
			<xsl:variable name="TransSeq" select="TRANSSEQ"/>
			<xsl:value-of select="substring(//PMSPSA35__RECORD[UNITNO=$Site and 
																COVSEQ=$CovSeq and 
																TRANSSEQ=$TransSeq]/BUREAU34, 3, 2)"/>
		  <!-- 29653 End -->															
        </RatingClassificationCd>
	<!-- Case 39344 -->
	<com.csc_EffectiveDt>
	     <xsl:call-template name="ConvertPTDateToISODate">
                <xsl:with-param name="Value" select="COVEFFDTE"/>
         </xsl:call-template>
    </com.csc_EffectiveDt>
		
	<com.csc_ExpirationDt>
		<xsl:call-template name="ConvertPTDateToISODate">
                <xsl:with-param name="Value" select="COVEXPDTE"/>
         </xsl:call-template>
	</com.csc_ExpirationDt>
	<!-- Case 39344 -->
        <Exposure>
          <xsl:value-of select="EXPOSURE"/>
        </Exposure>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_HostRowSeq</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="RECID"/>
            </OtherId>
          </OtherIdentifier>
        </ItemIdInfo>
      </WorkCompRateClass>
    </WorkCompLocInfo>
  </xsl:template>
  
<!-- Case 31594 Begin -->
  <xsl:template name="CreateWCLocInfo">
	<xsl:variable name="State" select="STATE"/>
	<WorkCompLocInfo>
	<!-- 101660 Begin -->
		<!--<xsl:variable name="Site" select="UNITNO"/>-->
	  <!--<xsl:variable name="Deleted" select="count(//PMSPWC04__RECORD[(DROPIND != 'N' and 
																		string-length(DROPIND)!=0) and 
																		SITE &lt; $Site])"/>
		<xsl:variable name="ServerSeq" select="$Site - $Deleted"/>
		<xsl:attribute name="LocationRef">
			<xsl:value-of select="concat('l',$ServerSeq)"/>
		</xsl:attribute>
	   <NumEmployees><xsl:value-of select="/*/PMSPWC04__RECORD[STATE=$State]/NUMEMPL"/></NumEmployees>-->
		<xsl:variable name="Site" select="SITE"/>
		
		<xsl:variable name="Location">
		 <xsl:call-template name="GetPosition"> 
		   <xsl:with-param name="Site" select="SITE"/>
		 </xsl:call-template>
		</xsl:variable>
		<!-- 55614 Begin -->
		<!--<xsl:attribute name="LocationRef"><xsl:value-of select="concat('l',$Location)"/></xsl:attribute>-->
		<xsl:attribute name="LocationRef"><xsl:value-of select="concat($KEY,'-l',$Location)"/></xsl:attribute>
		<!-- 55614 End -->	
	   <NumEmployees><xsl:value-of select="NUMEMPL"/></NumEmployees>
		
		<xsl:for-each select="/*/PMSPSA15__RECORD[UNITNO = $Site]">
		<!-- 101660 End -->
		<!--Issue # 50771 start-->
			<xsl:variable name="CovSeq" select="COVSEQ"/>
			<xsl:variable name="TransSeq" select="TRANSSEQ"/>
			<!--Issue # 50771 End-->
		<WorkCompRateClass>
        	<ActualRemunerationAmt>
				<Amt>
					<xsl:value-of select="LIMITAGGER"/>
				</Amt>
			</ActualRemunerationAmt>
			<NumEmployees>
			</NumEmployees>
			<NumEmployeesFullTime>
			</NumEmployeesFullTime>
			<NumEmployeesPartTime>
			</NumEmployeesPartTime>
			<Rate>
				<xsl:value-of select="TOTALPREM"/>
			</Rate>
			<RatingClassificationCd>
				
				<!-- ISsue 78200 modified the field size to 6 byte from 4
				<xsl:value-of select="CLASSNUM"/>-->
				<xsl:choose>
				<xsl:when test="string-length(CLASSNUM)='3'">
		  			<xsl:value-of select="concat(CLASSNUM,'$$$')"/>
				</xsl:when> 
				<xsl:when test="string-length(CLASSNUM)='4'">
		  			<xsl:value-of select="concat(CLASSNUM,'$$')"/>
				</xsl:when> 
				<xsl:when test="string-length(CLASSNUM)='5'">
		  			<xsl:value-of select="concat(CLASSNUM,'$')"/>
				</xsl:when> 
				<xsl:otherwise>
		  			<xsl:value-of select="CLASSNUM"/>
				</xsl:otherwise>
				</xsl:choose>

				<!--<xsl:variable name="CovSeq" select="COVSEQ"/>
				<xsl:variable name="TransSeq" select="TRANSSEQ"/>--><!--Issue # 50771 -->
				<!--62825 - Replaced // with /*/ -->
				<xsl:value-of select="substring(/*/PMSPSA35__RECORD[UNITNO=$Site and 
																	COVSEQ=$CovSeq and 
																	TRANSSEQ=$TransSeq]/BUREAU34, 3, 2)"/>
			</RatingClassificationCd>
			<Exposure>
				<xsl:value-of select="EXPOSURE"/>
			</Exposure>
			<!--Issue # 50771 start-->
			<xsl:choose> 
			<xsl:when test=" count(/*/PMSPSA36__RECORD[UNITNO=$Site and 
																	COVSEQ=$CovSeq and 
																				TRANSSEQ=$TransSeq])> 0">
				<!-- 55614 Begin
			<ItemIdInfo> -->
			<com.csc_ItemIdInfo>
			<!-- 55614 End -->
				<OtherIdentifier>
					<OtherIdTypeCd>DeemedType</OtherIdTypeCd>
					<OtherId>
						<xsl:value-of select="substring(/*/PMSPSA36__RECORD[UNITNO=$Site and 
																	COVSEQ=$CovSeq and 
																				TRANSSEQ=$TransSeq]/SPEC50,16,2)"/>
					</OtherId>
				</OtherIdentifier>
			<!-- 55614 Begin
			</ItemIdInfo> -->
				<OtherIdentifier>
					<!--<OtherIdTypeCd>com.csc_SequenceNumber</OtherIdTypeCd>-->		<!--103409-->
					<OtherIdTypeCd>SeqNbr</OtherIdTypeCd>								<!--103409-->
					<OtherId>
						<!--103409 Starts-->
						<!--<xsl:value-of select="substring(/*/PMSPSA36__RECORD[UNITNO=$Site and 
																	COVSEQ=$CovSeq and 
																				TRANSSEQ=$TransSeq]/SPEC50,16,2)"/>-->
						<xsl:value-of select="number(substring(/*/PMSPSA36__RECORD[UNITNO=$Site and 
																	COVSEQ=$CovSeq and 
																				TRANSSEQ=$TransSeq]/SPEC50,18,5))"/>
						<!--103409 Ends-->
					</OtherId>
				</OtherIdentifier>
			</com.csc_ItemIdInfo>
			<!-- 
			<com.csc_SequenceNumber>
			<xsl:value-of select="number(substring(/*/PMSPSA36__RECORD[UNITNO=$Site and 
																	COVSEQ=$CovSeq and 
																				TRANSSEQ=$TransSeq]/SPEC50,18,5))"/>
			</com.csc_SequenceNumber>-->
			<!-- 55614 End -->
			</xsl:when>
			<xsl:otherwise>
			<!--Issue # 50771 End-->
			<!-- 55614 Begin
			<ItemIdInfo> -->
			<com.csc_ItemIdInfo>
			<!-- 55614 End -->
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_HostRowSeq</OtherIdTypeCd>
					<OtherId>
						<xsl:value-of select="COVSEQ"/>
					</OtherId>
				</OtherIdentifier>
				<!-- 55614 Begin
			</ItemIdInfo> -->
			</com.csc_ItemIdInfo>
			<!-- 55614 End -->
			<!--Issue # 50771 Start-->
			</xsl:otherwise>
			</xsl:choose>
			<!--Issue # 50771 End-->
		</WorkCompRateClass>
		</xsl:for-each>              <!-- 101660-->
    </WorkCompLocInfo>
  </xsl:template>
<!-- Case 31594 End -->
    
    <!-- 29653 Start -->
    <xsl:template name="CreateWCRateInfo">
		<xsl:variable name="Site" select="SITE"/>
		<xsl:variable name="ServerSeq" select="position()"/>
		<Location>
			<xsl:attribute name="id"><xsl:value-of select="concat('l',$ServerSeq)"/></xsl:attribute>
			<ItemIdInfo>
				<InsurerId><xsl:value-of select="$ServerSeq"/></InsurerId>
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_HostRowSeq</OtherIdTypeCd>
					<OtherId><xsl:value-of select="$Site"/></OtherId>
				</OtherIdentifier>
			</ItemIdInfo>
			<Addr>
				<AddrTypeCd>StreetAddress</AddrTypeCd>
				<Addr1><xsl:value-of select="ADDRESS2"/></Addr1>
				<Addr2><!--<xsl:value-of select="ADDRESS2"/>--></Addr2>
				<City><xsl:value-of select="substring(CITYST,1,28)"/></City>
				<StateProvCd><xsl:value-of select="substring(CITYST,29,2)"/></StateProvCd>
				<PostalCode><xsl:value-of select="ZIP"/></PostalCode>
				<Country>USA</Country>
				<County/>
			</Addr>
			<xsl:for-each select="/*/PMSP1200__RECORD[$Site=USE0LOC]">
				<xsl:call-template name="CreateWorkCompAdditionalInterest"></xsl:call-template>
			</xsl:for-each>
		</Location>
    </xsl:template>
    <!-- 29653 End   -->
     <!-- 101660 Begin -->
    <xsl:template name="GetPosition">
         <xsl:param name="Site"/>
		<xsl:for-each select="/*/PMSPWC04__RECORD[DROPIND != 'Y']">
		    <xsl:sort select="SITE"/>
		    <xsl:if test="SITE=$Site">
		        <xsl:value-of select="position()"/>     
			 </xsl:if>
		</xsl:for-each>
    </xsl:template>
    <!-- 101660 End -->
</xsl:stylesheet>