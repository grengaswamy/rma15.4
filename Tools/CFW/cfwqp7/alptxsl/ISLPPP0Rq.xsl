<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:date="xalan://java.util.GregorianCalendar">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
E-Service case 31594 - WC Amendments
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq (Mod) - 31594
-->

  <xsl:template name="CreateISLPPP0">
    <xsl:variable name="TableName">ISLPPP0</xsl:variable>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ISLPPP0</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ISLPPP0__RECORD>
        <RECID>PP0</RECID>
        <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICY0NUM>
          <xsl:value-of select="$POL"/>
        </POLICY0NUM>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTER0CO>
          <xsl:value-of select="$MCO"/>
        </MASTER0CO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
		<!-- Case 31594 start -->
   		<xsl:variable name="tmp" select="date:new()"/>		
		<xsl:variable name="month" select="substring(concat('0',date:get($tmp, 2) + 1),string-length(date:get($tmp, 2) + 1))" />
    	<xsl:variable name="day" select="substring(concat('0',date:get($tmp, 5)),string-length(date:get($tmp, 5)))" />
    	<xsl:variable name="year" select="date:get($tmp, 1)" />
  		<xsl:variable name="ActDate">
			<xsl:value-of select="$year"/>/<xsl:value-of select="$month"/>/<xsl:value-of select="$day"/>
		</xsl:variable>
        <!-- Case 31594 end -->
        <ACTDATE>
          <xsl:call-template name="ConvertISODateToPTDate">
          		<xsl:with-param name="Value" select="$ActDate"/> <!-- Case 31594 -->
          </xsl:call-template>
        </ACTDATE>
        <ACTIONSEQ>0000</ACTIONSEQ>
        <EFF0DATE>
		<!-- Case 31594 start -->
		<xsl:choose>
			<xsl:when test="$TYPEACT='EN'">
				<xsl:call-template name="ConvertISODateToPTDate">
            	<xsl:with-param name="Value" select="TransactionEffectiveDt"/>
            	</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="ConvertISODateToPTDate">
            	<xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
            	</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<!-- Case 31594 end -->
        </EFF0DATE>
     	<TYPE0ACT><xsl:value-of select="$TYPEACT"/></TYPE0ACT> <!-- Case 31594 -->
        <RECSTATUS>U</RECSTATUS>
      </ISLPPP0__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="WCModRqPT.xml" htmlbaseurl="" outputurl="" processortype="msxml4" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->