<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <!-- 55614 begin -->
    <xsl:variable name="KEY">
  	        <xsl:value-of select="/*/PMSP0200__RECORD/SYMBOL"/>
	   	  <xsl:value-of select="/*/PMSP0200__RECORD/POLICY0NUM"/>
	   	  <xsl:value-of select="/*/PMSP0200__RECORD/MODULE"/>
   </xsl:variable>        
   <!-- 55614 end -->
    <xsl:template name="ConvertPTDateToISODate">
        <xsl:param name="Value"/>
  
        <xsl:variable name="ModifiedDateValue">
            <xsl:choose>
                <xsl:when test="$Value=''">0010101</xsl:when>
                <xsl:otherwise>
                <xsl:choose>               
                   <xsl:when test="string-length($Value) ='7'"><xsl:value-of select="$Value"/></xsl:when>
                   <xsl:otherwise>
                    <xsl:value-of select="concat('0',$Value)"/>
                   </xsl:otherwise>
                 </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

			<!--  Format a seven digit POINT date (i.e. 1123101) to an ISO date (i.e. 2001-12-31) -->
			<!--  Year - convert 3 digit to 4 digit for ISO -->
			<xsl:choose>
				<!--FSIT #112813 Starts-->
				<xsl:when test="substring($Value,4,4)='0000'">
				</xsl:when>
				<!--FSIT #112813 Ends-->
				<xsl:when test="substring($Value,1,1)='1'">
					<xsl:value-of select="concat('20',substring($ModifiedDateValue,2,2),'/')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat('19',substring($ModifiedDateValue,2,2),'/')"/>
				</xsl:otherwise>
			</xsl:choose>
			<!--FSIT #112813 Starts-->
			<xsl:choose>
				<!--  Month -->
				<xsl:when test="substring($Value,4,4)='0000'">
				</xsl:when>
				<xsl:otherwise>
					<!--FSIT #112813 Ends-->
					<xsl:value-of select="concat(substring($ModifiedDateValue,4,2),'/')"/>
					<!--  Day -->
					<xsl:value-of select="substring($ModifiedDateValue,6,2)"/>
					<!--FSIT #112813 Starts-->
				</xsl:otherwise>
			</xsl:choose>
			<!--FSIT #112813 Ends-->
		</xsl:template>
    
<!-- This template is no longer used, but retained for compatiblity with previous XSL for CPP --> 
    <xsl:template name="FormatField">
        <xsl:param name="TableName"/>
        <xsl:param name="FieldName"/>
        <xsl:param name="Value"/>
        <xsl:param name="FieldType"/>
        <xsl:param name="FieldLength"/>
        <xsl:param name="Precision">0</xsl:param>
        <xsl:choose>
            <xsl:when test="$FieldType='N'">
                <xsl:variable name="ModifiedNumericValue">
                    <xsl:choose>
                        <xsl:when test="string($Value)=''">0</xsl:when>
                        <xsl:when test="$Precision > 0">
                            <xsl:variable name="NewValue" select="number(translate($Value, ' $,', ''))"/>
                            <xsl:if test="string($NewValue)='NaN'">
                                <xsl:message terminate="yes"><xsl:value-of select="$FieldName"/> should be numeric. The value is '<xsl:value-of select="$Value"/>'
The context is: <xsl:value-of select="name()"/>
                                </xsl:message>
                            </xsl:if>
                            <xsl:variable name="Decimal" select="substring(concat(substring-after($NewValue, '.'), '00000000000'), 1, $Precision)"/>
                            <xsl:variable name="Whole">
                                <xsl:choose>
                                    <xsl:when test="string-length(substring-before($NewValue, '.')) > 0">
                                        <xsl:value-of select="concat(substring('00000000000', 1, $FieldLength - $Precision - string-length(substring-before($NewValue, '.'))), substring-before($NewValue, '.'))"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:if test="$FieldLength - $Precision - string-length($NewValue) &lt; 0">
                                            <xsl:message terminate="yes"><xsl:value-of select="$FieldName"/> has a format problem. The value is '<xsl:value-of select="$Value"/>'
The context is: <xsl:value-of select="name()"/>
                                            </xsl:message>
                                        </xsl:if>
                                        <xsl:value-of select="concat(substring('00000000000', 1, $FieldLength - $Precision - string-length($NewValue)), $NewValue)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:value-of select="concat($Whole, $Decimal)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="string(number(translate($Value, ' $,', '')))='NaN'">
                                <xsl:message terminate="yes"><xsl:value-of select="$FieldName"/> should be numeric. The value is '<xsl:value-of select="$Value"/>'
The context is: <xsl:value-of select="name()"/>
                                </xsl:message>
                            </xsl:if>
                            <xsl:value-of select="number(translate($Value, ' $,', ''))"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:if test="string-length($ModifiedNumericValue) > $FieldLength">
                    <xsl:message terminate="yes"><xsl:value-of select="$FieldName"/> has a length of <xsl:value-of select="$FieldLength"/>. The value '<xsl:value-of select="$Value"/>' is too long.</xsl:message>
                </xsl:if>
                <xsl:value-of select="concat(substring('00000000000', 1, $FieldLength - string-length($ModifiedNumericValue)), $ModifiedNumericValue)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="ModifiedStringValue">
                    <xsl:choose>
                        <xsl:when test="$Value=''"> </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$Value"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:value-of select="substring(concat($ModifiedStringValue, '                                                                                                    '), 1, $FieldLength)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="ConvertNumericStateCdToAlphaStateCd">
        <xsl:param name="Value"/>
		<xsl:choose>
			<xsl:when test="$Value='01'">AL</xsl:when>
			<xsl:when test="$Value='02'">AZ</xsl:when>
			<xsl:when test="$Value='03'">AR</xsl:when>
			<xsl:when test="$Value='04'">CA</xsl:when>
			<xsl:when test="$Value='05'">CO</xsl:when>
			<xsl:when test="$Value='06'">CT</xsl:when>
			<xsl:when test="$Value='07'">DE</xsl:when>
			<xsl:when test="$Value='08'">DC</xsl:when>
			<xsl:when test="$Value='09'">FL</xsl:when>
			<xsl:when test="$Value='10'">GA</xsl:when>
			<xsl:when test="$Value='11'">ID</xsl:when>
			<xsl:when test="$Value='12'">IL</xsl:when>
			<xsl:when test="$Value='13'">IN</xsl:when>
			<xsl:when test="$Value='14'">IA</xsl:when>
			<xsl:when test="$Value='15'">KS</xsl:when>
			<xsl:when test="$Value='16'">KY</xsl:when>
			<xsl:when test="$Value='17'">LA</xsl:when>
			<xsl:when test="$Value='18'">ME</xsl:when>
			<xsl:when test="$Value='19'">MD</xsl:when>
			<xsl:when test="$Value='20'">MA</xsl:when>
			<xsl:when test="$Value='21'">MI</xsl:when>
			<xsl:when test="$Value='22'">MN</xsl:when>
			<xsl:when test="$Value='23'">MS</xsl:when>
			<xsl:when test="$Value='24'">MO</xsl:when>
			<xsl:when test="$Value='25'">MT</xsl:when>
			<xsl:when test="$Value='26'">NE</xsl:when>
			<xsl:when test="$Value='27'">NV</xsl:when>
			<xsl:when test="$Value='28'">NH</xsl:when>
			<xsl:when test="$Value='29'">NJ</xsl:when>
			<xsl:when test="$Value='30'">NM</xsl:when>
			<xsl:when test="$Value='31'">NY</xsl:when>
			<xsl:when test="$Value='32'">NC</xsl:when>
			<xsl:when test="$Value='33'">ND</xsl:when>
			<xsl:when test="$Value='34'">OH</xsl:when>
			<xsl:when test="$Value='35'">OK</xsl:when>
			<xsl:when test="$Value='36'">OR</xsl:when>
			<xsl:when test="$Value='37'">PA</xsl:when>
			<xsl:when test="$Value='38'">RI</xsl:when>
			<xsl:when test="$Value='39'">SC</xsl:when>
			<xsl:when test="$Value='40'">SD</xsl:when>
			<xsl:when test="$Value='41'">TN</xsl:when>
			<xsl:when test="$Value='42'">TX</xsl:when>
			<xsl:when test="$Value='43'">UT</xsl:when>
			<xsl:when test="$Value='44'">VT</xsl:when>
			<xsl:when test="$Value='45'">VA</xsl:when>
			<xsl:when test="$Value='46'">WA</xsl:when>
			<xsl:when test="$Value='47'">WV</xsl:when>
			<xsl:when test="$Value='48'">WI</xsl:when>
			<xsl:when test="$Value='49'">WY</xsl:when>
			<xsl:when test="$Value='50'">HI</xsl:when>
			<xsl:when test="$Value='51'">AK</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Value"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
            
	<xsl:template name="BuildModFactor">
	    <xsl:param name="Code"/>
	    <xsl:param name="FactorField"/>
		<CreditOrSurcharge>
		    <CreditSurchargeDt/>
		    <CreditSurchargeCd><xsl:value-of select="$Code"/></CreditSurchargeCd>
		    <NumericValue>
		    	<FormatModFactor><xsl:value-of select="$FactorField"/></FormatModFactor>
		    </NumericValue>
		</CreditOrSurcharge>
	</xsl:template>
    
    <xsl:template name="TranslateCoverageCode">
        <xsl:param name="InsLine"/>
        <xsl:param name="CovCode"/>
        <xsl:choose>
            <xsl:when test="$InsLine='CF'">
				<!-- Issue 59878 - Start -->
				<!--xsl:choose>
					<xsl:when test="$CovCode='BLDG'">BLDG</xsl:when>
					<xsl:when test="$CovCode='BR'">BRWOR</xsl:when>
					<xsl:when test="$CovCode='BUSINC'">BUSIN</xsl:when>
					<xsl:when test="$CovCode='DEBRIS'">DEBRL</xsl:when>
					<xsl:when test="$CovCode='EE'">EE</xsl:when>
					<xsl:when test="$CovCode='FIRE-B'">OPTBD</xsl:when>
					<xsl:when test="$CovCode='FIRE-P'">OPTPP</xsl:when>
					<xsl:when test="$CovCode='IMPBET'">LIIAB</xsl:when>
					<xsl:when test="$CovCode='LOSS'">com.csc_LOSS</xsl:when>
					<xsl:when test="$CovCode='MINE'">MIN</xsl:when>
					<xsl:when test="$CovCode='ORD-A'">com.csc_ORDA</xsl:when>
					<xsl:when test="$CovCode='ORD-B'">com.csc_ORDB</xsl:when>
					<xsl:when test="$CovCode='ORD-C'">com.csc_ORDC</xsl:when>
					<xsl:when test="$CovCode='PEAK'">PS</xsl:when>
					<xsl:when test="$CovCode='PERS'">BPP</xsl:when>
					<xsl:when test="$CovCode='POLL'">CNPOL</xsl:when>
					<xsl:when test="$CovCode='PPO'">POTOP</xsl:when>
					<xsl:when test="$CovCode='SPOIL'">SPOIL</xsl:when>
					<xsl:when test="$CovCode='STOCK'">STOCK</xsl:when>
					<xsl:when test="$CovCode='VACANT'">VACPR</xsl:when>
					<xsl:when test="$CovCode='SPEC'">SPC</xsl:when>
					<xsl:when test="$CovCode='NYFEE'">com.csc_NYFEE</xsl:when>
					<xsl:otherwise>xxx</xsl:otherwise>
				</xsl:choose-->
				<xsl:value-of select="$CovCode"/>	
				<!-- Issue 59878 - End -->
            </xsl:when>
            <xsl:when test="$InsLine='GL'">
                <!-- Issue 59878 - Start -->
				<!--xsl:choose>
                    <xsl:when test="$CovCode='CCC'">CCCLL</xsl:when>
                    <xsl:when test="$CovCode='CONDO'">DOCON</xsl:when>
                    <xsl:when test="$CovCode='EBL'">com.csc_EBL</xsl:when>
                    <xsl:when test="$CovCode='PROF'">PROF</xsl:when>
                    <xsl:when test="$CovCode='VOLPD'">VOLPD</xsl:when>
                    <xsl:when test="$CovCode='PREMOP'">PREM</xsl:when>
                    <xsl:when test="$CovCode='POLL'">POLUT</xsl:when>
                    <xsl:when test="$CovCode='OCP'">OCP</xsl:when>
                    <xsl:when test="$CovCode='LIQ'">LIQUR</xsl:when>
                    <xsl:when test="$CovCode='RRPROT'">RRPRL</xsl:when>
                    <xsl:when test="$CovCode='ADDINS'">ADDIN</xsl:when>
                    <xsl:when test="$CovCode='ELEVTR'">com.csc_EEI</xsl:when>
                    <xsl:when test="$CovCode='LSDWKR'">com.csc_LW</xsl:when>
                    <xsl:when test="$CovCode='PRODCO'">PCO</xsl:when>
                    <xsl:when test="$CovCode='UNDTNK'">com.csc_UGTANK</xsl:when>
					<xsl:otherwise>xxx</xsl:otherwise>
                </xsl:choose-->
				<xsl:value-of select="$CovCode"/>	
				<!-- Issue 59878 - End -->
            </xsl:when>
            <xsl:when test="$InsLine='CR'">
				<!-- Issue 59878 - Start -->
                <!--xsl:choose>
                    <xsl:when test="$CovCode='ARBITR'">com.csc_ARB</xsl:when>
                    <xsl:when test="$CovCode='CLIENT'">com.csc_CLPROP</xsl:when>
                    <xsl:when test="$CovCode='EMDISC'">com.csc_EMPTHFT</xsl:when>
                    <xsl:when test="$CovCode='EMPDIS'">EMPDH</xsl:when>
                    <xsl:when test="$CovCode='EMPTPE'">FORMP</xsl:when>
                    <xsl:when test="$CovCode='EMPTPL'">FORMO</xsl:when>
                    <xsl:when test="$CovCode='EXTORT'">com.csc_EXTR</xsl:when>
                    <xsl:when test="$CovCode='FORGRY'">CCF</xsl:when>
                    <xsl:when test="$CovCode='FRAUD'">com.csc_FRAUD</xsl:when>
                    <xsl:when test="$CovCode='FUNDS'">com.csc_FTF</xsl:when>
                    <xsl:when test="$CovCode='GUEST1'">FORMK</xsl:when>
                    <xsl:when test="$CovCode='GUEST2'">FORML</xsl:when>
                    <xsl:when test="$CovCode='INSOTH'">com.csc_ITOP</xsl:when>
                    <xsl:when test="$CovCode='INSRBO'">com.csc_IRBOP</xsl:when>
                    <xsl:when test="$CovCode='INSRMS'">FORMQ</xsl:when>
                    <xsl:when test="$CovCode='INSRSB'">com.csc_IRSBOP</xsl:when>
                    <xsl:when test="$CovCode='INSTMS'">com.csc_ITMS</xsl:when>
                    <xsl:when test="$CovCode='LESSE1'">com.csc_SDBLTDDS</xsl:when>
                    <xsl:when test="$CovCode='LESSE2'">com.csc_SDBLRBOP</xsl:when>
                    <xsl:when test="$CovCode='OUTSDE'">OS</xsl:when>
                    <xsl:when test="$CovCode='PAPER'">com.csc_MOCM</xsl:when>
                    <xsl:when test="$CovCode='SAFE1A'">com.csc_SDCPL</xsl:when>
                    <xsl:when test="$CovCode='SAFE1B'">com.csc_SDRBCPPD</xsl:when>
                    <xsl:when test="$CovCode='SEC1A'">com.csc_SECDFI</xsl:when>
                    <xsl:when test="$CovCode='SEC1B'">com.csc_SECDPO</xsl:when>
					<xsl:otherwise>xxx</xsl:otherwise>
                </xsl:choose-->
				<xsl:value-of select="$CovCode"/>	
				<!-- Issue 59878 - End -->
            </xsl:when>
            <xsl:when test="$InsLine='IMC'">
				<!-- Issue 59878 - Start -->
                <!--xsl:choose>
                    <xsl:when test="$CovCode='ACCTS'">ACCTS</xsl:when>
                    <xsl:when test="$CovCode='BAILEE'">BAILE</xsl:when>
                    <xsl:when test="$CovCode='BOAT'">BOAT</xsl:when>
                    <xsl:when test="$CovCode='BR'">BRWOR</xsl:when>
                    <xsl:when test="$CovCode='COMMLA'">FINEA</xsl:when>
                    <xsl:when test="$CovCode='COMMLC'">CAMRA</xsl:when>
                    <xsl:when test="$CovCode='COMMLO'">com.csc_ORGAN</xsl:when>
                    <xsl:when test="$CovCode='COMMLM'">MUSFL</xsl:when>
                    <xsl:when test="$CovCode='EDP'">EDPEQ</xsl:when>
                    <xsl:when test="$CovCode='EQPDO'">com.csc_EQDOP</xsl:when>
                    <xsl:when test="$CovCode='EQPDP'">EQDFL</xsl:when>
                    <xsl:when test="$CovCode='EQPDS'">com.csc_EQDS</xsl:when>
                    <xsl:when test="$CovCode='EQUIP'">EQUFL</xsl:when>
                    <xsl:when test="$CovCode='PHYSUR'">PHYS</xsl:when>
                    <xsl:when test="$CovCode='RDOTV'">RDOTV</xsl:when>
                    <xsl:when test="$CovCode='SIGN'">SIGN</xsl:when>
                    <xsl:when test="$CovCode='THEATR'">THEATR</xsl:when>
                    <xsl:when test="$CovCode='VALPAP'">PAPER</xsl:when>
					<xsl:otherwise>xxx</xsl:otherwise>
                </xsl:choose-->
				<xsl:value-of select="$CovCode"/>	
				<!-- Issue 59878 - End -->
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>

	<xsl:template name="GenerateMinPrem">
	    <xsl:param name="InsLine"/>
	    <xsl:param name="Location">0</xsl:param>
	    <xsl:param name="SubLocation">0</xsl:param>
	    <xsl:param name="CovCode"/>
<!-- See if there is a Minimum Premium for this coverage -->
        <xsl:variable name="MinPremCovCode">
            <xsl:call-template name="GetMinPremCoverageCode">
                <xsl:with-param name="InsLine" select="$InsLine"/>
                <xsl:with-param name="CovCode" select="$CovCode"/>
            </xsl:call-template>
        </xsl:variable>
<!-- If there is no Minimum Premium for this coverage, skip this aggregate -->
		<xsl:if test="$MinPremCovCode != 'xxx'">
		    <!--commented and updated for 59878 <xsl:for-each select="/*/ASBYCPL1__RECORD[normalize-space(BYAGTX)=$InsLine and -->
			<xsl:for-each select="/*[1]/ASBYCPL1__RECORD[normalize-space(BYAGTX)=$InsLine and 
                                  number(BYBRNB)=$Location and number(BYEGNB)=$SubLocation and 
                                  normalize-space(BYAOTX)=$MinPremCovCode]">
                <CreditOrSurcharge>
                    <CreditSurchargeDt/>
                    <CreditSurchargeCd>APMP</CreditSurchargeCd>
                    <NumericValue>
                        <FormatCurrencyAmt>
                            <Amt><xsl:value-of select="BYA3VA"/></Amt>
                        </FormatCurrencyAmt>
                    </NumericValue>
                </CreditOrSurcharge>
            </xsl:for-each>
        </xsl:if>
	</xsl:template>
	    
	<xsl:template name="GetMinPremCoverageCode">
        <xsl:param name="InsLine"/>
        <xsl:param name="CovCode"/>
	    <xsl:choose>
            <xsl:when test="$InsLine='CF'">
                <xsl:choose>
                    <xsl:when test="$CovCode='INSLIN'">MINPRM</xsl:when>
					<!-- Issue 59878 - Start -->
					<!--xsl:otherwise>xxx</xsl:otherwise-->
					<xsl:otherwise><xsl:value-of select="$CovCode"/></xsl:otherwise>
					<!-- Issue 59878 - End -->
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='GL'">
                <xsl:choose>
                    <xsl:when test="$CovCode='LIQ'">MP/332</xsl:when>
                    <xsl:when test="$CovCode='PREMOP'">MP/334</xsl:when>
                    <xsl:when test="$CovCode='OCP'">MP/335</xsl:when>
                    <xsl:when test="$CovCode='PRODCO'">MP/336</xsl:when>
                    <xsl:when test="$CovCode='INSLIN'">INSMP</xsl:when>
					<!-- Issue 59878 - Start -->
					<!--xsl:otherwise>xxx</xsl:otherwise-->
					<xsl:otherwise><xsl:value-of select="$CovCode"/></xsl:otherwise>
					<!-- Issue 59878 - End -->
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='CR'">
                <xsl:choose>
                    <xsl:when test="$CovCode='OUTSDE'">OUTMIN</xsl:when>
                    <xsl:when test="$CovCode='EMDISC'">EMDMIN</xsl:when>                    
                    <xsl:when test="$CovCode='INSOTH'">OTHMIN</xsl:when>
                    <xsl:when test="$CovCode='INSRBO'">RBOMIN</xsl:when>
                    <xsl:when test="$CovCode='INSRMS'">RMSMIN</xsl:when>
                    <xsl:when test="$CovCode='INSRSB'">RSBMIN</xsl:when>
                    <xsl:when test="$CovCode='INSTMS'">TMSMIN</xsl:when>
                    <xsl:when test="$CovCode='INSLIN'">INSMIN</xsl:when>
					<!--59878 <xsl:otherwise>xxx</xsl:otherwise>-->
					<xsl:otherwise>
						<xsl:value-of select="$CovCode"/> 
					</xsl:otherwise>
					<!--59878 ends-->
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='IMC'">
                <xsl:choose>
                    <xsl:when test="$CovCode='INSLIN'">MINPRM</xsl:when>
					<!-- Issue 59878 - Start -->
					<!--xsl:otherwise>xxx</xsl:otherwise-->
					<xsl:otherwise><xsl:value-of select="$CovCode"/></xsl:otherwise>
					<!-- Issue 59878 - End -->
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>

	<xsl:template name="MinPremLookup">
        <xsl:param name="InsLine"/>
        <xsl:variable name="CovCode"><xsl:value-of select="BYAOTX"/></xsl:variable>
	    <xsl:choose>
            <xsl:when test="$InsLine='CF'">
                <xsl:choose>
                    <xsl:when test="$CovCode='MINPRM'">Y</xsl:when>
					<xsl:otherwise>N</xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='GL'">
                <xsl:choose>
                    <xsl:when test="$CovCode='MP/332'">Y</xsl:when>
                    <xsl:when test="$CovCode='MP/334'">Y</xsl:when>
                    <xsl:when test="$CovCode='MP/335'">Y</xsl:when>
                    <xsl:when test="$CovCode='MP/336'">Y</xsl:when>
                    <xsl:when test="$CovCode='INSMP'">Y</xsl:when>
					<xsl:otherwise>N</xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='CR'">
                <xsl:choose>
                    <xsl:when test="$CovCode='OUTMIN'">Y</xsl:when>
                    <xsl:when test="$CovCode='EMDMIN'">Y</xsl:when>                    
                    <xsl:when test="$CovCode='OTHMIN'">Y</xsl:when>
                    <xsl:when test="$CovCode='RBOMIN'">Y</xsl:when>
                    <xsl:when test="$CovCode='RMSMIN'">Y</xsl:when>
                    <xsl:when test="$CovCode='RSBMIN'">Y</xsl:when>
                    <xsl:when test="$CovCode='TMSMIN'">Y</xsl:when>
                    <xsl:when test="$CovCode='INSMIN'">Y</xsl:when>
					<xsl:otherwise>N</xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$InsLine='IMC'">
                <xsl:choose>
                    <xsl:when test="$CovCode='MINPRM'">Y</xsl:when>
					<xsl:otherwise>N</xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    
   <!-- 29653 Start -->
    <xsl:template name="CreateWCRate">
         <xsl:param name="Bureau"/>
         <xsl:param name="Spec50"/>
      
         <xsl:choose>
             <xsl:when test="substring($Bureau,1,1)!='G'">
			      <xsl:value-of select="concat(substring($Bureau,5,3),'.',substring($Bureau,8,3),'00')"/>
             </xsl:when>
             <xsl:otherwise>
                 <xsl:choose>
		            <xsl:when test="substring($Bureau,33,1)='2'">
		               <xsl:variable name="WCRate1" select="concat(substring($Bureau,5,3),'.',substring($Bureau,8,3),'00')"/>
		 	           <xsl:variable name="WCRate" select="($WCRate1 div 100)"/>
		 		       <xsl:choose>
		                   <xsl:when test="substring($Spec50,4,1)='A'">
		                       <xsl:value-of select="concat(substring($WCRate,1,3), substring($WCRate,4,3))"/>
		                   </xsl:when>
		                   <xsl:when test="substring($Spec50,4,1)='C'">
		     	               <xsl:value-of select="concat(substring($WCRate,1,3), substring($WCRate,4,3)-1)"/>
		                   </xsl:when>
		                   <xsl:otherwise>
			                   <xsl:value-of select="concat(substring($WCRate,1,3), substring($WCRate,4,3))"/>	                
		                   </xsl:otherwise>
                       </xsl:choose>
		 	        </xsl:when>
	                <xsl:otherwise>
		 	           <xsl:variable name="WCRate" select="concat(substring($Bureau,5,3),'.',substring($Bureau,8,3),'00')"/> 
		 		       <xsl:choose>
		                   <xsl:when test="substring($Spec50,4,1)='A'">
		                      <xsl:value-of select="concat(substring($WCRate,1,3),substring($WCRate,4,3))"/>
		                   </xsl:when>
		                   <xsl:when test="substring($Spec50,4,1)='C'">
		     	              <xsl:value-of select="concat(substring($WCRate,1,3),substring($WCRate,4,3)-1)"/>
		                   </xsl:when>
		                   <xsl:otherwise>
					           <xsl:value-of select="concat(substring($WCRate,1,3),substring($WCRate,4,3))"/>
                           </xsl:otherwise>
                       </xsl:choose>            
		            </xsl:otherwise>
                 </xsl:choose>
             </xsl:otherwise>
         </xsl:choose>
    </xsl:template>         
    <!-- 29653 End  -->     
       				            
</xsl:stylesheet>