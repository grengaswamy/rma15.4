<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="ACORD/InsuranceSvcRq/*" mode="PMSP0200">
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>PMSP0200</RECORD__NAME>
         </RECORD__NAME__ROW>
         <PMSP0200__RECORD>
            <TRANS0STAT>P</TRANS0STAT>
            <ID02>02</ID02>
            <SYMBOL><xsl:value-of select="$SYM"/></SYMBOL>
            <POLICY0NUM><xsl:value-of select="$POL"/></POLICY0NUM>
            <MODULE><xsl:value-of select="$MOD"/></MODULE>
            <MASTER0CO><xsl:value-of select="$MCO"/></MASTER0CO>
            <LOCATION><xsl:value-of select="$LOC"/></LOCATION>
            <EFF0DT>
               <xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="*/ContractTerm/EffectiveDt"/>
               </xsl:call-template>
            </EFF0DT>
            <EXP0DT>
               <xsl:call-template name="ConvertISODateToPTDate">
                  <xsl:with-param name="Value" select="*/ContractTerm/ExpirationDt"/>
               </xsl:call-template>
            </EXP0DT>
            <INSTAL0TRM>
  	         <xsl:call-template name="FormatData">
	   	     	<xsl:with-param name="FieldName">INSTAL0TRM</xsl:with-param>      
		      <xsl:with-param name="FieldLength">3</xsl:with-param>
		      <!-- 50444 Start -->
		      <xsl:with-param name="Value">
		       <xsl:choose>
				 <xsl:when test="*/com.csc_PolicyTermMonths='0'">999
				 </xsl:when>
				 <xsl:otherwise><xsl:value-of select="*/com.csc_PolicyTermMonths"/></xsl:otherwise>
				 </xsl:choose>
		      </xsl:with-param>
		      <!-- 50444 End   -->
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>                           
            </INSTAL0TRM>
            <NMB0INSTAL>
            <!-- 29563 Start -->
            	<xsl:choose>
					 <xsl:when test="$LOB = 'WC' or $LOB = 'WCV'">
					</xsl:when>
					<xsl:otherwise>
						01
					</xsl:otherwise>
				</xsl:choose>
			<!-- 29563 End -->
			</NMB0INSTAL>
            <RISK0STATE>
		   <xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
   	  		<!-- 29563 Start -->
			<xsl:with-param name="Value">
				<!-- Issue 98622 Starts-->
				<!--<xsl:choose>
					 <xsl:when test="$LOB = 'WC' or $LOB = 'WCV'">
						<xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="*/ControllingStateProvCd"/>
					</xsl:otherwise>
				</xsl:choose>-->
				<xsl:value-of select="*/ControllingStateProvCd"/>
				<!-- Issue 98622 Ends-->
			</xsl:with-param>
			 <!-- 29563 End -->
   		   </xsl:call-template>            
            </RISK0STATE>
            <COMPANY0NO><xsl:value-of select="$PCO"/></COMPANY0NO>
            <BRANCH>00</BRANCH>
            <PROFIT0CTR>
            <!-- 29563 Start -->
            	<xsl:choose>
				   <xsl:when test="$LOB = 'WC' or $LOB = 'WCV'">
					</xsl:when>
					<xsl:otherwise>
						001
					</xsl:otherwise>
				</xsl:choose>
			<!-- 29563 End -->
            </PROFIT0CTR>
            <FILLR1><xsl:value-of select="substring(Producer/ItemIdInfo/AgencyId,1,3)"/></FILLR1>
            <RPT0AGT0NR><xsl:value-of select="substring(Producer/ItemIdInfo/AgencyId,4,1)"/></RPT0AGT0NR>
            <FILLR2><xsl:value-of select="substring(Producer/ItemIdInfo/AgencyId,5,3)"/></FILLR2>
            <ENTER0DATE>
               <!-- 29653 Start      -->  
               <!-- <xsl:call-template name="ConvertISODateToPTDate"> -->
               <!-- Case 34770 Starts : Changed to TransactionEffectiveDt-->
               <!--<xsl:with-param name="Value" select="TransactionRequestDt"/>-->
	         <!--      <xsl:with-param name="Value" select="TransactionEffectiveDt"/> -->
	         <!-- Case 34770 ends-->	
               <!--   <xsl:with-param name="Value" select="TransactionEffectiveDt"/>-->
               <!-- </xsl:call-template>   --> 
		
		   <xsl:choose>
		   <xsl:when test="$TYPEACT='EN'">
		  	<xsl:call-template name="ConvertISODateToPTDate">
            			<xsl:with-param name="Value" select="TransactionEffectiveDt"/>
          		</xsl:call-template>
		   </xsl:when>
		   <xsl:otherwise>
		  	<xsl:call-template name="ConvertISODateToPTDate">
            			<xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
          		</xsl:call-template>
		</xsl:otherwise>
		</xsl:choose> 
	   <!-- 29653 End -->
            </ENTER0DATE>
            <TOT0AG0PRM><xsl:value-of select="*/CurrentTermAmt/Amt"/></TOT0AG0PRM>
            <LINE0BUS>
				<xsl:choose>
				<xsl:when test="$LOB='CA'">CPP
				</xsl:when>
				<xsl:otherwise><xsl:value-of select="$LOB"/></xsl:otherwise>
				</xsl:choose>
			</LINE0BUS>
             <!-- 50764 start -->
			 <!--  <ISSUE0CODE>Q</ISSUE0CODE> -->
			<xsl:choose>
				<xsl:when test="$TYPEACT='RB'">
					 <ISSUE0CODE>R</ISSUE0CODE>
				</xsl:when>
				<xsl:otherwise>
					 <!-- Issue 59878 - Start -->
					 <!--ISSUE0CODE>Q</ISSUE0CODE-->
					 <xsl:variable name="MessageType" select="name(.)"/>
					 <xsl:choose>				
					 	<xsl:when test="substring-after($MessageType, 'Policy') = 'AddRq'"> <!-- Issue 64220 -->
            		 		<ISSUE0CODE>N</ISSUE0CODE>										
			 		 	</xsl:when>														
					 	<xsl:otherwise>
             		 		<ISSUE0CODE>Q</ISSUE0CODE>
					 	</xsl:otherwise>
					 </xsl:choose>
					 <!-- Issue 59878 - End -->
				</xsl:otherwise>
			</xsl:choose>
			<!-- 50764 end -->
               <xsl:choose>
		       <!-- 39111 -->
				<xsl:when test="$LOB = 'FP'">
					<COMP0LINE>1</COMP0LINE>
				</xsl:when>
		       <!-- 39111 -->
                  <xsl:when test="$LOB = 'HP'">
             <COMP0LINE>2</COMP0LINE>
                  </xsl:when>
                  <xsl:when test="$LOB = 'CPP' or $LOB = 'BOP'">
             <COMP0LINE>3</COMP0LINE>
                  </xsl:when>
                 <xsl:when test="$LOB = 'CA'">
             <COMP0LINE>7</COMP0LINE>
                  </xsl:when>
                 <!-- 29563 Start -->
                 <!--<xsl:when test="$LOB = 'WCV'">-->
		 <!-- Issue found during DST testing (need to include WCA lob) -->		  	
	 	 <!--<xsl:when test="$LOB = 'WC' or $LOB = 'WCV'"> -->
 			<xsl:when test="$LOB = 'WC' or $LOB = 'WCV' or $LOB = 'WCA'">
             <COMP0LINE>5</COMP0LINE>
                  </xsl:when>
                 <!-- 29563 End -->
                  <xsl:otherwise>
            <COMP0LINE>6</COMP0LINE>
                  </xsl:otherwise>
               </xsl:choose>                       
			<!-- Issue 59878 C.0 changes- Start -->
	        <PAY0CODE><xsl:value-of select="substring(*/PaymentOption/PaymentPlanCd,1,1)" /></PAY0CODE>
			<MODE0CODE><xsl:value-of select="substring(*/PaymentOption/PaymentPlanCd,2,1)" /></MODE0CODE>
			<!--<PAY0CODE>
				<xsl:choose>
					<xsl:when test="$LOB='CPP'">
						<xsl:value-of select="*/PaymentOption/com.csc_BillingPlanCd" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="substring(*/PaymentOption/PaymentPlanCd,1,1)" />
					</xsl:otherwise>
				</xsl:choose>
			</PAY0CODE>
			<MODE0CODE>
				<xsl:choose>
					<xsl:when test="$LOB='CPP'">
						<xsl:value-of select="*/PaymentOption/PaymentPlanCd" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="substring(*/PaymentOption/PaymentPlanCd,2,1)" />
					</xsl:otherwise>
				</xsl:choose>
			</MODE0CODE>-->
			<!-- Issue 59878 - End -->
               <xsl:choose> 
                  <!--<xsl:when test="$LOB='CPP' or $LOB='BOP'">-->	<!--34771-->
				  <xsl:when test="$LOB='CPP'">						<!--34771-->				
            		<AUDIT0CODE><xsl:value-of select="CommlPolicy/CommlPolicySupplement/AuditFrequencyCd" /></AUDIT0CODE>
           			<KIND0CODE><xsl:value-of select="CommlPolicy/PaymentOption/com.csc_BillingPlanCd" /></KIND0CODE>
                  </xsl:when>
				  <!--34771 start-->
				  <xsl:when test="$LOB='BOP'">									
            		<AUDIT0CODE><xsl:value-of select="CommlPolicy/CommlPolicySupplement/AuditFrequencyCd" /></AUDIT0CODE>
           			<KIND0CODE>D</KIND0CODE>
                  </xsl:when>
				  <!--34771 end -->
		  <!-- 97397 start -->
		  <xsl:when test="$LOB='WC' or $LOB='WCV'">									
            		<AUDIT0CODE><xsl:value-of select="CommlPolicy/CommlPolicySupplement/AuditFrequencyCd" /></AUDIT0CODE>
           		<KIND0CODE>D</KIND0CODE>
                  </xsl:when>
		  <!-- 97397 end -->
                  <xsl:otherwise>
            		<AUDIT0CODE>N</AUDIT0CODE>
            		<KIND0CODE>D</KIND0CODE>
                  </xsl:otherwise>
               </xsl:choose>           
            <SORT0NAME>
               <xsl:choose>
                  <xsl:when test="$LOB='CPP' or $LOB='BOP'">
                     <xsl:value-of select="substring(InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,4)"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:value-of select="substring(InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname,1,4)"/>
                  </xsl:otherwise>
               </xsl:choose>
            </SORT0NAME>
            <PROD0CODE>00</PROD0CODE>
            <RENEWAL0CD>0</RENEWAL0CD>
			<!--59878 start-->
			<!-- AFI - Issue 96466 start -->
			<!--
			<RSN0AM01ST></RSN0AM01ST>
			<RSN0AM02ND></RSN0AM02ND>
			<RSN0AM03RD></RSN0AM03RD>
			-->			
            <xsl:choose>
             <xsl:when test="$LOB='BOP' or $LOB='CPP' or $LOB='WC' or $LOB = 'WCV' or $LOB = 'WCA' or $LOB='CA'">	
				 <!--<xsl:if test="string-length(normalize-space(CommlPolicy/com.csc_ReasonCd)) &gt; 0">-->		<!--103409-->
					 <xsl:if test="string-length(normalize-space(CommlPolicy/com.csc_ActionInfo/com.csc_ReasonCd)) &gt; 0">	<!--103409-->
					<RSN0AM01ST>
						<!--<xsl:value-of select="substring(CommlPolicy/com.csc_ReasonCd,1,1)"/>-->	<!--103409-->
						<xsl:value-of select="substring(CommlPolicy/com.csc_ActionInfo/com.csc_ReasonCd,1,1)"/>	<!--103409-->
					</RSN0AM01ST>
					<RSN0AM02ND>
						<!--<xsl:value-of select="substring(CommlPolicy/com.csc_ReasonCd,2,1)"/>--><!--103409-->
						<xsl:value-of select="substring(CommlPolicy/com.csc_ActionInfo/com.csc_ReasonCd,2,1)"/><!--103409-->
					</RSN0AM02ND>
					<RSN0AM03RD>
						<!--<xsl:value-of select="substring(CommlPolicy/com.csc_ReasonCd,3,1)"/>--><!--103409-->
						<xsl:value-of select="substring(CommlPolicy/com.csc_ActionInfo/com.csc_ReasonCd,3,1)"/><!--103409-->
					</RSN0AM03RD>
				</xsl:if>
             </xsl:when>
             <xsl:otherwise>
				<xsl:if test="string-length(normalize-space(PersPolicy/com.csc_ReasonCd)) &gt; 0">
					<RSN0AM01ST>
						<xsl:value-of select="substring(PersPolicy/com.csc_ReasonCd,1,1)"/>
					</RSN0AM01ST>
					<RSN0AM02ND>
						<xsl:value-of select="substring(PersPolicy/com.csc_ReasonCd,2,1)"/>
					</RSN0AM02ND>
					<RSN0AM03RD>
						<xsl:value-of select="substring(PersPolicy/com.csc_ReasonCd,3,1)"/>
					</RSN0AM03RD>
				</xsl:if>	
             </xsl:otherwise>
            </xsl:choose>
            <!-- AFI - Issue 96466 end -->
				
			<!--RENEW0PAY><xsl:value-of select="substring(*/PaymentOption/PaymentPlanCd,1,1)" /></RENEW0PAY>
            <RENEW0MODE><xsl:value-of select="substring(*/PaymentOption/PaymentPlanCd,2,1)" /></RENEW0MODE-->
			<RENEW0PAY>
					<xsl:choose>
					<!--Issue 59878 CPP C.O changes starts
					<xsl:when test="$LOB='CPP'">
						<xsl:value-of select="*/PaymentOption/com.csc_BillingPlanCd" />
					</xsl:when>-->
					<xsl:when test="$LOB='CPP'">D</xsl:when>
					<!--Issue 59878 CPP C.O changes end-->
					<xsl:otherwise>
						<xsl:value-of select="substring(*/PaymentOption/PaymentPlanCd,1,1)" />
					</xsl:otherwise>
				</xsl:choose>
			</RENEW0PAY>
            <RENEW0MODE>
				<xsl:choose>
					<!--Issue 59878 CPP C.O changes starts
					<xsl:when test="$LOB='CPP'">
						<xsl:value-of select="*/PaymentOption/PaymentPlanCd" />
					</xsl:when>-->
					<xsl:when test="$LOB='CPP'">0</xsl:when>
					<!--Issue 59878 CPP C.O changes ends-->
					<xsl:otherwise>
						<xsl:value-of select="substring(*/PaymentOption/PaymentPlanCd,2,1)" />
					</xsl:otherwise>
				</xsl:choose>
			</RENEW0MODE>
            <!--59878 End-->
            <ORI0INCEPT>
                <xsl:choose>
                  <xsl:when test="string-length(*/ContractTerm/EffectiveDt) &gt; 0">           
               <xsl:value-of select="number(substring(*/ContractTerm/EffectiveDt,1,1)) - 1"/>
               <xsl:value-of select="substring(*/ContractTerm/EffectiveDt,3,2)"/>
               <xsl:value-of select="substring(*/ContractTerm/EffectiveDt,6,2)"/>
                  </xsl:when>
                  <xsl:otherwise>10306</xsl:otherwise>
               </xsl:choose>               
            </ORI0INCEPT>
            <CUST0NO><xsl:value-of select="*/AccountNumberId"/></CUST0NO>
            <ZIP0POST><xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode"/></ZIP0POST>
            <xsl:choose>
               <!-- 29653 Start -->
               <!-- <xsl:when test="$LOB='CPP' or $LOB='BOP' or $LOB='CA'"> --> <!-- 111420 Added $LOB='WCA'-->
                    <xsl:when test="$LOB='CPP' or $LOB='BOP' or $LOB='CA' or $LOB='WCV' or $LOB='WC ' or $LOB='WCA' ">
               <!-- 29653 End  -->
                  <ADD0LINE01>
                     <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
                  </ADD0LINE01>
                  <ADD0LINE02>
                     <!-- Issue 50396 xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/ -->
										<!-- 111420 sys dst log 322 starts-->
										<xsl:choose >
											<xsl:when test="$LOB='WCV' or $LOB='WCA'">
												<xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo[SupplementaryNameCd='DBA']/SupplementaryName"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2"/>
											</xsl:otherwise> 
										</xsl:choose>
										<!-- 111420 sys dst log 322 ends-->
                  </ADD0LINE02>
                  <ADD0LINE03>
                     <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1"/>
                  </ADD0LINE03>
                  <ADD0LINE04>
                     <xsl:value-of select="concat(substring(concat(InsuredOrPrincipal/GeneralPartyInfo/Addr/City,'                            '), 1, 28),InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd)" />
                  </ADD0LINE04>
               </xsl:when>
               <xsl:otherwise>
                  <ADD0LINE01>
                     <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/com.csc_LongName"/> 
                  </ADD0LINE01>
                     <xsl:variable name="AddlInsured" select="InsuredOrPrincipal[InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd='AN']/GeneralPartyInfo/NameInfo/com.csc_LongName"/>                    
                  <ADD0LINE02>
                     <xsl:choose>
                    <xsl:when test="string-length($AddlInsured)">                  
                     <xsl:value-of select="$AddlInsured"/>
                   </xsl:when>
                   <xsl:otherwise>
                     <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2"/>
                   </xsl:otherwise>
                   </xsl:choose> 
                  </ADD0LINE02>
                  <ADD0LINE03>
                      <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1"/>
                  </ADD0LINE03>
                  <ADD0LINE04>
                     <xsl:value-of select="substring(concat(InsuredOrPrincipal/GeneralPartyInfo/Addr/City, '                                                                                                    '), 1, 28)"/>
                     <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd"/>
                  </ADD0LINE04>
               </xsl:otherwise>
            </xsl:choose>
			<!-- 34771 start -->
			<!--<TYPE0ACT>NB</TYPE0ACT>-->	<!-- 50764 - commented out -->
            <!--<TYPE0ACT>
			<xsl:choose>
				<xsl:when test="TransactionEffectiveDt = ''">NB</xsl:when>
				<xsl:when test="TransactionEffectiveDt != ''">EN</xsl:when>
			</xsl:choose>
			</TYPE0ACT>-->
			<!-- 34771 end -->
			<!-- 50764 start -->
			<xsl:choose>
				<xsl:when test="$TYPEACT='RB'">
					<TYPE0ACT>RB</TYPE0ACT>
				</xsl:when>
				<xsl:otherwise>
					<TYPE0ACT>NB</TYPE0ACT>
				</xsl:otherwise>
			</xsl:choose>
			<!-- 50764 end -->
		</PMSP0200__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>

<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->