<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform XML from the iSolutions database
into the ACORD XML Quote business message before sending to the Communications Frameworks   
E-Service case 14734 
***********************************************************************************************
-->

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

    <xsl:template name="CreateLocation">
         <xsl:variable name="Site" select="SITE"/>
         <Location>
                <xsl:attribute name="id"><xsl:value-of select="concat('l',$Site)"/></xsl:attribute>
        <ItemIdInfo>
            <InsurerId><xsl:value-of select="$Site"/></InsurerId>
        </ItemIdInfo>
            <Addr>
           <AddrTypeCd>StreetAddress</AddrTypeCd>
               <!-- 29653 Start -->
               <!--<Addr1><xsl:value-of select="ADDRESS1"/></Addr1>
               <Addr2><xsl:value-of select="OPTIONAL1"/></Addr2> -->
               <Addr1><xsl:value-of select="ADDRESS2"/></Addr1>
               <Addr2></Addr2>
               <!-- 29653 End  -->
               <City><xsl:value-of select="substring(CITYST,1,28)"/></City>
               <StateProvCd><xsl:value-of select="substring(CITYST,29,2)"/></StateProvCd>
               <PostalCode><xsl:value-of select="ZIP"/></PostalCode>
               <Country>USA</Country>
               <County/>
            </Addr>
	        <!-- 29653 Start -->
      	    <!-- <xsl:for-each select="/*/ISLPPP2__RECORD[$Site=DESC0SEQ]"> -->
	        <!--<xsl:for-each select="/*/PMSP1200__RECORD[$Site=USE0LOC]">-->		<!--103409-->
			<xsl:for-each select="/*/PMSP1200__RECORD[DEEMSTAT = '' and $Site=USE0LOC]">	<!--103409-->
	        <!-- 29653 End   -->
                <xsl:call-template name="CreateAdditionalInterest">
                </xsl:call-template>
            </xsl:for-each>
         </Location>
    </xsl:template>
    
<!-- Case 31594 Begin -->
	<xsl:template name="CreateWCLocation">
		<xsl:variable name="Site" select="SITE"/>
		<xsl:variable name="ServerSeq" select="position()"/>
		<Location>
			<xsl:attribute name="id"><xsl:value-of select="concat('l',$ServerSeq)"/></xsl:attribute>
			<ItemIdInfo>
				<InsurerId><xsl:value-of select="$ServerSeq"/></InsurerId>
				<OtherIdentifier>
					<OtherIdTypeCd>com.csc_HostRowSeq</OtherIdTypeCd>
					<OtherId><xsl:value-of select="$Site"/></OtherId>
				</OtherIdentifier>
			</ItemIdInfo>
			<Addr>
				<AddrTypeCd>StreetAddress</AddrTypeCd>
				<Addr1><xsl:value-of select="ADDRESS2"/></Addr1>
				<Addr2><!--<xsl:value-of select="ADDRESS2"/>--></Addr2>
				<City><xsl:value-of select="substring(CITYST,1,28)"/></City>
				<StateProvCd><xsl:value-of select="substring(CITYST,29,2)"/></StateProvCd>
				<PostalCode><xsl:value-of select="ZIP"/></PostalCode>
				<Country>USA</Country>
				<County/>
			</Addr>
			<!-- 62413 start -->
			<TaxCodeInfo>
				<TaxCd><xsl:value-of select="FEIN"/></TaxCd>
				<TaxTypeCd>FEIN</TaxTypeCd>
				<TaxCd><xsl:value-of select="UNEMPNO"/></TaxCd>
				<TaxTypeCd>UNEMPLOYMENT NUMBER</TaxTypeCd>
			</TaxCodeInfo>
			<!-- 62413 end -->
			<xsl:for-each select="/*/PMSP1200__RECORD[$Site=USE0LOC]">
				<xsl:call-template name="CreateWorkCompAdditionalInterest"></xsl:call-template>
			</xsl:for-each>
		</Location>
    </xsl:template>
<!-- Case 31594 End -->
</xsl:stylesheet>

