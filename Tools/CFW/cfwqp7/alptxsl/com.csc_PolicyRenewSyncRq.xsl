<!-- 50764 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform ACORD XML built from the
iSolutions database into Point XML before sending to the the Point system   
E-Service case  
***********************************************************************************************-->

    <xsl:include href="CommonFuncRq.xsl"/>  <!-- Common Template Templates -->
    <xsl:include href="PMSP0200Rq.xsl"/>    <!-- Basic Contract Record  -->

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/com.csc_PolicyRenewSyncRq/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/com.csc_PolicyRenewSyncRq/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/com.csc_PolicyRenewSyncRq/com.csc_InsuranceLineIssuingCompany"/>
    <xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/com.csc_PolicyRenewSyncRq/CompanyProductCd"/>
    <xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/com.csc_PolicyRenewSyncRq/PolicyNumber"/>
    <xsl:variable name="MOD">
  	         <xsl:call-template name="FormatData">
	   	     	<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
		      <xsl:with-param name="FieldLength">2</xsl:with-param>
		      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_PolicyRenewSyncRq/PolicyVersion"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
    </xsl:variable> 
    <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/com.csc_PolicyRenewSyncRq/LOBCd"/>  
    <!-- 29653 start -->
    <xsl:variable name="TYPEACT">
  	<xsl:choose>
		<!--<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode='N'">QQ</xsl:when>-->		<!--103409-->
		<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd='N'">QQ</xsl:when>			<!--103409-->
		<xsl:otherwise>EN</xsl:otherwise>
  	</xsl:choose>
    </xsl:variable> 
    <!-- 29653 end -->  
    <xsl:template match="/">

        <xsl:element name="com.csc_PolicyRenewSyncRq">
<!-- Build Policy Basic Contract Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_PolicyRenewSyncRq" mode="PMSP0200"/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->