<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:template match="ACORD/InsuranceSvcRq/*" mode="PMSPWC02">
    <xsl:variable name="EffDate" select="CommlPolicy/ContractTerm/EffectiveDt"/>
    <xsl:variable name="ExpDate" select="CommlPolicy/ContractTerm/ExpirationDt"/>
    <xsl:variable name="AnvDate" select="WorkCompLineBusiness/WorkCompRateState/AnniversaryRatingDt"/>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPWC02</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPWC02__RECORD>
		<WCSTATUS>P</WCSTATUS>
		<SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION> 
		<DEPOVR>100</DEPOVR>
		<DEPOVRAMT></DEPOVRAMT>
		<PDNBPCT></PDNBPCT>
		<RECALC></RECALC>
		<PICPOLNO></PICPOLNO>
		<PREVEFF></PREVEFF>
		<PREVCARR></PREVCARR>
		<FEIN>
		    <xsl:value-of select="InsuredOrPrincipal[@id='n0']/GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/>
        </FEIN>
		<POOLID>
			<xsl:value-of select="InsuredOrPrincipal[@id='n0']/InsuredOrPrincipalInfo/BusinessInfo/com.csc_NCCIIDNumber"/>
		</POOLID>
		<LEGALID>
			<xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/LegalEntityCd"/>
		</LEGALID>
		<REWPOL></REWPOL>
		<REIPOL></REIPOL>
		<COMMTYPE></COMMTYPE>
		<COMMRATE></COMMRATE>
		<BIACCIDENT>
			 <xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='EachClaim']/FormatCurrencyAmt/Amt) div 1000"/>
		</BIACCIDENT>
		<BIDISEMP>
			<xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='EachEmployee']/FormatCurrencyAmt/Amt) div 1000"/>
		</BIDISEMP>
		<BIDISPOL>
			<xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='PolicyLimit']/FormatCurrencyAmt/Amt) div 1000"/>
		</BIDISPOL>
		<VCACCIDENT>
			<xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='EachClaim']/FormatCurrencyAmt/Amt) div 1000"/>
		</VCACCIDENT>		
		<VCDISEMP>
			<xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='EachEmployee']/FormatCurrencyAmt/Amt) div 1000"/>
		</VCDISEMP>	
		<VCDISPOL>
			<xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='PolicyLimit']/FormatCurrencyAmt/Amt) div 1000"/>
		</VCDISPOL>		
		<POLPREM></POLPREM>
		<MINPREM></MINPREM>
		<LOSSCONST>00000000000</LOSSCONST>
		<EXPCONST></EXPCONST>
		<DEPPREM></DEPPREM>
		<PREMDISC></PREMDISC>
		<PDPCT></PDPCT>
		<EMPLIAB></EMPLIAB>
		<EMPMINPREM>00000000000</EMPMINPREM>
		<VCPREM></VCPREM>
		<TOTCOMM></TOTCOMM>
		<ENDEFFDATE>
			<xsl:call-template name="ConvertISODateToPTDate">
				<xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
			</xsl:call-template>
		</ENDEFFDATE>	
		<PICACT>10</PICACT>
		<FAIND></FAIND>
		<PRORATE></PRORATE>
		<TYPEEXP></TYPEEXP>
		<TYPECOV>01</TYPECOV>
		<PLANIND>01</PLANIND>
		<NONSTD>01</NONSTD>
		<ANVDATE>
			<xsl:call-template name="ConvertISODateToPTDate">
			  <xsl:with-param name="Value" select="WorkCompLineBusiness/WorkCompRateState/AnniversaryRatingDt"/>
			</xsl:call-template>  
		</ANVDATE>
		<ASSIGNDTE></ASSIGNDTE>
		<PHONE></PHONE>
		<DPCTOVRIND></DPCTOVRIND>
	</PMSPWC02__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet>