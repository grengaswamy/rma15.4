<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/com.csc_Form (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/com.csc_Form (Mod) - 31594
-->
  <xsl:template name="CreateISLPPP6">
    <xsl:param name="FormLevel"/>
    <xsl:variable name="TableName">ISLPPP6</xsl:variable>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ISLPPP6</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ISLPPP6__RECORD>
        <RECID>PP6</RECID>
        <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <xsl:choose>
          <xsl:when test="$FormLevel ='Policy'">
            <STATE>
              <xsl:value-of select="ControllingStateProvCd"/>
            </STATE>
          </xsl:when>
          <xsl:otherwise>
            <STATE>
              <xsl:value-of select="StateProvCd"/>
            </STATE>
          </xsl:otherwise>
        </xsl:choose>
        <SEQ>00</SEQ>
        <TRANSDTE>
          <xsl:call-template name="ConvertISODateToPTDate">
		  <!-- Case 31594 start 
            <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/CommlPolicy/ContractTerm/EffectiveDt"/>-->
			<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
		  <!-- Case 31594 end -->
          </xsl:call-template>
        </TRANSDTE>
        <FORMTBL>
          <xsl:for-each select="com.csc_Form">
            <xsl:call-template name="FormatData">
              <xsl:with-param name="FieldName">FORMTBL</xsl:with-param>
              <xsl:with-param name="FieldLength">11</xsl:with-param>
              <xsl:with-param name="Value" select="FormNumber"/>
              <xsl:with-param name="FieldType">A</xsl:with-param>
            </xsl:call-template>
          </xsl:for-each>
        </FORMTBL>
      </ISLPPP6__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->