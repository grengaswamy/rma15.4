<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
<!--02 Segment of 901 file-->
<xsl:include href="CommonFuncRq.xsl"/>  <!-- common routine -->
<xsl:include href="PMSP0000Rq.xsl"/>  <!-- Activity Record -->
<xsl:include href="BuildReinstate02PTSegmentRq.xsl"/>  <!-- Basic Contract Record -->

    <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/com.csc_CompanyPolicyProcessingId"/>
    <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/NAICCd"/>
    <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/com.csc_InsuranceLineIssuingCompany"/>
    <xsl:variable name="SYM">
        <xsl:choose>
            <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/CompanyProductCd)='TP'">H67</xsl:when>
            <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/CompanyProductCd)='THP'">H67</xsl:when>
            <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/CompanyProductCd)='THX'">H67</xsl:when> 
            <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/CompanyProductCd)='TX'">H67</xsl:when>          
            <xsl:otherwise><xsl:value-of select="normalize-space(/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/CompanyProductCd)"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="POL">
	  	    <xsl:call-template name="FormatData">
	   	  		<xsl:with-param name="FieldName">$POL</xsl:with-param>      
		     		 <xsl:with-param name="FieldLength">7</xsl:with-param>
		     		 <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/PolicyNumber"/>
		      		<xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template> 
	</xsl:variable> 
	<xsl:variable name="MOD">
  	         <xsl:call-template name="FormatData">
	   	  	<xsl:with-param name="FieldName">$MOD</xsl:with-param>      
		      <xsl:with-param name="FieldLength">2</xsl:with-param>
		      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/PolicyVersion"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>            
    </xsl:variable>
    <!-- ICH <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/LOBCd"/>   --> 
    <xsl:variable name="LOB">
    <xsl:choose>
       <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/LOBCd)= 'TP'">THP</xsl:when>
       <xsl:when test="normalize-space(/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/LOBCd)= 'TX'">THX</xsl:when>
       <xsl:otherwise>  
   
       <xsl:value-of select="/ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy/LOBCd"/>
       </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    <xsl:variable name="TYPEACT">RI</xsl:variable>



<xsl:template match="/"> 
<PolicyPartialImageReinstateRq> 
<!-- Build Policy Activity Record -->
            <xsl:apply-templates select="ACORD/InsuranceSvcRq/PolicyPartialImageReinstateRq/com.csc_PartialPolicy" mode="PMSP0000"/> 
	      <!--     <xsl:with-param name="TransactionType" select="'RI'"/>
            </xsl:apply-templates>  -->
<!--Build the XSL record for PMS0200 Cancellation Segment-->
	<xsl:call-template name ="BuildReinstate02PTSegmentRq" />
</PolicyPartialImageReinstateRq> 
</xsl:template> 
</xsl:stylesheet>