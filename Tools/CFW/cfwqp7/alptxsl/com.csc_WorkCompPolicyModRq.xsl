<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL is used by CFW to transform the ACORD Mod Request message into a POINT
XML message.
E-Service case 31594 
***********************************************************************************************
-->

  <xsl:include href="CommonFuncRq.xsl"/>
  <!-- Common Template Routines -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP0Rq.xsl"/> -->
  <xsl:include href="PMSP0000Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 0 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP1Rq.xsl"/> -->
  <xsl:include href="PMSP0200Rq.xsl"/>
  <xsl:include href="PMSPWC08Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 1 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP2Rq.xsl"/> -->
  <xsl:include href="PMSP1200Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 2 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP3Rq.xsl"/>-->
  <xsl:include href="PMSPWC04Rq.xsl"/>
  <xsl:include href="PMSPWC07Rq.xsl"/> 
  <xsl:include href="PMSPSA05Rq.xsl"/>
  <xsl:include href="PMSPSA06Rq.xsl"/>
  <xsl:include href="PMSPWC12Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 3 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP4Rq.xsl"/> -->
  <xsl:include href="PMSPSA15Rq.xsl"/>
  <xsl:include href="PMSPSA35Rq.xsl"/>
  <xsl:include href="PMSPSA36Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 4 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP5Rq.xsl"/> -->
  <xsl:include href="PMSPWC25Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 5 record -->
  <!-- 29653 Start -->
  <!--<xsl:include href="ISLPPP6Rq.xsl"/> -->
  <xsl:include href="PMSPWC02Rq.xsl"/>
  <xsl:include href="PMSPWC03Rq.xsl"/>
  <!-- 29653 End   -->
  <!-- WC PPP 6 record -->
  <!-- Case 39568 Start -->
  <xsl:include href="PTInfoRq.xsl"/>
  <!-- Case 39568 End   -->
  <!-- 57418 start -->
  <xsl:include href="BASP0200ERq.xsl"/>
  <!-- Issue 97429 Start-->
  <xsl:include href="PMSPEN01Rq.xsl"/>
  <!-- Issue 97429 End-->

  <xsl:include href="PMSPWC15Rq.xsl"/> <!-- 103186 -->
  
  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
  <xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
  <xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/NAICCd"/>
  <xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
  <xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/CompanyProductCd"/>
  <xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/PolicyNumber"/>
  <xsl:variable name="MOD">
    <xsl:call-template name="FormatData">
      <xsl:with-param name="FieldName">$MOD</xsl:with-param>
      <xsl:with-param name="FieldLength">2</xsl:with-param>
      <xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/PolicyVersion"/>
      <xsl:with-param name="FieldType">N</xsl:with-param>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy/LOBCd"/>
  <xsl:variable name="TYPEACT">EN</xsl:variable> 

  <xsl:template match="/">
    <xsl:element name="com.csc_WorkCompPolicyModRq">
      <!-- 39568 Start -->
      <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq" mode="CreatePTInfo"/> 
      <!-- 39568 End   -->
      <!-- WC PPP 0 record -->
      <!-- 29653 Start -->
      <!--<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq">-->
      <!--<xsl:call-template name="CreateISLPPP0"/> -->
      <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy" mode="PMSP0000"/>
      <!--</xsl:for-each>-->
      <!-- 29653 End   --> 
 	  <!-- Build BASP0200E record - 57418 start-->
	  <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/CommlPolicy" mode="BASP0200E"/>
	  <!-- 57418 end -->
      <!-- WC PPP 1 record -->
      <!-- 29653 Start -->
      <!--<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq"> -->
        <!-- 29653 Start -->
        <!--<xsl:call-template name="CreateISLPPP1">
          <xsl:with-param name="Action">Mod</xsl:with-param>
        </xsl:call-template> -->
        <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq" mode="PMSP0200"/> 
        
        <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq" mode="PMSPWC02"/> 
        <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq" mode="PMSPWC08"/> 
		<!-- 64227 Start -->
        <!--<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq" mode="PMSPWC08"/> -->
		<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq" mode="CreateEmpLimits"/>
        <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq" mode="CreateVolLimits"/>
		<!-- 64227 End -->
      <!--</xsl:for-each> -->
      <!-- 29653 End -->
      <!-- WC PPP 2 record : Additional Interests -->
	  <!--103186 Added a call for CreatePMSPWC15 template -->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location/AdditionalInterest">
         <xsl:variable name="Location" select="substring(../@id, 2,string-length(../@id)-1)"/><!-- Issue 97035 -->
        <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
          <!-- 29653 Start -->
          <!-- <xsl:call-template name="CreateISLPPP2"/> -->
          <xsl:call-template name="CreateAddlInterest">
                <xsl:with-param name="Location" select="$Location"/>    <!-- Issue 97035 -->
	  </xsl:call-template>						<!-- Issue 97035 -->
		   <xsl:if test="(substring(@id,1,1) = 'i')">
				<xsl:call-template name="CreatePMSPWC15"/>
			</xsl:if>
          <!-- 29653 End  -->
        </xsl:if>
      </xsl:for-each>
      <!-- WC PPP 3 record : Locations -->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo">
      	<!-- Case 31594 Begin -->
      	<xsl:sort select="@LocationRef"/>
      	<!-- Case 31594 End -->
        <!-- 29653 Start -->
        <!--<xsl:call-template name="CreateISLPPP3Defaults"/>-->
        <xsl:call-template name="CreatePMSPWC04Defaults"/>
        <xsl:call-template name="CreatePMSPWC07Defaults"/> 
        <xsl:call-template name="CreatePMSPSA05Defaults"/>
        <xsl:call-template name="CreatePMSPSA06Defaults"/>
        <xsl:call-template name="CreatePMSPWC12"/>
      <!-- 29653 End   -->
      </xsl:for-each>
      <!-- WC PPP 4 record : Class codes -->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass">
        <!--<xsl:call-template name="CreateISLPPP4">
          <xsl:with-param name="Action">Mod</xsl:with-param>
        </xsl:call-template> -->
        <!-- 29653 Start -->
			<!--<xsl:call-template name="CreateISLPPP4">
			  <xsl:with-param name="Action">Quote</xsl:with-param>
			</xsl:call-template> -->
			<xsl:call-template name="CreatePMSPSA15">
			  <xsl:with-param name="Action">Mod</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="CreatePMSPSA35">
			  <xsl:with-param name="Action">Mod</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="CreatePMSPSA36">
			  <xsl:with-param name="Action">Mod</xsl:with-param>
			</xsl:call-template>
			<!-- 29653 End -->
      </xsl:for-each>
      <!-- WC PPP 5 record : Optional Mods -->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/CreditOrSurcharge">
        <!-- The Experience mods are listed in this section, but should not be sent as
        optional mods. -->
        <xsl:if test="(CreditSurchargeCd != 'EXP1') and (CreditSurchargeCd != 'EXP2') and (CreditSurchargeCd != 'ARAP')">	<!-- Issue 62152 Add test for ARAP and not 0 -->
			<!-- Issue 94177 Removed NumericValue/FormatModFactor-->
           <!-- 29653 Start -->
          <!--<xsl:call-template name="CreateISLPPP5"/>-->
			<!-- Issue 94177 Starts-->
			<xsl:if test="(normalize-space(CreditSurchargeCd = 'RPAT') or (CreditSurchargeCd = 'ENDM'))">
				<xsl:if test ="(CreditSurchargeCd != 'EMOD')">
          <xsl:call-template name="CreatePMSPWC25"/>
				</xsl:if>
			</xsl:if>
			<xsl:if test ="(NumericValue/FormatModFactor != '0')">
				<xsl:call-template name="CreatePMSPWC25"/>
			</xsl:if>
			<!-- Issue 94177 Ends-->
          <!-- 29653 End --> 
        </xsl:if>
      </xsl:for-each>
      <!-- WC PPP 6 record : Optional Forms -->
      <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState">
        <xsl:if test="string-length(com.csc_Form[1]/FormNumber) &gt; 0">
          <!-- 29653 Start -->
          <!--<xsl:call-template name="CreateISLPPP6">
            <xsl:with-param name="FormLevel">State</xsl:with-param>
          </xsl:call-template> -->
          <xsl:call-template name="CreatePMSPWC03">
            <xsl:with-param name="FormLevel">State</xsl:with-param>
          </xsl:call-template>
          <!-- 29653 End  -->
        </xsl:if>
      </xsl:for-each>
	  <!-- Issue 97429 Start-->
	  <xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq" mode="PMSPEN01"/> 
	  <!-- Issue 97429 End-->
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->