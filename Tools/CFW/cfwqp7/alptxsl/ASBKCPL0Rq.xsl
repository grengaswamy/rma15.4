<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template name="CreateCoverageRec">
      <xsl:variable name="TableName">ASBKCPL1</xsl:variable>
      <BUS__OBJ__RECORD>
         <RECORD__NAME__ROW>
            <RECORD__NAME>ASBKCPL1</RECORD__NAME>
         </RECORD__NAME__ROW>
         <ASBKCPL1__RECORD>
            <BKAACD>
               <xsl:value-of select="$LOC"/>
            </BKAACD>
            <BKABCD>
               <xsl:value-of select="$MCO"/>
            </BKABCD>
            <BKARTX>
               <xsl:value-of select="$SYM"/>
            </BKARTX>
            <BKASTX>
               <xsl:value-of select="$POL"/>
            </BKASTX>
            <BKADNB>
               <xsl:value-of select="$MOD"/>
            </BKADNB>
            <BKAGTX>
               <xsl:value-of select="$LOB"/>
            </BKAGTX>
            <BKBRNB>00001</BKBRNB>
            <BKEGNB>00001</BKEGNB>
            <BKANTX>
               <!--<xsl:value-of select="$SYM"/>--> <!--issue # 40090-->
			     <xsl:value-of select="$LOB"/>  <!--issue # 40090-->
            </BKANTX>
            <BKAENB>
	         <xsl:call-template name="FormatData">
	   	     	<xsl:with-param name="FieldName">BKAENB</xsl:with-param>      
		      <xsl:with-param name="FieldLength">5</xsl:with-param>
		      <xsl:with-param name="Value" select="parent::node()/ItemIdInfo/InsurerId"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>                        
            </BKAENB>
            <BKAOTX>
               <xsl:value-of select="CoverageCd"/>
            </BKAOTX>
            <BKC0NB>1</BKC0NB>
            <BKC6ST>P</BKC6ST>
            <BKANDT>0000000</BKANDT>
            <BKAFNB>00</BKAFNB>
            <BKALDT>0000000</BKALDT>
            <BKAFDT>0000000</BKAFDT>
            <BKTYPE0ACT>
		<!-- 34771 start -->
			<!--103409 starts-->
			<!--<xsl:choose>
			<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
			<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when> --><!-- 50764 --><!--
			<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when> --><!-- 39039 --><!--
		</xsl:choose>-->

		<xsl:call-template name="ConvertPolicyStatusCd">
			<xsl:with-param name="PolicyStatusCd">
				<xsl:value-of select="//PolicyStatusCd"/>
			</xsl:with-param>
		</xsl:call-template>
		<!--103409 Ends-->
		<!-- 34771 end -->
	   </BKTYPE0ACT>
            <BKBSNB>9999</BKBSNB>
            <BKBBTX>
                     <xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/>
            </BKBBTX>
            <BKA9NB>
	         <xsl:call-template name="FormatData">
	   	     	<xsl:with-param name="FieldName">BKA9NB</xsl:with-param>      
		      <xsl:with-param name="FieldLength">11</xsl:with-param>
		      <xsl:with-param name="Precision">0</xsl:with-param>		      
		      <xsl:with-param name="Value" select="Deductible/FormatCurrencyAmt/Amt"/>
		      <xsl:with-param name="FieldType">N</xsl:with-param>
	         </xsl:call-template>                                    
            </BKA9NB>
            <BKETNB>1</BKETNB>
         </ASBKCPL1__RECORD>
      </BUS__OBJ__RECORD>
   </xsl:template>
</xsl:stylesheet>
