<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocInfo/WorkCompRateClass (Add)
-->
  <xsl:template name="CreatePMSPSA36">
    <xsl:param name="Action"/>
    <xsl:variable name="TableName">PMSPSA36</xsl:variable>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPSA36</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPSA36__RECORD>
      	<SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
         <UNITNO>
          <xsl:call-template name="FormatData">
            <xsl:with-param name="FieldName">UNITNO</xsl:with-param>
            <xsl:with-param name="FieldLength">5</xsl:with-param>
            <xsl:with-param name="Value" select="substring(../@LocationRef,2,string-length(../@LocationRef)-1)"/>
            <xsl:with-param name="FieldType">N</xsl:with-param>
          </xsl:call-template>
        </UNITNO>
        <COVSEQ>
        <!-- 29653 Start -->
		<!-- DST issue WC 017 start -->
        <xsl:choose>
            <xsl:when test="$TYPEACT = 'EN'">	
			<!--103409 Starts-->		
			<!--<xsl:if test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd='DeemedType'">
				<xsl:call-template name="FormatData">
	            		<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="position()"/>
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
	          		</xsl:call-template>
				</xsl:if>
			<xsl:if test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd!='DeemedType'">
                <xsl:call-template name="FormatData">
            		<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
             		<xsl:with-param name="FieldLength">4</xsl:with-param>
					<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier/OtherId"/>					
            		<xsl:with-param name="FieldType">N</xsl:with-param>
          		</xsl:call-template>
			</xsl:if>-->
			<xsl:choose>
				<xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd='DeemedType'">
					<xsl:call-template name="FormatData">
	            		<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="position()"/>
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
	          		</xsl:call-template>
				</xsl:when>
				<!--sys/dst log 396 Issue 103409 starts
				<xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd!='DeemedType'">
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd!='DeemedType']/OtherId"/>		
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</xsl:when>-->
				<xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd!='DeemedType'">
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">COVSEQ</xsl:with-param>
	             		<xsl:with-param name="FieldLength">4</xsl:with-param>
						<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='HostID']/OtherId"/>		
	            		<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<!--sys/dst log 396 Issue 103409 ends-->
			</xsl:choose>
			<!-- DST issue WC 017 end -->
            </xsl:when>
		    <xsl:otherwise>
                <xsl:value-of select="position()"/>
            </xsl:otherwise>
         </xsl:choose>
         <!-- 29653 End -->
        </COVSEQ>
       	<TRANSSEQ>0001</TRANSSEQ>
		<!-- 50771 start-->
		<xsl:variable name="UseInfo" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='DeemedType']/OtherId"/>     
		<SPEC50>0000000000000000000000000<xsl:value-of select="normalize-space($UseInfo)"/>
		<xsl:if test="normalize-space($UseInfo)=''"> 
				<xsl:value-of select="string('  ') "/>
		</xsl:if>
		<!-- 90224 Start -->
		<!--<xsl:call-template name="FormatData">
		<xsl:with-param name="FieldName">SARDESCSEQ</xsl:with-param>
		<xsl:with-param name="FieldLength">5</xsl:with-param>
		<xsl:with-param name="Value" select="com.csc_SequenceNumber"/>
		<xsl:with-param name="FieldType">N</xsl:with-param>
		</xsl:call-template> -->
      <!--Issue 90224 starts-->
      <xsl:choose>
        <xsl:when test="com.csc_ItemIdInfo/OtherIdentifier/OtherIdTypeCd='DeemedType'">
          <!--Issue 90224 ends-->

		<xsl:call-template name="FormatData">
            	<xsl:with-param name="FieldName">SARDESCSEQ</xsl:with-param>
            	<xsl:with-param name="FieldLength">5</xsl:with-param>
            	<!--<xsl:with-param name="Value" select="com.csc_SequenceNumber"/>-->		<!--103409-->
            	<xsl:with-param name="Value" select="com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='SeqNbr']/OtherId"/>		<!--103409-->
            	<xsl:with-param name="FieldType">N</xsl:with-param>
          	</xsl:call-template>
          <!--Issue 90224 starts-->
        </xsl:when>
        <xsl:otherwise>       
            <xsl:value-of select="string('     ') "/>
        </xsl:otherwise>
      </xsl:choose>
      <!--Issue 90224 ends-->
		</SPEC50>
		<!-- 50771 start-->
	</PMSPSA36__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->