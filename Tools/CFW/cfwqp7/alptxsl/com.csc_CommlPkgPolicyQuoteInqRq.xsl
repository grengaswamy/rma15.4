<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:include href="CommonFuncRq.xsl"/>
	<!-- Common Formatting Routines -->
	<xsl:include href="PMSP0000Rq.xsl"/>
	<!-- Activity Record -->
	<xsl:include href="PMSP0200Rq.xsl"/>
	<!-- Basic Contract Record -->
	<xsl:include href="PMSP1200Rq.xsl"/>
	<!-- Additional Interest Record -->
	<xsl:include href="ASBACPL1Rq.xsl"/>
	<!-- Policy Level Record -->
	<xsl:include href="ASBBCPL1Rq.xsl"/>
	<!-- Insurance Line Record -->
	<xsl:include href="ASBCCPL1Rq.xsl"/>
	<!-- Product Record -->
	<xsl:include href="ASBUCPL1Rq.xsl"/>
	<!-- Location Record -->
	<xsl:include href="ASBVCPL1Rq.xsl"/>
	<!-- Sublocation Record -->
	<xsl:include href="ASBYCPL1Rq.xsl"/>
	<!-- Coverage Record -->
	<xsl:include href="ASB5CPL1Rq.xsl"/>
	<!-- Unit Record -->
	<xsl:include href="ALB5CPL1Rq.xsl"/>
	<!-- Unit Record (Add'l Info) -->
	<!--59878 starts-->
	<xsl:include href="ASBZCPL1Rq.xsl"/><!-- Schedule Items -->
	<xsl:include href="PTInfoRq.xsl"/>
	<xsl:include href="PMSP1200ATRq.xsl"/>
	<xsl:include href="ASCSCPL1Rq.xsl"/>
	<xsl:include href="PTInfoRq.xsl"/>
	<xsl:include href="ASBECPL1Rq.xsl"/>
	<!--59878 ends-->
    <!-- Start Case 27708 - Add client files Jeff Simmons --> 
    <xsl:include href="BASCLT0100Rq.xsl"/>
    <xsl:include href="BASCLT0300Rq.xsl"/>
    <xsl:include href="BASCLT1400Rq.xsl"/>
    <xsl:include href="BASCLT1500Rq.xsl"/> 
    <!-- End   Case 27708 - Add client files Jeff Simmons --> 

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
	<xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/NAICCd"/>
	<xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/com.csc_InsuranceLineIssuingCompany"/>
	<xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/CompanyProductCd"/>
	<!-- 29653 start -->
	<xsl:variable name="TYPEACT">
		<xsl:choose>
			<!--<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode='N'">NB</xsl:when>-->			<!--103409-->
			<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd='N'">NB</xsl:when>			<!--103409-->
			<xsl:otherwise>EN</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- 29653 end -->

	<xsl:variable name="POL">
		<xsl:call-template name="FormatData">
			<xsl:with-param name="FieldName">$POL</xsl:with-param>
			<xsl:with-param name="FieldLength">7</xsl:with-param>
			<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/PolicyNumber"/>
			<xsl:with-param name="FieldType">N</xsl:with-param>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="MOD">
		<xsl:call-template name="FormatData">
			<xsl:with-param name="FieldName">$MOD</xsl:with-param>
			<xsl:with-param name="FieldLength">2</xsl:with-param>
			<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/PolicyVersion"/>
			<xsl:with-param name="FieldType">N</xsl:with-param>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/LOBCd"/>
	<xsl:variable name="PrimaryState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/ControllingStateProvCd"/>

	<xsl:template match="/">
		<xsl:element name="com.csc_CommlPkgPolicyQuoteInqRq">
			<!-- Issue 59878 - start -->
			<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq" mode="CreatePTInfo"/>
			<!-- Issue 59878 - end -->
			<!-- Build Policy Activity Record -->
			<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy" mode="PMSP0000"/>
			<!-- Build Policy Basic Contract Record -->
			<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq" mode="PMSP0200"/>
			<!-- Build Policy Level Additional Interest Records -->
			<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/AdditionalInterest">
				<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName) &gt; 0">
					<xsl:call-template name="CreateAddlInterest"/>
				</xsl:if>
			</xsl:for-each>
			<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location">
				<xsl:variable name="Location" select="ItemIdInfo/InsurerId"/>
				<!-- Build Location Records -->
				<xsl:call-template name="CreateLocation">
					<xsl:with-param name="Location" select="$Location"/>
				</xsl:call-template>
				<!-- Build SubLocation Records -->
				<xsl:for-each select="SubLocation">
					<xsl:call-template name="CreateSubLocation">
						<xsl:with-param name="Location" select="$Location"/>
					</xsl:call-template>
				</xsl:for-each>
				<!-- Build Additional Interests Attached to SubLocations -->
				<xsl:for-each select="SubLocation">
					<!--59878 starts-->
					<xsl:variable name="SubLocation" select="ItemIdInfo/InsurerId"/>
					<xsl:variable name="AddInsName" select="AdditionalInterest/AdditionalInterestInfo/NatureInterestCd"/>
					<xsl:if test="$AddInsName!=''">
						<xsl:for-each select="AdditionalInterest">
							<xsl:variable name="InsLine" select="com.csc_ItemIdInfo/OtherIdentifier/OtherId"/>
							<xsl:variable name="InterestType" select="AdditionalInterestInfo/NatureInterestCd"/>
							<xsl:variable name="AISequence" select="number(com.csc_ItemIdInfo/UseCodeSeq)-1"/>
							<!--59878 ends-->
							<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
								<xsl:call-template name="CreateAddlInterest">
									<xsl:with-param name="Location" select="$Location"/>
									<xsl:with-param name="SubLocation" select="$SubLocation"/>
								</xsl:call-template>
							</xsl:if>
							<!--</xsl:for-each> 59878 -->
							<!-- Build Additional Interests Attached to this Location -->
							<!--<xsl:for-each select="AdditionalInterest"> 59878-->

							<!--59878 starts-->
							<xsl:call-template name="CreateAddlInterest12AT">
								<xsl:with-param name="Location" select="$Location"/>
								<xsl:with-param name="SubLocation" select="$SubLocation"/>
								<xsl:with-param name="InsLine" select="$InsLine"/>
								<xsl:with-param name="InterestType" select="$InterestType"/>
								<xsl:with-param name="AISequence" select="$AISequence"/>
							</xsl:call-template>
						</xsl:for-each>
					</xsl:if>
					<!--<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                        <xsl:call-template name="CreateAddlInterest">
                            <xsl:with-param name="Location" select="$Location"/>    
                        </xsl:call-template>
                   </xsl:if>-->
					<!--59878 ends-->
				</xsl:for-each>
			</xsl:for-each>
			<!-- Build CPP Policy Level Record -->
			<xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq" mode="ASBACPL1"/>
			<!-- Build CPP Insurance Line Information Records -->
			<xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/com.csc_CommlPropertyLineBusiness"/>
			<xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/com.csc_GeneralLiabilityLineBusiness"/>
			<xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/com.csc_CrimeLineBusiness"/>
			<xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/com.csc_CommlInlandMarineLineBusiness"/>
           <xsl:apply-templates select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness"/>		<!-- Issue 67226 -->
           <!-- Start Case 27708 - Add client files Jeff Simmons --> 
		   <xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq">
              <xsl:call-template name="CreateBASCLT0100"></xsl:call-template>
              <xsl:call-template name="CreateBASCLT0300"></xsl:call-template>
              <xsl:call-template name="CreateBASCLT1400"></xsl:call-template>
           </xsl:for-each>
    
           <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/AdditionalInterest"> 
              <xsl:call-template name="CreateBASCLT1500"/> 
           </xsl:for-each> 
           
           <xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location">  
              <xsl:variable name="Location" select="ItemIdInfo/InsurerId"/>  
              <xsl:for-each select="SubLocation/AdditionalInterest">
                 <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">                
                   <xsl:call-template name="CreateBASCLT1500">
                      <xsl:with-param name="Location" select="$Location"/> 
                      <xsl:with-param name="SubLocation" select="ItemIdInfo/InsurerId"/>      
                          
                   </xsl:call-template>
                 </xsl:if>                    
              </xsl:for-each>
           
              <xsl:for-each select="AdditionalInterest">
                 <xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
                   <xsl:call-template name="CreateBASCLT1500">
                      <xsl:with-param name="Location" select="$Location"/>    
                   </xsl:call-template>
                 </xsl:if>                          
              </xsl:for-each>
           </xsl:for-each> 
           <!-- End   Case 27708 - Add client files Jeff Simmons --> 
			<!-- 59878 starts-->
			<xsl:apply-templates select="ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Producer" mode="CreatePTInfo"/>
			<!-- 59878 ends-->
		</xsl:element>
	</xsl:template>

	<xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/com.csc_GeneralLiabilityLineBusiness">
		<xsl:variable name="InsLine">GL</xsl:variable>
		<!-- Build GL Insurance Line Record -->
		<xsl:call-template name="BuildInsuranceLine">
			<xsl:with-param name="InsLine" select="$InsLine"/>
		</xsl:call-template>
		<!-- Build GL Unit Records  -->
		<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location">
			<xsl:variable name="id" select="@id"/>
			<xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/com.csc_GeneralLiabilityLineBusiness/LiabilityInfo/GeneralLiabilityClassification[@LocationRef=$id and CommlCoverage/CoverageCd!='PCO']"/>
			<xsl:if test="$Node !=''">
				<xsl:call-template name="BuildUnitRecord">
					<xsl:with-param name="Location" select="substring($id,2,string-length($id)-1)"/>
					<xsl:with-param name="RateState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location[@id=$id]/Addr/StateProvCd"/>
					<xsl:with-param name="InsLine" select="$InsLine"/>
					<xsl:with-param name="Product" select="$InsLine"/>
					<xsl:with-param name="Node" select="$Node"/>
				</xsl:call-template>
				<xsl:call-template name="BuildALB5CPL1">
					<xsl:with-param name="Location" select="substring($id,2,string-length($id)-1)"/>
					<xsl:with-param name="SubLocation" select="'0'"/>
					<xsl:with-param name="InsLine" select="$InsLine"/>
					<xsl:with-param name="Node" select="$Node"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
		<!-- Build GL Product Record -->
		<xsl:for-each select="com.csc_ModificationFactors">
			<xsl:call-template name="BuildProductRecord">
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState" select="StateProvCd"/>
			</xsl:call-template>
		</xsl:for-each>
		<!-- Issue 59878 - Start -->
		<!-- Build the Forms -->
		<xsl:for-each select="com.csc_Form">
			<xsl:if test="FormNumber !=''">
				<xsl:call-template name="BuildForms">
					<xsl:with-param name="InsLine">GL</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
		<!-- Issue 59878 - End -->
		<!-- Build GL Coverage Records -->
		<xsl:for-each select="LiabilityInfo/GeneralLiabilityClassification[CommlCoverage/CoverageCd!='PCO']">
			<xsl:variable name="LocationRef" select="substring(@LocationRef,2,string-length(@LocationRef)-1)"/>
			<xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location[@id=concat('l',$LocationRef)]/Addr/StateProvCd"/>
			<xsl:call-template name="BuildCoverage">
				<xsl:with-param name="Location">
					<xsl:choose>
						<xsl:when test="string-length($LocationRef)">
							<xsl:value-of select="$LocationRef"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'0'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState">
					<xsl:choose>
						<xsl:when test="string-length($LocState)">
							<xsl:value-of select="$LocState"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PrimaryState"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/com.csc_CommlPropertyLineBusiness">
		<xsl:variable name="InsLine">CF</xsl:variable>
		<!-- Build CF Insurance Line Record -->
		<xsl:call-template name="BuildInsuranceLine">
			<xsl:with-param name="InsLine" select="$InsLine"/>
		</xsl:call-template>
		<!-- Build CF Unit Records  -->
		<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlSubLocation">
			<xsl:variable name="LocationRef" select="@LocationRef"/>
			<xsl:variable name="SubLocationRef" select="@SubLocationRef"/>
			<xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/com.csc_CommlPropertyLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef and SubjectInsuranceCd!='BLNKT']"/>
			<xsl:if test="$Node !=''">
				<xsl:call-template name="BuildUnitRecord">
					<xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
					<xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
					<xsl:with-param name="RateState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
					<xsl:with-param name="InsLine" select="$InsLine"/>
					<xsl:with-param name="Product" select="$InsLine"/>
					<xsl:with-param name="Node" select="$Node"/>
				</xsl:call-template>
				<xsl:call-template name="BuildALB5CPL1">
					<xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
					<xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
					<xsl:with-param name="InsLine" select="$InsLine"/>
					<xsl:with-param name="Node" select="$Node"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
		<!-- Build CF Product Record -->
		<xsl:for-each select="com.csc_ModificationFactors">
			<xsl:call-template name="BuildProductRecord">
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState" select="StateProvCd"/>
			</xsl:call-template>
		</xsl:for-each>
		<!-- Issue 59878 - Start -->
		<!-- Build the Forms -->
		<xsl:for-each select="com.csc_Form">
			<xsl:if test="FormNumber !=''">
				<xsl:call-template name="BuildForms">
					<xsl:with-param name="InsLine">CF</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
		<!-- Issue 59878 - End -->
		<!-- Build CF Coverage Records -->
		<!-- Issue 59878 - Start -->
		<!--xsl:for-each select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd!='BLNKT']"-->
		<xsl:for-each select="PropertyInfo/CommlPropertyInfo[@LocationRef!='l0']">
		<!-- Issue 59878 - End -->
			<xsl:variable name="LocationRef" select="@LocationRef"/>
			<xsl:variable name="SubLocation" select="substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)"/>
			<!--Issue 102807 starts-->
			<xsl:variable name="SubLocationRef" select="@SubLocationRef"/>
			<xsl:variable name="AlarmClassCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlSubLocation[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]/AlarmAndSecurity/com.csc_AlarmClassCd"/>
			<xsl:variable name="WatchmanCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlSubLocation[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]/AlarmAndSecurity/com.csc_WatchmanCd"/>
			<xsl:variable name="AlarmConnectionCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlSubLocation[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]/AlarmAndSecurity/com.csc_AlarmConnectionCd"/>
			<!--Issue 102807 ends-->
			<xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
			<xsl:call-template name="BuildCoverage">
				<xsl:with-param name="Location">
					<xsl:choose>
						<xsl:when test="string-length($LocationRef)">
							<xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'0'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="SubLocation" select="$SubLocation"/>
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState">
					<xsl:choose>
						<xsl:when test="string-length($LocState)">
							<xsl:value-of select="$LocState"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PrimaryState"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<!--Issue 102807 starts-->
				<xsl:with-param name="AlarmClassCd" select="$AlarmClassCd"/>
				<xsl:with-param name="WatchmanCd" select="$WatchmanCd"/>
				<xsl:with-param name="AlarmConnectionCd" select="$AlarmConnectionCd"/>
				<!--Issue 102807 ends-->
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/com.csc_CrimeLineBusiness">
		<xsl:variable name="InsLine">CR</xsl:variable>
		<!-- Build Crime Insurance Line Record -->
		<xsl:call-template name="BuildInsuranceLine">
			<xsl:with-param name="InsLine" select="$InsLine"/>
		</xsl:call-template>
		<!-- Build Crime Unit Records  -->
		<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlSubLocation">
			<xsl:variable name="LocationRef" select="@LocationRef"/>
			<xsl:variable name="SubLocationRef" select="@SubLocationRef"/>
			<xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/com.csc_CrimeLineBusiness/com.csc_CommlCrimeInfo/com.csc_CommlCrimeClassification[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]"/>
			<xsl:if test="$Node !=''">
				<xsl:call-template name="BuildUnitRecord">
					<xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
					<xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
					<xsl:with-param name="RateState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
					<xsl:with-param name="InsLine" select="$InsLine"/>
					<xsl:with-param name="Product" select="$InsLine"/>
					<xsl:with-param name="Node" select="$Node"/>
				</xsl:call-template>
				<xsl:call-template name="BuildALB5CPL1">
					<xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
					<xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
					<xsl:with-param name="InsLine" select="$InsLine"/>
					<xsl:with-param name="Node" select="$Node"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
		<!-- Build Crime Product Record -->
		<xsl:for-each select="com.csc_ModificationFactors">
			<xsl:call-template name="BuildProductRecord">
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState" select="StateProvCd"/>
			</xsl:call-template>
		</xsl:for-each>
		<!-- Issue 59878 - Start -->
		<!-- Build the Forms -->
		<xsl:for-each select="com.csc_Form">
			<xsl:if test="FormNumber !=''">
				<xsl:call-template name="BuildForms">
					<xsl:with-param name="InsLine">CR</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
		<!-- Issue 59878 - End -->
		<!-- Build Crime Coverage Records -->
		<xsl:for-each select="com.csc_CommlCrimeInfo/com.csc_CommlCrimeClassification">
			<xsl:variable name="LocationRef" select="@LocationRef"/>
			<xsl:variable name="SubLocation" select="substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)"/>
			<xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
			<xsl:call-template name="BuildCoverage">
				<xsl:with-param name="Location">
					<xsl:choose>
						<xsl:when test="string-length($LocationRef)">
							<xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'0'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="SubLocation" select="$SubLocation"/>
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState">
					<xsl:choose>
						<xsl:when test="string-length($LocState)">
							<xsl:value-of select="$LocState"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PrimaryState"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/com.csc_CommlInlandMarineLineBusiness">
		<xsl:variable name="InsLine">IMC</xsl:variable>
		<!-- Build Inland Marine Insurance Line Record -->
		<xsl:call-template name="BuildInsuranceLine">
			<xsl:with-param name="InsLine" select="$InsLine"/>
		</xsl:call-template>
		<!-- Build Inland Marine Unit Records  -->
		<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlSubLocation">
			<xsl:variable name="LocationRef" select="@LocationRef"/>
			<xsl:variable name="SubLocationRef" select="@SubLocationRef"/>
			<!--  <xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/com.csc_CommlInlandMarineLineBusiness/com.csc_CommlInlandMarineInfo/com.csc_CommlInlandMarineClassification[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef and CommlCoverage/CoverageCd !='ACCTS']"/> -->
			<xsl:variable name="Node" select="/ACORD/InsuranceSvcRq/*/com.csc_CommlInlandMarineLineBusiness/com.csc_CommlInlandMarineInfo/com.csc_CommlInlandMarineClassification[@LocationRef=$LocationRef and @SubLocationRef=$SubLocationRef]"/>
			<xsl:if test="$Node !=''">
				<xsl:call-template name="BuildUnitRecord">
					<xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
					<xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
					<xsl:with-param name="RateState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
					<xsl:with-param name="InsLine" select="$InsLine"/>
					<xsl:with-param name="Product" select="$InsLine"/>
					<xsl:with-param name="Node" select="$Node"/>
				</xsl:call-template>
				<xsl:call-template name="BuildALB5CPL1">
					<xsl:with-param name="Location" select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
					<xsl:with-param name="SubLocation" select="substring($SubLocationRef,2,string-length($SubLocationRef)-1)"/>
					<xsl:with-param name="InsLine" select="$InsLine"/>
					<xsl:with-param name="Node" select="$Node"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
		<!-- Build Inland Marine Product Record -->
		<xsl:for-each select="com.csc_ModificationFactors">
			<xsl:call-template name="BuildProductRecord">
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState" select="StateProvCd"/>
			</xsl:call-template>
		</xsl:for-each>
		<!-- Issue 59878 - Start -->
		<!-- Build the Forms -->
		<xsl:for-each select="com.csc_Form">
    		<xsl:if test="FormNumber !=''">
				<xsl:call-template name="BuildForms">
					<xsl:with-param name="InsLine">IMC</xsl:with-param>
				</xsl:call-template>
    		</xsl:if>
		</xsl:for-each>
		<!-- Issue 59878 - End -->
		<!-- Build Inland Marine Coverage Records -->
		<xsl:for-each select="com.csc_CommlInlandMarineInfo/com.csc_CommlInlandMarineClassification">
			<xsl:variable name="LocationRef" select="@LocationRef"/>
			<xsl:variable name="SubLocation" select="substring(@SubLocationRef,2,string-length(@SubLocationRef)-1)"/>
			<xsl:variable name="LocState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/Location[@id=$LocationRef]/Addr/StateProvCd"/>
			<xsl:call-template name="BuildCoverage">
				<xsl:with-param name="Location">
					<xsl:choose>
						<xsl:when test="string-length($LocationRef)">
							<xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'0'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="SubLocation" select="$SubLocation"/>
				<xsl:with-param name="InsLine" select="$InsLine"/>
				<xsl:with-param name="RateState">
					<xsl:choose>
						<xsl:when test="string-length($LocState)">
							<xsl:value-of select="$LocState"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$PrimaryState"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
			<!-- Issue 59878 - Start-->
			<xsl:for-each select="CommlCoverage/PropertySchedule">
				<xsl:variable name="PropId" select="ItemIdInfo"/>
				<xsl:if test="$PropId !=''">
					<xsl:call-template name="BuildCoverageSupplement">
						<xsl:with-param name="Location">
							<xsl:choose>
								<xsl:when test="string-length($LocationRef)">
									<xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'0'"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="SubLocation" select="$SubLocation"/>
						<xsl:with-param name="InsLine" select="$InsLine"/>
						<xsl:with-param name="Position" select="1"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:for-each>
			<xsl:for-each select="CommlCoverage/Watercraft">
				<xsl:variable name="WaterId" select="com.csc_ItemIdInfo"/>
				<xsl:if test="$WaterId !=''">
					<xsl:call-template name="BuildCoverageSupplement">
						<xsl:with-param name="Location">
							<xsl:choose>
								<xsl:when test="string-length($LocationRef)">
									<xsl:value-of select="substring($LocationRef,2,string-length($LocationRef)-1)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'0'"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="SubLocation" select="$SubLocation"/>
						<xsl:with-param name="InsLine" select="$InsLine"/>
						<xsl:with-param name="Position" select="1"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:for-each>
			<!--Issue 59878 - END-->
		</xsl:for-each>
	</xsl:template>
	  <!-- Issue 67226 Start : Added CommlAuto Section to Add Rq-->
	<xsl:template match="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness ">
		<xsl:variable name="PrimaryState" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlPolicy/ControllingStateProvCd"/>
		<!--<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/com.csc_ModificationFactors/StateProvCd">-->	<!--Issue 59878-->
		<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/com.csc_ModificationFactors">	<!--Issue 59878-->
			<xsl:call-template name="BuildProductRecord">
				<xsl:with-param name="InsLine">CA</xsl:with-param>
				<!--<xsl:with-param name="RateState" select="."/>-->		<!--Issue 59878-->
				<xsl:with-param name="RateState" select="StateProvCd"/>		<!--Issue 59878-->
			</xsl:call-template>
		</xsl:for-each>
		<!-- Build Trailer Interchange ASBY Record  -->
		<xsl:for-each select="TruckersSupplement/TruckersTrailerInterchange/CommlCoverage">
			<xsl:if test="CoverageCd!=''">
				<xsl:call-template name="BuildTrailerCoverage">
					<xsl:with-param name="InsLine">CA</xsl:with-param>
					<xsl:with-param name="RateState" select="$PrimaryState"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
		<!-- Issue 59878 - Start -->
		<!-- Build the Forms -->
		<xsl:for-each select="com.csc_Form">
			<xsl:if test="FormNumber !=''">
				<xsl:call-template name="BuildForms">
					<xsl:with-param name="InsLine">CA</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</xsl:for-each>
		<!-- Issue 59878 - End -->
		<!-- Insurance Line Information -->
		<xsl:for-each select="CommlRateState[StateProvCd=$PrimaryState]">
			<xsl:call-template name="BuildInsuranceLine">
				<xsl:with-param name="InsLine">CA</xsl:with-param>
				<xsl:with-param name="RateState" select="$PrimaryState"></xsl:with-param>
			</xsl:call-template>
		</xsl:for-each>
		<!-- State and Vehicle Coverages -->
		<xsl:for-each select="CommlRateState">
			<xsl:variable name="RateState" select="StateProvCd"/>
			<xsl:variable name="VehCount" select="count(CommlVeh)"/>
			<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/ItemIdInfo/InsurerId != ''">
			<xsl:for-each select="CommlVeh">
				<xsl:variable name="Node" select="."/>
				<!-- Build Unit Records -->
				<xsl:call-template name="BuildUnitRecord">
					<xsl:with-param name="Location">0</xsl:with-param>
					<xsl:with-param name="SubLocation">0</xsl:with-param>
					<xsl:with-param name="RateState" select="$RateState"/>
					<xsl:with-param name="InsLine">CA</xsl:with-param>
					<xsl:with-param name="Product">CA</xsl:with-param>
					<xsl:with-param name="Node" select="$Node"/>
					<xsl:with-param name="UnitNbr" select="@id"></xsl:with-param>
				</xsl:call-template>
				<!--Additional Interests attached to this Vehicle-->
				<xsl:for-each select="AdditionalInterest">
					<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
						<xsl:call-template name="CreateAddlInterest">
							<xsl:with-param name="Location">0</xsl:with-param>
							<xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:for-each>
				<!-- Build Coverage Records -->
				<!-- Assign all State level coverages to each vehicle in that rate-state -->
				<xsl:variable name="VehicleNbr" select="ItemIdInfo/InsurerId"/>
				<xsl:for-each select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlCoverage[CoverageCd!='']">
				<xsl:if test="CoverageCd != 'PIP'">		<!--Issue 59878-->		
<!--Issue 66352 begin -->
					<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlCoverage[CoverageCd = 'UN']">
						<xsl:choose>
							<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/com.csc_VehicleUNInd = 'Y'">
<!--Issue 66352 end -->
					<xsl:call-template name="BuildCoverage">
						<xsl:with-param name="InsLine">CA</xsl:with-param>
						<xsl:with-param name="RateState" select="$RateState"/>
						<xsl:with-param name="UnitNbr" select="$VehicleNbr"></xsl:with-param>
					</xsl:call-template>
					<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlCoverage[CoverageCd = 'UN']">
						<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlCoverage/Limit[LimitAppliesToCd = 'UNDPD']/FormatCurrencyAmt/Amt != ''">
							<xsl:variable name="CovCode">UNPD</xsl:variable>
							<xsl:choose>
							<xsl:when test="$RateState = 'WA'">
							<xsl:call-template name="BuildCoverage">
								<xsl:with-param name="InsLine">CA</xsl:with-param>
								<xsl:with-param name="RateState" select="$RateState"/>
								<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
								<xsl:with-param name="UNPDInd">Y</xsl:with-param>
							</xsl:call-template>
							</xsl:when>
							</xsl:choose>
						</xsl:if>
					</xsl:if>
<!--Issue 66352 begin -->
							</xsl:when>
						</xsl:choose>
					</xsl:if>
				</xsl:if>	<!--Issue 59878-->
<!--Issue 66352 end -->
				</xsl:for-each>
				<!-- Vehicle Level Coverages -->
				<xsl:for-each select="CommlCoverage[CoverageCd != '']">
					<xsl:variable name="CovCd" select="./CoverageCd"/>
					<xsl:if test="$CovCd != 'CMP'">
						<xsl:if test="$CovCd != 'COL'">
							<xsl:if test="CoverageCd !='AMU'">
								<xsl:if test="$CovCd != 'ACM'">
									<xsl:if test="$CovCd != 'ACL'">
								<xsl:if test="$CovCd != 'LCM'">
									<!--<xsl:if test="$CovCd != 'LCO'">-->			<!--Issue 59878-->
									<xsl:if test="$CovCd != 'LCO' and $CovCd != 'SP'">	<!--Issue 59878-->
										<xsl:call-template name="BuildCoverage">
											<xsl:with-param name="InsLine">CA</xsl:with-param>
											<xsl:with-param name="RateState" select="$RateState"/>
											<xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"></xsl:with-param>
											<xsl:with-param name="IterationNbr" select="IterationNumber"></xsl:with-param>
										</xsl:call-template>
									</xsl:if>
								</xsl:if>
							</xsl:if>
						</xsl:if>
					</xsl:if>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
				<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlCoverage[CoverageCd = 'UM']">
<!--Issue 66352 begin -->
<!--Issue 66352 the original IF statement worked when the Primary State had UMPD, but when it doesn't offer UMPD and the other states on the policy do, the ASBY record is not being created for the other states.  Split this up and add the check for limit to each.-->							
			<xsl:if test="(/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlCoverage/Limit[LimitAppliesToCd = 'UMPD']/FormatCurrencyAmt/Amt != '' and /ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/com.csc_VehicleUMInd = 'Y' and /ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/com.csc_VehicleUMPDLimitCd != '') 
							or (/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/com.csc_VehicleUMInd = 'Y' and /ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/com.csc_VehicleUMPDLimitCd != '')">
<!--Issue 66352 end -->
					<xsl:variable name="CovCode" select="CoverageCd"/>
					<xsl:choose>
						<xsl:when test="$PrimaryState = 'AR' and $RateState = 'AR'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'NM' and $RateState = 'NM'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'IL' and $RateState = 'IL'">
						<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502:21102:01102</xsl:variable><!-- Issue 67226 --><!-- Issue 59878 added classcd -->
						<xsl:variable name="VehClassCd" select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/PrimaryClassCd"/>
						<xsl:if test="contains($VehClassCdArray, $VehClassCd)">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:if>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'LA' and $RateState = 'LA'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'TX' and $RateState = 'TX'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'UT' and $RateState = 'UT'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'OR' and $RateState = 'OR'">
						<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502</xsl:variable>
								<xsl:variable name="VehClassCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/PrimaryClassCd"/>
								<xsl:if test="contains($VehClassCdArray, $VehClassCd)">
									<xsl:call-template name="BuildCoverage">
										<xsl:with-param name="InsLine">CA</xsl:with-param>
										<xsl:with-param name="RateState" select="$RateState"/>
										<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
									</xsl:call-template>
								</xsl:if>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'CA' and $RateState = 'CA'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'IN' and $RateState = 'IN'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'GA' and $RateState = 'GA'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
<!-- Issue 67226 - START -->
						<xsl:when test="$PrimaryState = 'MS' and $RateState = 'MS'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'OH' and $RateState = 'OH'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
						<xsl:when test="$PrimaryState = 'TN' and $RateState = 'TN'">
						<xsl:call-template name="BuildCoverage">
							<xsl:with-param name="InsLine">CA</xsl:with-param>
							<xsl:with-param name="RateState" select="$RateState"/>
							<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
						</xsl:call-template>
						</xsl:when>
<!-- Issue 67226 - END -->
<!--Issue 66352 Begin -->
									<xsl:when test="($PrimaryState != 'AR' and $RateState = 'AR')
													or ($PrimaryState != 'CA' and $RateState = 'CA')
													or ($PrimaryState != 'GA' and $RateState = 'GA')
													or ($PrimaryState != 'IN' and $RateState = 'IN')
													or ($PrimaryState != 'LA' and $RateState = 'LA')
													or ($PrimaryState != 'MS' and $RateState = 'MS')
													or ($PrimaryState != 'NM' and $RateState = 'NM')
													or ($PrimaryState != 'OH' and $RateState = 'OH')
													or ($PrimaryState != 'TN' and $RateState = 'TN')
													or ($PrimaryState != 'TX' and $RateState = 'TX')
													or ($PrimaryState != 'UT' and $RateState = 'UT')">
										<xsl:call-template name="BuildCoverage">
											<xsl:with-param name="InsLine">CA</xsl:with-param>
											<xsl:with-param name="RateState" select="$RateState"/>
											<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
										</xsl:call-template>
									</xsl:when>
									
									<xsl:when test="$PrimaryState != 'IL' and $RateState = 'IL'">
										<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502:21102</xsl:variable>
										<xsl:variable name="VehClassCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/PrimaryClassCd"/>
										<xsl:if test="contains($VehClassCdArray, $VehClassCd)">
											<xsl:call-template name="BuildCoverage">
												<xsl:with-param name="InsLine">CA</xsl:with-param>
												<xsl:with-param name="RateState" select="$RateState"/>
												<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
											</xsl:call-template>
										</xsl:if>
									</xsl:when>									
									
									<xsl:when test="$PrimaryState != 'OR' and $RateState = 'OR'">
										<xsl:variable name="VehClassCdArray">7387:7381:7382:7383:7386:7388:7391:7392:7393:7394:7398:7399:7908:7911:9502</xsl:variable>
										<xsl:variable name="VehClassCd" select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState/CommlVeh[@id=concat('v',$VehicleNbr)]/CommlVehSupplement/PrimaryClassCd"/>
										<xsl:if test="contains($VehClassCdArray, $VehClassCd)">
											<xsl:call-template name="BuildCoverage">
												<xsl:with-param name="InsLine">CA</xsl:with-param>
												<xsl:with-param name="RateState" select="$RateState"/>
												<xsl:with-param name="UnitNbr" select="$VehicleNbr"/>
											</xsl:call-template>
										</xsl:if>
									</xsl:when>
<!--Issue 66352 End -->
					</xsl:choose>
				</xsl:if>
				</xsl:for-each>
<!-- Issue 67226 Start -->
				<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness/CommlRateState[StateProvCd=$RateState]/CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd != '']"> <!--Issue 66352 --> 
					<xsl:if test="CoverageCd = 'DUP'">
						<xsl:call-template name="BuildCoverage"> 
							<xsl:with-param name="InsLine">CA</xsl:with-param> 
							<xsl:with-param name="RateState" select="$RateState"/> 
							<xsl:with-param name="UnitNbr">0</xsl:with-param> 
						</xsl:call-template> 
					</xsl:if> 
				</xsl:for-each>
				<xsl:for-each select="AdditionalInterest[AdditionalInterestInfo/NatureInterestCd='BP']">
					<xsl:if test="string-length(GeneralPartyInfo/NameInfo/CommlName/CommercialName)">
						<xsl:call-template name="CreateNIInfo">
							<xsl:with-param name="UnitNbr" select="../ItemIdInfo/InsurerId"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:for-each>
 <!-- Issue 67226 End -->
			</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="/ACORD/InsuranceSvcRq/com.csc_CommlPkgPolicyQuoteInqRq/CommlAutoLineBusiness">
			<xsl:call-template name="CoveredAutoSymbol"/>
		</xsl:for-each>
	</xsl:template>
	<!-- Issue 67226 End : Added CommlAuto Section to Add Rq-->
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\requests\46841280668618584375CPQuoteInqRq.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
--><!-- Stylus Studio meta-information - (c) 2004-2006. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\NETSYS\APPSERVER\Log Files\74321280682081731250CPQuoteInqRq.xml" htmlbaseurl="" outputurl="" processortype="internal" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator=""/></scenarios><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext><MapperFilter side="source"></MapperFilter></MapperMetaTag>
</metaInformation>
-->