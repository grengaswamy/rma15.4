<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	
	<xsl:template name="CreateGeneralLiability">
		<com.csc_GeneralLiabilityLineBusiness>
		    <LOBCd>CGL</LOBCd>
            <CurrentTermAmt>
                <Amt><xsl:value-of select="BBA3VA"/></Amt>
            </CurrentTermAmt>
<!-- Insurance Line Limits -->
		    <LiabilityInfo>
<!-- Each Occurance -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">EAOCC</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBUSVA3"/></xsl:with-param>
		        </xsl:call-template>
<!-- General Aggregate -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">GENAG</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBUSVA5"/></xsl:with-param>
		        </xsl:call-template>
<!-- Personal Injury in Advertising -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">PIADV</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBC2VA"/></xsl:with-param>
		        </xsl:call-template>
<!-- Prod Co -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">PRDCO</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBUSVA4"/></xsl:with-param>
		        </xsl:call-template>
<!-- Liquor Liability Occurance -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">com.csc_LLOCC</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBCYVA"/></xsl:with-param>
		        </xsl:call-template>
<!-- Liquor Liability Aggregate -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">com.csc_LLAGG</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBCZVA"/></xsl:with-param>
		        </xsl:call-template>
<!-- Employee Benefits Limit Occurance -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">com.csc_EBLOCC</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBC0VA"/></xsl:with-param>
		        </xsl:call-template>
<!-- Employee Benefits Limit Aggregate -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">com.csc_EBLAGG</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBC1VA"/></xsl:with-param>
		        </xsl:call-template>
<!-- Fire Damage -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">FIRDM</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBC3VA"/></xsl:with-param>
		        </xsl:call-template>
<!-- Medical Expenses -->
		        <xsl:call-template name="BuildGLCommlCoverage">
		            <xsl:with-param name="Name">MEDEX</xsl:with-param>
		            <xsl:with-param name="LimitAmt"><xsl:value-of select="BBCXVA"/></xsl:with-param>
		        </xsl:call-template>
<!-- Build GL Insurance Line Classification Aggregates -->
                <!-- Issue 59878 - Start -->
				<!--xsl:for-each select="/*/ASBYCPL1__RECORD[BYAGTX='GL']"-->
				<xsl:for-each select="key('ILKey', 'GL')">
				<!-- Issue 59878 - Start -->
<!-- Ignore Insurance Line Minimum Premiums for now -->
<xsl:sort select="BYBRNB"/><!--Issue 102807 -->
                    <xsl:variable name="IsInsMinPremCov">
                        <xsl:call-template name="MinPremLookup">
                            <xsl:with-param name="InsLine">GL</xsl:with-param>
                        </xsl:call-template>
                    </xsl:variable>
                    <xsl:if test="$IsInsMinPremCov='N'">
	     <xsl:variable name="Location" select="BYBRNB"/>
	    <xsl:variable name="CovCode" select="normalize-space(BYAOTX)"/>
        <xsl:variable name="ACORDCovCode">
            <xsl:call-template name="TranslateCoverageCode">
                <xsl:with-param name="InsLine">GL</xsl:with-param>
                <xsl:with-param name="CovCode" select="$CovCode"/>
            </xsl:call-template>
        </xsl:variable>
         <!--Issue 102807 starts-->
            <xsl:variable name="GetForm">
            <xsl:if test="not(preceding-sibling::ASBYCPL1__RECORD[BYBRNB=$Location and BYAGTX='GL'])">
                   <xsl:text>1</xsl:text>
           </xsl:if>
           </xsl:variable>    
           <!--Issue 102807 ends-->    
        <GeneralLiabilityClassification>
            <xsl:attribute name="LocationRef">l<xsl:value-of select="$Location"/></xsl:attribute>
			<SublineCd><xsl:value-of select="BYAKCD"/></SublineCd>
		    <CommlCoverage>		    
			    <!-- Issue 59878 - Start -->
				<!--CoverageCd><xsl:value-of select="$ACORDCovCode"/></CoverageCd>
				<CoverageDesc/-->
				<CoverageCd>
				<xsl:choose>
				    <xsl:when test="$CovCode='ADDINS'">
					<xsl:value-of select="BYI4TX"/>
				    </xsl:when>
				    <xsl:otherwise>	
					<xsl:value-of select="$ACORDCovCode"/>
				    </xsl:otherwise>
				</xsl:choose>	
			    </CoverageCd>
				<xsl:choose>
				    <xsl:when test="$CovCode='AIBLT'">
						<CoverageDesc><xsl:value-of select="BYUSIN30"/></CoverageDesc>
					</xsl:when>
				    <xsl:when test="$CovCode='PROF'">
						<CoverageDesc><xsl:value-of select="BYCZST"/></CoverageDesc>
					</xsl:when>
					<xsl:otherwise>	
						<CoverageDesc/>
					</xsl:otherwise>
					<!-- Issue 59878 - End -->
				</xsl:choose>	
				<IterationNumber><xsl:value-of select="BYC0NB"/></IterationNumber>
				<xsl:choose>
				    <xsl:when test="$CovCode='EBL'">
        				<Limit>
        				    <FormatCurrencyAmt>
								<!-- Issue 59878 - Start -->
        					    <!--Amt><xsl:value-of select="BYAGVA"/></Amt-->
								<Amt><xsl:value-of select="../ASBBCPL1__RECORD[BBAGTX = 'GL']/BBC0VA"/></Amt>
								<!-- Issue 59878 - End -->
        				    </FormatCurrencyAmt>
        				    <LimitAppliesToCd>PerOcc</LimitAppliesToCd>
        				</Limit>
        				<Limit>
        				    <FormatCurrencyAmt>
								<!-- Issue 59878 - Start -->
        					    <!--Amt><xsl:value-of select="BYAHVA"/></Amt-->
								<Amt><xsl:value-of select="../ASBBCPL1__RECORD[BBAGTX = 'GL']/BBC0VA"/></Amt>
								<!-- Issue 59878 - End -->
        				    </FormatCurrencyAmt>
        				    <LimitAppliesToCd>Aggregate</LimitAppliesToCd>
        				</Limit>
				    </xsl:when>
					<!-- Issue - Start -->
					<xsl:when test="$CovCode='PREMOP' or $CovCode = 'PRODCO' or $CovCode = 'LIQ'">
        				<Limit>
        				    <FormatCurrencyAmt>
        					    <Amt><xsl:value-of select="../ASBBCPL1__RECORD[BBAGTX = 'GL']/BBUSVA3"/></Amt>	<!--UNI00042J-->	<!--UNI00042K-->
        				    </FormatCurrencyAmt>
        				    <LimitAppliesToCd></LimitAppliesToCd>
        				</Limit>
  				    </xsl:when>
					<xsl:when test="$CovCode='OCP'">
        				<Limit>
        				    <FormatCurrencyAmt>
        					    <Amt><xsl:value-of select="BYA5VA"/></Amt>
        				    </FormatCurrencyAmt>
        				    <LimitAppliesToCd></LimitAppliesToCd>
        				</Limit>
  				    </xsl:when>
					<!-- Issue 59878 - End -->
				    <xsl:otherwise>
        				<Limit>
        				    <FormatCurrencyAmt>
								<!-- Issue 59878 - Start -->
        					    <!--Amt><xsl:value-of select="BYAHVA"/></Amt-->
								<Amt><xsl:value-of select="BYUSVA3"/></Amt>
								<!-- Issue 59878 - End -->
        				    </FormatCurrencyAmt>
        				    <LimitAppliesToCd/>
        				</Limit>
				    </xsl:otherwise>
				</xsl:choose>
				<!-- Issue 59878 - Start -->
				<!--Deductible>
				    <FormatCurrencyAmt>
					    <Amt><xsl:value-of select="BYUSCD5"/></Amt>
				    </FormatCurrencyAmt>
				    <DeductibleTypeCd/>
				</Deductible-->
				<xsl:choose>
					<xsl:when test="$CovCode = 'PRODCO'">
						<xsl:variable name="BYUSCD5">
							<xsl:choose>
								<xsl:when test="normalize-space(BYUSCD5) != ''">
									<xsl:value-of select="preceding-sibling::ASBYCPL1__RECORD[1]/BYUSCD5"/>  
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise> 
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="BYPNTX">
							<xsl:choose>
								<xsl:when test="normalize-space(BYPNTX) != ''">
									<xsl:value-of select="BYPNTX"/> 
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise> 
							</xsl:choose>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$BYUSCD5 &gt; 0 and $BYPNTX &gt; 0 ">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYPNTX"/> BI/<xsl:value-of select="$BYUSCD5"/> PD</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:when test="$BYUSCD5 &gt; $BYPNTX">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYUSCD5"/> PD</Amt>
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:when test="$BYPNTX &gt; $BYUSCD5">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYPNTX"/> BI</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:otherwise>
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYUSCD5"/></Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:otherwise> 
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$CovCode = 'PREMOP'">
						<xsl:variable name="BYANCD">
							<xsl:choose>
								<xsl:when test="normalize-space(BYANCD) != ''">
									<xsl:value-of select="BYANCD"/> 
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise> 
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="BYPPTX">
							<xsl:choose>
								<xsl:when test="normalize-space(BYPPTX) != ''">
									<xsl:value-of select="BYPPTX"/> 
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise> 
							</xsl:choose>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$BYANCD &gt; 0 and $BYPPTX &gt; 0 ">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYPPTX"/> BI/<xsl:value-of select="$BYANCD"/> PD</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:when test="$BYANCD &gt; $BYPPTX ">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYANCD"/> PD</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:when test="$BYPPTX &gt; $BYANCD">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYPPTX"/> BI</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:otherwise>
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYANCD"/></Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:otherwise> 
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$CovCode = 'CTPREM'">
						<xsl:variable name="BYANCD">
							<xsl:choose>
								<xsl:when test="normalize-space(BYANCD) != ''">
									<xsl:value-of select="BYANCD"/> 
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise> 
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="BYPPTX">
							<xsl:choose>
								<xsl:when test="normalize-space(BYPPTX) != ''">
									<xsl:value-of select="BYPPTX"/> 
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise> 
							</xsl:choose>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$BYANCD &gt; 0 and $BYPPTX &gt; 0 ">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYPPTX"/> BI/<xsl:value-of select="$BYANCD"/> PD</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:when test="$BYANCD &gt; $BYPPTX ">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYANCD"/> PD</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:when test="$BYPPTX &gt; $BYANCD">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYPPTX"/> BI</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:otherwise>
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYANCD"/></Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:otherwise> 
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$CovCode = 'CTPROD'">
						<xsl:variable name="BYUSCD5">
							<xsl:choose>
								<xsl:when test="normalize-space(BYUSCD5) != ''">
									<xsl:value-of select="following-sibling::ASBYCPL1__RECORD[BYAOTX = 'PREMOP']/BYUSCD5"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise> 
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="BYPNTX">
							<xsl:choose>
								<xsl:when test="normalize-space(BYPNTX) != ''">
									<xsl:value-of select="BYPNTX"/> 
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise> 
							</xsl:choose>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$BYUSCD5 &gt; 0 and $BYPNTX &gt; 0 ">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYPNTX"/> BI/<xsl:value-of select="$BYUSCD5"/> PD</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:when test="$BYUSCD5 &gt; $BYPNTX">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYUSCD5"/> PD</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:when test="$BYPNTX &gt; $BYUSCD5">
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYPNTX"/> BI</Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:when>
							<xsl:otherwise>
								<Deductible>
								    <FormatCurrencyAmt>
									    <Amt><xsl:value-of select="$BYUSCD5"/></Amt>	
								    </FormatCurrencyAmt>
								    <DeductibleTypeCd/>
								</Deductible>
							</xsl:otherwise> 
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<Deductible>
						    <FormatCurrencyAmt>
							    <Amt><xsl:value-of select="BYPPTX"/></Amt> <!--UNI00042J-->
						    </FormatCurrencyAmt>
						    <DeductibleTypeCd/>
						</Deductible>
					</xsl:otherwise>
				</xsl:choose>
				<!-- Issue 59878 - End -->
				<Option>
				    <OptionTypeCd/> 
				    <OptionCd/>
				    <OptionValue/>
				</Option>
				<CurrentTermAmt>
				    <Amt><xsl:value-of select="BYA3VA"/></Amt>
				    <com.csc_OverrideInd>
				        <xsl:choose>
				            <xsl:when test="BYD3ST='0'">1</xsl:when>
				            <xsl:otherwise>0</xsl:otherwise>
				        </xsl:choose>
				    </com.csc_OverrideInd>
				</CurrentTermAmt>
			</CommlCoverage>
 		    <ClassCd><xsl:value-of select="BYNDTX"/></ClassCd>
		    <ClassCdDesc><xsl:value-of select="BYKKTX"/></ClassCdDesc>
		    <Exposure><xsl:value-of select="BYA5VA"/></Exposure>
		    <TerritoryCd><xsl:value-of select="BYAGNB"/></TerritoryCd>
		    <PremiumBasisCd><xsl:value-of select="BYIZTX"/></PremiumBasisCd>
	        <com.csc_PMACd><xsl:value-of select="BYAMCD"/></com.csc_PMACd>
	        <com.csc_ExposureBasisCd><xsl:value-of select="BYC2ST"/></com.csc_ExposureBasisCd>
	        <com.csc_FinalRatePct><xsl:value-of select="BYB5PC"/></com.csc_FinalRatePct>	<!-- Issue 59878 - Start -->
	        <com.csc_BaseRate/>
	        <com.csc_NumVendors/>
	        <com.csc_VendorPremiumAmt/>
	        <com.csc_DeletedExclusionsCd/>
	        <com.csc_IncreasedLimitTableAssignmentCd/>
			<com.csc_CoverageTypeCd/>
		<!--Issue 102807 starts-->
	     		<xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='GL' and BEBRNB =$Location]">
        	               <xsl:if test="$GetForm = '1'">
								    <xsl:call-template name="BuildCPPForm"/>
	    					 </xsl:if>
              </xsl:for-each>
		     <!--Issue 102807 ends-->
		</GeneralLiabilityClassification>
     </xsl:if>        
    </xsl:for-each>
    <!--Issue 102807 starts-->
	  <xsl:for-each select="/*/ASBECPL1__RECORD[BEAGTX='GL'and BEBRNB='0']">
	  		<xsl:call-template name="BuildCPPForm" />			
    	</xsl:for-each>	
 <!--Issue 102807 ends-->	
 </LiabilityInfo>    			
<!-- Now generate the Insurance Line Minimum Premium aggregate -->
<!-- Issue 59878 - Start -->
<!--xsl:variable name="InsLineMinPrem" select="sum(//ASBYCPL1__RECORD[BYAGTX='GL' and BYBRNB='0' and substring(BYKKTX,1,3)='MIN']/BYA3VA)"/>
  <xsl:if test="$InsLineMinPrem &gt; 0">
   <CreditOrSurcharge>
        <CreditSurchargeDt/>
        <CreditSurchargeCd>APMP</CreditSurchargeCd>
        <NumericValue>
          <FormatCurrencyAmt>
               <Amt><xsl:value-of select="format-number($InsLineMinPrem,'###,###,###.00')"/></Amt>
          </FormatCurrencyAmt>
         </NumericValue>
   </CreditOrSurcharge>
  </xsl:if-->  			
  <xsl:for-each select="/*/ASBYCPL1__RECORD[BYAGTX='GL' and BYBRNB='0' and substring(BYKKTX,1,3)='MIN']/BYA3VA"> <!--UNITRIN 648A?? code not marked-->
  <CreditOrSurcharge>
        <CreditSurchargeDt/>
        <CreditSurchargeCd><xsl:value-of select="../BYAOTX"/></CreditSurchargeCd>
        <NumericValue>
          <FormatCurrencyAmt>
               <Amt><xsl:value-of select="format-number(../BYA3VA,'###,###,###.00')"/></Amt>
          </FormatCurrencyAmt>
         </NumericValue>
   </CreditOrSurcharge>
  </xsl:for-each>
<!-- Issue 59878 - End -->
<!-- Build Modification Factors -->
			<!-- Issue 59878 - Start -->
            <!--xsl:for-each select="/*/ASBCCPL1__RECORD[BCAGTX='GL']"-->
			<xsl:for-each select="key('ILKey', 'GL')">
			<!-- Issue 59878 - End -->
    		    <com.csc_ModificationFactors>
        			<StateProvCd><xsl:value-of select="BCADCD"/>	</StateProvCd>
<!-- Experience Modifier -->
        			<xsl:call-template name="BuildModFactor">
         			    <xsl:with-param name="Code">EXP</xsl:with-param>
         			    <xsl:with-param name="FactorField"><xsl:value-of select="BCAMPC"/></xsl:with-param>
        			</xsl:call-template>
<!-- Schedule Modifier -->
        			<xsl:call-template name="BuildModFactor">
        			    <xsl:with-param name="Code">SCH</xsl:with-param>
        			    <xsl:with-param name="FactorField"><xsl:value-of select="BCBLPC"/></xsl:with-param>
        			</xsl:call-template>
<!-- IRPM Modifier -->
        			<xsl:call-template name="BuildModFactor">
        			    <xsl:with-param name="Code">IRPM</xsl:with-param>
        			    <xsl:with-param name="FactorField"><xsl:value-of select="BCANPC"/></xsl:with-param>
        			</xsl:call-template>
    		    </com.csc_ModificationFactors>
    		</xsl:for-each>
<!-- Build Forms --><!-- ICH00284B -->
	       
			<!--Issue 102807 commented out startrs-->
	       <!-- <xsl:for-each select="/*/ASBECPP__RECORD[BEAGTX='GL']">
		          <xsl:call-template name="BuildCPPForm" />
		          </xsl:for-each>-->
		<!--Issue 102807 commented out end-->
		</com.csc_GeneralLiabilityLineBusiness>
	</xsl:template>
	
	<xsl:template name="BuildGLCommlCoverage">
        <xsl:param name="Name"/>
        <xsl:param name="LimitAmt"/>
	    <CommlCoverage>
			<CoverageCd><xsl:value-of select="$Name"/></CoverageCd>
			<CoverageDesc/>
			<Limit>
				<FormatCurrencyAmt>
				    <Amt><xsl:value-of select="$LimitAmt"/></Amt>
				</FormatCurrencyAmt>
				<LimitAppliesToCd/>
			</Limit>
			<CurrentTermAmt>
				    <Amt/>
				    <com.csc_OverrideInd/>
			</CurrentTermAmt>
	    </CommlCoverage>
	</xsl:template>
	
</xsl:stylesheet>

