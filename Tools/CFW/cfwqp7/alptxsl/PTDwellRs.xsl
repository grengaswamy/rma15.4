<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:template name="CreateDwell">
		<xsl:param name="LOB">HP</xsl:param>
		<!-- 55614 begin-->
<!-- 39111	 Begin	
		<xsl:if test="$LOB='FP' and BQABCD = '28'">
			<Coverage>
				<CoverageCd>ANICL</CoverageCd>
				<Limit>
					<FormatCurrencyAmt>
						<Amt>25000.00</Amt>
					</FormatCurrencyAmt>
				</Limit>
				<CurrentTermAmt>
					<Amt/>
				</CurrentTermAmt>
			</Coverage>
		</xsl:if>
		<xsl:if test="$LOB='FP'">
			<xsl:choose>
				<xsl:when test="/*/ASC4CPL1__RECORD[C4AOTX = 'MOLD']"/>
				<xsl:otherwise>
					<Coverage>
						<CoverageCd>MOLD</CoverageCd>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>10000.00</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd>$AMT</LimitAppliesToCd>
						</Limit>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>50000.00</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd>$AGGR</LimitAppliesToCd>
						</Limit>
						<CurrentTermAmt>
							<Amt>0.00</Amt>
						</CurrentTermAmt>
					</Coverage>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
39111 -->
		<!-- 55614 end-->
		<Dwell>
		   <xsl:attribute name="id"><xsl:value-of select="concat($KEY,'-dwell',BQEGNB)"/></xsl:attribute>
			<PolicyTypeCd>
				<xsl:value-of select="BQHXTX"/>
			</PolicyTypeCd>
			<!-- 34770 starts added new case 37020--><!--<PurchaseDt><xsl:value-of select="BQAEDT"/></PurchaseDt>-->
			<PurchaseDt>
			  <!--   55614
				<xsl:variable name="PurchageDate" select="BQAEDT"/>
				<xsl:if test="$PurchageDate !='0'">
					<xsl:call-template name="ConvertPTDateToISODate">
						<xsl:with-param name="Value" select="$PurchageDate"/>
					</xsl:call-template>
				</xsl:if>
				-->
					<xsl:if test="BQAEDT != '0'">
					<xsl:call-template name="ConvertPTDateToISODate">
						<xsl:with-param name="Value" select="BQAEDT"/>
					</xsl:call-template>
				</xsl:if>
			</PurchaseDt><!-- 34770 Ends  case 37020-->
			<PurchasePriceAmt>
				<Amt><xsl:value-of select="BQBUVA"/></Amt>
			</PurchasePriceAmt>
			<FloorUnitLocated/>
			<AreaTypeSurroundingsCd><xsl:value-of select="BQM1TX"/></AreaTypeSurroundingsCd>
			<Construction>
				<ConstructionCd><xsl:value-of select="BQHYTX"/></ConstructionCd>
				<InsurerConstructionClassCd><xsl:value-of select="BQCZST"/></InsurerConstructionClassCd>            <!-- 55614 -->
				<YearBuilt><xsl:value-of select="BQDLNB"/></YearBuilt>
				<NumStories/>
				<RoofingMaterial><!-- 34770 start Case 37069 --><!--  <RoofMaterialCd>BQDBNB</RoofMaterialCd>-->
					<RoofMaterialCd><!-- 55614 <xsl:value-of select="BQDBNB"/>--><!-- 34770 Ends  Case 37069-->
					<xsl:choose>
							<xsl:when test="substring(BQAMCD,5,1) = ''">
								<xsl:value-of select="BQDBNB"/>
							</xsl:when>	
							<xsl:otherwise>	
							   <xsl:value-of select="substring(BQAMCD,5,1)"/>
							</xsl:otherwise>
						</xsl:choose>	
					</RoofMaterialCd>
				</RoofingMaterial>
				<!-- 55614 begin-->
				<!--<com.csc_ContructSuperiorInd><xsl:value-of select="BQCZST"/></com.csc_ContructSuperiorInd>
				<com.csc_TownRowHouseInd><xsl:value-of select="BQBNCD"/></com.csc_TownRowHouseInd>-->
				<!-- 55614 end-->
			</Construction>
			<DwellOccupancy>
				<NumRooms/>
				<NumApartments><!-- 34770 start Case 37021 -->
					<xsl:variable name="NumApartments" select="BQBNCD"/>
					<xsl:choose>
						<xsl:when test="$NumApartments ='N'">0</xsl:when>
						<xsl:when test="$NumApartments !='N'">
							<xsl:value-of select="$NumApartments"/>
						</xsl:when>
					</xsl:choose><!--<xsl:if test="$NumApartments ='N'">0</xsl:if> --><!-- 34770 Ends  Case 37021-->
				</NumApartments>
				<!-- 55614 begin-->
				<DwellUseCd>
					<xsl:choose>
						<xsl:when test="$LOB = 'HP'">
							<xsl:value-of select="BQIYTX"/>
						</xsl:when>
						<xsl:when test="$LOB = 'FP'">
							<xsl:value-of select="BQI0TX"/>
						</xsl:when>
					</xsl:choose>
				</DwellUseCd>
				<!-- 55614 end-->
				<OccupancyTypeCd><xsl:value-of select="BQH0TX"/></OccupancyTypeCd>
			</DwellOccupancy>
			<DwellRating>
				<TerritoryCd><xsl:value-of select="BQAGNB"/></TerritoryCd>
			</DwellRating>
			<BldgProtection>
				<NumUnitsInFireDivisions/>
				<FireProtectionClassCd><xsl:value-of select="BQH1TX"/></FireProtectionClassCd>
				<DistanceToFireStation>
					<NumUnits><!-- 55614 <xsl:value-of select="BQDINB"/>--> 
					<!-- 39111 Start <xsl:value-of select="/*/JUAPPD01__RECORD/CDDISTFIRE"/></NumUnits> 39111 End-->
					<!-- 39111 Start -->
					<xsl:choose>
						<xsl:when test="/*/JUAPPD01__RECORD">
							<xsl:value-of select="/*/JUAPPD01__RECORD/CDDISTHYDR"/>
						</xsl:when>	
						<xsl:otherwise>
							<xsl:value-of select="BQDINB"/>
						</xsl:otherwise>
					</xsl:choose>
					</NumUnits>
					<!-- 39111 End -->
					<UnitMeasurementCd>MI</UnitMeasurementCd>
				</DistanceToFireStation>
				<DistanceToHydrant>
					<NumUnits><!-- 55614 <xsl:value-of select="BQDHNB"/>--><!--xsl:value-of select="/*/JUAPPD01__RECORD/CDDISTHYDR"/></NumUnits-->
					<!-- 39111 Start -->
					<xsl:choose>
						<xsl:when test="/*/JUAPPD01__RECORD">
							<xsl:value-of select="/*/JUAPPD01__RECORD/CDDISTHYDR"/>
						</xsl:when>	
						<xsl:otherwise>
							<xsl:value-of select="BQDHNB"/>
						</xsl:otherwise>
					</xsl:choose>
					</NumUnits>
					<!-- 39111 End -->
					<UnitMeasurementCd>FT</UnitMeasurementCd>
				</DistanceToHydrant>
				<!--   55614 begin -->
				<!--
				<ProtectionDeviceBurglarCd><xsl:value-of select="BQBLCD"/></ProtectionDeviceBurglarCd>
	          <ProtectionDeviceFireCd/> 
	          <ProtectionDeviceSmokeCd/>
             <ProtectionDeviceSprinklerCd/> 
              -->    
              <!--55614 changes for HO endorsement issue starts-->
				<!--ProtectionDeviceBurglarCd><xsl:value-of select="BQI3TX"/></ProtectionDeviceBurglarCd>
				<ProtectionDeviceFireCd><xsl:value-of select="BQBLCD"/></ProtectionDeviceFireCd-->
				<ProtectionDeviceBurglarCd><xsl:value-of select="BQBLCD"/></ProtectionDeviceBurglarCd>
				<ProtectionDeviceFireCd></ProtectionDeviceFireCd>
				<!--55614 changes for HO endorsement issue ends-->
				<ProtectionDeviceSmokeCd/>
				<ProtectionDeviceSprinklerCd><xsl:value-of select="BQI4TX"/></ProtectionDeviceSprinklerCd>
				<!-- 55614 end -->
			</BldgProtection>
			<BldgImprovements>
			<!-- 55614 begin -->
			<!--
			    <ExteriorPaintYear/>
	          <HeatingImprovementCd/>
	          <HeatingImprovementYear/>
	          <PlumbingImprovementCd/>
	          <PlumbingImprovementYear/>
	          <RoofingImprovementCd/>
	          <RoofingImprovementYear/>
	          <WiringImprovementCd/>
	          <WiringImprovementYear/>
				<com.csc_HomeUpdatedInd><xsl:value-of select="BQIZTX"/></com.csc_HomeUpdatedInd>-->
			<!-- 55614 end -->	
			</BldgImprovements>
			<DwellInspectionValuation>
				<EstimatedReplCostAmt>
					<Amt/>
				</EstimatedReplCostAmt>
				<HeatSourcePrimaryCd><!--  55614 --><xsl:value-of select="/*/JUAPPD01__RECORD/CDHEATSRC"/></HeatSourcePrimaryCd>
				<NumFamilies><xsl:value-of select="BQDGNB"/></NumFamilies>
				<TotalArea>
					<NumUnits/>
					<UnitMeasurementCd>FT</UnitMeasurementCd>
				</TotalArea>
				<MarketValueAmt>
					<Amt/>
				</MarketValueAmt>
			</DwellInspectionValuation>
			<SwimmingPool>
				<AboveGroundInd/>
				<ApprovedFenceInd/>
			</SwimmingPool>
			<!-- 55614 begin -->
			<!--
			<Coverage>
	          <CoverageCd>DWELL</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt><xsl:value-of select="BQA6VA"/></Amt>
	             </FormatCurrencyAmt>
	          </Limit>
	          <Deductible>
	             <FormatCurrencyAmt>
		             <Amt><xsl:value-of select="BQA9NB"/></Amt>
	             </FormatCurrencyAmt> 
	             <DeductibleTypeCd><xsl:value-of select="BQBRCD"/></DeductibleTypeCd>
                <DeductibleAppliesToCd>AllPeril</DeductibleAppliesToCd>    
	          </Deductible>
	          <CurrentTermAmt>
	             <Amt><xsl:value-of select="BQA7VA"/></Amt>
	          </CurrentTermAmt> 
	       </Coverage>
	       <Coverage>
	          <CoverageCd>OS</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt><xsl:value-of select="BQA8VA"/></Amt>
	             </FormatCurrencyAmt>
	          </Limit>
	          <CurrentTermAmt>
	             <Amt><xsl:value-of select="BQA9VA"/></Amt>
	          </CurrentTermAmt> 
	       </Coverage>
	       <Coverage>
	          <CoverageCd>PP</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt><xsl:value-of select="BQBAVA"/></Amt>
	             </FormatCurrencyAmt>
	          </Limit>
	          <CurrentTermAmt>
	             <Amt><xsl:value-of select="BQBBVA"/></Amt>
	          </CurrentTermAmt> 
	       </Coverage>
	       <Coverage>
	          <CoverageCd>LOU</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt><xsl:value-of select="BQBCVA"/></Amt>
	             </FormatCurrencyAmt>
	          </Limit>
	          <CurrentTermAmt>
	             <Amt><xsl:value-of select="BQBDVA"/></Amt>
	          </CurrentTermAmt> 
	       </Coverage>
	       <Coverage>
	          <CoverageCd>PL</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt><xsl:value-of select="BQBEVA"/></Amt>
	             </FormatCurrencyAmt>
	          </Limit>
	          <CurrentTermAmt>
	             <Amt><xsl:value-of select="BQBFVA"/></Amt>
	          </CurrentTermAmt> 
	       </Coverage>
	       <Coverage>
	          <CoverageCd>MEDPM</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt><xsl:value-of select="BQBGVA"/></Amt>
	             </FormatCurrencyAmt>
	          </Limit>
	          <CurrentTermAmt>
	             <Amt><xsl:value-of select="BQBHVA"/></Amt>
	          </CurrentTermAmt> 
	       </Coverage>
		   <Coverage>
	          <CoverageCd>FVREP</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt/>
	             </FormatCurrencyAmt>
	          </Limit>
			  <Option>
				<OptionValue><xsl:value-of select="BQHZTX"/></OptionValue>
			  </Option>
	          <CurrentTermAmt>
	             <Amt/>
	          </CurrentTermAmt> 
	       </Coverage>
		   <Coverage>
	          <CoverageCd>INFGD</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt/>
	             </FormatCurrencyAmt>
	          </Limit>
			  <Option>
				<OptionValue><xsl:value-of select="BQAWPC"/></OptionValue>
			  </Option>
	          <CurrentTermAmt>
	             <Amt/>
	          </CurrentTermAmt> 
	       </Coverage>
		   		   <Coverage>
	          <CoverageCd>WINDX</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt/>
	             </FormatCurrencyAmt>
	          </Limit>
			  <Option>
				<OptionValue><xsl:value-of select="BQBPCD"/></OptionValue>
			  </Option>
	          <CurrentTermAmt>
	             <Amt/>
	          </CurrentTermAmt> 
	       </Coverage>
		   <Coverage>
	          <CoverageCd>com.csc_VALUP</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt/>
	             </FormatCurrencyAmt>
	          </Limit>
			  <Option>
				<OptionValue><xsl:value-of select="BQJBTX"/></OptionValue>			
			  </Option>
	          <CurrentTermAmt>
	             <Amt/>
	          </CurrentTermAmt> 
	       </Coverage>
		    Case 37019 Starts for FD02 34770
		   <Coverage>
	          <CoverageCd>com.csc_TheftDed</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt/>
	             </FormatCurrencyAmt>
	          </Limit>
			  <Option>
				<OptionValue><xsl:value-of select="BQBOCD"/></OptionValue>
			  </Option>
	          <CurrentTermAmt>
	             <Amt/>
	          </CurrentTermAmt> 
	       </Coverage>
		  Case 37019 Ends for FD02 34770
		  Case XXXX Starts for FD02 34770
		   <Coverage>
	          <CoverageCd>WNDSD</CoverageCd>
	          <Limit>
	             <FormatCurrencyAmt>
		        <Amt/>
	             </FormatCurrencyAmt>
	          </Limit>
			  <Option>
				<OptionValue><xsl:value-of select="BQCZST"/></OptionValue>
			  </Option>
	          <CurrentTermAmt>
	             <Amt/>
	          </CurrentTermAmt> 
	       </Coverage>
		  Case XXXX Ends for FD02 34770
     </Dwell>
     -->
			<xsl:if test="$LOB='HP'">
			<Coverage>
				<CoverageCd>DWELL</CoverageCd>
				<Limit>
					<FormatCurrencyAmt>
						<Amt>
							<xsl:value-of select="BQA6VA"/>
						</Amt>
					</FormatCurrencyAmt>
				</Limit>
				<Deductible>
					<FormatCurrencyAmt>
						<Amt>
							<xsl:value-of select="BQA9NB"/>
						</Amt>
					</FormatCurrencyAmt>
					<DeductibleTypeCd>
						<xsl:value-of select="BQBRCD"/>
					</DeductibleTypeCd>
					<DeductibleAppliesToCd>AllPeril</DeductibleAppliesToCd>
				</Deductible>
				<CurrentTermAmt>
					<Amt>
						<xsl:value-of select="BQA7VA"/>
					</Amt>
				</CurrentTermAmt>
			</Coverage>
				<Coverage>
					<CoverageCd>OS</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="BQA8VA"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="BQA9VA"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>PP</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="BQBAVA"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="BQBBVA"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>LOU</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="BQBCVA"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="BQBDVA"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>PL</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="BQBEVA"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="BQBFVA"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>MEDPM</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="BQBGVA"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="BQBHVA"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<xsl:if test="BQHZTX='Y'">
				<Coverage>
					<CoverageCd>FVREP</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				</xsl:if>
				<xsl:if test="BQAWPC >'0'">
				<Coverage>
					<CoverageCd>INFGD</CoverageCd>
					<Limit>
						<FormatPct>
							<xsl:value-of select="BQAWPC"/>
						</FormatPct>
					</Limit>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				</xsl:if>
				<xsl:if test="BQBPCD='Y'">
				<Coverage>
					<CoverageCd>WINDX</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				</xsl:if>
				<Coverage>
					<CoverageCd>VALUP</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>THEFTDED</CoverageCd>
					<Option>
						<OptionTypeCd>Ind1</OptionTypeCd>
						<OptionValue>
							<xsl:value-of select="BQC0ST"/>
						</OptionValue>
					</Option>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				<xsl:if test="BQM3TX='Y'">
				<Coverage>
					<CoverageCd>FLOOD</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				</xsl:if>
				<xsl:if test="BQC1ST='Y'">
				<Coverage>
					<CoverageCd>BLDRK</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				</xsl:if>
				<Coverage>
					<CoverageCd>HURR</CoverageCd>
					<Deductible>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="BQB1VA"/>
							</Amt>
						</FormatCurrencyAmt>
						<DeductibleTypeCd><xsl:value-of select="BQBRCD"/></DeductibleTypeCd>
						<DeductibleAppliesToCd>AllPeril</DeductibleAppliesToCd>
					</Deductible>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="BQB2VA"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<xsl:if test="BQHZTX='Y'">
				<Coverage>
					<CoverageCd>PPREP</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				</xsl:if>
			</xsl:if>
			<xsl:if test="$LOB='FP'">
			<Coverage>
				<CoverageCd>DWELL</CoverageCd>
				<Limit>
					<FormatCurrencyAmt>
						<Amt>
							<xsl:value-of select="BQA6VA"/>
						</Amt>
					</FormatCurrencyAmt>
				</Limit>
				<CurrentTermAmt>
					<Amt>
						<xsl:value-of select="BQA7VA"/>
					</Amt>
				</CurrentTermAmt>
				<Deductible>
					<FormatCurrencyAmt>
						<Amt>
							<xsl:value-of select="BQA9NB"/>
						</Amt>
					</FormatCurrencyAmt>
					<DeductibleTypeCd>
						<xsl:value-of select="BQBRCD"/>
					</DeductibleTypeCd>
					<DeductibleAppliesToCd>AllPeril</DeductibleAppliesToCd>
				</Deductible>
			</Coverage>
			   <Coverage>
					<CoverageCd>EC-A</CoverageCd>
					<CurrentTermAmt>
					<Amt>
						<xsl:value-of select="BQA7VA"/>
					</Amt>
				</CurrentTermAmt>
				</Coverage>
			   <Coverage>
					<CoverageCd>VMM-A</CoverageCd>
					<CurrentTermAmt>
					<Amt>
						<xsl:value-of select="BQBBVA"/>
					</Amt>
				</CurrentTermAmt>
				</Coverage>
				<xsl:if test="BQHZTX='Y'">
				<Coverage>
					<CoverageCd>FVREP</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				</xsl:if>
				<Coverage>
					<CoverageCd>OS</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<!--Issue 98727 Begins-->
							<!--xsl:value-of select="concat(BQA6VA*.10,'.00')"/-->
								<xsl:choose>
									<xsl:when test="contains(BQA6VA*.10, '.')">
										<xsl:value-of select='BQA6VA*.10'/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="concat(BQA6VA*.10,'.00')"/>
									</xsl:otherwise>
								</xsl:choose>
							<!--Issue 98727 Ends-->
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>PP</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="BQA8VA"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="BQA9VA"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>EC-C</CoverageCd>
					<CurrentTermAmt>
					<Amt>
						<xsl:value-of select="BQA9VA"/>
					</Amt>
				</CurrentTermAmt>
				</Coverage>
			   <Coverage>
					<CoverageCd>VMM-C</CoverageCd>
					<CurrentTermAmt>
					<Amt>
						<xsl:value-of select="BQBDVA"/>
					</Amt>
				</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>PL</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="BQBAVA"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="BQBBVA"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>MEDPM</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="BQBCVA"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt></Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>FRV</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<!--Issue 98727 Begins-->
								<!--xsl:value-of select="concat(BQA6VA*.10,'.00')"/-->
								<xsl:choose>
									<xsl:when test="contains(BQA6VA*.10, '.')">
										<xsl:value-of select='BQA6VA*.10'/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="concat(BQA6VA*.10,'.00')"/>
									</xsl:otherwise>
								</xsl:choose>
								<!--Issue 98727 Ends-->
								</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
			</xsl:if>
			<xsl:if test="BQBPCD='Y'">
				<Coverage>
					<CoverageCd>WINDX</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				</xsl:if>
			<!-- Issue 39111 Start -->
			<xsl:if test="BQM3TX='Y'">
				<Coverage>
					<CoverageCd>VMM</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
			</xsl:if>
			<xsl:if test="BQI0TX='Y'">
				<Coverage>
					<CoverageCd>DP0008</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
			</xsl:if>
			<xsl:if test="BQCZST='Y'">
				<Coverage>
					<CoverageCd>HP031</CoverageCd>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
			</xsl:if>
			<!-- Issue 39111 End -->
		<!--Issue 65776 Begin -->
		<xsl:if test="$LOB='HP' or $LOB='HO' or $LOB='FP'">
			<Coverage>
	    <CoverageCd>WNDSD</CoverageCd>
			  <Option>
				<OptionTypeCd></OptionTypeCd>
				<OptionValue><xsl:value-of select="format-number(/*/ASBQCPL1__RECORD/BQAEPC,'#')"/></OptionValue>			
			  </Option>
	    </Coverage>
	  </xsl:if>
	  <!--Issue 65776 End -->
		</Dwell>
		<xsl:if test="$LOB='HP' and BQABCD = '28'">
			<Coverage>
				<CoverageCd>ANICL</CoverageCd>
				<IterationNumber>1</IterationNumber>
				<Limit>
					<FormatCurrencyAmt>
						<Amt>25000.00</Amt>
					</FormatCurrencyAmt>
				</Limit>
				<CurrentTermAmt>
					<Amt/>
				</CurrentTermAmt>
			</Coverage>
		</xsl:if>
		<!-- 39111 Start
		<xsl:if test="$LOB='HP'">
			<xsl:choose>
				<xsl:when test="/*/ASC4CPL1__RECORD[C4AOTX = 'ORDLAW']"/>
				<xsl:otherwise>
					<Coverage>
						<CoverageCd>ORDLAW</CoverageCd>
						<IterationNumber>1</IterationNumber>
						<Limit>
							<FormatPct>25.00</FormatPct>
							<LimitAppliesToCd>COVA %</LimitAppliesToCd>
						</Limit>
						<CurrentTermAmt>
							<Amt>0.00</Amt>
						</CurrentTermAmt>
					</Coverage>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/*/ASC4CPL1__RECORD[C4AOTX = 'MOLD']"/>
				<xsl:otherwise>
					<Coverage>
						<CoverageCd>MOLD</CoverageCd>
						<IterationNumber>1</IterationNumber>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>10000.00</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd>$AMT</LimitAppliesToCd>
						</Limit>
						<Limit>
							<FormatCurrencyAmt>
								<Amt>50000.00</Amt>
							</FormatCurrencyAmt>
							<LimitAppliesToCd>$AGGR</LimitAppliesToCd>
						</Limit>
						<CurrentTermAmt>
							<Amt>0.00</Amt>
						</CurrentTermAmt>
					</Coverage>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		39111 End -->
		<!-- 55614 end -->
	</xsl:template>
</xsl:stylesheet>
