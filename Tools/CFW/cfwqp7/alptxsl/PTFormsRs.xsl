<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
    
    <xsl:template name="BuildForm">
        <Form>
            <FormName>
               <xsl:value-of select="BEB8NB"/>
            </FormName>
            <FormDesc>
                <xsl:value-of select="FORMDESC"/>
            </FormDesc>
            <EditionDt>
               <xsl:call-template name="ConvertPTDateToISODate">
                  <xsl:with-param name="Value"  select="BEAMDT"/>
               </xsl:call-template>            
            </EditionDt>
            <IterationNumber>
               <xsl:value-of select="BEB7NB"/>            
            </IterationNumber>
        </Form>
    </xsl:template>
<!-- Case 31594 Begin -->
    <xsl:template name="for-loop">
		<xsl:param name="start" select="1"/>
		<xsl:param name="increment" select="1"/>
		<xsl:param name="operator" select="'='"/>
		<xsl:param name="testValue" select="1"/>
		<xsl:param name="iteration" select="1"/>
		<xsl:param name="workNode"/>
		
		<xsl:variable name="newline">
			<xsl:text>
			</xsl:text>
		</xsl:variable>
		
		<xsl:variable name="testPassed">
			<xsl:choose>
				<xsl:when test="starts-with($operator, '=')">
					<xsl:if test="$start = $testValue">
						<xsl:text>true</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="starts-with($operator, '!=')">
					<xsl:if test="$start != $testValue">
						<xsl:text>true</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="starts-with($operator, '&lt;=')">
					<xsl:if test="$start &lt;= $testValue">
						<xsl:text>true</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="starts-with($operator, '&gt;=')">
					<xsl:if test="$start &gt;= $testValue">
						<xsl:text>true</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="starts-with($operator, '&lt;')">
					<xsl:if test="$start &lt; $testValue">
						<xsl:text>true</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:when test="starts-with($operator, '&gt;')">
					<xsl:if test="$start &gt; $testValue">
						<xsl:text>true</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:message terminate="yes">
						<xsl:text>Sorry, the for-loop emulator only </xsl:text>
						<xsl:text>handles six operators.</xsl:text>
						<xsl:value-of select="$newline"/>
						<xsl:text>The value </xsl:text>
						<xsl:value-of select="$operator"/>
						<xsl:text>is not allowed.</xsl:text>
						<xsl:value-of select="$newline"/>
					</xsl:message>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:if test="$testPassed='true'">
			<!-- Your logic begins here -->
			<com.csc_Form>
	        	<FormNumber>
	        		<xsl:variable name="start-pos" select="(($start - 1) * 11) + 1"/>
    	    	   	<xsl:value-of select="substring($workNode, $start-pos, 10)"/>
        		</FormNumber>
            	<FormName/>
	            <FormDesc>
    	        </FormDesc>
        	    <EditionDt>
            	</EditionDt>
	            <IterationNumber>
    	           <!--<xsl:value-of select="SEQ"/>-->
        	    </IterationNumber>
        	</com.csc_Form>
			<!-- Your logic ends here -->
			
			<xsl:call-template name="for-loop">
				<xsl:with-param name="start" select="$start + $increment"/>
				<xsl:with-param name="increment" select="$increment"/>
				<xsl:with-param name="operator" select="$operator"/>
				<xsl:with-param name="testValue" select="$testValue"/>
				<xsl:with-param name="iteration" select="$iteration + 1"/>
				<xsl:with-param name="workNode" select="$workNode"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="CreateOptionalForms">
		<xsl:variable name="FormsTbl" select="FORMTBL"/>
		<xsl:variable name="Length"   select="string-length($FormsTbl)"/>
		<xsl:variable name="FormsNum" select="round($Length div 11)"/>

		<xsl:call-template name="for-loop">
			<xsl:with-param name="start" select="1"/>
			<xsl:with-param name="increment" select="1"/>
			<xsl:with-param name="operator" select="'&lt;='"/>
			<xsl:with-param name="testValue" select="$FormsNum"/>
			<xsl:with-param name="workNode" select="$FormsTbl"/>
		</xsl:call-template>
	</xsl:template>
	<!-- Case 31594 End -->

	<!-- Issue 59878 starts -->
	<xsl:template name="BuildCPPForm">
	<!--Issue 102807 starts-->
		<!--com.csc_Form>
			
			<xsl:choose>
				<xsl:when test="BEAGTX='CA'">
					<xsl:attribute name="id">v<xsl:value-of select="BEB9NB"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="LocationRef">
						l<xsl:value-of select="BEBRNB"/>
					</xsl:attribute>
					<xsl:attribute name="SubLocationRef">
						s<xsl:value-of select="BEEGNB"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose-->
		<!--Issue 102807 ends-->		
			<Form>
				<FormNumber>
					<xsl:value-of select="BEB8NB"/>
				</FormNumber>
				<FormName>
					<xsl:value-of select="FORMDESC"/>
				</FormName>
				<EditionDt>
					<xsl:call-template name="ConvertPTDateToISODate">
						<xsl:with-param name="Value"  select="BEAMDT"/>
					</xsl:call-template>
				</EditionDt>
				<IterationNumber>
					<xsl:value-of select="BEB7NB"/>
				</IterationNumber>
				<FormActionCd>
					<xsl:value-of select="BEDUST"/>
				</FormActionCd>
			</Form>
		<!--/com.csc_Form--> <!--Issue 102807-->
	</xsl:template>
	<!-- Issue 59878 ends -->
</xsl:stylesheet>
