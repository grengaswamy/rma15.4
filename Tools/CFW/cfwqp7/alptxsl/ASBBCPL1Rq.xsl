
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="BuildInsuranceLine">
		<xsl:param name="InsLine"/>
		<xsl:param name="RateState"/>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>ASBBCPL1</RECORD__NAME>
			</RECORD__NAME__ROW>
			<ASBBCPL1__RECORD>
				<BBAACD>
					<xsl:value-of select="$LOC"/>
				</BBAACD>
				<BBABCD>
					<xsl:value-of select="$MCO"/>
				</BBABCD>
				<BBARTX>
					<xsl:value-of select="$SYM"/>
				</BBARTX>
				<BBASTX>
					<xsl:value-of select="$POL"/>
				</BBASTX>
				<BBADNB>
					<xsl:value-of select="$MOD"/>
				</BBADNB>
				<BBAGTX>
					<xsl:value-of select="$InsLine"/>
				</BBAGTX>
				<BBC6ST>P</BBC6ST>
				<BBANDT>
				</BBANDT>
				<!-- 59878 -->
				<BBTYPE0ACT>
					<!-- 34771 start -->
					<!--103409 starts-->
					<!--<xsl:choose>
						<xsl:when test="//com.csc_AmendmentMode = 'N'">NB</xsl:when>
						<xsl:when test="//com.csc_AmendmentMode = 'A'">EN</xsl:when>
						<xsl:when test="//com.csc_AmendmentMode = 'R'">RB</xsl:when>
						--><!-- 50764 --><!--
						<xsl:when test="//com.csc_AmendmentMode = 'NR'">NR</xsl:when>
						--><!-- 50764 --><!--
						<xsl:when test="//com.csc_AmendmentMode = 'CN'">CN</xsl:when>
						--><!-- 50764 --><!--
						<xsl:when test="//com.csc_AmendmentMode = 'RI'">RI</xsl:when>
						--><!-- 39039 --><!--
					</xsl:choose>-->

					<xsl:call-template name="ConvertPolicyStatusCd">
						<xsl:with-param name="PolicyStatusCd">
							<xsl:value-of select="//PolicyStatusCd"/>
						</xsl:with-param>
					</xsl:call-template>
					<!--103409 Ends-->
					<!-- 34771 end -->
				</BBTYPE0ACT>

				<xsl:variable name="BBAKPC">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">1.000</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAKPC>
					<xsl:value-of select="$BBAKPC"/>
				</BBAKPC>
				<xsl:variable name="BBALPC">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">1.000</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBALPC>
					<xsl:value-of select="$BBALPC"/>
				</BBALPC>
				<!--59878 starts-->
				<xsl:variable name="BBAL10CDE">
					<xsl:choose>
						<xsl:when test="$InsLine='CR'">
							<xsl:value-of select="com.csc_CommlCrimeInfo/CommlCoverage/CurrentTermAmt/com.csc_OverrideInd"/>
						</xsl:when>
						<xsl:when test="$InsLine='GL'">Y</xsl:when>
						<xsl:when test="$InsLine='CF'">
							<xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EQPBRK']/com.csc_EquipBreakdownGrade"/>
						</xsl:when>
						<xsl:when test="$InsLine='CA'">
							<xsl:choose>
								<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/com.csc_CertifiedActsOfTerrorism/com.csc_ExcludeCertifiedActsOfTerrorism='1'">N</xsl:when>
								<xsl:otherwise>Y</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAL10CDE>
					<xsl:value-of select="$BBAL10CDE"/>
				</BBAL10CDE>
				<!--59878 ends-->
				<!--59878 DST CA#65 addition of Amend Pollutants field starts -->
				<xsl:variable name="BBAL36TXT">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd='LIA']/AmendPollutantsInd"/>
						</xsl:when>
						<xsl:when test="$InsLine='CF'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/com.csc_CommlPropertyLineBusiness/PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/AmendPollutantsInd"/>
						</xsl:when>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/com.csc_GeneralLiabilityLineBusiness/LiabilityInfo/AmendPollutantsInd"/>
						</xsl:when>
						<xsl:otherwise>N</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAL36TXT>
					<xsl:value-of select="$BBAL36TXT"/>
				</BBAL36TXT>
				<!--59878 DST CA#65 addition of Amend Pollutants field ends -->
				<!--59878 DST CA#69 Addition of Cancellation form starts-->
				<xsl:variable name="BBAL1TCDE">
				<xsl:choose>
				<xsl:when test="$InsLine='CA'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd='LIA']/CancellationFormCd"/>
				</xsl:when>
				<xsl:otherwise>N</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAL1TCDE>
				<xsl:value-of select="$BBAL1TCDE"/>
				</BBAL1TCDE>
					<!--59878 DST CA#69 Addition of Cancellation form ends-->
				<xsl:variable name="BBAMPC">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">1.000</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAMPC>
					<xsl:value-of select="$BBAMPC"/>
				</BBAMPC>
				<xsl:variable name="BBANPC">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">1.000</xsl:when>
						<xsl:when test="$InsLine='BOP'">
							<!-- Case 37036 start -->
							<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/PropertyInfo/CreditOrSurcharge[CreditSurchargeCd='IRPM']/NumericValue/FormatModFactor" />-->
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/com.csc_ModificationFactors/CreditOrSurcharge[CreditSurchargeCd='IRPM']/NumericValue/FormatModFactor"/>
							<!-- Case 37036 start -->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBANPC>
					<xsl:value-of select="$BBANPC"/>
				</BBANPC>
				<xsl:variable name="BBAOPC">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">1.000</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAOPC>
					<xsl:value-of select="$BBAOPC"/>
				</BBAOPC>
				<xsl:variable name="BBCZST">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">N</xsl:when>
						<xsl:when test="$InsLine='CF'">
							<!-- Issue 59878 - Start -->
							<!--xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/com.csc_ReportingInd"-->
							<xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/com.csc_BlanketReportingInd">
							</xsl:value-of>
							<!-- Issue 59878 - end -->
						</xsl:when>
						<xsl:when test="$InsLine='CA'">
							<!--xsl:choose>
								<xsl:when test="(CommlAutoHiredInfo/HiredLiabilityCostAmt/Amt!='0' and CommlAutoHiredInfo/HiredLiabilityCostAmt/Amt!='') or CommlAutoHiredInfo/com.csc_IfAnyRatingBasisInd='Y'">Y</xsl:when>
								<xsl:otherwise>N</xsl:otherwise>
							</xsl:choose-->
							<!--<xsl:if test="(CommlAutoHiredInfo/HiredLiabilityCostAmt/Amt!='0' and CommlAutoHiredInfo/HiredLiabilityCostAmt/Amt!='') or CommlAutoHiredInfo/com.csc_IfAnyRatingBasisInd='Y'">Y</xsl:if>
<xsl:if test="CommlAutoHiredInfo/HiredLiabilityCostAmt/Amt='0' and CommlAutoHiredInfo/com.csc_IfAnyRatingBasisInd='N'">N</xsl:if>
<xsl:if test="CommlAutoHiredInfo/HiredLiabilityCostAmt/Amt='' and CommlAutoHiredInfo/com.csc_IfAnyRatingBasisInd=''">N</xsl:if>
<xsl:if test="CommlAutoHiredInfo/HiredLiabilityCostAmt/Amt and CommlAutoHiredInfo/com.csc_IfAnyRatingBasisInd">N</xsl:if>
-->
							<!-- Issue 59878 - Start -->
							<xsl:variable name="Val" select="CommlAutoHiredInfo/CommlCoverage[CoverageCd='HAL']/Option/OptionCd"/>
							<xsl:if test="normalize-space($Val)=''">N</xsl:if>
							<xsl:if test="$Val!=''">
								<xsl:value-of select="$Val"/>
							</xsl:if>
							<!-- Issue 59878 - End -->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBCZST>
					<xsl:value-of select="$BBCZST"/>
				</BBCZST>
				<xsl:variable name="BBC0ST">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">N</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<BBC0ST>
					<xsl:value-of select="$BBC0ST"/>
				</BBC0ST>
				<xsl:variable name="BBC1ST">
					<xsl:choose>
						<!-- Issue 66921 start <xsl:when test="$InsLine='GL'">N</xsl:when> -->
						<xsl:when test="$InsLine='GL'">
							<xsl:choose>
								<xsl:when test="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/com.csc_AuditTypeCd='CPP'">Y</xsl:when>
								<xsl:otherwise>N</xsl:otherwise>
							</xsl:choose>
						</xsl:when><!--Issue 66921 end -->
						<xsl:when test="$InsLine='CA'">
							<xsl:variable name="Val" select="CommlAutoNonOwnedInfo/IndividualLiabilityForEmployeesInd"/>
							<xsl:if test="normalize-space($Val)=''">N</xsl:if>
							<xsl:if test="$Val!=''">
								<xsl:value-of select="$Val"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBC1ST>
					<xsl:value-of select="$BBC1ST"/>
				</BBC1ST>
				<xsl:variable name="BBC2ST">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="LiabilityInfo/com.csc_AuditTypeCd"> <!-- Issue 59878 -->
							</xsl:value-of>
						</xsl:when>
						<!-- Issue 59878 - Start -->
						<!--xsl:when test="$InsLine='CA'">
							<xsl:variable name="Val" select="CommlAutoNonOwnedInfo/com.csc_IfAnyRatingBasisInd"/>
							<xsl:if test="normalize-space($Val)=''">N</xsl:if>
							<xsl:if test="$Val!=''">
								<xsl:value-of select="$Val"/>
							</xsl:if>
						</xsl:when-->
						<!--Issue 59878 DST#64 Social Service Volunteer Donors/Volunteers changes starts-->
			            <!--xsl:when test="$InsLine='CA' and CommlAutoNonOwnedInfo/com.csc_IfAnyRatingBasisInd='Y'">Y</xsl:when>
	                    <xsl:when test="$InsLine='CA' and CommlAutoNonOwnedInfo/NonOwnedInfo[NonOwnedGroupTypeCd='E']/NumNonOwned='' and CommlAutoNonOwnedInfo/com.csc_IfAnyRatingBasisInd='N'">N</xsl:when>
	                    <xsl:when test="$InsLine='CA' and CommlAutoNonOwnedInfo/NonOwnedInfo[NonOwnedGroupTypeCd='E']/NumNonOwned='0' and CommlAutoNonOwnedInfo/com.csc_IfAnyRatingBasisInd='N'">N</xsl:when>
	                    <xsl:when test="$InsLine='CA' and CommlAutoNonOwnedInfo/NonOwnedInfo[NonOwnedGroupTypeCd='E']/NumNonOwned!='0'">Y</xsl:when>
	                    <xsl:when test="$InsLine='CA'">N</xsl:when-->
	                   <xsl:when test="$InsLine='CA'">
							<xsl:value-of select="CommlAutoNonOwnedInfo/RiskTypeCd"> <!-- Issue 59878 -->
							</xsl:value-of>
						</xsl:when> 
	                <!--Issue 59878 DST#64 Social Service Volunteer Donors/Volunteers changes ends-->
						<!-- Issue 59878 - End -->
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBC2ST>
					<xsl:value-of select="$BBC2ST"/>
				</BBC2ST>
				<xsl:variable name="BBIYTX">
					<xsl:choose>
						<!-- Issue 59878 Start -->
						<xsl:when test="$InsLine='GL'">
  							<xsl:value-of select="LiabilityInfo/AuditFrequencyCd" /> 
  						</xsl:when>
						<!-- Issue 59878 End -->
						<xsl:when test="$InsLine='BOP'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/AuditFrequencyCd"/>
						</xsl:when>
						<xsl:when test="$InsLine='CA'">
							<!-- Issue 66352 Begin -->
							<!-- Issue 66352 Comment out xsl:variable name="Amt" select="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DUN']/Limit/FormatCurrencyAmt/Amt"/>
							<xsl:if test="normalize-space($Amt)=''">N</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if>-->
							<xsl:choose>
								<!-- Use UM limit (code, not actual limit)  for DOC UN also -->
								<xsl:when test="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DUN']">
									<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Limit[LimitAppliesToCd='com.csc_UNLimit']/FormatCurrencyAmt/Amt"></xsl:value-of><!--Issue 66352-->
								</xsl:when>
								<xsl:otherwise>N</xsl:otherwise>
							</xsl:choose>
							<!-- Issue 66352 End -->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBIYTX>
					<xsl:value-of select="$BBIYTX"/>
				</BBIYTX>
				<xsl:variable name="BBIZTX">
					<xsl:choose>
						<xsl:when test="$InsLine='BOP'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/com.csc_AuditTypeCd"/>
						</xsl:when>
						<xsl:when test="$InsLine='CA'">
						<!-- Issue 59878 Start -->							
							<!--xsl:variable name="Amt" select="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DUM']/Limit/FormatCurrencyAmt/Amt"/>
							<xsl:if test="normalize-space($Amt)=''">N</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if-->
							<xsl:choose>
								<xsl:when test="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DUM'] and ../CommlCoverage[CoverageCd='UM']">
									<xsl:value-of select="normalize-space(/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='com.csc_UMLimit']/FormatCurrencyAmt/Amt)"></xsl:value-of>
								</xsl:when>
								<xsl:when test="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DUM'] and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMISP']/FormatCurrencyAmt[Amt !='0']">
									<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMISP']/FormatCurrencyAmt/Amt"></xsl:value-of>
								</xsl:when>
								<xsl:when test="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DUM'] and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMISG']/FormatCurrencyAmt[Amt !='0']">
									<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMISG']/FormatCurrencyAmt/Amt"></xsl:value-of>
								</xsl:when>
								<xsl:otherwise>N</xsl:otherwise>

							</xsl:choose>
						<!-- Issue 59878 End -->
						</xsl:when>
						<!-- Issue 59878 Start -->
						<xsl:when test="$InsLine='CF'">
 							<xsl:choose>
  								<xsl:when test="com.csc_CertifiedActsOfTerrorism/com.csc_ExcludeCertifiedActsOfTerrorism='1'">N</xsl:when> 
  								<xsl:otherwise>Y</xsl:otherwise> 
  							</xsl:choose>
  						</xsl:when>
 						<xsl:when test="$InsLine='GL'">
 							<xsl:choose>
  								<xsl:when test="com.csc_CertifiedActsOfTerrorism/com.csc_ExcludeCertifiedActsOfTerrorism='1'">N</xsl:when> 
  								<xsl:otherwise>Y</xsl:otherwise> 
  							</xsl:choose>
  						</xsl:when>
						<!-- Issue 59878 End -->
					</xsl:choose>
				</xsl:variable>
				<BBIZTX>
					<xsl:value-of select="$BBIZTX"/>
				</BBIZTX>
				<xsl:variable name="BBI0TX">
					<xsl:choose>
							<!-- Issue 59878 Start -->

						<xsl:when test="$InsLine='CA' and $RateState ='LA' and CommlAutoDriveOtherCarInfo[NumIndividualsCovered='0'] or CommlAutoDriveOtherCarInfo[NumIndividualsCovered='']">N</xsl:when>
						<xsl:when test="$InsLine='CA' and $RateState ='LA' and CommlAutoDriveOtherCarInfo[NumIndividualsCovered!='0']">Y</xsl:when>
						<xsl:when test="$InsLine='CA' and CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DCL']">
							<!--xsl:variable name="Amt" select="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='LIA']/Option/OptionValue"/-->
							<xsl:variable name="Amt" select="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DCL']/Option/OptionValue"/>
							<xsl:if test="normalize-space($Amt)=''">N</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if>
						</xsl:when>						
						<xsl:otherwise>N</xsl:otherwise>
							<!-- Issue 59878 End -->

					</xsl:choose>
				</xsl:variable>
				<BBI0TX>
					<xsl:value-of select="$BBI0TX"/>
				</BBI0TX>
				<xsl:variable name="BBI1TX">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">N</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<BBI1TX>
					<xsl:value-of select="$BBI1TX"/>
				</BBI1TX>
				<xsl:variable name="BBI2TX">
					<xsl:choose>
						<!--Issue 66921 start <xsl:when test="$InsLine='GL'">N</xsl:when> -->
						<xsl:when test="$InsLine='GL'">Y</xsl:when><!--Issue 66921 end -->
							<xsl:when test="$InsLine='CA'">
								<!--Issue 59878 start-->
								<xsl:choose>
									<!--<xsl:when test="($RateState='IL' or $RateState='LA') and CommlAutoHiredInfo/CommlCoverage/CoverageCd='UM'">--> <!--Issue 59878-->
									<xsl:when test="($RateState='IL' or $RateState='LA') and CommlAutoHiredInfo/CommlCoverage/CoverageCd='HUM'">	<!--Issue 59878-->
										<xsl:value-of select="string('Y')"/>										
									</xsl:when>									
									<xsl:otherwise>
										<xsl:value-of select="string('N')"/>
									</xsl:otherwise>
								</xsl:choose>								
								<!--Issue 59878 end-->
							</xsl:when>
						<!--<xsl:when test="$InsLine='CA'">N</xsl:when>--> <!--Issue 59878-->
						<!-- Issue 59878 - Start -->
						<xsl:when test="$InsLine='CF'">
							<xsl:variable name="Amt" select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EQPBRK']/com.csc_ExcludeEquipBreakdown"/>
							<xsl:if test="$Amt = 'Y'">N</xsl:if>
							<xsl:if test="$Amt = 'N'">Y</xsl:if>
						</xsl:when>
						<!-- Issue 59878 - End -->
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBI2TX>
					<xsl:value-of select="$BBI2TX"/>
				</BBI2TX>
				<xsl:variable name="BBI3TX">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:variable name="Amt" select="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DCM']/Option/OptionValue"/>
							<xsl:if test="normalize-space($Amt)=''">N</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<BBI3TX>
					<xsl:value-of select="$BBI3TX"/>
				</BBI3TX>
				<xsl:variable name="BBI4TX">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:variable name="Amt" select="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DCO']/Option/OptionValue"/>
							<xsl:if test="normalize-space($Amt)=''">N</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<BBI4TX>
					<xsl:value-of select="$BBI4TX"/>
				</BBI4TX>
				<xsl:variable name="BBI5TX">
					<xsl:choose>
						<!--Issue 59878 start-->
						<xsl:when test="$InsLine='CA' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Option[OptionCd='com.csc_UNType'] and $RateState = 'IL' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoHiredInfo/CommlCoverage[CoverageCd = 'HUN']">
							<xsl:value-of select="normalize-space(/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Limit[LimitAppliesToCd='com.csc_HUNLimit']/FormatCurrencyAmt/Amt)"/>
						</xsl:when>
						<!--Issue 59878 end-->
						<xsl:when test="$InsLine='CA'">N</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<BBI5TX>
					<xsl:value-of select="$BBI5TX"/>
				</BBI5TX>
				<xsl:variable name="BBAKCD">
					<xsl:choose>
						<xsl:when test="$InsLine='BOP'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/PolicyTypeCd"/>
						</xsl:when>
						<!-- 59878 starts-->
						<!--<xsl:when test="$InsLine='CR'">C</xsl:when>-->

						<xsl:when test="$InsLine='CR'">
							<xsl:value-of select="../com.csc_CrimeLineBusiness/Form/FormNumber"/>
						</xsl:when>
						<!-- 59878 ends-->
						<!--issue #73609 starts-->
					<!-- Issue 66911 start-->
						<xsl:when test="$InsLine='CA' and $RateState = 'MO' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd = 'DUN']">
							<xsl:variable name="DOCUNTYPE" select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Option[OptionCd='com.csc_UNType']/OptionValue"/>
							<xsl:choose>
								<xsl:when test="normalize-space($DOCUNTYPE)='SL/S'">SL</xsl:when>
								<xsl:when test="normalize-space($DOCUNTYPE)='SPL/S'">SPLIT</xsl:when>
								<xsl:otherwise><xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Option[OptionCd='com.csc_UNType']/OptionValue"/></xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					<!-- Issue 66911 end-->
					<!-- Issue 59878 - Start -->
						<!--xsl:when test="$InsLine='CA'">
							<xsl:variable name="Amt" select="normalize-space(CommlVeh/CommlCoverage[CoverageCd='LIA']/Limit/FormatCurrencyAmt/Amt)"/>
							<xsl:if test="normalize-space($Amt)=''">0</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:choose>
									<xsl:when test="string-length($Amt)&gt;6">
										<xsl:value-of select="concat(substring($Amt ,0,string-length($Amt)-5), ' ' ,'M')"/>
									</xsl:when>
									<xsl:when test="string-length($Amt)&gt;3">
										<xsl:value-of select="concat(substring($Amt ,0,string-length($Amt)-2), ' ' ,'K')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$Amt"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
						</xsl:when-->
						<!--issue #73609 Ends-->
						<!--<xsl:when test="$InsLine='CA' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Option[OptionCd='com.csc_UNType'] and $RateState = 'IL' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoHiredInfo/CommlCoverage[CoverageCd = 'HUN']">-->
							<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Option[OptionCd='com.csc_UNLimit']/OptionValue"/>--> <!--Issue 59878-->
							<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Limit[LimitAppliesToCd='com.csc_UNLimit']/FormatCurrencyAmt/Amt"/>--> <!--Issue 59878-->
						<!--</xsl:when>-->
						<xsl:when test="$InsLine='CA' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Limit[LimitAppliesToCd='com.csc_UNLimit'] and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd = 'DUN']">
							<xsl:value-of select ="normalize-space(/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Limit[LimitAppliesToCd='com.csc_UNLimit']/FormatCurrencyAmt/Amt)"/>
						</xsl:when>
						<!--<xsl:when test="$InsLine='CA'">N</xsl:when>-->
						<xsl:when test="$InsLine='CF'">
							<!--xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/CommlCoverage/CommlCoverageSupplement/CoinsurancePct"-->
							<xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/com.csc_BlanketCoinsurancePct">
							</xsl:value-of>
							<!-- Issue 59878 - end -->
						</xsl:when>
						<xsl:otherwise>N</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAKCD>
					<xsl:value-of select="$BBAKCD"/>
				</BBAKCD>
				<xsl:variable name="BBALCD">
					<xsl:choose>
						<!-- 59878 starts-->
						<!--<xsl:when test="$InsLine='CR'">01</xsl:when>-->
						<xsl:when test="$InsLine='CR'">
							<xsl:value-of select="../com.csc_CrimeLineBusiness/LOBSubCd"/>
						</xsl:when>
						<!-- 59878 ends-->
						<xsl:when test="$InsLine='CA'">
						<!-- Issue 59878 - Start -->
							<!--xsl:value-of select="CommlVeh/CommlCoverage[CoverageCd='MED']/Limit/FormatCurrencyAmt/Amt"/-->
							<xsl:variable name="Amt" select="../CommlCoverage[CoverageCd='MP']/Limit/FormatCurrencyAmt/Amt"/>
							<xsl:choose>
								<xsl:when test="$Amt != '0' and $Amt != ''"><xsl:value-of select="$Amt"/></xsl:when>
								<xsl:otherwise>N</xsl:otherwise>							
							</xsl:choose>
						<!-- Issue 59878 - End -->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBALCD>
					<xsl:value-of select="$BBALCD"/>
				</BBALCD>
				<xsl:variable name="BBAMCD">
					<xsl:choose>
						<!--Issue #73609 Starts-->
						<!--<xsl:when test="$InsLine='CA'">0</xsl:when>-->
						<!-- Issue 59878 - Start-->
						<!--xsl:when test="$InsLine='CA'">
							<xsl:variable name="Amt" select="normalize-space(CommlVeh/CommlCoverage[CoverageCd='UM']/Limit/FormatCurrencyAmt/Amt)"/>
							<xsl:if test="normalize-space($Amt)=''">0</xsl:if> 
							<xsl:if test="$Amt!=''">
								<xsl:choose>
									<xsl:when test="string-length($Amt)&gt;6">
										<xsl:value-of select="concat(substring($Amt ,0,string-length($Amt)-5), ' ' ,'M')"/>
									</xsl:when>
									<xsl:when test="string-length($Amt)&gt;3">
										<xsl:value-of select="concat(substring($Amt ,0,string-length($Amt)-2), ' ' ,'K')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$Amt"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
						</xsl:when-->
						<xsl:when test="$InsLine='CA' and $RateState='AR' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt!=''">N</xsl:when>
						<xsl:when test="$InsLine='CA' and $RateState='AR' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt=''">N</xsl:when>          	
						<xsl:when test="$InsLine='CA' and $RateState='CO' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt!=''">N</xsl:when>          
						<xsl:when test="$InsLine='CA' and $RateState='CO' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt=''">N</xsl:when>          	
						<xsl:when test="$InsLine='CA' and $RateState='MN' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt!=''">N</xsl:when>         	
						<xsl:when test="$InsLine='CA' and $RateState='MN' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt=''">N</xsl:when>          	
						<xsl:when test="$InsLine='CA' and $RateState='NM' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt!=''">250</xsl:when>        
						<xsl:when test="$InsLine='CA' and $RateState='NM' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt=''">N</xsl:when>           
						<xsl:when test="$InsLine='CA' and $RateState='OR' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt!=''">N</xsl:when>          
						<xsl:when test="$InsLine='CA' and $RateState='OR' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt=''">N</xsl:when>          	
						<xsl:when test="$InsLine='CA' and $RateState='WI' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt!=''">N</xsl:when>          
						<xsl:when test="$InsLine='CA' and $RateState='WI' and ../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMPD']/FormatCurrencyAmt/Amt=''">N</xsl:when>          	
						<xsl:when test="$InsLine='CA' and $RateState='WA'">0</xsl:when>
						<xsl:when test="$InsLine='CR'">
							<xsl:value-of select="../com.csc_CrimeLineBusiness/com.cscConvertAggLimitInsCd"/>
						</xsl:when>
						<xsl:when test="$InsLine='CF'">0</xsl:when>
						<!-- 59878 ends-->
						<!--Issue #73609 Starts-->
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAMCD>
					<xsl:value-of select="$BBAMCD"/>
				</BBAMCD>
				<xsl:variable name="BBANCD">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<!-- Issue 59878 - Start -->
							<!--xsl:choose>
								<xsl:when test="$RateState='IN'">CSL</xsl:when>
								<xsl:otherwise>SL</xsl:otherwise>
							</xsl:choose-->
							<xsl:variable name="UMTYPE" select="../CommlCoverage[CoverageCd='UM']/Option[OptionCd='com.csc_UMType']/OptionValue"/>
							<xsl:choose>
								<xsl:when test="normalize-space($UMTYPE)!=''">
									<xsl:value-of select="$UMTYPE"/>
								</xsl:when>								
								<xsl:otherwise>N</xsl:otherwise>
							</xsl:choose>
							<!-- Issue 59878 - End -->
						</xsl:when>
						<xsl:when test="$InsLine='CF'">N</xsl:when>	<!-- Issue 59878 -->
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBANCD>
					<xsl:value-of select="$BBANCD"/>
				</BBANCD>
				<xsl:variable name="BBAEPC">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">1.000</xsl:when>
						<xsl:when test="$InsLine='CA'">0</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAEPC>
					<xsl:value-of select="$BBAEPC"/>
				</BBAEPC>
				<xsl:variable name="BBAFPC">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">1.000</xsl:when>
						<xsl:when test="$InsLine='CA'">0</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAFPC>
					<xsl:value-of select="$BBAFPC"/>
				</BBAFPC>
				<xsl:variable name="BBBKNB">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:value-of select="CommlAutoNonOwnedInfo/NonOwnedInfo[NonOwnedGroupTypeCd='E']/NumNonOwned"/>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBBKNB>
					<xsl:value-of select="$BBBKNB"/>
				</BBBKNB>
				<xsl:variable name="BBBLNB">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:value-of select="CommlAutoNonOwnedInfo/NonOwnedInfo[NonOwnedGroupTypeCd='V']/NumNonOwned"/>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBBLNB>
					<xsl:value-of select="$BBBLNB"/>
				</BBBLNB>
				<xsl:variable name="BBAGVA">
					<xsl:choose>
						<xsl:when test="$InsLine='CF'">
							<!--xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo/CommlCoverage[CoverageCd='BLDG']/Limit/FormatCurrencyAmt/Amt,'$,,','')"-->
							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/CommlCoverage[CoverageCd='BLDG']/Limit/FormatCurrencyAmt/Amt,'$,,','')">	<!-- Issue 59878 -->
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='CA'">0</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAGVA>
					<xsl:value-of select="$BBAGVA"/>
				</BBAGVA>
				<xsl:variable name="BBAHVA">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<!-- Issue 59878 - Start -->
							<!--xsl:variable name="Amt" select="CommlVeh/CommlCoverage[CoverageCd='UM']/Limit/FormatCurrencyAmt/Amt"/>
							<xsl:if test="normalize-space($Amt)=''">0</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if-->
							<xsl:variable name="UMTYPE" select="../CommlCoverage[CoverageCd='UM']/Option[OptionCd='com.csc_UMType']/OptionValue"/>
							<xsl:choose>
								<xsl:when test="normalize-space($UMTYPE)='SL'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMISG']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
								<xsl:when test="normalize-space($UMTYPE)='SPL BI'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMCSL']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
								<xsl:when test="normalize-space($UMTYPE)='CSL'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMCSL']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
								<xsl:when test="normalize-space($UMTYPE)='SL/S'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMISG']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
								<xsl:when test="normalize-space($UMTYPE)='CSLNST'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='CSLNST']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
								<xsl:when test="normalize-space($UMTYPE)='CSLSTK'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='CSLSTK']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
								<xsl:when test="normalize-space($UMTYPE)='SLNST'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='SLNST']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
								<xsl:when test="normalize-space($UMTYPE)='SLSTK'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='SLSTK']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
							<!-- Issue 59878 - End -->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAHVA>
					<xsl:value-of select="$BBAHVA"/>
				</BBAHVA>
				<xsl:variable name="BBBWVA">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">0</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBBWVA>
					<xsl:value-of select="$BBBWVA"/>
				</BBBWVA>
				<!-- 59878 starts -->
				<xsl:variable name="BBK3TX">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:choose>
								<xsl:when test="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='DUN']">I</xsl:when>
								<xsl:otherwise>N</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>I</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBK3TX>
					<xsl:value-of select="$BBK3TX"/>
				</BBK3TX>
				<!-- 59878 ends -->
				<xsl:variable name="BBK4TX">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">A</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBK4TX>
					<xsl:value-of select="$BBK4TX"/>
				</BBK4TX>
				<!--Issue 65728 Begins -->
				<xsl:variable name="BBK5TX">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:value-of select="CommlAutoDriveOtherCarInfo/CCTerritoryCd"/>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBK5TX>
					<xsl:value-of select="$BBK5TX"/>
				</BBK5TX>
				<!--Issue 65728 Ends -->
				<xsl:variable name="BBBFPC">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">1.000</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBBFPC>
					<xsl:value-of select="$BBBFPC"/>
				</BBBFPC>
				<xsl:if test="$InsLine='BOP'">
					<BBBGPC>
						<!-- Case 37036 start -->
						<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/PropertyInfo/CreditOrSurcharge[CreditSurchargeCd='EXN']/NumericValue/FormatModFactor" />-->
						<xsl:value-of select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/com.csc_ModificationFactors/CreditOrSurcharge[CreditSurchargeCd='EXN']/NumericValue/FormatModFactor"/>
						<!-- Case 37036 start -->
					</BBBGPC>
					<BBBLPC>
						<!-- Case 37036 start -->
						<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/PropertyInfo/CreditOrSurcharge[CreditSurchargeCd='SCH']/NumericValue/FormatModFactor" />-->
						<xsl:value-of select="/ACORD/InsuranceSvcRq/*/BOPLineBusiness/com.csc_ModificationFactors/CreditOrSurcharge[CreditSurchargeCd='SCH']/NumericValue/FormatModFactor"/>
						<!-- Case 37036 start -->
					</BBBLPC>
				</xsl:if>
				<xsl:variable name="BBUSCD5">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<!-- Issue 59878 - Start -->
							<!--xsl:variable name="Amt" select="CommlVeh/CommlCoverage[CoverageCd='UM']/Option[OptionCd='com.csc_UMSpecType']/OptionValue"/-->
							<!--	<xsl:if test="normalize-space($Amt)=''">0</xsl:if> -->
							<!--xsl:if test="normalize-space($Amt)=''">I</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if-->
							<xsl:choose>
								<xsl:when test="../CommlCoverage[CoverageCd='UM']/Option[OptionCd='com.csc_UMSpecType']/OptionValue !='0'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Option[OptionCd='com.csc_UMSpecType']/OptionValue"/>
								</xsl:when>
								<xsl:otherwise>N</xsl:otherwise>
							</xsl:choose>
							<!-- Issue 59878 - End -->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBUSCD5>
					<xsl:value-of select="$BBUSCD5"/>
				</BBUSCD5>
				<xsl:variable name="BBPLTX">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<!-- Issue 59878 - Start -->
							<!--xsl:variable name="Amt" select="CommlVeh/CommlCoverage[CoverageCd='LBD']/Deductible/DeductibleTypeCd"/>
							<xsl:if test="normalize-space($Amt)=''">0</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if-->
							<xsl:value-of select="../CommlCoverage[CoverageCd='LIA']/Deductible/DeductibleTypeCd"/>
							<!-- Issue 59878 - End -->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBPLTX>
					<xsl:value-of select="$BBPLTX"/>
				</BBPLTX>
				<xsl:variable name="BBPMTX">
					<xsl:choose>
<!--Issue 59878 start-->

						<xsl:when test="$InsLine='CA' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/CommlVehSupplement[com.csc_RentalVeh ='Y']">Y</xsl:when>
						<xsl:otherwise>N</xsl:otherwise>
<!--Issue 59878 ends-->

					</xsl:choose>
				</xsl:variable>
				<BBPMTX>
					<xsl:value-of select="$BBPMTX"/>
				</BBPMTX>
				<xsl:variable name="BBPNTX">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<!--Issue 59878 start-->
							<xsl:choose>
								<xsl:when test="CommlAutoNonOwnedInfo/NonOwnedInfo/CommlCoverage[CoverageCd='NUM'] and $RateState='IL'">Y</xsl:when>
								<xsl:when test="CommlAutoNonOwnedInfo/NonOwnedInfo/CommlCoverage[CoverageCd='NUM'] and $RateState='LA'">Y</xsl:when>
								<xsl:otherwise>
							<!--Issue 59878 end-->
							<xsl:variable name="Amt" select="CommlAutoNonOwnedInfo/NonOwnedInfo/CommlCoverage[CoverageCd='DUM']/Limit/FormatCurrencyAmt/Amt"/>
							<xsl:if test="normalize-space($Amt)=''">N</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if>
								</xsl:otherwise>	<!--Issue 59878 - End of Otherwise-->
							</xsl:choose>			<!--Issue 59878 - End of Choose-->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBPNTX>
					<xsl:value-of select="$BBPNTX"/>
				</BBPNTX>
				<xsl:variable name="BBPOTX">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<!--Issue # 73609 starts-->
							<!--<xsl:value-of select="CommlAutoHiredInfo/CommlCoverage[CoverageCd='CMP']/Deductible/FormatCurrencyAmt/Amt"/> -->
							<!-- Issu 59878 - Start -->
							<!--xsl:variable name="Amt" select="CommlAutoHiredInfo/CommlCoverage[CoverageCd='CMP']/Deductible/FormatCurrencyAmt/Amt"/>
							<xsl:if test="normalize-space($Amt)=''">0</xsl:if>
							<xsl:if test="normalize-space($Amt)='NA'">0</xsl:if>
							<xsl:if test="normalize-space($Amt)='0'">FULL</xsl:if>
							<xsl:if test="$Amt!='' or normalize-space($Amt)='0'or normalize-space($Amt)='FULL' ">
								<xsl:value-of select="$Amt"/>
							</xsl:if-->
							<xsl:value-of select="CommlAutoHiredInfo/CommlCoverage[CoverageCd='HCM']/Deductible/FormatCurrencyAmt/Amt"/>
							<!-- Issu 59878 - End -->
							<!--Issue # 73609 Ends-->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBPOTX>
					<xsl:value-of select="$BBPOTX"/>
				</BBPOTX>
				<xsl:variable name="BBPPTX">
					<xsl:choose>
						<!-- Issue 59878 - Start -->
						<!--xsl:when test="$InsLine='CA'">
							<xsl:variable name="Amt" select="CommlAutoNonOwnedInfo/NonOwnedInfo/CommlCoverage[CoverageCd='DUN']/Limit/FormatCurrencyAmt/Amt"/-->
							<!--issue #73609 starts-->
							<!--<xsl:if test="normalize-space($Amt)=''">N</xsl:if> -->
							<!--xsl:if test="normalize-space($Amt)=''">0</xsl:if-->
							<!--issue #73609 Ends-->
							<!--xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if-->
							<xsl:when test="$InsLine='CA' and $RateState='IL' and /ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoNonOwnedInfo/NonOwnedInfo/CommlCoverage/CoverageCd = 'NUN'">
								<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlCoverage[CoverageCd='UN']/Limit[LimitAppliesToCd='com.csc_UNLimit']/FormatCurrencyAmt/Amt"/>
							</xsl:when>
							<xsl:when test="$InsLine='CA'">N</xsl:when>
						<!--/xsl:when-->
						<!-- Issue 59878 - End -->
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBPPTX>
					<xsl:value-of select="$BBPPTX"/>
				</BBPPTX>
				<xsl:variable name="BBEGCD">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<!--issue #73609 starts-->
							<!--<xsl:value-of select="CommlAutoHiredInfo/CommlCoverage[CoverageCd='COL']/Deductible/FormatCurrencyAmt/Amt"/> -->
							<!-- Issue 59878 - Start -->
							<!--xsl:variable name="Amt" select="CommlAutoHiredInfo/CommlCoverage[CoverageCd='COL']/Deductible/FormatCurrencyAmt/Amt"/>
							<xsl:if test="normalize-space($Amt)=''">0</xsl:if> 
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if-->
							<xsl:value-of select="CommlAutoHiredInfo/CommlCoverage[CoverageCd='HCO']/Deductible/FormatCurrencyAmt/Amt"/>
							<!-- Issue 59878 - End -->
							<!--issue #73609 Ends-->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBEGCD>
					<xsl:value-of select="$BBEGCD"/>
				</BBEGCD>
				<xsl:variable name="BBEHCD">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<!-- Issue 59878 - Start -->
							<!--xsl:variable name="Amt" select="CommlVeh/CommlCoverage[CoverageCd='LBD']/Deductible/FormatCurrencyAmt/Amt"/-->
							<xsl:variable name="Amt" select="../CommlCoverage[CoverageCd='LIA']/Deductible/FormatCurrencyAmt/Amt"/>
							<!-- Issue 59878 - End -->
							<xsl:if test="normalize-space($Amt)=''">0</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBEHCD>
					<xsl:value-of select="$BBEHCD"/>
				</BBEHCD>
				<xsl:variable name="BBEICD">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'"><xsl:value-of select="../PMACd"/></xsl:when> <!-- Issue 59878 -->
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBEICD>
					<xsl:value-of select="$BBEICD"/>
				</BBEICD>
				<xsl:variable name="BBEKCD">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:variable name="Amt" select="CommlAutoHiredInfo/CommlCoverage[CoverageCd='HSP']/Option/OptionValue"/>
							<xsl:if test="normalize-space($Amt)=''">N</xsl:if>
							<xsl:if test="$Amt!=''">Y</xsl:if>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBEKCD>
					<xsl:value-of select="$BBEKCD"/>
				</BBEKCD>
				<xsl:variable name="BBUSVA3">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='EAOCC']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='CF'">
							<!-- <xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo/CommlCoverage[CoverageCd='BLDGBPP']/Limit/FormatCurrencyAmt/Amt,'$,,','')"> -->
							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/CommlCoverage[CoverageCd='BLDGPERS']/Limit/FormatCurrencyAmt/Amt,'$,,','')"><!--Issue 59878-->
							</xsl:value-of>
						</xsl:when>
						<!-- Issue 67226 Start -->
						<xsl:when test="$InsLine='CA'">
							<xsl:variable name="UMTYPE" select="../CommlCoverage[CoverageCd='UM']/Option[OptionCd='com.csc_UMType']/OptionValue"/>
							<xsl:choose>
								<xsl:when test="normalize-space($UMTYPE)='SPLNST'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='SPLNST']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
								<xsl:when test="normalize-space($UMTYPE)='SPLSTK'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='SPLSTK']/FormatCurrencyAmt/Amt"/>
								</xsl:when>								
					            <xsl:when test="normalize-space($UMTYPE)='SPLBI' or normalize-space($UMTYPE)='SPLIT'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMISP']/FormatCurrencyAmt/Amt"/>
								</xsl:when>	
								<xsl:when test="normalize-space($UMTYPE)='SPLBI' or normalize-space($UMTYPE)='SPL/S'">
									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Limit[LimitAppliesToCd='UMISP']/FormatCurrencyAmt/Amt"/>
								</xsl:when>															
						        <xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
							</xsl:when>
						<!-- Issue 67226 End -->
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBUSVA3>
					<xsl:value-of select="$BBUSVA3"/>
				</BBUSVA3>
				<xsl:variable name="BBUSVA4">
					<xsl:choose>
						<xsl:when test="$InsLine='BOP'">
							<xsl:value-of select="LiabilityInfo/CommlCoverage[CoverageCd='GENAG']/Limit/FormatCurrencyAmt/Amt">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='PRDCO']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='CF'">
							<!--xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo/CommlCoverage[CoverageCd='STOCK']/Limit/FormatCurrencyAmt/Amt,'$,,','')"-->
							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/CommlCoverage[CoverageCd='STOCK']/Limit/FormatCurrencyAmt/Amt,'$,,','')">	<!-- Issue 59878 -->
							</xsl:value-of>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBUSVA4>
					<xsl:value-of select="$BBUSVA4"/>
				</BBUSVA4>
				<xsl:variable name="BBUSVA5">
					<xsl:choose>
						<xsl:when test="$InsLine='BOP'">
							<xsl:value-of select="LiabilityInfo/CommlCoverage[CoverageCd='PRDCO']/Limit/FormatCurrencyAmt/Amt">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='GENAG']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='CF'">
							<!-- Issue 59878 - Start -->
							<!--xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo/CommlCoverage[CoverageCd='BUSEE']/Limit/FormatCurrencyAmt/Amt,'$,,','')"-->
							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/CommlCoverage[CoverageCd='BUSINC']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							<!-- Issue 59878 - End -->
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='CA'">
							<!-- Issue 59878 - Start -->
							<!--xsl:variable name="Amt" select="CommlVeh/CommlCoverage[CoverageCd='LIA']/Limit/FormatCurrencyAmt/Amt"/-->
							<xsl:variable name="Amt" select="../CommlCoverage[CoverageCd='LIA']/Limit/FormatCurrencyAmt/Amt"/>
							<!-- Issue 59878 - End -->
							<xsl:if test="normalize-space($Amt)=''">0</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBUSVA5>
					<xsl:value-of select="$BBUSVA5"/>
				</BBUSVA5>
				<xsl:variable name="BBCXVA">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='MEDEX']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when>
						<!-- Issue 59878 - Start -->
						<!--xsl:when test="$InsLine='CF'">
							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo/CommlCoverage[CoverageCd='EE']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when-->
						<xsl:when test="$InsLine='CF'">00000000000</xsl:when>
						<!-- Issue 59878 - End -->
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBCXVA>
					<xsl:value-of select="$BBCXVA"/>
				</BBCXVA>
				<xsl:variable name="BBCYVA">
					<xsl:choose>
						<xsl:when test="$InsLine='BOP'">
							<xsl:value-of select="LiabilityInfo/CommlCoverage[CoverageCd='MEDPM']/Limit/FormatCurrencyAmt/Amt">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='com.csc_LLOCC']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='CF'">
							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo/CommlCoverage[CoverageCd='BRWOR']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBCYVA>
					<xsl:value-of select="$BBCYVA"/>
				</BBCYVA>
				<xsl:variable name="BBCZVA">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='com.csc_LLAGG']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='CF'">
							<!-- Issue 59878 - Start -->
							<!--xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo/CommlCoverage[CoverageCd='BPP']/Limit/FormatCurrencyAmt/Amt,'$,,','')"-->
							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/CommlCoverage[CoverageCd='PERS']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
							<!-- Issue 59878 - End -->
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBCZVA>
					<xsl:value-of select="$BBCZVA"/>
				</BBCZVA>
				<xsl:variable name="BBC0VA">
					<xsl:choose>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='com.csc_EBLOCC']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when>
						<!--Issue 78346 starts-->
						<xsl:when test="$InsLine='CF'">00000015000</xsl:when><!--Issue 78346 ends-->
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBC0VA>
					<xsl:value-of select="$BBC0VA"/>
				</BBC0VA>
				<xsl:variable name="BBC1VA">
					<xsl:choose>
						<!--65788 Starts-->
						<xsl:when test="$InsLine='CF'">
							<xsl:choose>
							<!--<xsl:when test="PropertyInfo/CommlPropertyInfo/CommlCoverage[CoverageCd='SPLCLS']">--><!--UNI01130A-->
								<xsl:when test="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/CommlCoverage[CoverageCd='SPLCLS']"><!--UNI01130A-->
									<!--<xsl:value-of select="PropertyInfo/CommlPropertyInfo/CommlCoverage[CoverageCd='SPLCLS']/Limit/FormatCurrencyAmt/Amt"/>--><!--UNI01130A-->
									<xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='BLNKT']/CommlCoverage[CoverageCd='SPLCLS']/Limit/FormatCurrencyAmt/Amt"/><!--UNI01130A-->
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise> 
							</xsl:choose>
						</xsl:when>
						<!--65788 Ends-->
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='com.csc_EBLAGG']/Limit/FormatCurrencyAmt/Amt,'$,,','')">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='CA'">
							<xsl:value-of select="CommlAutoHiredInfo/HiredLiabilityCostAmt/Amt"/>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBC1VA>
					<xsl:value-of select="$BBC1VA"/>
				</BBC1VA>
				<xsl:variable name="BBC2VA">
					<xsl:choose>
						<xsl:when test="$InsLine='BOP'">
							<xsl:value-of select="LiabilityInfo/CommlCoverage[CoverageCd='FLL']/Limit/FormatCurrencyAmt/Amt">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='PIADV']/Limit/FormatCurrencyAmt/Amt,'$,,','')"/>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBC2VA>
					<xsl:value-of select="$BBC2VA"/>
				</BBC2VA>
				<xsl:variable name="BBC3VA">
					<xsl:choose>
						<xsl:when test="$InsLine='BOP'">
							<xsl:value-of select="LiabilityInfo/CommlCoverage[CoverageCd='EAOCC']/Limit/FormatCurrencyAmt/Amt">
							</xsl:value-of>
						</xsl:when>
						<xsl:when test="$InsLine='GL'">
							<xsl:value-of select="translate(LiabilityInfo/CommlCoverage[CoverageCd='FIRDM']/Limit/FormatCurrencyAmt/Amt,'$,,','')"/>
						</xsl:when>
						<xsl:when test="$InsLine='CA'">
							<xsl:variable name="Hired" select="CommlAutoHiredInfo"/>
							<xsl:if test="Hired/CommlCoverage[CoverageCd='HSP']">
								<xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBC3VA>
					<xsl:value-of select="$BBC3VA"/>
				</BBC3VA>
				<xsl:variable name="BBEJCD">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:variable name="Amt" select="CommlAutoDriveOtherCarInfo/CommlCoverage[CoverageCd='MED']/Limit/FormatCurrencyAmt/Amt"/>
							<xsl:if test="normalize-space($Amt)=''">N</xsl:if>
							<xsl:if test="$Amt!=''">
								<xsl:value-of select="$Amt"/>
							</xsl:if>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<BBEJCD>
					<xsl:value-of select="$BBEJCD"/>
				</BBEJCD>
				<xsl:variable name="BBUSNB3">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:value-of select="CommlAutoDriveOtherCarInfo/NumIndividualsCovered"/>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBUSNB3>
					<xsl:value-of select="$BBUSNB3"/>
				</BBUSNB3>
				<xsl:variable name="BBIFNB">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:value-of select="CommlAutoNonOwnedInfo/NonOwnedInfo[NonOwnedGroupTypeCd='com.csc_NumDonors']/NumNonOwned"/>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBIFNB>
					<xsl:value-of select="$BBIFNB"/>
				</BBIFNB>
				<!-- Issue 66352 Begin -->
				<xsl:choose>
					<xsl:when test="$InsLine='CA'">
						<xsl:choose>
							<xsl:when test="$RateState='LA'">
								<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/CommlVehSupplement/com.csc_VehicleUMInd = 'N'"><BBLCTX>N</BBLCTX></xsl:if>
								<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/CommlVehSupplement/com.csc_VehicleUMInd = ''"><BBLCTX>N</BBLCTX></xsl:if>
								<xsl:if test="/ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlVeh/CommlVehSupplement/com.csc_VehicleUMInd = 'Y'"><BBLCTX>B</BBLCTX></xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<BBLCTX>N</BBLCTX>
							</xsl:otherwise>
						</xsl:choose>
						<!-- Issue 59878 - Start -->
						<BBUSIN13>N</BBUSIN13>
						<!-- This one will depend on if there are any excluded drivers on the policy (later): -->
						<BBAL1ZCDE>N</BBAL1ZCDE>
						<!--Issue 59878 start-->
						<!--<BBUSIN15>N</BBUSIN15>-->	<!--Commented this code-->
						<!--Added this code-->
						<BBUSIN15>
							<xsl:value-of select="../CommlCoverage[CoverageCd = 'UM']/Option[OptionCd = 'com.csc_UMNonStacked']/OptionValue"/>
						</BBUSIN15>
						<!--Issue 59878 end-->
						<!-- Issue 59878 - Start -->
					</xsl:when>
				</xsl:choose>
				<!-- Issue 66352 End -->
				<!-- Issue 59878 start -->
				<xsl:choose>
 					<xsl:when test="$InsLine='GL'">
  						<BBK3TX /> 
  						<BBLCTX /> 
  						<BBBMPC>1.000</BBBMPC> 
  						<BBBAPC>1.000</BBBAPC> 
  						<BBBGPC>1.000</BBBGPC> 
  						<BBA0DT>0</BBA0DT> 
  						<BBUSIN13>N</BBUSIN13> 
  						<BBAL1ZCDE>N</BBAL1ZCDE> 
						<BBUSIN15>N</BBUSIN15> 
						<BBUSFT3>1.00000</BBUSFT3> 
  						<BBUSFT4>1.00000</BBUSFT4> 
						<BBUSFT5>1.00000</BBUSFT5> 
						<BBUSFT6>1.00000</BBUSFT6> 
						<BBUSFT7>1.00000</BBUSFT7> 
						<BBUSFT8>1.00000</BBUSFT8> 
  					</xsl:when>
  				</xsl:choose>
				<xsl:choose>
					<xsl:when test="$InsLine='CF'">
						<BBALWFVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/CommlCoverage[CoverageCd='BUSINCDP']/Limit/FormatCurrencyAmt/Amt,'$,,','')" /> 
  						</BBALWFVAL>
						<BBALWGVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/CommlCoverage[CoverageCd='BUSINCEE']/Limit/FormatCurrencyAmt/Amt,'$,,','')" /> 
  						</BBALWGVAL>
						<BBALWHVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/CommlCoverage[CoverageCd='OUTSDE']/Limit/FormatCurrencyAmt/Amt,'$,,','')" /> 
  						</BBALWHVAL>
						<BBALWIVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/CommlCoverage[CoverageCd='INSTMS']/Limit/FormatCurrencyAmt/Amt,'$,,','')" /> 
  						</BBALWIVAL>
						<BBALWJVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/CommlCoverage[CoverageCd='FORGERY']/Limit/FormatCurrencyAmt/Amt,'$,,','')" /> 
  						</BBALWJVAL>
						<BBALWKVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/CommlCoverage[CoverageCd='EMPDIS']/Limit/FormatCurrencyAmt/Amt,'$,,','')" /> 
  						</BBALWKVAL>
						<BBALWLVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/CommlCoverage[CoverageCd='SIGN']/Limit/FormatCurrencyAmt/Amt,'$,,','')" /> 
  						</BBALWLVAL>
						<BBALWMVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/CommlCoverage[CoverageCd='EDP']/Limit/FormatCurrencyAmt/Amt,'$,,','')" /> 
  						</BBALWMVAL>
						<BBALWNVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/CommlCoverage[CoverageCd='ACCTS']/Limit/FormatCurrencyAmt/Amt,'$,,','')" /> 
  						</BBALWNVAL>
						<BBALWOVAL>
  							<xsl:value-of select="translate(PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EQPBRK']/com.csc_EquipBreakdownPrem,'$,,','')" /> 
  						</BBALWOVAL>
						<BBAL1WCDE>
  							<xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/com.csc_ExpandedPropNumLocations" /> 
  						</BBAL1WCDE>
						<BBAL1XCDE>
  							<xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/com.csc_ExpandedBusinessIncomeInd" /> 
  						</BBAL1XCDE>
						<BBAL1YCDE>
  							<xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/com.csc_ExpandedCrimeInd" /> 
  						</BBAL1YCDE>
						<BBAL1ZCDE>
  							<xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/com.csc_ExpandedInlandMarineInd" /> 
  						</BBAL1ZCDE>
						<BBUSIN15>
  							<xsl:value-of select="PropertyInfo/CommlPropertyInfo[SubjectInsuranceCd='EXPPROP']/com.csc_ExpandedSpecialInd" /> 
  						</BBUSIN15>
  					</xsl:when>
					<xsl:when test="$InsLine='CA'">
						<xsl:choose>
							<xsl:when test="$RateState='NM'">
								<BBAL1YCDE>
  									<xsl:value-of select="../CommlCoverage[CoverageCd='UM']/Option[OptionCd='com.csc_UMNonStacked']/OptionValue" /> 
  								</BBAL1YCDE>
  							</xsl:when>
							<xsl:otherwise>
  								<BBAL1YCDE>N</BBAL1YCDE> 
  							</xsl:otherwise>
  						</xsl:choose>
  					</xsl:when>
  					<xsl:otherwise /> 
			      </xsl:choose>
				<!-- Issue 59878 End -->
				<!-- 59878 starts -->
				<xsl:variable name="BBAL1ZCDE">
					<xsl:choose>
						<xsl:when test="$InsLine='IMC'">
							<xsl:choose>
								<xsl:when test="com.csc_CertifiedActsOfTerrorism/com.csc_ExcludeCertifiedActsOfTerrorism='1'">N</xsl:when>
								<xsl:otherwise>Y</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="$InsLine='CR'">
							<xsl:choose>
								<xsl:when test="com.csc_CertifiedActsOfTerrorism/com.csc_ExcludeCertifiedActsOfTerrorism='1'">N</xsl:when>
								<xsl:otherwise>Y</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					</xsl:choose>
				</xsl:variable>
				<BBAL1ZCDE>
					<xsl:value-of select="$BBAL1ZCDE"/>
				</BBAL1ZCDE>
				<xsl:variable name="BBAL1UCDE">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:variable name="test" select="."/>
							<xsl:value-of select="../TaxCodeInfo/CntyTaxCd"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="//Location/TaxCodeInfo/CntyTaxCd"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAL1UCDE>
					<xsl:value-of select="$BBAL1UCDE"/>
				</BBAL1UCDE>
				<xsl:variable name="BBAL1VCDE">
					<xsl:choose>
						<xsl:when test="$InsLine='CA'">
							<xsl:value-of select="../TaxCodeInfo/TaxCd"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="//Location/TaxCodeInfo/TaxCd"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<BBAL1VCDE>
					<xsl:value-of select="$BBAL1VCDE"/>
				</BBAL1VCDE>
				<xsl:choose>
					<xsl:when test="$RateState = 'TX'">
						<BBUSIN14>N</BBUSIN14>
					</xsl:when>
					<xsl:otherwise>
						<BBUSIN14>Y</BBUSIN14>
					</xsl:otherwise>
				</xsl:choose>
				<!-- 59878 ends -->
				<!--
<xsl:if test="$InsLine='CA'">
<xsl:variable name="Hired" select="CommlAutoHiredInfo"/>
<BBCIVA><xsl:value-of select="translate($Hired/HiredLiabilityCostAmt/Amt,'$,,','')" /></BBCIVA>
<xsl:for-each select="$Hired/CommlCoverage">
<xsl:if test="CoverageCd='CMP'">
<BBPOTX><xsl:value-of select="translate(Deductible/FormatCurrencyAmt/Amt,'$,,','')" /></BBPOTX>
</xsl:if>
<xsl:if test="CoverageCd='COL'">
<BBEGCD><xsl:value-of select="translate(Deductible/FormatCurrencyAmt/Amt,'$,,','')" /></BBEGCD>
</xsl:if>
<xsl:if test="CoverageCd='HSP'">
<BBEKCD>Y</BBEKCD>
</xsl:if>
</xsl:for-each>
<xsl:variable name="NonOwned" select="CommlAutoNonOwnedInfo/NonOwnedInfo"/>
<BBBKNB><xsl:value-of select="$NonOwned/NumNonOwned" /></BBBKNB>
<xsl:for-each select="$NonOwned/CommlCoverage">
<xsl:if test="CoverageCd='UM'">
<BBPNTX><xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')" /></BBPNTX>
</xsl:if>
<xsl:if test="CoverageCd='UN'">
<BBPPTX><xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')" /></BBPPTX>
</xsl:if>
</xsl:for-each>
<xsl:variable name="DOC" select="CommlAutoDriveOtherCarInfo"/>
<BBUSNB3><xsl:value-of select="$DOC/NumIndividualsCovered" /></BBUSNB3>
<xsl:for-each select="$DOC/CommlCoverage">
<xsl:if test="CoverageCd='CMP'">
<BBI3TX><xsl:value-of select="Option/OptionValue" /></BBI3TX>
</xsl:if>
<xsl:if test="CoverageCd='LIA'">
<BBI0TX><xsl:value-of select="Option/OptionValue" /></BBI0TX>
</xsl:if>
<xsl:if test="CoverageCd='MED'">
<BBEJCD><xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')" /></BBEJCD>
</xsl:if>
<xsl:if test="CoverageCd='DUM'">
<BBIZTX><xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')" /></BBIZTX>
</xsl:if>
<xsl:if test="CoverageCd='DCO'">
<BBI4TX><xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')" /></BBI4TX>
</xsl:if>
<xsl:if test="CoverageCd='DUN'">
<BBIYTX><xsl:value-of select="translate(Limit/FormatCurrencyAmt/Amt,'$,,','')" /></BBIYTX>
</xsl:if>
</xsl:for-each>
</xsl:if>-->
			</ASBBCPL1__RECORD>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->