<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	
	<xsl:template name="CreatePersVeh">
		<PersVeh>
		    <xsl:variable name="VehNbr" select="BJAENB"/>
                <xsl:attribute name="id"><xsl:value-of select="concat('v',$VehNbr)"/></xsl:attribute>
                <xsl:attribute name="RatedDriverRef"><xsl:value-of select="concat('d',BJAKNB)"/></xsl:attribute>                
         	    <ItemIdInfo>
         	       <InsurerId><xsl:value-of select="$VehNbr"/></InsurerId>
        	    </ItemIdInfo>
               <Manufacturer><!-- 33385 Begin -->
			   	   <xsl:choose>
				   		<xsl:when test="string-length(substring-before(BJAXTX,' ')) = 0">
                   			<xsl:value-of select="BJAXTX"/>
				   		</xsl:when>
				   		<xsl:otherwise>
							<xsl:value-of select="substring-before(BJAXTX,' ')"/>
				   		</xsl:otherwise>
				   </xsl:choose><!-- 33385 End -->
                       
               </Manufacturer>
               <Model>
                   <xsl:value-of select="substring-after(BJAXTX,' ')"/>               
               </Model>
               <ModelYear>
                   <xsl:value-of select="BJAHNB"/>
               </ModelYear> 
               <VehBodyTypeCd>
                   <xsl:value-of select="BJAZTX"/>
               </VehBodyTypeCd>
               <CostNewAmt>
                    <Amt><!-- 33385 Begin -->
			   	   <xsl:choose>
				   		<xsl:when test="string-length(substring-before(BJAINB,'.')) = 0">
                   			<xsl:value-of select="BJAINB"/>
				   		</xsl:when>
				   		<xsl:otherwise>
							<xsl:value-of select="substring-before(BJAINB,'.')"/>
				   		</xsl:otherwise>
				   </xsl:choose><!-- 33385 End -->                    
                  </Amt>
               </CostNewAmt>  
               <NumDaysDrivenPerWeek>
                   <xsl:value-of select="BJANNB"/>                
               </NumDaysDrivenPerWeek>
               <EstimatedAnnualDistance>
                  <NumUnits>
                   <xsl:value-of select="BJAONB"/>                       
                  </NumUnits>
                  <UnitMeasurementCd>MI</UnitMeasurementCd>
               </EstimatedAnnualDistance>
               <FullTermAmt>
                  <Amt/> 
               </FullTermAmt>
	       <NumCylinders>
			<xsl:value-of select="BJAXNB"/>
	       </NumCylinders>
               <TerritoryCd>
                   <xsl:value-of select="BJAGNB"/>           
               </TerritoryCd>
               <VehIdentificationNumber>
                   <xsl:value-of select="BJAYTX"/>
               </VehIdentificationNumber>
               <VehSymbolCd>
                   <xsl:value-of select="BJAJNB"/>                                   
               </VehSymbolCd>
            <xsl:for-each select="/*/PMSP1200__RECORD[$VehNbr = substring(USE0LOC, string-length(USE0LOC)-string-length($VehNbr)+1, string-length($VehNbr))]">
                <xsl:call-template name="CreateAdditionalInterest">
					<xsl:with-param name="LOBCd">APV</xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
                <AntiLockBrakeCd>
                   <xsl:value-of select="BJCYST"/>
                </AntiLockBrakeCd>                                                
			<DaytimeRunningLightInd><!-- 33385 Begin -->
                   <xsl:value-of select="BJI4TX"/>
               </DaytimeRunningLightInd><!-- 33385 End -->
               <DistanceOneWay>
                  <NumUnits>
                      <xsl:value-of select="BJAMNB"/>                             
                  </NumUnits>
                  <UnitMeasurementCd>MI</UnitMeasurementCd>
               </DistanceOneWay>
               <PhysicalDamageRateClassCd>
                   <xsl:value-of select="BJAWCD"/>
               </PhysicalDamageRateClassCd>                                                    
               <AntiTheftDeviceCd>
                   <xsl:value-of select="BJI6TX"/>
               </AntiTheftDeviceCd>
               <RateClassCd>
                   <xsl:value-of select="BJAVCD"/>
               </RateClassCd>                                                                              
               <VehPerformanceCd>
                  <xsl:value-of select="BJACST"/>                           
               </VehPerformanceCd>
               <VehUseCd>
                  <xsl:value-of select="BJC5ST"/>                              
               </VehUseCd>
               <AirBagTypeCd>
                  <xsl:value-of select="BJCXST"/>                                
               </AirBagTypeCd>
		<xsl:for-each select="/*/ASBKCPL1__RECORD[($VehNbr = BKAENB and BKC7ST != 'Y')]">  <!-- 33385 -->
                  <xsl:call-template name="CreateCoverage">
                     <xsl:with-param name="LOB">APV</xsl:with-param>
                  </xsl:call-template>
              </xsl:for-each>
	      <!--start issue 39211-->
	      <com.csc_VehBodyTypeFreeformInd>
		<xsl:value-of select="BJADST"/>
	      </com.csc_VehBodyTypeFreeformInd>
	      <!--end issue 39211-->
	</PersVeh>
	</xsl:template>
</xsl:stylesheet>


<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->