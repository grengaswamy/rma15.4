<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
e-Service Case 31594 - Added code for WC Amendments
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq (Mod) - 31594
-->
  <xsl:template name="CreateISLPPP1">
    <xsl:param name="Action"/>
    <xsl:variable name="TableName">ISLPPP1</xsl:variable>
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>ISLPPP1</RECORD__NAME>
      </RECORD__NAME__ROW>
      <ISLPPP1__RECORD>
        <RECID>PP1</RECID>
        <SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICY0NUM>
          <xsl:value-of select="$POL"/>
        </POLICY0NUM>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTER0CO>
          <xsl:value-of select="$MCO"/>
        </MASTER0CO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <ACTDATE>
          <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
          </xsl:call-template>
        </ACTDATE>
        <ACTIONSEQ>0000</ACTIONSEQ>
        <AMEND0NUM>00</AMEND0NUM>
        <EFF0DATE>
		<!-- Case 31594 start -->
			<xsl:call-template name="ConvertISODateToPTDate">
				<xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
			</xsl:call-template>
		<!-- Case 31594 end -->
        </EFF0DATE>
        <EXP0DATE>
          <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="CommlPolicy/ContractTerm/ExpirationDt"/>
          </xsl:call-template>
        </EXP0DATE>
        <INSTAL0TRM>
           <xsl:call-template name="FormatData">
	   		<xsl:with-param name="FieldName">INSTAL0TRM</xsl:with-param>      
		      <xsl:with-param name="FieldLength">3</xsl:with-param>
		      <!-- 50444 Start -->
		      <xsl:with-param name="Value">
		       <xsl:choose>
				 <xsl:when test="*/com.csc_PolicyTermMonths='0'">999
				 </xsl:when>
				 <xsl:otherwise><xsl:value-of select="*/com.csc_PolicyTermMonths"/></xsl:otherwise>
				 </xsl:choose>
		      </xsl:with-param>
		      <!-- 50444 End   -->
	        <xsl:with-param name="FieldType">N</xsl:with-param>
	    </xsl:call-template> 
        </INSTAL0TRM>
        <RISK0STATE>
          <xsl:value-of select="WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef='l1']/StateProvCd"/>
        </RISK0STATE>
        <COMPANY0NO>
          <xsl:value-of select="$PCO"/>
        </COMPANY0NO>
        <AGENTNO>
          <xsl:value-of select="Producer/ItemIdInfo/AgencyId"/>
        </AGENTNO>
        <ENTER0DATE>
		<!-- Case 31594 start -->
		<xsl:choose>
		<xsl:when test="$TYPEACT='EN'">
		  <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="TransactionEffectiveDt"/>
          </xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="CommlPolicy/ContractTerm/ExpirationDt"/>
          </xsl:call-template>
		</xsl:otherwise>
		</xsl:choose>
		<!-- Case 31594 end -->
        </ENTER0DATE>
        <TOT0AG0PRM>00000000000</TOT0AG0PRM>
        <LINE0BUS>
		  <!--<xsl:value-of select="$SYS"/>  -->  <!--issue # 40090-->
          <xsl:value-of select="$LOB"/>    <!--issue # 40090-->
        </LINE0BUS>
        <ISSUE0CODE>Q</ISSUE0CODE>
        <PAY0CODE>
          <xsl:value-of select="substring(CommlPolicy/PaymentOption/PaymentPlanCd,1,1)"/>
        </PAY0CODE>
        <MODE0CODE>
          <xsl:choose>
            <xsl:when test="string-length(CommlPolicy/PaymentOption/PaymentPlanCd) > 0">
              <xsl:value-of select="substring(CommlPolicy/PaymentOption/PaymentPlanCd,2,1)"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </MODE0CODE>
        <AUDIT0CODE>
          <xsl:value-of select="CommlPolicy/CommlPolicySupplement/AuditFrequencyCd"/>
        </AUDIT0CODE>
        <KIND0CODE>D</KIND0CODE>
        <SORT0NAME>
          <xsl:value-of select="substring(InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName,1,4)"/>
        </SORT0NAME>
        <RENEWAL0CD>0</RENEWAL0CD>
        <REASONAMD>
        </REASONAMD>
        <RENEW0PAY>D<xsl:value-of select="CommlPolicy/PaymentOption/PaymentPlanCd"/>
        </RENEW0PAY>
        <RENEW0MODE>0<xsl:value-of select="CommlPolicy/PaymentOption/MethodPaymentCd"/>
        </RENEW0MODE>
        <RN0POL0SYM>
        </RN0POL0SYM>
        <RN0POL0NUM>
        </RN0POL0NUM>
        <ORI0INCEPT>
          <xsl:call-template name="ConvertISODateToPTDate">
            <xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
          </xsl:call-template>
        </ORI0INCEPT>
        <ZIP0POST>
          <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode"/>
        </ZIP0POST>
        <INS0NAME>
          <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
        </INS0NAME>
        <!-- This is the Optional line on POINT -->
        <ADD0LINE01>
          <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/SupplementaryNameInfo/SupplementaryName"/>
        </ADD0LINE01>
        <ADD0LINE02>
          <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1"/>
        </ADD0LINE02>
        <CITY0STATE>
          <xsl:value-of select="substring(concat(InsuredOrPrincipal/GeneralPartyInfo/Addr/City, '                                                                                                    '), 1, 28)"/>
          <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd"/>
        </CITY0STATE>
        <TYPE0ACT>
          <xsl:choose>
            <xsl:when test="$Action = 'Quote'">QQ</xsl:when>
             
           	<!--31594 start-->
			<xsl:when test="$Action = 'Mod'">EN</xsl:when> 

            <!-- Case 36925	Starts -->
            <xsl:when test="TransactionEffectiveDt = '01/01/1900'">NB</xsl:when>
            
            <xsl:when test="TransactionEffectiveDt = ''">NB</xsl:when>
            <!-- Case 36925 Ends -->
	
		  	<xsl:when test="TransactionEffectiveDt">EN</xsl:when>   
	        <!--31594 end -->

            <xsl:otherwise>NB</xsl:otherwise>
          </xsl:choose>
        </TYPE0ACT>
        <ELACC>
          <xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='EachClaim']/FormatCurrencyAmt/Amt) div 1000"/>
        </ELACC>
        <ELDISELIMT>
          <xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='EachEmployee']/FormatCurrencyAmt/Amt) div 1000"/>
        </ELDISELIMT>
        <ELPOLLIMIT>
          <xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='WCEL']/Limit[LimitAppliesToCd='PolicyLimit']/FormatCurrencyAmt/Amt) div 1000"/>
        </ELPOLLIMIT>
        <!-- Case 34003 Start  Jeff Simmons - Changed code to divide voluntary limits by 1000 to correct formating issue
        <VCACCLIMIT>
          <xsl:value-of select="WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='EachClaim']/FormatCurrencyAmt/Amt"/>
        </VCACCLIMIT>
        <VCDISLIMIT>
          <xsl:value-of select="WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='EachEmployee']/FormatCurrencyAmt/Amt"/>
        </VCDISLIMIT>
        <VCPOLLIMIT>
          <xsl:value-of select="WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='PolicyLimit']/FormatCurrencyAmt/Amt"/>
        </VCPOLLIMIT> -->
        <VCACCLIMIT>
          <xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='EachClaim']/FormatCurrencyAmt/Amt) div 1000"/>
        </VCACCLIMIT>
        <VCDISLIMIT>
          <xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='EachEmployee']/FormatCurrencyAmt/Amt) div 1000"/>
        </VCDISLIMIT>
        <VCPOLLIMIT>
          <xsl:value-of select="number(WorkCompLineBusiness/CommlCoverage[CoverageCd='VOL']/Limit[LimitAppliesToCd='PolicyLimit']/FormatCurrencyAmt/Amt) div 1000"/>
        </VCPOLLIMIT>
        <!-- Case 34003 End   Jeff Simmons  -->
        <ARDATE>
          <xsl:call-template name="ConvertISODateToPTDate">
            <!-- Case 39432 Start -->
          <!--  <xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/> -->
            <xsl:with-param name="Value" select="WorkCompLineBusiness/WorkCompRateState/AnniversaryRatingDt"/>
            
          <!-- Case 39432 End   -->  
          </xsl:call-template>
        </ARDATE>
        <POLPREMRET>00000000000</POLPREMRET>
        <MINPREMRET>00000000000</MINPREMRET>
        <LOSSCONSTR>00000000000</LOSSCONSTR>
        <EXPSCONSTR>00000000000</EXPSCONSTR>
        <DEPPREMRET>00000000000</DEPPREMRET>
        <PREMDISCNT>00000000000</PREMDISCNT>
        <PREMDISPER>000000</PREMDISPER>
        <EMPLIABRET>00000000000</EMPLIABRET>
        <EMPMINPREM>00000000000</EMPMINPREM>
        <VOLCOMPPRM>00000000000</VOLCOMPPRM>
        <TOTCOMMRET>00000000000</TOTCOMMRET>
        <LEGENTTYPE>
          <xsl:value-of select="InsuredOrPrincipal/GeneralPartyInfo/NameInfo/LegalEntityCd"/>
        </LEGENTTYPE>
        <FEIN>
          <xsl:value-of select="InsuredOrPrincipal[@id='n0']/GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/>
        </FEIN>
        <POOLID>000000000</POOLID>
        <OTHERBUR>
          <xsl:value-of select="InsuredOrPrincipal[@id='n0']/InsuredOrPrincipalInfo/BusinessInfo/com.csc_NCCIIDNumber"/>
        </OTHERBUR>
        <PREVCARR>
        </PREVCARR>
        <PICPOLNO>
        </PICPOLNO>
        <PREVEFF>
        </PREVEFF>
        <COMMPCT>000</COMMPCT>
        <!-- Case 33785 Start -->
        <CUST0NO>
        	<xsl:value-of select="CommlPolicy/com.csc_CustomerNumber"/>
        </CUST0NO>
        <!-- Case 33785 End   -->
      </ISLPPP1__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->