<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- 
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform ACORD XML into POINT segments
E-Service case 14734 
***********************************************************************************************

The base xpath for this segment:
/ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/Location (Quote)
/ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/Location (Mod) - 31594
-->
  <xsl:template name="CreatePMSPWC07">
    <xsl:variable name="TableName">PMSPWC07</xsl:variable>
    <xsl:variable name="LocationId" select="@id"/>
    <xsl:variable name="LocationNumber" select="substring(@id,2,string-length(@id)-1)"/>
    <xsl:variable name="EffDate" select="CommlPolicy/ContractTerm/EffectiveDt"/>
    <xsl:variable name="AnvDate" select="WorkCompLineBusiness/WorkCompRateState/AnniversaryRatingDt"/>
    <xsl:variable name="LocState" select="Addr/StateProvCd"/><!-- Issue 63993 -->
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPWC07</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPWC07__RECORD>
		<WCSTATUS>P</WCSTATUS>
		<SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <STATE>
          <xsl:value-of select="Addr/StateProvCd"/>
        </STATE>
		<ESCNO><xsl:value-of select="../WorkCompLineBusiness/WorkCompRateState[StateProvCd=$LocState]/NCCIIDNumber"/></ESCNO><!-- Issue 63993 added value of -->
		<EXPMOD> 
			<xsl:call-template name="FormatData">
            	<xsl:with-param name="FieldName">SITE</xsl:with-param>
            	<xsl:with-param name="FieldLength">4</xsl:with-param>
            	<xsl:with-param name="Precision">3</xsl:with-param>
            	<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/CreditOrSurcharge[CreditSurchargeCd='EXP1']/NumericValue/FormatModFactor"/>
            	<xsl:with-param name="FieldType">N</xsl:with-param>
          	</xsl:call-template>
        </EXPMOD>
		<xsl:choose>
			<xsl:when test="$EffDate &gt; $AnvDate">
	    		<ANVRATE> 
	    		<!-- 64892 Start -->  			
	    		<!--<xsl:with-param name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/> -->
		   		<xsl:variable name="Value" select="CommlPolicy/ContractTerm/EffectiveDt"/>
		   		<!-- 64892 End   -->
					<xsl:call-template name="FormatData">
            			<xsl:with-param name="FieldName">SITE</xsl:with-param>
            			<xsl:with-param name="FieldLength">4</xsl:with-param>
            			<xsl:with-param name="Precision">3</xsl:with-param>
            			<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/WorkCompLineBusiness/WorkCompRateState[WorkCompLocInfo/@LocationRef=$LocationId]/CreditOrSurcharge[CreditSurchargeCd='EXP2']/NumericValue/FormatModFactor"/>
            			<xsl:with-param name="FieldType">N</xsl:with-param>
          			</xsl:call-template>
          		</ANVRATE>
          	</xsl:when>	
           	<xsl:otherwise>
           		<ANVRATE>0</ANVRATE>
			</xsl:otherwise>
		</xsl:choose>
     	<ENDEFFDATE></ENDEFFDATE>
		<ARAPMOD>0</ARAPMOD>
		<ARAPRATE>0</ARAPRATE>
		<LGEDEDAMT>00000</LGEDEDAMT>
		<LDEDBAEXP></LDEDBAEXP>
		<LDEDRATE>1</LDEDRATE>
		<DEDTYPE></DEDTYPE>
		<DEDPCNT>00</DEDPCNT>
		<DEDAGGR>000000000</DEDAGGR>
		<DEDAMT>000000000</DEDAMT>
		<PDTABLE>
		<!-- 29653 Start -->
		<xsl:call-template name="FormatData">
        		<xsl:with-param name="FieldName">PDTABLE</xsl:with-param>
        		<xsl:with-param name="FieldLength">2</xsl:with-param>
        		<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/WorkCompLineBusiness/WorkCompRateState/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TableNumber']/OtherId"/>
        		<xsl:with-param name="FieldType">N</xsl:with-param>
      		</xsl:call-template>
		<!-- 29653 End -->
		</PDTABLE>
		<PDTABLE2></PDTABLE2>
            <!-- 29653 Start -->
		<!--<PDSLIND></PDSLIND> -->
	      <!--<PDSLIND><xsl:value-of select="../com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_FormInd']/OtherId"/></PDSLIND> -->
<PDSLIND><xsl:value-of select="/ACORD/InsuranceSvcRq/*/WorkCompLineBusiness/WorkCompRateState/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_FormInd']/OtherId"/></PDSLIND>
            <!-- 29653 End  -->
		<PDSLIND2></PDSLIND2>
		<PDOVRPCT>000000</PDOVRPCT>
		<PDOVRPCT2>000000</PDOVRPCT2>
		<PDOVRIND></PDOVRIND>
		<PDOVRIND2></PDOVRIND2>
	</PMSPWC07__RECORD>
    </BUS__OBJ__RECORD>
  </xsl:template>

  <!--
  The base xpath for this segment:
  /ACORD/InsuranceSvcRq/WorkCompPolicyQuoteInqRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Quote)
  /ACORD/InsuranceSvcRq/com.csc_WorkCompPolicyModRq/WorkCompLineBusiness/WorkCompRateState/WorkCompLocState (Mod) - 31594
  -->
  
<!-- Case 31594 begin - Modified the template to build one WC07 segement for each location -->
<xsl:template name="CreatePMSPWC07Defaults">
    <xsl:variable name="TableName">PMSPWC07</xsl:variable>
	<xsl:variable name="LocationNbr" select="@LocationRef"/>
	<xsl:variable name="LocationPath" select="/ACORD/InsuranceSvcRq/*/Location[@id=$LocationNbr]/Addr"/>
	<!--<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_AmendmentMode"/>-->		<!--103409-->
	<xsl:variable name="TranType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/PolicyStatusCd"/>				<!--103409-->
	<!--issue # 64227 starts-->
    <!--<xsl:variable name="EffDate" select="CommlPolicy/ContractTerm/EffectiveDt"/>
    <xsl:variable name="AnvDate" select="WorkCompLineBusiness/WorkCompRateState/AnniversaryRatingDt"/>-->
	  <xsl:variable name="EffDate" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/ContractTerm/EffectiveDt"/>
	   <xsl:variable name="AnvDate" select="../AnniversaryRatingDt"/>
	<!--issue # 64227 ends-->
	<xsl:if test="$TranType != 'A' or $LocationNbr != 'l0'">
    <BUS__OBJ__RECORD>
      <RECORD__NAME__ROW>
        <RECORD__NAME>PMSPWC07</RECORD__NAME>
      </RECORD__NAME__ROW>
      <PMSPWC07__RECORD>
		<WCSTATUS>P</WCSTATUS>
		<SYMBOL>
          <xsl:value-of select="$SYM"/>
        </SYMBOL>
        <POLICYNO>
          <xsl:value-of select="$POL"/>
        </POLICYNO>
        <MODULE>
          <xsl:value-of select="$MOD"/>
        </MODULE>
        <MASTERCO>
          <xsl:value-of select="$MCO"/>
        </MASTERCO>
        <LOCATION>
          <xsl:value-of select="$LOC"/>
        </LOCATION>
        <STATE>
          <xsl:value-of select="../StateProvCd"/>
        </STATE>
		<ESCNO><xsl:value-of select="../NCCIIDNumber"/></ESCNO><!-- Issue 63993 added value of -->
		<EXPMOD> 
				<!--Issue # 64227 starts-->		
			<!--<xsl:call-template name="FormatData">
            	<xsl:with-param name="FieldName">EXPMODE1</xsl:with-param>
		<xsl:with-param name="FieldLength">4</xsl:with-param> 
            	<xsl:with-param name="Precision">3</xsl:with-param> 
            	<xsl:with-param name="Value" select="../CreditOrSurcharge[CreditSurchargeCd='EXP1']/NumericValue/FormatModFactor"/>
            	<xsl:with-param name="FieldType">N</xsl:with-param>
          	</xsl:call-template>-->
            	<xsl:value-of select="../CreditOrSurcharge[CreditSurchargeCd='EXP1']/NumericValue/FormatModFactor"/>
		<!--Issue # 64227 ends-->
        </EXPMOD>
		<xsl:choose>
			<!--<xsl:when test="$EffDate &gt; $AnvDate">--> <!--Issue # 64227-->
	    	<xsl:when test="$EffDate != $AnvDate"> <!--Issue # 64227-->
	    		<ANVRATE>   			
				<!--Issue # 64227 starts-->			
		   		 <!-- 	<xsl:call-template name="FormatData">
            			<xsl:with-param name="FieldName">EXPMODE2</xsl:with-param>
            			<xsl:with-param name="FieldLength">4</xsl:with-param> 
            			<xsl:with-param name="Precision">3</xsl:with-param> 
            			<xsl:with-param name="Value" select="../CreditOrSurcharge[CreditSurchargeCd='EXP2']/NumericValue/FormatModFactor"/>
            			<xsl:with-param name="FieldType">N</xsl:with-param>
          			</xsl:call-template>-->
            			<xsl:value-of select="../CreditOrSurcharge[CreditSurchargeCd='EXP2']/NumericValue/FormatModFactor"/>
				<!--Issue # 64227 ends-->			
          		</ANVRATE>
          	</xsl:when>		
           	<xsl:otherwise>
           		<ANVRATE>0</ANVRATE>
			</xsl:otherwise>
		</xsl:choose>
     	<ENDEFFDATE></ENDEFFDATE>
		<ARAPMOD>0</ARAPMOD>
		<ARAPRATE>0</ARAPRATE>
		<LGEDEDAMT>00000</LGEDEDAMT>
		<LDEDBAEXP></LDEDBAEXP>
		<LDEDRATE>1</LDEDRATE>
		<DEDTYPE></DEDTYPE>
		<DEDPCNT>00</DEDPCNT>
		<DEDAGGR>000000000</DEDAGGR>
		<DEDAMT>000000000</DEDAMT>
		<PDTABLE>
      		<xsl:call-template name="FormatData">
        		<xsl:with-param name="FieldName">PDTABLE</xsl:with-param>
        		<xsl:with-param name="FieldLength">2</xsl:with-param>
        		<xsl:with-param name="Value" select="../com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TableNumber']/OtherId"/>
        		<xsl:with-param name="FieldType">N</xsl:with-param>
      		</xsl:call-template>
		</PDTABLE>
		<PDTABLE2></PDTABLE2>
		<PDSLIND><xsl:value-of select="../com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_FormInd']/OtherId"/></PDSLIND>
		<PDSLIND2></PDSLIND2>
		<PDOVRPCT>000000</PDOVRPCT>
		<PDOVRPCT2>000000</PDOVRPCT2>
		<PDOVRIND></PDOVRIND>
		<PDOVRIND2></PDOVRIND2>
	</PMSPWC07__RECORD>
    </BUS__OBJ__RECORD>
    </xsl:if>
  </xsl:template>
  <!-- Case 31594 end -->


</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2005. Progress Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/><MapperBlockPosition></MapperBlockPosition><TemplateContext></TemplateContext></MapperMetaTag>
</metaInformation>
--><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->