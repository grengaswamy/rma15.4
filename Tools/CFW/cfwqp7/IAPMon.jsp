<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- ------------------------------------------------------------------- --%>
<%-- This is a JSP to configure and maintain the IAP Listener.           --%>
<%-- ------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html"%>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="org.w3c.dom.Document" %>
<%@ page import="com.csc.cfw.iap.acp.ACPListener" %>
<%@ page import="com.csc.cfw.util.CommFwSystem" %>
<%@ page import="com.csc.fw.util.Configurable" %>
<%@ page import="com.csc.fw.util.FwException" %>
<%@ page import="com.csc.fw.util.FwSystem" %>
<%@ page import="com.csc.fw.util.PropertyFileManager" %>
<%@ page import="com.csc.fw.util.ServletUtil" %>
<%@ page errorPage="error.jsp" %>

<HTML>
  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <HEAD>
    <TITLE>IAP Monitor - Communications Framework </TITLE>
  </HEAD>
  <BODY BGCOLOR="#C0C0C0">
    

    <DIV ALIGN="Center" ID="HtmlDiv1">
    <TABLE>
      <TR>
        <TD ALIGN="Center"  >
          <H4>IAP Monitor</H4>
        </TD>
      </TR>
      <TR>
        <TD  >

<%
    FwSystem fwSys = CommFwSystem.current();
    String updateTextBuffer = null;
    // Control File Section - Determine the section to display
    boolean bIE = (ServletUtil.determineBrowserVersion(request).getType() == ServletUtil.BROWSER_MSIE);
    
    /**********************************/
    /* Check functions being executed */
    /**********************************/
    String func = request.getParameter("function");
    if (func != null && func.length() > 0)
    {
        if ("Shutdown IAP Listener".equalsIgnoreCase(func))
        {
            ACPListener.stopACPListener();
        }
        else
        if ("Restart IAP Listener".equalsIgnoreCase(func))
        {
            ACPListener.startACPListener();
        }
        else
        if ("Suspend".equalsIgnoreCase(func) || "Resume".equalsIgnoreCase(func))
        {
        	try
        	{
                //int idx = Integer.parseInt(request.getParameter("suspendIdx").toString());
                //Object[] configList = ProcessDaemon.configEntryKeys(null);
                //String name = configList[idx].toString();
                //ProcessDaemon daemon = (ProcessDaemon)ProcessDaemon.configEntryMap(null, null).get(name);
                //daemon.setSuspended("Suspend".equalsIgnoreCase(func));
        	}
        	catch (Exception e) { }
        }
    }
%>
          <P />
          <TABLE>
            <TR ALIGN="Center">
             <TD ALIGN="Center"  >
              <TABLE>

                <TR>
                  <TD WIDTH="100%"  >
                    <TABLE cellspacing="0" WIDTH="100%">
                      <TR>
                        <TD COLSPAN="6"><HR WIDTH="100%"></TD>
                      </TR>
                      
<%
					List connections = ACPListener.getListenerConnections();
					if (connections != null)
					{
%>
                      <!-- Start Display List of Connections -->
                      <TR>
                        <TD WIDTH="100%">
                          <h3><B>IAP Connections:</B></h3>
                          <table class="PAGEHLIGHT" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <th align="left" width="20%">Thread ID</th>
                              <th align="left" width="20%">State</th>
                              <th align="center" width="10%">Caller Address</th>
                              <th align="center" width="10%">Caller Host</th>
                              <th align="center" width="20%">Start Time</th>
                              <th align="center" width="20%">Time</th>
                            </tr>
<%
				String colorClass = null;
                ACPListener.Connection connection;
				Object[] connList = connections.toArray();
				for (int i = 0; i < connList.length; i++)
				{
					ACPListener.Connection conn = (ACPListener.Connection)connList[i];
					String name = conn.toString();
					String state = conn.getConnStateAsString();
					Date startTime = conn.getStartTime();
					Calendar cal = Calendar.getInstance();
					cal.setTime(startTime);
			        String startDt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(cal.getTime());//$NON-NLS-1$
			        
					colorClass = "SRE";
%>
                            <tr>
                              <td class="DARKBACK" width="100%" colspan="6"><img src="includes/spacer.gif" width="100%" height="1px"/></td>
                            </tr>
                            <tr>
                              <td align="left" width="20%" valign="middle"><b><%=name%></b></td>
                              <td align="left" width="20%" valign="middle"><b><%= state %></b></td>
                              <td align="right" width="10%" valign="middle"><b><%=conn.getHostAddress()%></b></td>
                              <td align="right" width="10%" valign="middle"><b><%=conn.getHostName()%>&nbsp;&nbsp;</b></td>
                              <td align="right" width="20%" valign="middle"><b><%= startDt %></b></td>
                              <td align="right" width="20%" valign="middle"><b><%=new Date().getTime() - startTime.getTime() %></b></td>
                            </tr>
<%
				}
%>
                          </table>
                        </TD>
                      </TR>
                      <!--  End Display List of Connections -->
<%
					}
%>
                      
                    </TABLE>
                  </TD>
                </TR>
                <TR>
                  <TD COLSPAN="3"><HR WIDTH="100%"></TD>
                </TR>

                <!-- IAP Monitor Log -->
                <tr>
                  <td>
                   <div align="center">
                    <b>IAP Monitor Log Display</b><br />
                    <textarea cols="80" wrap="OFF" rows="25"><%=ACPListener.getDisplayLogContents()%></textarea>
                   </div>
                   <br />
                   <form name="IAPMonAdminForm" action="IAPMon.jsp" method="POST" >
                     <input class="btnNoSize" type="submit" name="function" value="Shutdown IAP Listener" />
                     <input class="btnNoSize" type="submit" name="function" value="Restart IAP Listener" />
                   </form>
                  </td>
                </tr>


              </TABLE>
              </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
    </TABLE>

    </DIV>
  </BODY>
</HTML>
