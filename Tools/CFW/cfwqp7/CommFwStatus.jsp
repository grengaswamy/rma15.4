<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- ******************************************************************* --%>
<%-- * This is a JSP to show the Framework Utilities status.           * --%>
<%-- ******************************************************************* --%>
<%@ page language="java" contentType="text/html"%>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.reflect.Method" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="com.csc.fw.util.FwEnvelope" %>
<%@ page import="com.csc.fw.util.FwEvent" %>
<%@ page import="com.csc.fw.util.FwSecurityManager" %>
<%@ page import="com.csc.fw.util.FwSystem" %>
<%@ page import="com.csc.fw.util.Logger" %>
<%@ page import="com.csc.fw.util.LRUCache" %>
<%@ page import="com.csc.fw.util.PropertyFileConfigurator" %>
<%@ page import="com.csc.fw.util.PropertyFileManager" %>
<%@ page import="com.csc.fw.util.ServletUtil" %>
<%@ page errorPage="error.jsp" %>

<%@ include file="WEB-INF/jspf/FwSysFunc.jspf" %>
<%@include file="WEB-INF/jspf/FwSysInit.jspf" %>
<%

        jspDispLinks = false;
        String jspTitle = jspTitleStat;
        String jspHeader = jspHeaderStat;
        String jspLinkName = jspLinkNameStat;
        String jspLinkTitle = jspLinkTitleStat;
        
        //Detail Cache list flag
        boolean fCacheDetailList = false;
        boolean eventWindow = false;
        String eventId = null;
        long eventTimestamp = 0;
        
        //Boolean variable for Aux windows
        boolean traceWindow = false;
        boolean sysErrorWindow = false;
        boolean traceListDetailWindow = false;
        boolean sysErrorListDetailWindow = false;
        
        // Main Window flags
        boolean dispFileImage = false;
        boolean editPropWindow = false;
        boolean statusWindow = true;
        boolean javaPropWindow = true;
        boolean traceSummaryWindow = true;
        boolean sysErrorSummaryWindow = true;
        boolean ccfWindow = true;
        boolean as400Window = true;
        
        int sysErrorPurgeDays = 15;
        String strSysErrorPurgeDays = (String)request.getParameter("sysErrorPurgeDays");
        if (strSysErrorPurgeDays != null)
            try
            {
                sysErrorPurgeDays = Integer.parseInt(strSysErrorPurgeDays);
            }
            catch (Exception e)
            {
            }
        
        int tracePurgeDays = 15;
        String strTracePurgeDays = (String)request.getParameter("tracePurgeDays");
        if (strTracePurgeDays != null)
            try
            {
                tracePurgeDays = Integer.parseInt(strTracePurgeDays);
            }
            catch (Exception e)
            {
            }
        
        String s = request.getParameter("eventWindow");
        if (s != null && s.length() > 0) 
        {
            eventWindow = true;
            eventId = request.getParameter("eventId");
            if (eventId != null)
                eventTimestamp = Long.parseLong(eventId);
            
            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for Tracing Window
        String strTraceWindow = request.getParameter("traceWindow");
        if (strTraceWindow != null && strTraceWindow.length() > 0) 
        {
            traceWindow = true;
            
            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for System Error Window
        String strSysErrorWindow = request.getParameter("sysErrorWindow");
        if (strSysErrorWindow != null && strSysErrorWindow.length() > 0) 
        {
            sysErrorWindow = true;
            
            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for Trace Log List Window
        String strTraceListDetailWindow = request.getParameter("traceListDetailWindow");
        if (strTraceListDetailWindow != null && strTraceListDetailWindow.length() > 0) 
        {
            traceListDetailWindow = true;
            
            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for System Error Log List Window
        String strSysErrorListDetailWindow = request.getParameter("sysErrorListDetailWindow");
        if (strSysErrorListDetailWindow != null && strSysErrorListDetailWindow.length() > 0) 
        {
            sysErrorListDetailWindow = true;
            
            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for File Image Window
        String strDispFileImageWindow = request.getParameter("dispFileImage");
        if (strDispFileImageWindow != null && strDispFileImageWindow.length() > 0) 
        {
            dispFileImage = true;
            String fileName = (String)request.getParameter("FileName");
            if (fileName == null)
                if (traceWindow)
                {
                    Logger lgr = fwSystem.getTraceLogger();
                    fileName = lgr.getFileName();
                }
                else
                if (sysErrorWindow)
                {
                    Logger lgr = fwSystem.getSystemErrorLogger();
                    fileName = lgr.getFileName();
                }
            
            if (fileName == null)
            {
                String err = "<FONT SIZE=+1><B>Internal error: No trace file found for display</B></FONT>";
              %>
                <HTML>
                  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
                  <BODY BGCOLOR="#C0C0C0">
                            <HR SIZE="1" noshade="noshade" WIDTH="100%" /><BR />
                            <%=err %><BR />
                            <HR SIZE="1" noshade="noshade" WIDTH="100%" />
                  </BODY>
                </HTML>
              <%                
            }
            else
            try
            {
                FileInputStream stream = new FileInputStream(fileName);
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream, FwEnvelope.INTERNAL_ENCODING));
                Writer writer = response.getWriter();
                response.setContentType("text/plain");
                char[] buff = new char[16384];
                int cnt;
                while ((cnt = reader.read(buff)) >= 0)
                {
                    writer.write(buff, 0, cnt);
                }
                        
                reader.close();
                stream.close();
                writer.close();
            } 
            catch (IOException ex)
            {
                String err = "<FONT SIZE=+1><B>Error while loading Trace File: </B></FONT>" + ex.toString();
              %>
                <HTML>
                  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
                  <BODY BGCOLOR="#C0C0C0">
                            <HR SIZE="1" noshade="noshade" WIDTH="100%" /><BR />
                            <%=err %><BR />
                            <HR SIZE="1" noshade="noshade" WIDTH="100%" />
                  </BODY>
                </HTML>
              <%                
            }
        }
        
        if (!dispFileImage)
        {
%>
<%@include file="WEB-INF/jspf/FwDisplay.jspf" %>
<%
        } /* if (!dispFileImage) */
%>
