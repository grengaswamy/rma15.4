<%-- ----------------------------------------------------------------- --%>
<%-- This is a JSP to Test an XML Transaction.                         --%>
<%-- The XML will be directly entered by the user.                     --%>
<%-- ----------------------------------------------------------------- --%>
<%@ page language='java' contentType='text/html'%>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.reflect.Method" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Vector" %>
<%@ page import="com.csc.fw.util.FwSystem" %>
<%@ page import="com.csc.fw.util.ServletUtil" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page errorPage="error.jsp" %>
<%@ page pageEncoding="UTF-8" %>

<jsp:useBean id='commFwData' scope='page' class='com.csc.cfw.util.test.SOAPTestData'/>
<jsp:setProperty name='commFwData' property='*' />
<%
	String noTransformCache = null;
	String setOverrideProperties = null;
	String repostCount = null;
//	String xslTraceOptions = null;

	commFwData.newTest();
	// first check if the upload request coming in is a multipart request
	boolean isMultipart = ServletFileUpload.isMultipartContent(request);

	// if not, send to message page with the error message

	if (isMultipart)
	{
		//		request.setAttribute("msg", "Request was not multipart!");
		//		request.getRequestDispatcher("error.jsp").forward(request, response);
		//		return;

		// Create a factory for disk-based file items
		FileItemFactory factory = new DiskFileItemFactory();

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		List /* FileItem */ items = upload.parseRequest(request);

		// now iterate over this list
		Iterator itr = items.iterator();
		while (itr.hasNext())
		{
			FileItem item = (FileItem) itr.next();
		
			// check if the current item is a form field or an uploaded file
			if (item.isFormField())
			{
				// get the name of the field
				String fieldName = item.getFieldName();
				
				if ("noTransformCache".equals(fieldName))
				{
					noTransformCache = item.getString();
				}
				else
				if ("setOverrideProperties".equals(fieldName))
				{
					setOverrideProperties = item.getString();
				}
				else
				if ("repostCount".equals(fieldName))
				{
					repostCount = item.getString();
				}
				else
				if ("xslTraceOptions".equals(fieldName))
				{
					//xslTraceOptions = item.getString();
					commFwData.setXslTraceOptions(item);
				}
				else
				{
					// Dynamically invoke the setter
					String val = item.getString();
					if (val != null && val.length() > 0)
					{
						boolean isSet = false;
						for (int j = 0; j < 3 && !isSet; j++)
						{
							Object obj = val;
							Class cls = String.class;
							String setterName =
							    "set" //$NON-NLS-1$
							        + Character.toUpperCase(
							          fieldName.charAt(0))
							        + fieldName.substring(1);
							if (j == 1)
							{
								// Try an integer setter
								try 
								{
									obj = new Integer(Integer.parseInt(val));
									cls = int.class;
								}
								catch (Exception e) { continue; }
							}
							else
							if (j == 2)
							{
								// Try a boolean setter
								if (val.equals("true") || val.equals("false"))
								{
									obj = new Boolean(val);
									cls = boolean.class;
								}
								else
									continue;
							}
							
							try
							{
								Method setter =
								    commFwData.getClass().getMethod(
								        setterName,
								        new Class[] { cls } );
								if (setter != null)
								{
									setter.invoke(
									        commFwData,
									        new Object[] { obj } );
									isSet = true;
								}
							}
							catch (Exception e)
							{
							}
						}
					}
				}
			}
			else
			{
				// the item must be an uploaded file
				// save it to disk
		
				String fieldName = item.getFieldName();
				if (fieldName.equals("reqAttachName") && item.getSize() > 0) //$NON-NLS-1$
				{
				    String fileName = item.getName();
				    String contentType = item.getContentType();
				    boolean isInMemory = item.isInMemory();
				    long sizeInBytes = item.getSize();
					String fileSep = System.getProperty("file.separator");
					
					commFwData.createReqAttachment(item.getInputStream(), fileName, contentType);
					commFwData.setReqAttachName(fileName);
					//if (item.getSize() != 0)
					//{
					//	savedFileItem = item;
					//	savedFileItem.write(savedFile);
					//}
					//else
					//	error = "No Attachment Data was uploaded: " + fileName;
				}
			}
		}
		
	}
	
	boolean fExecute = ("Execute".equals(commFwData.function));
	boolean fForceValidationOff = false;
	if (fExecute)
	{
		commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "noTransformCache", noTransformCache));
		commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "setOverrideProperties", commFwData.getOverridePropertiesOption()));
		commFwData.setRepostCount(repostCount);
		//if (xslTraceOptions != null)
		//	commFwData.setXslTraceOptions(new String[] { xslTraceOptions } );
		commFwData.execute();
	}
%>


<%@page import="com.csc.cfw.util.test.CommFwTestData"%><HTML>
 <HEAD>
  <META http-equiv="Content-Type" content="text/html" charset="UTF-8" />
  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <TITLE>SOAP Test<%=fExecute ? " Response" : ""%> - Communications Framework</TITLE>
 </HEAD>
 <BODY BGCOLOR="#Cnotepad 0C0C0">
 <SCRIPT LANGUAGE="JAVASCRIPT">
  <%= commFwData.getXmlStringsAsJS() %>
 </SCRIPT>
 <DIV ALIGN="Center" ID="HtmlDiv1">
  <TABLE>
   <TR>
    <TD ALIGN="Center"  >
     <H4>Communications Framework SOAP Test</H4>
     <HR WIDTH="100%">
    </TD>
   </TR>
   <TR>
    <TD  >
      <TABLE>
       <TR ALIGN="Center">
        <TD ALIGN="Center"  >
          <FORM  METHOD="POST" NAME="CommFwTestForm" action="SOAPTest.jsp" enctype="multipart/form-data">

         <TABLE>

          <TR>
           <TD>
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Transaction SOAP Data</U></STRONG></FONT></P>
            </DIV>
            <B>Copy input SOAP message below:</B>
            <br>
            <TEXTAREA cols="75" WRAP="OFF" ROWS="20" NAME="requestXml"><%
            if (commFwData.getRequestXml() != null) {
            %><%=FwSystem.replaceAll(FwSystem.replaceAll(commFwData.getRequestXml(),"&","&amp;"),"<","&lt;") /*jsp:getProperty name='commFwData' property='requestXml'/*/%><%
            }
            %></TEXTAREA>
           </TD>
          </TR>

          <TR>
           <TD>
            <B>Enter optional SOAP message URL below:</B><BR />
            <INPUT NAME="xmlUrl"
<%  if (commFwData.getXmlUrl() != null)
    {
%>
                   VALUE="<jsp:getProperty name='commFwData' property='xmlUrl'/>"  
<%
    }
%>
                   SIZE="75">
           </TD>
          </TR>

          <tr>
           <td>
             <table border="1" width="100%">
              <tr>
               <td>   
                <DIV ALIGN="Center">
                 <P><FONT SIZE="+1"><STRONG><U>SOAP Settings</U></STRONG></FONT></P>
                </DIV>
                <table width="100%">
                  <tr>
                    <td colspan="2">
                      <b>Service Target URL:</b><br />
                      <input name="serviceEndpointUrl"
<%
    String sep = commFwData.getServiceEndpointUrl();
    if (sep == null)
    {
    	String scheme = request.getScheme();
    	int defaultPort = 80;
    	if ("https".equalsIgnoreCase(scheme))
    		defaultPort = 443;
    	
        sep = scheme + "://" + request.getServerName() 
               + (request.getServerPort() == defaultPort ? "" : ":" + request.getServerPort())
               + request.getContextPath() 
               + "/services/CommFwWebService";
        commFwData.setServiceEndpointUrl(sep);
    }
    
    if (sep != null)
    {
%>
                       value="<jsp:getProperty name='commFwData' property='serviceEndpointUrl'/>"  
<%
    }
%>
                       size="55">
                    </td>
                    <td>
                      <b>Connection Username:</b><br />
                      <input name="connectionUsername"
<%  if (commFwData.getConnectionUsername() != null)
    {
%>
                        value="<jsp:getProperty name='commFwData' property='connectionUsername'/>"  
<%
    }
%>
                        size="20" />
                    </td>
                  </tr>
                  <tr>
                    <td>
                     <b>SOAP Action:</b><br />
                     <input name="soapAction"
<%  if (commFwData.getSoapAction() != null)
    {
%>
                       value="<jsp:getProperty name='commFwData' property='soapAction'/>"  
<%
    }
%>
                       size="30">
                    </td>
                    <td>
                      <input value="true" type="checkbox" name="webServiceImplementationAxis2" <%=ServletUtil.setChecked("true", new Boolean(commFwData.isWebServiceImplementationAxis2()).toString()) %> ><b>Use Axis2 to Process</b>
                    </td>
                    <td>
                      <b>Connection Password:</b><br />
                      <input type="password" name="connectionPassword"
<%  if (commFwData.getConnectionPassword() != null)
    {
%>
                       value="<jsp:getProperty name='commFwData' property='connectionPassword'/>"  
<%
    }
%>
                       size="20" />
                    </td>
                  </tr>
                </table>
               </td>   
              </tr>
             </table>
           </td>
          </tr>

          <tr>
           <td width="100%">
            <div align="center">
            <table border="1" width="100%">
            
             <tr>
              <td width="100%">
               <table width="100%">
                <tr>
                 <td colspan="3">
                   <div align="center">
                   <P><FONT SIZE="+1"><STRONG><U>WS-Security Modifiers</U></STRONG></FONT></P>
                   </div>
                 </td>
                </tr>

          <!-- Web Service Security Section -->
                <tr>
                 <td>
                   <table>
                     <tr>
                       <td><b>User Name Token <br />User:</b></td>
                       <td><b>User Name Token <br />Password:</b></td>
                       <td><b>User Name Token <br />Password Type:</b></td>
                       <td><b>User Name Token <br />ID:</b></td>
                     </tr>
                     <tr>
                       <td>
                        <input name="usernameTokenUsername"
<%  if (commFwData.getUsernameTokenUsername() != null)
    {
%>
                         value="<jsp:getProperty name='commFwData' property='usernameTokenUsername'/>"  
<%
    }
%>
                         size="20" />
                       </td>
                       <td>
                        <input type="password" name="usernameTokenPassword"
<%  if (commFwData.getUsernameTokenPassword() != null)
    {
%>
                         value="<jsp:getProperty name='commFwData' property='usernameTokenPassword'/>"  
<%
    }
%>
                         size="20" />
                       </td>
                       <td>
                         <select name="usernameTokenPasswordType" SIZE="1">
                           <option value=""  <%=ServletUtil.setSelected("0",commFwData.getUsernameTokenPasswordType()) %> />
                           <option value="TEXT" <%=ServletUtil.setSelected("TEXT",commFwData.getUsernameTokenPasswordType()) %> />Plain Text
                           <option value="DIGEST" <%=ServletUtil.setSelected("DIGEST",commFwData.getUsernameTokenPasswordType()) %> />Message Digest
                         </select>
                       </td>
                       <td>
                        <input name="usernameTokenId"
<%  if (commFwData.getUsernameTokenId() != null)
    {
%>
                         value="<jsp:getProperty name='commFwData' property='usernameTokenId'/>"  
<%
    }
%>
                         size="20" />
                       </td>
                     </tr>
                    </table>
                 </td>
                </tr>
                
                <tr>
                 <td>
                   <table>
                     <tr>
                       <td><b>Encryption:</b></td>
                       <td><b>Signature:</b></td>
                       <td>&nbsp;</td>
                     </tr>
                     <tr>
                       <td>
                         <select name="encryptType" SIZE="1">
                           <option value=""  <%=ServletUtil.setSelected("0",commFwData.getEncryptType()) %> />
                           <option value="BODY" <%=ServletUtil.setSelected("BODY",commFwData.getEncryptType()) %> />Encrypt SOAP Body
                           <option value="UT" <%=ServletUtil.setSelected("UT",commFwData.getEncryptType()) %> />Encrypt Username Token
                           <option value="BODY_UT" <%=ServletUtil.setSelected("BODY_UT",commFwData.getEncryptType()) %> />Encrypt Body/Username Token
                           <option value="SAML" <%=ServletUtil.setSelected("SAML",commFwData.getEncryptType()) %> />Encrypt using SAML
                         </select>
                       </td>
                       <td>
                         <select name="signType" SIZE="1">
                           <option value=""  <%=ServletUtil.setSelected("0",commFwData.getSignType()) %> />
                           <option value="BODY" <%=ServletUtil.setSelected("BODY",commFwData.getSignType()) %> />Sign SOAP Envelope
                           <option value="SAML" <%=ServletUtil.setSelected("SAML",commFwData.getSignType()) %> />SAML Signature
                         </select>
                       </td>
                       <td>&nbsp;</td>
                     </tr>
                    </table>
                 </td>
                </tr>
                
                <tr>
                 <td>
                   <table>
                   
                     <tr>
                       <td><b>Key:</b></td>
                       <td>&nbsp;</td>
                       <td><b>Key Type</b></td>
                     </tr>
                     
                     <tr>
                       <td>
                        <input name="encryptKey"
<%  if (commFwData.getEncryptKey() != null)
    {
%>
                         value="<jsp:getProperty name='commFwData' property='encryptKey'/>"  
<%
    }
%>
                         size="20" />
                       </td>
                       <td>
                         <input value="true" type="checkbox" name="encryptKeyBase64" <%=ServletUtil.setChecked("true", commFwData.getEncryptKeyBase64()) %> ><b>Key Base64</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       </td>
                       <td>
                         <select name="encryptKeyType" SIZE="1">
                           <option value=""  <%=ServletUtil.setSelected("0",commFwData.getEncryptKeyType()) %> />
                           <option value="BST_DIRECT_REFERENCE" <%=ServletUtil.setSelected("BST_DIRECT_REFERENCE",commFwData.getEncryptKeyType()) %> />BST_DIRECT_REFERENCE
                           <option value="ISSUER_SERIAL" <%=ServletUtil.setSelected("ISSUER_SERIAL",commFwData.getEncryptKeyType()) %> />ISSUER_SERIAL
                           <option value="X509_KEY_IDENTIFIER" <%=ServletUtil.setSelected("X509_KEY_IDENTIFIER",commFwData.getEncryptKeyType()) %> />X509_KEY_IDENTIFIER
                           <option value="SKI_KEY_IDENTIFIER" <%=ServletUtil.setSelected("SKI_KEY_IDENTIFIER",commFwData.getEncryptKeyType()) %> />SKI_KEY_IDENTIFIER
                           <option value="EMBEDDED_KEYNAME" <%=ServletUtil.setSelected("EMBEDDED_KEYNAME",commFwData.getEncryptKeyType()) %> />EMBEDDED_KEYNAME
                           <option value="EMBED_SECURITY_TOKEN_REF" <%=ServletUtil.setSelected("EMBED_SECURITY_TOKEN_REF",commFwData.getEncryptKeyType()) %> />EMBED_SECURITY_TOKEN_REF
                           <option value="UT_SIGNING" <%=ServletUtil.setSelected("UT_SIGNING",commFwData.getEncryptKeyType()) %> />UT_SIGNING
                         </select>
                       </td>
                     </tr>
                    </table>
                 </td>
                </tr>
                
               </table>
              </td>
             </tr>
             
            </table>
            </div>
           </td>
          </tr>

          <!-- Attachments Section -->
          <tr>
           <td width="100%" border="1">
            <div align="center">
            <table border="1" width="100%">
            
             <tr>
              <td width="100%">
               <table width="100%">
                <tr>
                 <td colspan="3">
                   <div align="center">
                   <P><FONT SIZE="+1"><STRONG><U>Attachment Test</U></STRONG></FONT></P>
                   </div>
                 </td>
                </tr>

                <tr>
                 <td>
                   <table>
                     <tr>
                       <td><b>Upload Attachment:</b></td>
                     </tr>
                     <tr>
                       <td colspan="2">
                         <input type="file" name="reqAttachName"
                                size="60">
                       </td>
                     </tr>
                     <tr>
                       <td>
                         <b>MTOM Attachment XPath:</b><br />
                         <input type="text" name="attachMTOMXPath"
<%  if (commFwData.getAttachMTOMXPath() != null)
    {
%>
                         value="<jsp:getProperty name='commFwData' property='attachMTOMXPath'/>"  
<%
    }
%>
                                size="60" />
                       </td>
                       <td align="right" valign="top">
                         <input value="true" type="checkbox" name="reqAttachMTOM" <%=ServletUtil.setChecked("true", new Boolean(commFwData.isReqAttachMTOM()).toString()) %> ><b>MTOM Attachment</b><br /> (only valid for Axis2)
                       </td>
                     </tr>
                    </table>
                 </td>
                </tr>
                
               </table>
              </td>
             </tr>
             
            </table>
            </div>
           </td>
          </tr>

          <TR>
           <TD ALIGN="Left"  >
            <INPUT VALUE="Execute" TYPE="SUBMIT" NAME="function">   
            <INPUT VALUE="Reset" TYPE="RESET">
            <HR WIDTH="100%">
           </TD>
          </TR>

<%@ include file="WEB-INF/jspf/CFwXMLDataForm.jspf" %>

          </TABLE>
          </FORM>
          
          <FORM METHOD="POST" NAME="CommFwResultForm" ACTION="">
          <INPUT TYPE="hidden" NAME="locale" 
<%  if (commFwData.getLocale() != null)
    {
%>
                                 VALUE="<jsp:getProperty name='commFwData' property='locale'/>" 
<%
    }
%>
>

          <TABLE align="center" width="100%">

<%
    String soapMessageString = commFwData.getSoapMessageString();
    if (soapMessageString != null)
    {
%>
          <TR>
           <TD>
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Request SOAP Envelope</U></STRONG></FONT></P>
            </DIV>
            <TEXTAREA cols="75" WRAP="OFF" ROWS="9" NAME="soapMessageString"><%=FwSystem.xmlEncode(soapMessageString) /*jsp:getProperty name='commFwData' property='soapMessageString'/*/%></TEXTAREA>
            <DIV ALIGN="left">
              <A href="javascript:makeNewWindow(document.forms[1].soapMessageString.value, 'Request SOAP Envelope', 'text/xml')">Expand View</A>
            </DIV>
           </TD>
          </TR>
<%
}
%>

<%
    if (commFwData.getRspAttachName() != null)
    {
%>
          <TR>
           <TD>
            <DIV ALIGN="Center">
             <P><FONT SIZE="+1"><STRONG><U>Response Attachment Returned</U></STRONG></FONT></P>
            </DIV>
            <P><B>Name (Content ID): </B><%= commFwData.getRspAttachName() %></P>
           </TD>
          </TR>
<%
}
%>

          </TABLE>

<%@ include file="WEB-INF/jspf/CFwXMLResultForm.jspf" %>

          </FORM>
        </TD>
       </TR>
      </TABLE>
    </TD>
   </TR>
  </TABLE>
 </DIV>
 </BODY>
</HTML>
