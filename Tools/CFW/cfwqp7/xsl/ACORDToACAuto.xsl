<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xmlns:acord="http://www.ACORD.org/standards/PC_Surety/ACORD1.8.0/xml/" xmlns:csc="http://www.csc.com/standards/PC_SuretyExtensions/v1/xml/">
	<xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>
	
	<xsl:template name="AcordToACAuto" match="/">
		<xsl:variable name="PolicyNumber" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyNumber"/>
		<xsl:variable name="EntityNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty"/>
		<!--Begin Adding Policy Number tag-->
		<xsl:variable name="UnitNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/AutoLossInfo"/>
		<!--End Adding Policy Number tag-->
		<CLAIM  EVENT="EVT1" ID="CLM20">
			<CLAIM_NUMBER>GENERATE</CLAIM_NUMBER>
			<CLAIM_STATUS CODE="O">Open</CLAIM_STATUS>
			<FILING_STATE>
			<xsl:attribute name="CODE"><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ControllingStateProvCd"/></xsl:attribute>
			</FILING_STATE>
			<xsl:variable name="LossDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossDt"/>
			<ACCIDENT_DESCRIPTION CODE=""><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/IncidentDesc"/></ACCIDENT_DESCRIPTION>
			<DATE_OF_LOSS>
				<xsl:attribute name="YEAR">
					<xsl:value-of select="substring($LossDt,1,4)"/>
				</xsl:attribute>
				<xsl:attribute name="MONTH">
					<xsl:value-of select="substring($LossDt,6,2)"/>
				</xsl:attribute>
				<xsl:attribute name="DAY">
					<xsl:value-of select="substring($LossDt,9,2)"/>
				</xsl:attribute>
				<xsl:value-of select="$LossDt"/>
			</DATE_OF_LOSS>
			<xsl:variable name="LossTime" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossTime"/>
			<TIME_OF_LOSS>
				<xsl:attribute name="HOUR">
					<xsl:value-of select="substring($LossTime,1,2)"/>
				</xsl:attribute>
				<xsl:attribute name="MINUTE">
					<xsl:value-of select="substring($LossTime,4,2)"/>
				</xsl:attribute>
				<xsl:attribute name="SECOND">
					<xsl:value-of select="substring($LossTime,7,2)"/>
				</xsl:attribute>
				<xsl:value-of select="$LossTime"/>
			</TIME_OF_LOSS>
			<xsl:variable name="RptDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ClaimsReported/ReportedDt"/>
			<DATE_REPORTED>
				<xsl:attribute name="YEAR">
					<xsl:value-of select="substring($RptDt,1,4)"/>
				</xsl:attribute>
				<xsl:attribute name="MONTH">
					<xsl:value-of select="substring($RptDt,6,2)"/>
				</xsl:attribute>
				<xsl:attribute name="DAY">
					<xsl:value-of select="substring($RptDt,9,2)"/>
				</xsl:attribute>
				<xsl:value-of select="substring($RptDt,1,10)"/>
			</DATE_REPORTED>
			<TIME_REPORTED>
				<xsl:attribute name="HOUR">
					<xsl:value-of select="substring($RptDt,12,2)"/>
				</xsl:attribute>
				<xsl:attribute name="MINUTE">
					<xsl:value-of select="substring($RptDt,15,2)"/>
				</xsl:attribute>
				<xsl:attribute name="SECOND">
					<xsl:value-of select="substring($RptDt,18,2)"/>
				</xsl:attribute>
				<xsl:value-of select="substring($RptDt,12,8)"/>
			</TIME_REPORTED>
			<AUTHORITY_CONTACTED>
				<AUTHORITY>
					<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ResponsibleDept"/>
				</AUTHORITY>
				<REPORT_NUMBER>
					<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ClaimsReported/ReportNumber"/>
				</REPORT_NUMBER>
			</AUTHORITY_CONTACTED>
			<PREVIOUSLY_REPORTED>
				<xsl:attribute name="INDICATOR">
					<xsl:if test="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/PreviouslyReportedInd='1'">
						<xsl:text>Yes</xsl:text>
					</xsl:if>
					<xsl:if test="not(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/PreviouslyReportedInd ='1')">
						<xsl:text>No</xsl:text>
					</xsl:if>
				</xsl:attribute>
			</PREVIOUSLY_REPORTED>
			<POLICY>
				<POLICY_NUMBER>
					<xsl:attribute name="SYMBOL">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/CompanyProductCd"></xsl:value-of>
					</xsl:attribute>
					<xsl:attribute name="LOCATION">00</xsl:attribute>
					<xsl:attribute name="MODULE">
					    <xsl:choose>
						<xsl:when test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyVersion)&gt;0">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyVersion"/>
						</xsl:when>
						<xsl:otherwise>00</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:value-of select="$PolicyNumber"/>
				</POLICY_NUMBER>
				<MASTER_COMPANY>
					<xsl:attribute name="CODE">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/NAICCd"></xsl:value-of>
					</xsl:attribute>
				</MASTER_COMPANY>
				<LINE_OF_BUSINESS>
				<xsl:attribute name="CODE">CPP</xsl:attribute>
			</LINE_OF_BUSINESS>
				<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt)>1">
				<EFFECTIVE_DATE>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,9,2)"/>
					</xsl:attribute>
					</EFFECTIVE_DATE>
					</xsl:if>
				<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt)>1">
				<EXPIRATION_DATE>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,9,2)"/>
					</xsl:attribute>
				</EXPIRATION_DATE>
				</xsl:if>
				<xsl:for-each select="$EntityNode">
					<xsl:choose>
						<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='Insured'">
							<PARTY_INVOLVED CODE="PHLD">
								<xsl:attribute name="ID">
									<xsl:value-of select="concat('ENT',position())"/>
								</xsl:attribute>
								<ROLE>PolicyHolder</ROLE>
							</PARTY_INVOLVED>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</POLICY>
			<xsl:for-each select="$EntityNode">
			   <xsl:choose>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='CLM'">
						<INJURY>
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('INJ',position())"/>
							</xsl:attribute>
							<INJURED_PARTY>
								<xsl:attribute name="ID">
									<xsl:value-of select="concat('ENT',position())"/>
								</xsl:attribute>
							</INJURED_PARTY >
							<NATURE_OF_INJURY>
								<xsl:attribute name="CODE"><xsl:value-of select="ClaimsInjuredInfo/ClaimsInjury/InjuryNatureCd"/></xsl:attribute>
							</NATURE_OF_INJURY>
							<xsl:variable name="injDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossDt"/>
							<xsl:if test=" string-length($injDt)>0">
							<DATE_OF_INJURY>
								<xsl:attribute name="YEAR">
									<xsl:value-of select="substring($injDt,1,4)"/>
								</xsl:attribute>
								<xsl:attribute name="MONTH">
									<xsl:value-of select="substring($injDt,6,2)"/>
								</xsl:attribute>
								<xsl:attribute name="DAY">
									<xsl:value-of select="substring($injDt,9,2)"/>
								</xsl:attribute>
								<xsl:value-of select="$injDt"/>
							</DATE_OF_INJURY>
							</xsl:if>
							<TIME_OF_INJURY>
								<xsl:attribute name="HOUR">
									<xsl:value-of select="substring($LossTime,1,2)"/>
								</xsl:attribute>
								<xsl:attribute name="MINUTE">
									<xsl:value-of select="substring($LossTime,4,2)"/>
								</xsl:attribute>
								<xsl:attribute name="SECOND">
									<xsl:value-of select="substring($LossTime,7,2)"/>
								</xsl:attribute>
								<xsl:value-of select="$LossTime"/>
							</TIME_OF_INJURY>
							<PARTY_INVOLVED CODE="INJ">
								<xsl:attribute name="ID">
									<xsl:value-of select="concat('ENT',position())"/>
								</xsl:attribute>
								<ROLE>Injured</ROLE>
							</PARTY_INVOLVED>
						</INJURY>
		       		</xsl:when>
			   </xsl:choose>
			</xsl:for-each>
			<xsl:for-each select="$EntityNode">
				<xsl:choose>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='WIT'">
						<PARTY_INVOLVED CODE="WT">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Witness</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='CC'">
						<PARTY_INVOLVED CODE="CNT">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Contact</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
				<xsl:for-each select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/AutoLossInfo">
				<xsl:variable name="pos">
					<xsl:value-of select="concat('UNT',position())"/>
				</xsl:variable>
				<VEHICLE_LOSS>
					<UNIT_REFERENCE>
						<xsl:attribute name="ID">
							<xsl:value-of select="concat('UNT',position())"/>
						</xsl:attribute>
					</UNIT_REFERENCE>
					<xsl:if test="$pos='UNT1'">
						<DRIVER_RELATION_TO_OWNER>
							<xsl:attribute name="CODE">
								<xsl:choose>
									<xsl:when test="$EntityNode/ClaimsPartyInfo/RelationshipToInsuredCd='CH'">
										<xsl:text>C</xsl:text>
									</xsl:when>
									<xsl:when test="$EntityNode/ClaimsPartyInfo/RelationshipToInsuredCd ='EM'">
										<xsl:text>E</xsl:text>
									</xsl:when>
									<xsl:when test="$EntityNode/ClaimsPartyInfo/RelationshipToInsuredCd ='IN'">
										<xsl:text>I</xsl:text>
									</xsl:when>
									<xsl:when test="$EntityNode/ClaimsPartyInfo/RelationshipToInsuredCd ='OT'">
										<xsl:text>O</xsl:text>
									</xsl:when>
									<xsl:when test="$EntityNode/ClaimsPartyInfo/RelationshipToInsuredCd ='RE'">
										<xsl:text>R</xsl:text>
									</xsl:when>
									<xsl:when test="$EntityNode/ClaimsPartyInfo/RelationshipToInsuredCd ='SP'">
										<xsl:text>S</xsl:text>
									</xsl:when>
								</xsl:choose>
							</xsl:attribute>
						</DRIVER_RELATION_TO_OWNER>
					</xsl:if>
					<LOSS_INFORMATION>
						<DAMAGE_DESCRIPTION>
							<xsl:value-of select="DamageDesc"/>
						</DAMAGE_DESCRIPTION>
						<ESTIMATED_DAMAGE_AMOUNT>
							<xsl:value-of select="ProbableIncurredAmt/Amt"/>
						</ESTIMATED_DAMAGE_AMOUNT>
						<xsl:if test="(string-length(WhereSeenDesc)>0) or (string-length(com.csc_UnitLossData/com.csc_WhenPropertyCanBeSeen/com.csc_WhenPropertyCanBeSeenDesc)>0)">
						<CAN_BE_SEEN>
						 <WHERE>
						     <xsl:value-of select="concat(WhereSeenDesc,' ', com.csc_UnitLossData/com.csc_WhenPropertyCanBeSeen/com.csc_WhenPropertyCanBeSeenDesc)"/>
						 </WHERE>
						</CAN_BE_SEEN>
						</xsl:if>	
						<OTHER_INSURANCE>
							<xsl:for-each select="$UnitNode">
								<xsl:variable name="pos2">
									<xsl:value-of select="concat('UNT',position())"/>
									</xsl:variable>
					  				    <xsl:choose>
											<xsl:when test="$pos=$pos2">  
									             <xsl:for-each select="$EntityNode">
				 						            <xsl:choose>
											          <xsl:when test="((ClaimsPartyInfo/ClaimsPartyRoleCd='PRO')and not($pos='UNT1'))">
							                            <xsl:variable name="VehId" select="$pos"></xsl:variable>
							                            <xsl:variable name="NameId" select="@id"></xsl:variable>
									    					<xsl:if test="substring($VehId,4,1)=substring($NameId,3,1)+1">
						               							<POLICY_NUMBER>
				                 								<xsl:value-of select="ClaimsPartyInfo/OtherOrPriorPolicy/PolicyNumber"/>
									               				</POLICY_NUMBER>
															</xsl:if>
													  </xsl:when>				               				
									                </xsl:choose>
							                     </xsl:for-each>
									        </xsl:when>    
										</xsl:choose>
							</xsl:for-each>
						</OTHER_INSURANCE>
						<!--End Adding Policy Number tag -->		
					</LOSS_INFORMATION>
				</VEHICLE_LOSS>
			</xsl:for-each>
			<xsl:variable name="VehCount" select="count(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/AutoLossInfo)"/>
			<xsl:if test="$VehCount>0">
				<xsl:call-template name="AddInvolvedUnits">
					<xsl:with-param name="VehicleCount" select="$VehCount"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt)>0">
			<COMMENT_REFERENCE ID="CMT01"/>
			</xsl:if>
			</CLAIM>
			
			<!--ADDITIONAL_DATA>
			</ADDITIONAL_DATA-->
			

		<xsl:call-template name="AddInvolvedEntity"></xsl:call-template>
		<xsl:call-template name="AddEntityAddress"></xsl:call-template>
		<xsl:call-template name="AddAutoComments"></xsl:call-template>
		
	</xsl:template>
	
	<xsl:template name="AddInvolvedEntity">
		<xsl:param name="Entities" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq"/>
		<!--Begin 05/16/07 working on insured by problem -->					
		<xsl:param name="AddEntities" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty/ClaimsPartyInfo"/>
		<!--End 05/16/07 working on insured by problem -->					
		<ENTITIES>
			<xsl:variable name="EntityCount" select="count($Entities/ClaimsParty)"/>
			<!--begin 05/17/07 working on insured by problem -->
			<xsl:variable name="AddEntityCount" select="count($Entities/ClaimsParty/ClaimsPartyInfo/OtherOrPriorPolicy/InsurerName)"/>
			<!--Begin 07/03/07 working on insured by for Property LOB (only one and not on Party invovled) -->
			<xsl:variable name="AddEntityCountHome" select="count(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/InsurerName)"/>
			<!--End 07/03/07 working on insured by for Property LOB (only one and not on Party invovled) -->
			<xsl:attribute name="COUNT">
				<xsl:value-of select="$EntityCount + $AddEntityCount + $AddEntityCountHome"/>
			<!--End 05/17/07 working on insured by problem -->
			</xsl:attribute>
			<xsl:attribute name="NEXT_ID">
			<!--Begin 05/17/07 Adding Insured By tag -->			
							<xsl:value-of select="$EntityCount+$AddEntityCount+$AddEntityCountHome+1"/>
			<!--End 05/17/07 Adding Insured By tag -->										
			</xsl:attribute>
			<xsl:for-each select="$Entities/ClaimsParty">
				<ENTITY>
					<xsl:variable name="Id" select="concat('ENT',position())"/>
					<xsl:attribute name="ID">
						<xsl:value-of select="$Id"></xsl:value-of>
					</xsl:attribute>
					<xsl:choose>
				<xsl:when test="GeneralPartyInfo/NameInfo/CommlName">
				<xsl:attribute name="TYPE">Business</xsl:attribute>
					<!--<xsl:if test="$ClaimantType ='Individual'">-->
					<ENTITY_NAME>
						<BUSINESS>
							<LEGAL_NAME>
								<xsl:value-of select="GeneralPartyInfo/NameInfo/CommlName/CommercialName"></xsl:value-of>
							</LEGAL_NAME>
						</BUSINESS>
					</ENTITY_NAME>
					<xsl:if test="GeneralPartyInfo/NameInfo/TaxIdentity">
					<TAX_ID>
					<xsl:attribute name="TYPE"><xsl:value-of select="GeneralPartyInfo/NameInfo/TaxIdentity/TaxIdTypeCd"/></xsl:attribute>
					<xsl:value-of select="GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/>
					</TAX_ID>
					</xsl:if>
                     <xsl:if test="string-length(GeneralPartyInfo/Addr/Addr1)>0">
					<ADDRESS_REFERENCE PRIMARY="Yes">
						<xsl:attribute name="ID">
							<xsl:value-of select="concat('ADR',position())"></xsl:value-of>
						</xsl:attribute>
						<xsl:attribute name="CODE">INHL</xsl:attribute>
					</ADDRESS_REFERENCE>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
				<xsl:attribute name="TYPE">Individual</xsl:attribute>
					<!--<xsl:if test="$ClaimantType ='Individual'">-->
					<ENTITY_NAME>
						<INDIVIDUAL>
							<FIRST_NAME>
								<xsl:value-of select="GeneralPartyInfo/NameInfo/PersonName/GivenName"></xsl:value-of>
							</FIRST_NAME>
							<LAST_NAME>
								<xsl:value-of select="GeneralPartyInfo/NameInfo/PersonName/Surname"></xsl:value-of>
							</LAST_NAME>
						</INDIVIDUAL>
					</ENTITY_NAME>
					<xsl:if test="GeneralPartyInfo/NameInfo/TaxIdentity">
					<TAX_ID>
					<xsl:attribute name="TYPE"><xsl:value-of select="GeneralPartyInfo/NameInfo/TaxIdentity/TaxIdTypeCd"/></xsl:attribute>
					<xsl:value-of select="GeneralPartyInfo/NameInfo/TaxIdentity/TaxId"/>
					</TAX_ID>
					</xsl:if>
					<xsl:if test="string-length(GeneralPartyInfo/Addr/Addr1)>0">
					<ADDRESS_REFERENCE PRIMARY="Yes">
						<xsl:attribute name="ID">
							<xsl:value-of select="concat('ADR',position())"></xsl:value-of>
						</xsl:attribute>
						<xsl:attribute name="CODE">INHL</xsl:attribute>
					</ADDRESS_REFERENCE>
					</xsl:if>
					<DEMOGRAPHICS>
						<GENDER>
							<xsl:attribute name="CODE">
								<xsl:value-of select="PersonInfo/GenderCd"></xsl:value-of>
							</xsl:attribute>
						</GENDER>
						<MARITAL_STATUS>
							<xsl:attribute name="CODE">
								<xsl:value-of select="PersonInfo/MaritalStatusCd"></xsl:value-of>
							</xsl:attribute>
						</MARITAL_STATUS>
						<xsl:variable name="DateOfBirh">
								<xsl:value-of select="PersonInfo/BirthDt"/>
							</xsl:variable>
						<xsl:if test="string-length($DateOfBirh)>1">
						<DATE_OF_BIRTH>
							<xsl:attribute name="YEAR">
								<xsl:value-of select="substring($DateOfBirh,1,4)"></xsl:value-of>
							</xsl:attribute>
							<xsl:attribute name="MONTH">
								<xsl:value-of select="substring($DateOfBirh,6,2)"></xsl:value-of>
							</xsl:attribute>
							<xsl:attribute name="DAY">
								<xsl:value-of select="substring($DateOfBirh,9,2)"></xsl:value-of>
							</xsl:attribute>
						</DATE_OF_BIRTH>
						</xsl:if>
						<xsl:if test="ClaimsPartyInfo/ClaimsPartyRoleCd='DRV'">
						<DRIVERS_LICENSE>
						<STATE>
						<xsl:attribute name="CODE"><xsl:value-of select="ClaimsDriverInfo/StateProvCd"/></xsl:attribute>
						</STATE>
						<NUMBER><xsl:value-of select="ClaimsDriverInfo/DriversLicenseNumber"/></NUMBER>
						</DRIVERS_LICENSE>
						</xsl:if>
					</DEMOGRAPHICS>
					</xsl:otherwise>
					</xsl:choose>
					<xsl:for-each select="GeneralPartyInfo/Communications/*">
						<CONTACT>
							<WHO>
								<xsl:attribute name="ID">
									<xsl:value-of select="$Id"/>
								</xsl:attribute>
							</WHO>
							<xsl:choose>
								<xsl:when test="CommunicationUseCd='Home'">
									<TYPE CODE='IH'>Home</TYPE>
								</xsl:when>
								<xsl:when test="CommunicationUseCd='Business'">
									<TYPE CODE='BS'>Business</TYPE>
								</xsl:when>
							</xsl:choose>
							<INFORMATION>
								<xsl:value-of select="PhoneNumber"/>
							</INFORMATION>
						</CONTACT>
					</xsl:for-each>
				</ENTITY>
			</xsl:for-each>
			<!--begin 05/17/07 Adding Insured By tag -->
            <xsl:for-each select="$Entities/ClaimsParty"> 
				<xsl:if test="ClaimsPartyInfo/OtherOrPriorPolicy/InsurerName">
				<ENTITY>
					<xsl:variable name="Id" select="concat('ENT',position() + 300)"/>
					<xsl:attribute name="ID">
						<xsl:value-of select="$Id"></xsl:value-of>
					</xsl:attribute>
				<xsl:attribute name="TYPE">Business</xsl:attribute>					
					<ENTITY_NAME>
						<BUSINESS>
							<LEGAL_NAME>
								<xsl:value-of select="ClaimsPartyInfo/OtherOrPriorPolicy/InsurerName"></xsl:value-of>
							</LEGAL_NAME>
						</BUSINESS>
					</ENTITY_NAME>
				</ENTITY>					
            	</xsl:if>
			</xsl:for-each>
			<!--End 05/17/07 Adding Insured By tag-->			
			<!--begin 07/03/07 Adding Insured By tag for Property LOB (not at entity level in Accord XML) -->
				<xsl:if test="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/InsurerName">
				<ENTITY>
					<xsl:variable name="Id" select="concat('ENT',position() + 300)"/>
					<xsl:attribute name="ID">
						<xsl:value-of select="$Id"></xsl:value-of>
					</xsl:attribute>
				<xsl:attribute name="TYPE">Business</xsl:attribute>					
					<ENTITY_NAME>
						<BUSINESS>
							<LEGAL_NAME>
								<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/InsurerName"></xsl:value-of>
							</LEGAL_NAME>
						</BUSINESS>
					</ENTITY_NAME>
				</ENTITY>					
            	</xsl:if>
			<!--End 07/03/07 Adding Insured By tag-->			
		</ENTITIES>
	</xsl:template>
	<xsl:template name="AddInvolvedUnits">
		<xsl:param name="UnitNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/AutoLossInfo"/>
		<xsl:param name="EntityNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty"/>
		<xsl:param name="VehicleCount"></xsl:param>
		<INVOLVED_UNITS>
			<xsl:attribute name="COUNT">
				<xsl:value-of select="$VehicleCount"></xsl:value-of>
			</xsl:attribute>
			<xsl:for-each select="$UnitNode">
				<xsl:variable name="pos">
					<xsl:value-of select="concat('UNT',position())"/>
				</xsl:variable>
				<UNIT UNIT_TYPE="V">
					<xsl:attribute name="ID">
						<xsl:value-of select="$pos"></xsl:value-of>
					</xsl:attribute>
					<!--Begin 05/18/07 Adding Property Insured tag-->
					<OTHER_INSURANCE>
						<xsl:choose>
							<xsl:when test="OtherInsuranceInd='1'">
								<xsl:attribute name="INDICATOR">YES</xsl:attribute>					
							</xsl:when>
							<xsl:when test="OtherInsuranceInd='0'">
								<xsl:attribute name="INDICATOR">NO</xsl:attribute>					
							</xsl:when>
        				</xsl:choose>
					</OTHER_INSURANCE>
                    <!--End 05/18/07 Adding Property Insured tag--> 
					<VEHICLE>
						<xsl:attribute name="MAKE">
							<xsl:value-of select="VehInfo/Manufacturer"></xsl:value-of>
						</xsl:attribute>
						<xsl:attribute name="MODEL">
							<xsl:value-of select="VehInfo/Model"></xsl:value-of>
						</xsl:attribute>
						<xsl:attribute name="YEAR">
							<xsl:value-of select="VehInfo/ModelYear"></xsl:value-of>
						</xsl:attribute>
						<!--<VEHCILE_TYPE><xsl:value-of select="VehInfo/VehBodyTypeCd"></xsl:value-of></VEHCILE_TYPE>-->
						<SERIAL_NO>
							<xsl:value-of select="VehInfo/VehIdentificationNumber"></xsl:value-of>
						</SERIAL_NO>
						<REGISTRATION>
							<STATE>
								<xsl:attribute name="CODE">
									<xsl:value-of select="VehInfo/RegistrationStateProvCd"></xsl:value-of>
								</xsl:attribute>
							</STATE>
							<LICENSE>
								<xsl:value-of select="VehInfo/LicensePlateNumber"></xsl:value-of>
							</LICENSE>
						</REGISTRATION>
						<xsl:for-each select="$EntityNode">
						<xsl:choose>
							<xsl:when test="((ClaimsPartyInfo/ClaimsPartyRoleCd='DRV') and ($pos='UNT1'))">
					<PURPOSE_OF_USE>
					<xsl:attribute name="CODE"><xsl:value-of select="ClaimsDriverInfo/PurposeUse"/></xsl:attribute>
					<xsl:value-of select="ClaimsDriverInfo/PurposeUse"/>
					</PURPOSE_OF_USE>
					</xsl:when>
					</xsl:choose>
					</xsl:for-each>
					</VEHICLE>
					<xsl:for-each select="$EntityNode">
						<xsl:choose>
							<xsl:when test="((ClaimsPartyInfo/ClaimsPartyRoleCd='DRV') and ($pos='UNT1'))">
								<PARTY_INVOLVED CODE="DRVR">
									<xsl:attribute name="ID">
										<xsl:value-of select="concat('ENT',position())"/>
									</xsl:attribute>
									<TYPE>Driver</TYPE>
									
							
									<!--begin 06/28/07 Adding Relation to Insured -->
									<xsl:if test="string-length(ClaimsPartyInfo/RelationshipToInsuredCd)>0">
										<DATA_ITEM NAME="RELATION_TO_INSD">
											<xsl:attribute name="CODE">
												<xsl:choose>
													<xsl:when test="ClaimsPartyInfo/RelationshipToInsuredCd ='CH'">
														<xsl:text>C</xsl:text>
													</xsl:when>
													<xsl:when test="ClaimsPartyInfo/RelationshipToInsuredCd ='EM'">
														<xsl:text>E</xsl:text>
													</xsl:when>
													<xsl:when test="ClaimsPartyInfo/RelationshipToInsuredCd ='OF'">
														<xsl:text>OF</xsl:text>
													</xsl:when>
													<xsl:when test="ClaimsPartyInfo/RelationshipToInsuredCd ='G'">
														<xsl:text>G</xsl:text>
													</xsl:when>
													<xsl:when test="ClaimsPartyInfo/RelationshipToInsuredCd ='IN'">
														<xsl:text>I</xsl:text>
													</xsl:when>
													<xsl:when test="ClaimsPartyInfo/RelationshipToInsuredCd ='N'">
														<xsl:text>N</xsl:text>
													</xsl:when>
													<xsl:when test="ClaimsPartyInfo/RelationshipToInsuredCd ='OT'">
														<xsl:text>O</xsl:text>
													</xsl:when>
													<xsl:when test="ClaimsPartyInfo/RelationshipToInsuredCd ='RE'">
														<xsl:text>R</xsl:text>
													</xsl:when>
													<xsl:when test="ClaimsPartyInfo/RelationshipToInsuredCd ='SP'">
														<xsl:text>S</xsl:text>
													</xsl:when>
												</xsl:choose>
											</xsl:attribute>
										</DATA_ITEM>	
									</xsl:if>
									<!--End 06/28/07 Adding Relation to Insured -->
									
								</PARTY_INVOLVED>
							</xsl:when>
							<xsl:when test="((ClaimsPartyInfo/ClaimsPartyRoleCd='VEH') and ($pos='UNT1'))">
								<PARTY_INVOLVED CODE="OWN">
									<xsl:attribute name="ID">
										<xsl:value-of select="concat('ENT',position())"/>
									</xsl:attribute>
									<TYPE>Owner</TYPE>
								</PARTY_INVOLVED>
							</xsl:when>
							<xsl:when test="((ClaimsPartyInfo/ClaimsPartyRoleCd='DOC') and not($pos='UNT1'))">

                            <xsl:variable name="VehId" select="$pos"></xsl:variable>
                            <xsl:variable name="NameId" select="@id"></xsl:variable>
 

    					<xsl:if test="substring($VehId,4,1)=substring($NameId,3,1)+1">

 								<PARTY_INVOLVED CODE="DRVR">
									<xsl:attribute name="ID">
										<xsl:value-of select="concat('ENT',position())"/>
									</xsl:attribute>
									<TYPE>Driver</TYPE>
								</PARTY_INVOLVED>
							
						</xsl:if><!--UNI01525a-->
								
							</xsl:when>
							<xsl:when test="((ClaimsPartyInfo/ClaimsPartyRoleCd='PRO') and not($pos='UNT1'))">
							
							
                            <xsl:variable name="VehId" select="$pos"></xsl:variable>
                            <xsl:variable name="NameId" select="@id"></xsl:variable>

    					<xsl:if test="substring($VehId,4,1)=substring($NameId,3,1)+1">

								<PARTY_INVOLVED CODE="OWN">
									<xsl:attribute name="ID">
										<xsl:value-of select="concat('ENT',position())"/>
									</xsl:attribute>
									<TYPE>Owner</TYPE>
								</PARTY_INVOLVED>
							<xsl:if test="ClaimsPartyInfo/OtherOrPriorPolicy/InsurerName">
								<PARTY_INVOLVED CODE="OTHCMPNY">
									<xsl:attribute name="ID">
										<xsl:value-of select="concat('ENT',position() + 300)"/>
									</xsl:attribute>
									<TYPE>Other Company</TYPE>
								</PARTY_INVOLVED>
                            </xsl:if>		
						</xsl:if>								
							</xsl:when>
							
						</xsl:choose>
					</xsl:for-each>
				</UNIT>
			</xsl:for-each>
		</INVOLVED_UNITS>
	</xsl:template>
	<xsl:template name="AddEntityAddress">
	<xsl:param name="Adr" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq"/>
	<xsl:variable name="AdrCount" select="count($Adr/ClaimsParty/GeneralPartyInfo/Addr/Addr1[string-length()>1])"/>
		<ADDRESSES>
			<xsl:attribute name="COUNT"><xsl:value-of select="$AdrCount"/></xsl:attribute>
			<xsl:for-each select="$Adr/ClaimsParty">
			<xsl:if test="string-length(GeneralPartyInfo/Addr/Addr1)>0">
			<ADDRESS>
				<xsl:attribute name="ID">
				<xsl:value-of select="concat('ADR',position())"/>
				</xsl:attribute>
				<ADDRESS_LINE>
				<xsl:value-of select="GeneralPartyInfo/Addr/Addr1"/>
				</ADDRESS_LINE>
				<CITY>
				<xsl:value-of select="GeneralPartyInfo/Addr/City"/>
				</CITY>
				<STATE>
				<xsl:attribute name="CODE">
				<xsl:value-of select="GeneralPartyInfo/Addr/StateProvCd"/></xsl:attribute>
				</STATE>
				<POSTAL_CODE>
					<xsl:value-of select="GeneralPartyInfo/Addr/PostalCode"/>
				</POSTAL_CODE>
			</ADDRESS>
			</xsl:if>
			</xsl:for-each>
		</ADDRESSES>
	</xsl:template>		

	<xsl:template name="AddAutoComments">
		<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt)>0">
		<COMMENTS COUNT="1" NEXT_ID="2">
			<COMMENT ID="CMT01">
			<CATEGORY TYPE="N">FNOL General Comment</CATEGORY>
			<SUBJECT>Remark</SUBJECT>
			<AUTHOR USER_ID="UBIAgent"></AUTHOR>
	        <TEXT><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt"/></TEXT>
			</COMMENT>
		</COMMENTS>
		</xsl:if>
	</xsl:template>
	
	
</xsl:stylesheet>