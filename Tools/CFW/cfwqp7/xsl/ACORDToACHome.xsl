<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xmlns:acord="http://www.ACORD.org/standards/PC_Surety/ACORD1.8.0/xml/" xmlns:csc="http://www.csc.com/standards/PC_SuretyExtensions/v1/xml/">
	<xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>
	<xsl:template name="AcordToACHome" match="/">
	<xsl:variable name="PolicyNumber" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyNumber"/>
		<xsl:variable name="EntityNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty"/>
		<CLAIM ID="CLM1" EVENT="EVT1">
			<CLAIM_NUMBER>GENERATE</CLAIM_NUMBER>
			<CLAIM_STATUS CODE="O">Open</CLAIM_STATUS>
			<FILING_STATE>
					<xsl:attribute name="CODE"><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ControllingStateProvCd"/></xsl:attribute>
			</FILING_STATE>
			<xsl:variable name="LossDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossDt"/>
			<DATE_OF_LOSS>
				<xsl:attribute name="YEAR">
					<xsl:value-of select="substring($LossDt,1,4)"/>
				</xsl:attribute>
				<xsl:attribute name="MONTH">
					<xsl:value-of select="substring($LossDt,6,2)"/>
				</xsl:attribute>
				<xsl:attribute name="DAY">
					<xsl:value-of select="substring($LossDt,9,2)"/>
				</xsl:attribute>
				<xsl:value-of select="$LossDt"/>
			</DATE_OF_LOSS>
			<xsl:variable name="LossTime" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/LossTime"/>
			<TIME_OF_LOSS>
				<xsl:attribute name="HOUR">
					<xsl:value-of select="substring($LossTime,1,2)"/>
				</xsl:attribute>
				<xsl:attribute name="MINUTE">
					<xsl:value-of select="substring($LossTime,4,2)"/>
				</xsl:attribute>
				<xsl:attribute name="SECOND">
					<xsl:value-of select="substring($LossTime,7,2)"/>
				</xsl:attribute>
				<xsl:value-of select="$LossTime"/>
			</TIME_OF_LOSS>
			<xsl:variable name="RptDt" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ClaimsReported/ReportedDt"/>
			<DATE_REPORTED>
				<xsl:attribute name="YEAR">
					<xsl:value-of select="substring($RptDt,1,4)"/>
				</xsl:attribute>
				<xsl:attribute name="MONTH">
					<xsl:value-of select="substring($RptDt,6,2)"/>
				</xsl:attribute>
				<xsl:attribute name="DAY">
					<xsl:value-of select="substring($RptDt,9,2)"/>
				</xsl:attribute>
				<xsl:value-of select="substring($RptDt,1,10)"/>
			</DATE_REPORTED>
			<TIME_REPORTED>
				<xsl:attribute name="HOUR">
					<xsl:value-of select="substring($RptDt,12,2)"/>
				</xsl:attribute>
				<xsl:attribute name="MINUTE">
					<xsl:value-of select="substring($RptDt,15,2)"/>
				</xsl:attribute>
				<xsl:attribute name="SECOND">
					<xsl:value-of select="substring($RptDt,18,2)"/>
				</xsl:attribute>
				<xsl:value-of select="substring($RptDt,12,8)"/>
			</TIME_REPORTED>
			<AUTHORITY_CONTACTED>
				<AUTHORITY>
					<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ResponsibleDept"/>
				</AUTHORITY>
				<REPORT_NUMBER>
					<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ClaimsReported/ReportNumber"/>
				</REPORT_NUMBER>
			</AUTHORITY_CONTACTED>
			<PREVIOUSLY_REPORTED>
				<xsl:attribute name="INDICATOR">
					<xsl:if test="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/PreviouslyReportedInd='1'">
						<xsl:text>Yes</xsl:text>
					</xsl:if>
					<xsl:if test="not(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/PreviouslyReportedInd ='1')">
						<xsl:text>No</xsl:text>
					</xsl:if>
				</xsl:attribute>
			</PREVIOUSLY_REPORTED>
			<ACCIDENT_DESCRIPTION CODE=""><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/IncidentDesc"/></ACCIDENT_DESCRIPTION>
			<POLICY>
				<POLICY_NUMBER>
					<xsl:attribute name="SYMBOL">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/CompanyProductCd"></xsl:value-of>
					</xsl:attribute>
					<xsl:attribute name="LOCATION">00</xsl:attribute>
					<xsl:attribute name="MODULE">
					    <xsl:choose>
						<xsl:when test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyVersion)&gt;0">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/PolicyVersion"/>
						</xsl:when>
						<xsl:otherwise>00</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:value-of select="$PolicyNumber"/>
				</POLICY_NUMBER>
				<MASTER_COMPANY>
					<xsl:attribute name="CODE">
						<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/NAICCd"></xsl:value-of>
					</xsl:attribute>
				</MASTER_COMPANY>
				<LINE_OF_BUSINESS>
<!--Unitrin changed to CPP <xsl:attribute name="CODE">HP</xsl:attribute></LINE_OF_BUSINESS>-->
        			<xsl:attribute name="CODE">CPP</xsl:attribute></LINE_OF_BUSINESS>
 
				<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectivenDt)>1">
				<EFFECTIVE_DATE>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/EffectiveDt,9,2)"/>
					</xsl:attribute>
				</EFFECTIVE_DATE>
				</xsl:if>
				<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt)>1">
				<EXPIRATION_DATE>
					<xsl:attribute name="YEAR">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,1,4)"/>
					</xsl:attribute>
					<xsl:attribute name="MONTH">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,6,2)"/>
					</xsl:attribute>
					<xsl:attribute name="DAY">
						<xsl:value-of select="substring(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/ContractTerm/ExpirationDt,9,2)"/>
					</xsl:attribute>
				</EXPIRATION_DATE>
				</xsl:if>
				<xsl:for-each select="$EntityNode">
					<xsl:choose>
						<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='Insured'">
							<PARTY_INVOLVED CODE="PHLD">
								<xsl:attribute name="ID">
									<xsl:value-of select="concat('ENT',position())"/>
								</xsl:attribute>
								<ROLE>PolicyHolder</ROLE>
							</PARTY_INVOLVED>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</POLICY>
			
			<PROPERTY_LOSS>
			<UNIT_REFERENCE ID="UNT1"/>
			<LOSS_INFORMATION>
			<DAMAGE_DESCRIPTION><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/IncidentDesc"/></DAMAGE_DESCRIPTION>
			<ESTIMATED_DAMAGE_AMOUNT>
							<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/ProbableIncurredAmt/Amt"/>
						</ESTIMATED_DAMAGE_AMOUNT>
			<OTHER_INSURANCE>
				<POLICY_NUMBER>
					<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/PolicyNumber"/>
				</POLICY_NUMBER>
				<!--Begin 06/06/07 Adding Coverage and Policy Limits tags-->
				<COVERAGE>
					<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/Coverage/CoverageCd)>0">
						<COVERAGE_TYPE>
							<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/Coverage/CoverageCd"/>
						</COVERAGE_TYPE>
					</xsl:if>	
					<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/Coverage/Limit/FormatCurrencyAmt/Amt)>0">
						<COVERAGE_LIMIT>
							<xsl:attribute name="AMOUNT">
								<xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/Coverage/Limit/FormatCurrencyAmt/Amt"/>
							</xsl:attribute>Policy Limits</COVERAGE_LIMIT>
					</xsl:if>						
				</COVERAGE>
				<!--Begin 06/06/07 Adding Coverage and Policy Limits tags-->
			</OTHER_INSURANCE>
			</LOSS_INFORMATION>
			</PROPERTY_LOSS>
			<xsl:for-each select="$EntityNode">
				<xsl:choose>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='MORTG'">
						<PARTY_INVOLVED CODE="MT">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Mortgagee</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='CC'">
						<PARTY_INVOLVED CODE="CNT">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Contact</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
					<xsl:when test="ClaimsPartyInfo/ClaimsPartyRoleCd='OT'">
						<PARTY_INVOLVED CODE="OTH">
							<xsl:attribute name="ID">
								<xsl:value-of select="concat('ENT',position())"/>
							</xsl:attribute>
							<TYPE>Other</TYPE>
						</PARTY_INVOLVED>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
			<INVOLVED_UNITS COUNT="1">
			<UNIT ID="UNT1" UNIT_TYPE="L">
			<!--Begin 06/06/07 Adding Other Insurance tag-->
					<OTHER_INSURANCE>
						<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/InsurerName)>0">
							<xsl:attribute name="INDICATOR">YES</xsl:attribute>					
						</xsl:if>	
						<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/InsurerName)=0">
							<xsl:attribute name="INDICATOR">NO</xsl:attribute>					
						</xsl:if>	
					</OTHER_INSURANCE>
            <!--End 06/06/07 Adding Other Insurance tag--> 
			<DESCRIPTION><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsOccurrence/Addr/Addr1"/></DESCRIPTION>
	        <!--Begin 07/03/07 Adding Insured By tag (not at entity level for Property LOB) -->			
			<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/Policy/OtherOrPriorPolicy/InsurerName)>0">
				<PARTY_INVOLVED CODE="OTHCMPNY">
					<xsl:attribute name="ID">
						<xsl:value-of select="concat('ENT',position() + 300)"/>
					</xsl:attribute>
					<TYPE>Other Company</TYPE>
				</PARTY_INVOLVED>
             </xsl:if>		
			<!--End 07/03/07 Adding Insured By tag -->			
			</UNIT>
			</INVOLVED_UNITS>
			<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt)>0">
			<COMMENT_REFERENCE ID="CMT01"/>
			</xsl:if>
			</CLAIM>
			
			<!--ADDITIONAL_DATA-->
			<!-- Usually to enter supplemental fields-->
			<!--/ADDITIONAL_DATA-->
			
			<xsl:call-template name="AddInvolvedEntity"></xsl:call-template>
			<xsl:call-template name="AddEntityAddress"></xsl:call-template>
			<xsl:call-template name="AddHomeComments"></xsl:call-template>
			
	</xsl:template>
	
	<xsl:template name="AddHomeComments">
		<xsl:variable name="EntityNode" select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty"/>	
			<xsl:if test="string-length(/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt)>0">
			<COMMENTS COUNT="01" NEXT_ID="02">
					<COMMENT ID="CMT01">
					<CATEGORY TYPE="N">FNOL General Comment</CATEGORY>
					<SUBJECT>Remark</SUBJECT>
					<AUTHOR USER_ID="UBIAgent"></AUTHOR>
         			<TEXT><xsl:value-of select="/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/RemarksTxt"/></TEXT>
					</COMMENT>
			</COMMENTS>		
			</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>