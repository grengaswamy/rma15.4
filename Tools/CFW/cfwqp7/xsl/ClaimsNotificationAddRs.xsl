<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" xmlns:acord="http://www.ACORD.org/standards/PC_Surety/ACORD1.8.0/xml/" xmlns:csc="http://www.csc.com/standards/PC_SuretyExtensions/v1/xml/" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
   <xsl:template match="/">
      <ACORD>
         <SignonRs>
            <Status>
               <StatusCd />

               <StatusDesc />
            </Status>

            <ClientDt>
               <xsl:value-of select="concat(/CLAIMEXTRACT/HEADER/DATE_CREATED/@YEAR,'/',/CLAIMEXTRACT/HEADER/DATE_CREATED/@MONTH,'/',/CLAIMEXTRACT/HEADER/DATE_CREATED/@DAY)" />
            </ClientDt>

            <CustLangPref>US-en</CustLangPref>

            <ClientApp>
               <Org />

               <Name />

               <Version />
            </ClientApp>

            <ServerDt>
               <xsl:value-of select="concat(/CLAIMEXTRACT/HEADER/DATE_CREATED/@YEAR,'/',/CLAIMEXTRACT/HEADER/DATE_CREATED/@MONTH,'/',/CLAIMEXTRACT/HEADER/DATE_CREATED/@DAY)" />
            </ServerDt>

            <Language>US-en</Language>
         </SignonRs>

         <ClaimsSvcRs>
            <RqUID>
               <xsl:value-of select="/CLAIMEXTRACT/PROCESSING_MESSAGE/REPONSE_MESSAGE/REQUESTING_DOC/RqUID" />
            </RqUID>

            <ClaimsNotificationAddRs>
               <RqUID />

               <TransactionEffectiveDt />

               <TransactionResponseDt>
               </TransactionResponseDt>

               <CurCd />

               <MsgStatus>
                  <MsgStatusCd>
                     <xsl:choose>
                        <xsl:when test="/CLAIMEXTRACT/PROCESSING_MESSAGE/REPONSE_MESSAGE/MESSAGE='SUCCESSFUL'">Success</xsl:when>

                        <xsl:otherwise>Error</xsl:otherwise>
                     </xsl:choose>
                  </MsgStatusCd>

                  <MsgErrorCd />

                  <MsgStatusDesc>
                     <xsl:value-of select="/CLAIMEXTRACT/PROCESSING_MESSAGE/REPONSE_MESSAGE/ERRORS/ERROR" />
                  </MsgStatusDesc>
               </MsgStatus>

               <ClaimsOccurrence>
                  <ItemIdInfo>
                     <InsurerId>
                        <xsl:value-of select="/CLAIMEXTRACT/CLAIM/CLAIM_NUMBER" />
                     </InsurerId>
                  </ItemIdInfo>
               </ClaimsOccurrence>
            </ClaimsNotificationAddRs>
         </ClaimsSvcRs>
      </ACORD>
   </xsl:template>
</xsl:stylesheet>

