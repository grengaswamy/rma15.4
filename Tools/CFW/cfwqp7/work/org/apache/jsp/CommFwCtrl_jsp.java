package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.io.*;
import java.util.*;
import org.w3c.dom.Document;
import com.csc.cfw.process.AdapterFactory;
import com.csc.cfw.process.AdapterProperties;
import com.csc.cfw.process.MessageQueryCache;
import com.csc.cfw.process.MessageRouteFactory;
import com.csc.cfw.process.ProcessFactory;
import com.csc.cfw.recordbuilder.RecordManagerFactory;
import com.csc.cfw.transport.TransportFactory;
import com.csc.cfw.util.CommFwSystem;
import com.csc.fw.util.Configurable;
import com.csc.fw.util.FwException;
import com.csc.fw.util.FwSystem;
import com.csc.fw.util.PropertyFileManager;
import com.csc.fw.util.ServletUtil;
import com.csc.fw.util.WebServiceFactory;
import com.csc.fw.util.XMLDocument;
import com.csc.fw.util.XMLFileConfigurator;

public final class CommFwCtrl_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n");
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<HTML>\r\n");
      out.write("  <LINK href=\"includes/INFUtil.css\" type=\"text/css\" rel=\"stylesheet\">\r\n");
      out.write("  <HEAD>\r\n");
      out.write("    <TITLE>System Control - Communications Framework </TITLE>\r\n");
      out.write("    <META name=\"GENERATOR\" content=\"IBM WebSphere Studio\">\r\n");
      out.write("  </HEAD>\r\n");
      out.write("  <BODY BGCOLOR=\"#C0C0C0\">\r\n");
      out.write("    <script LANGUAGE=\"JavaScript\">\r\n");
      out.write("    /*----------------------------------------------------*/\r\n");
      out.write("    /* Set up the values and submit the Transaction form  */\r\n");
      out.write("    /* to create a new process.                           */\r\n");
      out.write("    /*----------------------------------------------------*/\r\n");
      out.write("    function createNewProcessFile(tranForm)\r\n");
      out.write("    {\r\n");
      out.write("        var procName = tranForm.transProcName.value;\r\n");
      out.write("        if (procName == null || procName.length == 0)\r\n");
      out.write("        {\r\n");
      out.write("            alert(\"No Process Name Specified\");\r\n");
      out.write("            return;\r\n");
      out.write("        }\r\n");
      out.write("        \r\n");
      out.write("        tranForm.creProcessName.value = procName;\r\n");
      out.write("        tranForm.submit();\r\n");
      out.write("    }\r\n");
      out.write("    </script>\r\n");
      out.write("    \r\n");
      out.write("    <script language=\"JavaScript\">\r\n");
      out.write("    var lastSearchPos = 0;\r\n");
      out.write("    var lastSearchText = \"\";\r\n");
      out.write("    /*----------------------------------------------------*/\r\n");
      out.write("    /* Find a text string in a text area                  */\r\n");
      out.write("    /*----------------------------------------------------*/\r\n");
      out.write("    function findTextAreaString(searchForm, searchTextArea)\r\n");
      out.write("    {\r\n");
      out.write("        var searchText;\r\n");
      out.write("        if (searchForm.searchTextInput)\r\n");
      out.write("            searchText = searchForm.searchTextInput.value;\r\n");
      out.write("        else\r\n");
      out.write("            searchText = prompt(\"Please enter search text:\",lastSearchText);\r\n");
      out.write("        if (!searchText)\r\n");
      out.write("        {\r\n");
      out.write("            alert(\"No search text specified\");\r\n");
      out.write("            return;\r\n");
      out.write("        }\r\n");
      out.write("        if (searchText != lastSearchText)\r\n");
      out.write("        {\r\n");
      out.write("            lastSearchText = searchText;\r\n");
      out.write("            lastSearchPos = 0;\r\n");
      out.write("        }\r\n");
      out.write("        var data = searchTextArea.value;\r\n");
      out.write("        searchText = searchText.toLowerCase();\r\n");
      out.write("        data = data.toLowerCase();\r\n");
      out.write("        var datasubstr = data.substring(lastSearchPos);\r\n");
      out.write("        var pos = datasubstr.search(searchText);\r\n");
      out.write("        if (pos < 0)\r\n");
      out.write("        {\r\n");
      out.write("            alert(\"Search text not found\");\r\n");
      out.write("            lastSearchPos = 0;\r\n");
      out.write("            return;\r\n");
      out.write("        }\r\n");
      out.write("        pos += lastSearchPos;\r\n");
      out.write("        var count = 0;\r\n");
      out.write("        for (var i = 0; i < pos; i++)\r\n");
      out.write("        {\r\n");
      out.write("            if (data.charAt(i) == '\\r') \r\n");
      out.write("                count++; \r\n");
      out.write("        }\r\n");
      out.write("        if (searchTextArea.createTextRange)\r\n");
      out.write("        {\r\n");
      out.write("            var rng = searchTextArea.createTextRange();\r\n");
      out.write("            if (rng)\r\n");
      out.write("            {\r\n");
      out.write("                //alert(\"Current search Pos: \" + pos);\r\n");
      out.write("                if (false) //if (false == rng.findText(searchText,lastSearchPos))\r\n");
      out.write("                    alert(\"Search text not found\");\r\n");
      out.write("                else\r\n");
      out.write("                {\r\n");
      out.write("                    rng.moveStart(\"character\", pos - count);\r\n");
      out.write("                    rng.collapse();\r\n");
      out.write("                    rng.select();\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("            \r\n");
      out.write("        }\r\n");
      out.write("        lastSearchPos = pos + searchText.length;\r\n");
      out.write("    }\r\n");
      out.write("    </script>\r\n");
      out.write("\r\n");
      out.write("    <DIV ALIGN=\"Center\" ID=\"HtmlDiv1\">\r\n");
      out.write("    <TABLE>\r\n");
      out.write("      <TR>\r\n");
      out.write("        <TD ALIGN=\"Center\"  >\r\n");
      out.write("          <H4>Communications Framework System Control</H4>\r\n");
      out.write("        </TD>\r\n");
      out.write("      </TR>\r\n");
      out.write("      <TR>\r\n");
      out.write("        <TD  >\r\n");
      out.write("\r\n");

    FwSystem fwSys = CommFwSystem.current();
    String updateTextBuffer = null;
    // Control File Section - Determine the section to display
    String cfs = request.getParameter("cfs");
    Configurable factory = null;
    XMLFileConfigurator xmlConfig = new XMLFileConfigurator(fwSys, request);
    boolean bIE = (ServletUtil.determineBrowserVersion(request).getType() == ServletUtil.BROWSER_MSIE);
    
    /***************************/
    /* Parameters for Adapters */
    /***************************/
    factory = AdapterFactory.current();
    String adapterFileName = factory.configFile();
    List adapterFileList = factory.configFileList(null);
    if (adapterFileList == null)
        adapterFileList = new ArrayList();
    if (adapterFileList.size() == 0)
        adapterFileList.add(adapterFileName);
    String adapterPropFile = request.getParameter("adapterPropFile");
    if (adapterPropFile != null)
        adapterFileName = adapterPropFile;
    else
    {
        /* Default the filename to CommFwAdapterInfo&#x25;APPLICATION&#x25;.xml */
        String adapterFileDefault = adapterFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && adapterFileDefault.endsWith(sfx))
        {
            adapterFileDefault = adapterFileDefault.substring(0, adapterFileDefault.length() - sfx.length());
            adapterFileDefault += app + sfx;
        }
        for (int i = 0; i < adapterFileList.size(); i++)
        {
            String s = adapterFileList.get(i).toString();
            if (adapterFileDefault.equals(s))
            {
                adapterFileName = adapterFileDefault;
                break;
            }
        }
    }
    
    String adapterFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, adapterFileName);
    String errorMessageAdapter = null;
    String ronlyBuffAdapter = null;
    boolean ronlyAdapter = false;
    if (adapterFilePath == null)
    {
        // The adapter file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(adapterFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffAdapter = sw.getBuffer().toString();
            ronlyAdapter = true;
        }
    }
    
    String adapterDisplayQueryCache = request.getParameter("adapterDisplayQueryCache");
    if (adapterDisplayQueryCache != null && adapterDisplayQueryCache.length() == 0)
    	adapterDisplayQueryCache = null;
    

    /*******************************/
    /* Parameters for Transactions */
    /*******************************/
    factory = ProcessFactory.current();
    String tranFileName = factory.configFile();
    String procPath = fwSys.getProperty(FwSystem.SYSPROP_processLocation).toString();
    if (procPath.endsWith("/"))
        procPath = procPath.substring(0, procPath.length() - 1);
    String path = request.getContextPath();
    int temp = (procPath.indexOf(path) + path.length());
    procPath = procPath.substring(temp);
    procPath = getServletConfig().getServletContext().getRealPath(procPath);
    
    String tranPropFile = request.getParameter("tranPropFile");
    String newProcessName = request.getParameter("creProcessName");
    if (newProcessName != null && newProcessName.length() > 0)
    {
        // We need to create a new process file.
        String fn = "Process" + FwSystem.replaceAll(newProcessName.trim(), " ", "_") + ".xml";
        tranPropFile = fn;
        String fnPath = procPath + File.separatorChar + fn;
        File procFile = new File(fnPath);
        if (!procFile.exists())
	        procFile.createNewFile();
    }
    
    List tranFileList = factory.configFileList(procPath);
    if (tranFileList == null)
        tranFileList = new ArrayList();
    if (tranFileList.size() == 0)
        tranFileList.add(tranFileName);
    if (tranPropFile != null)
        tranFileName = tranPropFile;
    else
    {
        /* Default the filename to CommFwMessages&#x25;APPLICATION&#x25;.xml */
        String tranFileDefault = tranFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && tranFileDefault.endsWith(sfx))
        {
            tranFileDefault = tranFileDefault.substring(0, tranFileDefault.length() - sfx.length());
            tranFileDefault += app + sfx;
        }
        for (int i = 0; i < tranFileList.size(); i++)
        {
            String s = tranFileList.get(i).toString();
            if (tranFileDefault.equals(s))
            {
                tranFileName = tranFileDefault;
                break;
            }
        }
    }
    String tranFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, tranFileName);
    if (tranFilePath == null)
    {
        tranFilePath = procPath + File.separatorChar + tranFileName;
        File file = new File(tranFilePath);
        if (!file.exists())
            tranFilePath = null;
    }
    
    String errorMessageTran = null;
    String ronlyBuffTran = null;
    boolean ronlyTran = false;
    if (tranFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(tranFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffTran = sw.getBuffer().toString();
            ronlyTran = true;
        }
    }

    /*****************************/
    /* Parameters for Transports */
    /*****************************/
    factory = TransportFactory.current();
    String transportFileName = factory.configFile();
    List transportFileList = factory.configFileList(null);
    if (transportFileList == null)
        transportFileList = new ArrayList();
    if (transportFileList.size() == 0)
        transportFileList.add(transportFileName);
    String transportPropFile = request.getParameter("transportPropFile");
    if (transportPropFile != null)
        transportFileName = transportPropFile;
    else
    {
        /* Default the filename to CommFwTransport&#x25;APPLICATION&#x25;.xml */
        String transportFileDefault = transportFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && transportFileDefault.endsWith(sfx))
        {
            transportFileDefault = transportFileDefault.substring(0, transportFileDefault.length() - sfx.length());
            transportFileDefault += app + sfx;
        }
        for (int i = 0; i < transportFileList.size(); i++)
        {
            String s = transportFileList.get(i).toString();
            if (transportFileDefault.equals(s))
            {
                transportFileName = transportFileDefault;
                break;
            }
        }
    }
    
    String transportFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, transportFileName);
    String errorMessageTransport = null;
    String ronlyBuffTransport = null;
    boolean ronlyTransport = false;
    if (transportFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(transportFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffTransport = sw.getBuffer().toString();
            ronlyTransport = true;
        }
    }
	
    /********************************/
    /* Parameters for Message Route */
    /********************************/
    factory = MessageRouteFactory.current();
    String messageRouteFileName = factory.configFile();
    List messageRouteFileList = factory.configFileList(null);
    if (messageRouteFileList == null)
    	messageRouteFileList = new ArrayList();
    if (messageRouteFileList.size() == 0)
    	messageRouteFileList.add(messageRouteFileName);
    String messageRoutePropFile = request.getParameter("messageRoutePropFile");
    if (messageRoutePropFile != null)
    	messageRouteFileName = messageRoutePropFile;
    else
    {
        /* Default the filename to CommFwMessageRoute&#x25;APPLICATION&#x25;.xml */
        String messageRouteFileDefault = messageRouteFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && messageRouteFileDefault.endsWith(sfx))
        {
        	messageRouteFileDefault = messageRouteFileDefault.substring(0, messageRouteFileDefault.length() - sfx.length());
        	messageRouteFileDefault += app + sfx;
        }
        for (int i = 0; i < messageRouteFileList.size(); i++)
        {
            String s = messageRouteFileList.get(i).toString();
            if (messageRouteFileDefault.equals(s))
            {
            	messageRouteFileName = messageRouteFileDefault;
                break;
            }
        }
    }
    
    String messageRouteFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, messageRouteFileName);
    String errorMessageMessageRoute = null;
    String ronlyBuffMessageRoute = null;
    boolean ronlyMessageRoute = false;
    if (messageRouteFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(messageRouteFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffMessageRoute = sw.getBuffer().toString();
            ronlyMessageRoute = true;
        }
    }
	
    /*********************************/
    /* Parameters for Record Control */
    /*********************************/
    factory = RecordManagerFactory.current();
    String recordCtrlFileName = factory.configFile();
    List recordCtrlFileList = factory.configFileList(null);
    if (recordCtrlFileList == null)
        recordCtrlFileList = new ArrayList();
    if (recordCtrlFileList.size() == 0)
        recordCtrlFileList.add(recordCtrlFileName);
    String recordCtrlPropFile = request.getParameter("recordCtrlPropFile");
    if (recordCtrlPropFile != null)
        recordCtrlFileName = recordCtrlPropFile;
    else
    {
        /* Default the filename to CommFwRecord&#x25;APPLICATION&#x25;.xml */
        String recordCtrlFileDefault = recordCtrlFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && recordCtrlFileDefault.endsWith(sfx))
        {
            recordCtrlFileDefault = recordCtrlFileDefault.substring(0, recordCtrlFileDefault.length() - sfx.length());
            recordCtrlFileDefault += app + sfx;
        }
        for (int i = 0; i < recordCtrlFileList.size(); i++)
        {
            String s = recordCtrlFileList.get(i).toString();
            if (recordCtrlFileDefault.equals(s))
            {
                recordCtrlFileName = recordCtrlFileDefault;
                break;
            }
        }
    }
    
    String recordCtrlFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, recordCtrlFileName);
    String errorMessageRecordCtrl = null;
    String ronlyBuffRecordCtrl = null;
    boolean ronlyRecordCtrl = false;
    if (recordCtrlFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(recordCtrlFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffRecordCtrl = sw.getBuffer().toString();
            ronlyRecordCtrl = true;
        }
    }
    
    /*******************************/
    /* Parameters for Web Services */
    /*******************************/
    String webSvcFileName = new WebServiceFactory().configFile();
    String webSvcFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, webSvcFileName);
    String errorMessageWebSvc = null;
    String ronlyBuffWebSvc = null;
    boolean ronlyWebSvc = false;
    if (webSvcFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(webSvcFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffWebSvc = sw.getBuffer().toString();
            ronlyWebSvc = true;
        }
    }

    if ("POST".equals(request.getMethod()))
    {
        String func = request.getParameter("function");
        if ("Reset Adapter Control Cache".equalsIgnoreCase(func))
        {
            AdapterFactory.reset();
        }
        else
        if ("Reset Data Query cache".equalsIgnoreCase(func) && adapterDisplayQueryCache != null)
        {
			String name = adapterDisplayQueryCache;
			factory = AdapterFactory.current();
			AdapterProperties properties = (AdapterProperties)factory.configEntryMap(name, null);
			MessageQueryCache cache = properties.getCachedQueryTransaction();
			cache.cacheClear();
        }
        if ("Update Adapter Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("AdapterTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        adapterFilePath,
                        adapterFileName,
                        "Ädapter",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageAdapter = "<FONT COLOR='#FF0000'><B>Updated Adapter XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageAdapter == null)
                try
                {
                    PropertyFileManager.writePropertyFile(adapterFilePath, updateTextBuffer);
                    AdapterFactory.current().configSetFileCache(adapterFileName, updateTextBuffer);
                    AdapterFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageAdapter = "<FONT COLOR='#FF0000'><B>Error updating Adapter file::<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Transport Control Cache".equalsIgnoreCase(func))
        {
            TransportFactory.reset();
        }
        else
        if ("Update Transport Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("TransportTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        transportFilePath,
                        transportFileName,
                        "Transport",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageTransport = "<FONT COLOR='#FF0000'><B>Updated Transport XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageTransport == null)
                try
                {
                    PropertyFileManager.writePropertyFile(transportFilePath, updateTextBuffer);
                    TransportFactory.current().configSetFileCache(transportFileName, updateTextBuffer);
                    TransportFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageTransport = "<FONT COLOR='#FF0000'><B>Error updating Transport file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Transaction Control Cache".equalsIgnoreCase(func))
        {
            ProcessFactory.reset();
        }
        else
        if ("Update Transaction Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("TransactionTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                boolean isMsgFile = tranFileName.startsWith("CommFwMessages");
                if (isMsgFile)
                {
                    xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        tranFilePath,
                        tranFileName,
                        "Message",
                        "name",
                        true,
                        true,
                        "location");
                }
                else
                {
                    xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        tranFilePath,
                        tranFileName,
                        "Transaction Control",
                        null,
                        false,
                        false,
                        null);
                }
            }
            catch (FwException e)
            {
                errorMessageTran = "<FONT COLOR='#FF0000'><B>Updated Transaction/Message XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageTran == null)
                try
                {
                    PropertyFileManager.writePropertyFile(tranFilePath, updateTextBuffer);
                    ProcessFactory.current().configSetFileCache(tranFileName, updateTextBuffer);
                    ProcessFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageTran = "<FONT COLOR='#FF0000'><B>Error updating Transaction/Message file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Record Control Cache".equalsIgnoreCase(func))
        {
            RecordManagerFactory.reset();
        }
        else
        if ("Update Record Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("RecordTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        recordCtrlFilePath,
                        recordCtrlFileName,
                        "Record Control",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageRecordCtrl = "<FONT COLOR='#FF0000'><B>Updated Record Control XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageRecordCtrl == null)
                try
                {
                    PropertyFileManager.writePropertyFile(recordCtrlFilePath, updateTextBuffer);
                    RecordManagerFactory.current().configSetFileCache(recordCtrlFileName, updateTextBuffer);
                    RecordManagerFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageRecordCtrl = "<FONT COLOR='#FF0000'><B>Error updating Record Control file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Message Route Control Cache".equalsIgnoreCase(func))
        {
            MessageRouteFactory.reset();
        }
        else
        if ("Update Message Route Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("MessageRouteTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        messageRouteFilePath,
                        messageRouteFileName,
                        "Message Route",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageMessageRoute = "<FONT COLOR='#FF0000'><B>Updated Message Route XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageMessageRoute == null)
                try
                {
                    PropertyFileManager.writePropertyFile(messageRouteFilePath, updateTextBuffer);
                    MessageRouteFactory.current().configSetFileCache(messageRouteFileName, updateTextBuffer);
                    MessageRouteFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageMessageRoute = "<FONT COLOR='#FF0000'><B>Error updating Message Route file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Web Services Control Cache".equalsIgnoreCase(func))
        {
            WebServiceFactory.reset();
        }
        else
        if ("Update Web Services Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("WebSvcTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        webSvcFilePath,
                        webSvcFileName,
                        "Web Services",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageWebSvc = "<FONT COLOR='#FF0000'><B>Updated Web Services XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageWebSvc == null)
                try
                {
                    PropertyFileManager.writePropertyFile(webSvcFilePath, updateTextBuffer);
                    WebServiceFactory.storeFactoryFileCache(updateTextBuffer);
                    WebServiceFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageWebSvc = "<FONT COLOR='#FF0000'><B>Error updating Web Services file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
    }

      out.write("\r\n");
      out.write("          <P />\r\n");
      out.write("          <TABLE>\r\n");
      out.write("            <TR ALIGN=\"Center\">\r\n");
      out.write("             <TD ALIGN=\"Center\"  >\r\n");
      out.write("              <TABLE>\r\n");
      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                    <TABLE cellspacing=\"0\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD ALIGN=\"left\"><A href=\"CommFwSys.jsp\">System Configuration</A></TD>\r\n");
      out.write("                        <TD ALIGN=\"center\"><A href=\"CommFwCtrl.jsp\">System Control</A></TD>\r\n");
      out.write("                        <TD ALIGN=\"center\"><A href=\"CommFwCfg.jsp\">Application Configuration</A></TD>\r\n");
      out.write("                        <TD ALIGN=\"right\"><A href=\"CommFwStatus.jsp\">System Status</A></TD>\t\t\t\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\">\r\n");
      out.write("                          <TABLE cellspacing=\"0\" WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"left\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=adapters\">Adapter Control</A></div></TD>\r\n");
      out.write("                              <TD ALIGN=\"center\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=transports\">Transport Control</A></div></TD>\r\n");
      out.write("                              <TD ALIGN=\"right\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=transactions\">Transaction Control</A></div></TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"left\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=records\">Record Control</A></div></TD>\r\n");
      out.write("                              <TD ALIGN=\"center\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=messageRoutes\">Message Route Control</A></div></TD>\r\n");
      out.write("                              <TD ALIGN=\"right\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=webSvcs\">Web Services Control</A></div></TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD COLSPAN=\"3\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                </TR>\r\n");
      out.write("\r\n");
      out.write("\r\n");

		if (cfs == null || "adapters".equals(cfs))
		{
			if (adapterDisplayQueryCache == null)
			{
			// Begin Adapters Control Section

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfwCfgAdapterForm\" ACTION=\"CommFwCtrl.jsp?cfs=adapters\">\r\n");
      out.write("                    <input name=\"adapterDisplayQueryCache\" type=\"hidden\" />&nbsp;\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Adapter Control</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\">\r\n");
      out.write("                                <B>Current Adapters:</B><BR />\r\n");
      out.write("                                <TABLE class=\"PAGEHLIGHT\" BORDER=\"0\" CELLPADDING=\"0\" CELLSPACING=\"0\" WIDTH=\"100%\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TH ALIGN=\"center\">Application</TH>\r\n");
      out.write("                                    <TH ALIGN=\"center\">Location</TH>\r\n");
      out.write("                                    <TH ALIGN=\"center\">Transport</TH>\r\n");
      out.write("                                    <TH ALIGN=\"center\">MessageRoute</TH>\r\n");
      out.write("                                    <TH ALIGN=\"center\">CommSfx</TH>\r\n");
      out.write("                                  </TR>\r\n");

				String colorClass = null;
				factory = AdapterFactory.current();
				Object[] names = factory.configEntryKeys(null);
				for (int i = 0; i < names.length; i++)
				{
					String name = names[i].toString();
					AdapterProperties properties = (AdapterProperties)factory.configEntryMap(name, null);

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"DARKBACK\" COLSPAN=\"5\"><img src=\"includes/spacer.gif\" width=\"100%\" height=\"1px\"></TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD ALIGN=\"left\" COLSPAN=\"5\"><B>");
      out.print(name);
      out.write("</B></TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD ALIGN=\"center\">");
      out.print("["+(String)properties.get("Application")+"]");
      out.write("</TD>\r\n");
      out.write("                                    <TD ALIGN=\"center\">");
      out.print("["+(String)properties.get("Location")+"]");
      out.write("</TD>\r\n");
      out.write("                                    <TD ALIGN=\"center\">");
      out.print("["+(String)properties.get("CommunicationTransport")+"]");
      out.write("</TD>\r\n");
      out.write("                                    <TD ALIGN=\"center\">");
      out.print("["+(String)properties.get("MessageRoute")+"]");
      out.write("</TD>\r\n");
      out.write("                                    <TD ALIGN=\"center\">");
      out.print("["+(String)properties.get("CommSfx")+"]");
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
				
					if (properties.getCachedQueryTransaction() != null)
					{
						MessageQueryCache cache = properties.getCachedQueryTransaction();
						int cacheCnt = cache.getCacheItemCnt();

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD colspan=\"5\">\r\n");
      out.write("                                       <input class=\"btnNoSize\" value=\"Data query cache\" type=\"button\" NAME=\"function\" \r\n");
      out.write("                                              onClick=\"document.CfwCfgAdapterForm.adapterDisplayQueryCache.value='");
      out.print(name);
      out.write("'; document.CfwCfgAdapterForm.submit();\"/>&nbsp;\r\n");
      out.write("                                       Cached queries: ");
      out.print( cacheCnt );
      out.write("\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
                        
					}

      out.write('\r');
      out.write('\n');
				}
				

      out.write("                              </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("                                <A NAME=\"adapters\"></A>\r\n");
      out.write("              ");

				if (adapterFileList.size() > 1)
				{
              
      out.write("\r\n");
      out.write("                               <SELECT NAME=\"adapterPropFile\" SIZE=\"1\" onChange=\"document.CfwCfgAdapterForm.submit()\">\r\n");
      out.write("              ");

						for (int i = 0; i < adapterFileList.size(); i++)
						{
							String filename = (String)adapterFileList.get(i);
              
      out.write("\r\n");
      out.write("                                 <OPTION VALUE=\"");
      out.print(filename);
      out.write('"');
      out.write(' ');
      out.write(' ');
      out.print(ServletUtil.setSelected(adapterFileName,filename) );
      out.write('>');
      out.print(filename);
      out.write("</OPTION>\r\n");
      out.write("              ");

						}
              
      out.write("\r\n");
      out.write("                               </SELECT>\r\n");
      out.write("              ");

				}
              
      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            \r\n");

		if (adapterFilePath != null || ronlyAdapter)
		{
			if (adapterFilePath != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD><B>Path:</B> <input size=\"80\" class=\"DISPLAY-ONLY-INPUT\" readonly=\"readonly\" value=\"");
      out.print(adapterFilePath);
      out.write("\" title=\"");
      out.print(adapterFilePath);
      out.write("\"></TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            \r\n");

			}
			
			if (errorMessageAdapter != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                ");
      out.print(errorMessageAdapter );
      out.write("<BR />\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
		
			}

      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"AdapterTextArea\">\r\n");

			if (errorMessageAdapter != null)
			{

      out.print(XMLDocument.encodeXml(updateTextBuffer, false));

			}
			else
			if (ronlyAdapter)
			{

      out.print(XMLDocument.encodeXml(ronlyBuffAdapter, false));

			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(adapterFilePath, writer);

      out.print(XMLDocument.encodeXml(sw.toString(), false));

			}

      out.write("</TEXTAREA>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
			if (bIE) {   
      out.write("\r\n");
      out.write("                             <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("                                <TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"javascript:findTextAreaString(document.CfwCfgAdapterForm, document.CfwCfgAdapterForm.AdapterTextArea);\">Search:</A>\r\n");
      out.write("                                      &nbsp;<input type=\"text\" name=\"searchTextInput\" />\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
			} /* if (bIE) */   
      out.write("\r\n");
      out.write(" \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Reset\" type=\"reset\">\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Reset Adapter Control Cache\" type=\"submit\" name=\"function\">\r\n");

			if (!ronlyAdapter)
			{

      out.write("\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Update Adapter Control File\" type=\"submit\" NAME=\"function\">\r\n");

			}

      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

		}

      out.write("\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

			// End Adapters Control Section
			}
			else /**if (adapterDisplayQueryCache == null)*/
			{
			// Begin Adapter query cache Section
				String name = adapterDisplayQueryCache;
				factory = AdapterFactory.current();
				AdapterProperties properties = (AdapterProperties)factory.configEntryMap(name, null);
				MessageQueryCache cache = properties.getCachedQueryTransaction();
				int cacheCnt = cache.getCacheItemCnt();

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfwCfgAdapterForm\" ACTION=\"CommFwCtrl.jsp?cfs=adapters\">\r\n");
      out.write("                    <input name=\"adapterDisplayQueryCache\" type=\"hidden\" value=\"");
      out.print(adapterDisplayQueryCache );
      out.write("\"/>&nbsp;\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Adapter Data Query Cache</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\">\r\n");
      out.write("                                <B>Adapter: </B><I>");
      out.print(name );
      out.write("</I><BR />\r\n");
      out.write("                                <TABLE class=\"PAGEHLIGHT\" BORDER=\"0\" CELLPADDING=\"0\" CELLSPACING=\"0\" WIDTH=\"100%\">\r\n");
      out.write("                                 <TR>\r\n");
      out.write("                                   <TD class=\"DARKBACK\" COLSPAN=\"2\"><img src=\"includes/spacer.gif\" width=\"100%\" height=\"1px\"></TD>\r\n");
      out.write("                                 </TR>\r\n");
      out.write("                                 <TR>\r\n");
      out.write("                                   <TD ALIGN=\"left\" ><B>Cache Size</B></TD>\r\n");
      out.write("                                   <TD ALIGN=\"left\" >");
      out.print(cache.getCacheSize());
      out.write("</TD>\r\n");
      out.write("                                 </TR>\r\n");
      out.write("                                 <TR>\r\n");
      out.write("                                   <TD ALIGN=\"left\" ><B>Cache Count</B></TD>\r\n");
      out.write("                                   <TD ALIGN=\"left\" >");
      out.print(cacheCnt);
      out.write("</TD>\r\n");
      out.write("                                 </TR>\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\">\r\n");
      out.write("                                <TABLE class=\"PAGEHLIGHT\" BORDER=\"0\" CELLPADDING=\"0\" CELLSPACING=\"0\" WIDTH=\"100%\">\r\n");

				String colorClass = null;
				Object[] items = cache.cacheList();
				for (int i = 0; i < items.length; i++)
				{
					String item = items[i].toString();

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"DARKBACK\" COLSPAN=\"5\"><img src=\"includes/spacer.gif\" width=\"100%\" height=\"1px\"></TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD ALIGN=\"left\" COLSPAN=\"5\">");
      out.print(item);
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
				}
				

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"DARKBACK\" COLSPAN=\"5\"><img src=\"includes/spacer.gif\" width=\"100%\" height=\"1px\"></TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                              </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            \r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Reset data query cache\" type=\"submit\" name=\"function\">\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Return to Adapters\" type=\"submit\" NAME=\"function\"\r\n");
      out.write("                                       onClick=\"document.CfwCfgAdapterForm.adapterDisplayQueryCache.value=null; document.CfwCfgAdapterForm.submit();\"/>&nbsp;\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

			// End Adapters Dispay Query Cache  Section
			}
		}

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

		if ("transports".equals(cfs))
		{
			// Begin Transports Control Section

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfwCfgTransportForm\" ACTION=\"CommFwCtrl.jsp?cfs=transports\">\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Transport Control</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
      out.write("                            <!-- Start Display List of Transports -->\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>Current Transports:</B><BR>\r\n");
      out.write("                                <TABLE class=\"PAGEHLIGHT\" BORDER=\"0\" CELLPADDING=\"0\" CELLSPACING=\"0\" WIDTH=\"100%\">\r\n");

				String colorClass = null;
				factory = TransportFactory.current();
				Object[] configList = factory.configEntryKeys(null);
				List list = new ArrayList();
				for (int i = 0; i < configList.length; i++)
				{
					list.add(configList[i].toString());
				}
				for (int i = 0; i < list.size(); i++)
				{
					String name = list.get(i).toString();

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"DARKBACK\" WIDTH=\"100%\"><img src=\"includes/spacer.gif\" width=\"100%\" height=\"1px\"></TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD ALIGN=\"left\" WIDTH=\"100%\"><B>");
      out.print(name);
      out.write("</B></TD>\r\n");
      out.write("                                  </TR>\r\n");
				}

      out.write("                              </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <!--  End Display List of Transports -->\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("              ");

				if (transportFileList.size() > 1)
				{
              
      out.write("\r\n");
      out.write("                               <SELECT NAME=\"transportPropFile\" SIZE=\"1\" onChange=\"document.CfwCfgTransportForm.submit()\">\r\n");
      out.write("              ");

						for (int i = 0; i < transportFileList.size(); i++)
						{
							String filename = (String)transportFileList.get(i);
              
      out.write("\r\n");
      out.write("                                 <OPTION VALUE=\"");
      out.print(filename);
      out.write('"');
      out.write(' ');
      out.write(' ');
      out.print(ServletUtil.setSelected(transportFileName,filename) );
      out.write('>');
      out.print(filename);
      out.write("</OPTION>\r\n");
      out.write("              ");

						}
              
      out.write("\r\n");
      out.write("                               </SELECT>\r\n");
      out.write("              ");

				}
              
      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");

		if (transportFilePath != null || ronlyTransport)
		{
			if (transportFilePath != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD><B>Path:</B> <input size=\"80\" class=\"DISPLAY-ONLY-INPUT\"  readonly=\"readonly\"  value=\"");
      out.print(transportFilePath);
      out.write("\" title=\"");
      out.print(transportFilePath);
      out.write("\"></TD>\r\n");
      out.write("                            </TR>\r\n");

			}
			
			if (errorMessageTransport != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                ");
      out.print(errorMessageTransport );
      out.write("<BR />\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
		
			}

      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"TransportTextArea\">\r\n");

			if (errorMessageTransport != null)
			{

      out.print(XMLDocument.encodeXml(updateTextBuffer, false));

			}
			else
			if (ronlyTransport)
			{

      out.print(XMLDocument.encodeXml(ronlyBuffTransport, false));

			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(transportFilePath, writer);

      out.print(XMLDocument.encodeXml(sw.toString(), false));

			}

      out.write("</TEXTAREA>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
			if (bIE) {   
      out.write("\r\n");
      out.write("                             <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("                                <TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"javascript:findTextAreaString(document.CfwCfgTransportForm, document.CfwCfgTransportForm.TransportTextArea);\">Search:</A>\r\n");
      out.write("                                      &nbsp;<input type=\"text\" name=\"searchTextInput\" />\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
			} /* if (bIE) */   
      out.write("\r\n");
      out.write(" \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Reset\" type=\"reset\">\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Reset Transport Control Cache\" type=\"submit\" name=\"function\">\r\n");

			if (!ronlyTransport)
			{

      out.write("\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Update Transport Control File\" type=\"submit\" name=\"function\">\r\n");

			}

      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

		}

      out.write("\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

			// End Transports Control Section
		}

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

		if ("transactions".equals(cfs))
		{
			// Begin Transactions Control Section

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfwCfgTranForm\" ACTION=\"CommFwCtrl.jsp?cfs=transactions\">\r\n");
      out.write("                    <A NAME=\"transactions\"></A>\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Transaction Control</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("                                <TABLE width=\"100%\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("              ");

				if (tranFileList != null && tranFileList.size() > 1)
				{
              
      out.write("\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                     <SELECT NAME=\"tranPropFile\" SIZE=\"1\" onChange=\"document.CfwCfgTranForm.submit()\">\r\n");
      out.write("              ");

						for (int i = 0; i < tranFileList.size(); i++)
						{
							String filename = (String)tranFileList.get(i);
              
      out.write("\r\n");
      out.write("                                       <OPTION VALUE=\"");
      out.print(filename);
      out.write('"');
      out.write(' ');
      out.write(' ');
      out.print(ServletUtil.setSelected(tranFileName,filename) );
      out.write('>');
      out.print(filename);
      out.write("</OPTION>\r\n");
      out.write("              ");

						}
              
      out.write("\r\n");
      out.write("                                     </SELECT>\r\n");
      out.write("                                    </TD>\r\n");
      out.write("              ");

				}
              
      out.write("\r\n");
      out.write("                            \r\n");
      out.write("                                    <TD align=\"right\">\r\n");
      out.write("                                      <TABLE>\r\n");
      out.write("                                        <TR>\r\n");
      out.write("                                          <TD>\r\n");
      out.write("                                            <A name='newProcessAnchor' href=\"javascript:createNewProcessFile(document.CfwCfgTranForm);\">New Process:</A>\r\n");
      out.write("                                            <input type='hidden' name='creProcessName' />\r\n");
      out.write("                                          </TD>\r\n");
      out.write("                                        </TR>\r\n");
      out.write("                                        <TR>\r\n");
      out.write("                                          <TD >Process<input name='transProcName' />.xml</TD>\r\n");
      out.write("                                        </TR>\r\n");
      out.write("                                      </TABLE>\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

		if (tranFilePath != null || ronlyTran)
		{
			if (tranFilePath != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD><B>Path:</B> <input size=\"80\" class=\"DISPLAY-ONLY-INPUT\" readonly=\"readonly\" value=\"");
      out.print(tranFilePath);
      out.write("\"title=\"");
      out.print(tranFilePath);
      out.write("\"></TD>\r\n");
      out.write("                            </TR>\r\n");

			}
			
			if (errorMessageTran != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                ");
      out.print(errorMessageTran );
      out.write("<BR />\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
		
			}

      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"TransactionTextArea\">\r\n");

			if (errorMessageTran != null)
			{

      out.print(XMLDocument.encodeXml(updateTextBuffer, false));

			}
			else
			if (ronlyTran)
			{

      out.print(XMLDocument.encodeXml(ronlyBuffTran, false));

			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(tranFilePath, writer);

      out.print(XMLDocument.encodeXml(sw.toString(), false));

			}

      out.write("</TEXTAREA>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write(" \r\n");
			if (bIE) {   
      out.write("\r\n");
      out.write("                             <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("                                <TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"javascript:findTextAreaString(document.CfwCfgTranForm, document.CfwCfgTranForm.TransactionTextArea);\">Search:</A>\r\n");
      out.write("                                      &nbsp;<input type=\"text\" name=\"searchTextInput\" />\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
			} /* if (bIE) */   
      out.write("\r\n");
      out.write(" \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Reset\" type=\"reset\">\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Reset Transaction Control Cache\" type=\"submit\" name=\"function\">\r\n");

			if (!ronlyTran)
			{

      out.write("\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Update Transaction Control File\" type=\"submit\" name=\"function\">\r\n");

			}

      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

		}

      out.write("\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

			// End Transactions Control Section
		}

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

		if ("records".equals(cfs))
		{
			// Begin Record Control Section

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfwCfgRecordForm\" ACTION=\"CommFwCtrl.jsp?cfs=records\">\r\n");
      out.write("                    <A NAME=\"records\"></A>\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Record Control</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
      out.write("                            <!--  Start Display List of Record Managers -->\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>Current Record Manager List:</B><BR>\r\n");
      out.write("                                <TABLE class=\"PAGEHLIGHT\" BORDER=\"0\" CELLPADDING=\"0\" CELLSPACING=\"0\" WIDTH=\"100%\">\r\n");

				String colorClass = null;
				factory = RecordManagerFactory.current();
				Object[] configList = factory.configEntryKeys(null);
				List list = new ArrayList();
				for (int i = 0; i < configList.length; i++)
				{
					list.add(configList[i].toString());
				}
				for (int i = 0; i < list.size(); i++)
				{
					String name = list.get(i).toString();

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"DARKBACK\" WIDTH=\"100%\"><img src=\"includes/spacer.gif\" width=\"100%\" height=\"1px\"/></TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD ALIGN=\"left\" WIDTH=\"100%\"><B>");
      out.print(name);
      out.write("</B></TD>\r\n");
      out.write("                                  </TR>\r\n");
				}

      out.write("                              </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <!--  End Display List of Record Managers -->\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("              ");

				if (recordCtrlFileList.size() > 1)
				{
              
      out.write("\r\n");
      out.write("                               <SELECT NAME=\"recordCtrlPropFile\" SIZE=\"1\" onChange=\"document.CfwCfgRecordForm.submit()\">\r\n");
      out.write("              ");

						for (int i = 0; i < recordCtrlFileList.size(); i++)
						{
							String filename = (String)recordCtrlFileList.get(i);
              
      out.write("\r\n");
      out.write("                                 <OPTION VALUE=\"");
      out.print(filename);
      out.write('"');
      out.write(' ');
      out.write(' ');
      out.print(ServletUtil.setSelected(recordCtrlFileName,filename) );
      out.write('>');
      out.print(filename);
      out.write("</OPTION>\r\n");
      out.write("              ");

						}
              
      out.write("\r\n");
      out.write("                               </SELECT>\r\n");
      out.write("              ");

				}
              
      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");

		if (recordCtrlFilePath != null || ronlyRecordCtrl)
		{
			if (recordCtrlFilePath != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD><B>Path:</B> <input size=\"80\" class=\"DISPLAY-ONLY-INPUT\" readonly=\"readonly\" value=\"");
      out.print(recordCtrlFilePath);
      out.write("\" title=\"");
      out.print(recordCtrlFilePath);
      out.write("\"></TD>\r\n");
      out.write("                            </TR>\r\n");

			}
			
			if (errorMessageRecordCtrl != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                ");
      out.print(errorMessageRecordCtrl );
      out.write("<BR />\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
		
			}

      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"RecordTextArea\">\r\n");

			if (errorMessageRecordCtrl != null)
			{

      out.print(XMLDocument.encodeXml(updateTextBuffer, false));

			}
			else
			if (ronlyRecordCtrl)
			{

      out.print(XMLDocument.encodeXml(ronlyBuffRecordCtrl, false));

			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(recordCtrlFilePath, writer);

      out.print(XMLDocument.encodeXml(sw.toString(), false));

			}

      out.write("</TEXTAREA>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
			if (bIE) {   
      out.write("\r\n");
      out.write("                             <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("                                <TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"javascript:findTextAreaString(document.CfwCfgRecordForm, document.CfwCfgRecordForm.RecordTextArea);\">Search:</A>\r\n");
      out.write("                                      &nbsp;<input type=\"text\" name=\"searchTextInput\" />\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
			} /* if (bIE) */   
      out.write("\r\n");
      out.write(" \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Reset\" type=\"reset\">\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Reset Record Control Cache\" type=\"submit\" name=\"function\">\r\n");

			if (!ronlyRecordCtrl)
			{

      out.write("\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Update Record Control File\" type=\"submit\" name=\"function\">\r\n");

			}

      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

		}

      out.write("\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

			// End Record Control Section
		}

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

		if ("messageRoutes".equals(cfs))
		{
			// Begin Message Route Section

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfwCfgMessageRouteForm\" ACTION=\"CommFwCtrl.jsp?cfs=messageRoutes\">\r\n");
      out.write("                    <A NAME=\"messageRoutes\"></A>\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Message Route</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
      out.write("                            <!--  Start Display List of Message Routes -->\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>Current Message Route List:</B><BR>\r\n");
      out.write("                                <TABLE class=\"PAGEHLIGHT\" BORDER=\"0\" CELLPADDING=\"0\" CELLSPACING=\"0\" WIDTH=\"100%\">\r\n");

				String colorClass = null;
				factory = MessageRouteFactory.current();
				Object[] configList = factory.configEntryKeys(null);
				List list = new ArrayList();
				for (int i = 0; i < configList.length; i++)
				{
					list.add(configList[i].toString());
				}
				for (int i = 0; i < list.size(); i++)
				{
					String name = list.get(i).toString();

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"DARKBACK\" WIDTH=\"100%\"><img src=\"includes/spacer.gif\" width=\"100%\" height=\"1px\"></TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD ALIGN=\"left\" WIDTH=\"100%\"><B>");
      out.print(name);
      out.write("</B></TD>\r\n");
      out.write("                                  </TR>\r\n");
				}

      out.write("                              </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <!--  End Display List of Message Routes -->\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("              ");

				if (messageRouteFileList.size() > 1)
				{
              
      out.write("\r\n");
      out.write("                               <SELECT NAME=\"messageRoutePropFile\" SIZE=\"1\" onChange=\"document.CfwCfgMessageRouteForm.submit()\">\r\n");
      out.write("              ");

						for (int i = 0; i < messageRouteFileList.size(); i++)
						{
							String filename = (String)messageRouteFileList.get(i);
              
      out.write("\r\n");
      out.write("                                 <OPTION VALUE=\"");
      out.print(filename);
      out.write('"');
      out.write(' ');
      out.write(' ');
      out.print(ServletUtil.setSelected(messageRouteFileName,filename) );
      out.write('>');
      out.print(filename);
      out.write("</OPTION>\r\n");
      out.write("              ");

						}
              
      out.write("\r\n");
      out.write("                               </SELECT>\r\n");
      out.write("              ");

				}
              
      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");

		if (messageRouteFilePath != null || ronlyMessageRoute)
		{
			if (messageRouteFilePath != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD><B>Path:</B> <input size=\"80\" class=\"DISPLAY-ONLY-INPUT\" readonly=\"readonly\" value=\"");
      out.print(messageRouteFilePath);
      out.write("\" title=\"");
      out.print(messageRouteFilePath);
      out.write("\"></TD>\r\n");
      out.write("                            </TR>\r\n");

			}
			
			if (errorMessageMessageRoute != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                ");
      out.print(errorMessageMessageRoute );
      out.write("<BR />\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
		
			}

      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"MessageRouteTextArea\">\r\n");

			if (errorMessageMessageRoute != null)
			{

      out.print(XMLDocument.encodeXml(updateTextBuffer, false));

			}
			else
			if (ronlyMessageRoute)
			{

      out.print(XMLDocument.encodeXml(ronlyBuffMessageRoute, false));

			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(messageRouteFilePath, writer);

      out.print(XMLDocument.encodeXml(sw.toString(), false));

			}

      out.write("</TEXTAREA>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
			if (bIE) {   
      out.write("\r\n");
      out.write("                             <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("                                <TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"javascript:findTextAreaString(document.CfwCfgMessageRouteForm, document.CfwCfgMessageRouteForm.MessageRouteTextArea);\">Search:</A>\r\n");
      out.write("                                      &nbsp;<input type=\"text\" name=\"searchTextInput\" />\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
			} /* if (bIE) */   
      out.write("\r\n");
      out.write(" \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Reset\" type=\"reset\">\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Reset Message Route Control Cache\" type=\"submit\" name=\"function\">\r\n");

			if (!ronlyRecordCtrl)
			{

      out.write("\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Update Message Route Control File\" type=\"submit\" name=\"function\">\r\n");

			}

      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

		}

      out.write("\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

			// End Message Route Section
		}

      out.write("\r\n");
      out.write("\r\n");

		if ("webSvcs".equals(cfs))
		{

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfwCfgWebSvcForm\" ACTION=\"CommFwCtrl.jsp?cfs=webSvcs\">\r\n");
      out.write("                    <A NAME=\"webSvcs\"></A>\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Web Services Control</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

		if (webSvcFilePath != null || ronlyWebSvc)
		{
			if (webSvcFilePath != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD><B>Path:</B> <input size=\"80\" class=\"DISPLAY-ONLY-INPUT\" readonly=\"readonly\" value=\"");
      out.print(webSvcFilePath);
      out.write("\" title=\"");
      out.print(webSvcFilePath);
      out.write("\"></TD>\r\n");
      out.write("                            </TR>\r\n");

			}
			
			if (errorMessageWebSvc != null)
			{

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                ");
      out.print(errorMessageWebSvc );
      out.write("<BR />\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
		
			}

      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"WebSvcTextArea\">\r\n");

			if (errorMessageWebSvc != null)
			{

      out.print(XMLDocument.encodeXml(updateTextBuffer, false));

			}
			else
			if (ronlyWebSvc)
			{

      out.print(XMLDocument.encodeXml(ronlyBuffWebSvc, false));

			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(webSvcFilePath, writer);

      out.print(XMLDocument.encodeXml(sw.toString(), false));

			}

      out.write("</TEXTAREA>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Reset\" type=\"reset\">\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Reset Web Services Control Cache\" type=\"submit\" name=\"function\">\r\n");

			if (!ronlyWebSvc)
			{

      out.write("\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;<input class=\"btnNoSize\" value=\"Update Web Services Control File\" type=\"submit\" name=\"function\">\r\n");

			}

      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

		}

      out.write("\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

		}

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                    <TABLE cellspacing=\"0\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\">\r\n");
      out.write("                          <TABLE cellspacing=\"0\" WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"left\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=adapters\">Adapter Control</A></div></TD>\r\n");
      out.write("                              <TD ALIGN=\"center\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=transports\">Transport Control</A></div></TD>\r\n");
      out.write("                              <TD ALIGN=\"right\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=transactions\">Transaction Control</A></div></TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"left\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=records\">Record Control</A></div></TD>\r\n");
      out.write("                              <TD ALIGN=\"center\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=messageRoutes\">Message Route Control</A></div></TD>\r\n");
      out.write("                              <TD ALIGN=\"right\"><div align=\"center\"><A href=\"CommFwCtrl.jsp?cfs=webSvcs\">Web Services Control</A></div></TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD ALIGN=\"left\"><A href=\"CommFwSys.jsp\">System Configuration</A></TD>\r\n");
      out.write("                        <TD ALIGN=\"center\"><A href=\"CommFwCtrl.jsp\">System Control</A></TD>\r\n");
      out.write("                        <TD ALIGN=\"center\"><A href=\"CommFwCfg.jsp\">Application Configuration</A></TD>\r\n");
      out.write("                        <TD ALIGN=\"right\"><A href=\"CommFwStatus.jsp\">System Status</A></TD>\t\t\t\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");
      out.write("\r\n");
      out.write("              </TABLE>\r\n");
      out.write("             </TD>\r\n");
      out.write("            </TR>\r\n");
      out.write("          </TABLE>\r\n");
      out.write("        </TD>\r\n");
      out.write("      </TR>\r\n");
      out.write("    </TABLE>\r\n");
      out.write("\r\n");
      out.write("    </DIV>\r\n");
      out.write("  </BODY>\r\n");
      out.write("</HTML>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
