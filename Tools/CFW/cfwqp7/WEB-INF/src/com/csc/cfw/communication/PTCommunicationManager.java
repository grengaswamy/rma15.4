package com.csc.cfw.communication;

/*****************************************************************************************************
 * $Id: PTCommunicationsManager.java,v 1.0 2000/11/01 rwdorsey Exp $
 *
 * Copyright (c) 2001 Computer Sciences Corporation, Inc. All Rights Reserved.
 *
 * $State: Exp $
 *****************************************************************************************************/
import java.util.List;

import com.csc.cfw.bsf.records.RecordWrapper;
import com.csc.cfw.process.CommFwAdapter;
import com.csc.cfw.process.Envelope;
import com.csc.cfw.process.data.PropertyValue;
import com.csc.cfw.recordbuilder.RecordManager;
import com.csc.cfw.xml.domain.CobolRecordFactory;
import com.csc.cfw.xml.domain.JavaRecord;
import com.csc.fw.util.FwException;
import com.csc.fw.util.FwListener;
import com.csc.fw.util.FwTimer;
import com.csc.fw.util.XMLDocument;
/**
 *	The responsibility of this class is to manage the interface to the POINT Driver.
 *	This class formats the data area and calls the appropriate transport handler to
 *	communicate to the POINT Driver and return the data results.
 */

public class PTCommunicationManager extends CommunicationManager
	implements CommFwCommunicationManager
{
	private static java.util.ResourceBundle resCommFwResourceBundle = java.util.ResourceBundle.getBundle("com.csc.cfw.process.CommFwResourceBundle");  //$NON-NLS-1$
	public final static String ErrorRecordBusObjName = "ERROR";//$NON-NLS-1$

	// Properties used by this class
	public final static String TRANSPORT_AS400DataLibrary = "AS400DataLibrary"; //$NON-NLS-1$
	public final static String TRANSPORT_ChunkSize = "ChunkSize"; //$NON-NLS-1$
	public final static String TRANSPORT_ForceErrorRec = "ForceErrorRec"; //$NON-NLS-1$

	// Host Timer definitions
	public final static String HOST_TIMER_SWITCH_REC_FIELD = "COM__ELAPSED__TIME__SW"; //$NON-NLS-1$
	public final static String HOST_TIMER_RETURN_REC_FIELD = "COM__ELAPSED__TIME__AMT"; //$NON-NLS-1$
	// Define the timer scale in milliseconds.  If negative, then the time
	// must be multiplied by that number to get milliseconds.  Otherwise,
	// the time must be devided by the scale to determine the milliseconds.
	public final static long   HOST_TIMER_SCALE_MILLIS = -10;

	private JavaRecord commAreaRecord = null;
	private JavaRecord errorRecord = null;;
	private boolean fForceErrorRec = true;

	private final static String pointCommunicationAreaRecordName = "com.csc.cfw.pt.records.PointCommunicationAreaRecord"; //$NON-NLS-1$
	private final static String pointHeaderRecordName = "com.csc.cfw.pt.records.PointHeaderRecord"; //$NON-NLS-1$
	private final static String pointErrorRecordName = "com.csc.cfw.pt.records.PointErrorRecord"; //$NON-NLS-1$

	/**
	 * PTCommunicationManager constructor.
	 */
	public PTCommunicationManager()
	{
		super();
	}

	/**
	 * Execute the Business Service Driver via the transport handler
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param outputs Array of output parameters.
	 * @return int A response code which consists of one of the following list:
	 *		<p>(MD_OK, MD_ERROR, MD_OK_MORE, MD_ERROR_MORE, MD_OK_DONE)</p>
	 */
	public int processRequest(Envelope envelope, CommFwAdapter adapter, Object[] outputs)
		throws FwException
	{
		int responseRet = MD_OK;
		boolean fProcess = true;
		long commandBeginTime = 0;
		boolean isTrace = (envelope.trace > 0);

		Process_Loop: while (fProcess)
		{
			// CFw Trace
			if (isTrace)
			{
				envelope.getListener().println(resCommFwResourceBundle.getString("cfw_tracemsg_beg_cmd") + hostServiceFunctionMapped); //$NON-NLS-1$
				commandBeginTime = System.currentTimeMillis();
			}

			// Execute the MainDriver using the transport handler.
			transport.processCommunication(envelope, adapter);

			// If the transport is not required to return data,
			// then the absence of an exception will be a successful
			// execution.
			if (transport.isNoCommunicationResponse())
			{
				if (isTrace)
				{
					FwListener listener = envelope.getListener(); 
					listener.println(resCommFwResourceBundle.getString("cfw_tracemsg_end_cmd") + hostServiceFunctionMapped); //$NON-NLS-1$
					listener.println(resCommFwResourceBundle.getString("cfw_tracemsg_cmd_time") + FwTimer.displayInterval(System.currentTimeMillis() - commandBeginTime, 1)); //$NON-NLS-1$
				}
				
				responseRet = processResponse((Object[])null, envelope, adapter, outputs);
				fProcess = false;
				continue;
			}

			// CFw Trace
			if (isTrace)
			{
				FwListener listener = envelope.getListener(); 
				listener.print(resCommFwResourceBundle.getString("cfw_tracemsg_end_cmd"));//$NON-NLS-1$
				  listener.println(hostServiceFunctionMapped);
				listener.println(resCommFwResourceBundle.getString("cfw_tracemsg_cmd_time"));//$NON-NLS-1$
				  listener.println(FwTimer.displayInterval(System.currentTimeMillis() - commandBeginTime, 1)); //$NON-NLS-1$
			}

			//Object[] inputRecords = transport.getInputRecords();
			Object[] outputRecords = transport.getOutputRecords();
			commAreaRecord.setRawBytes((byte[])outputRecords[0]);
			errorRecord.setRawBytes((byte[])outputRecords[2]);
			
			// CFw Trace
			if (isTrace)
			{
				FwListener listener = envelope.getListener(); 
				if (hostTimerActivated)
				{
					String sHostTime = null;
					
					try
					{
						sHostTime = commAreaRecord.getString(HOST_TIMER_RETURN_REC_FIELD, 0).trim();
					}
					catch (Exception e)
					{
						// Turn off timer if an error
						hostTimerActivated = false;
					}
					
					if (sHostTime != null)
					{
						listener.print("    ");//$NON-NLS-1$
						  listener.print(HOST_TIMER_RETURN_REC_FIELD);
						  listener.print("...");//$NON-NLS-1$
						  listener.println(sHostTime); //$NON-NLS-1$
					}
				}
			}

			responseRet = processResponse(outputRecords, envelope, adapter, outputs);
			if (responseRet == MD_OK_MORE || responseRet == MD_ERROR_MORE)
			{
				transport.setInputRecords(new Object[] {(byte[])outputRecords[0], (byte[])outputRecords[1], (byte[])outputRecords[2]});
			}
			else
				fProcess = false;
		}
			
		return responseRet;
	}

	/**
	 * Check the Main Driver return data and create an error record from the return data.  Return
	 * false if the commit-rollback indicator is not set to commit.
	 *
	 * @param outputRecords An array of output records
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param outputs Array of output parameters.
	 * @return int A response code which consists of one of the following list:
	 *		<p>(MD_OK, MD_ERROR, MD_OK_MORE, MD_ERROR_MORE, MD_OK_DONE)</p>
	 */
	public int processResponse(Object[] outputRecords, Envelope envelope, CommFwAdapter adapter, Object[] outputs)
		throws FwException
	{
		int ret = MD_OK;
		JavaRecord hdrRecord;
		RecordManager recman = adapter.getRecMan();

		if (outputRecords == null)
		{
			// If no data is returned then an empty error record will be
			// written to signify no error.
			hdrRecord = CobolRecordFactory.getCurrent().createRecord(pointHeaderRecordName, recman);
			hdrRecord.setString("RECORD__NAME", 0, "ERROR"); //$NON-NLS-1$//$NON-NLS-2$
			addRecord(hdrRecord);	
			addRecord(errorRecord);
			ret = MD_OK_DONE;
			fComplete = true;
		}
		else
		{
			if (hostTimerActivated)
			{
				try
				{
					String s = commAreaRecord.getString(HOST_TIMER_RETURN_REC_FIELD, 0).trim();
					if (s.length() > 0)
						hostTime += Long.parseLong(s);
				}
				catch (Exception e)
				{
					// Turn off timer if an error
					hostTimerActivated = false;
				}
			}
			
			// Valid data is returned.
			if (errorRecord.getString("ERROR__CODE", 0 /*ERROR__LST(0)*/).trim().length() > 0)//$NON-NLS-1$
			{
	//			ret = MD_ERROR;
				fError = true;
				fComplete = true;
			}
			//FSIT Issue 68866 Resolution 41003 Start
			else
			{
				fError = false;
			}
			//FSIT Issue 68866 Resolution 41003 End
			
			/* Write the error record if an error occured */			
			if (fError)
			{
				hdrRecord = CobolRecordFactory.getCurrent().createRecord(pointHeaderRecordName, recman);
				hdrRecord.setString("RECORD__NAME", 0, "ERROR");//$NON-NLS-2$//$NON-NLS-1$
				
				addRecord(hdrRecord);
				addRecord(errorRecord);

				//FSIT Issue 68866 Resolution 41003 Start
				fFirstPass = false;
				//FSIT Issue 68866 Resolution 41003 End
			} 
			else 
			{
				/* If Error Record is required and processing the last chunk and this is the first response */
				if ((fForceErrorRec) && ((fLastChunk) && (fFirstPass)))
				{
					hdrRecord = CobolRecordFactory.getCurrent().createRecord(pointHeaderRecordName, recman);
					hdrRecord.setString("RECORD__NAME", 0, "ERROR");//$NON-NLS-2$//$NON-NLS-1$
				
					addRecord(hdrRecord);
					addRecord(errorRecord);	
				
					fFirstPass = false;	
				}
			}
			/* Last chunk of data was sent so process return data records */
			if (fLastChunk)
	   		{
				addData((byte[])outputRecords[1], 0, commAreaRecord.getInt("COM__CHUNK__SIZE", 0));//$NON-NLS-1$
				fComplete = true;
	//			if (ret != MD_ERROR)
					if (commAreaRecord.getString("COM__LAST__CHUNK", 0).equals("Y"))//$NON-NLS-1$//$NON-NLS-2$
						if (fError)
							ret = MD_ERROR;
						else
							ret = MD_OK_DONE;
					else
						if (fError)
							ret = MD_ERROR_MORE;
						else
							ret = MD_OK_MORE;
			}
		}
		
		if (fComplete)
		{   
			outputs[0] = PropertyValue.createPropertyValue(
			                      envelope,
			                      adapter,
			                      null, 
			                      PropertyValue.DTYPE_BYTEARRAY, 
			                      abDriverArray, 
			                      iArrayOffset);
			hostResponseTotalSize = iArrayOffset;
		}

		return ret;
	}

	/**
	 * Format the call to the Point Command Driver.
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param inputs Array of input parameters.
	 * @param outputs Array of output parameters.
	 */
	public void sendMessage(Envelope envelope, CommFwAdapter adapter, Object[] inputs, Object[] outputs)
		throws FwException
	{
		RecordManager recman = adapter.getRecMan();
		hostServiceFunctionMapped = adapter.getHostServiceFunction(envelope);
		byte[] dataArray = null;
		
		// Values for returning host performance data.
		hostTimerActivated = envelope.isReturnPerfHeader() 
									 || envelope.isReturnPerfRpt()
									 || (envelope.getFwSystem().getTimerLevel() > 1);
		
		Object inVal = null;
		if ((inVal = inputs[0]) != null)
		{
			if (inVal instanceof byte[])
			{
				dataArray = (byte[])inVal;
			}
			else
			if (inVal instanceof PropertyValue)
			{
				PropertyValue prop = (PropertyValue)inVal;
				switch (prop.getDType())
				{
					case PropertyValue.DTYPE_BYTEARRAY:
						dataArray = (byte[])prop.getValue();
						break;
						
					case PropertyValue.DTYPE_STRING:
						try
						{
							dataArray = ((String)prop.getValue()).getBytes(Envelope.INTERNAL_ENCODING);
						}
						catch (Exception e)
						{
							throw new FwException(e);
						}
						break;
					
				}
			}
			else
			if (inVal instanceof String)
			{
				try
				{
					dataArray = ((String)inVal).getBytes(Envelope.INTERNAL_ENCODING);
				}
				catch (Exception e)
				{
					throw new FwException(e);
				}
			}
			else
			if (inVal instanceof JavaRecord)
			{
				dataArray = ((JavaRecord)inVal).getRawBytes();
			}
		}

		if (dataArray == null)
			throw new FwException(resCommFwResourceBundle.getString("cfw_err_comm_noinp")); //$NON-NLS-1$
		
		int dataLength = dataArray.length;
			
		// Set up the communication transport handler and structures.
		transport = adapter.getTransportHandler();
		
		try
		{
			if (this.commAreaRecord == null)
				this.commAreaRecord = 
						CobolRecordFactory.getCurrent().createRecord(
								pointCommunicationAreaRecordName, 
								recman);
			
			if (this.errorRecord == null)
				this.errorRecord = 
						CobolRecordFactory.getCurrent().createRecord(
								pointErrorRecordName, 
								recman);
			
			String tranName = hostServiceFunctionMapped;
			
			/*********************************************************/
			/* Set any configured communication record properties    */
			/*********************************************************/
			updateRecordFromProperties(envelope, adapter, commAreaRecord);
			
			// Create parameters for call
			String library = 
					(String)transport.getTransportProperty(
							TRANSPORT_AS400DataLibrary, 
							envelope, 
							adapter);

			// Setup first parameter - driver communication record
			commAreaRecord.setString("COM__TRANSACTION__CODE", 0, tranName); //$NON-NLS-1$
			if (envelope.getSignonID() != null)
				commAreaRecord.setString("COM__USER__ID", 0, envelope.getSignonID()); //$NON-NLS-1$
			if (library != null)
				commAreaRecord.setString("COM__LIB__NAME", 0, library); //$NON-NLS-1$

			String s = (String)transport.getProperty(TRANSPORT_ChunkSize);
			int maxChunkSize = Integer.parseInt(s);
			s = (String)transport.getProperty(TRANSPORT_ForceErrorRec);
			if (s != null)
				fForceErrorRec = Boolean.valueOf(s).booleanValue();
			else
				fForceErrorRec = true;
			// Setup second parameter - data records
			byte mainDriverData[] = new byte[maxChunkSize];

			// Setup third parameter - error records
			byte[] errorData = new byte[errorRecord.getSize()];
			System.arraycopy(errorRecord.getRawBytes(), 0, errorData, 0, errorRecord.getSize());

			int commandReturn = MD_OK;
			fError = false;	
			fComplete = false;
			fLastChunk = true;
			fFirstPass = true;
			// Set up metric counters
			hostTime = 0;
			hostRequestTotalSize = dataLength;
			hostResponseTotalSize = 0;
			

			if (adapter.getCommunicationsObject() != null)
			{
				fLastChunk = false;
				int currRecord = 0;
				List records = (List)adapter.getCommunicationsObject();
				commAreaRecord.setInt("COM__RECORD__COUNT", 0, records.size()); //$NON-NLS-1$
				String lineBusiness = XMLDocument.docGetDOMValueByXPath(envelope.getDOM(), "//LOBCd"); //$NON-NLS-1$
				if (lineBusiness == null || lineBusiness.length() == 0)
				{
					lineBusiness = "XXX"; //$NON-NLS-1$
				}
				// Set a unique member name to be used for chunking 
				commAreaRecord.setString(
						"COM__MEMBER__NAME", //$NON-NLS-1$
						0, 
						"Z" + (long) (Math.random() * 1000000000L)); //$NON-NLS-1$
				while (currRecord < records.size() && (commandReturn == MD_OK))
				{
					int chunkSize = 0;
					for (int i = currRecord; i < records.size(); i++,currRecord++)
					{
						RecordWrapper recWrapper = (RecordWrapper)records.get(i);
						JavaRecord custRec = (JavaRecord)recWrapper.getData();
						JavaRecord headRec = (JavaRecord)recWrapper.getHeader();
						//String recordName = headRec.getString("RECORD__NAME", 0).trim(); //$NON-NLS-1$
						// Moved the member name generation out of this loop to avoid
						// problems with chunking. 
						// commAreaRecord.setString("COM__MEMBER__NAME", 0, "Z" + (long) (Math.random() * 1000000000L));
						commAreaRecord.setString("COM__LOB", //$NON-NLS-1$ 
								0, 
								lineBusiness.substring(0, 
									(lineBusiness.length() > 3 ? 3 : lineBusiness.length()) ));
						if (chunkSize + custRec.getSize() + headRec.getSize() <= maxChunkSize)
						{
							System.arraycopy(headRec.getRawBytes(), 0, mainDriverData, chunkSize, headRec.getSize());
							System.arraycopy(custRec.getRawBytes(), 0, mainDriverData, chunkSize + headRec.getSize(), custRec.getSize());
							chunkSize += custRec.getSize() + headRec.getSize();
						}
						else
						{
							break;
						}
					}
						
					if (currRecord == records.size())
					{
						fLastChunk = true;
					}
					
					commAreaRecord.setInt("COM__CURRENT__RECORD", 0, currRecord); //$NON-NLS-1$
			            
					if (fFirstPass)
						commAreaRecord.setString("COM__FIRST__PASS", 0, "Y"); //$NON-NLS-1$ //$NON-NLS-2$
					else
						commAreaRecord.setString("COM__FIRST__PASS", 0, "N"); //$NON-NLS-1$ //$NON-NLS-2$
					
					if (fLastChunk)
					{
						/*fFirstPass indicates to CFW that this is the first chunk 
						 * for either the request or the response. The first
						 * response after chunking a request was returning no 
						 * error aggregate because fFirstPass was false from the
						 * request processing.
						 * When the last request chunk has been sent, reset the
						 * fFirstPass to true to tell CFW that the next processing
						 * will actually be the first pass on the response.
						 * 
						 */
						fFirstPass = true;
						commAreaRecord.setString("COM__LAST__CHUNK", 0, "Y"); //$NON-NLS-1$ //$NON-NLS-2$
					}
					else
						commAreaRecord.setString("COM__LAST__CHUNK", 0, "N"); //$NON-NLS-1$ //$NON-NLS-2$
					
					commAreaRecord.setString("COM__REQUEST__FLAG", 0, "Y"); //$NON-NLS-1$ //$NON-NLS-2$
					// Added Chunk_Size and MAX_CHUNK _SIZE  
					commAreaRecord.setInt("COM__CHUNK__SIZE", 0, chunkSize); //$NON-NLS-1$
					commAreaRecord.setInt("COM__MAX__CHUNK__SIZE", 0, maxChunkSize); //$NON-NLS-1$
					
					try
					{
						if (hostTimerActivated)
							commAreaRecord.setString(HOST_TIMER_SWITCH_REC_FIELD, 0, "Y"); //$NON-NLS-1$//$NON-NLS-2$
					}
					catch (Exception e)
					{
						// New feature not added to copybook.  Turn off host timer.
						hostTimerActivated = false;
					}
							
					transport.setInputRecords(new Object[] {commAreaRecord.getRawBytes(), mainDriverData, errorData});
					transport.setOutputRecords(new Object[transport.getInputRecords().length]);

					commandReturn = processRequest(envelope, adapter, outputs);
					fFirstPass = false;
				}
			}
			else		// communicationsObject is null, so raw data is being sent to the host.
			{
				int copySize;
				
				dataArray = convertAsciiToEbcdic(transport, dataArray, 0, dataLength);
				
				int numberOfChunks = dataLength / maxChunkSize;
				int lastChunkSize = dataLength % maxChunkSize;
				if (lastChunkSize > 0)
					numberOfChunks++;
			
				for (int i = 1; ((i <= numberOfChunks) && (commandReturn == MD_OK)); i++)
				{
					if (i == numberOfChunks)
					{
						this.fLastChunk = true;
					}
					else
						fLastChunk = false;
			    
					if (i == numberOfChunks && lastChunkSize > 0)
					    copySize = lastChunkSize;
					else
						copySize = maxChunkSize;
					
					System.arraycopy(dataArray, (i-1) * maxChunkSize, mainDriverData, 0, copySize);
				
					commAreaRecord.setString("COM__MEMBER__NAME", 0, envelope.getTranID()); //$NON-NLS-1$
					commAreaRecord.setInt("COM__RECORD__COUNT", 0, numberOfChunks); //$NON-NLS-1$
					commAreaRecord.setInt("COM__CURRENT__RECORD", 0, i); //$NON-NLS-1$
					if (fFirstPass)
						commAreaRecord.setString("COM__FIRST__PASS", 0, "Y"); //$NON-NLS-1$ //$NON-NLS-2$
					else
						commAreaRecord.setString("COM__FIRST__PASS", 0, "N"); //$NON-NLS-1$ //$NON-NLS-2$
					
					if (fLastChunk)
					{
						/*fFirstPass indicates to CFW that this is the first chunk 
						 * for either the request or the response. The first
						 * response after chunking a request was returning no 
						 * error aggregate because fFirstPass was false from the
						 * request processing.
						 * When the last request chunk has been sent, reset the
						 * fFirstPass to true to tell CFW that the next processing
						 * will actually be the first pass on the response.
						 * 
						 */
						fFirstPass = true;
						commAreaRecord.setString("COM__LAST__CHUNK", 0, "Y"); //$NON-NLS-1$ //$NON-NLS-2$
					}
					else
						commAreaRecord.setString("COM__LAST__CHUNK", 0, "N"); //$NON-NLS-1$ //$NON-NLS-2$
					commAreaRecord.setString("COM__REQUEST__FLAG", 0, "Y");//$NON-NLS-1$ //$NON-NLS-2$
					commAreaRecord.setInt("COM__CHUNK__SIZE", 0, copySize); //$NON-NLS-1$
					commAreaRecord.setInt("COM__MAX__CHUNK__SIZE", 0, maxChunkSize); //$NON-NLS-1$
					
					try
					{
						if (hostTimerActivated)
							commAreaRecord.setString(HOST_TIMER_SWITCH_REC_FIELD, 0, "Y"); //$NON-NLS-1$//$NON-NLS-2$
					}
					catch (Exception e)
					{
						// New feature not added to copybook.  Turn off host timer.
						hostTimerActivated = false;
					}
							
					transport.setInputRecords(new Object[] {commAreaRecord.getRawBytes(), mainDriverData, errorData});
					transport.setOutputRecords(new Object[transport.getInputRecords().length]);

					commandReturn = processRequest(envelope, adapter, outputs);
					fFirstPass = false;
				}
			}

			// Set transactionError in the envelope if we had an error.
			if (commandReturn == MD_ERROR)
				adapter.setTransactionError(true);

			if (hostTimerActivated)
			{
				// #hostTime is the sum of the host times in hundreths
				// of a second.  Set the event duration to the begin 
				// time + host time converted to milliseconds. 
				if (HOST_TIMER_SCALE_MILLIS < 0)
					envelope.addTimedEvent(Envelope.PTIME_HOST_TIME, hostTime * 
							(-HOST_TIMER_SCALE_MILLIS));
				else
					envelope.addTimedEvent(Envelope.PTIME_HOST_TIME, (long)(hostTime / 
							(HOST_TIMER_SCALE_MILLIS)));
			}
			
			envelope.addSizeEvent(Envelope.PTIME_HOST_REQ_SIZE, hostRequestTotalSize);
			envelope.addSizeEvent(Envelope.PTIME_HOST_RES_SIZE, hostResponseTotalSize);
			
			// If we are not completing the transaction, convert the EBCDIC byte
			// array to a readable format.
			if (transport.isConvertResponseCPToInternal())
				convertEbcdicToAscii(envelope, adapter, transport, false, outputs);
			else
			if (envelope.getStopAtStep() == CommFwAdapter.ACT_COMMUNICATE)
				convertEbcdicToAscii(envelope, adapter, transport, true, outputs);

			return;
		}
		catch (Exception exception)
		{
			throw new FwException(resCommFwResourceBundle.getString("cfw_excmsg_env_err"), exception); //$NON-NLS-1$
		}
	 	
	}

}
