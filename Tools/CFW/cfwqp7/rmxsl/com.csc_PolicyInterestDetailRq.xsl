<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:param name="app-version" select="default" />
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYYYtoCYYMMDD.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <xsl:variable name="Lob">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/LOBCd)"/>
    </xsl:variable>
    <xsl:variable name="baseLOBline">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_BaseLOBLine)"/>
    </xsl:variable>
    <xsl:variable name="UnitNumber">
      <xsl:value-of select="normalize-space(/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId)"/>
    </xsl:variable>
        
    <xsl:choose>
      <xsl:when test="$baseLOBline = 'WL' and string-length($UnitNumber) > 0">
        <xsl:call-template name="WCVRq"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="ALLRq"/>
      </xsl:otherwise>
    </xsl:choose>
    <!--<xsl:if test="$baseLOBline = 'WL'">
      <xsl:call-template name="WCVRq"/>
    </xsl:if>
    <xsl:if test="$baseLOBline = 'CL' or  $baseLOBline = 'AL' or  $baseLOBline = 'PL' or $baseLOBline = 'CL'">
      <xsl:call-template name="ALLRq"/>
    </xsl:if>-->

  </xsl:template>
    <xsl:template name= "WCVRq">
      <WCVSTINTRq>
        <BUS__OBJ__RECORD>
          <RECORD__NAME__ROW>
            <RECORD__NAME>action</RECORD__NAME>
          </RECORD__NAME__ROW>
          <ACTION__RECORD__ROW>
            <ACTION>WCVSTINTINQRq</ACTION>
          </ACTION__RECORD__ROW>
        </BUS__OBJ__RECORD>
        <BUS__OBJ__RECORD>
          <RECORD__NAME__ROW>
            <RECORD__NAME>SiteIntr</RECORD__NAME>
          </RECORD__NAME__ROW>
          <WC__EMP__CLASS__RECORDS>
            <KEY__LOCATION>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION>
            <KEY__MCO>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MCO>
            <KEY__POLICY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/CompanyProductCd"/>
            </KEY__POLICY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__POLICY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__POLICY__MODULE>
            <KEY__STATE__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId"/>
            </KEY__STATE__NUMBER>
            <KEY__SITE__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__SITE__NUMBER>
            <KEY__EMP__COVERAGE__SEQ>    </KEY__EMP__COVERAGE__SEQ>
            <KEY__EMP__CLASS__CODE>      </KEY__EMP__CLASS__CODE>
            <KEY__EMP__CLASS__V__C__INDICATOR> </KEY__EMP__CLASS__V__C__INDICATOR>
            <KEY__EMP__CLASS__DESCRIPTION__SEQ>  </KEY__EMP__CLASS__DESCRIPTION__SEQ>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__EFFECTIVE__DATE>9999999</PROC__EFFECTIVE__DATE>
            <BC__POLICY__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolicyCompany']/OtherId"/>
            </BC__POLICY__COMPANY>
            <BC__STATE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_State']/OtherId"/>
            </BC__STATE>
            <BC__LINE__OF__BUSINESS>WCV</BC__LINE__OF__BUSINESS>
            <KEY__USE__CODE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RoleCd']/OtherId"/> </KEY__USE__CODE>
            <KEY__SEQUENCE__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SeqNum']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__SEQUENCE__NUMBER>
            <KEY__DEEMED__NONDEEMED>N</KEY__DEEMED__NONDEEMED>
            <KEY__ID12>12</KEY__ID12>
          </WC__EMP__CLASS__RECORDS>
        </BUS__OBJ__RECORD>
      </WCVSTINTRq>
    </xsl:template>

    <xsl:template name= "ALLRq" >
    <CPPADDINRq>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>action</RECORD__NAME>
        </RECORD__NAME__ROW>
        <ACTION__RECORD__ROW>
          <ACTION>CPPADDININQRq</ACTION>
        </ACTION__RECORD__ROW>
      </BUS__OBJ__RECORD>
      <BUS__OBJ__RECORD>
        <RECORD__NAME__ROW>
          <RECORD__NAME>AddInt</RECORD__NAME>
        </RECORD__NAME__ROW>
        <POLICY__ADDLINT__ROW>
          <POLICY__KEY>
            <KEY__LOCATION__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            </KEY__LOCATION__COMPANY>
            <KEY__MASTER__COMPANY>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            </KEY__MASTER__COMPANY>
            <KEY__SYMBOL>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/CompanyProductCd"/>
            </KEY__SYMBOL>
            <KEY__POLICY__NUMBER>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/PolicyNumber"/>
            </KEY__POLICY__NUMBER>
            <KEY__MODULE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            </KEY__MODULE>
            <KEY__USE__CODE>
              <xsl:value-of select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RoleCd']/OtherId"/>
            </KEY__USE__CODE>
            <KEY__USE__LOCATION>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Location']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__USE__LOCATION>
            <KEY__SEQUENCE__NUMBER>
                <xsl:choose>
                    <xsl:when test="$app-version != '' and $app-version = '11.02'">
                        <xsl:call-template name="FormatIntParameterValue">
                            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SeqNum']/OtherId"/>
                            <xsl:with-param name="size">1</xsl:with-param>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="FormatIntParameterValue">
                            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SeqNum']/OtherId"/>
                            <xsl:with-param name="size">5</xsl:with-param>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </KEY__SEQUENCE__NUMBER>
            <KEY__INSURANCE__LINE>   </KEY__INSURANCE__LINE>
            <KEY__RISK__LOCATION__NUMBER>00000</KEY__RISK__LOCATION__NUMBER>
            <KEY__RISK__SUBLOCATION__NUMBER>00000</KEY__RISK__SUBLOCATION__NUMBER>
            <KEY__PRODUCT>      </KEY__PRODUCT>
            <KEY__UNIT__NUMBER>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </KEY__UNIT__NUMBER>
           <KEY__ID12>12</KEY__ID12>
	   <xsl:if test="$app-version != '' and $app-version &gt; 12"> 
	   <KEY__COVERAGE>      </KEY__COVERAGE>
            <KEY__COV__SEQ>00000</KEY__COV__SEQ>
            <KEY__ITEM__CODE>      </KEY__ITEM__CODE>
            <KEY__ITEM__SEQ>00000</KEY__ITEM__SEQ>
</xsl:if>
            </POLICY__KEY>
          <PROCESSING__INFO>
            <PROC__TRANSACTION__TYPE>IN</PROC__TRANSACTION__TYPE>
            <PROC__LDA__SYSTEM__DATE__OVERRIDE></PROC__LDA__SYSTEM__DATE__OVERRIDE>
            <PROC__ADDINT__LIST__INDIC/>
          </PROCESSING__INFO>
        </POLICY__ADDLINT__ROW>
      </BUS__OBJ__RECORD>
    </CPPADDINRq>
  </xsl:template>
</xsl:stylesheet>