<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <!-- FSIT# 215792 Resolution# 88992  start-->
  <xsl:param name="client-file" select="default" />
  <xsl:param name="baseLOBline" select="default"/>
  <!-- FSIT# 215792 Resolution# 88992  end-->
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
	<!-- FSIT# 215792 Resolution# 88992  start-->
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
	<!-- FSIT# 215792 Resolution# 88992  end-->
        </xsl:with-param>
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <com.csc_DriverDetailRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
	  <!-- FSIT# 215792 Resolution# 88992  start-->
          <com.csc_PolicyLevel>
            <PolicySummaryInfo>
              <PolicyNumber>
                <xsl:value-of select="normalize-space(//KEY__POLICY__NUMBER)"/>
              </PolicyNumber>
              <PolicyStatusCd/>
              <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
              <LOBCd></LOBCd>
              <GroupId></GroupId>
              <ContractTerm>
                <EffectiveDt></EffectiveDt>
                <ExpirationDt></ExpirationDt>
              </ContractTerm>
            </PolicySummaryInfo>
            <StateProvCd />
            <CompanyProductCd>
              <xsl:value-of select="normalize-space(//KEY__SYMBOL)"/>
            </CompanyProductCd>
            <ActionCd></ActionCd>
            <ContractNumber></ContractNumber>
            <com.csc_ItemIdInfo>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__LOCATION__COMPANY)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__MASTER__COMPANY)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__MODULE)"/>
                </OtherId>
              </OtherIdentifier>
            </com.csc_ItemIdInfo>
          </com.csc_PolicyLevel>
	  <!-- FSIT# 215792 Resolution# 88992  end-->
          <DriverInfo>
            <ItemIdInfo>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_DriverId</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__DRIVER__ID)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_DriverType</OtherIdTypeCd>
                <OtherId>
		<!-- FSIT# 215792 Resolution# 88992  start-->
                  <xsl:choose>
                    <xsl:when test="$baseLOBline = 'CL' ">
                     <xsl:value-of select="normalize-space(//DRVDTL__RELATION__TO__INSURED)"/>
                      </xsl:when>
                    <xsl:otherwise>
		    <!-- FSIT# 215792 Resolution# 88992  end-->
                      <xsl:value-of select="normalize-space(//KEY__DRIVER__CODE)"/>
         	      <!-- FSIT# 215792 Resolution# 88992  start-->
                    </xsl:otherwise>
                  </xsl:choose>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_InsLine</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__INSURANCE__LINE)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__PRODUCT)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_RiskLocationNumber</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__RISK__LOCATION__NUMBER)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_RiskSubLocationNumber</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__RISK__SUBLOCATION__NUMBER)"/>
		  <!-- FSIT# 215792 Resolution# 88992  end-->
                </OtherId>
              </OtherIdentifier>
            </ItemIdInfo>
            <License>
              <LicensedDt>
                <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
                  <xsl:with-param name="datefld" select="//DRVDTL__LICENSE__DATE"/>
                </xsl:call-template>
              </LicensedDt>
              <LicensePermitNumber>
                <xsl:value-of select="normalize-space(//DRVDTL__LICENSE__NUMBER)"/>
              </LicensePermitNumber>
              <StateProvCd>
                <xsl:value-of select="normalize-space(//DRVDTL__LICENSE__STATE)"/>
              </StateProvCd>
            </License>
            <PersonInfo>
              <BirthDt>
                <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
                  <xsl:with-param name="datefld" select="//DRVDTL__DATE__OF__BIRTH"/>
                </xsl:call-template>
              </BirthDt>
              <Age>
                <xsl:value-of select="normalize-space(//DRVDTL__AGE)"/>
              </Age>
              <GenderCd>
                <xsl:value-of select="normalize-space(//DRVDTL__SEX)"/>
              </GenderCd>
              <MaritalStatusCd>
                <xsl:value-of select="normalize-space(//DRVDTL__MARITAL__STATUS)"/>
              </MaritalStatusCd>
              <NameInfo>
                <CommlName>
                  <CommercialName/>
                </CommlName>
                <PersonName>
                  <TitlePrefix/>
                  <NameSuffix>
                    <xsl:value-of select="normalize-space(//DRVDTL__SUFFIX__NAME)"/>
                  </NameSuffix>
                  <Surname>
                    <xsl:value-of select="normalize-space(//DRVDTL__LAST__NAME)"/>
                  </Surname>
                  <GivenName>
                    <xsl:value-of select="normalize-space(//DRVDTL__FIRST__NAME)"/>
                  </GivenName>
                  <OtherGivenName>
                    <xsl:value-of select="normalize-space(//DRVDTL__MIDDLE__INITIAL)"/>
                  </OtherGivenName>
                  <NickName/>
                </PersonName>
                <LegalEntityCd></LegalEntityCd>
                <TaxIdentity>
                  <TaxId/>
                </TaxIdentity>
              </NameInfo>
            </PersonInfo>
            <com.csc_DriverStatus>
              <xsl:value-of select="normalize-space(//KEY__RECORD__STATUS)"/>
            </com.csc_DriverStatus>
            <com.csc_IssueCd>
              <xsl:value-of select="normalize-space(//PROC__LDA__ISSUE__CODE)"/>
            </com.csc_IssueCd>
            <com.csc_RateBook>
              <xsl:value-of select="normalize-space(//COM__RATEBOOK__RATEFILE)"/>
            </com.csc_RateBook>
            <com.csc.DriverClass>
              <xsl:value-of select="normalize-space(//DRVDTL__DRIVER__CLASS)"/>
            </com.csc.DriverClass>
            <com.csc.MVRInd>
              <xsl:value-of select="normalize-space(//DRVDTL__MVR__INDICATOR)"/>
            </com.csc.MVRInd>
            <com.csc_RelationToInsured>
	    <!-- FSIT# 215792 Resolution# 88992  start-->
              <xsl:choose>
                <xsl:when test="$baseLOBline != 'CL' ">
		<!-- FSIT# 215792 Resolution# 88992  end-->
              <xsl:value-of select="normalize-space(//DRVDTL__RELATION__TO__INSURED)"/>
	      <!-- FSIT# 215792 Resolution# 88992  start-->
                </xsl:when>
              </xsl:choose>
	      <!-- FSIT# 215792 Resolution# 88992  end-->
            </com.csc_RelationToInsured>
             <com.csc_PrincipalOperator_VH_1>
              <xsl:value-of select="normalize-space(//DRVDTL__PRINCIPAL__OPERATOR__VH__1)"/>
            </com.csc_PrincipalOperator_VH_1>
            <com.csc_PrincipalOperator_VH_2>
              <xsl:value-of select="normalize-space(//DRVDTL__PRINCIPAL__OPERATOR__VH__2)"/>
            </com.csc_PrincipalOperator_VH_2>
            <com.csc_PrincipalOperator_VH_3>
              <xsl:value-of select="normalize-space(//DRVDTL__PRINCIPAL__OPERATOR__VH__3)"/>
            </com.csc_PrincipalOperator_VH_3>
            <com.csc_ParttimeOperator_VH_1>
              <xsl:value-of select="normalize-space(//DRVDTL__PART__TIME__OPERATOR__VH__1)"/>
            </com.csc_ParttimeOperator_VH_1>
            <com.csc_ParttimeOperator_VH_2>
              <xsl:value-of select="normalize-space(//DRVDTL__PART__TIME__OPERATOR__VH__2)"/>
            </com.csc_ParttimeOperator_VH_2>
            <com.csc_ParttimeOperator_VH_3>
              <xsl:value-of select="normalize-space(//DRVDTL__PART__TIME__OPERATOR__VH__3)"/>
            </com.csc_ParttimeOperator_VH_3>
            <com_csc_ExcludedDriverInd>
              <xsl:value-of select="normalize-space(//DRVDTL__EXCLUDED__DRIVER__IND)"/>
            </com_csc_ExcludedDriverInd>
            <com.csc_UsrInd_6>
              <xsl:value-of select="normalize-space(//DRVDTL__USER__INDICATOR__6)"/>
            </com.csc_UsrInd_6>
          </DriverInfo>
        </com.csc_DriverDetailRs>
      </ClaimsSvcRs>
    </ACORD>
  </xsl:template>
</xsl:stylesheet>