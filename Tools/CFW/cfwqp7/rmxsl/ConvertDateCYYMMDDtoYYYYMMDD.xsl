<?xml version="1.0" encoding="UTF-8"?>
<!--
//********************************************************************************************
//* Programmer: Anwar M,Nishant Kumar       Date: 10/22/2007                        		 *
//* FSIT Issue: 72289                       Resolution : 42657                         		 *
//* Package   : Q00143 - Integration of POINT IN with WMS							    	 *
//* Description: Synchronization of Underwriter reassignment between POINT IN and WMS.	     *
//********************************************************************************************
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template name="cvtdateCYYMMDDtoYYYYMMDD">
		<xsl:param name="datefld"/>
		<xsl:choose>
			<!-- Taking the case of CYYMMDD -->
			<xsl:when test="string-length($datefld) = 7">
				<xsl:variable name="year" select="substring($datefld,2,2)"/>
				<xsl:variable name="month" select="substring($datefld,4,2)"/>
				<xsl:variable name="day" select="substring($datefld,6,2)"/>
				<xsl:variable name="cent" select="substring($datefld,1,1)"/>
				<xsl:if test="$cent=1">
				<!-- FSIT# 72289 Res# 42657 - Start -->	
				   <!-- <xsl:value-of select="concat(20,$year,'-',$month,'-',$day)"/> -->
				    <xsl:value-of select="concat(20,$year,$month,$day)"/>
				<!-- FSIT# 72289 Res# 42657 - End -->
				</xsl:if>
				<xsl:if test="$cent=0">
				<!-- FSIT# 72289 Res# 42657 - Start -->	
				    <!-- <xsl:value-of select="concat(19,$year,'-',$month,'-',$day)"/> -->
					<xsl:value-of select="concat(19,$year,$month,$day)"/>
				<!-- FSIT# 72289 Res# 42657 - End -->
				</xsl:if>
			</xsl:when>
	<!-- FSIT# 72289 Res# 42657 - Start-->
	<!-- taking the case of date in format CYYMM 
	<xsl:when test="string-length($datefld) = 5">
	<xsl:value-of select="concat(substring($datefld,4,2),'/',substring($datefld,2,2))"/>
	</xsl:when> -->
	<!-- FSIT# 72289 Res# 42657 - End -->

			<!-- taking the case of CYYMMDD where century indicator is 0 which would cause leading 0 to be ignored -->
			<xsl:when test="string-length($datefld) = 6">
				<xsl:variable name="year1" select="substring($datefld,1,2)"/>
				<xsl:variable name="month1" select="substring($datefld,3,2)"/>
				<xsl:variable name="day1" select="substring($datefld,5,2)"/>
				<xsl:variable name="dispcent1" select="19"/>
				<!-- FSIT# 72289 Res# 42657 - Start -->
				<!-- <xsl:value-of select="concat($dispcent1,$year1,'-',$month1,'-',$day1)"/> -->
				<xsl:value-of select="concat($dispcent1,$year1,$month1,$day1)"/>
				<!-- FSIT# 72289 Res# 42657 - End -->
			</xsl:when>
			<xsl:otherwise>
			<!-- taking the case of null date values -->
				<xsl:variable name="zerovalue">0</xsl:variable>
				<xsl:value-of select="$zerovalue"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>