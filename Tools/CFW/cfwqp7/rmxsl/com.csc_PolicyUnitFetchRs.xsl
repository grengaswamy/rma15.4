<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="DecomposeErrorRecord.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:include href="DecomposeUserArea.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <!--<xsl:param name="lob" select="default"/>-->
  <xsl:param name="issue-cd" select="default"/>
  <xsl:param name="baseLOBline" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <xsl:param name="statunit" select="default"/>
  <xsl:param name="unittypeind" select="default" />
  <!--mits 35925 start-->
  <xsl:param name="client-file" select="default"/>
  <!--mits 35925 end-->
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
        <!--mits 35925 start-->
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
        <!--mits 35925 end-->
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <xsl:apply-templates select="//ERROR__LST__ROW"/>
        <com.csc_PolicyUnitFetchRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
          <Policy id=''>
            <PolicyNumber>
              <xsl:value-of select="normalize-space(//KEY__POLICY__NUMBER)"/>
            </PolicyNumber>
            <CompanyProductCd>
              <xsl:value-of select="normalize-space(//KEY__SYMBOL)"/>
            </CompanyProductCd>
            <LOBCd>
              <xsl:value-of select="normalize-space(//BC__LINE__OF__BUSINESS)"/>
            </LOBCd>
            <com.csc_BaseLOBLine>
              <xsl:value-of select="normalize-space($baseLOBline)"/>
            </com.csc_BaseLOBLine>
            <com.csc_ActionCd></com.csc_ActionCd>
            <com.csc_IssueCd>
              <xsl:value-of select="normalize-space($issue-cd)"/>
            </com.csc_IssueCd>
          </Policy>
          <xsl:choose>
            <xsl:when test="//PERSONAL__UNIT__REC__ROW">
              <xsl:call-template name="HP-FPRow"/>
            </xsl:when>
            <xsl:when test="//ALR__VEHICLE__UNIT__ROW">
              <xsl:call-template name="APVRow"/>
            </xsl:when>
            <xsl:when test="//ALR__IMPLEMENTED__RECORDS__ROW">
              <xsl:call-template name="CPPRow"/>
            </xsl:when>
            <xsl:when test="//WC__SITE__FIELDS__RECORDS__ROW">
              <xsl:call-template name="WCVRow"/>
            </xsl:when>
            <xsl:when test="//PSA__RECORDS__ROW">
              <xsl:call-template name="InsuredUnitRow"/>
            </xsl:when>
          </xsl:choose>
        </com.csc_PolicyUnitFetchRs>
      </ClaimsSvcRs>
    </ACORD>
  </xsl:template>
  
  <xsl:template name="HP-FPRow" >
    <PropertyLossInfo id="">
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_PropertyKey</OtherIdTypeCd>
          <OtherId/>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
          <OtherId>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value" select="//KEY__UNIT__NUMBER"/>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
          <OtherId>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value">
                <xsl:value-of select="$statunit"/>
              </xsl:with-param>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
          <OtherId></OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//UNIT__RATE__STATE__UNIT)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__INSURANCE__LINE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__PRODUCT)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__RISK__LOCATION__NUMBER)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__RISK__SUBLOCATION__NUMBER)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <ItemDefinition>
        <ItemTypeCd/>
        <SerialIdNumber/>
        <com.csc_SerialIdNumber/>
      </ItemDefinition>
      <PremiumInfo>
        <PremiumAmt>
          <xsl:value-of select="normalize-space(//COM__RATE__PREMIUM)"/>
        </PremiumAmt>
      </PremiumInfo>
      <com.csc_UnitStatus>
        <xsl:value-of select="normalize-space(//KEY__RECORD__STATUS)"/>
      </com.csc_UnitStatus>
      <com.csc_ConstructionCd>
        <xsl:value-of select="normalize-space(//UNIT__CONSTRUCTION__CODE)"/>
      </com.csc_ConstructionCd>
      <com.csc_ProtectionClass>
        <xsl:value-of select="normalize-space(//UNIT__PROTECTION__CLASS)"/>
      </com.csc_ProtectionClass>
      <com.csc_ConditionClassCd/>
      <com.csc_BuildingQualityCd></com.csc_BuildingQualityCd>
      <com.csc_BuildingTotalFloorArea>
        <NumUnits></NumUnits>
        <UnitMeasurementCd/>
      </com.csc_BuildingTotalFloorArea>
      <com.csc_BuildingTotalFloorsNbr></com.csc_BuildingTotalFloorsNbr>
      <com.csc_PropertyIdentificationNumber></com.csc_PropertyIdentificationNumber>
      <com.csc_DwellUseCd></com.csc_DwellUseCd>
      <com.csc_YearBuilt>
        <xsl:value-of select="normalize-space(//UNIT__YEAR__OF__CONSTRUCTION)"/>
      </com.csc_YearBuilt>
      <com.csc_RateBook>
        <xsl:value-of select="normalize-space(//COM__RATEBOOK__RATEFILE)"/>
      </com.csc_RateBook>
      <com.csc_MobileHome>
        <Length>
          <NumUnits>
            <xsl:value-of select="normalize-space(//UNIT__LENGTH)"/>
          </NumUnits>
          <UnitMeasurementCd/>
        </Length>
        <TieDownCd></TieDownCd>
        <Width>
          <NumUnits>
            <xsl:value-of select="normalize-space(//UNIT__WIDTH)"/>
          </NumUnits>
          <UnitMeasurementCd/>
        </Width>
      </com.csc_MobileHome>
      <com.csc_UnitLossData>
        <com.csc_UnitDescription>
          <ActionCd></ActionCd>
          <com.csc_UnitDesc>
            <xsl:value-of select="normalize-space(//UNIT__DESCRIPTION__1)"/>
          </com.csc_UnitDesc>
          <com.csc_UnitCity>
            <xsl:value-of select="normalize-space(//UNIT__DESCRIPTION__2)"/>
          </com.csc_UnitCity>
          <com.csc_County>
            <xsl:value-of select="normalize-space(//UNIT__COUNTY)"/>
          </com.csc_County>
          <com.csc_Territory>
            <xsl:value-of select="normalize-space(//UNIT__TERRITORY)"/>
          </com.csc_Territory>
          <com.csc_Zipcode>
            <xsl:value-of select="normalize-space(//UNIT__ZIP__CODE)"/>
          </com.csc_Zipcode>
          <com.csc_NumberFamilies>
            <xsl:value-of select="normalize-space(//UNIT__NUMBER__OF__FAMILIES)"/>
          </com.csc_NumberFamilies>
          <com.csc_PurchaseDt>
            <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
              <xsl:with-param name="datefld" select="//UNIT__PURCHASE__DATE"/>
            </xsl:call-template>
          </com.csc_PurchaseDt>
          <com.csc_PurchaseAmt>
            <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
              <xsl:with-param name="datefld" select="//UNIT__PURCHASE__DATE"/>
            </xsl:call-template>
          </com.csc_PurchaseAmt>
          <com.csc_InsideCity>
            <xsl:value-of select="normalize-space(//UNIT__INSIDE__CITY)"/>
          </com.csc_InsideCity>
          <com.csc_OccupancyCd>
            <xsl:value-of select="normalize-space(//UNIT__OCCUPANCY__CODE)"/>
          </com.csc_OccupancyCd>
        </com.csc_UnitDescription>
      </com.csc_UnitLossData>
      <com.csc_ActionCd></com.csc_ActionCd>
      <com.csc_NatureBusinessCd/>
      <com.csc_PremisesInd/>
      <com.csc_LastChangeDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//COM__LAST__CHANGE__DATE"/>
        </xsl:call-template>
      </com.csc_LastChangeDt>
      <com.csc_ChangeEffDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//COM__CHANGE__EFFECTIVE__DATE"/>
        </xsl:call-template>
      </com.csc_ChangeEffDt>
      <com.csc_FireDistrict></com.csc_FireDistrict>
      <com.csc_FireDistrictCd></com.csc_FireDistrictCd>
      <com.csc_CoverageLossInfo>
        <Deductible>
          <DeductibleTypeCd>
            <xsl:value-of select="normalize-space(//UNIT__DEDUCTIBLE__TYPE)"/>
          </DeductibleTypeCd>
          <DeductibleAmt>
            <xsl:call-template name="FormatAmountValue">
              <xsl:with-param name="amount" select="//UNIT__COVERAGE__DEDUCTIBLE" />
            </xsl:call-template>
          </DeductibleAmt>
        </Deductible>
        <Coverage>
          <CoverageCd>COVA</CoverageCd>
          <Limit>
            <LimitAmt>
              <xsl:value-of select="normalize-space(//UNIT__COVERAGE__1__LIMIT)"/>
            </LimitAmt>
          </Limit>
          <CurrentTermAmt>
            <xsl:value-of select="normalize-space(//UNIT__COVERAGE__1__PREMIUM)"/>
          </CurrentTermAmt>
        </Coverage>
        <Coverage>
          <CoverageCd>COVB</CoverageCd>
          <Limit>
            <LimitAmt>
              <xsl:value-of select="normalize-space(//UNIT__COVERAGE__2__LIMIT)"/>
            </LimitAmt>
          </Limit>
          <CurrentTermAmt>
            <xsl:value-of select="normalize-space(//UNIT__COVERAGE__2__PREMIUM)"/>
          </CurrentTermAmt>
        </Coverage>
        <Coverage>
          <CoverageCd>COVC</CoverageCd>
          <Limit>
            <LimitAmt>
              <xsl:value-of select="normalize-space(//UNIT__COVERAGE__3__LIMIT)"/>
            </LimitAmt>
          </Limit>
          <CurrentTermAmt>
            <xsl:value-of select="normalize-space(//UNIT__COVERAGE__3__PREMIUM)"/>
          </CurrentTermAmt>
        </Coverage>
        <Coverage>
          <CoverageCd>COVD</CoverageCd>
          <Limit>
            <LimitAmt>
              <xsl:value-of select="normalize-space(//UNIT__COVERAGE__4__LIMIT)"/>
            </LimitAmt>
          </Limit>
          <CurrentTermAmt>
            <xsl:value-of select="normalize-space(//UNIT__COVERAGE__4__PREMIUM)"/>
          </CurrentTermAmt>
        </Coverage>
        <Coverage>
          <CoverageCd>COVE</CoverageCd>
          <Limit>
            <LimitAmt>
              <xsl:value-of select="normalize-space(//UNIT__COVERAGE__5__LIMIT)"/>
            </LimitAmt>
          </Limit>
          <CurrentTermAmt>
            <xsl:value-of select="normalize-space(//UNIT__COVERAGE__5__PREMIUM)"/>
          </CurrentTermAmt>
        </Coverage>
        <Coverage>
          <CoverageCd>COVF</CoverageCd>
          <Limit>
            <LimitAmt>
              <xsl:value-of select="normalize-space(//UNIT__COVERAGE__6__LIMIT)"/>
            </LimitAmt>
          </Limit>
          <CurrentTermAmt>
            <xsl:value-of select="normalize-space(//UNIT__COVERAGE__6__PREMIUM)"/>
          </CurrentTermAmt>
        </Coverage>
        <Coverage>
          <CoverageCd></CoverageCd>
          <Limit>
            <LimitAmt>
              <xsl:value-of select="normalize-space(//UNIT__COVERAGE__7__LIMIT)"/>
            </LimitAmt>
          </Limit>
          <CurrentTermAmt>
            <xsl:value-of select="normalize-space(//UNIT__COVERAGE__7__PREMIUM)"/>
          </CurrentTermAmt>
        </Coverage>
        <Coverage>
          <CoverageCd/>
          <Limit>
            <LimitAmt></LimitAmt>
          </Limit>
          <CurrentTermAmt>
            <xsl:value-of select="normalize-space(//UNIT__COVERAGE__8__PREMIUM)"/>
          </CurrentTermAmt>
        </Coverage>
        <Coverage>
          <CoverageCd/>
          <Limit>
            <LimitAmt></LimitAmt>
          </Limit>
          <CurrentTermAmt>
            <xsl:value-of select="normalize-space(//UNIT__COVERAGE__9__PREMIUM)"/>
          </CurrentTermAmt>
        </Coverage>
      </com.csc_CoverageLossInfo>
    </PropertyLossInfo>
  </xsl:template>

  <xsl:template name="APVRow">
    <AutoLossInfo id="">
      <VehInfo>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_VehicleKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
            <OtherId>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="//KEY__UNIT__NUMBER"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
            <OtherId>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value">
                  <xsl:value-of select="$statunit"/>
                </xsl:with-param>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
            <OtherId></OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//UNIT__RATE__STATE__UNIT)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//KEY__PRODUCT)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//KEY__INSURANCE__LINE)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//KEY__RISK__LOCATION__NUMBER)"/>              
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//KEY__RISK__SUBLOCATION__NUMBER)"/>
            </OtherId>
          </OtherIdentifier>
        </ItemIdInfo>
        <Manufacturer>
          <xsl:value-of select="normalize-space(//UNIT__MAKE__DESCRIPTION)"/>
        </Manufacturer>
        <Model>
          <xsl:value-of select="normalize-space(//UNIT__MAKE__DESCRIPTION)"/>
        </Model>
        <ModelYear>
          <xsl:value-of select="normalize-space(//UNIT__YEAR__MAKE)"/>
        </ModelYear>
      </VehInfo>
      <PremiumInfo>
        <PremiumAmt>
          <xsl:value-of select="normalize-space(//COM__RATE__PREMIUM)"/>
        </PremiumAmt>
      </PremiumInfo>
      <com.csc_RateBook>
        <xsl:value-of select="normalize-space(//COM__RATEBOOK__RATEFILE)"/>
      </com.csc_RateBook>
      <com.csc_PurchaseDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//UNIT__PURCHASE__DATE"/>
        </xsl:call-template>
      </com.csc_PurchaseDt>
      <com.csc_RegistrationStateProvCd/>
      <com.csc_OdometerReading>
        <NumUnits/>
        <UnitMeasurementCd/>
      </com.csc_OdometerReading>
      <com.csc_CostNewAmt>
        <Amt>
          <xsl:value-of select="normalize-space(//UNIT__COST__NEW)"/>
        </Amt>
        <CurCd/>
      </com.csc_CostNewAmt>
      <com.csc_ViolationCitation>
        <ActionCd></ActionCd>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_ViolationCitationKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <com.csc_ViolationCitationCd/>
      </com.csc_ViolationCitation>
      <com.csc_LeasedVehicleInd/>
      <com.csc_LicensePlateNumber/>
      <com.csc_VehicleIdentificationNumber>
        <xsl:value-of select="normalize-space(//UNIT__SERIAL__NUMBER)"/>
      </com.csc_VehicleIdentificationNumber>
      <com.csc_LeaseEndDt/>
      <com.csc_VehicleTypeCd>
        <xsl:value-of select="normalize-space(//UNIT__TYPE__VEHICLE)"/>
      </com.csc_VehicleTypeCd>
      <com.csc_BodyType>
        <xsl:value-of select="normalize-space(//UNIT__BODY__TYPE)"/>
      </com.csc_BodyType>
      <com.csc_EngineTypeCd/>
      <com.csc_ActualSpeed>
        <NumUnits/>
        <UnitMeasurementCd/>
      </com.csc_ActualSpeed>
      <com.csc_DrivableInd/>
      <com.csc_StatedAmt>
        <Amt>
          <xsl:value-of select="normalize-space(//UNIT__STATED__AMOUNT)"/>
        </Amt>
        <CurCd/>
      </com.csc_StatedAmt>
      <com.csc_TrailerInd/>
      <com.csc_DriverAssigned>
        <xsl:value-of select="normalize-space(//UNIT__DRIVER__ASSIGNED)"/>
      </com.csc_DriverAssigned>
      <com.csc_MilesToWork>
        <xsl:value-of select="normalize-space(//UNIT__MILES__TO__WORK)"/>
      </com.csc_MilesToWork>
      <com.csc_DaysPerWeek>
        <xsl:value-of select="normalize-space(//UNIT__DAYS__PER__WEEK)"/>
      </com.csc_DaysPerWeek>
      <com.csc_AnnualMiles>
        <xsl:value-of select="normalize-space(//UNIT__ANNUAL__MILES)"/>
      </com.csc_AnnualMiles>
      <com.csc_DriverSameInd/>
      <com.csc_DriverClass>
        <xsl:value-of select="normalize-space(//UNIT__DRIVER__CLASS)"/>
      </com.csc_DriverClass>
      <com.csc_OwnerSameInd/>
      <com.csc_DriverNumber/>
      <com.csc_DriverGuiltCd/>
      <com.csc_ViolationCitationInd/>
      <com.csc_ViolationCitationCd/>
      <com.csc_RoadTypeCd/>
      <com.csc_RoadSurfaceCd/>
      <com.csc_PrimaryClassCd>
        <xsl:value-of select="normalize-space(//UNIT__PRIMARY__CLASS__CODE)"/>
      </com.csc_PrimaryClassCd>
      <com.csc_SecondaryClassCd>
        <xsl:value-of select="normalize-space(//UNIT__SECONDARY__CLASS__CODE)"/>
      </com.csc_SecondaryClassCd>
      <com.csc_PostedSpeedLimitNumber>
        <NumUnits/>
        <UnitMeasurementCd/>
      </com.csc_PostedSpeedLimitNumber>
      <com.csc_LightConditionCd/>
      <com.csc_DenselyPopulatedAreaInd/>
      <com.csc_TrafficLightInd/>
      <com.csc_TotalLossInd/>
      <com.csc_WherePropertyCanBeSeenInfo>
        <ActionCd></ActionCd>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <com.csc_WherePropertyCanBeSeenDesc/>
      </com.csc_WherePropertyCanBeSeenInfo>
      <com.csc_MiscellaneousInd/>
      <com.csc_VehicleUse>
        <ActionCd>
          <xsl:value-of select="normalize-space(//UNIT__VEHICLE__USE__CODE)"/>
        </ActionCd>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <com.csc_VehicleUseDesc></com.csc_VehicleUseDesc>
      </com.csc_VehicleUse>
      <com.csc_DamageDescInfo>
        <ActionCd></ActionCd>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <DamageDesc/>
      </com.csc_DamageDescInfo>
      <com.csc_UnitLossData>
        <ReportNumber/>
        <com.csc_UnitDescription>
          <ActionCd></ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_UnitDesc></com.csc_UnitDesc>
          <com.csc_Territory>
            <xsl:value-of select="normalize-space(//UNIT__TERRITORY)"/>
          </com.csc_Territory>
          <com.csc_VehicleSymbol>
            <xsl:value-of select="normalize-space(//UNIT__VEHICLE__SYMBOL)"/>
          </com.csc_VehicleSymbol>
          <com.csc_Zipcode>
            <xsl:value-of select="normalize-space(//UNIT__ZIP__CODE)"/>
          </com.csc_Zipcode>
        </com.csc_UnitDescription>
        <com.csc_GarageLocation>
          <ActionCd/>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_ClmCommentKey</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_CmtFolderKey</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_GarageLocationDesc></com.csc_GarageLocationDesc>
        </com.csc_GarageLocation>
        <com.csc_LossPlace>
          <ActionCd></ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_LossPlaceDesc/>
        </com.csc_LossPlace>
        <com.csc_ObjectComment>
          <ActionCd></ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_ObjectCmtDesc/>
        </com.csc_ObjectComment>
        <com.csc_PermissionGivenInd/>
        <com.csc_BodilyInjuryInd/>
        <com.csc_AlcoholDrugsInd/>
        <com.csc_CurrentValueAmt>
          <Amt/>
          <CurCd/>
        </com.csc_CurrentValueAmt>
        <com.csc_ValueBeforeLossAmt>
          <Amt/>
          <CurCd/>
        </com.csc_ValueBeforeLossAmt>
        <com.csc_LossAdjustedInd/>
        <com.csc_AdjustedDt/>
        <com.csc_OtherCoverageInd/>
        <com.csc_ReinsuranceInd/>
        <com.csc_LossCessionNbr/>
        <com.csc_EstimatedLaborHours/>
        <com.csc_EstimatedMaterialCostAmt>
          <Amt/>
          <CurCd/>
        </com.csc_EstimatedMaterialCostAmt>
        <com.csc_EstimatedRepairHours/>
        <com.csc_EstimatedReplacementCostAmt>
          <Amt/>
          <CurCd/>
        </com.csc_EstimatedReplacementCostAmt>
        <com.csc_ActualLaborHours/>
        <com.csc_ActualMaterialCostAmt>
          <Amt/>
          <CurCd/>
        </com.csc_ActualMaterialCostAmt>
        <com.csc_ActualRepairHours/>
        <com.csc_ActualReplacementCostAmt>
          <Amt/>
          <CurCd/>
        </com.csc_ActualReplacementCostAmt>
        <com.csc_UnitTypeInd/>
        <com.csc_PurchaseAmt>
          <Amt/>
          <CurCd/>
        </com.csc_PurchaseAmt>
        <com.csc_AtFaultInd/>
        <com.csc_UnitStatus>
          <xsl:value-of select="normalize-space(//KEY__RECORD__STATUS)"/>
        </com.csc_UnitStatus>
        <com.csc_AirbagDiscount>
          <xsl:value-of select="normalize-space(//UNIT__AIRBAG__DISCOUNT__INDICATOR)"/>
        </com.csc_AirbagDiscount>
        <com.csc_AntiTheftInd>
          <xsl:value-of select="normalize-space(//UNIT__ANTI__THEFT__INDICATOR)"/>
        </com.csc_AntiTheftInd>
        <com.csc_AntiLockBrakes>
          <xsl:value-of select="normalize-space(//UNIT__ANTI__LOCK__BRAKES__DISCOUNT)"/>
        </com.csc_AntiLockBrakes>
        <com.csc_PermissionGiven>
          <ActionCd></ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_PermissionGivenDesc/>
        </com.csc_PermissionGiven>
        <com.csc_WhenPropertyCanBeSeen>
          <ActionCd></ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_WhenPropertyCanBeSeenDesc/>
        </com.csc_WhenPropertyCanBeSeen>
      </com.csc_UnitLossData>
      <com.csc_CoverageLossInfo>
        <Coverage>
          <CoverageCd/>
        </Coverage>
      </com.csc_CoverageLossInfo>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_ActionCd></com.csc_ActionCd>
    </AutoLossInfo>
  </xsl:template>

  <xsl:template name="CPPRow">
    <!--<xsl:variable name="RiskLocationNum" >
      <xsl:value-of select="normalize-space(//KEY__RISK__LOCATION__NUMBER)"/>
    </xsl:variable>-->
    
    <xsl:choose>
      <!--Case for Vehicle unit -->
      <xsl:when test="$unittypeind = 'V'">
        <AutoLossInfo id="">
          <VehInfo>
            <ItemIdInfo>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_VehicleKey</OtherIdTypeCd>
                <OtherId/>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
                <OtherId>
                  <xsl:call-template name="FormatIntParameterValue">
                    <xsl:with-param name="value" select="//KEY__UNIT__NUMBER"/>
                    <xsl:with-param name="size">5</xsl:with-param>
                  </xsl:call-template>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
                <OtherId>
                  <xsl:call-template name="FormatIntParameterValue">
                    <xsl:with-param name="value">
                      <xsl:value-of select="$statunit"/>
                    </xsl:with-param>
                    <xsl:with-param name="size">5</xsl:with-param>
                  </xsl:call-template>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
                <OtherId></OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__RATE__STATE)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__PRODUCT)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__INSURANCE__LINE)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__RISK__LOCATION__NUMBER)"/>
                </OtherId>
              </OtherIdentifier>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
                <OtherId>
                  <xsl:value-of select="normalize-space(//KEY__RISK__SUBLOCATION__NUMBER)"/>
                </OtherId>
              </OtherIdentifier>
            </ItemIdInfo>
            <Manufacturer>
              <xsl:value-of select="normalize-space(//UNIT__DESCRIPTION__1)"/>
            </Manufacturer>
            <Model>
              <xsl:value-of select="normalize-space(//UNIT__DESCRIPTION__1)"/>
            </Model>
            <ModelYear>
              <xsl:value-of select="normalize-space(//UNIT__YEAR)"/>
            </ModelYear>
          </VehInfo>
          <PremiumInfo>
            <PremiumAmt>
              <xsl:value-of select="normalize-space(//RATE__PREMIUM)"/>
            </PremiumAmt>
          </PremiumInfo>
          <com.csc_RateBook>
            <xsl:value-of select="normalize-space(//RATEBOOK__RATEFILE)"/>
          </com.csc_RateBook>
          <com.csc_PurchaseDt>
          </com.csc_PurchaseDt>
          <com.csc_RegistrationStateProvCd/>
          <com.csc_OdometerReading>
            <NumUnits/>
            <UnitMeasurementCd/>
          </com.csc_OdometerReading>
          <com.csc_CostNewAmt>
            <Amt>
            </Amt>
            <CurCd/>
          </com.csc_CostNewAmt>
          <com.csc_ViolationCitation>
            <ActionCd></ActionCd>
            <ItemIdInfo>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_ViolationCitationKey</OtherIdTypeCd>
                <OtherId/>
              </OtherIdentifier>
            </ItemIdInfo>
            <com.csc_ViolationCitationCd/>
          </com.csc_ViolationCitation>
          <com.csc_LeasedVehicleInd/>
          <com.csc_LicensePlateNumber/>
          <com.csc_VehicleIdentificationNumber>
            <xsl:value-of select="normalize-space(//UNIT__DESCRIPTION__2)"/>
          </com.csc_VehicleIdentificationNumber>
          <com.csc_LeaseEndDt/>
          <com.csc_VehicleTypeCd>
          </com.csc_VehicleTypeCd>
          <com.csc_BodyType>
          </com.csc_BodyType>
          <com.csc_EngineTypeCd/>
          <com.csc_ActualSpeed>
            <NumUnits/>
            <UnitMeasurementCd/>
          </com.csc_ActualSpeed>
          <com.csc_DrivableInd/>
          <com.csc_StatedAmt>
            <Amt>
              <xsl:value-of select="normalize-space(//UNIT__STATED__AMOUNT)"/>
            </Amt>
            <CurCd/>
          </com.csc_StatedAmt>
          <com.csc_TrailerInd/>
          <com.csc_DriverAssigned>
          </com.csc_DriverAssigned>
          <com.csc_MilesToWork>
          </com.csc_MilesToWork>
          <com.csc_DaysPerWeek>
          </com.csc_DaysPerWeek>
          <com.csc_AnnualMiles>
          </com.csc_AnnualMiles>
          <com.csc_DriverSameInd/>
          <com.csc_DriverClass>
          </com.csc_DriverClass>
          <com.csc_OwnerSameInd/>
          <com.csc_DriverNumber/>
          <com.csc_DriverGuiltCd/>
          <com.csc_ViolationCitationInd/>
          <com.csc_ViolationCitationCd/>
          <com.csc_RoadTypeCd/>
          <com.csc_RoadSurfaceCd/>
          <com.csc_PrimaryClassCd>
            <xsl:value-of select="normalize-space(//ALR__CLASS)"/>
          </com.csc_PrimaryClassCd>
          <com.csc_PrimaryClassDesc>
            <xsl:value-of select="normalize-space(//ALR__CLASS__DESC)"/>
          </com.csc_PrimaryClassDesc>
          <com.csc_SecondaryClassCd>
          </com.csc_SecondaryClassCd>
          <com.csc_PostedSpeedLimitNumber>
            <NumUnits/>
            <UnitMeasurementCd/>
          </com.csc_PostedSpeedLimitNumber>
          <com.csc_LightConditionCd/>
          <com.csc_DenselyPopulatedAreaInd/>
          <com.csc_TrafficLightInd/>
          <com.csc_TotalLossInd/>
          <com.csc_WherePropertyCanBeSeenInfo>
            <ActionCd></ActionCd>
            <ItemIdInfo>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
                <OtherId/>
              </OtherIdentifier>
            </ItemIdInfo>
            <com.csc_WherePropertyCanBeSeenDesc/>
          </com.csc_WherePropertyCanBeSeenInfo>
          <com.csc_MiscellaneousInd/>
          <com.csc_VehicleUse>
            <ActionCd>
            </ActionCd>
            <ItemIdInfo>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
                <OtherId/>
              </OtherIdentifier>
            </ItemIdInfo>
            <com.csc_VehicleUseDesc></com.csc_VehicleUseDesc>
          </com.csc_VehicleUse>
          <com.csc_DamageDescInfo>
            <ActionCd></ActionCd>
            <ItemIdInfo>
              <OtherIdentifier>
                <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
                <OtherId/>
              </OtherIdentifier>
            </ItemIdInfo>
            <DamageDesc/>
          </com.csc_DamageDescInfo>
          <com.csc_UnitLossData>
            <ReportNumber/>
            <com.csc_UnitDescription>
              <ActionCd></ActionCd>
              <ItemIdInfo>
                <OtherIdentifier>
                  <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
                  <OtherId/>
                </OtherIdentifier>
              </ItemIdInfo>
              <com.csc_UnitDesc></com.csc_UnitDesc>
              <com.csc_Territory>
              </com.csc_Territory>
              <com.csc_VehicleSymbol>
              </com.csc_VehicleSymbol>
              <com.csc_Zipcode>
                <xsl:value-of select="normalize-space(//UNIT__ZIP__CODE)"/>
              </com.csc_Zipcode>
              <com.csc_City>
                <xsl:value-of select="normalize-space(//STDFT__ADDRESS__CITY)"/>
              </com.csc_City>
              <com.csc_CountyCd>
                <xsl:value-of select="normalize-space(//STDFT__COUNTY__CODE)"/>
              </com.csc_CountyCd>
            </com.csc_UnitDescription>
            <com.csc_GarageLocation>
              <ActionCd/>
              <ItemIdInfo>
                <OtherIdentifier>
                  <OtherIdTypeCd>com.csc_ClmCommentKey</OtherIdTypeCd>
                  <OtherId/>
                </OtherIdentifier>
                <OtherIdentifier>
                  <OtherIdTypeCd>com.csc_CmtFolderKey</OtherIdTypeCd>
                  <OtherId/>
                </OtherIdentifier>
                <OtherIdentifier>
                  <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
                  <OtherId/>
                </OtherIdentifier>
              </ItemIdInfo>
              <com.csc_GarageLocationDesc></com.csc_GarageLocationDesc>
            </com.csc_GarageLocation>
            <com.csc_LossPlace>
              <ActionCd></ActionCd>
              <ItemIdInfo>
                <OtherIdentifier>
                  <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
                  <OtherId/>
                </OtherIdentifier>
              </ItemIdInfo>
              <com.csc_LossPlaceDesc/>
            </com.csc_LossPlace>
            <com.csc_ObjectComment>
              <ActionCd></ActionCd>
              <ItemIdInfo>
                <OtherIdentifier>
                  <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
                  <OtherId/>
                </OtherIdentifier>
              </ItemIdInfo>
              <com.csc_ObjectCmtDesc/>
            </com.csc_ObjectComment>
            <com.csc_PermissionGivenInd/>
            <com.csc_BodilyInjuryInd/>
            <com.csc_AlcoholDrugsInd/>
            <com.csc_CurrentValueAmt>
              <Amt/>
              <CurCd/>
            </com.csc_CurrentValueAmt>
            <com.csc_ValueBeforeLossAmt>
              <Amt/>
              <CurCd/>
            </com.csc_ValueBeforeLossAmt>
            <com.csc_LossAdjustedInd/>
            <com.csc_AdjustedDt/>
            <com.csc_OtherCoverageInd/>
            <com.csc_ReinsuranceInd/>
            <com.csc_LossCessionNbr/>
            <com.csc_EstimatedLaborHours/>
            <com.csc_EstimatedMaterialCostAmt>
              <Amt/>
              <CurCd/>
            </com.csc_EstimatedMaterialCostAmt>
            <com.csc_EstimatedRepairHours/>
            <com.csc_EstimatedReplacementCostAmt>
              <Amt/>
              <CurCd/>
            </com.csc_EstimatedReplacementCostAmt>
            <com.csc_ActualLaborHours/>
            <com.csc_ActualMaterialCostAmt>
              <Amt/>
              <CurCd/>
            </com.csc_ActualMaterialCostAmt>
            <com.csc_ActualRepairHours/>
            <com.csc_ActualReplacementCostAmt>
              <Amt/>
              <CurCd/>
            </com.csc_ActualReplacementCostAmt>
            <com.csc_UnitTypeInd/>
            <com.csc_PurchaseAmt>
              <Amt/>
              <CurCd/>
            </com.csc_PurchaseAmt>
            <com.csc_AtFaultInd/>
            <com.csc_UnitStatus>
              <xsl:value-of select="normalize-space(//KEY__RECORD__STATUS)"/>
            </com.csc_UnitStatus>
            <com.csc_AirbagDiscount>
            </com.csc_AirbagDiscount>
            <com.csc_AntiTheftInd>
            </com.csc_AntiTheftInd>
            <com.csc_AntiLockBrakes>
            </com.csc_AntiLockBrakes>
            <com.csc_PermissionGiven>
              <ActionCd></ActionCd>
              <ItemIdInfo>
                <OtherIdentifier>
                  <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
                  <OtherId/>
                </OtherIdentifier>
              </ItemIdInfo>
              <com.csc_PermissionGivenDesc/>
            </com.csc_PermissionGiven>
            <com.csc_WhenPropertyCanBeSeen>
              <ActionCd></ActionCd>
              <ItemIdInfo>
                <OtherIdentifier>
                  <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
                  <OtherId/>
                </OtherIdentifier>
              </ItemIdInfo>
              <com.csc_WhenPropertyCanBeSeenDesc/>
            </com.csc_WhenPropertyCanBeSeen>
          </com.csc_UnitLossData>
          <com.csc_CoverageLossInfo>
            <Coverage>
              <CoverageCd/>
            </Coverage>
          </com.csc_CoverageLossInfo>
          <com.csc_FinancialInd></com.csc_FinancialInd>
          <com.csc_ActionCd></com.csc_ActionCd>
          <com.csc_AdditionalUserArea>
            <xsl:choose>
              <xsl:when test="normalize-space($app-version) = 'PIJ'">
                <xsl:call-template name="DecomposeUserArea_PIJ"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="DecomposeUserArea"/>
              </xsl:otherwise>
            </xsl:choose>            
          </com.csc_AdditionalUserArea>
        </AutoLossInfo>
      </xsl:when>
      <xsl:when test="$unittypeind ='P'">     
        <PropertyLossInfo id="">
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_PropertyKey</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
              <OtherId>
                <xsl:call-template name="FormatIntParameterValue">
                  <xsl:with-param name="value" select="//KEY__UNIT__NUMBER"/>
                  <xsl:with-param name="size">5</xsl:with-param>
                </xsl:call-template>
              </OtherId>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
              <OtherId>
                <xsl:call-template name="FormatIntParameterValue">
                  <xsl:with-param name="value">
                    <xsl:value-of select="$statunit"/>
                  </xsl:with-param>
                  <xsl:with-param name="size">5</xsl:with-param>
                </xsl:call-template>
              </OtherId>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
              <OtherId></OtherId>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
              <OtherId>
                <xsl:value-of select="normalize-space(//KEY__RATE__STATE)"/>
              </OtherId>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
              <OtherId>
                <xsl:value-of select="normalize-space(//KEY__PRODUCT)"/>
              </OtherId>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
              <OtherId>
                <xsl:value-of select="normalize-space(//KEY__INSURANCE__LINE)"/>
              </OtherId>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
              <OtherId>
                <xsl:value-of select="normalize-space(//KEY__RISK__LOCATION__NUMBER)"/>
              </OtherId>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
              <OtherId>
                <xsl:value-of select="normalize-space(//KEY__RISK__SUBLOCATION__NUMBER)"/>
              </OtherId>
            </OtherIdentifier>
          </ItemIdInfo>
          <ItemDefinition>
            <ItemTypeCd/>
            <SerialIdNumber/>
            <com.csc_SerialIdNumber/>
          </ItemDefinition>
          <PremiumInfo>
            <PremiumAmt>
              <xsl:value-of select="normalize-space(//RATE__PREMIUM)"/>
            </PremiumAmt>
          </PremiumInfo>
          <com.csc_UnitStatus>
            <xsl:value-of select="normalize-space(//KEY__RECORD__STATUS)"/>
          </com.csc_UnitStatus>
          <com.csc_ConstructionCd>
          </com.csc_ConstructionCd>
          <com.csc_ProtectionClass>
          </com.csc_ProtectionClass>
          <com.csc_PrimaryClassCd>
            <xsl:value-of select="normalize-space(//ALR__CLASS)"/>
          </com.csc_PrimaryClassCd>
          <com.csc_PrimaryClassDesc>
            <xsl:value-of select="normalize-space(//ALR__CLASS__DESC)"/>
          </com.csc_PrimaryClassDesc>
          <com.csc_ConditionClassCd/>
          <com.csc_BuildingQualityCd></com.csc_BuildingQualityCd>
          <com.csc_BuildingTotalFloorArea>
            <NumUnits></NumUnits>
            <UnitMeasurementCd/>
          </com.csc_BuildingTotalFloorArea>
          <com.csc_BuildingTotalFloorsNbr></com.csc_BuildingTotalFloorsNbr>
          <com.csc_PropertyIdentificationNumber></com.csc_PropertyIdentificationNumber>
          <com.csc_DwellUseCd></com.csc_DwellUseCd>
          <com.csc_YearBuilt>
          </com.csc_YearBuilt>
          <com.csc_RateBook>
            <xsl:value-of select="normalize-space(//RATEBOOK__RATEFILE)"/>
          </com.csc_RateBook>
          <com.csc_MobileHome>
            <Length>
              <NumUnits>
              </NumUnits>
              <UnitMeasurementCd/>
            </Length>
            <TieDownCd></TieDownCd>
            <Width>
              <NumUnits>
              </NumUnits>
              <UnitMeasurementCd/>
            </Width>
          </com.csc_MobileHome>
          <com.csc_UnitLossData>
            <com.csc_UnitDescription>
              <ActionCd></ActionCd>
              <com.csc_UnitDesc>
                <xsl:value-of select="normalize-space(//UNIT__DESCRIPTION__1)"/>
                <xsl:value-of select="' '"/>
                <xsl:value-of select="normalize-space(//UNIT__DESCRIPTION__2)"/>
              </com.csc_UnitDesc>
              <com.csc_UnitCity>
                <xsl:value-of select="normalize-space(//STDFT__ADDRESS__CITY)"/>
              </com.csc_UnitCity>
              <com.csc_County>
                <xsl:value-of select="normalize-space(//STDFT__COUNTY__CODE)"/>
              </com.csc_County>
              <com.csc_Territory>
              </com.csc_Territory>
              <com.csc_Zipcode>
              </com.csc_Zipcode>
              <com.csc_NumberFamilies>
              </com.csc_NumberFamilies>
              <com.csc_PurchaseDt>
              </com.csc_PurchaseDt>
              <com.csc_PurchaseAmt>
              </com.csc_PurchaseAmt>
              <com.csc_InsideCity>
              </com.csc_InsideCity>
              <com.csc_OccupancyCd>
              </com.csc_OccupancyCd>
            </com.csc_UnitDescription>
          </com.csc_UnitLossData>
          <com.csc_ActionCd></com.csc_ActionCd>
          <com.csc_NatureBusinessCd/>
          <com.csc_PremisesInd/>
          <com.csc_LastChangeDt>
          </com.csc_LastChangeDt>
          <com.csc_ChangeEffDt> 
          </com.csc_ChangeEffDt>
          <com.csc_FireDistrict></com.csc_FireDistrict>
          <com.csc_FireDistrictCd></com.csc_FireDistrictCd>
          <com.csc_CoverageLossInfo>
          </com.csc_CoverageLossInfo>
          <com.csc_AdditionalUserArea>
            <xsl:choose>
              <xsl:when test="normalize-space($app-version) = 'PIJ'">
                <xsl:call-template name="DecomposeUserArea_PIJ"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="DecomposeUserArea"/>
              </xsl:otherwise>
            </xsl:choose>
          </com.csc_AdditionalUserArea>
        </PropertyLossInfo>
      </xsl:when>
    </xsl:choose>
   
  </xsl:template>

  <xsl:template name="WCVRow">
    <WorkCompLocInfo id="">
      <SICCd>
        <xsl:value-of select="normalize-space(//SITE__SIC__CODE)"/>
      </SICCd>
      <NAICSCd/>       
      <Location>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_SiteKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_SiteSequenceNum</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SITE__SEQUENCE__NO)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
            <OtherId>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="//KEY__SITE__NUMBER"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
            <OtherId>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value">
                  <xsl:value-of select="$statunit"/>
                </xsl:with-param>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
            <OtherId></OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_SiteNoRowInd</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(@SITE-NO-ROWS-INDIC)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
            <OtherId></OtherId>
          </OtherIdentifier>
        </ItemIdInfo>
        <Addr>
          <AddrTypeCd>MailingAddress</AddrTypeCd>
          <Addr1>
            <xsl:value-of select="normalize-space(//SITE__ADDRESS)"/>
          </Addr1>
          <Addr2/>
          <City>
            <xsl:value-of select="normalize-space(//SITE__CITY)"/>
          </City>
          <StateProvCd>
            <xsl:value-of select="normalize-space(//SITE__STATE)"/>
          </StateProvCd>
          <PostalCode>
            <xsl:value-of select="normalize-space(//SITE__ZIP__CODE)"/>
          </PostalCode>
          <County>
            <xsl:value-of select="normalize-space(//SITE__COUNTY__CODE)"/>
          </County>
        </Addr>
        <CountyTownCd></CountyTownCd>
        <RiskLocationCd></RiskLocationCd>
        <EarthquakeZoneCd></EarthquakeZoneCd>
        <TaxCodeInfo>
          <TaxCd>
            <xsl:value-of select="normalize-space(//SITE__TAX__LOCATION)"/>
          </TaxCd>
          <TaxIdentity>
            <TaxIdTypeCd>FEIN</TaxIdTypeCd>
            <TaxId>
              <xsl:value-of select="normalize-space(//SITE__FEIN)"/>
            </TaxId>
          </TaxIdentity>
          <TaxIdentity>
            <TaxIdTypeCd>SSN</TaxIdTypeCd>
            <TaxId/>
          </TaxIdentity>
          <TaxIdentity>
            <TaxIdTypeCd>Unemployment</TaxIdTypeCd>
            <TaxId>
              <xsl:value-of select="normalize-space(//SITE__UNEMPLOYMENT__ID__NUMBER)"/>
            </TaxId>
          </TaxIdentity>
        </TaxCodeInfo>
        <com.csc_BusinessType>
          <xsl:value-of select="normalize-space(//COM__TYPE__OF__BUSINESS__DESC)"/>
        </com.csc_BusinessType>
        <SubLocation></SubLocation>
        <AdditionalInterest></AdditionalInterest>
        <FireDistrict></FireDistrict>
        <FireDistrictCd></FireDistrictCd>
        <FireStation></FireStation>
        <com.csc_LegalEntity>
          <xsl:value-of select="normalize-space(//SITE__LEGAL__ENTITY)"/>
        </com.csc_LegalEntity>
        <Communications>
          <EmailInfo>
            <xsl:value-of select="normalize-space(//SITE__CONTACT)"/>
          </EmailInfo>
          <PhoneInfo>
            <xsl:value-of select="normalize-space(//SITE__PHONE__NUMBER)"/>
          </PhoneInfo>
          <WebsiteInfo></WebsiteInfo>
        </Communications>
        <LocationName>
          <xsl:value-of select="normalize-space(//SITE__NAME)"/>
        </LocationName>
        <com.csc_OptionalName>
          <xsl:value-of select="normalize-space(//SITE__NAME__OPTIONAL)"/>
        </com.csc_OptionalName>
        <LocationDesc></LocationDesc>
        <CatastropheZoneCd></CatastropheZoneCd>
      </Location>
      <NumEmployees>
        <xsl:value-of select="normalize-space(//SITE__NUMBER__OF__EMPLOYEES)"/>
      </NumEmployees>
      <HighestFloorNumberOccupied/>
      <com.csc_UnitStatusDesc>
        <xsl:value-of select="normalize-space(//COM__STATUS__DESCRIPTION)"/>
      </com.csc_UnitStatusDesc>
      <com.csc_PrimaryClassCd>
        <xsl:value-of select="normalize-space(//SITE__GOVER__CLASS__CODE)"/>
      </com.csc_PrimaryClassCd>
      <com.csc_PrimaryClassDesc>
        <xsl:value-of select="normalize-space(//SITE__GOVER__CLASS__DESC)"/>
      </com.csc_PrimaryClassDesc>
      <com.csc_EmpClassCd>
        <xsl:value-of select="normalize-space(//KEY__EMP__CLASS__CODE)"/>
      </com.csc_EmpClassCd>
      <com.csc_EmpClassDesc>
        <xsl:value-of select="normalize-space(//COM__EMP__CLASS__DESCRIPTION)"/>
      </com.csc_EmpClassDesc>
      <com.csc_AuditorsInfo>
        <com.csc_SiteAuditor>
          <xsl:value-of select="normalize-space(//SITE__AUDITOR)"/>
        </com.csc_SiteAuditor>
        <com.csc_SiteAuditorName>
          <xsl:value-of select="normalize-space(//SITE__AUDITOR__NAME)"/>
        </com.csc_SiteAuditorName>
        <com_csc_SiteAuditor_I>
          <xsl:value-of select="normalize-space(//SITE__AUDITOR__I)"/>
        </com_csc_SiteAuditor_I>
        <com_csc_SiteAuditor_C>
          <xsl:value-of select="normalize-space(//SITE__AUDITOR__C)"/>
        </com_csc_SiteAuditor_C>
        <com_csc_SiteAuditorName_I>
          <xsl:value-of select="normalize-space(//SITE__AUDITOR__NAME__I)"/>
        </com_csc_SiteAuditorName_I>
        <com_csc_SiteAuditorName_C>
          <xsl:value-of select="normalize-space(//SITE__AUDITOR__NAME__C)"/>
        </com_csc_SiteAuditorName_C>
        <com.csc_AuditType>
          <xsl:value-of select="normalize-space(//SITE__AUDIT__TYPE)"/>
        </com.csc_AuditType>
        <com.csc_Audit_Basis>
          <xsl:value-of select="normalize-space(//SITE__AUDIT__BASIS)"/>
        </com.csc_Audit_Basis>
      </com.csc_AuditorsInfo>
      <WorkCompRateClass>
        <ActualRemunerationAmt/>
        <IfAnyRatingBasisInd/>
        <NumEmployeesFullTime/>
        <NumEmployeesPartTime/>
        <Rate/>
        <PremiumBasisCd/>
        <RatingClassificationCd/>
        <Exposure/>
        <RatingClassificationDesc/>
        <RatingClassificationDescCd/>
        <ExposurePeriod/>
        <CommlCoverage/>
        <CreditOrSurcharge/>
        <CurrentTermAmt/>
        <WrittenAmt/>
      </WorkCompRateClass>     
    </WorkCompLocInfo>
  </xsl:template>

  <xsl:template name="InsuredUnitRow">
    <InsuredOrPrincipal>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_PropertyKey</OtherIdTypeCd>
          <OtherId/>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
          <OtherId>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value" select="//KEY__UNIT__NUMBER"/>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
          <OtherId>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value">
                <xsl:value-of select="$statunit"/>
              </xsl:with-param>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
          <OtherId></OtherId>
        </OtherIdentifier>
         <OtherIdentifier>
           <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
                <OtherId>
               </OtherId>
         </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_EntryDate</OtherIdTypeCd>
          <OtherId></OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <PremiumInfo>
        <PremiumAmt>
          <xsl:value-of select="normalize-space(//SA05__UNIT__PREMIUM)"/>
        </PremiumAmt>
      </PremiumInfo>
      <com.csc_InsuredNameIdInfo>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA05__TBPP008__FLDDESC01)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="//KEY__UNIT__NUMBER"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA05__TBPP008__FLDDESC02)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SA05__NAME)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA05__TBPP008__FLDDESC09)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SA05__DESCRIPTION__1)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA05__TBPP008__FLDDESC10)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SA05__DESCRIPTION__2)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA06__TBPP008__FLDDESC03)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SA06__ADDRESS1)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA06__TBPP008__FLDDESC04)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SA06__ADDRESS2)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA06__TBPP008__FLDDESC05)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SA06__CITY)"/>
              <xsl:value-of select="', '"/>
              <xsl:value-of select="normalize-space(//SA06__STATE)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA06__TBPP008__FLDDESC06)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SA06__ZIPCODE)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA06__TBPP008__FLDDESC07)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SA06__CONTACT)"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>
              <xsl:value-of select="normalize-space(//SA06__TBPP008__FLDDESC08)"/>
            </OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="normalize-space(//SA06__AREACODE)"/>
              <xsl:value-of select="'-'"/>
              <xsl:value-of select="normalize-space(//SA06__PHONE3)"/>
              <xsl:value-of select="', '"/>
              <xsl:value-of select="normalize-space(//SA06__PHONE4)"/>
            </OtherId>
          </OtherIdentifier>         
        </ItemIdInfo>
      </com.csc_InsuredNameIdInfo>
    </InsuredOrPrincipal>
  </xsl:template>
  
</xsl:stylesheet>