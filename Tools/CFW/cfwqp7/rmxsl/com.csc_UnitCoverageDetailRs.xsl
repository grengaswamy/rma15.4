<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="DecomposeErrorRecord.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:include href="DecomposeUserArea.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="lob" select="default"/>
  <xsl:param name="issue-cd" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <!--mits 35925 start-->
  <xsl:param name="client-file" select="default"/>
  <!--mits 35925 end-->
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
        <!--mits 35925 start-->
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
        <!--mits 35925 end-->
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <xsl:apply-templates select="//ERROR__LST__ROW"/>
        <com.csc_UnitCoverageDetailRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
          <Policy id=''>
            <PolicyNumber>
              <xsl:value-of select="normalize-space(//KEY__POLICY__NUMBER)"/>
            </PolicyNumber>
            <CompanyProductCd>
              <xsl:value-of select="normalize-space(//KEY__SYMBOL)"/>
            </CompanyProductCd>
            <LOBCd>
              <xsl:value-of select="normalize-space(//BC__LINE__OF__BUSINESS)"/>
            </LOBCd>
            <com.csc_ActionCd></com.csc_ActionCd>
            <com.csc_IssueCd>
              <xsl:value-of select="normalize-space($issue-cd)"/>
            </com.csc_IssueCd>
          </Policy>
          <xsl:choose>
            <xsl:when test="//PERSONAL__COVERAGE__REC__ROW">
              <xsl:call-template name="HP-FPRow"/>
            </xsl:when>
            <xsl:when test="//ALR__VEHICLE__COVERAGE__ROW">
              <xsl:call-template name="APVRow"/>
            </xsl:when>
            <xsl:when test="//ALR__IMPLEMENTED__RECORDS__ROW">
              <xsl:call-template name="CPPRow"/>
            </xsl:when>
            <xsl:when test="//WC__EMP__CLASS__RECORDS__ROW">
              <xsl:call-template name="WCVRow"/>
            </xsl:when>
            <xsl:when test="//PSA15__RECORDS__ROW">
              <xsl:call-template name="StatRow"/>
            </xsl:when>
          </xsl:choose>
        </com.csc_UnitCoverageDetailRs>
    </ClaimsSvcRs>
    </ACORD>
  </xsl:template>
  
  <xsl:template name="HP-FPRow" >
    <com.csc_CoverageLossInfo id="">
      <ActionCd></ActionCd>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__UNIT__NUMBER)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_CoverageSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__COVERAGE__SEQUENCE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_StatCoverageSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//COV__STAT__COVERAGE__SEQUENCE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__INSURANCE__LINE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_LocAddress</OtherIdTypeCd>
          <OtherId></OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_LocCity</OtherIdTypeCd>
          <OtherId></OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//BC__STATE)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <Coverage>
        <CoverageCd>
          <xsl:value-of select="normalize-space(//KEY__COVERAGE)"/>
        </CoverageCd>
        <CoverageDesc></CoverageDesc>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:value-of select="normalize-space(//COV__EXTENDED__COVERAGE__AMNT__1)"/>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>
            <xsl:value-of select="normalize-space(//COV__EXTENDED__COVERAGE__TEXT__1)"/>
          </LimitAppliesToCd>
        </Limit>
        <CurrentTermAmt>
          <xsl:value-of select="normalize-space(//COM__RATE__PREMIUM)"/>
        </CurrentTermAmt>
        <Deductible>
          <FormatCurrencyAmt>
            <Amt/>
            <CurCd/>
          </FormatCurrencyAmt>
          <DeductibleBasisCd/>
          <DeductibleTypeCd/>
          <DeductibleAppliesToCd/>
        </Deductible>
        <EffectiveDt>
          <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
            <xsl:with-param name="datefld" select="//PROC__EFFECTIVE__DATE"/>
          </xsl:call-template>
        </EffectiveDt>
        <ExpirationDt>
        </ExpirationDt>
        <com.csc_StateProvCd/>
        <com.csc_Exposure>
          <xsl:value-of select="normalize-space(//COV__EXPOSURE)"/>
        </com.csc_Exposure>
      </Coverage>
      <com.csc_Subline>
        <xsl:value-of select="normalize-space(//COV__SUBLINE__OF__BUSINESS)"/>  
      </com.csc_Subline>
      <com.csc_IssueCd>
        <xsl:value-of select="normalize-space(//PROC__LDA__ISSUE__CODE)"/>
      </com.csc_IssueCd>
      <com.csc_RecordStatus>
        <xsl:value-of select="normalize-space(//KEY__RECORD__STATUS)"/>
      </com.csc_RecordStatus>
      <com.csc_AmendmentNum>
        <xsl:value-of select="normalize-space(//COM__AMENDMENT__NUMBER)"/>
      </com.csc_AmendmentNum>
      <com.csc_ReasonAmendedDesc/>
      <com.csc_ReasonAmended>
        <xsl:value-of select="normalize-space(//COV__REASON__AMENDED)"/>
      </com.csc_ReasonAmended>
      <com.csc_RateBook>
        <xsl:value-of select="normalize-space(//COM__RATEBOOK__RATEFILE)"/>
      </com.csc_RateBook>
      <com.csc_PercentOfCoinsurance/>
      <com.csc_CancelDt/>
      <com.csc_ChangeDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//COM__LAST__CHANGE__DATE"/>
        </xsl:call-template>
      </com.csc_ChangeDt>
      <com.csc_ChangeEffDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//COM__CHANGE__EFFECTIVE__DATE"/>
        </xsl:call-template>
      </com.csc_ChangeEffDt>
      <com.csc_ManualEntryInd></com.csc_ManualEntryInd>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_DropRecordInd>
        <xsl:value-of select="normalize-space(//COM__DROP__RECORD__INDICATOR)"/>
      </com.csc_DropRecordInd>
      <com.csc_DropRecordDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//COM__DROP__RECORD__DATE"/>
        </xsl:call-template>
      </com.csc_DropRecordDt>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_ReInsuranceInd>
        <xsl:value-of select="normalize-space(//COV__REINSURANCE__INDICATOR)"/>
      </com.csc_ReInsuranceInd>
      <com.csc_BureauCauseOfLoss>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_BureauLossKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_CBLCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <ActionCd/>
        <com.csc_BureauCauseOfLossCd/>
        <com.csc_FinancialInd/>
      </com.csc_BureauCauseOfLoss>
    </com.csc_CoverageLossInfo>
  </xsl:template>

  <xsl:template name="APVRow">
    <com.csc_CoverageLossInfo id="">
      <ActionCd>A</ActionCd>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__UNIT__NUMBER)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_CoverageSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__COVERAGE__SEQUENCE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_StatCoverageSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//COV__STAT__COVERAGE__SEQUENCE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_DisplaySeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//COV__DISPLAY__SEQUENCE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__INSURANCE__LINE)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <Coverage>
        <CoverageCd>
          <xsl:value-of select="normalize-space(//KEY__COVERAGE)"/>
        </CoverageCd>
        <CoverageDes/>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:value-of select="normalize-space(//COV__COVERAGE__LIMIT)"/>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>CoverageLimit</LimitAppliesToCd>
        </Limit>
        <CurrentTermAmt>
          <xsl:value-of select="normalize-space(//COM__RATE__PREMIUM)"/>
        </CurrentTermAmt>
        <Deductible>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:value-of select="normalize-space(//COV__COVERAGE__DEDUCTIBLE)"/>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <DeductibleBasisCd/>
          <DeductibleTypeCd/>
          <DeductibleAppliesToCd/>
        </Deductible>
        <EffectiveDt>
          <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
            <xsl:with-param name="datefld" select="//COM__CHANGE__EFFECTIVE__DATE"/>
          </xsl:call-template>
        </EffectiveDt>
        <ExpirationDt>
        </ExpirationDt>
        <com.csc_StateProvCd/>
        <com.csc_Exposure>
          <xsl:value-of select="normalize-space(//COV__EXPOSURE)"/>
        </com.csc_Exposure>
      </Coverage>
      <com.csc_RecordStatus>
        <xsl:value-of select="normalize-space(//KEY__RECORD__STATUS)"/>
      </com.csc_RecordStatus>
      <com.csc_AmendmentNum>
        <xsl:value-of select="normalize-space(//COM__AMENDMENT__NUMBER)"/>
      </com.csc_AmendmentNum>
      <com.csc_ReasonAmendedDesc/>
      <com.csc_ReasonAmended>
        <xsl:value-of select="normalize-space(//COV__REASON__AMENDED)"/>
      </com.csc_ReasonAmended>
      <com.csc_RateBook>
        <xsl:value-of select="normalize-space(//COM__RATEBOOK__RATEFILE)"/>
      </com.csc_RateBook>
      <com.csc_PercentOfCoinsurance/>
      <com.csc_CancelDt/>
      <com.csc_ChangeDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//COM__LAST__CHANGE__DATE"/>
        </xsl:call-template>
      </com.csc_ChangeDt>
      <com.csc_ManualEntryInd></com.csc_ManualEntryInd>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_DropRecordInd/>
      <com.csc_DropRecordDt/>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_ReInsuranceInd>
        <xsl:value-of select="normalize-space(//COV__REINSURANCE__INDICATOR)"/>
      </com.csc_ReInsuranceInd>
      <com.csc_BureauCauseOfLoss>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_BureauLossKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_CBLCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <ActionCd/>
        <com.csc_BureauCauseOfLossCd/>
        <com.csc_FinancialInd/>
      </com.csc_BureauCauseOfLoss>
    </com.csc_CoverageLossInfo>
  </xsl:template>

  <xsl:template name="CPPRow">
    <com.csc_CoverageLossInfo id="">
      <ActionCd>A</ActionCd>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__UNIT__NUMBER)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_CoverageSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__COVERAGE__SEQUENCE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__PRODUCT)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__RATE__STATE)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <Coverage>
        <CoverageCd>
          <xsl:value-of select="normalize-space(//KEY__COVERAGE)"/>
        </CoverageCd>
        <CoverageDesc/>
        <Limit>
          <FormatCurrencyAmt>
            <Amt/>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>CoverageLimit</LimitAppliesToCd>
        </Limit>
        <CurrentTermAmt>
          <xsl:value-of select="normalize-space(//RATE__PREMIUM)"/>
        </CurrentTermAmt>
        <com.csc_TotalPremium/>
        <Deductible>
          <FormatCurrencyAmt>
            <Amt/>
            <CurCd/>
          </FormatCurrencyAmt>
          <DeductibleBasisCd/>
          <DeductibleTypeCd/>
          <DeductibleAppliesToCd/>
        </Deductible>
        <EffectiveDt>
          <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
            <xsl:with-param name="datefld" select="//CHANGE__EFFECTIVE__DATE"/>
          </xsl:call-template>
        </EffectiveDt>
        <ExpirationDt>
        </ExpirationDt>
        <com.csc_StateProvCd/>
        <com.csc_Exposure/>
      </Coverage>
      <com.csc_IssueCd>
        <xsl:value-of select="normalize-space(//PROC__LDA__ISSUE__CODE)"/>
      </com.csc_IssueCd>
      <com.csc_ClassCd>
        <xsl:value-of select="normalize-space(//ALR__CLASS)"/>
      </com.csc_ClassCd>
      <com.csc_ClassDesc>
        <xsl:value-of select="normalize-space(//ALR__CLASS__DESC)"/>
      </com.csc_ClassDesc>
      <com.csc_RecordStatus>
        <xsl:value-of select="normalize-space(//KEY__RECORD__STATUS)"/>
      </com.csc_RecordStatus>
      <com.csc_ChangeDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//LAST__CHANGE__DATE"/>
        </xsl:call-template>
      </com.csc_ChangeDt>
      <com.csc_AmendmentNum>
        <xsl:value-of select="normalize-space(//AMENDMENT__NUMBER)"/>
      </com.csc_AmendmentNum>
      <com.csc_ReasonAmendedDesc/>
      <com.csc_ReasonAmended>
        <xsl:value-of select="normalize-space(//REASON__AMENDED)"/>
      </com.csc_ReasonAmended>
      <com.csc_RateBook>
        <xsl:value-of select="normalize-space(//RATEBOOK__RATEFILE)"/>
      </com.csc_RateBook>
      <com.csc_PercentOfCoinsurance/>
      <com.csc_CancelDt/>
      <com.csc_ChangeDt/>
      <com.csc_ManualEntryInd></com.csc_ManualEntryInd>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_DropRecordInd>
        <xsl:value-of select="normalize-space(//DROP__RECORD__INDICATOR)"/>
      </com.csc_DropRecordInd>
      <com.csc_DropRecordDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//DROP__RECORD__DATE"/>
        </xsl:call-template>
      </com.csc_DropRecordDt>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_ReInsuranceInd/>
      <com.csc_BureauCauseOfLoss>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_BureauLossKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_CBLCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <ActionCd/>
        <com.csc_BureauCauseOfLossCd/>
        <com.csc_FinancialInd/>
      </com.csc_BureauCauseOfLoss>
      <com.csc_AdditionalUserArea>
        <xsl:call-template name="DecomposeUserArea"/>
      </com.csc_AdditionalUserArea>
    </com.csc_CoverageLossInfo>
  </xsl:template>

  <xsl:template name="WCVRow">
    <com.csc_CoverageLossInfo id="">
      <ActionCd></ActionCd>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__SITE__NUMBER)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_SiteName</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//EMP__SITE__NAME)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_ClassSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__EMP__CLASS__DESCRIPTION__SEQ)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//BC__STATE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RateStateDesc</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//COM__STATE__DESCRIPTION)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <Coverage>
        <CoverageCd>
          <xsl:value-of select="normalize-space(//KEY__EMP__CLASS__CODE)"/>
        </CoverageCd>
        <CoverageDesc>
          <xsl:value-of select="normalize-space(//COM__EMP__CLASS__DESCRIPTION)"/>
        </CoverageDesc>
        <Limit>
          <FormatCurrencyAmt>
            <Amt/>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>CoverageLimit</LimitAppliesToCd>
        </Limit>
        <CurrentTermAmt>
          <xsl:value-of select="normalize-space(//EMP__CLASS__ESTIMATED__PREMIUM)"/>
        </CurrentTermAmt>
        <com.csc_TotalPremium/>
        <Deductible>
          <FormatCurrencyAmt>
            <Amt/>
            <CurCd/>
          </FormatCurrencyAmt>
          <DeductibleBasisCd/>
          <DeductibleTypeCd/>
          <DeductibleAppliesToCd/>
        </Deductible>
        <EffectiveDt/>
        <ExpirationDt/>
        <com.csc_StateProvCd/>
        <com.csc_Exposure/>
        <com.csc_EstimatedPayroll>
          <xsl:value-of select="normalize-space(//EMP__CLASS__PAYROLL__AMOUNT)"/>
        </com.csc_EstimatedPayroll>
        <com.csc_ExpCovCd>
          <xsl:value-of select="normalize-space(//COM__CLASS__TYPE__COVERAGE__CODE)"/>
        </com.csc_ExpCovCd>
      </Coverage>
      <com.csc_IssueCd>
        <xsl:value-of select="normalize-space(//PROC__LDA__ISSUE__CODE)"/>
      </com.csc_IssueCd>
      <com.csc_RecordStatus>
        <xsl:value-of select="normalize-space(//COM__STATUS__DESCRIPTION)"/>
      </com.csc_RecordStatus>
      <com.csc_AmendmentNum/>
      <com.csc_ReasonAmendedDesc/>
      <com.csc_ReasonAmended/>
      <com.csc_RateBook/>
      <com.csc_PercentOfCoinsurance/>
      <com.csc_CancelDt/>
      <com.csc_ChangeDt/>
      <com.csc_ManualEntryInd></com.csc_ManualEntryInd>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_DropRecordInd/>
      <com.csc_DropRecordDt/>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_ReInsuranceInd/>
      <com.csc_VoluntaryCompInd>
        <xsl:value-of select="normalize-space(//KEY__EMP__CLASS__V__C__INDICATOR)"/>
      </com.csc_VoluntaryCompInd>
      <com.csc_Rate>
        <xsl:value-of select="normalize-space(//EMP__CLASS__ARD__RATE)"/>
      </com.csc_Rate>
      <com.csc_RateOverideInd>
        <xsl:value-of select="normalize-space(//EMP__CLASS__ARD__RATE__OVR__IND)"/>
      </com.csc_RateOverideInd>
      <com.csc_SplitRate>
        <xsl:value-of select="normalize-space(//EMP__CLASS__ARD__PLUS__12__RATE)"/>
      </com.csc_SplitRate>
      <com.csc_SplitRateOverideInd>
        <xsl:value-of select="normalize-space(//EMP__CLASS__ARD12__RATE__OVR__IND)"/>
      </com.csc_SplitRateOverideInd>
      <com.csc_BureauCauseOfLoss>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_BureauLossKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_CBLCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <ActionCd/>
        <com.csc_BureauCauseOfLossCd/>
        <com.csc_FinancialInd/>
      </com.csc_BureauCauseOfLoss>
      <com.csc_AcctDt/>
    </com.csc_CoverageLossInfo>
  </xsl:template>

  <xsl:template name="StatRow">
    <com.csc_CoverageLossInfo id="">
      <ActionCd>A</ActionCd>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__UNIT__NUMBER)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_CoverageSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__COVG__SEQ__NO)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_TrxnSeq</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//KEY__TRANS__SEQ__NO)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//SA15__STATE)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <Coverage>
        <CoverageCd>
          <xsl:value-of select="normalize-space(//SA15__MAJPERIL)"/>
        </CoverageCd>
        <CoverageDesc>
          <xsl:value-of select="normalize-space(//FMT15__COVRGEDESC)"/>
        </CoverageDesc>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:value-of select="normalize-space(//SA15__LIMITOCCUR)"/>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>OccurenceLimit</LimitAppliesToCd>
        </Limit>
        <Limit>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:value-of select="normalize-space(//SA15__LIMITAGGER)"/>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <LimitBasisCd/>
          <ValuationCd/>
          <LimitAppliesToCd>AggregateLimit</LimitAppliesToCd>
        </Limit>
        <CurrentTermAmt>
          <xsl:value-of select="normalize-space(//SA15__TOTALORIG)"/>
        </CurrentTermAmt>
        <com.csc_OriginalPremium>
          <xsl:value-of select="normalize-space(//SA15__ORIGINAL)"/>
        </com.csc_OriginalPremium>
        <com.csc_WrittenPremium>
          <xsl:value-of select="normalize-space(//SA15__PREMIUM)"/>
        </com.csc_WrittenPremium>
        <com.csc_TotalPremium>
          <xsl:value-of select="normalize-space(//SA15__TOTALPREM)"/>
        </com.csc_TotalPremium>
        <Deductible>
          <FormatCurrencyAmt>
            <Amt>
              <xsl:value-of select="normalize-space(//SA15__DEDUCTIBLE)"/>
            </Amt>
            <CurCd/>
          </FormatCurrencyAmt>
          <DeductibleBasisCd/>
          <DeductibleTypeCd/>
          <DeductibleAppliesToCd/>
        </Deductible>
        <EffectiveDt>
          <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
            <xsl:with-param name="datefld" select="//SA15__COVEFFDTE"/>
          </xsl:call-template>
        </EffectiveDt>
        <ExpirationDt>
          <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
            <xsl:with-param name="datefld" select="//SA15__COVEXPDTE"/>
          </xsl:call-template>
        </ExpirationDt>
        <com.csc_StateProvCd/>
        <com.csc_Exposure>
          <xsl:value-of select="normalize-space(//SA15__EXPOSURE)"/>
        </com.csc_Exposure>
        <com.csc_Commission>
          <xsl:value-of select="normalize-space(//SA15__COMMISSION)"/>
        </com.csc_Commission>
        <com.csc_CommissionAmt/>
        <TerritoryCd>
          <xsl:value-of select="normalize-space(//SA15__TERRITORY)"/>
        </TerritoryCd>
      </Coverage>
      <com.csc_IssueCd/>
      <com.csc_ClassCd>
        <xsl:value-of select="normalize-space(//SA15__CLASSNUM)"/>
      </com.csc_ClassCd>
      <com.csc_ClassDesc>
        <xsl:value-of select="normalize-space(//FMT15__CLASSDESC)"/>
      </com.csc_ClassDesc>
      <com.csc_PartCd/>
      <com.csc_RecordStatus>
        <xsl:value-of select="normalize-space(//SA15__COVSTATUS)"/>
      </com.csc_RecordStatus>
      <com.csc_AnnualStatement>
        <xsl:value-of select="normalize-space(//SA15__ASLINE)"/>
      </com.csc_AnnualStatement>
      <com.csc_ProductLine>
        <xsl:value-of select="normalize-space(//SA15__PRODLINE)"/>
      </com.csc_ProductLine>
      <com.csc_SublineCd>
        <xsl:value-of select="normalize-space(//SA15__SUBLINE)"/>
      </com.csc_SublineCd>
      <com.csc_Subline>
        <xsl:value-of select="normalize-space(//FMT15__SUBLINDESC)"/>
      </com.csc_Subline>
      <com.csc_BureauType>
        <xsl:value-of select="normalize-space(//KEY__TYPBUR)"/>
      </com.csc_BureauType>
      <com.csc_TaxLoc>
        <xsl:value-of select="normalize-space(//SA15__TAXLOC)"/>
      </com.csc_TaxLoc>
      <com.csc_ExtendDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//SA15__EXTENDDTE"/>
        </xsl:call-template>
      </com.csc_ExtendDt>
      <com.csc_RetroDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//SA15__RETRODTE"/>
        </xsl:call-template>
      </com.csc_RetroDt>
      <com.csc_ManualEntryInd></com.csc_ManualEntryInd>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_DropRecordInd/>
      <com.csc_TrxnDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//SA15__TRANSDTE"/>
        </xsl:call-template>
      </com.csc_TrxnDt>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_ReInsuranceInd>
        <xsl:value-of select="normalize-space(//SA15__REINSIND)"/>
      </com.csc_ReInsuranceInd>
      <com.csc_EntryDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
          <xsl:with-param name="datefld" select="//SA15__ENTRYDTE"/>
        </xsl:call-template>
      </com.csc_EntryDt>
      <com.csc_AcctDt>
        <xsl:call-template name="cvtdateCYYMMDDtoMMDDYY">
          <xsl:with-param name="datefld" select="//SA15__ACCTGDTE"/>
        </xsl:call-template>
      </com.csc_AcctDt>
      <com.csc_AuditFrequency>
        <xsl:value-of select="normalize-space(//SA15__AUDITFREQ)"/>
      </com.csc_AuditFrequency>
      <com.csc_BillingInd>
        <xsl:value-of select="normalize-space(//SA15__BILLIND)"/>
      </com.csc_BillingInd>
    </com.csc_CoverageLossInfo>
  </xsl:template>
  
</xsl:stylesheet>