<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoYYYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMtoMMYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="rq-uid" select="default"/>

   <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="$user-id"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="$session-id"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="$app-org"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="$app-name"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="$app-version"/>
        </xsl:with-param>
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="$rq-uid"/>
        </RqUID>
        <com.csc_PolicyDimEvalDateRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
		  <com.csc_ClmNumber>
			<xsl:value-of select="//Row/@OUT-CLAIM-NUMBER" />
		  </com.csc_ClmNumber>
		  <com.csc_LastPaymntDate>
			<xsl:call-template name="cvtdateCYYMMDDtoYYYYMMDD">
				<xsl:with-param name="datefld" select="//Row/@OUT-PAYMENT-DATE"/>
			</xsl:call-template> 
		  </com.csc_LastPaymntDate>
		  <com.csc_DateOfLoss>
			<xsl:call-template name="cvtdateCYYMMDDtoYYYYMMDD">
				<xsl:with-param name="datefld" select="//Row/@OUT-LOSS-DATE"/>
			</xsl:call-template> 
		  </com.csc_DateOfLoss>
		  <com.csc_PolicyInceptionDate>
			<xsl:call-template name="cvtdateCYYMMtoYYYYMM">
				<xsl:with-param name="datefld" select="//Row/@OUT-POLICY-INCEPT"/>
			</xsl:call-template> 
		  </com.csc_PolicyInceptionDate>
        </com.csc_PolicyDimEvalDateRs>
    </ClaimsSvcRs>
    </ACORD>
  </xsl:template>
 </xsl:stylesheet>