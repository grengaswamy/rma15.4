<!-- 
"This sample program is provided AS IS and may be used, executed, copied and modified without royalty payment by customer (a) for its own instruction and study, (b) in order to develop applications designed to run with an IBM WebSphere product, either for customer's own internal use or for redistribution by customer, as part of such an application, in customer's own products."

Product 5630-A36,  (C) COPYRIGHT International Business Machines Corp., 2001, 2002
All Rights Reserved * Licensed Materials - Property of IBM
--> 
<%@ page import="java.io.*, java.lang.reflect.*" %>
<%@ page isErrorPage="true" %>

<%
    boolean htmlOutput = true;
%>

<%@ include file="WEB-INF/jspf/errorInfo.jspf" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="DC.LANGUAGE" scheme="rfc1766"/>
</head>
<body marginwidth="0" leftmargin="0" bgcolor="ffffff">

<table width="90%">
  <tbody>
    <tr>
      <td width="2%"></td>
      <td width="98%">
      <hr />
      </td>
    </tr>
    <tr>
      <td bgcolor="#e7e4e7" rowspan="4"></td>
      <td><font color="#000000" size="+2">An Error has occured during Communications Framework processing</font>.</td>
    </tr>
    <tr>
      <td>
<%

      //output is all done here. 

      out.print("<br /><b>Processing request:</b> ");
      out.println(url);      
      out.print("<br /><b>StatusCode:</b> ");
      out.println(status_code);
      out.println("<br /><b>Message:</b> ");
      out.println(message);
      out.print("<br /><b>Exception:</b> ");
      out.println(exception_info);

%>
     </td>
    </tr>
    <tr>
      <td align="left"><br />Please Check the application server log files for details...</td>
    </tr>
    <tr>
      <td>
      <hr />
      </td>
    </tr>
  </tbody>
</table>
</body>
</html>
