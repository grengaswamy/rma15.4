<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2LocationRS" match="/">
		<xsl:for-each select="/*/PMDNXB1__SUB__LOC__RATING__SEG">

			<xsl:for-each select="PMDNXB1__SEGMENT__KEY/PMDNXB1__LEVEL__KEY/PMDNXB1__SUB__LOCATION__NUMBER__A">
				<xsl:variable name="UY-LOCNUM" select="string(number(../PMDNXB1__LOCATION__NUMBER__A/PMDNXB1__LOCATION__NUMBER))"/>
				<xsl:variable name="UY-SUBLOCNUM" select="string(number(PMDNXB1__SUB__LOCATION__NUMBER))"/>
				<Location>
					<xsl:attribute name="id">
						<xsl:value-of select="concat('l', $UY-LOCNUM)"/>
					</xsl:attribute>
					<ItemIdInfo>
						<InsurerId>
							<xsl:value-of select="$UY-LOCNUM"/>
						</InsurerId>
					</ItemIdInfo>
					<Addr>
						<AddrTypeCd/>
						<Addr1/>
						<Addr2/>
						<City/>
						<StateProvCd/>
						<PostalCode/>
						<Country/>
						<County/>
					</Addr>
					<SubLocation>
						<xsl:attribute name="id">
							<xsl:value-of select="concat('s', $UY-SUBLOCNUM)"/>
						</xsl:attribute>
						<ItemIdInfo>
							<InsurerId>
								<xsl:value-of select="$UY-SUBLOCNUM"/>
							</InsurerId>
						</ItemIdInfo>
						<SubLocationDesc/>
						<Addr>
							<AddrTypeCd/>
							<Addr1/>
							<Addr2/>
							<Addr3/>
							<Addr4/>
							<City/>
							<StateProvCd/>
							<PostalCode/>
							<Country/>
							<County/>
						</Addr>
					</SubLocation>
				</Location>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="pmdnxb1Record.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->