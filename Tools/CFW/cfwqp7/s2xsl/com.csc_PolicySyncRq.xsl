<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
<!--02 Segment of 901 file-->
<xsl:include href="CommonTplRq.xsl"/>
<xsl:include href="CommandAreaRecord.xsl"/>

<xsl:template match="/">
<com.csc_PolicySyncRq>
 <!--Build the XSL record for Policy Sync Request Segment-->
	<xsl:call-template name ="CommandAreaRecord" />
</com.csc_PolicySyncRq>
</xsl:template>
</xsl:stylesheet><!-- Stylesheet edited using Stylus Studio - (c)1998-2002 eXcelon Corp. -->