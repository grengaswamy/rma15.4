<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

<!--
*************************************************************************************************
This XSL stylesheet is used by CFW to transform Series II (Series2) XML into ACORD XML to
populate the iSolutions database.
Created by E-Service Case 34768.
*************************************************************************************************
-->
	<!--Common Include Files-->
	<xsl:include href="S2CommonFuncRs.xsl"/>
	<xsl:include href="S2SignOnRs.xsl"/>
	<xsl:include href="S2ProducerRs.xsl"/>
	<xsl:include href="S2InsuredOrPrincipalRs.xsl"/>
	<xsl:include href="S2AdditionalInterestRs.xsl"/>
        <xsl:include href="S2CreditSurchargeRs.xsl"/> 

	<!--BA, WC and BOP Common Include Files-->
	<xsl:include href="S2CommlPolicyRs.xsl"/>

	<!--HO, WC and BOP Common Include Files-->
	<xsl:include href="S2LocationRs.xsl"/>

	<!--Worker's Comp Include Files-->
	<xsl:include href="S2WCWorkSTATERs.xsl"/>
	<xsl:include href="S2WCCommlCoverage12GPRs.xsl"/>
	<xsl:include href="S2MessageStatusRs.xsl"/>

	<xsl:template match="/">
	<xsl:variable name="LastActivityDate" select="com.csc_PolicySyncRs/DESCRIPTION__FULL__SEG/DESCRIPTION__ISOL__WC__TRAILER/DESC__WC__LAST__TRANS__EFF__DATE"/>
	<xsl:variable name="LastLocationNum" select="com.csc_PolicySyncRs/DESCRIPTION__FULL__SEG/DESCRIPTION__ISOL__WC__TRAILER/DESC__WC__LAST__LOCATION__NBR"/>
		<ACORD>
			<xsl:call-template name="BuildSignOn"/>
			<InsuranceSvcRs>
				<RqUID/>
				<com.csc_PolicySyncRs>
					<RqUID/>
					<TransactionResponseDt/>
					<TransactionEffectiveDt/>
					<CurCd>USD</CurCd>
					<xsl:call-template name="BuildMessageStatus">
						<xsl:with-param name="MessageType">
							<xsl:value-of select="string('Policy Sync')"/>
						</xsl:with-param>
					</xsl:call-template>

					<!-- Determine the Line of Business for Response-->
					<xsl:for-each select="com.csc_PolicySyncRs/POLICY__INFORMATION__SEG">
						<xsl:variable name="LOB" select="PIF__LINE__BUSINESS"/>

						<xsl:if test="$LOB='WCP' or $LOB='WCA' or $LOB='WCV' or $LOB='WC ' or $LOB='WC'">
							<!-- WC -->
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Producer"/>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Business">
								<xsl:with-param name="LOB" select="$LOB"/>
							</xsl:apply-templates>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="CommlPolicy">
								<xsl:with-param name="LOB" select="$LOB"/>
								<xsl:with-param name="NextLocation" select="$LastLocationNum + 1"/>
							</xsl:apply-templates>
							<!--Create Location -->
							<xsl:call-template name="CreateLocationFrom43SEG">
								<xsl:with-param name="LOB" select="$LOB"/>
							</xsl:call-template>
							<WorkCompLineBusiness>
								<LOBCd>WCP</LOBCd>

								<!--Create Worker's Comp Rate State -->
								<xsl:call-template name="CreateWorkCompRateState"/>

								<!--Create Worker's Comp Commercial Coverage -->
								<xsl:call-template name="CreateCommlCoverage"/>

							</WorkCompLineBusiness>
							<!-- 55614 Begin -->
			          	<!-- <RemarkText IdRef="policy">Work Comp XML Sync</RemarkText>
			          			<com.csc_LastActivityInfo>
						  	 	<com.csc_LastActivityTypeCd /> 
  								<com.csc_LastActivityDt>
  									<xsl:call-template name="ConvertS2DateToISODate">
										<xsl:with-param name="Value" select="$LastActivityDate"/>
					                    		</xsl:call-template>
  								</com.csc_LastActivityDt>
		  					</com.csc_LastActivityInfo>
			          			<PolicySummaryInfo>Insert Amendment Twin</PolicySummaryInfo> -->
			           <PolicySummaryInfo>
								<FullTermAmt>
									<Amt><xsl:value-of select="/*/PMDGS1__PREMIUM__SUMMARY__SEG/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__TERM__PREM"/></Amt>
								</FullTermAmt>
								<PolicyStatusCd><xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__POLICY__STATUS__ON__PIF"/></PolicyStatusCd>
								<com.csc_LastAcyDt>
								     <xsl:call-template name="ConvertS2DateToISODate">
										<xsl:with-param name="Value" select="$LastActivityDate"/>
					                    		</xsl:call-template>
								</com.csc_LastAcyDt>
								<com.csc_TransAmt>
								    <Amt><xsl:value-of select="/*/PMDGS1__PREMIUM__SUMMARY__SEG/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__PREM__CHANGE"/></Amt>
								</com.csc_TransAmt>								
								<com.csc_ReasonAmendedCd><xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__REASON__AMENDED"/></com.csc_ReasonAmendedCd>
							</PolicySummaryInfo>			
			          <!-- 55614 Begin -->		
						</xsl:if>
					</xsl:for-each>
				</com.csc_PolicySyncRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet>