<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" omit-xml-declaration="yes" indent="yes"/>
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->
	<xsl:template name="CreditSurcharge">
		<xsl:param name="LOB"/>
		<xsl:param name="DwellInd"/>
           <!-- For all lines that may have GP usecode -->
                <xsl:for-each select="/*/DESCRIPTION__FULL__SEG">
                <xsl:if test="DESCRIPTION__KEY/DESCRIPTION__USE/USE__CODE ='GP'">

                                    <xsl:if test="$LOB='APV' or $LOB='HP'">
					<com.csc_CreditOrSurcharge>
                                   
						<CreditSurchargeCd><xsl:value-of select="DESCRIPTION__NEW__GROUP__DATA/DESCRIPTION__NEW__GROUP__CODE"/></CreditSurchargeCd>
						<NumericValue>
							<FormatModFactor>
								<xsl:value-of select="DESCRIPTION__NEW__GROUP__DATA/DESC__NEW__GROUP__DISC__PERCENT"/>
							</FormatModFactor>
						</NumericValue>
						<SecondaryCd>
							<xsl:value-of select="DESCRIPTION__NEW__GROUP__DATA/DESCRIPTION__NEW__GROUP__DESC"/>
						</SecondaryCd>
                                     
					</com.csc_CreditOrSurcharge>
                                     </xsl:if>

                                    <xsl:if test="$LOB='ACV' or $LOB='WCP' or $LOB='BOP' or $LOB='AFV'">	<!-- Issue 80442 added test for AFV -->
					<CreditOrSurcharge>
                                    
						<CreditSurchargeCd><xsl:value-of select="DESCRIPTION__NEW__GROUP__DATA/DESCRIPTION__NEW__GROUP__CODE"/></CreditSurchargeCd>
						<NumericValue>
							<FormatModFactor>
								<xsl:value-of select="DESCRIPTION__NEW__GROUP__DATA/DESC__NEW__GROUP__DISC__PERCENT"/>
							</FormatModFactor>
						</NumericValue>
						<SecondaryCd>
							<xsl:value-of select="DESCRIPTION__NEW__GROUP__DATA/DESCRIPTION__NEW__GROUP__DESC"/>
						</SecondaryCd>
                                   
					</CreditOrSurcharge>
                                     </xsl:if>
                </xsl:if>
                </xsl:for-each>

	</xsl:template>
</xsl:stylesheet>