<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform iSolutions data into Series II record format.
E-Service case 34768 
***********************************************************************************************
-->

<xsl:template name="S2StateRatingRecTemplate">
	<xsl:param name="StateRecType"/>
	<xsl:param name="StateCd"/>

	<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMDI4W1__STATE__RATING__REC</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMDI4W1__STATE__RATING__REC>
				<PMDI4W1__SEGMENT__KEY>
					<PMDI4W1__REC__LLBB>
						<PMDI4W1__REC__LENGTH/>
						<PMDI4W1__ACTION__CODE/>
						<PMDI4W1__FILE__ID/>
					</PMDI4W1__REC__LLBB>
					<PMDI4W1__SEGMENT__ID>43</PMDI4W1__SEGMENT__ID>
					<PMDI4W1__SEGMENT__STATUS>A</PMDI4W1__SEGMENT__STATUS>
					<PMDI4W1__TRANSACTION__DATE>
						<PMDI4W1__YEAR__TRANSACTION>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDI4W1__YEAR__TRANSACTION>
						<PMDI4W1__MONTH__TRANSACTION>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDI4W1__MONTH__TRANSACTION>
						<PMDI4W1__DAY__TRANSACTION>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDI4W1__DAY__TRANSACTION>
					</PMDI4W1__TRANSACTION__DATE>
					<PMDI4W1__SEGMENT__ID__KEY>
						<PMDI4W1__SEGMENT__LEVEL__CODE>I</PMDI4W1__SEGMENT__LEVEL__CODE>
						<PMDI4W1__SEGMENT__PART__CODE>X</PMDI4W1__SEGMENT__PART__CODE>
						<PMDI4W1__SUB__PART__CODE>1</PMDI4W1__SUB__PART__CODE>
						<PMDI4W1__INSURANCE__LINE>WC</PMDI4W1__INSURANCE__LINE>
					</PMDI4W1__SEGMENT__ID__KEY>
					<PMDI4W1__LEVEL__KEY>
						<PMDI4W1__WC__RATING__STATE>
							<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
								<xsl:with-param name="Value" select="$StateCd"/>
   							</xsl:call-template>
						</PMDI4W1__WC__RATING__STATE>
						<PMDI4W1__LOCATION__NUMBER>0000</PMDI4W1__LOCATION__NUMBER>
						<PMDI4W1__RISK__UNIT__GROUP>
							<xsl:choose>
								<xsl:when test="$StateRecType='OPTMOD'">
									<PMDI4W1__CLASS__ORDER__CODE>
										<xsl:value-of select="substring(CreditSurchargeCd, 1, 4)"/>
									</PMDI4W1__CLASS__ORDER__CODE>
									<PMDI4W1__CLASS__ORDER__SEQ>
										<xsl:value-of select="substring(CreditSurchargeCd, 5, 2)"/>
									</PMDI4W1__CLASS__ORDER__SEQ>
								</xsl:when>
								<xsl:otherwise>
									<PMDI4W1__CLASS__ORDER__CODE>0000</PMDI4W1__CLASS__ORDER__CODE>
									<PMDI4W1__CLASS__ORDER__SEQ>01</PMDI4W1__CLASS__ORDER__SEQ>
								</xsl:otherwise>
							</xsl:choose>
						</PMDI4W1__RISK__UNIT__GROUP>
						<PMDI4W1__RISK__UNIT>
							<xsl:choose>
								<xsl:when test="$StateRecType='NONPREM'">
									<PMDI4W1__REPORTING__CLASS__CODE>0000</PMDI4W1__REPORTING__CLASS__CODE>
								</xsl:when>
								<xsl:otherwise>
									<PMDI4W1__REPORTING__CLASS__CODE>
										<xsl:value-of select="substring(SecondaryCd,1,4)"/>
									</PMDI4W1__REPORTING__CLASS__CODE>
								</xsl:otherwise>
							</xsl:choose>
							<PMDI4W1__REPORTING__CLASS__SEQ>01</PMDI4W1__REPORTING__CLASS__SEQ>
						</PMDI4W1__RISK__UNIT>
						<PMDI4W1__SPLIT__RATE__SEQ>00</PMDI4W1__SPLIT__RATE__SEQ>
					</PMDI4W1__LEVEL__KEY>
					<PMDI4W1__ITEM__EFFECTIVE__DATE>
						<PMDI4W1__YEAR__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDI4W1__YEAR__ITEM__EFFECTIVE>
						<PMDI4W1__MONTH__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDI4W1__MONTH__ITEM__EFFECTIVE>
						<PMDI4W1__DAY__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDI4W1__DAY__ITEM__EFFECTIVE>
					</PMDI4W1__ITEM__EFFECTIVE__DATE>
					<PMDI4W1__VARIABLE__KEY>
						<PMDI4W1__AUDIT__NUMBER>00</PMDI4W1__AUDIT__NUMBER>
						<PMDI4W1__AUDIT__NUM__SEQ>0</PMDI4W1__AUDIT__NUM__SEQ>
						<fill_1/>		<!-- Filler length of 3 -->
					</PMDI4W1__VARIABLE__KEY>
					<PMDI4W1__PROCESS__DATE>
						<PMDI4W1__YEAR__PROCESS>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDI4W1__YEAR__PROCESS>
						<PMDI4W1__MONTH__PROCESS>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDI4W1__MONTH__PROCESS>
						<PMDI4W1__DAY__PROCESS>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDI4W1__DAY__PROCESS>
					</PMDI4W1__PROCESS__DATE>
				</PMDI4W1__SEGMENT__KEY>
				<PMDI4W1__SEGMENT__DATA>
					<PMDI4W1__ITEM__EXPIRE__DATE>
						<PMDI4W1__YEAR__ITEM__EXPIRE>
							<xsl:value-of select="substring($ExpDt,1,4)"/>
						</PMDI4W1__YEAR__ITEM__EXPIRE>
						<PMDI4W1__MONTH__ITEM__EXPIRE>
							<xsl:value-of select="substring($ExpDt,6,2)"/>
						</PMDI4W1__MONTH__ITEM__EXPIRE>
						<PMDI4W1__DAY__ITEM__EXPIRE>
							<xsl:value-of select="substring($ExpDt,9,2)"/>
						</PMDI4W1__DAY__ITEM__EXPIRE>
					</PMDI4W1__ITEM__EXPIRE__DATE>

			<!-- For a state's first IX record, we will populate the NON__STAT__DATA -->
					<xsl:choose>
						<xsl:when test="$StateRecType='NONPREM'">
							<PMDI4W1__NON__STAT__DATA>
								<PMDI4W1__INTRASTATE__ID__NUM>
									<xsl:value-of select="NCCIIDNumber"/>
								</PMDI4W1__INTRASTATE__ID__NUM>
								<PMDI4W1__UNMOD__STATE__PREM>0000000000</PMDI4W1__UNMOD__STATE__PREM>
								<PMDI4W1__STATE__TERM__PREM>0000000000</PMDI4W1__STATE__TERM__PREM>
								<PMDI4W1__DIVIDEND__PLAN__IND>
									<xsl:value-of select="ParticipatingPlanInd"/>
								</PMDI4W1__DIVIDEND__PLAN__IND>
								<PMDI4W1__AIRCRFT__SEAT__MAX__PRM>00000</PMDI4W1__AIRCRFT__SEAT__MAX__PRM>
								<PMDI4W1__USLH__PERCENT>0000</PMDI4W1__USLH__PERCENT>
								<PMDI4W1__COVERAGE__FLAG/>
								<PMDI4W1__DEPOSIT__PREM__PERCENT>000000</PMDI4W1__DEPOSIT__PREM__PERCENT>
								<PMDI4W1__CLASS__COVERAGE__IND/>
								<PMDI4W1__VOL__COMP__COV__FLAG/>
								<PMDI4W1__FEDERAL__COVERAGE__CODE/>
								<PMDI4W1__HIGHEST__MIN__PREM>00000</PMDI4W1__HIGHEST__MIN__PREM>
								<xsl:choose>
									<xsl:when test="StateProvCd = 'AR'">
										<PMDI4W1__ARKANSAS__DATA>
											<PMDI4W1__A__FILE__NUMBER/>
											<PMDI4W1__B__AR__NUMBER__OF__EMPL>
												<xsl:value-of select="WorkCompLocInfo/NumEmployees"/>
											</PMDI4W1__B__AR__NUMBER__OF__EMPL>
											<fill_4/>		<!-- Filler length of 11 -->
										</PMDI4W1__ARKANSAS__DATA>
									</xsl:when>
									<xsl:otherwise>	<!-- when StateProvCd = 'DC' or 'KY' or 'MI' or 'NH' or all other states -->
										<PMDI4W1__DIST__OF__COL__DATA>
											<PMDI4W1__A__DC__NUMBER__OF__EMPL>
												<xsl:value-of select="WorkCompLocInfo/NumEmployees"/>
											</PMDI4W1__A__DC__NUMBER__OF__EMPL>
											<fill_5/>		<!-- Filler length of 24 -->
										</PMDI4W1__DIST__OF__COL__DATA>
									</xsl:otherwise>
								</xsl:choose>
								<PMDI4W1__NS__AUDIT__SEG__BUILT__IND/>
								<PMDI4W1__NON__STAT__RATE__EXP__DATE>
									<PMDI4W1__YEAR__NON__STAT__RATE__EXP>
										<xsl:value-of select="substring($ExpDt,1,4)"/>
									</PMDI4W1__YEAR__NON__STAT__RATE__EXP>
									<PMDI4W1__MTH__NON__STAT__RATE__EXP>
										<xsl:value-of select="substring($ExpDt,6,2)"/>
									</PMDI4W1__MTH__NON__STAT__RATE__EXP>
									<PMDI4W1__DAY__NON__STAT__RATE__EXP>
										<xsl:value-of select="substring($ExpDt,9,2)"/>
									</PMDI4W1__DAY__NON__STAT__RATE__EXP>
								</PMDI4W1__NON__STAT__RATE__EXP__DATE>
								<PMDI4W1__ANNIVERSARY__RATE__DATE/>
								<PMDI4W1__COVII__PIECES__ERROR__SW/>
								<PMDI4W1__COVII__STD__MINIMUM>00000</PMDI4W1__COVII__STD__MINIMUM>
								<PMDI4W1__COVII__ADM__MINIMUM>00000</PMDI4W1__COVII__ADM__MINIMUM>
								<PMDI4W1__PMS__FUTURE__USE__1/>
								<PMDI4W1__CUSTOMER__FUTURE__USE__1/>
								<PMDI4W1__YR2000__CUST__USE__1/>
							</PMDI4W1__NON__STAT__DATA>
						</xsl:when>

			<!-- For a state's next IX record(s), we will populate the RATING__MODIFIER__DATA using the Optional mods -->
						<xsl:otherwise>
							<PMDI4W1__RATING__MODIFIER__DATA>
								<xsl:choose>
									<xsl:when test="string-length(NumericValue/FormatInteger) = 0">
										<PMDI4W1__GENERATED__SEG__IND>M</PMDI4W1__GENERATED__SEG__IND>
									</xsl:when>
									<xsl:otherwise>
										<PMDI4W1__GENERATED__SEG__IND>
											<xsl:value-of select="NumericValue/FormatInteger"/>
										</PMDI4W1__GENERATED__SEG__IND>
									</xsl:otherwise>
								</xsl:choose>
								<PMDI4W1__MODIFIER__DESC>
									<xsl:choose>
										<xsl:when test="string-length(SecondaryCd) &gt; 13">
											<xsl:value-of select="substring(SecondaryCd,5,10)"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="substring(SecondaryCd,5,(string-length(SecondaryCd) - 4))"/>
										</xsl:otherwise>
									</xsl:choose>
								</PMDI4W1__MODIFIER__DESC>
								<xsl:choose>
									<xsl:when test="substring(CreditSurchargeCd, 1, 3) = 'TAX'">
										<PMDI4W1__MODIFIER__RATE>00000</PMDI4W1__MODIFIER__RATE>
									</xsl:when>
									<xsl:otherwise>
										<PMDI4W1__MODIFIER__RATE>
											<xsl:value-of select="NumericValue/FormatModFactor"/>
										</PMDI4W1__MODIFIER__RATE>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:choose>
									<xsl:when test="NumericValue/FormatModFactor = '0' or ''">
										<PMDI4W1__MOD__FACTOR__MG__IND>G</PMDI4W1__MOD__FACTOR__MG__IND>
									</xsl:when>
									<xsl:otherwise>
										<PMDI4W1__MOD__FACTOR__MG__IND>M</PMDI4W1__MOD__FACTOR__MG__IND>
									</xsl:otherwise>
								</xsl:choose>
								<PMDI4W1__MODIFIER__PREMIUM>
										<xsl:call-template name="FormatData">
											<xsl:with-param name="FieldName">MODIFIER__PREMIUM</xsl:with-param>
											<xsl:with-param name="FieldLength">9</xsl:with-param>
											<xsl:with-param name="Value" select="NumericValue/FormatCurrencyAmt/Amt"/>
											<xsl:with-param name="FieldType">N</xsl:with-param>
											<xsl:with-param name="NumberMask">000000000</xsl:with-param>
										</xsl:call-template>
								</PMDI4W1__MODIFIER__PREMIUM>
								<xsl:choose>
									<xsl:when test="NumericValue/FormatCurrencyAmt/Amt &gt; '0'">
										<PMDI4W1__MOD__PREM__MG__IND>M</PMDI4W1__MOD__PREM__MG__IND>
									</xsl:when>
									<xsl:otherwise>
										<PMDI4W1__MOD__PREM__MG__IND>G</PMDI4W1__MOD__PREM__MG__IND>
									</xsl:otherwise>
								</xsl:choose>
								<PMDI4W1__MODIFIER__TYPE>
									<PMDI4W1__MODIFIER__TYPE__1>
										<xsl:choose>
											<xsl:when test="substring(CreditSurchargeCd, 1, 6) = '001001'">
												<xsl:value-of select="string('T')"/>
											</xsl:when>
											<xsl:when test="substring(CreditSurchargeCd, 1, 6) = '001002'">
												<xsl:value-of select="string('F')"/>
											</xsl:when>
											<xsl:when test="substring(CreditSurchargeCd, 1, 6) = '034001'">
												<xsl:value-of select="string('T')"/>
											</xsl:when>
											<xsl:when test="substring(CreditSurchargeCd, 1, 6) = '034002'">
												<xsl:value-of select="string('F')"/>
											</xsl:when>
											<xsl:when test="substring(CreditSurchargeCd, 1, 4) = '0330'">
												<xsl:value-of select="string('A')"/>
											</xsl:when>
											<xsl:when test="substring(CreditSurchargeCd, 1, 6) = '014001'">
												<xsl:value-of select="string('B')"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="string(' ')"/>
											</xsl:otherwise>
										</xsl:choose>
									</PMDI4W1__MODIFIER__TYPE__1>
									<PMDI4W1__MODIFIER__TYPE__2></PMDI4W1__MODIFIER__TYPE__2>
								</PMDI4W1__MODIFIER__TYPE>
								<PMDI4W1__MODIFIER__PREM__BASIS>000000</PMDI4W1__MODIFIER__PREM__BASIS>
								<PMDI4W1__MOD__BASIS__MG__IND/>
								<PMDI4W1__MODIFIER__MIN__PREM>00000</PMDI4W1__MODIFIER__MIN__PREM>
								<PMDI4W1__MOD__MIN__PREM__MG__IND/>
								<PMDI4W1__DEPOSIT__PREMIUM>0000000000</PMDI4W1__DEPOSIT__PREMIUM>
								<PMDI4W1__MODIFIER__USLH__FACTOR>00000</PMDI4W1__MODIFIER__USLH__FACTOR>
								<PMDI4W1__AUDIT__SEG__BUILT__IND/>
								<PMDI4W1__RATING__EXPIRE__DATE>
									<PMDI4W1__YEAR__RATING__EXPIRE>
										<xsl:value-of select="substring($ExpDt,1,4)"/>
									</PMDI4W1__YEAR__RATING__EXPIRE>
									<PMDI4W1__MONTH__RATING__EXPIRE>
										<xsl:value-of select="substring($ExpDt,6,2)"/>
									</PMDI4W1__MONTH__RATING__EXPIRE>
									<PMDI4W1__DAY__RATING__EXPIRE>
										<xsl:value-of select="substring($ExpDt,9,2)"/>
									</PMDI4W1__DAY__RATING__EXPIRE>
								</PMDI4W1__RATING__EXPIRE__DATE>
								<PMDI4W1__COVII__LIMIT__ID/>
								<PMDI4W1__MOD__ANNIV__RATE__DATE/>
								<PMDI4W1__VAR__DEDUCTIBLE__AMT>0000000</PMDI4W1__VAR__DEDUCTIBLE__AMT>
								<PMDI4W1__VAR__AGGR__DEDUCT__AMT>000000</PMDI4W1__VAR__AGGR__DEDUCT__AMT>
								<PMDI4W1__CA__EMOD__AT__INCEPTION/>
								<PMDI4W1__PMS__FUTURE__USE__2/>
								<PMDI4W1__CUSTOMER__FUTURE__USE__2/>
								<PMDI4W1__YR2000__CUST__USE__2/>
							</PMDI4W1__RATING__MODIFIER__DATA>
						</xsl:otherwise>
					</xsl:choose>
				</PMDI4W1__SEGMENT__DATA>
			</PMDI4W1__STATE__RATING__REC>
	</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>