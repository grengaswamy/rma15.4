<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="Poducer">
		<Producer>
			<ItemIdInfo>
				<AgencyId>
					<xsl:value-of select="PIF__AGENCY__NUMBER"/>
				</AgencyId>
			</ItemIdInfo>
			<GeneralPartyInfo>
				<NameInfo>
					<CommlName>
						<CommercialName/>
						<SupplementaryNameInfo>
							<SupplementaryNameCd/>
							<SupplementaryName/>
						</SupplementaryNameInfo>
					</CommlName>
					<LegalEntityCd/>
				</NameInfo>
				<Addr>
					<AddrTypeCd/>
					<Addr1/>
					<Addr2/>
					<Addr3/>
					<Addr4/>
					<City/>
					<StateProvCd/>
					<PostalCode/>
					<CountryCd/>
				</Addr>
				<Communications>
					<PhoneInfo>
						<PhoneTypeCd/>
						<CommunicationUseCd/>
						<PhoneNumber/>
					</PhoneInfo>
				</Communications>
			</GeneralPartyInfo>
			<ProducerInfo>
				<ContractNumber/>
				<ProducerSubCode>
					<xsl:value-of select="PIF__PRODUCER__CODE"/>
				</ProducerSubCode>
			</ProducerInfo>
		</Producer>
	</xsl:template>

	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="Person">
		<InsuredOrPrincipal>
			<xsl:attribute name="id"/>
			<GeneralPartyInfo>
				<NameInfo>
					<CommlName>
						<CommercialName>
							<xsl:value-of select="PIF__ADDRESS__LINE__1"/>
						</CommercialName>
						<SupplementaryNameInfo>
							<SupplementaryNameCd/>
							<SupplementaryName/>
						</SupplementaryNameInfo>
					</CommlName>
					<LegalEntityCd/>
					<com.csc_LongName>
						<xsl:value-of select="PIF__ADDRESS__LINE__1"/>
					</com.csc_LongName>
				</NameInfo>
				<Addr>
					<AddrTypeCd/>
					<Addr1>
						<xsl:value-of select="PIF__ADDRESS__LINE__2"/>
					</Addr1>
					<Addr2>
						<xsl:if test="PIF__ADDRESS__LINE__4 != ''">
							<xsl:value-of select="PIF__ADDRESS__LINE__3"/>
						</xsl:if>
					</Addr2>
					<Addr3/>
					<Addr4/>
					<xsl:choose>
						<xsl:when test="PIF__ADDRESS__LINE__4 != ''">
							<City>
								<xsl:value-of select="substring-before(PIF__ADDRESS__LINE__4,',')"/>
							</City>
							<StateProvCd>
								<xsl:value-of select="substring-after(PIF__ADDRESS__LINE__4,',')"/>
							</StateProvCd>
						</xsl:when>
						<xsl:when test="PIF__ADDRESS__LINE__3 != '' and PIF__ADDRESS__LINE__4 = ''">
							<City>
								<xsl:value-of select="substring-before(PIF__ADDRESS__LINE__3,',')"/>
							</City>
							<StateProvCd>
								<xsl:value-of select="substring-after(PIF__ADDRESS__LINE__3,',')"/>
							</StateProvCd>
						</xsl:when>
					</xsl:choose>
					<PostalCode>
						<xsl:variable name="PostCd">
							<xsl:if test="string-length(PIF__ZIP__POSTAL__CODE)= '5'">
								<xsl:value-of select="PIF__ZIP__POSTAL__CODE"/>
							</xsl:if>
							<xsl:if test="string-length(PIF__ZIP__POSTAL__CODE)!= '5'">
								<xsl:value-of select="substring(PIF__ZIP__POSTAL__CODE,2,5)"/>
							</xsl:if>
						</xsl:variable>
						<xsl:value-of select="$PostCd"/>
					</PostalCode>
					<CountryCd/>
					<County/>
				</Addr>
				<Communications>
					<PhoneInfo>
						<PhoneTypeCd/>
						<CommunicationUseCd/>
						<PhoneNumber/>
					</PhoneInfo>
				</Communications>
			</GeneralPartyInfo>
			<InsuredOrPrincipalInfo>
				<InsuredOrPrincipalRoleCd>I</InsuredOrPrincipalRoleCd>
			</InsuredOrPrincipalInfo>
		</InsuredOrPrincipal>
	</xsl:template>

	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="Policy">
		<xsl:variable name="TranEffYear" select="substring(concat('0000',PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR), string-length(PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR)+1,4)"/>
		<xsl:variable name="TranEffMonth" select="substring(concat('00',PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO), string-length(PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO)+1,2)"/>
		<xsl:variable name="TranEffDay" select="substring(concat('00',PIF__EFFECTIVE__DATE/PIF__EFF__DA), string-length(PIF__EFFECTIVE__DATE/PIF__EFF__DA)+1,2)"/>
		<xsl:variable name="TranEffDate">
			<xsl:call-template name="ConvertS2DateToISODate">
				<xsl:with-param name="Value" select="concat($TranEffYear,$TranEffMonth,$TranEffDay)"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="TranExpYear" select="substring(concat('0000',PIF__EXPIRATION__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR), string-length(PIF__EXPIRATION__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR)+1,4)"/>
		<xsl:variable name="TranExpMonth" select="substring(concat('00',PIF__EXPIRATION__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO), string-length(PIF__EXPIRATION__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO)+1,2)"/>
		<xsl:variable name="TranExpDay" select="substring(concat('00',PIF__EXPIRATION__DATE/PIF__EFF__DA), string-length(PIF__EXPIRATION__DATE/PIF__EFF__DA)+1,2)"/>
		<xsl:variable name="TranExpDate">
			<xsl:call-template name="ConvertS2DateToISODate">
				<xsl:with-param name="Value" select="concat($TranEffYear,$TranEffMonth,$TranEffDay)"/>
			</xsl:call-template>
		</xsl:variable>

		<CommlPolicy>
			<xsl:attribute name="id">
				<xsl:value-of select="PIF__ISOL__APP__ID"/>
			</xsl:attribute>
			<PolicyNumber>
				<xsl:value-of select="PIF__KEY/PIF__POLICY__NUMBER"/>
			</PolicyNumber>
			<PolicyVersion>
				<xsl:value-of select="PIF__KEY/PIF__MODULE"/>
			</PolicyVersion>
			<CompanyProductCd>
				<xsl:value-of select="normalize-space(PIF__KEY/PIF__SYMBOL)"/>
			</CompanyProductCd>
			<LOBCd>
				<xsl:value-of select="normalize-space(PIF__LINE__BUSINESS)"/>
			</LOBCd>
			<NAICCd>
				<xsl:value-of select="PIF__MASTER__CO__NUMBER"/>
			</NAICCd>
			<ControllingStateProvCd>
				<xsl:call-template name="ConvertNumericStateCdToAlphaStateCd">
					<xsl:with-param name="Value" select="PIF__RISK__STATE__PROV"/>
				</xsl:call-template>
			</ControllingStateProvCd>
			<ContractTerm>
				<EffectiveDt>
					<xsl:value-of select="$TranEffDate"/>
				</EffectiveDt>
				<ExpirationDt>
					<xsl:value-of select="$TranEffDate"/>
				</ExpirationDt>
			</ContractTerm>
			<CurrentTermAmt>
				<Amt>
					<xsl:choose>
						<xsl:when test="normalize-space(//PMDGS1__PREMIUM__SUMMARY__SEG/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__PREM__CHANGE)=''">0</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="/*/PMDGS1__PREMIUM__SUMMARY__SEG/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__PREM__CHANGE"/>
						</xsl:otherwise>
					</xsl:choose>
				</Amt>
			</CurrentTermAmt>
			<BillingMethodCd/>
			<xsl:for-each select="/*/PMD4T__FORMS__SEG">
				<xsl:for-each select="PMD4T__SEGMENT__DATA/PMD4T__FORMS__SECTIONS/PMD4T__FORMS__ENTRIES">
					<Form>
						<FormNumber>
							<xsl:value-of select="./PMD4T__FORM__NO"/>
						</FormNumber>
						<FormName/>
						<FormDesc/>
						<EditionDt>
							<xsl:variable name="EdDtYear" select="substring(concat('0000',./PMD4T__FORM__DATE/PMD4T__FORM__YEAR__A/PMD4T__FORM__YEAR), string-length(./PMD4T__FORM__DATE/PMD4T__FORM__YEAR__A/PMD4T__FORM__YEAR)+1,4)"/>
							<xsl:variable name="EdDtMonth" select="substring(concat('00',./PMD4T__FORM__DATE/PMD4T__FORM__MONTH__A/PMD4T__FORM__MONTH), string-length(./PMD4T__FORM__DATE/PMD4T__FORM__MONTH__A/PMD4T__FORM__MONTH)+1,2)"/>
							<xsl:variable name="EdDtDay">01</xsl:variable>
							<xsl:variable name="EdDate">
								<xsl:call-template name="ConvertS2DateToISODate">
									<xsl:with-param name="Value" select="concat($EdDtYear,$EdDtMonth,$EdDtDay)"/>
								</xsl:call-template>
							</xsl:variable>
						</EditionDt>
						<IterationNumber>
							<xsl:value-of select="position()"/>
						</IterationNumber>
					</Form>
				</xsl:for-each>
			</xsl:for-each>
			<OtherInsuranceWithCompanyCd/>
			<OtherOrPriorPolicy>
				<PolicyCd/>
				<PolicyNumber/>
				<LOBCd/>
				<InsurerName/>
				<ContractTerm>
					<EffectiveDt/>
					<ExpirationDt/>
				</ContractTerm>
				<PolicyAmt>
					<Amt/>
				</PolicyAmt>
			</OtherOrPriorPolicy>
			<PaymentOption>
				<PaymentPlanCd>
					<xsl:value-of select="concat(PIF__PAY__SERVICE__CODE,PIF__MODE__CODE)"/>
				</PaymentPlanCd>
				<MethodPaymentCd/>
				<DepositAmt>
					<Amt>
						<xsl:value-of select="../*/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__DEPOSIT__PREM"/>
					</Amt>
				</DepositAmt>
				<com.csc_BillingPlanCd/>
			</PaymentOption>
			<AccountNumberId>
				<xsl:value-of select="PIF__CUSTOMER__NUMBER"/>
			</AccountNumberId>

			<CommlPolicySupplement>
				<!-- Tax and Surcharge Amount for Policy -->
				<CreditOrSurcharge>
					<CreditSurchargeDt/>
					<CreditSurchargeCd>TAX</CreditSurchargeCd>
					<NumericValue>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:choose>
									<xsl:when test="normalize-space(//PMDGS4__SUR__TAX__FEE__SUMMARY__SEG/PMDGS4__SEGMENT__DATA/PMDGS4__TOTAL__TAX__SURCHARGE__CHG)=''">0</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="/*/PMDGS4__SUR__TAX__FEE__SUMMARY__SEG/PMDGS4__SEGMENT__DATA/PMDGS4__TOTAL__TAX__SURCHARGE__CHG"/>
									</xsl:otherwise>
								</xsl:choose>
							</Amt>
						</FormatCurrencyAmt>
					</NumericValue>
				</CreditOrSurcharge>
				<ReportingPeriodCd/>
				<OperationsDesc/>
				<PolicyTypeCd>BOP</PolicyTypeCd>
				<AuditFrequencyCd>Q</AuditFrequencyCd>
				<com.csc_AuditTypeCd/>
			</CommlPolicySupplement>
			<com.csc_CompanyPolicyProcessingId>
				<xsl:value-of select="substring(concat('00',PIF__LOCATION),number(string-length(PIF__LOCATION))+1,2)"/>
			</com.csc_CompanyPolicyProcessingId>
			<com.csc_InsuranceLineIssuingCompany>
				<xsl:value-of select="substring(concat('00',PIF__COMPANY__NUMBER),number(string-length(PIF__COMPANY__NUMBER))+1,2)"/>
			</com.csc_InsuranceLineIssuingCompany>
			<com.csc_PolicyTermMonths>
				<xsl:value-of select="PIF__INSTALLMENT__TERM"/>
			</com.csc_PolicyTermMonths>
			<com.csc_NextLocationAssignedId/>
		</CommlPolicy>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\SSR Test\4.454148462400003244BORSQuoteS2.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->