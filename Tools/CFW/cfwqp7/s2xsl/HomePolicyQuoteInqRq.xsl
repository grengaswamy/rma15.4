<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform ACORD XML built from the
iSolutions database into Series 2 XML before sending to the the Series 2 server   
E-Service Issue # 35270
***********************************************************************************************-->

	<xsl:include href="CommonTplRq.xsl"/>
	<xsl:include href="S2HOPinfo53.xsl"/>
	<xsl:include href="S2HODesc53.xsl"/>
	<xsl:include href="S2HODesc53Location.xsl"/>
	<xsl:include href="S2HORate53.xsl"/>
	<xsl:include href="S2HOExt53.xsl"/>
	<xsl:include href="S2EPF00.xsl"/> <!-- Segment 00 -->

	
	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
	<xsl:variable name="LOC" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/PersPolicy/com.csc_CompanyPolicyProcessingId"/>
	<xsl:variable name="MCO" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/PersPolicy/NAICCd"/>
	<xsl:variable name="PCO" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/PersPolicy/com.csc_InsuranceLineIssuingCompany"/>
	<xsl:variable name="SYM" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/PersPolicy/CompanyProductCd"/>
	<xsl:variable name="POL" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/PersPolicy/PolicyNumber"/>
	<xsl:variable name="MOD">
		<xsl:call-template name="FormatData">
			<xsl:with-param name="FieldName">$MOD</xsl:with-param>
			<xsl:with-param name="FieldLength">2</xsl:with-param>
			<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/PersPolicy/PolicyVersion"/>
			<xsl:with-param name="FieldType">N</xsl:with-param>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="LOB" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/PersPolicy/LOBCd"/>
	<xsl:variable name="TransEffDt" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/TransactionEffectiveDt"/>
	<xsl:variable name="TransRqDt" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/TransactionRequestDt"/>
	<xsl:variable name="EffDt" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/PersPolicy/ContractTerm/EffectiveDt"/>
	<xsl:variable name="Locationid" select="substring(/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/Location/@id,2,1)"/>
	<xsl:variable name="LocationAddTypeCd" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/Location/Addr/AddrTypeCd"/>
	<xsl:variable name="CoverageCd" select="/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/HomeLineBusiness/Coverage/CoverageCd"/>
	<xsl:variable name="WaterCraftid" select="substring(/ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/HomeLineBusiness/Watercraft/@id,3,1)"/>
	
	<xsl:template match="/"> 
	<xsl:element name="HomePolicyQuoteInqRq">
		<xsl:call-template name="S2HOPinfoTemplate">
			<xsl:with-param name="ProcessingType">Q</xsl:with-param>
			<xsl:with-param name="PolicyMode">
				<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/com.csc_AmendmentMode"/>-->		<!--103409-->
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/PolicyStatusCd"/>				<!--103409-->
			</xsl:with-param>
		</xsl:call-template>
		<xsl:if test="$LocationAddTypeCd != ''">
			<xsl:call-template name="S2HODescLocationTemplate"/>
		</xsl:if>
		<xsl:call-template name="S2HODescTemplate"/>
		<xsl:call-template name="S2HORateTemplate"/>
		<xsl:if test="ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/HomeLineBusiness/Dwell/BldgProtection/ProtectionDeviceBurglarCd &gt;0">
			<xsl:call-template name="S2HOProtectiveDeviceTemplate"/> 
		</xsl:if>
		<xsl:if test="ACORD/InsuranceSvcRq/HomePolicyQuoteInqRq/HomeLineBusiness/Dwell/Coverage[CoverageCd='FVREP']/Option/OptionValue = 'Y'">
			<xsl:call-template name="S2HOReplacementCostTemplate"/> 
		</xsl:if>
		<xsl:call-template name="S2HOExtCovDetTemplate"/>
		<xsl:if test="$WaterCraftid != '' ">
			<xsl:call-template name="S2HOExtCovWaterCraftTemplate"/>
		</xsl:if>
			<xsl:call-template name="S2EPF00Template">   
			<xsl:with-param name="ProcessingType">Q</xsl:with-param>
			<xsl:with-param name="PolicyMode">
				<!--<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/com.csc_AmendmentMode"/>-->		<!--103409-->
				<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/PolicyStatusCd"/>				<!--103409-->
			</xsl:with-param>
		</xsl:call-template>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>