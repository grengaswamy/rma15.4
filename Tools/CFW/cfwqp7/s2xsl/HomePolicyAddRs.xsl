<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series2 XML into ACORD XML
to return back to iSolutions   
E-Service Issue # 35270 
***********************************************************************************************-->
	<xsl:include href="CommonTplRq.xsl"/>
	<xsl:include href="S2HOErrorRs.xsl"/>
	<xsl:include href="S2HOPinfo53Rs.xsl"/>
	<xsl:include href="S2HODesc53Rs.xsl"/>
	<xsl:include href="S2HODesc53LocationRs.xsl"/>
	<xsl:include href="S2HORate53Rs.xsl"/>
	<xsl:include href="S2HOExt53Rs.xsl"/>
	<xsl:include href="S2HOSignOnRs.xsl"/>
  <!--<xsl:include href="S2HOPrem53Rs.xsl"/>-->
	<xsl:include href="S2CommonFuncRs.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="yes" indent="yes"/>
	<xsl:template match="/">
		<xsl:variable name="TranEffYear" select="substring(concat('0000',HomePolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR), string-length(HomePolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR)+1,4)"/>
		<xsl:variable name="TranEffMonth" select="substring(concat('00',HomePolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO), string-length(HomePolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO)+1,2)"/>
		<xsl:variable name="TranEffDay" select="substring(concat('00',HomePolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DA), string-length(HomePolicyAddRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DA)+1,2)"/>
		<xsl:variable name="TranEffDate">
			<xsl:call-template name="ConvertS2DateToISODate">
				<xsl:with-param name="Value" select="concat($TranEffYear,$TranEffMonth,$TranEffDay)"/>
			</xsl:call-template>
		</xsl:variable>
		<ACORD>
			<xsl:call-template name="HOS2BuildSignOn"/>
			<InsuranceSvcRs>
				<RqUID/>
				<HomePolicyAddRs>
					<RqUID/>
					<TransactionResponseDt/>
					<TransactionEffectiveDt>
						<xsl:value-of select="$TranEffDate"/>
					</TransactionEffectiveDt>
					<CurCd>USD</CurCd>
					<xsl:call-template name="S2HOErrorsRsTemplate"/>
					<xsl:apply-templates mode="Poducer" select="/*/POLICY__INFORMATION__SEG"/>
					<InsuredOrPrincipal>
						<xsl:apply-templates mode="Person" select="/*/POLICY__INFORMATION__SEG"/>
					</InsuredOrPrincipal>
					<xsl:apply-templates mode="Policy" select="/*/POLICY__INFORMATION__SEG"/>
					<!--<Location>
						<xsl:variable name="LJ-LOCNUM" select="HomePolicyAddRs/DESCRIPTION__FULL__SEG/DESCRIPTION__KEY/DESCRIPTION__USE/USE__LOCATION"/>
						<xsl:attribute name="id">
							<xsl:value-of select="concat('l',substring($LJ-LOCNUM,3,1))"/>
						</xsl:attribute>
					<ItemIdInfo>
						<InsurerId>
							<xsl:value-of select="$LJ-LOCNUM"/>
						</InsurerId>
					</ItemIdInfo>
						<xsl:apply-templates mode="Location" select="/*/DESCRIPTION__FULL__SEG"/>
					</Location>-->
			<!--Chcek for Location tag -->
					
					<HomeLineBusiness>
						<LOBCd>HP</LOBCd>
						<xsl:call-template name="S2HORateRsTemplate"/>
						<xsl:call-template name="S2HOExtCovDetS2RsTemplate"/>
						<QuestionAnswer>
							<QuestionCd/>
							<YesNoCd/>
							<Explanation/>
						</QuestionAnswer>
					</HomeLineBusiness>
				</HomePolicyAddRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="WaterCraft.xml" userelativepaths="yes" externalpreview="no" url="CFWintermediat Response File.xml" htmlbaseurl="" outputurl="..\WaterCraftRS.xml" processortype="internal" profilemode="0" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/><scenario default="no" name="original" userelativepaths="yes" externalpreview="no" url="..\WaterCraft_ImageXmlFROM CFW.xml" htmlbaseurl="" outputurl="..\WaterCraftRS.xml" processortype="internal" profilemode="0" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="..\WaterCraft.xml" srcSchemaRoot="ACORD" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\Hoext53.xml" destSchemaRoot="BUS__OBJ__RECORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->