<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template name="CreateCACommlCoverage">

		<xsl:for-each select="./SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES">
			<xsl:call-template name="AssignValues">
				<xsl:with-param name="LoopCtr" select="position()-1"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="AssignValues">
		<xsl:param name="CovData"/>
		<xsl:param name="LoopCtr"/>

		<xsl:variable name="CovCode" select="./*[@index=$LoopCtr]/*[contains(name(),'CODE')]"/>
		<xsl:variable name="CovDesc">
			<xsl:call-template name="TranslateCoverageDesc">
				<xsl:with-param name="CoverageCd">
					<xsl:value-of select="./*[@index=$LoopCtr]/*[contains(name(),'CODE')]"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="CovLimit" select="./*[@index=$LoopCtr]/*[contains(name(),'LIMIT')]"/>
		<xsl:variable name="CovPrem" select="./*[@index=$LoopCtr]/*[contains(name(),'PREMIUM')]"/>

		<xsl:if test="string-length($CovCode) &gt; 0">
			<CommlCoverage>
				<CoverageCd>
					<xsl:value-of select="$CovCode"/>
				</CoverageCd>
				<CoverageDesc>
					<xsl:value-of select="$CovDesc"/>
				</CoverageDesc>
				<xsl:if test="($CovCode &gt; 0 and $CovCode &lt; 36) or ($CovCode &gt; 36 and $CovCode &lt; 44) or $CovCode = 45">
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="normalize-space($CovLimit)"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
				</xsl:if>
				<xsl:if test="$CovCode = 36 or $CovCode = 44 or $CovCode = 46 or ($CovCode &gt; 46 and $CovCode &lt; 50)">
					<Deductible>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="concat(normalize-space($CovLimit),'.00')"/>
							</Amt>
						</FormatCurrencyAmt>
						<DeductibleTypeCd/>
					</Deductible>
				</xsl:if>
				<xsl:if test="normalize-space($CovCode) = 'Y'">
					<Option>
						<OptionTypeCd>YNInd1</OptionTypeCd>
						<OptionCd/>
						<OptionValue>1</OptionValue>
					</Option>
				</xsl:if>
				<xsl:if test="normalize-space($CovCode) = 'N'">
					<Option>
						<OptionTypeCd>YNInd1</OptionTypeCd>
						<OptionCd/>
						<OptionValue>0</OptionValue>
					</Option>
				</xsl:if>
				<CurrentTermAmt>
					<Amt>
						<xsl:variable name="TermAmt">
							<xsl:choose>
								<xsl:when test="contains($CovPrem,'{')">
									<xsl:value-of select="translate($CovPrem,'{','0')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="normalize-space($CovPrem)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:value-of select="$TermAmt"/>
					</Amt>
				</CurrentTermAmt>
			</CommlCoverage>
			<xsl:if test="number($LoopCtr) &lt; 15"> 
				<xsl:call-template name="AssignValues">
					<xsl:with-param name="LoopCtr" select="$LoopCtr + 1"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
