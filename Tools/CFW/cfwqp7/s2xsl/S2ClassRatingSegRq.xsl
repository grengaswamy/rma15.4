<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform iSolutions data into Series II record format.
E-Service case 34768 
***********************************************************************************************
-->

<xsl:template name="S2ClassRatingSegTemplate">

	<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMDU4W1__CLASS__RATING__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMDU4W1__CLASS__RATING__SEG>
				<PMDU4W1__SEGMENT__KEY>
					<PMDU4W1__REC__LLBB>
						<PMDU4W1__REC__LENGTH/>
						<PMDU4W1__ACTION__CODE/>
						<PMDU4W1__FILE__ID/>
					</PMDU4W1__REC__LLBB>
					<PMDU4W1__SEGMENT__ID>43</PMDU4W1__SEGMENT__ID>
					<PMDU4W1__SEGMENT__STATUS>A</PMDU4W1__SEGMENT__STATUS>
					<PMDU4W1__TRANSACTION__DATE>
						<PMDU4W1__YEAR__TRANSACTION>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDU4W1__YEAR__TRANSACTION>
						<PMDU4W1__MONTH__TRANSACTION>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDU4W1__MONTH__TRANSACTION>
						<PMDU4W1__DAY__TRANSACTION>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDU4W1__DAY__TRANSACTION>
					</PMDU4W1__TRANSACTION__DATE>
					<PMDU4W1__SEGMENT__ID__KEY>
						<PMDU4W1__SEGMENT__LEVEL__CODE>U</PMDU4W1__SEGMENT__LEVEL__CODE>
						<PMDU4W1__SEGMENT__PART__CODE>X</PMDU4W1__SEGMENT__PART__CODE>
						<PMDU4W1__SUB__PART__CODE>1</PMDU4W1__SUB__PART__CODE>
						<PMDU4W1__INSURANCE__LINE>WC</PMDU4W1__INSURANCE__LINE>
					</PMDU4W1__SEGMENT__ID__KEY>
					<PMDU4W1__LEVEL__KEY>
						<PMDU4W1__WC__RATING__STATE>
							<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
								<xsl:with-param name="Value" select="../../StateProvCd"/>
   							</xsl:call-template>
						</PMDU4W1__WC__RATING__STATE>
						<PMDU4W1__LOCATION__NUMBER>
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">LOCATION</xsl:with-param>
								<xsl:with-param name="FieldLength">4</xsl:with-param>
								<xsl:with-param name="Value" select="substring(../@LocationRef,2,string-length(../@LocationRef)-1)"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
								<xsl:with-param name="NumberMask">0000</xsl:with-param>
							</xsl:call-template>
						</PMDU4W1__LOCATION__NUMBER>
						<fill_0/>
						<PMDU4W1__RISK__UNIT>
							<PMDU4W1__CLASS__CODE>
								<xsl:value-of select="substring(RatingClassificationCd,1,4)"/>
							</PMDU4W1__CLASS__CODE>
							<PMDU4W1__CLASS__CODE__SEQ>
								<!--xsl:value-of select="substring(RatingClassificationCd,5,2)"/  78200 - Class code issue-->
								<xsl:value-of select="substring(RatingClassificationCd,7,2)"/>
							</PMDU4W1__CLASS__CODE__SEQ>
						</PMDU4W1__RISK__UNIT>
						<PMDU4W1__SPLIT__RATE__SEQ>00</PMDU4W1__SPLIT__RATE__SEQ>
					</PMDU4W1__LEVEL__KEY>
					<PMDU4W1__ITEM__EFFECTIVE__DATE>
						<PMDU4W1__YEAR__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDU4W1__YEAR__ITEM__EFFECTIVE>
						<PMDU4W1__MONTH__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDU4W1__MONTH__ITEM__EFFECTIVE>
						<PMDU4W1__DAY__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDU4W1__DAY__ITEM__EFFECTIVE>
					</PMDU4W1__ITEM__EFFECTIVE__DATE>
					<PMDU4W1__VARIABLE__KEY>
						<PMDU4W1__AUDIT__NUMBER>00</PMDU4W1__AUDIT__NUMBER>
						<PMDU4W1__AUDIT__NUM__SEQ>0</PMDU4W1__AUDIT__NUM__SEQ>
						<fill_2/>		<!-- Filler length of 3 -->
					</PMDU4W1__VARIABLE__KEY>
					<PMDU4W1__PROCESS__DATE>
						<PMDU4W1__YEAR__PROCESS>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDU4W1__YEAR__PROCESS>
						<PMDU4W1__MONTH__PROCESS>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDU4W1__MONTH__PROCESS>
						<PMDU4W1__DAY__PROCESS>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDU4W1__DAY__PROCESS>
					</PMDU4W1__PROCESS__DATE>
				</PMDU4W1__SEGMENT__KEY>
				<PMDU4W1__SEGMENT__DATA>
					<PMDU4W1__ITEM__EXPIRE__DATE>
						<PMDU4W1__YEAR__ITEM__EXPIRE>
							<xsl:value-of select="substring($ExpDt,1,4)"/>
						</PMDU4W1__YEAR__ITEM__EXPIRE>
						<PMDU4W1__MONTH__ITEM__EXPIRE>
							<xsl:value-of select="substring($ExpDt,6,2)"/>
						</PMDU4W1__MONTH__ITEM__EXPIRE>
						<PMDU4W1__DAY__ITEM__EXPIRE>
							<xsl:value-of select="substring($ExpDt,9,2)"/>
						</PMDU4W1__DAY__ITEM__EXPIRE>
					</PMDU4W1__ITEM__EXPIRE__DATE>
					<PMDU4W1__CLASS__TABLE__FLAG>N</PMDU4W1__CLASS__TABLE__FLAG>
					<PMDU4W1__CLASS__CODE__SUFFIX>
						<PMDU4W1__CLASS__CODE__SUFFIX__1>
							<xsl:value-of select="RatingClassificationDescCd"/>
						</PMDU4W1__CLASS__CODE__SUFFIX__1>
						<PMDU4W1__CLASS__CODE__SUFFIX__2/>
					</PMDU4W1__CLASS__CODE__SUFFIX>
					<PMDU4W1__PREMIUM__BASIS__TYP>
						<xsl:value-of select="PremiumBasisCd"/>
					</PMDU4W1__PREMIUM__BASIS__TYP>
					<PMDU4W1__PREMIUM__BASIS__AMT>
						<xsl:value-of select="ActualRemunerationAmt/Amt"/>
					</PMDU4W1__PREMIUM__BASIS__AMT>
					<PMDU4W1__GOVERN__CLASS__IND/>
					<PMDU4W1__RATE>00000</PMDU4W1__RATE>
					<PMDU4W1__RATE__MG__IND></PMDU4W1__RATE__MG__IND>
					<PMDU4W1__CLASS__TERM__PREM>000000000</PMDU4W1__CLASS__TERM__PREM>
					<PMDU4W1__CLASS__TERM__PRM__MG__IND></PMDU4W1__CLASS__TERM__PRM__MG__IND>
					<PMDU4W1__CLASS__DESC__IND>
						<!--xsl:value-of select="substring(RatingClassificationCd,5,2)"/ 78200 - Class code issue-->
						<xsl:value-of select="substring(RatingClassificationCd,7,2)"/>
					</PMDU4W1__CLASS__DESC__IND>
					<PMDU4W1__CLASS__CODE__DESCRIPTION>
						<PMDU4W1__CLASS__DESC__1ST__8__BYTES>
								<xsl:value-of select="substring(RatingClassificationDesc, 1, 8)"/>
						</PMDU4W1__CLASS__DESC__1ST__8__BYTES>
						<PMDU4W1__CLASS__DESC__REMAINDER>
								<xsl:value-of select="substring(RatingClassificationDesc, 9, 32)"/>
						</PMDU4W1__CLASS__DESC__REMAINDER>
					</PMDU4W1__CLASS__CODE__DESCRIPTION>
					<PMDU4W1__SIC__CODE/>
					<PMDU4W1__CLASS__DESC__FLAG/>
					<PMDU4W1__MINIMUM__PREMIUM>00000</PMDU4W1__MINIMUM__PREMIUM>
					<PMDU4W1__MIN__PRM__MG__IND>G</PMDU4W1__MIN__PRM__MG__IND>
					<PMDU4W1__SURCHARGE__AMOUNT>000000</PMDU4W1__SURCHARGE__AMOUNT>
					<PMDU4W1__SURCHARGE__TYPE/>
					<PMDU4W1__WAIVER__PCT>000</PMDU4W1__WAIVER__PCT>
					<PMDU4W1__WAIVER__PCT__MG__IND/>
					<PMDU4W1__WAIVER__TYPE>
						<PMDU4W1__WAIVER__TYPE__1/>
						<PMDU4W1__WAIVER__TYPE__2/>
					</PMDU4W1__WAIVER__TYPE>
					<PMDU4W1__WAIV__TYPE__MG__IND/>
					<PMDU4W1__EXPOSURE__COVERAGE__CODE>
						<PMDU4W1__EXPOS__COVERAGE__CODE__1>1</PMDU4W1__EXPOS__COVERAGE__CODE__1>
						<PMDU4W1__EXPOS__COVERAGE__CODE__2>1</PMDU4W1__EXPOS__COVERAGE__CODE__2>
					</PMDU4W1__EXPOSURE__COVERAGE__CODE>
					<PMDU4W1__EXPOS__CODE__MG__IND>G</PMDU4W1__EXPOS__CODE__MG__IND>
					<PMDU4W1__SEASONAL__RISK__IND>N</PMDU4W1__SEASONAL__RISK__IND>
					<PMDU4W1__LOSS__CONSTANT>000</PMDU4W1__LOSS__CONSTANT>
					<PMDU4W1__HAZARD__GROUP/>
					<PMDU4W1__DEPOSIT__PREMIUM>0000000000</PMDU4W1__DEPOSIT__PREMIUM>
					<PMDU4W1__TAX__LOCATION>
						<xsl:variable name="TaxLoc" select="../@LocationRef"/>
						<xsl:value-of select="/ACORD/InsuranceSvcRq/*/Location[@id=$TaxLoc]/TaxCodeInfo/TaxCd"/>
					</PMDU4W1__TAX__LOCATION>
					<PMDU4W1__WAIVER__PREMIUM>0000000000</PMDU4W1__WAIVER__PREMIUM>
					<PMDU4W1__SURCHARGE__PREMIUM>0000000000</PMDU4W1__SURCHARGE__PREMIUM>
					<PMDU4W1__AUDIT__SEG__BUILT__IND/>
					<PMDU4W1__RATING__EXPIRE__DATE>
						<PMDU4W1__YEAR__RATING__EXPIRE>
							<xsl:value-of select="substring($ExpDt,1,4)"/>
						</PMDU4W1__YEAR__RATING__EXPIRE>
						<PMDU4W1__MONTH__RATING__EXPIRE>
							<xsl:value-of select="substring($ExpDt,6,2)"/>
						</PMDU4W1__MONTH__RATING__EXPIRE>
						<PMDU4W1__DAY__RATING__EXPIRE>
							<xsl:value-of select="substring($ExpDt,9,2)"/>
						</PMDU4W1__DAY__RATING__EXPIRE>
					</PMDU4W1__RATING__EXPIRE__DATE>
					<PMDU4W1__MANUALLY__RATED__IND/>
					<PMDU4W1__ANNIVERSARY__RATE__DATE/>
					<PMDU4W1__BUREAU__RATE>00000</PMDU4W1__BUREAU__RATE>
					<PMDU4W1__A__RATED__INDICATOR/>
				</PMDU4W1__SEGMENT__DATA>
				<PMDU4W1__PMS__FUTURE__USE/>
				<PMDU4W1__CUSTOMER__FUTURE__USE/>
				<PMDU4W1__YR2000__CUST__USE/>
			</PMDU4W1__CLASS__RATING__SEG>
	</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>