<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" encoding="UTF-8"/>
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:variable name="pAV" select="'*'"/>
	<xsl:template match="/*/DRIVER__SEG" mode="CreateAccidentViolation">
		<xsl:call-template name="DriverEntries">
			<xsl:with-param name="Entries" select="DRIVER__ATTRIBUTE__AREA/DRIVER__ATTRIBUTES"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="DriverEntries">
		<xsl:param name="Entries"/>
		<xsl:variable name="DrvString">
			<xsl:if test="string-length($Entries) &gt; 0">
				<xsl:choose>
					<xsl:when test="string-length($Entries) = '180'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12),$pAV,substring($Entries,73,12),$pAV,substring($Entries,85,12),$pAV,substring($Entries,97,12),$pAV,substring($Entries,109,12),$pAV,substring($Entries,121,12),$pAV,substring($Entries,133,12),$pAV,substring($Entries,145,12),$pAV,substring($Entries,157,12),$pAV,substring($Entries,169,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '168'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12),$pAV,substring($Entries,73,12),$pAV,substring($Entries,85,12),$pAV,substring($Entries,97,12),$pAV,substring($Entries,109,12),$pAV,substring($Entries,121,12),$pAV,substring($Entries,133,12),$pAV,substring($Entries,145,12),$pAV,substring($Entries,157,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '156'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12),$pAV,substring($Entries,73,12),$pAV,substring($Entries,85,12),$pAV,substring($Entries,97,12),$pAV,substring($Entries,109,12),$pAV,substring($Entries,121,12),$pAV,substring($Entries,133,12),$pAV,substring($Entries,145,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '144'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12),$pAV,substring($Entries,73,12),$pAV,substring($Entries,85,12),$pAV,substring($Entries,97,12),$pAV,substring($Entries,109,12),$pAV,substring($Entries,121,12),$pAV,substring($Entries,133,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '132'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12),$pAV,substring($Entries,73,12),$pAV,substring($Entries,85,12),$pAV,substring($Entries,97,12),$pAV,substring($Entries,109,12),$pAV,substring($Entries,121,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '120'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12),$pAV,substring($Entries,73,12),$pAV,substring($Entries,85,12),$pAV,substring($Entries,97,12),$pAV,substring($Entries,109,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '108'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12),$pAV,substring($Entries,73,12),$pAV,substring($Entries,85,12),$pAV,substring($Entries,97,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '96'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12),$pAV,substring($Entries,73,12),$pAV,substring($Entries,85,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '84'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12),$pAV,substring($Entries,73,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '72'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12),$pAV,substring($Entries,61,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '60'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12),$pAV,substring($Entries,49,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '48'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12),$pAV,substring($Entries,37,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '36'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12),$pAV,substring($Entries,25,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '24'">
						<xsl:value-of select="concat(substring($Entries,1,12),$pAV,substring($Entries,13,12))"/>
					</xsl:when>
					<xsl:when test="string-length($Entries) = '12'">
						<xsl:value-of select="substring($Entries,1,12)"/>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="string-length($DrvString) &gt; 12">
				<xsl:call-template name="AssignVal">
					<xsl:with-param name="str1" select="substring-before($DrvString,$pAV)"/>
					<xsl:with-param name="str2" select="substring-after($DrvString,$pAV)"/>
					<xsl:with-param name="lob" select="/*/POLICY__INFORMATION__SEG/PIF__LINE__BUSINESS"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="string-length($DrvString) = 0">
				<xsl:call-template name="AssignVal">
					<xsl:with-param name="str1"/>
					<xsl:with-param name="str2"/>
					<xsl:with-param name="lob" select="/*/POLICY__INFORMATION__SEG/PIF__LINE__BUSINESS"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="AssignVal">
					<xsl:with-param name="str1" select="$DrvString"/>
					<xsl:with-param name="lob" select="/*/POLICY__INFORMATION__SEG/PIF__LINE__BUSINESS"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="AssignVal">
		<xsl:param name="str1"/>
		<xsl:param name="str2"/>
		<xsl:param name="lob"/>
		<xsl:if test="$lob = 'APV'">
			<xsl:if test="string-length($str1) &gt; 0">
				<AccidentViolation>
					<xsl:attribute name="DriverRef">
						<xsl:value-of select="concat('d',./DRIVER__ID)"/>
					</xsl:attribute>
					<AccidentViolationCd>
						<xsl:value-of select="substring($str1,1,4)"/>
					</AccidentViolationCd>
					<AccidentViolationDt>
						<xsl:value-of select="substring($str1,5,8)"/>
					</AccidentViolationDt>
					<AccidentViolationDesc/>
				</AccidentViolation>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="string-length(normalize-space($str2)) &gt; 12">
					<xsl:call-template name="AssignVal">
						<xsl:with-param name="str1" select="substring-before($str2,$pAV)"/>
						<xsl:with-param name="str2" select="substring-after($str2,$pAV)"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="string-length(normalize-space($str2)) = 12">
					<xsl:call-template name="AssignVal">
						<xsl:with-param name="str1" select="$str2"/>
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->