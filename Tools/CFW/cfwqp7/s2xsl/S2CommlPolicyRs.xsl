<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" encoding="UTF-8"/>
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->

	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="CommlPolicy">
		<xsl:param name="LOB"/>
		<xsl:param name="NextLocation"/>
		<CommlPolicy>
			<!-- Issue 80442 Begin -->
			<xsl:if test="string-length(PIF__ISOL__APP__ID) &gt; 0">     <!-- 55614 -->
			<xsl:attribute name="id">
				<xsl:value-of select="PIF__ISOL__APP__ID"/>
			</xsl:attribute>
			</xsl:if>          <!-- 55614 -->
			<!-- Issue 80442 End -->
			<PolicyNumber>
				<xsl:value-of select="PIF__KEY/PIF__POLICY__NUMBER"/>
			</PolicyNumber>
			<PolicyVersion>
				<xsl:value-of select="PIF__KEY/PIF__MODULE"/>
			</PolicyVersion>
			<CompanyProductCd>
				<!-- Case 34768 Begin -->
				<xsl:value-of select="PIF__KEY/PIF__SYMBOL"/>
				<!-- Case 34768 End -->
			</CompanyProductCd>
			<LOBCd>
				<!-- Issue 80442 Begin -->
				<xsl:choose>
					<xsl:when test="$LOB = 'CA'">
						<xsl:value-of select="$LOB"/>
					</xsl:when>
					<xsl:otherwise>
				<!-- Issue 80442 End -->
				<xsl:value-of select="PIF__LINE__BUSINESS"/>
				<!-- Issue 80442 Begin -->
					</xsl:otherwise>
				</xsl:choose>
				<!-- Issue 80442 End -->
			</LOBCd>
			<!-- Case 40090 Begin -->
			<LOBSubCd>
				<xsl:choose>
					<!-- Issue 80442 Begin -->
					<xsl:when test="$LOB = 'ACV' or $LOB = 'AFV' or $LOB = 'CA'">
						<xsl:value-of select="string('   ')"/>
					</xsl:when>
					<!-- Issue 80442 End -->
					<xsl:when test= "..//PMDL4W1__POLICY__RATING__REC/PMDL4W1__SEGMENT__DATA/PMDL4W1__POLICY__TYPE = 'S'">VOL</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="string('ARP')"/>
					</xsl:otherwise>
				</xsl:choose>
			</LOBSubCd>
			<!-- Case 40090 End -->
			<NAICCd>
				<xsl:value-of select="PIF__MASTER__CO__NUMBER__A"/>
			</NAICCd>
			<!-- Case 34768 Begin: Add Policy Type to determine if Assigned or Voluntary -->
			<!-- <com.csc_PolicyType>
				<xsl:value-of select="..//PMDL4W1__SEGMENT__DATA/PMDL4W1__POLICY__TYPE"/>
			</com.csc_PolicyType> -->
			<!-- Case 34768 End: -->
			<ControllingStateProvCd>
				<xsl:call-template name="BuildStateProvCd">
					<xsl:with-param name="StateCd" select="PIF__RISK__STATE__PROV"/>
				</xsl:call-template>
			</ControllingStateProvCd>
			<ContractTerm>
				<EffectiveDt>
					<!-- <xsl:value-of select="PIF__EFFECTIVE__DATE"/> -->
					<xsl:call-template name="ConvertS2DateToISODate">
						<xsl:with-param name="Value" select="PIF__EFFECTIVE__DATE"/>
					</xsl:call-template>
				</EffectiveDt>
				<ExpirationDt>
					<!-- <xsl:value-of select="PIF__EXPIRATION__DATE"/> -->
					<xsl:call-template name="ConvertS2DateToISODate">
						<xsl:with-param name="Value" select="PIF__EXPIRATION__DATE"/>
					</xsl:call-template>
				</ExpirationDt>
				<DurationPeriod>
					<NumUnits>
						<xsl:value-of select="PIF__INSTALLMENT__TERM"/>
					</NumUnits>
				</DurationPeriod>
			</ContractTerm>
			<BillingAccountNumber>
				<xsl:value-of select="../*/DESCRIPTION__FULL__SEG/DESCRIPTION__ARS__AA__INFO/DESC__AA__ARS__ACCOUNT__NUMBER"/>
			</BillingAccountNumber>
			<BillingMethodCd>
				<xsl:value-of select="/*/DESCRIPTION__FULL__SEG/DESCRIPTION__ARS__AA__INFO/DESC__AA__ARS__BILLING__TYPE"/>
			</BillingMethodCd>
			<CurrentTermAmt>
				<Amt>
					<!-- Issue 80442 Begin -->
					<xsl:choose>
						<xsl:when test="count(//ERROR__DESC__SEG) &gt; 0 and $LOB='CA'">
							<xsl:value-of select="string('0.00')"/>
						</xsl:when>
						<xsl:otherwise>
					<!-- Issue 80442 End -->
					<xsl:value-of select="../*/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__TERM__PREM"/>
					<!-- Issue 80442 Begin -->
						</xsl:otherwise>
					</xsl:choose>
					<!-- Issue 80442 End -->
				</Amt>
				<CurCd>USD</CurCd>
			</CurrentTermAmt>
			<xsl:for-each select="/*/DESCRIPTION__FULL_SEG">
				<xsl:if test="DESCRIPTION__KEY/DESCRIPTION__USE/USE__CODE='GP'">
					<GroupId>
						<xsl:value-of select="./DESCRIPTION__NEW__GROUP__DATA/DESCRIPTION__NEW__GROUP__CODE"/>
					</GroupId>
				</xsl:if>
			</xsl:for-each>
			<OtherInsuranceWithCompanyCd/>
			<PayorCd/>
			<RateEffectiveDt/>
			<RenewalBillingMethodCd>
				<xsl:value-of select="/*/DESCRIPTION__FULL__SEG/DESCRIPTION__ARS__AA__INFO/DESC__AA__ARS__BILLING__TYPE"/>
			</RenewalBillingMethodCd>
			<RenewalPayorCd/>
			<RenewalTerm>
				<EffectiveDt/>
				<ExpirationDt/>
			</RenewalTerm>

			<xsl:if test="$LOB='WCP'">
				<xsl:for-each select="/*/PMD4T__FORMS__SEG">
					<xsl:if test="(PMD4T__SEGMENT__KEY/PMD4T__SEGMENT__ID__KEY/PMD4T__SEGMENT__LEVEL__CODE='I' or PMD4T__SEGMENT__KEY/PMD4T__SEGMENT__ID__KEY/PMD4T__SEGMENT__LEVEL__CODE='L') and PMD4T__SEGMENT__KEY/PMD4T__SEGMENT__ID__KEY/PMD4T__SEGMENT__PART__CODE='T' and PMD4T__SEGMENT__KEY/PMD4T__SEGMENT__ID__KEY/PMD4T__INSURANCE__LINE='WC'">
					   <!-- 55614 Begin -->
						<!--<xsl:for-each select="PMD4T__SEGMENT__DATA/PMD4T__FORMS__SECTIONS/PMD4T__FORMS__ENTRIES[@index]">-->
							<xsl:for-each select="PMD4T__SEGMENT__DATA/PMD4T__FORMS__SECTIONS/PMD4T__FORMS__ENTRIES[string-length(PMD4T__FORM__NO) &gt; 0]">
						<!-- 55614 End -->	
							<Form>
								<FormNumber>
									<xsl:value-of select="./PMD4T__FORM__NO"/>
								</FormNumber>
								<EditionDt>
									<xsl:value-of select="./PMD4T__FORM__DATE"/>
								</EditionDt>
								<com.csc_ActionInfo>
									<ActionCd>
										<xsl:value-of select="./PMD4T__FORM__ACTION"/>
									</ActionCd>
								</com.csc_ActionInfo>
							</Form>
						</xsl:for-each>
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
			<PaymentOption>
				<PaymentPlanCd>
					<!-- xsl:value-of select="/*/DESCRIPTION__FULL__SEG/DESCRIPTION__ARS__AA__INFO/DESC__AA__ARS__BILLING__CLASS"/ -->
					<!-- xsl:value-of select="/*/PIF__PAY__SERVICE__CODE"/ -->
					<xsl:value-of select="concat(//PIF__PAY__SERVICE__CODE, //PIF__MODE__CODE)"/>
				</PaymentPlanCd>
				<xsl:if test="substring(//DESCRIPTION__FULL__SEG/DESCRIPTION__ARS__AA__INFO/DESC__AA__ARS__BILLING__CLASS, 1, 1)='E'">
					<DayMonthDue>
						<xsl:value-of select="/*/DESCRIPTION__FULL__SEG/DESC__YR2000__CUST__USE__RED/DESC__EFT__DEBIT__DAY"/>
					</DayMonthDue>
				</xsl:if>
				<MethodPaymentCd/>
				<NextTermPaymentPlanCd>
					<xsl:value-of select="/*/DESCRIPTION__FULL__SEG/DESCRIPTION__ARS__AA__INFO/DESC__RN__ARS__BILLING__CLASS"/>
				</NextTermPaymentPlanCd>
				<DepositAmt>
					<Amt/>
					<CurCd/>
				</DepositAmt>
				<DownPaymentPct/>
				<FirstPaymentDueDt/>
				<PaymentIntervalCd/>
				<NumPayments>
					<xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__NUMBER__INSTALLMENTS"/>
				</NumPayments>
				<xsl:if test="substring(//DESCRIPTION__FULL__SEG/DESCRIPTION__ARS__AA__INFO/DESC__AA__ARS__BILLING__CLASS, 1, 1)='E'">
					<ElectronicFundsTransfer>
						<FromAcct>
							<AccountNumberId>
								<xsl:value-of select="/*/DESCRIPTION__FULL__SEG/DESC__YR2000__CUST__USE__RED/DESC__EFT__BANK__ACCNT__NUM"/>
							</AccountNumberId>
							<AcctTypeCd>
								<xsl:value-of select="/*/DESCRIPTION__FULL__SEG/DESC__YR2000__CUST__USE__RED/DESC__EFT__BANK__ACCNT__TYPE"/>
							</AcctTypeCd>
							<CreditCardExpirationDt/>
							<BankInfo>
								<BankId>
									<xsl:value-of select="/*/DESCRIPTION__FULL__SEG/DESC__YR2000__CUST__USE__RED/DESC__EFT__BANK__ROUTE__NUM"/>
								</BankId>
								<BranchId/>
							</BankInfo>
							<PayrollDeductionInfo>
								<EmployeeId>
									<xsl:value-of select="/*/DESCRIPTION__FULL__SEG/DESCRIPTION__ARS__AA__INFO/DESC__AA__ARS__ADDITIONAL__ID"/>
								</EmployeeId>
							</PayrollDeductionInfo>
						</FromAcct>
					</ElectronicFundsTransfer>
				</xsl:if>
				<LOBCd/>
				<LOBSubCd/>
				<!--<com.csc_BillingPlanCd/>    55614 -->
			</PaymentOption>
			<AccountNumberId>
				<xsl:value-of select="PIF__CUSTOMER__NUMBER"/>
			</AccountNumberId>
			<CommlPolicySupplement>
			   <NatureBusinessCd><xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__SPECIAL__USE__A"/></NatureBusinessCd>      <!-- 55614 -->
				<LengthTimeInBusiness>
					<NumUnits>
						<xsl:value-of select="/*/UNDGI__SEGMENT/UNDGI__YEARS__IN__BUSINESS"/>
					</NumUnits>
					<UnitMeasurementCd>Years</UnitMeasurementCd>
				</LengthTimeInBusiness>
				<xsl:choose>
					<xsl:when test="/*/POLICY__INFORMATION__SEG/PIF__AUDIT__CODE='A'"><AuditFrequencyCd>AN</AuditFrequencyCd></xsl:when>
					<xsl:when test="/*/POLICY__INFORMATION__SEG/PIF__AUDIT__CODE='M'"><AuditFrequencyCd>MO</AuditFrequencyCd></xsl:when>
					<xsl:when test="/*/POLICY__INFORMATION__SEG/PIF__AUDIT__CODE='Q'"><AuditFrequencyCd>QT</AuditFrequencyCd></xsl:when>
					<xsl:when test="/*/POLICY__INFORMATION__SEG/PIF__AUDIT__CODE='S'"><AuditFrequencyCd>SA</AuditFrequencyCd></xsl:when>
				</xsl:choose>
				<com.csc_AuditTypeCd>
					<xsl:value-of select="//PMD4D__SEGMENT__DATA/PMD4D__TYPE"/>
				</com.csc_AuditTypeCd>
                                <xsl:variable name="DwellInd"/>
                                <xsl:call-template name="CreditSurcharge">
						<xsl:with-param name="POLICY-LOB" select="$LOB"/>
                                                <xsl:with-param name="DwellInd" select="'N'"/>
				</xsl:call-template>    
                   

				<xsl:if test="normalize-space(//UNDGI__SEGMENT/UNDGI__CLAIMS__NAME) != ''">
					<MiscParty>
						<GeneralPartyInfo>
 							<NameInfo>
								<CommlName>
									<xsl:value-of select="/*/UNDGI__SEGMENT/UNDGI__CLAIMS__NAME"/>
								</CommlName>
							</NameInfo>
							<Communications>
								<PhoneInfo>
									<PhoneNumber>
										<xsl:value-of select="/*/UNDGI__SEGMENT/UNDGI__CLAIMS__PHONE"/>
									</PhoneNumber>
								</PhoneInfo>
							</Communications>
						</GeneralPartyInfo>
						<MiscPartyInfo>
							<MiscPartyRoleCd>CC</MiscPartyRoleCd>
						</MiscPartyInfo>
					</MiscParty>
				</xsl:if>
				<xsl:if test="normalize-space(//UNDGI__SEGMENT/UNDGI__ACC__REC__NAME) != ''">
					<MiscParty>
						<GeneralPartyInfo>
							<NameInfo>
								<CommlName>
									<xsl:value-of select="/*/UNDGI__SEGMENT/UNDGI__ACC__REC__NAME"/>
								</CommlName>
							</NameInfo>
							<Communications>
								<PhoneInfo>
									<PhoneNumber>
										<xsl:value-of select="/*/UNDGI__SEGMENT/UNDGI__ACC__REC__PHONE"/>
									</PhoneNumber>
								</PhoneInfo>
							</Communications>
						</GeneralPartyInfo>
						<MiscPartyInfo>
							<MiscPartyRoleCd>A</MiscPartyRoleCd>
						</MiscPartyInfo>
					</MiscParty>
				</xsl:if>
				<xsl:if test="normalize-space(//UNDGI__SEGMENT/UNDGI__INSPECT__NAME) != ''">
					<MiscParty>
						<GeneralPartyInfo>
							<NameInfo>
								<CommlName>
									<xsl:value-of select="/*/UNDGI__SEGMENT/UNDGI__INSPECT__NAME"/>
								</CommlName>
							</NameInfo>
							<Communications>
								<PhoneInfo>
									<PhoneNumber>
										<xsl:value-of select="/*/UNDGI__SEGMENT/UNDGI__INSPECT__PHONE"/>
									</PhoneNumber>
								</PhoneInfo>
							</Communications>
						</GeneralPartyInfo>
						<MiscPartyInfo>
							<MiscPartyRoleCd>InspectionContact</MiscPartyRoleCd>
						</MiscPartyInfo>
					</MiscParty>
				</xsl:if>
				<!-- 55614 Begin -->
              <!--<com.csc_LengthTimeInOccupation>
					<NumUnits>
						<xsl:value-of select="/*/UNDGI__SEGMENT/UNDGI__YEARS__EXPERIENCE"/>
					</NumUnits>
					<UnitMeasurementCd>Years</UnitMeasurementCd>
				</com.csc_LengthTimeInOccupation> -->
          <!-- 55614 Begin -->
			</CommlPolicySupplement>
			<com.csc_CompanyPolicyProcessingId>
				<xsl:value-of select="PIF__LOCATION__A"/>
			</com.csc_CompanyPolicyProcessingId>
			<com.csc_InsuranceLineIssuingCompany>
				<xsl:choose>
	        	                <xsl:when test="$LOB='WCP'">
						<xsl:value-of select="PIF__COMPANY__NUMBER"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="PIF__MASTER__CO__NUMBER_A"/>
					</xsl:otherwise>
				</xsl:choose>
			</com.csc_InsuranceLineIssuingCompany>
			<com.csc_PolicyTermMonths>
				<xsl:value-of select="PIF__INSTALLMENT__TERM"/>
			</com.csc_PolicyTermMonths>
				<!-- 55614 Begin -->			
                  <!--      <xsl:if test="$LOB='WCP'">
                        	<com.csc_BusinessTypeCd>
					<xsl:value-of select="//POLICY__INFORMATION__SEG/PIF__SPECIAL__USE__A"/>
				</com.csc_BusinessTypeCd>
				<xsl:for-each select="/*/PMD4J__NAME__ADDR__SEG">
					<xsl:if test="PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__SEGMENT__LEVEL__CODE='J'                           and PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__SEGMENT__PART__CODE='J'                                                       and PMD4J__SEGMENT__KEY/PMD4J__SEGMENT__ID__KEY/PMD4J__SUB__PART__CODE='1'">
						<com.csc_NextLocationAssignedId>
							<xsl:value-of select="$NextLocation"/>
						</com.csc_NextLocationAssignedId>
					</xsl:if>
				</xsl:for-each>
                        </xsl:if>-->
              <!-- 55614 End -->          
                        <xsl:if test="$LOB='WCP'">
				<com.csc_ItemIdInfo>
					<OtherIdentifier>
						<OtherIdTypeCd>com.csc_NextAvailableLocationNbr</OtherIdTypeCd>
						<OtherId>
							<xsl:value-of select="$NextLocation"/>
						</OtherId>
					</OtherIdentifier>
				</com.csc_ItemIdInfo>
			</xsl:if>
		</CommlPolicy>
	</xsl:template>
	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="WCPolicy">
		<CommlPolicy>
			<xsl:attribute name="id">
				<xsl:value-of select="/POLICY__INFORMATION__SEG/PIF__ISOL__APP__ID"/>
			</xsl:attribute>
			<PolicyNumber>
				<xsl:value-of select="PIF__KEY/PIF__POLICY__NUMBER"/>
			</PolicyNumber>
			<PolicyVersion>
				<xsl:value-of select="PIF__KEY/PIF__MODULE"/>
			</PolicyVersion>
			<CompanyProductCd>
				<xsl:choose>
					<xsl:when test= "../*/PMDL4W1__SEGMENT__DATA/PMDL4W1__POLICY__TYPE = 'S'">WCV</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="PIF__KEY/PIF__SYMBOL"/>
					</xsl:otherwise>
				</xsl:choose>
				<!-- <xsl:value-of select="PIF__KEY/PIF__SYMBOL"/> -->
			</CompanyProductCd>
			<LOBCd>
				<xsl:value-of select="PIF__LINE__BUSINESS"/>
			</LOBCd>
			<NAICCd>
				<!--Issue 103869 Starts-->
				<!--<xsl:value-of select="PIF__MASTER__CO__NUMBER"/>-->
				<xsl:value-of select="PIF__MASTER__CO__NUMBER__A"/>
				<!--Issue 103869 Ends-->
			</NAICCd>
			<ControllingStateProvCd>
				<xsl:call-template name="BuildStateProvCd">
					<xsl:with-param name="StateCd" select="PIF__RISK__STATE__PROV"/>
				</xsl:call-template>
			</ControllingStateProvCd>
			<ContractTerm>
				<EffectiveDt>
					<!-- <xsl:value-of select="PIF__EFFECTIVE__DATE"/> -->
					<xsl:call-template name="ConvertS2DateToISODate">
						<xsl:with-param name="Value" select="PIF__EFFECTIVE__DATE"/>
					</xsl:call-template>
				</EffectiveDt>
				<ExpirationDt>
					<!-- <xsl:value-of select="PIF__EXPIRATION__DATE"/> -->
					<xsl:call-template name="ConvertS2DateToISODate">
						<xsl:with-param name="Value" select="PIF__EXPIRATION__DATE"/>
					</xsl:call-template>
				</ExpirationDt>
				<DurationPeriod>
					<NumUnits>
						<xsl:value-of select="PIF__INSTALLMENT__TERM"/>
					</NumUnits>
				</DurationPeriod>
			</ContractTerm>
			<BillingAccountNumber/>
			<BillingMethodCd/>
			<CurrentTermAmt>
				<Amt>
					<xsl:value-of select="/*/PMDGS1__PREMIUM__SUMMARY__SEG/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__TERM__PREM"/>
				</Amt>
				<CurCd>USD</CurCd>
			</CurrentTermAmt>

			<OtherInsuranceWithCompanyCd/>
			<PayorCd/>
			<RateEffectiveDt/>
			<RenewalBillingMethodCd/>
			<RenewalPayorCd/>
			<RenewalTerm>
				<EffectiveDt/>
				<ExpirationDt/>
			</RenewalTerm>

			<xsl:for-each select="/*/PMD4T__FORMS__SEG">
				<xsl:if test="(PMD4T__SEGMENT__KEY/PMD4T__SEGMENT__ID__KEY/PMD4T__SEGMENT__LEVEL__CODE='I' or PMD4T__SEGMENT__KEY/PMD4T__SEGMENT__ID__KEY/PMD4T__SEGMENT__LEVEL__CODE='L') and PMD4T__SEGMENT__KEY/PMD4T__SEGMENT__ID__KEY/PMD4T__SEGMENT__PART__CODE='T' and PMD4T__SEGMENT__KEY/PMD4T__SEGMENT__ID__KEY/PMD4T__INSURANCE__LINE='WC'">
					<xsl:for-each select="PMD4T__SEGMENT__DATA/PMD4T__FORMS__SECTIONS/PMD4T__FORMS__ENTRIES[@index]">
						<Form>
							<FormNumber>
								<xsl:value-of select="./PMD4T__FORM__NO"/>
							</FormNumber>
							<EditionDt>
								<xsl:value-of select="./PMD4T__FORM__DATE"/>
							</EditionDt>
							<com.csc_ActionInfo>
								<ActionCd>
									<xsl:value-of select="./PMD4T__FORM__ACTION"/>
								</ActionCd>
							</com.csc_ActionInfo>
						</Form>
					</xsl:for-each>
				</xsl:if>
			</xsl:for-each>
			<PaymentOption>
				<!--SYS/DST # 330 Starts-->
				<PaymentPlanCd>
				<xsl:value-of select="concat(//PIF__PAY__SERVICE__CODE, //PIF__MODE__CODE)"/>
				</PaymentPlanCd>
				<!--SYS/DST # 330 Ends-->
					<DayMonthDue/>
				<MethodPaymentCd/>
				<NextTermPaymentPlanCd/>
				<DepositAmt>
					<Amt/>
					<CurCd/>
				</DepositAmt>
				<DownPaymentPct/>
				<FirstPaymentDueDt/>
				<PaymentIntervalCd/>
				<NumPayments>
					<xsl:value-of select="/*/POLICY__INFORMATION__SEG/PIF__NUMBER__INSTALLMENTS"/>
				</NumPayments>
				<LOBCd/>
				<LOBSubCd/>
				<com.csc_BillingPlanCd/>
			</PaymentOption>
			<xsl:variable name="UnitNo">000</xsl:variable>
			<CommlPolicySupplement>
				<LengthTimeInBusiness/>
				<xsl:choose>
					<xsl:when test="/*/POLICY__INFORMATION__SEG/PIF__AUDIT__CODE='A'"><AuditFrequencyCd>AN</AuditFrequencyCd></xsl:when>
					<xsl:when test="/*/POLICY__INFORMATION__SEG/PIF__AUDIT__CODE='M'"><AuditFrequencyCd>MO</AuditFrequencyCd></xsl:when>
					<xsl:when test="/*/POLICY__INFORMATION__SEG/PIF__AUDIT__CODE='Q'"><AuditFrequencyCd>QT</AuditFrequencyCd></xsl:when>
					<xsl:when test="/*/POLICY__INFORMATION__SEG/PIF__AUDIT__CODE='S'"><AuditFrequencyCd>SA</AuditFrequencyCd></xsl:when>
				</xsl:choose>
				<com.csc_AuditTypeCd>
					<xsl:value-of select="//PMD4D__SEGMENT__DATA/PMD4D__TYPE"/>
				</com.csc_AuditTypeCd>
                                <com.csc_LengthTimeInOccupation/>
			</CommlPolicySupplement>
			<com.csc_CompanyPolicyProcessingId>
				<xsl:value-of select="PIF__LOCATION__A"/>
			</com.csc_CompanyPolicyProcessingId>
			<com.csc_InsuranceLineIssuingCompany>
				<xsl:value-of select="PIF__MASTER__CO__NUMBER"/>
			</com.csc_InsuranceLineIssuingCompany>
			<com.csc_PolicyTermMonths>
				<xsl:value-of select="PIF__INSTALLMENT__TERM"/>
			</com.csc_PolicyTermMonths>
                       	<com.csc_BusinessTypeCd>
				<xsl:value-of select="//POLICY__INFORMATION__SEG/PIF__SPECIAL__USE__A"/>
			</com.csc_BusinessTypeCd>
		</CommlPolicy>
	</xsl:template>

</xsl:stylesheet>
