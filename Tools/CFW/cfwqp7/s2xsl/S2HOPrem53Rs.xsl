<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--<xsl:include href="CommonTplRq.xsl"/> -->
	<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service Issue # 35270		 
***********************************************************************************************
-->

	<xsl:template name="S2HOPremRsTemplate">
		<xsl:param name="FormCode"/>
		<xsl:param name="IndexPosition"/>
		<xsl:param name="SequenceNumber"/>
		<xsl:for-each select="/*/HOMEOWNERS__PREMIUM__SEG/HOMEOWNERS__TABLE__1/MISCELLANEOUS__PREMIUMS">
			<xsl:if test="normalize-space(HOMEOWNERS__PREMIUM)!='' and HOMEOWNERS__PREMIUM__FORM != ''">
				<xsl:variable name="PremiumForm" select="HOMEOWNERS__PREMIUM__FORM"/>
				<xsl:choose>
					<xsl:when test="$PremiumForm = '801' or $PremiumForm = '802' or $PremiumForm = '803' or $PremiumForm = '805' or $PremiumForm = '806' or $PremiumForm = '808' or $PremiumForm = '809' or $PremiumForm = '818' or substring($PremiumForm,1,2) = 'FF'" >
						<xsl:if test="$FormCode = $PremiumForm">
							<xsl:variable name="PremiumAmt" select="HOMEOWNERS__PREMIUM"/>
							<xsl:value-of select="$PremiumAmt"/>
						</xsl:if>
					</xsl:when>
					<xsl:when test="$PremiumForm = '075'"> <!--Water carft-->
					  <xsl:variable name="SeqNumber" select="HOMEOWNERS__PREMIUM__SEGMENT"/>
						<xsl:if test="$FormCode = $PremiumForm and $SeqNumber =$SequenceNumber">
							<xsl:variable name="PremiumAmt" select="HOMEOWNERS__PREMIUM"/>
							<xsl:value-of select="$PremiumAmt"/>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$FormCode = $PremiumForm and @index =$IndexPosition">
							<xsl:variable name="PremiumAmt" select="HOMEOWNERS__PREMIUM"/>
							<xsl:value-of select="$PremiumAmt"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->