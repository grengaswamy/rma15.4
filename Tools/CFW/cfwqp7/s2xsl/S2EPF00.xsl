<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="S2EPF00Template"> 
    <xsl:param name="ProcessingType"/>	<!-- Case 34768 -->
    <xsl:param name="PolicyMode"/>	<!-- Case 34768 -->
    <!-- Case 34768 Begin -->
    <xsl:variable name="PolicyModule">
	<xsl:choose>
		<xsl:when test="$PolicyMode = 'N'">00</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$MOD"/>
		</xsl:otherwise>
	</xsl:choose>
    </xsl:variable>
    <!-- Case 34768 End -->
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>IEPF00__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<IEPF00__SEG>
				<IEPF00__LLBB>
					<fill_0/>
					<IEPF00__ACTION__CODE/>
					<IEPF00__FILE__ID/>
				</IEPF00__LLBB>
				<IEPF00__ID1>00</IEPF00__ID1>
				<IEPF00__ENTRY__CODE/>
				<IEPF00__OPER__ID>ECO</IEPF00__OPER__ID>
				<IEPF00__TRANS__ID/>
				<IEPF00__BATCH__DATE>
					<xsl:value-of select="concat(substring($TransEffDt,1,4),substring($TransEffDt,6,2),substring($TransEffDt,9,2))"/>
				</IEPF00__BATCH__DATE>
				<IEPF00__ORIGINAL__EFF__DATE>
					<xsl:value-of select="concat(substring($EffDt,1,4),substring($EffDt,6,2),substring($EffDt,9,2))"/>
				</IEPF00__ORIGINAL__EFF__DATE>
				<IEPF00__LOC>
					<!-- Case 34768: Just use $LOC -->
					<!-- Issue 35271 Added condition for BOP -->
					<!-- xsl:choose>
						<xsl:when test="$LOB='BOP'">
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/CommlPolicy/com.csc_CompanyPolicyProcessingId"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/com.csc_CompanyPolicyProcessingId"/>
						</xsl:otherwise>
					</xsl:choose -->
					<xsl:value-of select="$LOC"/>
				</IEPF00__LOC>
				<IEPF00__POLICY__NUM>
					<xsl:value-of select="$POL"/>
				</IEPF00__POLICY__NUM>
				<IEPF00__SYMBOL>
					<xsl:value-of select="$SYM"/>
				</IEPF00__SYMBOL>
				<IEPF00__MCO>
					<xsl:value-of select="$MCO"/>
				</IEPF00__MCO>
				<IEPF00__MOD>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">IEPF00__MOD</xsl:with-param>
						<xsl:with-param name="FieldLength">2</xsl:with-param>
						<!--<xsl:with-param name="Value" select="$MOD"/>--> <!-- Case 34768 -->
						<xsl:with-param name="Value" select="$PolicyModule"/>	<!-- Case 34768 -->
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</IEPF00__MOD>
				<IEPF00__ACTIVITY__DATE>
					<xsl:value-of select="concat(substring($TransRqDt,1,4),substring($TransRqDt,6,2),substring($TransRqDt,9,2))"/>
				</IEPF00__ACTIVITY__DATE>
				<IEPF00__ID1>00</IEPF00__ID1>
				<IEPF00__FILE/>
				<IEPF00__LOB>
					<xsl:value-of select="$LOB"/>
				</IEPF00__LOB>
				<IEPF00__ISSUE__CODE>
				<!-- Case 34768 Begin -->
					<xsl:value-of select="$PolicyMode"/>
				<!-- Case 34768 End -->
				</IEPF00__ISSUE__CODE>
				<IEPF00__BRANCH__CODE>
					<xsl:value-of select="string('  ')"/>
				</IEPF00__BRANCH__CODE>
				<IEPF00__NO__PROCESS__IND>
					<xsl:value-of select="string(' ')"/>
				</IEPF00__NO__PROCESS__IND>
				<IEPF00__INTERFACE>
					<xsl:value-of select="string(' ')"/>
				</IEPF00__INTERFACE>
				<IEPF00__FS__EDIT__TABLE>
					<xsl:value-of select="string('               ')"/>
				</IEPF00__FS__EDIT__TABLE>
				<IEPF00__FS__OPTION/>
				<fill_1/>
				<IEPF00__YR2000__CUST__USE/>
			</IEPF00__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet>