<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template name="BuildInstPmt02SegmentRq"> 
	<!-- <xsl:template match="/"> -->
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>POLICY__INFORMATION__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<POLICY__INFORMATION__SEG>
				<PIF__REC__LLBB/>
				<PIF__ID>02</PIF__ID>
				<PIF__KEY>
					<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_AccountPaymentRq/com.csc_AccountPaymentInfo/PolicyNumber">
						<PIF__POLICY__NUMBER>
							<xsl:value-of select="."/>
						</PIF__POLICY__NUMBER>
					</xsl:for-each>
				</PIF__KEY>
				<PIF__EFFECTIVE__DATE>
					<PIF__EFF__DATE__YR__MO>
						<PIF__EFF__YR>
							<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_AccountPaymentRq/TransactionEffectiveDt,1,4)"/>
						</PIF__EFF__YR>
						<PIF__EFF__MO>
							<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_AccountPaymentRq/TransactionEffectiveDt,6,2)"/>
						</PIF__EFF__MO>
					</PIF__EFF__DATE__YR__MO>
					<PIF__EFF__DA>
						<xsl:value-of select="substring(ACORD/InsuranceSvcRq/com.csc_AccountPaymentRq/TransactionEffectiveDt,9,2)"/>
					</PIF__EFF__DA>
				</PIF__EFFECTIVE__DATE>
				<PIF__ISSUE__CODE>A</PIF__ISSUE__CODE>
				<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_AccountPaymentRq/com.csc_AccountPaymentInfo/MethodPaymentCd">
					<PIF__SPECIAL__USE__A>
						<xsl:value-of select="."/>
					</PIF__SPECIAL__USE__A>
				</xsl:for-each>
				<xsl:for-each select="ACORD/InsuranceSvcRq/com.csc_AccountPaymentRq/com.csc_AccountPaymentInfo/com.csc_PaymentAmt/Amt">
					<PIF__SPECIAL__USE__B>
						<xsl:value-of select="."/>
					</PIF__SPECIAL__USE__B>
				</xsl:for-each>
			</POLICY__INFORMATION__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="com.csc_AccountPaymentRq.xml" userelativepaths="yes" externalpreview="no" url="..\XML Message\com.csc_AccountPaymentRq.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="..\XML Message\com.csc_AccountPaymentRq.xml" srcSchemaRoot="ACORD" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\Stub XML\Pinfo53Record.xml" destSchemaRoot="BUS__OBJ__RECORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->