<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2HORateTemplate" match="/">
		<xsl:for-each select="ACORD/InsuranceSvcRq/*/HomeLineBusiness/Dwell">
			<BUS__OBJ__RECORD>
				<RECORD__NAME__ROW>
					<RECORD__NAME>HOMEOWNERS__RATING__SEG</RECORD__NAME>
				</RECORD__NAME__ROW>
				<HOMEOWNERS__RATING__SEG>
					<HRR__REC__LLBB>
						<HRR__REC__LENGTH/>
						<HRR__ACTION__CODE/>
						<HRR__FILE__ID/>
					</HRR__REC__LLBB>
					<HRR__RECORD__ID>17</HRR__RECORD__ID>
					<HRR__KEY>
						<HRR__UNIT>0</HRR__UNIT>
						<HRR__AMENDMENT>0</HRR__AMENDMENT>
						<xsl:variable name="EffDt" select="../../PersPolicy/ContractTerm/EffectiveDt"/>
						<HRR__LAST__CHANGE>
							<HRR__LAST__CHANGE__YEAR>
								<xsl:value-of select="substring($EffDt,1,4)"/>
							</HRR__LAST__CHANGE__YEAR>
							<HRR__LAST__CHANGE__MONTH>
								<xsl:value-of select="substring($EffDt,6,2)"/>
							</HRR__LAST__CHANGE__MONTH>
							<HRR__LAST__CHANGE__DAY>
								<xsl:value-of select="substring($EffDt,9,2)"/>
							</HRR__LAST__CHANGE__DAY>
						</HRR__LAST__CHANGE>
					</HRR__KEY>
					<HRR__LOCATION__STATE>
						<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
							<xsl:with-param name="Value" select="../../PersPolicy/ControllingStateProvCd"/>
						</xsl:call-template>
					</HRR__LOCATION__STATE>
					<HRR__TERRITORY>
						<xsl:choose>
							<xsl:when test="string-length(DwellRating/TerritoryCd) &gt; 2">
								<xsl:value-of select="substring(DwellRating/TerritoryCd,string-length(DwellRating/TerritoryCd)-1,2)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="DwellRating/TerritoryCd"/>
							</xsl:otherwise>
						</xsl:choose>
					</HRR__TERRITORY>
					<HRR__FORM>
						<xsl:value-of select="PolicyTypeCd"/>
					</HRR__FORM>
					<HRR__NUMBER__OF__FAMILIES>
						<xsl:value-of select="DwellInspectionValuation/NumFamilies"/>
					</HRR__NUMBER__OF__FAMILIES>
					<HRR__CONSTRUCTION>
						<xsl:value-of select="Construction/ConstructionCd"/>
					</HRR__CONSTRUCTION>
					<HRR__PROTECTION>
						<HRR__PROTECT__A>
							<xsl:variable name="ProtClassCd" select="BldgProtection/FireProtectionClassCd"/>
							<xsl:value-of select="substring(concat('00',$ProtClassCd),1+string-length($ProtClassCd),1)"/>
						</HRR__PROTECT__A>
						<HRR__PROTECT__B>
							<xsl:variable name="ProtClassCd" select="BldgProtection/FireProtectionClassCd"/>
							<xsl:value-of select="substring(concat('00',$ProtClassCd),2+string-length($ProtClassCd),1)"/>
						</HRR__PROTECT__B>
					</HRR__PROTECTION>
					<HRR__DEDUCTIBLE__TYPE__AMOUNT>
						<HRR__DEDUCTIBLE__TYPE>
							<xsl:value-of select="Coverage[CoverageCd='DWELL']/Deductible/DeductibleTypeCd"/>
						</HRR__DEDUCTIBLE__TYPE>
						<HRR__DEDUCTIBLE__AMOUNT>
							<xsl:variable name="AmtTemp" select="substring-before(Coverage[CoverageCd='DWELL']/Deductible/FormatCurrencyAmt/Amt,'.')"/>
							<xsl:value-of select="substring(concat('00000',$AmtTemp),1+string-length($AmtTemp),5)"/>
						</HRR__DEDUCTIBLE__AMOUNT>
					</HRR__DEDUCTIBLE__TYPE__AMOUNT>
					<HRR__COVERAGE__A__LIMIT>
						<xsl:variable name="AmtTemp" select="substring-before(Coverage[CoverageCd='DWELL']/Limit/FormatCurrencyAmt/Amt,'.')"/>
						<xsl:value-of select="substring(concat('00000000',$AmtTemp),1+string-length($AmtTemp),8)"/>
					</HRR__COVERAGE__A__LIMIT>
					<HRR__COVERAGE__A__PREMIUM>
						<xsl:choose>
							<xsl:when test="substring-before(Coverage[CoverageCd='DWELL']/CurrentTermAmt/Amt,'.') &gt; 0">
								<xsl:value-of select="concat(substring-before(Coverage[CoverageCd='DWELL']/CurrentTermAmt/Amt,'.'),substring-after(Coverage[CoverageCd='DWELL']/CurrentTermAmt/Amt,'.'))"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="00000000"/>
							</xsl:otherwise>
						</xsl:choose>
					</HRR__COVERAGE__A__PREMIUM>
					<HRR__COVERAGE__B__LIMIT>
						<xsl:variable name="AmtTemp" select="substring-before(Coverage[CoverageCd='OS']/Limit/FormatCurrencyAmt/Amt,'.')"/>
						<xsl:value-of select="substring(concat('00000000',$AmtTemp),1+string-length($AmtTemp),8)"/>
					</HRR__COVERAGE__B__LIMIT>
					<HRR__COVERAGE__C__LIMIT>
						<xsl:variable name="AmtTemp" select="substring-before(Coverage[CoverageCd='PP']/Limit/FormatCurrencyAmt/Amt,'.')"/>
						<xsl:value-of select="substring(concat('00000000',$AmtTemp),1+string-length($AmtTemp),8)"/>
					</HRR__COVERAGE__C__LIMIT>
					<HRR__COVERAGE__C__PREMIUM>
						<xsl:choose>
							<xsl:when test="substring-before(Coverage[CoverageCd='PP']/CurrentTermAmt/Amt,'.') &gt; 0">
								<xsl:value-of select="concat(substring-before(Coverage[CoverageCd='PP']/CurrentTermAmt/Amt,'.'),substring-after(Coverage[CoverageCd='PP']/CurrentTermAmt/Amt,'.'))"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="00000000"/>
							</xsl:otherwise>
						</xsl:choose>
					</HRR__COVERAGE__C__PREMIUM>
					<HRR__COVERAGE__D__LIMIT>
						<xsl:variable name="AmtTemp" select="substring-before(Coverage[CoverageCd='LOU']/Limit/FormatCurrencyAmt/Amt,'.')"/>
						<xsl:value-of select="substring(concat('00000000',$AmtTemp),1+string-length($AmtTemp),8)"/>
					</HRR__COVERAGE__D__LIMIT>
					<HRR__COVERAGE__D__PREMIUM>
						<xsl:choose>
							<xsl:when test="substring-before(Coverage[CoverageCd='LOU']/CurrentTermAmt/Amt,'.') &gt; 0">
								<xsl:value-of select="concat(substring-before(Coverage[CoverageCd='LOU']/CurrentTermAmt/Amt,'.'),substring-after(Coverage[CoverageCd='LOU']/CurrentTermAmt/Amt,'.'))"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="00000000"/>
							</xsl:otherwise>
						</xsl:choose>
					</HRR__COVERAGE__D__PREMIUM>
					<HRR__COVERAGE__E__AND__F__LIMITS>
						<HRR__COVERAGE__E__LIMIT>
							<xsl:variable name="AmtTemp" select="Coverage[CoverageCd='PL']/Limit/FormatCurrencyAmt/Amt"/>
							<xsl:value-of select="substring(concat('00000000',$AmtTemp),1+string-length($AmtTemp),8)"/>
						</HRR__COVERAGE__E__LIMIT>
						<HRR__COVERAGE__F__LIMIT>
							<xsl:variable name="AmtTemp" select="Coverage[CoverageCd='MEDPM']/Limit/FormatCurrencyAmt/Amt"/>
							<xsl:value-of select="substring(concat('000000',$AmtTemp),1+string-length($AmtTemp),6)"/>
						</HRR__COVERAGE__F__LIMIT>
					</HRR__COVERAGE__E__AND__F__LIMITS>
					<HRR__COVERAGE__E__F__PREMIUM>
						<xsl:choose>
							<xsl:when test="substring-before(Coverage[CoverageCd='MEDP']/CurrentTermAmt/Amt,'.') &gt; 0">
								<xsl:value-of select="concat(substring-before(Coverage[CoverageCd='MEDP']/CurrentTermAmt/Amt,'.'),substring-after(Coverage[CoverageCd='MEDP']/CurrentTermAmt/Amt,'.'))"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="00000000"/>
							</xsl:otherwise>
						</xsl:choose>
					</HRR__COVERAGE__E__F__PREMIUM>
					<HRR__APPROVED__ROOF>
						<xsl:value-of select="Construction/RoofingMaterial/RoofMaterialCd"/>
					</HRR__APPROVED__ROOF>
					<HRR__HYDRANT>
						<xsl:variable name="TempValue" select="BldgProtection/DistanceToHydrant/NumUnits"/>
						<xsl:value-of select="substring(concat('0000',$TempValue),1+string-length($TempValue),4)"/>
					</HRR__HYDRANT>
					<HRR__INSIDE__CITY>
						<xsl:value-of select="AreaTypeSurroundingsCd"/>
					</HRR__INSIDE__CITY>
					<HRR__AUTOMATIC__VALUE__UP>
						<xsl:value-of select="Coverage[CoverageCd='com.csc_VALUP']/Option/OptionValue"/>
					</HRR__AUTOMATIC__VALUE__UP>
					<HRR__NO__OF__APARTMENTS>
						<xsl:variable name="TempValue" select="DwellOccupancy/NumApartments"/>
						<xsl:value-of select="substring(concat('00',$TempValue),1+string-length($TempValue),2)"/>
					</HRR__NO__OF__APARTMENTS>
					<HRR__LIABILITY__ZONE>
						<xsl:value-of select="string('00')"/>
					</HRR__LIABILITY__ZONE>
					<HRR__MONEY__ENDORSEMENT>
						<xsl:value-of select="string('000')"/>
					</HRR__MONEY__ENDORSEMENT>
					<HRR__FIRE__EC__RATE>
						<xsl:value-of select="string('000')"/>
					</HRR__FIRE__EC__RATE>
					<HRR__WIND__HAIL__EXCLUSION>
						<xsl:value-of select="Coverage[CoverageCd='WINDX']/Option/OptionValue"/>
					</HRR__WIND__HAIL__EXCLUSION>
					<HRR__THEFT__EXTENSION>
						<xsl:value-of select="string('N')"/>
					</HRR__THEFT__EXTENSION>
					<HRR__INFLATION__GUARD>
						<xsl:value-of select="Coverage[CoverageCd='INFGD']/Option/OptionValue"/>
					</HRR__INFLATION__GUARD>
					<HRR__THEFT__250__DEDUCTIBLE>
						<xsl:value-of select="Coverage[CoverageCd='com.csc_TheftDed']/Option/OptionValue"/>
					</HRR__THEFT__250__DEDUCTIBLE>
					<HRR__CLASS__RATED>
						<xsl:value-of select="string('0')"/>
					</HRR__CLASS__RATED>
					<HRR__YEAR__OF__CONSTRUCTION>
						<xsl:value-of select="Construction/YearBuilt"/>
					</HRR__YEAR__OF__CONSTRUCTION>
					<HRR__ZIP__CODE>
						<xsl:choose>
							<xsl:when test="../../Location/ItemIdInfo/InsurerId">
								<xsl:value-of select="substring(../../Location/Addr/PostalCode,1,5)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="substring(../../InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode,1,5)"/>
							</xsl:otherwise>
						</xsl:choose>
					</HRR__ZIP__CODE>
					<HRR__ORIG__ENDORSEMENT__RATE__BOOK>
						<xsl:value-of select="string('0')"/>
					</HRR__ORIG__ENDORSEMENT__RATE__BOOK>
					<HRR__TAX__LOCATION>
						<HRR__COUNTY__LOCATION>
							<xsl:value-of select="string('000')"/>
						</HRR__COUNTY__LOCATION>
						<HRR__CITY__LOCATION>
						<xsl:value-of select="string('000')"/>
						</HRR__CITY__LOCATION>
					</HRR__TAX__LOCATION>
					<HRR__CURR__REPLACEMENT__COST>
						<xsl:value-of select="string('00000')"/>
					</HRR__CURR__REPLACEMENT__COST>
					<HRR__CONTINGENT__LIABILITY>
						<xsl:value-of select="string('N')"/>
					</HRR__CONTINGENT__LIABILITY>
					<HRR__SPECIAL__LOSS__PCT>
						<xsl:value-of select="string('00')"/>
					</HRR__SPECIAL__LOSS__PCT>
					<HRR__CURR__REPLACE__COST__DATE/>
					<HRR__REPLACEMENT__COST>
						<xsl:if test="string-length(DwellInspectionValuation/EstimatedReplCostAmt/Amt) &lt; 6">
							<xsl:value-of select="substring(concat('00000',DwellInspectionValuation/EstimatedReplCostAmt/Amt),number(string-length(DwellInspectionValuation/EstimatedReplCostAmt/Amt))+1,5 )"/>
						</xsl:if>
						<xsl:if test="string-length(DwellInspectionValuation/EstimatedReplCostAmt/Amt)  &gt; 5">
							<xsl:value-of select="substring(DwellInspectionValuation/EstimatedReplCostAmt/Amt,1,5 )"/>
						</xsl:if>
					</HRR__REPLACEMENT__COST>
					<SECONDARY__RESIDENCE__USE>
						<!--<xsl:value-of select="DwellOccupancy/ResidenceTypeCd"/>-->
						<xsl:value-of select="DwellOccupancy/OccupancyTypeCd"/>
					</SECONDARY__RESIDENCE__USE>
					<MOBILE__HOME__TIE__DOWN__CODE>
						<xsl:value-of select="string('0')"/>
					</MOBILE__HOME__TIE__DOWN__CODE>
					<MOBILE__HOME__SEASONAL__OCCUPANCY/>
					<HRR__INSPECTION__YEAR/>
					<HRR__LAST__VALUE__CHANGE>
						<xsl:value-of select="string('0')"/>
					</HRR__LAST__VALUE__CHANGE>
					<HRR__INSPECTION__REPORT>
						<xsl:value-of select="string('0')"/>
					</HRR__INSPECTION__REPORT>
					<HRR__REPLACE__COST__METHOD>
						<xsl:value-of select="string('0')"/>
					</HRR__REPLACE__COST__METHOD>
					<HRR__AGENT__COMMISSION>
						<xsl:value-of select="string('000')"/>
					</HRR__AGENT__COMMISSION>
					<HRR__HISTORY__OPTION>
						<xsl:value-of select="string('N')"/>
					</HRR__HISTORY__OPTION>
					<HRR__NEW__PREMIUM>
						<xsl:value-of select="string('000000000')"/>
					</HRR__NEW__PREMIUM>
					<HRR__OLD__PREMIUM>
						<xsl:value-of select="string('000000000')"/>
					</HRR__OLD__PREMIUM>
					<HRR__PREMIUM__GROUP>
						<!--<xsl:value-of select="string('000')"/>-->
						<HRR__PREMIUM__GROUP__DIGIT1>
							<xsl:value-of select="string('0')"/>
						</HRR__PREMIUM__GROUP__DIGIT1>
						<HRR__PREMIUM__GROUP__DIGIT2__3>
							<xsl:value-of select="string('00')"/>
						</HRR__PREMIUM__GROUP__DIGIT2__3>
					</HRR__PREMIUM__GROUP>
					<HRR__ORIGINAL__RATE__BOOK>
						<xsl:value-of select="string('0')"/>
					</HRR__ORIGINAL__RATE__BOOK>
					<HRR__LAST__CHANGE__FACTOR>
						<xsl:value-of select="string('000')"/>
					</HRR__LAST__CHANGE__FACTOR>
					<HRR__HO15__INDICATOR/>
					<HRR__MBLHOME__ACV__INDICATOR>
						<xsl:value-of select="string('N')"/>
					</HRR__MBLHOME__ACV__INDICATOR>
					<MOBLHME__MH82__ENDORSE__INDICATOR>
						<xsl:value-of select="string('N')"/>
					</MOBLHME__MH82__ENDORSE__INDICATOR>
					<HRR__TERRITORY__VARIANCE>
						<xsl:value-of select="string('N')"/>
					</HRR__TERRITORY__VARIANCE>
					<HRR__FORM__VARIANCE/>
					<HRR__CONSTRUCTION__VARIANCE/>
					<HRR__PROTECTION__VARIANCE/>
					<HRR__DED__TYPE__VARIANCE/>
					<HRR__DED__AMOUNT__VARIANCE/>
					<HRR__W__H__EXCL__VARIANCE/>
					<HRR__THEFT__EXT__VARIANCE/>
					<HRR__THEFT__250__DED__VARIANCE/>
					<HRR__SPECIAL__LOSS__VARIANCE/>
					<HRR__VARIANCE__A__LIMIT/>
					<HRR__VARIANCE__B__LIMIT/>
					<HRR__VARIANCE__C__LIMIT/>
					<HRR__VARIANCE__D__LIMIT/>
					<HRR__VARIANCE__E__F__LIMITS/>
					<HRR__INFLATION__GUARD__VARIANCE/>
					<HRR__HAP__POLICY/>
					<HRR__HAP__MOD__INDICATOR/>
					<HRR__HO84__INDICATOR/>
					<NEW__HOME__DISC__INDICATOR/>
					<REPLACEMENT__COST__INDICATOR/>
					<HRR__PMS__FUTURE__USE/>
					<HRR__CUST__FUTURE__USE/>
					<HRR__YR2000__CUST__USE/>
					<HRR__DUP__KEY__SEQ__NUM/>
				</HOMEOWNERS__RATING__SEG>
			</BUS__OBJ__RECORD>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="WaterCraft.xml" userelativepaths="yes" externalpreview="no" url="..\WaterCraft.xml" htmlbaseurl="" outputurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="..\WaterCraft.xml" srcSchemaRoot="ACORD" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\horate53.Xml" destSchemaRoot="BUS__OBJ__RECORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->