<?xml version="1.0"?>
<!--
**********************************************************************************************
This XSL stylesheet is used by Communications Framework to transform XML from ACORD business
messages into POINT specific data records.
E-Service case XXXXX
**********************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template name="FormatData">
		<xsl:param name="FieldName"/>
		<xsl:param name="Value"/>
		<xsl:param name="FieldType"/>
		<xsl:param name="FieldLength"/>
		<xsl:param name="Precision">0</xsl:param>
		<xsl:param name="NumberMask">No Mask</xsl:param>

		<xsl:choose>
			<xsl:when test="$FieldType='N'">
				<xsl:variable name="FormatMask">
					<xsl:choose>
						<xsl:when test="$NumberMask != 'No Mask'">
							<xsl:value-of select="$NumberMask"/>
						</xsl:when>
						<xsl:when test="$Precision &gt; 0">
							<xsl:value-of select="concat(substring('00000000000',1,$FieldLength - $Precision),'.',substring('00000000000',1,$Precision))"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="substring('00000000000',1,$FieldLength)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="NewValue">
					<xsl:choose>
						<xsl:when test="string($Value)=''">0</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="number(translate($Value, ' $,', ''))"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="string($NewValue)='NaN'">
					<xsl:message terminate="yes">
						<xsl:value-of select="$FieldName"/>should be numeric. The value is '<xsl:value-of select="$Value"/>'
The context is: <xsl:value-of select="name()"/></xsl:message>
				</xsl:if>
				<xsl:value-of select="translate(format-number($NewValue,$FormatMask),'.','')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="ModifiedStringValue">
					<xsl:choose>
						<xsl:when test="$Value=''">
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Value"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:value-of select="substring(concat($ModifiedStringValue, '                                                                                                    '), 1, $FieldLength)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ConvertAlphaStateCdToNumericStateCd">
		<xsl:param name="Value"/>
		<xsl:choose>
			<xsl:when test="$Value='AL'">01</xsl:when>
			<xsl:when test="$Value='AZ'">02</xsl:when>
			<xsl:when test="$Value='AR'">03</xsl:when>
			<xsl:when test="$Value='CA'">04</xsl:when>
			<xsl:when test="$Value='CO'">05</xsl:when>
			<xsl:when test="$Value='CT'">06</xsl:when>
			<xsl:when test="$Value='DE'">07</xsl:when>
			<xsl:when test="$Value='DC'">08</xsl:when>
			<xsl:when test="$Value='FL'">09</xsl:when>
			<xsl:when test="$Value='GA'">10</xsl:when>
			<xsl:when test="$Value='ID'">11</xsl:when>
			<xsl:when test="$Value='IL'">12</xsl:when>
			<xsl:when test="$Value='IN'">13</xsl:when>
			<xsl:when test="$Value='IA'">14</xsl:when>
			<xsl:when test="$Value='KS'">15</xsl:when>
			<xsl:when test="$Value='KY'">16</xsl:when>
			<xsl:when test="$Value='LA'">17</xsl:when>
			<xsl:when test="$Value='ME'">18</xsl:when>
			<xsl:when test="$Value='MD'">19</xsl:when>
			<xsl:when test="$Value='MA'">20</xsl:when>
			<xsl:when test="$Value='MI'">21</xsl:when>
			<xsl:when test="$Value='MN'">22</xsl:when>
			<xsl:when test="$Value='MS'">23</xsl:when>
			<xsl:when test="$Value='MO'">24</xsl:when>
			<xsl:when test="$Value='MT'">25</xsl:when>
			<xsl:when test="$Value='NE'">26</xsl:when>
			<xsl:when test="$Value='NV'">27</xsl:when>
			<xsl:when test="$Value='NH'">28</xsl:when>
			<xsl:when test="$Value='NJ'">29</xsl:when>
			<xsl:when test="$Value='NM'">30</xsl:when>
			<xsl:when test="$Value='NY'">31</xsl:when>
			<xsl:when test="$Value='NC'">32</xsl:when>
			<xsl:when test="$Value='ND'">33</xsl:when>
			<xsl:when test="$Value='OH'">34</xsl:when>
			<xsl:when test="$Value='OK'">35</xsl:when>
			<xsl:when test="$Value='OR'">36</xsl:when>
			<xsl:when test="$Value='PA'">37</xsl:when>
			<xsl:when test="$Value='RI'">38</xsl:when>
			<xsl:when test="$Value='SC'">39</xsl:when>
			<xsl:when test="$Value='SD'">40</xsl:when>
			<xsl:when test="$Value='TN'">41</xsl:when>
			<xsl:when test="$Value='TX'">42</xsl:when>
			<xsl:when test="$Value='UT'">43</xsl:when>
			<xsl:when test="$Value='VT'">44</xsl:when>
			<xsl:when test="$Value='VA'">45</xsl:when>
			<xsl:when test="$Value='WA'">46</xsl:when>
			<xsl:when test="$Value='WV'">47</xsl:when>
			<xsl:when test="$Value='WI'">48</xsl:when>
			<xsl:when test="$Value='WY'">49</xsl:when>
			<xsl:when test="$Value='HI'">50</xsl:when>
			<xsl:when test="$Value='AK'">51</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Value"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Start Issue 35271 -->
	<xsl:template name="ConvertSubjectOfInsToNumeric">
		<xsl:param name="Value"/>
		<xsl:choose>
			<xsl:when test="$Value='BOILER'">106</xsl:when>
			<xsl:when test="$Value='CONDO'">116</xsl:when>
			<xsl:when test="$Value='MONEY'">113</xsl:when>
			<xsl:when test="$Value='PRFCMT'">120</xsl:when>
			<xsl:when test="$Value='ACCTS'">108</xsl:when>
			<xsl:when test="$Value='ADDMGR'">114</xsl:when>
			<xsl:when test="$Value='BURG'">102</xsl:when>
			<xsl:when test="$Value='CMPTR'">117</xsl:when>
			<xsl:when test="$Value='EGLASS'">103</xsl:when>
			<xsl:when test="$Value='IGLASS'">121</xsl:when>
			<xsl:when test="$Value='LOSS'">115</xsl:when>
			<xsl:when test="$Value='MECH'">119</xsl:when>
			<xsl:when test="$Value='SIGNS'">104</xsl:when>
			<xsl:when test="$Value='SPOIL'">118</xsl:when>
			<xsl:when test="$Value='VALPAP'">109</xsl:when>
			<xsl:when test="$Value='BPP'">101</xsl:when>
			<xsl:when test="$Value='BLDG'">100</xsl:when>
			<xsl:when test="$Value='EMPDIS'">105</xsl:when>
			<xsl:when test="$Value='NONOWN'">110</xsl:when>
			<xsl:when test="$Value='HIRED'">111</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="000"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ConvertCoverageEndorse">
		<xsl:param name="Value"/>
		<xsl:choose>
			<xsl:when test="$Value='BOILER'">BV</xsl:when>
			<xsl:when test="$Value='CONDO'">CM</xsl:when>
			<xsl:when test="$Value='MONEY'">MS</xsl:when>
			<xsl:when test="$Value='PRFCMT'">FD</xsl:when>
			<xsl:when test="$Value='ACCTS'">AR</xsl:when>
			<xsl:when test="$Value='ADDMGR'">AI</xsl:when>
			<xsl:when test="$Value='BURG'">BR</xsl:when>
			<xsl:when test="$Value='CMPTR'">EM</xsl:when>
			<xsl:when test="$Value='EGLASS'">EG</xsl:when>
			<xsl:when test="$Value='IGLASS'">IG</xsl:when>
			<xsl:when test="$Value='LOSS'">CL</xsl:when>
			<xsl:when test="$Value='MECH'">MB</xsl:when>
			<xsl:when test="$Value='SIGNS'">OS</xsl:when>
			<xsl:when test="$Value='SPOIL'">SC</xsl:when>
			<xsl:when test="$Value='VALPAP'">VP</xsl:when>
			<xsl:when test="$Value='BPP'">PP</xsl:when>
			<xsl:when test="$Value='BLDG'">BD</xsl:when>
			<xsl:when test="$Value='EMPDIS'">ED</xsl:when>
			<xsl:when test="$Value='NONOWN'">NO</xsl:when>
			<xsl:when test="$Value='HIRED'">HA</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="00"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- End Issue 35271 -->
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->