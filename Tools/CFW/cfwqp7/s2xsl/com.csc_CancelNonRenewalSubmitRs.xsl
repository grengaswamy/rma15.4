<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<ACORD>
			<InsuranceSvcRs>
				<com.csc_CancelNonRenewalSubmitRs>
					<TransactionEffectiveDt>
						<xsl:value-of select='concat(com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR,"-",com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO,"-",com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__EFFECTIVE__DATE/PIF__EFF__DA)'/>
					</TransactionEffectiveDt>
					<MsgStatus>
						<MsgStatusCd>00</MsgStatusCd>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__ISOL__ERROR__DESC">
							<MsgStatusDesc>
								<xsl:value-of select="."/>
							</MsgStatusDesc>
						</xsl:for-each>
					</MsgStatus>
					<com.csc_PartialPolicy>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__POLICY__NUMBER">
							<PolicyNumber>
								<xsl:value-of select="."/>
							</PolicyNumber>
						</xsl:for-each>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__MODULE">
							<PolicyVersion>
								<xsl:value-of select="."/>
							</PolicyVersion>
						</xsl:for-each>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__KEY/PIF__SYMBOL">
							<CompanyProductCd>
								<xsl:value-of select="."/>
							</CompanyProductCd>
						</xsl:for-each>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__MASTER__CO__NUMBER">
							<NAICCd>
								<xsl:value-of select="."/>
							</NAICCd>
						</xsl:for-each>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__LOCATION">
							<com.csc_CompanyPolicyProcessingId>
								<xsl:value-of select="."/>
							</com.csc_CompanyPolicyProcessingId>
						</xsl:for-each>
					</com.csc_PartialPolicy>
					<com.csc_CancelNonRenewalInfo>
						<xsl:for-each select="com.csc_CancelNonRenewalSubmitRs/POLICY__INFORMATION__SEG/PIF__REASON__AMENDED">
							<com.csc_ReasonCd>
								<xsl:value-of select="."/>
							</com.csc_ReasonCd>
						</xsl:for-each>
					</com.csc_CancelNonRenewalInfo>
				</com.csc_CancelNonRenewalSubmitRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Pinfo53Record.xml" userelativepaths="yes" externalpreview="no" url="file://d:\CSC's iSolutions\Development 41\Policy Cancellation\Stub XML\Pinfo53Record.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="file://d:\CSC's iSolutions\Development 41\Policy Cancellation\Stub XML\Pinfo53Record.xml" srcSchemaRoot="com.csc_CancelNonRenewalSubmitRs" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="file://d:\CSC's iSolutions\Development 41\Policy Cancellation\ACORD XML Messages\com.csc_CancelNonRenewalSubmitRs.xml" destSchemaRoot="ACORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->