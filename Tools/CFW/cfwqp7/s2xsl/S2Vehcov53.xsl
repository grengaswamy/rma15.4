<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output method="xml" indent="yes"/>

<xsl:template name="S2Vehcov53Template">
<xsl:param name="VehicleNumber"/>

	<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>VEHICLE__COVERAGE__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
		<VEHICLE__COVERAGE__SEG>
			<VCR__REC__LLBB>
				<VCR__REC__LENGTH/>
				<VCR__ACTION__CODE/>
				<VCR__FILE__ID/>
			</VCR__REC__LLBB>
			<VCR__ID>35</VCR__ID>
			<VCR__KEY>
				<VCR__UNIT__NUMBER>
					<xsl:value-of select="concat(substring('000',1,3 - string-length($VehicleNumber)),$VehicleNumber)"/>
				</VCR__UNIT__NUMBER>
				<VCR__SEGMENT__NUMBER>1</VCR__SEGMENT__NUMBER>
				<VCR__AMEND__NUMBER/>
			</VCR__KEY>
			<VCR__CHANGE__EFFECT__DATE>
				<ED__YEAR>
					<!-- ED__YEAR__CC><xsl:value-of select="substring($TransEffDt,1,2)"/></ED__YEAR__CC>
					<ED__YEAR__YY><xsl:value-of select="substring($TransEffDt,3,2)"/></ED__YEAR__YY -->
					<ED__YEAR__CC><xsl:value-of select="string('  ')"/></ED__YEAR__CC>
					<ED__YEAR__YY><xsl:value-of select="string('  ')"/></ED__YEAR__YY>
				</ED__YEAR>
				<!-- ED__MONTH><xsl:value-of select="substring($TransEffDt,6,2)"/></ED__MONTH>
				<ED__DAY><xsl:value-of select="substring($TransEffDt,9,2)"/></ED__DAY -->
				<ED__MONTH><xsl:value-of select="string('  ')"/></ED__MONTH>
				<ED__DAY><xsl:value-of select="string('  ')"/></ED__DAY>
			</VCR__CHANGE__EFFECT__DATE>
			<SII__VEHICLE__DATA>
				<VEHICLE__COVERAGE__CONTROL>
					<VEHICLE__REASON__AMENDED/>
					<VEHICLE__TOTAL__PREMIUM>
						<xsl:value-of select="string('00000000')"/>
					</VEHICLE__TOTAL__PREMIUM>
					<VEHICLE__OLD__PREMIUM>
						<xsl:value-of select="string('00000000')"/>
					</VEHICLE__OLD__PREMIUM>
					</VEHICLE__COVERAGE__CONTROL>
				<VCR__TABLE__OF__COVERAGES>
					<!-- Issue 80442 Begin -->
					<xsl:choose>
						<xsl:when test="$LOB = 'ACV' or $LOB = 'AFV'">
		   				<xsl:for-each select="CommlCoverage">		
						<VCR__COVERAGE__DATA>
							<xsl:attribute name="index"><xsl:value-of select="position() - 1"/></xsl:attribute>
							<VCR__COVERAGE__CODE>
								<xsl:choose>
									<xsl:when test="Deductible/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('  ')"/>
									</xsl:when>
									<xsl:when test="Limit/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('  ')"/>
									</xsl:when>
									<xsl:when test="Deductible/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('  ')"/>
									</xsl:when>
									<xsl:when test="Limit/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('  ')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="CoverageCd"/>
									</xsl:otherwise>
								</xsl:choose>
							</VCR__COVERAGE__CODE>
							<VCR__COVERAGE__LIMIT>
							<xsl:choose>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Option/OptionTypeCd='YNIn'">
											<xsl:value-of select="substring(concat(substring(Option/OptionValue,1,1),'                '), 1, 17)"/>
								</xsl:when>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt=''">
											<xsl:value-of select="substring(concat(substring(Limit/FormatCurrencyAmt/Amt,1,7),'               '), 1, 17)"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt=''">
									<xsl:value-of select="substring(concat(substring(Deductible/FormatCurrencyAmt/Amt,1,7),'               '),1,17)"/>
								</xsl:when>
								<xsl:when test="string-length(Limit/FormatCurrencyAmt/Amt) > '0'">
											<xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/>
								</xsl:when>
								<xsl:when test="string-length(Deductible/FormatCurrencyAmt/Amt) >'0'">
									<xsl:value-of select="Deductible/FormatCurrencyAmt/Amt"/>
								</xsl:when>
								<xsl:otherwise >
									<xsl:value-of select="substring(concat(substring(Limit/FormatCurrencyAmt/Amt,1,7),'       '),1,7)"/>
								</xsl:otherwise>
							</xsl:choose>
							</VCR__COVERAGE__LIMIT>
							<VCR__COVERAGE__PREMIUM__ALPH>
								<xsl:value-of select="string('       ')"/>
							</VCR__COVERAGE__PREMIUM__ALPH>
							<VCR__COVERAGE__RATE__BOOK__ID/>
						</VCR__COVERAGE__DATA>
						</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
					<!-- Issue 80442 End -->
		   				<xsl:for-each select="Coverage">		
						<!-- Case 35934 - Start -->
						<VCR__COVERAGE__DATA>
							<xsl:attribute name="index"><xsl:value-of select="position() - 1"/></xsl:attribute>
							<VCR__COVERAGE__CODE>
								<xsl:choose>
									<xsl:when test="Deductible/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('  ')"/>
									</xsl:when>
									<xsl:when test="Limit/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('  ')"/>
									</xsl:when>
									<xsl:when test="Deductible/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('  ')"/>
									</xsl:when>
									<xsl:when test="Limit/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('  ')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="CoverageCd"/>
									</xsl:otherwise>
								</xsl:choose>
							</VCR__COVERAGE__CODE>
							<VCR__COVERAGE__LIMIT>
						<!-- Case 35934 - End -->
							<xsl:choose>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<!-- Case 35934 - Start -->
								<!--xsl:when test="Option/OptionTypeCd='YNIn'">
											<xsl:value-of select="substring(concat(CoverageCd,substring(Option/OptionValue,1,1),'                '), 1, 17)"/>
								</xsl:when>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt=''">
											<xsl:value-of select="substring(concat(CoverageCd,substring(Limit/FormatCurrencyAmt/Amt,1,7),'               '), 1, 17)"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt=''">
									<xsl:value-of select="substring(concat(CoverageCd,substring(Deductible/FormatCurrencyAmt/Amt,1,7),'               '),1,17)"/>
								</xsl:when-->
								<xsl:when test="Option/OptionTypeCd='YNIn'">
											<xsl:value-of select="substring(concat(substring(Option/OptionValue,1,1),'                '), 1, 17)"/>
								</xsl:when>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt=''">
											<xsl:value-of select="substring(concat(substring(Limit/FormatCurrencyAmt/Amt,1,7),'               '), 1, 17)"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt=''">
									<xsl:value-of select="substring(concat(substring(Deductible/FormatCurrencyAmt/Amt,1,7),'               '),1,17)"/>
								</xsl:when>
								<!-- Case 39354 Begin -->
								<xsl:when test="string-length(Limit/FormatCurrencyAmt/Amt) > '0'">
											<xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/>
								</xsl:when>
								<xsl:when test="string-length(Deductible/FormatCurrencyAmt/Amt) >'0'">
									<xsl:value-of select="Deductible/FormatCurrencyAmt/Amt"/>
								</xsl:when>
								<!-- Case 39354 End -->
								<xsl:otherwise >
									<xsl:value-of select="substring(concat(substring(Limit/FormatCurrencyAmt/Amt,1,7),'       '),1,7)"/>
								</xsl:otherwise>
								<!-- Case 35934 - End -->
							</xsl:choose>
							<!-- Case 35934 - Start -->
							</VCR__COVERAGE__LIMIT>
<!-- Modified for Issue 35934 <VCR__COVERAGE__PREMIUM__ALPH>-->
							<VCR__COVERAGE__PREMIUM__ALPH>
								<!-- xsl:value-of select="string('0000000')"/ -->
								<xsl:value-of select="string('       ')"/>
							</VCR__COVERAGE__PREMIUM__ALPH>
							<VCR__COVERAGE__RATE__BOOK__ID/>
						</VCR__COVERAGE__DATA>
						<!-- Case 35934 - End -->
						</xsl:for-each>							
					<!-- Issue 80442 Begin -->
						</xsl:otherwise>
					</xsl:choose>
					<!-- Issue 80442 End -->
				</VCR__TABLE__OF__COVERAGES>
				<VCR__HAP__MOD__INDICATOR/>
				<VCR__FACTOR__HOLD__AREA/>
				<VCR__TEXAS__RATING__FACTORS>
					<COMM__TEX__LIAB__PRIM__FAC/>
					<COMM__TEX__COL__PRIM__FAC/>
					<COMM__TEX__OTC__PRIM__FAC/>
					<COMM__TEX__SECONDARY__FAC/>
					<!-- Case 35934 - Start -->
					<!--fill_0/-->
					<fill_7/>
					<!-- Case 35934 - End -->
				</VCR__TEXAS__RATING__FACTORS>
				<VCR__MISC__RATING__FACTORS>
					<COMM__MV__LIAB__PRIM__FAC/>
					<COMM__MV__COL__PRIM__FAC/>
					<COMM__MV__OTC__PRIM__FAC/>
					<COMM__MV__SECONDARY__FAC/>
					<!-- Case 35934 - Start -->
					<!--fill_1/-->
					<fill_8/>
					<!-- Case 35934 - End -->
				</VCR__MISC__RATING__FACTORS>
				<VCR__PP__ADDL__PREM>
					<OH__ADDL__PREM/>
					<!-- Case 35934 - Start -->
					<!--fill_2/-->
					<fill_9/>
					<!-- Case 35934 - End -->
				</VCR__PP__ADDL__PREM>
				<VCR__UMS__UMP__SW/>
			</SII__VEHICLE__DATA>
			<VCR__PMS__FUTURE__USE/>
			<VCR__CUST__FUTURE__USE/>
			<VCR__YR2000__CUST__USE/>
			<VCR__DUP__KEY__SEQ__NUM/>
		</VEHICLE__COVERAGE__SEG>
	</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>