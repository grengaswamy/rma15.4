<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="Poducer">
		<Producer>
			<ItemIdInfo>
				<AgencyId>
					<xsl:value-of select="PIF__AGENCY__NUMBER"/>
				</AgencyId>
			</ItemIdInfo>
			<GeneralPartyInfo>
				<NameInfo>
					<CommlName>
						<CommercialName/>
						<SupplementaryNameInfo>
							<SupplementaryNameCd/>
							<SupplementaryName/>
						</SupplementaryNameInfo>
					</CommlName>
				</NameInfo>
				<Addr>
					<AddrTypeCd/>
					<Addr1/>
					<Addr2/>
					<Addr3/>
					<Addr4/>
					<City/>
					<StateProvCd/>
					<StateProv/>
					<PostalCode/>
					<CountryCd/>
				</Addr>
				<Communications>
					<PhoneInfo>
						<PhoneTypeCd/>
						<CommunicationUseCd/>
						<PhoneNumber/>
					</PhoneInfo>
				</Communications>
			</GeneralPartyInfo>
			<ProducerInfo>
				<ContractNumber/>
				<ProducerSubCode>
					<xsl:value-of select="PIF__PRODUCER__CODE"/>
				</ProducerSubCode>
			</ProducerInfo>
		</Producer>
	</xsl:template>

	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="Person">
		<GeneralPartyInfo>
			<NameInfo>
				<PersonName>
					<Surname>
						<!--<xsl:value-of select="PIF__ADDRESS__LINE__1"/>-->
					</Surname>
					<GivenName/>
					<OtherGivenName/>
					<TitlePrefix/>
					<NameSuffix/>
				</PersonName>



				<com.csc_LongName>
					<xsl:value-of select="PIF__ADDRESS__LINE__1"/>
				</com.csc_LongName>
				<LegalEntityCd/>
			</NameInfo>
			<Addr>
				<AddrTypeCd/>
				<Addr1>
					<xsl:value-of select="PIF__ADDRESS__LINE__2"/>
				</Addr1>
				<Addr2>
					<xsl:if test="PIF__ADDRESS__LINE__4 != ''">
						<xsl:value-of select="PIF__ADDRESS__LINE__3"/>
					</xsl:if>
				</Addr2>
				<Addr3><xsl:value-of select="PIF__ADDRESS__LINE__2"/></Addr3>
				<Addr4/>
				<xsl:choose>
					<xsl:when test="PIF__ADDRESS__LINE__4 != ''">
						<City>
							<xsl:value-of select="substring-before(PIF__ADDRESS__LINE__4,',')"/>
						</City>
						<StateProvCd>
							<xsl:value-of select="substring-after(PIF__ADDRESS__LINE__4,',')"/>
						</StateProvCd>
					</xsl:when>
					<xsl:when test="PIF__ADDRESS__LINE__3 != '' and PIF__ADDRESS__LINE__4 = ''">
						<City>
							<xsl:value-of select="substring-before(PIF__ADDRESS__LINE__3,',')"/>
						</City>
						<StateProvCd>
							<xsl:value-of select="substring-after(PIF__ADDRESS__LINE__3,',')"/>
						</StateProvCd>
					</xsl:when>
				</xsl:choose>
				<StateProv/>
				<PostalCode>
					<xsl:variable name="PostCd">
						<xsl:if test="string-length(PIF__ZIP__POSTAL__CODE)= '5'">
							<xsl:value-of select="PIF__ZIP__POSTAL__CODE"/>
						</xsl:if>
						<xsl:if test="string-length(PIF__ZIP__POSTAL__CODE)!= '5'">
							<xsl:value-of select="substring(PIF__ZIP__POSTAL__CODE,2,5)"/>
						</xsl:if>
					</xsl:variable>
					<xsl:value-of select="$PostCd"/>
				</PostalCode>
				<CountryCd/>
				<Country/>
				<County/>
			</Addr>
			<Communications>
				<PhoneInfo>
					<PhoneTypeCd/>
					<CommunicationUseCd/>
					<PhoneNumber>
						<!--<xsl:value-of select="PIF__CUST__PHONE"/>-->
					</PhoneNumber>
				</PhoneInfo>
			</Communications>
		</GeneralPartyInfo>
		<InsuredOrPrincipalInfo>
			<InsuredOrPrincipalRoleCd/>
		</InsuredOrPrincipalInfo>
	</xsl:template>

	<xsl:template match="/*/POLICY__INFORMATION__SEG" mode="Policy">
		<xsl:variable name="TranEffYear" select="substring(concat('0000',PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR), string-length(PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR)+1,4)"/>
		<xsl:variable name="TranEffMonth" select="substring(concat('00',PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO), string-length(PIF__EFFECTIVE__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO)+1,2)"/>
		<xsl:variable name="TranEffDay" select="substring(concat('00',PIF__EFFECTIVE__DATE/PIF__EFF__DA), string-length(PIF__EFFECTIVE__DATE/PIF__EFF__DA)+1,2)"/>
		<xsl:variable name="TranEffDate">
			<xsl:call-template name="ConvertS2DateToISODate">
				<xsl:with-param name="Value" select="concat($TranEffYear,$TranEffMonth,$TranEffDay)"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="TranExpYear" select="substring(concat('0000',PIF__EXPIRATION__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR), string-length(PIF__EXPIRATION__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__YR)+1,4)"/>
		<xsl:variable name="TranExpMonth" select="substring(concat('00',PIF__EXPIRATION__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO), string-length(PIF__EXPIRATION__DATE/PIF__EFF__DATE__YR__MO/PIF__EFF__MO)+1,2)"/>
		<xsl:variable name="TranExpDay" select="substring(concat('00',PIF__EXPIRATION__DATE/PIF__EFF__DA), string-length(PIF__EXPIRATION__DATE/PIF__EFF__DA)+1,2)"/>
		<xsl:variable name="TranExpDate">
			<xsl:call-template name="ConvertS2DateToISODate">
				<xsl:with-param name="Value" select="concat($TranEffYear,$TranEffMonth,$TranEffDay)"/>
			</xsl:call-template>
		</xsl:variable>

		<PersPolicy>
			<xsl:attribute name="id">
				<xsl:value-of select="PIF__ISOL__APP__ID"/>
			</xsl:attribute>
			<PolicyNumber>
				<xsl:value-of select="PIF__KEY/PIF__POLICY__NUMBER"/>
			</PolicyNumber>
			<PolicyVersion>
				<xsl:value-of select="PIF__KEY/PIF__MODULE"/>
			</PolicyVersion>
			<CompanyProductCd>
				<xsl:value-of select="normalize-space(PIF__KEY/PIF__SYMBOL)"/>
			</CompanyProductCd>
			<LOBCd>
				<xsl:value-of select="normalize-space(PIF__LINE__BUSINESS)"/>
			</LOBCd>
			<NAICCd>
				<xsl:value-of select="PIF__MASTER__CO__NUMBER"/>
			</NAICCd>
			<ControllingStateProvCd>
				<xsl:call-template name="BuildStateProvCd">
					<xsl:with-param name="StateCd" select="PIF__RISK__STATE__PROV"/>
				</xsl:call-template>
			</ControllingStateProvCd>
			<ContractTerm>
				<EffectiveDt>
					<!--<xsl:call-template name="ConvertS2DateToISODate">
						<xsl:with-param name="Value" select="PIF__EFFECTIVE__DATE"/>
					</xsl:call-template>-->
					<xsl:value-of select="$TranEffDate"/>   
				</EffectiveDt>
				<ExpirationDt>
					<!--<xsl:call-template name="ConvertS2DateToISODate">
						<xsl:with-param name="Value" select="PIF__EXPIRATION__DATE"/>
					</xsl:call-template>-->
					<xsl:value-of select="$TranEffDate"/>  
				</ExpirationDt>
			</ContractTerm>

			<CurrentTermAmt>
				<Amt>
					<!--<xsl:value-of select="PIF__TOTAL__AGEN__PREM"/>-->
					<!--<xsl:value-of select="../*/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__TERM__PREM"/>-->
					<!--<xsl:value-of select="../HOMEOWNERS__PREMIUM__SEG/HOMEOWNERS__BASE__PREMIUM"/>-->
					<xsl:value-of select="../HOMEOWNERS__RATING__SEG/HRR__INFO/HRR__NEW__PREMIUM"/>
				</Amt>
			</CurrentTermAmt>

			<Form>
				<FormNumber/>
			</Form>

			<Loss>
				<CoverageCd/>
				<LossDt/>
				<LossDesc/>
				<ProbableIncurredAmt>
					<Amt/>
				</ProbableIncurredAmt>
			</Loss>
			<OtherOrPriorPolicy>
				<PolicyCd/>
				<PolicyNumber/>
				<LOBCd/>
				<InsurerName/>
				<ContractTerm>
					<ExpirationDt/>
				</ContractTerm>
				<PolicyAmt>
					<Amt/>
				</PolicyAmt>
			</OtherOrPriorPolicy>
			<PaymentOption>
				<PaymentPlanCd>
					<xsl:value-of select="concat(PIF__PAY__SERVICE__CODE,PIF__MODE__CODE)"/>
				</PaymentPlanCd>

				<MethodPaymentCd/>

				<DepositAmt>
					<Amt>
						<xsl:value-of select="../*/PMDGS1__SEGMENT__DATA/PMDGS1__POLICY__DEPOSIT__PREM"/>
					</Amt>
				</DepositAmt>
			</PaymentOption>
			<AccountNumberId>
				<xsl:value-of select="PIF__CUSTOMER__NUMBER"/>
			</AccountNumberId>

			<com.csc_CompanyPolicyProcessingId>
				<xsl:value-of select="PIF__LOCATION"/>
			</com.csc_CompanyPolicyProcessingId>
			<com.csc_InsuranceLineIssuingCompany>
				<!--<xsl:value-of select="PIF__MASTER__CO__NUMBER"/>-->
				<xsl:value-of select="PIF__COMPANY__NUMBER"/>
			</com.csc_InsuranceLineIssuingCompany>
			<com.csc_PolicyTermMonths>
				<xsl:value-of select="PIF__INSTALLMENT__TERM"/>
			</com.csc_PolicyTermMonths>
		</PersPolicy>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="CFWintermediat Response File.xml" userelativepaths="yes" externalpreview="no" url="CFWintermediat Response File.xml" htmlbaseurl="" outputurl="" processortype="internal" profilemode="0" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="CFWintermediat Response File.xml" srcSchemaRoot="HomePolicyQuoteInqRs" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\WaterCraftRS.xml" destSchemaRoot="ACORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->