<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2HOPinfoTemplate">
		<xsl:param name="ProcessingType"/>
		<xsl:variable name="InsOrPrinRoleCd" select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal/InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd"/>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>POLICY__INFORMATION__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<POLICY__INFORMATION__SEG>
				<PIF__REC__LLBB/>
				<PIF__ID>02</PIF__ID>
				<PIF__KEY>
					<PIF__SYMBOL>
						<xsl:value-of select="substring(concat($SYM,'   '),1,3)"/>
					</PIF__SYMBOL>
					<PIF__POLICY__NUMBER>
						<xsl:value-of select="$POL"/>
					</PIF__POLICY__NUMBER>
					<PIF__MODULE>
						<xsl:call-template name="FormatData">
							<xsl:with-param name="FieldName">PIF__MODULE</xsl:with-param>
							<xsl:with-param name="FieldLength">2</xsl:with-param>
							<xsl:with-param name="Value" select="$MOD"/>
							<xsl:with-param name="FieldType">N</xsl:with-param>
						</xsl:call-template>
					</PIF__MODULE>
				</PIF__KEY>
				<PIF__MASTER__CO__NUMBER>
					<xsl:value-of select="$MCO"/>
				</PIF__MASTER__CO__NUMBER>
				<PIF__LOCATION>
					<xsl:value-of select="$LOC"/>
				</PIF__LOCATION>
				<PIF__AMEND__NUMBER>
					<xsl:value-of select="string('00')"/>
				</PIF__AMEND__NUMBER>
				<xsl:variable name="EffDt" select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/EffectiveDt"/>
				<PIF__EFFECTIVE__DATE>
					<xsl:choose>
						<xsl:when test="$ProcessingType = 'Q'">
							<PIF__EFF__DATE__YR__MO>
								<PIF__EFF__YR>
									<xsl:value-of select="substring($EffDt,1,4)"/>
								</PIF__EFF__YR>
								<PIF__EFF__MO>
									<xsl:value-of select="substring($EffDt,6,2)"/>
								</PIF__EFF__MO>
							</PIF__EFF__DATE__YR__MO>
							<PIF__EFF__DA>
								<xsl:value-of select="substring($EffDt,9,2)"/>
							</PIF__EFF__DA>
						</xsl:when>
						<xsl:when test="$ProcessingType = 'I'">
							<PIF__EFF__DATE__YR__MO>
								<PIF__EFF__YR>
									<xsl:value-of select="substring($TransEffDt,1,4)"/>
								</PIF__EFF__YR>
								<PIF__EFF__MO>
									<xsl:value-of select="substring($TransEffDt,6,2)"/>
								</PIF__EFF__MO>
							</PIF__EFF__DATE__YR__MO>
							<PIF__EFF__DA>
								<xsl:value-of select="substring($TransEffDt,9,2)"/>
							</PIF__EFF__DA>
						</xsl:when>
					</xsl:choose>
				</PIF__EFFECTIVE__DATE>
				<xsl:variable name="ExpDt" select="/ACORD/InsuranceSvcRq/*/PersPolicy/ContractTerm/ExpirationDt"/>
				<PIF__EXPIRATION__DATE>
					<PIF__EXP__DATE__YR__MO>
						<PIF__EXP__YR>
							<xsl:value-of select="substring($ExpDt,1,4)"/>
						</PIF__EXP__YR>
						<PIF__EXP__MO>
							<xsl:value-of select="substring($ExpDt,6,2)"/>
						</PIF__EXP__MO>
					</PIF__EXP__DATE__YR__MO>
					<PIF__EXP__DA>
						<xsl:value-of select="substring($ExpDt,9,2)"/>
					</PIF__EXP__DA>
				</PIF__EXPIRATION__DATE>
				<PIF__INSTALLMENT__TERM>
					<xsl:variable name="InstallmentTerm" select="/ACORD/InsuranceSvcRq/*/PersPolicy/com.csc_PolicyTermMonths"/>
					<xsl:value-of select="substring(concat('000',$InstallmentTerm),1+string-length($InstallmentTerm),3)"/>
				</PIF__INSTALLMENT__TERM>
				<PIF__NUMBER__INSTALLMENTS>
					<xsl:value-of select="string('01')"/>
				</PIF__NUMBER__INSTALLMENTS>
				<PIF__RISK__STATE__PROV>
					<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
						<xsl:with-param name="Value" select="/ACORD/InsuranceSvcRq/*/PersPolicy/ControllingStateProvCd"/>
					</xsl:call-template>
				</PIF__RISK__STATE__PROV>
				<PIF__COMPANY__NUMBER>
					<xsl:value-of select="$PCO"/>
				</PIF__COMPANY__NUMBER>
				<PIF__BRANCH>
					<xsl:value-of select="string('00')"/>
				</PIF__BRANCH>
				<PIF__PROFIT__CENTER>
					<xsl:value-of select="string('000')"/>
				</PIF__PROFIT__CENTER>
				<PIF__AGENCY__NUMBER>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/Producer/ItemIdInfo/AgencyId"/>
				</PIF__AGENCY__NUMBER>
				<xsl:variable name="TransRqDt" select="/ACORD/InsuranceSvcRq/*/TransactionRequestDt"/>
				<PIF__ENTERED__DATE>
					<xsl:value-of select="concat(substring($TransRqDt,1,4),substring($TransRqDt,6,2),substring($TransRqDt,9,2))"/>
				</PIF__ENTERED__DATE>
				<PIF__TOTAL__AGEN__PREM>
					<!--<xsl:variable name="TotalAgenPrem" select="/ACORD/InsuranceSvcRq/*/PersPolicy/CurrentTermAmt/Amt"/>
					<xsl:value-of select="substring(concat('000000000000',$TotalAgenPrem),1+string-length($TotalAgenPrem),12) "/>-->
					<xsl:value-of select="string('000000000000')"/>
				</PIF__TOTAL__AGEN__PREM>
				<PIF__LINE__BUSINESS>
					<xsl:value-of select="substring(concat($LOB,'   '),1,3)"/>
				</PIF__LINE__BUSINESS>
				<PIF__ISSUE__CODE>
					<xsl:choose>
						<xsl:when test="$ProcessingType='Q'">
							<xsl:value-of select="string('N')"/>
						</xsl:when>
						<xsl:when test="$ProcessingType='I'">
							<xsl:value-of select="string('N')"/>
						</xsl:when>
						<xsl:when test="$ProcessingType='A'">
							<xsl:value-of select="string('A')"/>
						</xsl:when>
					</xsl:choose>
				</PIF__ISSUE__CODE>
				<PIF__COMPANY__LINE>2</PIF__COMPANY__LINE>
				<PIF__PAY__SERVICE__CODE>
					<xsl:choose>
						<xsl:when test="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,1,1) !=''">
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,1,1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string('D')"/>
						</xsl:otherwise>
					</xsl:choose>
				</PIF__PAY__SERVICE__CODE>
				<PIF__MODE__CODE>
					<xsl:choose>
						<xsl:when test="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,2,1) !=''">
							<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,2,1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string('0')"/>
						</xsl:otherwise>
					</xsl:choose>
				</PIF__MODE__CODE>
				<PIF__AUDIT__CODE>N</PIF__AUDIT__CODE>
				<PIF__KIND__CODE>D</PIF__KIND__CODE>
				<PIF__VARIATION__CODE>
					<xsl:value-of select="string('0')"/>
				</PIF__VARIATION__CODE>
				<xsl:if test="substring($InsOrPrinRoleCd,number(string-length($InsOrPrinRoleCd))-1,2)='NI'">
					<PIF__SORT__NAME>
						<xsl:value-of select="substring(ACORD/InsuranceSvcRq/*/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname,1,4)"/>
					</PIF__SORT__NAME>
				</xsl:if>
				<PIF__PRODUCER__CODE>
					<xsl:variable name="ProdCd" select="/ACORD/InsuranceSvcRq/*/Producer/ProducerInfo/ProducerSubCode"/>
					<xsl:value-of select="substring(concat('00',$ProdCd),1+string-length($ProdCd),2)"/>
				</PIF__PRODUCER__CODE>
				<PIF__UNDERWRITING__CODE>
					<PIF__REVIEW__CODE>N</PIF__REVIEW__CODE>
					<PIF__MVR__REPORT__YEAR>N</PIF__MVR__REPORT__YEAR>
					<PIF__RISK__GRADE__GUIDE>5</PIF__RISK__GRADE__GUIDE>
					<PIF__RISK__GRADE__UNDWR>5</PIF__RISK__GRADE__UNDWR>
					<PIF__RENEWAL__CODE>1</PIF__RENEWAL__CODE>
				</PIF__UNDERWRITING__CODE>
				<PIF__REASON__AMENDED>
					<xsl:value-of select="string('   ')"/>
				</PIF__REASON__AMENDED>
				<PIF__RENEW__PAY__CODE>
					<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,1,1)"/>
				</PIF__RENEW__PAY__CODE>
				<PIF__RENEW__MODE__CODE>
					<xsl:value-of select="substring(/ACORD/InsuranceSvcRq/*/PersPolicy/PaymentOption/PaymentPlanCd,2,1)"/>
				</PIF__RENEW__MODE__CODE>
				<PIF__RENEW__POLICY__SYMBOL>
					<xsl:value-of select="string('   ')"/>
				</PIF__RENEW__POLICY__SYMBOL>
				<PIF__RENEW__POLICY__NUMBER>
					<xsl:value-of select="string('       ')"/>
				</PIF__RENEW__POLICY__NUMBER>
				<PIF__ORIGINAL__INCEPT>
					<xsl:value-of select="string('      ')"/>
				</PIF__ORIGINAL__INCEPT>
				<PIF__CUSTOMER__NUMBER>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/AccountNumberId"/>
				</PIF__CUSTOMER__NUMBER>
				<PIF__SPECIAL__USE__A/>
				<PIF__SPECIAL__USE__B/>
				<xsl:for-each select="/ACORD/InsuranceSvcRq/*/InsuredOrPrincipal">
					<xsl:if test="substring(InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd ,number(string-length(InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd))-1,2)='NI'">
						<PIF__ZIP__POSTAL__CODE>
							<xsl:value-of select="substring(concat(' ',GeneralPartyInfo/Addr/PostalCode),1,6)"/>
						</PIF__ZIP__POSTAL__CODE>
						<PIF__ADDRESS__LINE__1>
							<xsl:if test="string-length(GeneralPartyInfo/NameInfo/com.csc_LongName) &gt; 30">
								<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/com.csc_LongName,1,30)"/>
							</xsl:if>
							<xsl:if test="string-length(GeneralPartyInfo/NameInfo/com.csc_LongName) &lt; 30">
								<xsl:value-of select="substring(GeneralPartyInfo/NameInfo/com.csc_LongName,1,30)"/>
							</xsl:if>
						</PIF__ADDRESS__LINE__1>
						<PIF__ADDRESS__LINE__2>
							<xsl:if test="string-length(GeneralPartyInfo/Addr/Addr1) &gt; 30">
								<xsl:value-of select="substring(GeneralPartyInfo/Addr/Addr1,1,30)"/>
							</xsl:if>
							<xsl:if test="string-length(GeneralPartyInfo/Addr/Addr1) &lt; 30">
								<xsl:value-of select="GeneralPartyInfo/Addr/Addr1"/>
							</xsl:if>
						</PIF__ADDRESS__LINE__2>
						<PIF__ADDRESS__LINE__3>
							<xsl:choose>
								<xsl:when test="normalize-space(GeneralPartyInfo/Addr/Addr2)=''">
									<xsl:value-of select="concat(GeneralPartyInfo/Addr/City,', ',GeneralPartyInfo/Addr/StateProvCd)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:if test="string-length(GeneralPartyInfo/Addr/Addr2) &gt; 30">
										<xsl:value-of select="substring(GeneralPartyInfo/Addr/Addr2,1,30)"/>
									</xsl:if>
									<xsl:if test="string-length(GeneralPartyInfo/Addr/Addr2) &lt; 30">
										<xsl:value-of select="GeneralPartyInfo/Addr/Addr2"/>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</PIF__ADDRESS__LINE__3>
						<PIF__ADDRESS__LINE__4>
							<xsl:choose>
								<xsl:when test="normalize-space(GeneralPartyInfo/Addr/Addr2)=''"></xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="concat(GeneralPartyInfo/Addr/City,', ',GeneralPartyInfo/Addr/StateProvCd)"/>
								</xsl:otherwise>
							</xsl:choose>
						</PIF__ADDRESS__LINE__4>
					</xsl:if>
				</xsl:for-each>
				<PIF__HISTORY__OPTION/>
				<PIF__FINAL__AUDIT__IND/>
				<PIF__SPECIAL__AUTO__CEDED/>
				<PIF__EXCESS__CLAIM__IND/>
				<PIF__PMS__TYPE__LEVEL>
					<xsl:value-of select="string('A')"/>
				</PIF__PMS__TYPE__LEVEL>
				<PIF__ANNIVERSARY__RERATE/>
				<PIF__NAME__PRINT__OPTION>
					<xsl:value-of select="string('0')"/>
				</PIF__NAME__PRINT__OPTION>
				<PIF__LEGAL__ENTITY/>
				<PIF__REINS__PROCESS__IND/>
				<PIF__POLICY__STATUS__ON__PIF/>
				<PIF__ACA__POLICY__CODE>
					<xsl:value-of select="string('N')"/>
				</PIF__ACA__POLICY__CODE>
				<PIF__ACA__DESTINATION/>
				<PIF__PC__ADDRESS__AREA/>
				<PIF__PC__SEQUENCE/>
				<PIF__UK__POSTAL__CODE/>
				<PIF__MOD__NUMBER/>
				<PIF__POLICY__START__TIME__A>
					<xsl:value-of select="string('    ')"/>
				</PIF__POLICY__START__TIME__A>
				<PIF__INSURED__OCCUPATION__CODE/>
				<PIF__INSURED__OCCUPATION__CODE__X/>
				<PIF__USA__INDICATORS/>
				<PIF__DEC__CHANGE__SW/>
				<PIF__PMS__FUTURE__USE/>
				<PIF__ISOL__APP__ID>
					<xsl:value-of select="/ACORD/InsuranceSvcRq/*/PersPolicy/@id"/>
				</PIF__ISOL__APP__ID>
				<PIF__CUST__FUTURE__USE/>
				<PIF__YR2000__CUST__USE/>
			</POLICY__INFORMATION__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet>