<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
***********************************************************************************************
This XSL stylesheet is used by Generic rating to transform XML from the iSolutions database
into the ACORD XML Quote business message before sending to the Communications Frameworks   
E-Service case .NET 
***********************************************************************************************
-->
  <xsl:include href="S2SignOnRs.xsl"/>
  <!-- Issue 80442 Begin -->
  <xsl:include href="S2CommonFuncRs.xsl"/>
  <xsl:include href="S2ErrorRs.xsl"/>
  <xsl:include href="S2ProducerRs.xsl"/>
  <xsl:include href="S2InsuredOrPrincipalRs.xsl"/>
  <xsl:include href="S2MessageStatusRs.xsl"/>
  <xsl:include href="S2CommlPolicyRs.xsl"/>
  <xsl:include href="S2CreditSurchargeRs.xsl"/>
  <xsl:include href="S2FormsRs.xsl"/>
  <xsl:include href="S2CACommlRateStateRs.xsl"/>
  <xsl:include href="S2CACommlVehRs.xsl"/>
  <xsl:include href="S2AdditionalInterestRs.xsl"/>
  <xsl:include href="S2CACommlCoverageRs.xsl"/>
  <!-- Issue 80442 End -->

  <xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>

	<xsl:template match="/">
		<ACORD>
		      <xsl:call-template name="BuildSignOn"/>
			<InsuranceSvcRs>
			        <RqUID/>
				<CommlAutoPolicyAddRs>
					<!-- Issue 80442 Begin
					<MsgStatus>
						<xsl:for-each select="CommlAutoPolicyAddRs/POLICY__INFORMATION__SEG/PIF__ISOL__STATUS">
							<MsgStatusCd>
								<xsl:value-of select="."/>
							</MsgStatusCd>
						</xsl:for-each>
						<xsl:for-each select="CommlAutoPolicyAddRs/POLICY__INFORMATION__SEG/PIF__ISOL__ERROR__CD">
							<MsgErrorCd>
								<xsl:value-of select="."/>
							</MsgErrorCd>
						</xsl:for-each>
						<xsl:for-each select="CommlAutoPolicyAddRs/POLICY__INFORMATION__SEG/PIF__ISOL__ERROR__DESC">
							<MsgStatusDesc>
								<xsl:value-of select="."/>
							</MsgStatusDesc>
						</xsl:for-each>
					</MsgStatus>
					<CommlPolicy>
						<xsl:attribute name="id">
							<xsl:value-of select="CommlAutoPolicyAddRs/POLICY__INFORMATION__SEG/PIF__ISOL__APP__ID"/>
						</xsl:attribute>
					</CommlPolicy>	-->

				        <RqUID/>
				          <TransactionResponseDt/>
				          <TransactionEffectiveDt/>
				          <CurCd>USD</CurCd>
					  <xsl:call-template name="BuildMessageStatus">
						<xsl:with-param name="MessageType">
							<xsl:value-of select="string('RATING')"/>
						</xsl:with-param>
					  </xsl:call-template>

					<xsl:for-each select="CommlAutoPolicyAddRs/POLICY__INFORMATION__SEG">
						<xsl:variable name="LOB" select="string('CA')"/>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Producer"/>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="Business"/>
							<xsl:apply-templates select="/*/POLICY__INFORMATION__SEG" mode="CommlPolicy">
								<xsl:with-param name="LOB" select="$LOB"/>
							</xsl:apply-templates>
					</xsl:for-each>

					<CommlAutoLineBusiness>
						<LOBCd>CA</LOBCd>
						<xsl:call-template name="CreateCommlRateState"/>
					</CommlAutoLineBusiness>
					<!-- Issue 80442 End -->
				</CommlAutoPolicyAddRs>
			</InsuranceSvcRs>
		</ACORD>
	</xsl:template>

</xsl:stylesheet>