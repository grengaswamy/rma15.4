<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="xml" indent="yes"/>
<!--
***********************************************************************************************
This XSL stylesheet is used by CFW to transform Series II data into Internal XML prior to 
populating the iSolutions database.
E-Service case 34768 
***********************************************************************************************
-->
	
	<xsl:template name="SeriesIIErrorsRs">
		<xsl:for-each select="WorkCompPolicyQuoteInqRs/POLICY__INFORMATION__SEG/PIF__ISOL__STATUS">
			<xsl:choose>
				<xsl:when test="WorkCompPolicyQuoteInqRs/POLICY__INFORMATION__SEG/PIF__ISOL__ERROR__CD > 0">
					<MsgStatus>
						<MsgStatusCd>
							<xsl:value-of select="WorkCompPolicyQuoteInqRs/POLICY__INFORMATION__SEG/PIF__ISOL__STATUS"/>
						</MsgStatusCd>
						<MsgErrorCd>
							<xsl:value-of select="WorkCompPolicyQuoteInqRs/POLICY__INFORMATION__SEG/PIF__ISOL__ERROR__CD"/>
						</MsgErrorCd>
						<MsgStatusDesc>
							<xsl:value-of select="WorkCompPolicyQuoteInqRs/POLICY__INFORMATION__SEG/PIF__ISOL__ERROR__DESC"/>
						</MsgStatusDesc>
					</MsgStatus>
				</xsl:when>
				<xsl:otherwise>
					<MsgStatus>
						<MsgStatusCd><xsl:text>INFO</xsl:text></MsgStatusCd>
						<MsgErrorCd>0</MsgErrorCd>
						<MsgStatusDesc>Success</MsgStatusDesc>
					</MsgStatus>
				</xsl:otherwise>
        		</xsl:choose>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
