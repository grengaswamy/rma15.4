<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="CovPremiumAmt" match="/">
		<xsl:for-each select="/*/PMDNXB1__SUB__LOC__RATING__SEG">
			<PropertyInfo>
				<CommlPropertyInfo>
					<xsl:for-each select="PMDNXB1__SEGMENT__KEY/PMDNXB1__LEVEL__KEY/PMDNXB1__SUB__LOCATION__NUMBER__A">
						<xsl:variable name="UY-LOCNUM" select="../PMDNXB1__LOCATION__NUMBER__A/PMDNXB1__LOCATION__NUMBER"/>
						<xsl:variable name="UY-SUBLOCNUM" select="PMDNXB1__SUB__LOCATION__NUMBER"/>

						<!-- Process Optional, Building and Propery And Insurance Line Coverage here-->
						<!-- Generate LocationRef and SubLocationRef -->
						<xsl:attribute name="LocationRef">l<xsl:value-of select="string(number($UY-LOCNUM))"/></xsl:attribute>
						<xsl:attribute name="SubLocationRef">s<xsl:value-of select="string(number($UY-SUBLOCNUM))"/></xsl:attribute>
						<SubjectInsuranceCd/>
						<xsl:for-each select="/*/PMDUYB1__RATE__OVERRIDE__SEG[PMDUYB1__SEGMENT__KEY/PMDUYB1__LEVEL__KEY/PMDUYB1__LOCATION__NUMBER__A/PMDUYB1__LOCATION__NUMBER=$UY-LOCNUM and PMDUYB1__SEGMENT__KEY/PMDUYB1__LEVEL__KEY/PMDUYB1__SUB__LOCATION__NUMBER__A/PMDUYB1__SUB__LOCATION__NUMBER=$UY-SUBLOCNUM]">
							<xsl:variable name="UY-SUBOFINS" select="PMDUYB1__SEGMENT__KEY/PMDUYB1__LEVEL__KEY/PMDUYB1__RISK__UNIT__GROUP__KEY/PMDUYB1__PMS__DEF__SUBJ__OF__INS/PMDUYB1__SUBJ__OF__INS"/>
							<CommlCoverage>
								<CoverageCd>
									<xsl:call-template name="ConvertSubjectOfInsToCovCode">
										<xsl:with-param name="Value" select="PMDUYB1__SEGMENT__KEY/PMDUYB1__LEVEL__KEY/PMDUYB1__RISK__UNIT__GROUP__KEY/PMDUYB1__PMS__DEF__SUBJ__OF__INS/PMDUYB1__SUBJ__OF__INS"/>
									</xsl:call-template>
								</CoverageCd>
								<IterationNumber>1</IterationNumber>
								<CoverageDesc/>
								<Limit>
									<FormatCurrencyAmt>
										<Amt>
											<!--<xsl:value-of select="/*/PMDUXB1__UNIT__GRP__RATING__SEG[PMDUXB1__SEGMENT__KEY/PMDUXB1__LEVEL__KEY/PMDUXB1__LOCATION__NUMBER__A/PMDUXB1__LOCATION__NUMBER=$UY-LOCNUM and PMDUXB1__SEGMENT__KEY/PMDUXB1__LEVEL__KEY/PMDUXB1__SUB__LOCATION__NUMBER__A/PMDUXB1__SUB__LOCATION__NUMBER=$UY-SUBLOCNUM]/PMDUXB1__SEGMENT__DATA/PMDUXB1__COMMON__DATA__NEW/PMDUXB1__LIMITA__EXP__A/PMDUXB1__LIMITA__EXP"/>-->
											<xsl:value-of select="/*/PMDUXB1__UNIT__GRP__RATING__SEG[PMDUXB1__SEGMENT__KEY/PMDUXB1__LEVEL__KEY/PMDUXB1__LOCATION__NUMBER__A/PMDUXB1__LOCATION__NUMBER=$UY-LOCNUM and PMDUXB1__SEGMENT__KEY/PMDUXB1__LEVEL__KEY/PMDUXB1__SUB__LOCATION__NUMBER__A/PMDUXB1__SUB__LOCATION__NUMBER=$UY-SUBLOCNUM and PMDUXB1__SEGMENT__KEY/PMDUXB1__LEVEL__KEY/PMDUXB1__RISK__UNIT__GROUP__KEY/PMDUXB1__PMS__DEF__SUBJ__OF__INS/PMDUXB1__SUBJ__OF__INS=$UY-SUBOFINS]/PMDUXB1__SEGMENT__DATA/PMDUXB1__COMMON__DATA__NEW/PMDUXB1__LIMITA__EXP__A/PMDUXB1__LIMITA__EXP"/>
										</Amt>
									</FormatCurrencyAmt>
									<LimitAppliesToCd/>
								</Limit>
								<Deductible>
									<FormatCurrencyAmt>
										<Amt>
											<xsl:value-of select="/*/PMDUXB1__UNIT__GRP__RATING__SEG[PMDUXB1__SEGMENT__KEY/PMDUXB1__LEVEL__KEY/PMDUXB1__LOCATION__NUMBER__A/PMDUXB1__LOCATION__NUMBER=$UY-LOCNUM and PMDUXB1__SEGMENT__KEY/PMDUXB1__LEVEL__KEY/PMDUXB1__SUB__LOCATION__NUMBER__A/PMDUXB1__SUB__LOCATION__NUMBER=$UY-SUBLOCNUM and PMDUXB1__SEGMENT__KEY/PMDUXB1__LEVEL__KEY/PMDUXB1__RISK__UNIT__GROUP__KEY/PMDUXB1__PMS__DEF__SUBJ__OF__INS/PMDUXB1__SUBJ__OF__INS=$UY-SUBOFINS]/PMDUXB1__SEGMENT__DATA/PMDUXB1__COMMON__DATA__NEW/PMDUXB1__DEDUCTIBLE__EXP__A/PMDUXB1__DEDUCTIBLE__EXP"/>
										</Amt>
									</FormatCurrencyAmt>
								</Deductible>
								<CurrentTermAmt>
									<Amt>
										<xsl:value-of select="PMDUYB1__SEGMENT__DATA/PMDUYB1__FINAL__PREM__A/PMDUYB1__FINAL__PREM"/>
									</Amt>
								</CurrentTermAmt>
							</CommlCoverage>
						</xsl:for-each>
					</xsl:for-each>
				</CommlPropertyInfo>
			</PropertyInfo>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="8.077766832400003008BORSInternalS2_03.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->