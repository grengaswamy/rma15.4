<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2HODescLocationTemplate">
		<xsl:for-each select="/ACORD/InsuranceSvcRq/*/Location">
			<BUS__OBJ__RECORD>
				<RECORD__NAME__ROW>
					<RECORD__NAME>DESCRIPTION__FULL__SEG</RECORD__NAME>
				</RECORD__NAME__ROW>
				<DESCRIPTION__FULL__SEG>
					<DESCRIPTION__SEG>
						<DESCRIPTION__REC__LLBB>
							<DESCRIPTION__REC__LENGTH/>
							<DESCRIPTION__ACTION__CODE/>
							<DESCRIPTION__FILE__ID/>
						</DESCRIPTION__REC__LLBB>
					</DESCRIPTION__SEG>
					<DESCRIPTION__ID>12</DESCRIPTION__ID>
					<DESCRIPTION__KEY>
						<DESCRIPTION__USE>
							<USE__CODE>LA</USE__CODE>
							<USE__LOCATION>
								<xsl:call-template name="FormatData">
									<xsl:with-param name="FieldName">USE__LOCATION</xsl:with-param>
									<xsl:with-param name="FieldLength">3</xsl:with-param>
									<xsl:with-param name="Value" select="parent::Location/ItemIdInfo/InsurerId"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template>
							</USE__LOCATION>
						</DESCRIPTION__USE>
						<DESCRIPTION__SEQUENCE>0</DESCRIPTION__SEQUENCE>
					</DESCRIPTION__KEY>
					<DESCRIPTION__INFORMATION>
						<DESCRIPTION__SORT__NAME>
							<xsl:value-of select="substring(../InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname,1,4)"/>
						</DESCRIPTION__SORT__NAME>
						<DESCRIPTION__ZIP__FILLER>0</DESCRIPTION__ZIP__FILLER>
						<DESCRIPTION__ZIP__CODE>
							<xsl:value-of select="substring(Addr/PostalCode,1,5)"/>
						</DESCRIPTION__ZIP__CODE>
						<DESCRIPTION__LINE__1>
							<xsl:value-of select="substring(../InsuredOrPrincipal/GeneralPartyInfo/NameInfo/com.csc_LongName,1,30)"/>
						</DESCRIPTION__LINE__1>
						<DESCRIPTION__LINE__2>
							<xsl:value-of select="substring(Addr/Addr1,1,30)"/>
						</DESCRIPTION__LINE__2>
						<DESCRIPTION__LINE__3>
							<xsl:value-of select="substring(Addr/Addr2,1,30)"/>
						</DESCRIPTION__LINE__3>
						<DESCRIPTION__LINE__4>
							<xsl:if test="string-length(Addr/City) &lt; 27">
								<xsl:value-of select="concat(Addr/City,', ')"/>
								<xsl:value-of select="Addr/StateProvCd"/>
							</xsl:if>
							<xsl:if test="string-length(Addr/City) &gt; 26">
								<xsl:value-of select="concat(substring(Addr/City,1,26),', ')"/>
								<xsl:value-of select="Addr/StateProvCd"/>
							</xsl:if>
						</DESCRIPTION__LINE__4>
					</DESCRIPTION__INFORMATION>
					<DESCRIPTION__NAME__CODE/>
					<DESC__UK__POSTAL__CODE/>
					<DESC__MISC__ADJ__ZIP__CODE/>
					<DESC__MISC__ADJ__TAX__ID/>
					<DESC__PMS__FUTURE__USE/>
					<DESCRIPTION__LOAN__NUMBER/>
					<DESC__ACA__IDENTIFIER__SAVE/>
					<DESC__CUST__FUTURE__USE/>
					<DESC__YR2000__CUST__USE/>
					<DESC__DUP__KEY__SEQ__NUM/>
				</DESCRIPTION__FULL__SEG>
			</BUS__OBJ__RECORD>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="WaterCraft.xml" userelativepaths="yes" externalpreview="no" url="WaterCraft.xml" htmlbaseurl="" outputurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="..\WaterCraft.xml" srcSchemaRoot="ACORD" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\desc53.xml" destSchemaRoot="BUS__OBJ__RECORD" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->