<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output method="xml" indent="yes"/>

<!-- Issue 80442 New Template -->
<xsl:template name="S2CACommonCoverages">
	<xsl:param name="VehCount"/>

	<xsl:variable name="HiredCount" select="count(//ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoHiredInfo/CommlCoverage)"/>
	<xsl:variable name="NonOwnCount" select="count(//ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoNonOwnedInfo/NonOwnedInfo/CommlCoverage)"/>

	<xsl:for-each select="//ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoHiredInfo/CommlCoverage">
		<xsl:call-template name="S2CACommonCovSeg0">
			<xsl:with-param name="VehNum" select="$VehCount + 9 + position()"/>
			<xsl:with-param name="CovType" select="string('HIRED AUTO')"/>
			<xsl:with-param name="CovState" select="../../StateProvCd"/>
			<xsl:with-param name="ClassCd" select="string('6619')"/>
			<xsl:with-param name="NumEmp" select="string('00')"/>
		</xsl:call-template>

		<xsl:call-template name="S2CACommonCovSeg1">
			<xsl:with-param name="VehNum" select="$VehCount + 9 + position()"/>
		</xsl:call-template>
	</xsl:for-each>



	<xsl:for-each select="//ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoNonOwnedInfo/NonOwnedInfo/CommlCoverage">
		<xsl:variable name="NumEmp" select="../../NonOwnedInfo[NonOwnedGroupTypeCd = 'E']/NumNonOwned"/>
		<xsl:variable name="ClassCode">
			<xsl:choose>
				<xsl:when test="$NumEmp &lt; '26'">6601</xsl:when>
				<xsl:when test="$NumEmp &lt; '101'">6602</xsl:when>
				<xsl:when test="$NumEmp &lt; '501'">6603</xsl:when>
				<xsl:when test="$NumEmp &lt; '1001'">6604</xsl:when>
				<xsl:otherwise>6605</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:call-template name="S2CACommonCovSeg0">
			<xsl:with-param name="VehNum" select="$VehCount + 9 + $HiredCount + position()"/>
			<xsl:with-param name="CovType" select="string('NON-OWNED')"/>
			<xsl:with-param name="CovState" select="../../../StateProvCd"/>
			<xsl:with-param name="ClassCd" select="$ClassCode"/>
			<xsl:with-param name="NumEmp" select="$NumEmp"/>
		</xsl:call-template>

		<xsl:call-template name="S2CACommonCovSeg1">
			<xsl:with-param name="VehNum" select="$VehCount + 9 + $HiredCount + position()"/>
		</xsl:call-template>
	</xsl:for-each>




	<xsl:for-each select="//ACORD/InsuranceSvcRq/*/CommlAutoLineBusiness/CommlRateState/CommlAutoDriveOtherCarInfo/CommlCoverage">
		<xsl:call-template name="S2CACommonCovSeg0">
			<xsl:with-param name="VehNum" select="$VehCount + 9 + $HiredCount + $NonOwnCount + position()"/>
			<xsl:with-param name="CovType" select="string('DOC')"/>
			<xsl:with-param name="CovState" select="../../StateProvCd"/>
			<xsl:with-param name="ClassCd" select="string('9020')"/>
			<xsl:with-param name="NumEmp" select="../NumIndividualsCovered"/>
		</xsl:call-template>

		<xsl:call-template name="S2CACommonCovSeg1">
			<xsl:with-param name="VehNum" select="$VehCount + 9 + $HiredCount + $NonOwnCount + position()"/>
		</xsl:call-template>
	</xsl:for-each>

</xsl:template>

<xsl:template name="S2CACommonCovSeg0">
	<xsl:param name="VehNum"/>
	<xsl:param name="CovType"/>
	<xsl:param name="CovState"/>
	<xsl:param name="ClassCd"/>
	<xsl:param name="NumEmp"/>

		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>VEHICLE__DESCRIPTION__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<VEHICLE__DESCRIPTION__SEG>
				<USVD__REC__LLBB>
					<USVD__REC__LENGTH/>
					<USVD__ACTION__CODE/>
					<USVD__FILE__ID/>
				</USVD__REC__LLBB>
				<USVD__ID>35</USVD__ID>
				<USVD__KEY>
					<USVD__UNIT__NUMBER>
						<xsl:call-template name="FormatData">
							<xsl:with-param name="FieldName">UNIT</xsl:with-param>
							<xsl:with-param name="FieldLength">3</xsl:with-param>
							<xsl:with-param name="Value" select="$VehNum"/>
							<xsl:with-param name="FieldType">N</xsl:with-param>
						</xsl:call-template>
					</USVD__UNIT__NUMBER>
					<USVD__SEGMENT__NUMBER>0</USVD__SEGMENT__NUMBER>
					<USVD__AMENDMENT__NUMBER/>
				</USVD__KEY>
				<USVD__STATE>
					<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
  						<xsl:with-param name="Value" select="$CovState"/>
					</xsl:call-template>
				</USVD__STATE>
					<USVD__TERRITORY>001</USVD__TERRITORY>
				<USVD__YEAR__MAKE>
					<USVD__YEAR__CC/>
					<USVD__YEAR__YY/>
				</USVD__YEAR__MAKE>
				<USVD__MAKE__DESC>
					<xsl:value-of select="$CovType"/>
				</USVD__MAKE__DESC>
				<USVD__SERIAL__NUMBER/>
				<USVD__TYPE__VEHICLE>
					<xsl:value-of select="string('CC')"/>
				</USVD__TYPE__VEHICLE>
				<USVD__PERFORMANCE/>
				<USVD__BODY/>
				<USVD__COST__NEW>
					<xsl:choose>
						<xsl:when test="$CovType = 'HIRED AUTO'">
							<xsl:value-of select="string('     ')"/>
						</xsl:when>
						<xsl:otherwise>00000</xsl:otherwise>
					</xsl:choose>
				</USVD__COST__NEW>
				<USVD__SYMBOL>
					<xsl:choose>
						<xsl:when test="$CovType = 'DOC'">
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">SYMBOL</xsl:with-param>
								<xsl:with-param name="FieldLength">2</xsl:with-param>
								<xsl:with-param name="Value" select="$NumEmp"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string('  ')"/>
						</xsl:otherwise>
					</xsl:choose>
				</USVD__SYMBOL>
				<USVD__DRIVER__ASSIGNED/>
				<USVD__USE__CODE/>
				<USVD__MILES__TO__WORK/>
				<USVD__DAYS__PER__WEEK/>
				<USVD__ANNUAL__MILES/>
				<USVD__VEH__CLASS__CODE>
					<xsl:value-of select="$ClassCd"/>
				</USVD__VEH__CLASS__CODE>
				<USVD__DRIVER__CLASS/>
				<USVD__MULTICAR__OVERRIDE/>
				<USVD__ANTILOCK__DISC/>
				<USVD__PIP__RATING__BASIS/>
				<USVD__RENEWAL__PR__VEHICLE/>
				<USVD__ANTITHEFT__DEVICE>
					<xsl:value-of select="string('N')"/>
				</USVD__ANTITHEFT__DEVICE>
				<USVD__PASSIVE__RESTRAINT/>
				<USVD__SPECIAL__USE/>
				<USVD__ZIP__POSTAL__CODE/>
				<USVD__TAX__LOCATION>0</USVD__TAX__LOCATION>
				<USVD__CONTROL/>
				<USVD__PURCHASE__DATE>
					<USVD__PURCHASE__DATE__CCYY>
						<USVD__PURCHASE__DATE__CC/>
						<USVD__PURCHASE__DATE__YY/>
					</USVD__PURCHASE__DATE__CCYY>
					<USVD__PURCHASE__DATE__MM/>
					<USVD__PURCHASE__DATE__DD/>
				</USVD__PURCHASE__DATE>
				<USVD__VEH__WEIGHT/>
				<USVD__STATED__AMOUNT__COLLISION/>
				<USVD__ANTIQUE__AUTO__VALUE/>
				<USVD__TORT__VALUE>
					<USVD__TORT__INDICATOR/>
					<fill_5/>
					<USVD__TORT__5/>
				</USVD__TORT__VALUE>
				<USVD__CUBIC__CENTIMETERS>
					<xsl:choose>
						<xsl:when test="$CovType = 'NON-OWNED'">
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">CUBIC__CM</xsl:with-param>
								<xsl:with-param name="FieldLength">4</xsl:with-param>
								<xsl:with-param name="Value" select="$NumEmp"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string('     ')"/>
						</xsl:otherwise>
					</xsl:choose>
				</USVD__CUBIC__CENTIMETERS>
				<USVD__CUBIC__CENTIMETERS__NUM/>
				<USVD__NUMBER__EMPL/>
				<USVD__COMMUTER__DISCOUNT/>
				<USVD__ALCOHOL__DRUG__DISC/>
				<USVD__STATED__AMOUNT>
					<xsl:choose>
						<xsl:when test="$CovType='HIRED AUTO'">
							<xsl:call-template name="FormatData">
								<xsl:with-param name="FieldName">STATED__AMOUNT</xsl:with-param>
								<xsl:with-param name="FieldLength">5</xsl:with-param>
								<xsl:with-param name="Value" select="../HiredLiabilityCostAmt/Amt"/>
								<xsl:with-param name="FieldType">N</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="string('00000')"/></xsl:otherwise>
					</xsl:choose>
				</USVD__STATED__AMOUNT>
				<USVD__STATED__AMOUNT__NUM/>
					<USVD__VIN/>
				<USVD__PL__NUM__TYPE>
					<USVD__PLATE__TYPE/>
					<USVD__PLATE__COLOR/>
					<USVD__PLATE__NUMBER/>
				</USVD__PL__NUM__TYPE>
				<USVD__SAFE__DRIVER__DISCOUNT/>
				<USVD__HAP__MOD__INDICATOR/>
				<USVD__COL__CLASS/>
				<USVD__FARTHEST__ZONE/>
				<USVD__CUSTOMER__DISCOUNT>
					<USVD__CUST__DISC__TYPES/>
					<fill_6/>
				</USVD__CUSTOMER__DISCOUNT>
				<USVD__SIZE/>
				<USVD__COMM__USE/>
				<USVD__RADIUS/>
				<USVD__SPECIAL__INDUSTRY/>
				<USVD__SCHOOL__BUS__POL__SUBDIV/>
				<USVD__SCHOOL__CHART__REG__PLATE/>
				<USVD__NON__PROFIT__RELIG__ORG/>
				<USVD__HA__DIRECT__EXCESS__SW/>
				<USVD__RIDE__SHARING/>
				<USVD__MUN__LIAB__VOL__WORKER/>
				<USVD__DRIVE__TRAIN__DUAL__CONTROL/>
				<USVD__SNOWMOBILE__PASS__HAZARD/>
				<USVD__RATE__BOOK__ID__STORE>
					<xsl:value-of select="string('A')"/>
				</USVD__RATE__BOOK__ID__STORE>
				<USVD__LEASED__AUTO__INDICATOR>
					<xsl:value-of select="string('N')"/>
				</USVD__LEASED__AUTO__INDICATOR>
				<USVD__FLEET__INDICATOR/>
				<USVD__CEDED__INDICATOR/>
				<USVD__REG__TERRITORY/>
				<USVD__STAT__TERRITORY/>
				<USVD__CPP__PACK__MOD__ASSIGN/>
				<USVD__POINT__DRIVER/>
				<USVD__PASS__OR__FAIL/>
				<USVD__MIN__RETAINED__SW/>
				<USVD__SUBSTITUTE/>
				<USVD__DDC__FACTOR>
					<xsl:value-of select="string('000000')"/>
				</USVD__DDC__FACTOR>
				<USVD__DDC__INDICATOR/>
				<USVD__ANTI__THEFT__FACTOR/>
				<USVD__PASS__RESTRAINT__FACT/>
				<USVD__GST__INDICATOR/>
				<USVD__GDD__INDICATOR/>
				<USVD__DTD__INDICATOR/>
				<USVD__GDD__FACTOR/>
				<USVD__DGD__FACTOR/>
				<USVD__DGD__INDICATOR/>
				<USVD__TOTAL__OPERATORS/>
				<USVD__YOUTHFUL__OPERATORS/>
				<USVD__POLICY__OPERATORS/>
				<USVD__TOTAL__VEHICLES/>
				<USVD__ANTILOCK__FACTOR/>
				<USVD__NH__SDIP__PERCENT/>
				<fill_2>
				<!-- USVD__CC__TYPE Not defined in the copybook -->
					<xsl:choose>
						<xsl:when test="$CovType='HIRED AUTO'"><xsl:value-of select="string('H            ')"/></xsl:when>
						<xsl:when test="$CovType='DOC'"><xsl:value-of select="string('B            ')"/></xsl:when>
						<xsl:when test="$CovType='NON-OWNED'"><xsl:value-of select="string('N            ')"/></xsl:when>
					</xsl:choose>
				<!-- /USVD__CC__TYPE -->
				</fill_2>
				<USVD__APC__FACTOR/>
				<USVD__APC__INDICATOR/>
				<USVD__WLW__FACTOR/>
				<USVD__WLW__INDICATOR/>
				<USVD__CDD__INDICATOR/>
				<USVD__VEHICLE__ADD__DATE/>
				<USVD__XX__RATE__BOOK__ID__TABLE>
					<USVD__XX__RATE__BOOK__ID__DATA>
						<USVD__XX__COV__RATE__BOOK__ID/>
					</USVD__XX__RATE__BOOK__ID__DATA>
				</USVD__XX__RATE__BOOK__ID__TABLE>
				<USVD__PMS__FUTURE__USE/>
				<USVD__CUST__FUTURE__USE>
					<USVD__VEH__MS__MC__CLASS>
						<USVD__VEH__MS__MC__PRI__CLASS/>
						<USVD__VEH__MS__MC__SEC__CLASS/>
					</USVD__VEH__MS__MC__CLASS>
					<fill_3/>
				</USVD__CUST__FUTURE__USE>
				<USVD__YR2000__CUST__USE/>
				<USVD__DAY__LIGHTS__FIELDS>
					<USVD__DAY__LIGHTS__DISC__FLDS>
							<USVD__DAY__LIGHTS__IND/>
					</USVD__DAY__LIGHTS__DISC__FLDS>
				</USVD__DAY__LIGHTS__FIELDS>
				<USVD__DUP__KEY__SEQ__NUM/>
			</VEHICLE__DESCRIPTION__SEG>
		</BUS__OBJ__RECORD>
</xsl:template>


<xsl:template name="S2CACommonCovSeg1">
	<xsl:param name="VehNum"/>

	<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>VEHICLE__COVERAGE__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
		<VEHICLE__COVERAGE__SEG>
			<VCR__REC__LLBB>
				<VCR__REC__LENGTH/>
				<VCR__ACTION__CODE/>
				<VCR__FILE__ID/>
			</VCR__REC__LLBB>
			<VCR__ID>35</VCR__ID>
			<VCR__KEY>
				<VCR__UNIT__NUMBER>
					<xsl:call-template name="FormatData">
						<xsl:with-param name="FieldName">UNIT</xsl:with-param>
						<xsl:with-param name="FieldLength">3</xsl:with-param>
						<xsl:with-param name="Value" select="$VehNum"/>
						<xsl:with-param name="FieldType">N</xsl:with-param>
					</xsl:call-template>
				</VCR__UNIT__NUMBER>
				<VCR__SEGMENT__NUMBER>1</VCR__SEGMENT__NUMBER>
				<VCR__AMEND__NUMBER/>
			</VCR__KEY>
			<VCR__CHANGE__EFFECT__DATE>
				<ED__YEAR>
					<ED__YEAR__CC><xsl:value-of select="string('  ')"/></ED__YEAR__CC>
					<ED__YEAR__YY><xsl:value-of select="string('  ')"/></ED__YEAR__YY>
				</ED__YEAR>
				<ED__MONTH><xsl:value-of select="string('  ')"/></ED__MONTH>
				<ED__DAY><xsl:value-of select="string('  ')"/></ED__DAY>
			</VCR__CHANGE__EFFECT__DATE>
			<SII__VEHICLE__DATA>
				<VEHICLE__COVERAGE__CONTROL>
					<VEHICLE__REASON__AMENDED/>
					<VEHICLE__TOTAL__PREMIUM>
						<xsl:value-of select="string('00000000')"/>
					</VEHICLE__TOTAL__PREMIUM>
					<VEHICLE__OLD__PREMIUM>
						<xsl:value-of select="string('00000000')"/>
					</VEHICLE__OLD__PREMIUM>
					</VEHICLE__COVERAGE__CONTROL>
				<VCR__TABLE__OF__COVERAGES>
					<!-- xsl:for-each select="CommlCoverage" -->
						<VCR__COVERAGE__DATA>
							<xsl:attribute name="index"><xsl:value-of select="position() - 1"/></xsl:attribute>
							<VCR__COVERAGE__CODE>
								<xsl:value-of select="CoverageCd"/>
							</VCR__COVERAGE__CODE>
							<VCR__COVERAGE__LIMIT>
							<xsl:choose>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt='N'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt='None'">
											<xsl:value-of select="string('')"/>
								</xsl:when>
								<xsl:when test="Option/OptionTypeCd='YNIn'">
											<xsl:value-of select="substring(concat(substring(Option/OptionValue,1,1),'                '), 1, 17)"/>
								</xsl:when>
								<xsl:when test="Deductible/FormatCurrencyAmt/Amt=''">
											<xsl:value-of select="substring(concat(substring(Limit/FormatCurrencyAmt/Amt,1,7),'               '), 1, 17)"/>
								</xsl:when>
								<xsl:when test="Limit/FormatCurrencyAmt/Amt=''">
									<xsl:value-of select="substring(concat(substring(Deductible/FormatCurrencyAmt/Amt,1,7),'               '),1,17)"/>
								</xsl:when>
								<xsl:when test="string-length(Limit/FormatCurrencyAmt/Amt) > '0'">
											<xsl:value-of select="Limit/FormatCurrencyAmt/Amt"/>
								</xsl:when>
								<xsl:when test="string-length(Deductible/FormatCurrencyAmt/Amt) >'0'">
									<xsl:value-of select="Deductible/FormatCurrencyAmt/Amt"/>
								</xsl:when>
								<xsl:otherwise >
									<xsl:value-of select="substring(concat(substring(Limit/FormatCurrencyAmt/Amt,1,7),'       '),1,7)"/>
								</xsl:otherwise>
							</xsl:choose>
							</VCR__COVERAGE__LIMIT>
							<VCR__COVERAGE__PREMIUM__ALPH>
								<xsl:value-of select="string('       ')"/>
							</VCR__COVERAGE__PREMIUM__ALPH>
							<VCR__COVERAGE__RATE__BOOK__ID/>
						</VCR__COVERAGE__DATA>
					<!-- /xsl:for-each -->
				</VCR__TABLE__OF__COVERAGES>
				<VCR__HAP__MOD__INDICATOR/>
				<VCR__FACTOR__HOLD__AREA/>
				<VCR__TEXAS__RATING__FACTORS>
					<COMM__TEX__LIAB__PRIM__FAC/>
					<COMM__TEX__COL__PRIM__FAC/>
					<COMM__TEX__OTC__PRIM__FAC/>
					<COMM__TEX__SECONDARY__FAC/>
					<fill_7/>
				</VCR__TEXAS__RATING__FACTORS>
				<VCR__MISC__RATING__FACTORS>
					<COMM__MV__LIAB__PRIM__FAC/>
					<COMM__MV__COL__PRIM__FAC/>
					<COMM__MV__OTC__PRIM__FAC/>
					<COMM__MV__SECONDARY__FAC/>
					<fill_8/>
				</VCR__MISC__RATING__FACTORS>
				<VCR__PP__ADDL__PREM>
					<OH__ADDL__PREM/>
					<fill_9/>
				</VCR__PP__ADDL__PREM>
				<VCR__UMS__UMP__SW/>
			</SII__VEHICLE__DATA>
			<VCR__PMS__FUTURE__USE/>
			<VCR__CUST__FUTURE__USE/>
			<VCR__YR2000__CUST__USE/>
			<VCR__DUP__KEY__SEQ__NUM/>
		</VEHICLE__COVERAGE__SEG>
	</BUS__OBJ__RECORD>
</xsl:template>

</xsl:stylesheet>
