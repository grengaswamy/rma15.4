<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>

<xsl:template name="S2AuditScheduleSegTemplate">
	<xsl:param name="TYPEACT"/>
	<xsl:variable name="TableName">PMD4D__AUDIT__SCHEDULE__SEG</xsl:variable>
	<xsl:variable name="AuditFreq" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/AuditFrequencyCd"/>
	<xsl:variable name="AuditType" select="/ACORD/InsuranceSvcRq/*/CommlPolicy/CommlPolicySupplement/com.csc_AuditTypeCd"/>

	<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMD4D__AUDIT__SCHEDULE__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMD4D__AUDIT__SCHEDULE__SEG>
				<PMD4D__REC__LLBB>
					<PMD4D__REC__LENGTH/>
					<PMD4D__ACTION__CODE/>
					<PMD4D__FILE__ID/>
				</PMD4D__REC__LLBB>
				<PMD4D__SEGMENT__ID>43</PMD4D__SEGMENT__ID>
				<PMD4D__SEGMENT__STATUS>A</PMD4D__SEGMENT__STATUS>
				<PMD4D__TRANSACTION__DATE>
						<PMD4D__YEAR__TRANSACTION>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMD4D__YEAR__TRANSACTION>
						<PMD4D__MONTH__TRANSACTION>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMD4D__MONTH__TRANSACTION>
						<PMD4D__DAY__TRANSACTION>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMD4D__DAY__TRANSACTION>
				</PMD4D__TRANSACTION__DATE>
				<PMD4D__SEGMENT__ID__KEY>
					<PMD4D__SEGMENT__LEVEL__CODE>L</PMD4D__SEGMENT__LEVEL__CODE>
					<PMD4D__SEGMENT__PART__CODE>D</PMD4D__SEGMENT__PART__CODE>
					<PMD4D__SUB__PART__CODE>1</PMD4D__SUB__PART__CODE>
					<PMD4D__INSURANCE__LINE>WC</PMD4D__INSURANCE__LINE>
				</PMD4D__SEGMENT__ID__KEY>
				<PMD4D__LEVEL__KEY>
					<PMD4D__LOCATION__NUMBER>0000</PMD4D__LOCATION__NUMBER>
					<PMD4D__RISK__UNIT__GROUP__KEY>
						<PMD4D__RISK__UNIT__GROUP/>
						<PMD4D__SEQ__RSK__UNT__GRP/>
					</PMD4D__RISK__UNIT__GROUP__KEY>
					<PMD4D__RISK__UNIT/>
					<PMD4D__SEQUENCE__RISK__UNIT>
						<PMD4D__RISK__SEQUENCE></PMD4D__RISK__SEQUENCE>
						<PMD4D__RISK__TYPE__IND/>
					</PMD4D__SEQUENCE__RISK__UNIT>
				</PMD4D__LEVEL__KEY>
				<PMD4D__ITEM__EFFECTIVE__DATE>
						<PMD4D__YEAR__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMD4D__YEAR__ITEM__EFFECTIVE>
						<PMD4D__MONTH__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMD4D__MONTH__ITEM__EFFECTIVE>
						<PMD4D__DAY__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMD4D__DAY__ITEM__EFFECTIVE>
				</PMD4D__ITEM__EFFECTIVE__DATE>
				<PMD4D__VARIABLE__KEY>
					<PMD4D__SEQ__AUDIT__SCHEDULE>
							<xsl:value-of select="string('001')"/>
					</PMD4D__SEQ__AUDIT__SCHEDULE>
				</PMD4D__VARIABLE__KEY>
				<PMD4D__PROCESS__DATE>
						<PMD4D__YEAR__PROCESS>
							<xsl:value-of select="substring($ActDate,1,4)"/>
						</PMD4D__YEAR__PROCESS>
						<PMD4D__MONTH__PROCESS>
							<xsl:value-of select="substring($ActDate,6,2)"/>
						</PMD4D__MONTH__PROCESS>
						<PMD4D__DAY__PROCESS>
							<xsl:value-of select="substring($ActDate,9,2)"/>
						</PMD4D__DAY__PROCESS>
				</PMD4D__PROCESS__DATE>
				<PMD4D__SEGMENT__DATA>
					<PMD4D__FREQUENCY>
						<xsl:value-of select="$AuditFreq"/>
					</PMD4D__FREQUENCY>
					<PMD4D__TYPE>
						<xsl:value-of select="$AuditType"/>
					</PMD4D__TYPE>
					<PMD4D__AUDIT__TABLE>
						<PMD4D__AUDIT__DATA index="0">
							<PMD4D__AUDIT__NUMBER>00</PMD4D__AUDIT__NUMBER>
							<PMD4D__STATUS/>
							<PMD4D__TRANSACTION__TYPE/>
							<PMD4D__AUDIT__TYPE/>
							<PMD4D__EFFECTIVE__DATE>00000000</PMD4D__EFFECTIVE__DATE>
							<PMD4D__EXPIRATION__DATE>00000000</PMD4D__EXPIRATION__DATE>
							<PMD4D__AUDIT__REQ__DATE>00000000</PMD4D__AUDIT__REQ__DATE>
							<PMD4D__ENTERED__DATE>00000000</PMD4D__ENTERED__DATE>
							<PMD4D__AUDITOR__CODE/>
							<PMD4D__AUDIT__COST>0000000</PMD4D__AUDIT__COST>
							<PMD4D__AUDIT__TIME>0000</PMD4D__AUDIT__TIME>
							<PMD4D__AUDIT__PREMIUM>00000000000</PMD4D__AUDIT__PREMIUM>
						</PMD4D__AUDIT__DATA>
						<PMD4D__AUDIT__DATA index="1">
							<PMD4D__AUDIT__NUMBER>00</PMD4D__AUDIT__NUMBER>
							<PMD4D__STATUS>
								<xsl:value-of select="string('X')"/>
							</PMD4D__STATUS>
							<PMD4D__TRANSACTION__TYPE>
								<xsl:value-of select="string('X')"/>
							</PMD4D__TRANSACTION__TYPE>
							<PMD4D__AUDIT__TYPE>
								<xsl:value-of select="string('X')"/>
							</PMD4D__AUDIT__TYPE>
							<PMD4D__EFFECTIVE__DATE>00000000</PMD4D__EFFECTIVE__DATE>
							<PMD4D__EXPIRATION__DATE>00000000</PMD4D__EXPIRATION__DATE>
							<PMD4D__AUDIT__REQ__DATE>00000000</PMD4D__AUDIT__REQ__DATE>
							<PMD4D__ENTERED__DATE>00000000</PMD4D__ENTERED__DATE>
							<PMD4D__AUDITOR__CODE>
								<xsl:value-of select="string('XXXXXX')"/>
							</PMD4D__AUDITOR__CODE>
							<PMD4D__AUDIT__COST>0000000</PMD4D__AUDIT__COST>
							<PMD4D__AUDIT__TIME>0000</PMD4D__AUDIT__TIME>
							<PMD4D__AUDIT__PREMIUM>00000000000</PMD4D__AUDIT__PREMIUM>
						</PMD4D__AUDIT__DATA>
						<PMD4D__AUDIT__DATA index="2">
							<PMD4D__AUDIT__NUMBER>00</PMD4D__AUDIT__NUMBER>
							<PMD4D__STATUS>
								<xsl:value-of select="string('X')"/>
							</PMD4D__STATUS>
							<PMD4D__TRANSACTION__TYPE>
								<xsl:value-of select="string('X')"/>
							</PMD4D__TRANSACTION__TYPE>
							<PMD4D__AUDIT__TYPE>
								<xsl:value-of select="string('X')"/>
							</PMD4D__AUDIT__TYPE>
							<PMD4D__EFFECTIVE__DATE>00000000</PMD4D__EFFECTIVE__DATE>
							<PMD4D__EXPIRATION__DATE>00000000</PMD4D__EXPIRATION__DATE>
							<PMD4D__AUDIT__REQ__DATE>00000000</PMD4D__AUDIT__REQ__DATE>
							<PMD4D__ENTERED__DATE>00000000</PMD4D__ENTERED__DATE>
							<PMD4D__AUDITOR__CODE>
								<xsl:value-of select="string('XXXXXX')"/>
							</PMD4D__AUDITOR__CODE>
							<PMD4D__AUDIT__COST>0000000</PMD4D__AUDIT__COST>
							<PMD4D__AUDIT__TIME>0000</PMD4D__AUDIT__TIME>
							<PMD4D__AUDIT__PREMIUM>00000000000</PMD4D__AUDIT__PREMIUM>
						</PMD4D__AUDIT__DATA>
						<PMD4D__AUDIT__DATA index="3">
							<PMD4D__AUDIT__NUMBER>00</PMD4D__AUDIT__NUMBER>
							<PMD4D__STATUS>
								<xsl:value-of select="string('X')"/>
							</PMD4D__STATUS>
							<PMD4D__TRANSACTION__TYPE>
								<xsl:value-of select="string('X')"/>
							</PMD4D__TRANSACTION__TYPE>
							<PMD4D__AUDIT__TYPE>
								<xsl:value-of select="string('X')"/>
							</PMD4D__AUDIT__TYPE>
							<PMD4D__EFFECTIVE__DATE>00000000</PMD4D__EFFECTIVE__DATE>
							<PMD4D__EXPIRATION__DATE>00000000</PMD4D__EXPIRATION__DATE>
							<PMD4D__AUDIT__REQ__DATE>00000000</PMD4D__AUDIT__REQ__DATE>
							<PMD4D__ENTERED__DATE>00000000</PMD4D__ENTERED__DATE>
							<PMD4D__AUDITOR__CODE>
								<xsl:value-of select="string('XXXXXX')"/>
							</PMD4D__AUDITOR__CODE>
							<PMD4D__AUDIT__COST>0000000</PMD4D__AUDIT__COST>
							<PMD4D__AUDIT__TIME>0000</PMD4D__AUDIT__TIME>
							<PMD4D__AUDIT__PREMIUM>00000000000</PMD4D__AUDIT__PREMIUM>
						</PMD4D__AUDIT__DATA>
					</PMD4D__AUDIT__TABLE>
					<PMD4D__CANCELLATION__DATE/>
					<PMD4D__NUMBER__OF__REQSTS/>
					<PMD4D__AUDIT__DATES__IND>P</PMD4D__AUDIT__DATES__IND>
					<PMD4D__PMS__FUTURE__USE/>
					<PMD4D__CANCELLATION__TYPE/>
					<PMD4D__PREVIOUS__AUDIT__TYPE/>
					<PMD4D__CUSTOMER__FUTURE__USE/>
					<PMD4D__YR2000__CUST__USE/>
				</PMD4D__SEGMENT__DATA>
			</PMD4D__AUDIT__SCHEDULE__SEG>
	</BUS__OBJ__RECORD>
</xsl:template>
</xsl:stylesheet>