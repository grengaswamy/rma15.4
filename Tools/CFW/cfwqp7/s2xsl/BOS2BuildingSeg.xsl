<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="S2BldgTemplate" match="/">
		<xsl:variable name="RefLocId" select="../@id"/>
		<xsl:variable name="RefSubLocId" select="@id"/>
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PMDNXB1__SUB__LOC__RATING__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PMDNXB1__SUB__LOC__RATING__SEG>
				<PMDNXB1__SEGMENT__KEY>
					<PMDNXB1__REC__LLBB/>
					<PMDNXB1__SEGMENT__ID>43</PMDNXB1__SEGMENT__ID>
					<PMDNXB1__SEGMENT__STATUS>A</PMDNXB1__SEGMENT__STATUS>
					<PMDNXB1__TRANSACTION__DATE>
						<PMDNXB1__YEAR__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,1,4)"/>
						</PMDNXB1__YEAR__TRANSACTION>
						<PMDNXB1__MONTH__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,6,2)"/>
						</PMDNXB1__MONTH__TRANSACTION>
						<PMDNXB1__DAY__TRANSACTION>
							<xsl:value-of select="substring($TransEffDt,9,2)"/>
						</PMDNXB1__DAY__TRANSACTION>
					</PMDNXB1__TRANSACTION__DATE>
					<PMDNXB1__SEGMENT__ID__KEY>
						<PMDNXB1__SEGMENT__LEVEL__CODE>N</PMDNXB1__SEGMENT__LEVEL__CODE>
						<PMDNXB1__SEGMENT__PART__CODE>X</PMDNXB1__SEGMENT__PART__CODE>
						<PMDNXB1__SUB__PART__CODE>1</PMDNXB1__SUB__PART__CODE>
						<PMDNXB1__INSURANCE__LINE>BP</PMDNXB1__INSURANCE__LINE>
					</PMDNXB1__SEGMENT__ID__KEY>
					<PMDNXB1__LEVEL__KEY>
						<PMDNXB1__LOCATION__NUMBER__A>
							<PMDNXB1__LOCATION__NUMBER>
								<xsl:call-template name="FormatData">
									<xsl:with-param name="FieldName">SITE</xsl:with-param>
									<xsl:with-param name="FieldLength">4</xsl:with-param>
									<xsl:with-param name="Value" select="../ItemIdInfo/InsurerId"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template>
							</PMDNXB1__LOCATION__NUMBER>
						</PMDNXB1__LOCATION__NUMBER__A>
						<PMDNXB1__SUB__LOCATION__NUMBER__A>
							<PMDNXB1__SUB__LOCATION__NUMBER>
								<xsl:call-template name="FormatData">
									<xsl:with-param name="FieldName">SITE</xsl:with-param>
									<xsl:with-param name="FieldLength">3</xsl:with-param>
									<xsl:with-param name="Value" select="ItemIdInfo/InsurerId"/>
									<xsl:with-param name="FieldType">N</xsl:with-param>
								</xsl:call-template>
							</PMDNXB1__SUB__LOCATION__NUMBER>
						</PMDNXB1__SUB__LOCATION__NUMBER__A>
					</PMDNXB1__LEVEL__KEY>
					<PMDNXB1__ITEM__EFFECTIVE__DATE>
						<PMDNXB1__YEAR__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,1,4)"/>
						</PMDNXB1__YEAR__ITEM__EFFECTIVE>
						<PMDNXB1__MONTH__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,6,2)"/>
						</PMDNXB1__MONTH__ITEM__EFFECTIVE>
						<PMDNXB1__DAY__ITEM__EFFECTIVE>
							<xsl:value-of select="substring($EffDt,9,2)"/>
						</PMDNXB1__DAY__ITEM__EFFECTIVE>
					</PMDNXB1__ITEM__EFFECTIVE__DATE>
					<PMDNXB1__VARIABLE__KEY>
						<fill_0>
							<xsl:value-of select="string('      ')"/>
						</fill_0>
					</PMDNXB1__VARIABLE__KEY>
					<PMDNXB1__PROCESS__DATE>
						<PMDNXB1__YEAR__PROCESS>
							<xsl:value-of select="substring($TransRqDt,1,4)"/>
						</PMDNXB1__YEAR__PROCESS>
						<PMDNXB1__MONTH__PROCESS>
							<xsl:value-of select="substring($TransRqDt,6,2)"/>
						</PMDNXB1__MONTH__PROCESS>
						<PMDNXB1__DAY__PROCESS>
							<xsl:value-of select="substring($TransRqDt,9,2)"/>
						</PMDNXB1__DAY__PROCESS>
					</PMDNXB1__PROCESS__DATE>
				</PMDNXB1__SEGMENT__KEY>
				<PMDNXB1__SEGMENT__DATA>
					<PMDNXB1__TEXAS__IND>
						<xsl:value-of select="string('  ')"/>
					</PMDNXB1__TEXAS__IND>
					<PMDNXB1__COMPANY__NUMBER__A>
						<PMDNXB1__COMPANY__NUMBER>
							<xsl:value-of select="$MCO"/>
						</PMDNXB1__COMPANY__NUMBER>
					</PMDNXB1__COMPANY__NUMBER__A>
					<PMDNXB1__STATE__CODE__A>
						<PMDNXB1__STATE__CODE>
							<xsl:call-template name="ConvertAlphaStateCdToNumericStateCd">
								<xsl:with-param name="Value" select="../Addr/StateProvCd"/>
							</xsl:call-template>
						</PMDNXB1__STATE__CODE>
					</PMDNXB1__STATE__CODE__A>
					<PMDNXB1__TERR__CODE__A>
						<PMDNXB1__TERR__CODE>
							<xsl:choose>
								<xsl:when test="normalize-space(../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/com.csc_SubLocationInfo/TerritoryCd)!=''">
									<xsl:value-of select="../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/com.csc_SubLocationInfo/TerritoryCd"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="string('000')"/>
								</xsl:otherwise>
							</xsl:choose>
						</PMDNXB1__TERR__CODE>
					</PMDNXB1__TERR__CODE__A>
					<PMDNXB1__BLDG__CLASS__CODE>
						<xsl:value-of select="../../BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/ClassCd"/>
					</PMDNXB1__BLDG__CLASS__CODE>
					<PMDNXB1__PROP__CLASS__CODE>
						<xsl:value-of select="../../BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/ClassCd"/>
					</PMDNXB1__PROP__CLASS__CODE>
					<PMDNXB1__BUILDING__DESC__IND>C</PMDNXB1__BUILDING__DESC__IND>
					<PMDNXB1__PROPERTY__DESC__IND>C</PMDNXB1__PROPERTY__DESC__IND>
					<PMDNXB1__BLDG__RATE__NUMB__A>
						<PMDNXB1__BLDG__RATE__NUMB>
							<xsl:value-of select="../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/com.csc_SubLocationInfo/com.csc_ContentsRateGroupCd"/>
						</PMDNXB1__BLDG__RATE__NUMB>
					</PMDNXB1__BLDG__RATE__NUMB__A>
					<PMDNXB1__PROP__RATE__NUMB__A>
						<PMDNXB1__PROP__RATE__NUMB>
							<xsl:value-of select="../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/com.csc_SubLocationInfo/com.csc_ContentsRateGroupCd"/>
						</PMDNXB1__PROP__RATE__NUMB>
					</PMDNXB1__PROP__RATE__NUMB__A>
					<PMDNXB1__TYPE__FORM>
						<xsl:choose>
							<xsl:when test="../../CommlPolicy/CommlPolicySupplement/PolicyTypeCd='SP'">S</xsl:when>
							<xsl:when test="../../CommlPolicy/CommlPolicySupplement/PolicyTypeCd='ST'">N</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="../../CommlPolicy/CommlPolicySupplement/PolicyTypeCd"/>
							</xsl:otherwise>
						</xsl:choose>
					</PMDNXB1__TYPE__FORM>
					<PMDNXB1__OCC__TYPE>
						<xsl:value-of select="../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/InterestCd"/>
					</PMDNXB1__OCC__TYPE>
					<PMDNXB1__BLDG__ACT__CASH__A>
						<PMDNXB1__BLDG__ACT__CASH>
							<xsl:value-of select="../../BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/CommlCoverage/Limit/ValuationCd"/>
						</PMDNXB1__BLDG__ACT__CASH>
					</PMDNXB1__BLDG__ACT__CASH__A>
					<PMDNXB1__CONSTRUCTION__TYPE__A>
						<PMDNXB1__CONSTRUCTION__TYPE>
							<xsl:choose>
								<xsl:when test="normalize-space(../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/Construction/ConstructionCd)!=''">
									<xsl:value-of select="../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/Construction/ConstructionCd"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="string('0')"/>
								</xsl:otherwise>
							</xsl:choose>
						</PMDNXB1__CONSTRUCTION__TYPE>
					</PMDNXB1__CONSTRUCTION__TYPE__A>
					<PMDNXB1__RISK__TYPE>
						<xsl:value-of select="string('  ')"/>
					</PMDNXB1__RISK__TYPE>
					<PMDNXB1__RATING__ID>
						<xsl:value-of select="../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/BldgProtection/ProtectionDeviceSprinklerCd"/>
					</PMDNXB1__RATING__ID>
					<PMDNXB1__PROTECT__CLASS__A>
						<PMDNXB1__PROTECT__CLASS>
							<xsl:choose>
								<xsl:when test="normalize-space(../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/BldgProtection/FireProtectionClassCd)!=''">
									<xsl:value-of select="../../CommlSubLocation[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/BldgProtection/FireProtectionClassCd"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="string('00')"/>
								</xsl:otherwise>
							</xsl:choose>
						</PMDNXB1__PROTECT__CLASS>
					</PMDNXB1__PROTECT__CLASS__A>
					<PMDNXB1__PROP__RATE__GRP__A>
						<PMDNXB1__PROP__RATE__GRP>
							<xsl:choose>
								<xsl:when test="normalize-space(../../CommlSubLocation/com.csc_SubLocationInfo/com.csc_ClassRateGroupCd)='O' or normalize-space(../../CommlSubLocation/com.csc_SubLocationInfo/com.csc_ClassRateGroupCd)='A' or normalize-space(../../CommlSubLocation/com.csc_SubLocationInfo/com.csc_ClassRateGroupCd)=''">
									<xsl:value-of select="string('00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="../../CommlSubLocation/com.csc_SubLocationInfo/com.csc_ClassRateGroupCd"/>
								</xsl:otherwise>
							</xsl:choose>
						</PMDNXB1__PROP__RATE__GRP>
					</PMDNXB1__PROP__RATE__GRP__A>
					<PMDNXB1__BLDG__AUTO__INCR__A>
						<PMDNXB1__BLDG__AUTO__INCR>
							<xsl:choose>
								<xsl:when test="normalize-space(../../BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId and SubjectInsuranceCd = 'BLDG']/CommlCoverage[CoverageCd='INFL']/Option/OptionValue)!=''">
									<xsl:value-of select="../../BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId and SubjectInsuranceCd = 'BLDG']/CommlCoverage[CoverageCd='INFL']/Option/OptionValue"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="string('00')"/>
								</xsl:otherwise>
							</xsl:choose>
						</PMDNXB1__BLDG__AUTO__INCR>
					</PMDNXB1__BLDG__AUTO__INCR__A>
					<PMDNXB1__LIABILITY__LIMIT__A>
						<PMDNXB1__LIABILITY__LIMIT>
							<xsl:value-of select="../../BOPLineBusiness/LiabilityInfo/CommlCoverage[CoverageCd='EAOCC']/Limit/FormatCurrencyAmt/Amt"/>
						</PMDNXB1__LIABILITY__LIMIT>
					</PMDNXB1__LIABILITY__LIMIT__A>
					<PMDNXB1__FIRE__LEGAL__LIMIT__A>
						<PMDNXB1__FIRE__LEGAL__LIMIT>
							<xsl:value-of select="../../BOPLineBusiness/LiabilityInfo/CommlCoverage[CoverageCd='FLL']/Limit/FormatCurrencyAmt/Amt"/>
						</PMDNXB1__FIRE__LEGAL__LIMIT>
					</PMDNXB1__FIRE__LEGAL__LIMIT__A>
					<PMDNXB1__DRUGGIST__LIABILITY>
						<PMDNXB1__DRUG__LIAB>
							<xsl:choose>
								<xsl:when test="normalize-space(../../BOPLineBusiness/LiabilityInfo/CommlCoverage[CoverageCd='MEDPM']/Limit/FormatCurrencyAmt/Amt)='1'">
									<xsl:value-of select="string('Y')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="string('N')"/>
								</xsl:otherwise>
							</xsl:choose>
						</PMDNXB1__DRUG__LIAB>
					</PMDNXB1__DRUGGIST__LIABILITY>
					<PMDNXB1__INDIVID__RISK__MOD__A>
						<PMDNXB1__INDIVID__RISK__MOD>
							<xsl:value-of select="../../BOPLineBusiness/com.csc_ModificationFactors/CreditOrSurcharge[CreditSurchargeCd='IRPM']/NumericValue/FormatModFactor"/>
						</PMDNXB1__INDIVID__RISK__MOD>
					</PMDNXB1__INDIVID__RISK__MOD__A>
					<PMDNXB1__TOTAL__OPT__PREMIUM__A>
						<PMDNXB1__TOT__OPT__PREM>
							<xsl:value-of select="string('000000000')"/>
						</PMDNXB1__TOT__OPT__PREM>
					</PMDNXB1__TOTAL__OPT__PREMIUM__A>
					<PMDNXB1__FINAL__BUS__PREMIUM__A>
						<PMDNXB1__FINAL__BUS__PREMIUM>
							<xsl:value-of select="string('000000000')"/>
						</PMDNXB1__FINAL__BUS__PREMIUM>
					</PMDNXB1__FINAL__BUS__PREMIUM__A>
					<PMDNXB1__LOCATION__COUNTER__A>
						<PMDNXB1__LOCATION__COUNTER>
							<xsl:value-of select="string('00')"/>
						</PMDNXB1__LOCATION__COUNTER>
					</PMDNXB1__LOCATION__COUNTER__A>
					<PMDNXB1__DEC__CHANGE__FLAG>
						<xsl:value-of select="string(' ')"/>
					</PMDNXB1__DEC__CHANGE__FLAG>
					<PMDNXB1__SPOIL__CLASS__GRP__A>
						<PMDNXB1__SPOIL__CLASS__GRP>
							<xsl:choose>
								<xsl:when test="normalize-space(../../BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/com.csc_SpoilageClassificationCd)!=''">
									<xsl:value-of select="../../BOPLineBusiness/PropertyInfo/CommlPropertyInfo[@LocationRef=$RefLocId and @SubLocationRef=$RefSubLocId]/com.csc_SpoilageClassificationCd"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="string('1')"/>
								</xsl:otherwise>
							</xsl:choose>
						</PMDNXB1__SPOIL__CLASS__GRP>
					</PMDNXB1__SPOIL__CLASS__GRP__A>
					<PMDNXB1__ALK__ATTORNEY__FEE__A>
						<PMDNXB1__ATTORNEYS__FEES__A>
							<PMDNXB1__ALK__ATTORNEY__FEE>
								<PMDNXB1__ATTORNEYS__FEES__X>
									<xsl:value-of select="string('        ')"/>
								</PMDNXB1__ATTORNEYS__FEES__X>
							</PMDNXB1__ALK__ATTORNEY__FEE>
						</PMDNXB1__ATTORNEYS__FEES__A>
					</PMDNXB1__ALK__ATTORNEY__FEE__A>
					<PMDNXB1__COMM__REDUCTION__A>
						<PMDNXB1__COMM__REDUCTION>1.000</PMDNXB1__COMM__REDUCTION>
					</PMDNXB1__COMM__REDUCTION__A>
					<PMDNXB1__DEL__PROP__RATE__GRP__A>
						<PMDNXB1__DEL__PROP__RATE__GRP>
							<xsl:value-of select="string('00')"/>
						</PMDNXB1__DEL__PROP__RATE__GRP>
					</PMDNXB1__DEL__PROP__RATE__GRP__A>
					<PMDNXB1__DEL__BLDG__RATE__NUMB__A>
						<PMDNXB1__DEL__BLDG__RATE__NUMB>
							<xsl:value-of select="string('  ')"/>
						</PMDNXB1__DEL__BLDG__RATE__NUMB>
					</PMDNXB1__DEL__BLDG__RATE__NUMB__A>
					<PMDNXB1__DEL__PROP__RATE__NUMB__A>
						<PMDNXB1__DEL__PROP__RATE__NUMB>
							<xsl:value-of select="string('  ')"/>
						</PMDNXB1__DEL__PROP__RATE__NUMB>
					</PMDNXB1__DEL__PROP__RATE__NUMB__A>
					<PMDNXB1__DEL__BLDG__CLASS__CODE>
						<xsl:value-of select="string('      ')"/>
					</PMDNXB1__DEL__BLDG__CLASS__CODE>
					<PMDNXB1__DEL__PROP__CLASS__CODE>
						<xsl:value-of select="string('      ')"/>
					</PMDNXB1__DEL__PROP__CLASS__CODE>
					<PMDNXB1__LOCATION__DELETED__SW>
						<xsl:value-of select="string(' ')"/>
					</PMDNXB1__LOCATION__DELETED__SW>
					<PMDNXB1__FUTURE__USE>
						<xsl:value-of select="string('                                                                                                                                                                  ')"/>
					</PMDNXB1__FUTURE__USE>
					<PMDNXB1__CUSTOMER__USAGE>
						<xsl:value-of select="string('                              ')"/>
					</PMDNXB1__CUSTOMER__USAGE>
					<PMDNXB1__YR2000__CUST__USE>
						<xsl:value-of select="string('                                                                                                    ')"/>
					</PMDNXB1__YR2000__CUST__USE>
				</PMDNXB1__SEGMENT__DATA>
			</PMDNXB1__SUB__LOC__RATING__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="BORQ_S2_ACORD.xml" htmlbaseurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->