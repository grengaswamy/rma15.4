<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="S2PCRTLTemplate"> 
		<BUS__OBJ__RECORD>
			<RECORD__NAME__ROW>
				<RECORD__NAME>PROCESS__CONTROL__SEG</RECORD__NAME>
			</RECORD__NAME__ROW>
			<PROCESS__CONTROL__SEG>
				<PCR__REC__LLBB>
					<PCR__REC__LENGTH/>
					<PCR__ACTION__CODE/>
					<PCR__FILE__ID/>
				</PCR__REC__LLBB>
				<PCR__CTL__ID>00</PCR__CTL__ID>
				<PCR__REASON__AMENDED>
					<xsl:value-of select="/*/ChangeStatus/ChangeDesc"/>
				</PCR__REASON__AMENDED>
				<PCR__AGENTS__CASH__PREM/>
				<PCR__ISSUE__CODE/>
				<PCR__EFFECTIVE__DATE>
					<PCR__EFF__YEAR/>
					<PCR__EFF__MONTH/>
					<PCR__EFF__DAY/>
				</PCR__EFFECTIVE__DATE>
				<PCR__NUMBER__STAT__REQ/>
				<PCR__BILLING__CODE/>
				<PCR__LOSS__CODE/>
				<PCR__ALPHA__LOSS__INDEX/>
				<PCR__RISK__STAT__PROV/>
				<PCR__AGENCY__TAX__LOCATION/>
				<PCR__ORIG__ISSUE__CODE/>
				<PCR__WRITTEN__PREMIUM/>
				<PCR__TEXAS__IM__LIABILITY/>
				<PCR__RICO__AUTO/>
				<fill_1/>
				<PCR__POL__STATUS__SD__CP/>
				<PCR__AUTO__ON__CPP__POLICY__IND/>
				<PCR__ERROR__CODES>
					<PCR__ERR__CODES>
						<PCR__ERR__FIELD/>
					</PCR__ERR__CODES>
				</PCR__ERROR__CODES>
				<PCR__ADDITIONAL__DEC__HEADINGS>
					<PCR__ADDL__DEC__HEADERS>
						<PCR__ADH__USE/>
						<PCR__ADH__COPIES/>
						<PCR__ADH__COPIES__NUM/>
					</PCR__ADDL__DEC__HEADERS>
				</PCR__ADDITIONAL__DEC__HEADINGS>
				<PCR__AUDIT__WORKSHEET__IND/>
				<PCR__REVISED__SIMPLE__SW/>
				<PCR__TEXAS__CF__ON__POLICY__IND/>
				<PCR__DEC__CHANGE__SW/>
				<PCR__OFFSET__ONSET__POLICY__IND/>
				<PCR__MVR__ORDERS>
					<PCR__MVRS__ORDERED/>
				</PCR__MVR__ORDERS>
				<PCR__CPP__IM__IND/>
				<PCR__WCP__POLICY__TAX__AMT/>
				<PCR__PMS__FUTURE__USE/>
				<PCR__CUST__FUTURE__USE/>
				<PCR__YR2000__CUST__USE/>
			</PROCESS__CONTROL__SEG>
		</BUS__OBJ__RECORD>
	</xsl:template>
</xsl:stylesheet>