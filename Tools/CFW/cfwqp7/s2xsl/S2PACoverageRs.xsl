<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" encoding="UTF-8"/>
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<!-- Template for Vehicle Coverage-->
	<xsl:template match="/*/VEHICLE__COVERAGE__SEG" mode="CreateCoverage">
		<xsl:for-each select="./SII__VEHICLE__DATA/VCR__TABLE__OF__COVERAGES">
			<xsl:call-template name="AssignValues">
				<xsl:with-param name="LoopCtr" select="position()-1"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="AssignValues">
		<xsl:param name="CovData"/>
		<xsl:param name="LoopCtr"/>
		<!--Modified for Issue 35934
			<xsl:variable name="CovCode" select="./*/*[contains(name(),concat('CODE', $LoopCtr))]"/>
			<xsl:variable name="CovLimit" select="./*/*[contains(name(),concat('LIMIT', $LoopCtr))]"/>
			<xsl:variable name="CovPrem" select="./*/*[contains(name(),concat('PREMIUM', $LoopCtr))]"/>-->
		<xsl:variable name="CovCode" select="./*[@index=$LoopCtr]/*[contains(name(),'CODE')]"/>
		<!--CovDesc - Added for Issue 35934-->
		<xsl:variable name="CovDesc">
			<xsl:call-template name="TranslateCoverageDesc">
				<xsl:with-param name="CoverageCd">
					<xsl:value-of select="./*[@index=$LoopCtr]/*[contains(name(),'CODE')]"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="CovLimit" select="./*[@index=$LoopCtr]/*[contains(name(),'LIMIT')]"/>
		<xsl:variable name="CovPrem" select="./*[@index=$LoopCtr]/*[contains(name(),'PREMIUM')]"/>
		<xsl:if test="string-length($CovCode) &gt; 0">
			<Coverage>
				<CoverageCd>
					<xsl:value-of select="$CovCode"/>
				</CoverageCd>
				<!--Modified for Issue 35934<CoverageDesc/>-->
				<CoverageDesc>
					<xsl:value-of select="$CovDesc"/>
				</CoverageDesc>
				<xsl:if test="($CovCode &gt; 0 and $CovCode &lt; 36) or ($CovCode &gt; 36 and $CovCode &lt; 44) or $CovCode = 45">
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="normalize-space($CovLimit)"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
				</xsl:if>
				<xsl:if test="$CovCode = 36 or $CovCode = 44 or $CovCode = 46 or ($CovCode &gt; 46 and $CovCode &lt; 50)">
					<Deductible>
						<FormatCurrencyAmt>
							<Amt>
								<!-- xsl:value-of select="normalize-space($CovLimit)"/ -->
								<xsl:value-of select="concat(normalize-space($CovLimit),'.00')"/>
							</Amt>
						</FormatCurrencyAmt>
						<DeductibleTypeCd/>
					</Deductible>
				</xsl:if>
				<xsl:if test="normalize-space($CovCode) = 'Y'">
					<Option>
						<OptionTypeCd>YNInd1</OptionTypeCd>
						<OptionCd/>
						<OptionValue>1</OptionValue>
					</Option>
				</xsl:if>
				<xsl:if test="normalize-space($CovCode) = 'N'">
					<Option>
						<OptionTypeCd>YNInd1</OptionTypeCd>
						<OptionCd/>
						<OptionValue>0</OptionValue>
					</Option>
				</xsl:if>
				<CurrentTermAmt>
					<Amt>
						<xsl:variable name="TermAmt">
							<xsl:choose>
								<xsl:when test="contains($CovPrem,'{')">
									<xsl:value-of select="translate($CovPrem,'{','0')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="normalize-space($CovPrem)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:value-of select="$TermAmt"/>
					</Amt>
				</CurrentTermAmt>
			</Coverage>
			<xsl:if test="number($LoopCtr) &lt; 15"> 
				<xsl:call-template name="AssignValues">
					<xsl:with-param name="LoopCtr" select="$LoopCtr + 1"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2002 eXcelon Corp.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->