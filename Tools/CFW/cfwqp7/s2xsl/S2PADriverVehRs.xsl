<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes" encoding="UTF-8"/>
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:template match="/*/DRIVER__SEG" mode="CreateDriverVeh">
		<xsl:variable name="DrvNo" select="./DRIVER__ID"/>
		<xsl:variable name="PO" select="./DRIVER__PRIN__OPERATOR/PO__VEHICLE__NO"/>
		<xsl:variable name="PT" select="./DRIVER__PT__OPERATOR/PT__VEHICLE__NO"/>
		<xsl:if test="string-length($PO) &gt; 0">
			<xsl:if test="substring($PO,1,1) &gt; '0'">
				<DriverVeh>
					<xsl:attribute name="DriverRef">
						<xsl:value-of select="concat('d',$DrvNo)"/>
					</xsl:attribute>
					<xsl:attribute name="VehRef">
						<!--Modified for Issue 35934<xsl:value-of select="concat('v00',substring($PO,1,1))"/>-->
						<xsl:value-of select="concat('v0',substring($PO,1,1))"/>
					</xsl:attribute>
					<UsePct>
						<xsl:text>100</xsl:text>
					</UsePct>
				</DriverVeh>
			</xsl:if>
			<xsl:if test="substring($PO,2,1) &gt; '0'">
				<DriverVeh>
					<xsl:attribute name="DriverRef">
						<xsl:value-of select="concat('d',$DrvNo)"/>
					</xsl:attribute>
					<xsl:attribute name="VehRef">
					<!--Modified for Issue 35934<xsl:value-of select="concat('v00',substring($PO,2,1))"/>-->
						<xsl:value-of select="concat('v0',substring($PO,2,1))"/>
					</xsl:attribute>
					<UsePct>
						<xsl:text>100</xsl:text>
					</UsePct>
				</DriverVeh>
			</xsl:if>
			<xsl:if test="substring($PO,3,1) &gt; '0'">
				<DriverVeh>
					<xsl:attribute name="DriverRef">
						<xsl:value-of select="concat('d',$DrvNo)"/>
					</xsl:attribute>
					<xsl:attribute name="VehRef">
					<!--Modified for Issue 35934<xsl:value-of select="concat('v00',substring($PO,3,1))"/>-->
						<xsl:value-of select="concat('v0',substring($PO,3,1))"/>
					</xsl:attribute>
					<UsePct>
						<xsl:text>100</xsl:text>
					</UsePct>
				</DriverVeh>
			</xsl:if>
		</xsl:if>
		<xsl:if test="string-length($PT) &gt; 0">
			<xsl:if test="substring($PT,1,1) &gt; '0'">
				<DriverVeh>
					<xsl:attribute name="DriverRef">
						<xsl:value-of select="concat('d',$DrvNo)"/>
					</xsl:attribute>
					<xsl:attribute name="VehRef">
					<!--Modified for Issue 35934<xsl:value-of select="concat('v00',substring($PT,1,1))"/>-->
						<xsl:value-of select="concat('v0',substring($PT,1,1))"/>
					</xsl:attribute>
					<UsePct>
						<xsl:text>50</xsl:text>
					</UsePct>
				</DriverVeh>
			</xsl:if>
			<xsl:if test="substring($PT,2,1) &gt; '0'">
				<DriverVeh>
					<xsl:attribute name="DriverRef">
						<xsl:value-of select="concat('d',$DrvNo)"/>
					</xsl:attribute>
					<xsl:attribute name="VehRef">
					<!--Modified for Issue 35934<xsl:value-of select="concat('v00',substring($PT,2,1))"/>-->
						<xsl:value-of select="concat('v0',substring($PT,2,1))"/>
					</xsl:attribute>
					<UsePct>
						<xsl:text>50</xsl:text>
					</UsePct>
				</DriverVeh>
			</xsl:if>
			<xsl:if test="substring($PT,3,1) &gt; '0'">
				<DriverVeh>
					<xsl:attribute name="DriverRef">
						<xsl:value-of select="concat('d',$DrvNo)"/>
					</xsl:attribute>
					<xsl:attribute name="VehRef">
					<!--Modified for Issue 35934<xsl:value-of select="concat('v00',substring($PT,3,1))"/>-->
						<xsl:value-of select="concat('v0',substring($PT,3,1))"/>
					</xsl:attribute>
					<UsePct>
						<xsl:text>50</xsl:text>
					</UsePct>
				</DriverVeh>
			</xsl:if>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
<!-- Stylus Studio meta-information - (c)1998-2003. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->