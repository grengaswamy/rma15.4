<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!--<xsl:template name="S2HORateRsTemplate" match="/">-->
	<xsl:template name="S2HORateRsTemplate">
		<xsl:for-each select="*/HOMEOWNERS__RATING__SEG">
			<Dwell>
				<xsl:attribute name="id">
					<xsl:value-of select="concat('dwell','1')"/>
				</xsl:attribute>
				<PolicyTypeCd>
					<xsl:value-of select="HRR__INFO/HRR__FORM"/>
				</PolicyTypeCd>
				<PurchaseDt/>
				<PurchasePriceAmt>
					<Amt/>
				</PurchasePriceAmt>
				<FloorUnitLocated/>
				<NumEmployeesFullTimeResidence/>
				<AreaTypeSurroundingsCd>
					<xsl:value-of select="HRR__INFO/HRR__INSIDE__CITY"/>
				</AreaTypeSurroundingsCd>
				<Construction>
					<ConstructionCd>
						<xsl:value-of select="HRR__INFO/HRR__CONSTRUCTION"/>
					</ConstructionCd>
					<YearBuilt>
						<xsl:value-of select="HRR__INFO/HRR__YEAR__OF__CONSTRUCTION"/>
					</YearBuilt>
					<NumStories/>
					<RoofingMaterial>
						<RoofMaterialCd>
							<xsl:value-of select="HRR__INFO/HRR__APPROVED__ROOF"/>
						</RoofMaterialCd>
						<RoofMaterialPct/>
					</RoofingMaterial>
				</Construction>
				<DwellOccupancy>
					<NumRooms/>
					<NumApartments/>
					<ResidenceTypeCd>
						<!--<xsl:value-of select="HRR__INFO/SECONDARY__RESIDENCE__USE"/>-->
					</ResidenceTypeCd>
					<OccupancyTypeCd>
						<xsl:value-of select="HRR__INFO/SECONDARY__RESIDENCE__USE"/>
					</OccupancyTypeCd>
				</DwellOccupancy>
				<DwellRating>
					<TerritoryCd>
						<xsl:value-of select="concat('0',HRR__INFO/HRR__TERRITORY)"/>
					</TerritoryCd>
				</DwellRating>
				<BldgProtection>
					<NumUnitsInFireDivisions/>
					<FireProtectionClassCd>
						<xsl:value-of select="concat(HRR__INFO/HRR__PROTECTION/HRR__PROTECT__A,HRR__INFO/HRR__PROTECTION/HRR__PROTECT__B)"/>
					</FireProtectionClassCd>
					<DistanceToFireStation>
						<NumUnits/>
						<UnitMeasurementCd/>
					</DistanceToFireStation>
					<DistanceToHydrant>
						<NumUnits>
							<xsl:value-of select="HRR__INFO/HRR__HYDRANT"/>
						</NumUnits>
						<UnitMeasurementCd>
							<xsl:text>MT</xsl:text>
						</UnitMeasurementCd>
					</DistanceToHydrant>
					<ProtectionDeviceBurglarCd>
					<xsl:variable name="ProtectiveDeviceCode">  
					<xsl:choose> 
						<xsl:when test="../PROPERTY__EXTENSION__SEG/PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE='H216'">
							<xsl:value-of select="substring(substring-after(../PROPERTY__EXTENSION__SEG/PROPERTY__EXTENSION__DATA/PER__PARAMETERS/PER__DESC__LINE__1,'TYPE='),1,1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string('0')"/> 
						</xsl:otherwise> 					
					</xsl:choose>
					</xsl:variable>
					<xsl:value-of select="$ProtectiveDeviceCode"/>  
					</ProtectionDeviceBurglarCd>
					<ProtectionDeviceFireCd/>
					<ProtectionDeviceSmokeCd/>
					<ProtectionDeviceSprinklerCd/>
				</BldgProtection>
				<BldgImprovements>
					<ExteriorPaintYear/>
					<HeatingImprovementCd/>
					<HeatingImprovementYear/>
					<PlumbingImprovementCd/>
					<PlumbingImprovementYear/>
					<RoofingImprovementCd/>
					<RoofingImprovementYear/>
					<WiringImprovementCd/>
					<WiringImprovementYear/>
				</BldgImprovements>
				<DwellInspectionValuation>
					<DwellStyleCd/>
					<EstimatedReplCostAmt>
						<Amt>
							<xsl:value-of select="HRR__INFO/HRR__REPLACEMENT__COST"/>
						</Amt>
					</EstimatedReplCostAmt>
					<GeneralShapeCd/>
					<HeatSourcePrimaryCd/>
					<NumFamilies>
						<xsl:value-of select="HRR__INFO/HRR__NUMBER__OF__FAMILIES"/>
					</NumFamilies>
					<TotalArea>
						<NumUnits/>
						<UnitMeasurementCd/>
					</TotalArea>
					<MarketValueAmt>
						<Amt/>
					</MarketValueAmt>
				</DwellInspectionValuation>
				<SwimmingPool>
					<AboveGroundInd/>
					<ApprovedFenceInd/>
				</SwimmingPool>
				<xsl:variable name="FVREPOptionVal">  
					<xsl:choose> 
						<xsl:when test="../PROPERTY__EXTENSION__SEG/PROPERTY__EXTENSION__DATA/PER__USE__SEQUENCE/PER__USE='H290'">
							<xsl:value-of select="string('Y')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="string('N')"/> 
						</xsl:otherwise> 					
					</xsl:choose>
					</xsl:variable>
				<Coverage>
					<CoverageCd>com.csc_TheftDed</CoverageCd>
					<CoverageDesc/>
					<Limit>
						<FormatCurrencyAmt>
							<Amt/>
						</FormatCurrencyAmt>
					</Limit>
					<Deductible>
						<FormatCurrencyAmt>
							<Amt/>
						</FormatCurrencyAmt>
						<DeductibleTypeCd/>
					</Deductible>
					<Option>
						<OptionTypeCd>TheftDed</OptionTypeCd>
						<OptionCd/>
						<OptionValue>
							<xsl:value-of select="HRR__INFO/HRR__THEFT__250__DEDUCTIBLE"/>
						</OptionValue>
					</Option>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>DWELL</CoverageCd>
					<CoverageDesc/>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="HRR__INFO/HRR__COVERAGE__A__LIMIT"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<Deductible>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="HRR__INFO/HRR__DEDUCTIBLE__TYPE__AMOUNT/HRR__DEDUCTIBLE__AMOUNT"/>
							</Amt>
						</FormatCurrencyAmt>
						<DeductibleTypeCd>
							<xsl:value-of select="HRR__INFO/HRR__DEDUCTIBLE__TYPE__AMOUNT/HRR__DEDUCTIBLE__TYPE"/>
						</DeductibleTypeCd>
					</Deductible>
					<Option>
						<OptionTypeCd/>
						<OptionCd/>
						<OptionValue/>
					</Option>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="HRR__INFO/HRR__COVERAGE__A__PREMIUM"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>OS</CoverageCd>
					<CoverageDesc/>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="HRR__INFO/HRR__COVERAGE__B__LIMIT"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>PP</CoverageCd>
					<CoverageDesc/>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="HRR__INFO/HRR__COVERAGE__C__LIMIT"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="HRR__INFO/HRR__COVERAGE__C__PREMIUM"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>LOU</CoverageCd>
					<CoverageDesc/>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="HRR__INFO/HRR__COVERAGE__D__LIMIT"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<Deductible>
						<FormatCurrencyAmt>
							<Amt/>
						</FormatCurrencyAmt>
						<DeductibleTypeCd/>
					</Deductible>
					<Option>
						<OptionTypeCd/>
						<OptionCd/>
						<OptionValue/>
					</Option>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="HRR__INFO/HRR__COVERAGE__D__PREMIUM"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>PL</CoverageCd>
					<CoverageDesc/>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
							    <xsl:value-of select="HRR__INFO/HRR__COVERAGE__E__AND__F__LIMITS/HRR__COVERAGE__E__LIMIT"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<Deductible>
						<FormatCurrencyAmt>
							<Amt/>
						</FormatCurrencyAmt>
						<DeductibleTypeCd/>
					</Deductible>
					<Option>
						<OptionTypeCd/>
						<OptionCd/>
						<OptionValue/>
					</Option>
					<CurrentTermAmt>
						<Amt>
							<xsl:value-of select="HRR__INFO/HRR__COVERAGE__E__F__PREMIUM"/>
						</Amt>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>MEDPM</CoverageCd>
					<CoverageDesc/>
					<Limit>
						<FormatCurrencyAmt>
							<Amt>
								<xsl:value-of select="HRR__INFO/HRR__COVERAGE__E__AND__F__LIMITS/HRR__COVERAGE__F__LIMIT"/>
							</Amt>
						</FormatCurrencyAmt>
					</Limit>
					<Deductible>
						<FormatCurrencyAmt>
							<Amt/>
						</FormatCurrencyAmt>
						<DeductibleTypeCd/>
					</Deductible>
					<Option>
						<OptionTypeCd/>
						<OptionCd/>
						<OptionValue/>
					</Option>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>INFGD</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt/>
						</FormatCurrencyAmt>
					</Limit>
					<Option>
						<OptionValue>
							<xsl:value-of select=" substring(concat('00',HRR__INFO/HRR__INFLATION__GUARD),string-length(HRR__INFO/HRR__INFLATION__GUARD)+1,2)"/>
						</OptionValue>
					</Option>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>WINDX</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt/>
						</FormatCurrencyAmt>
					</Limit>
					<Option>
						<OptionValue>
							<xsl:value-of select="HRR__INFO/HRR__WIND__HAIL__EXCLUSION"/>
						</OptionValue>
					</Option>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
				<Coverage>
					<CoverageCd>FVREP</CoverageCd>
					<Limit>
						<FormatCurrencyAmt>
							<Amt/>
						</FormatCurrencyAmt>
					</Limit>
					<Option>
						<OptionValue>
							<xsl:value-of select="string($FVREPOptionVal)"/>
						</OptionValue>
					</Option>
					<CurrentTermAmt>
						<Amt/>
					</CurrentTermAmt>
				</Coverage>
			</Dwell>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>