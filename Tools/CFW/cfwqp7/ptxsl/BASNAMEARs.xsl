<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<!--*********************************************************************************************-->
<!--* Programmer : 	Ramnik Singh           		Date       : 11/15/2006           		    *-->
<!--* FSIT Number: 	75542	    		        Resolution : 42213	                            *-->
<!--* Package	 : 	Q00034 - FSIT # 75542 -NAICS Code - Need to capture this code at client level.      *-->
<!--*			 Need multi seq #'s to capture changes in descriptio.                               *-->
<!--* Description: 	 NAICS Code - Need to capture this code at client level.                	    *-->
<!--*                    Need multi seq #'s to capture changes in descriptio.                          	    *-->
<!--*********************************************************************************************-->
<!--* Programmer	: Vishal Bansal				Date      : 03/29/2007	       *-->
<!--* FSIT Number	: 91223	       				Resolution Number: 43359       *--> 
<!--* Package Name      : Q00134a-FSIT#91223- Auto Assign Group Number          	       *-->
<!--* Description	: Added the code to send the PROCESSING__INFO fileds in response.      *-->
<!--********************************************************************************************-->

<xsl:include href="Replace-String.xsl"/>
<xsl:include href="DecomposeErrorRecord.xsl"/>
<xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>

	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
	<xsl:apply-templates select="//ERROR__LST__ROW"/>
	<xsl:apply-templates select="/BASNAMEARs/NA__FIELDS__RECORDS__ROW">
	</xsl:apply-templates>
	
	</xsl:template>
	<xsl:template  name="test1" match="/BASNAMEARs/NA__FIELDS__RECORDS__ROW">
	
						
			<xsl:for-each select="KEY__FIELDS__ROW/child::node()">
			<xsl:variable name="nodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'__'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template></xsl:variable>
			<xsl:if test="string-length($nodename)>0">
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text> 	
			</xsl:if>
			</xsl:for-each>
						
			<xsl:if test="//CUSTOMER__INFO__ATTRIBUTES">
			<xsl:for-each select="CUSTOMER__INFO__ATTRIBUTES/child::node()">
			<xsl:variable name="nodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'__'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template></xsl:variable>
			<xsl:if test="string-length($nodename)>0">
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			</xsl:for-each>
			</xsl:if>
			
			<xsl:if test="//NAME__DETAILS">
			<xsl:for-each select="NAME__DETAILS/child::node()">
			<xsl:variable name="nodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'__'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template></xsl:variable>
			<xsl:if test="string-length($nodename)>0">
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:choose><xsl:when test="contains(name(),'DATE')"><xsl:call-template name="cvtdateCYYMMDDtoMMDDYY"><xsl:with-param name="datefld" select="."/></xsl:call-template></xsl:when><xsl:otherwise><xsl:value-of select="."/></xsl:otherwise></xsl:choose><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			</xsl:for-each>

<xsl:variable name="WorkPhoneNumber" select="//A__PHONE__1"/>
				<xsl:variable name="Workphone" select = "substring($WorkPhoneNumber, 1,10)"/>
				<xsl:variable name="Workextension" select="substring($WorkPhoneNumber, 12,16)"/>
				<A_PHONE_1><xsl:value-of select="$Workphone"/></A_PHONE_1>
				<AB_PHONE_1_EXT><xsl:value-of select="$Workextension"/></AB_PHONE_1_EXT>
				
<xsl:variable name="ContactPhoneNumber" select="//AI__PHONE__1"/>
				<xsl:variable name="Contactphone" select = "substring($ContactPhoneNumber, 1,10)"/>
				<xsl:variable name="Contactextension" select="substring($ContactPhoneNumber, 12,16)"/>
				<AI_PHONE_1><xsl:value-of select="$Contactphone"/></AI_PHONE_1>
				<AB_PHONE_2_EXT><xsl:value-of select="$Contactextension"/></AB_PHONE_2_EXT>

			</xsl:if>
			
			
			<xsl:if test="//CUSTOMER__INFORMATION">
			<xsl:for-each select="CUSTOMER__INFORMATION/child::node()">
			<xsl:variable name="nodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'__'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template></xsl:variable>
			<xsl:if test="string-length($nodename)>0">
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			</xsl:for-each>
			</xsl:if>


			<xsl:if test="//ADDITIONAL__ADDRESS">
			<xsl:for-each select="ADDITIONAL__ADDRESS/child::node()">
			<xsl:variable name="nodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'__'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template></xsl:variable>
			<xsl:if test="string-length($nodename)>0">
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			</xsl:for-each>
			</xsl:if>

							
			<!-- FSIT # 75542 Resolution # 42213 Begin -->
            <xsl:if test="//NAICS__INFO__RECORD">
			<xsl:for-each select="NAICS__INFO__RECORD/child::node()">
			<xsl:variable name="nodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'__'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template></xsl:variable>
			<xsl:if test="string-length($nodename)>0">
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			</xsl:for-each>
			</xsl:if>

			<xsl:if test="//BAS__FIELDS__RECORD__0100">
			<xsl:for-each select="BAS__FIELDS__RECORD__0100/child::node()">
			<xsl:variable name="nodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'__'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template></xsl:variable>
			<xsl:if test="string-length($nodename)>0">
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			</xsl:for-each>
			</xsl:if>

          <xsl:if test="//BAS__FIELDS__RECORD__0900">
			<xsl:for-each select="BAS__FIELDS__RECORD__0900/child::node()">
			<xsl:variable name="nodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'__'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template></xsl:variable>
			<xsl:if test="string-length($nodename)>0">
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			</xsl:for-each>
			</xsl:if>
           <!-- FSIT # 75542 Resolution # 42213 End -->
	   <!-- FSIT# 91223 Resolution # 43359 -Start-->
	   <xsl:if test="//PROCESSING__INFO">
			<xsl:for-each select="PROCESSING__INFO/child::node()">
			<xsl:variable name="nodename"><xsl:call-template name="replace-string"><xsl:with-param name="text" select="name()"/><xsl:with-param name="from" select="'__'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template></xsl:variable>
			<xsl:if test="string-length($nodename)>0">
			<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&lt;/</xsl:text><xsl:value-of select="$nodename"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			</xsl:if>
			</xsl:for-each>
			</xsl:if>
	   <!-- FSIT# 91223 Resolution # 43359 -End-->


		 <xsl:text disable-output-escaping="yes">&lt;/POINTXML&gt;</xsl:text>
				
	</xsl:template>


	
</xsl:stylesheet>