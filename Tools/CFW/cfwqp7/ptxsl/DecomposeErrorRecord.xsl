 <!--<?xml version="1.0" encoding="UTF-8"?>-->
 <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="DecomposeErrorRecord" match="//ERROR__LST__ROW">

<xsl:text disable-output-escaping="yes">&lt;POINTXML&gt;</xsl:text>
<xsl:for-each select="ERROR__LST">
<xsl:variable name="inc" select="1"/>
<xsl:if test="string-length(ERROR__MSG)>0">
<xsl:text disable-output-escaping="yes">&lt;ERROR</xsl:text><xsl:value-of select="@index+$inc"></xsl:value-of><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="ERROR__MSG"/>
<xsl:text disable-output-escaping="yes">&lt;/ERROR</xsl:text><xsl:value-of select="@index+$inc"></xsl:value-of><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
<!-- Resolution No 40013 start-->
<xsl:text disable-output-escaping="yes">&lt;ERRORSEV</xsl:text><xsl:value-of select="@index+$inc"></xsl:value-of><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="ERROR__SEVERITY"/>
<xsl:text disable-output-escaping="yes">&lt;/ERRORSEV</xsl:text><xsl:value-of select="@index+$inc"></xsl:value-of><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
<xsl:text disable-output-escaping="yes">&lt;ERRORSWITCH</xsl:text><xsl:value-of select="@index+$inc"></xsl:value-of><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:value-of select="ERROR__SWITCH"/>
<xsl:text disable-output-escaping="yes">&lt;/ERRORSWITCH</xsl:text><xsl:value-of select="@index+$inc"></xsl:value-of><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
<xsl:text disable-output-escaping="yes">&lt;ERRORFIELD</xsl:text><xsl:value-of select="@index+$inc"></xsl:value-of><xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:call-template name="replace-string"><xsl:with-param name="text" select="ERROR__FIELD"/><xsl:with-param name="from" select="'-'"/><xsl:with-param name="to" select="'_'"/></xsl:call-template>
<xsl:text disable-output-escaping="yes">&lt;/ERRORFIELD</xsl:text><xsl:value-of select="@index+$inc"></xsl:value-of><xsl:text disable-output-escaping="yes">&gt;</xsl:text>
<!--Resolution No 40013 end-->
</xsl:if>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>