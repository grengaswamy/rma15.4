<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- ------------------------------------------------------------------- --%>
<%-- This is a JSP to configure the Communications Framework properties. --%>
<%-- ------------------------------------------------------------------- --%>
<%@ page language='java' contentType='text/html'%>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="org.w3c.dom.Document" %>
<%@ page import="com.csc.cfw.process.AdapterFactory" %>
<%@ page import="com.csc.cfw.process.AdapterProperties" %>
<%@ page import="com.csc.cfw.process.MessageQueryCache"%>
<%@ page import="com.csc.cfw.process.MessageRouteFactory"%>
<%@ page import="com.csc.cfw.process.ProcessFactory" %>
<%@ page import="com.csc.cfw.recordbuilder.RecordManagerFactory" %>
<%@ page import="com.csc.cfw.transport.TransportFactory" %>
<%@ page import="com.csc.cfw.util.CommFwSystem" %>
<%@ page import="com.csc.fw.util.Configurable" %>
<%@ page import="com.csc.fw.util.FwException" %>
<%@ page import="com.csc.fw.util.FwSystem" %>
<%@ page import="com.csc.fw.util.PropertyFileManager" %>
<%@ page import="com.csc.fw.util.ServletUtil" %>
<%@ page import="com.csc.fw.util.WebServiceFactory" %>
<%@ page import="com.csc.fw.util.XMLDocument" %>
<%@ page import="com.csc.fw.util.XMLFileConfigurator" %>
<%@ page errorPage="error.jsp" %>

<HTML>
  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <HEAD>
    <TITLE>System Control - Communications Framework </TITLE>
    <META name="GENERATOR" content="IBM WebSphere Studio">
  </HEAD>
  <BODY BGCOLOR="#C0C0C0">
    <script LANGUAGE="JavaScript">
    /*----------------------------------------------------*/
    /* Set up the values and submit the Transaction form  */
    /* to create a new process.                           */
    /*----------------------------------------------------*/
    function createNewProcessFile(tranForm)
    {
        var procName = tranForm.transProcName.value;
        if (procName == null || procName.length == 0)
        {
            alert("No Process Name Specified");
            return;
        }
        
        tranForm.creProcessName.value = procName;
        tranForm.submit();
    }
    </script>
    
    <script language="JavaScript">
    var lastSearchPos = 0;
    var lastSearchText = "";
    /*----------------------------------------------------*/
    /* Find a text string in a text area                  */
    /*----------------------------------------------------*/
    function findTextAreaString(searchForm, searchTextArea)
    {
        var searchText;
        if (searchForm.searchTextInput)
            searchText = searchForm.searchTextInput.value;
        else
            searchText = prompt("Please enter search text:",lastSearchText);
        if (!searchText)
        {
            alert("No search text specified");
            return;
        }
        if (searchText != lastSearchText)
        {
            lastSearchText = searchText;
            lastSearchPos = 0;
        }
        var data = searchTextArea.value;
        searchText = searchText.toLowerCase();
        data = data.toLowerCase();
        var datasubstr = data.substring(lastSearchPos);
        var pos = datasubstr.search(searchText);
        if (pos < 0)
        {
            alert("Search text not found");
            lastSearchPos = 0;
            return;
        }
        pos += lastSearchPos;
        var count = 0;
        for (var i = 0; i < pos; i++)
        {
            if (data.charAt(i) == '\r') 
                count++; 
        }
        if (searchTextArea.createTextRange)
        {
            var rng = searchTextArea.createTextRange();
            if (rng)
            {
                //alert("Current search Pos: " + pos);
                if (false) //if (false == rng.findText(searchText,lastSearchPos))
                    alert("Search text not found");
                else
                {
                    rng.moveStart("character", pos - count);
                    rng.collapse();
                    rng.select();
                }
            }
            
        }
        lastSearchPos = pos + searchText.length;
    }
    </script>

    <DIV ALIGN="Center" ID="HtmlDiv1">
    <TABLE>
      <TR>
        <TD ALIGN="Center"  >
          <H4>Communications Framework System Control</H4>
        </TD>
      </TR>
      <TR>
        <TD  >

<%
    FwSystem fwSys = CommFwSystem.current();
    String updateTextBuffer = null;
    // Control File Section - Determine the section to display
    String cfs = request.getParameter("cfs");
    Configurable factory = null;
    XMLFileConfigurator xmlConfig = new XMLFileConfigurator(fwSys, request);
    boolean bIE = (ServletUtil.determineBrowserVersion(request).getType() == ServletUtil.BROWSER_MSIE);
    
    /***************************/
    /* Parameters for Adapters */
    /***************************/
    factory = AdapterFactory.current();
    String adapterFileName = factory.configFile();
    List adapterFileList = factory.configFileList(null);
    if (adapterFileList == null)
        adapterFileList = new ArrayList();
    if (adapterFileList.size() == 0)
        adapterFileList.add(adapterFileName);
    String adapterPropFile = request.getParameter("adapterPropFile");
    if (adapterPropFile != null)
        adapterFileName = adapterPropFile;
    else
    {
        /* Default the filename to CommFwAdapterInfo&#x25;APPLICATION&#x25;.xml */
        String adapterFileDefault = adapterFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && adapterFileDefault.endsWith(sfx))
        {
            adapterFileDefault = adapterFileDefault.substring(0, adapterFileDefault.length() - sfx.length());
            adapterFileDefault += app + sfx;
        }
        for (int i = 0; i < adapterFileList.size(); i++)
        {
            String s = adapterFileList.get(i).toString();
            if (adapterFileDefault.equals(s))
            {
                adapterFileName = adapterFileDefault;
                break;
            }
        }
    }
    
    String adapterFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, adapterFileName);
    String errorMessageAdapter = null;
    String ronlyBuffAdapter = null;
    boolean ronlyAdapter = false;
    if (adapterFilePath == null)
    {
        // The adapter file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(adapterFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffAdapter = sw.getBuffer().toString();
            ronlyAdapter = true;
        }
    }
    
    String adapterDisplayQueryCache = request.getParameter("adapterDisplayQueryCache");
    if (adapterDisplayQueryCache != null && adapterDisplayQueryCache.length() == 0)
    	adapterDisplayQueryCache = null;
    

    /*******************************/
    /* Parameters for Transactions */
    /*******************************/
    factory = ProcessFactory.current();
    String tranFileName = factory.configFile();
    String procPath = fwSys.getProperty(FwSystem.SYSPROP_processLocation).toString();
    if (procPath.endsWith("/"))
        procPath = procPath.substring(0, procPath.length() - 1);
    String path = request.getContextPath();
    int temp = (procPath.indexOf(path) + path.length());
    procPath = procPath.substring(temp);
    procPath = getServletConfig().getServletContext().getRealPath(procPath);
    
    String tranPropFile = request.getParameter("tranPropFile");
    String newProcessName = request.getParameter("creProcessName");
    if (newProcessName != null && newProcessName.length() > 0)
    {
        // We need to create a new process file.
        String fn = "Process" + FwSystem.replaceAll(newProcessName.trim(), " ", "_") + ".xml";
        tranPropFile = fn;
        String fnPath = procPath + File.separatorChar + fn;
        File procFile = new File(fnPath);
        if (!procFile.exists())
	        procFile.createNewFile();
    }
    
    List tranFileList = factory.configFileList(procPath);
    if (tranFileList == null)
        tranFileList = new ArrayList();
    if (tranFileList.size() == 0)
        tranFileList.add(tranFileName);
    if (tranPropFile != null)
        tranFileName = tranPropFile;
    else
    {
        /* Default the filename to CommFwMessages&#x25;APPLICATION&#x25;.xml */
        String tranFileDefault = tranFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && tranFileDefault.endsWith(sfx))
        {
            tranFileDefault = tranFileDefault.substring(0, tranFileDefault.length() - sfx.length());
            tranFileDefault += app + sfx;
        }
        for (int i = 0; i < tranFileList.size(); i++)
        {
            String s = tranFileList.get(i).toString();
            if (tranFileDefault.equals(s))
            {
                tranFileName = tranFileDefault;
                break;
            }
        }
    }
    String tranFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, tranFileName);
    if (tranFilePath == null)
    {
        tranFilePath = procPath + File.separatorChar + tranFileName;
        File file = new File(tranFilePath);
        if (!file.exists())
            tranFilePath = null;
    }
    
    String errorMessageTran = null;
    String ronlyBuffTran = null;
    boolean ronlyTran = false;
    if (tranFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(tranFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffTran = sw.getBuffer().toString();
            ronlyTran = true;
        }
    }

    /*****************************/
    /* Parameters for Transports */
    /*****************************/
    factory = TransportFactory.current();
    String transportFileName = factory.configFile();
    List transportFileList = factory.configFileList(null);
    if (transportFileList == null)
        transportFileList = new ArrayList();
    if (transportFileList.size() == 0)
        transportFileList.add(transportFileName);
    String transportPropFile = request.getParameter("transportPropFile");
    if (transportPropFile != null)
        transportFileName = transportPropFile;
    else
    {
        /* Default the filename to CommFwTransport&#x25;APPLICATION&#x25;.xml */
        String transportFileDefault = transportFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && transportFileDefault.endsWith(sfx))
        {
            transportFileDefault = transportFileDefault.substring(0, transportFileDefault.length() - sfx.length());
            transportFileDefault += app + sfx;
        }
        for (int i = 0; i < transportFileList.size(); i++)
        {
            String s = transportFileList.get(i).toString();
            if (transportFileDefault.equals(s))
            {
                transportFileName = transportFileDefault;
                break;
            }
        }
    }
    
    String transportFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, transportFileName);
    String errorMessageTransport = null;
    String ronlyBuffTransport = null;
    boolean ronlyTransport = false;
    if (transportFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(transportFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffTransport = sw.getBuffer().toString();
            ronlyTransport = true;
        }
    }
	
    /********************************/
    /* Parameters for Message Route */
    /********************************/
    factory = MessageRouteFactory.current();
    String messageRouteFileName = factory.configFile();
    List messageRouteFileList = factory.configFileList(null);
    if (messageRouteFileList == null)
    	messageRouteFileList = new ArrayList();
    if (messageRouteFileList.size() == 0)
    	messageRouteFileList.add(messageRouteFileName);
    String messageRoutePropFile = request.getParameter("messageRoutePropFile");
    if (messageRoutePropFile != null)
    	messageRouteFileName = messageRoutePropFile;
    else
    {
        /* Default the filename to CommFwMessageRoute&#x25;APPLICATION&#x25;.xml */
        String messageRouteFileDefault = messageRouteFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && messageRouteFileDefault.endsWith(sfx))
        {
        	messageRouteFileDefault = messageRouteFileDefault.substring(0, messageRouteFileDefault.length() - sfx.length());
        	messageRouteFileDefault += app + sfx;
        }
        for (int i = 0; i < messageRouteFileList.size(); i++)
        {
            String s = messageRouteFileList.get(i).toString();
            if (messageRouteFileDefault.equals(s))
            {
            	messageRouteFileName = messageRouteFileDefault;
                break;
            }
        }
    }
    
    String messageRouteFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, messageRouteFileName);
    String errorMessageMessageRoute = null;
    String ronlyBuffMessageRoute = null;
    boolean ronlyMessageRoute = false;
    if (messageRouteFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(messageRouteFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffMessageRoute = sw.getBuffer().toString();
            ronlyMessageRoute = true;
        }
    }
	
    /*********************************/
    /* Parameters for Record Control */
    /*********************************/
    factory = RecordManagerFactory.current();
    String recordCtrlFileName = factory.configFile();
    List recordCtrlFileList = factory.configFileList(null);
    if (recordCtrlFileList == null)
        recordCtrlFileList = new ArrayList();
    if (recordCtrlFileList.size() == 0)
        recordCtrlFileList.add(recordCtrlFileName);
    String recordCtrlPropFile = request.getParameter("recordCtrlPropFile");
    if (recordCtrlPropFile != null)
        recordCtrlFileName = recordCtrlPropFile;
    else
    {
        /* Default the filename to CommFwRecord&#x25;APPLICATION&#x25;.xml */
        String recordCtrlFileDefault = recordCtrlFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && recordCtrlFileDefault.endsWith(sfx))
        {
            recordCtrlFileDefault = recordCtrlFileDefault.substring(0, recordCtrlFileDefault.length() - sfx.length());
            recordCtrlFileDefault += app + sfx;
        }
        for (int i = 0; i < recordCtrlFileList.size(); i++)
        {
            String s = recordCtrlFileList.get(i).toString();
            if (recordCtrlFileDefault.equals(s))
            {
                recordCtrlFileName = recordCtrlFileDefault;
                break;
            }
        }
    }
    
    String recordCtrlFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, recordCtrlFileName);
    String errorMessageRecordCtrl = null;
    String ronlyBuffRecordCtrl = null;
    boolean ronlyRecordCtrl = false;
    if (recordCtrlFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(recordCtrlFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffRecordCtrl = sw.getBuffer().toString();
            ronlyRecordCtrl = true;
        }
    }
    
    /*******************************/
    /* Parameters for Web Services */
    /*******************************/
    String webSvcFileName = new WebServiceFactory().configFile();
    String webSvcFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, webSvcFileName);
    String errorMessageWebSvc = null;
    String ronlyBuffWebSvc = null;
    boolean ronlyWebSvc = false;
    if (webSvcFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(webSvcFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffWebSvc = sw.getBuffer().toString();
            ronlyWebSvc = true;
        }
    }

    if ("POST".equals(request.getMethod()))
    {
        String func = request.getParameter("function");
        if ("Reset Adapter Control Cache".equalsIgnoreCase(func))
        {
            AdapterFactory.reset();
        }
        else
        if ("Reset Data Query cache".equalsIgnoreCase(func) && adapterDisplayQueryCache != null)
        {
			String name = adapterDisplayQueryCache;
			factory = AdapterFactory.current();
			AdapterProperties properties = (AdapterProperties)factory.configEntryMap(name, null);
			MessageQueryCache cache = properties.getCachedQueryTransaction();
			cache.cacheClear();
        }
        if ("Update Adapter Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("AdapterTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        adapterFilePath,
                        adapterFileName,
                        "�dapter",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageAdapter = "<FONT COLOR='#FF0000'><B>Updated Adapter XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageAdapter == null)
                try
                {
                    PropertyFileManager.writePropertyFile(adapterFilePath, updateTextBuffer);
                    AdapterFactory.current().configSetFileCache(adapterFileName, updateTextBuffer);
                    AdapterFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageAdapter = "<FONT COLOR='#FF0000'><B>Error updating Adapter file::<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Transport Control Cache".equalsIgnoreCase(func))
        {
            TransportFactory.reset();
        }
        else
        if ("Update Transport Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("TransportTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        transportFilePath,
                        transportFileName,
                        "Transport",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageTransport = "<FONT COLOR='#FF0000'><B>Updated Transport XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageTransport == null)
                try
                {
                    PropertyFileManager.writePropertyFile(transportFilePath, updateTextBuffer);
                    TransportFactory.current().configSetFileCache(transportFileName, updateTextBuffer);
                    TransportFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageTransport = "<FONT COLOR='#FF0000'><B>Error updating Transport file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Transaction Control Cache".equalsIgnoreCase(func))
        {
            ProcessFactory.reset();
        }
        else
        if ("Update Transaction Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("TransactionTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                boolean isMsgFile = tranFileName.startsWith("CommFwMessages");
                if (isMsgFile)
                {
                    xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        tranFilePath,
                        tranFileName,
                        "Message",
                        "name",
                        true,
                        true,
                        "location");
                }
                else
                {
                    xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        tranFilePath,
                        tranFileName,
                        "Transaction Control",
                        null,
                        false,
                        false,
                        null);
                }
            }
            catch (FwException e)
            {
                errorMessageTran = "<FONT COLOR='#FF0000'><B>Updated Transaction/Message XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageTran == null)
                try
                {
                    PropertyFileManager.writePropertyFile(tranFilePath, updateTextBuffer);
                    ProcessFactory.current().configSetFileCache(tranFileName, updateTextBuffer);
                    ProcessFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageTran = "<FONT COLOR='#FF0000'><B>Error updating Transaction/Message file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Record Control Cache".equalsIgnoreCase(func))
        {
            RecordManagerFactory.reset();
        }
        else
        if ("Update Record Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("RecordTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        recordCtrlFilePath,
                        recordCtrlFileName,
                        "Record Control",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageRecordCtrl = "<FONT COLOR='#FF0000'><B>Updated Record Control XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageRecordCtrl == null)
                try
                {
                    PropertyFileManager.writePropertyFile(recordCtrlFilePath, updateTextBuffer);
                    RecordManagerFactory.current().configSetFileCache(recordCtrlFileName, updateTextBuffer);
                    RecordManagerFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageRecordCtrl = "<FONT COLOR='#FF0000'><B>Error updating Record Control file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Message Route Control Cache".equalsIgnoreCase(func))
        {
            MessageRouteFactory.reset();
        }
        else
        if ("Update Message Route Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("MessageRouteTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        messageRouteFilePath,
                        messageRouteFileName,
                        "Message Route",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageMessageRoute = "<FONT COLOR='#FF0000'><B>Updated Message Route XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageMessageRoute == null)
                try
                {
                    PropertyFileManager.writePropertyFile(messageRouteFilePath, updateTextBuffer);
                    MessageRouteFactory.current().configSetFileCache(messageRouteFileName, updateTextBuffer);
                    MessageRouteFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageMessageRoute = "<FONT COLOR='#FF0000'><B>Error updating Message Route file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Web Services Control Cache".equalsIgnoreCase(func))
        {
            WebServiceFactory.reset();
        }
        else
        if ("Update Web Services Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("WebSvcTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        webSvcFilePath,
                        webSvcFileName,
                        "Web Services",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageWebSvc = "<FONT COLOR='#FF0000'><B>Updated Web Services XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageWebSvc == null)
                try
                {
                    PropertyFileManager.writePropertyFile(webSvcFilePath, updateTextBuffer);
                    WebServiceFactory.storeFactoryFileCache(updateTextBuffer);
                    WebServiceFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageWebSvc = "<FONT COLOR='#FF0000'><B>Error updating Web Services file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
    }
%>
          <P />
          <TABLE>
            <TR ALIGN="Center">
             <TD ALIGN="Center"  >
              <TABLE>

                <TR>
                  <TD WIDTH="100%"  >
                    <TABLE cellspacing="0" WIDTH="100%">
                      <TR>
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                      <TR>
                        <TD ALIGN="left"><A href="CommFwSys.jsp">System Configuration</A></TD>
                        <TD ALIGN="center"><A href="CommFwCtrl.jsp">System Control</A></TD>
                        <TD ALIGN="center"><A href="CommFwCfg.jsp">Application Configuration</A></TD>
                        <TD ALIGN="right"><A href="CommFwStatus.jsp">System Status</A></TD>			
                      </TR>
                      <TR>
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                      <TR>
                        <TD COLSPAN="4">
                          <TABLE cellspacing="0" WIDTH="100%">
                            <TR>
                              <TD ALIGN="left"><div align="center"><A href="CommFwCtrl.jsp?cfs=adapters">Adapter Control</A></div></TD>
                              <TD ALIGN="center"><div align="center"><A href="CommFwCtrl.jsp?cfs=transports">Transport Control</A></div></TD>
                              <TD ALIGN="right"><div align="center"><A href="CommFwCtrl.jsp?cfs=transactions">Transaction Control</A></div></TD>
                            </TR>
                            <TR>
                              <TD ALIGN="left"><div align="center"><A href="CommFwCtrl.jsp?cfs=records">Record Control</A></div></TD>
                              <TD ALIGN="center"><div align="center"><A href="CommFwCtrl.jsp?cfs=messageRoutes">Message Route Control</A></div></TD>
                              <TD ALIGN="right"><div align="center"><A href="CommFwCtrl.jsp?cfs=webSvcs">Web Services Control</A></div></TD>
                            </TR>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>
                <TR>
                  <TD COLSPAN="3"><HR WIDTH="100%"></TD>
                </TR>


<%
		if (cfs == null || "adapters".equals(cfs))
		{
			if (adapterDisplayQueryCache == null)
			{
			// Begin Adapters Control Section
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgAdapterForm" ACTION="CommFwCtrl.jsp?cfs=adapters">
                    <input name="adapterDisplayQueryCache" type="hidden" />&nbsp;
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Adapter Control</FONT></FONT></B>
                              </TD>
                            </TR>
                            <TR>
                              <TD WIDTH="100%">
                                <B>Current Adapters:</B><BR />
                                <TABLE class="PAGEHLIGHT" BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
                                  <TR>
                                    <TH ALIGN="center">Application</TH>
                                    <TH ALIGN="center">Location</TH>
                                    <TH ALIGN="center">Transport</TH>
                                    <TH ALIGN="center">MessageRoute</TH>
                                    <TH ALIGN="center">CommSfx</TH>
                                  </TR>
<%
				String colorClass = null;
				factory = AdapterFactory.current();
				Object[] names = factory.configEntryKeys(null);
				for (int i = 0; i < names.length; i++)
				{
					String name = names[i].toString();
					AdapterProperties properties = (AdapterProperties)factory.configEntryMap(name, null);
%>
                                  <TR>
                                    <TD class="DARKBACK" COLSPAN="5"><img src="includes/spacer.gif" width="100%" height="1px"></TD>
                                  </TR>
                                  <TR>
                                    <TD ALIGN="left" COLSPAN="5"><B><%=name%></B></TD>
                                  </TR>
                                  <TR>
                                    <TD ALIGN="center"><%="["+(String)properties.get("Application")+"]"%></TD>
                                    <TD ALIGN="center"><%="["+(String)properties.get("Location")+"]"%></TD>
                                    <TD ALIGN="center"><%="["+(String)properties.get("CommunicationTransport")+"]"%></TD>
                                    <TD ALIGN="center"><%="["+(String)properties.get("MessageRoute")+"]"%></TD>
                                    <TD ALIGN="center"><%="["+(String)properties.get("CommSfx")+"]"%></TD>
                                  </TR>
<%				
					if (properties.getCachedQueryTransaction() != null)
					{
						MessageQueryCache cache = properties.getCachedQueryTransaction();
						int cacheCnt = cache.getCacheItemCnt();
%>
                                  <TR>
                                    <TD colspan="5">
                                       <input class="btnNoSize" value="Data query cache" type="button" NAME="function" 
                                              onClick="document.CfwCfgAdapterForm.adapterDisplayQueryCache.value='<%=name%>'; document.CfwCfgAdapterForm.submit();"/>&nbsp;
                                       Cached queries: <%= cacheCnt %>
                                    </TD>
                                  </TR>
<%                        
					}
%>
<%				}
				
%>                              </TABLE>
                              </TD>
                            </TR>
                            
                            <TR>
                              <TD>
                                <A NAME="adapters"></A>
              <%
				if (adapterFileList.size() > 1)
				{
              %>
                               <SELECT NAME="adapterPropFile" SIZE="1" onChange="document.CfwCfgAdapterForm.submit()">
              <%
						for (int i = 0; i < adapterFileList.size(); i++)
						{
							String filename = (String)adapterFileList.get(i);
              %>
                                 <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(adapterFileName,filename) %>><%=filename%></OPTION>
              <%
						}
              %>
                               </SELECT>
              <%
				}
              %>
                              </TD>
                            </TR>
                            
<%
		if (adapterFilePath != null || ronlyAdapter)
		{
			if (adapterFilePath != null)
			{
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT" readonly="readonly" value="<%=adapterFilePath%>" title="<%=adapterFilePath%>"></TD>
                            </TR>
                            
<%
			}
			
			if (errorMessageAdapter != null)
			{
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageAdapter %><BR />
                              </TD>
                            </TR>
<%		
			}
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="AdapterTextArea">
<%
			if (errorMessageAdapter != null)
			{
%><%=XMLDocument.encodeXml(updateTextBuffer, false)%><%
			}
			else
			if (ronlyAdapter)
			{
%><%=XMLDocument.encodeXml(ronlyBuffAdapter, false)%><%
			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(adapterFilePath, writer);
%><%=XMLDocument.encodeXml(sw.toString(), false)%><%
			}
%></TEXTAREA>
                              </TD>
                            </TR>

<%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgAdapterForm, document.CfwCfgAdapterForm.AdapterTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>
 
                            <TR>
                              <TD ALIGN="Left"  >
                                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Reset Adapter Control Cache" type="submit" name="function">
<%
			if (!ronlyAdapter)
			{
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Update Adapter Control File" type="submit" NAME="function">
<%
			}
%>
                              </TD>
                            </TR>
<%
		}
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
			// End Adapters Control Section
			}
			else /**if (adapterDisplayQueryCache == null)*/
			{
			// Begin Adapter query cache Section
				String name = adapterDisplayQueryCache;
				factory = AdapterFactory.current();
				AdapterProperties properties = (AdapterProperties)factory.configEntryMap(name, null);
				MessageQueryCache cache = properties.getCachedQueryTransaction();
				int cacheCnt = cache.getCacheItemCnt();
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgAdapterForm" ACTION="CommFwCtrl.jsp?cfs=adapters">
                    <input name="adapterDisplayQueryCache" type="hidden" value="<%=adapterDisplayQueryCache %>"/>&nbsp;
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Adapter Data Query Cache</FONT></FONT></B>
                              </TD>
                            </TR>
                            <TR>
                              <TD WIDTH="100%">
                                <B>Adapter: </B><I><%=name %></I><BR />
                                <TABLE class="PAGEHLIGHT" BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
                                 <TR>
                                   <TD class="DARKBACK" COLSPAN="2"><img src="includes/spacer.gif" width="100%" height="1px"></TD>
                                 </TR>
                                 <TR>
                                   <TD ALIGN="left" ><B>Cache Size</B></TD>
                                   <TD ALIGN="left" ><%=cache.getCacheSize()%></TD>
                                 </TR>
                                 <TR>
                                   <TD ALIGN="left" ><B>Cache Count</B></TD>
                                   <TD ALIGN="left" ><%=cacheCnt%></TD>
                                 </TR>
                                </TABLE>
                              </TD>
                            </TR>
                            <TR>
                              <TD WIDTH="100%">
                                <TABLE class="PAGEHLIGHT" BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<%
				String colorClass = null;
				Object[] items = cache.cacheList();
				for (int i = 0; i < items.length; i++)
				{
					String item = items[i].toString();
%>
                                  <TR>
                                    <TD class="DARKBACK" COLSPAN="5"><img src="includes/spacer.gif" width="100%" height="1px"></TD>
                                  </TR>
                                  <TR>
                                    <TD ALIGN="left" COLSPAN="5"><%=item%></TD>
                                  </TR>
<%				}
				
%>
                                  <TR>
                                    <TD class="DARKBACK" COLSPAN="5"><img src="includes/spacer.gif" width="100%" height="1px"></TD>
                                  </TR>
                              </TABLE>
                              </TD>
                            </TR>
                            

                            <TR>
                              <TD ALIGN="Left"  >
                                <input class="btnNoSize" value="Reset data query cache" type="submit" name="function">
                                <input class="btnNoSize" value="Return to Adapters" type="submit" NAME="function"
                                       onClick="document.CfwCfgAdapterForm.adapterDisplayQueryCache.value=null; document.CfwCfgAdapterForm.submit();"/>&nbsp;
                              </TD>
                            </TR>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
			// End Adapters Dispay Query Cache  Section
			}
		}
%>


<%
		if ("transports".equals(cfs))
		{
			// Begin Transports Control Section
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgTransportForm" ACTION="CommFwCtrl.jsp?cfs=transports">
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Transport Control</FONT></FONT></B>
                              </TD>
                            </TR>

                            <!-- Start Display List of Transports -->
                            <TR>
                              <TD WIDTH="100%"  >
                                <B>Current Transports:</B><BR>
                                <TABLE class="PAGEHLIGHT" BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<%
				String colorClass = null;
				factory = TransportFactory.current();
				Object[] configList = factory.configEntryKeys(null);
				List list = new ArrayList();
				for (int i = 0; i < configList.length; i++)
				{
					list.add(configList[i].toString());
				}
				for (int i = 0; i < list.size(); i++)
				{
					String name = list.get(i).toString();
%>
                                  <TR>
                                    <TD class="DARKBACK" WIDTH="100%"><img src="includes/spacer.gif" width="100%" height="1px"></TD>
                                  </TR>
                                  <TR>
                                    <TD ALIGN="left" WIDTH="100%"><B><%=name%></B></TD>
                                  </TR>
<%				}
%>                              </TABLE>
                              </TD>
                            </TR>
                            <!--  End Display List of Transports -->

                            <TR>
                              <TD>
              <%
				if (transportFileList.size() > 1)
				{
              %>
                               <SELECT NAME="transportPropFile" SIZE="1" onChange="document.CfwCfgTransportForm.submit()">
              <%
						for (int i = 0; i < transportFileList.size(); i++)
						{
							String filename = (String)transportFileList.get(i);
              %>
                                 <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(transportFileName,filename) %>><%=filename%></OPTION>
              <%
						}
              %>
                               </SELECT>
              <%
				}
              %>
                              </TD>
                            </TR>

<%
		if (transportFilePath != null || ronlyTransport)
		{
			if (transportFilePath != null)
			{
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT"  readonly="readonly"  value="<%=transportFilePath%>" title="<%=transportFilePath%>"></TD>
                            </TR>
<%
			}
			
			if (errorMessageTransport != null)
			{
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageTransport %><BR />
                              </TD>
                            </TR>
<%		
			}
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="TransportTextArea">
<%
			if (errorMessageTransport != null)
			{
%><%=XMLDocument.encodeXml(updateTextBuffer, false)%><%
			}
			else
			if (ronlyTransport)
			{
%><%=XMLDocument.encodeXml(ronlyBuffTransport, false)%><%
			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(transportFilePath, writer);
%><%=XMLDocument.encodeXml(sw.toString(), false)%><%
			}
%></TEXTAREA>
                              </TD>
                            </TR>

<%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgTransportForm, document.CfwCfgTransportForm.TransportTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>
 
                            <TR>
                              <TD ALIGN="Left"  >
                                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Reset Transport Control Cache" type="submit" name="function">
<%
			if (!ronlyTransport)
			{
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Update Transport Control File" type="submit" name="function">
<%
			}
%>
                              </TD>
                            </TR>
<%
		}
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
			// End Transports Control Section
		}
%>


<%
		if ("transactions".equals(cfs))
		{
			// Begin Transactions Control Section
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgTranForm" ACTION="CommFwCtrl.jsp?cfs=transactions">
                    <A NAME="transactions"></A>
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Transaction Control</FONT></FONT></B>
                              </TD>
                            </TR>
                            <TR>
                              <TD>
                                <TABLE width="100%">
                                  <TR>
              <%
				if (tranFileList != null && tranFileList.size() > 1)
				{
              %>
                                    <TD>
                                     <SELECT NAME="tranPropFile" SIZE="1" onChange="document.CfwCfgTranForm.submit()">
              <%
						for (int i = 0; i < tranFileList.size(); i++)
						{
							String filename = (String)tranFileList.get(i);
              %>
                                       <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(tranFileName,filename) %>><%=filename%></OPTION>
              <%
						}
              %>
                                     </SELECT>
                                    </TD>
              <%
				}
              %>
                            
                                    <TD align="right">
                                      <TABLE>
                                        <TR>
                                          <TD>
                                            <A name='newProcessAnchor' href="javascript:createNewProcessFile(document.CfwCfgTranForm);">New Process:</A>
                                            <input type='hidden' name='creProcessName' />
                                          </TD>
                                        </TR>
                                        <TR>
                                          <TD >Process<input name='transProcName' />.xml</TD>
                                        </TR>
                                      </TABLE>
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%
		if (tranFilePath != null || ronlyTran)
		{
			if (tranFilePath != null)
			{
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT" readonly="readonly" value="<%=tranFilePath%>"title="<%=tranFilePath%>"></TD>
                            </TR>
<%
			}
			
			if (errorMessageTran != null)
			{
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageTran %><BR />
                              </TD>
                            </TR>
<%		
			}
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="TransactionTextArea">
<%
			if (errorMessageTran != null)
			{
%><%=XMLDocument.encodeXml(updateTextBuffer, false)%><%
			}
			else
			if (ronlyTran)
			{
%><%=XMLDocument.encodeXml(ronlyBuffTran, false)%><%
			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(tranFilePath, writer);
%><%=XMLDocument.encodeXml(sw.toString(), false)%><%
			}
%></TEXTAREA>
                              </TD>
                            </TR>
 
<%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgTranForm, document.CfwCfgTranForm.TransactionTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>
 
                            <TR>
                              <TD ALIGN="Left"  >
                                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Reset Transaction Control Cache" type="submit" name="function">
<%
			if (!ronlyTran)
			{
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Update Transaction Control File" type="submit" name="function">
<%
			}
%>
                              </TD>
                            </TR>
<%
		}
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
			// End Transactions Control Section
		}
%>


<%
		if ("records".equals(cfs))
		{
			// Begin Record Control Section
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgRecordForm" ACTION="CommFwCtrl.jsp?cfs=records">
                    <A NAME="records"></A>
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Record Control</FONT></FONT></B>
                              </TD>
                            </TR>

                            <!--  Start Display List of Record Managers -->
                            <TR>
                              <TD WIDTH="100%"  >
                                <B>Current Record Manager List:</B><BR>
                                <TABLE class="PAGEHLIGHT" BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<%
				String colorClass = null;
				factory = RecordManagerFactory.current();
				Object[] configList = factory.configEntryKeys(null);
				List list = new ArrayList();
				for (int i = 0; i < configList.length; i++)
				{
					list.add(configList[i].toString());
				}
				for (int i = 0; i < list.size(); i++)
				{
					String name = list.get(i).toString();
%>
                                  <TR>
                                    <TD class="DARKBACK" WIDTH="100%"><img src="includes/spacer.gif" width="100%" height="1px"/></TD>
                                  </TR>
                                  <TR>
                                    <TD ALIGN="left" WIDTH="100%"><B><%=name%></B></TD>
                                  </TR>
<%				}
%>                              </TABLE>
                              </TD>
                            </TR>
                            <!--  End Display List of Record Managers -->

                            <TR>
                              <TD>
              <%
				if (recordCtrlFileList.size() > 1)
				{
              %>
                               <SELECT NAME="recordCtrlPropFile" SIZE="1" onChange="document.CfwCfgRecordForm.submit()">
              <%
						for (int i = 0; i < recordCtrlFileList.size(); i++)
						{
							String filename = (String)recordCtrlFileList.get(i);
              %>
                                 <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(recordCtrlFileName,filename) %>><%=filename%></OPTION>
              <%
						}
              %>
                               </SELECT>
              <%
				}
              %>
                              </TD>
                            </TR>

<%
		if (recordCtrlFilePath != null || ronlyRecordCtrl)
		{
			if (recordCtrlFilePath != null)
			{
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT" readonly="readonly" value="<%=recordCtrlFilePath%>" title="<%=recordCtrlFilePath%>"></TD>
                            </TR>
<%
			}
			
			if (errorMessageRecordCtrl != null)
			{
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageRecordCtrl %><BR />
                              </TD>
                            </TR>
<%		
			}
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="RecordTextArea">
<%
			if (errorMessageRecordCtrl != null)
			{
%><%=XMLDocument.encodeXml(updateTextBuffer, false)%><%
			}
			else
			if (ronlyRecordCtrl)
			{
%><%=XMLDocument.encodeXml(ronlyBuffRecordCtrl, false)%><%
			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(recordCtrlFilePath, writer);
%><%=XMLDocument.encodeXml(sw.toString(), false)%><%
			}
%></TEXTAREA>
                              </TD>
                            </TR>

<%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgRecordForm, document.CfwCfgRecordForm.RecordTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>
 
                            <TR>
                              <TD ALIGN="Left"  >
                                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Reset Record Control Cache" type="submit" name="function">
<%
			if (!ronlyRecordCtrl)
			{
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Update Record Control File" type="submit" name="function">
<%
			}
%>
                              </TD>
                            </TR>
<%
		}
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
			// End Record Control Section
		}
%>


<%
		if ("messageRoutes".equals(cfs))
		{
			// Begin Message Route Section
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgMessageRouteForm" ACTION="CommFwCtrl.jsp?cfs=messageRoutes">
                    <A NAME="messageRoutes"></A>
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Message Route</FONT></FONT></B>
                              </TD>
                            </TR>

                            <!--  Start Display List of Message Routes -->
                            <TR>
                              <TD WIDTH="100%"  >
                                <B>Current Message Route List:</B><BR>
                                <TABLE class="PAGEHLIGHT" BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<%
				String colorClass = null;
				factory = MessageRouteFactory.current();
				Object[] configList = factory.configEntryKeys(null);
				List list = new ArrayList();
				for (int i = 0; i < configList.length; i++)
				{
					list.add(configList[i].toString());
				}
				for (int i = 0; i < list.size(); i++)
				{
					String name = list.get(i).toString();
%>
                                  <TR>
                                    <TD class="DARKBACK" WIDTH="100%"><img src="includes/spacer.gif" width="100%" height="1px"></TD>
                                  </TR>
                                  <TR>
                                    <TD ALIGN="left" WIDTH="100%"><B><%=name%></B></TD>
                                  </TR>
<%				}
%>                              </TABLE>
                              </TD>
                            </TR>
                            <!--  End Display List of Message Routes -->

                            <TR>
                              <TD>
              <%
				if (messageRouteFileList.size() > 1)
				{
              %>
                               <SELECT NAME="messageRoutePropFile" SIZE="1" onChange="document.CfwCfgMessageRouteForm.submit()">
              <%
						for (int i = 0; i < messageRouteFileList.size(); i++)
						{
							String filename = (String)messageRouteFileList.get(i);
              %>
                                 <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(messageRouteFileName,filename) %>><%=filename%></OPTION>
              <%
						}
              %>
                               </SELECT>
              <%
				}
              %>
                              </TD>
                            </TR>

<%
		if (messageRouteFilePath != null || ronlyMessageRoute)
		{
			if (messageRouteFilePath != null)
			{
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT" readonly="readonly" value="<%=messageRouteFilePath%>" title="<%=messageRouteFilePath%>"></TD>
                            </TR>
<%
			}
			
			if (errorMessageMessageRoute != null)
			{
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageMessageRoute %><BR />
                              </TD>
                            </TR>
<%		
			}
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="MessageRouteTextArea">
<%
			if (errorMessageMessageRoute != null)
			{
%><%=XMLDocument.encodeXml(updateTextBuffer, false)%><%
			}
			else
			if (ronlyMessageRoute)
			{
%><%=XMLDocument.encodeXml(ronlyBuffMessageRoute, false)%><%
			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(messageRouteFilePath, writer);
%><%=XMLDocument.encodeXml(sw.toString(), false)%><%
			}
%></TEXTAREA>
                              </TD>
                            </TR>

<%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgMessageRouteForm, document.CfwCfgMessageRouteForm.MessageRouteTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>
 
                            <TR>
                              <TD ALIGN="Left"  >
                                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Reset Message Route Control Cache" type="submit" name="function">
<%
			if (!ronlyRecordCtrl)
			{
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Update Message Route Control File" type="submit" name="function">
<%
			}
%>
                              </TD>
                            </TR>
<%
		}
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
			// End Message Route Section
		}
%>

<%
		if ("webSvcs".equals(cfs))
		{
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgWebSvcForm" ACTION="CommFwCtrl.jsp?cfs=webSvcs">
                    <A NAME="webSvcs"></A>
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Web Services Control</FONT></FONT></B>
                              </TD>
                            </TR>
<%
		if (webSvcFilePath != null || ronlyWebSvc)
		{
			if (webSvcFilePath != null)
			{
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT" readonly="readonly" value="<%=webSvcFilePath%>" title="<%=webSvcFilePath%>"></TD>
                            </TR>
<%
			}
			
			if (errorMessageWebSvc != null)
			{
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageWebSvc %><BR />
                              </TD>
                            </TR>
<%		
			}
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="WebSvcTextArea">
<%
			if (errorMessageWebSvc != null)
			{
%><%=XMLDocument.encodeXml(updateTextBuffer, false)%><%
			}
			else
			if (ronlyWebSvc)
			{
%><%=XMLDocument.encodeXml(ronlyBuffWebSvc, false)%><%
			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(webSvcFilePath, writer);
%><%=XMLDocument.encodeXml(sw.toString(), false)%><%
			}
%></TEXTAREA>
                              </TD>
                            </TR>
                            <TR>
                              <TD ALIGN="Left"  >
                                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Reset Web Services Control Cache" type="submit" name="function">
<%
			if (!ronlyWebSvc)
			{
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Update Web Services Control File" type="submit" name="function">
<%
			}
%>
                              </TD>
                            </TR>
<%
		}
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
		}
%>


                <TR>
                  <TD WIDTH="100%"  >
                    <TABLE cellspacing="0" WIDTH="100%">
                      <TR>
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                      <TR>
                        <TD COLSPAN="4">
                          <TABLE cellspacing="0" WIDTH="100%">
                            <TR>
                              <TD ALIGN="left"><div align="center"><A href="CommFwCtrl.jsp?cfs=adapters">Adapter Control</A></div></TD>
                              <TD ALIGN="center"><div align="center"><A href="CommFwCtrl.jsp?cfs=transports">Transport Control</A></div></TD>
                              <TD ALIGN="right"><div align="center"><A href="CommFwCtrl.jsp?cfs=transactions">Transaction Control</A></div></TD>
                            </TR>
                            <TR>
                              <TD ALIGN="left"><div align="center"><A href="CommFwCtrl.jsp?cfs=records">Record Control</A></div></TD>
                              <TD ALIGN="center"><div align="center"><A href="CommFwCtrl.jsp?cfs=messageRoutes">Message Route Control</A></div></TD>
                              <TD ALIGN="right"><div align="center"><A href="CommFwCtrl.jsp?cfs=webSvcs">Web Services Control</A></div></TD>
                            </TR>
                          </TABLE>
                        </TD>
                      </TR>
                      <TR>
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                      <TR>
                        <TD ALIGN="left"><A href="CommFwSys.jsp">System Configuration</A></TD>
                        <TD ALIGN="center"><A href="CommFwCtrl.jsp">System Control</A></TD>
                        <TD ALIGN="center"><A href="CommFwCfg.jsp">Application Configuration</A></TD>
                        <TD ALIGN="right"><A href="CommFwStatus.jsp">System Status</A></TD>			
                      </TR>
                      <TR>
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>

              </TABLE>
             </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
    </TABLE>

    </DIV>
  </BODY>
</HTML>
