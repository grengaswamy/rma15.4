<%-- ------------------------------------------------------------------- --%>
<%-- This is a JSP to return the Communications Framework Transaction properties. --%>
<%-- ------------------------------------------------------------------- --%>
<%@page language='java' contentType='text/xml'%>
<%@page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.csc.fw.util.*" %>
<%@ page import="com.csc.cfw.util.CommFwSystem" %>
<%@ page import="com.csc.cfw.process.ProcessFactory" %>
<%@ page import="com.csc.fw.util.Configurable" %>

<%

    FwSystem fwSys = CommFwSystem.current();          
    /*******************************/
    /* Parameters for Transactions */
    /*******************************/
    Configurable factory = ProcessFactory.current();
    String tranFileName = factory.configFile();
    String procPath = fwSys.getProperty(FwSystem.SYSPROP_processLocation).toString();
    if (procPath.endsWith("/"))
        procPath = procPath.substring(0, procPath.length() - 1);
    String path = request.getContextPath();
    int temp = (procPath.indexOf(path) + path.length());
    procPath = procPath.substring(temp);
    procPath = getServletConfig().getServletContext().getRealPath(procPath);
    
    String tranPropFile = request.getParameter("tranPropFile");
    String newProcessName = request.getParameter("creProcessName");
    if (newProcessName != null && newProcessName.length() > 0)
    {
        // We need to create a new process file.
        String fn = "Process" + FwSystem.replaceAll(newProcessName.trim(), " ", "_") + ".xml";
        tranPropFile = fn;
        String fnPath = procPath + File.separatorChar + fn;
        File procFile = new File(fnPath);
        if (!procFile.exists())
	        procFile.createNewFile();
    }
    
    List tranFileList = factory.configFileList(procPath);
    if (tranFileList == null)
        tranFileList = new ArrayList();
    if (tranFileList.size() == 0)
        tranFileList.add(tranFileName);
    if (tranPropFile != null)
        tranFileName = tranPropFile;
    else
    {
        /* Default the filename to CommFwMessages&#x25;&#x25;APPLICATION&#x25;&#x25;.xml */
        String tranFileDefault = tranFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && tranFileDefault.endsWith(sfx))
        {
            tranFileDefault = tranFileDefault.substring(0, tranFileDefault.length() - sfx.length());
            tranFileDefault += app + sfx;
        }
        for (int i = 0; i < tranFileList.size(); i++)
        {
            String s = tranFileList.get(i).toString();
            if (tranFileDefault.equals(s))
            {
                tranFileName = tranFileDefault;
                break;
            }
        }
    }
    String tranFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, tranFileName);
    if (tranFilePath == null)
    {
        tranFilePath = procPath + File.separatorChar + tranFileName;
        File file = new File(tranFilePath);
        if (!file.exists())
            tranFilePath = null;
    }
    
    String errorMessageTran = null;
    String ronlyBuffTran = null;
    boolean ronlyTran = false;
    if (tranFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(tranFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffTran = sw.getBuffer().toString();
            ronlyTran = true;
        }
    }

    if (ronlyTran)
    {
%><%= ronlyBuffTran %><%
    }
    else
    {
        String fileName = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, tranFileName);
        if (fileName != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter writer = new PrintWriter(sw);
            PropertyFileManager.readPropertyFile(fileName, writer);
%>
<%=sw.toString()%>
<%
        }
    }
%>
