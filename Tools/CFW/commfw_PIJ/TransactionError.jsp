<%-- ----------------------------------------------------------------- --%>
<%-- This is a JSP to get a TransactionError                           --%>
<%-- The XML will be generated.                                        --%>
<%-- ----------------------------------------------------------------- --%>
<%@page language='java' contentType='text/html'%>
<%@page import="java.util.Enumeration" %>
<%@page import="java.util.Vector" %>
<%@page import="com.csc.fw.util.FwSystem" %>
<%@page import="com.csc.fw.util.ServletUtil" %>

<jsp:useBean id='commFwData' scope='page' class='com.csc.cfw.util.test.ApplicationData'/>
<jsp:setProperty name='commFwData' property='*' />
<%
boolean fExecute = (commFwData.function != null);
boolean fForceValidationOff = true;
commFwData.newTest();
commFwData.buildFormParmsFromRequest(request);
commFwData.getAuthCredentials(request, session);
boolean searchCriteriaScreen = false;
boolean canListScreen = "canList".equals(request.getParameter("nextScreen"));
boolean detailScreen = "detail".equals(request.getParameter("nextScreen"));
String refNumber = commFwData.getFormParameter("referenceNumber");
if (!fExecute || refNumber == null || refNumber.length() == 0)
{
    searchCriteriaScreen = true;
    canListScreen = false;
    detailScreen = false;
}
else
if (canListScreen && !"YES".equals(request.getParameter("truncatedSearch")))
{
    canListScreen = false;
    detailScreen = true;
}	
if (fExecute)
{
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "noTransformCache", request.getParameter("noTransformCache")));
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "setOverrideProperties", commFwData.getOverridePropertiesOption()));
	commFwData.setXslTraceOptions(request.getParameterValues("xslTraceOptions"));
	commFwData.execute();
}
%>

<HTML>
  <HEAD>
    <META http-equiv="Content-Type" content="text/html" charset="UTF-8" />
    <LINK rel="stylesheet" type="text/css" href="includes/INFStyles.css" />
    <TITLE>Error Information Summary</TITLE>
  </HEAD>
  <BODY>
    <SCRIPT LANGUAGE="JAVASCRIPT">
      <%= commFwData.getXmlStringsAsJS() %>
    </SCRIPT>
      <TABLE border="0" width="100%">
        <TBODY>
          <TR>
            <TD width="100%" align="center">
              <FORM method="post" name="CommFwForm" action="TransactionError.jsp">
              <INPUT TYPE="hidden" NAME="tranName" value="TransactionErrorLocate">
              <INPUT TYPE="hidden" NAME="transformResponse" value="true">
              <TABLE border="0" width="100%">
<%
            if (searchCriteriaScreen)
            {
%>                    	
				<TR>
				  <TD align="center">
                    <input type="hidden" name="nextScreen" value="canList">
                    <TABLE border="0" cellpadding="0" cellspacing="0" width="70%" Cols="5">
                      <TR>
                        <TD class="PAGEHDARK" align="left" height="40px" colspan="6" valign="bottom">
                          <FONT class="fTitlePage">&nbsp;Error Information Retrieval</FONT>
                        </TD>
                      </TR>
                      <TR>
                        <TD Colspan="6"><br /></TD>
                      </TR>
                      <TR align="center">
                        <TD Align="Left" Colspan="6">
                          <input type="Radio" name="truncatedSearch" value="NO" />
                          <font class="fLabel">Perform an Exact Search</font>
                        </TD>
                      </TR>
                      <TR>
                        <TD Align="Left" Colspan="6">
                          <input type="Radio" name="truncatedSearch" value="YES" />
                            <font class="fLabel">Perform a Truncated Search</font>
                        </TD>
                      </TR>
                      <TR Align="Left">
                        <TD Colspan="3">
                          <font class="fLabel"><BR /></font>
                        </TD>
                      </TR>
                      <TR>
                        <TD Align="Left" Colspan="6">
                          <font class="fLabel">Error Reference Number:</font><br />
                          <input type="text" name="referenceNumber" id="txtReferenceNumber" size="40" style="" maxlength="10" value=""></input>
                        </TD>
                      </TR>
                      <TR Align="Left">
                        <TD Colspan="6">
                          <font class="fLabel"><BR /></font>
                        </TD>
                      </TR>
                      <TR Align="left" Height="25px">
                        <TD Align="left" Colspan="6">
                          <input TYPE="submit" name="function" value="Submit" style="width:110px" />
                        </TD>
                      </TR>
                      <TR>
                        <TD valign="top" align="left" COLSPAN="6"><HR /></TD>
                      </TR>
                    </TABLE>                  
                  </TD>
                </TR>
<%
            }
            else
            if (canListScreen)
            {
%>                    	
                <TR>
                  <TD width="100%">
                    <TABLE border="0" cellspacing="0" width="100%" cellpadding="0">
                      <TR>
                        <TD class="PAGEHDARK">&nbsp; </TD>
                        <TD class="PAGEHDARK">
                          <FONT class="fTitleApp"></FONT>
                          <BR />
                          <FONT class="fTitlePage">Error Search List</FONT>
                        </TD>
                        <TD class="PAGEHPREFILL" width="50%" />
                        <!-- <TD class="PAGEHDARK" width="50%" /> -->
                      </TR>
                    </TABLE>
                  </TD>
                </TR>
				<TR>
				  <TD>
				   <div align="center">
                    <br/>
                    <TABLE border="0" cellspacing="0" width="100%" cellpadding="0">
                      <THEAD>
                        <TR>
                          <TD>&nbsp;</TD>
                          <TD align="left" colspan="5">
                            <input type="hidden" name="nextScreen" value="detail">
                            <A onClick="document.CommFwForm.submit()">Return to Search Page</A><br/><br/>
                          </TD>
                        </TR>
                        <TR>
                          <TH>&nbsp;</TH>
                          <TH align="left">
                            <STRONG>
                              <FONT>Error Reference Number</FONT>
                            </STRONG>
                          </TH>
                          <TH align="left" Colspan="4">
                            <STRONG>
                              <FONT>Error Description</FONT>
                             </STRONG>
                          </TH>
                        </TR>
                      </THEAD>
                      <TBODY>
<%
                        boolean moreValues = true;
                        int canListCnt = 0;
                        while (moreValues)
                        {
                            String refNbr = commFwData.getFormParameter("referenceNumber" + (canListCnt + 1));
                            String errorText = commFwData.getFormParameter("errorText" + (canListCnt + 1));
                            if (refNbr != null && refNbr.length() > 0) 
                            {
%>
                        <TR>
                          <TD>
                            <input type="radio" name="referenceNumber" value="<%=refNbr%>" />&nbsp;
                          </TD>
                          <TD align="left">
                            <font><%=refNbr%></font>
                          </TD>
                          <TD align="left" Colspan="4">
                            <font><%=errorText%></font>
                          </TD>
                        </TR>
<%
                                canListCnt++;
                            }
                            else
                                moreValues = false;
                        }
%>
                        <TR Align="Left">
                          <TD Colspan="6">
                            <font class="fLabel"><BR /></font>
                          </TD>
                        </TR>
                        <TR Align="Center" Height="25px">
                          <TD Align="left" Colspan="6">
<%
                         if (canListCnt > 0)
                         {
%>
                            <input TYPE="submit" name="function" value="Submit" style="width:110px" /><br/>
<%
                         }
                         else
                         {
%>
                             <A onClick="document.CommFwForm.submit()">Return to Search Page</A>
<%
                         }
%>
                          </TD>
                        </TR>
                      </TBODY>
                    </TABLE>
                    </div>
                  </TD>
                </TR>
                <TR>
                  <TD valign="top" align="left" COLSPAN="5"><HR /></TD>
                </TR>

<%
            }
            else
            if (detailScreen)
            {
%>                  
                <TR>
                  <TD width="100%">
                    <TABLE border="0" cellspacing="0" width="100%" cellpadding="0">
                      <TR>
                        <TD class="PAGEHDARK">&nbsp; </TD>
                        <TD class="PAGEHDARK">
                          <FONT class="fTitleApp"></FONT>
                          <BR />
                          <FONT class="fTitlePage">Error Information Summary</FONT>
                        </TD>
                        <TD class="PAGEHPREFILL" width="50%" />
                        <!-- <TD class="PAGEHDARK" width="50%" /> -->
                      </TR>
                    </TABLE>
                  </TD>
                </TR>
                <TR>
                  <TD width="100%">
                    <DIV align="center">
                      <CENTER>
                        <TABLE  border="0" cellpadding="0" cellspacing="0" width="98%">
                          <TR align="center">
                            <TD align="left">&nbsp;</TD>
                          </TR>
                        </TABLE>
                      </CENTER>
                    </DIV>
                  </TD>
                </TR>
                <TR>
                  <TD Align="Center" width="100%"></TD>
                </TR>
                <TR>
                  <TD Align="left">
                    &nbsp;&nbsp;&nbsp;<A onClick="document.CommFwForm.submit()">Return to Search Page</A>
                  </TD>
                </TR>
                <TR>
                  <TD width="100%">
                    <DIV align="center">
                      <CENTER>
                        <TABLE border="0" cellpadding="0" cellspacing="0" width="98%" Cols="6">
                          <TR>
                            <TD class="PAGEHDARK" align="center" Colspan="6">
                              <STRONG>
                                <FONT CLASS="fTitleApp">Error General Information</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Error Reference Number:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="4">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Error Message:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("referenceNumber")%></font>
                            </TD>
                            <TD Colspan="4">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("ErrorText")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Failed UOW ID:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="4">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Failed UOW Name:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("FailedUOWID")%></font>
                            </TD>
                            <TD Colspan="4">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("TransactionName")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Original Failure Timestamp:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Failure Type Code:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Failed Program Name:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("SystemTimestamp")%></font>
                            </TD>
                            <TD Colspan="2"><%=commFwData.getFormParameter("ProgramErrorCd")%></TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("ProgramName")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Failed Section:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="4">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Additional Error Text:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel10pt"><%=commFwData.getFormParameter("ProgramSection")%></font>
                            </TD>
                            <TD Colspan="4">
                              <font class="fLabel10pt"><%=commFwData.getFormParameter("ProgramAdditionalInfo")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">SQLCODE:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="4">
                              <STRONG>
                                <FONT CLASS="flabel8pt">SQLERRMC:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("SQLCd")%></font>
                            </TD>
                            <TD Colspan="4">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("SQLErrorMsg")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">EIBRESP:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="4">
                              <STRONG>
                                <FONT CLASS="flabel8pt">EIBRESP2:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("SystemError1")%></font> 
                            </TD>
                            <TD Colspan="4">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("SystemError2")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Error Comment:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="4">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Failed Location ID:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("RemarkText")%></font>
                            </TD>
                            <TD Colspan="4">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("FailedLocationID")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Failed Activity:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="4">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Failure Priority:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("ActivityText")%></font>
                            </TD>
                            <TD Colspan="4">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("ErrorPriority")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Failure Key Information:</FONT>
                              </STRONG>
                            </TD>
                          </TR>                          
                         <TR>
                            <TD Colspan="6">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("KeyText")%></font>
                            </TD>
                          </TR>                          
                          <TR Height="20px">
                            <TD Colspan="6">
                                <HR />
                              <font class="fLabel">
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD class="PAGEHDARK" align="center" Colspan="6">
                              <STRONG>
                                <FONT CLASS="fTitleApp">Error Detail Information</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Message Control Module:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Primary Business Object:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Message Storage Type:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("ErrorDetailProgramName1")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("ErrorDetailProgramName2")%></font>
                             </TD>
                             <TD Colspan="2">
                               <font class="fLabel8pt"><%=commFwData.getFormParameter("StorageTypeCd")%></font>
                             </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Passed Activity:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Number of Req. Rows:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Number of Switch Rows:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("ActivityCd")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts1")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts2")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Number of Header Rows:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Number of Data Rows:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Number of NL Warn Rows:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts3")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts4")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts5")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Number of NLBE Rows:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Num. Processed Hdr Rows:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Num. Processed Data Rows:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts6")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts7")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts8")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Num. Processed NL Warn Rows:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="4">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Num. Processed NLBE Rows:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts9")%></font>
                            </TD>
                            <TD Colspan="4">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataRowCounts10")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Lock Type:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Lock Action:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Lock Delete Time Interval:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataLockTypeCd")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataLockAction")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("DataLockTime")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Lock Control Indicator:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Data Privacy Indicator:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Security Context:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("SecurityInd1")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("SecurityInd3")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("SecurityContext")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Authority Level:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Association Type:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Audit Indicator:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("SecurityLevel")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("AssociationTypeCd")%></font>
                            </TD>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("AuditInd")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Audit Module:</FONT>
                              </STRONG>
                            </TD>
                            <TD Colspan="4">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Keep/Clear Storage Indicator:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="2">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("AuditIndProgramName")%></font>
                            </TD>
                            <TD Colspan="4">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("StorageInd")%></font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <STRONG>
                                <FONT CLASS="flabel8pt">Audit Data:</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel8pt"><%=commFwData.getFormParameter("AuditText")%></font>
                            </TD>
                          </TR>
                          <TR Height="20px">
                            <TD Colspan="6">
                              <HR />
                              <font class="fLabel">
                              </font>
                            </TD>
                          </TR>
                          <TR>
                            <TD class="PAGEHDARK" align="center" Colspan="6">
                              <STRONG>
                                <FONT CLASS="fTitleApp">Error Trace Information</FONT>
                              </STRONG>
                            </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <font class="fLabel">
                                <BR />
                              </font>
                            </TD>
                          </TR>
                          <TR>
                           <TD>
                            <TABLE Width="55%" Border="0" Align="Left">
                              <TR>
                                <TD Align="Left">
                                  <STRONG>
                                    <FONT CLASS="flabel8pt">UserID:</FONT>
                                  </STRONG>
                                </TD>
                                <TD Align="Left">
                                  <STRONG>
                                    <FONT CLASS="flabel8pt">Failure Timestamp:</FONT>
                                  </STRONG>
                                </TD>
                                <TD Align="Left">
                                  <STRONG>
                                    <FONT CLASS="flabel8pt">Failure Key Information:</FONT>
                                  </STRONG>
                                </TD>
                              </TR>
                            </TABLE>
                           </TD>
                          </TR>
                          <TR>
                            <TD Colspan="6">
                              <TABLE Width="100%" Border="0">

<%
	boolean moreValues = true;
	for (int i = 0; moreValues; i++)
	{
		String otherId = commFwData.getFormParameter("ErrorTraceInfoOtherId" + (i + 1));
		if (otherId != null && otherId.length() > 0) 
		{	
%>
                                <TR>
                                  <TD align="left" Colspan="1">
                                    <font class="fLabel8pt"><%=otherId%></font>
                                  </TD>
                                  <TD align="left" Colspan="1">
                                    <font class="fLabel8pt"><%=commFwData.getFormParameter("ErrorTraceInfoSystemTimestamp" + (i + 1))%></font>
                                  </TD>
                                  <TD align="left" Colspan="4">
                                    <font class="fLabel8pt"><%=commFwData.getFormParameter("ErrorTraceInfoErrorTraceText" + (i + 1))%></font>
                                  </TD>
                                </TR>
<%
		}
		else
			moreValues = false;
	}
%>
                                 
                                <TR>
                                  <TD align="left" Colspan="6">
                                    <font class="fLabel">
                                      <BR />
                                    </font>
                                  </TD>
                                </TR>
                              </TABLE>
                            </TD>
                          </TR>
                          
                          <TR>
                            <TD valign="top" align="left" COLSPAN="6">
                              <HR />
                            </TD>
                          </TR>
                        </TABLE>
                      </CENTER>
                    </DIV>
                  </TD>
                </TR>
                <!-- <TR>
                  <TD width="100%">
                    <CENTER>
                      <TABLE border="0" cellspacing="0" cellpadding="0" width="98%">
                        <TR>
                          <TD width="80%" align="middle">
                            <A href=" " onClick="DoSubmit('EXIT');return false;">Exit</A>
                          </TD>
                        </TR>
                        <TR>
                          <TD height="15px" />
                        </TR>
                        
                      </TABLE>
                    </CENTER>
                  </TD>
                </TR> -->
                <TR>
                  <TD width="100%">
                   </TD>
                </TR>
<%
            }
%>                  
              </TABLE>


              <TABLE align="center" width="100%">
                <%@ include file="WEB-INF/jspf/CFwXMLDataForm.jspf" %>
              </TABLE>
              </FORM>

              <FORM METHOD="POST" NAME="CommFwResultForm" ACTION="">
              <INPUT TYPE="hidden" NAME="locale" 
<%  if (commFwData.getLocale() != null)
    {
%>
                                 VALUE="<jsp:getProperty name='commFwData' property='locale'/>" 
<%
    }
%>
>

                <TABLE align="center" width="100%">
                  <%@ include file="WEB-INF/jspf/CFwXMLResultForm.jspf" %>

                </TABLE>
              </FORM>
            </TD>
          </TR>
        </TBODY>
      </TABLE>
  </BODY>
</HTML>
