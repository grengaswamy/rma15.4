POST /commfw/services/CommFwService HTTP/1.0
Content-Type: text/xml; charset=utf-8
Accept: application/soap+xml, application/dime, multipart/related, text/*
User-Agent: Axis/1.1
Host: localhost
Cache-Control: no-cache
Pragma: no-cache
SOAPAction: ""
Content-Length: 2161

<?xml version="1.0" encoding="UTF-8"?>
   <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> 
      <soapenv:Body>  
         <ns1:processMessage soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://www.csc.com/cfw/soap-broker">   
            <options xsi:type="xsd:string">genNewTranID=true</options>   
            <message xsi:type="xsd:string">&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#xd;&lt;ACORD&gt;&#xd;&lt;SignonRq&gt;&#xd;&lt;SignonPswd&gt;&#xd;&lt;SignonRole/&gt;&#xd;&lt;CustId&gt;&#xd;&lt;SPName/&gt;&#xd;&lt;CustLoginId&gt;SE31301&lt;/CustLoginId&gt;&#xd;&lt;/CustId&gt;&#xd;&lt;CustPswd&gt;&#xd;&lt;EncryptionTypeCd&gt;NONE&lt;/EncryptionTypeCd&gt;&#xd;&lt;Pswd/&gt;&#xd;&lt;/CustPswd&gt;&#xd;&lt;/SignonPswd&gt;&#xd;&lt;ClientDt/&gt;&#xd;&lt;CustLangPref&gt;en_US&lt;/CustLangPref&gt;&#xd;&lt;ClientApp&gt;&#xd;&lt;Org/&gt;&#xd;&lt;Name&gt;S3&lt;/Name&gt;&#xd;&lt;Version/&gt;&#xd;&lt;/ClientApp&gt;&#xd;&lt;com.csc_SessKey&gt;45285989533154979705571211666125&lt;/com.csc_SessKey&gt;&#xd;&lt;/SignonRq&gt;&#xd;&lt;InsuranceSvcRq&gt;&#xd;&lt;RqUID&gt;59588022299608171012814167210661&lt;/RqUID&gt;&#xd;&lt;LOCATE_CLIENT&gt;&#xd;&lt;Customer&gt;&#xd;&lt;CustomerName&gt;&#xd;&lt;NameType&gt;P&lt;/NameType&gt;&#xd;&lt;PersonName&gt;&#xd;&lt;LastName&gt;McDaniel&lt;/LastName&gt;&#xd;&lt;FirstName/&gt;&#xd;&lt;MiddleName/&gt;&#xd;&lt;/PersonName&gt;&#xd;&lt;/CustomerName&gt;&#xd;&lt;CustomerAddress&gt;&#xd;&lt;Addr1/&gt;&#xd;&lt;Addr2/&gt;&#xd;&lt;City/&gt;&#xd;&lt;StateProv/&gt;&#xd;&lt;PostalCode/&gt;&#xd;&lt;/CustomerAddress&gt;&#xd;&lt;CustomerContact&gt;&#xd;&lt;ContactType&gt;P&lt;/ContactType&gt;&#xd;&lt;PersonContact&gt;&#xd;&lt;DayPhone/&gt;&#xd;&lt;EvePhone/&gt;&#xd;&lt;/PersonContact&gt;&#xd;&lt;/CustomerContact&gt;&#xd;&lt;/Customer&gt;&#xd;&lt;/LOCATE_CLIENT&gt;&#xd;&lt;/InsuranceSvcRq&gt;&#xd;&lt;/ACORD&gt;&#xd;</message>  
         </ns1:processMessage> 
      </soapenv:Body>
   </soapenv:Envelope>
