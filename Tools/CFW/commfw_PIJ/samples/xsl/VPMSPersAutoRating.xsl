<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output  method="xml" indent="yes"/>
	
    <xsl:template match="/">
	
        <VPMSPersAutoRating>
            <BUS__OBJ__RECORD>
                <xsl:copy-of select="/*/MDRV__COMMON"/>
            </BUS__OBJ__RECORD>
            <xsl:for-each select="/*/UDAT__UOW__BUS__OBJ__NM">
                <BUS__OBJ__RECORD>
                    <URQM__MSG__HDR>
                        <URQM__MSG__BUS__OBJ__NM><xsl:value-of select="."/></URQM__MSG__BUS__OBJ__NM>
                        <URQM__ACTION__CODE>FETCH</URQM__ACTION__CODE>
                        <URQM__BUS__OBJ__DATA__LENGTH/>
                    </URQM__MSG__HDR>
                    <xsl:copy-of select="following-sibling::*[1]"/>
                </BUS__OBJ__RECORD>
            </xsl:for-each>
        </VPMSPersAutoRating>

    </xsl:template>

</xsl:stylesheet>
