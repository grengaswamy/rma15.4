#-------------------------------------------------------------------------------
#--- Create Communications Framework Queues for a specified context
#-------------------------------------------------------------------------------


if {[llength $argv] < 1 || [llength $argv] > 1} {
    puts ""
    set tIncorrectNumArgs "Incorrect number of arguments"
    puts "$tIncorrectNumArgs"
    exit $FAIL
}

#-------------------------------------------------------------------------------
#--- Extract the arguments from the argument list and validate
#-------------------------------------------------------------------------------

if {[llength $argv] > 0} {
    set contextName   [lindex $argv 0]
} 
 

#-------------------------------------------------------------------------------
#--- Get the ActiveMQ Resource Adapter
#-------------------------------------------------------------------------------
set jmspRA [$AdminConfig getid "/J2CResourceAdapter:ActiveMQ JMS Resource Adapter/"]

#-------------------------------------------------------------------------------
#--- Create the QueueConnectionFactory jms/commfw_ConnectionFactory
#-------------------------------------------------------------------------------
set factoryName ${contextName}_ConnectionFactory
set jmsfactoryName jms/${contextName}_ConnectionFactory
if {[catch {$AdminTask createJ2CConnectionFactory $jmspRA [subst {-connectionFactoryInterface javax.jms.QueueConnectionFactory -name $factoryName -jndiName $jmsfactoryName}]} result]} {
           puts "Connection Factory $factoryName is not created, exception = $result"
} else {
           puts "Connection Factory $factoryName created"
}
 
#-------------------------------------------------------------------------------
#--- Create the Queue objects
#-------------------------------------------------------------------------------
set queName ${contextName}_CFWRequestQueue
set queJNDIName jms/${contextName}_CFWRequestQueue
set jmsQue [$AdminTask createJ2CAdminObject $jmspRA [subst {-adminObjectInterface javax.jms.Queue -name $queName -jndiName $queJNDIName}]]
set jmsQueProps [lindex [$AdminConfig showAttribute $jmsQue properties] 0]
foreach prop $jmsQueProps { if {[regexp PhysicalName $prop] == 1} { $AdminConfig modify $prop [subst {{value $queName}}]  } }
puts "Created queue $queName"

set queName ${contextName}_CFWResponseQueue
set queJNDIName jms/${contextName}_CFWResponseQueue
set jmsQue [$AdminTask createJ2CAdminObject $jmspRA [subst {-adminObjectInterface javax.jms.Queue -name $queName -jndiName $queJNDIName}]]
set jmsQueProps [lindex [$AdminConfig showAttribute $jmsQue properties] 0]
foreach prop $jmsQueProps { if {[regexp PhysicalName $prop] == 1} { $AdminConfig modify $prop [subst {{value $queName}}]  } }
puts "Created queue $queName"

set queName ${contextName}_CFWAsynchRequestQueue
set queJNDIName jms/${contextName}_CFWAsynchRequestQueue
set jmsQue [$AdminTask createJ2CAdminObject $jmspRA [subst {-adminObjectInterface javax.jms.Queue -name $queName -jndiName $queJNDIName}]]
set jmsQueProps [lindex [$AdminConfig showAttribute $jmsQue properties] 0]
foreach prop $jmsQueProps { if {[regexp PhysicalName $prop] == 1} { $AdminConfig modify $prop [subst {{value $queName}}]  } }
puts "Created queue $queName"

set queName ${contextName}_CFWAsynchResponseQueue
set queJNDIName jms/${contextName}_CFWAsynchResponseQueue
set jmsQue [$AdminTask createJ2CAdminObject $jmspRA [subst {-adminObjectInterface javax.jms.Queue -name $queName -jndiName $queJNDIName}]]
set jmsQueProps [lindex [$AdminConfig showAttribute $jmsQue properties] 0]
foreach prop $jmsQueProps { if {[regexp PhysicalName $prop] == 1} { $AdminConfig modify $prop [subst {{value $queName}}]  } }
puts "Created queue $queName"

set queName ${contextName}_CFWAsynchErrorQueue
set queJNDIName jms/${contextName}_CFWAsynchErrorQueue
set jmsQue [$AdminTask createJ2CAdminObject $jmspRA [subst {-adminObjectInterface javax.jms.Queue -name $queName -jndiName $queJNDIName}]]
set jmsQueProps [lindex [$AdminConfig showAttribute $jmsQue properties] 0]
foreach prop $jmsQueProps { if {[regexp PhysicalName $prop] == 1} { $AdminConfig modify $prop [subst {{value $queName}}]  } }
puts "Created queue $queName"

set queName ${contextName}_CFWQueue
set queJNDIName jms/${contextName}_CFWQueue
set jmsQue [$AdminTask createJ2CAdminObject $jmspRA [subst {-adminObjectInterface javax.jms.Queue -name $queName -jndiName $queJNDIName}]]
set jmsQueProps [lindex [$AdminConfig showAttribute $jmsQue properties] 0]
foreach prop $jmsQueProps { if {[regexp PhysicalName $prop] == 1} { $AdminConfig modify $prop [subst {{value $queName}}]  } }
puts "Created queue $queName"

#-------------------------------------------------------------------------------
#--- Save the configuration changes
#-------------------------------------------------------------------------------
$AdminConfig save
