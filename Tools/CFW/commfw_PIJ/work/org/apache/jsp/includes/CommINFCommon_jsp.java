package org.apache.jsp.includes;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.SkipPageException;

import com.csc.cfw.util.CommFwSystem;

public final class CommINFCommon_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n");
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<HTML>\r\n");
      out.write("    <HEAD>\r\n");
      out.write("        <TITLE></TITLE>\r\n");
      out.write("        <META http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\r\n");
      out.write("        <LINK href=\"InfHomeMenu.css\" type=\"text/css\" rel=\"stylesheet\">\r\n");
      out.write("        <META name=\"GENERATOR\" />\r\n");
      out.write("    </HEAD>\r\n");
      out.write("\r\n");
      out.write(" <BODY>\r\n");
      out.write("  <FORM name=\"GenericScreen\">\r\n");
      out.write("   <TABLE width=\"100%\" border=\"0\">\r\n");
      out.write("     <TBODY>\r\n");
      out.write("       <TR>\r\n");
      out.write("         <TD width=\"100%\">\r\n");
      out.write("           <TABLE width=\"100%\" border=\"0\">\r\n");
      out.write("             <TBODY>\r\n");
      out.write("              <TR>\r\n");
      out.write("                <TD align=\"middle\" width=\"100%\"></TD></TR>\r\n");
      out.write("              <TR>\r\n");
      out.write("                <TD width=\"100%\">\r\n");
      out.write("                  <DIV align=\"center\">\r\n");
      out.write("                    <CENTER>\r\n");
      out.write("                      <TABLE cellSpacing=\"0\" cols=\"1\" cellPadding=\"1\" width=\"98%\" border=\"0\">\r\n");
      out.write("                        <TBODY>\r\n");
      out.write("                          <TR height=\"65\">\r\n");
      out.write("                            <TD><FONT class=\"fLabel\"><BR /></FONT></TD></TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"INFHome.htm\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG SRC=\"b-home.gif\" border=\"0\" /></A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"../CommFwStatus.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG src=\"status.gif\" border=\"0\" name=\"CommFw Status\" Label=\"CommFw System Status\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"../CommFwSys.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG src=\"system.gif\" border=\"0\" name=\"CommFw System\" Label=\"CommFw System Settings\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"../CommFwCfg.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG src=\"Config.gif\" border=\"0\" name=\"CommFw Configuration\" Label=\"CommFw Configuration\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"../CommFwCtrl.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG src=\"control.gif\" border=\"0\" name=\"CommFw Control\" Label=\"CommFw Control\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"../QueueMonitor.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG src=\"QueueMonitor.gif\" border=\"0\" name=\"Queue Monitor\" Label=\"Queue Monitor\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"../QueueAdmin.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG src=\"QueueAdmin.gif\" border=\"0\" name=\"Queue Administration\" Label=\"Queue Administration\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"../CodeTransMenu.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG src=\"CodeTrans.gif\" border=\"0\" name=\"Code Translation Support\" Label=\"Code Translation Support\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"../ProcessDaemonMaint.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG src=\"ProcessDaemon.gif\" border=\"0\" name=\"ProcessDaemon\" Label=\"Process Daemon Maintenance\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <HR Width=\"100%\"/>\r\n");
      out.write("                              <A HREF=\"../XMLTest.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                                <IMG src=\"XMLTest.gif\" border=\"0\" name=\"XML Test\" Label=\"XML Test\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                          <TR>\r\n");
      out.write("                            <TD>\r\n");
      out.write("                              <A HREF=\"../SOAPTest.jsp\" Target=\"HomeMain\">\r\n");
      out.write("\t\t\t\t                <IMG src=\"SOAPTest.gif\" border=\"0\" name=\"SOAP Test\" Label=\"SOAP Test\"> \r\n");
      out.write("                              </A>\r\n");
      out.write("                            </TD>\r\n");
      out.write("                          </TR>\r\n");
      out.write("                         <TR>\r\n");
      out.write("                           <TD>\r\n");
      out.write("                             <A HREF=\"../ConvertCopybook.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                               <IMG src=\"Copybook.gif\" border=\"0\" name=\"XML Test\" Label=\"XML Test\"> \r\n");
      out.write("                             </A>\r\n");
      out.write("                           </TD>\r\n");
      out.write("                         </TR>\r\n");
      out.write("\t      \r\n");

        String app = (String)CommFwSystem.current().getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if ("S3".equals(app) || "XC".equals(app) || "CFW".equals(app) || "ALL".equals(app))
        {

      out.write("\r\n");
      out.write("                         <TR>\r\n");
      out.write("                           <TD>\r\n");
      out.write("                             <HR Width=\"100%\"/>\r\n");
      out.write("                             <div align=\"center\"> <b><u><font color=\"#FFFFFF\" size=\"-1\"> Exceed </font></u></b></div>\r\n");
      out.write("                             <br/>\r\n");
      out.write("                             <A HREF=\"../InfrastructureTest.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                               <IMG src=\"test.gif\" border=\"0\" name=\"Infrastructure Test\" Label=\"Infrastructure Test\"> \r\n");
      out.write("                             </A>\r\n");
      out.write("                           </TD>\r\n");
      out.write("                         </TR>\r\n");
      out.write("                         <TR>\r\n");
      out.write("                           <TD>\r\n");
      out.write("                             <A HREF=\"../ClientTest.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                               <IMG src=\"client.gif\" border=\"0\" name=\"Cleint Test\" Label=\"Cleint Test\"> \r\n");
      out.write("                             </A>\r\n");
      out.write("                           </TD>\r\n");
      out.write("                         </TR>\r\n");
      out.write("                         <TR>\r\n");
      out.write("                           <TD>\r\n");
      out.write("                             <A HREF=\"../TransactionError.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                               <IMG src=\"tranError.gif\" border=\"0\" name=\"Transaction Error\" Label=\"Transaction Error\"> \r\n");
      out.write("                             </A>\r\n");
      out.write("                           </TD>\r\n");
      out.write("                         </TR>\r\n");

            if ("true".equals(CommFwSystem.current().getProperty("IAPListenerEnabled")))
            {

      out.write("\r\n");
      out.write("                         <TR>\r\n");
      out.write("                           <TD>\r\n");
      out.write("                             <A HREF=\"../IAPMon.jsp\" Target=\"HomeMain\">\r\n");
      out.write("                               <IMG src=\"IAPMonitor.gif\" border=\"0\" name=\"IAPMonitor\" Label=\"IAP Monitor\"> \r\n");
      out.write("                             </A>\r\n");
      out.write("                           </TD>\r\n");
      out.write("                         </TR>\r\n");

            }
        }

      out.write("\r\n");
      out.write("\r\n");
      out.write("                         <TR>\r\n");
      out.write("                          <TD>\r\n");
      out.write("                            <HR Width=\"100%\"/>\r\n");
      out.write("                            <A href=\"javascript:top.window.close()\">\r\n");
      out.write("                              <IMG src=\"Exit.gif\" border=\"0\"> \r\n");
      out.write("                            </A>\r\n");
      out.write("                          </TD>\r\n");
      out.write("                         </TR>\r\n");
      out.write("                        </TBODY>\r\n");
      out.write("                      </TABLE>\r\n");
      out.write("                    </CENTER>\r\n");
      out.write("                  </DIV>\r\n");
      out.write("                </TD>\r\n");
      out.write("              </TR>\r\n");
      out.write("             </TBODY>\r\n");
      out.write("           </TABLE>\r\n");
      out.write("         </TD>\r\n");
      out.write("       </TR>\r\n");
      out.write("     </TBODY>\r\n");
      out.write("   </TABLE>\r\n");
      out.write("  </FORM>\r\n");
      out.write(" </BODY>\r\n");
      out.write("</HTML>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
