package org.apache.jsp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.SkipPageException;

import com.csc.fw.util.FwEnvelope;
import com.csc.fw.util.FwEvent;
import com.csc.fw.util.FwSecurityManager;
import com.csc.fw.util.FwSystem;
import com.csc.fw.util.LRUCache;
import com.csc.fw.util.Logger;
import com.csc.fw.util.PropertyFileConfigurator;
import com.csc.fw.util.PropertyFileManager;
import com.csc.fw.util.ServletUtil;

public final class CommFwSys_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


    public boolean executeCustomFunctions(String function)
    {
        boolean isExecuted = true;
        
        if ("Reset Record Cache".equalsIgnoreCase(function))
            com.csc.cfw.xml.domain.CobolRecordFactory.getCurrent().recordDescCache.reset();
        else
        if ("Reset Connection Manager".equalsIgnoreCase(function))
        {
            //com.csc.cfw.transport.ccf.CommFwCCFContext.reset();
            try
            {
                Class tcls = Class.forName("com.csc.cfw.transport.ccf.CommFwCCFContext");
                java.lang.reflect.Method meth = tcls.getMethod("reset", new Class[0]);
                meth.invoke(null, new Object[0]);
            }
            catch (Throwable e)
            {
                // Ignore exceptions since class may not be supported
            }
        }
        else
        if ("Reset AS400 Connection Manager".equalsIgnoreCase(function))
            com.csc.cfw.transport.as400.CommFwAS400Context.reset();
        else
            isExecuted = false;
        return isExecuted;
    }


  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(3);
    _jspx_dependants.add("/WEB-INF/jspf/FwSysFunc.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/FwSysInit.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/FwDisplay.jspf");
  }

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n");
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');

        /*******************************************************************/
        /* Customization section of FwSys/Status.jsp.  You can customize   */
        /* the following properties for your applications that use         */
        /* the Framework Utilities.                                        */
        /*******************************************************************/
        String jspTitleSys = "System Configuration - Communications Framework";
        String jspTitleStat = "Status - Communications Framework";
        String jspHeaderSys = "Communications Framework System Configuration";
        String jspHeaderStat = "Communications Framework Status";
        String jspTitleStatus = "Status/Information";
        String jspTitleAppVersion = "Communications Framework Version";
        String jspAppVersion = com.csc.cfw.Version.version();
        
        String cssURL = "includes/INFUtil.css";
        
        boolean jspDispLinks = true;
        String jspLinkNameSys = "CommFwSys.jsp";
        String jspLinkTitleSys = "System Configuration";
        String jspLinkName2 = "CommFwCtrl.jsp";
        String jspLinkTitle2 = "System Control";
        String jspLinkName3 = "CommFwCfg.jsp";
        String jspLinkTitle3 = "Application Configuration";
        String jspLinkNameStat = "CommFwStatus.jsp";
        String jspLinkTitleStat = "System Status";
        
        // Set the values of ccfCon or as400con to null if not needed.
        // Do not remove definitions.
        Object ccfCon = null;
        try
        {
            Class tcls = Class.forName("com.csc.cfw.transport.ccf.CommFwCCFContext");
            java.lang.reflect.Method meth = tcls.getMethod("getConnectionManager", new Class[0]);
            ccfCon = meth.invoke(null, new Object[0]);
        }
        catch (Throwable e)
        {
            // Ignore exceptions since class may not be supported
        }
        
        Object as400Con = null;
        try
        {
            Class tcls = Class.forName("com.csc.cfw.transport.as400.CommFwAS400Context");
            java.lang.reflect.Method meth = tcls.getMethod("getConnectionManager", new Class[0]);
            as400Con = meth.invoke(null, new Object[0]);
        }
        catch (Throwable e)
        {
            // Ignore exceptions since class may not be supported
        }
        
        String xalanVersion = null;
        try
        {
            Class xcls = Class.forName("org.apache.xalan.Version");
            java.lang.reflect.Method meth = xcls.getMethod("getVersion", new Class[0]);
            xalanVersion = (String)meth.invoke(null, new Object[0]);
        }
        catch (Throwable e)
        {
            // Ignore exceptions since class may not be supported
        }
        
        String xercesVersion = null;
        try
        {
            Class xcls = Class.forName("org.apache.xerces.impl.Version");
            java.lang.reflect.Method meth = xcls.getMethod("getVersion", new Class[0]);
            xercesVersion = (String)meth.invoke(null, new Object[0]);
        }
        catch (Throwable e)
        {
            // Ignore exceptions since class may not be supported
        }
        
        
        FwSystem fwSystem = com.csc.cfw.util.CommFwSystem.current();
        FwSecurityManager secMan = fwSystem.getSecurityManager();
        
        // Set to null if not needed.
        // Do not remove definition.
        LRUCache recordCache = com.csc.cfw.xml.domain.CobolRecordFactory.getCurrent().recordDescCache;
        LRUCache templateCache = fwSystem.getTransformCache().templateCache;
        LRUCache xmlCache = fwSystem.getXMLCache().domCache;
        
        boolean securityEnabled = false;
        boolean bLoadFrDisk = false;
        String propFilename = fwSystem.getPropertyFilename();
        String sysFilename = propFilename;
        boolean bAllMatchFiles = false;
        String propDir = "";
        String propExt = ".properties";
        String propFile = request.getParameter("propFile");
        if (propFile != null)
            propFilename = propDir + propFile;
        boolean isFileSys = propFilename.equals(sysFilename);
        Map propMap = fwSystem.getProperties();
        String jspSysText = "System ";
        if (!isFileSys)
        {
            propMap = PropertyFileManager.current().getProperties(propFilename);
            jspSysText = "";
        }
        
        // HTML Help file setup - Start
        String htmlDir = "/html";
        String htmlext=".html";
        String htmlFilename = null;
        String htmlFilePath = null;
        int idx = propFilename.lastIndexOf("/");
        int idx2 = propFilename.lastIndexOf(System.getProperty("file.separator"));
        if (idx2 > idx)
            idx = idx2;
        if(idx >= 0)
        {
            htmlFilename = propFilename.substring(idx + 1);
        }
        htmlFilename += htmlext;
        
        htmlFilePath = ServletUtil.fullFilePath(
                getServletConfig().getServletContext(), 
                request, 
                htmlFilename,htmlDir);
        // HTML Help file setup - End

        String jspPropSetButtonText = "Update Runtime Properties Only";
        String jspPropUpdateButtonText = "Update Property File";
        String jspPropHeadText = jspSysText + "Properties";
        
        /*******************************************************************/
        /* End of Customization section of FwSys.jsp.                  */
        /*******************************************************************/

      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');

        String jspTitle = jspTitleSys;
        String jspHeader = jspHeaderSys;
        String jspLinkName = jspLinkNameSys;
        String jspLinkTitle = jspLinkTitleSys;

        //Detail Cache list flag
        boolean fCacheDetailList = false;
        boolean eventWindow = false;
        String eventId = null;
        long eventTimestamp = 0;
        
        //Boolean variable for Aux windows
        boolean traceWindow = false;
        boolean sysErrorWindow = false;
        boolean traceListDetailWindow = false;
        boolean sysErrorListDetailWindow = false;

        // Main Window flags
        boolean dispFileImage = false;
        boolean editPropWindow = true;
        boolean statusWindow = true;
        boolean javaPropWindow = true;
        boolean traceSummaryWindow = true;
        boolean sysErrorSummaryWindow = true;
        boolean ccfWindow = true;
        boolean as400Window = true;
        
        int sysErrorPurgeDays = 15;
        String strSysErrorPurgeDays = (String)request.getParameter("sysErrorPurgeDays");
        if (strSysErrorPurgeDays != null)
            try
            {
                sysErrorPurgeDays = Integer.parseInt(strSysErrorPurgeDays);
            }
            catch (Exception e)
            {
            }
        
        int tracePurgeDays = 15;
        String strTracePurgeDays = (String)request.getParameter("tracePurgeDays");
        if (strTracePurgeDays != null)
            try
            {
                tracePurgeDays = Integer.parseInt(strTracePurgeDays);
            }
            catch (Exception e)
            {
            }

        String s = request.getParameter("eventWindow");
        if (s != null && s.length() > 0) 
        {
            eventWindow = true;
            eventId = request.getParameter("eventId");
            if (eventId != null)
                eventTimestamp = Long.parseLong(eventId);

            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for Tracing Window
        String strTraceWindow = request.getParameter("traceWindow");
        if (strTraceWindow != null && strTraceWindow.length() > 0) 
        {
            traceWindow = true;

            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for System Error Window
        String strSysErrorWindow = request.getParameter("sysErrorWindow");
        if (strSysErrorWindow != null && strSysErrorWindow.length() > 0) 
        {
            sysErrorWindow = true;

            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for Trace Log List Window
        String strTraceListDetailWindow = request.getParameter("traceListDetailWindow");
        if (strTraceListDetailWindow != null && strTraceListDetailWindow.length() > 0) 
        {
            traceListDetailWindow = true;

            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for System Error Log List Window
        String strSysErrorListDetailWindow = request.getParameter("sysErrorListDetailWindow");
        if (strSysErrorListDetailWindow != null && strSysErrorListDetailWindow.length() > 0) 
        {
            sysErrorListDetailWindow = true;

            // Disable main windows
            editPropWindow = false;
            statusWindow = false;
            javaPropWindow = false;
            traceSummaryWindow = false;
            sysErrorSummaryWindow = false;
            ccfWindow = false;
            as400Window = false;
        }
        
        // Added for File Image Window
        String strDispFileImageWindow = request.getParameter("dispFileImage");
        if (strDispFileImageWindow != null && strDispFileImageWindow.length() > 0) 
        {
            dispFileImage = true;
            String fileName = (String)request.getParameter("FileName");
            if (fileName == null)
                if (traceWindow)
                {
                    Logger lgr = fwSystem.getTraceLogger();
                    fileName = lgr.getFileName();
                }
                else
                if (sysErrorWindow)
                {
                    Logger lgr = fwSystem.getSystemErrorLogger();
                    fileName = lgr.getFileName();
                }
            
            if (fileName == null)
            {
                String err = "<FONT SIZE=+1><B>Internal error: No trace file found for display</B></FONT>";
              
      out.write("\r\n");
      out.write("                <HTML>\r\n");
      out.write("                  <LINK href=\"includes/INFUtil.css\" type=\"text/css\" rel=\"stylesheet\"/>\r\n");
      out.write("                  <BODY BGCOLOR=\"#C0C0C0\">\r\n");
      out.write("                            <HR SIZE=\"1\" NOSHADE=\"noshade\" WIDTH=\"100%\" /><BR />\r\n");
      out.write("                            ");
      out.print(err );
      out.write("<BR />\r\n");
      out.write("                            <HR SIZE=\"1\" NOSHADE=\"noshade\" WIDTH=\"100%\" />\r\n");
      out.write("                  </BODY>\r\n");
      out.write("                </HTML>\r\n");
      out.write("              ");
                
            }
            else
            try
            {
                FileInputStream stream = new FileInputStream(fileName);
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream, FwEnvelope.INTERNAL_ENCODING));
                Writer writer = response.getWriter();
                response.setContentType("text/plain");
                char[] buff = new char[16384];
                int cnt;
                while ((cnt = reader.read(buff)) >= 0)
                {
                    writer.write(buff, 0, cnt);
                }
                        
                reader.close();
                stream.close();
                writer.close();
            } 
            catch (IOException ex)
            {
                String err = "<FONT SIZE=+1><B>Error while loading Trace File: </B></FONT>" + ex.toString();
              
      out.write("\r\n");
      out.write("                <HTML>\r\n");
      out.write("                  <LINK href=\"includes/INFUtil.css\" type=\"text/css\" rel=\"stylesheet\"/>\r\n");
      out.write("                  <BODY BGCOLOR=\"#C0C0C0\">\r\n");
      out.write("                            <HR SIZE=\"1\" noshade=\"noshade\" WIDTH=\"100%\" /><BR />\r\n");
      out.write("                            ");
      out.print(err );
      out.write("<BR />\r\n");
      out.write("                            <HR SIZE=\"1\" noshade=\"noshade\" WIDTH=\"100%\" />\r\n");
      out.write("                  </BODY>\r\n");
      out.write("                </HTML>\r\n");
      out.write("              ");
                
            }
        }
        
        if (!dispFileImage)
        {

      out.write('\r');
      out.write('\n');
      out.write("<HTML>\r\n");
      out.write("  <HEAD>\r\n");
      out.write("    <TITLE>");
      out.print(jspTitle);
      out.write("</TITLE>\r\n");
      out.write("    <META name=\"GENERATOR\" content=\"IBM WebSphere Studio\">\r\n");
      out.write("  </HEAD>\r\n");
      out.write("  <LINK href=\"");
      out.print(cssURL);
      out.write("\" type=text/css rel=stylesheet>\r\n");
      out.write("  <BODY BGCOLOR=\"#C0C0C0\">\r\n");
      out.write("    <script language=\"JavaScript\">\r\n");
      out.write("    var lastSearchPos = 0;\r\n");
      out.write("    var lastSearchText = \"\";\r\n");
      out.write("    /*----------------------------------------------------*/\r\n");
      out.write("    /* Find a text string in a text area                  */\r\n");
      out.write("    /*----------------------------------------------------*/\r\n");
      out.write("    function findTextAreaString(searchForm, searchTextArea)\r\n");
      out.write("    {\r\n");
      out.write("        var searchText;\r\n");
      out.write("        if (searchForm.searchTextInput)\r\n");
      out.write("            searchText = searchForm.searchTextInput.value;\r\n");
      out.write("        else\r\n");
      out.write("            searchText = prompt(\"Please enter search text:\",lastSearchText);\r\n");
      out.write("        if (!searchText)\r\n");
      out.write("        {\r\n");
      out.write("            alert(\"No search text specified\");\r\n");
      out.write("            return;\r\n");
      out.write("        }\r\n");
      out.write("        if (searchText != lastSearchText)\r\n");
      out.write("        {\r\n");
      out.write("            lastSearchText = searchText;\r\n");
      out.write("            lastSearchPos = 0;\r\n");
      out.write("        }\r\n");
      out.write("        var data = searchTextArea.value;\r\n");
      out.write("        searchText = searchText.toLowerCase();\r\n");
      out.write("        data = data.toLowerCase();\r\n");
      out.write("        var datasubstr = data.substring(lastSearchPos);\r\n");
      out.write("        var pos = datasubstr.search(searchText);\r\n");
      out.write("        if (pos < 0)\r\n");
      out.write("        {\r\n");
      out.write("            alert(\"Search text not found\");\r\n");
      out.write("            lastSearchPos = 0;\r\n");
      out.write("            return;\r\n");
      out.write("        }\r\n");
      out.write("        pos += lastSearchPos;\r\n");
      out.write("        var count = 0;\r\n");
      out.write("        for (var i = 0; i < pos; i++)\r\n");
      out.write("        {\r\n");
      out.write("            if (data.charAt(i) == '\\r') \r\n");
      out.write("                count++; \r\n");
      out.write("        }\r\n");
      out.write("        if (searchTextArea.createTextRange)\r\n");
      out.write("        {\r\n");
      out.write("            var rng = searchTextArea.createTextRange();\r\n");
      out.write("            if (rng)\r\n");
      out.write("            {\r\n");
      out.write("                //alert(\"Current search Pos: \" + pos);\r\n");
      out.write("                if (false) //if (false == rng.findText(searchText,lastSearchPos))\r\n");
      out.write("                    alert(\"Search text not found\");\r\n");
      out.write("                else\r\n");
      out.write("                {\r\n");
      out.write("                    rng.moveStart(\"character\", pos - count);\r\n");
      out.write("                    rng.collapse();\r\n");
      out.write("                    rng.select();\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("            \r\n");
      out.write("        }\r\n");
      out.write("        lastSearchPos = pos + searchText.length;\r\n");
      out.write("    }\r\n");
      out.write("    </script>\r\n");
      out.write("\r\n");
      out.write("    <DIV ALIGN=\"Center\" ID=\"HtmlDiv1\">\r\n");
      out.write("    <TABLE>\r\n");
      out.write("      <TR>\r\n");
      out.write("        <TD  >\r\n");
      out.write("\r\n");
      out.write("          <TABLE CELLSPACING=\"10\">\r\n");
      out.write("            <TR ALIGN=\"Center\">\r\n");
      out.write("             <TD ALIGN=\"Center\"  >\r\n");
      out.write("              <TABLE CELLSPACING=\"15\">\r\n");
      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                    <H4 ALIGN=\"CENTER\">");
      out.print(jspHeader);
      out.write("</H4>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        String FUNC_RELOADFROMDISK   = "Reload From Disk";
        String FUNC_RESET            = "Reset";
        String FUNC_RESETXSLCACHE    = "Reset XSL Cache";
        String FUNC_RESETXMLCACHE    = "Reset XML Cache";
        String FUNC_RESETRECORDCACHE = "Reset Record Cache";
        String FUNC_CACHEDETAILLIST  = "Cache Detail List";
        String FUNC_PURGETRACELOG    = "Purge Trace Log Files";
        String FUNC_PURGESYSERRORLOG = "Purge System Error Files";
        String FUNC_DELETEXSLCLASSES = "Delete Compiled XSL";

        String func = request.getParameter("function");
        boolean bIE = (ServletUtil.determineBrowserVersion(request).getType() == ServletUtil.BROWSER_MSIE);
        
        // Properties windows editing variables/initialization
        String systemFileName = null;
        String securityFileName = null;
        boolean ronlySystem = false;
        String[] propFileList = null;
        PropertyFileConfigurator propConfig = null;
        String xformClassesDir = (String)fwSystem.getProperty("XFORMCLASSESDIR"); //$NON-NLS-1$
        boolean xformClassesExists = false;
        try {
            if (xformClassesDir != null && new File(xformClassesDir).isDirectory())
                xformClassesExists = true;
        } catch (Exception e) {}
        
        
        if (editPropWindow)
        {
            if (FUNC_RELOADFROMDISK.equalsIgnoreCase(func))
            {
                bLoadFrDisk = true;
            }
            propConfig = new PropertyFileConfigurator(fwSystem, propFilename, propMap, bLoadFrDisk, request);
            systemFileName = propConfig.getConfigFilePath();
            securityFileName = ServletUtil.fullFilePath(
                                     getServletConfig().getServletContext(), 
                                     request, 
                                     fwSystem.getSecurityManager().getSecurityFileName());
            ronlySystem = propConfig.isRonlyFile();
            
            if (bAllMatchFiles && systemFileName != null)
            {
                class Filter implements FilenameFilter
                {
                    String ext;
    
                    Filter(String exten)
                    {
                        ext = exten;
                    }
                            
                    public boolean accept(File directory, String name)
                    {
                            if (ext != null && name.endsWith(ext))
                                    return true;
                            else
                                    return false;
                    }
                };
                    
                String filesep = System.getProperty("file.separator");
                int i = systemFileName.lastIndexOf("/");
                int i2 = systemFileName.lastIndexOf(filesep);
                if (i2 > i)
                        i = i2;
                propFileList = new File(systemFileName.substring(0, i)).list(new Filter(propExt));
            }
            
            /*************************************************************************            
             * Check to see if security file initialization needs to be performed
             **************************************************************************/
            if (secMan.isSecurePropertiesEnabled() && securityFileName != null)
            {
                if (secMan.getEncryptionKey() != null)
                    securityEnabled = true;
                else
                {
                    String errReturn = secMan.initSecFile(securityFileName);
                    if (errReturn == null)
                        securityEnabled = true;
                    else
                    {
    
      out.write("\r\n");
      out.write("                    <TR>\r\n");
      out.write("                      <TD WIDTH=\"100%\"  >\r\n");
      out.write("                        <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" /><BR />\r\n");
      out.write("                        <FONT SIZE=+1><B>");
      out.print(errReturn );
      out.write("</B></FONT><BR />\r\n");
      out.write("                        <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" />\r\n");
      out.write("                      </TD>\r\n");
      out.write("                    </TR>\r\n");
      out.write("    ");
                
                    }
                }
            }
            
            if (securityEnabled && systemFileName != null)
            {
                StringBuffer buff = new StringBuffer();
                if (secMan.fixUnsecureKeys(systemFileName, buff))
                {
                    try
                    {
                        PropertyFileManager.writePropertyFile(systemFileName, buff.toString());
                    }
                    catch (IOException e)
                    {
                        String err = "<FONT SIZE=+1><B>Error updating properties file security values: </B></FONT>" + e.toString();
    
      out.write("\r\n");
      out.write("                    <TR>\r\n");
      out.write("                      <TD WIDTH=\"100%\"  >\r\n");
      out.write("                        <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" /><BR />\r\n");
      out.write("                        ");
      out.print(err );
      out.write("<BR />\r\n");
      out.write("                        <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" />\r\n");
      out.write("                      </TD>\r\n");
      out.write("                    </TR>\r\n");
      out.write("    ");
                
                    }
                }
            }
        }
            
        if (FUNC_RESETXSLCACHE.equalsIgnoreCase(func))
        {
            fwSystem.getTransformCache().reset();
            templateCache = fwSystem.getTransformCache().templateCache;
        }
        else
        if (FUNC_RESETXMLCACHE.equalsIgnoreCase(func))
        {
            fwSystem.getXMLCache().reset();
            xmlCache = fwSystem.getXMLCache().domCache;
        }
        else
        if (true/*"POST".equals(request.getMethod())*/)
        {
            if (FUNC_PURGESYSERRORLOG.equalsIgnoreCase(func)
                || FUNC_PURGETRACELOG.equalsIgnoreCase(func))
            {
                int purgeDays = 0;
                Logger lgr = null;
                if (FUNC_PURGETRACELOG.equalsIgnoreCase(func))
                {
                    lgr = fwSystem.getTraceLogger();
                    purgeDays = tracePurgeDays;
                }
                else
                if (FUNC_PURGESYSERRORLOG.equalsIgnoreCase(func))
                {
                    lgr = fwSystem.getSystemErrorLogger();
                    purgeDays = sysErrorPurgeDays;
                }
                String fileName = lgr.getFileName();
                File lgrFile = new File(fileName);
                if (lgrFile.isDirectory())
                {
                    String fileList[] = lgrFile.list();
                    long maxTimeMilli = ((long)purgeDays * (24L * 1000L * 60L * 60L));
                    Date maxDate = new Date(System.currentTimeMillis() - maxTimeMilli);
                    for (int i = 0; i < fileList.length; i++)
                    {
                        String fn = fileList[i];
                        if (!fn.endsWith(".log"))
                            continue;
                        File file = new File(fileName + "/" + fn);
                        Date fileDate = new Date(file.lastModified());
                        if (file.isFile() && maxDate.after(fileDate))
                            file.delete();
                    }
                }
            } /* end if (FUNC_PURGETRACELOG || FUNC_PURGESYSERRORLOG) */
            else
            if (FUNC_DELETEXSLCLASSES.equals(func))
            {
                String fileName = xformClassesDir;
                if (xformClassesExists)
                {
                    File clsDir = new File(fileName);
                    String fileList[] = clsDir.list();
                    for (int i = 0; i < fileList.length; i++)
                    {
                        String fn = fileList[i];
                        if (!fn.endsWith(".class"))
                            continue;
                        File file = new File(fileName + "/" + fn);
                        file.delete();
                    }
                }
            } /* end if (FUNC_DELETEXSLCLASSES.equalsIgnoreCase(func)) */
            else
            if (jspPropSetButtonText.equalsIgnoreCase(func))
            {
                String newBuffer = request.getParameter("SystemPropTextArea");
                    propConfig.updatePropertiesWithFile(newBuffer, propMap, request, isFileSys);
            }
            else
            if (FUNC_CACHEDETAILLIST.equalsIgnoreCase(func))
                fCacheDetailList = true;
            else
                // The method #executeCustomFunctions(String) return true
                // if the function was executed. If not executed, keep
                // checking.
            if (executeCustomFunctions(func))
                ;
            else
            if (jspPropUpdateButtonText.equalsIgnoreCase(func))
            {
                String newBuffer = request.getParameter("SystemPropTextArea");
                try
                {
                    propConfig.writeConfigPropertyFile(newBuffer, propMap, request, isFileSys);
                }
                catch (Exception e)
                {
                    String err = "<FONT SIZE=+1><B>Error updating properties file: </B></FONT>" + e.toString();

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                    <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" /><BR />\r\n");
      out.write("                    ");
      out.print(err );
      out.write("<BR />\r\n");
      out.write("                    <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" />\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");
                
                }
            } /* end if (jspPropUpdateButtonText.equalsIgnoreCase(func)) */
        } /* end if ("POST".equals(request.getMethod())) */
        
        /*************************************************************
        *   Start displaying windows table sections
        **************************************************************/
        
        if (jspDispLinks)
        {
            /*************************************************************
            *  Top of page links
            **************************************************************/


      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                    <TABLE WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD ALIGN=\"left\"><A href=\"");
      out.print(jspLinkNameSys);
      out.write('"');
      out.write('>');
      out.print(jspLinkTitleSys);
      out.write("</A></TD>\r\n");

            if (jspLinkName2 != null)
            {

      out.write("\r\n");
      out.write("                        <TD ALIGN=\"center\"><A href=\"");
      out.print(jspLinkName2);
      out.write('"');
      out.write('>');
      out.print(jspLinkTitle2);
      out.write("</A></TD>\r\n");

            } /* end if (jspLinkName2 != null) */
            
            if (jspLinkName3 != null)
            {

      out.write("\r\n");
      out.write("                        <TD ALIGN=\"center\"><A href=\"");
      out.print(jspLinkName3);
      out.write('"');
      out.write('>');
      out.print(jspLinkTitle3);
      out.write("</A></TD>\r\n");

            } /* end if (jspLinkName3 != null) */
            
            if (jspLinkNameStat != null)
            {

      out.write("\r\n");
      out.write("                        <TD ALIGN=\"right\"><A href=\"");
      out.print(jspLinkNameStat);
      out.write('"');
      out.write('>');
      out.print(jspLinkTitleStat);
      out.write("</A></TD>\r\n");

            } /* end if (jspLinkNameStat != null) */
            

      out.write("\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (jspDispLinks) */

        if (editPropWindow)
        {
            /*************************************************************
            *  Configuration Properties editing section
            **************************************************************/


      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfgSystemForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">");
      out.print(jspPropHeadText);
      out.write("</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            \r\n");
      out.write("                            \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("              ");

                                if (propFileList != null && propFileList.length > 1)
                                {
              
      out.write("\r\n");
      out.write("                               <SELECT NAME=\"propFile\" SIZE=\"1\" onChange=\"document.CfgSystemForm.submit()\">\r\n");
      out.write("              ");

                                    for (int i = 0; i < propFileList.length; i++)
                                    {
                                        String filename = propFileList[i];
              
      out.write("\r\n");
      out.write("                                 <OPTION VALUE=\"");
      out.print(filename);
      out.write('"');
      out.write(' ');
      out.write(' ');
      out.print(ServletUtil.setSelected(propFilename,propDir + filename) );
      out.write('>');
      out.print(filename.substring(0, filename.length() - propExt.length()));
      out.write("</OPTION>\r\n");
      out.write("              ");

                                    }
              
      out.write("\r\n");
      out.write("                               </SELECT>\r\n");
      out.write("              ");

                                }
              
      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            \r\n");
      out.write("                            ");

                                if (systemFileName != null)
                                {
                            
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD><B>Path:</B> <input size=\"80\" class=\"DISPLAY-ONLY-INPUT\" readonly=\"readonly\" value=\"");
      out.print(systemFileName);
      out.write("\" title=\"");
      out.print(systemFileName);
      out.write("\"></TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            ");

                                }
                            
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TEXTAREA COLS=\"75\" WRAP=\"OFF\" ROWS=\"20\" NAME=\"SystemPropTextArea\">\r\n");
      out.print( propConfig.getFileBuffer());
      out.write("\r\n");
      out.write("</TEXTAREA>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
			if (bIE) {   
      out.write("\r\n");
      out.write("                             <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("                                <TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"javascript:findTextAreaString(document.CfgSystemForm, document.CfgSystemForm.SystemPropTextArea);\">Search:</A>\r\n");
      out.write("                                      &nbsp;<input type=\"text\" name=\"searchTextInput\" />\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
			} /* if (bIE) */   
      out.write("\r\n");
      out.write(" \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(FUNC_RESET);
      out.write("\" type=\"reset\">\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(jspPropSetButtonText);
      out.write("\" type=\"submit\" name=\"function\">\r\n");

                                if (systemFileName != null)
                                {

      out.write("\r\n");
      out.write("                                  &nbsp;<input class=\"btnNoSize\" value=\"");
      out.print(FUNC_RELOADFROMDISK);
      out.write("\" type=\"submit\" name=\"function\">\r\n");
      out.write("                                  &nbsp;<input class=\"btnNoSize\" value=\"");
      out.print(jspPropUpdateButtonText);
      out.write("\" type=\"submit\" name=\"function\">\r\n");

                                  if (htmlFilePath != null)
                                  {

      out.write("\r\n");
      out.write("                                    &nbsp;&nbsp;&nbsp;&nbsp;<input value=\"Properties Help\" type=\"button\" name=\"help\" OnClick=\"javascript:window.open('");
      out.print("../" + htmlDir + "/"+ htmlFilename);
      out.write("', '','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,status=yes');\" >\r\n");

                                  }

      out.write('\r');
      out.write('\n');

                                }

      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (editPropWindow) */

        if (statusWindow)
        {
            /*************************************************************
            *  Status section
            **************************************************************/


      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfgStatusForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">");
      out.print(jspTitleStatus);
      out.write("</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TABLE BGCOLOR=\"#FFFFDD\" BORDER=\"1\" \"WIDTH=100%\">\r\n");

                            {
                                int xformObjectCnt = 0;
                                int xmlObjectCnt = 0;
                                int recCnt = 0;

                                if (templateCache != null)
                                    xformObjectCnt = templateCache.size();

                                if (xmlCache != null)
                                    xmlObjectCnt = xmlCache.size();

                                if (recordCache != null)
                                    recCnt = recordCache.size();

                                if (jspAppVersion != null)
                                {


      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">");
      out.print(jspTitleAppVersion);
      out.write("</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(jspAppVersion);
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                                }

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Framework Utilities Version</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(com.csc.fw.util.Version.version());
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("\r\n");

                                if (xalanVersion != null)
                                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Xalan (XSL Transformer) Version</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(xalanVersion);
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                                }

      out.write("\r\n");
      out.write("\r\n");

                                if (xercesVersion != null)
                                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Xerces (XML parser) Version</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(xercesVersion);
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                                }

      out.write("\r\n");
      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">XSL Transformer class</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(javax.xml.transform.TransformerFactory.newInstance().getClass().getName());
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">XML Parser class</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(javax.xml.parsers.DocumentBuilderFactory.newInstance().getClass().getName());
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("\r\n");

                                String webrootpath = (String)fwSystem.getProperty("WEBROOTPATH");
                                if (webrootpath != null && webrootpath.length() > 0)
                                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">WEBROOTPATH</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\"><input size=\"47\" class=\"DISPLAY-ONLY-INPUT\" readonly=\"readonly\" value=\"");
      out.print(webrootpath);
      out.write("\" title=\"");
      out.print(webrootpath);
      out.write("\"></TD>\r\n");
      out.write("                                  </TR>\r\n");

                                }

      out.write("\r\n");
      out.write("\r\n");

                                String webcontextpath = (String)fwSystem.getProperty("WEBCONTEXTPATH");
                                if (webcontextpath != null && webcontextpath.length() > 0)
                                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">WEBCONTEXTPATH</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(webcontextpath);
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                                }

      out.write("\r\n");
      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Free Memory</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(NumberFormat.getInstance().format(Runtime.getRuntime().freeMemory()));
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  \r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Total Memory</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(NumberFormat.getInstance().format(Runtime.getRuntime().totalMemory()));
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Secure Properties Enabled</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(new Boolean(fwSystem.getSecurityManager().isSecurePropertiesEnabled()).toString());
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Secure Properties Encrypted</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(new Boolean(fwSystem.getSecurityManager().isSecurePropertiesEncrypted()).toString());
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Secure Logging Enabled</TD>\r\n");
      out.write("                                    <TD class=\"SRE\" align=\"right\">");
      out.print(new Boolean(fwSystem.getSecurityManager().isSecureLoggingEnabled()).toString());
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                                if (templateCache != null)
                                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Cached Stylesheets</TD>\r\n");
      out.write("                                    <TD class=\"SRE\">\r\n");
      out.write("                                      <DIV align='right'>");
      out.print("Count: " + String.valueOf(xformObjectCnt) + "" );
      out.write("</DIV>\r\n");

                                    if (fCacheDetailList)
                                    {
                                        boolean pline = false;
                                        if (templateCache != null)
                                        {
                                            Iterator iterator = templateCache.iterator();
                                            if (iterator != null)
                                                while (iterator.hasNext())
                                                {
                                                    if (!pline)
                                                    {

      out.write("                                    <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" />\r\n");
      out.write("                                      <DIV ALIGN=\"left\">\r\n");
                                                  }

      out.write("                                    ");
      out.print(iterator.next().toString());
      out.write("<BR />\r\n");

                                                    pline = true;
                                                }
                                        } /* end if (cache != null) */
                                    
                                        if (!pline)
                                        {

      out.write("                                    </DIV>\r\n");
                                      }
                                    } /* end if (fCacheDetailList) */

      out.write("\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("\r\n");

                                } /* end if (templateCache != null) */
                                
                                if (xmlCache != null)
                                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Cached XML</TD>\r\n");
      out.write("                                    <TD class=\"SRE\">\r\n");
      out.write("                                      <DIV align='right'>");
      out.print("Count: " + String.valueOf(xmlObjectCnt) + "" );
      out.write("</DIV>\r\n");

                                    if (fCacheDetailList)
                                    {
                                        boolean pline = false;
                                        if (xmlCache != null)
                                        {
                                            Iterator iterator = xmlCache.iterator();
                                            if (iterator != null)
                                                while (iterator.hasNext())
                                                {
                                                    if (!pline)
                                                    {

      out.write("                                    <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" />\r\n");
      out.write("                                      <DIV ALIGN=\"left\">\r\n");
                                                  }

      out.write("                                    ");
      out.print(iterator.next().toString());
      out.write("<BR />\r\n");

                                                    pline = true;
                                                }
                                        } /* end if (cache != null) */
                                    
                                        if (!pline)
                                        {

      out.write("                                    </DIV>\r\n");
                                      }
                                    } /* end if (fCacheDetailList) */

      out.write("\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("\r\n");

                                } /* end if (xmlCache != null) */
                                
                                if (recordCache != null)
                                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\">Cached Records</TD>\r\n");
      out.write("                                    <TD class=\"SRE\">\r\n");
      out.write("                                      <DIV align='right'>");
      out.print("Count: " + String.valueOf(recCnt) + "");
      out.write("</DIV>\r\n");

                                    if (fCacheDetailList)
                                    {
                                        boolean pline = false;
                                        if (recordCache != null)
                                        {
                                            Iterator iterator = recordCache.iterator();
                                            if (iterator != null)
                                                while (iterator.hasNext())
                                                {
                                                    if (!pline)
                                                    {

      out.write("                                    <HR SIZE=\"0\" NOSHADE WIDTH=\"100%\" />\r\n");
      out.write("                                      <DIV ALIGN=\"left\">\r\n");
                                                  }

      out.write("                                    ");
      out.print(iterator.next().toString());
      out.write("<BR />\r\n");

                                                    pline = true;
                                                }
                                        }
                                    
                                        if (!pline)
                                        {

      out.write("                                    </DIV>\r\n");
                                      }
                                
                                    } /* end if (fCacheDetailList) */

      out.write("\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");

                                } /* end if (recordCache != null) */
                            }

      out.write("\r\n");
      out.write("\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            \r\n");
      out.write("\r\n");

            boolean eventsFound = false;
            Iterator itr = fwSystem.getSystemStatus().getEventIterator();
            while (null != itr && itr.hasNext())
            {
                if (!eventsFound)
                {
                    eventsFound = true;

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <HR WIDTH=\"100%\" />\r\n");
      out.write("                                <B>System Status Events:</B><BR>\r\n");
      out.write("                                <TABLE BORDER=\"1\" \"WIDTH=100%\">\r\n");

                }
                FwEvent event = (FwEvent)itr.next();
                if (null != event)
                {
                    String bgcolor = "";
                    switch (event.level)
                    {
                        case 1:
                            bgcolor = "BGCOLOR=\"#FFFFFF\"";
                            break;
                        case 2:
                            bgcolor = "BGCOLOR=\"#FFFFBB\"";
                            break;
                        case 3:
                        case 4:
                            bgcolor = "BGCOLOR=\"#FF8080\"";
                            break;
                    }
                
                    String propFileParm = "";
                    String propFileReq = (String)request.getParameter("propFile");
                    if (propFileReq != null)
                        propFileParm = "propFile=" + propFileReq + "&";
            

      out.write("\r\n");
      out.write("                                  <TR ");
      out.print(bgcolor );
      out.write(" >\r\n");
      out.write("                                    <TD><A href=\"");
      out.print(jspLinkName);
      out.write('?');
      out.print(propFileParm);
      out.write("eventWindow=true&eventId=");
      out.print(Long.toString(event.timestamp) );
      out.write('"');
      out.write(' ');
      out.write('>');
      out.print(new Date(event.timestamp).toString() );
      out.write("</A></TD><TD>");
      out.print(event.description );
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                }
            } /* end while (itr.hasNext()) */

            if (eventsFound)
            {

      out.write("\r\n");
      out.write("                              </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

            } /* end if (eventsFound) */
        

      out.write("\r\n");
      out.write("\r\n");
          if (xformClassesExists || templateCache != null || xmlCache != null || recordCache != null)
            { 
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <HR WIDTH=\"100%\" />\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(FUNC_CACHEDETAILLIST);
      out.write("\" type=\"submit\" name=\"function\">\r\n");
              if (templateCache != null)
                { 
      out.write("\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(FUNC_RESETXSLCACHE);
      out.write("\" type=\"submit\" name=\"function\">\r\n");
              }
                if (xmlCache != null)
                { 
      out.write("\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(FUNC_RESETXMLCACHE);
      out.write("\" type=\"submit\" name=\"function\">\r\n");
              }
                if (recordCache != null)
                { 
      out.write("\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(FUNC_RESETRECORDCACHE);
      out.write("\" type=\"submit\" name=\"function\">\r\n");
              }
                if (xformClassesExists)
                { 
      out.write("\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(FUNC_DELETEXSLCLASSES);
      out.write("\" type=\"submit\" name=\"function\">\r\n");
              } 
      out.write("\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
          } /* end if (templateCache != null || xmlCache != null || recordCache != null)*/ 
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (statusWindow) */

        if (ccfWindow && ccfCon != null)
        {
            /*************************************************************
            *  Common Connector Framework summary section
            **************************************************************/


      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfgCCFStatusForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Common Connector Framework Status/Information</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TABLE BORDER=\"1\" \"WIDTH=100%\">\r\n");

                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"SRE\" colspan=\"2\">");
      out.print("Connection Management:\n&nbsp;&nbsp;" + FwSystem.replaceAll(FwSystem.replaceAll(FwSystem.replaceAll(ccfCon.toString(), ",", "<BR/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"), "\n", "<BR/>&nbsp;&nbsp;"), " ", "&nbsp;") );
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                }

      out.write("                              </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <HR WIDTH=\"100%\" />\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Reset Connection Manager\" type=\"submit\" name=\"function\">\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (ccfWindow && ccfCon != null) */

        if (as400Window && as400Con != null)
        {
            /*************************************************************
            *  AS400 Connection Manager section
            **************************************************************/


      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfgAS400StatusForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">AS400 Connection Status/Information</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TABLE BORDER=\"1\" \"WIDTH=100%\">\r\n");

                                try
                                {
                                    Method getUsers =
                                        as400Con.getClass().getMethod(
                                                "getUsers",
                                                new Class[] {} );
                                    Enumeration keys = 
                                        (Enumeration)getUsers.invoke(
                                                as400Con,
                                                new Object[] {} );
                                    while (keys.hasMoreElements())
                                    {
                                        String key = (String)keys.nextElement();

      out.write("\r\n");
      out.write("                                   <TR><TD class=\"SRE\">");
      out.print("Connection:     " + key );
      out.write("</TD></TR>\r\n");

                                    }
                                }
                                catch (Exception e)
                                {
                                }
                                
                                try
                                {
                                    // CleanupInterval
                                    Method getCleanupInterval =
                                        as400Con.getClass().getMethod(
                                                "getCleanupInterval",
                                                new Class[] {} );
                                    Object cleanupInterval = 
                                        getCleanupInterval.invoke(
                                                as400Con,
                                                new Object[] {} );
                                    // MaxConnections
                                    Method getMaxConnections =
                                        as400Con.getClass().getMethod(
                                                "getMaxConnections",
                                                new Class[] {} );
                                    Object maxConnections = 
                                        getMaxConnections.invoke(
                                                as400Con,
                                                new Object[] {} );
                                    // MaxInactivity
                                    Method getMaxInactivity =
                                        as400Con.getClass().getMethod(
                                                "getMaxInactivity",
                                                new Class[] {} );
                                    Object maxInactivity = 
                                        getMaxInactivity.invoke(
                                                as400Con,
                                                new Object[] {} );
                                    // MaxLifetime
                                    Method getMaxLifetime =
                                        as400Con.getClass().getMethod(
                                                "getMaxLifetime",
                                                new Class[] {} );
                                    Object maxLifetime = 
                                        getMaxLifetime.invoke(
                                                as400Con,
                                                new Object[] {} );
                                    // MaxUseCount
                                    Method getMaxUseCount =
                                        as400Con.getClass().getMethod(
                                                "getMaxUseCount",
                                                new Class[] {} );
                                    Object maxUseCount = 
                                        getMaxUseCount.invoke(
                                                as400Con,
                                                new Object[] {} );
                                    // MaxUseTime
                                    Method getMaxUseTime =
                                        as400Con.getClass().getMethod(
                                                "getMaxUseTime",
                                                new Class[] {} );
                                    Object maxUseTime = 
                                        getMaxUseTime.invoke(
                                                as400Con,
                                                new Object[] {} );

      out.write("\r\n");
      out.write("                                   <TR><TD class=\"SRE\">");
      out.print("CleanupInterval: " + cleanupInterval.toString() );
      out.write("</TD></TR>\r\n");
      out.write("                                   <TR><TD class=\"SRE\">");
      out.print("MaxConnections:  " + maxConnections.toString() );
      out.write("</TD></TR>\r\n");
      out.write("                                   <TR><TD class=\"SRE\">");
      out.print("MaxInactivity:   " + maxInactivity.toString() );
      out.write("</TD></TR>\r\n");
      out.write("                                   <TR><TD class=\"SRE\">");
      out.print("MaxLifetime:     " + maxLifetime.toString() );
      out.write("</TD></TR>\r\n");
      out.write("                                   <TR><TD class=\"SRE\">");
      out.print("MaxUseCount:     " + maxUseCount.toString() );
      out.write("</TD></TR>\r\n");
      out.write("                                   <TR><TD class=\"SRE\">");
      out.print("MaxUseTime:      " + maxUseTime.toString() );
      out.write("</TD></TR>\r\n");

                                }
                                catch (Exception e)
                                {
                                }

      out.write("                              </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"Left\"  >\r\n");
      out.write("                                <HR WIDTH=\"100%\" />\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Reset AS400 Connection Manager\" type=\"submit\" name=\"function\">\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (as400Window && as400Con != null) */

        if ( (traceListDetailWindow || traceSummaryWindow) 
             && fwSystem.getTraceLevel() > 0)
        {
            /*************************************************************
            *  Trace log summary section or Detail file list screen
            **************************************************************/

            Logger lgr = fwSystem.getTraceLogger();
            String fileName = lgr.getFileName();
            File lgrFile = new File(fileName);
            String propFileParm = "";
            String propFileReq = (String)request.getParameter("propFile");
            if (propFileReq != null)
                propFileParm = "propFile=" + propFileReq + "&";


      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfgTraceLogStatusForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">System Trace Logging</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

            
            if (lgrFile.isDirectory())
            {
                String fileList[] = lgrFile.list();

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>System Trace Logs :</B>&nbsp;&nbsp;&nbsp;&nbsp;Files: ");
      out.print(fileList.length);
      out.write("\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(FUNC_PURGETRACELOG);
      out.write("\" TYPE=\"SUBMIT\" NAME=\"function\"><B> over </B>\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(tracePurgeDays);
      out.write("\" SIZE=\"5\" TYPE=\"TEXT\" NAME=\"tracePurgeDays\"><B> days old</B><BR />\r\n");
      out.write("                                <TABLE BGCOLOR=\"#FFFFDD\" BORDER=\"1\" \"WIDTH=100%\">\r\n");
      out.write("\r\n");

                if (!traceListDetailWindow)
                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"");
      out.print(jspLinkName);
      out.write('?');
      out.print(propFileParm);
      out.write("traceListDetailWindow=true\">");
      out.print(lgr.getFileName());
      out.write("</A>\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");

                }
                else
                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                     <input type=\"hidden\" name=\"traceListDetailWindow\" value=\"true\" />\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  \r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD><B>Directory:</B><BR />");
      out.print(lgr.getFileName());
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                    boolean interleaveFlag = false;
                    String colorClass = null;
                    for (int i = fileList.length - 1; i >= 0; i--)
                    {
                        String filePath = lgr.getFileName() + '/' + fileList[i];
                        fileName = fileList[i];
                        if (interleaveFlag)
                            colorClass = "SRO";
                        else
                            colorClass = "SRE";
                        
                        interleaveFlag = !interleaveFlag;

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"");
      out.print(colorClass);
      out.write("\">\r\n");
      out.write("                                      <A href=\"");
      out.print(jspLinkName);
      out.write('?');
      out.print(propFileParm);
      out.write("traceWindow=true&retWindow=traceListDetailWindow&FileName=");
      out.print(filePath);
      out.write('"');
      out.write('>');
      out.print(fileName);
      out.write("</A>\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");

                    }
                }

      out.write("                                     \r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

                if (traceListDetailWindow)
                {

      out.write("                                     \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"center\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Return to ");
      out.print(jspLinkTitle);
      out.write("\" onClick=\"document.CfgTraceLogStatusForm.traceListDetailWindow.value = ''; return true; \" type=\"submit\" name=\"tracefunction\">&nbsp;&nbsp;\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

                }
            } /* end if (lgrFile.isDirectory()) */
            else 
            {
                long fileSize = lgrFile.length();
                boolean dspFileImage = (fileSize > 100000);
                String targAttr = dspFileImage ? "target=\"window\" " : "";
                String fileImageParm = dspFileImage ? "dispFileImage=true&" : "";

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>System Trace Log :</B><BR>\r\n");
      out.write("                                <TABLE BGCOLOR=\"#FFFFDD\" BORDER=\"1\" \"WIDTH=100%\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"");
      out.print(jspLinkName);
      out.write('?');
      out.print(fileImageParm);
      out.print(propFileParm);
      out.write("traceWindow=true\" ");
      out.print(targAttr);
      out.write(' ');
      out.write('>');
      out.print(fileName);
      out.write("</A>\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR> \r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

            } /* end else if (lgrFile.isDirectory()) */

      out.write("\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if ( (sysErrorListDetailWindow || sysErrorSummaryWindow) */

        if ( (sysErrorListDetailWindow || sysErrorSummaryWindow) 
             && fwSystem.getSystemErrorLogger().getLevel() > 0)
        {
            /*************************************************************
            *  System error summary section or Detail file list screen
            **************************************************************/

            Logger lgr = fwSystem.getSystemErrorLogger();
            String fileName = lgr.getFileName();
            File lgrFile = new File(fileName);
            String propFileParm = "";
            String propFileReq = (String)request.getParameter("propFile");
            if (propFileReq != null)
                propFileParm = "propFile=" + propFileReq + "&";

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfgSystemErrorLogStatusForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">System Error Logging</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

            if (lgrFile.isDirectory())
            {
                String fileList[] = lgrFile.list();

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>System Error Logs :</B>&nbsp;&nbsp;&nbsp;&nbsp;Files: ");
      out.print(fileList.length);
      out.write("\r\n");
      out.write("                                &nbsp;&nbsp;&nbsp;&nbsp;\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(FUNC_PURGESYSERRORLOG);
      out.write("\" type=\"submit\" name=\"function\"><B> over </B>\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"");
      out.print(sysErrorPurgeDays);
      out.write("\" size=\"5\" type=\"text\" name=\"sysErrorPurgeDays\"><B> days old</B><BR />\r\n");
      out.write("                                <TABLE BGCOLOR=\"#FFFFDD\" BORDER=\"1\" \"WIDTH=100%\">\r\n");

                if (!sysErrorListDetailWindow)
                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"");
      out.print(jspLinkName);
      out.write('?');
      out.print(propFileParm);
      out.write("sysErrorListDetailWindow=true\">");
      out.print(lgr.getFileName());
      out.write("</A>\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");

                }
                else
                {

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                     <input type=\"hidden\" name=\"sysErrorListDetailWindow\" value=\"true\" />\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  \r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD><B>Directory:</B><BR />");
      out.print(lgr.getFileName());
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                    boolean interleaveFlag = false;
                    String colorClass = null;
                    for (int i = fileList.length - 1; i >= 0; i--)
                    {
                        String filePath = lgr.getFileName() + '/' + fileList[i];
                        fileName = fileList[i];
                        if (interleaveFlag)
                            colorClass = "SRO";
                        else
                            colorClass = "SRE";
                        
                        interleaveFlag = !interleaveFlag;

      out.write("\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD class=\"");
      out.print(colorClass);
      out.write("\">\r\n");
      out.write("                                      <A href=\"");
      out.print(jspLinkName);
      out.write("?sysErrorWindow=true&retWindow=sysErrorListDetailWindow&");
      out.print(propFileParm);
      out.write("FileName=");
      out.print(filePath);
      out.write('"');
      out.write('>');
      out.print(fileName);
      out.write("</A>\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");

                    }
                }

      out.write("                                     \r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

                if (sysErrorListDetailWindow)
                {

      out.write("                                     \r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"center\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Return to ");
      out.print(jspLinkTitle);
      out.write("\" onClick=\"document.CfgSystemErrorLogStatusForm.sysErrorListDetailWindow.value = ''; return true; \" type=\"submit\" name=\"tracefunction\">&nbsp;&nbsp;\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

                }
            } /* end if (lgrFile.isDirectory()) */
            else 
            {
                long fileSize = lgrFile.length();
                boolean dspFileImage = (fileSize > 100000);
                String targAttr = dspFileImage ? "target=\"window\" " : "";
                String fileImageParm = dspFileImage ? "dispFileImage=true&" : "";

      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>System Error Log :</B><BR>\r\n");
      out.write("                                <TABLE BGCOLOR=\"#FFFFDD\" BORDER=\"1\" \"WIDTH=100%\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"");
      out.print(jspLinkName);
      out.write('?');
      out.print(fileImageParm);
      out.print(propFileParm);
      out.write("sysErrorWindow=true\" ");
      out.print(targAttr);
      out.write(' ');
      out.write('>');
      out.print(fileName);
      out.write("</A>\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR> \r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");

            } /* end else if (lgrFile.isDirectory()) */

      out.write("\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if ( (sysErrorListDetailWindow || sysErrorSummaryWindow) */

        if (javaPropWindow)
        {
            /*************************************************************
            *  Java System Properties section
            **************************************************************/

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"CfgJavaSystemForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Java System Properties</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <TEXTAREA COLS=\"75\" WRAP=\"OFF\" READONLY ROWS=\"15\" NAME=\"SystemPropTextArea\">\r\n");

                                {
                                    ArrayList list = new ArrayList(System.getProperties().keySet());
                                    Collections.sort(list);
                                    Iterator items = list.iterator();
                                    Map sysProps = System.getProperties();
                                    while (items.hasNext())
                                    {
                                        String key = (String)items.next();
                                        Object oval = (sysProps.get(key));
                                        String val = (oval != null ? oval.toString() : "[null]");

      out.print(key + "=" + val );
      out.write('\r');
      out.write('\n');

                                    }
                                }

      out.write("</TEXTAREA>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
			if (bIE) {   
      out.write("\r\n");
      out.write("                             <TR>\r\n");
      out.write("                              <TD>\r\n");
      out.write("                                <TABLE width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
      out.write("                                  <TR>\r\n");
      out.write("                                    <TD>\r\n");
      out.write("                                      <A href=\"javascript:findTextAreaString(document.CfgJavaSystemForm, document.CfgJavaSystemForm.SystemPropTextArea);\">Search:</A>\r\n");
      out.write("                                      &nbsp;<input type=\"text\" name=\"searchTextInput\" />\r\n");
      out.write("                                    </TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
			} /* if (bIE) */   
      out.write("\r\n");
      out.write(" \r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (javaPropWindow) */

        if (eventWindow)
        {
            /*************************************************************
            *  Event Detail screen
            **************************************************************/

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"EventDetailForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <input type=\"hidden\" name=\"eventWindow\" value=\"true\" />\r\n");
      out.write("                    <input type=\"hidden\" name=\"eventId\" value=\"");
      out.print(eventId );
      out.write("\" />\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">System Event Detail</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>System Status Event Id: ");
      out.print(eventId );
      out.write("</B><BR>\r\n");
      out.write("                                <TABLE BGCOLOR=\"#FFFFDD\" BORDER=\"1\" \"WIDTH=100%\">\r\n");
                                
                String lastId = null;
                String nextId = null;
                Iterator itr = fwSystem.getSystemStatus().getEventIterator();
                while (itr.hasNext())
                {
                    FwEvent event = (FwEvent)itr.next();
                    if (event.timestamp == eventTimestamp)
                    {
                        String bgcolor = "";
                        switch (event.level)
                        {
                            case 1:
                                bgcolor = "BGCOLOR=\"#FFFFFF\"";
                                break;
                            case 2:
                                bgcolor = "BGCOLOR=\"#FFFFBB\"";
                                break;
                            case 3:
                            case 4:
                                bgcolor = "BGCOLOR=\"#FF8080\"";
                                break;
                        }

      out.write("\r\n");
      out.write("                                  <TR ");
      out.print(bgcolor );
      out.write(" >\r\n");
      out.write("                                    <TD><B>Date:</B></TD><TD>");
      out.print(new Date(event.timestamp).toString() );
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");
      out.write("                                  <TR ");
      out.print(bgcolor );
      out.write(" >\r\n");
      out.write("                                    <TD><B>Event Description:</B></TD><TD>");
      out.print(event.description );
      out.write("</TD>\r\n");
      out.write("                                  </TR>\r\n");

                        if (event.excText != null)
                        {

      out.write("\r\n");
      out.write("                                  <TR ");
      out.print(bgcolor );
      out.write(" >\r\n");
      out.write("                                    <TD><B>Event Detail:</B></TD>\r\n");
      out.write("                                    <TD><TEXTAREA COLS=\"70\" WRAP=\"OFF\" ROWS=\"15\">");
      out.print(FwSystem.replaceAll(event.excText, "<", "&lt;")  );
      out.write("</TEXTAREA></TD>\r\n");
      out.write("                                  </TR>\r\n");

                        }
                        if (event.message != null)
                        {

      out.write("\r\n");
      out.write("                                  <TR ");
      out.print(bgcolor );
      out.write(" >\r\n");
      out.write("                                    <TD><B>Event Message:</B></TD>\r\n");
      out.write("                                    <TD><TEXTAREA COLS=\"70\" WRAP=\"OFF\" ROWS=\"20\">");
      out.print(FwSystem.replaceAll(event.message, "<", "&lt;") );
      out.write("</TEXTAREA></TD>\r\n");
      out.write("                                  </TR>\r\n");

                        }

                        if (itr.hasNext())
                        {
                            FwEvent nextEvent = (FwEvent)itr.next();
                            nextId = "" + nextEvent.timestamp;
                        }

                        break;
                    }
                    else
                    {
                        lastId = "" + event.timestamp;
                    }
                    
                } /* end while (itr.hasNext()) */

      out.write("\r\n");
      out.write("                                </TABLE>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"center\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"&lt;&lt;\" type=\"submit\" ");
      out.print( lastId != null ? " onClick=\"document.EventDetailForm.eventId.value='" + lastId + "'; return true; \" " : "disabled='true'; " );
      out.write("name=\"eventfunction\">&nbsp;&nbsp;\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Return to ");
      out.print(jspLinkTitle);
      out.write("\" onClick=\"document.EventDetailForm.eventWindow.value = ''; return true; \" type=\"submit\" name=\"eventfunction\">&nbsp;&nbsp;\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"&gt;&gt;\" type=\"submit\" ");
      out.print( nextId != null ? " onClick=\"document.EventDetailForm.eventId.value='" + nextId + "'; return true; \" " : "DISABLED='true' " );
      out.write("name=\"eventfunction\">\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (eventWindow) */

        if (traceWindow)
        {
            /*************************************************************
            *  Trace Log Detail screen
            **************************************************************/

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"LogTraceForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <input type=\"hidden\" name=\"traceWindow\" value=\"true\" />\r\n");
      out.write("                    \r\n");
      out.write("                  ");

                    String retWindow = (String)request.getParameter("retWindow");
                    if (retWindow != null)
                    {
                  
      out.write("\r\n");
      out.write("                    <input type=\"hidden\" name=\"");
      out.print(retWindow);
      out.write("\" value=\"true\" />\r\n");
      out.write("                    \r\n");
      out.write("                  ");

                    }
                    String propFileBack = (String)request.getParameter("propFile");
                    Logger lgr = fwSystem.getTraceLogger();
                    String fileName = lgr.getFileName();
                    File lgrFile = new File(fileName);
                        
                    if (lgrFile.isDirectory())
                    {
                        fileName = (String)request.getParameter("FileName");
                    }
                        
                    StringBuffer lgrMessage = null;
                     
                    try
                    {
                        FileInputStream stream = new FileInputStream(fileName);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, FwEnvelope.INTERNAL_ENCODING));
                        lgrMessage = new StringBuffer();
                        int i;
                        while ((i = reader.read()) >= 0)
                        {
                            char ch = (char)i;
                            switch (ch)
                            {
                                case '&':
                                    lgrMessage.append("&amp;");
                                    break;
                                case '<':
                                    lgrMessage.append("&lt;");
                                    break;
                                default:
                                    lgrMessage.append(ch);
                                    break;
                            }
                        }
                        
                        reader.close();
                        stream.close();
                    } 
                    catch (IOException ex)
                    {
                        String err = "<FONT SIZE=+1><B>Error while loading Trace File: </B></FONT>" + ex.toString();
                  
      out.write("\r\n");
      out.write("                            <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" /><BR />\r\n");
      out.write("                            ");
      out.print(err );
      out.write("<BR />\r\n");
      out.write("                            <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" />\r\n");
      out.write("                  ");
                
                    }
                    if (propFileBack != null)
                    {
                  
      out.write("\r\n");
      out.write("                        <input type=\"hidden\" name=\"propFile\" value=\"");
      out.print(propFileBack);
      out.write("\" />\r\n");
      out.write("                  ");
                
                    }
                  
      out.write("\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">Logging Trace Detail</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>Logging Trace File Name:</B> <input size=\"72\" class=\"DISPLAY-ONLY-INPUT\" readonly=\"readonly\" value=\"");
      out.print(fileName );
      out.write("\" title=\"");
      out.print(fileName );
      out.write("\"><BR>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                                <TD>\r\n");
      out.write("                                    <TEXTAREA COLS=\"85\" WRAP=\"OFF\" ROWS=\"25\">");
      out.print( lgrMessage.toString() );
      out.write("</TEXTAREA>\r\n");
      out.write("                                </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"center\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Return to ");
      out.print(jspLinkTitle);
      out.write("\" onClick=\"document.LogTraceForm.traceWindow.value = ''; return true; \" type=\"submit\" name=\"tracefunction\">&nbsp;&nbsp;\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (traceWindow) */

        if (sysErrorWindow)
        {
            /*************************************************************
            *  System Error Log Detail screen
            **************************************************************/

      out.write("\r\n");
      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                   <FORM METHOD=\"POST\" NAME=\"SysErrorForm\" ACTION=\"");
      out.print(jspLinkName);
      out.write("\">\r\n");
      out.write("                    <input type=\"hidden\" name=\"sysErrorWindow\" value=\"true\" />\r\n");
      out.write("                    \r\n");
      out.write("                  ");

                    String retWindow = (String)request.getParameter("retWindow");
                    if (retWindow != null)
                    {
                  
      out.write("\r\n");
      out.write("                    <input type=\"hidden\" name=\"");
      out.print(retWindow);
      out.write("\" value=\"true\" />\r\n");
      out.write("                    \r\n");
      out.write("                  ");

                    }
                    String propFileBack = (String)request.getParameter("propFile");
                    Logger lgr = fwSystem.getSystemErrorLogger();
                    String fileName = lgr.getFileName();
                    File lgrFile = new File(fileName);
                        
                    if (lgrFile.isDirectory())
                    {
                        fileName = (String)request.getParameter("FileName");
                    }
                        
                    StringBuffer lgrMessage = null;
                     
                    try
                    {
                        FileInputStream stream = new FileInputStream(fileName);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, FwEnvelope.INTERNAL_ENCODING));
                        lgrMessage = new StringBuffer();
                        int i;
                        while ((i = reader.read()) >= 0)
                        {
                            char ch = (char)i;
                            switch (ch)
                            {
                                case '&':
                                    lgrMessage.append("&amp;");
                                    break;
                                case '<':
                                    lgrMessage.append("&lt;");
                                    break;
                                default:
                                    lgrMessage.append(ch);
                                    break;
                            }
                        }
                        
                        reader.close();
                        stream.close();
                    } 
                    catch (IOException ex)
                    {
                        String err = "<FONT SIZE=+1><B>Error while loading System Error File: </B></FONT>" + ex.toString();
                  
      out.write("\r\n");
      out.write("                            <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" /><BR />\r\n");
      out.write("                            ");
      out.print(err );
      out.write("<BR />\r\n");
      out.write("                            <HR SIZE=\"1\" NOSHADE WIDTH=\"100%\" />\r\n");
      out.write("                  ");
                
                    }
                    if (propFileBack != null)
                    {
                  
      out.write("\r\n");
      out.write("                    <input type=\"hidden\" name=\"propFile\" value=\"");
      out.print(propFileBack);
      out.write("\" />\r\n");
      out.write("                  ");
                
                    }
                  
      out.write("\r\n");
      out.write("                    <TABLE BORDER=\"2\" WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD WIDTH=\"100%\"  >\r\n");
      out.write("                          <TABLE WIDTH=\"100%\">\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD BGCOLOR=\"#0000FF\" WIDTH=\"100%\"  ><B><FONT SIZE=\"3\"><FONT COLOR=\"#FFFFFF\">System Error Detail</FONT></FONT></B>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD WIDTH=\"100%\"  >\r\n");
      out.write("                                <B>System Error File Name:</B> <input size=\"72\" class=\"DISPLAY-ONLY-INPUT\" readonly=\"readonly\" value=\"");
      out.print(fileName );
      out.write("\" title=\"");
      out.print(fileName );
      out.write("\"><BR>\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                                <TD>\r\n");
      out.write("                                    <TEXTAREA COLS=\"85\" WRAP=\"OFF\" ROWS=\"25\">");
      out.print( lgrMessage.toString() );
      out.write("</TEXTAREA>\r\n");
      out.write("                                </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                            <TR>\r\n");
      out.write("                              <TD ALIGN=\"center\"  >\r\n");
      out.write("                                <input class=\"btnNoSize\" value=\"Return to ");
      out.print(jspLinkTitle);
      out.write("\" onClick=\"document.SysErrorForm.sysErrorWindow.value = ''; return true; \" type=\"submit\" name=\"sysErrorFunction\">&nbsp;&nbsp;\r\n");
      out.write("                              </TD>\r\n");
      out.write("                            </TR>\r\n");
      out.write("                          </TABLE>\r\n");
      out.write("                        </TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                   </FORM>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (sysErrorWindow) */

        if (jspDispLinks)
        {
            /*************************************************************
            *  Bottom of page links
            **************************************************************/

      out.write("\r\n");
      out.write("                <TR>\r\n");
      out.write("                  <TD WIDTH=\"100%\"  >\r\n");
      out.write("                    <TABLE WIDTH=\"100%\">\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD ALIGN=\"left\"><A href=\"");
      out.print(jspLinkNameSys);
      out.write('"');
      out.write('>');
      out.print(jspLinkTitleSys);
      out.write("</A></TD>\r\n");

            if (jspLinkName2 != null)
            {

      out.write("\r\n");
      out.write("                        <TD ALIGN=\"center\"><A href=\"");
      out.print(jspLinkName2);
      out.write('"');
      out.write('>');
      out.print(jspLinkTitle2);
      out.write("</A></TD>\r\n");

            } /* end if (jspLinkName2 != null) */
            
            if (jspLinkName3 != null)
            {

      out.write("\r\n");
      out.write("                        <TD ALIGN=\"center\"><A href=\"");
      out.print(jspLinkName3);
      out.write('"');
      out.write('>');
      out.print(jspLinkTitle3);
      out.write("</A></TD>\r\n");

            } /* end if (jspLinkName3 != null) */
            
            if (jspLinkNameStat != null)
            {

      out.write("\r\n");
      out.write("                        <TD ALIGN=\"right\"><A href=\"");
      out.print(jspLinkNameStat);
      out.write('"');
      out.write('>');
      out.print(jspLinkTitleStat);
      out.write("</A></TD>\r\n");

            } /* end if (jspLinkNameStat != null) */
            

      out.write("\r\n");
      out.write("            \r\n");
      out.write("                      </TR>\r\n");
      out.write("                      <TR>\r\n");
      out.write("                        <TD COLSPAN=\"4\"><HR WIDTH=\"100%\"></TD>\r\n");
      out.write("                      </TR>\r\n");
      out.write("                    </TABLE>\r\n");
      out.write("                  </TD>\r\n");
      out.write("                </TR>\r\n");

        } /* end if (jspDispLinks) */


      out.write("\r\n");
      out.write("\r\n");
      out.write("              </TABLE>\r\n");
      out.write("             </TD>\r\n");
      out.write("            </TR>\r\n");
      out.write("          </TABLE>\r\n");
      out.write("        </TD>\r\n");
      out.write("      </TR>\r\n");
      out.write("    </TABLE>\r\n");
      out.write("\r\n");
      out.write("    </DIV>\r\n");
      out.write("  </BODY>\r\n");
      out.write("</HTML>\r\n");
      out.write("\r\n");
      out.write('\r');
      out.write('\n');

        } /* if (!dispFileImage) */

      out.write("\r\n");
      out.write("        ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
