<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="lob" select="default"/>
  <xsl:param name="baseLOBline" select="default"/>
  <xsl:param name="issue-cd" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <xsl:param name="CL-vehicleunitmapping" select="default" />
  <xsl:param name="CL-propertyunitmapping" select="default" />

  <xsl:variable name="VehicleUnitMapping">
    <xsl:call-template name="ConvertLowercase">
      <xsl:with-param name="text" select="$CL-vehicleunitmapping"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="PropertyUnitMapping">
    <xsl:call-template name="ConvertLowercase">
      <xsl:with-param name="text" select="$CL-propertyunitmapping"/>
    </xsl:call-template>
  </xsl:variable>
   
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="$user-id"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="$session-id"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="$app-org"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="$app-name"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="$app-version"/>
        </xsl:with-param>
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="$rq-uid"/>
        </RqUID>
        <com.csc_PolicyUnitListRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
          <xsl:apply-templates select="/POINTJDBCRs/Rows/Row">
            <xsl:with-param name="issue-cd">
              <xsl:value-of select="$issue-cd"/>
            </xsl:with-param>
            <xsl:with-param name="lob">
              <xsl:value-of select="$lob"/>
            </xsl:with-param>
            <xsl:with-param name="baseLOBline">
              <xsl:value-of select="$baseLOBline"/>
            </xsl:with-param>
          </xsl:apply-templates>
        </com.csc_PolicyUnitListRs>
      </ClaimsSvcRs>
    </ACORD>
  </xsl:template>
  
  <xsl:template match="/POINTJDBCRs/Rows/Row" >
    <xsl:param name="issue-cd"/>
    <xsl:param name="lob"/>
    <xsl:param name="baseLOBline"/>
    <xsl:choose>
      <xsl:when test="$issue-cd = 'M'">
        <xsl:if test="string-length(STAT-UNIT-NO) > 0">
          <xsl:call-template name="InsuredUnitRow"/>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$baseLOBline = 'AL'">
          <xsl:if test="string-length(STAT-UNIT-NO) > 0">
            <xsl:call-template name="VehicleRow"/>
          </xsl:if>
        </xsl:if>
        <xsl:if test="$baseLOBline='PL'">
          <xsl:if test="string-length(STAT-UNIT-NO) > 0">
            <xsl:call-template name="PropertyRow"/>
          </xsl:if>
        </xsl:if>
        <xsl:if test="$baseLOBline='CL'">
          <xsl:variable name="UnitIdentifier">
            <xsl:call-template name="ConvertLowercase">
              <xsl:with-param name="text" select="SUMDESC"/>  
            </xsl:call-template>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="string-length(STAT-UNIT-NO) > 0 and contains($VehicleUnitMapping,$UnitIdentifier)">
              <xsl:call-template name="VehicleRow"/>
            </xsl:when>
            <xsl:when test="string-length(STAT-UNIT-NO) > 0 and contains($PropertyUnitMapping,$UnitIdentifier)">
              <xsl:call-template name="PropertyRow"/>
            </xsl:when>
          </xsl:choose>-->
        </xsl:if>
        <xsl:if test="$baseLOBline='WL'">
          <xsl:if test="string-length(STAT-UNIT-NO) > 0">
            <xsl:call-template name="WCSiteRow"/>
          </xsl:if>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="VehicleRow">
    <AutoLossInfo id="">
      <VehInfo>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_VehicleKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
            <OtherId>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="UNIT-NUMBER"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
            <OtherId>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="STAT-UNIT-NO"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
            <OtherId></OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="RATE-STATE"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="PRODUCT"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="RISK-LOC"/>             
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="RISK-SUBLOC"/>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="INSURANCE-LINE"/>
            </OtherId>
          </OtherIdentifier>
        </ItemIdInfo>
        <Manufacturer>
          <xsl:value-of select="UNIT-NAME"/>
        </Manufacturer>
        <Model>
          <xsl:value-of select="VEHICLE-MODEL"/>
        </Model>
        <ModelYear>
          <xsl:value-of select="VEHICLE-MAKE-YEAR"/>
        </ModelYear>
      </VehInfo>
      <PremiumInfo>
        <PremiumAmt>
          <xsl:value-of select="PREMIUM"/>
        </PremiumAmt>
      </PremiumInfo>
      <com.csc_PurchaseDt/>
      <com.csc_RegistrationStateProvCd/>
      <com.csc_OdometerReading>
        <NumUnits/>
        <UnitMeasurementCd/>
      </com.csc_OdometerReading>
      <com.csc_CostNewAmt>
        <Amt/>
        <CurCd/>
      </com.csc_CostNewAmt>
      <com.csc_ViolationCitation>
        <ActionCd></ActionCd>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_ViolationCitationKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <com.csc_ViolationCitationCd/>
      </com.csc_ViolationCitation>
      <com.csc_LeasedVehicleInd/>
      <com.csc_LicensePlateNumber/>
      <com.csc_VehicleIdentificationNumber>
        <xsl:value-of select="VIN"/>
      </com.csc_VehicleIdentificationNumber>
      <com.csc_LeaseEndDt/>
      <com.csc_VehicleTypeCd></com.csc_VehicleTypeCd>
      <com.csc_EngineTypeCd/>
      <com.csc_ActualSpeed>
        <NumUnits/>
        <UnitMeasurementCd/>
      </com.csc_ActualSpeed>
      <com.csc_DrivableInd/>
      <com.csc_StatedAmt>
        <Amt></Amt>
        <CurCd/>
      </com.csc_StatedAmt>
      <com.csc_TrailerInd/>
      <com.csc_DriverSameInd/>
      <com.csc_OwnerSameInd/>
      <com.csc_DriverNumber/>
      <com.csc_DriverGuiltCd/>
      <com.csc_ViolationCitationInd/>
      <com.csc_ViolationCitationCd/>
      <com.csc_RoadTypeCd/>
      <com.csc_RoadSurfaceCd/>
      <com.csc_PostedSpeedLimitNumber>
        <NumUnits/>
        <UnitMeasurementCd/>
      </com.csc_PostedSpeedLimitNumber>
      <com.csc_LightConditionCd/>
      <com.csc_DenselyPopulatedAreaInd/>
      <com.csc_TrafficLightInd/>
      <com.csc_TotalLossInd/>
      <com.csc_WherePropertyCanBeSeenInfo>
        <ActionCd></ActionCd>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <com.csc_WherePropertyCanBeSeenDesc/>
      </com.csc_WherePropertyCanBeSeenInfo>
      <com.csc_MiscellaneousInd/>
      <com.csc_VehicleUse>
        <ActionCd></ActionCd>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <com.csc_VehicleUseDesc></com.csc_VehicleUseDesc>
      </com.csc_VehicleUse>
      <com.csc_DamageDescInfo>
        <ActionCd></ActionCd>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <DamageDesc/>
      </com.csc_DamageDescInfo>
      <com.csc_UnitLossData>
        <ReportNumber/>
        <com.csc_UnitDescription>
          <ActionCd></ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_UnitDesc></com.csc_UnitDesc>
        </com.csc_UnitDescription>
        <com.csc_GarageLocation>
          <ActionCd/>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_ClmCommentKey</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_CmtFolderKey</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_GarageLocationDesc></com.csc_GarageLocationDesc>
        </com.csc_GarageLocation>
        <com.csc_LossPlace>
          <ActionCd></ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_LossPlaceDesc/>
        </com.csc_LossPlace>
        <com.csc_ObjectComment>
          <ActionCd>A</ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_ObjectCmtDesc/>
        </com.csc_ObjectComment>
        <com.csc_PermissionGivenInd/>
        <com.csc_BodilyInjuryInd/>
        <com.csc_AlcoholDrugsInd/>
        <com.csc_CurrentValueAmt>
          <Amt/>
          <CurCd/>
        </com.csc_CurrentValueAmt>
        <com.csc_ValueBeforeLossAmt>
          <Amt/>
          <CurCd/>
        </com.csc_ValueBeforeLossAmt>
        <com.csc_LossAdjustedInd/>
        <com.csc_AdjustedDt/>
        <com.csc_OtherCoverageInd/>
        <com.csc_ReinsuranceInd/>
        <com.csc_LossCessionNbr/>
        <com.csc_EstimatedLaborHours/>
        <com.csc_EstimatedMaterialCostAmt>
          <Amt/>
          <CurCd/>
        </com.csc_EstimatedMaterialCostAmt>
        <com.csc_EstimatedRepairHours/>
        <com.csc_EstimatedReplacementCostAmt>
          <Amt/>
          <CurCd/>
        </com.csc_EstimatedReplacementCostAmt>
        <com.csc_ActualLaborHours/>
        <com.csc_ActualMaterialCostAmt>
          <Amt/>
          <CurCd/>
        </com.csc_ActualMaterialCostAmt>
        <com.csc_ActualRepairHours/>
        <com.csc_ActualReplacementCostAmt>
          <Amt/>
          <CurCd/>
        </com.csc_ActualReplacementCostAmt>
        <com.csc_UnitTypeInd/>
        <com.csc_PurchaseAmt>
          <Amt/>
          <CurCd/>
        </com.csc_PurchaseAmt>
        <com.csc_AtFaultInd/>
        <com.csc_UnitStatus>
          <xsl:value-of select="TRANS-STATUS"/>
        </com.csc_UnitStatus>
        <com.csc_PermissionGiven>
          <ActionCd></ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_PermissionGivenDesc/>
        </com.csc_PermissionGiven>
        <com.csc_WhenPropertyCanBeSeen>
          <ActionCd></ActionCd>
          <ItemIdInfo>
            <OtherIdentifier>
              <OtherIdTypeCd>com.csc_TCCCheckSum</OtherIdTypeCd>
              <OtherId/>
            </OtherIdentifier>
          </ItemIdInfo>
          <com.csc_WhenPropertyCanBeSeenDesc/>
        </com.csc_WhenPropertyCanBeSeen>
      </com.csc_UnitLossData>
      <com.csc_CoverageLossInfo>
        <Coverage>
          <CoverageCd/>
        </Coverage>
      </com.csc_CoverageLossInfo>
      <com.csc_FinancialInd></com.csc_FinancialInd>
      <com.csc_ActionCd></com.csc_ActionCd>
    </AutoLossInfo>
  </xsl:template>

  <xsl:template name="PropertyRow">
    <xsl:param name="lob"/>
    <xsl:param name="baseLOBline"/>
    <PropertyLossInfo id="">
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_PropertyKey</OtherIdTypeCd>
          <OtherId/>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
          <OtherId>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value" select="UNIT-NUMBER"/>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
          <OtherId>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value" select="STAT-UNIT-NO"/>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
          <OtherId></OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="RATE-STATE"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="PRODUCT"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="RISK-LOC"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="RISK-SUBLOC"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
            <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="INSURANCE-LINE"/>
            </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <ItemDefinition>
        <ItemTypeCd/>
        <SerialIdNumber/>
        <com.csc_SerialIdNumber/>
      </ItemDefinition>
      <PremiumInfo>
        <PremiumAmt>
          <xsl:value-of select="PREMIUM"/>
        </PremiumAmt>
      </PremiumInfo>
      <com.csc_UnitStatus>
          <xsl:value-of select="TRANS-STATUS"/>
      </com.csc_UnitStatus>
      <com.csc_ConstructionCd/>
      <com.csc_ConditionClassCd/>
      <com.csc_BuildingQualityCd></com.csc_BuildingQualityCd>
      <com.csc_BuildingTotalFloorArea>
        <NumUnits></NumUnits>
        <UnitMeasurementCd/>
      </com.csc_BuildingTotalFloorArea>
      <com.csc_BuildingTotalFloorsNbr></com.csc_BuildingTotalFloorsNbr>
      <com.csc_PropertyIdentificationNumber></com.csc_PropertyIdentificationNumber>
      <com.csc_DwellUseCd></com.csc_DwellUseCd>
      <com.csc_YearBuilt/>
      <com.csc_MobileHome>
        <Length>
          <NumUnits></NumUnits>
          <UnitMeasurementCd/>
        </Length>
        <TieDownCd></TieDownCd>
        <Width>
          <NumUnits></NumUnits>
          <UnitMeasurementCd/>
        </Width>
      </com.csc_MobileHome>
      <com.csc_UnitLossData>
        <com.csc_UnitDescription>
          <ActionCd></ActionCd>
          <com.csc_UnitDesc>
            <xsl:value-of select="UNIT-NAME"/>
          </com.csc_UnitDesc>
          <com.csc_UnitAddress>
            <xsl:value-of select="PROPERTY-ADDRESS"/>
          </com.csc_UnitAddress>
          <com.csc_UnitCity>
            <xsl:value-of select="SITE-CITY"/>
          </com.csc_UnitCity>
          <com.csc_UnitState>
            <xsl:value-of select="SITE-STATE"/>
          </com.csc_UnitState>
          <com.csc_UnitZipCode>
            <xsl:value-of select="SITE-ZIP"/>
          </com.csc_UnitZipCode>
        </com.csc_UnitDescription>
      </com.csc_UnitLossData>
      <com.csc_ActionCd></com.csc_ActionCd>
      <com.csc_NatureBusinessCd/>
      <com.csc_PremisesInd/>
      <com.csc_FireDistrict></com.csc_FireDistrict>
      <com.csc_FireDistrictCd></com.csc_FireDistrictCd>
      <com.csc_CoverageLossInfo>
        <Coverage>
          <CoverageCd/>
          <Limit>
            <LimitAmt/>
          </Limit>
          <CurrentTermAmt/>
        </Coverage>
        <Coverage>
          <CoverageCd/>
          <Limit>
            <LimitAmt/>
          </Limit>
          <CurrentTermAmt/>
        </Coverage>
        <Coverage>
          <CoverageCd/>
          <Limit>
            <LimitAmt/>
          </Limit>
          <CurrentTermAmt/>
        </Coverage>
        <Coverage>
          <CoverageCd/>
          <Limit>
            <LimitAmt/>
          </Limit>
          <CurrentTermAmt/>
        </Coverage>
        <Coverage>
          <CoverageCd/>
          <Limit>
            <LimitAmt/>
          </Limit>
          <CurrentTermAmt/>
        </Coverage>
        <Coverage>
          <CoverageCd/>
          <Limit>
            <LimitAmt/>
          </Limit>
          <CurrentTermAmt/>
        </Coverage>
      </com.csc_CoverageLossInfo>
    </PropertyLossInfo>
  </xsl:template>

  <xsl:template name="WCSiteRow">
    <WorkCompLocInfo id="">
      <SICCd/>
      <NAICSCd/>        
      <Location>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_SiteKey</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
            <OtherId>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="UNIT-NUMBER"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
            <OtherId>
              <xsl:call-template name="FormatIntParameterValue">
                <xsl:with-param name="value" select="STAT-UNIT-NO"/>
                <xsl:with-param name="size">5</xsl:with-param>
              </xsl:call-template>
            </OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
            <OtherId></OtherId>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_SiteNoRowInd</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
            <OtherId>
              <xsl:value-of select="INSURANCE-LINE"/>
            </OtherId>
          </OtherIdentifier>
        </ItemIdInfo>
        <Addr>
          <AddrTypeCd>MailingAddress</AddrTypeCd>
          <Addr1>
            <xsl:value-of select="PROPERTY-ADDRESS"/>
          </Addr1>
          <Addr2/>
          <City>
            <xsl:value-of select="SITE-CITY"/>
          </City>
          <StateProvCd>
            <xsl:value-of select="SITE-STATE"/>
          </StateProvCd>
          <PostalCode>
            <xsl:value-of select="SITE-ZIP"/>
          </PostalCode>
          <County></County>
        </Addr>
        <CountyTownCd></CountyTownCd>
        <RiskLocationCd></RiskLocationCd>
        <EarthquakeZoneCd></EarthquakeZoneCd>
        <TaxCodeInfo>
          <TaxCd/>
        </TaxCodeInfo>
        <SubLocation></SubLocation>
        <AdditionalInterest></AdditionalInterest>
        <FireDistrict></FireDistrict>
        <FireDistrictCd></FireDistrictCd>
        <FireStation></FireStation>
        <Communications>
          <EmailInfo/>
          <PhoneInfo></PhoneInfo>
          <WebsiteInfo></WebsiteInfo>
        </Communications>
        <LocationName>
          <xsl:value-of select="UNIT-NAME"/>
        </LocationName>
        <LocationDesc></LocationDesc>
        <CatastropheZoneCd></CatastropheZoneCd>
      </Location>
      <NumEmployees/>
      <HighestFloorNumberOccupied/>
      <WorkCompRateClass>
        <ActualRemunerationAmt/>
        <IfAnyRatingBasisInd/>
        <NumEmployees/>
        <NumEmployeesFullTime/>
        <NumEmployeesPartTime/>
        <Rate/>
        <PremiumBasisCd/>
        <RatingClassificationCd/>
        <Exposure/>
        <RatingClassificationDesc/>
        <RatingClassificationDescCd/>
        <ExposurePeriod/>
        <CommlCoverage/>
        <CreditOrSurcharge/>
        <CurrentTermAmt/>
        <WrittenAmt/>
      </WorkCompRateClass>     
    </WorkCompLocInfo>
  </xsl:template>

  <xsl:template name="InsuredUnitRow">
    <InsuredOrPrincipal>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_PropertyKey</OtherIdTypeCd>
          <OtherId/>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
          <OtherId>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value" select="UNIT-NUMBER"/>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
          <OtherId>
            <xsl:call-template name="FormatIntParameterValue">
              <xsl:with-param name="value" select="STAT-UNIT-NO"/>
              <xsl:with-param name="size">5</xsl:with-param>
            </xsl:call-template>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_CoverageUnitNumber</OtherIdTypeCd>
          <OtherId></OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_EntryDate</OtherIdTypeCd>
          <OtherId/>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="INSURANCE-LINE"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <GeneralPartyInfo>
        <NameInfo>
          <CommlName>
            <CommercialName>
              <xsl:value-of select="UNIT-NAME"/>
            </CommercialName>
          </CommlName>
          <PersonName>
            <TitlePrefix/>
            <NameSuffix/>
            <Surname/>
            <GivenName/>
            <OtherGivenName/>
            <NickName/>
          </PersonName>
          <LegalEntityCd></LegalEntityCd>
          <TaxIdentity/>
        </NameInfo>
        <Addr>
          <AddrTypeCd>MailingAddress</AddrTypeCd>
          <Addr1/>
          <City/>
          <StateProvCd/>
          <PostalCode/>
          <County/>
        </Addr>
      </GeneralPartyInfo>
      <PremiumInfo>
        <PremiumAmt>
          <xsl:value-of select="PREMIUM"/>
        </PremiumAmt>
      </PremiumInfo>
      <com.csc_InsuredNameIdInfo>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd>DESCUSE1</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
          <OtherIdentifier>
            <OtherIdTypeCd>DESCUSE2</OtherIdTypeCd>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
      </com.csc_InsuredNameIdInfo>
    </InsuredOrPrincipal>
  </xsl:template>
  
</xsl:stylesheet>