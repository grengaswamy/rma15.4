package com.csc.cfw.process;

/*****************************************************************************************************
 * $Id: Invoke.java,v 1.0 2003-07-04 dmcdaniel Exp $
 *
 * Copyright (c) 2003 Computer Sciences Corporation, Inc. All Rights Reserved.
 *
 * $State: Exp $
 *****************************************************************************************************/
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.activation.DataHandler;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.csc.cfw.communication.CommandManager;
import com.csc.cfw.process.data.PropertyAssignOpts;
import com.csc.cfw.process.data.PropertyValue;
import com.csc.cfw.process.data.PropertyValueByteArray;
import com.csc.cfw.process.data.PropertyValueFile;
import com.csc.cfw.process.data.PropertyValueInFile;
import com.csc.cfw.process.data.PropertyValueOutFile;
import com.csc.cfw.recordbuilder.CommFwRecordComposer;
import com.csc.cfw.recordbuilder.CommFwRecordDecomposer;
import com.csc.cfw.recordbuilder.RecordManager;
import com.csc.cfw.transport.Transport;
import com.csc.cfw.util.CommFwSystem;
import com.csc.cfw.xml.domain.IXmlRecordReader;
import com.csc.cfw.xml.domain.IXmlRecordRenderer;
import com.csc.cfw.xml.domain.JavaRecord;
import com.csc.fw.util.FwException;
import com.csc.fw.util.FwListener;
import com.csc.fw.util.FwSystem;
import com.csc.fw.util.Logger;
import com.csc.fw.util.TransformerSupport;
import com.csc.fw.util.XMLDocument;

/**
 * A BPEL "invoke" activity statement. This also processes the legacy BPMS
 * "action" activity statement. 
 *
 * @author Darrell McDaniel
 * 
 * Changes:
 * 
 *  2004-09-12  Changed the internal direction of inputs and outputs.
 *              BPEL4WS and BPML have the concept of input and output reversed.
 *              In BPML, output is what you are sending to the service and input
 *              is what you are receiving from the service (this is basically a
 *              caller or client perspective).  In BPEL, input is what you are
 *              sending to the service and output is what is returned from the
 *              service (this would be the server perspective).  The BPEL version
 *              is more of the normal view of inputs and outputs and the
 *              internal perspective has been changed to use the BPEL version.
 *               
 */
public class Invoke extends ComplexActivity
{
	private static com.csc.cfw.util.CommFwNLSMessages resCommFwResourceBundle = new com.csc.cfw.util.CommFwNLSMessages();
	public final static String COMMFW_INV_PORTTYPE = "CommFwInvoke"; //$NON-NLS-1$
	public final static String COMMFW_ACT_PORTTYPE = "CommFwAction"; //$NON-NLS-1$
	
	public final static String ACT_OP_REQ_ENV      = "commfw-req-env";     //$NON-NLS-1$
	public final static String ACT_OP_REQ_XFORM    = "commfw-req-xform";   //$NON-NLS-1$
	public final static String ACT_OP_REQ_CONV     = "commfw-req-conv";    //$NON-NLS-1$
	public final static String ACT_OP_COMMUNICATE  = "commfw-communicate"; //$NON-NLS-1$
	public final static String ACT_OP_RES_CONV     = "commfw-res-conv";    //$NON-NLS-1$
	public final static String ACT_OP_RES_XFORM    = "commfw-res-xform";   //$NON-NLS-1$
	public final static String ACT_OP_RES_COMMIT   = "commfw-commit";      //$NON-NLS-1$
	public final static String ACT_OP_XML_REC      = "commfw-xml-to-rec";  //$NON-NLS-1$
	public final static String ACT_OP_REC_XML      = "commfw-rec-to-xml";  //$NON-NLS-1$
	public final static String ACT_OP_REC_DATA     = "commfw-rec-to-data"; //$NON-NLS-1$
	public final static String ACT_OP_DATA_REC     = "commfw-data-to-rec"; //$NON-NLS-1$
	public final static String ACT_OP_TRANSPORT    = "commfw-transport";   //$NON-NLS-1$
	public final static String ACT_OP_STATUS       = "commfw-status";      //$NON-NLS-1$
	public final static String ACT_OP_STATUS_WARN  = "commfw-status-warning"; //$NON-NLS-1$
	public final static String ACT_OP_STATUS_ERROR = "commfw-status-error"; //$NON-NLS-1$
	public final static String ACT_OP_STATUS_LOG   = "commfw-status-log";  //$NON-NLS-1$
	public final static String ACT_OP_REQ_STORE    = "commfw-req-store";   //$NON-NLS-1$
	public final static String ACT_OP_RES_STORE    = "commfw-res-store";   //$NON-NLS-1$
	public final static String ACT_OP_REQ_DATA     = "commfw-req-data";    //$NON-NLS-1$
	
	// The elements in the #commfw_ops array are the predefined
	// operations that map to the old CommFw "Step" type.
	public final static String[] commfw_ops = {
				null,                  // Unassigned
				null,                  // ACT_REQ_ENV
				ACT_OP_REQ_XFORM,
				ACT_OP_REQ_CONV,
				ACT_OP_COMMUNICATE,
				ACT_OP_RES_CONV,
				ACT_OP_RES_XFORM,
				ACT_OP_RES_COMMIT,
				ACT_OP_XML_REC,
				ACT_OP_REC_XML,
				ACT_OP_REC_DATA,
				ACT_OP_DATA_REC,
				ACT_OP_TRANSPORT,
				ACT_OP_STATUS,
				ACT_OP_REQ_STORE,
				ACT_OP_RES_STORE,
				ACT_OP_REQ_DATA,
				ACT_OP_STATUS_WARN,
				ACT_OP_STATUS_ERROR,
				ACT_OP_STATUS_LOG,
	};

	class OutputParm
	{
		public String variable;
		public String element;
		public String xpath;
		public String part;
		public PropertyAssignOpts assignOpts;
	};
	
	class InputParm
	{
		public String type;
		public String element;
		public String expression;
		public String variable;
		public String xpath;
		public String value;
		public Element valueElement;
		public String part;
		public boolean transformParm;
		public PropertyAssignOpts assignOpts;
	};
	
	class Stylesheet
	{
		String name;
		String variable;
		String part;
		String expression;
		Element stylesheetElement;
	};
	
	boolean stepActivity;
	
	private String portType;
	private String port;
	private String operation;
	private String locate;
	private List outputParms;
	private List inputParms;
	private String validateOpt;
	private String parserId;
	private String schemaName;
	private String schemaNamespace;
	
	private String partnerLink;
	private String bpeOutputVariable;
	private String bpeInputVariable;
	private String bpexOutputAttachments;
	private String bpexInputAttachments;
	private String bpexOutputAttachmentNames;
	private String bpexInputAttachmentNames;
	private List correlations;
	private String transportName;
	private String transportMod;

	// FaultHandlers must be cloned separately.
	private List faultHandlers;
	private FaultHandler faultAllHandler;
	
	/*
	 * Variables to display input/output elements since they 
	 * are reversed in BPEL/BPML
	 */
	String inputElementDisp;
	String outputElementDisp;

	boolean transformAction = false;
	Stylesheet stysht;
	
	public int overrideIdx;
	public String overrideName;
	
	private PropertyAssignOpts defaultAssignOpts = null;

	/**
	 * Action constructor.
	 */
	public Invoke()
	{
		super();
	}

	/**
	 * Invoke constructor.
	 * @param aName Name of this step
	 */
	public Invoke(String aName)
	{
		super(aName);
	}

	/**
	 * Add a FaultHandler.
	 * @param faultHandler A FaultHandler instance.
	 */
	public void addFaultHandler(FaultHandler faultHandler)
	{
		if (faultHandlers == null)
			faultHandlers = new ArrayList();
		
		faultHandlers.add(faultHandler);
	}

	/**
	 * Build this activity from the current definition.
	 * @return Defined Activity.  Defaults to <b>this</b> instance.
	 * @param defNode Node definition for this Activity.
	 * @param parentActivity Parent Activity of this activity 
	 *        if not null.
	 */
	public Activity buildActivity(Node defNode, Activity parentActivity)
	{
		super.buildActivity(defNode, parentActivity);
		String nodeName = getActivityTypeName();

		if (ProcessFactory.BPMACT_CFW_STEP.equalsIgnoreCase(nodeName))  //$NON-NLS-1$
		{
			stepActivity = true;
			inputElementDisp = "input"; //$NON-NLS-1$
			outputElementDisp = "output"; //$NON-NLS-1$
			PropertyList properties = new PropertyList();
			storeAggregateProperties(defNode, properties);
			boolean propFound = false;
			Iterator keys = properties.keySet().iterator();
			while (keys.hasNext())
			{
				String s = (String)keys.next();
				if ("Type".equalsIgnoreCase(s))//$NON-NLS-1$
				{
					type = Integer.parseInt( (String)properties.get(s) );
					if (type > 0 && type < commfw_ops.length)
					{
						portType = COMMFW_INV_PORTTYPE;
						operation = commfw_ops[type];
					}
				}
				else
				if ("Description".equalsIgnoreCase(s))//$NON-NLS-1$
					description = (String)properties.get(s);
				else
					propFound = true;
			}
			
			if (propFound)
				setProperties(properties);
		}
		else
		if ("StepProperties".equalsIgnoreCase(nodeName))  //$NON-NLS-1$
		{
			// Get the activity override index.  We are not going to generate an
			// error if the index is not found or invalid but this activity override
			// will probably not work because the index is invalid and will not
			// be found at the proper activity.
			setBPEL(false);
			int activityIndex = 0;
			String s = XMLDocument.nodeAttributeValue(defNode, "index"); //$NON-NLS-1$
			try
			{
				if (s != null && s.trim().length() > 0)
					activityIndex = Integer.parseInt(s.trim());
			}
			catch (Exception e) { } // Ignore invalid index - but activity entry will be invalid
			overrideIdx = activityIndex;
			PropertyList properties = new PropertyList(2);
			storeAggregateProperties(defNode, properties);
			setProperties(properties);
		}
			
		if (ProcessFactory.BPMACT_INVOKE.equalsIgnoreCase(nodeName) || stepActivity)
		{
			// BPEL4WS
			setBPEL(true);
			// Outputs/Inputs are the defaults for BPEL
			inputElementDisp = "input"; //$NON-NLS-1$
			outputElementDisp = "output"; //$NON-NLS-1$
			defaultAssignOpts = PropertyAssignOpts.createAssignOptions(defNode);
			if (!stepActivity)
			{
				portType = XMLDocument.nodeAttributeValue(defNode, "portType");  //$NON-NLS-1$
				port = XMLDocument.nodeAttributeValue(defNode, "port");  //$NON-NLS-1$
				operation = XMLDocument.nodeAttributeValue(defNode, "operation");  //$NON-NLS-1$
				partnerLink = XMLDocument.nodeAttributeValue(defNode, "partnerLink");  //$NON-NLS-1$
			}
			bpeOutputVariable = XMLDocument.nodeAttributeValue(defNode, "outputVariable");  //$NON-NLS-1$
			bpeInputVariable = XMLDocument.nodeAttributeValue(defNode, "inputVariable");  //$NON-NLS-1$
			/*NS*/bpexOutputAttachments = XMLDocument.nodeAttributeValue(defNode, "outputAttachments");  //$NON-NLS-1$
			/*NS*/bpexInputAttachments = XMLDocument.nodeAttributeValue(defNode, "inputAttachments");  //$NON-NLS-1$
			/*NS*/bpexOutputAttachmentNames = XMLDocument.nodeAttributeValue(defNode, "outputAttachmentNames");  //$NON-NLS-1$
			/*NS*/bpexInputAttachmentNames = XMLDocument.nodeAttributeValue(defNode, "inputAttachmentNames");  //$NON-NLS-1$			
			/*NS*/locate = XMLDocument.nodeAttributeValue(defNode, "locate");  //$NON-NLS-1$
			/*NS*/transportName = XMLDocument.nodeAttributeValue(defNode, "transportName");  //$NON-NLS-1$
			/*NS*/transportMod = XMLDocument.nodeAttributeValue(defNode, "transportMod");  //$NON-NLS-1$
			for (Node layoutNode = XMLDocument.nodeFirstChildElement(defNode); 
			     null != layoutNode; 
			     layoutNode = XMLDocument.nodeNextElement(layoutNode))
			{
				buildActivityForInvoke(layoutNode);
				if (ProcessFactory.BPMACT_CORRELATIONS.equals(layoutNode.getNodeName()))
				{
					correlations = new ArrayList();
					for (Node corrNode = XMLDocument.nodeFirstChildElement(layoutNode); 
					     null != corrNode; 
					     corrNode = XMLDocument.nodeNextElement(corrNode))
					{
						Activity activity = ProcessFactory.buildActivity(corrNode, this);
						if (!(activity instanceof Correlation))
						{
							// Error message here
						}
						else
						{
							correlations.add(activity);
						}
					}
				}
				else
				if (!stepActivity)
				{
					Activity activity = ProcessFactory.buildActivity(layoutNode, this);
					if (activity != null)
						addActivity(activity);
				}
			}
		}
		else
		if (ProcessFactory.BPMACT_ACTION.equalsIgnoreCase(nodeName))
		{
			// BPML
			setBPEL(false);
			// Outputs/Inputs are reversed for BPML
			outputElementDisp = "input"; //$NON-NLS-1$
			inputElementDisp = "output"; //$NON-NLS-1$
			defaultAssignOpts = PropertyAssignOpts.createAssignOptions(defNode);
			portType = XMLDocument.nodeAttributeValue(defNode, "portType");  //$NON-NLS-1$
			port = XMLDocument.nodeAttributeValue(defNode, "port");  //$NON-NLS-1$
			operation = XMLDocument.nodeAttributeValue(defNode, "operation");  //$NON-NLS-1$
			locate = XMLDocument.nodeAttributeValue(defNode, "locate");  //$NON-NLS-1$
			/*NS*/bpexOutputAttachments = XMLDocument.nodeAttributeValue(defNode, "inputAttachments");  //$NON-NLS-1$
			/*NS*/bpexInputAttachments = XMLDocument.nodeAttributeValue(defNode, "outputAttachments");  //$NON-NLS-1$
			/*NS*/bpexOutputAttachmentNames = XMLDocument.nodeAttributeValue(defNode, "inputAttachmentNames");  //$NON-NLS-1$
			/*NS*/bpexInputAttachmentNames = XMLDocument.nodeAttributeValue(defNode, "outputAttachmentNames");  //$NON-NLS-1$			
			/*NS*/transportName = XMLDocument.nodeAttributeValue(defNode, "transportName");  //$NON-NLS-1$
			/*NS*/transportMod = XMLDocument.nodeAttributeValue(defNode, "transportMod");  //$NON-NLS-1$
			for (Node layoutNode = XMLDocument.nodeFirstChildElement(defNode); 
				 null != layoutNode; 
				 layoutNode = XMLDocument.nodeNextElement(layoutNode))
			{
				buildActivityForInvoke(layoutNode);
			}
		}
		else
		if (ProcessFactory.BPMACT_TRANSFORM.equalsIgnoreCase(nodeName))
		{
			// Local "transform" extended activity.
			setBPEL(true);
			validateOpt = XMLDocument.nodeAttributeValue(defNode, "validate");  //$NON-NLS-1$
			parserId = XMLDocument.nodeAttributeValue(defNode, "parser-id");  //$NON-NLS-1$
			schemaName = XMLDocument.nodeAttributeValue(defNode, "schemaName");  //$NON-NLS-1$
			schemaNamespace = XMLDocument.nodeAttributeValue(defNode, "schemaNamespace");  //$NON-NLS-1$
			// Outputs/Inputs are the defaults for BPEL
			inputElementDisp = "input"; //$NON-NLS-1$
			outputElementDisp = "output"; //$NON-NLS-1$
			defaultAssignOpts = PropertyAssignOpts.createAssignOptions(defNode);
			String ss = XMLDocument.nodeAttributeValue(defNode, "stylesheet"); //$NON-NLS-1$
			if (ss != null)
			{
				stysht = new Stylesheet();
				stysht.name = ss;
			}
			
			transformAction = true;
			type = CommFwAdapter.ACT_REQ_XFORM;
			for (Node layoutNode = XMLDocument.nodeFirstChildElement(defNode); 
				 null != layoutNode; 
				 layoutNode = XMLDocument.nodeNextElement(layoutNode))
			{
				buildActivityForInvoke(layoutNode);
			}
		}
		
		// Map action to a CommFw transform/convert action if this is
		// a CommFw portType.
		if (operation != null 
		    && (COMMFW_ACT_PORTTYPE.equals(portType)
		        || COMMFW_INV_PORTTYPE.equals(portType)))
		{
			int cfwType = -1;
			for (int i = 0; i < commfw_ops.length; i++)
				if (operation.equals(commfw_ops[i]))
				{
					cfwType = i;
					break;
				}

			if (cfwType > 0)
				type = cfwType;
		}
		
		return this;
	}

	/**
	 * Process Action/Invoke Input, Output and Stylesheet elements.
	 * @param layoutNode Node definition for this Activity element.
	 */
	private void buildActivityForInvoke(Node layoutNode)
	{
		/***********************************************************
		 * Output elements and Input elements are reversed in
		 * BPEL4WS and BPML. The native type used will be BPEL4WS
		 * which means that an "input" is a request parameter to
		 * be sent to a service and an "output" is a response
		 * parameter received from the service.
		 * NOTE: I initially had this reversed since I assumed
		 * the BPEL4WS usage was the same as BPML.  So I am now
		 * trying to correct the problem. 
		 ***********************************************************/
		String layoutNodeName = getActivityTypeName(layoutNode); 
		if ( (isBPEL() && "output".equalsIgnoreCase(layoutNodeName)) //$NON-NLS-1$
			|| (!isBPEL() && "input".equalsIgnoreCase(layoutNodeName)) ) //$NON-NLS-1$
		{
			outputElementDisp = layoutNodeName;
			OutputParm output = new OutputParm();
			if (isBPEL())
			{
				// Ignore outputs if an output message already defined.
				if (bpeOutputVariable != null)
					return;
				
				output.variable = XMLDocument.nodeAttributeValue(layoutNode, "variable");  //$NON-NLS-1$
				output.part = XMLDocument.nodeAttributeValue(layoutNode, "part");  //$NON-NLS-1$
				output.xpath = XMLDocument.nodeAttributeValue(layoutNode, "query");  //$NON-NLS-1$
			}
			else
			{
				output.variable = XMLDocument.nodeAttributeValue(layoutNode, "property");  //$NON-NLS-1$
				output.element = XMLDocument.nodeAttributeValue(layoutNode, "element");  //$NON-NLS-1$
				output.xpath = XMLDocument.nodeAttributeValue(layoutNode, "xpath");  //$NON-NLS-1$
			}
			
			output.assignOpts = PropertyAssignOpts.createAssignOptions(layoutNode);
			if (outputParms == null)
				outputParms = new ArrayList();
					
			outputParms.add(output);
		}
		else
		if ( (isBPEL() && "input".equalsIgnoreCase(layoutNodeName)) //$NON-NLS-1$
		     || (!isBPEL() && "output".equalsIgnoreCase(layoutNodeName)) ) //$NON-NLS-1$
		{
			inputElementDisp = layoutNodeName;
			InputParm input = new InputParm();
			if (isBPEL())
			{
				// Ignore inputs if an input message already defined.
				if (bpeInputVariable != null)
					return;
				
				input.variable = XMLDocument.nodeAttributeValue(layoutNode, "variable");  //$NON-NLS-1$
				input.part = XMLDocument.nodeAttributeValue(layoutNode, "part");  //$NON-NLS-1$
				input.xpath = XMLDocument.nodeAttributeValue(layoutNode, "query");  //$NON-NLS-1$
				input.expression = XMLDocument.nodeAttributeValue(layoutNode, "expression");  //$NON-NLS-1$
				Element child = XMLDocument.nodeFirstChildElement(layoutNode);
				if (child != null)
				{
					input.valueElement = child;
					if (input.type == null)
						input.type = PropertyValue.TYPE_ELEMENT;
				}
				else
				{
					String val = XMLDocument.nodeElementValue(layoutNode);
					if (val != null)
					{
						input.value = val;
						if (input.type == null)
							input.type = PropertyValue.TYPE_STRING;
					}
				}
				input.assignOpts = PropertyAssignOpts.createAssignOptions(layoutNode);
			}
			else
			{
				input.type = XMLDocument.nodeAttributeValue(layoutNode, "type");  //$NON-NLS-1$
				input.element = XMLDocument.nodeAttributeValue(layoutNode, "element");  //$NON-NLS-1$
				input.expression = XMLDocument.nodeAttributeValue(layoutNode, "xpath");  //$NON-NLS-1$
				Node child = XMLDocument.nodeFirstChildElement(layoutNode);
				if (child != null)
					if ("source".equals(child.getNodeName())) //$NON-NLS-1$
					{
						input.variable = XMLDocument.nodeAttributeValue(child, "property");  //$NON-NLS-1$
						input.xpath = XMLDocument.nodeAttributeValue(child, "xpath");  //$NON-NLS-1$
						if (input.variable == null)
							input.variable = XMLDocument.nodeElementValue(child);
					}
					else
					if ("value".equalsIgnoreCase(child.getNodeName())) //$NON-NLS-1$
					{
						// If a child element exists, use that as an element in the assignment.
						Element valueRootElement = XMLDocument.nodeFirstChildElement(child);  
						if (valueRootElement != null)
						{
							try
							{
								Document doc = XMLDocument.createDOM((String)null, (String)null, true);  
								XMLDocument.appendElement(doc, (Element)doc.importNode(valueRootElement, true));
								input.valueElement = doc.getDocumentElement();  
							}
							catch (FwException e)
							{
								throw new IllegalArgumentException(e.toString());
							}
						}
						else
						if ((input.value = XMLDocument.nodeElementValue(child)) != null)
						{
							input.value = input.value.trim();
						}
						else
							input.value = ""; //$NON-NLS-1$
					}
			}
								
			if (inputParms == null)
				inputParms = new ArrayList();
					
			inputParms.add(input);
		}
		else
		if ("stylesheet".equalsIgnoreCase(layoutNodeName)) //$NON-NLS-1$
		{
			stysht = new Stylesheet();
			if ((stysht.name = XMLDocument.nodeAttributeValue(
										layoutNode, "name")) == null  //$NON-NLS-1$
				&& ((stysht.variable = XMLDocument.nodeAttributeValue(
										layoutNode, "variable")) == null)  //$NON-NLS-1$
				&& ((stysht.expression = XMLDocument.nodeAttributeValue(
										layoutNode, "expression")) == null))  //$NON-NLS-1$
			{
				stysht.stylesheetElement = XMLDocument.nodeFirstChildElement(layoutNode);
			}
		}
		else
		if (ProcessFactory.BPMACT_CATCH.equalsIgnoreCase(layoutNodeName))
		{
			FaultHandler handler = (FaultHandler)ProcessFactory.buildActivity(layoutNode, this);
			addFaultHandler(handler);
		}
		else
		if (ProcessFactory.BPMACT_CATCHALL.equalsIgnoreCase(layoutNodeName))
		{
			FaultHandler handler = (FaultHandler)ProcessFactory.buildActivity(layoutNode, this);
			faultAllHandler = handler;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Object clone()
	{
		// First make a generic clone of this object.
		Invoke copy = (Invoke)super.clone();
		if (faultHandlers  != null)
		{
			List newFaultHandlers = new ArrayList();
			copy.faultHandlers = newFaultHandlers;
			for (Iterator it = faultHandlers.iterator(); it.hasNext(); )
			{
				Object obj = it.next();
				newFaultHandlers.add(((Activity)obj).clone());
			}
		}
		
		if (faultAllHandler  != null)
		{
			copy.faultAllHandler = (FaultHandler)faultAllHandler.clone();
		}
		return copy;
	}

	/**
	 * Send the records formatted into a message buffer to the CommandManager for processing.
	 *
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param directTransport If <code>true</code> bypass the CommunicationManager
	 *         and call the transport layer directly.
	 */
	public void communicate(Envelope envelope, CommFwAdapter adapter, boolean directTransport)
		throws FwException
	{
		// Statistics: Set begin event time
		int ptime = envelope.beginTimerEvent(Envelope.PTIME_REQ_COMM);
		
		try
		{
			Object[] commInputParams = processRequestParms(envelope, adapter);
			Object[] commOutputParams = null;
			
			// Build the return data array.
			int returnCnt = 0;
			if (isBPEL() && this.bpeOutputVariable != null)
			{
				Object outProp = adapter.getPropertyVariable(this.bpeOutputVariable);
				if (outProp instanceof PropertyValue)
				{
					PropertyValue out = (PropertyValue)outProp;
					if (out.getDType() == PropertyValue.DTYPE_MESSAGE)
					{
						Message msg = adapter.getMessageType(out.getType(), out.getTypeNs());
						returnCnt = msg.partCount();
					}
				}
	  				
				commOutputParams = new Object[returnCnt];
			}
			else  				
			if (outputParms == null || outputParms.size() == 0)
				commOutputParams = new Object[1];
			else
			{
				returnCnt = outputParms.size();
				commOutputParams = new Object[returnCnt];
				
				// Any response properties that are File property types
				// need to be directly written to. So we need to store
				// these types in the output array.
				for (int i = 0; i < returnCnt; i++)
				{
					OutputParm outParm = (OutputParm)outputParms.get(i);
					if (outParm.variable != null)
					{
						Object obj = adapter.getPropertyVariable(outParm.variable);
						if (obj != null)
							if (obj instanceof PropertyValueFile)
							{
								PropertyValueFile fileProp = (PropertyValueFile)obj;
								commOutputParams[i] = fileProp;
							}
					}
				}
			}
			
			Transport transport = adapter.getTransportHandler();
			if (transportName != null)
			{
				// If the transport is overridden, we only
				// want to do it the first time so only
				// create a new transport if it doesn't
				// match.
				if (!transportName.equals(transport.getName()))
				{
					adapter.transportHandler(envelope, adapter, transportName);
					transport = adapter.getTransportHandler();
				}
			}
			
			transport.setInputAttachmentList(null);
			transport.setOutputAttachmentList(null);
			transport.setInputAttachmentNames(null);
			transport.setOutputAttachmentNames(null);			
			
			// Handle the input (request) attachment list if specified.
			if (this.bpexInputAttachments != null)
			{
				Object inProp = adapter.getPropertyVariable(this.bpexInputAttachments);
				if (inProp instanceof PropertyValue)
				{
					PropertyValue in = (PropertyValue)inProp;
					if (in.getDType() == PropertyValue.DTYPE_ATTACHMENTS)
					{
						transport.setInputAttachmentList((List)in.getValue());
					}
				}
				
				if (transport.getInputAttachmentList() == null)
				{
					ProcessException pe = new ProcessException(
							resCommFwResourceBundle.getString("cfw_err_invoke_attvar_inv"), //$NON-NLS-1$
							new String[] { this.bpexInputAttachments } );
					pe.setFaultName(
							ProcessException.BPWS_NS_URI, 
							ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
					throw pe;
				}
				
				inProp = adapter.getPropertyVariable(this.bpexInputAttachmentNames);
				if (inProp instanceof PropertyValue)
				{
					PropertyValue in = (PropertyValue)inProp;
					if (in.getDType() == PropertyValue.DTYPE_ELEMENT)
					{
						transport.setInputAttachmentNames((ArrayList)in.convertElementToList());
					}
				}								  
			}
			
			// If we are overriding the commSfx, set the
			// value and reset it after the transport call.
			if (transportMod != null)
			{
				envelope.setCommSfx(transportMod);
			}
			
			if (envelope.isTestLogRun() && envelope.getTestLogPath() != null)
				// Restore the data from the saved response store.
				testLogDataReturn(envelope, adapter, commOutputParams);
			else
			if (directTransport)
			{
				// Call the TransportHandler directly.
				transport.setInputRecords(commInputParams);
				transport.setOutputRecords(commOutputParams);
				adapter.setPropertyInCurrentScope(Transport.TRANSPORT_port, port);
				adapter.setPropertyInCurrentScope(Transport.TRANSPORT_portType, portType);
				adapter.setPropertyInCurrentScope(Transport.TRANSPORT_operation, operation);
				if (locate != null)
				{
					String s = null;
					
					// The transport property lookup looks for system properties, transport
					// properties and defined variables.
					s = (String)transport.getTransportProperty(locate, envelope, adapter);
					// If no transport property was found, use the locate as a URL.
					if (s == null)
						s = locate;
					
					adapter.setPropertyInCurrentScope(Transport.TRANSPORT_locate, s);
					adapter.setPropertyInCurrentScope(Transport.TRANSPORT_WSWsdl, s);
				}
				
				transport.processCommunication(envelope, adapter);
				// Reset commInputs in case they have been reallocated.
				commOutputParams = transport.getOutputRecords();
			}
			else
			{
				// Call the CommunicationManager via the CommandManager.
				new CommandManager().initiateCommunicationManager(envelope, adapter, commInputParams, commOutputParams);
			}
			
			if (envelope.isTestLogSave())
				testLogDataSave(envelope, adapter, commOutputParams);
			
			// If we are overriding the commSfx, set the
			// value and reset it after the transport call.
			if (transportMod != null)
			{
				envelope.setCommSfx(null);
			}
			
			processResponseParms(commOutputParams, returnCnt, envelope, adapter);
			
			// Handle a response attachment list.
			if (this.bpexOutputAttachments != null)
			{
				PropertyValue dst = null;
				Object prop = null;
				if ((prop = adapter.getPropertyVariable(this.bpexOutputAttachments)) != null
					&& prop instanceof PropertyValue )
				{
					dst = (PropertyValue)prop;
				}
				
				List attachmentList = transport.getOutputAttachmentList();
				if (dst != null && attachmentList != null)
				{			
					if (envelope.getProcessTrace() > 1)
						envelope.getProcessTraceListener().println(
								"<"  //$NON-NLS-1$ 
								+ outputElementDisp 
								+ "AttachmentValue property=\"" //$NON-NLS-1$ 
								+ dst.getName() 
								+ "\">" //$NON-NLS-1$ 
								+ attachmentList.toString() 
								+ "</" //$NON-NLS-1$
								+ outputElementDisp
								+ "AttachmentValue>");//$NON-NLS-1$
				
					if (dst.getDType() == PropertyValue.DTYPE_ATTACHMENTS)
					{
						dst.assignValue(
						       adapter, 
						       PropertyValue.DTYPE_ATTACHMENTS, 
						       attachmentList, 
						       0, 
						       (String)null, 
						       (String)null,
							   (PropertyAssignOpts)null);
					}
				}
				
				if (this.bpexOutputAttachmentNames != null)
				{
					dst = null;
					prop = null;
					if ((prop = adapter.getPropertyVariable(this.bpexOutputAttachmentNames)) != null
						&& prop instanceof PropertyValue )
					{
						dst = (PropertyValue)prop;
					}
					
					attachmentList = transport.getOutputAttachmentNames();
					if (dst != null && attachmentList != null)
					{			
						if (envelope.getProcessTrace() > 1)
							envelope.getProcessTraceListener().println(
									"<"   //$NON-NLS-1$
									+ outputElementDisp 
									+ "AttachmentNamesValue property=\"" //$NON-NLS-1$ 
									+ dst.getName() 
									+ "\">" //$NON-NLS-1$ 
									+ attachmentList.toString() 
									+ "</" //$NON-NLS-1$
									+ outputElementDisp
									+ "AttachmentNamesValue>"); //$NON-NLS-1$
						
						if (dst.getDType() == PropertyValue.DTYPE_ELEMENT)
						{
							dst.assignValueDetermineType(
								adapter, 
								attachmentList, 
								0, 
								(String)null, 
								(String)null,
								(PropertyAssignOpts)null);					 		
						}
					}
				}	  			  							
			} // end attachment processing
		}
		catch (ProcessException e)
		{
			if (e.getType() == 0)
				e.setType(FwException.ERRORTYPE_COMMUNICATION);
			throw e;
		}
		catch (FwException e)
		{
			if (e.getType() == 0)
				e.setType(FwException.ERRORTYPE_COMMUNICATION);
			ProcessException pe = new ProcessException(e);
			pe.setFaultName(
					ProcessException.CFW_NS_URI, 
					ProcessException.FAULT_CFW_COMMUNICATION_FAILURE);
			throw pe;
		}
		catch (Exception exception)
		{
			ProcessException pe = new ProcessException(
					resCommFwResourceBundle.getString("cfw_excmsg_cmdmgr_exc"), 
					exception); //$NON-NLS-1$
			pe.setFaultName(
					ProcessException.CFW_NS_URI, 
					ProcessException.FAULT_CFW_COMMUNICATION_FAILURE);
			throw pe;
		}
		finally
		{
			// Statistics: Set end event time
			envelope.endTimerEvent(ptime);
		}
		
		return;
	}

	/**
	 * Format EIS formatted XML to Java Records (Decompose).
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param callConvRecToXml If <code>true</code> then convert the
	 *        records to EIS formatted XML.
	 */
	public void convertHostDataToRecords(Envelope envelope, CommFwAdapter adapter, boolean callConvRecToXml)
		throws FwException
	{
		// Statistics: Set begin event time
		int ptime = envelope.beginTimerEvent(Envelope.PTIME_RES_CONV);
		
		try
		{
			// Define a stream if this is using the stream decomposer.
			InputStream stream = null;
			PropertyValueInFile iFileProp = null;
			
			// The default is to use the data stored in CommFwAdapter.PROC_DATA
			// for the decompose process.
			// See if we need to copy an alternate output property to
			// CommFwAdapter.PROC_DATA.
			if (inputParms != null && inputParms.size() > 0)
			{
				boolean valueAssigned = false;
				InputParm in = (InputParm)inputParms.get(0);
				if (in.variable != null)
				{
					PropertyValue prop = (PropertyValue)adapter.getPropertyVariable(in.variable);
					if (prop != null)
					{
						if (prop.getDType() == PropertyValue.DTYPE_IN_FILE)
						{
							iFileProp = (PropertyValueInFile)prop;
							stream = iFileProp.getStream();
						}
						else
							prop.storeValueToRecData(adapter);
						
						valueAssigned = true;
					}
				}
				else
				if (in.value != null)
				{
					try
					{
						byte[] bytes = in.value.getBytes(Envelope.INTERNAL_ENCODING);
						adapter.setRecData(bytes, bytes.length);
						valueAssigned = true;
					}
					catch (UnsupportedEncodingException e)
					{
						throw new FwException(e);
					}
				}

				if (!valueAssigned)
				{
					ProcessException pe = new ProcessException(
							resCommFwResourceBundle.getString("cfw_err_undef-illegal_inpparm"), //$NON-NLS-1$
							new String[] { inputElementDisp, operation });
					pe.setFaultName(
							ProcessException.BPWS_NS_URI, 
							ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
					throw pe;
				}
			}

			// The decomposer converts EIS data
			// to Java Records.
			CommFwRecordDecomposer recDecomp = adapter.getRecMan().newRecordDecomposerInstance();
			List records = null;
			
			if (stream == null)
			{
				records = recDecomp.decompose(envelope, adapter);
			}
			else
			{
				try
				{
					records = recDecomp.decomposeStream(stream, envelope, adapter);
				}
				catch (IOException e)
				{
				  	// Put code in here to handle end of file.
					ProcessException pe = new ProcessException(
							resCommFwResourceBundle.getString("cfw_err_invoke_var_ioerror"), //$NON-NLS-1$
							new String[] { iFileProp.getName() },
							e);
					pe.setFaultName(
							ProcessException.CFW_NS_URI, 
							ProcessException.FAULT_CFW_IO_ERROR);
					throw pe;
				}
			}

			boolean valueAssigned = false;
			if (callConvRecToXml || outputParms == null || outputParms.size() == 0)
			{
				// No defined input (decomposed records to store).
				// Use the default recordset identifier for storage.
				// If this calls convertRecordsToXML, then the input
				// applies to the XML destination, not the record
				// destination.
				PropertyValue prop = (PropertyValue)adapter.getPropertyVariable(CommFwAdapter.PROC_RECORDSET);
				if (prop == null)
				{
					prop = PropertyValue.createPropertyValue(
					                         envelope,
					                         adapter,
					                         CommFwAdapter.PROC_RECORDSET, 
					                         PropertyValue.DTYPE_RECORDSET, 
					                         PropertyValue.TYPE_RECORDSET,
					                         records);
					adapter.setPropertyInCurrentScope(CommFwAdapter.PROC_RECORDSET, prop);
					valueAssigned = true;
				}
				else
				{
					prop.assignValue(
					        adapter, 
					        PropertyValue.DTYPE_RECORDSET, 
					        records, 
					        0, 
					        (String)null, 
					        (String)null,
							(PropertyAssignOpts)null);
					valueAssigned = true;
				}
			}
			else
			{
				Object dst = null;
				OutputParm out = (OutputParm)outputParms.get(0);
				if (out.variable != null
					&& (dst = adapter.getPropertyVariable(out.variable)) != null)
				{
					if ((dst instanceof PropertyValue) 
						&& ((PropertyValue)dst).getDType() == PropertyValue.DTYPE_RECORDSET)
					{
						((PropertyValue)dst).assignValue(
						                       adapter, 
						                       PropertyValue.DTYPE_RECORDSET, 
						                       records, 
						                       0, 
						                       (String)null, 
						                       (String)null,
											   out.assignOpts);
						valueAssigned = true;
					}
				}
			}

			if (!valueAssigned)
			{
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString("cfw_err_undef-illegal_outparm"), //$NON-NLS-1$
						new String[] { outputElementDisp, operation });
				pe.setFaultName(
						ProcessException.BPWS_NS_URI, 
						ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
				throw pe;
			}

		}
		catch (ProcessException exception)
		{
			exception.setType(FwException.ERRORTYPE_CONVERSION);
			throw exception;
		}
		catch (FwException exception)
		{
			ProcessException pe = new ProcessException(
					exception, 
					FwException.ERRORTYPE_CONVERSION);
			pe.setFaultName(
					ProcessException.CFW_NS_URI, 
					ProcessException.FAULT_CFW_RECORD_CONV_FAILURE);
			throw pe;
		}
		finally
		{
			// Statistics: Set end event time
			envelope.endTimerEvent(ptime);
		}


		if (callConvRecToXml)
		{
			convertRecordsToXml(envelope, adapter, true);
		}
		
		return;
	}

	/**
	 * Convert Java Records to a raw data format for the EIS target system (Compose).
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param callConvXmlToRec If <code>true</code> then convert the
	 *        EIS formatted XML to records before calling the composer.
	 */
	public void convertRecordsToHostData(
			Envelope envelope, 
			CommFwAdapter adapter, 
			boolean callConvXmlToRec)
		throws FwException
	{
		CommFwRecordComposer recComp;
		List records = null;
		
		if (envelope.isBypassWithSAX())
		{
			// We've already done everything for record conversion - just leave
			envelope.setBypassWithSAX(false);
			return;
		}
		
		if (callConvXmlToRec)
		{
			records = convertXmlToRecords(envelope, adapter);
		}
		
		// Statistics: Set begin event time
		int ptime = envelope.beginTimerEvent(Envelope.PTIME_REQ_CONV);
		
		try
		{
			if (!callConvXmlToRec)
			{
				if (inputParms == null || inputParms.size() == 0)
				{
					// No defined output (records to convert).
					// Use the default recordset identifier.
					records = (List)adapter.getProperty(CommFwAdapter.PROC_RECORDSET);
				}
				else
				{
					Object src = null;
					InputParm inp = (InputParm)inputParms.get(0);
					if (inp.variable != null
						&& (src = adapter.getPropertyVariable(inp.variable)) != null)
					{
						if ((src instanceof PropertyValue) 
							&& ((PropertyValue)src).getDType() == PropertyValue.DTYPE_RECORDSET)
							records = (List)((PropertyValue)src).getValue();
					}
				}
			}
			
			// The record composer converts Java records
			// to raw data for the EIS layer.
			recComp = adapter.getRecMan().newRecordComposerInstance();
			recComp.composeDataArea(records, envelope, adapter);
			
			// The composed data is stored in CommFwAdapter.PROC_DATA.
			// See if we need to copy it to an input field.
			if (outputParms != null && outputParms.size() > 0)
			{
				Object prop = null;
				OutputParm out = (OutputParm)outputParms.get(0);
				if (out.variable != null
					&& (prop = adapter.getPropertyVariable(out.variable)) != null
					&& prop instanceof PropertyValue )
				{
					PropertyValue dst = (PropertyValue)prop;
					dst.assignValue(
					          adapter, 
					          PropertyValue.DTYPE_BYTEARRAY, 
					          adapter.getRecData(), 
					          adapter.getRecDataLength(), 
					          null, 
					          null,
							  out.assignOpts);
				}
			}
			
		}
		catch (ProcessException exception)
		{
			exception.setType(FwException.ERRORTYPE_CONVERSION);
			throw exception;
		}
		catch (FwException exception)
		{
			ProcessException pe = new ProcessException(
					exception, 
					FwException.ERRORTYPE_CONVERSION);
			pe.setFaultName(
					ProcessException.CFW_NS_URI, 
					ProcessException.FAULT_CFW_RECORD_CONV_FAILURE);
			throw pe;
		}
		finally
		{
			// Statistics: Set end event time
			envelope.endTimerEvent(ptime);
		}

		return;
	}

	/**
	 * Convert the Java Records list to an XML stream.
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param fromConversion If this is <code>true</code>, then the output
	 *        that is specified does not apply to this operation. 
	 */
	public void convertRecordsToXml(
			Envelope envelope, 
			CommFwAdapter adapter,
			boolean fromConversion)
		throws FwException
	{
		// Statistics: Set begin event time
		int ptime = envelope.beginTimerEvent(Envelope.PTIME_REC_TOXML);
		
		try
		{
			// Render the Records into XML.  The root node will be the transaction
			// name converted to a response entity.
			List records = null;
			if (fromConversion || inputParms == null || inputParms.size() == 0)
			{
				// No defined input (records to convert).
				// Use the default recordset identifier.
				// Also default will be used if this was
				// called from conversion because the output
				// applied to the record conversion process,
				// not this operation.
				records = (List)adapter.getProperty(CommFwAdapter.PROC_RECORDSET);
			}
			else
			{
				Object src = null;
				InputParm inp = (InputParm)inputParms.get(0);
				if (inp.variable != null
					&& (src = adapter.getPropertyVariable(inp.variable)) != null)
				{
					if ((src instanceof PropertyValue) 
						&& ((PropertyValue)src).getDType() == PropertyValue.DTYPE_RECORDSET)
						records = (List)((PropertyValue)src).getValue();
				}
			}

			if (records == null)
			{
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString("cfw_err_undef-illegal_inpparm"), //$NON-NLS-1$
						new String[] { inputElementDisp, operation });
				pe.setFaultName(
						ProcessException.BPWS_NS_URI, 
						ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
				throw pe;
			}
			
			String aTransactionName = adapter.getTranName();
			String respSfx = (String)adapter.getProperty(CommFwAdapter.ADAPTER_MSG_ResponseSfx);
			aTransactionName = Envelope.responseTransactionTag(aTransactionName, respSfx);
			RecordManager recMan = adapter.getRecMan();
			String encoding = null;
			if (outputParms != null && outputParms.size() > 0)
			{
				OutputParm out = (OutputParm)outputParms.get(0);
				if (out.assignOpts != null && out.assignOpts.getEncoding() != null)
				{
					encoding = out.assignOpts.getEncoding();
					encoding = adapter.getDynamicStringConstant(encoding, envelope, adapter);
				}
			}
			IXmlRecordRenderer aRenderer = recMan.getRecordXMLRenderer(aTransactionName, encoding, envelope);
			byte[] b = aRenderer.processComplete(records);

			boolean valueAssigned = false;
			if (outputParms == null || outputParms.size() == 0)
			{
				// Store the rendered XML output data in recData buffer
				adapter.setRecData(b, b.length);
				valueAssigned = true;
			}
			else
			{
				Object prop = null;
				OutputParm out = (OutputParm)outputParms.get(0);
				if (out.variable != null
					&& (prop = adapter.getPropertyVariable(out.variable)) != null
					&& prop instanceof PropertyValue )
				{
					PropertyValue dst = (PropertyValue)prop;
					dst.assignValue(
					          adapter, 
					          PropertyValue.DTYPE_BYTEARRAY, 
					          b, 
					          b.length, 
					          (String)null, 
					          (String)null,
							  out.assignOpts);
					valueAssigned = true;
				}
			}

			if (!valueAssigned)
			{
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString("cfw_err_undef-illegal_outparm"), //$NON-NLS-1$
						new String[] { outputElementDisp, operation });
				pe.setFaultName(
						ProcessException.BPWS_NS_URI, 
						ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
				throw pe;
			}

			// CFw Trace
			int traceLevel = 3;
			if (envelope.trace >= traceLevel)
			{
				envelope.getListener().println(traceLevel, resCommFwResourceBundle.getString("cfw_tracemsg_intrsp_xml_begin")); //$NON-NLS-1$
				envelope.getListener().println(traceLevel, new String(b, Envelope.INTERNAL_ENCODING));
				envelope.getListener().println(traceLevel, resCommFwResourceBundle.getString("cfw_tracemsg_intrsp_xml_end")); //$NON-NLS-1$
			}

		}
		catch (ProcessException exception)
		{
			exception.setType(FwException.ERRORTYPE_CONVERSION);
			throw exception;
		}
		catch (FwException exception)
		{
			ProcessException pe = new ProcessException(
					exception, 
					FwException.ERRORTYPE_CONVERSION);
			pe.setFaultName(
					ProcessException.CFW_NS_URI, 
					ProcessException.FAULT_CFW_RECORD_CONV_FAILURE);
			throw pe;
		}
		catch (Exception e)
		{
			// Handle the error here
			ProcessException pe = new ProcessException(
					resCommFwResourceBundle.getString("cfw_excmsg_jrec_render"), //$NON-NLS-1$ 
					e,
					FwException.ERRORTYPE_CONVERSION);
			pe.setFaultName(
					ProcessException.CFW_NS_URI, 
					ProcessException.FAULT_CFW_RECORD_CONV_FAILURE);
			throw pe;
		}
		finally
		{
			// Statistics: Set end event time
			envelope.endTimerEvent(ptime);
		}

		return;
	}

	/**
	 * Convert EIS formatted XML to Java Records.
	 * @return A list of Java Records is returned. The input property is also set
	 *         or the property CommFwAdapter.PROC_RECORDSET if no input has been
	 *         defined..
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 */
	public List convertXmlToRecords(Envelope envelope, CommFwAdapter adapter)
		throws FwException
	{
		List records = null;

		// Statistics: Set begin event time
		int ptime = envelope.beginTimerEvent(Envelope.PTIME_XML_TOREC);
		
		try
		{
			InputSource xmlMsgSource = null;
			
			if (inputParms == null || inputParms.size() == 0)
			{
				// No defined output (XML to convert).
				// Use the default record data.
				xmlMsgSource = new InputSource(new ByteArrayInputStream(adapter.getRecData()));
			}
			else
			{
				InputParm inp = (InputParm)inputParms.get(0);
				if (inp.variable != null)
					xmlMsgSource = new InputSource(adapter.getPropertyAsStream(inp.variable, inp.part));
			}

			if (xmlMsgSource == null)
			{
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString("cfw_err_undef-illegal_inpparm"), //$NON-NLS-1$
						new String[] { inputElementDisp, operation });
				pe.setFaultName(
						ProcessException.BPWS_NS_URI, 
						ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
				throw pe;
			}
			
			// Call XML Record Reader. the XML Message will be passed to the reader,
			// a vector of Record Wrappers will be returned;
			RecordManager recMan = adapter.getRecMan();
			IXmlRecordReader xmlRecordReader = recMan.getRecordXMLReader(envelope);
			records = xmlRecordReader.process(xmlMsgSource);
			
			boolean valueAssigned = false;
			if (outputParms == null || outputParms.size() == 0)
			{
				// No defined output (records to convert).
				// Use the default recordset identifier.
				PropertyValue prop = (PropertyValue)adapter.getPropertyVariable(CommFwAdapter.PROC_RECORDSET);
				if (prop == null)
				{
					prop = PropertyValue.createPropertyValue(
					                         envelope,
					                         adapter,
					                         CommFwAdapter.PROC_RECORDSET, 
					                         PropertyValue.DTYPE_RECORDSET, 
					                         PropertyValue.TYPE_RECORDSET, 
					                         records);
					adapter.setPropertyInCurrentScope(CommFwAdapter.PROC_RECORDSET, prop);
					valueAssigned = true;
				}
				else
				{
					prop.assignValue(
					           adapter, 
					           PropertyValue.DTYPE_RECORDSET, 
					           records, 
					           0, 
					           (String)null, 
					           (String)null,
							   (PropertyAssignOpts)null);
					valueAssigned = true;
				}
			}
			else
			{
				Object dst = null;
				OutputParm out = (OutputParm)outputParms.get(0);
				if (out.variable != null
					&& (dst = adapter.getPropertyVariable(out.variable)) != null)
				{
					if ((dst instanceof PropertyValue) 
						&& ((PropertyValue)dst).getDType() == PropertyValue.DTYPE_RECORDSET)
					{
						((PropertyValue)dst).assignValue(
						                        adapter, 
						                        PropertyValue.DTYPE_RECORDSET, 
						                        records, 
						                        0, 
						                        (String)null, 
						                        (String)null,
												out.assignOpts);
						valueAssigned = true;
					}
				}
			}

			if (!valueAssigned)
			{
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString("cfw_err_undef-illegal_outparm"), //$NON-NLS-1$
						new String[] { outputElementDisp, operation });
				pe.setFaultName(
						ProcessException.BPWS_NS_URI, 
						ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
				throw pe;
			}
			
			envelope.addSizeEvent(Envelope.PTIME_COMP_REC_CNT, records.size());
		}
		catch (ProcessException exception)
		{
			exception.setType(FwException.ERRORTYPE_CONVERSION);
			throw exception;
		}
		catch (FwException exception)
		{
			ProcessException pe = new ProcessException(
					exception, 
					FwException.ERRORTYPE_CONVERSION);
			pe.setFaultName(
					ProcessException.CFW_NS_URI, 
					ProcessException.FAULT_CFW_RECORD_CONV_FAILURE);
			throw pe;
		}
		catch (SAXException e)
		{
			// Handle the error here
			ProcessException pe = new ProcessException(
					resCommFwResourceBundle.getString("cfw_excmsg_cmpsr_recerr"), //$NON-NLS-1$ 
					new String[] { e.getMessage() }, 
					e,
					FwException.ERRORTYPE_CONVERSION);
			pe.setFaultName(
					ProcessException.CFW_NS_URI, 
					ProcessException.FAULT_CFW_RECORD_CONV_FAILURE);
			throw pe;
		}
		finally
		{
			// Statistics: Set end event time
			envelope.endTimerEvent(ptime);
		}

		return records;
	}

	/**
	 * Return the current operation if defined.
	 * @return Operation
	 */
	public String getOperation()
	{
		return operation;
	}

	/**
	 * Return the override index.
	 * @return The index of the step to override.
	 */
	public int getOverrideIdx()
	{
		return overrideIdx;
	}

	/**
	 * Execute the Action activity.
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 */
	public void process(Envelope envelope, CommFwAdapter adapter)
		throws FwException
	{
		System.out.println("Inside ---> process");
		System.out.println("Inside ---> process, Transaction Name is" + envelope.getTranName());
		int procTrace = envelope.getProcessTrace();
		if (procTrace > 1)
			envelope.getProcessTraceListener().print(FwSystem.currentDateTime() + ':');
		if (procTrace > 0)
			envelope.getProcessTraceListener().println(this.toString());

		try
		{
			switch (type)
			{
				case CommFwAdapter.ACT_REQ_ENV:
					break;
				
				case CommFwAdapter.ACT_REQ_XFORM:
				case CommFwAdapter.ACT_REQ_DATA:
					// Adapt (Transform) message from Request XML to Request RecordFormat XML
					transformXmlMessage(envelope, adapter, false, 
							type == CommFwAdapter.ACT_REQ_DATA);
					break;
				
				case CommFwAdapter.ACT_REQ_CONV:
					// Call the composer to build the buffer to send to the command manager
					convertRecordsToHostData(envelope, adapter, true);
					break;
				
				case CommFwAdapter.ACT_COMMUNICATE:
					// Call the command manager
					communicate(envelope, adapter, false);
					break;
				
				case CommFwAdapter.ACT_RES_CONV:
					// Convert returned host data buffer to Record Format XML
					convertHostDataToRecords(envelope, adapter, true);
					break;
				
				case CommFwAdapter.ACT_RES_XFORM:
					// Adapt (Transform) message from Response RecordFormat XML to Response XML
					transformXmlMessage(envelope, adapter, true, false);
	
					if (envelope.isValidateErrorResponse() && envelope.getValidateErrorException() != null)
					{
						boolean validateNonFatal = envelope.getFwSystem().getBooleanProperty( FwSystem.SYSPROP_validationErrorsNonFatal, envelope.getParameters() );
						Broker.logValidationException(envelope.getValidateErrorException(), false, envelope, adapter, true);
						if (!validateNonFatal)
						{
							ProcessException pe = new ProcessException(
									envelope.getValidateErrorException());
							pe.setFaultName(
									ProcessException.CFW_NS_URI, 
									ProcessException.FAULT_CFW_TRANSFORMATION_FAILURE);
							throw pe;
						}
							
					}
					break;
				
				case CommFwAdapter.ACT_RES_COMMIT:
					// Commit the UOW if required.
					if (adapter.getTransportHandler() != null)
						adapter.getTransportHandler().commitRollbackTransaction(adapter.isTransactionError(), envelope, adapter, true);
					break;
				
				case CommFwAdapter.ACT_REC_DATA:
					// Call the composer to build the data buffer.
					convertRecordsToHostData(envelope, adapter, false);
					break;
				
				case CommFwAdapter.ACT_DATA_REC:
					// Convert returned host data buffer to Record Format XML
					convertHostDataToRecords(envelope, adapter, false);
					break;
				
				case CommFwAdapter.ACT_REC_XML:
					// Call the XML Renderer to render records into XML.
					convertRecordsToXml(envelope, adapter, false);
					break;
				
				case CommFwAdapter.ACT_XML_REC:
					// Call the XMLRecordReader to build Java Records from EIS XML.
					convertXmlToRecords(envelope, adapter);
					break;
				
				case CommFwAdapter.ACT_TRANSPORT:
					// Call the command manager
					communicate(envelope, adapter, true);
					break;
				
				case CommFwAdapter.ACT_STATUS:
				case CommFwAdapter.ACT_STATUS_WARN:
				case CommFwAdapter.ACT_STATUS_ERROR:
				case CommFwAdapter.ACT_STATUS_LOG:
					// Call the status event and log feature
					systemStatus(envelope, adapter, type);
					break;
				
				default:
					// Default will be a direct WS transport call
					communicate(envelope, adapter, true);
					break;
				
			}
		}
		catch (ProcessException e)
		{
			boolean isHandled = adapter.handleExceptionFault(
					envelope,
					adapter,
					e,
					faultHandlers,
					faultAllHandler);
			if (!isHandled)
				throw e;
		}
	}

	/**
	 * Create an array of request data parameters to send to the service.
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @return An array of data parameters
	 */
	private Object[] processRequestParms(Envelope envelope, CommFwAdapter adapter)
		throws FwException
	{
		
		System.out.println("Inside ---> processRequestParms");
		Object[] inpParms = null;

		if ( isBPEL() && (this.bpeInputVariable != null) )
		{
			Object inpProp = null;
			if (this.bpeInputVariable != null) 
				inpProp = adapter.getPropertyVariable(this.bpeInputVariable);
			
			if (inpProp != null && inpProp instanceof PropertyValue)
			{
				PropertyValue in = (PropertyValue)inpProp;
				if (in.getDType() == PropertyValue.DTYPE_MESSAGE)
				{
					Message msg = adapter.getMessageType(in.getType(), in.getTypeNs());
					int count = msg.partCount();
					inpParms = new Object[count];
					List partNames = msg.partNames();
					for (int i = 0; i < count; i++ )
					{
						String partName = (String)partNames.get(i);
						inpParms[i] = in.getPart(adapter, partName).getValue();
					}
				}
			}
			
			if (inpParms == null)
			{
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString("cfw_err_invoke_invar_inv"), //$NON-NLS-1$
						new String[] { this.bpeInputVariable } );
				pe.setFaultName(
						ProcessException.BPWS_NS_URI, 
						ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
				throw pe;
			}
		}
		else
		{
			// The default is to use the data stored in CommFwAdapter.PROC_DATA
			// See if we need to copy an alternate output property to
			// CommFwAdapter.PROC_DATA.
			if (inputParms == null || inputParms.size() == 0)
			{
				inpParms = new Object[] { adapter.getRecData() };
					
				if (envelope.getProcessTrace() > 1)
				{
					String inputTagName = inputElementDisp + "Value>"; //$NON-NLS-1$ 
					envelope.getProcessTraceListener().println(
							"<" + inputTagName  //$NON-NLS-1$
							+ PropertyValueByteArray.toPropertyDump(adapter.getRecData()) 
							+ "</" + inputTagName);//$NON-NLS-1$
				}
			}
			else
			{
				int count = inputParms.size();
				inpParms = new Object[count];
				for (int i = 0; i < count; i++)
				{
					boolean valueAssigned = false;
					InputParm in = (InputParm)inputParms.get(i);
					if (in.variable != null)
					{
						Object property = adapter.getPropertyVariable(in.variable);
						if (property != null)
						{
							if (property instanceof PropertyValue)
							{
								PropertyValue dataProperty = (PropertyValue)property;
								if (in.type != null)
								{
									PropertyValue inProperty = 
									       PropertyValue.createPropertyValue(
									             envelope, 
									             adapter, 
									             null, 
									             in.type);
									inProperty.assignValue(
									             adapter, 
									             dataProperty, 
									             null,
									             in.xpath,
												 in.assignOpts);
									
									inpParms[i] = inProperty.getValue();
									valueAssigned = true;
								}
								else
								{
									// If using a part, make that the destination.
									// Will return an error if not a Message or part not found. 
									if (in.part != null)
										dataProperty = dataProperty.getPart(adapter, in.part);
		
									// No type defined just pass the properties value.
									inpParms[i] = dataProperty.getValue();
									valueAssigned = true;
								}
							}
							else
							{
								inpParms[i] = property;
								valueAssigned = true;
							}
						}
					}
					else
					if (in.expression != null)
					{
						inpParms[i] = adapter.getLocalProperties().executeXPath(in.expression).toString();
						valueAssigned = true;
					}
					else
					if (in.valueElement != null)
					{
						if (in.type != null)
							inpParms[i] = PropertyValue.allocateDataValue(
												adapter, 
												in.type, 
												in.valueElement);
						else
							inpParms[i] = PropertyValue.allocateDataValue(
												adapter, 
												PropertyValue.TYPE_ELEMENT, 
												in.valueElement);
					}
					else
					{
						inpParms[i] = PropertyValue.allocateDataValue(adapter, in.type, in.value);
						valueAssigned = true;
					}
	
					if (!valueAssigned)
					{
						ProcessException pe = new ProcessException(
								resCommFwResourceBundle.getString("cfw_err_undef-illegal_inpparm"), //$NON-NLS-1$
								new String[] { inputElementDisp, operation });
						pe.setFaultName(
								ProcessException.BPWS_NS_URI, 
								ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
						throw pe;
				}
					else		
					if (envelope.getProcessTrace() > 1)
					{
						String inputTagName = inputElementDisp + "Value>"; //$NON-NLS-1$
						Object val = inpParms[i];
						if (val instanceof byte[])
							val = PropertyValueByteArray.toPropertyDump((byte[])val);
						
						envelope.getProcessTraceListener().println(
								"<" + inputTagName  //$NON-NLS-1$
								+ val 
								+ "</" + inputTagName);//$NON-NLS-1$
					}
				}
			}
		}

		return 	inpParms;
	}

	/**
	 * Handle response parameters from a request that will be assigned to 
	 * output property variables or message parts.
	 * @param commOutputs Returned values from a communications request.
	 * @param returnCnt Number of parameters to process for the return.
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 */
	private void processResponseParms(Object[] commOutputs, int returnCnt, Envelope envelope, CommFwAdapter adapter)
		throws FwException
	{
		
		System.out.println("Inside ---> processResponseParms");
		
		String propertyName = null;
		Message msg = null;
		List partNames = null;
		int partCnt = 0;
		PropertyValue outMsg = null;
		boolean processOutputs = true;
		
		if (isBPEL() && this.bpeOutputVariable != null)
		{
			processOutputs = true;
			propertyName = this.bpeOutputVariable;
			Object prop = adapter.getPropertyVariable(propertyName);
			if (prop instanceof PropertyValue)
			{
				outMsg = (PropertyValue)prop;
				if (outMsg.getDType() != PropertyValue.DTYPE_MESSAGE)
				{
					ProcessException pe = new ProcessException(
							resCommFwResourceBundle.getString("cfw_err_invoke_outvar_inv"), //$NON-NLS-1$
							new String[] { propertyName } );
					pe.setFaultName(
							ProcessException.BPWS_NS_URI, 
							ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
					throw pe;
				}
				
				msg = adapter.getMessageType(outMsg.getType(), outMsg.getTypeNs());
				partCnt = msg.partCount();
				partNames = msg.partNames();
			}
		}
		else
		if (this.outputParms == null || this.outputParms.size() == 0)
		{
			// The communications output data is stored in CommFwAdapter.PROC_DATA.
			// See if we need to copy it to an input field.
			processOutputs = false;
			Object retVal = null;
			if ((retVal = commOutputs[0]) != null)
			{
				if (envelope.getProcessTrace() > 1)
				{
					String outputTagName = outputElementDisp + "Value>"; //$NON-NLS-1$ 
					envelope.getProcessTraceListener().println(
							"<" + outputTagName  //$NON-NLS-1$
							+ retVal.toString() 
							+ "</" + outputTagName); //$NON-NLS-1$
				}
				
				if (retVal instanceof PropertyValue)
				{
					PropertyValue src = (PropertyValue)retVal;
					PropertyValue outProperty = 
					          PropertyValue.createPropertyValue(
					                    envelope, 
					                    adapter, 
					                    null, 
					                    PropertyValue.TYPE_BYTEARRAY);
					outProperty.assignValue(
						adapter, 
						src, 
						(String)null,
						(String)null,
						defaultAssignOpts);
					adapter.setRecData( (byte[])outProperty.getValue(), outProperty.getValueSize() );
				}
				else
				{
					PropertyValue outProperty = null;
					if(retVal instanceof DataHandler)
					{
						outProperty = 
							  PropertyValue.createPropertyValue(
										envelope, 
										adapter, 
										null, 
										PropertyValue.TYPE_ATTACHMENTPART);
						outProperty.assignValueDetermineType(
										adapter, 
										retVal, 
										0, 
										(String)null, 
										(String)null,
										defaultAssignOpts);
										
						adapter.setRecDataObject(outProperty.getValue());		
						envelope.setResContentType(Envelope.octetContentType);														
					}
					else
					{
						outProperty = 
					          PropertyValue.createPropertyValue(
					          		envelope,
									adapter,
									null,
									PropertyValue.TYPE_BYTEARRAY);
						outProperty.assignValueDetermineType(
									adapter,
									retVal,
									0,
									(String)null,
									(String)null,
									defaultAssignOpts);
						adapter.setRecData( (byte[])outProperty.getValue(), outProperty.getValueSize() );
					}					                    
				}
			}
		}
		
		if (processOutputs)
		{
			for (int i = 0; i < returnCnt; i++)
			{
				PropertyValue dst = null;
				String xpath = null;
				PropertyAssignOpts assignOpts = defaultAssignOpts;
				if (outMsg != null)
				{
					// BPEL4WS.  Get Message part.
					if (i < partCnt)
					{
						dst = outMsg.getPart(adapter, (String)partNames.get(i));
					}
				}
				else
				if (this.outputParms != null)
				{
					if (i < this.outputParms.size())
					{
						Object prop = null;
						OutputParm out = (OutputParm)outputParms.get(i);
						propertyName = out.variable;
						if (out.assignOpts != null)
							assignOpts = out.assignOpts;
						
						if (propertyName != null
							&& (prop = adapter.getPropertyVariable(propertyName)) != null
							&& prop instanceof PropertyValue )
						{
							dst = (PropertyValue)prop;
							// If using a part, make that the destination.
							// Will return an error if not a Message or part not found. 
							if (out.part != null)
								dst = dst.getPart(adapter, out.part);
		
							xpath = out.xpath;
						}
					}
				}
				
				if (dst != null)
				{					
					if (envelope.getProcessTrace() > 1 && commOutputs[i] != null)
					{
						String outputTagName = outputElementDisp + "Value"; //$NON-NLS-1$ 
						envelope.getProcessTraceListener().println(
								"<" + outputTagName  //$NON-NLS-1$
								+ " variable=\"" + dst.getName() //$NON-NLS-1$ 
								+ "\">" + commOutputs[i].toString() //$NON-NLS-1$ 
								+ "</" + outputTagName + '>');//$NON-NLS-1$
					}
				
					Object retVal = null;
					if ((retVal = commOutputs[i]) != null)
					{
						if (retVal instanceof PropertyValue)
						{
							PropertyValue src = (PropertyValue)retVal;
							dst.assignValue(
							          adapter, 
							          src, 
							          xpath, 
							          (String)null,
									  assignOpts);
						}
						else
						{
							dst.assignValueDetermineType(
							          adapter, 
							          retVal, 
							          0, 
							          xpath, 
							          (String)null,
									  assignOpts);
						}
					}
				}
				else
				{
					ProcessException pe = new ProcessException(
							resCommFwResourceBundle.getString("cfw_err_undef-illegal_output_prop"), //$NON-NLS-1$
							new String[] { outputElementDisp, operation, propertyName });
					pe.setFaultName(
							ProcessException.BPWS_NS_URI, 
							ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
					throw pe;
				}
			}
		}
	}

	/**
	 * An action to write to the system status.
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param actType Status activity type (AC_OP_STATUS*).
	 */
	public void systemStatus(Envelope envelope, CommFwAdapter adapter, int actType)
		throws FwException
	{
		Object inpParms[] = processRequestParms(envelope, adapter);

		String inpParm1 = null;
		String inpParm2 = null;
		String inpParm3 = null;
		for (int i = 0; i < inpParms.length; i++)
		{
			Object parm = inpParms[i];
			PropertyValue prop = PropertyValue.createPropertyValue(
					envelope, 
					adapter, 
					"", //$NON-NLS-1$ 
					PropertyValue.TYPE_STRING);
			prop.assignValueDetermineType(adapter, parm, 0, null, null, null);
			String parmStg = prop.toString();
			if (parmStg != null)
				if (i == 0)
					inpParm1 = parmStg;
				else
					if (i == 1)
						inpParm2 = parmStg;
					else
						if (i == 2)
							inpParm3 = parmStg;
		}
		
		String id = CommFwSystem.currentTimestamp() + '.' + inpParm1;
		switch (type)
		{
			case CommFwAdapter.ACT_STATUS:
				envelope.getFwSystem().getSystemStatus().logInfoEvent(id, inpParm2, null);
				break;
			case CommFwAdapter.ACT_STATUS_WARN:
				envelope.getFwSystem().getSystemStatus().logWarningEvent(id, inpParm2, null);
				break;
			case CommFwAdapter.ACT_STATUS_ERROR:
				envelope.getFwSystem().getSystemStatus().logErrorEvent(id, inpParm2, null);
				break;
			case CommFwAdapter.ACT_STATUS_LOG:
				// #inpParm1 is the log path. #inpParm2 is the fileName. #inpParm3 is the text to write.
				File file = new File(inpParm1);
				if (!file.exists())
					file.mkdirs();
				Logger logger = new Logger(CommFwSystem.current());
				logger.initLog(
						ACT_OP_STATUS_LOG,
						"1", //$NON-NLS-1$ 
						inpParm1);
				logger.writeLog(inpParm2, inpParm3, 0);
				break;
		}
	}

	/**
	 * Return the test log data for this transaction step. The response to return is based on the
	 * sequence at in Envelope.getTestLogReturnSeq(). 
	 * @param envelope Envelope Holds global information for this transaction..
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param outputs Array of output parameters.
	 */
	public void testLogDataReturn(Envelope envelope, CommFwAdapter adapter, Object[] outputs)
	       throws FwException
	{
		String filename = envelope.getTestLogPath();

		if (!filename.endsWith(File.separator) && !filename.endsWith("\\")) //$NON-NLS-1$
			filename += File.separator;
		
		String seqSfx = ""; //$NON-NLS-1$
		int seqNbr = envelope.getTestLogReturnSeq();
		if (seqNbr > 0)
			seqSfx += '[' + new Integer(seqNbr).toString() + ']';
		envelope.setTestLogReturnSeq(++seqNbr);
		filename += envelope.getTestLogName() + "-"     //$NON-NLS-1$
		            + envelope.getTestLogId() + seqSfx + ".dat"; //$NON-NLS-1$

		try
		{
			File file = new File(filename);
			int filesize = (int)file.length();
			if (filesize == 0 || !file.exists())
				throw new FwException(resCommFwResourceBundle.getString("Playback_datafile_notfound"), new String[] {filename}); //$NON-NLS-1$

			byte[] data = new byte[filesize];
			DataInputStream in = new DataInputStream(new FileInputStream(file));
			in.readFully(data);
			outputs[0] = data;
		}
		catch (IOException exception)
		{
			throw new FwException(resCommFwResourceBundle.getString("Playback_datafile_IO_Err"), new String[] {filename}); //$NON-NLS-1$
		}
	}

	/**
	 * Save the test log data for this transaction step. The data files to save is based on the
	 * sequence at Envelope.getTestLogReturnSeq(). 
	 * @param envelope Envelope Holds global information for this transaction..
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param outputs Array of output parameters.
	 */
	public void testLogDataSave(Envelope envelope, CommFwAdapter adapter, Object[] outputs)
	       throws FwException
	{
		Object retVal = null;
		if ((retVal = outputs[0]) != null)
		{
			if (retVal instanceof byte[])
			{
				byte[] bytes = (byte[])retVal;
				envelope.setTestLogReturnDataLength(bytes.length);
				envelope.setTestLogReturnData(bytes);
			}
			else
			if (retVal instanceof PropertyValue)
			{
				PropertyValue prop = (PropertyValue)retVal;
				switch (prop.getDType())
				{
					case PropertyValue.DTYPE_BYTEARRAY:
						envelope.setTestLogReturnDataLength( prop.getValueSize() );
						envelope.setTestLogReturnData( (byte[])prop.getValue() );
						break;
					
					case PropertyValue.DTYPE_STRING:
						byte[] bytes = null;
						try
						{
							bytes = ((String)prop.getValue()).getBytes(Envelope.INTERNAL_ENCODING);
						}
						catch (Exception e)
						{
							throw new FwException(e);
						}
						
						envelope.setTestLogReturnDataLength(bytes.length);
						envelope.setTestLogReturnData(bytes);
						break;
					
				}
			}
			else
			if (retVal instanceof String || retVal instanceof Node)
			{
				byte[] bytes = null;
				String encoding = Envelope.INTERNAL_ENCODING;
				try
				{
					if (retVal instanceof Node)
						retVal = XMLDocument.convertDOMToText((Node)retVal);
					bytes = ((String)retVal).getBytes(encoding);
				}
				catch (Exception e)
				{
					throw new FwException(e);
				}
				
				envelope.setTestLogReturnDataLength(bytes.length);
				envelope.setTestLogReturnData(bytes);
			}
			else
			if (retVal instanceof JavaRecord)
			{
				byte[] bytes = ((JavaRecord)retVal).getRawBytes();
				envelope.setTestLogReturnDataLength(bytes.length);
				envelope.setTestLogReturnData(bytes);
			}
		}
	}

	/**
	 * Transform an XML message from one message format to another.
	 *
	 * @param envelope Envelope contains global request information.
	 * @param adapter CommFwAdapter contains process specific information.
	 * @param isResponse If <code>false</code> this is a request message.  If
	 *		the value is <code>true</code> this is a response message.
	 * @param isTransformToHostData If <code>true</code> then this transformation
	 *        will output directly to SAX using the processSAX() method of 
	 *        XMLRecordReader then send the records to the compose step.
	 */
	public void transformXmlMessage(
			Envelope envelope, 
			CommFwAdapter adapter, 
			boolean isResponse,
			boolean isTransformToHostData)
		throws FwException
	{
		// Statistics: Set begin event time
		int ptime = envelope.beginTimerEvent( 
				(isResponse ? Envelope.PTIME_RES_XFORM : Envelope.PTIME_REQ_XFORM) );
		
		try
		{
			boolean sourceIsRecData = false;
			boolean outputToSAX = false;
			RecordManager recMan = adapter.getRecMan();
			IXmlRecordReader xmlRecordReader = recMan.getRecordXMLReader(envelope);
			FwListener listener = new FwListener();
			Source xformSource = envelope.getSource();
			Result outResult = null;
			ByteArrayOutputStream baos = null;
			PropertyValue outProperty = null;
			PropertyAssignOpts assignOpts = defaultAssignOpts;
			byte outBytes[] = null;
			int outBytesSize = 0;
			boolean useSAXReq = false;
			

			/************************************************
			 * Check if validation requested 
			 ***********************************************/
			boolean validateOutput = false;
			if (validateOpt != null && validateOpt.length() > 0)
			{
				validateOutput = ("yes".equalsIgnoreCase(validateOpt)
						|| "true".equalsIgnoreCase(validateOpt));
			}
			
			/************************************************
			** Allocate input for the Transformation
			*************************************************/
			if (inputParms == null || inputParms.size() == 0)
			{
				// No defined input (XML to transform).
				// Use the default record data.
				if (xformSource == null)
				{
					sourceIsRecData = true;
					//xformSource = new StreamSource(new InputStreamReader(new ByteArrayInputStream(adapter.getRecData()), Envelope.INTERNAL_ENCODING));
					xformSource = new StreamSource(new ByteArrayInputStream(adapter.getRecData()));
				}
			}
			else
			{
				InputParm in = (InputParm)inputParms.get(0);
				if (in.variable != null)
					xformSource = adapter.getPropertyAsSource(in.variable, in.part);
					System.out.println("THE VALUE IS ------> " + adapter.getPropertyAsString(in.variable, in.part));
			}
			
			if (xformSource == null)
			{
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString("cfw_err_undef-illegal_inpparm"), //$NON-NLS-1$
						new String[] { inputElementDisp, operation });
				pe.setFaultName(
						ProcessException.BPWS_NS_URI, 
						ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
				throw pe;
			}
			
			/************************************************
			** Allocate output for the Transformation
			*************************************************/	
			if (outputParms == null || outputParms.size() == 0)
			{
				// No defined output (XML transform result).
				
				// See if we need to perform special SAX bypass operations
				if (envelope.getTrace() < 3 && envelope.getStopAtStep() == 0)
				{
					boolean saxTransformDisabled = false;
					try
					{
						saxTransformDisabled = adapter.getPropertyAsBoolean(Adapter.ADAPTER_MSG_TransformToSAXOptimizerDisabled, null);
					}
					catch (FwException e) { /* Ignore errors */ }
					
					if (!saxTransformDisabled
					    && getType() == CommFwAdapter.ACT_REQ_XFORM 
					    && getParentActivity() != null)
					{
						Activity nextActivity = ((ComplexActivity)getParentActivity()).peekNextActivity();
						if (nextActivity != null 
						    && nextActivity instanceof Invoke
						    && nextActivity.getType() == CommFwAdapter.ACT_REQ_CONV)
						{
							Invoke nextInvoke = (Invoke)nextActivity;
							if ((nextInvoke.inputParms == null 
							     || nextInvoke.inputParms.size() == 0) 
							    && (nextInvoke.outputParms == null
							        || nextInvoke.outputParms.size() == 0))
							        useSAXReq = true;
						}
					}
				}
				
				if ((!validateOutput && isTransformToHostData) || useSAXReq)
				{
					outResult = new SAXResult(xmlRecordReader.processSAX());
					outputToSAX = true;
				}
				else
				{
					// Use the default record data.
					baos = new ByteArrayOutputStream();
					//OutputStreamWriter writer = new OutputStreamWriter(baos, Envelope.INTERNAL_ENCODING);
					outResult = new StreamResult(baos);
				}
			}
			else
			{
				OutputParm out = (OutputParm)outputParms.get(0);
				if (out.variable != null)
				{
					if (out.assignOpts != null)
						assignOpts = out.assignOpts;
					
					outProperty = (PropertyValue)adapter.getPropertyVariable(out.variable);
					if (outProperty != null)
					{
						int type = outProperty.getDType();
						if (out.part != null && type == PropertyValue.DTYPE_MESSAGE)
						{
							outProperty = outProperty.getPart(adapter, out.part);
							type = outProperty.getDType();
						}
						
						switch (type)
						{
							case PropertyValue.DTYPE_ELEMENT:
								outResult = new DOMResult();
								break;
							
							case PropertyValue.DTYPE_RECORDSET:
								if (xmlRecordReader.isSAXCapable())
								{
									outResult = new SAXResult(xmlRecordReader.processSAX());
									outputToSAX = true;
								}
								break;
							
							case PropertyValue.DTYPE_OUT_FILE:
							case PropertyValue.DTYPE_STRING:
							case PropertyValue.DTYPE_BYTEARRAY:
								//String encoding = Envelope.INTERNAL_ENCODING;
								//if (assignOpts != null)
								//	encoding = assignOpts.getEncoding();
								
								if (isTransformToHostData)
								{
									outResult = new SAXResult(xmlRecordReader.processSAX());
									outputToSAX = true;
								}
								else
								{
									baos = new ByteArrayOutputStream();
									//OutputStreamWriter writer = new OutputStreamWriter(baos, encoding);
									outResult = new StreamResult(baos);
								}
								break;

						}
					}
				}
			}

			if (outResult == null)
			{
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString("cfw_err_undef-illegal_outparm"), //$NON-NLS-1$
						new String[] { outputElementDisp, operation });
				pe.setFaultName(
						ProcessException.BPWS_NS_URI, 
						ProcessException.FAULT_BPWS_MISMATCHED_ASSIGN);
				throw pe;
			}
			
			
			/************************************************
			** Allocate a transformer for the Transformation
			*************************************************/
			String ssName = null;
			if (stysht != null)
			{
				if ((ssName = stysht.name) == null)
					if (stysht.variable != null)
					{
						Object var = adapter.getProperty(stysht.variable);
						if (var != null)
							ssName = var.toString();
					}
					else
					if (stysht.expression != null)
					{
						ssName = adapter.getLocalProperties().executeXPath(
											stysht.expression).toString();
					}
			}
			
			Transformer xslprocessor = adapter.getTransformer(envelope, isResponse, ssName);
			TransformerSupport xformSupport = TransformerSupport.getInstance(xslprocessor);
			// Start transform trace if specified.
			boolean traceStarted = false;
			if (envelope.getXslTrace() != 0)
			{
				traceStarted = xformSupport.startTransformTrace(envelope, xslprocessor);
				if (traceStarted)
					envelope.getListener().printlnFmt(resCommFwResourceBundle.getString("cfw_tracemsg_xform_begin"), new String[] { adapter.getTemplateName() });//$NON-NLS-1$
			}
			
			/************************************************
			** Now we're ready to do the Transformation
			*************************************************/	
			try
			{
				adapter.setTransformParms(envelope, xslprocessor, xformSupport, null);
				xslprocessor.setErrorListener(listener);
				xslprocessor.transform(xformSource, outResult);
				// CFw Trace
				int traceLevel = (isResponse ? 2 : 3);
				if ((envelope.trace >= traceLevel) && (baos != null))
				{
					envelope.getListener().println(traceLevel, resCommFwResourceBundle.getString(
						(isResponse ? "cfw_tracemsg_rsp_xml_hdr" : "cfw_tracemsg_reqint_xml_hdr") ));//$NON-NLS-1$//$NON-NLS-2$
					String logMsg = baos.toString(Envelope.INTERNAL_ENCODING);
					if (envelope.getFwSystem().getSecurityManager().isSecureLoggingEnabled())
					{
						logMsg = envelope.getFwSystem().getSecurityManager().maskSecureXMLFields(logMsg);
					}
					envelope.getListener().println(traceLevel, logMsg);
					envelope.getListener().println(traceLevel, resCommFwResourceBundle.getString(
						(isResponse ? "cfw_tracemsg_rsp_xml_trl" : "cfw_tracemsg_reqint_xml_trl") ));//$NON-NLS-1$//$NON-NLS-2$
				}
				
				if (outputToSAX)
				{
					List records = xmlRecordReader.getRecords();
					envelope.addSizeEvent(Envelope.PTIME_COMP_REC_CNT, records.size());
					PropertyValue prop = PropertyValue.createPropertyValue(
	                         envelope,
	                         adapter,
	                         CommFwAdapter.PROC_RECORDSET, 
	                         PropertyValue.DTYPE_RECORDSET, 
	                         PropertyValue.TYPE_RECORDSET, 
	                         records);
					adapter.setPropertyInCurrentScope(CommFwAdapter.PROC_RECORDSET, prop);
				}
			}
			catch (TransformerException exception)
			{
				// Stop transform trace.
				if (envelope.getXslTrace() != 0 && xformSupport != null)
				{
					xformSupport.stopTransformTrace(envelope, xslprocessor);
					if (traceStarted)
						envelope.getListener().printlnFmt(resCommFwResourceBundle.getString("cfw_tracemsg_xform_end"), new String[] {adapter.getTemplateName() }); //$NON-NLS-1$
				}

				String reqrsp = (isResponse ? "cfw_excmsg_xform_response" : "cfw_excmsg_xform_request");//$NON-NLS-1$//$NON-NLS-2$
				String errmsg = "cfw_excmsg_xform_err_msg"; //$NON-NLS-1$
				String[] errmsgArgs; 
				if (sourceIsRecData)
				{
					String msgStg;
					try
					{
						msgStg = new String(adapter.getRecData(), 
								envelope.getRqEncoding() != null && envelope.getRqEncoding().length() > 0  
								? envelope.getRqEncoding() 
								: Envelope.INTERNAL_ENCODING);
					}
					catch (Exception e)
					{
						msgStg = new String(adapter.getRecData()); 
					}
					if (envelope.getFwSystem().getSecurityManager().isSecureLoggingEnabled())
					{
						msgStg = "[Message display suppressed by secure logging]";
					}

					errmsg = "cfw_excmsg_xform_xml_msg_data";//$NON-NLS-1$
					errmsgArgs = new String[] { resCommFwResourceBundle.getString(reqrsp), listener.toString(), msgStg };
				}
				else
					errmsgArgs = new String[] { resCommFwResourceBundle.getString(reqrsp), listener.toString() };
				
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString(errmsg),
						errmsgArgs, exception,
						FwException.ERRORTYPE_TRANSFORM);
				pe.setFaultName(
						ProcessException.CFW_NS_URI, 
						ProcessException.FAULT_CFW_TRANSFORMATION_FAILURE);
				throw pe;
			}
			catch (Exception exception)
			{
				// Stop transform trace.
				if (envelope.getXslTrace() != 0)
				{
					xformSupport.stopTransformTrace(envelope, xslprocessor);
					if (traceStarted)
						envelope.getListener().printlnFmt(resCommFwResourceBundle.getString("cfw_tracemsg_xform_end"), new String[] {adapter.getTemplateName() }); //$NON-NLS-1$
				}
				
				String reqrsp = (isResponse ? "cfw_excmsg_xform_response" : "cfw_excmsg_xform_request");//$NON-NLS-1$//$NON-NLS-2$
				ProcessException pe = new ProcessException(
						resCommFwResourceBundle.getString("cfw_excmsg_xform_err_msg"), //$NON-NLS-1$
						new String[] { resCommFwResourceBundle.getString(reqrsp), listener.toString() }, 
						exception,
						FwException.ERRORTYPE_TRANSFORM);
				pe.setFaultName(
						ProcessException.CFW_NS_URI, 
						ProcessException.FAULT_CFW_TRANSFORMATION_FAILURE);
				throw pe;
			}
			
			// Stop transform trace.
			if (envelope.getXslTrace() != 0)
			{
				xformSupport.stopTransformTrace(envelope, xslprocessor);
				if (traceStarted)
					envelope.getListener().printlnFmt(resCommFwResourceBundle.getString("cfw_tracemsg_xform_end"), new String[] {adapter.getTemplateName() }); //$NON-NLS-1$
			}
			
			// Validate Response XML if flag set.
			if (isResponse && envelope.isValidateRs() && baos != null)
				adapter.validateXML(envelope, baos.toByteArray(), null, true, false, null, null);
			else
				if (validateOutput && baos != null)
				{
					adapter.validateXML(envelope, baos.toByteArray(), parserId, false, true, schemaNamespace, schemaName);
				}

			// If we are going directly from request XML to Host Data
			// we are doing a compose step since we should already
			// have our output record set.
			if (isTransformToHostData || useSAXReq)
			{
				// The record composer converts Java records
				// to raw data for the EIS layer.
				CommFwRecordComposer recComp = recMan.newRecordComposerInstance();
				recComp.composeDataArea(xmlRecordReader.getRecords(), envelope, adapter);
				envelope.setBypassWithSAX(true);
			}
			
			/************************************************
			** Store the Transformation result
			*************************************************/	
			if (outProperty != null)
			{
				switch (outProperty.getDType())
				{
					case PropertyValue.DTYPE_ELEMENT:
						outProperty.assignValue(
						               adapter, 
						               PropertyValue.DTYPE_ELEMENT, 
						               ((DOMResult)outResult).getNode(), 
						               0, 
						               (String)null, 
						               (String)null,
									   assignOpts);
						if (validateOutput && baos == null)
						{
							try
							{
								String enc = assignOpts.getEncoding();
								if (enc != null)
									enc = adapter.getDynamicStringConstant(enc, envelope, adapter);
								if (enc == null)
									enc = Envelope.INTERNAL_ENCODING;
								String msg = XMLDocument.convertDOMToText(
										(Node)outProperty.getValue(),
										enc,
										assignOpts.isOmitXmlDecl(),
										assignOpts.isIndent(),
										assignOpts.getMethod(),
										assignOpts.getVersion());
								byte[] byts = msg.getBytes(enc);  
								adapter.validateXML(envelope, byts, assignOpts.getParserId(), false, true, schemaNamespace, schemaName);
							}
							catch (UnsupportedEncodingException e)
							{
								ProcessException pe = new ProcessException(e);
								pe.setFaultName(
										ProcessException.CFW_NS_URI, 
										ProcessException.FAULT_CFW_ASSIGN_EXEC_ERROR);
								throw pe;
							}
						}
						break;
					
					case PropertyValue.DTYPE_RECORDSET:
						outProperty.assignValue(
						               adapter, 
						               PropertyValue.DTYPE_RECORDSET, 
						               xmlRecordReader.getRecords(), 
						               0, 
						               (String)null, 
						               (String)null,
									   assignOpts);
						break;
					
					case PropertyValue.DTYPE_STRING:
					case PropertyValue.DTYPE_BYTEARRAY:
						if (isTransformToHostData)
						{
							outBytes = adapter.getRecData();
							outBytesSize = adapter.getRecDataLength();
						}
						else
						{
							outBytes = baos.toByteArray();
							outBytesSize = baos.size();
						}
						outProperty.assignValue(
						               adapter, 
						               PropertyValue.DTYPE_BYTEARRAY, 
						               outBytes, 
						               outBytesSize, 
						               (String)null, 
						               (String)null,
									   assignOpts);
						break;
					
					case PropertyValue.DTYPE_OUT_FILE:
						((PropertyValueOutFile)outProperty).getWriter().write(baos.toString(Envelope.INTERNAL_ENCODING));
						break;
				}
			}
			else
			{
				if (baos != null)
				{
					adapter.setRecData(baos.toByteArray(), baos.size());
				}
			}
			
			// Initialize source to null in case there are multiple transforms
			envelope.setSource(null);
		}
		catch (FwException exception)
		{
			exception.setType(FwException.ERRORTYPE_TRANSFORM);
			throw exception;
		}
		catch (Exception exception)
		{
			String reqrsp = (isResponse ? "cfw_excmsg_xform_response" : "cfw_excmsg_xform_request");//$NON-NLS-1$//$NON-NLS-2$
			ProcessException pe = new ProcessException(
					resCommFwResourceBundle.getString("cfw_excmsg_xform_err_msg"), //$NON-NLS-1$
					new String[] { resCommFwResourceBundle.getString(reqrsp), "??????" }, //$NON-NLS-1$
					exception,
					FwException.ERRORTYPE_TRANSFORM);
			pe.setFaultName(
					ProcessException.CFW_NS_URI, 
					ProcessException.FAULT_CFW_TRANSFORMATION_FAILURE);
			throw pe;
		}
		finally
		{
			// Statistics: Set end event time
			envelope.endTimerEvent(ptime);
		}
		
		return;
	}

	/* Display the XML of the current activity
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		StringBuffer buff = new StringBuffer();
		if (isBPEL())
		{
			// WS-BPEL
			buff.append('<');
			buff.append(this.activityTypeName);
			if (name != null)
			{
				buff.append(" name=\""); //$NON-NLS-1$
				buff.append(getName());
				buff.append('"');
			}
			if (partnerLink != null)
			{
				buff.append(" partnerLink=\""); //$NON-NLS-1$
				buff.append(partnerLink);
				buff.append('"');
			}
			if (portType != null)
			{
				buff.append(" portType=\""); //$NON-NLS-1$
				buff.append(portType);
				buff.append('"');
			}
			if (port != null)
			{
				buff.append(" port=\""); //$NON-NLS-1$
				buff.append(port);
				buff.append('"');
			}
			if (operation != null)
			{
				buff.append(" operation=\""); //$NON-NLS-1$
				buff.append(operation);
				buff.append('"');
			}
			if (bpeOutputVariable != null)
			{
				buff.append(" outputVariable=\""); //$NON-NLS-1$
				buff.append(bpeOutputVariable);
				buff.append('"');
			}
			if (bpeInputVariable != null)
			{
				buff.append(" inputVariable=\""); //$NON-NLS-1$
				buff.append(bpeInputVariable);
				buff.append('"');
			}
			/*NS*/if (bpexInputAttachments != null)
			/*NS*/{
			/*NS*/	buff.append(" inputAttachments=\""); //$NON-NLS-1$
			/*NS*/	buff.append(bpexInputAttachments);
			/*NS*/	buff.append('"');

			/*NS*/}
			/*NS*/if (bpexInputAttachmentNames != null)
			/*NS*/{
			/*NS*/	buff.append(" inputAttachmentNames=\""); //$NON-NLS-1$
			/*NS*/	buff.append(bpexInputAttachmentNames);
			/*NS*/	buff.append('"');
			/*NS*/}
			/*NS*/if (bpexOutputAttachments != null)
			/*NS*/{
			/*NS*/	buff.append(" outputAttachments=\""); //$NON-NLS-1$
			/*NS*/	buff.append(bpexOutputAttachments);
			/*NS*/	buff.append('"');
			/*NS*/}
			/*NS*/if (bpexOutputAttachmentNames != null)
			/*NS*/{
			/*NS*/	buff.append("outputAttachmentNames=\""); //$NON-NLS-1$
			/*NS*/	buff.append(bpexOutputAttachmentNames);
			/*NS*/	buff.append("\" "); //$NON-NLS-1$
			/*NS*/}
			/*NS*/if (locate != null)
			/*NS*/{
			/*NS*/	buff.append(" locate=\""); //$NON-NLS-1$
			/*NS*/	buff.append(locate);
			/*NS*/	buff.append('"');
			/*NS*/}
			/*NS*/if (transportName != null)
			/*NS*/{
			/*NS*/	buff.append(" transportName=\""); //$NON-NLS-1$
			/*NS*/	buff.append(transportName);
			/*NS*/	buff.append('"');
			/*NS*/}
			/*NS*/if (transportMod != null)
			/*NS*/{
			/*NS*/	buff.append(" transportMod=\""); //$NON-NLS-1$
			/*NS*/	buff.append(transportMod);
			/*NS*/	buff.append('"');
			/*NS*/}
			
			buff.append(">\n"); //$NON-NLS-1$
			if (correlations != null)
			{
				for (Iterator it = correlations.iterator(); it.hasNext(); )
				{
					Activity activity = (Activity)it.next();
					buff.append(activity.toString());
					buff.append('\n');
				}
			}
		}
		else
		{
			// BPML
			buff.append('<');
			buff.append(this.activityTypeName);
			if (name != null)
			{
				buff.append(" name=\""); //$NON-NLS-1$
				buff.append(getName());
				buff.append('"');
			}
			if (portType != null)
			{
				buff.append(" portType=\""); //$NON-NLS-1$
				buff.append(portType);
				buff.append('"');
			}
			if (port != null)
			{
				buff.append(" port=\""); //$NON-NLS-1$
				buff.append(port);
				buff.append('"');
			}
			if (operation != null)
			{
				buff.append(" operation=\""); //$NON-NLS-1$
				buff.append(operation);
				buff.append('"');
			}
			if (locate != null)
			{
				buff.append(" locate=\""); //$NON-NLS-1$
				buff.append(locate);
				buff.append('"');
			}
			/*NS*/if (bpexInputAttachments != null)
			/*NS*/{
			/*NS*/	buff.append(" outputAttachments=\""); //$NON-NLS-1$
			/*NS*/	buff.append(bpexInputAttachments);
			/*NS*/	buff.append('"');

			/*NS*/}
			/*NS*/if (bpexInputAttachmentNames != null)
			/*NS*/{
			/*NS*/	buff.append(" outputAttachmentNames=\""); //$NON-NLS-1$
			/*NS*/	buff.append(bpexInputAttachmentNames);
			/*NS*/	buff.append("\" "); //$NON-NLS-1$
			/*NS*/}
			/*NS*/if (bpexOutputAttachments != null)
			/*NS*/{
			/*NS*/	buff.append(" inputAttachments=\""); //$NON-NLS-1$
			/*NS*/	buff.append(bpexOutputAttachments);
			/*NS*/	buff.append('"');
			/*NS*/}
			/*NS*/if (bpexOutputAttachmentNames != null)
			/*NS*/{
			/*NS*/	buff.append(" inputAttachmentNames=\""); //$NON-NLS-1$
			/*NS*/	buff.append(bpexOutputAttachmentNames);
			/*NS*/	buff.append('"');
			/*NS*/}
			/*NS*/if (transportName != null)
			/*NS*/{
			/*NS*/	buff.append(" transportName=\""); //$NON-NLS-1$
			/*NS*/	buff.append(transportName);
			/*NS*/	buff.append('"');
			/*NS*/}
			/*NS*/if (transportMod != null)
			/*NS*/{
			/*NS*/	buff.append(" transportMod=\""); //$NON-NLS-1$
			/*NS*/	buff.append(transportMod);
			/*NS*/	buff.append('"');
			/*NS*/}
			
			buff.append(">\n"); //$NON-NLS-1$
		}
			
		if (inputParms != null)
			for (int i = 0; i < inputParms.size(); i++)
			{
				InputParm inp = (InputParm)inputParms.get(i);
				buff.append("\n\t<"); //$NON-NLS-1$
				buff.append(inputElementDisp);
				if (isBPEL())
				{
					// WS-BPEL Extension
					if (inp.variable != null)
					{
						buff.append(" variable=\""); //$NON-NLS-1$
						buff.append(inp.variable);
						buff.append('"');
					}
					if (inp.part != null)
					{
						buff.append(" part=\""); //$NON-NLS-1$
						buff.append(inp.part);
						buff.append('"');
					}
					if (inp.xpath != null)
					{
						buff.append(" query=\""); //$NON-NLS-1$
						buff.append(inp.xpath);
						buff.append('"');
					}
					if (inp.expression != null)
					{
						buff.append(" expression=\""); //$NON-NLS-1$
						buff.append(inp.expression);
						buff.append('"');
					}
					if (inp.value != null)
					{
						buff.append('>');
						buff.append(inp.value);
						buff.append("</"); //$NON-NLS-1$
						buff.append(inputElementDisp);
					}
					if (inp.valueElement != null)
					{
						buff.append('>');
						try
						{
							buff.append(XMLDocument.convertDOMToText(inp.valueElement));
						}
						catch (Exception e)
						{
							buff.append(inp.valueElement.toString());
						}
						buff.append("</"); //$NON-NLS-1$
						buff.append(inputElementDisp);
					}
					buff.append("/>"); //$NON-NLS-1$
				}
				else
				{
					// BPML
					if (inp.type != null)
					{
						buff.append(" type=\""); //$NON-NLS-1$
						buff.append(inp.type);
						buff.append('"');
					}
					if (inp.element != null)
					{
						buff.append(" element=\""); //$NON-NLS-1$
						buff.append(inp.element);
						buff.append('"');
					}
					if (inp.expression != null)
					{
						buff.append(" xpath=\""); //$NON-NLS-1$
						buff.append(inp.expression);
						buff.append('"');
					}
					
					if (inp.variable != null || inp.value != null)
					{
						buff.append(">"); //$NON-NLS-1$
						if (inp.value != null)
						{
							buff.append("\n\t\t<value>"); //$NON-NLS-1$
							buff.append(inp.value);
							buff.append("</value>"); //$NON-NLS-1$
						}
						else
						if (inp.variable != null)
						{
							buff.append("\n\t\t<source property=\""); //$NON-NLS-1$
							buff.append(inp.variable);
							buff.append('"');
							if (inp.xpath != null)
							{
								buff.append(" xpath=\""); //$NON-NLS-1$
								buff.append(inp.xpath);
								buff.append('"');
							}
							buff.append("/>"); //$NON-NLS-1$
						}
						buff.append("\n\t</output>"); //$NON-NLS-1$
					}
					else
					{
						buff.append("/>"); //$NON-NLS-1$
					}
				}
			}
		
		if (outputParms != null)
			for (int i = 0; i < outputParms.size(); i++)
			{
				OutputParm out = (OutputParm)outputParms.get(i);
				buff.append("\n\t<"); //$NON-NLS-1$
				buff.append(outputElementDisp);
				if (isBPEL())
				{
					// WS-BPEL Extension
					if (out.variable != null)
					{
						buff.append(" variable=\""); //$NON-NLS-1$
						buff.append(out.variable);
						buff.append('"');
					}
					if (out.part != null)
					{
						buff.append(" part=\""); //$NON-NLS-1$
						buff.append(out.part);
						buff.append('"');
					}
					if (out.xpath != null)
					{
						buff.append(" query=\""); //$NON-NLS-1$
						buff.append(out.xpath);
						buff.append('"');
					}
				}
				else
				{
					// BPML
					if (out.variable != null)
					{
						buff.append(" property=\""); //$NON-NLS-1$
						buff.append(out.variable);
						buff.append("\" "); //$NON-NLS-1$
					}
					if (out.element != null)
					{
						buff.append(" element=\""); //$NON-NLS-1$
						buff.append(out.element);
						buff.append("\" "); //$NON-NLS-1$
					}
					if (out.xpath != null)
					{
						buff.append(" xpath=\""); //$NON-NLS-1$
						buff.append(out.xpath);
						buff.append("\" "); //$NON-NLS-1$
					}
				}
				if (out.assignOpts != null)
					buff.append(out.assignOpts.toString());
				
				buff.append("/>"); //$NON-NLS-1$
			}

		if (stysht != null)
		{				
			buff.append("\n\t<stylesheet "); //$NON-NLS-1$
			if (stysht.stylesheetElement != null)
			{
				buff.append(">\n");//$NON-NLS-1$
				buff.append("\n\t\t"); //$NON-NLS-1$
				try
				{
					buff.append(XMLDocument.convertDOMToText(stysht.stylesheetElement));
				}
				catch (Exception e)
				{
					buff.append(e.toString());
				}
				buff.append("\n\t</stylesheet>"); //$NON-NLS-1$
			}
			else
			{
				if (stysht.name != null)
				{
					buff.append(" name=\""); //$NON-NLS-1$
					buff.append(stysht.name);
					buff.append('"');
				}
				else
				if (stysht.variable != null)
				{
					buff.append(" variable=\""); //$NON-NLS-1$
					buff.append(stysht.variable);
					buff.append('"');
				}
				else
				if (stysht.expression != null)
				{
					buff.append(" expression=\""); //$NON-NLS-1$
					buff.append(stysht.expression);
					buff.append('"');
				}
				
				buff.append("/>"); //$NON-NLS-1$
			}
		}
		
		return buff.toString();
	}

}
