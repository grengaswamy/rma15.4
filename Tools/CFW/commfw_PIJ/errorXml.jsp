<%@ page language="java" contentType="text/xml"%>
<%@ page import="java.io.*, java.lang.reflect.*, java.text.MessageFormat" %>
<%@ page import="com.csc.cfw.process.*, com.csc.fw.util.*" %>
<%@ page isErrorPage="true" %>

<%
    boolean htmlOutput = true; %>

<%@ include file="WEB-INF/jspf/errorInfo.jspf" %>
<%
    java.util.ResourceBundle resCommFwResourceBundle = java.util.ResourceBundle.getBundle("com.csc.cfw.process.CommFwResourceBundle");  //$NON-NLS-1$
    String DEFAULT_EXCEPTIONFILE = "DefaultException.txt"; //$NON-NLS-1$
    String ExceptionFormat = null;

		Envelope envelope = new Envelope();
		CommFwAdapter adapter = null;
		boolean debug = true;
		FwException cfe = new FwException(theException);

		String tranName = null;
		String tranNameRs = null;
		
		if (adapter != null)
			tranName = adapter.getTranName();
		if (tranName == null)
			tranName = envelope.getTranName();
		if (tranName == null)
		{
			tranName = "CommunicationsFramework"; //$NON-NLS-1$
			tranNameRs = tranName;
		}
		else
			tranNameRs = Envelope.responseTransactionTag(tranName, null);
			
		String rootName = "ACORD";
		String rootNodeName = (rootName != null
								? rootName
								: "ACORD");//$NON-NLS-1$
		String servRespName = Envelope.responseTransactionTag(
								envelope.domServiceNodeName != null
								? envelope.domServiceNodeName
								: "InsuranceSvcRq", null );//$NON-NLS-1$

		// Get the default exception format file.  Check to see if there is
		// an adapter specific error format.
		String format = ExceptionFormat;
		String s = null;
		try
		{
			BufferedReader exceptionFileReader = null;
			if (adapter != null)
				s = (String)adapter.getProperty(CommFwAdapter.ADAPTER_ExceptionFileName);
					
			if (s != null && s.length() > 0)
				exceptionFileReader = FwSystem.getResourceAsReader(s);
			
			if (exceptionFileReader != null)
			{
				StringWriter buffer = new StringWriter();
				PrintWriter outf = new PrintWriter(buffer, true);
				String line;
				while ((line = exceptionFileReader.readLine()) != null)
				{
					outf.println(line);
				}
				outf.close();
				exceptionFileReader.close();
				format = buffer.toString();
			}
		}
		catch (Exception e)
		{
			envelope.getFwSystem().getSystemStatus().logWarningEvent(
			                       FwSystem.currentTimestamp(), 
			                       new FwException(resCommFwResourceBundle.getString("cfw_err_build_exc_fmt_file"),  //$NON-NLS-1$
			                                       new Object[] { s }, 
			                                       e).toString(), 
			                       (String)null);
		}

		// If no adapter specific format and the default one is not valued
		// then setup the default format.
		if (format == null)
		{
			try
			{
				s = DEFAULT_EXCEPTIONFILE; 
				BufferedReader exceptionFileReader = FwSystem.getResourceAsReader(s);
				StringWriter buffer = new StringWriter();
				PrintWriter outf = new PrintWriter(buffer, true);
				String line;
				while ((line = exceptionFileReader.readLine()) != null)
					outf.println(line);
				outf.close();
				exceptionFileReader.close();
				format = buffer.toString();
				ExceptionFormat = format;
			}
			catch (Throwable e)
			{
				envelope.getFwSystem().getSystemStatus().logWarningEvent(
				                       FwSystem.currentTimestamp(), 
				                       new FwException(resCommFwResourceBundle.getString("cfw_err_build_exc_fmt_file"),  //$NON-NLS-1$
				                                       new Object[] { s }, 
				                                       e).toString(),
				                       (String)null);
			}
		}

		// If we had trouble finding our default exception format file,  Our backup
		// is to use a hardcoded definition in the program..
		if (format == null)
		{
			String statusCodeVal = resCommFwResourceBundle.getString("cfw_exc_dispmsg_status"); //$NON-NLS-1$
			String statusSeverityVal = resCommFwResourceBundle.getString("cfw_exc_dispmsg_severity"); //$NON-NLS-1$
			String extStatusCodeVal = resCommFwResourceBundle.getString("cfw_exc_dispmsg_ext_status"); //$NON-NLS-1$

			StringWriter buffer = new StringWriter();
			PrintWriter writer = new PrintWriter(buffer, true);
			writer.println(resCommFwResourceBundle.getString("cfw_xml_header")); //$NON-NLS-1$
			writer.println("<{0}>");//$NON-NLS-1$
			writer.println("  <Status>");//$NON-NLS-1$
			writer.println("    <StatusCd>" + statusSeverityVal +"</StatusCd>");//$NON-NLS-1$//$NON-NLS-2$
			writer.println("    <StatusDesc>{3}</StatusDesc>");//$NON-NLS-1$
			writer.println("  </Status>");//$NON-NLS-1$
			writer.println(" <{1}>");//$NON-NLS-1$
			writer.println("  <{2}>");//$NON-NLS-1$
			writer.println("    <MsgStatus>");//$NON-NLS-1$
			writer.println("      <MsgStatusCd>" + statusSeverityVal +"</MsgStatusCd>");//$NON-NLS-1$//$NON-NLS-2$
			writer.println("    <MsgStatusDesc>{3}</MsgStatusDesc>");//$NON-NLS-1$
			writer.println("    <ExtendedStatus>");//$NON-NLS-1$
			writer.println("      <ExtendedStatusCd>" + statusSeverityVal + "</ExtendedStatusCd>");//$NON-NLS-1$//$NON-NLS-2$
			writer.println("      <ExtendedStatusDesc>{4}</ExtendedStatusDesc>");//$NON-NLS-1$
			writer.println("    </ExtendedStatus>");//$NON-NLS-1$
			writer.println("    <ExtendedStatus>");//$NON-NLS-1$
			writer.println("    <ExtendedStatusCd>UserError</ExtendedStatusCd>");//$NON-NLS-1$
			writer.println(resCommFwResourceBundle.getString("cfw_exc_xml_opentag") + resCommFwResourceBundle.getString("cfw_classmsg_def") + resCommFwResourceBundle.getString("cfw_exc_xml_closetag"));//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
			writer.println("    </ExtendedStatus>");//$NON-NLS-1$
			writer.println("    </MsgStatus>");//$NON-NLS-1$
			writer.println("  </{2}>");//$NON-NLS-1$
			writer.println(" </{1}>");//$NON-NLS-1$
			writer.println("</{0}>");//$NON-NLS-1$

			format = buffer.toString();
		}

		// We should now have an exception format pattern.  Return a string with
		// the substituted parameters.	
		out.println(MessageFormat.format(format,
					new Object[]
						{	rootNodeName,
							servRespName,
							tranNameRs,
							cfe.getTypeMsg(),
							exception_info } ));

%>
