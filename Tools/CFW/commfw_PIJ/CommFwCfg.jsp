<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- ------------------------------------------------------------------- --%>
<%-- This is a JSP to configure the Communications Framework properties. --%>
<%-- ------------------------------------------------------------------- --%>
<%@ page language='java' contentType='text/html'%>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="org.w3c.dom.Document" %>
<%@ page import="com.csc.cfw.process.ProcessFactory" %>
<%@ page import="com.csc.cfw.recordbuilder.RecordManagerFactory" %>
<%@ page import="com.csc.cfw.util.CommFwSystem" %>
<%@ page import="com.csc.cfw.xml.domain.CobolRecordFactory" %>
<%@ page import="com.csc.fw.util.FwException" %>
<%@ page import="com.csc.fw.util.FwSystem" %>
<%@ page import="com.csc.fw.util.PropertyFileManager" %>
<%@ page import="com.csc.fw.util.ServletUtil" %>
<%@ page import="com.csc.fw.util.XMLDocument" %>
<%@ page import="com.csc.fw.util.XMLFileConfigurator" %>
<%@ page errorPage="error.jsp" %>

<HTML>
  <LINK href="includes/INFUtil.css" type=text/css rel=stylesheet>
  <HEAD>
    <TITLE>Application Configuration - Communications Framework</TITLE>
    <META name="GENERATOR" content="IBM WebSphere Studio">
  </HEAD>
  <BODY BGCOLOR="#C0C0C0">
    
    <script language="JavaScript">
    var lastSearchPos = 0;
    var lastSearchText = "";
    /*----------------------------------------------------*/
    /* Find a text string in a text area                  */
    /*----------------------------------------------------*/
    function findTextAreaString(searchForm, searchTextArea)
    {
        var searchText;
        if (searchForm.searchTextInput)
            searchText = searchForm.searchTextInput.value;
        else
            searchText = prompt("Please enter search text:",lastSearchText);
        if (!searchText)
        {
            alert("No search text specified");
            return;
        }
        if (searchText != lastSearchText)
        {
            lastSearchText = searchText;
            lastSearchPos = 0;
        }
        var data = searchTextArea.value;
        searchText = searchText.toLowerCase();
        data = data.toLowerCase();
        var datasubstr = data.substring(lastSearchPos);
        var pos = datasubstr.search(searchText);
        if (pos < 0)
        {
            alert("Search text not found");
            lastSearchPos = 0;
            return;
        }
        pos += lastSearchPos;
        var count = 0;
        for (var i = 0; i < pos; i++)
        {
            if (data.charAt(i) == '\r') 
                count++; 
        }
        if (searchTextArea.createTextRange)
        {
            var rng = searchTextArea.createTextRange();
            if (rng)
            {
                //alert("Current search Pos: " + pos);
                if (false) //if (false == rng.findText(searchText,lastSearchPos))
                    alert("Search text not found");
                else
                {
                    rng.moveStart("character", pos - count);
                    rng.collapse();
                    rng.select();
                }
            }
            
        }
        lastSearchPos = pos + searchText.length;
    }
    </script>

    <DIV ALIGN="Center" ID="HtmlDiv1">
    <TABLE>
      <TR>
        <TD ALIGN="Center"  >
          <H4>Communications Framework Application Configuration</H4>
        </TD>
      </TR>

      <TR>
        <TD  >
          <TABLE>
            <TR ALIGN="Center"><TD ALIGN="Center"  >
              <TABLE>

<%
    FwSystem fwSys = CommFwSystem.current();
    String updateTextBuffer = null;
    XMLFileConfigurator xmlConfig = new XMLFileConfigurator(fwSys, request);
    boolean bIE = (ServletUtil.determineBrowserVersion(request).getType() == ServletUtil.BROWSER_MSIE);

    String recordFileName = "XMLTagMap.properties";
    String recordFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, recordFileName);
    String ronlyBuffRecord = null;
    boolean ronlyRecord = false;
    if (recordFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(recordFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffRecord = sw.getBuffer().toString();
            ronlyRecord = true;
        }
    }

    String tranFileName = ProcessFactory.current().configFile();
    String procPath = fwSys.getProperty(FwSystem.SYSPROP_processLocation).toString();
    if (procPath.endsWith("/"))
        procPath = procPath.substring(0, procPath.length() - 1);
    String path = request.getContextPath();
    int temp = (procPath.indexOf(path) + path.length());
    procPath = procPath.substring(temp);
    procPath = getServletConfig().getServletContext().getRealPath(procPath);
    List tranFileList = ProcessFactory.current().configFileList(procPath);
    if (tranFileList == null)
        tranFileList = new ArrayList();
    if (tranFileList.size() == 0)
        tranFileList.add(tranFileName);
    String tranPropFile = request.getParameter("tranPropFile");
    if (tranPropFile != null)
        tranFileName = tranPropFile;
    else
    {
        /* Default the filename to CommFwMessages&#x25;&#x25;APPLICATION&#x25;&#x25;.xml */
        String tranFileDefault = tranFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && tranFileDefault.endsWith(sfx))
        {
            tranFileDefault = tranFileDefault.substring(0, tranFileDefault.length() - sfx.length());
            tranFileDefault += app + sfx;
        }
        for (int i = 0; i < tranFileList.size(); i++)
        {
            String s = tranFileList.get(i).toString();
            if (tranFileDefault.equals(s))
            {
                tranFileName = tranFileDefault;
                break;
            }
        }
    }
    String tranFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, tranFileName);
    if (tranFilePath == null)
    {
        tranFilePath = procPath + "/" + tranFileName;
        File file = new File(tranFilePath);
        if (!file.exists())
            tranFilePath = null;
    }
    
    String errorMessageTran = null;
    String ronlyBuffTran = null;
    boolean ronlyTran = false;
    if (tranFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(tranFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffTran = sw.getBuffer().toString();
            ronlyTran = true;
        }
    }



    String recordCtrlFileName = RecordManagerFactory.current().configFile();
    List recordCtrlFileList = RecordManagerFactory.current().configFileList(null);
    if (recordCtrlFileList == null)
        recordCtrlFileList = new ArrayList();
    if (recordCtrlFileList.size() == 0)
        recordCtrlFileList.add(recordCtrlFileName);
    String recordCtrlPropFile = request.getParameter("recordCtrlPropFile");
    if (recordCtrlPropFile != null)
        recordCtrlFileName = recordCtrlPropFile;
    else
    {
        /* Default the filename to CommFwRecord&#x25;&#x25;APPLICATION&#x25;&#x25;.xml */
        String recordCtrlFileDefault = recordCtrlFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && recordCtrlFileDefault.endsWith(sfx))
        {
            recordCtrlFileDefault = recordCtrlFileDefault.substring(0, recordCtrlFileDefault.length() - sfx.length());
            recordCtrlFileDefault += app + sfx;
        }
        for (int i = 0; i < recordCtrlFileList.size(); i++)
        {
            String s = recordCtrlFileList.get(i).toString();
            if (recordCtrlFileDefault.equals(s))
            {
                recordCtrlFileName = recordCtrlFileDefault;
                break;
            }
        }
    }
    
    String recordCtrlFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, recordCtrlFileName);
    String errorMessageRecordCtrl = null;
    String ronlyBuffRecordCtrl = null;
    boolean ronlyRecordCtrl = false;
    if (recordCtrlFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(recordCtrlFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffRecordCtrl = sw.getBuffer().toString();
            ronlyRecordCtrl = true;
        }
    }
    

    if ("POST".equals(request.getMethod()))
    {
        String func = request.getParameter("function");
        if ("Set Record Properties".equalsIgnoreCase(func))
        {
            String propFilename = CobolRecordFactory.resFileName;
            StringWriter chgBuff = new StringWriter();
            PrintWriter logWriter = new PrintWriter(chgBuff);
            PropertyFileManager.updatePropertiesWithFile(
                    request.getParameter("RecordPropTextArea"), 
                    CobolRecordFactory.getCurrent().register,
                    logWriter);
            logWriter.close();
            fwSys.logFileChange(request, propFilename, chgBuff.toString());
        }
        else
        if ("Reset XSL Cache".equalsIgnoreCase(func))
            CommFwSystem.current().getTransformCache().reset();                   
        else
        if ("Reset Record Cache".equalsIgnoreCase(func))
            CobolRecordFactory.getCurrent().recordDescCache.reset();
        else
        if ("Update Record Property File".equalsIgnoreCase(func))
        {
            String newBuffer = request.getParameter("RecordPropTextArea");
            try
            {
                PropertyFileManager.writePropertyFile(recordFilePath, newBuffer);
                String propFilename = CobolRecordFactory.resFileName;
                StringWriter chgBuff = new StringWriter();
                PrintWriter logWriter = new PrintWriter(chgBuff);
                PropertyFileManager.updatePropertiesWithFile(
                        newBuffer,
                        CobolRecordFactory.getCurrent().register,
                        logWriter);
                logWriter.close();
                fwSys.logFileChange(request, propFilename, chgBuff.toString());
            }
            catch (IOException e)
            {
                String err = "<FONT SIZE=+1><B>Error updating Record Mapping file [" + recordFileName + "]: </B></FONT>" + e.toString();
%>
                <tr>
                  <td width="100%"  >
                    <hr size="1" NOSHADE width="100%" /><br />
                    <%=err %><br />
                    <hr size="1" NOSHADE width="100%" />
                  </td>
                </tr>
<%
            }
        }
        else
        if ("Update Transaction Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("TransactionTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                boolean isMsgFile = tranFileName.startsWith("CommFwMessages");
                if (isMsgFile)
                {
                    xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        tranFilePath,
                        tranFileName,
                        "Message",
                        "name",
                        true,
                        true,
                        "location");
                }
                else
                {
                    xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        tranFilePath,
                        tranFileName,
                        "Transaction Control",
                        null,
                        false,
                        false,
                        null);
                }
            }
            catch (FwException e)
            {
                errorMessageTran = "<FONT COLOR='#FF0000'><B>Updated Transaction/Message XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageTran == null)
                try
                {
                    PropertyFileManager.writePropertyFile(tranFilePath, updateTextBuffer);
                    ProcessFactory.current().configSetFileCache(tranFileName, updateTextBuffer);
                    ProcessFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageTran = "<FONT COLOR='#FF0000'><B>Error updating Transaction/Message file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Reset Record Control Cache".equalsIgnoreCase(func))
        {
            RecordManagerFactory.reset();
        }
        else
        if ("Update Record Control File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("RecordTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        recordCtrlFilePath,
                        recordCtrlFileName,
                        "Record Control",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageRecordCtrl = "<FONT COLOR='#FF0000'><B>Updated Record Control XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageRecordCtrl == null)
                try
                {
                    PropertyFileManager.writePropertyFile(recordCtrlFilePath, updateTextBuffer);
                    RecordManagerFactory.current().configSetFileCache(recordCtrlFileName, updateTextBuffer);
                    RecordManagerFactory.reset();
                }
                catch (IOException e)
                {
                    errorMessageRecordCtrl = "<FONT COLOR='#FF0000'><B>Error updating Record Control file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
    }
%>

                <TR>
                  <TD WIDTH="100%"  >
                    <TABLE WIDTH="100%">
                      <TR "WIDTH=100%">
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                      <TR WIDTH="100%">
                        <TD ALIGN="left"><A href="CommFwSys.jsp">System Configuration</A></TD>
                        <TD ALIGN="center"><A href="CommFwCtrl.jsp">System Control</A></TD>
                        <TD ALIGN="center"><A href="CommFwCfg.jsp">Application Configuration</A></TD>
                        <TD ALIGN="right"><A href="CommFwStatus.jsp">System Status</A></TD>			
                      </TR>
                      <TR "WIDTH=100%">
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>


<%
        if (!CobolRecordFactory.getCurrent().isMapDeactivated())
        {
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgRecordMapForm" ACTION="CommFwCfg.jsp">
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Record Map Properties</FONT></FONT></B>
                              </TD>
                            </TR>
                            <%
            if (recordFilePath != null)
            {
                            %>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT" readonly="readonly" value="<%=recordFilePath%>" title="<%=recordFilePath%>"></TD>
                            </TR>
                            <%
            }
                            %>
                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="RecordPropTextArea">
<%
            if (recordFilePath != null)
            {
%><%=PropertyFileManager.mergeFileWithProperties(recordFilePath, CobolRecordFactory.getCurrent().register)%><%
            }
            else
            if (ronlyRecord)
            {
%><%=ronlyBuffRecord%><%
            }
            else
            if (recordFilePath == null)
            {
                Iterator items = CobolRecordFactory.getCurrent().register.keySet().iterator();
                while (items.hasNext())
                {
                    String key = (String)items.next();
%><%=key+"="+(String)(CobolRecordFactory.getCurrent().register.get(key))%>
<%
                }
            }
%></TEXTAREA>
                              </TD>
                            </TR>

<%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgRecordMapForm, document.CfwCfgRecordMapForm.RecordPropTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>

                            <TR>
                              <TD ALIGN="Left"  >
                                <input class="btnNoSize" value="Set Record Properties" type="submit" name="function">   <input class="btnNoSize" value="Reset" type="reset">
<%
            if (recordFilePath != null)
            {
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Update Record Property File" type="submit" name="function">
<%
            }
%>
                              </TD>
                            </TR>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
        }
%>


                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgRecordForm" ACTION="CommFwCfg.jsp#records">
                   <A NAME="records"></A>
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Record Control</FONT></FONT></B>
                              </TD>
                            </TR>

                            <TR>
                              <TD>
<%
        if (recordCtrlFileList.size() > 1)
        {
%>
                               <SELECT NAME="recordCtrlPropFile" SIZE="1" onChange="document.CfwCfgRecordForm.submit()">
<%
            for (int i = 0; i < recordCtrlFileList.size(); i++)
            {
                String filename = (String)recordCtrlFileList.get(i);
%>
                                 <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(recordCtrlFileName,filename) %>><%=filename%></OPTION>
<%
            }
%>
                               </SELECT>
<%
        }
%>
                              </TD>
                            </TR>

<%
        if (recordCtrlFilePath != null || ronlyRecordCtrl)
        {
            if (recordCtrlFilePath != null)
            {
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT" readonly="readonly" value="<%=recordCtrlFilePath%>" title="<%=recordCtrlFilePath%>"></TD>
                            </TR>
<%
            }
            
            if (errorMessageRecordCtrl != null)
            {
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageRecordCtrl %><BR />
                              </TD>
                            </TR>
<%
            }
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="RecordTextArea">
<%
            if (errorMessageRecordCtrl != null)
            {
%><%=updateTextBuffer%><%                
            }
            else
            if (ronlyRecordCtrl)
            {
%><%=ronlyBuffRecordCtrl%><%
            }
            else
            {
                StringWriter sw = new StringWriter();
                PrintWriter writer = new PrintWriter(sw);
                PropertyFileManager.readPropertyFile(recordCtrlFilePath, writer);
%><%=sw.toString()%><%
            }
%></TEXTAREA>
                              </TD>
                            </TR>

 <%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgRecordForm, document.CfwCfgRecordForm.RecordTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>
 
                            <TR>
                              <TD ALIGN="Left"  >
                                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Reset Record Control Cache" type="submit" name="function">
<%
            if (!ronlyRecordCtrl)
            {
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Update Record Control File" type="submit" name="function">
<%
            }
%>
                              </TD>
                            </TR>
<%
        }
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>


                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgTranForm" ACTION="CommFwCfg.jsp#transactions">
                   <A NAME="transactions"></A>
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Transaction Control</FONT></FONT></B>
                              </TD>
                            </TR>
                            <TR>
                              <TD>
<%
        if (tranFileList != null && tranFileList.size() > 1)
        {
%>
                               <SELECT NAME="tranPropFile" SIZE="1" onChange="document.CfwCfgTranForm.submit()">
<%
            for (int i = 0; i < tranFileList.size(); i++)
            {
                String filename = (String)tranFileList.get(i);
%>
                                 <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(tranFileName,filename) %>><%=filename%></OPTION>
<%
            }
%>
                               </SELECT>
<%
        }
%>
                              </TD>
                            </TR>
                            
<%
        if (tranFilePath != null || ronlyTran)
        {
            if (tranFilePath != null)
            {
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT" readonly="readonly" value="<%=tranFilePath%>" title="<%=tranFilePath%>"></TD>
                            </TR>
<%
            }
            
            if (errorMessageTran != null)
            {
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageTran %><BR />
                              </TD>
                            </TR>
<%
            }
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="75" WRAP="OFF" ROWS="20" NAME="TransactionTextArea">
<%
            if (errorMessageTran != null)
            {
%><%=updateTextBuffer%><%                
            }
			else
			if (ronlyTran)
			{
%><%=ronlyBuffTran%><%
			}
            else
            {
                StringWriter sw = new StringWriter();
                PrintWriter writer = new PrintWriter(sw);
                PropertyFileManager.readPropertyFile(tranFilePath, writer);
%><%=sw.toString()%><%
            }
%></TEXTAREA>
                              </TD>
                            </TR>

<%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgTranForm, document.CfwCfgTranForm.TransactionTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>
 
                            <TR>
                              <TD ALIGN="Left"  >
                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Reset Transaction Control Cache" type="submit" name="function">
<%
            if (!ronlyRecordCtrl)
            {
%>
                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="btnNoSize" value="Update Transaction Control File" type="submit" name="function">
<%
            }
%>
                              </TD>
                            </TR>
<%
        }
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>


                <TR>
                  <TD WIDTH="100%"  >
                    <TABLE WIDTH="100%">
                      <TR "WIDTH=100%">
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                      <TR WIDTH="100%">
                        <TD ALIGN="left"><A href="CommFwSys.jsp">System Configuration</A></TD>
                        <TD ALIGN="center"><A href="CommFwCtrl.jsp">System Control</A></TD>
                        <TD ALIGN="center"><A href="CommFwCfg.jsp">Application Configuration</A></TD>
                        <TD ALIGN="right"><A href="CommFwStatus.jsp">System Status</A></TD>			
                      </TR>
                      <TR "WIDTH=100%">
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                    </TABLE>
                  </TD>
                </TR>


              </TABLE>
            </TR>
          </TABLE>
        </TD>
      </TR>
    </TABLE>

    </DIV>
  </BODY>
</HTML>
