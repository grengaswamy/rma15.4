<%-- ----------------------------------------------------------------- --%>
<%-- This is a JSP to Test an XML Transaction.                         --%>
<%-- The XML will be directly entered by the user.                     --%>
<%-- ----------------------------------------------------------------- --%>
<%@page language='java' contentType='text/html'%>
<%@page import="java.util.Enumeration" %>
<%@page import="java.util.Vector" %>
<%@page import="com.csc.cfw.util.CommFwSystem" %>
<%@page import="com.csc.fw.util.FwSystem" %>
<%@page import="com.csc.fw.util.ServletUtil" %>

<jsp:useBean id='commFwData' scope='page' class='com.csc.cfw.util.test.CommFwTestData'/>
<jsp:setProperty name='commFwData' property='*' />
<%
boolean fExecute = (commFwData.function != null);
boolean fForceValidationOff = true;
if (fExecute)
{
	commFwData.newTest();
	commFwData.getAuthCredentials(request, session);
	String path = (String)CommFwSystem.current().getProperty("InfrastructureXML");
	if (path == null)
		path = "/xml";
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "noTransformCache", request.getParameter("noTransformCache")));
	commFwData.setAdditionalSwitches(ServletUtil.setSwitch(commFwData.getAdditionalSwitches(), "setOverrideProperties", commFwData.getOverridePropertiesOption()));
	commFwData.setRepostCount(request.getParameter("repostCount"));
	commFwData.setXslTraceOptions(request.getParameterValues("xslTraceOptions"));
	if ("Execute Full Trip".equals(commFwData.function))
		commFwData.setXmlUrl(path + "/InstallTestValid.xml");
	else
	if ("Execute Error Trip".equals(commFwData.function))
		commFwData.setXmlUrl(path + "/InstallTestFail.xml");
	else
		commFwData.setXmlUrl(path + "/InstallTestUwrn.xml");
	commFwData.execute();
}
else
{
	commFwData.newTest();
}
%>

<HTML>
 <HEAD>
  <META http-equiv="Content-Type" content="text/html" charset="UTF-8" />
  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <TITLE>Infrastructure Test<%=fExecute ? " Response" : "" %> - Communications Framework</TITLE>
 </HEAD>
 <BODY BGCOLOR="#C0C0C0">
 <SCRIPT LANGUAGE="JAVASCRIPT">
  <%= commFwData.getXmlStringsAsJS() %>
 </SCRIPT>
 <DIV ALIGN="Center" ID="HtmlDiv1">
  <!--FORM METHOD="POST" NAME="InfrastructureTestForm" ACTION="InfrastructureTest.jsp"-->
  <TABLE>
   <TR>
    <TD ALIGN="Center"  >
     <H4>Infrastructure Test</H4>
     <HR WIDTH="100%">
    </TD>
   </TR>
   <TR>
    <TD align="center">
      <FORM METHOD="POST" NAME="InfrastructureTestForm" ACTION="InfrastructureTest.jsp">
      <TABLE>
       <TR>
	    <TD Colspan="1" align="Center">
         <TABLE Width="100%">
          <TR Colspan="1" align="Center">
	        <TD align="Left">
              <INPUT VALUE="Execute Full Trip" TYPE="SUBMIT" NAME="function">
            </TD>  
	        <TD align="Center">
              <INPUT VALUE="Execute Error Trip" TYPE="SUBMIT" NAME="function">
            </TD>
            <TD align="Right">
              <INPUT VALUE="Execute Warning Trip" TYPE="SUBMIT" NAME="function">
            </TD>
	      </TR>
          <TR>
            <TD Colspan="3"><HR WIDTH="100%"></TD>
          </TR>
         </TABLE>
        </TD>
       </TR>

<%@ include file="WEB-INF/jspf/CFwXMLDataForm.jspf" %>

      </TABLE>
      </FORM>

      <FORM METHOD="POST" NAME="CommFwResultForm" ACTION="">
        <INPUT TYPE="hidden" NAME="locale" 
<%  if (commFwData.getLocale() != null)
    {
%>
                                 VALUE="<jsp:getProperty name='commFwData' property='locale'/>" 
<%
    }
%>
>

        <TABLE>

<%@ include file="WEB-INF/jspf/CFwXMLResultForm.jspf" %>

        </TABLE>
      </FORM>
    </TD>
   </TR>
  </TABLE>
  <!--/FORM-->
 </DIV>
 </BODY>
</HTML>
