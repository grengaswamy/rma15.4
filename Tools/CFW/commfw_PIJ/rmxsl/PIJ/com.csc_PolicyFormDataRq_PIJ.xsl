<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:template match="/">
    <PT4JPROC>
      <xsl:call-template name="ProcName" />
    </PT4JPROC>
  </xsl:template>
  <xsl:template name= "ProcName" >
    <xsl:variable name="LOB" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_BaseLOBLine" />
    <SQLStmt>
      <xsl:choose>
        <xsl:when test="$LOB='WL'">
          <xsl:variable name="PolicyNum" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/PolicyNumber" />
          <xsl:variable name="Symbol" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/CompanyProductCd" />
          <xsl:variable name="Module" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId" />
          <xsl:variable name="MasterCompany" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId" />
          <xsl:variable name="Locationcompany" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId" />
          <xsl:variable name="State" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitStateID']/OtherId" />
          SELECT
          DISTINCT A.WC33_STATUS As "OUT-Z1AAST",  A.WC33_FORMID AS "OUT-Z1B8NB",  A.WC33_RATEOP AS "OUT-RATEOP",   A.WC33_ENDEFFDATE AS "Z1AMDT",
          A.WC33_ENTRYDTE AS "OUT-ENTRYDTE",    A.WC33_FORMADDED AS "OUT-REPRINT-FORM-INDIC",  A.WC33_STATE AS "OUT-Z1STATE", 
          B.TBWC20_STATE, B.TBWC20_DESC AS "OUT-ZRIETX"
          FROM PMSPWC33 AS A, TBWC20 AS B
          WHERE
          ((A.WC33_STATE = B.TBWC20_STATE OR
          A.WC33_STATE = '99')  AND
          A.WC33_FORMID = B.TBWC20_FORM)
          AND
          A.WC33_SYMBOL    = '<xsl:value-of select="$Symbol"/>'     AND
            A.WC33_POLICY_NO = '<xsl:value-of select="$PolicyNum"/>' AND
            A.WC33_MODULE    = '<xsl:value-of select="$Module"/>'      AND
            A.WC33_MASTER_CO = '<xsl:value-of select="$MasterCompany"/>'      AND
            A.WC33_LOCATION  = '<xsl:value-of select="$Locationcompany"/>'      AND
            A.WC33_STATE     = <xsl:value-of select="$State"/>
          </xsl:when>
        <xsl:otherwise>
          CALL BASFORMSLST1('<xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/CompanyProductCd"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/PolicyNumber"/>
            <xsl:with-param name="size">7</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId"/>
            <xsl:with-param name="size">2</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId"/>
            <xsl:with-param name="size">3</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatIntParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">5</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatIntParameterValue">
            <xsl:with-param name="value" select="''"/>
            <xsl:with-param name="size">5</xsl:with-param>
          </xsl:call-template>
          <xsl:call-template name="FormatIntParameterValue">
            <xsl:with-param name="value" select="/ACORD/ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId"/>
            <xsl:with-param name="size">5</xsl:with-param>
          </xsl:call-template>')
        </xsl:otherwise>
      </xsl:choose>
    </SQLStmt>
  </xsl:template>
</xsl:stylesheet>