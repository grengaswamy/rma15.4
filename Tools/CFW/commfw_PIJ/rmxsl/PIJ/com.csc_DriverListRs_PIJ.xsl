<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <xsl:param name="client-file" select="default"/>

    <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
          <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <com.csc_DriverListRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
          <xsl:apply-templates select="/POINTJDBCRs/Rows/Row" />
        </com.csc_DriverListRs>
    </ClaimsSvcRs>
    </ACORD>
  </xsl:template>

  <xsl:template match="/POINTJDBCRs/Rows/Row" >
    <xsl:if test="string-length(DR-TYPE) > 0 and string-length(DR-NAME) > 0">
      <xsl:call-template name="DriverRow"/>  
    </xsl:if>
  </xsl:template>

  <xsl:template name="DriverRow">
    <DriverInfo>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_DriverId</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(DR-ID)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_DriverType</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(DR-TYPE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_DriverProduct</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(DR-PRODUCT)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(DR-LOCATION)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(DR-SUB-LOC)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <License>
        <LicensedDt>
          <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
            <xsl:with-param name="datefld" select="DR-LIC-DATE"/>
          </xsl:call-template>
        </LicensedDt>
        <LicensePermitNumber>
          <xsl:value-of select="normalize-space(DR-LIC-NUM)"/>
        </LicensePermitNumber>
        <StateProvCd>
          <xsl:value-of select="normalize-space(DR-LIC-STATE)"/>
        </StateProvCd>
      </License>
      <PersonInfo>
        <BirthDt>
          <xsl:call-template name="cvtdateCYYMMDDtoMMDDYYYY">
            <xsl:with-param name="datefld" select="DR-DOB"/>
          </xsl:call-template>
        </BirthDt>
        <NameInfo>
          <CommlName>
            <CommercialName>
              <xsl:value-of select="normalize-space(DR-NAME)"/>
            </CommercialName>
          </CommlName>
          <PersonName>
            <TitlePrefix/>
            <NameSuffix/>
            <Surname/>
            <GivenName/>
            <OtherGivenName/>
            <NickName/>
          </PersonName>
          <LegalEntityCd></LegalEntityCd>
          <TaxIdentity>
            <TaxId>
              <xsl:value-of select="normalize-space(DR-FEIN)"/>
            </TaxId>
          </TaxIdentity>
        </NameInfo>
      </PersonInfo>
      <com.csc_DriverStatus>
        <xsl:value-of select="normalize-space(DR-STATUS-DESC)"/>
      </com.csc_DriverStatus>
      <com.csc_IssueCd>
        <xsl:value-of select="normalize-space(DR-ISSUE-CODE)"/>
      </com.csc_IssueCd>
    </DriverInfo>
  </xsl:template>
</xsl:stylesheet>