<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="Change-Status.xsl"/>
  <xsl:include href="DecomposeErrorRecord.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:include href="ConvertDateMMDDYYYYtoCYYMMDD.xsl"/>
  <xsl:output  method="xml" indent="yes"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="loss-date" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <xsl:param name="client-file" select="default"/>
  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <xsl:apply-templates select="//ERROR__LST__ROW"/>
        <com.csc_PolicyInquiryRs>
          <RqUID/>
          <TransactionResponseDt/>
          <TransactionEffectiveDt/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_DetailInfo>
            <Status>
              <StatusCd/>
              <StatusDesc/>
              <com.csc_ServerStatusCode/>
              <com.csc_Severity/>
            </Status>
            <com.csc_RoutingInfo>PolicyNbr|PolicyId|QuoteNbr0|TrsEffDt|OgnEffDt</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
            <xsl:apply-templates select="//POLICY__INFORMATION__REC__ROW"/>
            <xsl:if test="//P21__RECORDS__ROW">
              <xsl:call-template name="PolicyInfo"/>
            </xsl:if>
          </com.csc_DetailInfo>
        </com.csc_PolicyInquiryRs>
      </ClaimsSvcRs>
    </ACORD>
  </xsl:template>

  <xsl:template  name="PolicyInfo" match="//POLICY__INFORMATION__REC__ROW">
    <com.csc_PolicyLevel>
      <PolicySummaryInfo>
        <ItemIdInfo>
          <OtherIdentifier>
            <OtherIdTypeCd/>
            <OtherId/>
          </OtherIdentifier>
        </ItemIdInfo>
        <PolicyNumber com.csc_colind="ReadOnly">
          <xsl:value-of select="normalize-space(//PIF__POLICY__NUMBER)"/>
        </PolicyNumber>
        <FullTermAmt>
          <Amt/>
        </FullTermAmt>
        <PolicyStatusCd com.csc_colind="ReadOnly">
          <xsl:call-template name="Change-Status">
            <xsl:with-param name="status" select="normalize-space(//PIF__TRANS__STAT)"/>
          </xsl:call-template>
        </PolicyStatusCd>
        <com.csc_TransactionStatusDesc com.csc_colind="ReadOnly"/>
        <com.csc_InstallmentTerm>
          <xsl:value-of select="normalize-space(//PIF__INSTAL__TERM)"/>
        </com.csc_InstallmentTerm>
        <com.csc_InstallmentNo>
          <xsl:value-of select="normalize-space(//PIF__NUM__INSTAL)"/>
        </com.csc_InstallmentNo>
        <com.csc_TransAmt>
          <Amt/>
        </com.csc_TransAmt>
        <LOBCd>
          <xsl:value-of select="normalize-space(substring(concat(//PIF__LINE__BUSINESS,'   '), 1, 3))"/>
        </LOBCd>
        <GroupId/>
      </PolicySummaryInfo>
      <StateProvCd com.csc_colind="ReadOnly">
        <xsl:value-of select="normalize-space(//PIF__RISK__STATE__PROV)"/>
      </StateProvCd>
      <CompanyProductCd com.csc_colind="ReadOnly">
        <xsl:value-of select="normalize-space(//PIF__SYMBOL)"/>
      </CompanyProductCd>
      <NAICCd com.csc_colind="ReadOnly"/>
      <ContractNumber>
        <xsl:value-of select="normalize-space(//PIF__AGENCY__NUMBER)"/>
      </ContractNumber>
      <com.csc_AgentName>
        <xsl:value-of select="normalize-space(//PIF__AGENT__NAME)"/>
      </com.csc_AgentName>
      <com.csc_AgentPremium>
        <xsl:value-of select="normalize-space(//PIF__TOTAL__AGEN__PREM)"/>
      </com.csc_AgentPremium>
      <com.csc_IssueCd>
        <xsl:value-of select="normalize-space(//PIF__ISSUE__CODE)"/>
      </com.csc_IssueCd>
      <com.csc_IssueCodeDesc>
        <xsl:value-of select="normalize-space(//PIF__ISSUE__CODE__DESC)"/>
      </com.csc_IssueCodeDesc>
      <com.csc_ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//PIF__LOCATION)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//PIF__MASTER__CO__NUMBER)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//PIF__MODULE)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_PolCompany</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(//PIF__COMPANY__NUMBER)"/>
          </OtherId>
        </OtherIdentifier>
      </com.csc_ItemIdInfo>
      <com.csc_QuoteInfo>
        <CompanysQuoteNumber com.csc_colind="ReadOnly"/>
        <IterationNumber com.csc_colind="ReadOnly"/>
        <QuotePreparedDt com.csc_colind="ReadOnly"/>
      </com.csc_QuoteInfo>
      <com.csc_BranchCd>
        <xsl:value-of select="normalize-space(//PIF__BRANCH)"/>
      </com.csc_BranchCd>
      <com.csc_ProfitCenter>
        <xsl:value-of select="normalize-space(//PIF__PROFIT__CENTER)"/>
      </com.csc_ProfitCenter>
      <com.csc_ContractTerm>
        <EffectiveDt com.csc_colind="ReadOnly">
          <xsl:call-template name="cvtdate">
            <xsl:with-param name="datefld" select="//PIF__EFFEC__DATE"/>
          </xsl:call-template>
        </EffectiveDt>
        <ExpirationDt com.csc_colind="ReadOnly">
          <xsl:call-template name="cvtdate">
            <xsl:with-param name="datefld" select="//PIF__EXPIR__DATE"/>
          </xsl:call-template>
        </ExpirationDt>
      </com.csc_ContractTerm>
      <com.csc_DataAffectedCd com.csc_colind="ReadOnly"/>
      <com.csc_ActivityDescCd com.csc_colind="ReadOnly">
        <xsl:call-template name="Show-Activity">
          <xsl:with-param name="activity" select="normalize-space(//PIF__TYPE__ACTIVITY)"/>
          <xsl:with-param name="status" select="normalize-space(//PIF__TRANS__STAT)"/>
        </xsl:call-template>
      </com.csc_ActivityDescCd>
      <com.csc_TransactionEffectiveDt com.csc_colind="ReadOnly"/>
      <com.csc_EnteredDt com.csc_colind="ReadOnly">
        <xsl:call-template name="cvtdate">
          <xsl:with-param name="datefld" select="//PIF__ENTERED__DATE"/>
        </xsl:call-template>
      </com.csc_EnteredDt>
      <com.csc_CancellationDt com.csc_colind="ReadOnly">
        <xsl:call-template name="cvtdate">
          <xsl:with-param name="datefld" select="//PIF__CANCL__DATE"/>
        </xsl:call-template>
      </com.csc_CancellationDt>
      <InsuredOrPrincipal>
        <ItemIdInfo>
          <AgencyId/>
          <CustPermId>
            <xsl:value-of select="normalize-space(//PIF__CUSTOMER__NUMBER)"/>
          </CustPermId>
          <com.csc_ClientSeqNum/>
          <com.csc_AddressSeqNum/>
        </ItemIdInfo>
        <GeneralPartyInfo>
          <NameInfo>
            <CommlName>
                <CommercialName>
                  <xsl:value-of select="normalize-space(//PIF__ADDRESS__LINE__1)"/>
               </CommercialName>
            </CommlName>
            <PersonName>
              <TitlePrefix/>
              <NameSuffix/>
              <Surname></Surname>
              <GivenName></GivenName>
              <OtherGivenName/>
              <NickName>
                <xsl:value-of select="normalize-space(//PIF__SORT__NAME)"/>
              </NickName>
            </PersonName>
            <LegalEntityCd/>
            <TaxIdentity></TaxIdentity>
            <BirthDt/>
            <NameType/>
          </NameInfo>
          <Addr>
            <AddrTypeCd>MailingAddress</AddrTypeCd>
            <Addr1>
              <xsl:value-of select="normalize-space(//PIF__ADDRESS__LINE__2)"/>
            </Addr1>
            <Addr2>
              <xsl:value-of select="normalize-space(//PIF__ADDRESS__LINE__3)"/>
            </Addr2>
            <City>
              <xsl:value-of select="normalize-space(substring(//PIF__ADDRESS__LINE__4,1,28))"/>
            </City>
            <StateProvCd>
              <xsl:value-of select="normalize-space(substring(//PIF__ADDRESS__LINE__4,29,2))"/>
            </StateProvCd>
            <PostalCode>
              <xsl:value-of select="normalize-space(//PIF__ZIP__POSTAL__CODE)"/>
            </PostalCode>
            <County/>
          </Addr>
        </GeneralPartyInfo>
        <InsuredOrPrincipalInfo>
          <InsuredOrPrincipalRoleCd>Insured</InsuredOrPrincipalRoleCd>
          <BusinessInfo>
            <SICCd/>
          </BusinessInfo>
        </InsuredOrPrincipalInfo>
      </InsuredOrPrincipal>
      <com.csc_Insurer>
        <GeneralPartyInfo>
          <NameInfo>
            <CommlName>
              <CommercialName/>
            </CommlName>
            <TaxIdentity/>
          </NameInfo>
          <Addr>
            <AddrTypeCd>MailingAddress</AddrTypeCd>
            <Addr1/>
            <City/>
            <StateProvCd/>
            <PostalCode/>
            <County/>
          </Addr>
        </GeneralPartyInfo>
      </com.csc_Insurer>
      <com.csc_LongName com.csc_colind="ReadOnly"/>
      <com.csc_UserId com.csc_colind="ReadOnly"/>
      <com.csc_RenewalCd>
        <xsl:value-of select="normalize-space(//PIF__RENEWAL__CODE)"/>
      </com.csc_RenewalCd>
      <com.csc_RenewalNumber>
        <xsl:value-of select="normalize-space(//PIF__RENEW__POLICY__NUMBER)"/>
      </com.csc_RenewalNumber>
      <com.csc_CancelRewriteCd com.csc_colind="ReadOnly"/>
      <com.csc_OOSInProcessInd/>
      <com.csc_TierRatingCd/>
      <com.csc_WIPOpenInd/>
      <com.csc_RestrictedAccessInd/>
      <com.csc_SupportDataDt com.csc_colind="ReadOnly"/>
      <com.csc_MultiObjectsInd/>
      <com.csc_ReasonAmdDesc>
        <xsl:value-of select="normalize-space(//PIF__REASON__AMEND__DESC)"/>
      </com.csc_ReasonAmdDesc>
      <com.csc_PayByModeDesc>
        <xsl:value-of select="normalize-space(//PIF__PAYBY__MODE__DESC)"/>
      </com.csc_PayByModeDesc>
      <com.csc_RenewPayByDesc>
        <xsl:value-of select="normalize-space(//PIF__RENEW__PAYBY__DESC)"/>
      </com.csc_RenewPayByDesc>
       <com.csc_PolicyCurCd com.csc_colind="ReadOnly">
          <xsl:value-of select="normalize-space(//PIF__POLICY__CURRENCY)"/>
        </com.csc_PolicyCurCd>
    </com.csc_PolicyLevel>
  </xsl:template>
  
  <xsl:template name="Show-Activity">
    <xsl:param name="activity"/>
    <xsl:param name="status"/>
    <xsl:choose>
      <xsl:when test="($status='V') and ($activity='CN')">
        <xsl:variable name="FinalVal">Cancelled</xsl:variable>
        <xsl:value-of select="$FinalVal"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
