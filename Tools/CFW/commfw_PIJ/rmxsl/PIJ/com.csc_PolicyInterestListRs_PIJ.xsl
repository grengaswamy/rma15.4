<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:include href="RMCommonTemplates.xsl"/>
  <xsl:include href="ConvertDateMMDDYYtoCYYMMDD.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYY.xsl"/>
  <xsl:include href="ConvertDateCYYMMDDtoMMDDYYYY.xsl"/>
  <xsl:output  method="xml" indent="yes" media-type="text\xml"/>
  <xsl:param name="user-id" select="default"/>
  <xsl:param name="session-id" select="default"/>
  <xsl:param name="app-org" select="default"/>
  <xsl:param name="app-name" select="default"/>
  <xsl:param name="app-version" select="default"/>
  <xsl:param name="rq-uid" select="default"/>
  <xsl:param name="client-file" select="default" />

  <xsl:template match="/">
    <ACORD>
      <xsl:call-template name="SignonInfo">
        <xsl:with-param name="user-id">
          <xsl:value-of select="normalize-space($user-id)"/>
        </xsl:with-param>
        <xsl:with-param name="session-id">
          <xsl:value-of select="normalize-space($session-id)"/>
        </xsl:with-param>
        <xsl:with-param name="app-org">
          <xsl:value-of select="normalize-space($app-org)"/>
        </xsl:with-param>
        <xsl:with-param name="app-name">
          <xsl:value-of select="normalize-space($app-name)"/>
        </xsl:with-param>
        <xsl:with-param name="app-version">
          <xsl:value-of select="normalize-space($app-version)"/>
        </xsl:with-param>
        <xsl:with-param name="client-file">
          <xsl:value-of select="normalize-space($client-file)"/>
        </xsl:with-param>
      </xsl:call-template>
      <ClaimsSvcRs>
        <Status>
          <StatusCd/>
          <StatusDesc/>
          <com.csc_ServerStatusCode/>
          <com.csc_Severity/>
        </Status>
        <RqUID>
          <xsl:value-of select="normalize-space($rq-uid)"/>
        </RqUID>
        <com.csc_PolicyInterestListRs>
          <RqUID/>
          <TransactionResponseDt/>
          <CurCd/>
          <MsgStatus>
            <MsgStatusCd/>
          </MsgStatus>
          <com.csc_PagingInfo>
            <Status>
              <StatusCd/>
            </Status>
            <com.csc_ListTypeCd/>
            <com.csc_RoutingInfo>RecordSeqNbr0</com.csc_RoutingInfo>
            <com.csc_ListCompleteInd>Y</com.csc_ListCompleteInd>
          </com.csc_PagingInfo>
          <xsl:apply-templates select="/POINTJDBCRs/Rows/Row" />
        </com.csc_PolicyInterestListRs>
      </ClaimsSvcRs>
    </ACORD>
  </xsl:template>

  <xsl:template match="/POINTJDBCRs/Rows/Row" >
    <xsl:if test="string-length(PI-INT-TYPE) > 0 and string-length(PI-NAME) > 0">
      <xsl:call-template name="InterestRow"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="InterestRow">
    <InsuredOrPrincipal>
      <ItemIdInfo>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_Location</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(PI-INT-LOCATION)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_SeqNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(PI-INT-SEQ)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_ClientSeqNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(PI-CLIENT-SEQ)"/>
          </OtherId>
        </OtherIdentifier>
        <OtherIdentifier>
          <OtherIdTypeCd>com.csc_AddressSeqNum</OtherIdTypeCd>
          <OtherId>
            <xsl:value-of select="normalize-space(PI-ADDRESS-SEQ)"/>
          </OtherId>
        </OtherIdentifier>
      </ItemIdInfo>
      <GeneralPartyInfo>
        <NameInfo>
          <CommlName>
            <CommercialName>
              <xsl:value-of select="normalize-space(PI-NAME )"/>
            </CommercialName>
          </CommlName>
          <PersonName>
            <TitlePrefix/>
            <NameSuffix/>
            <Surname/>
            <GivenName/>
            <OtherGivenName/>
            <NickName/>
          </PersonName>
          <LegalEntityCd></LegalEntityCd>
          <TaxIdentity/>
        </NameInfo>
        <Addr>
          <AddrTypeCd>MailingAddress</AddrTypeCd>
          <Addr1>
            <xsl:value-of select="normalize-space(PI-ADDRESS)"/>
          </Addr1>
          <City>
            <xsl:value-of select="normalize-space(PI-CITY)"/>
          </City>
          <StateProvCd>
            <xsl:value-of select="normalize-space(PI-STATE)"/>
          </StateProvCd>
          <PostalCode/>
          <County/>
        </Addr>
      </GeneralPartyInfo>
      <InsuredOrPrincipalInfo>
        <InsuredOrPrincipalRoleCd>
          <xsl:value-of select="normalize-space(PI-INT-TYPE)"/>
        </InsuredOrPrincipalRoleCd>
        <BusinessInfo>
          <SICCd></SICCd>
        </BusinessInfo>
      </InsuredOrPrincipalInfo>
    </InsuredOrPrincipal>
  </xsl:template>
</xsl:stylesheet>