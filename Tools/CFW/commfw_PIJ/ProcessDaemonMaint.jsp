<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- ------------------------------------------------------------------- --%>
<%-- This is a JSP to configure and maintain the Process Daemon.         --%>
<%-- ------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html"%>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="org.w3c.dom.Document" %>
<%@ page import="com.csc.cfw.process.SubscriberRegister" %>
<%@ page import="com.csc.cfw.process.ProcessDaemon" %>
<%@ page import="com.csc.cfw.util.CommFwSystem" %>
<%@ page import="com.csc.fw.util.Configurable" %>
<%@ page import="com.csc.fw.util.FwException" %>
<%@ page import="com.csc.fw.util.FwSystem" %>
<%@ page import="com.csc.fw.util.PropertyFileManager" %>
<%@ page import="com.csc.fw.util.ServletUtil" %>
<%@ page import="com.csc.fw.util.WebServiceFactory" %>
<%@ page import="com.csc.fw.util.XMLDocument" %>
<%@ page import="com.csc.fw.util.XMLFileConfigurator" %>
<%@ page errorPage="error.jsp" %>

<HTML>
  <LINK href="includes/INFUtil.css" type="text/css" rel="stylesheet">
  <HEAD>
    <TITLE>Process Daemon Maintenance - Communications Framework </TITLE>
  </HEAD>
  <BODY BGCOLOR="#C0C0C0">
    
    <script language="JavaScript">
    var lastSearchPos = 0;
    var lastSearchText = "";
    /*----------------------------------------------------*/
    /* Find a text string in a text area                  */
    /*----------------------------------------------------*/
    function findTextAreaString(searchForm, searchTextArea)
    {
        var searchText = "";
        if (searchForm.searchTextInput)
            searchText = searchForm.searchTextInput.value;
        else
            searchText = prompt("Please enter search text:",lastSearchText);
        if (!searchText)
        {
            alert("No search text specified");
            return;
        }
        if (searchText != lastSearchText)
        {
            lastSearchText = searchText;
            lastSearchPos = 0;
        }
        var data = searchTextArea.value;
        searchText = searchText.toLowerCase();
        data = data.toLowerCase();
        var datasubstr = data.substring(lastSearchPos);
        var pos = datasubstr.search(searchText);
        if (pos < 0)
        {
            alert("Search text not found");
            lastSearchPos = 0;
            return;
        }
        pos += lastSearchPos;
        var count = 0;
        for (var i = 0; i < pos; i++)
        {
            if (data.charAt(i) == '\r') 
                count++; 
        }
        if (searchTextArea.createTextRange)
        {
            var rng = searchTextArea.createTextRange();
            if (rng)
            {
                //alert("Current search Pos: " + pos);
                if (false) //if (false == rng.findText(searchText,lastSearchPos))
                    alert("Search text not found");
                else
                {
                    rng.moveStart("character", pos - count);
                    rng.collapse();
                    rng.select();
                }
            }
            
        }
        lastSearchPos = pos + searchText.length;
    }
    </script>

    <DIV ALIGN="Center" ID="HtmlDiv1">
    <TABLE>
      <TR>
        <TD ALIGN="Center"  >
          <H4>Process Daemon Maintenance</H4>
        </TD>
      </TR>
      <TR>
        <TD  >

<%
    FwSystem fwSys = CommFwSystem.current();
    String updateTextBuffer = null;
    // Control File Section - Determine the section to display
    XMLFileConfigurator xmlConfig = new XMLFileConfigurator(fwSys, request);
    boolean bIE = (ServletUtil.determineBrowserVersion(request).getType() == ServletUtil.BROWSER_MSIE);
    
    /****************************************/
    /* Parameters for Process Daemon Config */
    /****************************************/
    String daemonFileName = ProcessDaemon.configFile();
    List daemonFileList = ProcessDaemon.configFileList(null);
    if (daemonFileList == null)
        daemonFileList = new ArrayList();
    if (daemonFileList.size() == 0)
        daemonFileList.add(daemonFileName);
    String daemonPropFile = request.getParameter("daemonPropFile");
    if (daemonPropFile != null)
        daemonFileName = daemonPropFile;
    else
    {
        /* Default the filename to ProcessDaemon&#x25;APPLICATION&#x25;.xml */
        String daemonFileDefault = daemonFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && daemonFileDefault.endsWith(sfx))
        {
            daemonFileDefault = daemonFileDefault.substring(0, daemonFileDefault.length() - sfx.length());
            daemonFileDefault += app + sfx;
        }
        for (int i = 0; i < daemonFileList.size(); i++)
        {
            String s = daemonFileList.get(i).toString();
            if (daemonFileDefault.equals(s))
            {
                daemonFileName = daemonFileDefault;
                break;
            }
        }
    }
    
    String daemonFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, daemonFileName);
    String errorMessageDaemon = null;
    String ronlyBuffDaemon = null;
    boolean ronlyDaemon = false;
    if (daemonFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(daemonFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffDaemon = sw.getBuffer().toString();
            ronlyDaemon = true;
        }
    }

	
    /*********************************************/
    /* Parameters for Subscriber Register Config */
    /*********************************************/
    List subRegFileList = SubscriberRegister.configFileList(null);
    String subRegFileName = SubscriberRegister.configFile();
    if (subRegFileList == null)
    	subRegFileList = new ArrayList();
    if (subRegFileList.size() == 0)
        subRegFileList.add(subRegFileName);
    String subRegPropFile = request.getParameter("subRegPropFile");
    if (subRegPropFile != null)
        subRegFileName = subRegPropFile;
    else
    {
        /* Default the filename to SubscriberRegister&#x25;APPLICATION&#x25;.xml */
        String subRegFileDefault = subRegFileName;
        String sfx = ".xml";
        String app = (String)fwSys.getProperty(CommFwSystem.SYSPROP_APPLICATION);
        if (app != null && subRegFileDefault.endsWith(sfx))
        {
            subRegFileDefault = subRegFileDefault.substring(0, subRegFileDefault.length() - sfx.length());
            subRegFileDefault += app + sfx;
        }
        for (int i = 0; i < subRegFileList.size(); i++)
        {
            String s = subRegFileList.get(i).toString();
            if (subRegFileDefault.equals(s))
            {
                subRegFileName = subRegFileDefault;
                break;
            }
        }
    }
    
    String subRegFilePath = ServletUtil.fullFilePath(getServletConfig().getServletContext(), request, subRegFileName);
    String errorMessageSubReg = null;
    String ronlyBuffSubReg = null;
    boolean ronlySubReg = false;
    if (subRegFilePath == null)
    {
        // The file could not be found.  See if the classloader can find the file.
        BufferedReader rdr = FwSystem.getResourceAsReader(subRegFileName);
        if (rdr != null)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for (String line = rdr.readLine(); line != null; line = rdr.readLine())
            {
                pw.println(line);
            }
            
            pw.close();
            ronlyBuffSubReg = sw.getBuffer().toString();
            ronlySubReg = true;
        }
    }

	
    /**********************************/
    /* Check functions being executed */
    /**********************************/
    String func = request.getParameter("function");
    if (func != null && func.length() > 0)
    {
        if ("Shutdown Process Daemons".equalsIgnoreCase(func))
        {
            ProcessDaemon.shutdownProcesses(false);
        }
        else
        if ("Restart Process Daemons".equalsIgnoreCase(func))
        {
            ProcessDaemon.restartInactiveProcesses();
        }
        else
        if ("Suspend".equalsIgnoreCase(func) || "Resume".equalsIgnoreCase(func))
        {
        	try
        	{
                int idx = Integer.parseInt(request.getParameter("suspendIdx").toString());
                Object[] configList = ProcessDaemon.configEntryKeys(null);
                String name = configList[idx].toString();
                ProcessDaemon daemon = (ProcessDaemon)ProcessDaemon.configEntryMap(null, null).get(name);
                daemon.setSuspended("Suspend".equalsIgnoreCase(func));
        	}
        	catch (Exception e) { }
        }
        else
        if ("Update Process Daemon File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("DaemonConfigTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        daemonFilePath,
                        daemonFileName,
                        "ProcessDaemon",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageDaemon = "<FONT COLOR='#FF0000'><B>Updated Process Daemon XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageDaemon == null)
                try
                {
                    PropertyFileManager.writePropertyFile(daemonFilePath, updateTextBuffer);
                    ProcessDaemon.configSetFileCache(daemonFileName, updateTextBuffer);
                    ProcessDaemon.initializeDaemonConfig(null, daemonFileName, new File(daemonFilePath).getParent(), true);
                }
                catch (IOException e)
                {
                    errorMessageDaemon = "<FONT COLOR='#FF0000'><B>Error updating Process Daemon configuration file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
        else
        if ("Update Subscriber Register File".equalsIgnoreCase(func))
        {
            updateTextBuffer = request.getParameter("SubRegConfigTextArea");
            try
            {
                Document newDoc = XMLDocument.parseDoc(updateTextBuffer);
                xmlConfig.logXMLChanges(
                        fwSys,
                        newDoc, 
                        updateTextBuffer,
                        subRegFilePath,
                        subRegFileName,
                        "SubscriberRegister",
                        "name",
                        true,
                        false,
                        null);
            }
            catch (FwException e)
            {
                errorMessageSubReg = "<FONT COLOR='#FF0000'><B>Updated Subscriber Register XML text is not well formed and was not saved.</B></FONT>";
            }
            if (errorMessageSubReg == null)
                try
                {
                    PropertyFileManager.writePropertyFile(subRegFilePath, updateTextBuffer);
                    SubscriberRegister.configSetFileCache(subRegFileName, updateTextBuffer);
                }
                catch (IOException e)
                {
                    errorMessageSubReg = "<FONT COLOR='#FF0000'><B>Error updating Subscriber Register configuration file:<BR />&nbsp;&nbsp;&nbsp; " + e.toString() + "</B></FONT>";
                }
        }
    }
%>
          <P />
          <TABLE>
            <TR ALIGN="Center">
             <TD ALIGN="Center"  >
              <TABLE>

                <TR>
                  <TD WIDTH="100%"  >
                    <TABLE cellspacing="0" WIDTH="100%">
                      <TR>
                        <TD COLSPAN="4"><HR WIDTH="100%"></TD>
                      </TR>
                      
                      <!-- Start Display List of Daemons -->
                      <TR>
                        <TD WIDTH="100%">
                          <B>Process Daemons:</B><BR/>
                          <table class="PAGEHLIGHT" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <th align="left" width="20%">Process Name</th>
                              <th align="left" width="20%">Status</th>
                              <th align="right" width="10%">Count</th>
                              <th align="right" width="10%">Interval&nbsp;&nbsp;</th>
                              <th align="left" width="20%">Last Exec Time</th>
                              <th align="left" width="20%">Suspend</th>
                            </tr>
<%
				String colorClass = null;
				Object[] configList = ProcessDaemon.configEntryKeys(null);
				for (int i = 0; i < configList.length; i++)
				{
					String name = configList[i].toString();
					ProcessDaemon daemon = (ProcessDaemon)ProcessDaemon.configEntryMap(null, null).get(name);
					String status = daemon.getStateDescription();
					Long count = new Long(daemon.getExecutionCount());
					String lastExecTime = daemon.getLastExecutionTime();
					colorClass = "SRE";
%>
                            <tr>
                              <td class="DARKBACK" width="100%" colspan="6"><img src="includes/spacer.gif" width="100%" height="1px"/></td>
                            </tr>
                            <tr>
                              <td align="left" width="20%" valign="middle"><b><%=name%></b></td>
                              <td align="left" width="20%" valign="middle"><b><%=status%></b></td>
                              <td align="right" width="10%" valign="middle"><b><%=count%></b></td>
                              <td align="right" width="10%" valign="middle"><b><%=daemon.getInterval()%>&nbsp;&nbsp;</b></td>
                              <td align="left" width="20%" valign="middle"><b><%=lastExecTime%></b></td>
                              <td align="left" width="20%" valign="bottom">
                               <form action="ProcessDaemonMaint.jsp" name="SuspendDaemonForm<%= i %>" method="POST" >
                                <input type="hidden" name="suspendIdx" value="<%=i %>" />
<%
					if (daemon.getState() != ProcessDaemon.STATE_ID_DISABLED)
					{
						if (daemon.isSuspended())
						{
%>
                                <input type="submit" name="function" value="Resume" />
<%
						}
						else
						{
%>
                                <input type="submit" name="function" value="Suspend" />
<%
						}
					}
					else
					{
%>
                                &nbsp;
<%
					}
%>
                               </form>
                              </td>
                            </tr>
<%				}
%>                        </table>
                        </TD>
                      </TR>
                      <!--  End Display List of Daemons -->
                      
                    </TABLE>
                  </TD>
                </TR>
                <TR>
                  <TD COLSPAN="3"><HR WIDTH="100%"></TD>
                </TR>

                <!-- Process Daemon Log -->
                <tr>
                  <td>
                   <div align="center">
                    <b>Process Daemon Log Display</b><br />
                    <textarea cols="80" wrap="OFF" rows="25"><%= ProcessDaemon.getDisplayLogContents()%></textarea>
                   </div>
                   <br />
                   <form name="ProcessDaemonAdminForm" action="ProcessDaemonMaint.jsp" method="POST" >
                     <input class="btnNoSize" type="submit" name="function" value="Shutdown Process Daemons" />
                     <input class="btnNoSize" type="submit" name="function" value="Restart Process Daemons" />
                   </form>
                  </td>
                </tr>

<%
		// Begin Process Daemon Configuration Section
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgDaemonForm" ACTION="ProcessDaemonMaint.jsp">
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Process Daemon Configuration</FONT></FONT></B>
                              </TD>
                            </TR>

                            <TR>
                              <TD>
              <%
				if (daemonFileList.size() > 1)
				{
              %>
                               <SELECT NAME="daemonPropFile" SIZE="1" onChange="document.CfwCfgDaemonForm.submit()">
              <%
						for (int i = 0; i < daemonFileList.size(); i++)
						{
							String filename = (String)daemonFileList.get(i);
              %>
                                 <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(daemonFileName,filename) %>><%=filename%></OPTION>
              <%
						}
              %>
                               </SELECT>
              <%
				}
              %>
                              </TD>
                            </TR>

<%
		if (daemonFilePath != null || ronlyDaemon)
		{
			if (daemonFilePath != null)
			{
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT"  readonly="readonly"  value="<%=daemonFilePath%>" title="<%=daemonFilePath%>"></TD>
                            </TR>
<%
			}
			
			if (errorMessageDaemon != null)
			{
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageDaemon %><BR />
                              </TD>
                            </TR>
<%		
			}
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="80" WRAP="OFF" ROWS="20" NAME="DaemonConfigTextArea">
<%
			if (errorMessageDaemon != null)
			{
%><%=XMLDocument.encodeXml(updateTextBuffer, false)%><%
			}
			else
			if (ronlyDaemon)
			{
%><%=XMLDocument.encodeXml(ronlyBuffDaemon, false)%><%
			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(daemonFilePath, writer);
%><%=XMLDocument.encodeXml(sw.toString(), false)%><%
			}
%></TEXTAREA>
                              </TD>
                            </TR>

<%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgDaemonForm, document.CfwCfgDaemonForm.DaemonConfigTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>
 
                            <TR>
                              <TD ALIGN="Left"  >
                                <b>NOTE:</b> Changes made to this file only take effect when the
                                Communications Framework application is restarted.<br/>
                                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;
<%
			if (!ronlyDaemon)
			{
%>
                                &nbsp;<input class="btnNoSize" value="Update Process Daemon File" type="submit" name="function">
<%
			}
%>
                              </TD>
                            </TR>
<%
		}
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
			// End Process Daemon Configuration Section
			
		// Begin Subscriber Register Configuration Section
%>
                <TR>
                  <TD WIDTH="100%"  >
                   <FORM METHOD="POST" NAME="CfwCfgSubRegForm" ACTION="ProcessDaemonMaint.jsp">
                    <TABLE BORDER="2" WIDTH="100%">
                      <TR>
                        <TD WIDTH="100%"  >
                          <TABLE WIDTH="100%">
                            <TR>
                              <TD BGCOLOR="#0000FF" WIDTH="100%"  ><B><FONT SIZE="3"><FONT COLOR="#FFFFFF">Subscriber Register Configuration</FONT></FONT></B>
                              </TD>
                            </TR>

                            <TR>
                              <TD>
              <%
				if (subRegFileList.size() > 1)
				{
              %>
                               <SELECT NAME="subRegPropFile" SIZE="1" onChange="document.CfwCfgSubRegForm.submit()">
              <%
						for (int i = 0; i < subRegFileList.size(); i++)
						{
							String filename = (String)subRegFileList.get(i);
              %>
                                 <OPTION VALUE="<%=filename%>"  <%=ServletUtil.setSelected(subRegFileName,filename) %>><%=filename%></OPTION>
              <%
						}
              %>
                               </SELECT>
              <%
				}
              %>
                              </TD>
                            </TR>

<%
		if (subRegFilePath != null || ronlySubReg)
		{
			if (subRegFilePath != null)
			{
%>
                            <TR>
                              <TD><B>Path:</B> <input size="80" class="DISPLAY-ONLY-INPUT"  readonly="readonly"  value="<%=subRegFilePath%>" title="<%=subRegFilePath%>"></TD>
                            </TR>
<%
			}
			
			if (errorMessageSubReg != null)
			{
%>
                            <TR>
                              <TD WIDTH="100%"  >
                                <%=errorMessageSubReg %><BR />
                              </TD>
                            </TR>
<%		
			}
%>

                            <TR>
                              <TD WIDTH="100%"  >
                                <TEXTAREA COLS="80" WRAP="OFF" ROWS="20" NAME="SubRegConfigTextArea">
<%
			if (errorMessageSubReg != null)
			{
%><%=XMLDocument.encodeXml(updateTextBuffer, false)%><%
			}
			else
			if (ronlySubReg)
			{
%><%=XMLDocument.encodeXml(ronlyBuffSubReg, false)%><%
			}
			else
			{
				StringWriter sw = new StringWriter();
				PrintWriter writer = new PrintWriter(sw);
				PropertyFileManager.readPropertyFile(subRegFilePath, writer);
%><%=XMLDocument.encodeXml(sw.toString(), false)%><%
			}
%></TEXTAREA>
                              </TD>
                            </TR>

<%			if (bIE) {   %>
                             <TR>
                              <TD>
                                <TABLE width="100%" cellspacing="0" cellpadding="0">
                                  <TR>
                                    <TD>
                                      <A href="javascript:findTextAreaString(document.CfwCfgsubRegForm, document.CfwCfgSubRegForm.SubRegConfigTextArea);">Search:</A>
                                      &nbsp;<input type="text" name="searchTextInput" />
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
<%			} /* if (bIE) */   %>
 
                            <TR>
                              <TD ALIGN="Left"  >
                                <b>NOTE:</b> Changes made to this file only take effect when the
                                Communications Framework application is restarted.<br/>
                                <input class="btnNoSize" value="Reset" type="reset">
                                &nbsp;
<%
			if (!ronlySubReg)
			{
%>
                                &nbsp;<input class="btnNoSize" value="Update Subscriber Register File" type="submit" name="function">
<%
			}
%>
                              </TD>
                            </TR>
<%
		}
%>
                          </TABLE>
                        </TD>
                      </TR>
                    </TABLE>
                   </FORM>
                  </TD>
                </TR>
<%
			// End Subscriber Register Configuration Section
%>


              </TABLE>
              </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
    </TABLE>

    </DIV>
  </BODY>
</HTML>
