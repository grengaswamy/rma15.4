--DROPPING EXISTING TABLES
DROP TABLE ASYNC_TRACKER
DROP TABLE CUSTOMIZE
DROP TABLE SESSION_IDS
DROP TABLE SESSIONS_X_BINARY
DROP TABLE SESSIONS
GO

/****** Object:  UserDefinedFunction [dbo].[UDF_PROCESSDATE]    Script Date: 11/30/2007 13:03:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[UDF_PROCESSDATE]') AND xtype in (N'FN', N'IF', N'TF'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [UDF_PROCESSDATE] (@LASTCHANGE varchar(14))
RETURNS datetime
AS
BEGIN
		--DECLARE ALL REQUIRE VARIABLES FOR PROCESSING THE DATE STRING
		DECLARE @LASTCHANGEDATE varchar(50),
		@YEAR varchar(4),
		@MONTH varchar(2),
		@DAY varchar(2),
		@HOUR varchar(2),
		@MIN varchar(2),
		@SECONDS varchar(2),
		@SESSIONDATE datetime

	--SET UP ALL THE REQUIRED VALUES FOR CONSTRUCTING THE PROPER DATETIME STRING
	SET @YEAR = SUBSTRING(@LASTCHANGE, 1, 4)
	SET @MONTH = SUBSTRING(@LASTCHANGE, 5,2)
	SET @DAY = SUBSTRING(@LASTCHANGE, 7,2)
	SET @HOUR = SUBSTRING(@LASTCHANGE, 9,2)
	SET @MIN = SUBSTRING(@LASTCHANGE, 11,2)
	SET @SECONDS = SUBSTRING(@LASTCHANGE, 13,2)

	--CHECK IF THE SECONDS FIELD IS EMPTY TO PROVIDE
	--BACKWARDS COMPATIBILITY WITH THE OLDER WEBFARMSESSION DATABASE
	IF @SECONDS = ''''
		BEGIN
			SET @SECONDS = ''00''
		END

	--CONCATENATE THE DATE TIME STRING SO THAT IT CAN BE PROPERLY CAST TO A DATE TIME DATA TYPE
	SET @LASTCHANGEDATE = @YEAR + ''-'' + @MONTH + ''-'' + @DAY + '' '' + @HOUR + '':'' + @MIN + '':'' + @SECONDS + ''.000''

	SET @SESSIONDATE = CAST(@LASTCHANGEDATE AS DATETIME)
    RETURN(@SESSIONDATE)
END
' 
END
GO
/****** Object:  Table [dbo].[ASYNC_TRACKER]    Script Date: 11/30/2007 13:03:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[ASYNC_TRACKER]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [ASYNC_TRACKER](
	[ASYNCID] [varchar](50) NOT NULL,
	[ASYNCSTATUS] [int] NULL,
	[ASYNCSTATE] [image] NULL,
	[PERCENTAGECOMPLETION] [float] NULL,
 CONSTRAINT [PK_ASYNC_TRACKER] PRIMARY KEY CLUSTERED 
(
	[ASYNCID] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SESSION_IDS]    Script Date: 11/30/2007 13:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[SESSION_IDS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [SESSION_IDS](
	[SYSTEM_TABLE_NAME] [char](80) NOT NULL,
	[NEXT_UNIQUE_ID] [int] NOT NULL,
 CONSTRAINT [PK_SESSION_IDS] PRIMARY KEY CLUSTERED 
(
	[SYSTEM_TABLE_NAME] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CUSTOMIZE]    Script Date: 11/30/2007 13:03:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CUSTOMIZE]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [CUSTOMIZE](
	[ID] [int] NOT NULL,
	[FOLDER] [varchar](100) NULL,
	[FILENAME] [varchar](100) NOT NULL,
	[TYPE] [tinyint] NULL,
	[IS_BINARY] [bit] NULL,
	[CONTENT] [text] NULL,
 CONSTRAINT [PK_CUSTOMIZE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SESSIONS]    Script Date: 11/30/2007 13:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[SESSIONS]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [SESSIONS]
(
	[SID] [varchar](40) NOT NULL,
	[SESSION_ROW_ID] [int] NOT NULL,
	[LASTCHANGE] [varchar](14) NULL,
	[DATA] [text] NULL,
	[USERDATA1] [int] NULL,
	[SEARCH_XML] [text] NULL
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SESSIONS_X_BINARY]    Script Date: 11/30/2007 13:03:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[SESSIONS_X_BINARY]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [SESSIONS_X_BINARY]
(
	[SESSION_X_BIN_ROW_ID] [int] NOT NULL,
	[SESSION_ROW_ID] [int] NULL,
	[BINARY_TYPE] [varchar](100) NULL,
	[BINARY_NAME] [varchar](100) NULL,
	[BINARY_VALUE] [image] NULL
)
END
GO
SET ANSI_PADDING OFF
GO

--INSERT DATA INTO THE SESSION DATABASE

--INSERT SESSION IDS
INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('SESSIONS_X_BINARY',1)
INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('SESSIONS',1)
INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('CUSTOMIZE',1)

-- BEGINNING TRANSACTION DATA
PRINT 'Beginning transaction DATA'
BEGIN TRANSACTION _DATA_
GO

SET NOCOUNT ON
GO

-- Deleting from table: CUSTOMIZE
PRINT 'Deleting from table: CUSTOMIZE'
DELETE FROM [dbo].[CUSTOMIZE]

-- Insert scripts for table: CUSTOMIZE
PRINT 'Inserting rows into table: CUSTOMIZE'
IF NOT EXISTS(SELECT * FROM [dbo].[CUSTOMIZE] WHERE [ID]=0)
	INSERT INTO [dbo].[CUSTOMIZE] ([ID], [FOLDER], [FILENAME], [TYPE], [IS_BINARY], [CONTENT]) VALUES (0, NULL, 'customize_captions', 0, 0, '<RMAdminSettings title="System Customization">
		<Captions title="Captions/Messages">
			<CompanyName default="***" title="Company Name:">THE COMPANY</CompanyName>
			<AppTitle default="Riskmaster.Net" title="Application Title:">Riskmaster.Net</AppTitle>
			<ReportAppTitle default="Sortmaster" title="Application Title:">Sortmaster</ReportAppTitle>
			<AppCopyright default=") 2004 by CSC, All Rights Reserved." title="Application Title:">) 2004 by CSC, All Rights Reserved.</AppCopyright>
			<ErrContact default="For assistance please consult the System Administrator." title="Contact on Error:">For assistance please consult the System Administrator.</ErrContact>
			<AlertExistingRec default="You are working on a new record and this functionality is available for existing records only. Please save the data and try again." title="Alert Existing Record Required:">You are working on a new record and this functionality is available for existing records only. Please save the data and try again.</AlertExistingRec>
		</Captions>
		<Paths title="Paths">
			<OverrideDocPath default="***" title="Override Documents Path:"></OverrideDocPath>
		</Paths>
	</RMAdminSettings>
</customize_captions>')
IF NOT EXISTS(SELECT * FROM [dbo].[CUSTOMIZE] WHERE [ID]=1)
	INSERT INTO [dbo].[CUSTOMIZE] ([ID], [FOLDER], [FILENAME], [TYPE], [IS_BINARY], [CONTENT]) VALUES (1, NULL, 'customize_settings', 0, 0, '<customize_settings>
	<RMAdminSettings title="System Customization">
		<TextAreaSize title="Text Area Size">
			<TextML title="TextMl">
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</TextML>
			<FreeCode title="Freecode">
				<Width default="30">30</Width>
				<Height default="8">8</Height>
			</FreeCode>
			<ReadOnlyMemo title="Readonly Memo">
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</ReadOnlyMemo>
			<Memo title="Memo">
				<Width default="30">30</Width>
				<Height default="5">5</Height>
			</Memo>
		</TextAreaSize>
		<Buttons title="Buttons">
			<Document title="Documents">-1</Document>
			<Search title="Search">-1</Search>
			<File title="Files">-1</File>
			<Diary title="Diaries">-1</Diary>
			<Report title="Reports">-1</Report>
		</Buttons>
		<Other>
			<Soundex title="Choose Soundex on Searches">0</Soundex>
			<ShowName title="Show User Name on Menu">-1</ShowName>
			<ShowLogin title="Show Login on Menu">-1</ShowLogin>
			<SaveShowActiveDiary title="Save Show Active Diary">-1</SaveShowActiveDiary>
		</Other>
		<Funds title="Funds Page Settings">
			<OFAC title="OFAC Check:">-1</OFAC>
		</Funds>
	</RMAdminSettings>
</customize_settings>')
IF NOT EXISTS(SELECT * FROM [dbo].[CUSTOMIZE] WHERE [ID]=2)
	INSERT INTO [dbo].[CUSTOMIZE] ([ID], [FOLDER], [FILENAME], [TYPE], [IS_BINARY], [CONTENT]) VALUES (2, NULL, 'customize_reports', 0, 0, '<customize_reports>
				<RMAdminSettings title="System Customization">
					<ReportEmail title="Report Email Settings">
						<From title="Override From:"/>
						<FromAddr title="Override From Address:"/>
					</ReportEmail>
					<ReportMenu title="Report Menu Links">
						<BILabel title="Business Intelligence" default="Business Intelligence" value="-1" />
						<ReportLabel title="Std Reports Queue" default="Std Reports Queue" value="-1">
							<AvlReports title="Available Reports:">-1</AvlReports>
							<JobQueue title="Job Queue:">-1</JobQueue>
							<NewReport title="Post New Report:">-1</NewReport>
							<DeleteReport title="Delete Report:">-1</DeleteReport>
							<ScheduleReport title="Schedule Reports:">-1</ScheduleReport>
							<ViewSchedReport title="View Scheduled Reports:">-1</ViewSchedReport>
						</ReportLabel>
						<SMLabel title="Std Reports Designers" default="Std Reports Designers" value="-1">
							<Designer title="Designer:">-1</Designer>
							<DraftReport title="Draft Reports:">-1</DraftReport>
							<PostDraftRpt title="Post Draft Reports:">-1</PostDraftRpt>
						</SMLabel>
						<ExecSummLabel title="Exec. Summary" default="Exec. Summary" value="-1">
							<Configuration title="Configuration:">-1</Configuration>
							<Claim title="Claim:">-1</Claim>
							<Event title="Event:">-1</Event>
						</ExecSummLabel>
						<OtherLabel title="Other Reports" default="Other Reports" value="-1">
							<OSHA300 title="OSHA 200:">-1</OSHA300>
							<OSHA301 title="OSHA 301:">-1</OSHA301>
							<OSHA300A title="OSHA 300A:">-1</OSHA300A>
							<OSHASharpsLog title="OSHA Sharps Log:">-1</OSHASharpsLog>
							<DCCLabel title="DCC" default="DCC">-1</DCCLabel>
						</OtherLabel>
						<ReportQueue title="Report Queue Buttons">
							<Archive title="OSHA 200:">-1</Archive>
							<Email title="OSHA 200:">-1</Email>
							<Delete title="OSHA 200:">-1</Delete>
						</ReportQueue>
					</ReportMenu>
				</RMAdminSettings>
			</customize_reports>')
IF NOT EXISTS(SELECT * FROM [dbo].[CUSTOMIZE] WHERE [ID]=3)
	INSERT INTO [dbo].[CUSTOMIZE] ([ID], [FOLDER], [FILENAME], [TYPE], [IS_BINARY], [CONTENT]) VALUES (3, NULL, 'customize_search', 0, 0, '<customize_search>
	<RMAdminSettings title="System Customization">
		<Search title="Search Links">
			<Claim title="Claims">-1</Claim>
			<Event title="Events">-1</Event>
			<Employee title="Employees">-1</Employee>
			<Entity title="Entities">-1</Entity>
			<Vehicle title="Vehicles">-1</Vehicle>
			<Policy title="Policies">-1</Policy>
			<Fund title="Funds">-1</Fund>
			<Patient title="Patients">-1</Patient>
			<Physician title="Physicians">-1</Physician>
			<LeavePlan title="LeavePlans">-1</LeavePlan>
		</Search>
	</RMAdminSettings>
</customize_search>')


-- COMMITTING TRANSACTION DATA
PRINT 'Committing transaction DATA'
IF @@TRANCOUNT>0
	COMMIT TRANSACTION _DATA_
GO

SET NOEXEC OFF
GO