if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUSTOMIZE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CUSTOMIZE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SESSIONS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SESSIONS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SESSIONS_X_BINARY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SESSIONS_X_BINARY]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SESSION_IDS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SESSION_IDS]
GO

CREATE TABLE [dbo].[CUSTOMIZE] (
	[ID] [int] NOT NULL ,
	[FOLDER] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILENAME] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TYPE] [tinyint] NULL ,
	[IS_BINARY] [bit] NULL ,
	[CONTENT] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[SESSIONS] (
	[SID] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LASTCHANGE] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DATA] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[USERDATA1] [int] NULL ,
	[SEARCH_XML] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SESSION_ROW_ID] [int] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[SESSIONS_X_BINARY] (
	[SESSION_X_BIN_ROW_ID] [int] NOT NULL ,
	[SESSION_ROW_ID] [int] NOT NULL ,
	[BINARY_TYPE] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BINARY_NAME] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BINARY_VALUE] [image] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[SESSION_IDS] (
	[SYSTEM_TABLE_NAME] [char] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NEXT_UNIQUE_ID] [int] NOT NULL 
) ON [PRIMARY]
INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('SESSIONS_X_BINARY',2)
INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('SESSIONS',2)
INSERT INTO SESSION_IDS (SYSTEM_TABLE_NAME,NEXT_UNIQUE_ID) VALUES ('CUSTOMIZE',2)

GO
-- Script generated on 1/4/2001 3:58 PM

BEGIN TRANSACTION            
  DECLARE @JobID BINARY(16)  
  DECLARE @ReturnCode INT    
  SELECT @ReturnCode = 0     
IF (SELECT COUNT(*) FROM msdb.dbo.syscategories WHERE name = N'[Uncategorized (Local)]') < 1 
  EXECUTE msdb.dbo.sp_add_category @name = N'[Uncategorized (Local)]'

  -- Delete the job with the same name (if it exists)
  SELECT @JobID = job_id     
  FROM   msdb.dbo.sysjobs    
  WHERE (name = N'Expire Web Session')       
  IF (@JobID IS NOT NULL)    
  BEGIN  
  -- Check if the job is a multi-server job  
  IF (EXISTS (SELECT  * 
              FROM    msdb.dbo.sysjobservers 
              WHERE   (job_id = @JobID) AND (server_id <> 0))) 
  BEGIN 
    -- There is, so abort the script 
    RAISERROR (N'Unable to import job ''Expire Web Session'' since there is already a multi-server job with this name.', 16, 1) 
    GOTO QuitWithRollback  
  END 
  ELSE 
    -- Delete the [local] job 
    EXECUTE msdb.dbo.sp_delete_job @job_name = N'Expire Web Session' 
    SELECT @JobID = NULL
  END 

BEGIN 

  -- Add the job
  EXECUTE @ReturnCode = msdb.dbo.sp_add_job @job_id = @JobID OUTPUT , @job_name = N'Expire Web Session', @owner_login_name = N'sa', @description = N'No description available.', @category_name = N'[Uncategorized (Local)]', @enabled = 1, @notify_level_email = 0, @notify_level_page = 0, @notify_level_netsend = 0, @notify_level_eventlog = 2, @delete_level= 0
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the job steps
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Clean Session Older that 1 hour', @command = N'DECLARE @string_today varchar(12)
DECLARE @stmp varchar(12)
SET @stmp=LTRIM(STR(DATEPART(yyyy, GETDATE())))
SET @string_today=@stmp

SET @stmp=LTRIM(STR(DATEPART(mm, GETDATE())))
IF LEN(@stmp)=1
   SET @stmp=''0''+@stmp
SET @string_today=@string_today+@stmp

SET @stmp=LTRIM(STR(DATEPART(dd, GETDATE())))
IF LEN(@stmp)=1
   SET @stmp=''0''+@stmp
SET @string_today=@string_today+@stmp

SET @stmp=LTRIM(STR(DATEPART(hh, GETDATE())))
IF LEN(@stmp)=1
   SET @stmp=''0''+@stmp
SET @string_today=@string_today+@stmp

SET @stmp=LTRIM(STR(DATEPART(mi, GETDATE())))
IF LEN(@stmp)=1
   SET @stmp=''0''+@stmp
SET @string_today=@string_today+@stmp



SET @stmp=LTRIM(STR(DATEPART(ss, GETDATE())))
IF LEN(@stmp)=1
   SET @stmp=''0''+@stmp
SET @string_today=@string_today+@stmp

DELETE FROM SESSIONS_X_BINARY WHERE SESSION_ROW_ID IN (SELECT SESSION_ROW_ID FROM SESSIONS WHERE CAST(@string_today AS NUMERIC)  - CAST(LASTCHANGE AS NUMERIC) >= 360 OR DATA IS NULL)
DELETE FROM SESSIONS WHERE CAST(@string_today AS NUMERIC)  - CAST(LASTCHANGE AS NUMERIC) >= 360 OR DATA IS NULL', @database_name = N'WebFarmSession', @server = N'', @database_user_name = N'', @subsystem = N'TSQL', @cmdexec_success_code = 0, @flags = 0, @retry_attempts = 0, @retry_interval = 1, @output_file_name = N'', @on_success_step_id = 0, @on_success_action = 1, @on_fail_step_id = 0, @on_fail_action = 2
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 
  EXECUTE @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1 

  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the job schedules
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id = @JobID, @name = N'Expire Web Session', @enabled = 1, @freq_type = 4, @active_start_date = 20000427, @active_start_time = 0, @freq_interval = 1, @freq_subday_type = 4, @freq_subday_interval = 10, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_end_date = 99991231, @active_end_time = 235959
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the Target Servers
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'(local)' 
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

END
COMMIT TRANSACTION          
GOTO   EndSave              
QuitWithRollback:
  IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION 
EndSave: 


GO
