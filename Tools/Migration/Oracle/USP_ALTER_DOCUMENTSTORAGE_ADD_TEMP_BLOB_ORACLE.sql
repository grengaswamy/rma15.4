create or replace
PROCEDURE USP_ADD_TEMP_BLOB_ORACLE
AS 
BEGIN
	DECLARE 
		sSQL VARCHAR2(1000);
	BEGIN
		sSQL:= 'ALTER TABLE DOCUMENT_STORAGE ADD DOC_BLOB_TEMP BLOB';
                EXECUTE IMMEDIATE sSQL;  
                
                COMMIT;
        END;
	
END;
