using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.Tools.MCMBulkCopy;


namespace Riskmaster.Tools.MCMBulkCopy
{
    /**************************************************************
	 * $File		: Program.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/18/2008
	 * $Author		: Rahul Solanki 
	 * $Comment		: The tool migrates the comments attached to events and claims into enhanced notes. 	 
	*************************************************************
    *	The tool can either be run directly or can be executed via a batch file/installer in silent mode.
    *   Command line parameters-
    *  Riskmaster.Tools.mcm.exe uid pwd DSN
    *		
    */

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] Args )
        {
           Login m_objLogin = null;
            try
            {
                //for getting parameters from command line in case app is run in silent mode               
                if (Args.Length>0)  //handle running automatically
                {
                    m_objLogin = new Login();                    
                    //Disabling Silent run until exception catching for all errors in Userlogin control is implemented
                    AppGlobals.bSilentMode = true;
                    
                    AppGlobals.sUser = Args[0];
                    AppGlobals.sPwd = Args[1];
                    AppGlobals.sDSN = Args[2];                    

                    m_objLogin.AuthenticateUser(AppGlobals.sDSN, AppGlobals.sUser, AppGlobals.sPwd , out AppGlobals.Userlogin);
                    AppGlobals.ConnectionString = AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString;                     
                }
                else
                {
                                        
                    AppGlobals.bSilentMode = false;
                    System.Windows.Forms.Application.EnableVisualStyles();
                    System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);                    
                }
                System.Windows.Forms.Application.Run(new FormMCMBulkCopy());  
            }
            catch (Exception p_oExp)
            {
                string strErrMessage = "There seems to be some problem. \nError description:\n\n";
                //if (!AppGlobals.bSilentMode)
                MessageBox.Show(strErrMessage + p_oExp.Message, "mcm: Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
            finally
            {
                if (m_objLogin != null)
                    m_objLogin.Dispose();                
            }
        }
    }   
}