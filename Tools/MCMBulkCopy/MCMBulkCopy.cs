using System;
using System.Xml.Xsl;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Timers;
//using Riskmaster.DataModel;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.Common.Win32;
using Riskmaster.Db;
using Riskmaster.Application.FileStorage;
using Riskmaster.Common;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Settings;
using Oracle.DataAccess;
using System.Configuration;

namespace Riskmaster.Tools.MCMBulkCopy
{
    /**************************************************************
	 * $File		: frmMCM.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 04/01/2008
	 * $Author		: Rahul Solanki 
	 * $Comment		: The tool migrates the docs attached(and mail merge templates) into MCM. 	 
	*************************************************************   
    *	The tool can either be run directly or can be executed via a bacth file/installer in silent mode.
    *   Command line parameters-
    *  Riskmaster.Tools.MCM.exe uid pwd DSN
    * 
    *  NOTE: DSN is Case sensitive.    
    */

    public partial class FormMCMBulkCopy : Form
    {        
        private Login m_objLogin = new Login();                
        private Riskmaster.Db.DbConnection m_objDbConnection ;

        private DbReader m_objDbReaderRetriveRc = null;

        public static  TextWriter m_sw1;   
        //public static  StreamWriter m_sw1;   
               
        private string m_nowDateTime = Conversion.ToDbDate(System.DateTime.Now);
        private string m_nowTime = Conversion.ToDbTime(System.DateTime.Now);
        private static Boolean m_bIsDebugMode = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["debug"]);
        private static int m_iresetIISInterval = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["resetIISInterval"]);
        private ManualResetEvent locker = new ManualResetEvent(true);

        private static System.Timers.Timer ResetTimer;
        private static System.Timers.Timer countDownTimer;
        
       
     //   DataModelFactory m_objDataModelFactory = null;
        RMConfigurator objConfig = null;
        FileStorageManager objFileStorageManager = null;
        FileStorageManager objFileStorageManagerIndividualUser = null;

        StorageType m_enmDocumentStorageType = 0;
        string m_sDestinationStoragePath = string.Empty;

        //  -1 = Transmitted
        //   0 or null = Not transmitted

        public FormMCMBulkCopy()
        {
            InitializeComponent(); 
        }
        
        private void OnCountDownTimedEvent(object source, ElapsedEventArgs e)
        {
            //Changed by bsharma33 for MTIS 28132 
            //toolStripStatusLabel3.Text = "[" + (ResetTimer.Interval / 60000).ToString() + " min to IIS reset]";  
            //End changes by bsharma33 for MTIS 28132               
            ////aTimer.Enabled = false;
        
        }


        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            //Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime);

            locker.Set();

            Process pr = new Process();
            pr.StartInfo.FileName = @"c:\windows\system32\iisreset.exe";
            //pr.StartInfo.Arguments = "test.dat";
            pr.Start();
            pr.WaitForExit();
            //while (pr.HasExited == false)
            //    if ((DateTime.Now.Second % 5) == 0)
            //    { // Show a tick every five seconds.
            //        System.Threading.Thread.Sleep(1000);
            //    }
            locker.Reset();

        }

        private void frmMCM_Load(object sender, EventArgs e)
        {  
            if (AppGlobals.bSilentMode)
            {
                this.Hide();
            }
            try
            {
                m_sw1 = new StreamWriter("MCM.log", true);                
            }
            catch (Exception exp)
            {
                //writeLog(string.Format("ERROR:[Log file Open error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            if (AppGlobals.bSilentMode)
            {
                writeLog("Running in silent mode; DSN: " + AppGlobals.sDSN);
            }
            writeLog("-----------------------------");
            writeLog("Form Loaded... authenticating");
            writeLog("-----------------------------");

            Boolean bCon = false;
            try
            {
                if (!AppGlobals.bSilentMode)
                {
                    bCon = m_objLogin.RegisterApplication(this.Handle.ToInt32(), 0, ref AppGlobals.Userlogin);
                }

                bool bConversionSuccess = false;
                string sDummyFileMode = System.Configuration.ConfigurationManager.AppSettings["UseDummyFiles"];
                AppGlobals.bUseDummyFiles = Conversion.CastToType<Boolean>(sDummyFileMode, out bConversionSuccess);
                
                // MITS 31645 start
                //AppGlobals.sBackUpStoragePath = System.Configuration.ConfigurationManager.AppSettings["BackupStoragePath"];
                // MITS 31645 end
                

                if (!AppGlobals.bSilentMode && !bCon)
                {
                    // for invalid Login
                    if (m_objLogin != null) m_objLogin.Dispose();
                    writeLog("Invalid Logon. terminating");
                    System.Windows.Forms.Application.Exit();
                }
                else
                {                    
                    writeLog("Authentication successful");
                    AppGlobals.ConnectionString = AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString;
                    m_objDbConnection = DbFactory.GetDbConnection(AppGlobals.ConnectionString);                    

                    txtDocPathType.Text = (Convert.ToBoolean(AppGlobals.Userlogin.objRiskmasterDatabase.DocPathType))?"Database":"File System";
                    txtDocPath.Text = AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath;
                    m_enmDocumentStorageType = StorageType.FileSystemStorage;
                    m_sDestinationStoragePath = string.Empty;

                    // checking to see if MCM is ON or NOT
                    writeLog("checking to see if MCM is Enabled");

                    if (Convert.ToInt32(GetSingleValue_Sql("SELECT USE_ACROSOFT_INTERFACE FROM SYS_PARMS", AppGlobals.ConnectionString))!=-1)
                    { 
                        // throw error if MCM is disabled & exit.
                        if (!AppGlobals.bSilentMode)
                        {
                            MessageBox.Show("MCM is disabled; \nDoc migration can continue..."); 
                        }
                        writeLog("MCM is disabled; cannot proceed");
                        //todo r5
                        //System.Windows.Forms.Application.Exit();                        
                    }
                    writeLog("MCM is Enabled.");


                    // Loading Document details...
                    setStatus("Loading Documents info...");
                    writeLog("Loading Documents info...");
                    PopulateDocInfoGrid();

                    AppGlobals.m_dsDocs.Tables[0].Columns.Add("Size",typeof(long));
                    AppGlobals.m_dsMergeDocs.Tables[0].Columns.Add("Size",typeof(long));
                    
                    setStatus("Click on the 'Start' button to start porting Documents to MCM...");
                    
                    if (AppGlobals.bUseDummyFiles || this.m_enmDocumentStorageType.ToString()!="0" )
                    {
                        // hiding the fields for "total file size" in case dummy file transfer is used
                        txtTotalSize.Hide();
                        label15.Hide();
                    }

                    //retriving file size info
                    writeLog("Loading Documents size info...");
                    bgWorkerPopulateFileinfo.RunWorkerAsync();
                                        
                    

                    if (AppGlobals.bSilentMode)
                    {
                        btnStartTransfer_Click(null, null);
                    }
                }                
                if (AppGlobals.bSilentMode)
                {
                    this.Hide();
                    System.Windows.Forms.Application.Exit();
                }
            }
            catch (Exception ex)
            {
                if (!AppGlobals.bSilentMode)
                {
                    writeLog(string.Format("ERROR:[form Load error] \nmessage:{0}\nStackTrace{1}  ", ex.Message.ToString(), ex.StackTrace.ToString()));
                    throw new Exception(ex.Message.ToString(), ex.InnerException);
                }                               
            }
            

        }

        // Populate Doc Info Grid
        private void PopulateDocInfoGrid()
        {

           int i = 0;           
            
           // use stringbuilder here // MITS 31645 : includes two more columns
           string sqlClaims = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOCUMENT_NAME,DOCUMENT.DOCUMENT_FILEPATH,DOCUMENT_FILENAME,"
                    + "DOCUMENT_ATTACH.TABLE_NAME,DOCUMENT_ATTACH.RECORD_ID,"
                    + "DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.CREATE_DATE, DOCUMENT.FOLDER_ID,"
                    + "DOCUMENT.USER_ID, DOCUMENT.DOCUMENT_TYPE,  DOCUMENT.MCM_TRANSFER_STATUS FROM DOCUMENT LEFT OUTER JOIN DOCUMENT_ATTACH"
                    + " ON DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID WHERE NOT(MCM_TRANSFER_STATUS=-1) OR DOCUMENT.MCM_TRANSFER_STATUS IS NULL "
                    + " ORDER BY DOCUMENT.USER_ID";
            //ORDER BY TABLE_NAME

            string sqlMerge = "SELECT DISTINCT MERGE_FORM.FORM_ID,FORM_NAME,FORM_FILE_NAME, MCM_TRANSFER_STATUS FROM MERGE_FORM WHERE NOT(MCM_TRANSFER_STATUS=-1) OR MCM_TRANSFER_STATUS IS NULL";

            writeLog("Loading doc info...");
            try
            {

                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objDbConnection, sqlClaims);
                AppGlobals.m_dsDocs.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsDocs);                

                ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objDbConnection, sqlMerge);
                AppGlobals.m_dsMergeDocs.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsMergeDocs);

                writeLog("Load complete..datasets populated...");

                AppGlobals.m_dsErroneousDocs.Tables.Add("RMX-to-MCM transfer Report");
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("RecordID");
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("RecordNumber");
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("AttachedTo");
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("DocumentID");
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("DocumentName");
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("DocumentFileName");
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("DocRetrivalSuccess");
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("McmOutputXml");
                // MITS 31645 start
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("FilePath");
                AppGlobals.m_dsErroneousDocs.Tables[0].Columns.Add("FileName");
                // MITS 31645 end

                // only executed if debug is set to true in config file
                if (m_bIsDebugMode)
                {
                    AppGlobals.m_dsDocs.WriteXml(Directory.GetCurrentDirectory() + "\\AttachDocInfo_" + DateTime.Now.Ticks.ToString() + ".xml");
                    AppGlobals.m_dsMergeDocs.WriteXml(Directory.GetCurrentDirectory() + "\\MergeDocInfo_" + DateTime.Now.Ticks.ToString() + ".xml");

                    

                }
              
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[PopulateDocInfoGrid] \nmessage:{0}\n\nStackTrace{1}  ", exp.Message.ToString(),  exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            writeLog("Load operation completed.");

            txtDocsCount.Text = (AppGlobals.m_dsDocs.Tables[0].Rows.Count + AppGlobals.m_dsMergeDocs.Tables[0].Rows.Count).ToString();
        }

        // sets text for the status panel box
        private void setStatus(string p_status)
        {
            toolStripStatusLabel1.Text = p_status;
        }
                
        private void setBgStatus(string p_status)
        {
            //toolStripStatusLabel1.Text = p_status;
            AppGlobals.sStatusMessage = p_status;
            bgWorkerMain.ReportProgress(1);
        }
        
        private void frmMCM_FormClosing(object sender, FormClosingEventArgs e)
        {
            writeLog("Disposing all objects" );
            if (m_objLogin!=null)   m_objLogin.Dispose();
            if (AppGlobals.m_dsDocs!=null) AppGlobals.m_dsDocs.Dispose();
            //if (AppGlobals.m_dsEnhNotes!=null) AppGlobals.m_dsEnhNotes.Dispose();
            if (m_objDbConnection!=null) m_objDbConnection.Dispose();
            if (m_sw1 != null)
            {                
                m_sw1.Close();
                m_sw1.Dispose();
            }
            
            // cancel all bg workers here
            bgWorkerPopulateFileinfo.CancelAsync();
            bgWorkerMain.CancelAsync();
        }


        //skhare7   //skhare7 MITS 21874
        public struct RecordCollection
        {
            public string EventNumber;
            public string PolicyNumber;
            public string ClaimNumber;
          
        }

     //skhare7 MITS 21874
        public RecordCollection GetRecordValues(string sTableName, int iPrimaryKey)
        {
            string sSQL = string.Empty;
            
            //RecordCollection rc = new RecordCollection();
            try
            {

                switch (sTableName.ToLower())
                {

                    case "event":
                        sSQL = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                           return new RecordCollection {EventNumber=  Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }

                        break;

                    case "claim":

                        sSQL = "SELECT E.EVENT_NUMBER,C.CLAIM_NUMBER FROM CLAIM C,EVENT E WHERE C.EVENT_ID=E.EVENT_ID AND C.CLAIM_ID=" + iPrimaryKey;
                        
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]), ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1]) };

                          
                        }
                        break;

                    case "pipatient":
                    case "piwitness":
                    case "piemployee":
                        sSQL = "select E.EVENT_NUMBER,C.CLAIM_NUMBER from PERSON_INVOLVED PI,EVENT E,CLAIM C " 
                            + "where E.EVENT_ID=PI.EVENT_ID and C.EVENT_ID=E.EVENT_ID and PI.PI_ROW_ID = " 
                            + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                                , ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1]) };

                        
                        }
                        break;

                    case "policyenh":
                        sSQL = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID=" + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection {PolicyNumber=  Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }

                        break;

                    case "eventdatedtext":
                          sSQL = "select E.EVENT_NUMBER  from EVENT_X_DATED_TEXT ED,EVENT E,CLAIM C "
                              +"where ED.EVENT_ID=E.EVENT_ID and ED.EV_DT_ROW_ID=" 
                              + iPrimaryKey;
                         m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1]) };
                           
                        }

                        break;

                    case "claimant":
                         sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER  from CLAIMANT CL,EVENT E,CLAIM C where cl.CLAIM_ID=C.CLAIM_ID and "
                         + "E.EVENT_ID=C.EVENT_ID and CL.CLAIMANT_ROW_ID=" + iPrimaryKey;
                         m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                , ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }

                        break;

                    case "defendant":
                         sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER  from DEFENDANT D,EVENT E,CLAIM C where D.CLAIM_ID=C.CLAIM_ID and "
                        + "E.EVENT_ID=C.EVENT_ID and D.DEFENDANT_ROW_ID =" + iPrimaryKey;
                         m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                , ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }
                        break;

                    case "policy":
                         sSQL = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID=" + iPrimaryKey;
                         m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { PolicyNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }

                        break;
                    //skhare7:MITS 22743
                    case "litigation":
                        sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER from CLAIM_X_LITIGATION CL ,CLAIM C,EVENT E where C.CLAIM_ID=CL.CLAIM_ID and C.EVENT_ID=E.EVENT_ID and CL.LITIGATION_ROW_ID ="
                            + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;
                    case "expert":
                        sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER from EXPERT EP ,CLAIM_X_LITIGATION CL ,CLAIM C,EVENT E  where C.CLAIM_ID=CL.CLAIM_ID and E.EVENT_ID=C.EVENT_ID"
                                 + "and CL.LITIGATION_ROW_ID=EP.LITIGATION_ROW_ID and EP.EXPERT_ROW_ID ="
                                 + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;
                    case "adjuster dated text":
                        sSQL = "select  C.CLAIM_NUMBER,E.EVENT_NUMBER from ADJUST_DATED_TEXT AT,CLAIM_ADJUSTER CA,CLAIM C,EVENT E where "
                                +"AT.ADJ_ROW_ID=CA.ADJ_ROW_ID and C.CLAIM_ID=CA.CLAIM_ID and C.EVENT_ID=E.EVENT_ID"
                                 +"and AT.ADJ_DTTEXT_ROW_ID ="
                                    + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;
                    case "plan":
                        sSQL = "select E.EVENT_NUMBER,c.CLAIM_NUMBER from CLAIM c , DISABILITY_PLAN DP,EVENT E "
                               + "where  c.CLAIM_ID=DP.PLAN_ID and E.EVENT_ID=c.EVENT_ID and "
                               + "DP.PLAN_ID=" + iPrimaryKey;
                              
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                            };

                        }

                        break;
                    //skhare7:MITS 22743 End
                    default:
                        return new RecordCollection();

                }

                return new RecordCollection();
              

            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(), p_oException.StackTrace.ToString()));
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                //m_objDbReaderRetriveRc.Dispose();
            }
          
        }
        //skhare7 MITS 21874 End
        public static string GetSingleValue_Sql(string sSQL, string strConnectionString)
        {
            string sValue = string.Empty;
            DbReader objReader = null;
            try
            {
                objReader = DbFactory.GetDbReader(strConnectionString, sSQL);
                if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                if (objReader.Read())
                {
                    sValue = Conversion.ConvertObjToStr(objReader[0]);
                }
            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(),  p_oException.StackTrace.ToString()));               
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                objReader.Dispose();
            }
            return sValue;
        }

      

       
        public void ExecuteSql(string p_sSQL)
        {
            if (m_bIsDebugMode) writeLog("ExecuteSql: executing sql " + p_sSQL);
            try
            {   
                m_objDbConnection.ExecuteNonQuery(p_sSQL);
            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[ExecuteSql] \nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(), p_oException.StackTrace.ToString()));
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {   
                //objDbConnection.Dispose();
            }            
        }

        public static void writeLog(string p_strLogText)
        {
            string sMessage = string.Format("[{0}] {1}", System.DateTime.Now.ToString(), p_strLogText);            
            try
            {                
                m_sw1.WriteLine(sMessage);
                m_sw1.Flush();                
            }
            catch (Exception exp)
            {
                throw new Exception("Error Logging Exception :" + exp.Message.ToString(), exp.InnerException);
            }
            
        }

        private void btnStartTransfer_Click(object sender, EventArgs e)
        {

            if (m_iresetIISInterval > 0)
            {
                ResetTimer = new System.Timers.Timer(m_iresetIISInterval * 60000);
                
                // Hook up the Elapsed event for the timer.
                ResetTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                ResetTimer.AutoReset = true;
                ResetTimer.Enabled = true;

                countDownTimer = new System.Timers.Timer(60000);

                countDownTimer.Elapsed += new ElapsedEventHandler(OnCountDownTimedEvent);
                countDownTimer.AutoReset = true;
                countDownTimer.Enabled = true;

                OnCountDownTimedEvent(null,null);

            }
            else
            {
                //toolStripStatusLabel3.Text = "Timer disabled";
                ////aTimer.Enabled = false;
            }
            
            
            btnPauseResume.Enabled = true;
            btnStartTransfer.Enabled = false;
            setStatus("Starting doc transfer");
            writeLog("Starting doc transfer");

            try
            {
                bgWorkerMain.RunWorkerAsync();
            }
            catch (Exception exp)
            {                
                writeLog(string.Format("ERROR:[StartTransfer] \nmessage:{0}\nStackTrace{1}  ", exp.Message.ToString(),  exp.StackTrace.ToString()));
            }
        }

        private void bgWorkerMain_DoWork(object sender, DoWorkEventArgs e)
        {           
            #region Declarations
            string sIndiUserPath = "";
            //Claim objClaim = null;
            //Event objEvent = null;
            //PiEmployee objPI = null;
            //PiPatient objPatient = null;
            //PiWitness objWitness = null;
            //EventXDatedText objDatedText = null;
            //Claimant objClaimant = null;
            //Defendant objDefendant = null;
            //Policy objPolicy = null;
            //PolicyEnh objPolicyEnh = null;
            MemoryStream objFileContent = null;
            string sSQL = string.Empty;
            string sDocId = string.Empty;
            string sTableName = string.Empty;
            Boolean bfileRetrivalStatus = false;
            string sRecordNumberForLog = string.Empty;
            string sEventNumber = string.Empty;
            string sClaimNumber = string.Empty;
            string sPolicyName = string.Empty;
            string sEventId = string.Empty;
            string sClaimId = string.Empty;
            string sAcrosoftAttachmentsTypeKey = string.Empty;
            string sAcrosoftGeneralStorageTypeKey = string.Empty;
            string sAcrosoftPolicyTypeKey = string.Empty;
            string sAcrosoftUsersTypeKey = string.Empty;
            string sFileName = string.Empty;
            int iRetCd = 0;
            byte[] bFileContent = null;
            string sDocTitle = string.Empty;
            string sCreateTs = string.Empty;
            string sModifiedCreateTs = string.Empty;
            Acrosoft objAcrosoft = null;
            string sAppExcpXml = string.Empty;
            string sSessionId = string.Empty;
            string sReturnXmlstring = string.Empty;
            //XmlDocument objXmlDoc = null;
            //XmlElement objTempElement = null;
            //XmlElement objParentElement = null;
            string sAttachedRecord = String.Empty;
            string sPIEmployeeId = string.Empty;
            string sPIPatientId = string.Empty;
            string sPIWitnessId = string.Empty;
            string sEventDatedText = string.Empty;
            string sClaimant = string.Empty;
            string sDefendant = string.Empty;
            string sPolicyId = string.Empty;
            string sFormName = string.Empty;
            string sFormFileName = string.Empty;
            string sAuthor = string.Empty;
            //skhare7 MITS 22743
            string sPlan = string.Empty;
            string sLitigation = string.Empty;
            string sExpert = string.Empty;
            string sAdjusterText = string.Empty;

           //skhare7 MITS   22743 end
            //MemoryStream objMemoryStream = null;
            string sEventFolderFriendlyName = "";
            string sClaimFolderFriendlyName = "";
            string sPolicyFolderFriendlyName = "";
            string sUsersFolderFriendlyName = "";
            string sGeneralFolderFriendlyName = "";
            string sUserDocLoc="";
            string sMigrationStatus = "";
            bool bMigrationStatus = false;
            //string sFileNameValue = string.Empty;
			// MITS 31645 start
            string sFilePath = string.Empty;
			int isRetrivalSuccessFull =0;
            //string sourceFile = "";
            //string destFile = "";         
            bool fileExists = false;          
			// MITS 31645 end
            int iRowsCount = AppGlobals.m_dsDocs.Tables[0].Rows.Count;
            int i=0;
            long iSize = 0;

            #endregion

            try
            {
                //sReturnXmlstring = "<Documents><Attachments><event></event><claim></claim><piemployee></piemployee><pipatient></pipatient><piwitness></piwitness>"
                //    + "<eventdatedtext></eventdatedtext><claimant></claimant><defendant></defendant><policy></policy></Attachments><Templates></Templates><TemplateHeaders></TemplateHeaders><orphandocuments></orphandocuments></Documents>";
                //objXmlDoc = new XmlDocument();
                //objXmlDoc.LoadXml(sReturnXmlstring);
                //Raman 10/13/2009 : Riskmaster.Config not used now
                //objConfig = new RMConfigurator();


                RMConfigurationManager.GetAcrosoftSettings();

                sAcrosoftAttachmentsTypeKey = AcrosoftSection.AcrosoftAttachmentsTypeKey;
                sAcrosoftGeneralStorageTypeKey = AcrosoftSection.AcrosoftGeneralStorageTypeKey;
                sAcrosoftPolicyTypeKey = AcrosoftSection.AcrosoftPolicyTypeKey; 
                sAcrosoftUsersTypeKey = AcrosoftSection.AcrosoftUsersTypeKey; 
                sEventFolderFriendlyName = AcrosoftSection.EventFolderFriendlyName;
                sClaimFolderFriendlyName = AcrosoftSection.ClaimFolderFriendlyName;
                sPolicyFolderFriendlyName = AcrosoftSection.PolicyFolderFriendlyName;
                sUsersFolderFriendlyName = AcrosoftSection.UsersFolderFriendlyName;
                sGeneralFolderFriendlyName = AcrosoftSection.GeneralFolderFriendlyName; 
                m_enmDocumentStorageType = (StorageType)AppGlobals.Userlogin.objRiskmasterDatabase.DocPathType;
                m_sDestinationStoragePath = AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath;
                objFileStorageManager = new FileStorageManager(this.m_enmDocumentStorageType, this.m_sDestinationStoragePath);
                objFileStorageManagerIndividualUser = new FileStorageManager(this.m_enmDocumentStorageType, this.m_sDestinationStoragePath);
                // MITS 31645 start
                FileStorageManager objFileStorageFilePath = new FileStorageManager(this.m_enmDocumentStorageType, this.m_sDestinationStoragePath);
                // MITS 31645 end
                //objFileStorageManagerIndividualUser.DestinationStoragePath
                
                objFileContent = new MemoryStream();
              //  m_objDataModelFactory = new DataModelFactory(AppGlobals.Userlogin);

                //rsolanki2 :  start updates for MCM mits 19200 
                Boolean bSuccess = false;
                string sTempAcrosoftUserId = AppGlobals.Userlogin.LoginName;
                string sTempAcrosoftPassword = AppGlobals.Userlogin.Password;

                if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                {
                    if (AcrosoftSection.AcroUserMapSection.UserMap[AppGlobals.Userlogin.LoginName] != null
                        && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[AppGlobals.Userlogin.LoginName].RmxUser))
                    {
                        sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[AppGlobals.Userlogin.LoginName].AcrosoftUserId;
                        sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[AppGlobals.Userlogin.LoginName].AcrosoftPassword;
                    }
                    else
                    {
                        sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                        sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                    }
                }   
                // end 19200

                objAcrosoft = new Acrosoft();
                if (objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionId, out sAppExcpXml) != 0)
                {
                    // in case the Authentication fails at acrosoft, we nede to exit.
                    // In this case the userid (with same LoginName & password) needs to be created thru the acrosoft admin tool  
                    if (!AppGlobals.bSilentMode)
                    {
                        MessageBox.Show("Acrosoft Authentication unsuccessfull. terminating.");
                    }
                    writeLog("Acrosoft Authentication unsuccessfull. terminating.");
                    System.Windows.Forms.Application.Exit();                
                }

                for (i = 0; i < iRowsCount; i++)
                {

                    locker.WaitOne();

                    sMigrationStatus = AppGlobals.m_dsDocs.Tables[0].Rows[i]["MCM_TRANSFER_STATUS"].ToString();

                    sAppExcpXml = string.Empty;
                    bfileRetrivalStatus = false;    

                    //doing a null check
                    if (sMigrationStatus.Length > 0)
                    {
                        
                        bMigrationStatus = (Convert.ToInt32(sMigrationStatus.Trim())==-1)?true:false;
                            //Convert.ToBoolean(Convert.ToInt32(sMigrationStatus.Trim()));
                    }
                    else
                    {
                        bMigrationStatus = false;
                    }

                    // this check is unrequired in case we load only doc with migration status !=0 but anyways 
                    // implementing it as in case any changes are made in future
                    if (!bMigrationStatus)
                    {
                        sFileName = AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_FILENAME"].ToString();
                        sDocTitle = AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_NAME"].ToString();
                        // MITS 31645 start
						sFilePath = AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_FILEPATH"].ToString();
						// MITS 31645 end
                        try
                        {
                            #region uploading attachment to MCM

                            writeLog("---------------");
                            iRetCd = 0;                            
                            sTableName = AppGlobals.m_dsDocs.Tables[0].Rows[i]["TABLE_NAME"].ToString();                            
                            sCreateTs = AppGlobals.m_dsDocs.Tables[0].Rows[i]["CREATE_DATE"].ToString();
                            sAuthor = AppGlobals.m_dsDocs.Tables[0].Rows[i]["USER_ID"].ToString();
                            sDocId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_ID"].ToString();

                            if (AppGlobals.bUseDummyFiles || this.m_enmDocumentStorageType.ToString() != "FileSystemStorage")
                            {
                                iSize = 1;
                            }
                            else if (AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"] != null && AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"].ToString().Length > 0)
                            {
                                iSize = Convert.ToInt64(AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"]);
                                //bfileRetrivalStatus = true;
                            }
                            else
                            {
                                // means that the utility was unable to get the size of the file in the first place..
                                // the utility will do a re-check ont he file....
                                // instead we could also have skiped on this file.
                                iSize = 0;
                                writeLog("possibly erroneous file: " + sFileName);
                                //bfileRetrivalStatus = false;
                            }

                            //checking individual doc path here.
                            if (!AppGlobals.sdUserDocPaths.ContainsKey(sAuthor))
                            {
                                sIndiUserPath = GetSingleValue_Sql(string.Format("SELECT DOC_PATH FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '{0}'", sAuthor).Trim(), m_objLogin.SecurityDsn);
                                AppGlobals.sdUserDocPaths.Add(sAuthor, sIndiUserPath);
                            }
                            else
                            {
                                sIndiUserPath = AppGlobals.sdUserDocPaths[sAuthor];
                            }

                            if (sIndiUserPath != "" && objFileStorageManagerIndividualUser.DestinationStoragePath.ToLower() != sIndiUserPath.ToLower())
                            {
                                objFileStorageManagerIndividualUser = new FileStorageManager(this.m_enmDocumentStorageType, sIndiUserPath);
                            }

                            //// MITS 31645 start
                            if (!string.IsNullOrEmpty(sFilePath))
                            {
                                objFileStorageFilePath = new FileStorageManager(this.m_enmDocumentStorageType, sFilePath);

                                //sourceFile = System.IO.Path.Combine(sFilePath, sFileName);
                                //destFile = System.IO.Path.Combine(AppGlobals.sBackUpStoragePath, sFileName);
                                fileExists = false;

                                if (AppGlobals.bUseDummyFiles)
                                {

                                    //if (!System.IO.Directory.Exists(AppGlobals.sBackUpStoragePath))
                                    //{
                                    //    System.IO.Directory.CreateDirectory(AppGlobals.sBackUpStoragePath);
                                    //}

                                    isRetrivalSuccessFull = objFileStorageFilePath.FileExists(sFileName, out fileExists);
                                    //if (fileExists)
                                    //{
                                    //    System.IO.File.Copy(sourceFile, destFile, true);
                                    //    writeLog(string.Format("{0} copied to {1}", sourceFile, destFile));
                                    //}
                                }
                            }
                           // MITS 31645 end
                            // value for updating progress bar here
                            AppGlobals.lProcessedFileSize += iSize;

                            //checking individual doc path here.
                            //if (!AppGlobals.sdUserDocPaths.ContainsKey(sAuthor))
                            //{                         
                            //    //sAuthor 
                            //    //GetSingleValue_Sql
                            //    sUserDocLoc = GetSingleValue_Sql(string.Format("SELECT DOC_PATH FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '{0}'", sAuthor), AppGlobals.Userlogin.ToString());
                            //    AppGlobals.sdUserDocPaths.Add("sAuthor", sUserDocLoc);
                            //}
                                                        
                            setBgStatus("attempting file transfer for:" + sFileName);
                            writeLog(string.Format("attempting file transfer for:{0}\n FileType:{1}\nDocument Name:{2}\nauthor:{4} ", sFileName, sTableName, sDocTitle, sCreateTs, sAuthor));

                            sAttachedRecord = "";

                            if (sCreateTs != "")
                            {
                                sModifiedCreateTs = sCreateTs.Substring(0, 4) + "/" + sCreateTs.Substring(4, 2) + "/"
                                    + sCreateTs.Substring(6, 2) + " " + sCreateTs.Substring(8, 2) + ":"
                                    + sCreateTs.Substring(10, 2);
                            }
                            if (AppGlobals.bUseDummyFiles)
                            {
							// MITS 31645 start
                                if (!string.IsNullOrEmpty(sFilePath) && fileExists)
                                {
                                    iRetCd = 1;
                                    objFileContent = new MemoryStream(FileAsByteArray(@System.IO.Path.GetFullPath("dummy.txt")));
                                }
                                else if (!string.IsNullOrEmpty(sFilePath) && !fileExists)
                                {
                                    iRetCd = 99;
                                    writeLog(string.Format("Document_Path : File Name:{0} does not exist", sFileName));
                                }
                                else
                                {
                                    iRetCd = 1;
                                    objFileContent = new MemoryStream(FileAsByteArray(@System.IO.Path.GetFullPath("dummy.txt")));
                                }
								// MITS 31645 end
                            }

                            RecordCollection rcTemp;
                            #region switch statement
                            switch (sTableName.ToLower())
                            {
                                case "event": sEventId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                 //   writeLog("creating DM object-event");
                                   // objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                                  //  writeLog("navigating...");
                                 //   objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));
                                    
                                    
                                    //sEventNumber = GetRecordValues("event", Conversion.ConvertStrToInteger(sEventId)).EventNumber;
                                    rcTemp = GetRecordValues("event", Conversion.ConvertStrToInteger(sEventId));
                                    sRecordNumberForLog = sEventNumber = rcTemp.EventNumber;

                                    writeLog("extracting object info.");
                                   // sEventNumber = objEvent.EventNumber;
                                    sRecordNumberForLog = sAttachedRecord = sEventNumber;
                                    //skhare7 MITS 21874 End
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("EVENT: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                            //writeLog(string.Format("OutXML:{0}", objFileContent.to));
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("EVENT: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }


                                        if (iRetCd == 1)
                                        { 
                                            writeLog("EVENT: File retrival successfull");
                                            bfileRetrivalStatus = true;
                                        }
                                        else
                                        { 
                                            writeLog("EVENT: File retrival failure");
                                            bfileRetrivalStatus = false;
                                        }
                                    }

                                    if (iRetCd == 1)
                                    {

                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0) 
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml); // MITS 31645
                                        }

                                    }
                                    break;
                                case "claim": sClaimId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                   // writeLog("creating DM object-claim");
                                   
                                   // objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                                    //writeLog("navigating...");
                                   // objClaim.MoveTo(Conversion.ConvertStrToInteger(sClaimId));
                                    //sEventNumber = GetRecordValues("claim", Conversion.ConvertStrToInteger(sClaimId)).EventNumber;
                                    //sClaimNumber = GetRecordValues("claim", Conversion.ConvertStrToInteger(sClaimId)).ClaimNumber;

                                    rcTemp = GetRecordValues("claim", Conversion.ConvertStrToInteger(sClaimId));
                                    sEventNumber = rcTemp.EventNumber;

                                    //sEventNumber = rcTemp.EventNumber;
                                    sRecordNumberForLog = sClaimNumber = rcTemp.ClaimNumber;


                                    writeLog("extracting object info.");
                                   
                                   // sEventNumber = objClaim.EventNumber;
                                  //  sClaimNumber = objClaim.ClaimNumber;
                                    //skhare7 MITS 21874 End
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("CLAIM: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);                                                                                           
                                                
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("CLAIM: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
										// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("CLAIM: File retrival successfull");
                                            bfileRetrivalStatus = true;
                                        }
                                        else
                                        { 
                                            writeLog("CLAIM: File retrival failure");
                                            bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft

                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;

                                case "piemployee": sPIEmployeeId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                   // writeLog("creating DM object-piemployee");
                                  //  objPI = (PiEmployee)m_objDataModelFactory.GetDataModelObject("PiEmployee", false);
                                   // writeLog("navigating...");
                                   // objPI.MoveTo(Conversion.ConvertStrToInteger(sPIEmployeeId));                                    
                                  //  sEventId = objPI.EventId.ToString();
                                   // writeLog("creating DM object-event");
                                   // objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                                   // writeLog("navigating...");
                                  //  objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));
                                    sEventNumber = GetRecordValues("piemployee", Conversion.ConvertStrToInteger(sPIEmployeeId)).EventNumber;

                                   // sClaimNumber = GetRecordValues(sTableName, Conversion.ConvertStrToInteger(sClaimId)).ClaimNumber;
                                    writeLog("extracting object info.");
                                   //sEventNumber = objEvent.EventNumber;
                                    sRecordNumberForLog = sAttachedRecord = sEventNumber;
                                    //skhare7 MITS 21874 End
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("piemployee: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("piemployee: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
										// MITS 31645 end										
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("piemployee: File retrival successfull");
                                            bfileRetrivalStatus = true;
                                        }
                                        else
                                        { 
                                            writeLog("piemployee: File retrival failure");
                                            bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;

                                case "pipatient": sPIPatientId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                 //   writeLog("creating DM object-pipatient");
                                   // objPatient = (PiPatient)m_objDataModelFactory.GetDataModelObject("PiPatient", false);
                                   // writeLog("navigating...");
                                  //  objPatient.MoveTo(Conversion.ConvertStrToInteger(sPIPatientId));                                    
                                  //  sEventId = objPatient.EventId.ToString();
                                 //   writeLog("creating DM object-Event");
                                  //  objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                                  //  writeLog("navigating...");
                                //    objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));
                                    //sEventNumber = GetRecordValues("pipatient", Conversion.ConvertStrToInteger(sPIPatientId)).EventNumber;
                                    //sClaimNumber = GetRecordValues("pipatient", Conversion.ConvertStrToInteger(sPIPatientId)).ClaimNumber;

                                    rcTemp = GetRecordValues("pipatient", Conversion.ConvertStrToInteger(sPIPatientId));
                                    sClaimNumber = rcTemp.ClaimNumber;
                                    sEventNumber = rcTemp.EventNumber;

                                    writeLog("extracting object info.");
                                  //  sEventNumber = objEvent.EventNumber;
                                    //skhare7 MITS 21874 End
                                    sRecordNumberForLog = sAttachedRecord = sEventNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("pipatient: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("pipatient: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("pipatient: File retrival successfull"); bfileRetrivalStatus = true; 
                                        }
                                        else
                                        {
                                            writeLog("pipatient: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }

                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;

                                case "piwitness": sPIWitnessId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                   // writeLog("creating DM object-piwitness");
                                 //   objWitness = (PiWitness)m_objDataModelFactory.GetDataModelObject("PiWitness", false);
                                   // writeLog("navigating...");
                                   // objWitness.MoveTo(Conversion.ConvertStrToInteger(sPIWitnessId));
                                   // sEventId = objWitness.EventId.ToString();
                                   // writeLog("creating DM object-Event");
                                   // objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                                  //  writeLog("navigating...");
                                  //  objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));

                                     rcTemp = GetRecordValues("piwitness", Conversion.ConvertStrToInteger(sPIWitnessId));

                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                    //sEventNumber = GetRecordValues("piwitness", Conversion.ConvertStrToInteger(sPIWitnessId)).EventNumber;
                                    //sClaimNumber = GetRecordValues("piwitness", Conversion.ConvertStrToInteger(sPIWitnessId)).ClaimNumber;

                                    //skhare7 MITS 21874 End
                                    writeLog("extracting object info.");
                                //    sEventNumber = objEvent.EventNumber;
                                    sRecordNumberForLog = sAttachedRecord = sEventNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("piwitness: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("piwitness: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("piwitness: File retrival successfull"); bfileRetrivalStatus = true; 
                                        }
                                        else
                                        {
                                            writeLog("piwitness: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }

                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;

                                case "eventdatedtext": sEventDatedText = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                   // writeLog("creating DM object-eventdatedtext");
                                   // objDatedText = (EventXDatedText)m_objDataModelFactory.GetDataModelObject("EventXDatedText", false);
                                   // writeLog("navigating...");
                                    //objDatedText.MoveTo(Conversion.ConvertStrToInteger(sEventDatedText));
                                   // sEventId = objDatedText.EventId.ToString();
                                   // writeLog("creating DM object-event");
                                   // objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                                   // writeLog("navigating...");
                                 //   objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));
                                    sEventNumber = GetRecordValues("eventdatedtext", Conversion.ConvertStrToInteger(sEventId)).EventNumber;
                                  //  sClaimNumber = GetRecordValues(sTableName, Conversion.ConvertStrToInteger(sPIWitnessId)).ClaimNumber;
                                    writeLog("extracting object info.");
                                  //  sEventNumber = objEvent.EventNumber;
                                    //skhare7 MITS 21874 End
                                    sRecordNumberForLog = sAttachedRecord = sEventNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("eventdatedtext: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("eventdatedtext: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        {
                                            writeLog("eventdatedtext: File retrival successfull"); bfileRetrivalStatus = true; 
                                        }
                                        else
                                        {
                                            writeLog("eventdatedtext: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, "", sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, "", sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;

                                case "claimant": sClaimant = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                  //  writeLog("creating DM object-claimant");        
                                    //objClaimant = (Claimant)m_objDataModelFactory.GetDataModelObject("Claimant", false);
                                   // writeLog("navigating...");
                                   // objClaimant.MoveTo(Conversion.ConvertStrToInteger(sClaimant));
                                  //  sClaimId = objClaimant.ClaimId.ToString();
                                    //writeLog("creating DM object-claim");
                                   // objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                                  //  writeLog("navigating...");
                                   // objClaim.MoveTo(Conversion.ConvertStrToInteger(sClaimId));
                                  //  writeLog("extracting object info.");
                                    //sEventNumber = GetRecordValues("claimant", Conversion.ConvertStrToInteger(sClaimant)).EventNumber;
                                    //sClaimNumber = GetRecordValues("claimant", Conversion.ConvertStrToInteger(sClaimant)).ClaimNumber;

                                    rcTemp = GetRecordValues("claimant", Conversion.ConvertStrToInteger(sClaimant));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;


                                  //  sEventNumber = objClaim.EventNumber;
                                    //sClaimNumber = objClaim.ClaimNumber;
                                    //skhare7 MITS 21874 End
                                    sRecordNumberForLog = sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("claimant: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("claimant: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("claimant: File retrival successfull"); bfileRetrivalStatus = true; 
                                        }
                                        else
                                        {
                                            writeLog("claimant: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;

                                case "defendant": sDefendant = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                  //  writeLog("creating DM object-defendant");
                                  //  objDefendant = (Defendant)m_objDataModelFactory.GetDataModelObject("Defendant", false);
                                   // writeLog("navigating...");
                                   // objDefendant.MoveTo(Conversion.ConvertStrToInteger(sDefendant));
                                   // sClaimId = objDefendant.ClaimId.ToString();
                                   // writeLog("creating DM object-claim");
                                  //  objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                                   // writeLog("navigating...");
                                  //  objClaim.MoveTo(Conversion.ConvertStrToInteger(sClaimId));
                                    //sEventNumber = GetRecordValues("defendant", Conversion.ConvertStrToInteger(sDefendant)).EventNumber;
                                    //sClaimNumber = GetRecordValues("defendant", Conversion.ConvertStrToInteger(sDefendant)).ClaimNumber;

                                    rcTemp= GetRecordValues("defendant", Conversion.ConvertStrToInteger(sDefendant));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                    writeLog("extracting object info.");
                                   // sEventNumber = objClaim.EventNumber;
                                  //  sClaimNumber = objClaim.ClaimNumber;
                                    //skhare7 MITS 21874 End
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("defendant: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("defendant: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("defendant: File retrival successfull"); bfileRetrivalStatus = true; 
                                        }
                                        else
                                        {
                                            writeLog("defendant: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;

                                //skhare7:MITS 22743
                                case "expert": sExpert = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                  
                                    //sEventNumber = GetRecordValues("expert", Conversion.ConvertStrToInteger(sExpert)).EventNumber;
                                    //sClaimNumber = GetRecordValues("expert", Conversion.ConvertStrToInteger(sExpert)).ClaimNumber;

                                    rcTemp = GetRecordValues("expert", Conversion.ConvertStrToInteger(sExpert));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                    writeLog("extracting object info.");
                                   
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("defendant: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("defendant: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("defendant: File retrival successfull"); bfileRetrivalStatus = true; }
                                        else
                                        {
                                            writeLog("defendant: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;
                               
                                case "plan": sPlan = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                  
                                    //sEventNumber = GetRecordValues("plan", Conversion.ConvertStrToInteger(sPlan)).EventNumber;
                                    //sClaimNumber = GetRecordValues("plan", Conversion.ConvertStrToInteger(sPlan)).ClaimNumber;

                                    rcTemp = GetRecordValues("plan", Conversion.ConvertStrToInteger(sPlan));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                    writeLog("extracting object info.");
                                    
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("plan: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("plan: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("defendant: File retrival successfull"); bfileRetrivalStatus = true; 
                                        }
                                        else
                                        {
                                            writeLog("defendant: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;
                                case "litigation": sLitigation = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();

                                    rcTemp = GetRecordValues("litigation", Conversion.ConvertStrToInteger(sLitigation));

                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;


                                    writeLog("extracting object info.");
                                    
                                    //skhare7 MITS 21874 End
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("litigation: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("litigation: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("litigation: File retrival successfull"); bfileRetrivalStatus = true; 
                                        }
                                        else
                                        {
                                            writeLog("litigation: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;
                                case "adjuster dated text": sAdjusterText = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                 
                                    rcTemp= GetRecordValues("adjuster dated text", Conversion.ConvertStrToInteger(sAdjusterText));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                    writeLog("extracting object info.");
                                   
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("adjuster dated text: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("adjuster dated text: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        {
                                            writeLog("defendant: File retrival successfull"); bfileRetrivalStatus = true;
                                        }
                                        else
                                        {
                                            writeLog("defendant: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateAttachmentFolder(sSessionId, sAcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber, sEventFolderFriendlyName, sClaimFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;
                                //skhare7:MITS 22743 End
                                case "policy": sPolicyId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                 //   writeLog("creating DM object-policy");
                                 //   objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
                                  //  writeLog("navigating...");
                                   // objPolicy.MoveTo(Conversion.ConvertStrToInteger(sPolicyId));
                                    sPolicyName = GetRecordValues("policy", Conversion.ConvertStrToInteger(sPolicyId)).PolicyNumber;
                                   
                                    writeLog("extracting object info.");
                                  //  sPolicyName = objPolicy.PolicyName;
                                    //skhare7 MITS 21874 End
                                    sAttachedRecord = sPolicyName;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("policy: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("policy: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        {
                                            writeLog("policy: File retrival successfull"); bfileRetrivalStatus = true; 
                                        }
                                        else
                                        {
                                            writeLog("policy: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreatePolicyFolder(sSessionId, sAcrosoftPolicyTypeKey, sPolicyName, sPolicyFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sPolicyName, sAcrosoftPolicyTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sPolicyName, sAcrosoftPolicyTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645

                                            //iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftAttachmentsTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);
                                        }

                                    }


                                    break;
                                
                                //MITS 18136 : Raman Bhatia 10/14/2009
                                    //skhare7 MITS 21889 :Enhanced policy added for different LOB's
                                //Adding MCM support for Policy Management
                                case "policyenh":
                                case "policyenhgl":
                                case "policyenhpc":
                                case "policyenhwc":
                                case "policyenhal": 
                                    sPolicyId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                  //  writeLog("creating DM object-policy");
                                 //   objPolicyEnh = (PolicyEnh)m_objDataModelFactory.GetDataModelObject("PolicyEnh", false);
                                  //  writeLog("navigating...");
                                  //  objPolicyEnh.MoveTo(Conversion.ConvertStrToInteger(sPolicyId));
                                    sPolicyName = GetRecordValues("policyenh", Conversion.ConvertStrToInteger(sPolicyId)).PolicyNumber;
                                    writeLog("extracting object info.");
                                    //sPolicyName = objPolicyEnh.PolicyName;
                                    //skhare7 MITS 21874 End
                                    sAttachedRecord = sPolicyName;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("policy: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("policy: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("policy: File retrival successfull"); bfileRetrivalStatus = true;
                                        }
                                        else
                                        {
                                            writeLog("policy: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreatePolicyFolder(sSessionId, sAcrosoftPolicyTypeKey, sPolicyName, sPolicyFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sPolicyName, sAcrosoftPolicyTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sPolicyName, sAcrosoftPolicyTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }


                                    break;
                           
                         
                        

                                //skhare7 MITS 21889 
                                default:

                                    sTableName = "OrphanDocuments";
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("OrphanDocuments: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
										// MITS 31645 start
                                            if (!string.IsNullOrEmpty(sFilePath))
                                            {
                                                iRetCd = objFileStorageFilePath.RetrieveFile(sFileName, out objFileContent);
                                            }
                                            else
                                            {
                                                if (m_bIsDebugMode) writeLog("OrphanDocuments: retriving File thru individual doc location ");
                                                iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                            }
											// MITS 31645 end
                                        }

                                        if (iRetCd == 1)
                                        { 
                                            writeLog("OrphanDocuments: File retrival successfull"); bfileRetrivalStatus = true; 
                                        }
                                        else
                                        {
                                            writeLog("OrphanDocuments: File retrival failure"); bfileRetrivalStatus = false;
                                        }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        objAcrosoft.CreateUserFolder(sSessionId, sAcrosoftUsersTypeKey, sAuthor, sUsersFolderFriendlyName, out sAppExcpXml);
                                        //Posting document to Acrosoft
                                        iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sAuthor, sAcrosoftUsersTypeKey, "", sModifiedCreateTs, sAuthor, out sAppExcpXml);// MITS 31645
                                        if (iRetCd != 0)
                                        {
                                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sDocId + "_" + sFileName, bFileContent, sDocTitle, "", sEventNumber, sClaimNumber, sAcrosoftUsersTypeKey, "", sModifiedCreateTs, "", out sAppExcpXml);// MITS 31645
                                        }

                                    }

                                    break;
                            } // switch
                            #endregion
                            writeLog("MCM upload output xml :-" + sAppExcpXml);
                            writeLog("Updating Doc transfer status in db");
                            if (iRetCd == 0 && !sAppExcpXml.StartsWith("error", StringComparison.OrdinalIgnoreCase) )
                            {
                                // updating processed file count in GUI
                                AppGlobals.iProcessedFileCount++;

                                //udating status in db
                                ExecuteSql(string.Format("UPDATE DOCUMENT SET MCM_TRANSFER_STATUS=-1 WHERE DOCUMENT_ID={0}", sDocId)); //, m_objDbConnection                                    
                            }
                            else
                            {
                                //("RecordID")
                                //("RecordNumber")
                                //("AttachedTo")
                                //("DocumentID")
                                //("DocumentName")
                                //("DocumentFileName")
                                //("DocRetrivalSuccess")
                                //("McmOutputXml")

                                AppGlobals.m_dsErroneousDocs.Tables[0].Rows.Add(
                                      AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString()
                                    , sAttachedRecord
                                    , sTableName  
                                    , sDocId
                                    , System.Security.SecurityElement.Escape(sDocTitle)
                                    , System.Security.SecurityElement.Escape(sFileName)                                    
                                    , bfileRetrivalStatus.ToString()
                                    , System.Security.SecurityElement.Escape(sAppExcpXml)
                                        );
                            }
                            bgWorkerMain.ReportProgress(0);

                            //objTempElement = objXmlDoc.CreateElement("DocumentName");
                            //objTempElement.InnerText = sFileName;
                            //if (iRetCd == 0)
                            //    objTempElement.SetAttribute("Exported", "True");
                            //else
                            //    objTempElement.SetAttribute("Exported", "False");

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            AppGlobals.m_dsErroneousDocs.Tables[0].Rows.Add(
                                      AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString()
                                    , sAttachedRecord
                                    , sTableName
                                    , sDocId
                                    , System.Security.SecurityElement.Escape(sDocTitle)
                                    , System.Security.SecurityElement.Escape(sFileName)
                                    , bfileRetrivalStatus.ToString()
                                    , System.Security.SecurityElement.Escape(sAppExcpXml)
                                        );

                            writeLog(string.Format("ERROR:[bgWorkerMain_moving_attachments -inner block] \nFileName:{0} \nDocName{1}\n Message:{2}", sFileName, sDocTitle, ex.Message.ToString()));
                        }
                    }
                }

            }
            catch (Exception exp)
            {
                //objTempElement = objXmlDoc.CreateElement("DocumentName");
                //objTempElement.InnerText = sFileName;
                //objTempElement.SetAttribute("Exported", "False");
                
                //Filename to be writeLog here  
                writeLog(string.Format("ERROR:[bgWorkerMain_moving_attachments: outer block] \nmessage:{0}\nStackTrace{1}\nFileName{2}  ", exp.Message.ToString(),  exp.StackTrace.ToString(), sFileName));
              
            }
            finally
            {
                //objTempElement.SetAttribute("AttachedRecord", sAttachedRecord);
                //objParentElement = (XmlElement)objXmlDoc.SelectSingleNode("//" + sTableName.ToLower());
                //objParentElement.AppendChild(objTempElement);
            }

            //clearing dataset holding details of Atathcments (this is unrequired now as we havealready moved them to MCM)
            writeLog("--------------------------------\nAttachment transfer completed. initiating mail merge files transfer\n--------------------------------");
            AppGlobals.m_dsDocs.Tables[0].Clear();

            #region uploading mail merge templates
            if (objAcrosoft == null)
            {
                objAcrosoft = new Acrosoft();
            }
            objAcrosoft.CreateGeneralFolder(sSessionId, sAcrosoftGeneralStorageTypeKey, "Templates", sGeneralFolderFriendlyName, out sAppExcpXml);

            for (i = 0; i < AppGlobals.m_dsMergeDocs.Tables[0].Rows.Count;i++ )
            {
                sAppExcpXml = string.Empty;
                bfileRetrivalStatus = false;    

                locker.WaitOne();

                iRetCd = 0;

                sMigrationStatus = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["MCM_TRANSFER_STATUS"].ToString();
                //doing a null check
                if (sMigrationStatus.Length > 0)
                {
                    bMigrationStatus = (Convert.ToInt32(sMigrationStatus.Trim()) == -1) ? true : false;
                    //bMigrationStatus = Convert.ToBoolean(sMigrationStatus.Trim());
                }
                else
                {
                    bMigrationStatus = false;
                }

                // this check is unrequired in case we load only doc with migration status !=0 but anyways 
                // implementing it as in case any changes are made in future
                if (!bMigrationStatus)
                {
                    try
                    {
                        sFormName = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_NAME"].ToString();
                        sFormFileName = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_FILE_NAME"].ToString();
                        sDocId = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_ID"].ToString();

                        if (AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["Size"] != null && AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["Size"].ToString().Length >0)
                        {
                            iSize = Convert.ToInt64(AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["Size"]);
                        }
                        else
                        {
                            // means that the utility was unable to get the size of the file in the first place..
                            // the utility will do a re-check ont he file....
                            // instead we could also have skiped on this file.
                            iSize = 0;
                            writeLog("possibly erroneous file: " + sFileName);
                        }
                        
                        writeLog("---------------");
                        setBgStatus("attempting file transfer :" + sFormFileName);
                        writeLog(string.Format("attempting file transfer :{0}\nDocument Name:{1}", sFormFileName, sFormName));

                        // value for updating progress bar here
                        AppGlobals.lProcessedFileSize += iSize;
                        
                        // we are now updating the merge file count only when both the merge file & its HDR
                        // quivalent has got uploaded.
                        //AppGlobals.iProcessedFileCount++;

                        iRetCd = objFileStorageManager.RetrieveFile(sFormFileName, out objFileContent);

                        if (iRetCd == 1)
                        { writeLog("Merge: File retrival successfull"); bfileRetrivalStatus = true; }
                        else
                        {
                            writeLog("Merge: File retrival failure"); bfileRetrivalStatus = false;
                        }

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFormFileName, bFileContent, sFormName, "", "Templates", sAcrosoftGeneralStorageTypeKey, "", "", sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFormFileName, bFileContent, sFormName, "", "Templates", sAcrosoftGeneralStorageTypeKey, "", "", "", out sAppExcpXml);
                            }

                        }
                       
                        //objTempElement = objXmlDoc.CreateElement("TemplateName");
                        //objTempElement.InnerText = sFormFileName;
                        //if (iRetCd == 0)
                        //    objTempElement.SetAttribute("Exported", "True");
                        //else
                        //    objTempElement.SetAttribute("Exported", "False");
                    }
                    catch (Exception exp)
                    {
                        AppGlobals.m_dsErroneousDocs.Tables[0].Rows.Add(
                                     sDocId
                                   , ""
                                   , "MailMergeDoc"
                                   , sDocId
                                   , System.Security.SecurityElement.Escape(sFormName)
                                   , System.Security.SecurityElement.Escape(sFormFileName)
                                   , bfileRetrivalStatus.ToString()
                                   , System.Security.SecurityElement.Escape(sAppExcpXml)
                                       );
                        //objTempElement = objXmlDoc.CreateElement("TemplateName");
                        //objTempElement.InnerText = sFormFileName;
                        //objTempElement.SetAttribute("Exported", "False");

                        //toDO : write filename here
                        writeLog(string.Format("ERROR:[bgWorkerMain_moving_merge_files] \nmessage:{0}\nStackTrace{1}  ", exp.Message.ToString(), exp.StackTrace.ToString()));

                    }
                    finally
                    {
                        //objParentElement = (XmlElement)objXmlDoc.SelectSingleNode("//Templates");
                        //objParentElement.AppendChild(objTempElement);
                    }

                    //Posting hdr files
                    try
                    {
                        //sFormName = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_NAME"].ToString();
                        iRetCd = 0;
                        sFormFileName = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_FILE_NAME"].ToString();

                        sFormFileName = sFormFileName.Replace(sFormFileName.Substring(sFormFileName.Length - 4, 4), ".HDR");
                        
                        writeLog("---------------");
                        setBgStatus("attempting HDR file transfer for:" + sFormFileName);
                        writeLog(string.Format("attempting HDR file transfer :{0}", sFormFileName));
                        
                        iRetCd = objFileStorageManager.RetrieveFile(sFormFileName, out objFileContent);
                        
                        if (iRetCd == 1)
                        { writeLog("Merge-hdr: File retrival successfull"); bfileRetrivalStatus = true; }
                        else
                        {
                            writeLog("Merge-hdr: File retrival failure"); bfileRetrivalStatus = false;
                        }

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            //Posting document to Acrosoft
                            iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFormFileName, bFileContent, sFormName, "", "Templates", sAcrosoftGeneralStorageTypeKey, "", "", sAuthor, out sAppExcpXml);
                            if (iRetCd != 0)
                            {
                                iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFormFileName, bFileContent, sFormName, "", "Templates", sAcrosoftGeneralStorageTypeKey, "", "", "", out sAppExcpXml);
                            }

                        }
                        //objTempElement = objXmlDoc.CreateElement("HeaderName");
                        //objTempElement.InnerText = sFormFileName;
                        //if (iRetCd == 0)
                        //    objTempElement.SetAttribute("Exported", "True");
                        //else
                        //    objTempElement.SetAttribute("Exported", "False");

                        //updating status in db only when both template & hdr file ahs got uploaded to MCM
                        if (iRetCd == 0 && !sAppExcpXml.StartsWith("error", StringComparison.OrdinalIgnoreCase))
                        {
                            AppGlobals.iProcessedFileCount++;
                            ExecuteSql(string.Format("UPDATE MERGE_FORM SET MCM_TRANSFER_STATUS=-1 WHERE FORM_ID={0}", sDocId)); //, m_objDbConnection
                        }
                        else
                        {
                            AppGlobals.m_dsErroneousDocs.Tables[0].Rows.Add(
                                     sDocId
                                   , ""
                                   , "MailMergeHDR"
                                   , sDocId
                                   , System.Security.SecurityElement.Escape(sFormName)
                                   , System.Security.SecurityElement.Escape(sFormFileName)
                                   , bfileRetrivalStatus.ToString()
                                   , System.Security.SecurityElement.Escape(sAppExcpXml)
                                       );
                        }
                        
                    }
                    catch (Exception exp)
                    {
                        AppGlobals.m_dsErroneousDocs.Tables[0].Rows.Add(
                                    sDocId
                                  , ""
                                  , "MailMergeHDR"
                                  , sDocId
                                  , System.Security.SecurityElement.Escape(sFormName)
                                  , System.Security.SecurityElement.Escape(sFormFileName)
                                  , bfileRetrivalStatus.ToString()
                                  , System.Security.SecurityElement.Escape(sAppExcpXml)
                                      );

                        //objTempElement = objXmlDoc.CreateElement("HeaderName");
                        //objTempElement.InnerText = sFormFileName;
                        //objTempElement.SetAttribute("Exported", "False");

                        //toDO : write filename here
                        writeLog(string.Format("ERROR:[bgWorkerMain_moving_merge_hdr_files] \nmessage:{0}\nStackTrace:{1}\nFileName:{2}  ", exp.Message.ToString(), exp.StackTrace.ToString(), sFormFileName));
                    }
                    finally
                    {
                        //objParentElement = (XmlElement)objXmlDoc.SelectSingleNode("//TemplateHeaders");
                        //objParentElement.AppendChild(objTempElement);
                    }

                    bgWorkerMain.ReportProgress(0);
                }
            }
            
            #endregion

          
            //if (objEvent != null)
            //    objEvent.Dispose();
            //if (objClaim != null)
            //    objClaim.Dispose();
            //if (objPI != null)
            //    objPI.Dispose();
            //if (objPolicy != null)
            //    objPolicy.Dispose();
            //if (objClaimant != null)
            //    objClaimant.Dispose();
            //if (objDefendant != null)
            //    objDefendant.Dispose();
            //if (objDatedText != null)
            //    objDatedText.Dispose();
            //if (objWitness != null)
            //    objWitness.Dispose();
            //if (objPatient != null)
            //    objPatient.Dispose();
            //if (objFileContent != null)
            //    objFileContent.Dispose();
            //if (objPolicyEnh != null)
            //    objPolicyEnh.Dispose();
            
            btnStartTransfer.Enabled = false;

        }

        private void bgWorkerPopulateFileinfo_DoWork(object sender, DoWorkEventArgs e)
        {
            //try catch to be put in retrive file info here
            int i=0;            
            long size=0;
            string sStatus = string.Empty;
            string sAuthor = "";
            string sIndiUserPath = "";
            
            System.IO.DirectoryInfo dirInfo = new DirectoryInfo(AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath.ToString());
            System.IO.DirectoryInfo dirInfoIndi = new DirectoryInfo(AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath.ToString());            
            System.IO.FileInfo[] fileNames;



            if (!AppGlobals.bUseDummyFiles && this.m_enmDocumentStorageType.ToString() == "FileSystemStorage")
            {
                #region regular transfer

                for (; i < AppGlobals.m_dsDocs.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        sStatus = AppGlobals.m_dsDocs.Tables[0].Rows[i]["MCM_TRANSFER_STATUS"].ToString();
                        sAuthor = AppGlobals.m_dsDocs.Tables[0].Rows[i]["USER_ID"].ToString();


                        //checking individual doc path here.
                        if (!AppGlobals.sdUserDocPaths.ContainsKey(sAuthor))
                        {
                            sIndiUserPath = GetSingleValue_Sql(string.Format("SELECT DOC_PATH FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '{0}'", sAuthor), m_objLogin.SecurityDsn);
                            AppGlobals.sdUserDocPaths.Add(sAuthor, sIndiUserPath);
                        }
                        else
                        {
                            sIndiUserPath = AppGlobals.sdUserDocPaths[sAuthor];
                        }

                        if (sIndiUserPath != "" && dirInfoIndi.FullName.ToLower() != @sIndiUserPath.ToLower())
                        {
                            dirInfoIndi = new DirectoryInfo(@sIndiUserPath);
                        }

                        // updating file size info here 
                        if (sStatus.Length == 0 || sStatus == "" || sStatus == "0")
                        {
                            //TO DO: additional checks for individual user's upload loc here...
                            if (sIndiUserPath != "")
                            {
                                fileNames = dirInfoIndi.GetFiles(AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_FILENAME"].ToString());
                            }
                            else
                            {
                                fileNames = dirInfo.GetFiles(AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_FILENAME"].ToString());
                            }

                            //we assume that only 1 file would be returned by the doc name
                            if (fileNames.GetLength(0) > 0)
                            {
                                size = size + fileNames[0].Length;
                                AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"] = fileNames[0].Length;

                                // rsolanki2: we are setting the value of lFileSize in each iteration coz it might be used in another thread in parallel
                                AppGlobals.lTotalFileSize = size;
                                bgWorkerPopulateFileinfo.ReportProgress(0, null);
                            }
                        }
                        else
                        {
                            AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"] = -1;
                        }
                    }
                    catch (Exception ex)
                    {
                        //skipping on file errors
                    }
                }

                // updating file info for Mail merge files
                for (i = 0; i < AppGlobals.m_dsMergeDocs.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        sStatus = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["MCM_TRANSFER_STATUS"].ToString();

                        // updating file size info here 
                        if (sStatus.Length == 0 || sStatus == "" || sStatus == "0")
                        {
                            //TO DO: additional checks for individual user's upload loc here...
                            fileNames = dirInfo.GetFiles(AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_FILE_NAME"].ToString());

                            //we assume that only 1 file would be returned by the doc name
                            if (fileNames.GetLength(0) > 0)
                            {

                                size = size + fileNames[0].Length;
                                AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["Size"] = fileNames[0].Length;

                                // rsolanki2: we are setting the value of lFileSize in each iteration coz it might be used in another thread in parallel
                                AppGlobals.lTotalFileSize = size;
                                bgWorkerPopulateFileinfo.ReportProgress(0, null);
                            }
                        }
                        else
                        {
                            AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"] = -1;
                        }
                    }
                    catch (Exception ex)
                    {
                        //skipping on file errors 
                    }
                }
                #endregion
            }
            else
            {
                #region "Document db" Or "the Dummy file transfer mode"

                
                //progressBar1.Maximum = AppGlobals.m_dsDocs.Tables[0].Rows.Count
                //    + AppGlobals.m_dsMergeDocs.Tables[0].Rows.Count;

                foreach (DataRow row in AppGlobals.m_dsDocs.Tables[0].Rows)
                {
                    row["Size"] = 1;                
                }
                foreach (DataRow row in AppGlobals.m_dsMergeDocs.Tables[0].Rows)
                {
                    row["Size"] = 1;
                }

                //txtTotalSize.Hide();
                //label15.Hide();
                
                #endregion
            }
            
        }

        private void bgWorkerPopulateFileinfo_reportProgress(object sender, ProgressChangedEventArgs e)
        {
         
            //if (AppGlobals.lTotalFileSize > 1073741824)
            //{
            //    txtTotalSize.Text = (AppGlobals.lProcessedFileSize / 1073741824).ToString() + "  GB";
            //}
            //else 
            if (AppGlobals.lTotalFileSize > 1048576)
            {
                txtTotalSize.Text = (AppGlobals.lTotalFileSize / 1048576).ToString() + "  MB";
            }
            else if (AppGlobals.lTotalFileSize > 1024)
            {
                txtTotalSize.Text = (AppGlobals.lTotalFileSize / 1024).ToString() + "  KB";
            }
            else
            {
                txtTotalSize.Text = AppGlobals.lTotalFileSize.ToString() + "  Bytes";
            }
            
            txtRemainingFilesCount.Text = AppGlobals.iProcessedFileCount.ToString();
          
        }

        private void bgWorkerMain_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int iProgressbarvalue = 0;
            if (e.ProgressPercentage == 1)
            {
                toolStripStatusLabel1.Text = AppGlobals.sStatusMessage; 
            }
            else
            {
                txtRemainingFilesCount.Text = AppGlobals.iProcessedFileCount.ToString();
                iProgressbarvalue = Convert.ToInt32(AppGlobals.lProcessedFileSize * 100 / AppGlobals.lTotalFileSize);
                if (iProgressbarvalue<=100)
	            {
                    progressBar1.Value = iProgressbarvalue;
                    toolStripStatusLabel2.Text = progressBar1.Value.ToString() + " % completed";
	            }
                writeLog("\nprocessed file count : " + AppGlobals.iProcessedFileCount);
            }

            //updating timer info
            //toolStripStatusLabel3.Text = aTimer.Interval.ToString();
        }

        private void bgWorkerMain_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //aTimer.Enabled = false;

            // if Not in silent mode then show success box
            if (!AppGlobals.bSilentMode)
            {
                btnPauseResume.Enabled = false;

                progressBar1.Value = 100;
                toolStripStatusLabel2.Text = "100 % completed";

                CreateExcelWorkbook(AppGlobals.m_dsErroneousDocs
                        , Path.Combine(Directory.GetCurrentDirectory(), "ErroneousFilesReport" + DateTime.Now.Ticks.ToString() + ".xls"));

                MessageBox.Show("Migration of docs to MCM completed.");
                setStatus("Migration completed");
                 //Changed by bsharma33 for MTIS 28132 
                if(ResetTimer!=null)
                ResetTimer.Enabled = false;
                if (countDownTimer != null)
                countDownTimer.Enabled = false;
                
                //toolStripStatusLabel3.Text = string.Empty;
                //End changes by bsharma33 for MITS 28132
            }            
            writeLog("Migration of docs to MCM completed.");

        }

        private void bgWorkerPopulateFileinfo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // only executed if debug is set to true in config file
            if (m_bIsDebugMode)
            {
                AppGlobals.m_dsDocs.WriteXml(Directory.GetCurrentDirectory() + "\\AttachDocInfoWithSizeInfo_" + DateTime.Now.Ticks.ToString() + ".xml");
                AppGlobals.m_dsMergeDocs.WriteXml(Directory.GetCurrentDirectory() + "\\MergeDocInfoWithSizeInfo_" + DateTime.Now.Ticks.ToString() + ".xml");
            }
        }
        private byte[] FileAsByteArray(string p_sFileName)
        {
            byte[] arrRet = null;
            FileStream objFStream = null;
            BinaryReader objBReader = null;
            try
            {
                objFStream = new FileStream(p_sFileName, FileMode.Open);
                objBReader = new BinaryReader(objFStream);
                arrRet = objBReader.ReadBytes((int)objFStream.Length);
            }
            catch (Exception p_objException)
            {
                writeLog("error retriving dummy file");
            }
            finally
            {
                if (objFStream != null)
                {
                    objFStream.Close();
                    objFStream = null;
                }
                if (objBReader != null)
                {
                    objBReader.Close();
                    objBReader = null;
                }
            }
            return arrRet;
        }

        private void btnPauseResume_Click(object sender, EventArgs e)
        {
            if (btnPauseResume.Text == "Pause")
            {
                locker.Reset();
                ResetTimer.Enabled = false;
                countDownTimer.Enabled = false;
                btnPauseResume.Text = "Resume";
            }
            else
            {
                locker.Set();
                ResetTimer.Enabled = true;
                countDownTimer.Enabled = true;
                btnPauseResume.Text = "Pause";
            }
        }

        public static void CreateExcelWorkbook(DataSet ds, String sExcelReportFilename)
        {
            //DataSet ds = new DataSet();
            //ds.ReadXml(@"F:\tfs_r7_ps1\Tools\MCMBulkCopy\Bin\Debug\AttachDocInfo_634409906872500000.xml");

            XmlDataDocument xmlDataDoc = new XmlDataDocument(ds);

            XslCompiledTransform xst = new XslCompiledTransform();
            
            xst.Load(Path.Combine(Directory.GetCurrentDirectory(),"Excel.xsl"));
            //string sOut = @"c:\myexcel.xls";

            //XmlWriter xw = new XmlWriter (sOut );

            StreamWriter sWriter = new StreamWriter(sExcelReportFilename);
            XmlWriter xmlWriter = XmlTextWriter.Create(sWriter);


            xst.Transform(xmlDataDoc, null, xmlWriter);
            
            sWriter.Flush();
            sWriter.Close();


        }
    }
}