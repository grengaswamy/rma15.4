using System;
using System.IO;
using System.Xml;

//using Riskmaster.Application.AcrosoftWebserviceWrapper.com.acrosoft.asdemo;
//using Riskmaster.Application.AcrosoftWebserviceWrapper.com.acrosoft.asdemo1;
//using Riskmaster.Application.AcrosoftWebserviceWrapper.com.acrosoft.asdemo2;
//using Riskmaster.Application.AcrosoftWebserviceWrapper.com.acrosoft.asdemo3;

using Riskmaster.Tools.MCMBulkCopy.com.acrosoft.asdemo;
using Riskmaster.Tools.MCMBulkCopy.com.acrosoft.asdemo1;
using Riskmaster.Tools.MCMBulkCopy.com.acrosoft.asdemo2;
using Riskmaster.Tools.MCMBulkCopy.com.acrosoft.asdemo3;

using Riskmaster.Common;

#region //10/11/06 - moved the config info to Riskmaster.config
	/*******************************************************************
	//In case the new constructors are missing from
	//tool generated refernce.cs file
	*********************************************************************
	//this constructor needs to be added in reference.cs for AuthService
		public AuthService(string url) 
		{
				this.Url = url;
		}
	// AND from existing constructor following lines need to be commented
	       public AuthService() {
				//string urlSetting = System.Configuration.ConfigurationSettings.AppSettings["AcrosoftWebserviceWrapper.com.acrosoft.asdemo.AuthService"];
				//if ((urlSetting != null)) {
				//this.Url = string.Concat(urlSetting, "");
				//}
				//else {
                this.Url = "http://asdemo.acrosoft.com/ASServicesSetup/AuthService.asmx";
				//}
        }
		
	*********************************************************************        
	//this constructor needs to be added in reference.cs for ASObjectWebService
		public ASObjectWebService(string url) 
		{
			this.Url = url;
		}

	// AND from existing constructor following lines need to be commented

		public ASObjectWebService() {
            //string urlSetting = System.Configuration.ConfigurationSettings.AppSettings["AcrosoftWebserviceWrapper.com.acrosoft.asdemo1.ASObjectWebService"];
			//if ((urlSetting != null)) {
			//this.Url = string.Concat(urlSetting, "");
			//}
			//else {
                this.Url = "http://asdemo.acrosoft.com/ASServicesSetup/ASObjectWebService.asmx";
			//            }
			}
	*********************************************************************        
	//this constructor needs to be added in reference.cs for IndexService
		public IndexService(string url) 
		{
			this.Url = url;
		}
	// AND from existing constructor following lines need to be commented
	        
			public IndexService() {
            //string urlSetting = System.Configuration.ConfigurationSettings.AppSettings["AcrosoftWebserviceWrapper.com.acrosoft.asdemo3.IndexService"];
			//if ((urlSetting != null)) {
			//this.Url = string.Concat(urlSetting, "");
			//}
			//else {
                this.Url = "http://asdemo.acrosoft.com/ASServicesSetup/IndexService.asmx";
			//}
        }
	********************************************************************/
#endregion

namespace Riskmaster.Tools.MCMBulkCopy.AcrosoftWebserviceWrapper
{
	/// <summary>
	/// This Class is a wrapper to call the Acrosoft WebServices.
	/// </summary>
	public class Acrosoft
	{
		string m_sSessionId;
        string m_sSearchData;
		
        /// <summary>
        /// Default class constructor
        /// </summary>
		public Acrosoft()
		{
			m_sSessionId = string.Empty;
            m_sSearchData = string.Empty;

            //Initiate all Acrosoft settings
            //TODO: Determine if this instantiation needs to be present in multiple classes
            //TODO: or if it can be instantiated and managed in a central location
            RMConfigurationManager.GetAcrosoftSettings();
		}
		#region Authenticate a User
		/// <summary>
		/// This method is used to authenticate a user in Acrosoft
		/// </summary>

		public int Authenticate(string p_sUserId , string p_sPassword ,out string p_sSessionId , out string p_sAppExcpXml)
		{
			string sSessionId;
			string sAppExcpXml=string.Empty;
			int iRetId = 0;
			try
			{
				//10/11/06 - moved the config info to Riskmaster.config
				//Pls see note on top in case the new constructor is missing from
				//tool generated refernce.cs file
                AuthService authService = new AuthService();
                //Configure the URL dynamically using the publicly exposed Url property
                //rather than overloading the constructor (which causes VS.Net 2008 to fail the build)
                authService.Url = AcrosoftSection.AuthService;
				iRetId = authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out sAppExcpXml);
                if (string.IsNullOrEmpty(sSessionId))
                {
                    Log.Write("MCM Authentication failed with message: " + sAppExcpXml);
                }
				p_sSessionId = sSessionId;
				p_sAppExcpXml = sAppExcpXml;
				
			}
			catch(Exception e)
			{
				p_sSessionId = "";
                p_sAppExcpXml = "Error" + sAppExcpXml;
                Log.Write("MCM Authentication failed with message: " + e.ToString());

			}
			return iRetId;
			
		}

		#endregion
		#region Store Object In Acrosoft
		/// <summary>
		/// These methods are overloads to store an object in Acrosoft
		/// </summary>
		public int StoreObjectBuffer(string p_sUserId , string p_sPassword , string p_sFileName , string p_sDocTitle , string p_sDocType , string p_sFolderKey, string p_sTypeKey , string p_sSubFolder , string p_sCreateTS , string sAuthId , out string p_sAppExcpXml)
		{
			string strStoreObjectXml = "";
			int iRetCd = 0;
			string sSessionId = "";
			string sAppExcpXml = "";
			byte[] fileBuffer = null;
						
			try
			{
				iRetCd = GetFileBuffer(p_sFileName , ref fileBuffer);
				
				strStoreObjectXml = GetStoreObjectXml(p_sFileName , p_sDocTitle , p_sDocType , p_sFolderKey , p_sTypeKey , p_sSubFolder , p_sCreateTS , sAuthId);

				//10/11/06 - moved the config info to Riskmaster.config
				//AuthService authService = new AuthService();
				AuthService authService = new AuthService();//RMConfigurator.Value("AuthService"));
				//08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                //gagnihotri R5 MCM changes 
                //authService.Url = RMConfigurator.Value("AuthService");
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out sAppExcpXml);
				if(sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//ASObjectWebService objectService = new ASObjectWebService();
					ASObjectWebService objectService = new ASObjectWebService();//RMConfigurator.Value("ASObjectWebService"));
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objectService.Url = RMConfigurator.Value("ASObjectWebService");
                    objectService.Url = AcrosoftSection.ASObjectWebService;
                    iRetCd = objectService.StoreObjectBuffer(sSessionId , fileBuffer , ref strStoreObjectXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int StoreObjectBuffer(string p_sSessionId , string p_sFileName , string p_sDocTitle , string p_sDocType , string p_sFolderKey, string p_sTypeKey , string p_sSubFolder , string p_sCreateTS , string sAuthId , out string p_sAppExcpXml)
		{
			string strStoreObjectXml = "";
			int iRetCd = 0;
			string sAppExcpXml = "";
			byte[] fileBuffer = null;
						
			try
			{
				iRetCd = GetFileBuffer(p_sFileName , ref fileBuffer);
				
				strStoreObjectXml = GetStoreObjectXml(p_sFileName , p_sDocTitle , p_sDocType , p_sFolderKey , p_sTypeKey , p_sSubFolder , p_sCreateTS , sAuthId);

				if(p_sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//ASObjectWebService objectService = new ASObjectWebService();
					ASObjectWebService objectService = new ASObjectWebService();//RMConfigurator.Value("ASObjectWebService"));
                    //gagnihotri R5 MCM changes
                    //objectService.Url = RMConfigurator.Value("ASObjectWebService");
                    objectService.Url = AcrosoftSection.ASObjectWebService;
                    iRetCd = objectService.StoreObjectBuffer(p_sSessionId , fileBuffer , ref strStoreObjectXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int StoreObjectBuffer(string p_sUserId , string p_sPassword , string p_sFileName , byte[] p_sFileBuffer , string p_sDocTitle , string p_sDocType , string p_sFolderKey, string p_sTypeKey , string p_sSubFolder , string p_sCreateTS, string sAuthId , out string p_sAppExcpXml)
		{
			string strStoreObjectXml = "";
			int iRetCd = 0;
			string sSessionId = "";
			string sAppExcpXml = "";
									
			try
			{
				strStoreObjectXml = GetStoreObjectXml(p_sFileName , p_sDocTitle , p_sDocType , p_sFolderKey , p_sTypeKey , p_sSubFolder , p_sCreateTS , sAuthId);
				//10/11/06 - moved the config info to Riskmaster.config
				//AuthService authService = new AuthService();
				AuthService authService = new AuthService();//RMConfigurator.Value("AuthService"));
                //08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                //gagnihotri R5 MCM changes
                //authService.Url = RMConfigurator.Value("AuthService");
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out sAppExcpXml);
				if(sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//ASObjectWebService objectService = new ASObjectWebService();
					ASObjectWebService objectService = new ASObjectWebService();//RMConfigurator.Value("ASObjectWebService"));
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objectService.Url = RMConfigurator.Value("ASObjectWebService");
                    objectService.Url = AcrosoftSection.ASObjectWebService;
                    iRetCd = objectService.StoreObjectBuffer(sSessionId , p_sFileBuffer , ref strStoreObjectXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int StoreObjectBuffer(string p_sSessionId , string p_sFileName , byte[] p_sFileBuffer , string p_sDocTitle , string p_sDocType , string p_sFolderKey, string p_sTypeKey , string p_sSubFolder , string p_sCreateTS, string sAuthId , out string p_sAppExcpXml)
		{
			string strStoreObjectXml = "";
			int iRetCd = 0;
			string sAppExcpXml = "";
									
			try
			{
				strStoreObjectXml = GetStoreObjectXml(p_sFileName , p_sDocTitle , p_sDocType , p_sFolderKey , p_sTypeKey , p_sSubFolder , p_sCreateTS , sAuthId);

				if(p_sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//ASObjectWebService objectService = new ASObjectWebService();
					ASObjectWebService objectService = new ASObjectWebService();//RMConfigurator.Value("ASObjectWebService"));
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objectService.Url = RMConfigurator.Value("ASObjectWebService");
                    objectService.Url = AcrosoftSection.ASObjectWebService;
                    iRetCd = objectService.StoreObjectBuffer(p_sSessionId , p_sFileBuffer , ref strStoreObjectXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int StoreObjectBuffer(string p_sUserId , string p_sPassword , string p_sFileName , string p_sDocTitle , string p_sDocType , string p_sEventNumber , string p_sClaimNumber, string p_sTypeKey , string p_sSubFolder , string p_sCreateTS, string sAuthId , out string p_sAppExcpXml)
		{
			string strStoreObjectXml = "";
			int iRetCd = 0;
			string sSessionId = "";
			string sAppExcpXml = "";
			byte[] fileBuffer = null;
			string sFolderKey = "";
						
			try
			{
				iRetCd = GetFileBuffer(p_sFileName , ref fileBuffer);
				
				if (p_sEventNumber=="") p_sEventNumber = "0";
				if (p_sClaimNumber=="") p_sClaimNumber = "0";
				
				sFolderKey = p_sEventNumber.ToString() + '|' + p_sClaimNumber.ToString();
				
				strStoreObjectXml = GetStoreObjectXml(p_sFileName , p_sDocTitle , p_sDocType , sFolderKey , p_sTypeKey , p_sSubFolder , p_sCreateTS , sAuthId);
				//10/11/06 - moved the config info to Riskmaster.config
				//AuthService authService = new AuthService();
				AuthService authService = new AuthService();//RMConfigurator.Value("AuthService"));
                //08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                //gagnihotri R5 MCM changes
                //authService.Url = RMConfigurator.Value("AuthService");
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out sAppExcpXml);
				if(sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//ASObjectWebService objectService = new ASObjectWebService();
					ASObjectWebService objectService = new ASObjectWebService();//RMConfigurator.Value("ASObjectWebService"));
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objectService.Url = RMConfigurator.Value("ASObjectWebService");
                    objectService.Url = AcrosoftSection.ASObjectWebService;
                    iRetCd = objectService.StoreObjectBuffer(sSessionId , fileBuffer , ref strStoreObjectXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int StoreObjectBuffer(string p_sUserId , string p_sPassword , string p_sFileName , byte[] p_fileBuffer , string p_sDocTitle , string p_sDocType , string p_sEventNumber , string p_sClaimNumber, string p_sTypeKey , string p_sSubFolder , string p_sCreateTS, string sAuthId , out string p_sAppExcpXml)
		{
			string strStoreObjectXml = "";
			int iRetCd = 0;
			string sSessionId = "";
			string sAppExcpXml = "";
			string sFolderKey = "";
						
			try
			{
				if (p_sEventNumber=="") p_sEventNumber = "0";
				if (p_sClaimNumber=="") p_sClaimNumber = "0";
				
				sFolderKey = p_sEventNumber.ToString() + '|' + p_sClaimNumber.ToString();
				
				strStoreObjectXml = GetStoreObjectXml(p_sFileName , p_sDocTitle , p_sDocType , sFolderKey , p_sTypeKey , p_sSubFolder , p_sCreateTS , sAuthId);
				//10/11/06 - moved the config info to Riskmaster.config
				//AuthService authService = new AuthService();
				AuthService authService = new AuthService();//RMConfigurator.Value("AuthService"));
                //08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                //gagnihotri R5 MCM changes
                //authService.Url = RMConfigurator.Value("AuthService");
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out sAppExcpXml);
				if(sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//ASObjectWebService objectService = new ASObjectWebService();
					ASObjectWebService objectService = new ASObjectWebService();//RMConfigurator.Value("ASObjectWebService"));
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objectService.Url = RMConfigurator.Value("ASObjectWebService");
                    objectService.Url = AcrosoftSection.ASObjectWebService;
                    iRetCd = objectService.StoreObjectBuffer(sSessionId , p_fileBuffer , ref strStoreObjectXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int StoreObjectBuffer(string p_sSessionId , string p_sFileName , byte[] p_fileBuffer , string p_sDocTitle , string p_sDocType , string p_sEventNumber , string p_sClaimNumber, string p_sTypeKey , string p_sSubFolder , string p_sCreateTS, string sAuthId , out string p_sAppExcpXml)
		{
			string strStoreObjectXml = "";
			int iRetCd = 0;
			string sAppExcpXml = "";
			string sFolderKey = "";
						
			try
			{
				if (p_sEventNumber=="") p_sEventNumber = "0";
				if (p_sClaimNumber=="") p_sClaimNumber = "0";
				
				sFolderKey = p_sEventNumber.ToString() + '|' + p_sClaimNumber.ToString();
				
				strStoreObjectXml = GetStoreObjectXml(p_sFileName , p_sDocTitle , p_sDocType , sFolderKey , p_sTypeKey , p_sSubFolder , p_sCreateTS , sAuthId);

				if(p_sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//ASObjectWebService objectService = new ASObjectWebService();
					ASObjectWebService objectService = new ASObjectWebService();//RMConfigurator.Value("ASObjectWebService"));
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objectService.Url = RMConfigurator.Value("ASObjectWebService");
                    objectService.Url = AcrosoftSection.ASObjectWebService;
                    iRetCd = objectService.StoreObjectBuffer(p_sSessionId , p_fileBuffer , ref strStoreObjectXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
                p_sAppExcpXml = "Error:" + sAppExcpXml;
			}
			return iRetCd;

		}

#endregion
		#region Private Methods
		private string GetStoreObjectXml(string p_sFileName , string p_sDocTitle , string p_sDocType , string p_sFolderKey, string p_sTypeKey , string p_sSubFolder , string p_sCreateTS , string p_sAuthId)
		{
			string strStoreObjectXml = "";

            //rsolanki2: string concat optimizations 
            strStoreObjectXml = "<StoreObjectData>"
                + ((p_sFileName.Length > 0)     ? ("<FileName>" + System.Security.SecurityElement.Escape(p_sFileName) + "</FileName>") : string.Empty)
                + ((p_sDocTitle.Length > 0)     ? ("<DocTitle>" + System.Security.SecurityElement.Escape(p_sDocTitle) + "</DocTitle>") : string.Empty)
                + ((p_sDocType.Length > 0)      ? ("<DocType>" + System.Security.SecurityElement.Escape(p_sDocType) + "</DocType>") : string.Empty)
                + ((p_sFolderKey.Length > 0)    ? ("<FolderKey>" + System.Security.SecurityElement.Escape(p_sFolderKey) + "</FolderKey>") : string.Empty)
                + ((p_sCreateTS.Length > 0)     ? ("<CreateTs>" + System.Security.SecurityElement.Escape(p_sCreateTS) + "</CreateTs>") : string.Empty)
                + ((p_sAuthId.Length > 0)       ? ("<AuthId>" + System.Security.SecurityElement.Escape(p_sAuthId) + "</AuthId>") : string.Empty)
                + ((p_sTypeKey.Length > 0)      ? ("<TypeKey>" + System.Security.SecurityElement.Escape(p_sTypeKey) + "</TypeKey>") : string.Empty)
                + ((p_sSubFolder.Length > 0)    ? ("<SubFolder>" + System.Security.SecurityElement.Escape(p_sSubFolder) + "</SubFolder>") : string.Empty)
                + "</StoreObjectData>";


            //strStoreObjectXml = "<StoreObjectData>";
				
            //if (p_sFileName.Length > 0)
            //{
            //    strStoreObjectXml += "<FileName>" +
            //        System.Security.SecurityElement.Escape(p_sFileName) +
            //        "</FileName>";
            //}

            //if (p_sDocTitle.Length > 0)
            //{
            //    strStoreObjectXml += "<DocTitle>" +
            //        System.Security.SecurityElement.Escape(p_sDocTitle) +
            //        "</DocTitle>";
            //}

            //if (p_sDocType.Length > 0)
            //{
            //    strStoreObjectXml += "<DocType>" +
            //        p_sDocType +
            //        "</DocType>";
            //}

            //if (p_sFolderKey.Length > 0)
            //{
            //    strStoreObjectXml += "<FolderKey>" +
            //        p_sFolderKey +
            //        "</FolderKey>";
            //}

            //if (p_sCreateTS.Length > 0)
            //{
            //    strStoreObjectXml += "<CreateTs>" +
            //        p_sCreateTS +
            //        "</CreateTs>";
            //}

            //if (p_sAuthId.Length > 0)
            //{
            //    strStoreObjectXml += "<AuthId>" +
            //        p_sAuthId +
            //        "</AuthId>";
            //}

            ///*
            //    if (p_sCabinetID.Length > 0)
            //    {
            //        strStoreObjectXml += "<CabinetId>" +
            //            txtCabinetID.Text +
            //            "</CabinetId>";
            //    }

            //    if (txtIndexClassID.Text.Length > 0)
            //    {
            //        strStoreObjectXml += "<IndexClassId>" +
            //            txtIndexClassID.Text +
            //            "</IndexClassId>";
            //    }
            //    */

            //if (p_sTypeKey.Length > 0)
            //{
            //    strStoreObjectXml += "<TypeKey>" +
            //        p_sTypeKey +
            //        "</TypeKey>";
            //}

            //if (p_sSubFolder.Length > 0)
            //{
            //    strStoreObjectXml += "<SubFolder>" +
            //        p_sSubFolder +
            //        "</SubFolder>";
            //}

            ///*
            //    if (txtCreateTs.Text.Length > 0)
            //    {
            //        strStoreObjectXml += "<CreateTs>" +
            //            txtCreateTs.Text +
            //            "</CreateTs>";
            //    }
            //    */

            ////			if (txtMediaID.Text.Length > 0)
            ////			{
            ////				strStoreObjectXml += "<MediaId>" +
            ////					txtMediaID.Text	+
            ////					"</MediaId>";
            ////			}

            //strStoreObjectXml += "</StoreObjectData>";

            return strStoreObjectXml;
		}
		private int GetFileBuffer(string p_sFileName , ref byte[] fileBuffer)
		{
			FileStream		fileStream;
			BinaryReader	binaryRdr;
			int				iRetCd = 42;
			
			if (File.Exists(p_sFileName))
			{
				try
				{
					// Open existing for read only
					fileStream	= File.OpenRead(p_sFileName);
					binaryRdr	= new BinaryReader(fileStream);
					fileBuffer	= binaryRdr.ReadBytes(Convert.ToInt32(fileStream.Length));

					binaryRdr.Close();
					fileStream.Close();
					iRetCd = 0;
				}
				catch (IOException e)
				{
					// As necessary we can add more specific responses 
					// to the system exceptions.
					string strMessage = "There was an IO Error retrieving file [" +
						p_sFileName + "].";

				}
				catch (Exception e)
				{
					string strMessage = "There was an Error reading the file [" +
						p_sFileName + "].";

				}
			}
			else
			{
				string strMessage = "The file was not found at [" +
					p_sFileName + "].";

			}

			return iRetCd;
		}

		private void WriteBinaryFile(string fileName, byte[] fileBuffer)
		{
			FileStream		fileStream;
			BinaryWriter	binaryWrtr;

			try
			{
				// create new or overwrite existing
				fileStream	= new FileStream(fileName, FileMode.Create);
				binaryWrtr	= new BinaryWriter(fileStream);
				binaryWrtr.Write(fileBuffer);

				binaryWrtr.Close();
				fileStream.Close();
			}
			catch (Exception ex)
			{
				
			}
		}
		
#endregion
		#region Fetch Objects From Acrosoft
        public int SearchFolder(string p_sUserId, string p_sPassword, string p_sEventno, string p_sClaimNo, string p_sGeneralFolderFriendlyName, out string p_sAppExcpXml)
        {
            int iRetCd = 0;
            string sFolderKey = string.Empty;
            string sSessionId = "";
            string sFolderData = "";
            string p_sXmlDoc = "";
            ASBaseWebService objService;
            //ASBaseWebService objser;

            try
            {

                p_sAppExcpXml = "";
                if (p_sClaimNo == "P")
                    sFolderKey = "RMXPOL|" + p_sEventno;
                else
                    sFolderKey = "RMXEVNT|" + p_sEventno + "|" + p_sClaimNo + "";

                sFolderData = "<BaseData><CabinetId>RMXCABID</CabinetId><FolderId></FolderId><FolderKey>" + sFolderKey + "</FolderKey></BaseData>";

                AuthService authService = new AuthService();//RMConfigurator.Value("AuthService"));
                //08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                //gagnihotri R5 MCM changes
                //authService.Url = RMConfigurator.Value("AuthService");
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId, p_sPassword, out sSessionId, out p_sAppExcpXml);
                if (sSessionId != "")
                {

                    objService = new ASBaseWebService();//RMConfigurator.Value("IndexService"));


                    objService.Url = AcrosoftSection.ASBaseWebService;

                    iRetCd = objService.GetAppFolder(sSessionId, sFolderData, out p_sXmlDoc, out p_sAppExcpXml);
                }
                else p_sAppExcpXml = "Error";


            }
            catch (Exception e)
            {
                p_sAppExcpXml = "Error";
                p_sXmlDoc = "";
            }
            return iRetCd;

        }
        //skhare7  MITS 23131
    
      
        //skhare7:MITS 23407
        public int GetAttachmentFolderDocumentList(string p_sUserId, string p_sPassword, string p_sEventno, string p_sClaimNo, string p_sEventFolderFriendlyName, string p_sClaimFolderFriendlyName, out string p_sXmlDoc, out string p_sAppExcpXml)
        {
            int iRetCd = 0;
        
            string sSessionId = "";
            string sFolderData = "";
            string sSearchData = "";
            IndexService objService;
       
            try
            {
                p_sXmlDoc = "";
                p_sAppExcpXml = "";
                sFolderData ="<NDXData><IndexName>Riskmaster</IndexName></NDXData>";
                sSearchData = "<ASNDXSearchInput><" + p_sEventFolderFriendlyName + ">" + p_sEventno + "</"+ p_sEventFolderFriendlyName  +"><"+p_sClaimFolderFriendlyName +">"+ p_sClaimNo + "</"+p_sClaimFolderFriendlyName+"></ASNDXSearchInput>";

                AuthService authService = new AuthService();//RMConfigurator.Value("AuthService"));
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId, p_sPassword, out sSessionId, out p_sAppExcpXml);
                if (sSessionId != "")
                {
                 
                    objService = new IndexService();//RMConfigurator.Value("IndexService"));
                 
                    objService.Url = AcrosoftSection.IndexService;
                    ASBaseWebService objser = new ASBaseWebService();
                    objser.Url = AcrosoftSection.ASBaseWebService;
                iRetCd = objService.GetIndexFolderDocs(sSessionId, sFolderData, sSearchData, out p_sXmlDoc, out p_sAppExcpXml);
                 
                }
                else p_sAppExcpXml = "Error";


            }
            catch (Exception e)
            {
                p_sAppExcpXml = "Error";
                p_sXmlDoc = "";
            }
            return iRetCd;

        }
        //skhare7:MITS 23407 End
		public int GetGeneralFolderDocumentList(string p_sUserId , string p_sPassword , string p_sFolderKey , string p_sAcrosoftGeneralStorageTypeKey , string p_sGeneralFolderFriendlyName , out string p_sXmlDoc , out string p_sAppExcpXml)
		{
			int iRetCd = 0;
     
			string sSessionId = "";
			string sFolderData = "";
			string sSearchData="";
			IndexService objService;

									
			try
			{
				p_sXmlDoc = "";	
				p_sAppExcpXml = "";
				sFolderData = "<NDXData><IndexName>" + p_sAcrosoftGeneralStorageTypeKey + "</IndexName>" + "</NDXData>";
				sSearchData = "<ASNDXSearchInput><" + p_sGeneralFolderFriendlyName +  ">" + p_sFolderKey + "</" + p_sGeneralFolderFriendlyName + "></ASNDXSearchInput>";
                
                //Raman Bhatia 07/14/2008
                //Let the search data be set from module level variable incase Friendly Name has not been sent
                if (p_sGeneralFolderFriendlyName == "")
                {
                    sSearchData = m_sSearchData;
                }
                
				//10/11/06 - moved the config info to Riskmaster.config
				//AuthService authService = new AuthService();
				AuthService authService = new AuthService();//RMConfigurator.Value("AuthService"));
                //08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                //gagnihotri R5 MCM changes
                //authService.Url = RMConfigurator.Value("AuthService");
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out p_sAppExcpXml);
				if(sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//objService = new IndexService();
					objService = new IndexService();//RMConfigurator.Value("IndexService"));
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objService.Url = RMConfigurator.Value("IndexService");
                    objService.Url = AcrosoftSection.IndexService;
                    iRetCd = objService.GetIndexFolderDocs(sSessionId, sFolderData, sSearchData, out p_sXmlDoc, out p_sAppExcpXml);
                 
				}
				else p_sAppExcpXml = "Error";

				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
				p_sXmlDoc = "";
			}
			return iRetCd;
		}
      

      

        public string GetGlobalSearchCriteria
        {
            get
            {
                return m_sSearchData;
            }
            set
            {
                m_sSearchData = value;
            }
		}

		public int FetchGeneralFolderDocumentContent(string p_sUserId , string p_sPassword , string p_sFileName , string p_sFolderKey , string p_sAcrosoftGeneralStorageTypeKey , string p_sGeneralFolderFriendlyName , out byte[] fileBuffer , out string p_sAppExcpXml)
		{
			int iRetCd = 0;
			string sAppExcpXml = "";
			string sXmlDoc = "";
			string sSessionID = "";
			fileBuffer = null;
			string sRetrieveObjectXml = "";
			p_sAppExcpXml = "";
			string sMediaID = "";
			XmlDocument xmlDocument = null;
			XmlNodeList objXmlNameNodeList = null;
			XmlNodeList objXmlIDNodeList = null;
			ASObjectWebService objService = null;
						
			try
			{
				iRetCd = GetGeneralFolderDocumentList(p_sUserId , p_sPassword , p_sFolderKey , p_sAcrosoftGeneralStorageTypeKey , p_sGeneralFolderFriendlyName , out sXmlDoc , out sAppExcpXml);
				if(iRetCd == 0)
				{
					xmlDocument = new XmlDocument();
					xmlDocument.LoadXml(sXmlDoc);
					objXmlNameNodeList = xmlDocument.SelectNodes("//Name");
					objXmlIDNodeList = xmlDocument.SelectNodes("//ID");
					for (int i = 0 ; i< objXmlNameNodeList.Count ; i++)
					{
						if(p_sFileName.ToLower() == objXmlNameNodeList[i].InnerText.ToLower())
						{
							sMediaID = objXmlIDNodeList[i].InnerText;
						} // if

					} // for
					if(sMediaID != "")
					{
						iRetCd = Authenticate(p_sUserId , p_sPassword , out sSessionID , out sAppExcpXml);
						sRetrieveObjectXml = "<RetrieveObjectData><FileName></FileName><MediaId>" + sMediaID + "</MediaId></RetrieveObjectData>";
						//10/11/06 - moved the config info to Riskmaster.config
						//objService = new ASObjectWebService();
						objService = new ASObjectWebService();//RMConfigurator.Value("ASObjectWebService"));
                        //08/22/2008 Raman Bhatia
                        //We need to set the url as per Riskmaster.Config
                        //gagnihotri R5 MCM changes
                        //objService.Url = RMConfigurator.Value("ASObjectWebService");
                        objService.Url = AcrosoftSection.ASObjectWebService;
                        iRetCd = objService.RetrieveObjectBuffer(sSessionID , ref sRetrieveObjectXml , out fileBuffer , out sAppExcpXml);
					} // if

					return iRetCd;

				} // if
				else
				{
					return iRetCd;
				} // else

			}
			
		
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
				sXmlDoc = "";
				sMediaID = "";
			}
			return iRetCd;
		}
   
		public int FetchAndWriteGeneralFolderDocument(string p_sUserId , string p_sPassword , string p_sFileName , string p_sFolderKey , string p_sAcrosoftGeneralStorageTypeKey , string p_sGeneralFolderFriendlyName , string p_sFullTargetFileName , out string p_sAppExcpXml)
		{
			int iRetCd = 0;
			string sAppExcpXml = "";
			string sXmlDoc = "";
			string sSessionID = "";
			byte[] fileBuffer = null;
			string sRetrieveObjectXml = "";
			p_sAppExcpXml = "";
			string sMediaID = "";
			XmlDocument xmlDocument = null;
			XmlNodeList objXmlNameNodeList = null;
			XmlNodeList objXmlIDNodeList = null;
			ASObjectWebService objService = null;
						
			try
			{
				iRetCd = GetGeneralFolderDocumentList(p_sUserId , p_sPassword , p_sFolderKey , p_sAcrosoftGeneralStorageTypeKey , p_sGeneralFolderFriendlyName , out sXmlDoc , out sAppExcpXml);
				if(iRetCd == 0)
				{
					xmlDocument = new XmlDocument();
					xmlDocument.LoadXml(sXmlDoc);
					objXmlNameNodeList = xmlDocument.SelectNodes("//Name");
					objXmlIDNodeList = xmlDocument.SelectNodes("//ID");
					for (int i = 0 ; i< objXmlNameNodeList.Count ; i++)
					{
						if(p_sFileName.ToLower() == objXmlNameNodeList[i].InnerText.ToLower())
						{
							sMediaID = objXmlIDNodeList[i].InnerText;
						} // if

					} // for
					if(sMediaID != "")
					{
						iRetCd = Authenticate(p_sUserId , p_sPassword , out sSessionID , out sAppExcpXml);
						sRetrieveObjectXml = "<RetrieveObjectData><FileName></FileName><MediaId>" + sMediaID + "</MediaId></RetrieveObjectData>";
						//10/11/06 - moved the config info to Riskmaster.config
						//objService = new ASObjectWebService();
						objService = new ASObjectWebService();//RMConfigurator.Value("ASObjectWebService"));
                        //08/22/2008 Raman Bhatia
                        //We need to set the url as per Riskmaster.Config
                        //gagnihotri R5 MCM changes
                        //objService.Url = RMConfigurator.Value("ASObjectWebService");
                        objService.Url = AcrosoftSection.ASObjectWebService;
                        iRetCd = objService.RetrieveObjectBuffer(sSessionID , ref sRetrieveObjectXml , out fileBuffer , out sAppExcpXml);
						WriteBinaryFile(p_sFullTargetFileName , fileBuffer);
					} // if

					return iRetCd;

				} // if
				else
				{
					return iRetCd;
				} // else

			}
			
		
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
				sXmlDoc = "";
				sMediaID = "";
			}
			return iRetCd;
		}

		#endregion
        //skhare7:MITS 23407 Start
        public int DeletePDF(string p_sUserId, string p_sPassword, string p_sFileName, string p_sEventNo, string p_sClaimNo, string p_sEventFriendlyName,string p_sClaimFriendlyName, out string p_sAppExcpXml)
        {
            int iRetCd = 0;
            string sAppExcpXml = "";
            string sXmlDoc = "";
            string sSessionID = "";
            string sNDXDeleteObjData = "";
            p_sAppExcpXml = "";
            string sMediaID = "";
            XmlDocument xmlDocument = null;
            XmlNodeList objXmlNameNodeList = null;
            XmlNodeList objXmlIDNodeList = null;
            IndexService objIndexService = null;

            try
            {

                iRetCd = GetAttachmentFolderDocumentList(p_sUserId, p_sPassword, p_sEventNo, p_sClaimNo, p_sEventFriendlyName,p_sClaimFriendlyName, out sXmlDoc, out sAppExcpXml);
                if (iRetCd == 0)
                {
                    xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(sXmlDoc);
                    objXmlNameNodeList = xmlDocument.SelectNodes("//Name");
                    objXmlIDNodeList = xmlDocument.SelectNodes("//ID");
                    for (int i = 0; i < objXmlNameNodeList.Count; i++)
                    {
                        if (p_sFileName.ToLower() == objXmlNameNodeList[i].InnerText.ToLower())
                        {
                            sMediaID = objXmlIDNodeList[i].InnerText;
                        } // if

                    } // for
                    if (sMediaID != "")
                    {
                        iRetCd = Authenticate(p_sUserId, p_sPassword, out sSessionID, out sAppExcpXml);
                        sNDXDeleteObjData = "<NDXDeleteObjData><MediaId>" + sMediaID + "</MediaId></NDXDeleteObjData>";
                      
                        objIndexService = new IndexService();//RMConfigurator.Value("IndexService"));
                        
                        objIndexService.Url = AcrosoftSection.IndexService;
                        iRetCd = objIndexService.DeleteDocument(sSessionID, sNDXDeleteObjData, out sAppExcpXml);
                    } // if

                    return iRetCd;

                } // if
                else
                {
                    return iRetCd;
                } // else

            }


            catch (Exception e)
            {
                p_sAppExcpXml = "Error";
                sXmlDoc = "";
                sMediaID = "";
            }
            return iRetCd;
        }
        //skhare7:MITS 23407 End
		#region Delete Object in Acrosoft
		public int DeleteFile(string p_sUserId , string p_sPassword , string p_sFileName , string p_sFolderKey , string p_sTypeKey , string p_sFriendlyName , out string p_sAppExcpXml)
		{
			int iRetCd = 0;
			string sAppExcpXml = "";
			string sXmlDoc = "";
			string sSessionID = "";
			string sNDXDeleteObjData = "";
			p_sAppExcpXml = "";
			string sMediaID = "";
			XmlDocument xmlDocument = null;
			XmlNodeList objXmlNameNodeList = null;
			XmlNodeList objXmlIDNodeList = null;
			IndexService objIndexService = null;
						
			try
			{
				iRetCd = GetGeneralFolderDocumentList(p_sUserId , p_sPassword , p_sFolderKey , p_sTypeKey , p_sFriendlyName , out sXmlDoc , out sAppExcpXml);
				if(iRetCd == 0)
				{
					xmlDocument = new XmlDocument();
					xmlDocument.LoadXml(sXmlDoc);
					objXmlNameNodeList = xmlDocument.SelectNodes("//Name");
					objXmlIDNodeList = xmlDocument.SelectNodes("//ID");
					for (int i = 0 ; i< objXmlNameNodeList.Count ; i++)
					{
						if(p_sFileName.ToLower() == objXmlNameNodeList[i].InnerText.ToLower())
						{
							sMediaID = objXmlIDNodeList[i].InnerText;
						} // if

					} // for
					if(sMediaID != "")
					{
						iRetCd = Authenticate(p_sUserId , p_sPassword , out sSessionID , out sAppExcpXml);
						sNDXDeleteObjData = "<NDXDeleteObjData><MediaId>" + sMediaID + "</MediaId></NDXDeleteObjData>";
						//10/11/06 - moved the config info to Riskmaster.config
						//objIndexService = new IndexService();
						objIndexService = new IndexService();//RMConfigurator.Value("IndexService"));
                        //08/22/2008 Raman Bhatia
                        //We need to set the url as per Riskmaster.Config
                        //gagnihotri R5 MCM changes
                        //objIndexService.Url = RMConfigurator.Value("IndexService");
                        objIndexService.Url = AcrosoftSection.IndexService;
                        iRetCd = objIndexService.DeleteDocument(sSessionID , sNDXDeleteObjData , out sAppExcpXml);
					} // if

					return iRetCd;

				} // if
				else
				{
					return iRetCd;
				} // else

			}
			
		
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
				sXmlDoc = "";
				sMediaID = "";
			}
			return iRetCd;
		}

		#endregion
		#region Create Folders In Acrosoft
		public int CreateAttachmentFolder(string p_sUserId , string p_sPassword , string p_sAcrosoftAttachmentsTypeKey , string p_sEventNumber , string p_sClaimNumber, string p_sEventFolderFriendlyName , string p_sClaimFolderFriendlyName , out string p_sAppExcpXml)
		{
			int iRetCd = 0;
			string sSessionId = "";
			string sAppExcpXml = "";
						
			try
			{
				//10/11/06 - moved the config info to Riskmaster.config
				//AuthService authService = new AuthService();
				AuthService authService = new AuthService();
                //08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out sAppExcpXml);
				if(sSessionId!="")
				{
					iRetCd = CreateAttachmentFolder(sSessionId , p_sAcrosoftAttachmentsTypeKey , p_sEventNumber , p_sClaimNumber , p_sEventFolderFriendlyName , p_sClaimFolderFriendlyName , out sAppExcpXml);

					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int CreateAttachmentFolder(string p_sSessionId , string p_sAcrosoftAttachmentsTypeKey , string p_sEventNumber , string p_sClaimNumber, string p_sEventFolderFriendlyName , string p_sClaimFolderFriendlyName , out string p_sAppExcpXml)
		{
			string strNDXMakeAppFldDataXml = "";
			int iRetCd = 0;
			string sAppExcpXml = "";
			IndexService objService = null;
						
			try
			{
				if (p_sEventNumber=="") p_sEventNumber = "0";
				if (p_sClaimNumber=="") p_sClaimNumber = "0";
				
				strNDXMakeAppFldDataXml = "<NDXMakeAppFldData><IndexName>" + p_sAcrosoftAttachmentsTypeKey + "</IndexName><" + p_sEventFolderFriendlyName + ">" 
					+ p_sEventNumber + "</" + p_sEventFolderFriendlyName + "><" + p_sClaimFolderFriendlyName + ">" 
					+ p_sClaimNumber + "</" + p_sClaimFolderFriendlyName + "></NDXMakeAppFldData>";	

				if(p_sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//objService = new IndexService();
					objService = new IndexService();
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    objService.Url = AcrosoftSection.IndexService;
                    iRetCd = objService.CreateFolder(p_sSessionId , strNDXMakeAppFldDataXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			finally
			{
				objService = null;
			} // finally

			return iRetCd;

		}

		public int CreateUserFolder(string p_sUserId , string p_sPassword , string p_sAcrosoftUsersTypeKey , string p_sUser , string p_sUsersFolderFriendlyName , out string p_sAppExcpXml)
		{
			int iRetCd = 0;
			string sSessionId = "";
			string sAppExcpXml = "";
						
			try
			{
				//10/11/06 - moved the config info to Riskmaster.config
				//AuthService authService = new AuthService();
				AuthService authService = new AuthService();
                //08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out sAppExcpXml);
				if(sSessionId!="")
				{
					iRetCd = CreateUserFolder(sSessionId , p_sAcrosoftUsersTypeKey , p_sUser , p_sUsersFolderFriendlyName , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int CreateUserFolder(string p_sSessionId , string p_sAcrosoftUsersTypeKey , string p_sUser , string p_sUsersFolderFriendlyName , out string p_sAppExcpXml)
		{
			string strNDXMakeAppFldDataXml = "";
			int iRetCd = 0;
			string sAppExcpXml = "";
			IndexService objService = null;
						
			try
			{
				strNDXMakeAppFldDataXml = "<NDXMakeAppFldData><IndexName>" + p_sAcrosoftUsersTypeKey + "</IndexName><" + p_sUsersFolderFriendlyName + ">" 
					+ p_sUser + "</" + p_sUsersFolderFriendlyName + "></NDXMakeAppFldData>";	

				if(p_sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//objService = new IndexService();
					objService = new IndexService();
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objService.Url = RMConfigurator.Value("IndexService");
                    objService.Url = AcrosoftSection.IndexService;
                    iRetCd = objService.CreateFolder(p_sSessionId , strNDXMakeAppFldDataXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
                p_sAppExcpXml = "Error:" + sAppExcpXml;
			}
			finally
			{
				objService = null;
			} // finally

			return iRetCd;

		}

		public int CreatePolicyFolder(string p_sUserId , string p_sPassword , string p_sAcrosoftPolicyTypeKey , string p_sPolicyNumber , string p_sPolicyFolderFriendlyName , out string p_sAppExcpXml)
		{
			int iRetCd = 0;
			string sSessionId = "";
			string sAppExcpXml = "";
						
			try
			{
				//10/11/06 - moved the config info to Riskmaster.config
				//AuthService authService = new AuthService();
				AuthService authService = new AuthService();//RMConfigurator.Value("AuthService"));
                //08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                //gagnihotri R5 MCM changes
                //authService.Url = RMConfigurator.Value("AuthService");
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out sAppExcpXml);
				if(sSessionId!="")
				{
					iRetCd = CreatePolicyFolder(sSessionId , p_sAcrosoftPolicyTypeKey , p_sPolicyNumber , p_sPolicyFolderFriendlyName , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int CreateGeneralFolder(string p_sUserId , string p_sPassword , string p_sAcrosoftGeneralStorageTypeKey , string p_sFolderName , string p_sGeneralFolderFriendlyName , out string p_sAppExcpXml)
		{
			int iRetCd = 0;
			string sSessionId = "";
			string sAppExcpXml = "";
						
			try
			{
				//10/11/06 - moved the config info to Riskmaster.config
				//AuthService authService = new AuthService();
				AuthService authService = new AuthService();//RMConfigurator.Value("AuthService"));
                //08/22/2008 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                //gagnihotri R5 MCM changes
                //authService.Url = RMConfigurator.Value("AuthService");
                authService.Url = AcrosoftSection.AuthService;
                authService.Authenticate(p_sUserId , p_sPassword , out sSessionId ,out sAppExcpXml);
				if(sSessionId!="")
				{
					iRetCd = CreateGeneralFolder(sSessionId , p_sAcrosoftGeneralStorageTypeKey , p_sFolderName , p_sGeneralFolderFriendlyName , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			return iRetCd;

		}

		public int CreatePolicyFolder(string p_sSessionId , string p_sAcrosoftPolicyTypeKey , string p_sPolicyNumber , string p_sPolicyFolderFriendlyName , out string p_sAppExcpXml)
		{
			string strNDXMakeAppFldDataXml = "";
			int iRetCd = 0;
			string sAppExcpXml = "";
			IndexService objService = null;
						
			try
			{
				strNDXMakeAppFldDataXml = "<NDXMakeAppFldData><IndexName>" + p_sAcrosoftPolicyTypeKey + "</IndexName><" + p_sPolicyFolderFriendlyName + ">" 
					+ p_sPolicyNumber + "</" + p_sPolicyFolderFriendlyName + "></NDXMakeAppFldData>";	

				if(p_sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//objService = new IndexService();
					objService = new IndexService();//RMConfigurator.Value("IndexService"));
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objService.Url = RMConfigurator.Value("IndexService");
                    objService.Url = AcrosoftSection.IndexService;
                    iRetCd = objService.CreateFolder(p_sSessionId , strNDXMakeAppFldDataXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			finally
			{
				objService = null;
			} // finally

			return iRetCd;

		}

		public int CreateGeneralFolder(string p_sSessionId , string p_sAcrosoftGeneralStorageTypeKey , string p_sFolderName , string p_sGeneralFolderFriendlyName , out string p_sAppExcpXml)
		{
			string strNDXMakeAppFldDataXml = "";
			int iRetCd = 0;
			string sAppExcpXml = "";
			IndexService objService = null;
						
			try
			{
				strNDXMakeAppFldDataXml = "<NDXMakeAppFldData><IndexName>" + p_sAcrosoftGeneralStorageTypeKey + "</IndexName><" + p_sGeneralFolderFriendlyName + ">" 
					+ p_sFolderName + "</" + p_sGeneralFolderFriendlyName + "></NDXMakeAppFldData>";	

				if(p_sSessionId!="")
				{
					//10/11/06 - moved the config info to Riskmaster.config
					//objService = new IndexService();
					objService = new IndexService();//RMConfigurator.Value("IndexService"));
                    //08/22/2008 Raman Bhatia
                    //We need to set the url as per Riskmaster.Config
                    //gagnihotri R5 MCM changes
                    //objService.Url = RMConfigurator.Value("IndexService");
                    objService.Url = AcrosoftSection.IndexService;
                    iRetCd = objService.CreateFolder(p_sSessionId , strNDXMakeAppFldDataXml , out sAppExcpXml);
					p_sAppExcpXml = sAppExcpXml;
				}
				else p_sAppExcpXml = "Error";
				
			}
			catch(Exception e)
			{
				p_sAppExcpXml = "Error";
			}
			finally
			{
				objService = null;
			} // finally

			return iRetCd;

		}
		#endregion
        //rsolanki2: updates for re-indexing the folders in MCM
        #region re-index folders in MCM 
        public int ReIndexFolder(string sSessionId, string ndxReindexFldXml, out string p_sAppExcpXml)
        {
            int iRetCd = 0;
            //string sSessionId = "";
            //string sAppExcpXml = "";
            //Riskmaster.Application.AcrosoftWebserviceWrapper.com.acrosoft.asdemo3.
            IndexService IndService = new IndexService();
            IndService.Url = AcrosoftSection.IndexService;

            try
            {                   
                
                //IndService.PreAuthenticate (p_sUserId, p_sPassword, out sSessionId, out sAppExcpXml);
                if (sSessionId != "")
                {
                    iRetCd = IndService.ReIndexFolder(sSessionId, ndxReindexFldXml, out p_sAppExcpXml);
                    //ReIndexFolder(sSessionId, p_sAcrosoftUsersTypeKey, p_sUser, p_sUsersFolderFriendlyName, out sAppExcpXml);
                    //p_sAppExcpXml = sAppExcpXml;
                }
                else p_sAppExcpXml = "Error";

            }
            catch (Exception e)
            {
                p_sAppExcpXml = "Error:" + e.Message;
            }
            return iRetCd;

        }

        #endregion 
        #region Change Acrosoft Password
        /// <summary>
        /// This method is used to authenticate a user in Acrosoft and change its password
        /// </summary>

        public int AuthChangePassword(string p_sUserId, string p_sOldPwd, string p_sNewPwd, out string p_sSessionId, out string p_sAppExcpXml)
        {
            string sSessionId;
            string sAppExcpXml;
            int iRetId = 0;
            try
            {

                AuthService authService = new AuthService();
                //11/23/2009 Raman Bhatia
                //We need to set the url as per Riskmaster.Config
                authService.Url = AcrosoftSection.AuthService;
                iRetId = authService.AuthChangePassword(p_sUserId, p_sOldPwd, p_sNewPwd, out sSessionId, out sAppExcpXml);
                p_sSessionId = sSessionId;
                p_sAppExcpXml = sAppExcpXml;

            }
            catch (Exception e)
            {
                p_sSessionId = "";
                p_sAppExcpXml = "Error";
            }
            return iRetId;

        }

        #endregion
    }
}

