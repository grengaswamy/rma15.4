# ==============================================================================================
# 
# Microsoft PowerShell Source File -- Created with SAPIEN Technologies PrimalScript 2007
# 
# NAME: Signs specified code using the digital signing certificates
# 
# AUTHOR: Computer Sciences Corporation , CSC
# DATE  : 11/15/2008
# 
# COMMENT: 
# 
# ==============================================================================================
function SignCode
{
	
	$SignToolBasePath = $args[0]
	$DigitalSgtPath = $args[1]
	$ExecutablePath = $args[2]
	$DigitalSgtPwd = "riskmaster.2k8"
	$VerisignTimestamp = "http://timestamp.verisign.com/scripts/timstamp.dll"
	$SignToolPath = "`"$SignToolBasePath\signtool.exe`""
	$SignToolArgs = "sign /f `"$DigitalSgtPath`" /p $DigitalSgtPwd /t $VerisignTimestamp `"$ExecutablePath`""
	#Write-Host "$SignToolPath $SignToolArgs"
	$objStartInfo = New-Object System.Diagnostics.ProcessStartInfo
	$objStartInfo.FileName = $SignToolPath
	$objStartInfo.windowStyle ="Minimized"
	$objStartInfo.arguments = $SignToolArgs
	[System.Diagnostics.Process]::Start($objStartInfo)
}

If ($args.Length -ne 3)
{
	Write-Warning "This script requires 3 arguments:"
	Write-Warning "Signtool Base Path: Ex: C:\Program Files\Microsoft SDKs\Windows\v6.0A\bin"
	Write-Warning "Digital Signature Path (i.e. path to .pfx file): Ex: E:\home\build\Tools\Code Signing\riskmastersigncode.pfx"
	Write-Warning "Executable/Assembly Path: C:\mysetup.exe"
	Write-Warning "NOTE: Make sure that all paths are surrounded by double quotes if the paths contain spaces."
	
	
}
else
{
	$SignToolBasePath = $args[0]
	$DigSignPath = $args[1]
	$ExecutablePath = $args[2]
	
	#Call the SignCode function to sign the specified executable
	SignCode $SignToolBasePath $DigSignPath $ExecutablePath
}

# SIG # Begin signature block
# MIIJmgYJKoZIhvcNAQcCoIIJizCCCYcCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUvbFUOntr4qZWm1Oig9Nu7Mm2
# 1IugggcCMIIDTjCCAregAwIBAgIBCjANBgkqhkiG9w0BAQUFADCBzjELMAkGA1UE
# BhMCWkExFTATBgNVBAgTDFdlc3Rlcm4gQ2FwZTESMBAGA1UEBxMJQ2FwZSBUb3du
# MR0wGwYDVQQKExRUaGF3dGUgQ29uc3VsdGluZyBjYzEoMCYGA1UECxMfQ2VydGlm
# aWNhdGlvbiBTZXJ2aWNlcyBEaXZpc2lvbjEhMB8GA1UEAxMYVGhhd3RlIFByZW1p
# dW0gU2VydmVyIENBMSgwJgYJKoZIhvcNAQkBFhlwcmVtaXVtLXNlcnZlckB0aGF3
# dGUuY29tMB4XDTAzMDgwNjAwMDAwMFoXDTEzMDgwNTIzNTk1OVowVTELMAkGA1UE
# BhMCWkExJTAjBgNVBAoTHFRoYXd0ZSBDb25zdWx0aW5nIChQdHkpIEx0ZC4xHzAd
# BgNVBAMTFlRoYXd0ZSBDb2RlIFNpZ25pbmcgQ0EwgZ8wDQYJKoZIhvcNAQEBBQAD
# gY0AMIGJAoGBAMa4uSdgrwvjkWll236N7ZHmqvG+1e3+bdQsf9Fwd/smmVe03T8w
# uNwh6miNgZL8LkuRNYQg8tpKurT85tqI8iDFIZIJR5WgCRymeb6xTB388YpuVNJp
# ofFMkzpB/n3UZHtjRfdgYB0xHaTp0w+L+24mJLOo/+XlkNS0wtxQYK5ZAgMBAAGj
# gbMwgbAwEgYDVR0TAQH/BAgwBgEB/wIBADBABgNVHR8EOTA3MDWgM6Axhi9odHRw
# Oi8vY3JsLnRoYXd0ZS5jb20vVGhhd3RlUHJlbWl1bVNlcnZlckNBLmNybDAdBgNV
# HSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwMwDgYDVR0PAQH/BAQDAgEGMCkGA1Ud
# EQQiMCCkHjAcMRowGAYDVQQDExFQcml2YXRlTGFiZWwyLTE0NDANBgkqhkiG9w0B
# AQUFAAOBgQB2spzuE58b9i00kpRFczTcjmsuXPxMfYnrw2jx15kPLh0XyLUWi77N
# igUG8hlJOgNbBckgjm1S4XaBoMNliiJn5BxTUzdGv7zXL+t7ntAURWxAIQjiXXV2
# ZjAe9N+Cii+986IMvx3bnxSimnI3TbB3SOhKPwnOVRks7+YHJOGv7DCCA6wwggMV
# oAMCAQICEBCm2vQXz72NtQyfeVxeAi0wDQYJKoZIhvcNAQEFBQAwVTELMAkGA1UE
# BhMCWkExJTAjBgNVBAoTHFRoYXd0ZSBDb25zdWx0aW5nIChQdHkpIEx0ZC4xHzAd
# BgNVBAMTFlRoYXd0ZSBDb2RlIFNpZ25pbmcgQ0EwHhcNMDgxMTI0MDAwMDAwWhcN
# MTAxMTI0MjM1OTU5WjB1MQswCQYDVQQGEwJVUzERMA8GA1UECBMITWljaGlnYW4x
# EzARBgNVBAcTClNvdXRoZmllbGQxDDAKBgNVBAoTA0NTQzEiMCAGA1UECxMZRmlu
# YW5jaWFsIFNlcnZpY2VzIFNlY3RvcjEMMAoGA1UEAxMDQ1NDMIIBIjANBgkqhkiG
# 9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvTP4bvnmMV6TjwkeDzG7f/VUdncPkZk9SFfH
# 0w+rAswpW4KOJPrjihgx3FRyOWdm/4hBDq+6q2/hwMYDSe4Q92l5U6MoToR8RUFX
# u7YILa+MtDJ5M9104vQKI/PJs0wyKTTa6hgh5+FSNSd6V8dMR1h7V2dCeRti9C21
# /Xv08yTR0jKNMkIswZTGEkrfZ8987ziJODMXApUVXPjzF9/OGPqXY8D95bxAY1n6
# l6NiG2S2sEyoubrx/dSk9lVhjw8lp/ibr+XT+go7bsyycsQRSsL/39SqKR39d0on
# le7/GLFyEQM0heWVVQ/V1EuJsMA5+QALM+r3s6Ls51Yd36O9LwIDAQABo4HYMIHV
# MAwGA1UdEwEB/wQCMAAwPgYDVR0fBDcwNTAzoDGgL4YtaHR0cDovL2NybC50aGF3
# dGUuY29tL1RoYXd0ZUNvZGVTaWduaW5nQ0EuY3JsMB8GA1UdJQQYMBYGCCsGAQUF
# BwMDBgorBgEEAYI3AgEWMB0GA1UdBAQWMBQwDjAMBgorBgEEAYI3AgEWAwIHgDAy
# BggrBgEFBQcBAQQmMCQwIgYIKwYBBQUHMAGGFmh0dHA6Ly9vY3NwLnRoYXd0ZS5j
# b20wEQYJYIZIAYb4QgEBBAQDAgQQMA0GCSqGSIb3DQEBBQUAA4GBADCw1zPrR9V5
# wHEo/qMVH1GXqxcD0NkcJOp8BPReJCEMooJ/3WkSvYGlMawIa6nV8/l0lr2mwEc1
# i3+0ZWjBV8407u5hkbYMTnTH8RyAbRGiAwmElYKj+vIh9KXLja4fXJ5+Ghagct+A
# VU8H1BtGyQNyUQOIVEdMPc4F/VZ/zGabMYICAjCCAf4CAQEwaTBVMQswCQYDVQQG
# EwJaQTElMCMGA1UEChMcVGhhd3RlIENvbnN1bHRpbmcgKFB0eSkgTHRkLjEfMB0G
# A1UEAxMWVGhhd3RlIENvZGUgU2lnbmluZyBDQQIQEKba9BfPvY21DJ95XF4CLTAJ
# BgUrDgMCGgUAoHAwEAYKKwYBBAGCNwIBDDECMAAwGQYJKoZIhvcNAQkDMQwGCisG
# AQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcCARYwIwYJKoZIhvcN
# AQkEMRYEFGkO/v6FqsFGegQQcqLAQVp5pDW/MA0GCSqGSIb3DQEBAQUABIIBAJ7d
# VgnSx7UucP3oBFQIinQvx1ogSSHJf5/G06bxBUqTwC1ZDoe67/q0DMi/oEMY4ecR
# 0Mcdqjlhrhbs+ugQuXO6jr/ZRWA95kYolhqeJJSMu6o34KdHJl/6hMzcbt3gwWKG
# Xpo844gPhjuydGbuUJlHlzWDbzWy9LHUjRs6fbFw8h5wDSR5oTfRfz35qtug4zk+
# 4Rjiwt02yDIg55uTnnYr1KSbCK1DuXz1QIYqZjUAh/hvRaopwOI2xWLPOLmUx/pz
# VgdQRPUipZ1aJgfwSbpn0Qa+gWheGud2L8YkhjcbiwMpP7dBJmRlrUIHfsWIk//u
# ns6ywej7B6F8TpBjnLM=
# SIG # End signature block
