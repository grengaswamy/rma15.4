Option Explicit
'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 4.1
'
' NAME: rotate-ism.vbs
'
' AUTHOR:  Samir Vaidya
' DATE  : 09/08/2008
'
' COMMENT: Use this script during autobuild to:
' 1.)  apply a build number to the version string (in the 4th position which 
' is not considered at all by any automated tool - just for our reference.
' 2.) Apply a new MSI package GUID which is necessary for Windows Installer to 
' recognize that the MSI has changed from anything it could already have cached 
' on the target machine during a previous install.
' 3.) Apply a new Product code that can be used during Major Upgrade installations
'==========================================================================

Dim sNewProductVersion
Dim sISMProjectFullPath
Dim objISProject
sISMProjectFullPath = WScript.Arguments.Item(0)
sNewProductVersion = WScript.Arguments.Item(1)

'sISMProjectFullPath = "E:\home\package-r4\projects\RMXSSLSupport2.ism"

' Dim objArgs,strArg
' Set objArgs = WScript.Arguments
' For Each strArg In objArgs
'      WScript.Echo strArg
' Next 

'Set the constant for the InstallShield IDE Project library
'Const INSTALLSHIELD_IDE = "ISWiAuto15.ISWiProject"
'InstallShield 2013 support
Const INSTALLSHIELD_IDE = "ISWiAuto20.ISWiProject" 

' instantiate the Developer Automation interface
Dim oISM
Set oISM = CreateObject(INSTALLSHIELD_IDE)


' open a project read-write
oISM.OpenProject sISMProjectFullPath, False

'Apply new Product version within ISM
WScript.Echo("Initial Product Version: " & oISM.ProductVersion)
oISM.ProductVersion = sNewProductVersion 

WScript.Echo("New Product Version: " & oISM.ProductVersion)

'Apply a new "MSI" package code every time.
WScript.Echo("Initial MSI Package Code: " & oISM.PackageCode)
oISM.PackageCode = oISM.GenerateGUID()
WScript.Echo("New MSI Package Code: " & oISM.PackageCode)

'Apply a new Product Code every time
WScript.Echo("Initial Product code: " & oISM.ProductCode)
oISM.ProductCode = oISM.GenerateGUID()
WScript.Echo("New Product Code: " & oISM.ProductCode)

' save changes and clean up
oISM.SaveProject: oISM.CloseProject: Set oISM = Nothing
WScript.Echo("Completed Load and Save of ISM")


Function GenerateGuid()
	Dim strGuid
	Dim objGuidGenerator
	
	Set objGuidGenerator = CreateObject("Scriptlet.TypeLib")
	
	GenerateGuid = objGuidGenerator.Guid
End Function
