// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.to;

import bphx.c2ab.util.DBAccessStatus;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.SignType;
import bphx.sdf.datatype.ABODate;
import bphx.sdf.datatype.ABOTime;
import java.math.BigDecimal;
import com.csc.pt.svc.data.id.Bashlgbq00ID;

public class Bashlgbq00TO {
	private DBAccessStatus dBAccessStatus;
	private String loguser = "";
	private ABODate logdate = new ABODate();
	private ABOTime logtime = new ABOTime();
	private char bqc7st;
	private int bqandt;
	private short bqafnb;
	private int bqaldt;
	private int bqafdt;
	private String bqtype0act = "";
	private String bqbatx = "";
	private char bqdzst;
	private char bqd0st;
	private char bqd2st;
	private char bqd3st;
	
	
	private BigDecimal bqa3va = BigDecimal.ZERO;
	private BigDecimal bqbrva = BigDecimal.ZERO;
	private BigDecimal bqbsva = BigDecimal.ZERO;
	private BigDecimal bqbtva = BigDecimal.ZERO;
	private BigDecimal bqaupc = BigDecimal.ZERO;
	private String bqaqcd = "";
	private String bqarcd = "";
	private String bqascd = "";
	private String bqeetx = "";
	private String bqbccd = "";
	private String bqagnb = "";
	private BigDecimal bqa5va = BigDecimal.ZERO;
	private String bqhwtx = "";
	private char bqhxtx;
	private char bqhytx;
	private char bqhztx;
	private char bqh0tx;
	private String bqh1tx = "";
	private char bqbicd;
	private String bqbjcd = "";
	private String bqbkcd = "";
	private String bqh2tx = "";
	private String bqapnb = "";
	private char bqblcd;
	private char bqbmcd;
	private char bqbncd;
	private char bqbocd;
	private char bqbpcd;
	private char bqbqcd;
	private char bqbrcd;
	private String bqbscd = "";
	private char bqjbtx;
	private char bqjctx;
	private String bqdbnb = "";
	private String bqdcnb = "";
	private String bqddnb = "";
	private String bqdenb = "";
	private int bqdfnb;
	private int bqdgnb;
	private int bqdhnb;
	private int bqdinb;
	private int bqabnb;
	private int bqdjnb;
	private int bqdknb;
	private short bqdlnb;
	private short bqdmnb;
	private short bqdnnb;
	private BigDecimal bqavpc = BigDecimal.ZERO;
	private BigDecimal bqawpc = BigDecimal.ZERO;
	private BigDecimal bqaxpc = BigDecimal.ZERO;
	
	private BigDecimal bqa6va = BigDecimal.ZERO;
	private BigDecimal bqa7va = BigDecimal.ZERO;
	private BigDecimal bqa8va = BigDecimal.ZERO;
	private BigDecimal bqa9va = BigDecimal.ZERO;
	private BigDecimal bqbava = BigDecimal.ZERO;
	private BigDecimal bqbbva = BigDecimal.ZERO;
	private BigDecimal bqbcva = BigDecimal.ZERO;
	private BigDecimal bqbdva = BigDecimal.ZERO;
	private BigDecimal bqbeva = BigDecimal.ZERO;
	private BigDecimal bqbfva = BigDecimal.ZERO;
	private BigDecimal bqbgva = BigDecimal.ZERO;
	private BigDecimal bqbhva = BigDecimal.ZERO;
	private BigDecimal bqbiva = BigDecimal.ZERO;
	private BigDecimal bqbjva = BigDecimal.ZERO;
	private BigDecimal bqa9nb = BigDecimal.ZERO;
	private char bqczst;
	private char bqc0st;
	private char bqc1st;
	private char bqc2st;
	private char bqiytx;
	private char bqiztx;
	private char bqi0tx;
	private char bqi1tx;
	private char bqi2tx;
	private char bqi3tx;
	private char bqi4tx;
	private char bqi5tx;
	private String bqakcd = "";
	private String bqalcd = "";
	private String bqamcd = "";
	private String bqancd = "";
	private BigDecimal bqaepc = BigDecimal.ZERO;
	private BigDecimal bqafpc = BigDecimal.ZERO;
	private int bqbknb;
	private int bqblnb;
	
	
	private BigDecimal bqagva = BigDecimal.ZERO;
	private BigDecimal bqahva = BigDecimal.ZERO;
	private char bqj1tx;
	private int bqaedt;
	private BigDecimal bqbuva = BigDecimal.ZERO;
	private String bqm1tx = "";
	private char bqm2tx;
	private char bqm3tx;
	
	
	private BigDecimal bqb1va = BigDecimal.ZERO;
	private BigDecimal bqb2va = BigDecimal.ZERO;
	private Bashlgbq00ID id = new Bashlgbq00ID();

	public DBAccessStatus getDBAccessStatus() {
		return this.dBAccessStatus;
	}

	public void setDBAccessStatus(DBAccessStatus dBAccessStatus) {
		this.dBAccessStatus = dBAccessStatus;
	}

	public String getLoguser() {
		return this.loguser;
	}

	public void setLoguser(String loguser) {
		this.loguser = loguser;
	}

	public ABODate getLogdate() {
		return this.logdate.clone();
	}

	public void setLogdate(ABODate logdate) {
		this.logdate.assign(logdate);
	}

	public ABOTime getLogtime() {
		return this.logtime.clone();
	}

	public void setLogtime(ABOTime logtime) {
		this.logtime.assign(logtime);
	}


	public int getLogseqnum() {
		return this.id.getLogseqnum();
	}

	public void setLogseqnum(int logseqnum) {
		this.id.setLogseqnum(logseqnum);
	}

	public String getLocation() {
		return this.id.getLocation();
	}

	public void setLocation(String location) {
		this.id.setLocation(location);
	}

	public String getMasterco() {
		return this.id.getMasterco();
	}

	public void setMasterco(String masterco) {
		this.id.setMasterco(masterco);
	}

	public String getSymbol() {
		return this.id.getSymbol();
	}

	public void setSymbol(String symbol) {
		this.id.setSymbol(symbol);
	}

	public String getPolicyno() {
		return this.id.getPolicyno();
	}

	public void setPolicyno(String policyno) {
		this.id.setPolicyno(policyno);
	}

	public String getModule() {
		return this.id.getModule();
	}

	public void setModule(String module) {
		this.id.setModule(module);
	}

	public String getInsline() {
		return this.id.getInsline();
	}

	public void setInsline(String insline) {
		this.id.setInsline(insline);
	}

	public int getRiskloc() {
		return this.id.getRiskloc();
	}

	public void setRiskloc(int riskloc) {
		this.id.setRiskloc(riskloc);
	}

	public int getRisksubloc() {
		return this.id.getRisksubloc();
	}

	public void setRisksubloc(int risksubloc) {
		this.id.setRisksubloc(risksubloc);
	}

	public String getProduct() {
		return this.id.getProduct();
	}

	public void setProduct(String product) {
		this.id.setProduct(product);
	}

	public int getUnitno() {
		return this.id.getUnitno();
	}

	public void setUnitno(int unitno) {
		this.id.setUnitno(unitno);
	}

	public char getRecstatus() {
		return this.id.getRecstatus();
	}

	public void setRecstatus(char recstatus) {
		this.id.setRecstatus(recstatus);
	}
	public char getBqc7st() {
		return this.bqc7st;
	}

	public void setBqc7st(char bqc7st) {
		this.bqc7st = bqc7st;
	}

	public int getBqandt() {
		return this.bqandt;
	}

	public void setBqandt(int bqandt) {
		this.bqandt = bqandt;
	}

	public short getBqafnb() {
		return this.bqafnb;
	}

	public void setBqafnb(short bqafnb) {
		this.bqafnb = bqafnb;
	}

	public int getBqaldt() {
		return this.bqaldt;
	}

	public void setBqaldt(int bqaldt) {
		this.bqaldt = bqaldt;
	}

	public int getBqafdt() {
		return this.bqafdt;
	}

	public void setBqafdt(int bqafdt) {
		this.bqafdt = bqafdt;
	}

	public String getBqtype0act() {
		return this.bqtype0act;
	}

	public void setBqtype0act(String bqtype0act) {
		this.bqtype0act = bqtype0act;
	}

	public String getBqbatx() {
		return this.bqbatx;
	}

	public void setBqbatx(String bqbatx) {
		this.bqbatx = bqbatx;
	}

	public char getBqdzst() {
		return this.bqdzst;
	}

	public void setBqdzst(char bqdzst) {
		this.bqdzst = bqdzst;
	}

	public char getBqd0st() {
		return this.bqd0st;
	}

	public void setBqd0st(char bqd0st) {
		this.bqd0st = bqd0st;
	}

	public char getBqd2st() {
		return this.bqd2st;
	}

	public void setBqd2st(char bqd2st) {
		this.bqd2st = bqd2st;
	}

	public char getBqd3st() {
		return this.bqd3st;
	}

	public void setBqd3st(char bqd3st) {
		this.bqd3st = bqd3st;
	}

	public BigDecimal getBqa3va() {
		return this.bqa3va;
	}

	public void setBqa3va(BigDecimal bqa3va) {
		this.bqa3va = bqa3va;
	}

	public BigDecimal getBqbrva() {
		return this.bqbrva;
	}

	public void setBqbrva(BigDecimal bqbrva) {
		this.bqbrva = bqbrva;
	}

	public BigDecimal getBqbsva() {
		return this.bqbsva;
	}

	public void setBqbsva(BigDecimal bqbsva) {
		this.bqbsva = bqbsva;
	}

	public BigDecimal getBqbtva() {
		return this.bqbtva;
	}

	public void setBqbtva(BigDecimal bqbtva) {
		this.bqbtva = bqbtva;
	}

	public BigDecimal getBqaupc() {
		return this.bqaupc;
	}

	public void setBqaupc(BigDecimal bqaupc) {
		this.bqaupc = bqaupc;
	}

	public String getBqaqcd() {
		return this.bqaqcd;
	}

	public void setBqaqcd(String bqaqcd) {
		this.bqaqcd = bqaqcd;
	}

	public String getBqarcd() {
		return this.bqarcd;
	}

	public void setBqarcd(String bqarcd) {
		this.bqarcd = bqarcd;
	}

	public String getBqascd() {
		return this.bqascd;
	}

	public void setBqascd(String bqascd) {
		this.bqascd = bqascd;
	}

	public String getBqeetx() {
		return this.bqeetx;
	}

	public void setBqeetx(String bqeetx) {
		this.bqeetx = bqeetx;
	}

	public String getBqbccd() {
		return this.bqbccd;
	}

	public void setBqbccd(String bqbccd) {
		this.bqbccd = bqbccd;
	}

	public String getBqagnb() {
		return this.bqagnb;
	}

	public void setBqagnb(String bqagnb) {
		this.bqagnb = bqagnb;
	}

	public BigDecimal getBqa5va() {
		return this.bqa5va;
	}

	public void setBqa5va(BigDecimal bqa5va) {
		this.bqa5va = bqa5va;
	}

	public String getBqhwtx() {
		return this.bqhwtx;
	}

	public void setBqhwtx(String bqhwtx) {
		this.bqhwtx = bqhwtx;
	}

	public char getBqhxtx() {
		return this.bqhxtx;
	}

	public void setBqhxtx(char bqhxtx) {
		this.bqhxtx = bqhxtx;
	}

	public char getBqhytx() {
		return this.bqhytx;
	}

	public void setBqhytx(char bqhytx) {
		this.bqhytx = bqhytx;
	}

	public char getBqhztx() {
		return this.bqhztx;
	}

	public void setBqhztx(char bqhztx) {
		this.bqhztx = bqhztx;
	}

	public char getBqh0tx() {
		return this.bqh0tx;
	}

	public void setBqh0tx(char bqh0tx) {
		this.bqh0tx = bqh0tx;
	}

	public String getBqh1tx() {
		return this.bqh1tx;
	}

	public void setBqh1tx(String bqh1tx) {
		this.bqh1tx = bqh1tx;
	}

	public char getBqbicd() {
		return this.bqbicd;
	}

	public void setBqbicd(char bqbicd) {
		this.bqbicd = bqbicd;
	}

	public String getBqbjcd() {
		return this.bqbjcd;
	}

	public void setBqbjcd(String bqbjcd) {
		this.bqbjcd = bqbjcd;
	}

	public String getBqbkcd() {
		return this.bqbkcd;
	}

	public void setBqbkcd(String bqbkcd) {
		this.bqbkcd = bqbkcd;
	}

	public String getBqh2tx() {
		return this.bqh2tx;
	}

	public void setBqh2tx(String bqh2tx) {
		this.bqh2tx = bqh2tx;
	}

	public String getBqapnb() {
		return this.bqapnb;
	}

	public void setBqapnb(String bqapnb) {
		this.bqapnb = bqapnb;
	}

	public char getBqblcd() {
		return this.bqblcd;
	}

	public void setBqblcd(char bqblcd) {
		this.bqblcd = bqblcd;
	}

	public char getBqbmcd() {
		return this.bqbmcd;
	}

	public void setBqbmcd(char bqbmcd) {
		this.bqbmcd = bqbmcd;
	}

	public char getBqbncd() {
		return this.bqbncd;
	}

	public void setBqbncd(char bqbncd) {
		this.bqbncd = bqbncd;
	}

	public char getBqbocd() {
		return this.bqbocd;
	}

	public void setBqbocd(char bqbocd) {
		this.bqbocd = bqbocd;
	}

	public char getBqbpcd() {
		return this.bqbpcd;
	}

	public void setBqbpcd(char bqbpcd) {
		this.bqbpcd = bqbpcd;
	}

	public char getBqbqcd() {
		return this.bqbqcd;
	}

	public void setBqbqcd(char bqbqcd) {
		this.bqbqcd = bqbqcd;
	}

	public char getBqbrcd() {
		return this.bqbrcd;
	}

	public void setBqbrcd(char bqbrcd) {
		this.bqbrcd = bqbrcd;
	}

	public String getBqbscd() {
		return this.bqbscd;
	}

	public void setBqbscd(String bqbscd) {
		this.bqbscd = bqbscd;
	}

	public char getBqjbtx() {
		return this.bqjbtx;
	}

	public void setBqjbtx(char bqjbtx) {
		this.bqjbtx = bqjbtx;
	}

	public char getBqjctx() {
		return this.bqjctx;
	}

	public void setBqjctx(char bqjctx) {
		this.bqjctx = bqjctx;
	}

	public String getBqdbnb() {
		return this.bqdbnb;
	}

	public void setBqdbnb(String bqdbnb) {
		this.bqdbnb = bqdbnb;
	}

	public String getBqdcnb() {
		return this.bqdcnb;
	}

	public void setBqdcnb(String bqdcnb) {
		this.bqdcnb = bqdcnb;
	}

	public String getBqddnb() {
		return this.bqddnb;
	}

	public void setBqddnb(String bqddnb) {
		this.bqddnb = bqddnb;
	}

	public String getBqdenb() {
		return this.bqdenb;
	}

	public void setBqdenb(String bqdenb) {
		this.bqdenb = bqdenb;
	}

	public int getBqdfnb() {
		return this.bqdfnb;
	}

	public void setBqdfnb(int bqdfnb) {
		this.bqdfnb = bqdfnb;
	}

	public int getBqdgnb() {
		return this.bqdgnb;
	}

	public void setBqdgnb(int bqdgnb) {
		this.bqdgnb = bqdgnb;
	}

	public int getBqdhnb() {
		return this.bqdhnb;
	}

	public void setBqdhnb(int bqdhnb) {
		this.bqdhnb = bqdhnb;
	}

	public int getBqdinb() {
		return this.bqdinb;
	}

	public void setBqdinb(int bqdinb) {
		this.bqdinb = bqdinb;
	}

	public int getBqabnb() {
		return this.bqabnb;
	}

	public void setBqabnb(int bqabnb) {
		this.bqabnb = bqabnb;
	}

	public int getBqdjnb() {
		return this.bqdjnb;
	}

	public void setBqdjnb(int bqdjnb) {
		this.bqdjnb = bqdjnb;
	}

	public int getBqdknb() {
		return this.bqdknb;
	}

	public void setBqdknb(int bqdknb) {
		this.bqdknb = bqdknb;
	}

	public short getBqdlnb() {
		return this.bqdlnb;
	}

	public void setBqdlnb(short bqdlnb) {
		this.bqdlnb = bqdlnb;
	}

	public short getBqdmnb() {
		return this.bqdmnb;
	}

	public void setBqdmnb(short bqdmnb) {
		this.bqdmnb = bqdmnb;
	}

	public short getBqdnnb() {
		return this.bqdnnb;
	}

	public void setBqdnnb(short bqdnnb) {
		this.bqdnnb = bqdnnb;
	}

	public BigDecimal getBqavpc() {
		return this.bqavpc;
	}

	public void setBqavpc(BigDecimal bqavpc) {
		this.bqavpc = bqavpc;
	}

	public BigDecimal getBqawpc() {
		return this.bqawpc;
	}

	public void setBqawpc(BigDecimal bqawpc) {
		this.bqawpc = bqawpc;
	}

	public BigDecimal getBqaxpc() {
		return this.bqaxpc;
	}

	public void setBqaxpc(BigDecimal bqaxpc) {
		this.bqaxpc = bqaxpc;
	}

	public BigDecimal getBqa6va() {
		return this.bqa6va;
	}

	public void setBqa6va(BigDecimal bqa6va) {
		this.bqa6va = bqa6va;
	}

	public BigDecimal getBqa7va() {
		return this.bqa7va;
	}

	public void setBqa7va(BigDecimal bqa7va) {
		this.bqa7va = bqa7va;
	}

	public BigDecimal getBqa8va() {
		return this.bqa8va;
	}

	public void setBqa8va(BigDecimal bqa8va) {
		this.bqa8va = bqa8va;
	}

	public BigDecimal getBqa9va() {
		return this.bqa9va;
	}

	public void setBqa9va(BigDecimal bqa9va) {
		this.bqa9va = bqa9va;
	}

	public BigDecimal getBqbava() {
		return this.bqbava;
	}

	public void setBqbava(BigDecimal bqbava) {
		this.bqbava = bqbava;
	}

	public BigDecimal getBqbbva() {
		return this.bqbbva;
	}

	public void setBqbbva(BigDecimal bqbbva) {
		this.bqbbva = bqbbva;
	}

	public BigDecimal getBqbcva() {
		return this.bqbcva;
	}

	public void setBqbcva(BigDecimal bqbcva) {
		this.bqbcva = bqbcva;
	}

	public BigDecimal getBqbdva() {
		return this.bqbdva;
	}

	public void setBqbdva(BigDecimal bqbdva) {
		this.bqbdva = bqbdva;
	}

	public BigDecimal getBqbeva() {
		return this.bqbeva;
	}

	public void setBqbeva(BigDecimal bqbeva) {
		this.bqbeva = bqbeva;
	}

	public BigDecimal getBqbfva() {
		return this.bqbfva;
	}

	public void setBqbfva(BigDecimal bqbfva) {
		this.bqbfva = bqbfva;
	}

	public BigDecimal getBqbgva() {
		return this.bqbgva;
	}

	public void setBqbgva(BigDecimal bqbgva) {
		this.bqbgva = bqbgva;
	}

	public BigDecimal getBqbhva() {
		return this.bqbhva;
	}

	public void setBqbhva(BigDecimal bqbhva) {
		this.bqbhva = bqbhva;
	}

	public BigDecimal getBqbiva() {
		return this.bqbiva;
	}

	public void setBqbiva(BigDecimal bqbiva) {
		this.bqbiva = bqbiva;
	}

	public BigDecimal getBqbjva() {
		return this.bqbjva;
	}

	public void setBqbjva(BigDecimal bqbjva) {
		this.bqbjva = bqbjva;
	}

	public BigDecimal getBqa9nb() {
		return this.bqa9nb;
	}

	public void setBqa9nb(BigDecimal bqa9nb) {
		this.bqa9nb = bqa9nb;
	}

	public char getBqczst() {
		return this.bqczst;
	}

	public void setBqczst(char bqczst) {
		this.bqczst = bqczst;
	}

	public char getBqc0st() {
		return this.bqc0st;
	}

	public void setBqc0st(char bqc0st) {
		this.bqc0st = bqc0st;
	}

	public char getBqc1st() {
		return this.bqc1st;
	}

	public void setBqc1st(char bqc1st) {
		this.bqc1st = bqc1st;
	}

	public char getBqc2st() {
		return this.bqc2st;
	}

	public void setBqc2st(char bqc2st) {
		this.bqc2st = bqc2st;
	}

	public char getBqiytx() {
		return this.bqiytx;
	}

	public void setBqiytx(char bqiytx) {
		this.bqiytx = bqiytx;
	}

	public char getBqiztx() {
		return this.bqiztx;
	}

	public void setBqiztx(char bqiztx) {
		this.bqiztx = bqiztx;
	}

	public char getBqi0tx() {
		return this.bqi0tx;
	}

	public void setBqi0tx(char bqi0tx) {
		this.bqi0tx = bqi0tx;
	}

	public char getBqi1tx() {
		return this.bqi1tx;
	}

	public void setBqi1tx(char bqi1tx) {
		this.bqi1tx = bqi1tx;
	}

	public char getBqi2tx() {
		return this.bqi2tx;
	}

	public void setBqi2tx(char bqi2tx) {
		this.bqi2tx = bqi2tx;
	}

	public char getBqi3tx() {
		return this.bqi3tx;
	}

	public void setBqi3tx(char bqi3tx) {
		this.bqi3tx = bqi3tx;
	}

	public char getBqi4tx() {
		return this.bqi4tx;
	}

	public void setBqi4tx(char bqi4tx) {
		this.bqi4tx = bqi4tx;
	}

	public char getBqi5tx() {
		return this.bqi5tx;
	}

	public void setBqi5tx(char bqi5tx) {
		this.bqi5tx = bqi5tx;
	}

	public String getBqakcd() {
		return this.bqakcd;
	}

	public void setBqakcd(String bqakcd) {
		this.bqakcd = bqakcd;
	}

	public String getBqalcd() {
		return this.bqalcd;
	}

	public void setBqalcd(String bqalcd) {
		this.bqalcd = bqalcd;
	}

	public String getBqamcd() {
		return this.bqamcd;
	}

	public void setBqamcd(String bqamcd) {
		this.bqamcd = bqamcd;
	}

	public String getBqancd() {
		return this.bqancd;
	}

	public void setBqancd(String bqancd) {
		this.bqancd = bqancd;
	}

	public BigDecimal getBqaepc() {
		return this.bqaepc;
	}

	public void setBqaepc(BigDecimal bqaepc) {
		this.bqaepc = bqaepc;
	}

	public BigDecimal getBqafpc() {
		return this.bqafpc;
	}

	public void setBqafpc(BigDecimal bqafpc) {
		this.bqafpc = bqafpc;
	}

	public int getBqbknb() {
		return this.bqbknb;
	}

	public void setBqbknb(int bqbknb) {
		this.bqbknb = bqbknb;
	}

	public int getBqblnb() {
		return this.bqblnb;
	}

	public void setBqblnb(int bqblnb) {
		this.bqblnb = bqblnb;
	}

	public BigDecimal getBqagva() {
		return this.bqagva;
	}

	public void setBqagva(BigDecimal bqagva) {
		this.bqagva = bqagva;
	}

	public BigDecimal getBqahva() {
		return this.bqahva;
	}

	public void setBqahva(BigDecimal bqahva) {
		this.bqahva = bqahva;
	}

	public char getBqj1tx() {
		return this.bqj1tx;
	}

	public void setBqj1tx(char bqj1tx) {
		this.bqj1tx = bqj1tx;
	}

	public int getBqaedt() {
		return this.bqaedt;
	}

	public void setBqaedt(int bqaedt) {
		this.bqaedt = bqaedt;
	}

	public BigDecimal getBqbuva() {
		return this.bqbuva;
	}

	public void setBqbuva(BigDecimal bqbuva) {
		this.bqbuva = bqbuva;
	}

	public String getBqm1tx() {
		return this.bqm1tx;
	}

	public void setBqm1tx(String bqm1tx) {
		this.bqm1tx = bqm1tx;
	}

	public char getBqm2tx() {
		return this.bqm2tx;
	}

	public void setBqm2tx(char bqm2tx) {
		this.bqm2tx = bqm2tx;
	}

	public char getBqm3tx() {
		return this.bqm3tx;
	}

	public void setBqm3tx(char bqm3tx) {
		this.bqm3tx = bqm3tx;
	}

	public BigDecimal getBqb1va() {
		return this.bqb1va;
	}

	public void setBqb1va(BigDecimal bqb1va) {
		this.bqb1va = bqb1va;
	}

	public BigDecimal getBqb2va() {
		return this.bqb2va;
	}

	public void setBqb2va(BigDecimal bqb2va) {
		this.bqb2va = bqb2va;
	}
	public Bashlgbq00ID getId() {
		return this.id;
	}

	public void setId(Bashlgbq00ID id) {
		this.id = id;
	}
}
