package com.csc.pt.svc.rsk;

import bphx.c2ab.cobol.CalledProgList;
import bphx.c2ab.data.SPResultSetTO;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DynamicCall;
import bphx.c2ab.util.DynamicCallException;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.ReturnException;
import bphx.c2ab.util.Types;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.datatype.ABODecimal;
import bphx.sdf.util.ABOSystem;

import com.csc.pt.svc.ctrl.Rec00fmtRec3;
import com.csc.pt.svc.data.dao.Pmsj1200DAO;
import com.csc.pt.svc.data.dao.Pmsj1201DAO;
import com.csc.pt.svc.data.dao.Pmsj1202DAO;
import com.csc.pt.svc.data.dao.Pmsjwc04DAO;
import com.csc.pt.svc.data.dao.Pmsl0247DAO;
import com.csc.pt.svc.data.dao.Pmsl0248DAO;
import com.csc.pt.svc.data.dao.Pmsl0249DAO;
import com.csc.pt.svc.data.dao.Pmsl0250DAO;
import com.csc.pt.svc.data.dao.Pmsl0251DAO;
import com.csc.pt.svc.data.dao.Pmsl0252DAO;
import com.csc.pt.svc.data.dao.Pmsl0253DAO;
import com.csc.pt.svc.data.dao.Pmsl0254DAO;
import com.csc.pt.svc.data.dao.Pmsl0255DAO;
import com.csc.pt.svc.data.dao.Pmsl0256DAO;
import com.csc.pt.svc.data.dao.Pmsl0257DAO;
import com.csc.pt.svc.data.dao.Pmsl0258DAO;
import com.csc.pt.svc.data.dao.Pmsl0259DAO;
import com.csc.pt.svc.data.dao.Pmsl0268DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.dao.Pmspmm80DAO;
import com.csc.pt.svc.data.to.Pmsj1200TO;
import com.csc.pt.svc.data.to.Pmsj1201TO;
import com.csc.pt.svc.data.to.Pmsj1202TO;
import com.csc.pt.svc.data.to.Pmsjwc04TO;
import com.csc.pt.svc.data.to.Pmsl0247TO;
import com.csc.pt.svc.data.to.Pmsl0248TO;
import com.csc.pt.svc.data.to.Pmsl0249TO;
import com.csc.pt.svc.data.to.Pmsl0250TO;
import com.csc.pt.svc.data.to.Pmsl0251TO;
import com.csc.pt.svc.data.to.Pmsl0252TO;
import com.csc.pt.svc.data.to.Pmsl0253TO;
import com.csc.pt.svc.data.to.Pmsl0254TO;
import com.csc.pt.svc.data.to.Pmsl0255TO;
import com.csc.pt.svc.data.to.Pmsl0256TO;
import com.csc.pt.svc.data.to.Pmsl0257TO;
import com.csc.pt.svc.data.to.Pmsl0258TO;
import com.csc.pt.svc.data.to.Pmsl0259TO;
import com.csc.pt.svc.data.to.Pmsl0268TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.data.to.Pmspmm80TO;
import com.csc.pt.svc.rsk.to.Rskdbio028TO;
import com.csc.pt.svc.db.AddnlIntIndicator;
import com.csc.pt.svc.db.AddnlTypeIndicator;
import com.csc.pt.svc.db.BcListEntryArr13;
import com.csc.pt.svc.db.FeinIndicator;
import com.csc.pt.svc.db.QwcAspList;
import com.csc.pt.svc.db.QwcCurList;
import com.csc.pt.svc.db.QwcGrpList;
import com.csc.pt.svc.db.QwcJobi0100;
import com.csc.pt.svc.db.QwcJobi0150;
import com.csc.pt.svc.db.QwcJobi0200;
import com.csc.pt.svc.db.QwcJobi0300;
import com.csc.pt.svc.db.QwcJobi0400;
import com.csc.pt.svc.db.QwcJobi0500;
import com.csc.pt.svc.db.QwcJobi0600;
import com.csc.pt.svc.db.QwcJobi0700;
import com.csc.pt.svc.db.QwcJobi0750;
import com.csc.pt.svc.db.QwcJobi0800;
import com.csc.pt.svc.db.QwcJobi0900;
import com.csc.pt.svc.db.QwcJobi1000;
import com.csc.pt.svc.db.QwcLibList;
import com.csc.pt.svc.db.QwcLibList2;
import com.csc.pt.svc.db.QwcSigList;
import com.csc.pt.svc.db.QwcTzoneList;
import com.csc.pt.svc.db.WsChkIndic;
import com.csc.pt.svc.db.WsProcessControlSw6;
import com.csc.pt.svc.db.to.Pmsj1200RecTO;
import com.csc.pt.svc.db.to.Pmsj1201RecTO;
import com.csc.pt.svc.db.to.Pmsj1202RecTO;
import com.csc.pt.svc.db.to.Pmsjwc04RecTO;
import com.csc.pt.svc.db.to.Pmsl0247RecTO;
import com.csc.pt.svc.db.to.Pmsl0248RecTO;
import com.csc.pt.svc.db.to.Pmsl0249RecTO;
import com.csc.pt.svc.db.to.Pmsl0250RecTO;
import com.csc.pt.svc.db.to.Pmsl0251RecTO;
import com.csc.pt.svc.db.to.Pmsl0252RecTO2;
import com.csc.pt.svc.db.to.Pmsl0253RecTO2;
import com.csc.pt.svc.db.to.Pmsl0254RecTO2;
import com.csc.pt.svc.db.to.Pmsl0255RecTO2;
import com.csc.pt.svc.db.to.Pmsl0256RecTO2;
import com.csc.pt.svc.db.to.Pmsl0257RecTO2;
import com.csc.pt.svc.db.to.Pmsl0258RecTO2;
import com.csc.pt.svc.db.to.Pmsl0259RecTO2;
import com.csc.pt.svc.db.to.Pmsl0268RecTO;
import com.csc.pt.svc.db.to.Pmsp0200RecTO3;
import com.csc.pt.svc.db.to.Pmspmm80RecTO;
import com.csc.pt.svc.isl.Cobolsqlca;
import com.csc.pt.svc.pas.wc.LocalDataArea;



public class Rskdbio028 {
	private static final String PROGRAM_ID = "RSKDBIO028";
	public Pmsp0200RecTO3 pmsp0200RecTO3 = new Pmsp0200RecTO3();
	public Pmsl0253RecTO2 pmsl0253RecTO2 = new Pmsl0253RecTO2();
	public Pmsl0254RecTO2 pmsl0254RecTO2 = new Pmsl0254RecTO2();
	public Pmsl0247RecTO pmsl0247RecTO = new Pmsl0247RecTO();
	public Pmsl0248RecTO pmsl0248RecTO = new Pmsl0248RecTO();
	public Pmsl0249RecTO pmsl0249RecTO = new Pmsl0249RecTO();
	public Pmsl0259RecTO2 pmsl0259RecTO2 = new Pmsl0259RecTO2();
	public Pmsl0252RecTO2 pmsl0252RecTO2 = new Pmsl0252RecTO2();
	public Pmsl0255RecTO2 pmsl0255RecTO2 = new Pmsl0255RecTO2();
	public Pmsl0256RecTO2 pmsl0256RecTO2 = new Pmsl0256RecTO2();
	public Pmsl0257RecTO2 pmsl0257RecTO2 = new Pmsl0257RecTO2();
	public Pmsl0258RecTO2 pmsl0258RecTO2 = new Pmsl0258RecTO2();
	public Pmsl0250RecTO pmsl0250RecTO = new Pmsl0250RecTO();
	public Pmsl0251RecTO pmsl0251RecTO = new Pmsl0251RecTO();
	public Pmspmm80RecTO pmspmm80RecTO = new Pmspmm80RecTO();
	public Pmsl0268RecTO pmsl0268RecTO = new Pmsl0268RecTO();
	public Pmsjwc04RecTO pmsjwc04RecTO = new Pmsjwc04RecTO();
	public Pmsj1200RecTO pmsj1200RecTO = new Pmsj1200RecTO();
	public Pmsj1201RecTO pmsj1201RecTO = new Pmsj1201RecTO();
	public Pmsj1202RecTO pmsj1202RecTO = new Pmsj1202RecTO();
	private Cobolsqlca cobolsqlca = new Cobolsqlca();
	private String holdPreviousType = "";
	private short wsCounter = Types.INVALID_SHORT_VAL;
	private char endFileRead = ' ';
	public static final char END_READ_REACHED = 'Y';
	private char endCrossRead = ' ';
	public static final char END_FILE_REACHED = 'Y';
	private String ws1FileStatus = "";
	public static final String WS_GOOD_RETURN = "00";
	private short wsCtLength;
	private short wsZcLength;
	private short wsSnLength;
	private char wsErrorSw = 'Y';
	public static final char WS_ERROR_SET = 'Y';
	private short wsAttrNum;
	private int wsCount;
	private int numOfEntrys;
	private int numOfEntrysForSql;
	private String wsReturnCode = "";
	public static final String GOOD_RETURN_FROM_IO = "0000000";
	private char wsControlSw = ' ';
	private WsProcessControlSw6 wsProcessControlSw6 = new WsProcessControlSw6();
	private short wsNameLength;
	private short wsAddnlIntNameLen;
	private short wsDbanameLength;
	private FeinIndicator feinIndicator = new FeinIndicator();
	private AddnlIntIndicator addnlIntIndicator = new AddnlIntIndicator();
	private AddnlTypeIndicator addnlTypeIndicator = new AddnlTypeIndicator();
	private WsChkIndic wsChkIndic = new WsChkIndic();
	private String wsTmpModule = "";
	private String wsTmpMaster0co = "";
	private Rec00fmtRec3 rec00fmtRec3 = new Rec00fmtRec3();
	private LocalDataArea localDataArea = new LocalDataArea();
	private int jobiReceiverLength = 200;
	private String jobiFormatName = "";
	private String jobiJobName = "*";
	private String jobiJobNameInt = "";
	private int rskdbio028filler4;
	private String qusrjobi = "QUSRJOBI";
	private QwcJobi0100 qwcJobi0100 = new QwcJobi0100();
	private QwcJobi0150 qwcJobi0150 = new QwcJobi0150();
	private QwcJobi0200 qwcJobi0200 = new QwcJobi0200();
	private QwcJobi0300 qwcJobi0300 = new QwcJobi0300();
	private QwcAspList qwcAspList = new QwcAspList();
	private QwcJobi0400 qwcJobi0400 = new QwcJobi0400();
	private QwcJobi0500 qwcJobi0500 = new QwcJobi0500();
	private QwcGrpList qwcGrpList = new QwcGrpList();
	private QwcTzoneList qwcTzoneList = new QwcTzoneList();
	private QwcJobi0600 qwcJobi0600 = new QwcJobi0600();
	private QwcLibList qwcLibList = new QwcLibList();
	private QwcJobi0700 qwcJobi0700 = new QwcJobi0700();
	private QwcLibList2 qwcLibList2 = new QwcLibList2();
	private QwcJobi0750 qwcJobi0750 = new QwcJobi0750();
	private QwcSigList qwcSigList = new QwcSigList();
	private QwcJobi0800 qwcJobi0800 = new QwcJobi0800();
	private QwcCurList qwcCurList = new QwcCurList();
	private QwcJobi0900 qwcJobi0900 = new QwcJobi0900();
	private QwcJobi1000 qwcJobi1000 = new QwcJobi1000();
	private BcListEntryArr13[] bcListEntryArr13 = ABOCollectionCreator.getReferenceFactory(BcListEntryArr13.class).createArray(101);
	private char holdBcTrans0stat = ' ';
	private String holdBcSymbol = "";
	private String holdBcPolicy0num = "";
	private String holdBcModule = "";
	private String holdBcMaster0co = "";
	private String holdBcLocation = "";
	private String holdBcEffdt = "";
	private String holdBcExpdt = "";
	private String holdBcRisk0state = "";
	private String holdBcCompany0no = "";
	private String holdBcAgency = "";
	private String holdBcLine0bus = "";
	private String holdBcZip0post = "";
	private String holdBcAdd0line01 = "";
	private String holdBcAdd0line02 = "";
	private String holdBcAdd0line03 = "";
	private String holdBcAdd0line04 = "";
	private String holdBcState = "";
	private char holdBcRenewal0cd = ' ';
	private char holdBcIssue0code = ' ';
	private char holdBcPay0code = ' ';
	private char holdBcMode0code = ' ';
	private String holdBcSort0name = "";
	private String holdBcCust0no = "";
	private String holdBcSpec0use0a = "";
	private String holdBcSpec0use0b = "";
	private String holdBcCanceldate = "";
	private String holdBcMrsseq = "";
	private String holdBcStatusDesc = "";
	//FSIT: 152640, Resolution: 55204 - Begin
	//private ABODecimal holdBcTot0ag0prm = new ABODecimal(11, 2);
	private ABODecimal holdBcTot0ag0prm = new ABODecimal(20, 2);
	//FSIT: 152640, Resolution: 55204 - End
	private String holdBcType0act = "";
	private Rskdbio028TO rskdbio028TO = new Rskdbio028TO();
	private Pmsp0200DAO pmsp0200DAO = new Pmsp0200DAO();
	private Pmsl0253DAO pmsl0253DAO = new Pmsl0253DAO();
	private Pmsl0254DAO pmsl0254DAO = new Pmsl0254DAO();
	private Pmsl0247DAO pmsl0247DAO = new Pmsl0247DAO();
	private Pmsl0248DAO pmsl0248DAO = new Pmsl0248DAO();
	private Pmsl0249DAO pmsl0249DAO = new Pmsl0249DAO();
	private Pmsl0259DAO pmsl0259DAO = new Pmsl0259DAO();
	private Pmsl0252DAO pmsl0252DAO = new Pmsl0252DAO();
	private Pmsl0255DAO pmsl0255DAO = new Pmsl0255DAO();
	private Pmsl0256DAO pmsl0256DAO = new Pmsl0256DAO();
	private Pmsl0257DAO pmsl0257DAO = new Pmsl0257DAO();
	private Pmsl0258DAO pmsl0258DAO = new Pmsl0258DAO();
	private Pmsl0250DAO pmsl0250DAO = new Pmsl0250DAO();
	private Pmsl0251DAO pmsl0251DAO = new Pmsl0251DAO();
	private Pmspmm80DAO pmspmm80DAO = new Pmspmm80DAO();
	private Pmsl0268DAO pmsl0268DAO = new Pmsl0268DAO();
	private Pmsjwc04DAO pmsjwc04DAO = new Pmsjwc04DAO();
	private Pmsj1200DAO pmsj1200DAO = new Pmsj1200DAO();
	private Pmsj1201DAO pmsj1201DAO = new Pmsj1201DAO();
	private Pmsj1202DAO pmsj1202DAO = new Pmsj1202DAO();
	private SPResultSetTO resultSetTO = new SPResultSetTO();
	private Pmspmm80TO pmspmm80TO = new Pmspmm80TO();

	private Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
	private Pmsl0259TO pmsl0259TO = new Pmsl0259TO();
	private Pmsjwc04TO pmsjwc04TO = new Pmsjwc04TO();
	private Pmsj1200TO pmsj1200TO = new Pmsj1200TO();
	private Pmsj1201TO pmsj1201TO = new Pmsj1201TO();
	private Pmsj1202TO pmsj1202TO = new Pmsj1202TO();
	private Pmsl0255TO pmsl0255TO = new Pmsl0255TO();
	private Pmsl0256TO pmsl0256TO = new Pmsl0256TO();
	private Pmsl0257TO pmsl0257TO = new Pmsl0257TO();
	private Pmsl0258TO pmsl0258TO = new Pmsl0258TO();
	private Pmsl0252TO pmsl0252TO = new Pmsl0252TO();
	private Pmsl0250TO pmsl0250TO = new Pmsl0250TO();
	private Pmsl0251TO pmsl0251TO = new Pmsl0251TO();
	private Pmsl0248TO pmsl0248TO = new Pmsl0248TO();
	private Pmsl0247TO pmsl0247TO = new Pmsl0247TO();
	private Pmsl0249TO pmsl0249TO = new Pmsl0249TO();
	private Pmsl0253TO pmsl0253TO = new Pmsl0253TO();
	private Pmsl0254TO pmsl0254TO = new Pmsl0254TO();
	private Pmsl0268TO pmsl0268TO = new Pmsl0268TO();

	public Rskdbio028() {
		for (int idx = 1; idx <= 101; idx++) {
			bcListEntryArr13[(idx) - 1] = new BcListEntryArr13();
		}
	}

	public void mainSubroutine() {
		main1();
		exitProgram();
		exit();
	}

	public void main1() {
		// * If MAXRECORDS is less than one, just return an empty resultset
		if (rskdbio028TO.getInMaxrecords() < 1) {
			numOfEntrysForSql = 0;
			resultSetTO.addResultSet(bcListEntryArr13, numOfEntrysForSql);
			exit();
		}
		// THIS SECTION OF THE CODE TO DETERMINE THE LOGICAL FILE
		// TO BE USED FOR THE SEARCH PROCESS USING THE HIGH ORDER KEY
		if (rskdbio028TO.getInState().compareTo("") != 0) {
			wsProcessControlSw6.setValue("2");
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0) {
			wsProcessControlSw6.setValue("1");
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0) {
			wsProcessControlSw6.setValue("5");
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0) {
			wsProcessControlSw6.setValue("4");
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0) {
			wsProcessControlSw6.setValue("3");
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0) {
			wsProcessControlSw6.setValue("10");
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0) {
			wsProcessControlSw6.setValue("8");
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0) {
			wsProcessControlSw6.setValue("9");
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0) {
			wsProcessControlSw6.setValue("11");
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0) {
			wsProcessControlSw6.setValue("13");
		}
		if (rskdbio028TO.getInName().compareTo("") != 0) {
			wsProcessControlSw6.setValue("12");
		}
		if (rskdbio028TO.getInFein().compareTo("") != 0) {
			wsProcessControlSw6.setValue("17");
		}
		if (rskdbio028TO.getInDbaname().compareTo("") != 0) {
			wsProcessControlSw6.setValue("16");
		}
		if (rskdbio028TO.getInAddnlIntName().compareTo("") != 0) {
			wsProcessControlSw6.setValue("14");
		}
		if (rskdbio028TO.getInAddnlIntType().compareTo("") != 0) {
			wsProcessControlSw6.setValue("15");
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0) {
			if (rskdbio028TO.getInPolicy0num().compareTo("") != 0) {
				if (rskdbio028TO.getInModule().compareTo("") != 0) {
					wsProcessControlSw6.setValue("6");
				} else {
					wsProcessControlSw6.setValue("7");
				}
			}
		}
		//
		//
		// PERFORM 9900-GET-USER-INFO THRU 9900-EXIT.
		//
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		// MOVE CURR-USRPRF-NAME OF QWC-JOBI0600
		pmspmm80RecTO.pmm80Rec.setUsrprf(rskdbio028TO.getInUserid());
		pmspmm80TO = Pmspmm80DAO.selectById(pmspmm80TO, pmspmm80RecTO.pmm80Rec.getUsrprf());
		if (pmspmm80TO.getDBAccessStatus().isSuccess()) {
			pmspmm80RecTO.setData(pmspmm80TO.getData());
		}
		ws1FileStatus = Functions.subString(pmspmm80TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			pmspmm80RecTO.pmm80Rec.initPmm80Spaces();
		}
		// DETERMINE THE LENGTH OF LAST-NAME & SORT-NAME
		if (rskdbio028TO.getInName().compareTo("") != 0) {
			wsNameLength = ((short) 30);
			while (!(Functions.subString(rskdbio028TO.getInName(), wsNameLength, 1).compareTo("") != 0)) {
				exit1();
				wsNameLength = ((short) (wsNameLength + -1));
			}
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0) {
			wsSnLength = ((short) 4);
			while (!(Functions.subString(rskdbio028TO.getInSort0name(), wsSnLength, 1).compareTo("") != 0)) {
				exit1();
				wsSnLength = ((short) (wsSnLength + -1));
			}
		}
		// DETERMINE THE LENGTH OF City
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0) {
			wsCtLength = ((short) 28);
			while (!(Functions.subString(rskdbio028TO.getInAdd0line04(), wsCtLength, 1).compareTo("") != 0)) {
				exit1();
				wsCtLength = ((short) (wsCtLength + -1));
			}
		} else {
			wsCtLength = ((short) (28 % 100));
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0) {
			wsZcLength = ((short) 10);
			while (!(Functions.subString(rskdbio028TO.getInZip0post(), wsZcLength, 1).compareTo("") != 0)) {
				exit1();
				wsZcLength = ((short) (wsZcLength + -1));
			}
		} else {
			wsZcLength = ((short) (10 % 100));
		}
		if (rskdbio028TO.getInDbaname().compareTo("") != 0) {
			wsDbanameLength = ((short) 30);
			while (!(Functions.subString(rskdbio028TO.getInDbaname(), wsDbanameLength, 1).compareTo("") != 0)) {
				exit1();
				wsDbanameLength = ((short) (wsDbanameLength + -1));
			}
		}
		if (rskdbio028TO.getInAddnlIntName().compareTo("") != 0) {
			wsAddnlIntNameLen = ((short) 30);
			while (!(Functions.subString(rskdbio028TO.getInAddnlIntName(), wsAddnlIntNameLen, 1).compareTo("") != 0)) {
				exit1();
				wsAddnlIntNameLen = ((short) (wsAddnlIntNameLen + -1));
			}
		}
		if (!Functions.isNumber(rskdbio028TO.getInMaxrecords()) || rskdbio028TO.getInMaxrecords() > 100) {
			rskdbio028TO.setInMaxrecords(((short) 100));
		}
		//
		numOfEntrys = 0 % 100000;
		initBcListTableSpaces();
		//
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (wsProcessControlSw6.isPolicyFullKey()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0259RecTO2.rec02fmtRec2.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0259RecTO2.rec02fmtRec2.setLocation(rskdbio028TO.getInLocation());
			pmsl0259RecTO2.rec02fmtRec2.setSymbol(rskdbio028TO.getInSymbol());
			pmsl0259RecTO2.rec02fmtRec2.setModule(rskdbio028TO.getInModule());
			pmsl0259RecTO2.rec02fmtRec2.setPolicy0num(rskdbio028TO.getInPolicy0num());
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.openPmsl0259TOCsr(KeyType.GREATER_OR_EQ, pmsl0259RecTO2.rec02fmtRec2.getLocation(),
				pmsl0259RecTO2.rec02fmtRec2.getPolicy0num(), pmsl0259RecTO2.rec02fmtRec2.getSymbol(), pmsl0259RecTO2.rec02fmtRec2.getMaster0co(),
				pmsl0259RecTO2.rec02fmtRec2.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				readPmsl0259();
			}
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1200DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isPolicySymbol()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0255RecTO2.rec02fmtRec4.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0255RecTO2.rec02fmtRec4.setLocation(rskdbio028TO.getInLocation());
			pmsl0255RecTO2.rec02fmtRec4.setSymbol(rskdbio028TO.getInSymbol());
			pmsl0255RecTO2.rec02fmtRec4.setPolicy0num("");
			pmsl0255RecTO2.rec02fmtRec4.setModule(Types.HIGH_STR_VAL);
			cobolsqlca.setDBAccessStatus(pmsl0255DAO.openPmsl0255TOCsr(KeyType.GREATER_OR_EQ, pmsl0255RecTO2.rec02fmtRec4.getLocation(),
				pmsl0255RecTO2.rec02fmtRec4.getMaster0co(), pmsl0255RecTO2.rec02fmtRec4.getSymbol(), pmsl0255RecTO2.rec02fmtRec4.getPolicy0num(),
				pmsl0255RecTO2.rec02fmtRec4.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				readPmsl0255();
			}
			cobolsqlca.setDBAccessStatus(pmsl0255DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isPolicyNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0256RecTO2.rec02fmtRec5.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0256RecTO2.rec02fmtRec5.setLocation(rskdbio028TO.getInLocation());
			pmsl0256RecTO2.rec02fmtRec5.setPolicy0num(rskdbio028TO.getInPolicy0num());
			pmsl0256RecTO2.rec02fmtRec5.setSymbol("");
			pmsl0256RecTO2.rec02fmtRec5.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0256DAO.openPmsl0256TOCsr(KeyType.GREATER_OR_EQ, pmsl0256RecTO2.rec02fmtRec5.getLocation(),
				pmsl0256RecTO2.rec02fmtRec5.getMaster0co(), pmsl0256RecTO2.rec02fmtRec5.getPolicy0num(), pmsl0256RecTO2.rec02fmtRec5.getSymbol(),
				pmsl0256RecTO2.rec02fmtRec5.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				readPmsl0256();
			}
			cobolsqlca.setDBAccessStatus(pmsl0256DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isPolicyModule()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0257RecTO2.rec02fmtRec6.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0257RecTO2.rec02fmtRec6.setLocation(rskdbio028TO.getInLocation());
			pmsl0257RecTO2.rec02fmtRec6.setModule(rskdbio028TO.getInModule());
			cobolsqlca.setDBAccessStatus(pmsl0257DAO.openPmsl0257TOCsr(KeyType.GREATER_OR_EQ, pmsl0257RecTO2.rec02fmtRec6.getLocation(),
				pmsl0257RecTO2.rec02fmtRec6.getMaster0co(), pmsl0257RecTO2.rec02fmtRec6.getModule(), pmsl0257RecTO2.rec02fmtRec6.getSymbol(),
				pmsl0257RecTO2.rec02fmtRec6.getPolicy0num()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				readPmsl0257();
			}
			cobolsqlca.setDBAccessStatus(pmsl0257DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isPolicySymbolNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0258RecTO2.rec02fmtRec7.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0258RecTO2.rec02fmtRec7.setLocation(rskdbio028TO.getInLocation());
			pmsl0258RecTO2.rec02fmtRec7.setSymbol(rskdbio028TO.getInSymbol());
			pmsl0258RecTO2.rec02fmtRec7.setPolicy0num(rskdbio028TO.getInPolicy0num());
			pmsl0258RecTO2.rec02fmtRec7.setModule(Types.HIGH_STR_VAL);
			cobolsqlca.setDBAccessStatus(pmsl0258DAO.openPmsl0258TOCsr(KeyType.GREATER_OR_EQ, pmsl0258RecTO2.rec02fmtRec7.getLocation(),
				pmsl0258RecTO2.rec02fmtRec7.getMaster0co(), pmsl0258RecTO2.rec02fmtRec7.getSymbol(), pmsl0258RecTO2.rec02fmtRec7.getPolicy0num(),
				pmsl0258RecTO2.rec02fmtRec7.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				readPmsl0258();
			}
			cobolsqlca.setDBAccessStatus(pmsl0258DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1200DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isCustomerNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0252RecTO2.rec02fmtRec3.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0252RecTO2.rec02fmtRec3.setLocation(rskdbio028TO.getInLocation());
			pmsl0252RecTO2.rec02fmtRec3.setCust0no(rskdbio028TO.getInCust0no());
			pmsl0252RecTO2.rec02fmtRec3.setSymbol("");
			pmsl0252RecTO2.rec02fmtRec3.setPolicy0num("");
			pmsl0252RecTO2.rec02fmtRec3.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0252DAO.openPmsl0252TOCsr(KeyType.GREATER_OR_EQ, pmsl0252RecTO2.rec02fmtRec3.getMaster0co(),
				pmsl0252RecTO2.rec02fmtRec3.getLocation(), pmsl0252RecTO2.rec02fmtRec3.getCust0no(), pmsl0252RecTO2.rec02fmtRec3.getSymbol(),
				pmsl0252RecTO2.rec02fmtRec3.getPolicy0num(), pmsl0252RecTO2.rec02fmtRec3.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				readPmsl0252();
			}
			cobolsqlca.setDBAccessStatus(pmsl0252DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isInsured()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0250RecTO.rec02fmtRec11.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0250RecTO.rec02fmtRec11.setLocation(rskdbio028TO.getInLocation());
			pmsl0250RecTO.rec02fmtRec11.setAdd0line01FromReference(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength), 1, wsNameLength);
			pmsl0250RecTO.rec02fmtRec11.setSymbol("");
			pmsl0250RecTO.rec02fmtRec11.setPolicy0num("");
			pmsl0250RecTO.rec02fmtRec11.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0250DAO.openPmsl0250TOCsr(KeyType.GREATER_OR_EQ, pmsl0250RecTO.rec02fmtRec11.getMaster0co(),
				pmsl0250RecTO.rec02fmtRec11.getLocation(), pmsl0250RecTO.rec02fmtRec11.getAdd0line01(), pmsl0250RecTO.rec02fmtRec11.getSymbol(),
				pmsl0250RecTO.rec02fmtRec11.getPolicy0num(), pmsl0250RecTO.rec02fmtRec11.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				readPmsl0250();
			}
			cobolsqlca.setDBAccessStatus(pmsl0259DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isSortName()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0251RecTO.rec02fmtRec12.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0251RecTO.rec02fmtRec12.setLocation(rskdbio028TO.getInLocation());
			pmsl0251RecTO.rec02fmtRec12.setSort0name(rskdbio028TO.getInSort0name());
			pmsl0251RecTO.rec02fmtRec12.setSymbol("");
			pmsl0251RecTO.rec02fmtRec12.setPolicy0num("");
			pmsl0251RecTO.rec02fmtRec12.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0251DAO.openPmsl0251TOCsr(KeyType.GREATER_OR_EQ, pmsl0251RecTO.rec02fmtRec12.getMaster0co(),
				pmsl0251RecTO.rec02fmtRec12.getLocation(), pmsl0251RecTO.rec02fmtRec12.getSort0name(), pmsl0251RecTO.rec02fmtRec12.getSymbol(),
				pmsl0251RecTO.rec02fmtRec12.getPolicy0num(), pmsl0251RecTO.rec02fmtRec12.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				readPmsl0251();
			}
			cobolsqlca.setDBAccessStatus(pmsl0251DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isCityName()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0248RecTO.rec02fmtRec14.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0248RecTO.rec02fmtRec14.setLocation(rskdbio028TO.getInLocation());
			pmsl0248RecTO.rec02fmtRec14.setCity(Functions.subString(ABOSystem.Upper(rskdbio028TO.getInAdd0line04()), 1, 28));
			pmsl0248RecTO.rec02fmtRec14.setSymbol("");
			pmsl0248RecTO.rec02fmtRec14.setPolicy0num("");
			pmsl0248RecTO.rec02fmtRec14.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0248DAO.openPmsl0248TOCsr(KeyType.GREATER_OR_EQ, pmsl0248RecTO.rec02fmtRec14.getMaster0co(),
				pmsl0248RecTO.rec02fmtRec14.getLocation(), pmsl0248RecTO.rec02fmtRec14.getCity(), pmsl0248RecTO.rec02fmtRec14.getSymbol(),
				pmsl0248RecTO.rec02fmtRec14.getPolicy0num(), pmsl0248RecTO.rec02fmtRec14.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				rng2000ReadPmsl0248();
			}
			cobolsqlca.setDBAccessStatus(pmsl0248DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isStateCode()) {
			pmsp0200RecTO3.rec02fmtRec.setMaster0co(rskdbio028TO.getInMaster0co());
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0247RecTO.rec02fmtRec13.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0247RecTO.rec02fmtRec13.setLocation(rskdbio028TO.getInLocation());
			pmsl0247RecTO.rec02fmtRec13.setState(rskdbio028TO.getInState());
			pmsl0247RecTO.rec02fmtRec13.setSymbol("");
			pmsl0247RecTO.rec02fmtRec13.setPolicy0num("");
			pmsl0247RecTO.rec02fmtRec13.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0247DAO.openPmsl0247TOCsr(KeyType.GREATER_OR_EQ, pmsl0247RecTO.rec02fmtRec13.getMaster0co(),
				pmsl0247RecTO.rec02fmtRec13.getLocation(), pmsl0247RecTO.rec02fmtRec13.getState(), pmsl0247RecTO.rec02fmtRec13.getSymbol(),
				pmsl0247RecTO.rec02fmtRec13.getPolicy0num(), pmsl0247RecTO.rec02fmtRec13.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				rng2000ReadPmsl0247();
			}
			cobolsqlca.setDBAccessStatus(pmsl0247DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isPostalCode()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0249RecTO.rec02fmtRec15.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0249RecTO.rec02fmtRec15.setLocation(rskdbio028TO.getInLocation());
			pmsl0249RecTO.rec02fmtRec15.setZip0post(rskdbio028TO.getInZip0post());
			pmsl0249RecTO.rec02fmtRec15.setSymbol("");
			pmsl0249RecTO.rec02fmtRec15.setPolicy0num("");
			pmsl0249RecTO.rec02fmtRec15.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0249DAO.openPmsl0249TOCsr(KeyType.GREATER_OR_EQ, pmsl0249RecTO.rec02fmtRec15.getMaster0co(),
				pmsl0249RecTO.rec02fmtRec15.getLocation(), pmsl0249RecTO.rec02fmtRec15.getZip0post(), pmsl0249RecTO.rec02fmtRec15.getSymbol(),
				pmsl0249RecTO.rec02fmtRec15.getPolicy0num(), pmsl0249RecTO.rec02fmtRec15.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				rng2000ReadPmsl0249();
			}
			cobolsqlca.setDBAccessStatus(pmsl0249DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isAgentNumber()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0253RecTO2.rec02fmtRec9.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0253RecTO2.rec02fmtRec9.setLocation(rskdbio028TO.getInLocation());
			pmsl0253RecTO2.setRec02fmtAgencyFromString(Functions.subString(rskdbio028TO.getInAgency(), 1, 0));
			pmsl0253RecTO2.rec02fmtRec9.setSymbol("");
			pmsl0253RecTO2.rec02fmtRec9.setPolicy0num("");
			pmsl0253RecTO2.rec02fmtRec9.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0253DAO.openPmsl0253TOCsr(KeyType.GREATER_OR_EQ, pmsl0253RecTO2.rec02fmtRec9.getMaster0co(),
				pmsl0253RecTO2.rec02fmtRec9.getLocation(), pmsl0253RecTO2.rec02fmtRec9.getFillr1(), pmsl0253RecTO2.rec02fmtRec9.getRpt0agt0nr(),
				pmsl0253RecTO2.rec02fmtRec9.getFillr2(), pmsl0253RecTO2.rec02fmtRec9.getSymbol(), pmsl0253RecTO2.rec02fmtRec9.getPolicy0num(),
				pmsl0253RecTO2.rec02fmtRec9.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				rng2000ReadPmsl0253();
			}
			cobolsqlca.setDBAccessStatus(pmsl0253DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		if (wsProcessControlSw6.isLineOfBusiness()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0254RecTO2.rec02fmtRec10.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0254RecTO2.rec02fmtRec10.setLocation(rskdbio028TO.getInLocation());
			pmsl0254RecTO2.rec02fmtRec10.setLine0bus(rskdbio028TO.getInLine0bus());
			pmsl0254RecTO2.rec02fmtRec10.setSymbol("");
			pmsl0254RecTO2.rec02fmtRec10.setPolicy0num("");
			pmsl0254RecTO2.rec02fmtRec10.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0254DAO.openPmsl0254TOCsr(KeyType.GREATER_OR_EQ, pmsl0254RecTO2.rec02fmtRec10.getMaster0co(),
				pmsl0254RecTO2.rec02fmtRec10.getLocation(), pmsl0254RecTO2.rec02fmtRec10.getLine0bus(), pmsl0254RecTO2.rec02fmtRec10.getSymbol(),
				pmsl0254RecTO2.rec02fmtRec10.getPolicy0num(), pmsl0254RecTO2.rec02fmtRec10.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endFileRead == END_READ_REACHED || numOfEntrys == rskdbio028TO.getInMaxrecords())) {
				rng2000ReadPmsl0254();
			}
			cobolsqlca.setDBAccessStatus(pmsl0254DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		//
		if (wsProcessControlSw6.isDbaName()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsl0268RecTO.pmsl0268Record.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsl0268RecTO.pmsl0268Record.setLocation(rskdbio028TO.getInLocation());
			pmsl0268RecTO.pmsl0268Record.setAdd0line02(rskdbio028TO.getInDbaname());
			pmsl0268RecTO.pmsl0268Record.setSymbol("");
			pmsl0268RecTO.pmsl0268Record.setPolicy0num("");
			pmsl0268RecTO.pmsl0268Record.setModule("");
			cobolsqlca.setDBAccessStatus(pmsl0268DAO.openPmsl0268TOCsr(KeyType.GREATER_OR_EQ, pmsl0268RecTO.pmsl0268Record.getLocation(),
				pmsl0268RecTO.pmsl0268Record.getMaster0co(), pmsl0268RecTO.pmsl0268Record.getAdd0line02(), pmsl0268RecTO.pmsl0268Record.getSymbol(),
				pmsl0268RecTO.pmsl0268Record.getPolicy0num(), pmsl0268RecTO.pmsl0268Record.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				cobolsqlca.setDBAccessStatus(pmsl0268DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				return;
			}
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			while (!(endFileRead == END_READ_REACHED)) {
				rng2000ReadPmsl0268();
			}
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1200DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsl0268DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		//
		if (wsProcessControlSw6.isFein()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'N') {
				processPmsjwc04();
			}
			if (rskdbio028TO.getInFeinIndInt() == 'Y' && rskdbio028TO.getInFeinIndSite() == 'N') {
				processPmsj1200();
			}
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'Y') {
				processPmsjwc04();
				processPmsj1200();
			}
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1200DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		//
		if (wsProcessControlSw6.isAddnlIntName()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsj1202RecTO.rcdj1202Rec.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsj1202RecTO.rcdj1202Rec.setLocation(rskdbio028TO.getInLocation());
			pmsj1202RecTO.rcdj1202Rec.setDesc0line1(rskdbio028TO.getInAddnlIntName());
			pmsj1202RecTO.rcdj1202Rec.setSymbol("");
			pmsj1202RecTO.rcdj1202Rec.setPolicy0num("");
			pmsj1202RecTO.rcdj1202Rec.setModule("");
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.openPmsj1202TOCsr(KeyType.GREATER_OR_EQ, pmsj1202RecTO.rcdj1202Rec.getLocation(),
				pmsj1202RecTO.rcdj1202Rec.getMaster0co(), pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), pmsj1202RecTO.rcdj1202Rec.getSymbol(),
				pmsj1202RecTO.rcdj1202Rec.getPolicy0num(), pmsj1202RecTO.rcdj1202Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(pmsj1200DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsj1202();
			}
			endFileRead = ' ';
			pmsj1202RecTO.rcdj1202Rec.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsj1202RecTO.rcdj1202Rec.setLocation(rskdbio028TO.getInLocation());
			pmsj1202RecTO.rcdj1202Rec.setDesc0line1(Functions.subString(ABOSystem.Lower(rskdbio028TO.getInAddnlIntName()), 1, 30));
			pmsj1202RecTO.rcdj1202Rec.setSymbol("");
			pmsj1202RecTO.rcdj1202Rec.setPolicy0num("");
			pmsj1202RecTO.rcdj1202Rec.setModule("");
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.openPmsj1202TOCsr(KeyType.GREATER_OR_EQ, pmsj1202RecTO.rcdj1202Rec.getLocation(),
				pmsj1202RecTO.rcdj1202Rec.getMaster0co(), pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), pmsj1202RecTO.rcdj1202Rec.getSymbol(),
				pmsj1202RecTO.rcdj1202Rec.getPolicy0num(), pmsj1202RecTO.rcdj1202Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				cobolsqlca.setDBAccessStatus(pmsj1200DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				return;
			}
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsj1202();
			}
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1200DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
		//
		if (wsProcessControlSw6.isAddnlIntType()) {
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			pmsj1201RecTO.rcdj1201Rec.setMaster0co(rskdbio028TO.getInMaster0co());
			pmsj1201RecTO.rcdj1201Rec.setLocation(rskdbio028TO.getInLocation());
			pmsj1201RecTO.rcdj1201Rec.setUse0code(rskdbio028TO.getInAddnlIntType());
			pmsj1201RecTO.rcdj1201Rec.setSymbol("");
			pmsj1201RecTO.rcdj1201Rec.setPolicy0num("");
			pmsj1201RecTO.rcdj1201Rec.setModule("");
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.openPmsj1201TOCsr(KeyType.GREATER_OR_EQ, pmsj1201RecTO.rcdj1201Rec.getLocation(),
				pmsj1201RecTO.rcdj1201Rec.getMaster0co(), pmsj1201RecTO.rcdj1201Rec.getUse0code(), pmsj1201RecTO.rcdj1201Rec.getSymbol(),
				pmsj1201RecTO.rcdj1201Rec.getPolicy0num(), pmsj1201RecTO.rcdj1201Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				cobolsqlca.setDBAccessStatus(pmsj1201DAO.closeCsr());
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				return;
			}
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			// open. No translation required
			cobolsqlca = new Cobolsqlca();
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			while (!(endFileRead == END_READ_REACHED)) {
				readPmsj1201();
			}
			cobolsqlca.setDBAccessStatus(pmsjwc04DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1200DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.closeCsr());
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		}
	}

	public void exitProgram() {
		// close. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		// close. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		//
		numOfEntrysForSql = numOfEntrys;
		resultSetTO.addResultSet(bcListEntryArr13, numOfEntrysForSql);
	}

	public void exit() {
		throw new ReturnException();
	}

	public String readPmsl0248() {
		pmsl0248TO = pmsl0248DAO.fetchNext(pmsl0248TO);
		pmsl0248RecTO.setData(pmsl0248TO.getData());
		pmsl0248RecTO.setData(pmsl0248RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0248TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0248TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0248RecTO.rec02fmtRec14.getCompany0no()) != 0) {
				return "";
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0248RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return "";
			}
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0248RecTO.rec02fmtRec14.getLocation()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0248RecTO.rec02fmtRec14.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		// IF FUNCTION UPPER-CASE (IN-ADD0LINE04)
		// NOT EQUAL CITY OF PMSL0248-REC
		// MOVE 'Y' TO END-FILE-READ
		// GO TO 2000-EXIT-PMSL0248
		// END-IF.
		if (ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
			ABOSystem.Upper(Functions.subString(pmsl0248RecTO.rec02fmtRec14.getCity(), 1, wsCtLength))) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return "";
			}
			return "2000-READ-PMSL0248";
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0248RecTO.rec02fmtRec14.getAdd0line04(), 29, 2))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0248RecTO.rec02fmtRec14.getZip0post(), 1, wsZcLength)) != 0) {
			return "";
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0248RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0248RecTO.rec02fmtRec14.getLine0bus()) != 0) {
			return "";
		}
		if (pmsl0248RecTO.rec02fmtRec14.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0248RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0248RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0248RecTO.rec02fmtRec14.getTrans0stat();
		holdBcSymbol = pmsl0248RecTO.rec02fmtRec14.getSymbol();
		holdBcPolicy0num = pmsl0248RecTO.rec02fmtRec14.getPolicy0num();
		holdBcModule = pmsl0248RecTO.rec02fmtRec14.getModule();
		holdBcMaster0co = pmsl0248RecTO.rec02fmtRec14.getMaster0co();
		holdBcLocation = pmsl0248RecTO.rec02fmtRec14.getLocation();
		holdBcEffdt = pmsl0248RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0248RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0248RecTO.rec02fmtRec14.getRisk0state();
		holdBcCompany0no = pmsl0248RecTO.rec02fmtRec14.getCompany0no();
		holdBcAgency = pmsl0248RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0248RecTO.rec02fmtRec14.getLine0bus();
		holdBcZip0post = pmsl0248RecTO.rec02fmtRec14.getZip0post();
		holdBcAdd0line01 = pmsl0248RecTO.rec02fmtRec14.getAdd0line01();
		holdBcAdd0line02 = pmsl0248RecTO.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0248RecTO.rec02fmtRec14.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0248RecTO.rec02fmtRec14.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0248RecTO.rec02fmtRec14.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0248RecTO.rec02fmtRec14.getRenewal0cd();
		holdBcIssue0code = pmsl0248RecTO.rec02fmtRec14.getIssue0code();
		holdBcPay0code = pmsl0248RecTO.rec02fmtRec14.getPay0code();
		holdBcMode0code = pmsl0248RecTO.rec02fmtRec14.getMode0code();
		holdBcSort0name = pmsl0248RecTO.rec02fmtRec14.getSort0name();
		holdBcCust0no = pmsl0248RecTO.rec02fmtRec14.getCust0no();
		holdBcSpec0use0a = pmsl0248RecTO.rec02fmtRec14.getSpec0use0a();
		holdBcSpec0use0b = pmsl0248RecTO.rec02fmtRec14.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0258RecTO2.rec02fmtRec7.getCanceldate();
		}
		holdBcMrsseq = pmsl0248RecTO.rec02fmtRec14.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0248RecTO.rec02fmtRec14.getTot0ag0prm());
		holdBcType0act = pmsl0248RecTO.rec02fmtRec14.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0248RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0248RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return "";
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return "";
		}
		return "";
	}

	public void exitPmsl0248() {
	// exit
	}

	public String readPmsl0247() {
		pmsl0247TO = pmsl0247DAO.fetchNext(pmsl0247TO);
		pmsl0247RecTO.setData(pmsl0247TO.getData());
		pmsl0247RecTO.setData(pmsl0247RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0247TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0247TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0247RecTO.rec02fmtRec13.getCompany0no()) != 0) {
				return "";
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0247RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return "";
			}
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0247RecTO.rec02fmtRec13.getLocation()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0247RecTO.rec02fmtRec13.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInState().compareTo(pmsl0247RecTO.rec02fmtRec13.getState()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return "";
			}
			return "2000-READ-PMSL0247";
		}
		// IF FUNCTION UPPER-CASE (IN-ADD0LINE04)
		// NOT EQUAL SPACES AND
		// FUNCTION UPPER-CASE (IN-ADD0LINE04)
		// NOT EQUAL
		// FUNCTION UPPER-CASE(ADD0LINE04 OF PMSL0247-REC (1 : 28))
		// GO TO 2000-EXIT-PMSL0247
		// END-IF.
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& Functions.subString(ABOSystem.Upper(rskdbio028TO.getInAdd0line04()), 1, wsCtLength).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0247RecTO.rec02fmtRec13.getAdd0line04(), 1, wsCtLength))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0247RecTO.rec02fmtRec13.getZip0post(), 1, wsZcLength)) != 0) {
			return "";
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0247RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0247RecTO.rec02fmtRec13.getLine0bus()) != 0) {
			return "";
		}
		if (pmsl0247RecTO.rec02fmtRec13.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0247RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0247RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0247RecTO.rec02fmtRec13.getTrans0stat();
		holdBcSymbol = pmsl0247RecTO.rec02fmtRec13.getSymbol();
		holdBcPolicy0num = pmsl0247RecTO.rec02fmtRec13.getPolicy0num();
		holdBcModule = pmsl0247RecTO.rec02fmtRec13.getModule();
		holdBcMaster0co = pmsl0247RecTO.rec02fmtRec13.getMaster0co();
		holdBcLocation = pmsl0247RecTO.rec02fmtRec13.getLocation();
		holdBcEffdt = pmsl0247RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0247RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0247RecTO.rec02fmtRec13.getRisk0state();
		holdBcCompany0no = pmsl0247RecTO.rec02fmtRec13.getCompany0no();
		holdBcAgency = pmsl0247RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0247RecTO.rec02fmtRec13.getLine0bus();
		holdBcZip0post = pmsl0247RecTO.rec02fmtRec13.getZip0post();
		holdBcAdd0line01 = pmsl0247RecTO.rec02fmtRec13.getAdd0line01();
		holdBcAdd0line02 = pmsl0247RecTO.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0247RecTO.rec02fmtRec13.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0247RecTO.rec02fmtRec13.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0247RecTO.rec02fmtRec13.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0247RecTO.rec02fmtRec13.getRenewal0cd();
		holdBcIssue0code = pmsl0247RecTO.rec02fmtRec13.getIssue0code();
		holdBcPay0code = pmsl0247RecTO.rec02fmtRec13.getPay0code();
		holdBcMode0code = pmsl0247RecTO.rec02fmtRec13.getMode0code();
		holdBcSort0name = pmsl0247RecTO.rec02fmtRec13.getSort0name();
		holdBcCust0no = pmsl0247RecTO.rec02fmtRec13.getCust0no();
		holdBcSpec0use0a = pmsl0247RecTO.rec02fmtRec13.getSpec0use0a();
		holdBcSpec0use0b = pmsl0247RecTO.rec02fmtRec13.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0247RecTO.rec02fmtRec13.getCanceldate();
		}
		holdBcMrsseq = pmsl0247RecTO.rec02fmtRec13.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0247RecTO.rec02fmtRec13.getTot0ag0prm());
		holdBcType0act = pmsl0247RecTO.rec02fmtRec13.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0247RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0247RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return "";
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return "";
		}
		return "";
	}

	public void exitPmsl0247() {
	// exit
	}

	public String readPmsl0249() {
		pmsl0249TO = pmsl0249DAO.fetchNext(pmsl0249TO);
		pmsl0249RecTO.setData(pmsl0249TO.getData());
		pmsl0249RecTO.setData(pmsl0249RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0249TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0249TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0249RecTO.rec02fmtRec15.getCompany0no()) != 0) {
				return "";
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0249RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return "";
			}
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0249RecTO.rec02fmtRec15.getLocation()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0249RecTO.rec02fmtRec15.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
			Functions.subString(pmsl0249RecTO.rec02fmtRec15.getZip0post(), 1, wsZcLength)) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return "";
			}
			return "2000-READ-PMSL0249";
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0249RecTO.rec02fmtRec15.getAdd0line04(), 29, 2))) != 0) {
			return "";
		}
		// IF FUNCTION UPPER-CASE (IN-ADD0LINE04)
		// NOT EQUAL SPACES AND
		// FUNCTION UPPER-CASE (IN-ADD0LINE04)
		// NOT EQUAL
		// FUNCTION UPPER-CASE(ADD0LINE04 OF PMSL0249-REC(1 : 28))
		// GO TO 2000-EXIT-PMSL0249
		// END-IF.
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0249RecTO.rec02fmtRec15.getAdd0line04(), 1, wsCtLength))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0249RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0249RecTO.rec02fmtRec15.getLine0bus()) != 0) {
			return "";
		}
		if (pmsl0249RecTO.rec02fmtRec15.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0249RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0249RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0249RecTO.rec02fmtRec15.getTrans0stat();
		holdBcSymbol = pmsl0249RecTO.rec02fmtRec15.getSymbol();
		holdBcPolicy0num = pmsl0249RecTO.rec02fmtRec15.getPolicy0num();
		holdBcModule = pmsl0249RecTO.rec02fmtRec15.getModule();
		holdBcMaster0co = pmsl0249RecTO.rec02fmtRec15.getMaster0co();
		holdBcLocation = pmsl0249RecTO.rec02fmtRec15.getLocation();
		holdBcEffdt = pmsl0249RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0249RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0249RecTO.rec02fmtRec15.getRisk0state();
		holdBcCompany0no = pmsl0249RecTO.rec02fmtRec15.getCompany0no();
		holdBcAgency = pmsl0249RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0249RecTO.rec02fmtRec15.getLine0bus();
		holdBcZip0post = pmsl0249RecTO.rec02fmtRec15.getZip0post();
		holdBcAdd0line01 = pmsl0249RecTO.rec02fmtRec15.getAdd0line01();
		holdBcAdd0line02 = pmsl0249RecTO.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0249RecTO.rec02fmtRec15.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0249RecTO.rec02fmtRec15.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0249RecTO.rec02fmtRec15.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0249RecTO.rec02fmtRec15.getRenewal0cd();
		holdBcIssue0code = pmsl0249RecTO.rec02fmtRec15.getIssue0code();
		holdBcPay0code = pmsl0249RecTO.rec02fmtRec15.getPay0code();
		holdBcMode0code = pmsl0249RecTO.rec02fmtRec15.getMode0code();
		holdBcSort0name = pmsl0249RecTO.rec02fmtRec15.getSort0name();
		holdBcCust0no = pmsl0249RecTO.rec02fmtRec15.getCust0no();
		holdBcSpec0use0a = pmsl0249RecTO.rec02fmtRec15.getSpec0use0a();
		holdBcSpec0use0b = pmsl0249RecTO.rec02fmtRec15.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0249RecTO.rec02fmtRec15.getCanceldate();
		}
		holdBcMrsseq = pmsl0249RecTO.rec02fmtRec15.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0249RecTO.rec02fmtRec15.getTot0ag0prm());
		holdBcType0act = pmsl0249RecTO.rec02fmtRec15.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0249RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0249RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return "";
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return "";
		}
		return "";
	}

	public void exitPmsl0249() {
	// exit
	}

	public String readPmsl0253() {
		pmsl0253TO = pmsl0253DAO.fetchNext(pmsl0253TO);
		pmsl0253RecTO2.setData(pmsl0253TO.getData());
		pmsl0253RecTO2.setData(pmsl0253RecTO2.getData());
		ws1FileStatus = Functions.subString(pmsl0253TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0253TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0253RecTO2.rec02fmtRec9.getCompany0no()) != 0) {
				return "";
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0253RecTO2.getRec02fmtAgencyAsStr()) != 0) {
				return "";
			}
		}
		if (rskdbio028TO.getInAgency().compareTo(pmsl0253RecTO2.getRec02fmtAgencyAsStr()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return "";
			}
			return "2000-READ-PMSL0253";
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0253RecTO2.rec02fmtRec9.getZip0post(), 1, wsZcLength)) != 0) {
			return "";
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0253RecTO2.rec02fmtRec9.getAdd0line04(), 29, 2))) != 0) {
			return "";
		}
		// IF FUNCTION UPPER-CASE (IN-ADD0LINE04)
		// NOT EQUAL SPACES AND
		// FUNCTION UPPER-CASE (IN-ADD0LINE04)
		// NOT EQUAL
		// FUNCTION UPPER-CASE(ADD0LINE04 OF PMSL0253-REC(1 : 28))
		// GO TO 2000-EXIT-PMSL0253
		// END-IF.
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0253RecTO2.rec02fmtRec9.getAdd0line04(), 1, wsCtLength))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0253RecTO2.rec02fmtRec9.getLine0bus()) != 0) {
			return "";
		}
		if (pmsl0253RecTO2.rec02fmtRec9.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0253RecTO2.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0253RecTO2.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0253RecTO2.rec02fmtRec9.getTrans0stat();
		holdBcSymbol = pmsl0253RecTO2.rec02fmtRec9.getSymbol();
		holdBcPolicy0num = pmsl0253RecTO2.rec02fmtRec9.getPolicy0num();
		holdBcModule = pmsl0253RecTO2.rec02fmtRec9.getModule();
		holdBcMaster0co = pmsl0253RecTO2.rec02fmtRec9.getMaster0co();
		holdBcLocation = pmsl0253RecTO2.rec02fmtRec9.getLocation();
		holdBcEffdt = pmsl0253RecTO2.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0253RecTO2.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0253RecTO2.rec02fmtRec9.getRisk0state();
		holdBcCompany0no = pmsl0253RecTO2.rec02fmtRec9.getCompany0no();
		holdBcAgency = pmsl0253RecTO2.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0253RecTO2.rec02fmtRec9.getLine0bus();
		holdBcZip0post = pmsl0253RecTO2.rec02fmtRec9.getZip0post();
		holdBcAdd0line01 = pmsl0253RecTO2.rec02fmtRec9.getAdd0line01();
		holdBcAdd0line02 = pmsl0253RecTO2.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0253RecTO2.rec02fmtRec9.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0253RecTO2.rec02fmtRec9.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0253RecTO2.rec02fmtRec9.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0253RecTO2.rec02fmtRec9.getRenewal0cd();
		holdBcIssue0code = pmsl0253RecTO2.rec02fmtRec9.getIssue0code();
		holdBcPay0code = pmsl0253RecTO2.rec02fmtRec9.getPay0code();
		holdBcMode0code = pmsl0253RecTO2.rec02fmtRec9.getMode0code();
		holdBcSort0name = pmsl0253RecTO2.rec02fmtRec9.getSort0name();
		holdBcCust0no = pmsl0253RecTO2.rec02fmtRec9.getCust0no();
		holdBcSpec0use0a = pmsl0253RecTO2.rec02fmtRec9.getSpec0use0a();
		holdBcSpec0use0b = pmsl0253RecTO2.rec02fmtRec9.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0253RecTO2.rec02fmtRec9.getCanceldate();
		}
		holdBcMrsseq = pmsl0253RecTO2.rec02fmtRec9.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0253RecTO2.rec02fmtRec9.getTot0ag0prm());
		holdBcType0act = pmsl0253RecTO2.rec02fmtRec9.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0253RecTO2.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0253RecTO2.getRec02fmtExpdtAsStr()) > 0 ){
				return "";
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return "";
		}
		return "";
	}

	public void exitPmsl0253() {
	// exit
	}

	public String readPmsl0254() {
		pmsl0254TO = pmsl0254DAO.fetchNext(pmsl0254TO);
		pmsl0254RecTO2.setData(pmsl0254TO.getData());
		pmsl0254RecTO2.setData(pmsl0254RecTO2.getData());
		ws1FileStatus = Functions.subString(pmsl0254TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0254TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0254RecTO2.rec02fmtRec10.getCompany0no()) != 0) {
				return "";
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0254RecTO2.getRec02fmtAgencyAsStr()) != 0) {
				return "";
			}
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0
			&& rskdbio028TO.getInLocation().compareTo(pmsl0254RecTO2.rec02fmtRec10.getLocation()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0254RecTO2.rec02fmtRec10.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInLine0bus().compareTo(pmsl0254RecTO2.rec02fmtRec10.getLine0bus()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return "";
			}
			return "2000-READ-PMSL0254";
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0254RecTO2.getRec02fmtAgencyAsStr()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0254RecTO2.rec02fmtRec10.getZip0post(), 1, wsZcLength)) != 0) {
			return "";
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0254RecTO2.rec02fmtRec10.getAdd0line04(), 29, 2))) != 0) {
			return "";
		}
		// IF FUNCTION UPPER-CASE (IN-ADD0LINE04)
		// NOT EQUAL SPACES AND
		// FUNCTION UPPER-CASE (IN-ADD0LINE04)
		// NOT EQUAL
		// FUNCTION UPPER-CASE(ADD0LINE04 OF PMSL0254-REC(1 : 28))
		// GO TO 2000-EXIT-PMSL0254
		// END-IF.
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0254RecTO2.rec02fmtRec10.getAdd0line04(), 1, wsCtLength))) != 0) {
			return "";
		}
		if (pmsl0254RecTO2.rec02fmtRec10.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0254RecTO2.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0254RecTO2.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0254RecTO2.rec02fmtRec10.getTrans0stat();
		holdBcSymbol = pmsl0254RecTO2.rec02fmtRec10.getSymbol();
		holdBcPolicy0num = pmsl0254RecTO2.rec02fmtRec10.getPolicy0num();
		holdBcModule = pmsl0254RecTO2.rec02fmtRec10.getModule();
		holdBcMaster0co = pmsl0254RecTO2.rec02fmtRec10.getMaster0co();
		holdBcLocation = pmsl0254RecTO2.rec02fmtRec10.getLocation();
		holdBcEffdt = pmsl0254RecTO2.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0254RecTO2.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0254RecTO2.rec02fmtRec10.getRisk0state();
		holdBcCompany0no = pmsl0254RecTO2.rec02fmtRec10.getCompany0no();
		holdBcAgency = pmsl0254RecTO2.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0254RecTO2.rec02fmtRec10.getLine0bus();
		holdBcZip0post = pmsl0254RecTO2.rec02fmtRec10.getZip0post();
		holdBcAdd0line01 = pmsl0254RecTO2.rec02fmtRec10.getAdd0line01();
		holdBcAdd0line02 = pmsl0254RecTO2.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0254RecTO2.rec02fmtRec10.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0254RecTO2.rec02fmtRec10.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0254RecTO2.rec02fmtRec10.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0254RecTO2.rec02fmtRec10.getRenewal0cd();
		holdBcIssue0code = pmsl0254RecTO2.rec02fmtRec10.getIssue0code();
		holdBcPay0code = pmsl0254RecTO2.rec02fmtRec10.getPay0code();
		holdBcMode0code = pmsl0254RecTO2.rec02fmtRec10.getMode0code();
		holdBcSort0name = pmsl0254RecTO2.rec02fmtRec10.getSort0name();
		holdBcCust0no = pmsl0254RecTO2.rec02fmtRec10.getCust0no();
		holdBcSpec0use0a = pmsl0254RecTO2.rec02fmtRec10.getSpec0use0a();
		holdBcSpec0use0b = pmsl0254RecTO2.rec02fmtRec10.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0254RecTO2.rec02fmtRec10.getCanceldate();
		}
		holdBcMrsseq = pmsl0254RecTO2.rec02fmtRec10.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0254RecTO2.rec02fmtRec10.getTot0ag0prm());
		holdBcType0act = pmsl0254RecTO2.rec02fmtRec10.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0254RecTO2.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0254RecTO2.getRec02fmtExpdtAsStr()) > 0 ){
				return "";
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return "";
		}
		return "";
	}

	public void exitPmsl0254() {
	// exit
	}

	public void readPmsl0259() {
		pmsl0259TO = pmsl0259DAO.fetchNext(pmsl0259TO);
		pmsl0259RecTO2.setData(pmsl0259TO.getData());
		pmsl0259RecTO2.setData(pmsl0259RecTO2.getData());
		ws1FileStatus = Functions.subString(pmsl0259TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0259TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0259RecTO2.rec02fmtRec2.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0259RecTO2.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0259RecTO2.rec02fmtRec2.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0259RecTO2.rec02fmtRec2.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (pmsl0259RecTO2.rec02fmtRec2.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0
			|| pmsl0259RecTO2.rec02fmtRec2.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0
			|| pmsl0259RecTO2.rec02fmtRec2.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio028TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0259RecTO2.rec02fmtRec2.getCust0no())) != 0) {
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0259RecTO2.rec02fmtRec2.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0259RecTO2.rec02fmtRec2.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0259RecTO2.rec02fmtRec2.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0259RecTO2.rec02fmtRec2.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0259RecTO2.rec02fmtRec2.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0259RecTO2.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0259RecTO2.rec02fmtRec2.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio028TO.getInDbaname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInDbaname(), 1, wsDbanameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0259RecTO2.getRec02fmtAdd0line02AsStr(), 1, wsDbanameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInFein().compareTo("") != 0) {
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'N') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsl0259RecTO2.rec02fmtRec2.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio028TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio028TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio028TO.getInFeinIndInt() == 'Y' && rskdbio028TO.getInFeinIndSite() == 'N') {
				pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsl0259RecTO2.rec02fmtRec2.getMaster0co());
				pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
				pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
				pmsj1200RecTO.rcdj1200Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsj1200RecTO.rcdj1200Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
				pmsj1200RecTO.rcdj1200Rec.setModule(rskdbio028TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsj1200TO =
					Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());*/
				pmsj1200TO =
					pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
					pmsj1200RecTO.setData(pmsj1200TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'Y') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsl0259RecTO2.rec02fmtRec2.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio028TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio028TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					feinIndicator.setValue('N');
				}
				if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
					feinIndicator.setValue('Y');
				} else {
					feinIndicator.setValue('N');
				}
				if (feinIndicator.isFeinNotFound()) {
					pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsl0259RecTO2.rec02fmtRec2.getMaster0co());
					pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
					pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
					pmsj1200RecTO.rcdj1200Rec.setSymbol(rskdbio028TO.getInSymbol());
					pmsj1200RecTO.rcdj1200Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
					pmsj1200RecTO.rcdj1200Rec.setModule(rskdbio028TO.getInModule());
					// BPHX changes start - fix 157870
					/*pmsj1200TO =
						Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());*/
					pmsj1200TO =
						pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());
					// BPHX changes end - fix 157870
					if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
						pmsj1200RecTO.setData(pmsj1200TO.getData());
					}
					ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
					if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
						feinIndicator.setValue('N');
					}
					if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
						feinIndicator.setValue('Y');
					} else {
						feinIndicator.setValue('N');
					}
				}
				if (feinIndicator.isFeinNotFound()) {
					return;
				}
			}
		}
		if (rskdbio028TO.getInAddnlIntType().compareTo("") != 0) {
			pmsj1201RecTO.rcdj1201Rec.setMaster0co(pmsl0259RecTO2.rec02fmtRec2.getMaster0co());
			wsTmpMaster0co = pmsl0259RecTO2.rec02fmtRec2.getMaster0co();
			pmsj1201RecTO.rcdj1201Rec.setLocation(rskdbio028TO.getInLocation());
			pmsj1201RecTO.rcdj1201Rec.setUse0code(rskdbio028TO.getInAddnlIntType());
			pmsj1201RecTO.rcdj1201Rec.setSymbol(rskdbio028TO.getInSymbol());
			pmsj1201RecTO.rcdj1201Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
			pmsj1201RecTO.rcdj1201Rec.setModule(rskdbio028TO.getInModule());
			wsTmpModule = rskdbio028TO.getInModule();
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.openPmsj1201TOCsr(KeyType.GREATER_OR_EQ, pmsj1201RecTO.rcdj1201Rec.getLocation(),
				pmsj1201RecTO.rcdj1201Rec.getMaster0co(), pmsj1201RecTO.rcdj1201Rec.getUse0code(), pmsj1201RecTO.rcdj1201Rec.getSymbol(),
				pmsj1201RecTO.rcdj1201Rec.getPolicy0num(), pmsj1201RecTO.rcdj1201Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endCrossRead == END_FILE_REACHED || addnlTypeIndicator.isAddnlTypeFound())) {
				readPmsj1201Int();
			}
			if (addnlTypeIndicator.isAddnlTypeNotFound()) {
				return;
			}
			if (addnlTypeIndicator.isAddnlTypeFound() && rskdbio028TO.getInAddnlIntName().compareTo("") != 0) {
				if (addnlIntIndicator.isAddnlIntFound()) {
					wsChkIndic.setValue('N');
				} else {
					return;
				}
			}
		}
		if (rskdbio028TO.getInAddnlIntName().compareTo("") != 0 && !wsChkIndic.isNoCheckRequired()) {
			endCrossRead = ' ';
			pmsj1202RecTO.rcdj1202Rec.setMaster0co(pmsl0259RecTO2.rec02fmtRec2.getMaster0co());
			wsTmpMaster0co = pmsl0259RecTO2.rec02fmtRec2.getMaster0co();
			pmsj1202RecTO.rcdj1202Rec.setLocation(rskdbio028TO.getInLocation());
			pmsj1202RecTO.rcdj1202Rec.setDesc0line1(rskdbio028TO.getInAddnlIntName());
			pmsj1202RecTO.rcdj1202Rec.setSymbol(rskdbio028TO.getInSymbol());
			pmsj1202RecTO.rcdj1202Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
			pmsj1202RecTO.rcdj1202Rec.setModule(rskdbio028TO.getInModule());
			wsTmpModule = rskdbio028TO.getInModule();
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.openPmsj1202TOCsr(KeyType.GREATER_OR_EQ, pmsj1202RecTO.rcdj1202Rec.getLocation(),
				pmsj1202RecTO.rcdj1202Rec.getMaster0co(), pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), pmsj1202RecTO.rcdj1202Rec.getSymbol(),
				pmsj1202RecTO.rcdj1202Rec.getPolicy0num(), pmsj1202RecTO.rcdj1202Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endCrossRead == END_FILE_REACHED || addnlIntIndicator.isAddnlIntFound())) {
				readPmsj1202Int();
			}
			if (addnlIntIndicator.isAddnlIntNotFound()) {
				endCrossRead = ' ';
				pmsj1202RecTO.rcdj1202Rec.setMaster0co(pmsl0259RecTO2.rec02fmtRec2.getMaster0co());
				wsTmpMaster0co = pmsl0259RecTO2.rec02fmtRec2.getMaster0co();
				pmsj1202RecTO.rcdj1202Rec.setLocation(rskdbio028TO.getInLocation());
				pmsj1202RecTO.rcdj1202Rec.setDesc0line1(Functions.subString(ABOSystem.Lower(rskdbio028TO.getInAddnlIntName()), 1, 30));
				pmsj1202RecTO.rcdj1202Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsj1202RecTO.rcdj1202Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
				pmsj1202RecTO.rcdj1202Rec.setModule(rskdbio028TO.getInModule());
				wsTmpModule = rskdbio028TO.getInModule();
				cobolsqlca.setDBAccessStatus(pmsj1202DAO.openPmsj1202TOCsr(KeyType.GREATER_OR_EQ, pmsj1202RecTO.rcdj1202Rec.getLocation(),
					pmsj1202RecTO.rcdj1202Rec.getMaster0co(), pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), pmsj1202RecTO.rcdj1202Rec.getSymbol(),
					pmsj1202RecTO.rcdj1202Rec.getPolicy0num(), pmsj1202RecTO.rcdj1202Rec.getModule()));
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				if (cobolsqlca.isInvalidKey()) {
					return;
				}
				while (!(endCrossRead == END_FILE_REACHED || addnlIntIndicator.isAddnlIntFound())) {
					readPmsj1202Int();
				}
			}
			if (addnlIntIndicator.isAddnlIntNotFound()) {
				return;
			}
		}
		if (pmsl0259RecTO2.rec02fmtRec2.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0259RecTO2.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0259RecTO2.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0259RecTO2.rec02fmtRec2.getTrans0stat();
		holdBcSymbol = pmsl0259RecTO2.rec02fmtRec2.getSymbol();
		holdBcPolicy0num = pmsl0259RecTO2.rec02fmtRec2.getPolicy0num();
		holdBcModule = pmsl0259RecTO2.rec02fmtRec2.getModule();
		holdBcMaster0co = pmsl0259RecTO2.rec02fmtRec2.getMaster0co();
		holdBcLocation = pmsl0259RecTO2.rec02fmtRec2.getLocation();
		holdBcEffdt = pmsl0259RecTO2.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0259RecTO2.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0259RecTO2.rec02fmtRec2.getRisk0state();
		holdBcCompany0no = pmsl0259RecTO2.rec02fmtRec2.getCompany0no();
		holdBcAgency = pmsl0259RecTO2.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0259RecTO2.rec02fmtRec2.getLine0bus();
		holdBcZip0post = pmsl0259RecTO2.rec02fmtRec2.getZip0post();
		holdBcAdd0line01 = pmsl0259RecTO2.rec02fmtRec2.getAdd0line01();
		holdBcAdd0line02 = pmsl0259RecTO2.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0259RecTO2.rec02fmtRec2.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0259RecTO2.rec02fmtRec2.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0259RecTO2.rec02fmtRec2.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0259RecTO2.rec02fmtRec2.getRenewal0cd();
		holdBcIssue0code = pmsl0259RecTO2.rec02fmtRec2.getIssue0code();
		holdBcPay0code = pmsl0259RecTO2.rec02fmtRec2.getPay0code();
		holdBcMode0code = pmsl0259RecTO2.rec02fmtRec2.getMode0code();
		holdBcSort0name = pmsl0259RecTO2.rec02fmtRec2.getSort0name();
		holdBcCust0no = pmsl0259RecTO2.rec02fmtRec2.getCust0no();
		holdBcSpec0use0a = pmsl0259RecTO2.rec02fmtRec2.getSpec0use0a();
		holdBcSpec0use0b = pmsl0259RecTO2.rec02fmtRec2.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0259RecTO2.rec02fmtRec2.getCanceldate();
		}
		holdBcMrsseq = pmsl0259RecTO2.rec02fmtRec2.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0259RecTO2.rec02fmtRec2.getTot0ag0prm());
		holdBcType0act = pmsl0259RecTO2.rec02fmtRec2.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0247RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0247RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsj1202Int() {
		pmsj1202TO = pmsj1202DAO.fetchNext(pmsj1202TO);
		pmsj1202RecTO.setData(pmsj1202TO.getData());
		pmsj1202RecTO.setData(pmsj1202RecTO.getData());
		ws1FileStatus = Functions.subString(pmsj1202TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj1202TO.getDBAccessStatus().isEOF()) {
			endCrossRead = 'Y';
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endCrossRead = 'Y';
			return;
		}
		if (wsTmpMaster0co.compareTo(pmsj1202RecTO.rcdj1202Rec.getMaster0co()) == 0
			&& rskdbio028TO.getInLocation().compareTo(pmsj1202RecTO.rcdj1202Rec.getLocation()) == 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), 1, wsAddnlIntNameLen))) == 0
			&& rskdbio028TO.getInSymbol().compareTo(pmsj1202RecTO.rcdj1202Rec.getSymbol()) == 0
			&& rskdbio028TO.getInPolicy0num().compareTo(pmsj1202RecTO.rcdj1202Rec.getPolicy0num()) == 0
			&& wsTmpModule.compareTo(pmsj1202RecTO.rcdj1202Rec.getModule()) == 0) {
			addnlIntIndicator.setValue('Y');
		}
	}

	public void readPmsj1201Int() {
		pmsj1201TO = pmsj1201DAO.fetchNext(pmsj1201TO);
		pmsj1201RecTO.setData(pmsj1201TO.getData());
		pmsj1201RecTO.setData(pmsj1201RecTO.getData());
		ws1FileStatus = Functions.subString(pmsj1201TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj1201TO.getDBAccessStatus().isEOF()) {
			endCrossRead = 'Y';
			addnlTypeIndicator.setValue('N');
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endCrossRead = 'Y';
			return;
		}
		if (wsTmpMaster0co.compareTo(pmsj1201RecTO.rcdj1201Rec.getMaster0co()) == 0
			&& rskdbio028TO.getInLocation().compareTo(pmsj1201RecTO.rcdj1201Rec.getLocation()) == 0
			&& rskdbio028TO.getInAddnlIntType().compareTo(pmsj1201RecTO.rcdj1201Rec.getUse0code()) == 0
			&& rskdbio028TO.getInSymbol().compareTo(pmsj1201RecTO.rcdj1201Rec.getSymbol()) == 0
			&& rskdbio028TO.getInPolicy0num().compareTo(pmsj1201RecTO.rcdj1201Rec.getPolicy0num()) == 0
			&& wsTmpModule.compareTo(pmsj1201RecTO.rcdj1201Rec.getModule()) == 0) {
			addnlTypeIndicator.setValue('Y');
			if (rskdbio028TO.getInAddnlIntName().compareTo("") != 0
				&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
					ABOSystem.Upper(Functions.subString(pmsj1201RecTO.rcdj1201Rec.getDesc0line1(), 1, wsAddnlIntNameLen))) == 0) {
				addnlIntIndicator.setValue('Y');
			}
		}
	}

	public void readPmsl0255() {
		pmsl0255TO = pmsl0255DAO.fetchNext(pmsl0255TO);
		pmsl0255RecTO2.setData(pmsl0255TO.getData());
		pmsl0255RecTO2.setData(pmsl0255RecTO2.getData());
		ws1FileStatus = Functions.subString(pmsl0255TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0255TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0255RecTO2.rec02fmtRec4.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0255RecTO2.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0255RecTO2.rec02fmtRec4.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0255RecTO2.rec02fmtRec4.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0255RecTO2.rec02fmtRec4.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio028TO.getInPolicy0num().compareTo(pmsl0255RecTO2.rec02fmtRec4.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && rskdbio028TO.getInModule().compareTo(pmsl0255RecTO2.rec02fmtRec4.getModule()) != 0) {
			return;
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio028TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0255RecTO2.rec02fmtRec4.getCust0no())) != 0) {
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0255RecTO2.rec02fmtRec4.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0255RecTO2.rec02fmtRec4.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0255RecTO2.rec02fmtRec4.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0255RecTO2.rec02fmtRec4.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0255RecTO2.rec02fmtRec4.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0255RecTO2.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0255RecTO2.rec02fmtRec4.getLine0bus()) != 0) {
			return;
		}
		if (pmsl0255RecTO2.rec02fmtRec4.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0255RecTO2.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0255RecTO2.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0255RecTO2.rec02fmtRec4.getTrans0stat();
		holdBcSymbol = pmsl0255RecTO2.rec02fmtRec4.getSymbol();
		holdBcPolicy0num = pmsl0255RecTO2.rec02fmtRec4.getPolicy0num();
		holdBcModule = pmsl0255RecTO2.rec02fmtRec4.getModule();
		holdBcMaster0co = pmsl0255RecTO2.rec02fmtRec4.getMaster0co();
		holdBcLocation = pmsl0255RecTO2.rec02fmtRec4.getLocation();
		holdBcEffdt = pmsl0255RecTO2.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0255RecTO2.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0255RecTO2.rec02fmtRec4.getRisk0state();
		holdBcCompany0no = pmsl0255RecTO2.rec02fmtRec4.getCompany0no();
		holdBcAgency = pmsl0255RecTO2.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0255RecTO2.rec02fmtRec4.getLine0bus();
		holdBcZip0post = pmsl0255RecTO2.rec02fmtRec4.getZip0post();
		holdBcAdd0line01 = pmsl0255RecTO2.rec02fmtRec4.getAdd0line01();
		holdBcAdd0line02 = pmsl0255RecTO2.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0255RecTO2.rec02fmtRec4.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0255RecTO2.rec02fmtRec4.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0255RecTO2.rec02fmtRec4.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0255RecTO2.rec02fmtRec4.getRenewal0cd();
		holdBcIssue0code = pmsl0255RecTO2.rec02fmtRec4.getIssue0code();
		holdBcPay0code = pmsl0255RecTO2.rec02fmtRec4.getPay0code();
		holdBcMode0code = pmsl0255RecTO2.rec02fmtRec4.getMode0code();
		holdBcSort0name = pmsl0255RecTO2.rec02fmtRec4.getSort0name();
		holdBcCust0no = pmsl0255RecTO2.rec02fmtRec4.getCust0no();
		holdBcSpec0use0a = pmsl0255RecTO2.rec02fmtRec4.getSpec0use0a();
		holdBcSpec0use0b = pmsl0255RecTO2.rec02fmtRec4.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0255RecTO2.rec02fmtRec4.getCanceldate();
		}
		holdBcMrsseq = pmsl0255RecTO2.rec02fmtRec4.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0255RecTO2.rec02fmtRec4.getTot0ag0prm());
		holdBcType0act = pmsl0255RecTO2.rec02fmtRec4.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0255RecTO2.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0255RecTO2.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			return;
		}
	}

	public void readPmsl0256() {
		pmsl0256TO = pmsl0256DAO.fetchNext(pmsl0256TO);
		pmsl0256RecTO2.setData(pmsl0256TO.getData());
		pmsl0256RecTO2.setData(pmsl0256RecTO2.getData());
		ws1FileStatus = Functions.subString(pmsl0256TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0256TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0256RecTO2.rec02fmtRec5.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0256RecTO2.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0256RecTO2.rec02fmtRec5.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0256RecTO2.rec02fmtRec5.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0256RecTO2.rec02fmtRec5.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && rskdbio028TO.getInSymbol().compareTo(pmsl0256RecTO2.rec02fmtRec5.getSymbol()) != 0) {
			return;
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && rskdbio028TO.getInModule().compareTo(pmsl0256RecTO2.rec02fmtRec5.getModule()) != 0) {
			return;
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio028TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0256RecTO2.rec02fmtRec5.getCust0no())) != 0) {
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0256RecTO2.rec02fmtRec5.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0256RecTO2.rec02fmtRec5.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0256RecTO2.rec02fmtRec5.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0256RecTO2.rec02fmtRec5.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0256RecTO2.rec02fmtRec5.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0256RecTO2.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0256RecTO2.rec02fmtRec5.getLine0bus()) != 0) {
			return;
		}
		if (pmsl0256RecTO2.rec02fmtRec5.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0256RecTO2.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0256RecTO2.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0256RecTO2.rec02fmtRec5.getTrans0stat();
		holdBcSymbol = pmsl0256RecTO2.rec02fmtRec5.getSymbol();
		holdBcPolicy0num = pmsl0256RecTO2.rec02fmtRec5.getPolicy0num();
		holdBcModule = pmsl0256RecTO2.rec02fmtRec5.getModule();
		holdBcMaster0co = pmsl0256RecTO2.rec02fmtRec5.getMaster0co();
		holdBcLocation = pmsl0256RecTO2.rec02fmtRec5.getLocation();
		holdBcEffdt = pmsl0256RecTO2.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0256RecTO2.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0256RecTO2.rec02fmtRec5.getRisk0state();
		holdBcCompany0no = pmsl0256RecTO2.rec02fmtRec5.getCompany0no();
		holdBcAgency = pmsl0256RecTO2.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0256RecTO2.rec02fmtRec5.getLine0bus();
		holdBcZip0post = pmsl0256RecTO2.rec02fmtRec5.getZip0post();
		holdBcAdd0line01 = pmsl0256RecTO2.rec02fmtRec5.getAdd0line01();
		holdBcAdd0line02 = pmsl0256RecTO2.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0256RecTO2.rec02fmtRec5.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0256RecTO2.rec02fmtRec5.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0256RecTO2.rec02fmtRec5.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0256RecTO2.rec02fmtRec5.getRenewal0cd();
		holdBcIssue0code = pmsl0256RecTO2.rec02fmtRec5.getIssue0code();
		holdBcPay0code = pmsl0256RecTO2.rec02fmtRec5.getPay0code();
		holdBcMode0code = pmsl0256RecTO2.rec02fmtRec5.getMode0code();
		holdBcSort0name = pmsl0256RecTO2.rec02fmtRec5.getSort0name();
		holdBcCust0no = pmsl0256RecTO2.rec02fmtRec5.getCust0no();
		holdBcSpec0use0a = pmsl0256RecTO2.rec02fmtRec5.getSpec0use0a();
		holdBcSpec0use0b = pmsl0256RecTO2.rec02fmtRec5.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0256RecTO2.rec02fmtRec5.getCanceldate();
		}
		holdBcMrsseq = pmsl0256RecTO2.rec02fmtRec5.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0256RecTO2.rec02fmtRec5.getTot0ag0prm());
		holdBcType0act = pmsl0256RecTO2.rec02fmtRec5.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0256RecTO2.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0256RecTO2.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsl0257() {
		pmsl0257TO = pmsl0257DAO.fetchNext(pmsl0257TO);
		pmsl0257RecTO2.setData(pmsl0257TO.getData());
		pmsl0257RecTO2.setData(pmsl0257RecTO2.getData());
		ws1FileStatus = Functions.subString(pmsl0257TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0257TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0257RecTO2.rec02fmtRec6.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0257RecTO2.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0257RecTO2.rec02fmtRec6.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& rskdbio028TO.getInPolicy0num().compareTo(pmsl0257RecTO2.rec02fmtRec6.getPolicy0num()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0257RecTO2.rec02fmtRec6.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && rskdbio028TO.getInSymbol().compareTo(pmsl0257RecTO2.rec02fmtRec6.getSymbol()) != 0) {
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0257RecTO2.rec02fmtRec6.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio028TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0257RecTO2.rec02fmtRec6.getCust0no())) != 0) {
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0257RecTO2.rec02fmtRec6.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0257RecTO2.rec02fmtRec6.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0257RecTO2.rec02fmtRec6.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0257RecTO2.rec02fmtRec6.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0257RecTO2.rec02fmtRec6.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0257RecTO2.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0257RecTO2.rec02fmtRec6.getLine0bus()) != 0) {
			return;
		}
		if (pmsl0257RecTO2.rec02fmtRec6.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0257RecTO2.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0257RecTO2.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0257RecTO2.rec02fmtRec6.getTrans0stat();
		holdBcSymbol = pmsl0257RecTO2.rec02fmtRec6.getSymbol();
		holdBcPolicy0num = pmsl0257RecTO2.rec02fmtRec6.getPolicy0num();
		holdBcModule = pmsl0257RecTO2.rec02fmtRec6.getModule();
		holdBcMaster0co = pmsl0257RecTO2.rec02fmtRec6.getMaster0co();
		holdBcLocation = pmsl0257RecTO2.rec02fmtRec6.getLocation();
		holdBcEffdt = pmsl0257RecTO2.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0257RecTO2.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0257RecTO2.rec02fmtRec6.getRisk0state();
		holdBcCompany0no = pmsl0257RecTO2.rec02fmtRec6.getCompany0no();
		holdBcAgency = pmsl0257RecTO2.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0257RecTO2.rec02fmtRec6.getLine0bus();
		holdBcZip0post = pmsl0257RecTO2.rec02fmtRec6.getZip0post();
		holdBcAdd0line01 = pmsl0257RecTO2.rec02fmtRec6.getAdd0line01();
		holdBcAdd0line02 = pmsl0257RecTO2.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0257RecTO2.rec02fmtRec6.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0257RecTO2.rec02fmtRec6.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0257RecTO2.rec02fmtRec6.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0257RecTO2.rec02fmtRec6.getRenewal0cd();
		holdBcIssue0code = pmsl0257RecTO2.rec02fmtRec6.getIssue0code();
		holdBcPay0code = pmsl0257RecTO2.rec02fmtRec6.getPay0code();
		holdBcMode0code = pmsl0257RecTO2.rec02fmtRec6.getMode0code();
		holdBcSort0name = pmsl0257RecTO2.rec02fmtRec6.getSort0name();
		holdBcCust0no = pmsl0257RecTO2.rec02fmtRec6.getCust0no();
		holdBcSpec0use0a = pmsl0257RecTO2.rec02fmtRec6.getSpec0use0a();
		holdBcSpec0use0b = pmsl0257RecTO2.rec02fmtRec6.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0257RecTO2.rec02fmtRec6.getCanceldate();
		}
		holdBcMrsseq = pmsl0257RecTO2.rec02fmtRec6.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0257RecTO2.rec02fmtRec6.getTot0ag0prm());
		holdBcType0act = pmsl0257RecTO2.rec02fmtRec6.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0257RecTO2.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0257RecTO2.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsl0258() {
		pmsl0258TO = pmsl0258DAO.fetchNext(pmsl0258TO);
		pmsl0258RecTO2.setData(pmsl0258TO.getData());
		pmsl0258RecTO2.setData(pmsl0258RecTO2.getData());
		ws1FileStatus = Functions.subString(pmsl0258TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0258TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0258RecTO2.rec02fmtRec7.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0258RecTO2.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0258RecTO2.rec02fmtRec7.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0
			|| pmsl0258RecTO2.rec02fmtRec7.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && pmsl0258RecTO2.rec02fmtRec7.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0258RecTO2.rec02fmtRec7.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0258RecTO2.rec02fmtRec7.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio028TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsl0258RecTO2.rec02fmtRec7.getCust0no())) != 0) {
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0258RecTO2.rec02fmtRec7.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0258RecTO2.rec02fmtRec7.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0258RecTO2.rec02fmtRec7.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0258RecTO2.rec02fmtRec7.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0258RecTO2.rec02fmtRec7.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0258RecTO2.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0258RecTO2.rec02fmtRec7.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio028TO.getInDbaname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInDbaname(), 1, wsDbanameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0258RecTO2.getRec02fmtAdd0line02AsStr(), 1, wsDbanameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInFein().compareTo("") != 0) {
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'N') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsl0258RecTO2.rec02fmtRec7.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio028TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(pmsl0258RecTO2.rec02fmtRec7.getModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio028TO.getInFeinIndInt() == 'Y' && rskdbio028TO.getInFeinIndSite() == 'N') {
				pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsl0258RecTO2.rec02fmtRec7.getMaster0co());
				pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
				pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
				pmsj1200RecTO.rcdj1200Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsj1200RecTO.rcdj1200Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
				pmsj1200RecTO.rcdj1200Rec.setModule(pmsl0258RecTO2.rec02fmtRec7.getModule());
				// BPHX changes start - fix 157870
				/*pmsj1200TO =
					Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());*/
				pmsj1200TO =
					pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
					pmsj1200RecTO.setData(pmsj1200TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'Y') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsl0258RecTO2.rec02fmtRec7.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio028TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(pmsl0258RecTO2.rec02fmtRec7.getModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					feinIndicator.setValue('N');
				}
				if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
					feinIndicator.setValue('Y');
				} else {
					feinIndicator.setValue('N');
				}
				if (feinIndicator.isFeinNotFound()) {
					pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsl0258RecTO2.rec02fmtRec7.getMaster0co());
					pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
					pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
					pmsj1200RecTO.rcdj1200Rec.setSymbol(rskdbio028TO.getInSymbol());
					pmsj1200RecTO.rcdj1200Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
					pmsj1200RecTO.rcdj1200Rec.setModule(pmsl0258RecTO2.rec02fmtRec7.getModule());
					// BPHX changes start - fix 157870
					/*pmsj1200TO =
						Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());*/
					pmsj1200TO =
						pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());
					// BPHX changes end - fix 157870
					if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
						pmsj1200RecTO.setData(pmsj1200TO.getData());
					}
					ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
					if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
						feinIndicator.setValue('N');
					}
					if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
						feinIndicator.setValue('Y');
					} else {
						feinIndicator.setValue('N');
					}
				}
				if (feinIndicator.isFeinNotFound()) {
					return;
				}
			}
		}
		//
		if (rskdbio028TO.getInAddnlIntType().compareTo("") != 0) {
			pmsj1201RecTO.rcdj1201Rec.setMaster0co(pmsl0258RecTO2.rec02fmtRec7.getMaster0co());
			wsTmpMaster0co = pmsl0258RecTO2.rec02fmtRec7.getMaster0co();
			pmsj1201RecTO.rcdj1201Rec.setLocation(rskdbio028TO.getInLocation());
			pmsj1201RecTO.rcdj1201Rec.setUse0code(rskdbio028TO.getInAddnlIntType());
			pmsj1201RecTO.rcdj1201Rec.setSymbol(rskdbio028TO.getInSymbol());
			pmsj1201RecTO.rcdj1201Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
			pmsj1201RecTO.rcdj1201Rec.setModule(pmsl0258RecTO2.rec02fmtRec7.getModule());
			wsTmpModule = pmsl0258RecTO2.rec02fmtRec7.getModule();
			cobolsqlca.setDBAccessStatus(pmsj1201DAO.openPmsj1201TOCsr(KeyType.GREATER_OR_EQ, pmsj1201RecTO.rcdj1201Rec.getLocation(),
				pmsj1201RecTO.rcdj1201Rec.getMaster0co(), pmsj1201RecTO.rcdj1201Rec.getUse0code(), pmsj1201RecTO.rcdj1201Rec.getSymbol(),
				pmsj1201RecTO.rcdj1201Rec.getPolicy0num(), pmsj1201RecTO.rcdj1201Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endCrossRead == END_FILE_REACHED || addnlTypeIndicator.isAddnlTypeFound())) {
				readPmsj1201Int();
			}
			if (addnlTypeIndicator.isAddnlTypeNotFound()) {
				return;
			}
			if (addnlTypeIndicator.isAddnlTypeFound() && rskdbio028TO.getInAddnlIntName().compareTo("") != 0) {
				if (addnlIntIndicator.isAddnlIntFound()) {
					wsChkIndic.setValue('N');
				} else {
					return;
				}
			}
		}
		//
		if (rskdbio028TO.getInAddnlIntName().compareTo("") != 0 && !wsChkIndic.isNoCheckRequired()) {
			endCrossRead = ' ';
			pmsj1202RecTO.rcdj1202Rec.setMaster0co(pmsl0258RecTO2.rec02fmtRec7.getMaster0co());
			wsTmpMaster0co = pmsl0258RecTO2.rec02fmtRec7.getMaster0co();
			pmsj1202RecTO.rcdj1202Rec.setLocation(rskdbio028TO.getInLocation());
			pmsj1202RecTO.rcdj1202Rec.setDesc0line1(rskdbio028TO.getInAddnlIntName());
			pmsj1202RecTO.rcdj1202Rec.setSymbol(rskdbio028TO.getInSymbol());
			pmsj1202RecTO.rcdj1202Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
			pmsj1202RecTO.rcdj1202Rec.setModule(pmsl0258RecTO2.rec02fmtRec7.getModule());
			wsTmpModule = pmsl0258RecTO2.rec02fmtRec7.getModule();
			cobolsqlca.setDBAccessStatus(pmsj1202DAO.openPmsj1202TOCsr(KeyType.GREATER_OR_EQ, pmsj1202RecTO.rcdj1202Rec.getLocation(),
				pmsj1202RecTO.rcdj1202Rec.getMaster0co(), pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), pmsj1202RecTO.rcdj1202Rec.getSymbol(),
				pmsj1202RecTO.rcdj1202Rec.getPolicy0num(), pmsj1202RecTO.rcdj1202Rec.getModule()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				return;
			}
			while (!(endCrossRead == END_FILE_REACHED || addnlIntIndicator.isAddnlIntFound())) {
				readPmsj1202Int();
			}
			if (addnlIntIndicator.isAddnlIntNotFound()) {
				endCrossRead = ' ';
				pmsj1202RecTO.rcdj1202Rec.setMaster0co(pmsl0258RecTO2.rec02fmtRec7.getMaster0co());
				wsTmpMaster0co = pmsl0258RecTO2.rec02fmtRec7.getMaster0co();
				pmsj1202RecTO.rcdj1202Rec.setLocation(rskdbio028TO.getInLocation());
				pmsj1202RecTO.rcdj1202Rec.setDesc0line1(Functions.subString(ABOSystem.Lower(rskdbio028TO.getInAddnlIntName()), 1, 30));
				pmsj1202RecTO.rcdj1202Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsj1202RecTO.rcdj1202Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
				pmsj1202RecTO.rcdj1202Rec.setModule(pmsl0258RecTO2.rec02fmtRec7.getModule());
				wsTmpModule = pmsl0258RecTO2.rec02fmtRec7.getModule();
				cobolsqlca.setDBAccessStatus(pmsj1202DAO.openPmsj1202TOCsr(KeyType.GREATER_OR_EQ, pmsj1202RecTO.rcdj1202Rec.getLocation(),
					pmsj1202RecTO.rcdj1202Rec.getMaster0co(), pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), pmsj1202RecTO.rcdj1202Rec.getSymbol(),
					pmsj1202RecTO.rcdj1202Rec.getPolicy0num(), pmsj1202RecTO.rcdj1202Rec.getModule()));
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				if (cobolsqlca.isInvalidKey()) {
					return;
				}
				while (!(endCrossRead == END_FILE_REACHED || addnlIntIndicator.isAddnlIntFound())) {
					readPmsj1202Int();
				}
			}
			if (addnlIntIndicator.isAddnlIntNotFound()) {
				return;
			}
		}
		if (pmsl0258RecTO2.rec02fmtRec7.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0258RecTO2.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0258RecTO2.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0258RecTO2.rec02fmtRec7.getTrans0stat();
		holdBcSymbol = pmsl0258RecTO2.rec02fmtRec7.getSymbol();
		holdBcPolicy0num = pmsl0258RecTO2.rec02fmtRec7.getPolicy0num();
		holdBcModule = pmsl0258RecTO2.rec02fmtRec7.getModule();
		holdBcMaster0co = pmsl0258RecTO2.rec02fmtRec7.getMaster0co();
		holdBcLocation = pmsl0258RecTO2.rec02fmtRec7.getLocation();
		holdBcEffdt = pmsl0258RecTO2.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0258RecTO2.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0258RecTO2.rec02fmtRec7.getRisk0state();
		holdBcCompany0no = pmsl0258RecTO2.rec02fmtRec7.getCompany0no();
		holdBcAgency = pmsl0258RecTO2.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0258RecTO2.rec02fmtRec7.getLine0bus();
		holdBcZip0post = pmsl0258RecTO2.rec02fmtRec7.getZip0post();
		holdBcAdd0line01 = pmsl0258RecTO2.rec02fmtRec7.getAdd0line01();
		holdBcAdd0line02 = pmsl0258RecTO2.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0258RecTO2.rec02fmtRec7.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0258RecTO2.rec02fmtRec7.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0258RecTO2.rec02fmtRec7.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0258RecTO2.rec02fmtRec7.getRenewal0cd();
		holdBcIssue0code = pmsl0258RecTO2.rec02fmtRec7.getIssue0code();
		holdBcPay0code = pmsl0258RecTO2.rec02fmtRec7.getPay0code();
		holdBcMode0code = pmsl0258RecTO2.rec02fmtRec7.getMode0code();
		holdBcSort0name = pmsl0258RecTO2.rec02fmtRec7.getSort0name();
		holdBcCust0no = pmsl0258RecTO2.rec02fmtRec7.getCust0no();
		holdBcSpec0use0a = pmsl0258RecTO2.rec02fmtRec7.getSpec0use0a();
		holdBcSpec0use0b = pmsl0258RecTO2.rec02fmtRec7.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0258RecTO2.rec02fmtRec7.getCanceldate();
		}
		holdBcMrsseq = pmsl0258RecTO2.rec02fmtRec7.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0258RecTO2.rec02fmtRec7.getTot0ag0prm());
		holdBcType0act = pmsl0258RecTO2.rec02fmtRec7.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0258RecTO2.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0258RecTO2.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsl0252() {
		pmsl0252TO = pmsl0252DAO.fetchNext(pmsl0252TO);
		pmsl0252RecTO2.setData(pmsl0252TO.getData());
		pmsl0252RecTO2.setData(pmsl0252RecTO2.getData());
		ws1FileStatus = Functions.subString(pmsl0252TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0252TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0252RecTO2.rec02fmtRec3.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0252RecTO2.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (pmsl0252RecTO2.rec02fmtRec3.getCust0no().compareTo(rskdbio028TO.getInCust0no()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && pmsl0252RecTO2.rec02fmtRec3.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& pmsl0252RecTO2.rec02fmtRec3.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && pmsl0252RecTO2.rec02fmtRec3.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0252RecTO2.rec02fmtRec3.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0252RecTO2.rec02fmtRec3.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0252RecTO2.rec02fmtRec3.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0252RecTO2.rec02fmtRec3.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0252RecTO2.rec02fmtRec3.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0252RecTO2.rec02fmtRec3.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0252RecTO2.rec02fmtRec3.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0252RecTO2.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0252RecTO2.rec02fmtRec3.getLine0bus()) != 0) {
			return;
		}
		if (pmsl0252RecTO2.rec02fmtRec3.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0252RecTO2.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0252RecTO2.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0252RecTO2.rec02fmtRec3.getTrans0stat();
		holdBcSymbol = pmsl0252RecTO2.rec02fmtRec3.getSymbol();
		holdBcPolicy0num = pmsl0252RecTO2.rec02fmtRec3.getPolicy0num();
		holdBcModule = pmsl0252RecTO2.rec02fmtRec3.getModule();
		holdBcMaster0co = pmsl0252RecTO2.rec02fmtRec3.getMaster0co();
		holdBcLocation = pmsl0252RecTO2.rec02fmtRec3.getLocation();
		holdBcEffdt = pmsl0252RecTO2.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0252RecTO2.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0252RecTO2.rec02fmtRec3.getRisk0state();
		holdBcCompany0no = pmsl0252RecTO2.rec02fmtRec3.getCompany0no();
		holdBcAgency = pmsl0252RecTO2.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0252RecTO2.rec02fmtRec3.getLine0bus();
		holdBcZip0post = pmsl0252RecTO2.rec02fmtRec3.getZip0post();
		holdBcAdd0line01 = pmsl0252RecTO2.rec02fmtRec3.getAdd0line01();
		holdBcAdd0line02 = pmsl0252RecTO2.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0252RecTO2.rec02fmtRec3.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0252RecTO2.rec02fmtRec3.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0252RecTO2.rec02fmtRec3.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0252RecTO2.rec02fmtRec3.getRenewal0cd();
		holdBcIssue0code = pmsl0252RecTO2.rec02fmtRec3.getIssue0code();
		holdBcPay0code = pmsl0252RecTO2.rec02fmtRec3.getPay0code();
		holdBcMode0code = pmsl0252RecTO2.rec02fmtRec3.getMode0code();
		holdBcSort0name = pmsl0252RecTO2.rec02fmtRec3.getSort0name();
		holdBcCust0no = pmsl0252RecTO2.rec02fmtRec3.getCust0no();
		holdBcSpec0use0a = pmsl0252RecTO2.rec02fmtRec3.getSpec0use0a();
		holdBcSpec0use0b = pmsl0252RecTO2.rec02fmtRec3.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0252RecTO2.rec02fmtRec3.getCanceldate();
		}
		holdBcMrsseq = pmsl0252RecTO2.rec02fmtRec3.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0252RecTO2.rec02fmtRec3.getTot0ag0prm());
		holdBcType0act = pmsl0252RecTO2.rec02fmtRec3.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0252RecTO2.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0252RecTO2.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsl0250() {
		pmsl0250TO = pmsl0250DAO.fetchNext(pmsl0250TO);
		pmsl0250RecTO.setData(pmsl0250TO.getData());
		pmsl0250RecTO.setData(pmsl0250RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0250TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0250TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0250RecTO.rec02fmtRec11.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0250RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0 && pmsl0250RecTO.rec02fmtRec11.getCust0no().compareTo(rskdbio028TO.getInCust0no()) != 0) {
			return;
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && pmsl0250RecTO.rec02fmtRec11.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& pmsl0250RecTO.rec02fmtRec11.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && pmsl0250RecTO.rec02fmtRec11.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0250RecTO.rec02fmtRec11.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0250RecTO.rec02fmtRec11.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0250RecTO.rec02fmtRec11.getAdd0line01(), 1, wsNameLength))) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0250RecTO.rec02fmtRec11.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0250RecTO.rec02fmtRec11.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0250RecTO.rec02fmtRec11.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0250RecTO.rec02fmtRec11.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0250RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0250RecTO.rec02fmtRec11.getLine0bus()) != 0) {
			return;
		}
		if (pmsl0250RecTO.rec02fmtRec11.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0250RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0250RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0250RecTO.rec02fmtRec11.getTrans0stat();
		holdBcSymbol = pmsl0250RecTO.rec02fmtRec11.getSymbol();
		holdBcPolicy0num = pmsl0250RecTO.rec02fmtRec11.getPolicy0num();
		holdBcModule = pmsl0250RecTO.rec02fmtRec11.getModule();
		holdBcMaster0co = pmsl0250RecTO.rec02fmtRec11.getMaster0co();
		holdBcLocation = pmsl0250RecTO.rec02fmtRec11.getLocation();
		holdBcEffdt = pmsl0250RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0250RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0250RecTO.rec02fmtRec11.getRisk0state();
		holdBcCompany0no = pmsl0250RecTO.rec02fmtRec11.getCompany0no();
		holdBcAgency = pmsl0250RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0250RecTO.rec02fmtRec11.getLine0bus();
		holdBcZip0post = pmsl0250RecTO.rec02fmtRec11.getZip0post();
		holdBcAdd0line01 = pmsl0250RecTO.rec02fmtRec11.getAdd0line01();
		holdBcAdd0line02 = pmsl0250RecTO.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0250RecTO.rec02fmtRec11.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0250RecTO.rec02fmtRec11.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0250RecTO.rec02fmtRec11.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0250RecTO.rec02fmtRec11.getRenewal0cd();
		holdBcIssue0code = pmsl0250RecTO.rec02fmtRec11.getIssue0code();
		holdBcPay0code = pmsl0250RecTO.rec02fmtRec11.getPay0code();
		holdBcMode0code = pmsl0250RecTO.rec02fmtRec11.getMode0code();
		holdBcSort0name = pmsl0250RecTO.rec02fmtRec11.getSort0name();
		holdBcCust0no = pmsl0250RecTO.rec02fmtRec11.getCust0no();
		holdBcSpec0use0a = pmsl0250RecTO.rec02fmtRec11.getSpec0use0a();
		holdBcSpec0use0b = pmsl0250RecTO.rec02fmtRec11.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0250RecTO.rec02fmtRec11.getCanceldate();
		}
		holdBcMrsseq = pmsl0250RecTO.rec02fmtRec11.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0250RecTO.rec02fmtRec11.getTot0ag0prm());
		holdBcType0act = pmsl0250RecTO.rec02fmtRec11.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0250RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0250RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsl0251() {
		pmsl0251TO = pmsl0251DAO.fetchNext(pmsl0251TO);
		pmsl0251RecTO.setData(pmsl0251TO.getData());
		pmsl0251RecTO.setData(pmsl0251RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0251TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0251TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0251RecTO.rec02fmtRec12.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0251RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0 && pmsl0251RecTO.rec02fmtRec12.getCust0no().compareTo(rskdbio028TO.getInCust0no()) != 0) {
			return;
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && pmsl0251RecTO.rec02fmtRec12.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& pmsl0251RecTO.rec02fmtRec12.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && pmsl0251RecTO.rec02fmtRec12.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsl0251RecTO.rec02fmtRec12.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0251RecTO.rec02fmtRec12.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0251RecTO.rec02fmtRec12.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0251RecTO.rec02fmtRec12.getSort0name(), 1, wsSnLength))) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0251RecTO.rec02fmtRec12.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0251RecTO.rec02fmtRec12.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0251RecTO.rec02fmtRec12.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0251RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsl0251RecTO.rec02fmtRec12.getLine0bus()) != 0) {
			return;
		}
		if (pmsl0251RecTO.rec02fmtRec12.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0251RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0251RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0251RecTO.rec02fmtRec12.getTrans0stat();
		holdBcSymbol = pmsl0251RecTO.rec02fmtRec12.getSymbol();
		holdBcPolicy0num = pmsl0251RecTO.rec02fmtRec12.getPolicy0num();
		holdBcModule = pmsl0251RecTO.rec02fmtRec12.getModule();
		holdBcMaster0co = pmsl0251RecTO.rec02fmtRec12.getMaster0co();
		holdBcLocation = pmsl0251RecTO.rec02fmtRec12.getLocation();
		holdBcEffdt = pmsl0251RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0251RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0251RecTO.rec02fmtRec12.getRisk0state();
		holdBcCompany0no = pmsl0251RecTO.rec02fmtRec12.getCompany0no();
		holdBcAgency = pmsl0251RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0251RecTO.rec02fmtRec12.getLine0bus();
		holdBcZip0post = pmsl0251RecTO.rec02fmtRec12.getZip0post();
		holdBcAdd0line01 = pmsl0251RecTO.rec02fmtRec12.getAdd0line01();
		holdBcAdd0line02 = pmsl0251RecTO.getRec02fmtAdd0line02AsStr();
		holdBcAdd0line03 = pmsl0251RecTO.rec02fmtRec12.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0251RecTO.rec02fmtRec12.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0251RecTO.rec02fmtRec12.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0251RecTO.rec02fmtRec12.getRenewal0cd();
		holdBcIssue0code = pmsl0251RecTO.rec02fmtRec12.getIssue0code();
		holdBcPay0code = pmsl0251RecTO.rec02fmtRec12.getPay0code();
		holdBcMode0code = pmsl0251RecTO.rec02fmtRec12.getMode0code();
		holdBcSort0name = pmsl0251RecTO.rec02fmtRec12.getSort0name();
		holdBcCust0no = pmsl0251RecTO.rec02fmtRec12.getCust0no();
		holdBcSpec0use0a = pmsl0251RecTO.rec02fmtRec12.getSpec0use0a();
		holdBcSpec0use0b = pmsl0251RecTO.rec02fmtRec12.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0251RecTO.rec02fmtRec12.getCanceldate();
		}
		holdBcMrsseq = pmsl0251RecTO.rec02fmtRec12.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0251RecTO.rec02fmtRec12.getTot0ag0prm());
		holdBcType0act = pmsl0251RecTO.rec02fmtRec12.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0251RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0251RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return;
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsj1201() {
		pmsj1201TO = pmsj1201DAO.fetchNext(pmsj1201TO);
		pmsj1201RecTO.setData(pmsj1201TO.getData());
		pmsj1201RecTO.setData(pmsj1201RecTO.getData());
		ws1FileStatus = Functions.subString(pmsj1201TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj1201TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsj1201RecTO.rcdj1201Rec.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsj1201RecTO.getRcdj1201AgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsj1201RecTO.rcdj1201Rec.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsj1201RecTO.rcdj1201Rec.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInAddnlIntName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1201RecTO.rcdj1201Rec.getDesc0line1(), 1, wsAddnlIntNameLen))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAddnlIntType().compareTo("") != 0
			&& pmsj1201RecTO.rcdj1201Rec.getUse0code().compareTo(rskdbio028TO.getInAddnlIntType()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && pmsj1201RecTO.rcdj1201Rec.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& pmsj1201RecTO.rcdj1201Rec.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && pmsj1201RecTO.rcdj1201Rec.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio028TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsj1201RecTO.rcdj1201Rec.getCust0no())) != 0) {
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1201RecTO.rcdj1201Rec.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1201RecTO.rcdj1201Rec.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1201RecTO.rcdj1201Rec.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsj1201RecTO.rcdj1201Rec.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsj1201RecTO.rcdj1201Rec.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsj1201RecTO.getRcdj1201AgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsj1201RecTO.rcdj1201Rec.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio028TO.getInDbaname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInDbaname(), 1, wsDbanameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1201RecTO.getRcdj1201Add0line02AsStr(), 1, wsDbanameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInFein().compareTo("") != 0) {
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'N') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsj1201RecTO.rcdj1201Rec.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio028TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio028TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio028TO.getInFeinIndInt() == 'Y' && rskdbio028TO.getInFeinIndSite() == 'N') {
				pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsj1201RecTO.rcdj1201Rec.getMaster0co());
				pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
				pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
				pmsj1200RecTO.rcdj1200Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsj1200RecTO.rcdj1200Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
				pmsj1200RecTO.rcdj1200Rec.setModule(rskdbio028TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsj1200TO =
					Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());*/
				pmsj1200TO =
					pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
					pmsj1200RecTO.setData(pmsj1200TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'Y') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsj1201RecTO.rcdj1201Rec.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio028TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio028TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					feinIndicator.setValue('N');
				}
				if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
					feinIndicator.setValue('Y');
				} else {
					feinIndicator.setValue('N');
				}
				if (feinIndicator.isFeinNotFound()) {
					pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsj1201RecTO.rcdj1201Rec.getMaster0co());
					pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
					pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
					pmsj1200RecTO.rcdj1200Rec.setSymbol(rskdbio028TO.getInSymbol());
					pmsj1200RecTO.rcdj1200Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
					pmsj1200RecTO.rcdj1200Rec.setModule(rskdbio028TO.getInModule());
					// BPHX changes start - fix 157870
					/*pmsj1200TO =
						Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());*/
					pmsj1200TO =
						pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());
					// BPHX changes end - fix 157870
					if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
						pmsj1200RecTO.setData(pmsj1200TO.getData());
					}
					ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
					if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
						feinIndicator.setValue('N');
					}
					if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
						feinIndicator.setValue('Y');
					} else {
						feinIndicator.setValue('N');
					}
				}
				if (feinIndicator.isFeinNotFound()) {
					return;
				}
			}
		}
		//
		if (pmsj1201RecTO.rcdj1201Rec.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsj1201RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
				// continue
			}
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsj1201RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsj1201RecTO.rcdj1201Rec.getTrans0stat();
		holdBcSymbol = pmsj1201RecTO.rcdj1201Rec.getSymbol();
		holdBcPolicy0num = pmsj1201RecTO.rcdj1201Rec.getPolicy0num();
		holdBcModule = pmsj1201RecTO.rcdj1201Rec.getModule();
		holdBcMaster0co = pmsj1201RecTO.rcdj1201Rec.getMaster0co();
		holdBcLocation = pmsj1201RecTO.rcdj1201Rec.getLocation();
		holdBcEffdt = pmsj1201RecTO.getRcdj1201EffdtAsStr();
		holdBcExpdt = pmsj1201RecTO.getRcdj1201ExpdtAsStr();
		holdBcRisk0state = pmsj1201RecTO.rcdj1201Rec.getRisk0state();
		holdBcCompany0no = pmsj1201RecTO.rcdj1201Rec.getCompany0no();
		holdBcAgency = pmsj1201RecTO.getRcdj1201AgencyAsStr();
		holdBcLine0bus = pmsj1201RecTO.rcdj1201Rec.getLine0bus();
		holdBcZip0post = pmsj1201RecTO.rcdj1201Rec.getZip0post();
		holdBcAdd0line01 = pmsj1201RecTO.rcdj1201Rec.getAdd0line01();
		holdBcAdd0line02 = pmsj1201RecTO.getRcdj1201Add0line02AsStr();
		holdBcAdd0line03 = pmsj1201RecTO.rcdj1201Rec.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsj1201RecTO.rcdj1201Rec.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsj1201RecTO.rcdj1201Rec.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsj1201RecTO.rcdj1201Rec.getRenewal0cd();
		holdBcIssue0code = pmsj1201RecTO.rcdj1201Rec.getIssue0code();
		holdBcPay0code = pmsj1201RecTO.rcdj1201Rec.getPay0code();
		holdBcMode0code = pmsj1201RecTO.rcdj1201Rec.getMode0code();
		holdBcSort0name = pmsj1201RecTO.rcdj1201Rec.getSort0name();
		holdBcCust0no = pmsj1201RecTO.rcdj1201Rec.getCust0no();
		holdBcSpec0use0a = pmsj1201RecTO.rcdj1201Rec.getSpec0use0a();
		holdBcSpec0use0b = pmsj1201RecTO.rcdj1201Rec.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsj1201RecTO.rcdj1201Rec.getCanceldate();
		}
		holdBcMrsseq = pmsj1201RecTO.rcdj1201Rec.getMrsseq();
		holdBcTot0ag0prm.assign(pmsj1201RecTO.rcdj1201Rec.getTot0ag0prm());
		holdBcType0act = pmsj1201RecTO.rcdj1201Rec.getType0act();
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void readPmsj1202() {
		pmsj1202TO = pmsj1202DAO.fetchNext(pmsj1202TO);
		pmsj1202RecTO.setData(pmsj1202TO.getData());
		pmsj1202RecTO.setData(pmsj1202RecTO.getData());
		ws1FileStatus = Functions.subString(pmsj1202TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj1202TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsj1202RecTO.rcdj1202Rec.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsj1202RecTO.getRcdj1202AgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsj1202RecTO.rcdj1202Rec.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsj1202RecTO.rcdj1202Rec.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInAddnlIntName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAddnlIntName(), 1, wsAddnlIntNameLen)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1202RecTO.rcdj1202Rec.getDesc0line1(), 1, wsAddnlIntNameLen))) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
			}
			return;
		}
		if (rskdbio028TO.getInAddnlIntType().compareTo("") != 0
			&& pmsj1202RecTO.rcdj1202Rec.getUse0code().compareTo(rskdbio028TO.getInAddnlIntType()) != 0) {
			return;
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && pmsj1202RecTO.rcdj1202Rec.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& pmsj1202RecTO.rcdj1202Rec.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && pmsj1202RecTO.rcdj1202Rec.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0
			&& ABOSystem.Upper(rskdbio028TO.getInCust0no()).compareTo(ABOSystem.Upper(pmsj1202RecTO.rcdj1202Rec.getCust0no())) != 0) {
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1202RecTO.rcdj1202Rec.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1202RecTO.rcdj1202Rec.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1202RecTO.rcdj1202Rec.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsj1202RecTO.rcdj1202Rec.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsj1202RecTO.rcdj1202Rec.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsj1202RecTO.getRcdj1202AgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsj1202RecTO.rcdj1202Rec.getLine0bus()) != 0) {
			return;
		}
		if (rskdbio028TO.getInDbaname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInDbaname(), 1, wsDbanameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1202RecTO.getRcdj1202Add0line02AsStr(), 1, wsDbanameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInFein().compareTo("") != 0) {
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'N') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsj1202RecTO.rcdj1202Rec.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio028TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio028TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio028TO.getInFeinIndInt() == 'Y' && rskdbio028TO.getInFeinIndSite() == 'N') {
				pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsj1202RecTO.rcdj1202Rec.getMaster0co());
				pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
				pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
				pmsj1200RecTO.rcdj1200Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsj1200RecTO.rcdj1200Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
				pmsj1200RecTO.rcdj1200Rec.setModule(rskdbio028TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsj1200TO =
					Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());*/
				pmsj1200TO =
					pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
					pmsj1200RecTO.setData(pmsj1200TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
					return;
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return;
				}
			}
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'Y') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsj1202RecTO.rcdj1202Rec.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(rskdbio028TO.getInSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(rskdbio028TO.getInPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(rskdbio028TO.getInModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					feinIndicator.setValue('N');
				}
				if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
					feinIndicator.setValue('Y');
				} else {
					feinIndicator.setValue('N');
				}
				if (feinIndicator.isFeinNotFound()) {
					pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsj1202RecTO.rcdj1202Rec.getMaster0co());
					pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
					pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
					pmsj1200RecTO.rcdj1200Rec.setSymbol(rskdbio028TO.getInSymbol());
					pmsj1200RecTO.rcdj1200Rec.setPolicy0num(rskdbio028TO.getInPolicy0num());
					pmsj1200RecTO.rcdj1200Rec.setModule(rskdbio028TO.getInModule());
					// BPHX changes start - fix 157870
					/*pmsj1200TO =
						Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());*/
					pmsj1200TO =
						pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());
					// BPHX changes end - fix 157870
					if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
						pmsj1200RecTO.setData(pmsj1200TO.getData());
					}
					ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
					if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
						feinIndicator.setValue('N');
					}
					if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
						feinIndicator.setValue('Y');
					} else {
						feinIndicator.setValue('N');
					}
				}
				if (feinIndicator.isFeinNotFound()) {
					return;
				}
			}
		}
		if (pmsj1202RecTO.rcdj1202Rec.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsj1202RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
				// continue
			}
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsj1202RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsj1202RecTO.rcdj1202Rec.getTrans0stat();
		holdBcSymbol = pmsj1202RecTO.rcdj1202Rec.getSymbol();
		holdBcPolicy0num = pmsj1202RecTO.rcdj1202Rec.getPolicy0num();
		holdBcModule = pmsj1202RecTO.rcdj1202Rec.getModule();
		holdBcMaster0co = pmsj1202RecTO.rcdj1202Rec.getMaster0co();
		holdBcLocation = pmsj1202RecTO.rcdj1202Rec.getLocation();
		holdBcEffdt = pmsj1202RecTO.getRcdj1202EffdtAsStr();
		holdBcExpdt = pmsj1202RecTO.getRcdj1202ExpdtAsStr();
		holdBcRisk0state = pmsj1202RecTO.rcdj1202Rec.getRisk0state();
		holdBcCompany0no = pmsj1202RecTO.rcdj1202Rec.getCompany0no();
		holdBcAgency = pmsj1202RecTO.getRcdj1202AgencyAsStr();
		holdBcLine0bus = pmsj1202RecTO.rcdj1202Rec.getLine0bus();
		holdBcZip0post = pmsj1202RecTO.rcdj1202Rec.getZip0post();
		holdBcAdd0line01 = pmsj1202RecTO.rcdj1202Rec.getAdd0line01();
		holdBcAdd0line02 = pmsj1202RecTO.getRcdj1202Add0line02AsStr();
		holdBcAdd0line03 = pmsj1202RecTO.rcdj1202Rec.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsj1202RecTO.rcdj1202Rec.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsj1202RecTO.rcdj1202Rec.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsj1202RecTO.rcdj1202Rec.getRenewal0cd();
		holdBcIssue0code = pmsj1202RecTO.rcdj1202Rec.getIssue0code();
		holdBcPay0code = pmsj1202RecTO.rcdj1202Rec.getPay0code();
		holdBcMode0code = pmsj1202RecTO.rcdj1202Rec.getMode0code();
		holdBcSort0name = pmsj1202RecTO.rcdj1202Rec.getSort0name();
		holdBcCust0no = pmsj1202RecTO.rcdj1202Rec.getCust0no();
		holdBcSpec0use0a = pmsj1202RecTO.rcdj1202Rec.getSpec0use0a();
		holdBcSpec0use0b = pmsj1202RecTO.rcdj1202Rec.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsj1202RecTO.rcdj1202Rec.getCanceldate();
		}
		holdBcMrsseq = pmsj1202RecTO.rcdj1202Rec.getMrsseq();
		holdBcTot0ag0prm.assign(pmsj1202RecTO.rcdj1202Rec.getTot0ag0prm());
		holdBcType0act = pmsj1202RecTO.rcdj1202Rec.getType0act();
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public String readPmsl0268() {
		pmsl0268TO = pmsl0268DAO.fetchNext(pmsl0268TO);
		pmsl0268RecTO.setData(pmsl0268TO.getData());
		pmsl0268RecTO.setData(pmsl0268RecTO.getData());
		ws1FileStatus = Functions.subString(pmsl0268TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsl0268TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsl0268RecTO.pmsl0268Record.getCompany0no()) != 0) {
				return "";
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsl0268RecTO.getRec02fmtAgencyAsStr()) != 0) {
				return "";
			}
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0 && pmsl0268RecTO.pmsl0268Record.getCust0no().compareTo(rskdbio028TO.getInCust0no()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && pmsl0268RecTO.pmsl0268Record.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& pmsl0268RecTO.pmsl0268Record.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && pmsl0268RecTO.pmsl0268Record.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0
			&& rskdbio028TO.getInLocation().compareTo(pmsl0268RecTO.pmsl0268Record.getLocation()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsl0268RecTO.pmsl0268Record.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0268RecTO.pmsl0268Record.getAdd0line01(), 1, wsNameLength))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0268RecTO.pmsl0268Record.getSort0name(), 1, wsSnLength))) != 0) {
			return "2000-READ-PMSL0268";
		}
		if (rskdbio028TO.getInDbaname().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInDbaname(), 1, wsDbanameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0268RecTO.pmsl0268Record.getAdd0line02(), 1, wsDbanameLength))) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return "";
			}
			return "2000-READ-PMSL0268";
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsl0268RecTO.pmsl0268Record.getAdd0line04(), 1, wsCtLength))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsl0268RecTO.pmsl0268Record.getAdd0line04(), 29, 2))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsl0268RecTO.pmsl0268Record.getZip0post(), 1, wsZcLength)) != 0) {
			return "";
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsl0268RecTO.getRec02fmtAgencyAsStr()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0
			&& rskdbio028TO.getInLine0bus().compareTo(pmsl0268RecTO.pmsl0268Record.getLine0bus()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInFein().compareTo("") != 0) {
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'N') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsl0268RecTO.pmsl0268Record.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(pmsl0268RecTO.pmsl0268Record.getSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(pmsl0268RecTO.pmsl0268Record.getPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(pmsl0268RecTO.pmsl0268Record.getModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					return "";
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return "";
				}
			}
			if (rskdbio028TO.getInFeinIndInt() == 'Y' && rskdbio028TO.getInFeinIndSite() == 'N') {
				pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsl0268RecTO.pmsl0268Record.getMaster0co());
				pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
				pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
				pmsj1200RecTO.rcdj1200Rec.setSymbol(pmsl0268RecTO.pmsl0268Record.getSymbol());
				pmsj1200RecTO.rcdj1200Rec.setPolicy0num(pmsl0268RecTO.pmsl0268Record.getPolicy0num());
				pmsj1200RecTO.rcdj1200Rec.setModule(pmsl0268RecTO.pmsl0268Record.getModule());
				// BPHX changes start - fix 157870
				/*pmsj1200TO =
					Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());*/
				pmsj1200TO =
					pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
						pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
						pmsj1200RecTO.rcdj1200Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
					pmsj1200RecTO.setData(pmsj1200TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
					return "";
				}
				if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
					return "";
				}
			}
			if (rskdbio028TO.getInFeinIndSite() == 'Y' && rskdbio028TO.getInFeinIndInt() == 'Y') {
				pmsjwc04RecTO.rcdjwc04Rec.setMasterco(pmsl0268RecTO.pmsl0268Record.getMaster0co());
				pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
				pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
				pmsjwc04RecTO.rcdjwc04Rec.setSymbol(pmsl0268RecTO.pmsl0268Record.getSymbol());
				pmsjwc04RecTO.rcdjwc04Rec.setPolicyno(pmsl0268RecTO.pmsl0268Record.getPolicy0num());
				pmsjwc04RecTO.rcdjwc04Rec.setModule(pmsl0268RecTO.pmsl0268Record.getModule());
				// BPHX changes start - fix 157870
				/*pmsjwc04TO =
					Pmsjwc04DAO.selectById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());*/
				pmsjwc04TO =
					pmsjwc04DAO.fetchById(pmsjwc04TO, pmsjwc04RecTO.rcdjwc04Rec.getLocation(), pmsjwc04RecTO.rcdjwc04Rec.getMasterco(),
						pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(), pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(),
						pmsjwc04RecTO.rcdjwc04Rec.getModule());
				// BPHX changes end - fix 157870
				if (pmsjwc04TO.getDBAccessStatus().isSuccess()) {
					pmsjwc04RecTO.setData(pmsjwc04TO.getData());
				}
				ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsjwc04TO.getDBAccessStatus().isInvalidKey()) {
					feinIndicator.setValue('N');
				}
				if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
					feinIndicator.setValue('Y');
				} else {
					feinIndicator.setValue('N');
				}
				if (feinIndicator.isFeinNotFound()) {
					pmsj1200RecTO.rcdj1200Rec.setMaster0co(pmsl0268RecTO.pmsl0268Record.getMaster0co());
					pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
					pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
					pmsj1200RecTO.rcdj1200Rec.setSymbol(pmsl0268RecTO.pmsl0268Record.getSymbol());
					pmsj1200RecTO.rcdj1200Rec.setPolicy0num(pmsl0268RecTO.pmsl0268Record.getPolicy0num());
					pmsj1200RecTO.rcdj1200Rec.setModule(pmsl0268RecTO.pmsl0268Record.getModule());
					// BPHX changes start - fix 157870
					/*pmsj1200TO =
						Pmsj1200DAO.selectById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());*/
					pmsj1200TO =
						pmsj1200DAO.fetchById(pmsj1200TO, pmsj1200RecTO.rcdj1200Rec.getLocation(), pmsj1200RecTO.rcdj1200Rec.getMaster0co(),
							pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(), pmsj1200RecTO.rcdj1200Rec.getPolicy0num(),
							pmsj1200RecTO.rcdj1200Rec.getModule());
					// BPHX changes end - fix 157870
					if (pmsj1200TO.getDBAccessStatus().isSuccess()) {
						pmsj1200RecTO.setData(pmsj1200TO.getData());
					}
					ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
					if (pmsj1200TO.getDBAccessStatus().isInvalidKey()) {
						feinIndicator.setValue('N');
					}
					if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
						feinIndicator.setValue('Y');
					} else {
						feinIndicator.setValue('N');
					}
				}
				if (feinIndicator.isFeinNotFound()) {
					return "";
				}
			}
		}
		if (pmsl0268RecTO.pmsl0268Record.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsl0268RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
				// continue
			}
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsl0268RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsl0268RecTO.pmsl0268Record.getTrans0stat();
		holdBcSymbol = pmsl0268RecTO.pmsl0268Record.getSymbol();
		holdBcPolicy0num = pmsl0268RecTO.pmsl0268Record.getPolicy0num();
		holdBcModule = pmsl0268RecTO.pmsl0268Record.getModule();
		holdBcMaster0co = pmsl0268RecTO.pmsl0268Record.getMaster0co();
		holdBcLocation = pmsl0268RecTO.pmsl0268Record.getLocation();
		holdBcEffdt = pmsl0268RecTO.getRec02fmtEffdtAsStr();
		holdBcExpdt = pmsl0268RecTO.getRec02fmtExpdtAsStr();
		holdBcRisk0state = pmsl0268RecTO.pmsl0268Record.getRisk0state();
		holdBcCompany0no = pmsl0268RecTO.pmsl0268Record.getCompany0no();
		holdBcAgency = pmsl0268RecTO.getRec02fmtAgencyAsStr();
		holdBcLine0bus = pmsl0268RecTO.pmsl0268Record.getLine0bus();
		holdBcZip0post = pmsl0268RecTO.pmsl0268Record.getZip0post();
		holdBcAdd0line01 = pmsl0268RecTO.pmsl0268Record.getAdd0line01();
		holdBcAdd0line02 = pmsl0268RecTO.pmsl0268Record.getAdd0line02();
		holdBcAdd0line03 = pmsl0268RecTO.pmsl0268Record.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsl0268RecTO.pmsl0268Record.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsl0268RecTO.pmsl0268Record.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsl0268RecTO.pmsl0268Record.getRenewal0cd();
		holdBcIssue0code = pmsl0268RecTO.pmsl0268Record.getIssue0code();
		holdBcPay0code = pmsl0268RecTO.pmsl0268Record.getPay0code();
		holdBcMode0code = pmsl0268RecTO.pmsl0268Record.getMode0code();
		holdBcSort0name = pmsl0268RecTO.pmsl0268Record.getSort0name();
		holdBcCust0no = pmsl0268RecTO.pmsl0268Record.getCust0no();
		holdBcSpec0use0a = pmsl0268RecTO.pmsl0268Record.getSpec0use0a();
		holdBcSpec0use0b = pmsl0268RecTO.pmsl0268Record.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsl0268RecTO.pmsl0268Record.getCanceldate();
		}
		holdBcMrsseq = pmsl0268RecTO.pmsl0268Record.getMrsseq();
		holdBcTot0ag0prm.assign(pmsl0268RecTO.pmsl0268Record.getTot0ag0prm());
		holdBcType0act = pmsl0268RecTO.pmsl0268Record.getType0act();
		//RSK - START
		
		if(!rskdbio028TO.getInLossdte().equals("") && !rskdbio028TO.getInLossdte().equals("0000000")){
			if(rskdbio028TO.getInLossdte().compareTo(pmsl0268RecTO.getRec02fmtEffdtAsStr()) < 0 || rskdbio028TO.getInLossdte().compareTo(pmsl0268RecTO.getRec02fmtExpdtAsStr()) > 0 ){
				return "";
			}
		}
		
		//RSK - END
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return "";
		}
		return "";
	}

	public void exitPmsl0268() {
	// exit
	}

	public void processPmsjwc04() {
		pmsjwc04RecTO.rcdjwc04Rec.setMasterco(rskdbio028TO.getInMaster0co());
		pmsjwc04RecTO.rcdjwc04Rec.setLocation(rskdbio028TO.getInLocation());
		pmsjwc04RecTO.rcdjwc04Rec.setFein(rskdbio028TO.getInFein());
		pmsjwc04RecTO.rcdjwc04Rec.setSymbol("");
		pmsjwc04RecTO.rcdjwc04Rec.setPolicyno("");
		pmsjwc04RecTO.rcdjwc04Rec.setModule("");
		cobolsqlca.setDBAccessStatus(pmsjwc04DAO.openPmsjwc04TOCsr(KeyType.GREATER_OR_EQ, pmsjwc04RecTO.rcdjwc04Rec.getLocation(),
			pmsjwc04RecTO.rcdjwc04Rec.getMasterco(), pmsjwc04RecTO.rcdjwc04Rec.getFein(), pmsjwc04RecTO.rcdjwc04Rec.getSymbol(),
			pmsjwc04RecTO.rcdjwc04Rec.getPolicyno(), pmsjwc04RecTO.rcdjwc04Rec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		while (!(endFileRead == END_READ_REACHED)) {
			readPmsjwc04();
		}
	}

	public void readPmsjwc04() {
		pmsjwc04TO = pmsjwc04DAO.fetchNext(pmsjwc04TO);
		pmsjwc04RecTO.setData(pmsjwc04TO.getData());
		pmsjwc04RecTO.setData(pmsjwc04RecTO.getData());
		ws1FileStatus = Functions.subString(pmsjwc04TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsjwc04TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return;
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endFileRead = END_READ_REACHED;
			return;
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getCompany0no()) != 0) {
				return;
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsjwc04RecTO.getRcdjwc04AgencyAsStr()) != 0) {
				return;
			}
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0 && pmsjwc04RecTO.rcdjwc04Rec.getCust0no().compareTo(rskdbio028TO.getInCust0no()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getLocation()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0 && rskdbio028TO.getInMaster0co().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getMasterco()) != 0) {
			endFileRead = 'Y';
			return;
		}
		if (rskdbio028TO.getInFein().compareTo("") != 0 && pmsjwc04RecTO.rcdjwc04Rec.getFein().compareTo(rskdbio028TO.getInFein()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return;
			}
			return;
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && pmsjwc04RecTO.rcdjwc04Rec.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0) {
			return;
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& pmsjwc04RecTO.rcdjwc04Rec.getPolicyno().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			return;
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && pmsjwc04RecTO.rcdjwc04Rec.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			return;
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsjwc04RecTO.rcdjwc04Rec.getAdd0line01(), 1, wsNameLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsjwc04RecTO.rcdjwc04Rec.getSort0name(), 1, wsSnLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsjwc04RecTO.rcdjwc04Rec.getAdd0line04(), 1, wsCtLength))) != 0) {
			return;
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsjwc04RecTO.rcdjwc04Rec.getAdd0line04(), 29, 2))) != 0) {
			return;
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsjwc04RecTO.rcdjwc04Rec.getZip0post(), 1, wsZcLength)) != 0) {
			return;
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsjwc04RecTO.getRcdjwc04AgencyAsStr()) != 0) {
			return;
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsjwc04RecTO.rcdjwc04Rec.getLine0bus()) != 0) {
			return;
		}
		if (pmsjwc04RecTO.rcdjwc04Rec.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsjwc04RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
				// continue
			}
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsjwc04RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// * Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsjwc04RecTO.rcdjwc04Rec.getTrans0stat();
		holdBcSymbol = pmsjwc04RecTO.rcdjwc04Rec.getSymbol();
		holdBcPolicy0num = pmsjwc04RecTO.rcdjwc04Rec.getPolicyno();
		holdBcModule = pmsjwc04RecTO.rcdjwc04Rec.getModule();
		holdBcMaster0co = pmsjwc04RecTO.rcdjwc04Rec.getMasterco();
		holdBcLocation = pmsjwc04RecTO.rcdjwc04Rec.getLocation();
		holdBcEffdt = pmsjwc04RecTO.getRcdjwc04EffdtAsStr();
		holdBcExpdt = pmsjwc04RecTO.getRcdjwc04ExpdtAsStr();
		holdBcRisk0state = pmsjwc04RecTO.rcdjwc04Rec.getRisk0state();
		holdBcCompany0no = pmsjwc04RecTO.rcdjwc04Rec.getCompany0no();
		holdBcAgency = pmsjwc04RecTO.getRcdjwc04AgencyAsStr();
		holdBcLine0bus = pmsjwc04RecTO.rcdjwc04Rec.getLine0bus();
		holdBcZip0post = pmsjwc04RecTO.rcdjwc04Rec.getZip0post();
		holdBcAdd0line01 = pmsjwc04RecTO.rcdjwc04Rec.getAdd0line01();
		holdBcAdd0line02 = pmsjwc04RecTO.getRcdjwc04Add0line02AsStr();
		holdBcAdd0line03 = pmsjwc04RecTO.rcdjwc04Rec.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsjwc04RecTO.rcdjwc04Rec.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsjwc04RecTO.rcdjwc04Rec.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsjwc04RecTO.rcdjwc04Rec.getRenewal0cd();
		holdBcIssue0code = pmsjwc04RecTO.rcdjwc04Rec.getIssue0code();
		holdBcPay0code = pmsjwc04RecTO.rcdjwc04Rec.getPay0code();
		holdBcMode0code = pmsjwc04RecTO.rcdjwc04Rec.getMode0code();
		holdBcSort0name = pmsjwc04RecTO.rcdjwc04Rec.getSort0name();
		holdBcCust0no = pmsjwc04RecTO.rcdjwc04Rec.getCust0no();
		holdBcSpec0use0a = pmsjwc04RecTO.rcdjwc04Rec.getSpec0use0a();
		holdBcSpec0use0b = pmsjwc04RecTO.rcdjwc04Rec.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsjwc04RecTO.rcdjwc04Rec.getCanceldate();
		}
		holdBcMrsseq = pmsjwc04RecTO.rcdjwc04Rec.getMrsseq();
		holdBcTot0ag0prm.assign(pmsjwc04RecTO.rcdjwc04Rec.getTot0ag0prm());
		holdBcType0act = pmsjwc04RecTO.rcdjwc04Rec.getType0act();
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return;
		}
	}

	public void processPmsj1200() {
		endFileRead = ' ';
		pmsj1200RecTO.rcdj1200Rec.setMaster0co(rskdbio028TO.getInMaster0co());
		pmsj1200RecTO.rcdj1200Rec.setLocation(rskdbio028TO.getInLocation());
		pmsj1200RecTO.rcdj1200Rec.setSsn(rskdbio028TO.getInFein());
		pmsj1200RecTO.rcdj1200Rec.setSymbol("");
		pmsj1200RecTO.rcdj1200Rec.setPolicy0num("");
		pmsj1200RecTO.rcdj1200Rec.setModule("");
		cobolsqlca.setDBAccessStatus(pmsj1200DAO.openPmsj1200TOCsr(KeyType.GREATER_OR_EQ, pmsj1200RecTO.rcdj1200Rec.getLocation(),
			pmsj1200RecTO.rcdj1200Rec.getMaster0co(), pmsj1200RecTO.rcdj1200Rec.getSsn(), pmsj1200RecTO.rcdj1200Rec.getSymbol(),
			pmsj1200RecTO.rcdj1200Rec.getPolicy0num(), pmsj1200RecTO.rcdj1200Rec.getModule()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			return;
		}
		while (!(endFileRead == END_READ_REACHED)) {
			rng2000ReadPmsj1200();
		}
	}

	public String readPmsj1200() {
		pmsj1200TO = pmsj1200DAO.fetchNext(pmsj1200TO);
		pmsj1200RecTO.setData(pmsj1200TO.getData());
		pmsj1200RecTO.setData(pmsj1200RecTO.getData());
		ws1FileStatus = Functions.subString(pmsj1200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsj1200TO.getDBAccessStatus().isEOF()) {
			endFileRead = 'Y';
			return "";
		}
		if (!(ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0)) {
			endFileRead = END_READ_REACHED;
			return "";
		}
		if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getCompany0no().compareTo(pmsj1200RecTO.rcdj1200Rec.getCompany0no()) != 0) {
				return "";
			}
		}
		if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo("") != 0) {
			if (pmspmm80RecTO.pmm80Rec.getAgnmnbr().compareTo(pmsj1200RecTO.getRcdj1200AgencyAsStr()) != 0) {
				return "";
			}
		}
		if (rskdbio028TO.getInCust0no().compareTo("") != 0 && pmsj1200RecTO.rcdj1200Rec.getCust0no().compareTo(rskdbio028TO.getInCust0no()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInLocation().compareTo("") != 0 && rskdbio028TO.getInLocation().compareTo(pmsj1200RecTO.rcdj1200Rec.getLocation()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInMaster0co().compareTo("") != 0
			&& rskdbio028TO.getInMaster0co().compareTo(pmsj1200RecTO.rcdj1200Rec.getMaster0co()) != 0) {
			endFileRead = 'Y';
			return "";
		}
		if (rskdbio028TO.getInFein().compareTo("") != 0 && pmsj1200RecTO.rcdj1200Rec.getSsn().compareTo(rskdbio028TO.getInFein()) != 0) {
			if (rskdbio028TO.getInMaster0co().compareTo("") != 0) {
				endFileRead = 'Y';
				return "";
			}
			return "";
		}
		if (rskdbio028TO.getInSymbol().compareTo("") != 0 && pmsj1200RecTO.rcdj1200Rec.getSymbol().compareTo(rskdbio028TO.getInSymbol()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInPolicy0num().compareTo("") != 0
			&& pmsj1200RecTO.rcdj1200Rec.getPolicy0num().compareTo(rskdbio028TO.getInPolicy0num()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInModule().compareTo("") != 0 && pmsj1200RecTO.rcdj1200Rec.getModule().compareTo(rskdbio028TO.getInModule()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInName().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInName(), 1, wsNameLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1200RecTO.rcdj1200Rec.getAdd0line01(), 1, wsNameLength))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInSort0name().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInSort0name(), 1, wsSnLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1200RecTO.rcdj1200Rec.getSort0name(), 1, wsSnLength))) != 0) {
			return "2000-READ-PMSJ1200";
		}
		if (rskdbio028TO.getInAdd0line04().compareTo("") != 0
			&& ABOSystem.Upper(Functions.subString(rskdbio028TO.getInAdd0line04(), 1, wsCtLength)).compareTo(
				ABOSystem.Upper(Functions.subString(pmsj1200RecTO.rcdj1200Rec.getAdd0line04(), 1, wsCtLength))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInState().compareTo("") != 0
			&& rskdbio028TO.getInState().compareTo(ABOSystem.Upper(Functions.subString(pmsj1200RecTO.rcdj1200Rec.getAdd0line04(), 29, 2))) != 0) {
			return "";
		}
		if (rskdbio028TO.getInZip0post().compareTo("") != 0
			&& Functions.subString(rskdbio028TO.getInZip0post(), 1, wsZcLength).compareTo(
				Functions.subString(pmsj1200RecTO.rcdj1200Rec.getZip0post(), 1, wsZcLength)) != 0) {
			return "";
		}
		if (rskdbio028TO.getInAgency().compareTo("") != 0 && rskdbio028TO.getInAgency().compareTo(pmsj1200RecTO.getRcdj1200AgencyAsStr()) != 0) {
			return "";
		}
		if (rskdbio028TO.getInLine0bus().compareTo("") != 0 && rskdbio028TO.getInLine0bus().compareTo(pmsj1200RecTO.rcdj1200Rec.getLine0bus()) != 0) {
			return "";
		}
		if (pmsj1200RecTO.rcdj1200Rec.getTrans0stat() == 'V') {
			pmsp0200RecTO3.setData(pmsj1200RecTO.getData());
			pmsp0200RecTO3.rec02fmtRec.setTrans0stat('P');
			pmsp0200TO =
				Pmsp0200DAO.selectById(pmsp0200TO, pmsp0200RecTO3.rec02fmtRec.getId02(), pmsp0200RecTO3.rec02fmtRec.getLocation(),
					pmsp0200RecTO3.rec02fmtRec.getPolicy0num(), pmsp0200RecTO3.rec02fmtRec.getSymbol(), pmsp0200RecTO3.rec02fmtRec.getMaster0co(),
					pmsp0200RecTO3.rec02fmtRec.getModule(), pmsp0200RecTO3.rec02fmtRec.getTrans0stat());
			if (pmsp0200TO.getDBAccessStatus().isSuccess()) {
				pmsp0200RecTO3.setData(pmsp0200TO.getData());
				pmsp0200RecTO3.setData(pmsp0200RecTO3.getData());
			}
			ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
			if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
				// continue
			}
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				pmsj1200RecTO.setData(pmsp0200RecTO3.getData());
			}
		}
		// * Moving the data to the temporary variables start here.
		holdBcTrans0stat = pmsj1200RecTO.rcdj1200Rec.getTrans0stat();
		holdBcSymbol = pmsj1200RecTO.rcdj1200Rec.getSymbol();
		holdBcPolicy0num = pmsj1200RecTO.rcdj1200Rec.getPolicy0num();
		holdBcModule = pmsj1200RecTO.rcdj1200Rec.getModule();
		holdBcMaster0co = pmsj1200RecTO.rcdj1200Rec.getMaster0co();
		holdBcLocation = pmsj1200RecTO.rcdj1200Rec.getLocation();
		holdBcEffdt = pmsj1200RecTO.getRcdj1200EffdtAsStr();
		holdBcExpdt = pmsj1200RecTO.getRcdj1200ExpdtAsStr();
		holdBcRisk0state = pmsj1200RecTO.rcdj1200Rec.getRisk0state();
		holdBcCompany0no = pmsj1200RecTO.rcdj1200Rec.getCompany0no();
		holdBcAgency = pmsj1200RecTO.getRcdj1200AgencyAsStr();
		holdBcLine0bus = pmsj1200RecTO.rcdj1200Rec.getLine0bus();
		holdBcZip0post = pmsj1200RecTO.rcdj1200Rec.getZip0post();
		holdBcAdd0line01 = pmsj1200RecTO.rcdj1200Rec.getAdd0line01();
		holdBcAdd0line02 = pmsj1200RecTO.getRcdj1200Add0line02AsStr();
		holdBcAdd0line03 = pmsj1200RecTO.rcdj1200Rec.getAdd0line03();
		holdBcAdd0line04 = Functions.subString(pmsj1200RecTO.rcdj1200Rec.getAdd0line04(), 1, 28);
		holdBcState = Functions.subString(pmsj1200RecTO.rcdj1200Rec.getAdd0line04(), 29, 2);
		holdBcRenewal0cd = pmsj1200RecTO.rcdj1200Rec.getRenewal0cd();
		holdBcIssue0code = pmsj1200RecTO.rcdj1200Rec.getIssue0code();
		holdBcPay0code = pmsj1200RecTO.rcdj1200Rec.getPay0code();
		holdBcMode0code = pmsj1200RecTO.rcdj1200Rec.getMode0code();
		holdBcSort0name = pmsj1200RecTO.rcdj1200Rec.getSort0name();
		holdBcCust0no = pmsj1200RecTO.rcdj1200Rec.getCust0no();
		holdBcSpec0use0a = pmsj1200RecTO.rcdj1200Rec.getSpec0use0a();
		holdBcSpec0use0b = pmsj1200RecTO.rcdj1200Rec.getSpec0use0b();
		if (holdBcRenewal0cd != '9') {
			holdBcCanceldate = "";
		} else {
			holdBcCanceldate = pmsj1200RecTO.rcdj1200Rec.getCanceldate();
		}
		holdBcMrsseq = pmsj1200RecTO.rcdj1200Rec.getMrsseq();
		holdBcTot0ag0prm.assign(pmsj1200RecTO.rcdj1200Rec.getTot0ag0prm());
		holdBcType0act = pmsj1200RecTO.rcdj1200Rec.getType0act();
		processEntry();
		conditionCheck();
		if (numOfEntrys == rskdbio028TO.getInMaxrecords()) {
			endFileRead = 'Y';
			return "";
		}
		return "";
	}

	public void exitPmsj1200() {
	// exit
	}

	public void exit1() {
	// exit
	}

	public void processEntry() {
		//
		// Move the data to the table
		//
		numOfEntrys = (1 + numOfEntrys) % 100000;
		wsCount = numOfEntrys;
		//
		bcListEntryArr13[(wsCount) - 1].setBcTrans0stat(holdBcTrans0stat);
		bcListEntryArr13[(wsCount) - 1].setBcSymbol(holdBcSymbol);
		bcListEntryArr13[(wsCount) - 1].setBcPolicy0num(holdBcPolicy0num);
		bcListEntryArr13[(wsCount) - 1].setBcModule(holdBcModule);
		bcListEntryArr13[(wsCount) - 1].setBcMaster0co(holdBcMaster0co);
		bcListEntryArr13[(wsCount) - 1].setBcLocation(holdBcLocation);
		bcListEntryArr13[(wsCount) - 1].setBcEffdt(holdBcEffdt);
		bcListEntryArr13[(wsCount) - 1].setBcExpdt(holdBcExpdt);
		bcListEntryArr13[(wsCount) - 1].setBcRisk0state(holdBcRisk0state);
		bcListEntryArr13[(wsCount) - 1].setBcCompany0no(holdBcCompany0no);
		bcListEntryArr13[(wsCount) - 1].setBcAgency(holdBcAgency);
		bcListEntryArr13[(wsCount) - 1].setBcLine0bus(holdBcLine0bus);
		bcListEntryArr13[(wsCount) - 1].setBcZip0post(holdBcZip0post);
		bcListEntryArr13[(wsCount) - 1].setBcAdd0line01(holdBcAdd0line01);
		bcListEntryArr13[(wsCount) - 1].setBcAdd0line02(holdBcAdd0line02);
		bcListEntryArr13[(wsCount) - 1].setBcAdd0line03(holdBcAdd0line03);
		bcListEntryArr13[(wsCount) - 1].setBcAdd0line04(holdBcAdd0line04);
		bcListEntryArr13[(wsCount) - 1].setBcState(holdBcState);
		bcListEntryArr13[(wsCount) - 1].setBcRenewal0cd(holdBcRenewal0cd);
		bcListEntryArr13[(wsCount) - 1].setBcIssue0code(holdBcIssue0code);
		bcListEntryArr13[(wsCount) - 1].setBcPay0code(holdBcPay0code);
		bcListEntryArr13[(wsCount) - 1].setBcMode0code(holdBcMode0code);
		bcListEntryArr13[(wsCount) - 1].setBcSort0name(holdBcSort0name);
		bcListEntryArr13[(wsCount) - 1].setBcCust0no(holdBcCust0no);
		bcListEntryArr13[(wsCount) - 1].setBcSpec0use0a(holdBcSpec0use0a);
		bcListEntryArr13[(wsCount) - 1].setBcSpec0use0b(holdBcSpec0use0b);
		bcListEntryArr13[(wsCount) - 1].setBcCanceldate(holdBcCanceldate);
		bcListEntryArr13[(wsCount) - 1].setBcMrsseq(holdBcMrsseq);
		bcListEntryArr13[(wsCount) - 1].setBcTot0ag0prm(holdBcTot0ag0prm.clone());
		if (bcListEntryArr13[(wsCount) - 1].getBcRenewal0cd() == '0') {
			bcListEntryArr13[(wsCount) - 1].setBcRenewal0cdDesc("Alright For Agent To Renew");
		} else {
			if (bcListEntryArr13[(wsCount) - 1].getBcRenewal0cd() == '1') {
				bcListEntryArr13[(wsCount) - 1].setBcRenewal0cdDesc("Computer Renew");
			} else {
				if (bcListEntryArr13[(wsCount) - 1].getBcRenewal0cd() == '2') {
					bcListEntryArr13[(wsCount) - 1].setBcRenewal0cdDesc("Alright For Home Office To renew");
				} else {
					if (bcListEntryArr13[(wsCount) - 1].getBcRenewal0cd() == '3') {
						bcListEntryArr13[(wsCount) - 1].setBcRenewal0cdDesc("Already Renewed");
					} else {
						if (bcListEntryArr13[(wsCount) - 1].getBcRenewal0cd() == '4') {
							bcListEntryArr13[(wsCount) - 1].setBcRenewal0cdDesc("Agent Issued First Policy, Computer To Renew");
						} else {
							if (bcListEntryArr13[(wsCount) - 1].getBcRenewal0cd() == '7') {
								bcListEntryArr13[(wsCount) - 1].setBcRenewal0cdDesc("Non-Renewal With A Notice");
							} else {
								if (bcListEntryArr13[(wsCount) - 1].getBcRenewal0cd() == '8') {
									bcListEntryArr13[(wsCount) - 1].setBcRenewal0cdDesc("Non-Renewal Without A Notice");
								} else {
									if (bcListEntryArr13[(wsCount) - 1].getBcRenewal0cd() == '9') {
										bcListEntryArr13[(wsCount) - 1].setBcRenewal0cdDesc("Cancelled Policy");
									} else {
										bcListEntryArr13[(wsCount) - 1].setBcRenewal0cdDesc("");
									}
								}
							}
						}
					}
				}
			}
		}
		//
		// READ PMSL0004 *
		// --------------*
		rec00fmtRec3.setSymbol(holdBcSymbol);
		rec00fmtRec3.setPolicy0num(holdBcPolicy0num);
		rec00fmtRec3.setModule(holdBcModule);
		rec00fmtRec3.setMaster0co(holdBcMaster0co);
		rec00fmtRec3.setLocation(holdBcLocation);
		wsControlSw = 'Y';
		try {
			call1();
		} catch (DynamicCallException ex) {
			if (ex.getPgmName().compareTo("BASL000401") != 0) {
				throw ex;
			} else {
				wsReturnCode = "FS00";
			}
		}
		//
		// If no record was returned then the policy is verified so
		// set the status on the activity record to a 'V'
		if (!(wsReturnCode.compareTo(GOOD_RETURN_FROM_IO) == 0)) {
			// MOVE 'V' TO TRANS0STAT OF REC00FMT
			rec00fmtRec3.setTrans0stat(holdBcTrans0stat);
			rec00fmtRec3.setType0act(holdBcType0act);
		}
		//
		bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("Verified");
		//
		//
		if (rec00fmtRec3.getType0act().compareTo("NB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("NB Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("NB Pending");
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("EN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("EN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("EN Pending");
				}
			}
			return;
		}
		//
		bcListEntryArr13[(wsCount) - 1].setBcTrans0stat(rec00fmtRec3.getTrans0stat());
		if (bcListEntryArr13[(wsCount) - 1].getBcRenewal0cd() == '9' && bcListEntryArr13[(wsCount) - 1].getBcCanceldate().compareTo("") != 0) {
			if (bcListEntryArr13[(wsCount) - 1].getBcTrans0stat() == 'V') {
				bcListEntryArr13[(wsCount) - 1].setBcTrans0stat('C');
				bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("Cancelled");
				return;
			}
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("CN") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr13[(wsCount) - 1].setBcTrans0stat('1');
				bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("CN Error");
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("CN Pending");
					bcListEntryArr13[(wsCount) - 1].setBcTrans0stat('X');
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("RB") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("RN Error");
				bcListEntryArr13[(wsCount) - 1].setBcTrans0stat('2');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("RN Pending");
					bcListEntryArr13[(wsCount) - 1].setBcTrans0stat('W');
				}
			}
			return;
		}
		//
		if (rec00fmtRec3.getType0act().compareTo("RI") == 0) {
			if (rec00fmtRec3.getTrans0stat() == 'E') {
				bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("RI Error");
				bcListEntryArr13[(wsCount) - 1].setBcTrans0stat('3');
			} else {
				if (rec00fmtRec3.getTrans0stat() != 'V') {
					bcListEntryArr13[(wsCount) - 1].setBcStatusDesc("RI Pending");
					bcListEntryArr13[(wsCount) - 1].setBcTrans0stat('I');
				}
			}
			return;
		}
	}

	public void conditionCheck() {
		// B0782 END-IF
		if (numOfEntrys > 1) {
			holdPreviousType = Functions.subString(bcListEntryArr13[(wsCount) - 1].getBcStatusDesc(), 1, 10);
			wsCounter = ((short) 1);
			// B0782 IF HOLD-PREVIOUS-TYPE = 'CN Pending'
			holdPreviousType = "";
			wsCounter = ((short) 1);
			while (!(wsCounter >= numOfEntrys)) {
				checkPending();
				wsCounter = ((short) (wsCounter + 1));
			}
		}
	}

	public void checkPending() {
		if (bcListEntryArr13[(wsCounter) - 1].getBcSymbol().compareTo(bcListEntryArr13[(numOfEntrys) - 1].getBcSymbol()) == 0
			&& bcListEntryArr13[(wsCounter) - 1].getBcPolicy0num().compareTo(bcListEntryArr13[(numOfEntrys) - 1].getBcPolicy0num()) == 0
			&& bcListEntryArr13[(wsCounter) - 1].getBcModule().compareTo(bcListEntryArr13[(numOfEntrys) - 1].getBcModule()) == 0
			&& bcListEntryArr13[(wsCounter) - 1].getBcMaster0co().compareTo(bcListEntryArr13[(numOfEntrys) - 1].getBcMaster0co()) == 0
			&& bcListEntryArr13[(wsCounter) - 1].getBcLocation().compareTo(bcListEntryArr13[(numOfEntrys) - 1].getBcLocation()) == 0) {
			wsCount = (wsCount - 1) % 100000;
			numOfEntrys = (numOfEntrys - 1) % 100000;
		}
	}

	public void rng2000ReadPmsl0248() {
		String retcode = "";
		boolean goto2000ReadPmsl0248 = false;
		do {
			goto2000ReadPmsl0248 = false;
			retcode = readPmsl0248();
		} while (retcode.compareTo("2000-READ-PMSL0248") == 0);
		exitPmsl0248();
	}

	public void rng2000ReadPmsl0247() {
		String retcode = "";
		boolean goto2000ReadPmsl0247 = false;
		do {
			goto2000ReadPmsl0247 = false;
			retcode = readPmsl0247();
		} while (retcode.compareTo("2000-READ-PMSL0247") == 0);
		exitPmsl0247();
	}

	public void rng2000ReadPmsl0249() {
		String retcode = "";
		boolean goto2000ReadPmsl0249 = false;
		do {
			goto2000ReadPmsl0249 = false;
			retcode = readPmsl0249();
		} while (retcode.compareTo("2000-READ-PMSL0249") == 0);
		exitPmsl0249();
	}

	public void rng2000ReadPmsl0253() {
		String retcode = "";
		boolean goto2000ReadPmsl0253 = false;
		do {
			goto2000ReadPmsl0253 = false;
			retcode = readPmsl0253();
		} while (retcode.compareTo("2000-READ-PMSL0253") == 0);
		exitPmsl0253();
	}

	public void rng2000ReadPmsl0254() {
		boolean goto2000ReadPmsl0254 = false;
		String retcode = "";
		do {
			goto2000ReadPmsl0254 = false;
			retcode = readPmsl0254();
		} while (retcode.compareTo("2000-READ-PMSL0254") == 0);
		exitPmsl0254();
	}

	public void rng2000ReadPmsl0268() {
		boolean goto2000ReadPmsl0268 = false;
		String retcode = "";
		do {
			goto2000ReadPmsl0268 = false;
			retcode = readPmsl0268();
		} while (retcode.compareTo("2000-READ-PMSL0268") == 0);
		exitPmsl0268();
	}

	public void rng2000ReadPmsj1200() {
		String retcode = "";
		boolean goto2000ReadPmsj1200 = false;
		do {
			goto2000ReadPmsj1200 = false;
			retcode = readPmsj1200();
		} while (retcode.compareTo("2000-READ-PMSJ1200") == 0);
		exitPmsj1200();
	}

	public SPResultSetTO runSP(Rskdbio028TO rskdbio028TO) {
		this.rskdbio028TO = rskdbio028TO;
		run();
		return resultSetTO;
	}

	public SPResultSetTO runSP(byte[] dynamic_inputParameters) {
		rskdbio028TO.setInputParameters(dynamic_inputParameters);
		return runSP(rskdbio028TO);
	}

	public Rskdbio028TO run(Rskdbio028TO rskdbio028TO) {
		this.rskdbio028TO = rskdbio028TO;
		run();
		return this.rskdbio028TO;
	}

	public void run(byte[] dynamic_inputParameters) {
		if (dynamic_inputParameters != null) {
			rskdbio028TO.setInputParameters(dynamic_inputParameters);
		}
		run(rskdbio028TO);
		if (dynamic_inputParameters != null) {
			DataConverter.arrayCopy(rskdbio028TO.getInputParameters(), rskdbio028TO.getInputParametersSize(), dynamic_inputParameters, 1);
		}
	}

	public void call1() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[rec00fmtRec3.getRec00fmtSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
		DataConverter.arrayCopy(rec00fmtRec3.getRec00fmt(), rec00fmtRec3.getRec00fmtSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, wsControlSw);
		DynamicCall.invoke("BASL000401", new Object[] { ((Object) arr_p0), ((Object) arr_p1), ((Object) arr_p2) });
		wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
		rec00fmtRec3.setRec00fmt(arr_p1);
		wsControlSw = DataConverter.readChar(arr_p2, 1);
	}

	public static Rskdbio028 getInstance() {
		Rskdbio028 iRskdbio028 = ((Rskdbio028) CalledProgList.getInst("RSKDBIO028"));
		if (iRskdbio028 == null) {
			// create an instance if needed
			iRskdbio028 = new Rskdbio028();
			CalledProgList.addInst("RSKDBIO028", ((Object) iRskdbio028));
		}
		return iRskdbio028;
	}

	public void run() {
		try {
			mainSubroutine();
		} catch (ReturnException re) {
			// normal termination of the program
		}
		//BPHX changes start - fix 158236
		finally {
			pmsl0250DAO.closeCsr();
		}
		//BPHX changes end - fix 158236
	}

	public static void main(String[] args) {
		getInstance().run();
	}

	public int getBcListTableSize() {
		return 32320;
	}

	public byte[] getBcListTable() {
		byte[] buf = new byte[getBcListTableSize()];
		int offset = 1;
		for (int idx = 1; idx <= bcListEntryArr13.length; idx++) {
			DataConverter.arrayCopy(bcListEntryArr13[(idx) - 1].getData(), 1, bcListEntryArr13[(idx) - 1].getSize(), buf, offset);
			offset += bcListEntryArr13[(idx) - 1].getSize();
		}
		return buf;
	}

	public void initBcListTableSpaces() {
		for (int idx = 1; idx <= bcListEntryArr13.length; idx++) {
			bcListEntryArr13[(idx) - 1].initSpaces();
		}
	}
}