// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.util.DBAccessStatus;
import bphx.sdf.sql.util.ABOSqlDB2Conversion;

import com.csc.pt.svc.data.to.Bashlgb500TO;

public class Bashlgb500DAO {
	public static DBAccessStatus insertRec(Bashlgb500TO bashlgb500TO) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASHLGB500");
			Query query = session.getNamedQuery("Bashlgb500DAO_insertRec");
			query.setString("bashlgb500TO_loguser", bashlgb500TO.getLoguser());
			query.setDate("bashlgb500TO_logdate", ABOSqlDB2Conversion.dateToSQL(bashlgb500TO.getLogdate()));
			query.setTime("bashlgb500TO_logtime", ABOSqlDB2Conversion.timeToSQL(bashlgb500TO.getLogtime()));
			query.setInteger("bashlgb500TO_logseqnum", bashlgb500TO.getLogseqnum());
			query.setString("bashlgb500TO_location", bashlgb500TO.getLocation());
			query.setString("bashlgb500TO_masterco", bashlgb500TO.getMasterco());
			query.setString("bashlgb500TO_symbol", bashlgb500TO.getSymbol());
			query.setString("bashlgb500TO_policyno", bashlgb500TO.getPolicyno());
			query.setString("bashlgb500TO_module", bashlgb500TO.getModule());
			query.setString("bashlgb500TO_insline", bashlgb500TO.getInsline());
			query.setInteger("bashlgb500TO_riskloc", bashlgb500TO.getRiskloc());
			query.setInteger("bashlgb500TO_risksubloc", bashlgb500TO.getRisksubloc());
			query.setString("bashlgb500TO_product", bashlgb500TO.getProduct());
			query.setInteger("bashlgb500TO_unitno", bashlgb500TO.getUnitno());
			query.setCharacter("bashlgb500TO_recstatus", bashlgb500TO.getRecstatus());
			query.setCharacter("bashlgb500TO_b5c7st", bashlgb500TO.getB5c7st());
			query.setInteger("bashlgb500TO_b5andt", bashlgb500TO.getB5andt());
			query.setShort("bashlgb500TO_b5afnb", bashlgb500TO.getB5afnb());
			query.setInteger("bashlgb500TO_b5aldt", bashlgb500TO.getB5aldt());
			query.setInteger("bashlgb500TO_b5afdt", bashlgb500TO.getB5afdt());
			query.setString("bashlgb500TO_b5type0act", bashlgb500TO.getB5type0act());
			query.setString("bashlgb500TO_b5batx", bashlgb500TO.getB5batx());
			query.setCharacter("bashlgb500TO_b5dzst", bashlgb500TO.getB5dzst());
			query.setCharacter("bashlgb500TO_b5d0st", bashlgb500TO.getB5d0st());
			query.setCharacter("bashlgb500TO_b5d2st", bashlgb500TO.getB5d2st());
			query.setCharacter("bashlgb500TO_b5d3st", bashlgb500TO.getB5d3st());
			query.setBigDecimal("bashlgb500TO_b5a3va", bashlgb500TO.getB5a3va());
			query.setBigDecimal("bashlgb500TO_b5brva", bashlgb500TO.getB5brva());
			query.setBigDecimal("bashlgb500TO_b5bsva", bashlgb500TO.getB5bsva());
			query.setBigDecimal("bashlgb500TO_b5btva", bashlgb500TO.getB5btva());
			query.setBigDecimal("bashlgb500TO_b5aupc", bashlgb500TO.getB5aupc());
			query.setString("bashlgb500TO_b5aqcd", bashlgb500TO.getB5aqcd());
			query.setString("bashlgb500TO_b5arcd", bashlgb500TO.getB5arcd());
			query.setString("bashlgb500TO_b5ascd", bashlgb500TO.getB5ascd());
			query.setString("bashlgb500TO_b5eetx", bashlgb500TO.getB5eetx());
			query.setString("bashlgb500TO_b5pttx", bashlgb500TO.getB5pttx());
			query.setString("bashlgb500TO_b5kktx", bashlgb500TO.getB5kktx());
			query.setString("bashlgb500TO_b5kwtx", bashlgb500TO.getB5kwtx());
			query.setString("bashlgb500TO_b5bccd", bashlgb500TO.getB5bccd());
			query.setString("bashlgb500TO_b5agnb", bashlgb500TO.getB5agnb());
			query.setString("bashlgb500TO_b5bjcd", bashlgb500TO.getB5bjcd());
			query.setString("bashlgb500TO_b5bkcd", bashlgb500TO.getB5bkcd());
			query.setCharacter("bashlgb500TO_b5latx", bashlgb500TO.getB5latx());
			query.setCharacter("bashlgb500TO_b5lbtx", bashlgb500TO.getB5lbtx());
			query.setString("bashlgb500TO_b5h1tx", bashlgb500TO.getB5h1tx());
			query.setCharacter("bashlgb500TO_b5hytx", bashlgb500TO.getB5hytx());
			query.setBigDecimal("bashlgb500TO_b5a9nb", bashlgb500TO.getB5a9nb());
			query.setCharacter("bashlgb500TO_b5o6tx", bashlgb500TO.getB5o6tx());
			query.setString("bashlgb500TO_b5pqtx", bashlgb500TO.getB5pqtx());
			query.setString("bashlgb500TO_b5prtx", bashlgb500TO.getB5prtx());
			query.setInteger("bashlgb500TO_b5fjnb", bashlgb500TO.getB5fjnb());
			query.setInteger("bashlgb500TO_b5fknb", bashlgb500TO.getB5fknb());
			query.setInteger("bashlgb500TO_b5flnb", bashlgb500TO.getB5flnb());
			query.setBigDecimal("bashlgb500TO_b5a0va", bashlgb500TO.getB5a0va());
			query.setBigDecimal("bashlgb500TO_b5b7va", bashlgb500TO.getB5b7va());
			query.setBigDecimal("bashlgb500TO_b5azva", bashlgb500TO.getB5azva());
			query.setCharacter("bashlgb500TO_b5czst", bashlgb500TO.getB5czst());
			query.setCharacter("bashlgb500TO_b5c0st", bashlgb500TO.getB5c0st());
			query.setCharacter("bashlgb500TO_b5c1st", bashlgb500TO.getB5c1st());
			query.setCharacter("bashlgb500TO_b5c2st", bashlgb500TO.getB5c2st());
			query.setCharacter("bashlgb500TO_b5iytx", bashlgb500TO.getB5iytx());
			query.setCharacter("bashlgb500TO_b5iztx", bashlgb500TO.getB5iztx());
			query.setCharacter("bashlgb500TO_b5i0tx", bashlgb500TO.getB5i0tx());
			query.setCharacter("bashlgb500TO_b5i1tx", bashlgb500TO.getB5i1tx());
			query.setCharacter("bashlgb500TO_b5i2tx", bashlgb500TO.getB5i2tx());
			query.setCharacter("bashlgb500TO_b5i3tx", bashlgb500TO.getB5i3tx());
			query.setCharacter("bashlgb500TO_b5i4tx", bashlgb500TO.getB5i4tx());
			query.setCharacter("bashlgb500TO_b5i5tx", bashlgb500TO.getB5i5tx());
			query.setCharacter("bashlgb500TO_b5usin13", bashlgb500TO.getB5usin13());
			query.setCharacter("bashlgb500TO_b5usin14", bashlgb500TO.getB5usin14());
			query.setCharacter("bashlgb500TO_b5usin15", bashlgb500TO.getB5usin15());
			query.setCharacter("bashlgb500TO_b5usin16", bashlgb500TO.getB5usin16());
			query.setCharacter("bashlgb500TO_b5usin17", bashlgb500TO.getB5usin17());
			query.setCharacter("bashlgb500TO_b5usin18", bashlgb500TO.getB5usin18());
			query.setCharacter("bashlgb500TO_b5usin19", bashlgb500TO.getB5usin19());
			query.setCharacter("bashlgb500TO_b5usin20", bashlgb500TO.getB5usin20());
			query.setCharacter("bashlgb500TO_b5usin21", bashlgb500TO.getB5usin21());
			query.setCharacter("bashlgb500TO_b5usin22", bashlgb500TO.getB5usin22());
			query.setCharacter("bashlgb500TO_b5usin23", bashlgb500TO.getB5usin23());
			query.setCharacter("bashlgb500TO_b5usin24", bashlgb500TO.getB5usin24());
			query.setCharacter("bashlgb500TO_b5usin25", bashlgb500TO.getB5usin25());
			query.setCharacter("bashlgb500TO_b5usin26", bashlgb500TO.getB5usin26());
			query.setCharacter("bashlgb500TO_b5usin27", bashlgb500TO.getB5usin27());
			query.setCharacter("bashlgb500TO_b5usin28", bashlgb500TO.getB5usin28());
			query.setCharacter("bashlgb500TO_b5usin29", bashlgb500TO.getB5usin29());
			query.setCharacter("bashlgb500TO_b5usin30", bashlgb500TO.getB5usin30());
			query.setString("bashlgb500TO_b5akcd", bashlgb500TO.getB5akcd());
			query.setString("bashlgb500TO_b5alcd", bashlgb500TO.getB5alcd());
			query.setString("bashlgb500TO_b5amcd", bashlgb500TO.getB5amcd());
			query.setString("bashlgb500TO_b5ancd", bashlgb500TO.getB5ancd());
			query.setString("bashlgb500TO_b5uscd5", bashlgb500TO.getB5uscd5());
			query.setString("bashlgb500TO_b5pltx", bashlgb500TO.getB5pltx());
			query.setString("bashlgb500TO_b5pmtx", bashlgb500TO.getB5pmtx());
			query.setString("bashlgb500TO_b5pntx", bashlgb500TO.getB5pntx());
			query.setString("bashlgb500TO_b5potx", bashlgb500TO.getB5potx());
			query.setString("bashlgb500TO_b5pptx", bashlgb500TO.getB5pptx());
			query.setBigDecimal("bashlgb500TO_b5aepc", bashlgb500TO.getB5aepc());
			query.setBigDecimal("bashlgb500TO_b5afpc", bashlgb500TO.getB5afpc());
			query.setBigDecimal("bashlgb500TO_b5usft3", bashlgb500TO.getB5usft3());
			query.setBigDecimal("bashlgb500TO_b5usft4", bashlgb500TO.getB5usft4());
			query.setBigDecimal("bashlgb500TO_b5usft5", bashlgb500TO.getB5usft5());
			query.setBigDecimal("bashlgb500TO_b5usft6", bashlgb500TO.getB5usft6());
			query.setBigDecimal("bashlgb500TO_b5usft7", bashlgb500TO.getB5usft7());
			query.setBigDecimal("bashlgb500TO_b5usft8", bashlgb500TO.getB5usft8());
			query.setBigDecimal("bashlgb500TO_b5usft9", bashlgb500TO.getB5usft9());
			query.setBigDecimal("bashlgb500TO_b5usft10", bashlgb500TO.getB5usft10());
			query.setBigDecimal("bashlgb500TO_b5usft11", bashlgb500TO.getB5usft11());
			query.setBigDecimal("bashlgb500TO_b5usft12", bashlgb500TO.getB5usft12());
			query.setBigDecimal("bashlgb500TO_b5bypc", bashlgb500TO.getB5bypc());
			query.setBigDecimal("bashlgb500TO_b5bzpc", bashlgb500TO.getB5bzpc());
			query.setBigDecimal("bashlgb500TO_b5b0pc", bashlgb500TO.getB5b0pc());
			query.setBigDecimal("bashlgb500TO_b5b1pc", bashlgb500TO.getB5b1pc());
			query.setBigDecimal("bashlgb500TO_b5b2pc", bashlgb500TO.getB5b2pc());
			query.setBigDecimal("bashlgb500TO_b5b3pc", bashlgb500TO.getB5b3pc());
			query.setBigDecimal("bashlgb500TO_b5b4pc", bashlgb500TO.getB5b4pc());
			query.setBigDecimal("bashlgb500TO_b5b5pc", bashlgb500TO.getB5b5pc());
			query.setBigDecimal("bashlgb500TO_b5agva", bashlgb500TO.getB5agva());
			query.setBigDecimal("bashlgb500TO_b5ahva", bashlgb500TO.getB5ahva());
			query.setBigDecimal("bashlgb500TO_b5usva3", bashlgb500TO.getB5usva3());
			query.setBigDecimal("bashlgb500TO_b5usva4", bashlgb500TO.getB5usva4());
			query.setBigDecimal("bashlgb500TO_b5usva5", bashlgb500TO.getB5usva5());
			query.setInteger("bashlgb500TO_b5bknb", bashlgb500TO.getB5bknb());
			query.setInteger("bashlgb500TO_b5blnb", bashlgb500TO.getB5blnb());
			query.setInteger("bashlgb500TO_b5usnb3", bashlgb500TO.getB5usnb3());
			query.setInteger("bashlgb500TO_b5usnb4", bashlgb500TO.getB5usnb4());
			query.setInteger("bashlgb500TO_b5usnb5", bashlgb500TO.getB5usnb5());
			query.setInteger("bashlgb500TO_b5a5dt", bashlgb500TO.getB5a5dt());
			query.setInteger("bashlgb500TO_b5a6dt", bashlgb500TO.getB5a6dt());
			query.setInteger("bashlgb500TO_b5a7dt", bashlgb500TO.getB5a7dt());
			query.setInteger("bashlgb500TO_b5a8dt", bashlgb500TO.getB5a8dt());
			query.setInteger("bashlgb500TO_b5a9dt", bashlgb500TO.getB5a9dt());
			query.setString("bashlgb500TO_b5dcnb", bashlgb500TO.getB5dcnb());
			query.setString("bashlgb500TO_b5ddnb", bashlgb500TO.getB5ddnb());
			query.setBigDecimal("bashlgb500TO_b5cxva", bashlgb500TO.getB5cxva());
			query.setBigDecimal("bashlgb500TO_b5cyva", bashlgb500TO.getB5cyva());
			query.setBigDecimal("bashlgb500TO_b5czva", bashlgb500TO.getB5czva());
			query.setBigDecimal("bashlgb500TO_b5c0va", bashlgb500TO.getB5c0va());
			query.setBigDecimal("bashlgb500TO_b5c1va", bashlgb500TO.getB5c1va());
			query.setBigDecimal("bashlgb500TO_b5c2va", bashlgb500TO.getB5c2va());
			query.setBigDecimal("bashlgb500TO_b5c3va", bashlgb500TO.getB5c3va());
			query.setBigDecimal("bashlgb500TO_b5alvxval", bashlgb500TO.getB5alvxval());
			query.setBigDecimal("bashlgb500TO_b5alvyval", bashlgb500TO.getB5alvyval());
			query.setBigDecimal("bashlgb500TO_b5alvzval", bashlgb500TO.getB5alvzval());
			query.setBigDecimal("bashlgb500TO_b5alv0val", bashlgb500TO.getB5alv0val());
			query.setBigDecimal("bashlgb500TO_b5alv1val", bashlgb500TO.getB5alv1val());
			query.setBigDecimal("bashlgb500TO_b5alv2val", bashlgb500TO.getB5alv2val());
			query.setBigDecimal("bashlgb500TO_b5alv3val", bashlgb500TO.getB5alv3val());
			query.setBigDecimal("bashlgb500TO_b5alv4val", bashlgb500TO.getB5alv4val());
			query.setBigDecimal("bashlgb500TO_b5alwpval", bashlgb500TO.getB5alwpval());
			query.setString("bashlgb500TO_b5egcd", bashlgb500TO.getB5egcd());
			query.setString("bashlgb500TO_b5ehcd", bashlgb500TO.getB5ehcd());
			query.setString("bashlgb500TO_b5eicd", bashlgb500TO.getB5eicd());
			query.setString("bashlgb500TO_b5ejcd", bashlgb500TO.getB5ejcd());
			query.setString("bashlgb500TO_b5ekcd", bashlgb500TO.getB5ekcd());
			query.setString("bashlgb500TO_b5al1scde", bashlgb500TO.getB5al1scde());
			query.setString("bashlgb500TO_b5al1tcde", bashlgb500TO.getB5al1tcde());
			query.setString("bashlgb500TO_b5al1ucde", bashlgb500TO.getB5al1ucde());
			query.setString("bashlgb500TO_b5al1vcde", bashlgb500TO.getB5al1vcde());
			query.setString("bashlgb500TO_b5al1wcde", bashlgb500TO.getB5al1wcde());
			query.setString("bashlgb500TO_b5al15cde", bashlgb500TO.getB5al15cde());
			query.setInteger("bashlgb500TO_b5ibnb", bashlgb500TO.getB5ibnb());
			query.setInteger("bashlgb500TO_b5icnb", bashlgb500TO.getB5icnb());
			query.setInteger("bashlgb500TO_b5idnb", bashlgb500TO.getB5idnb());
			query.setInteger("bashlgb500TO_b5ienb", bashlgb500TO.getB5ienb());
			query.setInteger("bashlgb500TO_b5ifnb", bashlgb500TO.getB5ifnb());
			query.setInteger("bashlgb500TO_b5ignb", bashlgb500TO.getB5ignb());
			query.setInteger("bashlgb500TO_b5ihnb", bashlgb500TO.getB5ihnb());
			query.setInteger("bashlgb500TO_b5aliqnbr", bashlgb500TO.getB5aliqnbr());
			query.setInteger("bashlgb500TO_b5alirnbr", bashlgb500TO.getB5alirnbr());
			query.setInteger("bashlgb500TO_b5alisnbr", bashlgb500TO.getB5alisnbr());
			query.setInteger("bashlgb500TO_b5alitnbr", bashlgb500TO.getB5alitnbr());
			query.setInteger("bashlgb500TO_b5aliunbr", bashlgb500TO.getB5aliunbr());
			query.setInteger("bashlgb500TO_b5alivnbr", bashlgb500TO.getB5alivnbr());
			query.setInteger("bashlgb500TO_b5bedt", bashlgb500TO.getB5bedt());
			query.setInteger("bashlgb500TO_b5bfdt", bashlgb500TO.getB5bfdt());
			query.setInteger("bashlgb500TO_b5alsedte", bashlgb500TO.getB5alsedte());
			query.setCharacter("bashlgb500TO_b5al4htxt", bashlgb500TO.getB5al4htxt());
			query.setCharacter("bashlgb500TO_b5al4itxt", bashlgb500TO.getB5al4itxt());
			query.setCharacter("bashlgb500TO_b5al4jtxt", bashlgb500TO.getB5al4jtxt());
			query.setCharacter("bashlgb500TO_b5al4ktxt", bashlgb500TO.getB5al4ktxt());
			query.setCharacter("bashlgb500TO_b5al4ltxt", bashlgb500TO.getB5al4ltxt());
			query.setCharacter("bashlgb500TO_b5al4mtxt", bashlgb500TO.getB5al4mtxt());
			query.setCharacter("bashlgb500TO_b5al4ntxt", bashlgb500TO.getB5al4ntxt());
			query.setCharacter("bashlgb500TO_b5al4otxt", bashlgb500TO.getB5al4otxt());
			query.setCharacter("bashlgb500TO_b5al4ptxt", bashlgb500TO.getB5al4ptxt());
			query.setCharacter("bashlgb500TO_b5al4qtxt", bashlgb500TO.getB5al4qtxt());
			query.setString("bashlgb500TO_b5al36txt", bashlgb500TO.getB5al36txt());
			query.setString("bashlgb500TO_b5al3vtxt", bashlgb500TO.getB5al3vtxt());
			query.setString("bashlgb500TO_b5al3wtxt", bashlgb500TO.getB5al3wtxt());
			query.setString("bashlgb500TO_b5al3xtxt", bashlgb500TO.getB5al3xtxt());
			query.setString("bashlgb500TO_b5al3ytxt", bashlgb500TO.getB5al3ytxt());
			query.setString("bashlgb500TO_b5al37txt", bashlgb500TO.getB5al37txt());
			query.setString("bashlgb500TO_b5dcvn", bashlgb500TO.getB5dcvn());
			query.setString("bashlgb500TO_b5aavn", bashlgb500TO.getB5aavn());
			query.setInteger("bashlgb500TO_b5akdt", bashlgb500TO.getB5akdt());
			query.setInteger("bashlgb500TO_b5abtm", bashlgb500TO.getB5abtm());

			
			query.setCharacter("bashlgb500TO_b5usin42", bashlgb500TO.getB5usin42());
			query.setCharacter("bashlgb500TO_b5usin43", bashlgb500TO.getB5usin43());
			query.setCharacter("bashlgb500TO_b5usin44", bashlgb500TO.getB5usin44());
			query.setCharacter("bashlgb500TO_b5usin45", bashlgb500TO.getB5usin45());
			query.setCharacter("bashlgb500TO_b5usin46", bashlgb500TO.getB5usin46());
			query.setCharacter("bashlgb500TO_b5usin47", bashlgb500TO.getB5usin47());
			query.setCharacter("bashlgb500TO_b5usin48", bashlgb500TO.getB5usin48());
			query.setCharacter("bashlgb500TO_b5usin49", bashlgb500TO.getB5usin49());
			query.setCharacter("bashlgb500TO_b5usin50", bashlgb500TO.getB5usin50());
			query.setCharacter("bashlgb500TO_b5usin51", bashlgb500TO.getB5usin51());
			query.setCharacter("bashlgb500TO_b5usin52", bashlgb500TO.getB5usin52());
			query.setCharacter("bashlgb500TO_b5usin53", bashlgb500TO.getB5usin53());
			query.setCharacter("bashlgb500TO_b5usin54", bashlgb500TO.getB5usin54());
			query.setCharacter("bashlgb500TO_b5usin55", bashlgb500TO.getB5usin55());
			query.setCharacter("bashlgb500TO_b5usin56", bashlgb500TO.getB5usin56());
			query.setCharacter("bashlgb500TO_b5usin57", bashlgb500TO.getB5usin57());
			query.setCharacter("bashlgb500TO_b5usin58", bashlgb500TO.getB5usin58());
			query.setCharacter("bashlgb500TO_b5usin59", bashlgb500TO.getB5usin59());
			query.setCharacter("bashlgb500TO_b5usin60", bashlgb500TO.getB5usin60());
			query.setCharacter("bashlgb500TO_b5usin61", bashlgb500TO.getB5usin61());
			
			query.setString("bashlgb500TO_b5al25cde", bashlgb500TO.getB5al25cde());
			query.setString("bashlgb500TO_b5al26cde", bashlgb500TO.getB5al26cde());
			query.setString("bashlgb500TO_b5al27cde", bashlgb500TO.getB5al27cde());
			query.setString("bashlgb500TO_b5al28cde", bashlgb500TO.getB5al28cde());
		    query.setString("bashlgb500TO_b5al29cde", bashlgb500TO.getB5al29cde());
			query.setString("bashlgb500TO_b5al30cde", bashlgb500TO.getB5al30cde());
			query.setString("bashlgb500TO_b5al31cde", bashlgb500TO.getB5al31cde());
			query.setString("bashlgb500TO_b5al32cde", bashlgb500TO.getB5al32cde());
			query.setString("bashlgb500TO_b5al33cde", bashlgb500TO.getB5al33cde());
			query.setString("bashlgb500TO_b5al34cde", bashlgb500TO.getB5al34cde());
			query.setString("bashlgb500TO_b5al35cde", bashlgb500TO.getB5al35cde());
			query.setString("bashlgb500TO_b5al36cde", bashlgb500TO.getB5al36cde());
			query.setString("bashlgb500TO_b5al37cde", bashlgb500TO.getB5al37cde());
			query.setString("bashlgb500TO_b5al38cde", bashlgb500TO.getB5al38cde());
			query.setString("bashlgb500TO_b5al39cde", bashlgb500TO.getB5al39cde());
			query.setString("bashlgb500TO_b5al40cde", bashlgb500TO.getB5al40cde());
			query.setString("bashlgb500TO_b5al41cde", bashlgb500TO.getB5al41cde());
			query.setString("bashlgb500TO_b5al42cde", bashlgb500TO.getB5al42cde());
			query.setString("bashlgb500TO_b5al43cde", bashlgb500TO.getB5al43cde());
			query.setString("bashlgb500TO_b5al44cde", bashlgb500TO.getB5al44cde());
					
			query.setBigDecimal("bashlgb500TO_b5aldjpct", bashlgb500TO.getB5aldjpct());
			query.setBigDecimal("bashlgb500TO_b5aldkpct", bashlgb500TO.getB5aldkpct());
			query.setBigDecimal("bashlgb500TO_b5aldlpct", bashlgb500TO.getB5aldlpct());
			query.setBigDecimal("bashlgb500TO_b5aldmpct", bashlgb500TO.getB5aldmpct());
			query.setBigDecimal("bashlgb500TO_b5aldnpct", bashlgb500TO.getB5aldnpct());
			query.setBigDecimal("bashlgb500TO_b5aldopct", bashlgb500TO.getB5aldopct());
			query.setBigDecimal("bashlgb500TO_b5aldppct", bashlgb500TO.getB5aldppct());
			query.setBigDecimal("bashlgb500TO_b5aldqpct", bashlgb500TO.getB5aldqpct());
			query.setBigDecimal("bashlgb500TO_b5aldrpct", bashlgb500TO.getB5aldrpct());
			query.setBigDecimal("bashlgb500TO_b5aldspct", bashlgb500TO.getB5aldspct());
			query.setBigDecimal("bashlgb500TO_b5aldtpct", bashlgb500TO.getB5aldtpct());
			query.setBigDecimal("bashlgb500TO_b5aldupct", bashlgb500TO.getB5aldupct());
			query.setBigDecimal("bashlgb500TO_b5aldvpct", bashlgb500TO.getB5aldvpct());
			query.setBigDecimal("bashlgb500TO_b5aldwpct", bashlgb500TO.getB5aldwpct());
			query.setBigDecimal("bashlgb500TO_b5aldxpct", bashlgb500TO.getB5aldxpct());
			query.setBigDecimal("bashlgb500TO_b5aldypct", bashlgb500TO.getB5aldypct());
			query.setBigDecimal("bashlgb500TO_b5aldzpct", bashlgb500TO.getB5aldzpct());
			query.setBigDecimal("bashlgb500TO_b5alaapct", bashlgb500TO.getB5alaapct());
			query.setBigDecimal("bashlgb500TO_b5alabpct", bashlgb500TO.getB5alabpct());
			query.setBigDecimal("bashlgb500TO_b5alacpct", bashlgb500TO.getB5alacpct());
			
			query.setInteger("bashlgb500TO_b5aljknbr", bashlgb500TO.getB5aljknbr());
			query.setInteger("bashlgb500TO_b5aljlnbr", bashlgb500TO.getB5aljlnbr());
			query.setInteger("bashlgb500TO_b5aljmnbr", bashlgb500TO.getB5aljmnbr());
			query.setInteger("bashlgb500TO_b5aljnnbr", bashlgb500TO.getB5aljnnbr());
			query.setInteger("bashlgb500TO_b5aljonbr", bashlgb500TO.getB5aljonbr());
			query.setInteger("bashlgb500TO_b5aljpnbr", bashlgb500TO.getB5aljpnbr());
			query.setInteger("bashlgb500TO_b5aljqnbr", bashlgb500TO.getB5aljqnbr());
			query.setInteger("bashlgb500TO_b5aljrnbr", bashlgb500TO.getB5aljrnbr());
			query.setInteger("bashlgb500TO_b5aljsnbr", bashlgb500TO.getB5aljsnbr());
			query.setInteger("bashlgb500TO_b5aljtnbr", bashlgb500TO.getB5aljtnbr());
			query.setInteger("bashlgb500TO_b5aljunbr", bashlgb500TO.getB5aljunbr());
			query.setInteger("bashlgb500TO_b5aljvnbr", bashlgb500TO.getB5aljvnbr());
			query.setInteger("bashlgb500TO_b5aljwnbr", bashlgb500TO.getB5aljwnbr());
			query.setInteger("bashlgb500TO_b5aljxnbr", bashlgb500TO.getB5aljxnbr());
			query.setInteger("bashlgb500TO_b5aljynbr", bashlgb500TO.getB5aljynbr());
			query.setInteger("bashlgb500TO_b5aljznbr", bashlgb500TO.getB5aljznbr());
			query.setInteger("bashlgb500TO_b5alaanbr", bashlgb500TO.getB5alaanbr());
			query.setInteger("bashlgb500TO_b5alabnbr", bashlgb500TO.getB5alabnbr());
			query.setInteger("bashlgb500TO_b5alacnbr", bashlgb500TO.getB5alacnbr());
			query.setInteger("bashlgb500TO_b5aladnbr", bashlgb500TO.getB5aladnbr());
			
			query.setBigDecimal("bashlgb500TO_b5alwqval", bashlgb500TO.getB5alwqval());
			query.setBigDecimal("bashlgb500TO_b5alwrval", bashlgb500TO.getB5alwrval());
			query.setBigDecimal("bashlgb500TO_b5alwsval", bashlgb500TO.getB5alwsval());
			query.setBigDecimal("bashlgb500TO_b5alwtval", bashlgb500TO.getB5alwtval());
			query.setBigDecimal("bashlgb500TO_b5alwuval", bashlgb500TO.getB5alwuval());
			query.setBigDecimal("bashlgb500TO_b5alwvval", bashlgb500TO.getB5alwvval());
			query.setBigDecimal("bashlgb500TO_b5alwwval", bashlgb500TO.getB5alwwval());
			query.setBigDecimal("bashlgb500TO_b5alwxval", bashlgb500TO.getB5alwxval());
			query.setBigDecimal("bashlgb500TO_b5alwyval", bashlgb500TO.getB5alwyval());
			query.setBigDecimal("bashlgb500TO_b5alwzval", bashlgb500TO.getB5alwzval());
			query.setBigDecimal("bashlgb500TO_b5alaaval", bashlgb500TO.getB5alaaval());
			query.setBigDecimal("bashlgb500TO_b5alabval", bashlgb500TO.getB5alabval());
			query.setBigDecimal("bashlgb500TO_b5alacval", bashlgb500TO.getB5alacval());
			query.setBigDecimal("bashlgb500TO_b5aladval", bashlgb500TO.getB5aladval());
			query.setBigDecimal("bashlgb500TO_b5alaeval", bashlgb500TO.getB5alaeval());
			query.setBigDecimal("bashlgb500TO_b5alafval", bashlgb500TO.getB5alafval());
			query.setBigDecimal("bashlgb500TO_b5alagval", bashlgb500TO.getB5alagval());
			query.setBigDecimal("bashlgb500TO_b5alahval", bashlgb500TO.getB5alahval());
			query.setBigDecimal("bashlgb500TO_b5alaival", bashlgb500TO.getB5alaival());
			query.setBigDecimal("bashlgb500TO_b5alajval", bashlgb500TO.getB5alajval());
			
			query.setInteger("bashlgb500TO_b5alsjdte", bashlgb500TO.getB5alsjdte());
			query.setInteger("bashlgb500TO_b5alskdte", bashlgb500TO.getB5alskdte());
			query.setInteger("bashlgb500TO_b5alsldte", bashlgb500TO.getB5alsldte());
			query.setInteger("bashlgb500TO_b5alsmdte", bashlgb500TO.getB5alsmdte());
			query.setInteger("bashlgb500TO_b5alsndte", bashlgb500TO.getB5alsndte());
			query.setInteger("bashlgb500TO_b5alsodte", bashlgb500TO.getB5alsodte());
			query.setInteger("bashlgb500TO_b5alspdte", bashlgb500TO.getB5alspdte());
			query.setInteger("bashlgb500TO_b5alsqdte", bashlgb500TO.getB5alsqdte());
			query.setInteger("bashlgb500TO_b5alsrdte", bashlgb500TO.getB5alsrdte());
			query.setInteger("bashlgb500TO_b5alssdte", bashlgb500TO.getB5alssdte());
			query.setInteger("bashlgb500TO_b5alstdte", bashlgb500TO.getB5alstdte());
			query.setInteger("bashlgb500TO_b5alsudte", bashlgb500TO.getB5alsudte());
			query.setInteger("bashlgb500TO_b5alsvdte", bashlgb500TO.getB5alsvdte());
			query.setInteger("bashlgb500TO_b5alswdte", bashlgb500TO.getB5alswdte());
			query.setInteger("bashlgb500TO_b5alsxdte", bashlgb500TO.getB5alsxdte());
			query.setInteger("bashlgb500TO_b5alsydte", bashlgb500TO.getB5alsydte());
			query.setInteger("bashlgb500TO_b5alszdte", bashlgb500TO.getB5alszdte());
			query.setInteger("bashlgb500TO_b5alaadte", bashlgb500TO.getB5alaadte());
			query.setInteger("bashlgb500TO_b5alabdte", bashlgb500TO.getB5alabdte());
			query.setInteger("bashlgb500TO_b5alacdte", bashlgb500TO.getB5alacdte());
						
			query.setString("bashlgb500TO_b5al6txt", bashlgb500TO.getB5al6txt());
			query.setString("bashlgb500TO_b5al7txt", bashlgb500TO.getB5al7txt());
			query.setString("bashlgb500TO_b5al8txt", bashlgb500TO.getB5al8txt());
			query.setString("bashlgb500TO_b5al9txt", bashlgb500TO.getB5al9txt());
			query.setString("bashlgb500TO_b5al10txt", bashlgb500TO.getB5al10txt());
			query.setString("bashlgb500TO_b5al11txt", bashlgb500TO.getB5al11txt());
			query.setString("bashlgb500TO_b5al12txt", bashlgb500TO.getB5al12txt());
			query.setString("bashlgb500TO_b5al13txt", bashlgb500TO.getB5al13txt());
			query.setString("bashlgb500TO_b5al14txt", bashlgb500TO.getB5al14txt());
			query.setString("bashlgb500TO_b5al15txt", bashlgb500TO.getB5al15txt());
			query.setString("bashlgb500TO_b5al16txt", bashlgb500TO.getB5al16txt());
			query.setString("bashlgb500TO_b5al17txt", bashlgb500TO.getB5al17txt());
			query.setString("bashlgb500TO_b5al18txt", bashlgb500TO.getB5al18txt());
			query.setString("bashlgb500TO_b5al19txt", bashlgb500TO.getB5al19txt());
			query.setString("bashlgb500TO_b5al20txt", bashlgb500TO.getB5al20txt());
			query.setString("bashlgb500TO_b5al21txt", bashlgb500TO.getB5al21txt());
			query.setString("bashlgb500TO_b5al22txt", bashlgb500TO.getB5al22txt());
			query.setString("bashlgb500TO_b5al23txt", bashlgb500TO.getB5al23txt());
			query.setString("bashlgb500TO_b5al24txt", bashlgb500TO.getB5al24txt());
			query.setString("bashlgb500TO_b5al25txt", bashlgb500TO.getB5al25txt());
			
			query.setString("bashlgb500TO_b5al26txt", bashlgb500TO.getB5al26txt());
			query.setString("bashlgb500TO_b5al27txt", bashlgb500TO.getB5al27txt());
			query.setString("bashlgb500TO_b5al28txt", bashlgb500TO.getB5al28txt());
			query.setString("bashlgb500TO_b5al29txt", bashlgb500TO.getB5al29txt());
			query.setString("bashlgb500TO_b5al30txt", bashlgb500TO.getB5al30txt());
			query.setString("bashlgb500TO_b5al31txt", bashlgb500TO.getB5al31txt());
			query.setString("bashlgb500TO_b5al32txt", bashlgb500TO.getB5al32txt());
			query.setString("bashlgb500TO_b5al33txt", bashlgb500TO.getB5al33txt());
			query.setString("bashlgb500TO_b5al34txt", bashlgb500TO.getB5al34txt());
			query.setString("bashlgb500TO_b5al35txt", bashlgb500TO.getB5al35txt());
			query.setString("bashlgb500TO_b5al38txt", bashlgb500TO.getB5al38txt());
			query.setString("bashlgb500TO_b5al39txt", bashlgb500TO.getB5al39txt());
			query.setString("bashlgb500TO_b5al40txt", bashlgb500TO.getB5al40txt());
			query.setString("bashlgb500TO_b5al41txt", bashlgb500TO.getB5al41txt());
			query.setString("bashlgb500TO_b5al42txt", bashlgb500TO.getB5al42txt());
			query.setString("bashlgb500TO_b5al43txt", bashlgb500TO.getB5al43txt());
			query.setString("bashlgb500TO_b5al44txt", bashlgb500TO.getB5al44txt());
			query.setString("bashlgb500TO_b5al45txt", bashlgb500TO.getB5al45txt());
			query.setString("bashlgb500TO_b5al46txt", bashlgb500TO.getB5al46txt());
			query.setString("bashlgb500TO_b5al47txt", bashlgb500TO.getB5al47txt());
						
			int result = query.executeUpdate();
			status.setNumOfRows(result);

		} catch (org.hibernate.HibernateException ex) {
			status = new DBAccessStatus(ex);
		}
		return status;
	}

	public Bashlgb500TO readHistoryFIle(String location, String policyno, String symbol, String masterco, String module,
			String unitno, int riskloc, int risksubloc, String insline, String product, int lossdate) {
			DBAccessStatus status = new DBAccessStatus();
			Bashlgb500TO localbashlgb500TO = null;
			
		try{
			Session session = HibernateSessionFactory.current().getSession("BASHLGB500");
			Query query = session.getNamedQuery("Bashlgb500TO_GETHISTORYRECORDS");
			query.setParameter("location", location);
			query.setParameter("masterco", masterco);
			query.setParameter("symbol", symbol);
			query.setParameter("policyno", policyno);			
			query.setParameter("module", module);
			query.setParameter("unitno", unitno);
			query.setParameter("riskloc", riskloc);
			query.setParameter("risksubloc", risksubloc);
			query.setParameter("insline", insline);
			query.setParameter("product", product);
			query.setParameter("lossdate", lossdate);
			
			query.setMaxResults(1);
			List resultList = query.list();


			if (resultList != null && resultList.size() > 0) {
				Object obj = resultList.get(0);
				if (obj != null) {
					localbashlgb500TO = (Bashlgb500TO)(obj);
					
					status = new DBAccessStatus(DBAccessStatus.DB_OK);
					localbashlgb500TO.setDBAccessStatus(status);
				} 
				
			}
			else status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		 catch (HibernateException ex) {
			status.setException(ex);
		}
		 
		
		return localbashlgb500TO;
		
	}


}
