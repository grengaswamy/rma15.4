// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.to;

import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DBAccessStatus;
import bphx.c2ab.util.SignType;
import bphx.sdf.datatype.ABODate;
import bphx.sdf.datatype.ABOTime;
import com.csc.pt.svc.data.id.Bashlgbu00ID;

public class Bashlgbu00TO {
	private DBAccessStatus dBAccessStatus;
	private String loguser = "";
	private ABODate logdate = new ABODate();
	private ABOTime logtime = new ABOTime();
	private char buc7st;
	private int buandt;
	private String butype0act = "";
	private int buafdt;
	private int bualdt;
	private String bueftx = "";
	private String buegtx = "";
	private String bueitx = "";
	private String buejtx = "";
	private String buapnb = "";
	private Bashlgbu00ID id = new Bashlgbu00ID();

	public DBAccessStatus getDBAccessStatus() {
		return this.dBAccessStatus;
	}

	public void setDBAccessStatus(DBAccessStatus dBAccessStatus) {
		this.dBAccessStatus = dBAccessStatus;
	}

	public String getLoguser() {
		return this.loguser;
	}

	public void setLoguser(String loguser) {
		this.loguser = loguser;
	}

	public ABODate getLogdate() {
		return this.logdate.clone();
	}

	public void setLogdate(ABODate logdate) {
		this.logdate.assign(logdate);
	}

	public ABOTime getLogtime() {
		return this.logtime.clone();
	}

	public void setLogtime(ABOTime logtime) {
		this.logtime.assign(logtime);
	}

	public int getLogseqnum() {
		return this.id.getLogseqnum();
	}

	public void setLogseqnum(int logseqnum) {
		this.id.setLogseqnum(logseqnum);
	}

	public String getLocation() {
		return this.id.getLocation();
	}

	public void setLocation(String location) {
		this.id.setLocation(location);
	}

	public String getMasterco() {
		return this.id.getMasterco();
	}

	public void setMasterco(String masterco) {
		this.id.setMasterco(masterco);
	}

	public String getSymbol() {
		return this.id.getSymbol();
	}

	public void setSymbol(String symbol) {
		this.id.setSymbol(symbol);
	}

	public String getPolicyno() {
		return this.id.getPolicyno();
	}

	public void setPolicyno(String policyno) {
		this.id.setPolicyno(policyno);
	}

	public String getModule() {
		return this.id.getModule();
	}

	public void setModule(String module) {
		this.id.setModule(module);
	}

	public int getRiskloc() {
		return this.id.getRiskloc();
	}

	public void setRiskloc(int riskloc) {
		this.id.setRiskloc(riskloc);
	}


	public char getRecstatus() {
		return this.id.getRecstatus();
	}

	public void setRecstatus(char recstatus) {
		this.id.setRecstatus(recstatus);
	}

	public char getBuc7st() {
		return this.buc7st;
	}

	public void setBuc7st(char buc7st) {
		this.buc7st = buc7st;
	}

	public int getBuandt() {
		return this.buandt;
	}

	public void setBuandt(int buandt) {
		this.buandt = buandt;
	}

	public String getButype0act() {
		return this.butype0act;
	}

	public void setButype0act(String butype0act) {
		this.butype0act = butype0act;
	}

	public int getBuafdt() {
		return this.buafdt;
	}

	public void setBuafdt(int buafdt) {
		this.buafdt = buafdt;
	}

	public int getBualdt() {
		return this.bualdt;
	}

	public void setBualdt(int bualdt) {
		this.bualdt = bualdt;
	}

	public String getBueftx() {
		return this.bueftx;
	}

	public void setBueftx(String bueftx) {
		this.bueftx = bueftx;
	}

	public String getBuegtx() {
		return this.buegtx;
	}

	public void setBuegtx(String buegtx) {
		this.buegtx = buegtx;
	}

	public String getBueitx() {
		return this.bueitx;
	}

	public void setBueitx(String bueitx) {
		this.bueitx = bueitx;
	}

	public String getBuejtx() {
		return this.buejtx;
	}

	public void setBuejtx(String buejtx) {
		this.buejtx = buejtx;
	}

	public String getBuapnb() {
		return this.buapnb;
	}

	public void setBuapnb(String buapnb) {
		this.buapnb = buapnb;
	}
	
	public Bashlgbu00ID getId() {
		return this.id;
	}

	public void setId(Bashlgbu00ID id) {
		this.id = id;
	}
}
