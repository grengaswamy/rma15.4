// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.ProcessInfoObject;
import bphx.c2ab.util.DataConverter;

public class Rskclunit1TO extends ProcessInfoObject {
	private String location = "";
	private String masterco = "";
	private String symbol = "";
	private String policyno = "";
	private String module = "";
	private String lossDate = "";

	public Rskclunit1TO(byte[] claimData) {
		basicInitialization();
		setClaimData(claimData);
	}

	public Rskclunit1TO() {
		basicInitialization();
	}

	public int getClaimDataSize() {
		return 23;
	}

	public void setClaimData(byte[] buf) {
		int offset = 1;
		location = DataConverter.readString(buf, offset, 2);
		offset += 2;
		masterco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		symbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		policyno = DataConverter.readString(buf, offset, 7);
		offset += 7;
		module = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lossDate = DataConverter.readString(buf, offset, 7);
	}

	public byte[] getClaimData() {
		byte[] buf = new byte[getClaimDataSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, location, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, masterco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, symbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, policyno, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, module, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lossDate, 7);
		return buf;
	}

	public void basicInitialization() {}


	public String getSymbol() {
		return this.symbol;
	}

	public String getPolicyno() {
		return this.policyno;
	}

	public String getModule() {
		return this.module;
	}

	public String getLocation() {
		return this.location;
	}

	public String getMasterco() {
		return this.masterco;
	}

	public String getLossDate() {
		return lossDate;
	}

	public void setLossDate(String lossDate) {
		this.lossDate = lossDate;
	}
}
