package com.csc.pt.svc.rsk;

import bphx.c2ab.cobol.CalledProgList;
import bphx.c2ab.data.SPResultSetTO;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.ReturnException;
import bphx.sdf.collections.IABODynamicArray;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.datatype.ABODecimal;
import bphx.sdf.util.ABOSystem;
//FSIT # 119900 Res # 47654 Start
import com.csc.pt.svc.common.Baspprenst;
import com.csc.pt.svc.common.ErrorListRecord;
import com.csc.pt.svc.common.Ws03RtrvMsgWorkArea;
//FSIT # 119900 Res # 47654 End
import com.csc.pt.svc.ctrl.to.Pmsp0200RecTO;
import com.csc.pt.svc.custom.common.Cpyerrhrtn;
import com.csc.pt.svc.data.dao.Asarrel1DAO;
import com.csc.pt.svc.data.dao.Asbqcpl1DAO;
import com.csc.pt.svc.data.dao.Bascltj013DAO;
import com.csc.pt.svc.data.dao.Bassys07l1DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.to.Asarrel1TO;
import com.csc.pt.svc.data.to.Asbqcpl1TO;
import com.csc.pt.svc.data.to.Bascltj013TO;
import com.csc.pt.svc.data.to.Bassys07l1TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.rsk.BcListEntryArr23;
import com.csc.pt.svc.db.Ws04ErrorDetailArea;
import com.csc.pt.svc.db.to.Asarrel1RecTO;
import com.csc.pt.svc.db.to.Asbqcpl1RecTO;
import com.csc.pt.svc.rsk.dao.Asb1cppDAO;
import com.csc.pt.svc.rsk.to.Rskdbio043TO;
import com.csc.pt.svc.db.to.Bassys0700RecTO;
import com.csc.pt.svc.db.to.CltjRecTO;
import com.csc.pt.svc.isl.Cobolsqlca;
import com.csc.pt.svc.util.Bascltc01;

/**
*****************************************************************
* REF#  B0918    PGMR: PremKumar               DATE: 2004-07-16 *
* PROJ# BAS0918  SI# BAS1040715A1              SRC:BASRIN       *
* PROBLEM: TREE-HM-Coverage list & Unit list only reading lob   *
* RESOLUTION: Made code changes to read BASSYS0700 with complete*
* key and then tracking back converting it into partial key     *
* until the first hit occurs.                                   *
*****************************************************************
* REF#  B0941     PGMR:C.VAN HORN       DATE:08/02/2004         *
* PROJ# BAS0941   SI#  BAS1040802A1     SRC: QCBLLESRC/BASDINOBJ*
* PROBLEM :THE LAST CHANGE DATE IS NOT PASS BACK TO BE DISPLAYED*
* ON THE GRID.                                                  *
*                                                               *
* RESOLUTION : ADD THE BC-LAST-CHANGE-DATE TO DATA RETURNED     *
* TO THE GRIDS.                                                 *
*****************************************************************
* REF#  B0959     PGMR:C.VAN HORN       DATE:08/16/2004         *
* PROJ# BAS0959   SI#  BAS1040818A1     SRC: QCBLLESRC/BASDINOBJ*
* PROBLEM :ENSURE THAT THE CORRECT INSLINE IS PRESENT IN DATA   *
*                                                               *
* RESOLUTION : ADD BC-INS-LINE TO RETURN DATA                   *
* Also added code to display 'Drop Pending' has the status if   *
* the record has a status of 'P' and drop ind is set to 'Y'     *
* Also commented out the code that reads the BASSYS0700         *
* table for each record selected...this will be done inside the *
* corresponding xxxFrame.jsp and thus read only once.           *
* Added code to return the IssueCode back to the grid           *
*****************************************************************
* REF-NO: 39837      PGMR:                 DATE: 2005-01-07
* PROJ#: BAS1007     FSIT#                 SRC:
* PROBLEM: A PTF from IBM required that the stored procedures
*          be changed to add the RETURN TO CLIENT phrase be
*          added to the SET RESULT SETS command.
* RESOLUTION:
*          Changed the program to add the RETURN TO CLIENT
*          phrase to the SET RESULT SETS command.
* NOTE: Code added using program CVTSTRPRC.
*****************************************************************
* REF : 42027      PGMR: Dhiraj Pandey        DATE: 08/28/2006  *
* PROJ: BAS1887    FSIT ISSUE NO: 89351       SRC : BNRRINOBJ   *
*                  RESOLUTION ID: 42027                         *
* PROBLEM   : Out of sequence records are shown on the tree.    *
* RESOLUTION: If the unit record has 'OSE' as its reason amended*
*             code, it means this record is a future record     *
*             during out of sequence endorsement. These records *
*             should not be shown on Treee.                     *
*             This is for unit for HP LOB.                      *
*****************************************************************
* REF-NO: 42594    PGMR: Dhiraj Pandey        DATE: 11/10/2006  *
* PROJ #: BAS2047  FSIT ISSUE: 94620          SRC:  BNRQINOBJ   *
*                  RESOLUTION: 42594                            *
* PROBLEM:                                                      *
* POL-Unnecessary statements in job logs                        *
* RESOLUTION:                                                   *
* DISPLAY statements have been included in this program for     *
* debugging purpose. These statements have been commented as    *
* they are not required.                                        *
*****************************************************************
*****************************************************************
* REF#:  47783      PGMR: Mark Williams     DATE: 11/16/2006    *
* PROJ#: BAS2045                            SRC:  BNRRINOBJ     *
* FSIT Issue:  47783 - BASSYS enhancement                       *
*                                                               *
* PROBLEM:     BASSYS0700 has been split into BASSYS0701 and    *
*              BASSYS0702.                                      *
*                                                               *
* RESOLUTION:  Open the logical BASSYS07L1 instead of           *
*              BASSYS0700.                                      *
*****************************************************************
* REF : 43427      PGMR: Dhiraj Pandey        DATE: 04/02/2007  *
* PROJ: BAS2169    FSIT ISSUE NO: 102529      SRC : BNRSINOBJ   *
*                  RESOLUTION ID:  43427                        *
* PROBLEM:                                                      *
* OTH - Stored Procedures have problems when sent a key higher  *
* than any on the file.                                         *
* RESOLUTION :                                                  *
* 'Good Return' condition needs to be included so that all      *
* invalid file status can be taken care.                        *
*****************************************************************
* REF : 47654      PGMR: Brij M Garg          DATE: 01/05/2009  * 
* PROJ: B119900    FSIT ISSUE NO: 119900      SRC : BNRR00OBJ   * 
*                  RESOLUTION ID: 47654                         * 
* PROBLEM: POL - User will be able to see the status of any pre-* 
* renewal changes while inquiring the policy.                   * 
* RESOLUTION: New program BASPPRENST is called from this program* 
* which is used to read next record from corresponding files    * 
* and return WS-BATYPE0ACT (Activity Type). If activity type is * 
* 'C' then record status is changed to 'Verified-R'. BASPPRENST * 
* will retrieve the pre-renewal status for a pending policy rcd.* 
*****************************************************************  
****************************************************************** 
* REF#: 50708      PGMR: Manish Kumar     DATE: 11/28/2009      * 
* PROJ#: B133877   FSIT# 133877           SRC: BNRR00OBJ        * 
* *                  RESL# 50708                                  * 
* PROBLEM: Search is not displaying valid construction type due * 
*          to which misleading information is generated.        * 
* RESOLUTION: Search should be made first on the basis of valid * 
*          insurance type and product type. If nothing is return* 
*          -ed then it will start with default product type then* 
*          default insurance type.                              * 
***************************************************************** 
*/

public class Rskdbio043 {
	private static final String PROGRAM_ID = "RSKDBIO043";
	public Asbqcpl1RecTO asbqcpl1RecTO = new Asbqcpl1RecTO();
	public Bassys0700RecTO bassys0700RecTO = new Bassys0700RecTO();
	public Asarrel1RecTO asarrel1RecTO = new Asarrel1RecTO();
	public CltjRecTO cltjRecTO = new CltjRecTO();
	public Pmsp0200RecTO pmsp0200RecTO = new Pmsp0200RecTO();
	private Cobolsqlca cobolsqlca = new Cobolsqlca();
	private char nameAddressIndicator = ' ';
	private IABODynamicArray<BcListEntryArr23> bcListEntryArr23 =
		ABOCollectionCreator.getReferenceFactory(BcListEntryArr23.class).createDynamicArray(1);
	private int numOfEntrys;
	private int numOfEntrysForSql;
	private String ws1FileStatus = "";
	public static final String WS_GOOD_RETURN = "00";
	//FSIT# 133877 RESL# 50708 Start
	public static final String RECORD_NOT_FOUND = "23";
	//FSIT# 133877 RESL# 50708 End
	
	private int wsCount;
	private char wsRead02RecSw = ' ';
	public static final char WS_READ_02_REC = 'Y';
	//FSIT: 152640, Resolution: 55204 - Begin
	//private ABODecimal wsEditMaskCurr = new ABODecimal(11, 2);
	private ABODecimal wsEditMaskCurr = new ABODecimal(20, 2);
	//FSIT: 152640, Resolution: 55204 - End
	private Rskdbio043TO rskdbio043TO = new Rskdbio043TO();
	private Asbqcpl1DAO asbqcpl1DAO = new Asbqcpl1DAO();
	private Bassys07l1DAO bassys07l1DAO = new Bassys07l1DAO();
	private Asarrel1DAO asarrel1DAO = new Asarrel1DAO();
	private Bascltj013DAO bascltj013DAO = new Bascltj013DAO();
	private Pmsp0200DAO pmsp0200DAO = new Pmsp0200DAO();
	private Bassys07l1TO bassys07l1TO = new Bassys07l1TO();
	
	private Bascltj013TO bascltj013TO = new Bascltj013TO();
	private Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
	private Asbqcpl1TO asbqcpl1TO = new Asbqcpl1TO();
	private Asarrel1TO asarrel1TO = new Asarrel1TO();
	private SPResultSetTO resultSetTO = new SPResultSetTO();
	
	//FSIT # 119900 Res # 47654 Start
	private int wsRenewMod;
	private String wsLocationCo = "";
	private String wsMasterCo = "";
	private String wsPolSymbol = "";
	private String wsPolicyNum = "";
	private String wsPolModule = "";
	private String wsInsuranceLine = ""; 
	private int wsRiskLocNo; 
	private int wsRiskLocSubNo ;
	private String wsProduct = ""; 
	private String wsRateState = "";
	private int wsUnitNo; 
	private String wsCoverage = ""; 
	private int wsCoverageSequence ; 
	private int wsDriverId ; 
	private String wsFileName = ""; 
	private char wsBatype0Act = ' '; 
	
	//FSIT # 119900 Res # 47654 End

	//FSIT# 133877 RESL# 50708 Start
	private String wsInsLineChk="";
	private String wsProductChk="";
	private char wsRecordFound='N';
	private char wsErrorOccured='N';
	public  Ws04ErrorDetailArea  ws04ErrorDetailArea = new Ws04ErrorDetailArea();
	public Ws03RtrvMsgWorkArea ws03RtrvMsgWorkArea= new Ws03RtrvMsgWorkArea();
	public ErrorListRecord errorListRecord= new ErrorListRecord();
	 public Cpyerrhrtn cpyerrhrtn = new Cpyerrhrtn();
	//FSIT# 133877 RESL# 50708 End
	public Rskdbio043() {
		bcListEntryArr23.assign((1) - 1, new BcListEntryArr23());
	}

	public void mainSubroutine() {
		programRskdbio043();
		exit();
	}

	public void programRskdbio043() {
		wsRead02RecSw = 'Y';
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		// open. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		asbqcpl1RecTO.initSpaces();
		asbqcpl1RecTO.fbqcpeuRec.setBqabcd(rskdbio043TO.getInMaster0co());
		asbqcpl1RecTO.fbqcpeuRec.setBqaacd(rskdbio043TO.getInLocation());
		asbqcpl1RecTO.fbqcpeuRec.setBqartx(rskdbio043TO.getInSymbol());
		asbqcpl1RecTO.fbqcpeuRec.setBqastx(rskdbio043TO.getInPolicy0num());
		asbqcpl1RecTO.fbqcpeuRec.setBqadnb(rskdbio043TO.getInModule());
		cobolsqlca.setDBAccessStatus(asbqcpl1DAO.openAsbqcpl1TOCsr(KeyType.GREATER_OR_EQ, asbqcpl1RecTO.fbqcpeuRec.getBqaacd(),
			asbqcpl1RecTO.fbqcpeuRec.getBqabcd(), asbqcpl1RecTO.fbqcpeuRec.getBqartx(), asbqcpl1RecTO.fbqcpeuRec.getBqastx(),
			asbqcpl1RecTO.fbqcpeuRec.getBqadnb(), asbqcpl1RecTO.fbqcpeuRec.getBqagtx(), asbqcpl1RecTO.fbqcpeuRec.getBqbrnb(),
			asbqcpl1RecTO.fbqcpeuRec.getBqegnb(), asbqcpl1RecTO.fbqcpeuRec.getBqantx(), asbqcpl1RecTO.fbqcpeuRec.getBqaenb(),
			asbqcpl1RecTO.fbqcpeuRec.getBqc6st()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		numOfEntrys = 0 % 100000;
		initBcListTableSpaces();
		rng2000ReadAsbqcpl1();
		cobolsqlca.setDBAccessStatus(asarrel1DAO.closeCsr());
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		cobolsqlca.setDBAccessStatus(asbqcpl1DAO.closeCsr());
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		// close. No translation required
		cobolsqlca = new Cobolsqlca();
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		cobolsqlca.setDBAccessStatus(bascltj013DAO.closeCsr());
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		cobolsqlca.setDBAccessStatus(pmsp0200DAO.closeCsr());
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		numOfEntrysForSql = numOfEntrys;
		resultSetTO.addResultSet(bcListEntryArr23, numOfEntrysForSql);
	}

	public void exit() {
		throw new ReturnException();
	}

	public String readAsbqcpl1() {
		asbqcpl1TO = asbqcpl1DAO.fetchNext(asbqcpl1TO);
		asbqcpl1RecTO.setData(asbqcpl1TO.getData());
		asbqcpl1RecTO.setData(asbqcpl1RecTO.getData());
		ws1FileStatus = Functions.subString(asbqcpl1TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (asbqcpl1TO.getDBAccessStatus().isEOF()) {
			return "";
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			// continue
		} else {
			return "";
		}
		if (rskdbio043TO.getInMaster0co().compareTo(asbqcpl1RecTO.fbqcpeuRec.getBqabcd()) != 0
			|| rskdbio043TO.getInLocation().compareTo(asbqcpl1RecTO.fbqcpeuRec.getBqaacd()) != 0
			|| rskdbio043TO.getInSymbol().compareTo(asbqcpl1RecTO.fbqcpeuRec.getBqartx()) != 0
			|| rskdbio043TO.getInPolicy0num().compareTo(asbqcpl1RecTO.fbqcpeuRec.getBqastx()) != 0
			|| rskdbio043TO.getInModule().compareTo(asbqcpl1RecTO.fbqcpeuRec.getBqadnb()) != 0) {
			// GO TO 2000-EXIT-ASBQCPL1.
			// GO TO 2000-EXIT-ASBQCPL1.
			return "";
		} else {
			if (asbqcpl1RecTO.fbqcpeuRec.getBqbatx().compareTo("OSE") == 0) {
				return "2000-READ-ASBQCPL1";
			}
		}
		rng5000ProcessEntry();
		// DISPLAY BC-LOC-ADD(WS-COUNT) " "
		// BC-LOC-CITY(WS-COUNT) " "
		// BC-RATE-STATE(WS-COUNT) " "
		// BC-YEAR-OF-CONSTR(WS-COUNT) " "
		// BC-TYPE-OF-CONSTR(WS-COUNT).
		// DISPLAY BC-LOC-ADD(WS-COUNT) " "
		// BC-LOC-CITY(WS-COUNT) " "
		// BC-RATE-STATE(WS-COUNT) " "
		// BC-YEAR-OF-CONSTR(WS-COUNT) " "
		// BC-TYPE-OF-CONSTR(WS-COUNT).
		return "2000-READ-ASBQCPL1";
	}

	public void exitAsbqcpl1() {
	// exit
	}

	public void processEntry() {
		numOfEntrys = (1 + numOfEntrys) % 100000;
		wsCount = numOfEntrys;
		getBcListEntryArr23(wsCount).setBcUnitNumber(ABOSystem.convertUsingString(asbqcpl1RecTO.fbqcpeuRec.getBqaenb(), "S99999"));
		if (asbqcpl1RecTO.fbqcpeuRec.getBqc6st() == 'V') {
		//FSIT # 119900 Res # 47654 Start
			InitializeWsPreRenewalRec();
			wsInsuranceLine = asbqcpl1RecTO.fbqcpeuRec.getBqagtx();   
			wsRiskLocNo = asbqcpl1RecTO.fbqcpeuRec.getBqbrnb();
			wsRiskLocSubNo = asbqcpl1RecTO.fbqcpeuRec.getBqegnb();
			wsProduct = asbqcpl1RecTO.fbqcpeuRec.getBqantx();
			wsUnitNo = asbqcpl1RecTO.fbqcpeuRec.getBqaenb();
			wsFileName = "ASBQCPP";
			CallPgmChkNxtRecord();
			if(wsBatype0Act == 'C'){
				getBcListEntryArr23(wsCount).setBcStatus("Verified-R");
			}else{
				getBcListEntryArr23(wsCount).setBcStatus("Verified");
			} 
		//FSIT # 119900 Res # 47654 End
		} else {
			if (asbqcpl1RecTO.fbqcpeuRec.getBqc6st() == 'P') {
				if (asbqcpl1RecTO.fbqcpeuRec.getBqc7st() == 'Y') {
					getBcListEntryArr23(wsCount).setBcStatus("Drop Pending");
				} else {
					getBcListEntryArr23(wsCount).setBcStatus("Pending");
				}
			} else {
				if (asbqcpl1RecTO.fbqcpeuRec.getBqc6st() == 'D') {
					getBcListEntryArr23(wsCount).setBcStatus("Drop");
				} else {
					if (asbqcpl1RecTO.fbqcpeuRec.getBqc6st() == 'H') {
						getBcListEntryArr23(wsCount).setBcStatus("History");
					}
				}
			}
		}
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqa3va());
		getBcListEntryArr23(wsCount).setBcRatePremium(getWsEditMaskCurrFormatted());
		getBcListEntryArr23(wsCount).setBcRiskLocationNumber(ABOSystem.convertUsingString(asbqcpl1RecTO.fbqcpeuRec.getBqbrnb(), "S99999"));
		getBcListEntryArr23(wsCount).setBcRiskSublocationNumber(ABOSystem.convertUsingString(asbqcpl1RecTO.fbqcpeuRec.getBqegnb(), "S99999"));
		getBcListEntryArr23(wsCount).setBcProduct(asbqcpl1RecTO.fbqcpeuRec.getBqantx());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqa6va());
		getBcListEntryArr23(wsCount).setBcCoverageLimit1(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqa7va());
		getBcListEntryArr23(wsCount).setBcCoveragePremium1(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqa8va());
		getBcListEntryArr23(wsCount).setBcCoverageLimit2(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqa9va());
		getBcListEntryArr23(wsCount).setBcCoveragePremium2(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqbava());
		getBcListEntryArr23(wsCount).setBcCoverageLimit3(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqbbva());
		getBcListEntryArr23(wsCount).setBcCoveragePremium3(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqbcva());
		getBcListEntryArr23(wsCount).setBcCoverageLimit4(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqbdva());
		getBcListEntryArr23(wsCount).setBcCoveragePremium4(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqbeva());
		getBcListEntryArr23(wsCount).setBcCoverageLimit5(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqbfva());
		getBcListEntryArr23(wsCount).setBcCoveragePremium5(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqbgva());
		getBcListEntryArr23(wsCount).setBcCoverageLimit6(getWsEditMaskCurrFormatted());
		wsEditMaskCurr.assign(asbqcpl1RecTO.fbqcpeuRec.getBqbhva());
		getBcListEntryArr23(wsCount).setBcCoveragePremium6(getWsEditMaskCurrFormatted());
		getBcListEntryArr23(wsCount).setBcLastChangeDate(ABOSystem.convertUsingString(asbqcpl1RecTO.fbqcpeuRec.getBqaldt(), "S9999999"));
		getBcListEntryArr23(wsCount).setBcRateState(asbqcpl1RecTO.fbqcpeuRec.getBqbccd());
		getBcListEntryArr23(wsCount).setBcYearOfConstr(ABOSystem.convertUsingString(asbqcpl1RecTO.fbqcpeuRec.getBqdlnb(), "S9999"));
		getBcListEntryArr23(wsCount).setBcInsLine(asbqcpl1RecTO.fbqcpeuRec.getBqagtx());
		// PERFORM 6000-LOAD-TARGET THRU 6000-EXIT.
		// PERFORM 6000-LOAD-TARGET THRU 6000-EXIT.
		rng7000ReadAsarrel1();
		wsRead02RecSw = 'Y';
		rng8000ReadBascltj013();
		if (wsRead02RecSw == WS_READ_02_REC) {
			rng9000ReadPmsp0200IssueCode();
		}
		//RSK - START
		readAsb1cpp();
		//RSK - END
	}
	
	//RSK - Start
	public void readAsb1cpp(){
		getBcListEntryArr23(wsCount).setBcStatUnit(Integer.toString(asbqcpl1RecTO.fbqcpeuRec.getBqaenb()));
		String unit = Asb1cppDAO.fetchUnit(asbqcpl1RecTO.fbqcpeuRec.getBqaacd(), asbqcpl1RecTO.fbqcpeuRec.getBqabcd(), asbqcpl1RecTO.fbqcpeuRec.getBqartx(), 
				asbqcpl1RecTO.fbqcpeuRec.getBqastx(), asbqcpl1RecTO.fbqcpeuRec.getBqadnb(), asbqcpl1RecTO.fbqcpeuRec.getBqagtx(), 
				asbqcpl1RecTO.fbqcpeuRec.getBqbrnb(), asbqcpl1RecTO.fbqcpeuRec.getBqegnb(), asbqcpl1RecTO.fbqcpeuRec.getBqantx(), asbqcpl1RecTO.fbqcpeuRec.getBqaenb());
		if (unit != null){
			getBcListEntryArr23(wsCount).setBcStatUnit(unit);
		}
	}
	//RSK - End

	public void readAsarrel1() {
		//FSIT# 133877 RESL# 50708 Start
		asarrel1RecTO.asarrel1Record2.setAraacd(rskdbio043TO.getInLocation());
		asarrel1RecTO.asarrel1Record2.setArabcd(rskdbio043TO.getInMaster0co());
		asarrel1RecTO.asarrel1Record2.setAraccd(rskdbio043TO.getInPolco());
		asarrel1RecTO.asarrel1Record2.setAradcd(rskdbio043TO.getInState());
		asarrel1RecTO.asarrel1Record2.setAradtx(rskdbio043TO.getInLob());
		asarrel1RecTO.asarrel1Record2.setAragtx(wsInsuranceLine);
		wsInsLineChk=wsInsuranceLine;
		asarrel1RecTO.asarrel1Record2.setArantx(wsProduct);
		wsProductChk=wsProduct;
		asarrel1RecTO.asarrel1Record2.setAraotx("999999");
		asarrel1RecTO.asarrel1Record2.setArbunb(72);
		asarrel1RecTO.asarrel1Record2.setArajtx("PP CONSTRUCTION CODE");
		asarrel1RecTO.asarrel1Record2.setArdqst('A');
		asarrel1RecTO.asarrel1Record2.setAramtx(new Character(asbqcpl1RecTO.fbqcpeuRec.getBqhytx()).toString());
		startAsarrel1();
		if(wsErrorOccured=='Y'){
			return;
		}
		readAsarrel17200();
		if(wsRecordFound=='Y'){
			next();
			return;
		}
		asarrel1RecTO.asarrel1Record2.setAraacd(rskdbio043TO.getInLocation());
		asarrel1RecTO.asarrel1Record2.setArabcd(rskdbio043TO.getInMaster0co());
		asarrel1RecTO.asarrel1Record2.setAraccd(rskdbio043TO.getInPolco());
		asarrel1RecTO.asarrel1Record2.setAradcd(rskdbio043TO.getInState());
		asarrel1RecTO.asarrel1Record2.setAradtx(rskdbio043TO.getInLob());
		asarrel1RecTO.asarrel1Record2.setAragtx(wsInsuranceLine);
		wsInsLineChk=wsInsuranceLine;
		asarrel1RecTO.asarrel1Record2.setArantx("999999");
		wsProductChk="999999";
		if(wsInsuranceLine.equals("")){
			wsInsLineChk="999";
		}
		asarrel1RecTO.asarrel1Record2.setAraotx("999999");
		asarrel1RecTO.asarrel1Record2.setArbunb(72);
		asarrel1RecTO.asarrel1Record2.setArajtx("PP CONSTRUCTION CODE");
		asarrel1RecTO.asarrel1Record2.setArdqst('A');
		asarrel1RecTO.asarrel1Record2.setAramtx(new Character(asbqcpl1RecTO.fbqcpeuRec.getBqhytx()).toString());
		startAsarrel1();
		if(wsErrorOccured=='Y'){
			return;
		}
		readAsarrel17200();
		if(wsRecordFound=='Y'){
			next();
			return;
		}
		//FSIT# 133877 RESL# 50708 End
		asarrel1RecTO.asarrel1Record2.setAraacd(rskdbio043TO.getInLocation());
		asarrel1RecTO.asarrel1Record2.setArabcd(rskdbio043TO.getInMaster0co());
		asarrel1RecTO.asarrel1Record2.setAraccd(rskdbio043TO.getInPolco());
		asarrel1RecTO.asarrel1Record2.setAradcd(rskdbio043TO.getInState());
		asarrel1RecTO.asarrel1Record2.setAradtx(rskdbio043TO.getInLob());
		asarrel1RecTO.asarrel1Record2.setAragtx("999");
		asarrel1RecTO.asarrel1Record2.setArantx("999999");
		asarrel1RecTO.asarrel1Record2.setAraotx("999999");
		asarrel1RecTO.asarrel1Record2.setArbunb(72);
		asarrel1RecTO.asarrel1Record2.setArajtx("PP CONSTRUCTION CODE");
		asarrel1RecTO.asarrel1Record2.setArdqst('A');
		asarrel1RecTO.asarrel1Record2.setAramtx(new Character(asbqcpl1RecTO.fbqcpeuRec.getBqhytx()).toString());
		cobolsqlca.setDBAccessStatus(asarrel1DAO.openAsarrel1TOCsr(KeyType.GREATER_OR_EQ, asarrel1RecTO.asarrel1Record2.getAraacd(),
			asarrel1RecTO.asarrel1Record2.getArabcd(), asarrel1RecTO.asarrel1Record2.getAraccd(), asarrel1RecTO.asarrel1Record2.getAradcd(),
			asarrel1RecTO.asarrel1Record2.getAradtx(), asarrel1RecTO.asarrel1Record2.getAragtx(), asarrel1RecTO.asarrel1Record2.getArantx(),
			asarrel1RecTO.asarrel1Record2.getAraotx(), asarrel1RecTO.asarrel1Record2.getArbunb(), asarrel1RecTO.asarrel1Record2.getArajtx(),
			asarrel1RecTO.asarrel1Record2.getArdqst(), asarrel1RecTO.asarrel1Record2.getAramtx()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		asarrel1TO = asarrel1DAO.fetchNext(asarrel1TO);
		asarrel1RecTO.setData(asarrel1TO.getData());
		ws1FileStatus = Functions.subString(asarrel1TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (asarrel1TO.getDBAccessStatus().isInvalidKey()) {
			// continue
		}
		
	}
	//FSIT# 133877 RESL# 50708 Start
	public void next(){
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			if (Functions.subString(asarrel1RecTO.asarrel1Record2.getAredtx(), 1, 1).compareTo("@") == 0) {
				getBcListEntryArr23(wsCount).setBcTypeOfConstr(Functions.subString(asarrel1RecTO.asarrel1Record2.getAredtx(), 2, 24));
			} else {
				getBcListEntryArr23(wsCount).setBcTypeOfConstr(asarrel1RecTO.asarrel1Record2.getAredtx());
			}
		}
	}
	
	public void startAsarrel1(){
		cobolsqlca.setDBAccessStatus(asarrel1DAO.openAsarrel1TOCsr(KeyType.GREATER_OR_EQ, asarrel1RecTO.asarrel1Record2.getAraacd(),
				asarrel1RecTO.asarrel1Record2.getArabcd(), asarrel1RecTO.asarrel1Record2.getAraccd(), asarrel1RecTO.asarrel1Record2.getAradcd(),
				asarrel1RecTO.asarrel1Record2.getAradtx(), asarrel1RecTO.asarrel1Record2.getAragtx(), asarrel1RecTO.asarrel1Record2.getArantx(),
				asarrel1RecTO.asarrel1Record2.getAraotx(), asarrel1RecTO.asarrel1Record2.getArbunb(), asarrel1RecTO.asarrel1Record2.getArajtx(),
				asarrel1RecTO.asarrel1Record2.getArdqst(), asarrel1RecTO.asarrel1Record2.getAramtx()));
			ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
			if (cobolsqlca.isInvalidKey()) {
				// continue
			}
			if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
				//continue
			}else{
				if(ws1FileStatus.compareTo(RECORD_NOT_FOUND) == 0){
					wsRecordFound='N';
					return;
				}else{
					wsRecordFound='N';
					gatherErrEventData7100E();
				}
			}
	}
	
	public void gatherErrEventData7100E(){
		wsErrorOccured='Y';
		ws04ErrorDetailArea.init();
		ws04ErrorDetailArea.setWs04ErrSqlDtastgDta(Functions.concat(ws04ErrorDetailArea.getWs04ErrSqlDtastgDta(), 1000, 
				"Attempted start for: ", new String[] { "; ",
			    rskdbio043TO.getInLocation(), "; ",
			    rskdbio043TO.getInMaster0co(),"; ",
			    rskdbio043TO.getInPolco(),"; ",
			    rskdbio043TO.getInState(),"; ",
			    rskdbio043TO.getInLob(),"; ",
			    wsInsLineChk,"; ",
			    wsProductChk,"; ",
			    "999999","; ",
			    "72","; ",
			    "PP CONSTRUCTION CODE","; ",
			    "A","; ",
			    Character.toString(asbqcpl1RecTO.fbqcpeuRec.getBqhytx()),"; "}));
		ws04ErrorDetailArea.ws04SqlErrorSw.setNoSqlError(); 
		ws04ErrorDetailArea.setWs04ErrParaName("7100-START-ASARREL1");
		ws04ErrorDetailArea.setWs04ErrFileName("ASARREL1");
		ws04ErrorDetailArea.setWs04ErrErrorTyp('S');
		ws04ErrorDetailArea.setWs04ErrErrorCode(ws1FileStatus);
		ws03RtrvMsgWorkArea.setWs03MsgFile("PMSMSG");
		ws03RtrvMsgWorkArea.setWs03MsgId("RCV0372");
		ws03RtrvMsgWorkArea.setWs03MsgData("Bad start on ASARREL1 file");
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
			    
	}
	public void readAsarrel17200(){
		asarrel1TO = asarrel1DAO.fetchNext(asarrel1TO);
		asarrel1RecTO.setData(asarrel1TO.getData());
		ws1FileStatus = Functions.subString(asarrel1TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (asarrel1TO.getDBAccessStatus().isInvalidKey()) {
			// continue
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0){
			if(asarrel1RecTO.asarrel1Record2.getAraacd().equals(rskdbio043TO.getInLocation()) &&
					asarrel1RecTO.asarrel1Record2.getArabcd().equals(rskdbio043TO.getInMaster0co()) &&
					asarrel1RecTO.asarrel1Record2.getAraccd().equals(rskdbio043TO.getInPolco()) &&
					asarrel1RecTO.asarrel1Record2.getAradcd().equals(rskdbio043TO.getInState()) &&
					asarrel1RecTO.asarrel1Record2.getAradtx().equals(rskdbio043TO.getInLob()) &&
					asarrel1RecTO.asarrel1Record2.getAramtx().equals(Character.toString(asbqcpl1RecTO.fbqcpeuRec.getBqhytx())) &&
					asarrel1RecTO.asarrel1Record2.getArantx().equals(wsProductChk) &&
					asarrel1RecTO.asarrel1Record2.getAragtx().equals(wsInsLineChk)){
				wsRecordFound='Y';
			}
			else{
				wsRecordFound='N';
				gatherErrEventData7200E();
			}
		}
	}
	
	public void gatherErrEventData7200E(){
		ws04ErrorDetailArea.init();
		ws04ErrorDetailArea.setWs04ErrSqlDtastgDta(Functions.concat(ws04ErrorDetailArea.getWs04ErrSqlDtastgDta(), 1000, 
				"Attempted read for: ", new String[] { "; ",
			    rskdbio043TO.getInLocation(), "; ",
			    rskdbio043TO.getInMaster0co(),"; ",
			    rskdbio043TO.getInPolco(),"; ",
			    rskdbio043TO.getInState(),"; ",
			    rskdbio043TO.getInLob(),"; ",
			    wsInsLineChk,"; ",
			    wsProductChk,"; ",
			    "999999","; ",
			    "72","; ",
			    "PP CONSTRUCTION CODE","; ",
			    "A","; ",
			    Character.toString(asbqcpl1RecTO.fbqcpeuRec.getBqhytx()),"; "}));
		ws04ErrorDetailArea.ws04SqlErrorSw.setNoSqlError(); 
		ws04ErrorDetailArea.setWs04ErrParaName("7200-READ-ASARREL1");
		ws04ErrorDetailArea.setWs04ErrFileName("ASARREL1");
		ws04ErrorDetailArea.setWs04ErrErrorTyp('S');
		ws04ErrorDetailArea.setWs04ErrErrorCode(ws1FileStatus);
		ws03RtrvMsgWorkArea.setWs03MsgFile("PMSMSG");
		ws03RtrvMsgWorkArea.setWs03MsgId("RCV0372");
		ws03RtrvMsgWorkArea.setWs03MsgData("Bad read on ASARREL1 file");
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
	//FSIT# 133877 RESL# 50708 End
	public void readBascltj013() {
		Bascltc01 bascltc01;
		if (asbqcpl1RecTO.fbqcpeuRec.getBqdcnb().compareTo("") != 0) {
			getBcListEntryArr23(wsCount).setBcLocAdd(asbqcpl1RecTO.fbqcpeuRec.getBqdcnb());
			getBcListEntryArr23(wsCount).setBcLocCity(Functions.subString(asbqcpl1RecTO.fbqcpeuRec.getBqddnb(), 1, 28));
		} else {
			bascltc01 = Bascltc01.getInstance();
			nameAddressIndicator = bascltc01.run(nameAddressIndicator);
			if (nameAddressIndicator == 'Y') {
				cltjRecTO.bascltj013Record.setLocation(rskdbio043TO.getInLocation());
				cltjRecTO.bascltj013Record.setMaster0co(rskdbio043TO.getInMaster0co());
				cltjRecTO.bascltj013Record.setPolicy0num(rskdbio043TO.getInPolicy0num());
				cltjRecTO.bascltj013Record.setSymbol(rskdbio043TO.getInSymbol());
				cltjRecTO.bascltj013Record.setModule(rskdbio043TO.getInModule());
				cltjRecTO.bascltj013Record.setTrans0stat(' ');
				cobolsqlca.setDBAccessStatus(bascltj013DAO.openBascltj013TOCsr(KeyType.GREATER_OR_EQ, cltjRecTO.bascltj013Record.getLocation(),
					cltjRecTO.bascltj013Record.getMaster0co(), cltjRecTO.bascltj013Record.getPolicy0num(), cltjRecTO.bascltj013Record.getSymbol(),
					cltjRecTO.bascltj013Record.getModule(), cltjRecTO.bascltj013Record.getTrans0stat()));
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				if (cobolsqlca.isInvalidKey()) {
					// continue
				}
				bascltj013TO = bascltj013DAO.fetchNext(bascltj013TO);
				cltjRecTO.setData(bascltj013TO.getData());
				ws1FileStatus = Functions.subString(bascltj013TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (bascltj013TO.getDBAccessStatus().isInvalidKey()) {
					// continue
				}
				if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
					getBcListEntryArr23(wsCount).setBcLocAdd(Functions.subString(cltjRecTO.bascltj013Record.getAdd0line03(), 1, 30));
					getBcListEntryArr23(wsCount).setBcLocCity(Functions.subString(cltjRecTO.bascltj013Record.getAdd0line04(), 1, 28));
				}
			} else {
				pmsp0200RecTO.rec02fmtRec.setId02("02");
				pmsp0200RecTO.rec02fmtRec.setLocation(rskdbio043TO.getInLocation());
				pmsp0200RecTO.rec02fmtRec.setPolicy0num(rskdbio043TO.getInPolicy0num());
				pmsp0200RecTO.rec02fmtRec.setSymbol(rskdbio043TO.getInSymbol());
				pmsp0200RecTO.rec02fmtRec.setMaster0co(rskdbio043TO.getInMaster0co());
				pmsp0200RecTO.rec02fmtRec.setModule(rskdbio043TO.getInModule());
				pmsp0200RecTO.rec02fmtRec.setTrans0stat(' ');
				cobolsqlca.setDBAccessStatus(pmsp0200DAO.openPmsp0200TOCsr(KeyType.GREATER_OR_EQ, pmsp0200RecTO.rec02fmtRec.getId02(),
					pmsp0200RecTO.rec02fmtRec.getLocation(), pmsp0200RecTO.rec02fmtRec.getPolicy0num(), pmsp0200RecTO.rec02fmtRec.getSymbol(),
					pmsp0200RecTO.rec02fmtRec.getMaster0co(), pmsp0200RecTO.rec02fmtRec.getModule(), pmsp0200RecTO.rec02fmtRec.getTrans0stat()));
				ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
				if (cobolsqlca.isInvalidKey()) {
					// continue
				}
				pmsp0200TO = pmsp0200DAO.fetchNext(pmsp0200TO);
				pmsp0200RecTO.setData(pmsp0200TO.getData());
				ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
				if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
					// continue
				}
				if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
					wsRead02RecSw = 'N';
					getBcListEntryArr23(wsCount).setBcLocAdd(pmsp0200RecTO.rec02fmtRec.getAdd0line03());
					getBcListEntryArr23(wsCount).setBcLocCity(Functions.subString(pmsp0200RecTO.rec02fmtRec.getAdd0line04(), 1, 28));
					getBcListEntryArr23(wsCount).setBcIssueCode(pmsp0200RecTO.rec02fmtRec.getIssue0code());
				}
			}
		}
	}

	public void readPmsp0200IssueCode() {
		pmsp0200RecTO.rec02fmtRec.setId02("02");
		pmsp0200RecTO.rec02fmtRec.setLocation(rskdbio043TO.getInLocation());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(rskdbio043TO.getInPolicy0num());
		pmsp0200RecTO.rec02fmtRec.setSymbol(rskdbio043TO.getInSymbol());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(rskdbio043TO.getInMaster0co());
		pmsp0200RecTO.rec02fmtRec.setModule(rskdbio043TO.getInModule());
		pmsp0200RecTO.rec02fmtRec.setTrans0stat(' ');
		cobolsqlca.setDBAccessStatus(pmsp0200DAO.openPmsp0200TOCsr(KeyType.GREATER_OR_EQ, pmsp0200RecTO.rec02fmtRec.getId02(),
			pmsp0200RecTO.rec02fmtRec.getLocation(), pmsp0200RecTO.rec02fmtRec.getPolicy0num(), pmsp0200RecTO.rec02fmtRec.getSymbol(),
			pmsp0200RecTO.rec02fmtRec.getMaster0co(), pmsp0200RecTO.rec02fmtRec.getModule(), pmsp0200RecTO.rec02fmtRec.getTrans0stat()));
		ws1FileStatus = Functions.subString(cobolsqlca.getFileIOCode(), 1, 2);
		if (cobolsqlca.isInvalidKey()) {
			// continue
		}
		pmsp0200TO = pmsp0200DAO.fetchNext(pmsp0200TO);
		pmsp0200RecTO.setData(pmsp0200TO.getData());
		ws1FileStatus = Functions.subString(pmsp0200TO.getDBAccessStatus().getFileIOCode(), 1, 2);
		if (pmsp0200TO.getDBAccessStatus().isInvalidKey()) {
			// continue
		}
		if (ws1FileStatus.compareTo(WS_GOOD_RETURN) == 0) {
			getBcListEntryArr23(wsCount).setBcIssueCode(pmsp0200RecTO.rec02fmtRec.getIssue0code());
		}
	}
	
	public void rng2000ReadAsbqcpl1() {
		String retcode = "";
		boolean goto2000ReadAsbqcpl1 = false;
		do {
			goto2000ReadAsbqcpl1 = false;
			retcode = readAsbqcpl1();
		} while (retcode.compareTo("2000-READ-ASBQCPL1") == 0);
		exitAsbqcpl1();
	}

	public void rng5000ProcessEntry() {
		processEntry();
	}

	public void rng7000ReadAsarrel1() {
		readAsarrel1();
	}

	public void rng8000ReadBascltj013() {
		readBascltj013();
	}

	public void rng9000ReadPmsp0200IssueCode() {
		readPmsp0200IssueCode();
	}

	public SPResultSetTO runSP(Rskdbio043TO rskdbio043TO) {
		this.rskdbio043TO = rskdbio043TO;
		run();
		return resultSetTO;
	}

	public SPResultSetTO runSP(byte[] dynamic_inputParameters) {
		rskdbio043TO.setInputParameters(dynamic_inputParameters);
		return runSP(rskdbio043TO);
	}

	public Rskdbio043TO run(Rskdbio043TO rskdbio043TO) {
		this.rskdbio043TO = rskdbio043TO;
		run();
		return this.rskdbio043TO;
	}

	public void run(byte[] dynamic_inputParameters) {
		if (dynamic_inputParameters != null) {
			rskdbio043TO.setInputParameters(dynamic_inputParameters);
		}
		run(rskdbio043TO);
		if (dynamic_inputParameters != null) {
			DataConverter.arrayCopy(rskdbio043TO.getInputParameters(), rskdbio043TO.getInputParametersSize(), dynamic_inputParameters, 1);
		}
	}

	public BcListEntryArr23 getBcListEntryArr23(int index) {
		if (index > bcListEntryArr23.size()) {
			// for an element beyond current array limits, all intermediate
			// items will be added with default elements;
			bcListEntryArr23.ensureCapacity(index);
			for (int i = bcListEntryArr23.size() + 1; i <= index; i++) {
				bcListEntryArr23.append(new BcListEntryArr23());
				// assign default value
			}
		}
		return bcListEntryArr23.get((index) - 1);
	}

	public static Rskdbio043 getInstance() {
		Rskdbio043 iRskdbio043 = ((Rskdbio043) CalledProgList.getInst("RSKDBIO043"));
		if (iRskdbio043 == null) {
			// create an instance if needed
			iRskdbio043 = new Rskdbio043();
			CalledProgList.addInst("RSKDBIO043", ((Object) iRskdbio043));
		}
		return iRskdbio043;
	}

	public void run() {
		try {
			mainSubroutine();
		} catch (ReturnException re) {
			// normal termination of the program
		}
	}

	public static void main(String[] args) {
		getInstance().run();
	}

	public int getBcListTableSize() {
		return 190500;
	}

	public byte[] getBcListTable() {
		byte[] buf = new byte[getBcListTableSize()];
		int offset = 1;
		for (int idx = 1; idx <= bcListEntryArr23.size(); idx++) {
			DataConverter.arrayCopy(bcListEntryArr23.get((idx) - 1).getData(), 1, BcListEntryArr23.getSize(), buf, offset);
			offset += BcListEntryArr23.getSize();
		}
		if (bcListEntryArr23.size() < 500) {
			byte[] rowBuf = DataConverter.fillEndMarker(BcListEntryArr23.getSize());
			for (int idx = bcListEntryArr23.size() + 1; idx <= 500; idx++) {
				DataConverter.arrayCopy(rowBuf, BcListEntryArr23.getSize(), buf, offset);
				offset += BcListEntryArr23.getSize();
			}
		}
		return buf;
	}

	public void initBcListTableSpaces() {
		for (int idx = 1; idx <= bcListEntryArr23.size(); idx++) {
			if (bcListEntryArr23.get((idx) - 1) != null) {
				bcListEntryArr23.get((idx) - 1).initSpaces();
			}
		}
	}

	public String getWsEditMaskCurrFormatted() {
		//FSIT: 152640, Resolution: 55204 - Begin
		//return ABOSystem.convertUsingString(wsEditMaskCurr.clone(), "-ZZZ,ZZZ,ZZ9.99");
		return ABOSystem.convertUsingString(wsEditMaskCurr.clone(), "-ZZZ,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
		//FSIT: 152640, Resolution: 55204 - End
	}
	//FSIT # 119900 Res # 47654 Start	
	
	/*method CallPgmChkNxtRecord is used to call program BASPPRENST which is reading next pending record
	from the file passed to this program as parameter and returns activity type into wsBatype0Act               */
	public void CallPgmChkNxtRecord(){
		Baspprenst baspprenst;
		wsRenewMod=0;
		wsLocationCo=rskdbio043TO.getInLocation();
		wsMasterCo=rskdbio043TO.getInMaster0co();
		wsPolSymbol=rskdbio043TO.getInSymbol();
		wsPolicyNum =rskdbio043TO.getInPolicy0num();
		if(Functions.isNumber(rskdbio043TO.getInModule())){
			wsRenewMod= Integer.valueOf(rskdbio043TO.getInModule()).intValue();
		}
		wsRenewMod= wsRenewMod+1;
		wsPolModule = new Integer(wsRenewMod).toString();
		baspprenst = Baspprenst.getInstance();
		setWsPreRenewalrec(baspprenst.run(getWsPreRenewalrec()));
	}
	
	public void InitializeWsPreRenewalRec(){
		wsLocationCo = "";
		wsMasterCo = "";
		wsPolSymbol = "";
		wsPolicyNum = "";
		wsPolModule = "";
		wsInsuranceLine = ""; 
		wsRiskLocNo = 0; 
		wsRiskLocSubNo = 0;
		wsProduct = ""; 
		wsRateState = "";
		wsUnitNo = 0; 
		wsCoverage = ""; 
		wsCoverageSequence = 0 ; 
		wsDriverId =0; 
		wsFileName = ""; 
		wsBatype0Act = ' ';
	}
	
	public void setWsPreRenewalrec(byte[] buf){
		int offset = 1;
		wsLocationCo  = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsMasterCo = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsPolSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsPolicyNum = DataConverter.readString(buf, offset, 7);
		offset += 7;
		wsPolModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsInsuranceLine = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsRiskLocNo = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsRiskLocSubNo = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsProduct  = DataConverter.readString(buf, offset, 6);
		offset += 6;
		wsRateState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsUnitNo = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsCoverage = DataConverter.readString(buf, offset, 6);
		offset += 6;
		wsCoverageSequence = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsDriverId = DataConverter.readInt(buf, offset, 5);
		offset += 5;
		wsFileName = DataConverter.readString(buf, offset, 10);
		offset += 10;
		wsBatype0Act = DataConverter.readChar(buf, offset);		
	}
	public byte[] getWsPreRenewalrec() {
		byte[] buf = new byte[69];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsLocationCo, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsMasterCo, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsPolSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, wsPolicyNum, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, wsPolModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsInsuranceLine, 3);
		offset += 3;
		DataConverter.writeInt(buf, offset, wsRiskLocNo, 5);
		offset += 1;
		DataConverter.writeInt(buf, offset, wsRiskLocSubNo, 5);
		offset += 1;
		DataConverter.writeString(buf, offset, wsProduct, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, wsRateState, 2);
		offset += 2;
		DataConverter.writeInt(buf, offset, wsUnitNo, 5);
		offset += 1;
		DataConverter.writeString(buf, offset, wsCoverage, 6);
		offset += 6;
		DataConverter.writeInt(buf, offset, wsCoverageSequence, 5);
		offset += 1;
		DataConverter.writeInt(buf, offset, wsDriverId, 5);
		offset += 1;
		DataConverter.writeString(buf, offset, wsFileName, 10);
		offset += 10;
		DataConverter.writeChar(buf, offset, wsBatype0Act);
		return buf;
	}
	//FSIT # 119900 Res # 47654 End
}
