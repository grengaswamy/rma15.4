// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.util.DBAccessStatus;
import bphx.sdf.sql.util.ABOSqlDB2Conversion;

import com.csc.pt.svc.data.to.Bashlgbu00TO;

public class Bashlgbu00DAO {
	public static DBAccessStatus insertRec(Bashlgbu00TO bashlgbu00TO) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASHLGBU00");
			Query query = session.getNamedQuery("Bashlgbu00DAO_insertRec");
			query.setString("bashlgbu00TO_loguser", bashlgbu00TO.getLoguser());
			query.setDate("bashlgbu00TO_logdate", ABOSqlDB2Conversion.dateToSQL(bashlgbu00TO.getLogdate()));
			query.setTime("bashlgbu00TO_logtime", ABOSqlDB2Conversion.timeToSQL(bashlgbu00TO.getLogtime()));
			query.setInteger("bashlgbu00TO_logseqnum", bashlgbu00TO.getLogseqnum());
			query.setString("bashlgbu00TO_location", bashlgbu00TO.getLocation());
			query.setString("bashlgbu00TO_masterco", bashlgbu00TO.getMasterco());
			query.setString("bashlgbu00TO_symbol", bashlgbu00TO.getSymbol());
			query.setString("bashlgbu00TO_policyno", bashlgbu00TO.getPolicyno());
			query.setString("bashlgbu00TO_module", bashlgbu00TO.getModule());
			query.setInteger("bashlgbu00TO_riskloc", bashlgbu00TO.getRiskloc());
			query.setCharacter("bashlgbu00TO_recstatus", bashlgbu00TO.getRecstatus());
			query.setCharacter("bashlgbu00TO_buc7st", bashlgbu00TO.getBuc7st());
			query.setInteger("bashlgbu00TO_buandt", bashlgbu00TO.getBuandt());
			query.setString("bashlgbu00TO_butype0act", bashlgbu00TO.getButype0act());
			query.setInteger("bashlgbu00TO_buafdt", bashlgbu00TO.getBuafdt());
			query.setInteger("bashlgbu00TO_bualdt", bashlgbu00TO.getBualdt());
			query.setString("bashlgbu00TO_bueftx", bashlgbu00TO.getBueftx());
			query.setString("bashlgbu00TO_buegtx", bashlgbu00TO.getBuegtx());
			query.setString("bashlgbu00TO_bueitx", bashlgbu00TO.getBueitx());
			query.setString("bashlgbu00TO_buejtx", bashlgbu00TO.getBuejtx());
			query.setString("bashlgbu00TO_buapnb", bashlgbu00TO.getBuapnb());

			int result = query.executeUpdate();
			status.setNumOfRows(result);

		} catch (org.hibernate.HibernateException ex) {
			status = new DBAccessStatus(ex);
		}
		return status;
	}

	public Bashlgbu00TO readHistoryFIle(String location, String policyno, String symbol, String masterco, String module,
			int riskloc, int lossdate) {
			DBAccessStatus status = new DBAccessStatus();
			Bashlgbu00TO localbashlgbu00TO = null;

		try{
			Session session = HibernateSessionFactory.current().getSession("BASHLGBU00");
			Query query = session.getNamedQuery("Bashlgbu00TO_GETHISTORYRECORDS");
			query.setParameter("location", location);
			query.setParameter("masterco", masterco);
			query.setParameter("symbol", symbol);
			query.setParameter("policyno", policyno);			
			query.setParameter("module", module);
			query.setParameter("riskloc", riskloc);
			query.setParameter("lossdate", lossdate);
			query.setMaxResults(1);
			List resultList = query.list();


			if (resultList != null && resultList.size() > 0) {
				Object obj = resultList.get(0);
				if (obj != null) {
					localbashlgbu00TO = (Bashlgbu00TO)(obj);
					
					status = new DBAccessStatus(DBAccessStatus.DB_OK);
					localbashlgbu00TO.setDBAccessStatus(status);
				} 
				
				
			}
			else status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		 catch (HibernateException ex) {
			status.setException(ex);
		}
		
		return localbashlgbu00TO;
		
	}
}
