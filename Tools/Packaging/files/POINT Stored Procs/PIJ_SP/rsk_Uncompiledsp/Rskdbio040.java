// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk;

import java.math.BigDecimal;

import bphx.c2ab.data.Initialize;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DynamicCall;
import bphx.c2ab.util.DynamicCallException;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.IProcessInfo;
import bphx.c2ab.util.ProcessInfo;
import bphx.c2ab.util.ReturnException;
import bphx.sdf.collections.IABODynamicArray;
import bphx.sdf.collections.creation.ABOCollectionCreator;

import com.csc.pt.svc.clm.Clt0100recRec;
import com.csc.pt.svc.clm.Clt1400recRec;
import com.csc.pt.svc.common.Ws03RtrvMsgWorkArea;
import com.csc.pt.svc.common.Ws04ErrorDetailArea;
import com.csc.pt.svc.common.WsAddlIntsCsrSw;
import com.csc.pt.svc.common.WsCltAddlIntsCsrSw;
import com.csc.pt.svc.common.WsInsCsrSw;
import com.csc.pt.svc.ctrl.Clt0300recRec;
import com.csc.pt.svc.ctrl.Clt1500recRec2;
import com.csc.pt.svc.ctrl.to.Pmsp0200RecTO;
import com.csc.pt.svc.ctrl.to.Pmsp1200RecTO;
import com.csc.pt.svc.custom.common.Cpyerrhrtn;
import com.csc.pt.svc.data.dao.BaspprofitDAO;
import com.csc.pt.svc.data.dao.Bassys1700DAO;
import com.csc.pt.svc.data.dao.Pmsp0200Basclt1400Basclt0100Basclt0300DAO;
import com.csc.pt.svc.data.dao.Pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200DAO;
import com.csc.pt.svc.data.dao.Pmsp0200Basclt1500Basclt0100Basclt0300DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.dao.Pmspmm80DAO;
import com.csc.pt.svc.data.dao.Pmspwc04DAO;
import com.csc.pt.svc.data.dao.Tbd301DAO;
import com.csc.pt.svc.data.to.BaspprofitTO;
import com.csc.pt.svc.data.to.Bassys1700TO;
import com.csc.pt.svc.data.to.Pmsl0004TO;
import com.csc.pt.svc.data.to.Pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO;
import com.csc.pt.svc.data.to.Pmsp0200Basclt1400Basclt0100Basclt0300TO;
import com.csc.pt.svc.data.to.Pmsp0200Basclt1500Basclt0100Basclt0300TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.data.to.Pmspmm80TO;
import com.csc.pt.svc.data.to.Tbd301TO;
import com.csc.pt.svc.db.BcListEntryArr24;
import com.csc.pt.svc.db.SsnIndicator;
import com.csc.pt.svc.isl.Cobolsqlca;
import com.csc.pt.svc.nav.Basl000401;
import com.csc.pt.svc.nav.to.Basl000401TO;
import com.csc.pt.svc.pay.ErrorListRecord;
import com.csc.pt.svc.rsk.dao.Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitDAO;
import com.csc.pt.svc.rsk.to.Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO;
import com.csc.pt.svc.rsk.to.Rskdbio040TO;

/**

*/

public class Rskdbio040 extends ProcessInfo<Rskdbio040TO>  {
	protected Cobolsqlca cobolsqlca = new Cobolsqlca();
	protected Cpyerrhrtn cpyerrhrtn = new Cpyerrhrtn();


	protected Pmsp0200RecTO pmsp0200RecTO = new Pmsp0200RecTO();
	protected Clt1400recRec basclt1400RecTO = new Clt1400recRec();
	protected Clt1500recRec2 basclt1500RecTO = new Clt1500recRec2();
	protected Clt0100recRec basclt0100RecTO =  new Clt0100recRec();
	protected Clt0300recRec basclt0300RecTO = new Clt0300recRec();
	protected Pmsp1200RecTO pmsp1200RecTO = new Pmsp1200RecTO();
	protected BaspprofitTO baspprofitTO = new BaspprofitTO();
	protected Ws03RtrvMsgWorkArea ws03RtrvMsgWorkArea = new Ws03RtrvMsgWorkArea();
	protected ErrorListRecord errorListRecord = new ErrorListRecord();
	protected Ws04ErrorDetailArea ws04ErrorDetailArea = new Ws04ErrorDetailArea();
	protected Pmsp0200Basclt1400Basclt0100Basclt0300DAO pmsp0200Basclt1400Basclt0100Basclt0300DAO = new Pmsp0200Basclt1400Basclt0100Basclt0300DAO();
	protected Pmsp0200Basclt1400Basclt0100Basclt0300TO pmsp0200Basclt1400Basclt0100Basclt0300TO = new Pmsp0200Basclt1400Basclt0100Basclt0300TO();
	protected Pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200DAO pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200DAO = new Pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200DAO();
	protected Pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO = new Pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO();
	protected Pmsp0200Basclt1500Basclt0100Basclt0300DAO pmsp0200Basclt1500Basclt0100Basclt0300DAO = new Pmsp0200Basclt1500Basclt0100Basclt0300DAO();
	protected Pmsp0200Basclt1500Basclt0100Basclt0300TO pmsp0200Basclt1500Basclt0100Basclt0300TO = new Pmsp0200Basclt1500Basclt0100Basclt0300TO();
	protected Pmsl0004TO rec00fmtRec3 = Initialize.initSpaces(new Pmsl0004TO());
	protected Pmspwc04DAO pmspwc04dao =  new Pmspwc04DAO();
	protected int wsCount;
	protected int numOfEntrys;
	protected int numOfEntrysForSql;
	protected String wsReturnCode = "";
	protected static final String GOOD_RETURN_FROM_IO = "0000000";
	protected char wsControlSw = ' ';
	protected String ws1Agency = "";
	protected String wsEffdt = "";
	protected String wsExpdt = "";
	protected int wsCounter;
	protected String holdPreviousType = "";
	protected String wsReadFile = "";
	protected char wsData = ' ';
	protected boolean wsResultSetSw = false;
	protected WsInsCsrSw wsInsCsrSw = new WsInsCsrSw();
	protected WsAddlIntsCsrSw wsAddlIntsCsrSw= new WsAddlIntsCsrSw();
	protected WsCltAddlIntsCsrSw wsCltAddlIntsCsrSw =new WsCltAddlIntsCsrSw();
	protected boolean wsInsFlag = false;
	protected boolean wsAddnFlag = false;
	protected boolean wsWc04Flag = false;
	protected boolean wsPopArray = false;
	protected boolean wsSearchClient = false;
	protected SsnIndicator ssnIndicator = new SsnIndicator();
	protected IABODynamicArray<BcListEntryArr38> bcListEntryArr38 = ABOCollectionCreator.getReferenceFactory(BcListEntryArr38.class).createDynamicArray(1);
	protected char holdBcTrans0stat = ' ';
	protected String holdBcSymbol = "";
	protected String holdBcPolicy0num = "";
	protected String holdBcModule = "";
	protected String holdBcMaster0co = "";
	protected String holdBcLocation = "";
	protected String holdBcEffdt = "";
	protected String holdBcExpdt = "";
	protected String holdBcRisk0state = "";
	protected String holdBcCompany0no = "";
	protected String holdBcAgency = "";
	protected String holdBcLine0bus = "";
	protected String holdBcZipcode = "";
	protected String holdBcLastName = "";
	protected String holdBcFirstName = "";
	protected char holdBcMiddleName = ' ';
	protected String holdBcAdd0line02 = "";
	protected String holdBcAddRln1 = "";
	protected String holdBcCity = "";
	protected String holdBcState = "";
	protected char holdBcRenewal0cd = ' ';
	protected char holdBcIssue0code = ' ';
	protected String holdBcFedTaxId = "";
	protected char holdBcPay0code = ' ';
	protected char holdBcMode0code = ' ';
	protected String holdBcSort0name = "";
	protected String holdBcCust0no = "";
	protected String holdBcSpec0use0a = "";
	protected String holdBcSpec0use0b = "";
	protected String holdBcCanceldate = "";
	protected String holdBcMrsseq = "";
	protected String holdBcGroupNo = "";
	protected String holdBcPhone1 = "";
	protected String holdBcSsn = "";
	protected char holdBcNameType = ' ';
	protected char holdBcNameStatus = ' ';
	protected String holdBcStatusDesc = "";
	protected BigDecimal holdBcTot0ag0prm = BigDecimal.ZERO;
	protected String holdBcType0act = "";


	protected String holdbcori0incept     = "";
	protected String holdbcinsurername   = "";
	protected String holdbcinsureraddress= "";
	protected String holdbcinsurercityst = "";
	protected String holdbcinsurerzip    = "";
	protected long holdbcclientseq      = 0;
	protected int holdbcaddressseq     = 0;
	protected String holdbcbirthdate   = "";


	protected char var1='Q';
	protected char var2='0';
	protected String wsMco = "";
	protected String wsLoc = "";
	protected String wsSymb = "";
	protected String wsPolNo = "";
	protected String wsMod = "";
	protected String wsLastNam = "";
	protected String wsFirstNam = "";
	protected String wsLob = "";
	protected String wsCustNo = "";
	protected String wsPhone = "";
	protected String wsGroupNo = "";
	protected String wsCity = "";
	protected String wsState = "";
	protected String wsZip = "";
	protected String wsSortName = "";
	protected String wsMailing = "";
	protected String wsAgen = "";
	protected String wsAdIntTyp = "";
	protected String wsAdIntNam = "";
	protected String wsLoanNo = "";
	protected String wsLegalAdd = "";
	protected String wsLaCity = "";
	protected String wsLaState = "";
	protected String wsLaZip = "";
	protected String wsMasterCo = "";
	protected String wsLocation = "";
	protected String wsSymbol = "";
	protected String wsModule = "";
	protected String wsPolicyNo = "";
	protected String wsSSN = "";
	protected String wsFein = "";
	protected String userID = "";
	protected String wsCmpny = "";
	protected String wsAgency = "";
	protected String wsSegment = "";
	protected String wsBusUnit = "";
	protected short sqlIoOk;
	protected short sqlNoRecord = ((short) 100);
	protected short sqlInvalidToken = ((short) -104);
	protected short sqlObjectNotFound = ((short) -201);
	protected short sqlCursorAlreadyOpen = ((short) -502);
	protected short sqlRecordExists = ((short) -803);
	protected short sqlObjectLock = ((short) -913);
	protected short sqlTableLock = ((short) -7958);
	protected short sqlTooManyHostVariable = ((short) 326);
	protected String noRecordFound="";
	protected String recordFound="";
	protected String wsMaxModule="";
	protected short ind1;
	protected Pmsp0200DAO pmsp0200DAO = new Pmsp0200DAO();
	protected String var3="03";
	protected char var4='B';
	protected String []tPASearch = null;
	protected String mco1 = null;
	protected int length = 1;
	protected String mco[] = null;
	protected String pco[] = null;
	protected String agentCode[] = null;
	protected String businessSegment[] = null;
	protected String businessUnit[] = null;
	protected boolean isRecordExists = false;
	protected int pos = 0;
	protected String wsPco;
	protected Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitDAO pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitDAO = new Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitDAO();
	protected Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO = new Pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO();
	
	protected Rskdbio040 () {
		this.data = new Rskdbio040TO();

		bcListEntryArr38.assign((1) - 1, new BcListEntryArr38());
	}
	@Override
	protected void mainSubroutine (){
	initialization();
	if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred() || !wsResultSetSw){
		return;
	}
	if (this.data.getInTPASearch().equals("")) {
	prepareSearchVariable();
	openSearchCursor();
	if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
		return;
	}
	processSearchCursor();
	overPara();
		}
		else if(checkBassys1700Switch()){
			tPASearch = this.data.getInTPASearch().split("\\|");
			mco = stringToArr(tPASearch[0],2);
			pco = stringToArr(tPASearch[1],2);
			agentCode = stringToArr(tPASearch[2],7);
			businessSegment = stringToArr(tPASearch[3],8);
			businessUnit = stringToArr(tPASearch[4],10);
			for (int i = 0; i<length;i++)
			{
			prepareSearchVariablesforTPA();
			openSearchCursonforTPA();
			if (isRecordExists) {
				while(!(wsInsCsrSw.isEndOfCursor())){
						fetchTPASearchCursor();
						moveAddlIntData();
						if(!wsPopArray)
						{
							return;
						}
						processEntry();
						checkDuplicatePara();
				}
			}
			}
			overPara();
		}
	
}
	
protected void prepareSearchVariablesforTPA()
{
	wsLoc = this.data.getInLocation();
	wsSymb = this.data.getInSymbol();
	wsPolNo = this.data.getInPolicy0num();
	if(this.data.getInModule().equals("XX")){
		getMaxModule();
	}
	wsMod = this.data.getInModule();
	wsLastNam = this.data.getInLastName().toUpperCase();
	wsFirstNam = this.data.getInFirstName().toUpperCase();
	wsCustNo = this.data.getInCust0no();
	wsSortName = this.data.getInSort0name().toUpperCase();
	wsCity =this.data.getInCity().toUpperCase();
	wsZip = this.data.getInZip0post();
	wsState= this.data.getInState().toUpperCase();
	wsPhone = this.data.getInPhone1();
	wsGroupNo = this.data.getInGroupNo();
	wsLob = this.data.getInLine0bus();
	wsAdIntTyp = this.data.getInAddnlIntType().toUpperCase();
	wsAdIntNam= this.data.getInAddnlIntName().toUpperCase();
	wsLoanNo=  this.data.getInLoanNum();
	wsLegalAdd= this.data.getInLegalAdd().toUpperCase();
	wsLaCity =this.data.getInLaCity().toUpperCase();
	wsLaState = this.data.getInLaState().toUpperCase();
	wsLaZip = this.data.getInLaZip();
	wsMailing = this.data.getInMailIngAdd();
	
	wsMco = mco[pos];
	wsPco = pco[pos];
	wsAgen =  agentCode[pos];
	wsSegment =  businessSegment[pos];
	wsBusUnit = businessUnit[pos];
	pos++;
}

protected void openSearchCursonforTPA()
{
	if(wsReadFile.equalsIgnoreCase("INS-INFO")){
			openPolInsuredInfoCsrforTPA();
			if (!isRecordExists) 
			{	
				return;
			}
	}
}
protected void openPolInsuredInfoCsrforTPA()
{
	wsInsCsrSw.setStartOfCursor();
	if(this.data.getInWildCardPolicySearch().equalsIgnoreCase(KeyType.SWITCH_ON)) {

		cobolsqlca.setDBAccessStatus(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitDAO.openTPASearchCursor(true,wsMco,wsPco,wsAgen,wsSegment.replaceAll("\\s", ""),wsBusUnit.replaceAll("\\s", ""), wsLoc,wsSymb,wsPolNo,wsMod,wsLastNam,
				wsFirstNam,wsLob,wsCustNo,wsZip,wsPhone,wsFein, wsGroupNo, wsCity,wsState,wsSortName,wsMailing, var1, var2, var3, var4, this.data.getInMaxrecords(),this.data.getInWildCardPolicySearch() ));
	} else {

			cobolsqlca.setDBAccessStatus(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitDAO.openTPASearchCursor(false,wsMco,wsPco,wsAgen,wsSegment.replaceAll("\\s", ""),wsBusUnit.replaceAll("\\s", ""), wsLoc,wsSymb,wsPolNo,wsMod,wsLastNam,
					wsFirstNam,wsLob,wsCustNo,wsZip,wsPhone,wsFein, wsGroupNo, wsCity,wsState,wsSortName,wsMailing, var1, var2, var3, var4, this.data.getInMaxrecords(),this.data.getInWildCardPolicySearch() ));
	}

	if (cobolsqlca.getSqlCode() == sqlIoOk) {
		isRecordExists = true;
	} 
	else if (cobolsqlca.getSqlCode() == sqlNoRecord) {
		isRecordExists = false;
	}

}

protected void fetchTPASearchCursor()
{
	pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO = pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitDAO.fetchTPASearchCursor(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO, false);
	
	if(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getDBAccessStatus().isSuccess()) {
		pmsp0200RecTO.rec02fmtRec.setTrans0stat(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getTrans0Stat());
		pmsp0200RecTO.rec02fmtRec.setEff0yr(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getEffYr());
		pmsp0200RecTO.rec02fmtRec.setEff0mo(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getEffMo());
		pmsp0200RecTO.rec02fmtRec.setEff0da(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getEffDa());
		pmsp0200RecTO.rec02fmtRec.setExp0yr(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getExpYr());
		pmsp0200RecTO.rec02fmtRec.setExp0mo(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getExpMo());
		pmsp0200RecTO.rec02fmtRec.setExp0da(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getExpDa());
		pmsp0200RecTO.rec02fmtRec.setRisk0state(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getRisk0state());
		pmsp0200RecTO.rec02fmtRec.setCompany0no(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getCompany0no());
		pmsp0200RecTO.rec02fmtRec.setFillr1(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getFillr1());
		pmsp0200RecTO.rec02fmtRec.setRpt0agt0nr(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getRpt0agt0nr());
		pmsp0200RecTO.rec02fmtRec.setFillr2(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getFillr2());
		pmsp0200RecTO.rec02fmtRec.setFillr3(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getFillr3());
		pmsp0200RecTO.rec02fmtRec.setFillr4(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getFillr4());
		pmsp0200RecTO.rec02fmtRec.setTot0ag0prm(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getTot0ag0prm());
		pmsp0200RecTO.rec02fmtRec.setLine0bus(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getLine0bus());
		pmsp0200RecTO.rec02fmtRec.setIssue0code(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getIssue0code());
		pmsp0200RecTO.rec02fmtRec.setPay0code(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getPay0code());
		pmsp0200RecTO.rec02fmtRec.setMode0code(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getMode0code());
		pmsp0200RecTO.rec02fmtRec.setSort0name(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getSort0name());
		pmsp0200RecTO.rec02fmtRec.setRenewal0cd(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getRenewal0cd());
		pmsp0200RecTO.rec02fmtRec.setCust0no(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getCust0no());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0a(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getSpec0use0a());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0b(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getSpec0use0b());
		pmsp0200RecTO.rec02fmtRec.setZip0post(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getZip0post());
		pmsp0200RecTO.rec02fmtRec.setAdd0line01(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getAdd0line01());
		pmsp0200RecTO.rec02fmtRec.setAdd0line03(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getAdd0line03());
		pmsp0200RecTO.rec02fmtRec.setAdd0line04(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getAdd0line04());
		pmsp0200RecTO.rec02fmtRec.setType0act(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getType0act());
		pmsp0200RecTO.rec02fmtRec.setCanceldate(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getCanceldate());
		pmsp0200RecTO.rec02fmtRec.setMrsseq(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getMrsseq());
		pmsp0200RecTO.rec02fmtRec.setLocation(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getLocation());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getMasterCo());
		pmsp0200RecTO.rec02fmtRec.setSymbol(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getSymbol());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getPolicy0Num());
		pmsp0200RecTO.rec02fmtRec.setModule(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getModule());
		basclt1400RecTO.setCltseqnum(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getCltseqnum());
		basclt1400RecTO.setAddrseqnum(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getAddrseqnum());
		basclt0100RecTO.setGroupno(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getGroupno());
		basclt0100RecTO.setNametype(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getNametype());
		basclt0100RecTO.setNamestatus(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getNamestatus());
		basclt0100RecTO.setClientname(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getClientname());
		basclt0100RecTO.setFirstname(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getFirstname());
		basclt0100RecTO.setMidname(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getMidname());
		basclt0100RecTO.setDba(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getDba());
		basclt0100RecTO.setPhone1(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getPhone1());
		basclt0100RecTO.setFedtaxid(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getFedtaxid());
		basclt0100RecTO.setSsn(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getSsn());
		basclt0300RecTO.setAddrln1(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getAddrln1());
		basclt0300RecTO.setCity(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getCity());
		basclt0300RecTO.setState(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getState());
		basclt0300RecTO.setZipcode(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getZipcode());
	
	    if(wsInsCsrSw.isStartOfCursor()) {
		wsInsCsrSw.setFetchingCursor();
	    }
} else {
	if(pmsp0200Basclt1400Basclt0100Basclt0300BaspprofitTO.getDBAccessStatus().isRecNotFound()) {
		if(wsInsCsrSw.isStartOfCursor()) {
			wsInsCsrSw.setEndOfCursor();
		} else if (wsInsCsrSw.isFetchingCursor()){
			wsInsCsrSw.setEndOfCursor();
		}
	} else {
		ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
		ws04ErrorDetailArea.setWs04ErrParaName("5100-FETCH-POL-INSURED-CURSOR");
		ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
		ws04ErrorDetailArea.setWs04ErrFileName("INS_CURSOR");
		ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		r000PrepErrorListPara();
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}
	
}

protected String[] stringToArr(String tPASearch, int len ) {
	
	int arraylen = tPASearch.length()/len;
	String [] strarray = new String[arraylen];
	int i =0, j=0;
	while (j < arraylen)
	{
		strarray[j] = tPASearch.substring(i, i+len);
		i = i+ len;
		j = j+1;
	}
	length = arraylen;
	return strarray;

}

protected void initialization(){
	if(this.data.getInMaxrecords()<1){
		numOfEntrys = 0;
		wsResultSetSw = false;
	}else{
		wsResultSetSw = true;
	}
	if(this.data.getInMaxrecords()>100){
		this.data.setInMaxrecords((short)100);
	}
	userID = this.data.getInUserid();
	retrieveMm80UserData();
	if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
		return;
	}
	ws03RtrvMsgWorkArea.init();
	errorListRecord.initSpaces();
	ws04ErrorDetailArea.init();
	numOfEntrys = 0;
	ws04ErrorDetailArea.setWs04HoldProgName("RSKDBIO040");
	if(!("").equals(this.data.getInSymbol()) ||!("").equals(this.data.getInPolicy0num()) || !("").equals(this.data.getInModule()) ||
			!("").equals(this.data.getInLastName()) ||!("").equals(this.data.getInFirstName()) ||!("").equals(this.data.getInLine0bus()) ||
			!("").equals(this.data.getInCust0no()) ||!("").equals(this.data.getInPhone1()) ||!("").equals(this.data.getInGroupNo()) ||
			!("").equals(this.data.getInCity()) || !("").equals(this.data.getInState()) ||!("").equals(this.data.getInZip0post()) ||
			!("").equals(this.data.getInSort0name()) || !("").equals(this.data.getInAgency()) || !("").equals(this.data.getInMailIngAdd())){
		wsInsFlag = true;
	}else{
		wsInsFlag = false;
	}

	if(!("").equals(this.data.getInAddnlIntType()) ||!("").equals(this.data.getInAddnlIntName()) ||!("").equals(this.data.getInLegalAdd()) ||!("").equals(this.data.getInLoanNum()) ||
		 	!("").equals(this.data.getInLaCity()) ||!("").equals(this.data.getInLaState()) ||!("").equals(this.data.getInLaZip()) ){
		wsAddnFlag= true;
	}else{
		wsAddnFlag = false;
	}

	if(this.data.getInSsn()!="" ){
		wsWc04Flag = true;
	}else{
		wsWc04Flag = false;
	}

	if((wsInsFlag || wsWc04Flag) && !wsAddnFlag)
		wsReadFile = "INS-INFO";

	if(wsAddnFlag) {
		wsReadFile = "ADDN-INT";
	}

	if(wsWc04Flag && this.data.getInSsnSiteInd() == 'N' && this.data.getInSsnCltInd() == 'Y'){
		wsFein= this.data.getInSsn();
	}else{
		wsFein = "";
	}
}

protected boolean checkBassys1700Switch() {
	Bassys1700TO bassys1700TO = new Bassys1700TO();
	bassys1700TO = Bassys1700DAO.selectByFuncname(bassys1700TO, "HIERARCHY ENHANCEMENT");
	if(bassys1700TO.getSwitchval().equals("Y")){
		return true;
	}else
	{
		return false;
	}
}

protected void retrieveMm80UserData(){
	Pmspmm80TO pmspmm80TO= new Pmspmm80TO();
	pmspmm80TO = Pmspmm80DAO.selectByUsrprf(pmspmm80TO, userID);
	cobolsqlca.setDBAccessStatus(pmspmm80TO.getDBAccessStatus());
	if (!cobolsqlca.isEOF()) {
		wsCmpny= pmspmm80TO.getCompany0no();
		wsAgency = pmspmm80TO.getAgnmnbr();
	}
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
	}else {
		ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
		ws04ErrorDetailArea.setWs04ErrErrorTyp('S');
		ws04ErrorDetailArea.setWs04ErrSqlDtastgDta(userID);
		ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		ws04ErrorDetailArea.setWs04ErrParaName("1100-RETRIEVE-MM80-USER-DATA");
		ws04ErrorDetailArea.setWs04ErrFileName("PMSPMM80");
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}

protected void prepareSearchVariable() {
	wsMco = this.data.getInMaster0co();
	Functions.replaceAll(wsMco, " ", "%");
	wsLoc = this.data.getInLocation();
	Functions.replaceAll(wsLoc, " ", "%");
	wsSymb = this.data.getInSymbol();
	Functions.replaceAll(wsSymb, " ", "%");
	wsPolNo = this.data.getInPolicy0num();
	Functions.replaceAll(wsPolNo, " ", "%");
	//If the GO button is clicked on and the Module is left blank.
	if(this.data.getInModule().equals("XX")){
		getMaxModule();
	}
	wsMod = this.data.getInModule();
	Functions.replaceAll(wsMod, " ", "%");
	wsLastNam = this.data.getInLastName().toUpperCase();
	Functions.replaceAll(wsLastNam, " ", "%");
	wsFirstNam = this.data.getInFirstName().toUpperCase();
	Functions.replaceAll(wsFirstNam, " ", "%");
	wsCustNo = this.data.getInCust0no();
	Functions.replaceAll(wsCustNo, " ", "%");
	wsSortName = this.data.getInSort0name().toUpperCase();
	Functions.replaceAll(wsSortName, " ", "%");
	wsCity =this.data.getInCity().toUpperCase();
	Functions.replaceAll(wsCity, " ", "%");
	wsZip = this.data.getInZip0post();
	Functions.replaceAll(wsZip, " ", "%");
	wsState= this.data.getInState().toUpperCase();
	Functions.replaceAll(wsState, " ", "%");
	wsAgen = this.data.getInAgency();
	Functions.replaceAll(wsAgen, " ", "%");
	wsPhone = this.data.getInPhone1();
	Functions.replaceAll(wsPhone, " ", "%");
	wsGroupNo = this.data.getInGroupNo();
	Functions.replaceAll(wsGroupNo, " ", "%");
	Functions.replaceAll(wsFein, " ", "%");
	wsLob = this.data.getInLine0bus();
	Functions.replaceAll(wsLob, " ", "%");
	wsAdIntTyp = this.data.getInAddnlIntType().toUpperCase();
	Functions.replaceAll(wsAdIntTyp, " ", "%");
	wsAdIntNam= this.data.getInAddnlIntName().toUpperCase();
	Functions.replaceAll(wsAdIntNam, " ", "%");
	wsLoanNo=  this.data.getInLoanNum();
	Functions.replaceAll(wsLoanNo, " ", "%");
	wsLegalAdd= this.data.getInLegalAdd().toUpperCase();
	Functions.replaceAll(wsLegalAdd, " ", "%");
	wsLaCity =this.data.getInLaCity().toUpperCase();
	Functions.replaceAll(wsLaCity, " ", "%");
	wsLaState = this.data.getInLaState().toUpperCase();
	Functions.replaceAll(wsLaState, " ", "%");
	wsLaZip = this.data.getInLaZip();
	Functions.replaceAll(wsLaZip, " ", "%");
	wsMailing = this.data.getInMailIngAdd();
	Functions.replaceAll(wsMailing, " ", "%");
	if(this.data.getInLegalAdd()=="" || this.data.getInLaCity() =="" || this.data.getInLaState() =="" || this.data.getInLaZip() ==""){
		wsAdIntTyp ="LA";
		Functions.replaceAll(wsAdIntTyp, " ", "%");
		wsSearchClient = true;
	}
}

protected void openSearchCursor(){
	if(wsReadFile.equalsIgnoreCase("INS-INFO")){
		openPolInsuredInfoCsr();
		if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
			return;
		}else{
			if(cobolsqlca.getSqlCode() == sqlCursorAlreadyOpen){
				closePolInsuredCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()) {
					return;
				}
				openPolInsuredInfoCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()) {
					return;
				}
			} else {
				return;
			}
		}
	}
	if(wsReadFile.equalsIgnoreCase("ADDN-INT")){
		openAddlIntsInfoCsr();
		if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
			return;
		}else{
			if(cobolsqlca.getSqlCode() == sqlCursorAlreadyOpen){
				closeAddlIntsInfoCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()) {
					return;
				}
				openAddlIntsInfoCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()) {
					return;
				}
			}else{
				return;
			}
		}
	}
	if(wsReadFile.equalsIgnoreCase("CLT-AI")){
		openCltAddlIntInfCsr();
		if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
			return;
		}else{
			if(cobolsqlca.getSqlCode() == sqlCursorAlreadyOpen){
				closeCltAddIntInfCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()) {
					return;
				}
				openCltAddlIntInfCsr();
				if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()) {
					return;
				}
			}else{
				return;
			}
		}
	}
}

protected void openPolInsuredInfoCsr(){
	wsInsCsrSw.setStartOfCursor();
	pmsp0200RecTO.rec02fmtRec.setLocation(wsLoc);
	pmsp0200RecTO.rec02fmtRec.setMaster0co(wsMco);
	pmsp0200RecTO.rec02fmtRec.setSymbol(wsSymb);
	pmsp0200RecTO.rec02fmtRec.setPolicy0num(wsPolNo);
	pmsp0200RecTO.rec02fmtRec.setModule(wsMod);
	if(this.data.getInWildCardPolicySearch().equalsIgnoreCase(KeyType.SWITCH_ON)) {

		cobolsqlca.setDBAccessStatus(pmsp0200Basclt1400Basclt0100Basclt0300DAO.openFeinCursor(true,wsLoc,wsMco,wsSymb,wsPolNo,wsMod,wsLastNam,wsFirstNam,wsLob,

		wsCustNo,wsZip,wsPhone,wsFein, wsGroupNo, wsCity,wsState,wsSortName,wsMailing,wsAgen, var1, var2, var3, var4, this.data.getTypeAct(),"",false, this.data.getInMaxrecords(),this.data.getInWildCardPolicySearch() ));
	} else {


		cobolsqlca.setDBAccessStatus(pmsp0200Basclt1400Basclt0100Basclt0300DAO.openFeinCursor(false,wsLoc,wsMco,wsSymb,wsPolNo,wsMod,wsLastNam,wsFirstNam,wsLob,
 		wsCustNo,wsZip,wsPhone,wsFein, wsGroupNo, wsCity,wsState,wsSortName,wsMailing,wsAgen, var1, var2, var3, var4, this.data.getTypeAct(),"",false, this.data.getInMaxrecords(),this.data.getInWildCardPolicySearch()));
	}
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
	} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.setWs04ErrParaName("3100-OPEN-POL-INSURED-INFO-CSR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("INS_CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			r000PrepErrorListPara();
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}

protected void r000PrepErrorListPara() {
	ws04ErrorDetailArea.setWs04ErrSqlDtastgDta(
			Functions.concat(
			ws04ErrorDetailArea.getWs04ErrSqlDtastgDta(), 1000,
			Functions.trimAfter(wsSymb, "%"), new String[] { "; ",
			Functions.trimAfter(wsPolNo, "%"), "; ",
			Functions.trimAfter(wsMod, "%"), "; ",
			Functions.trimAfter(wsLastNam, "%"), "; ",
			Functions.trimAfter(wsFirstNam, "%"), "; ",
			Functions.trimAfter(wsLob, "%"), "; ",
			Functions.trimAfter(wsCustNo, "%"), "; ",
			Functions.trimAfter(wsPhone, "%"), "; ",
			Functions.trimAfter(wsGroupNo, "%"), "; ",
			Functions.trimAfter(wsCity, "%"), "; ",
			Functions.trimAfter(wsState, "%"), "; ",
			Functions.trimAfter(wsZip, "%"), "; ",
			Functions.trimAfter(wsSortName, "%"), "; ",
			Functions.trimAfter(wsMailing, "%"), "; ",
			Functions.trimAfter(wsAgen, "%"), "; ",
			Functions.trimAfter(String.valueOf(var1), "%"), "; ",
			Functions.trimAfter(String.valueOf(var2), "%"), "; "
			}));
}


protected void openAddlIntsInfoCsr(){
		wsAddlIntsCsrSw.setStartOfCursor();
		if (this.data.getInWildCardPolicySearch().equalsIgnoreCase(KeyType.SWITCH_ON)) {
			cobolsqlca.setDBAccessStatus(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200DAO.openAddIntCursor(true, wsMco, wsLoc, wsSymb, wsPolNo, wsMod, wsLastNam,
					wsFirstNam,wsCustNo,wsSortName,	wsPhone,wsFein,wsGroupNo,wsLob, wsMailing,wsCity ,wsState,wsZip,wsAdIntTyp, wsAdIntNam ,wsLoanNo,
				wsLegalAdd,wsLaCity, wsLaState, wsLaZip, wsAgen,var1, var2, var3, var4));
			cobolsqlca.setDBAccessStatus(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200DAO.openAddIntCursor(false, wsMco, wsLoc, wsSymb, wsPolNo, wsMod, wsLastNam,
					wsFirstNam,wsCustNo,wsSortName,	wsPhone,wsFein,wsGroupNo,wsLob, wsMailing,wsCity ,wsState,wsZip,wsAdIntTyp, wsAdIntNam ,wsLoanNo,
				wsLegalAdd,wsLaCity, wsLaState, wsLaZip, wsAgen,var1, var2, var3, var4));
		}
		if (cobolsqlca.getSqlCode() == sqlIoOk) {
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.setWs04ErrParaName("3300-OPEN-ADDL-INTS-INFO-CSR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("ADDINT_CURSOR");
			ws04ErrorDetailArea.setWs04ErrSqlDtastgDta("Search Join Cursor");
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			r000PrepErrorListPara();
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
}

protected void openCltAddlIntInfCsr(){
	wsCltAddlIntsCsrSw.setStartOfCursor();
	cobolsqlca.setDBAccessStatus(pmsp0200Basclt1500Basclt0100Basclt0300DAO.openAddIntCltCursor(wsMco,wsLoc,
			wsSymb,wsPolNo,wsMod,wsLastNam,wsFirstNam, wsCustNo,wsSortName,wsPhone,wsFein,
			wsGroupNo, wsLob, wsAdIntTyp,wsAdIntNam, wsLegalAdd, wsLaCity,wsLaState,wsLaZip,wsMailing,
			wsAgen, var1, var2, var3, var4));
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
	} else {
		ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
		ws04ErrorDetailArea.setWs04ErrParaName("3500-OPEN-CLT-ADDL-INT-INF-CSR");
		ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
		ws04ErrorDetailArea.setWs04ErrFileName("ADDINTCLT_CURSOR");
		ws04ErrorDetailArea.setWs04ErrSqlDtastgDta("Search Join Cursor");
		ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		r000PrepErrorListPara();
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}

protected void closePolInsuredCsr(){
	cobolsqlca.setDBAccessStatus(pmsp0200Basclt1400Basclt0100Basclt0300DAO.closeCsr());
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
	} else {
		ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
		ws04ErrorDetailArea.setWs04ErrParaName("3200-CLOSE-POL-INSURED-CSR");
		ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}

protected void closeAddlIntsInfoCsr(){

	cobolsqlca.setDBAccessStatus(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200DAO.closeCsr());
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
	} else {
		ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
		ws04ErrorDetailArea.setWs04ErrParaName("3400-CLOSE-ADDL-INTS-INFO-CSR");
		ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}

protected void closeCltAddIntInfCsr(){
	cobolsqlca.setDBAccessStatus(pmsp0200Basclt1500Basclt0100Basclt0300DAO.closeCsr());
	if (cobolsqlca.getSqlCode() == sqlIoOk) {
	} else {
		ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
		ws04ErrorDetailArea.setWs04ErrParaName("3600-CLOSE-CLT-ADD-INT-INF-CSR");
		ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
		ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
		cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
		cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
		cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
		cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
		ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
		ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
		errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
	}
}


protected void processSearchCursor(){
	if(wsReadFile.equalsIgnoreCase("INS-INFO")){
		while(!(wsInsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())){
			processPolInsuredCsr();
		}
	}

	if(wsReadFile.equalsIgnoreCase("ADDN-INT")){
		while(!(wsAddlIntsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())){
			processAddlIntsInfCsr();
		}
		if(wsSearchClient && !this.data.getInAddnlIntName().trim().equalsIgnoreCase("")){
			wsReadFile = "CLT-AI";
		}
	}

	if(wsReadFile.equalsIgnoreCase("CLT-AI")){
		openSearchCursor();
		if(ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
			return;
		}
		while(!(wsCltAddlIntsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())){
			processCltAddlIntCsr();
		}
   }
}

protected void processPolInsuredCsr() {
	fetchPolInsuredCsr();
	if(wsInsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
		return;
	}
	movePolInsAiData();
	if(wsPopArray){
	} else {
		return;
	}
	processEntry();
	checkDuplicatePara();
}

protected void fetchPolInsuredCsr(){
	pmsp0200Basclt1400Basclt0100Basclt0300TO = pmsp0200Basclt1400Basclt0100Basclt0300DAO.fetchFeinCursor(pmsp0200Basclt1400Basclt0100Basclt0300TO, false);
	if(pmsp0200Basclt1400Basclt0100Basclt0300TO.getDBAccessStatus().isSuccess()) {
		pmsp0200RecTO.rec02fmtRec.setTrans0stat(pmsp0200Basclt1400Basclt0100Basclt0300TO.getTrans0Stat());
		pmsp0200RecTO.rec02fmtRec.setEff0yr(pmsp0200Basclt1400Basclt0100Basclt0300TO.getEffYr());
		pmsp0200RecTO.rec02fmtRec.setEff0mo(pmsp0200Basclt1400Basclt0100Basclt0300TO.getEffMo());
		pmsp0200RecTO.rec02fmtRec.setEff0da(pmsp0200Basclt1400Basclt0100Basclt0300TO.getEffDa());
		pmsp0200RecTO.rec02fmtRec.setExp0yr(pmsp0200Basclt1400Basclt0100Basclt0300TO.getExpYr());
		pmsp0200RecTO.rec02fmtRec.setExp0mo(pmsp0200Basclt1400Basclt0100Basclt0300TO.getExpMo());
		pmsp0200RecTO.rec02fmtRec.setExp0da(pmsp0200Basclt1400Basclt0100Basclt0300TO.getExpDa());
		pmsp0200RecTO.rec02fmtRec.setRisk0state(pmsp0200Basclt1400Basclt0100Basclt0300TO.getRisk0state());
		pmsp0200RecTO.rec02fmtRec.setCompany0no(pmsp0200Basclt1400Basclt0100Basclt0300TO.getCompany0no());
		pmsp0200RecTO.rec02fmtRec.setFillr1(pmsp0200Basclt1400Basclt0100Basclt0300TO.getFillr1());
		pmsp0200RecTO.rec02fmtRec.setRpt0agt0nr(pmsp0200Basclt1400Basclt0100Basclt0300TO.getRpt0agt0nr());
		pmsp0200RecTO.rec02fmtRec.setFillr2(pmsp0200Basclt1400Basclt0100Basclt0300TO.getFillr2());
		pmsp0200RecTO.rec02fmtRec.setFillr3(pmsp0200Basclt1400Basclt0100Basclt0300TO.getFillr3());
		pmsp0200RecTO.rec02fmtRec.setFillr4(pmsp0200Basclt1400Basclt0100Basclt0300TO.getFillr4());
		pmsp0200RecTO.rec02fmtRec.setTot0ag0prm(pmsp0200Basclt1400Basclt0100Basclt0300TO.getTot0ag0prm());
		pmsp0200RecTO.rec02fmtRec.setLine0bus(pmsp0200Basclt1400Basclt0100Basclt0300TO.getLine0bus());
		pmsp0200RecTO.rec02fmtRec.setIssue0code(pmsp0200Basclt1400Basclt0100Basclt0300TO.getIssue0code());
		pmsp0200RecTO.rec02fmtRec.setPay0code(pmsp0200Basclt1400Basclt0100Basclt0300TO.getPay0code());
		pmsp0200RecTO.rec02fmtRec.setMode0code(pmsp0200Basclt1400Basclt0100Basclt0300TO.getMode0code());
		pmsp0200RecTO.rec02fmtRec.setSort0name(pmsp0200Basclt1400Basclt0100Basclt0300TO.getSort0name());
		pmsp0200RecTO.rec02fmtRec.setRenewal0cd(pmsp0200Basclt1400Basclt0100Basclt0300TO.getRenewal0cd());
		pmsp0200RecTO.rec02fmtRec.setCust0no(pmsp0200Basclt1400Basclt0100Basclt0300TO.getCust0no());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0a(pmsp0200Basclt1400Basclt0100Basclt0300TO.getSpec0use0a());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0b(pmsp0200Basclt1400Basclt0100Basclt0300TO.getSpec0use0b());
		pmsp0200RecTO.rec02fmtRec.setZip0post(pmsp0200Basclt1400Basclt0100Basclt0300TO.getZip0post());
		pmsp0200RecTO.rec02fmtRec.setAdd0line01(pmsp0200Basclt1400Basclt0100Basclt0300TO.getAdd0line01());
		pmsp0200RecTO.rec02fmtRec.setAdd0line03(pmsp0200Basclt1400Basclt0100Basclt0300TO.getAdd0line03());
		pmsp0200RecTO.rec02fmtRec.setAdd0line04(pmsp0200Basclt1400Basclt0100Basclt0300TO.getAdd0line04());
		pmsp0200RecTO.rec02fmtRec.setType0act(pmsp0200Basclt1400Basclt0100Basclt0300TO.getType0act());
		pmsp0200RecTO.rec02fmtRec.setCanceldate(pmsp0200Basclt1400Basclt0100Basclt0300TO.getCanceldate());
		pmsp0200RecTO.rec02fmtRec.setMrsseq(pmsp0200Basclt1400Basclt0100Basclt0300TO.getMrsseq());
		pmsp0200RecTO.rec02fmtRec.setLocation(pmsp0200Basclt1400Basclt0100Basclt0300TO.getLocation());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(pmsp0200Basclt1400Basclt0100Basclt0300TO.getMasterCo());
		pmsp0200RecTO.rec02fmtRec.setSymbol(pmsp0200Basclt1400Basclt0100Basclt0300TO.getSymbol());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(pmsp0200Basclt1400Basclt0100Basclt0300TO.getPolicy0Num());
		pmsp0200RecTO.rec02fmtRec.setModule(pmsp0200Basclt1400Basclt0100Basclt0300TO.getModule());
		basclt1400RecTO.setCltseqnum(pmsp0200Basclt1400Basclt0100Basclt0300TO.getCltseqnum());
		basclt1400RecTO.setAddrseqnum(pmsp0200Basclt1400Basclt0100Basclt0300TO.getAddrseqnum());
		basclt0100RecTO.setGroupno(pmsp0200Basclt1400Basclt0100Basclt0300TO.getGroupno());
		basclt0100RecTO.setNametype(pmsp0200Basclt1400Basclt0100Basclt0300TO.getNametype());
		basclt0100RecTO.setNamestatus(pmsp0200Basclt1400Basclt0100Basclt0300TO.getNamestatus());
		basclt0100RecTO.setClientname(pmsp0200Basclt1400Basclt0100Basclt0300TO.getClientname());
		basclt0100RecTO.setFirstname(pmsp0200Basclt1400Basclt0100Basclt0300TO.getFirstname());
		basclt0100RecTO.setMidname(pmsp0200Basclt1400Basclt0100Basclt0300TO.getMidname());
		basclt0100RecTO.setDba(pmsp0200Basclt1400Basclt0100Basclt0300TO.getDba());
		basclt0100RecTO.setPhone1(pmsp0200Basclt1400Basclt0100Basclt0300TO.getPhone1());
		basclt0100RecTO.setFedtaxid(pmsp0200Basclt1400Basclt0100Basclt0300TO.getFedtaxid());
		basclt0100RecTO.setSsn(pmsp0200Basclt1400Basclt0100Basclt0300TO.getSsn());
		basclt0300RecTO.setAddrln1(pmsp0200Basclt1400Basclt0100Basclt0300TO.getAddrln1());
		basclt0300RecTO.setCity(pmsp0200Basclt1400Basclt0100Basclt0300TO.getCity());
		basclt0300RecTO.setState(pmsp0200Basclt1400Basclt0100Basclt0300TO.getState());
		basclt0300RecTO.setZipcode(pmsp0200Basclt1400Basclt0100Basclt0300TO.getZipcode());
		if(wsInsCsrSw.isStartOfCursor()) {
			wsInsCsrSw.setFetchingCursor();
		}
	} else {
		if(pmsp0200Basclt1400Basclt0100Basclt0300TO.getDBAccessStatus().isRecNotFound()) {
			if(wsInsCsrSw.isStartOfCursor()) {
				wsInsCsrSw.setEndOfCursor();
			} else if (wsInsCsrSw.isFetchingCursor()){
				wsInsCsrSw.setEndOfCursor();
			}
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.setWs04ErrParaName("5100-FETCH-POL-INSURED-CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("INS_CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			r000PrepErrorListPara();
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
	}
}

protected void movePolInsAiData(){
	wsEffdt = Functions.concat(wsEffdt, 1000,
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getEff0yr(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getEff0mo(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getEff0da(), " "));

	wsExpdt = Functions.concat(wsExpdt, 1000,
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getExp0yr(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getExp0mo(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getExp0da(), " "));

	ws1Agency = Functions.concat(ws1Agency, 1000,
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getFillr1(), " "),
		Functions.trimAfter(Character.toString(pmsp0200RecTO.rec02fmtRec.getRpt0agt0nr()), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getFillr2(), " "));

	if(!wsCmpny.equalsIgnoreCase("")){
		if(wsCmpny.equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getMaster0co())){
		}else{
			wsPopArray =false; return;

		}
	}
	if(!wsAgency.equalsIgnoreCase("")){
		if(wsAgency.equalsIgnoreCase(ws1Agency)){
		}else{
			wsPopArray =false; return;

		}
	}

	if(!this.data.getInSymbol().equalsIgnoreCase("")){
		if( !(this.data.getInSymbol().equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getSymbol()))){
			wsPopArray =false; return;
		}
	}

	if(!this.data.getInLine0bus().equalsIgnoreCase("")){
		if(!(this.data.getInLine0bus().equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getLine0bus()))){
			wsPopArray =false; return;
		}
	}

	//RSK - Start

	if(!this.data.getInLossdte().equals("") && !this.data.getInLossdte().equals("0000000")){
		if(this.data.getInLossdte().compareTo(wsEffdt) < 0 || this.data.getInLossdte().compareTo(wsExpdt) > 0 ){
			wsPopArray = false;
			return;
		}
	}
	//RSK - End
	if (!pmsp0200RecTO.rec02fmtRec.getCanceldate().equals("") && !pmsp0200RecTO.rec02fmtRec.getCanceldate().equals("0000000")) {
		if(this.data.getInLossdte().compareTo(pmsp0200RecTO.rec02fmtRec.getCanceldate()) > 0 ){
			wsPopArray = false;
			return;
		}
	}
	
	if(!this.data.getInSsn().equalsIgnoreCase(""))
	{
		if(this.data.getInSsnSiteInd() ==  'N' && this.data.getInSsnCltInd() == 'Y')
		{
			if(this.data.getInSsn().equalsIgnoreCase(basclt0100RecTO.getSsn())){
			} else {
				wsPopArray =false; return;
			}
		}
		if(this.data.getInSsnSiteInd() ==  'Y' && this.data.getInSsnCltInd() == 'N'){
			wsSSN = this.data.getInSsn();
			wsSymbol= pmsp0200RecTO.rec02fmtRec.getSymbol();
			wsPolicyNo=pmsp0200RecTO.rec02fmtRec.getPolicy0num();
			wsModule= pmsp0200RecTO.rec02fmtRec.getModule();
			wsMasterCo =pmsp0200RecTO.rec02fmtRec.getMaster0co();
			wsLocation = pmsp0200RecTO.rec02fmtRec.getLocation();
			readPmspwc04Para();
			if(ssnIndicator.isSsnNotFound() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
				wsPopArray =false; return;
			}
		}


		if(this.data.getInSsnSiteInd() ==  'Y' && this.data.getInSsnCltInd() == 'Y'){
			if(this.data.getInSsn().equalsIgnoreCase(basclt0100RecTO.getSsn())){
				ssnIndicator.setValue('Y');
			}else{
				ssnIndicator.setValue('N');
			}
			if(ssnIndicator.isSsnNotFound()){
				wsSSN = this.data.getInSsn();
				wsSymbol= pmsp0200RecTO.rec02fmtRec.getSymbol();
				wsPolicyNo=pmsp0200RecTO.rec02fmtRec.getPolicy0num();
				wsModule= pmsp0200RecTO.rec02fmtRec.getModule();
				wsMasterCo =pmsp0200RecTO.rec02fmtRec.getMaster0co();
				wsLocation = pmsp0200RecTO.rec02fmtRec.getLocation();
				readPmspwc04Para();
				if(ssnIndicator.isSsnNotFound() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
					wsPopArray =false; return;
				}
			}
		}
	}
		wsPopArray = true;
		holdBcTrans0stat = pmsp0200RecTO.rec02fmtRec.getTrans0stat();
		holdBcSymbol = pmsp0200RecTO.rec02fmtRec.getSymbol();
		holdBcPolicy0num = pmsp0200RecTO.rec02fmtRec.getPolicy0num();
		holdBcModule = pmsp0200RecTO.rec02fmtRec.getModule();
		holdbcclientseq = basclt1400RecTO.getCltseqnum();
		holdbcaddressseq = basclt1400RecTO.getAddrseqnum();
		holdbcori0incept =  Integer.toString(pmsp0200RecTO.rec02fmtRec.getOri0incept());
		holdBcMaster0co = pmsp0200RecTO.rec02fmtRec.getMaster0co();
		holdBcLocation = pmsp0200RecTO.rec02fmtRec.getLocation();
		holdBcEffdt = wsEffdt;
		holdBcExpdt = wsExpdt;
		holdBcRisk0state = pmsp0200RecTO.rec02fmtRec.getRisk0state();
		holdBcCompany0no = pmsp0200RecTO.rec02fmtRec.getCompany0no();
		holdBcAgency = ws1Agency;
		holdBcLine0bus = pmsp0200RecTO.rec02fmtRec.getLine0bus();
		holdBcRenewal0cd = pmsp0200RecTO.rec02fmtRec.getRenewal0cd();
		holdBcIssue0code = pmsp0200RecTO.rec02fmtRec.getIssue0code();
		holdBcPay0code = pmsp0200RecTO.rec02fmtRec.getPay0code();
		holdBcMode0code = pmsp0200RecTO.rec02fmtRec.getMode0code();
		holdBcSort0name = pmsp0200RecTO.rec02fmtRec.getSort0name();
		holdBcCust0no = pmsp0200RecTO.rec02fmtRec.getCust0no();
		holdBcSpec0use0a =pmsp0200RecTO.rec02fmtRec.getSpec0use0a();
		holdBcSpec0use0b =pmsp0200RecTO.rec02fmtRec.getSpec0use0b();
		if(holdBcRenewal0cd != '9'){
			holdBcCanceldate = "";
		}else{
			holdBcCanceldate = pmsp0200RecTO.rec02fmtRec.getCanceldate();
		}
		holdBcMrsseq = pmsp0200RecTO.rec02fmtRec.getMrsseq();
		holdBcTot0ag0prm = pmsp0200RecTO.rec02fmtRec.getTot0ag0prm();
		holdBcType0act = pmsp0200RecTO.rec02fmtRec.getType0act();
		holdBcLastName = basclt0100RecTO.getClientname();
		holdBcFirstName = basclt0100RecTO.getFirstname();
		if(basclt0100RecTO.getMidname() != null && (!("").equals(basclt0100RecTO.getMidname().trim()))) {
			holdBcMiddleName = basclt0100RecTO.getMidname().trim().charAt(0);
		} else {
			holdBcMiddleName = ' ';
		}
		holdBcFedTaxId = basclt0100RecTO.getFedtaxid();
		holdBcGroupNo = basclt0100RecTO.getGroupno();
		holdBcPhone1 = basclt0100RecTO.getPhone1();
		holdBcSsn = basclt0100RecTO.getSsn();
		holdBcNameType = basclt0100RecTO.getNametype();
		holdBcNameStatus = basclt0100RecTO.getNamestatus();
		holdBcAdd0line02 = basclt0100RecTO.getDba();
		holdBcAddRln1 = basclt0300RecTO.getAddrln1();
		holdBcCity = basclt0300RecTO.getCity();
		holdBcState = basclt0300RecTO.getState();
		holdBcZipcode =basclt0300RecTO.getZipcode();
		holdbcbirthdate = basclt0100RecTO.getBirthdate().toString();
		if(numOfEntrys == this.data.getInMaxrecords()){
			wsInsCsrSw.setEndOfCursor();
			wsAddlIntsCsrSw.setEndOfCursor();
		}
}

protected void processAddlIntsInfCsr()
{
	fetchAddlIntCsr();
	if(wsAddlIntsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())
	{
		return;
	}
	movePolInsAiData();
	if(wsPopArray && ! wsAddlIntsCsrSw.isEndOfCursor())
	{
	} else {
		return;
	}
	processEntry();
	checkDuplicatePara();
}

protected void fetchAddlIntCsr()
{
	pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO = pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200DAO.fetchAddIntCursor(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO);
	if(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getDBAccessStatus().isSuccess()) {
		pmsp0200RecTO.rec02fmtRec.setTrans0stat(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getTrans0Stat());
		pmsp0200RecTO.rec02fmtRec.setEff0yr(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getEffYr());
		pmsp0200RecTO.rec02fmtRec.setEff0mo(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getEffMo());
		pmsp0200RecTO.rec02fmtRec.setEff0da(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getEffDa());
		pmsp0200RecTO.rec02fmtRec.setExp0yr(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getExpYr());
		pmsp0200RecTO.rec02fmtRec.setExp0mo(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getExpMo());
		pmsp0200RecTO.rec02fmtRec.setExp0da(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getExpDa());
		pmsp0200RecTO.rec02fmtRec.setRisk0state(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getRisk0state());
		pmsp0200RecTO.rec02fmtRec.setCompany0no(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getCompany0no());
		pmsp0200RecTO.rec02fmtRec.setFillr1(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getFillr1());
		pmsp0200RecTO.rec02fmtRec.setRpt0agt0nr(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getRpt0agt0nr());
		pmsp0200RecTO.rec02fmtRec.setFillr2(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getFillr2());
		pmsp0200RecTO.rec02fmtRec.setFillr3(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getFillr3());
		pmsp0200RecTO.rec02fmtRec.setFillr4(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getFillr4());
		pmsp0200RecTO.rec02fmtRec.setTot0ag0prm(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getTot0ag0prm());
		pmsp0200RecTO.rec02fmtRec.setLine0bus(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getLine0bus());
		pmsp0200RecTO.rec02fmtRec.setIssue0code(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getIssue0code());
		pmsp0200RecTO.rec02fmtRec.setPay0code(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getPay0code());
		pmsp0200RecTO.rec02fmtRec.setMode0code(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getMode0code());
		pmsp0200RecTO.rec02fmtRec.setSort0name(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getSort0name());
		pmsp0200RecTO.rec02fmtRec.setRenewal0cd(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getRenewal0cd());
		pmsp0200RecTO.rec02fmtRec.setCust0no(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getCust0no());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0a(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getSpec0use0a());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0b(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getSpec0use0b());
		pmsp0200RecTO.rec02fmtRec.setZip0post(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getZip0post());
		pmsp0200RecTO.rec02fmtRec.setAdd0line01(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getAdd0line01());
		pmsp0200RecTO.rec02fmtRec.setAdd0line03(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getAdd0line03());
		pmsp0200RecTO.rec02fmtRec.setAdd0line04(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getAdd0line04());
		pmsp0200RecTO.rec02fmtRec.setType0act(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getType0act());
		pmsp0200RecTO.rec02fmtRec.setCanceldate(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getCanceldate());
		pmsp0200RecTO.rec02fmtRec.setMrsseq(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getMrsseq());
		pmsp0200RecTO.rec02fmtRec.setLocation(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getLocation());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getMasterCo());
		pmsp0200RecTO.rec02fmtRec.setSymbol(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getSymbol());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getPolicy0Num());
		pmsp0200RecTO.rec02fmtRec.setModule(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getModule());
		basclt1400RecTO.setCltseqnum(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getCltseqnum());
		basclt1400RecTO.setAddrseqnum(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getAddrseqnum());
		basclt0100RecTO.setLongname(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getLongname());
		basclt0100RecTO.setGroupno(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getGroupno());
		basclt0100RecTO.setNametype(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getNametype());
		basclt0100RecTO.setNamestatus(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getNamestatus());
		basclt0100RecTO.setClientname(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getClientname());
		basclt0100RecTO.setFirstname(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getFirstname());
		basclt0100RecTO.setMidname(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getMidname());
		basclt0100RecTO.setDba(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getDba());
		basclt0100RecTO.setPhone1(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getPhone1());
		basclt0100RecTO.setFedtaxid(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getFedtaxid());
		basclt0100RecTO.setSsn(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getSsn());
		basclt0300RecTO.setAddrln1(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getAddrln1());
		basclt0300RecTO.setCity(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getCity());
		basclt0300RecTO.setState(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getState());
		basclt0300RecTO.setZipcode(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getZipcode());
		pmsp1200RecTO.rec12fmtRec.setLoannum(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getLoanNum());
		pmsp1200RecTO.rec12fmtRec.setUse0code(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getLoanNum());
		pmsp1200RecTO.rec12fmtRec.setDzip0code(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getDzip0code());
		pmsp1200RecTO.rec12fmtRec.setDesc0line1(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getDesc0line1());
		pmsp1200RecTO.rec12fmtRec.setDesc0line2(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getDesc0line2());
		pmsp1200RecTO.rec12fmtRec.setDesc0line3(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getDesc0line3());
		pmsp1200RecTO.rec12fmtRec.setDesc0line4(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getDesc0line4());
		if(wsAddlIntsCsrSw.isStartOfCursor()) {
			wsAddlIntsCsrSw.setFetchingCursor();
		}
	} else {
		if(pmsp0200Basclt1400Basclt0100Basclt0300Pmsp1200TO.getDBAccessStatus().isRecNotFound()) {
			if(wsAddlIntsCsrSw.isStartOfCursor()) {
				wsAddlIntsCsrSw.setEndOfCursor();
			} else if (wsAddlIntsCsrSw.isFetchingCursor()){
				wsAddlIntsCsrSw.setEndOfCursor();
			}
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.setWs04ErrParaName("6100-FETCH-ADDL-INT-CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("ADDINT_CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			r000PrepErrorListPara();
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
	}
}

protected void processCltAddlIntCsr() {
	fetchCltAddlIntCsr();
	if(wsCltAddlIntsCsrSw.isEndOfCursor() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred())
	{
		return;
	}
	moveAddlIntData();
	if(wsPopArray)
	{
	} else {
		return;
	}
	processEntry();
	checkDuplicatePara();
}

protected void fetchCltAddlIntCsr() {
	pmsp0200Basclt1500Basclt0100Basclt0300TO = pmsp0200Basclt1500Basclt0100Basclt0300DAO.fetchAddIntCltCursor(pmsp0200Basclt1500Basclt0100Basclt0300TO);
	if(pmsp0200Basclt1500Basclt0100Basclt0300TO.getDBAccessStatus().isSuccess()) {
		pmsp0200RecTO.rec02fmtRec.setTrans0stat(pmsp0200Basclt1500Basclt0100Basclt0300TO.getTrans0Stat());
		pmsp0200RecTO.rec02fmtRec.setEff0yr(pmsp0200Basclt1500Basclt0100Basclt0300TO.getEffYr());
		pmsp0200RecTO.rec02fmtRec.setEff0mo(pmsp0200Basclt1500Basclt0100Basclt0300TO.getEffMo());
		pmsp0200RecTO.rec02fmtRec.setEff0da(pmsp0200Basclt1500Basclt0100Basclt0300TO.getEffDa());
		pmsp0200RecTO.rec02fmtRec.setExp0yr(pmsp0200Basclt1500Basclt0100Basclt0300TO.getExpYr());
		pmsp0200RecTO.rec02fmtRec.setExp0mo(pmsp0200Basclt1500Basclt0100Basclt0300TO.getExpMo());
		pmsp0200RecTO.rec02fmtRec.setExp0da(pmsp0200Basclt1500Basclt0100Basclt0300TO.getExpDa());
		pmsp0200RecTO.rec02fmtRec.setRisk0state(pmsp0200Basclt1500Basclt0100Basclt0300TO.getRisk0state());
		pmsp0200RecTO.rec02fmtRec.setCompany0no(pmsp0200Basclt1500Basclt0100Basclt0300TO.getCompany0no());
		pmsp0200RecTO.rec02fmtRec.setFillr1(pmsp0200Basclt1500Basclt0100Basclt0300TO.getFillr1());
		pmsp0200RecTO.rec02fmtRec.setRpt0agt0nr(pmsp0200Basclt1500Basclt0100Basclt0300TO.getRpt0agt0nr());
		pmsp0200RecTO.rec02fmtRec.setFillr2(pmsp0200Basclt1500Basclt0100Basclt0300TO.getFillr2());
		pmsp0200RecTO.rec02fmtRec.setFillr3(pmsp0200Basclt1500Basclt0100Basclt0300TO.getFillr3());
		pmsp0200RecTO.rec02fmtRec.setFillr4(pmsp0200Basclt1500Basclt0100Basclt0300TO.getFillr4());
		pmsp0200RecTO.rec02fmtRec.setTot0ag0prm(pmsp0200Basclt1500Basclt0100Basclt0300TO.getTot0ag0prm());
		pmsp0200RecTO.rec02fmtRec.setLine0bus(pmsp0200Basclt1500Basclt0100Basclt0300TO.getLine0bus());
		pmsp0200RecTO.rec02fmtRec.setIssue0code(pmsp0200Basclt1500Basclt0100Basclt0300TO.getIssue0code());
		pmsp0200RecTO.rec02fmtRec.setPay0code(pmsp0200Basclt1500Basclt0100Basclt0300TO.getPay0code());
		pmsp0200RecTO.rec02fmtRec.setMode0code(pmsp0200Basclt1500Basclt0100Basclt0300TO.getMode0code());
		pmsp0200RecTO.rec02fmtRec.setSort0name(pmsp0200Basclt1500Basclt0100Basclt0300TO.getSort0name());
		pmsp0200RecTO.rec02fmtRec.setRenewal0cd(pmsp0200Basclt1500Basclt0100Basclt0300TO.getRenewal0cd());
		pmsp0200RecTO.rec02fmtRec.setCust0no(pmsp0200Basclt1500Basclt0100Basclt0300TO.getCust0no());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0a(pmsp0200Basclt1500Basclt0100Basclt0300TO.getSpec0use0a());
		pmsp0200RecTO.rec02fmtRec.setSpec0use0b(pmsp0200Basclt1500Basclt0100Basclt0300TO.getSpec0use0b());
		pmsp0200RecTO.rec02fmtRec.setZip0post(pmsp0200Basclt1500Basclt0100Basclt0300TO.getZip0post());
		pmsp0200RecTO.rec02fmtRec.setAdd0line01(pmsp0200Basclt1500Basclt0100Basclt0300TO.getAdd0line01());
		pmsp0200RecTO.rec02fmtRec.setAdd0line03(pmsp0200Basclt1500Basclt0100Basclt0300TO.getAdd0line03());
		pmsp0200RecTO.rec02fmtRec.setAdd0line04(pmsp0200Basclt1500Basclt0100Basclt0300TO.getAdd0line04());
		pmsp0200RecTO.rec02fmtRec.setType0act(pmsp0200Basclt1500Basclt0100Basclt0300TO.getType0act());
		pmsp0200RecTO.rec02fmtRec.setCanceldate(pmsp0200Basclt1500Basclt0100Basclt0300TO.getCanceldate());
		pmsp0200RecTO.rec02fmtRec.setMrsseq(pmsp0200Basclt1500Basclt0100Basclt0300TO.getMrsseq());
		pmsp0200RecTO.rec02fmtRec.setLocation(pmsp0200Basclt1500Basclt0100Basclt0300TO.getLocation());
		pmsp0200RecTO.rec02fmtRec.setMaster0co(pmsp0200Basclt1500Basclt0100Basclt0300TO.getMasterCo());
		pmsp0200RecTO.rec02fmtRec.setSymbol(pmsp0200Basclt1500Basclt0100Basclt0300TO.getSymbol());
		pmsp0200RecTO.rec02fmtRec.setPolicy0num(pmsp0200Basclt1500Basclt0100Basclt0300TO.getPolicy0Num());
		pmsp0200RecTO.rec02fmtRec.setModule(pmsp0200Basclt1500Basclt0100Basclt0300TO.getModule());
		basclt1500RecTO.setUse0code(pmsp0200Basclt1500Basclt0100Basclt0300TO.getUse0code());
		basclt1500RecTO.setUse0loc(pmsp0200Basclt1500Basclt0100Basclt0300TO.getUse0loc());
		basclt1500RecTO.setDesc0seq(pmsp0200Basclt1500Basclt0100Basclt0300TO.getDesc0seq());
		basclt1500RecTO.setCltseqnum(pmsp0200Basclt1500Basclt0100Basclt0300TO.getCltseqnum());
		basclt1500RecTO.setAddrseqnum(pmsp0200Basclt1500Basclt0100Basclt0300TO.getAddrseqnum());
		basclt0100RecTO.setLongname(pmsp0200Basclt1500Basclt0100Basclt0300TO.getLongname());
		basclt0100RecTO.setGroupno(pmsp0200Basclt1500Basclt0100Basclt0300TO.getGroupno());
		basclt0100RecTO.setNametype(pmsp0200Basclt1500Basclt0100Basclt0300TO.getNametype());
		basclt0100RecTO.setNamestatus(pmsp0200Basclt1500Basclt0100Basclt0300TO.getNamestatus());
		basclt0100RecTO.setClientname(pmsp0200Basclt1500Basclt0100Basclt0300TO.getClientname());
		basclt0100RecTO.setFirstname(pmsp0200Basclt1500Basclt0100Basclt0300TO.getFirstname());
		basclt0100RecTO.setMidname(pmsp0200Basclt1500Basclt0100Basclt0300TO.getMidname());
		basclt0100RecTO.setDba(pmsp0200Basclt1500Basclt0100Basclt0300TO.getDba());
		basclt0100RecTO.setPhone1(pmsp0200Basclt1500Basclt0100Basclt0300TO.getPhone1());
		basclt0100RecTO.setFedtaxid(pmsp0200Basclt1500Basclt0100Basclt0300TO.getFedtaxid());
		basclt0100RecTO.setSsn(pmsp0200Basclt1500Basclt0100Basclt0300TO.getSsn());
		basclt0300RecTO.setAddrln1(pmsp0200Basclt1500Basclt0100Basclt0300TO.getAddrln1());
		basclt0300RecTO.setCity(pmsp0200Basclt1500Basclt0100Basclt0300TO.getCity());
		basclt0300RecTO.setState(pmsp0200Basclt1500Basclt0100Basclt0300TO.getState());
		basclt0300RecTO.setZipcode(pmsp0200Basclt1500Basclt0100Basclt0300TO.getZipcode());

		if(wsCltAddlIntsCsrSw.isStartOfCursor()) {
			wsCltAddlIntsCsrSw.setFetchingCursor();
		}
	} else {
		if(pmsp0200Basclt1500Basclt0100Basclt0300TO.getDBAccessStatus().isRecNotFound()) {
			if(wsCltAddlIntsCsrSw.isStartOfCursor()) {
				wsCltAddlIntsCsrSw.setEndOfCursor();
			} else if (wsCltAddlIntsCsrSw.isFetchingCursor()){
				wsCltAddlIntsCsrSw.setEndOfCursor();
			}
		} else {
			ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
			ws04ErrorDetailArea.setWs04ErrParaName("7100-FETCH-ADDL-INT-CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
			ws04ErrorDetailArea.setWs04ErrFileName("ADDINTCLT_CURSOR");
			ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
			ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
			r000PrepErrorListPara();
			cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
			cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
			cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
			cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
			ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
			ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
			errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
		}
	}
}

protected void moveAddlIntData() {
	wsEffdt = Functions.concat(wsEffdt, 1000,
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getEff0yr(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getEff0mo(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getEff0da(), " "));

	wsExpdt = Functions.concat(wsExpdt, 1000,
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getExp0yr(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getExp0mo(), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getExp0da(), " "));

	ws1Agency = Functions.concat(ws1Agency, 1000,
			Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getFillr1(), " "),
		Functions.trimAfter(Character.toString(pmsp0200RecTO.rec02fmtRec.getRpt0agt0nr()), " "),
		Functions.trimAfter(pmsp0200RecTO.rec02fmtRec.getFillr2(), " "));

	if(!wsCmpny.equalsIgnoreCase("")){
		if(wsCmpny.equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getMaster0co())){
		}else{
			wsPopArray =false; return;
		}
	}
	if(!wsAgency.equalsIgnoreCase("")){
		if(wsAgency.equalsIgnoreCase(ws1Agency)){
		}else{
			wsPopArray =false; return;
		}
	}

	if(!(this.data.getInLocation().equalsIgnoreCase(""))&& !(this.data.getInLocation().equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getLocation()))) {
		wsPopArray =false; return;
	}

	if(!(this.data.getInMaster0co().equalsIgnoreCase("")) && !(this.data.getInMaster0co().equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getMaster0co()))){
		wsPopArray =false; return;
	}

	if(!(this.data.getInSymbol().equalsIgnoreCase("")) && !(this.data.getInSymbol().equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getSymbol()))){
		wsPopArray =false; return;
	}

	if(!(this.data.getInLine0bus().equalsIgnoreCase("")) && !(this.data.getInLine0bus().equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getLine0bus()))){
		wsPopArray =false; return;
	}

	if(!this.data.getInLine0bus().equalsIgnoreCase("")){
		if(!(this.data.getInLine0bus().equalsIgnoreCase(pmsp0200RecTO.rec02fmtRec.getLine0bus()))){
			wsPopArray =false; return;
		}
	}

	//RSK - Start

	if(!this.data.getInLossdte().equals("") && !this.data.getInLossdte().equals("0000000")){
		if(this.data.getInLossdte().compareTo(wsEffdt) < 0 || this.data.getInLossdte().compareTo(wsExpdt) > 0 ){
			wsPopArray = false;
			return;
		}
	}
	//RSK - End
	if (!pmsp0200RecTO.rec02fmtRec.getCanceldate().equals("") && !pmsp0200RecTO.rec02fmtRec.getCanceldate().equals("0000000")) {
		if(this.data.getInLossdte().compareTo(pmsp0200RecTO.rec02fmtRec.getCanceldate()) > 0 ){
			wsPopArray = false;
			return;
		}
	}

	if(!this.data.getInSsn().equalsIgnoreCase(""))
	{
		if(this.data.getInSsnSiteInd() ==  'N' && this.data.getInSsnCltInd() == 'Y')
		{
			if(this.data.getInSsn().equalsIgnoreCase(basclt0100RecTO.getSsn())){
			} else {
				wsPopArray =false; return;
			}
		}
		if(this.data.getInSsnSiteInd() ==  'Y' && this.data.getInSsnCltInd() == 'N'){
			wsSSN = this.data.getInSsn();
			wsSymbol= pmsp0200RecTO.rec02fmtRec.getSymbol();
			wsPolicyNo=pmsp0200RecTO.rec02fmtRec.getPolicy0num();
			wsModule= pmsp0200RecTO.rec02fmtRec.getModule();
			wsMasterCo =pmsp0200RecTO.rec02fmtRec.getMaster0co();
			wsLocation = pmsp0200RecTO.rec02fmtRec.getLocation();

			readPmspwc04Para();
			if(ssnIndicator.isSsnNotFound() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
				wsPopArray =false; return;
			}
		}

		if(this.data.getInSsnSiteInd() ==  'Y' && this.data.getInSsnCltInd() == 'Y'){
			if(this.data.getInSsn().equalsIgnoreCase(basclt0100RecTO.getSsn())){
				ssnIndicator.setValue('Y');
			}else{
				ssnIndicator.setValue('N');
			}
			if(ssnIndicator.isSsnNotFound()){
				wsSSN = this.data.getInSsn();
				wsSymbol= pmsp0200RecTO.rec02fmtRec.getSymbol();
				wsPolicyNo=pmsp0200RecTO.rec02fmtRec.getPolicy0num();
				wsModule= pmsp0200RecTO.rec02fmtRec.getModule();
				wsMasterCo =pmsp0200RecTO.rec02fmtRec.getMaster0co();
				wsLocation = pmsp0200RecTO.rec02fmtRec.getLocation();
				readPmspwc04Para();
				if(ssnIndicator.isSsnNotFound() || ws04ErrorDetailArea.ws04ErrorSw.isErrorOccurred()){
					wsPopArray =false; return;
				}
			}
		}
	}
		wsPopArray = true;
		holdBcTrans0stat = pmsp0200RecTO.rec02fmtRec.getTrans0stat();
		holdBcSymbol = pmsp0200RecTO.rec02fmtRec.getSymbol();
		holdBcPolicy0num = pmsp0200RecTO.rec02fmtRec.getPolicy0num();
		holdBcModule = pmsp0200RecTO.rec02fmtRec.getModule();
		holdbcclientseq = basclt1400RecTO.getCltseqnum();
		holdbcaddressseq = basclt1400RecTO.getAddrseqnum();
		holdBcMaster0co = pmsp0200RecTO.rec02fmtRec.getMaster0co();
		holdBcLocation = pmsp0200RecTO.rec02fmtRec.getLocation();
		holdBcEffdt = wsEffdt;
		holdBcExpdt = wsExpdt;
		holdBcRisk0state = pmsp0200RecTO.rec02fmtRec.getRisk0state();
		holdBcCompany0no = pmsp0200RecTO.rec02fmtRec.getCompany0no();
		holdBcAgency = ws1Agency;
		holdBcLine0bus = pmsp0200RecTO.rec02fmtRec.getLine0bus();
		holdBcRenewal0cd = pmsp0200RecTO.rec02fmtRec.getRenewal0cd();
		holdBcIssue0code = pmsp0200RecTO.rec02fmtRec.getIssue0code();
		holdBcPay0code = pmsp0200RecTO.rec02fmtRec.getPay0code();
		holdBcMode0code = pmsp0200RecTO.rec02fmtRec.getMode0code();
		holdBcSort0name = pmsp0200RecTO.rec02fmtRec.getSort0name();
		holdBcCust0no = pmsp0200RecTO.rec02fmtRec.getCust0no();
		holdBcSpec0use0a =pmsp0200RecTO.rec02fmtRec.getSpec0use0a();
		holdBcSpec0use0b =pmsp0200RecTO.rec02fmtRec.getSpec0use0b();
		if(holdBcRenewal0cd !=9){
			holdBcCanceldate = "";
		}else{
			holdBcCanceldate = pmsp0200RecTO.rec02fmtRec.getCanceldate();
		}
		holdBcMrsseq = pmsp0200RecTO.rec02fmtRec.getMrsseq();
		holdBcTot0ag0prm = pmsp0200RecTO.rec02fmtRec.getTot0ag0prm();
		holdBcType0act = pmsp0200RecTO.rec02fmtRec.getType0act();
		holdBcLastName = basclt0100RecTO.getClientname();
		holdBcFirstName = basclt0100RecTO.getFirstname();
		if (!basclt0100RecTO.getMidname().equals("")) {
		holdBcMiddleName = basclt0100RecTO.getMidname().trim().charAt(1); 
		}
		holdBcFedTaxId = basclt0100RecTO.getFedtaxid();
		holdBcGroupNo = basclt0100RecTO.getGroupno();
		holdBcPhone1 = basclt0100RecTO.getPhone1();
		holdBcSsn = basclt0100RecTO.getSsn();
		holdBcNameType = basclt0100RecTO.getNametype();
		holdBcNameStatus = basclt0100RecTO.getNamestatus();
		holdBcAdd0line02 = basclt0100RecTO.getDba();
		holdBcAddRln1 = basclt0300RecTO.getAddrln1();
		holdBcCity = basclt0300RecTO.getCity();
		holdBcState = basclt0300RecTO.getState();
		holdBcZipcode =basclt0300RecTO.getZipcode();
		if(numOfEntrys == this.data.getInMaxrecords()){
			wsCltAddlIntsCsrSw.setEndOfCursor();
		}
}

protected BcListEntryArr38 getBcListEntryArr38(int index) {
	if (index > bcListEntryArr38.size()) {
		// for an element beyond current array limits, all intermediate
		bcListEntryArr38.ensureCapacity(index);
		for (int i = bcListEntryArr38.size() + 1; i <= index; i++) {
			bcListEntryArr38.append(new BcListEntryArr38());
			// assign default value
		}
	}
	return bcListEntryArr38.get((index) - 1);
}

protected void processEntry(){
	numOfEntrys = (1 + numOfEntrys) % 100000;
	wsCount = numOfEntrys;
	getBcListEntryArr38(wsCount).setBcTrans0stat(holdBcTrans0stat);
	getBcListEntryArr38(wsCount).setBcSymbol(holdBcSymbol);
	getBcListEntryArr38(wsCount).setBcPolicy0num(holdBcPolicy0num);
	getBcListEntryArr38(wsCount).setBcModule(holdBcModule);
	getBcListEntryArr38(wsCount).setBcori0incept(holdbcori0incept);
	getBcListEntryArr38(wsCount).setBcMaster0co(holdBcMaster0co);
	getBcListEntryArr38(wsCount).setBcLocation(holdBcLocation);
	getBcListEntryArr38(wsCount).setBcEffdt(holdBcEffdt);
	getBcListEntryArr38(wsCount).setBcExpdt(holdBcExpdt);
	getBcListEntryArr38(wsCount).setBcRisk0state(holdBcRisk0state);
	getBcListEntryArr38(wsCount).setBcCompany0no(holdBcCompany0no);
	getBcListEntryArr38(wsCount).setBcAgency(holdBcAgency);
	getBcListEntryArr38(wsCount).setBcLine0bus(holdBcLine0bus);
	getBcListEntryArr38(wsCount).setBcRenewal0cd(holdBcRenewal0cd);
	getBcListEntryArr38(wsCount).setBcIssue0code(holdBcIssue0code);
	getBcListEntryArr38(wsCount).setBcPay0code(holdBcPay0code);
	getBcListEntryArr38(wsCount).setBcMode0code(holdBcMode0code);
	getBcListEntryArr38(wsCount).setBcSort0name(holdBcSort0name);
	getBcListEntryArr38(wsCount).setBcCust0no(holdBcCust0no);
	getBcListEntryArr38(wsCount).setBcSpec0use0a(holdBcSpec0use0a);
	getBcListEntryArr38(wsCount).setBcSpec0use0b(holdBcSpec0use0b);
	getBcListEntryArr38(wsCount).setBcCanceldate(holdBcCanceldate);
	getBcListEntryArr38(wsCount).setBcMrsseq(holdBcMrsseq);
	getBcListEntryArr38(wsCount).setBcTot0ag0prm(holdBcTot0ag0prm);
	getBcListEntryArr38(wsCount).setBcLastname(holdBcLastName);
	getBcListEntryArr38(wsCount).setBcFirstname(holdBcFirstName);
	getBcListEntryArr38(wsCount).setBcMiddlename(holdBcMiddleName);
	getBcListEntryArr38(wsCount).setBcFedtaxid(holdBcFedTaxId);
	getBcListEntryArr38(wsCount).setBcGroupno(holdBcGroupNo);
	getBcListEntryArr38(wsCount).setBcPhone1(holdBcPhone1);
	getBcListEntryArr38(wsCount).setBcSsn(holdBcSsn);
	getBcListEntryArr38(wsCount).setBcNametype(holdBcNameType);
	getBcListEntryArr38(wsCount).setBcNamestatus(holdBcNameStatus);
	getBcListEntryArr38(wsCount).setBcAdd0line02(holdBcAdd0line02);
	getBcListEntryArr38(wsCount).setBcAdd0line03(holdBcAddRln1);
	getBcListEntryArr38(wsCount).setBcAdd0line04(holdBcCity);
	getBcListEntryArr38(wsCount).setBcState(holdBcState);
	getBcListEntryArr38(wsCount).setBcZip0post(holdBcZipcode);
	getBcListEntryArr38(wsCount).setBcclientseq(holdbcclientseq);
	getBcListEntryArr38(wsCount).setBcaddressseq(holdbcaddressseq);
	getBcListEntryArr38(wsCount).setBcbirthdate(holdbcbirthdate);

	readTbd301();


	if (getBcListEntryArr38(wsCount).getBcRenewal0cd() == '0') {
		getBcListEntryArr38(wsCount).setBcRenewal0cdDesc("Alright For Agent To Renew");
	} else {
		if (getBcListEntryArr38(wsCount).getBcRenewal0cd() == '1') {
			getBcListEntryArr38(wsCount).setBcRenewal0cdDesc("Computer Renew");
		} else {
			if (getBcListEntryArr38(wsCount).getBcRenewal0cd() == '2') {
				getBcListEntryArr38(wsCount).setBcRenewal0cdDesc("Alright For Home Office To renew");
			} else {
				if (getBcListEntryArr38(wsCount).getBcRenewal0cd() == '3') {
					getBcListEntryArr38(wsCount).setBcRenewal0cdDesc("Already Renewed");
				} else {
					if (getBcListEntryArr38(wsCount).getBcRenewal0cd() == '4') {
						getBcListEntryArr38(wsCount).setBcRenewal0cdDesc("Agent Issued First Policy, Computer To Renew");
					} else {
						if (getBcListEntryArr38(wsCount).getBcRenewal0cd() == '7') {
							getBcListEntryArr38(wsCount).setBcRenewal0cdDesc("Non-Renewal With A Notice");
						} else {
							if (getBcListEntryArr38(wsCount).getBcRenewal0cd() == '8') {
								getBcListEntryArr38(wsCount).setBcRenewal0cdDesc("Non-Renewal Without A Notice");
							} else {
								if (getBcListEntryArr38(wsCount).getBcRenewal0cd() == '9') {
									getBcListEntryArr38(wsCount).setBcRenewal0cdDesc("Cancelled Policy");
								} else {
									getBcListEntryArr38(wsCount).setBcRenewal0cdDesc("");
								}
							}
						}
					}
				}
			}
		}
	}

	rec00fmtRec3.setSymbol(holdBcSymbol);
	rec00fmtRec3.setPolicy0num(holdBcPolicy0num);
	rec00fmtRec3.setModule(holdBcModule);
	rec00fmtRec3.setMaster0co(holdBcMaster0co);
	rec00fmtRec3.setLocation(holdBcLocation);
	wsControlSw = 'Y';
	try {
		call1();
	} catch (DynamicCallException ex) {
		if (ex.getPgmName().compareTo("BASL000401") != 0) {
			throw ex;
		} else {
			wsReturnCode = "FS00";
		}
	}

	if (!(wsReturnCode.compareTo(GOOD_RETURN_FROM_IO) == 0)) {
		rec00fmtRec3.setTrans0stat(holdBcTrans0stat);
		rec00fmtRec3.setType0act(holdBcType0act);
	}

	getBcListEntryArr38(wsCount).setBcStatusDesc("Verified");
	if (rec00fmtRec3.getType0act().compareTo("NB") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr38(wsCount).setBcStatusDesc("NB Error");
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr38(wsCount).setBcStatusDesc("NB Pending");
			}
		}
		return;
	}

	if (rec00fmtRec3.getType0act().compareTo("EN") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr38(wsCount).setBcStatusDesc("EN Error");
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr38(wsCount).setBcStatusDesc("EN Pending");
			}
		}
		return;
	}

	getBcListEntryArr38(wsCount).setBcTrans0stat(rec00fmtRec3.getTrans0stat());
	if (getBcListEntryArr38(wsCount).getBcRenewal0cd() == '9' && getBcListEntryArr38(wsCount).getBcCanceldate().compareTo("") != 0) {
		if (getBcListEntryArr38(wsCount).getBcTrans0stat() == 'V') {
			getBcListEntryArr38(wsCount).setBcTrans0stat('C');
			getBcListEntryArr38(wsCount).setBcStatusDesc("Cancelled");
			return;
		}
	}

	if (rec00fmtRec3.getType0act().compareTo("CN") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr38(wsCount).setBcTrans0stat('1');
			getBcListEntryArr38(wsCount).setBcStatusDesc("CN Error");
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr38(wsCount).setBcStatusDesc("CN Pending");
				getBcListEntryArr38(wsCount).setBcTrans0stat('X');
			}
		}
		return;
	}

	if (rec00fmtRec3.getType0act().compareTo("RB") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr38(wsCount).setBcStatusDesc("RN Error");
			getBcListEntryArr38(wsCount).setBcTrans0stat('2');
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr38(wsCount).setBcStatusDesc("RN Pending");
				getBcListEntryArr38(wsCount).setBcTrans0stat('W');
			}
		}
		return;
	}

	if (rec00fmtRec3.getType0act().compareTo("RI") == 0) {
		if (rec00fmtRec3.getTrans0stat() == 'E') {
			getBcListEntryArr38(wsCount).setBcStatusDesc("RI Error");
			getBcListEntryArr38(wsCount).setBcTrans0stat('3');
		} else {
			if (rec00fmtRec3.getTrans0stat() != 'V') {
				getBcListEntryArr38(wsCount).setBcStatusDesc("RI Pending");
				getBcListEntryArr38(wsCount).setBcTrans0stat('I');
			}
		}
		return;
	}
}

protected void readTbd301(){
	Tbd301TO tbd301TO = new Tbd301TO();
	tbd301TO = Tbd301DAO.selectById(tbd301TO, holdBcLocation,
			holdBcMaster0co,
			holdBcCompany0no);
	if (!tbd301TO.getDBAccessStatus().isSuccess()) {
		tbd301TO = Tbd301DAO.selectById(tbd301TO, holdBcLocation,
				holdBcMaster0co,
				"");
	}
	if (!tbd301TO.getDBAccessStatus().isSuccess()) {
		tbd301TO = Tbd301DAO.selectById(tbd301TO, holdBcLocation,
				"",
				"");
	}
	getBcListEntryArr38(wsCount).setBcinsurername(tbd301TO.getNameadd());
	getBcListEntryArr38(wsCount).setBcinsureraddress(tbd301TO.getStreetadd());
	getBcListEntryArr38(wsCount).setBcinsurercityst(tbd301TO.getCitystate());
	getBcListEntryArr38(wsCount).setBcinsurerzip(tbd301TO.getZipcode());

}

protected void checkDuplicatePara(){
	if(numOfEntrys>1){
		holdPreviousType = getBcListEntryArr38(wsCount).getBcStatusDesc();
		wsCounter = 1;
		holdPreviousType = "";
		while(!(wsCounter >= numOfEntrys)){
			processDuplicatePara();
			wsCounter = wsCounter + 1;
		}
	}
}

protected void processDuplicatePara()
{
	if( (getBcListEntryArr38(wsCounter)).getBcSymbol().equalsIgnoreCase(getBcListEntryArr38(numOfEntrys).getBcSymbol())
			&& getBcListEntryArr38(wsCounter).getBcPolicy0num().equalsIgnoreCase(getBcListEntryArr38(numOfEntrys).getBcPolicy0num())
			&& getBcListEntryArr38(wsCounter).getBcModule().equalsIgnoreCase(getBcListEntryArr38(numOfEntrys).getBcModule())
			&& getBcListEntryArr38(wsCounter).getBcMaster0co().equalsIgnoreCase(getBcListEntryArr38(numOfEntrys).getBcMaster0co())
			&& getBcListEntryArr38(wsCounter).getBcLocation().equalsIgnoreCase(getBcListEntryArr38(numOfEntrys).getBcLocation())){
		wsCount = wsCount-1;
		numOfEntrys = numOfEntrys -1;
	}
}

protected void readPmspwc04Para(){
		cobolsqlca.setDBAccessStatus(pmspwc04dao.openEscheatCursor(wsLocation, wsSymbol, wsPolicyNo,wsMasterCo,wsModule,wsSSN));
		if (cobolsqlca.getSqlCode() == sqlIoOk) {
			ssnIndicator.setSsnFound();
		} else {
			if (cobolsqlca.getSqlCode() == sqlNoRecord) {
				ssnIndicator.setSsnNotFound();
			} else {
				ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
				ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
				ws04ErrorDetailArea.setWs04ErrSqlDtastgDta(
						Functions.concat(
								ws04ErrorDetailArea.getWs04ErrSqlDtastgDta(), 1000,
								Functions.trimAfter(wsSymb, "%"), new String[] { "; ",
								Functions.trimAfter(wsPolNo, "%"), "; ",
								Functions.trimAfter(wsMod, "%"), "; "}));
				ws04ErrorDetailArea.setWs04ErrErrorCode(Integer.toString(cobolsqlca.getSqlCode()));
				ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
				ws04ErrorDetailArea.setWs04ErrParaName("7777-CHECK-FOR-RECORD");
				ws04ErrorDetailArea.setWs04ErrFileName("PMSPWC04");
				cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
				cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
				cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
				cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
				ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
				ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
				errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
			 }
		}
}

protected void overPara(){
	numOfEntrysForSql = numOfEntrys;
	resultSetTO.addResultSet(bcListEntryArr38, numOfEntrysForSql);
}

protected void exit() {
	throw new ReturnException();
}


	public static Rskdbio040TO getToFromByteArray(byte[] dynamic_inputParameters) {
		Rskdbio040TO localRskdbio040TO = new Rskdbio040TO();

	if (dynamic_inputParameters != null) {
		localRskdbio040TO.setInputParameters(dynamic_inputParameters);
}

		return localRskdbio040TO;
}
	public static void setByteArrayFromTo(Rskdbio040TO localRskdbio040TO, byte[] dynamic_inputParameters){

	if (dynamic_inputParameters != null) {
		DataConverter.arrayCopy(localRskdbio040TO.getInputParameters(), localRskdbio040TO.getInputParametersSize(), dynamic_inputParameters, 1);
}

}

protected void call1() {
	byte[] arr_p0 = new byte[7];
	byte[] arr_p1 = new byte[Pmsl0004TO.getSize()];
	byte[] arr_p2 = new byte[1];
	DataConverter.writeString(arr_p0, 1, wsReturnCode, 7);
	DataConverter.arrayCopy(rec00fmtRec3.getData(), Pmsl0004TO.getSize(), arr_p1, 1);
	DataConverter.writeChar(arr_p2, 1, wsControlSw);
	IProcessInfo<Basl000401TO> basl000401 = DynamicCall.getInfoProcessor(Basl000401.class);
	Basl000401TO callingBasl000401TO = Basl000401.getToFromByteArray((arr_p0), (arr_p1), (arr_p2));
	callingBasl000401TO = basl000401.run(callingBasl000401TO);
	Basl000401.setByteArrayFromTo(callingBasl000401TO, (arr_p0), (arr_p1), (arr_p2));
	wsReturnCode = DataConverter.readString(arr_p0, 1, 7);
	rec00fmtRec3.setData(arr_p1);
	wsControlSw = DataConverter.readChar(arr_p2, 1);
}

protected int getBcListTableSize() {
	return 177500;
}

protected byte[] getBcListTable() {
	byte[] buf = new byte[getBcListTableSize()];
	int offset = 1;
	for (int idx = 1; idx <= bcListEntryArr38.size(); idx++) {
		DataConverter.arrayCopy(bcListEntryArr38.get((idx) - 1).getData(), 1, BcListEntryArr24.getSize(), buf, offset);
		offset += BcListEntryArr24.getSize();
	}
	if (bcListEntryArr38.size() < 500) {
		byte[] rowBuf = DataConverter.fillEndMarker(BcListEntryArr38.getSize());
		for (int idx = bcListEntryArr38.size() + 1; idx <= 500; idx++) {
			DataConverter.arrayCopy(rowBuf, BcListEntryArr38.getSize(), buf, offset);
			offset += BcListEntryArr38.getSize();
		}
	}
	return buf;
}

protected void initBcListTableSpaces() {
	for (int idx = 1; idx <= bcListEntryArr38.size(); idx++) {
		if (bcListEntryArr38.get((idx) - 1) != null) {
			bcListEntryArr38.get((idx) - 1).initSpaces();
		}
	}
}
protected void getMaxModule(){
	Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
	pmsp0200TO = Pmsp0200DAO.selectMaxMasterco(pmsp0200TO, wsMco, wsLoc, wsSymb, wsPolNo, var1, var2, var3, var4);
	cobolsqlca.setDBAccessStatus(pmsp0200TO.getDBAccessStatus());
	if (!cobolsqlca.isEOF() || cobolsqlca.getSqlCode() == sqlIoOk ) {
		wsMaxModule = pmsp0200TO.getModule();
		this.data.setInModule(wsMaxModule);
	}
	else
	{
		gatherErrorEventData3700E();
	}
}

protected void gatherErrorEventData3700E(){
	ws04ErrorDetailArea.initWs04ErrorDetailDataArea();
	ws04ErrorDetailArea.ws04SqlErrorSw.setSqlError();
	ws04ErrorDetailArea.setWs04ErrParaName("3700-GET-MAX-MODULE");
	ws04ErrorDetailArea.setWs04ErrErrorTyp('H');
	ws04ErrorDetailArea.setWs04ErrFileName("PMSP0200");
	r000PrepErrorListPara();
	cpyerrhrtn.ws04ErrorDetailArea.setData(ws04ErrorDetailArea.getData());
	cpyerrhrtn.ws03RtrvMsgWorkArea.setData(ws03RtrvMsgWorkArea.getData());
	cpyerrhrtn.errorListRecord.setData(errorListRecord.getData());
	cpyerrhrtn.r200ErrorHandlingRoutine(cobolsqlca);
	ws04ErrorDetailArea.setData(cpyerrhrtn.ws04ErrorDetailArea.getData());
	ws03RtrvMsgWorkArea.setData(cpyerrhrtn.ws03RtrvMsgWorkArea.getData());
	errorListRecord.setData(cpyerrhrtn.errorListRecord.getData());
}
}
