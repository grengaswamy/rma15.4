// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.ProcessInfoObject;
import bphx.c2ab.util.DataConverter;

public class Rskdbio066TO extends ProcessInfoObject {
	private String lsLocation = "";
	private String lsMasterco = "";
	private String lsSymbol = "";
	private String lsPolicyNum = "";
	private String lsModule = "";
	private String lsIntType = "";
	private String lsIntLocation = "";
	private String lsIntSeq = "";
	private String lsName = "";
	private String lsAddress = "";
	private String lsCity = "";
	private String lsState = "";

	public Rskdbio066TO(byte[] lsInputParams) {
		basicInitialization();
		setLsInputParams(lsInputParams);
	}

	public Rskdbio066TO() {
		basicInitialization();
	}

	public static int getLsInputParamsSize() {
		return 122;
	}

	public void setLsInputParams(byte[] buf) {
		int offset = 1;
		lsLocation = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsMasterco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		lsPolicyNum = DataConverter.readString(buf, offset, 7);
		offset += 7;
		lsModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsIntType = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsIntLocation = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsIntSeq = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsName = DataConverter.readString(buf, offset, 31);
		offset += 31;
		lsAddress = DataConverter.readString(buf, offset, 31);
		offset += 31;
		lsCity = DataConverter.readString(buf, offset, 29);
		offset += 29;
		lsState = DataConverter.readString(buf, offset, 3);
		offset += 3;
		assert (offset - 1 == buf.length) : offset + "-1 != " + buf.length;
	}

	public byte[] getLsInputParams() {
		byte[] buf = new byte[getLsInputParamsSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, lsLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsMasterco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, lsPolicyNum, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, lsModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsIntType, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsIntLocation, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsIntSeq, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsName, 31);
		offset += 31;
		DataConverter.writeString(buf, offset, lsAddress, 31);
		offset += 31;
		DataConverter.writeString(buf, offset, lsCity, 29);
		offset += 29;
		DataConverter.writeString(buf, offset, lsState, 3);
		offset += 3;
		assert (offset - 1 == buf.length) : offset + "-1 != " + buf.length;
		return buf;
	}

	public void basicInitialization() {}

	public String getLsLocation() {
		return this.lsLocation;
	}

	public String getLsMasterco() {
		return this.lsMasterco;
	}

	public String getLsSymbol() {
		return this.lsSymbol;
	}

	public String getLsPolicyNum() {
		return this.lsPolicyNum;
	}

	public String getLsModule() {
		return this.lsModule;
	}

	public String getLsIntType() {
		return this.lsIntType;
	}

	public String getLsIntLocation() {
		return this.lsIntLocation;
	}

	public String getLsIntSeq() {
		return this.lsIntSeq;
	}

	public String getLsName() {
		return this.lsName;
	}

	public String getLsAddress() {
		return this.lsAddress;
	}

	public String getLsCity() {
		return this.lsCity;
	}

	public String getLsState() {
		return this.lsState;
	}
}
