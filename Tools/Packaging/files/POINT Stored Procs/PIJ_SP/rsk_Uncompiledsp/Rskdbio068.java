// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk;

import java.math.BigDecimal;

import bphx.c2ab.cobol.CalledProgList;
import bphx.c2ab.sql.TypeCast;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DynamicCall;
import bphx.c2ab.util.DynamicSqlDAO;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.IProcessInfo;
import bphx.c2ab.util.ProcessInfo;
import bphx.c2ab.util.SignType;
import bphx.sdf.collections.IABODynamicArray;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.util.ABOSystem;

import com.csc.pt.svc.LiteralGenerator;
import com.csc.pt.svc.common.LnkParmsRcrtvppi00;
import com.csc.pt.svc.common.Rcrtvppi00;
import com.csc.pt.svc.common.to.Rcrtvppi00TO;
import com.csc.pt.svc.ctrl.Clt1500recRec2;
import com.csc.pt.svc.data.dao.Bassys0500DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.to.Bassys0500TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.db.ItemIntsRecordRec;
import com.csc.pt.svc.db.LsWhereClause11;
import com.csc.pt.svc.db.PolintRecordRec;
import com.csc.pt.svc.db.Ws1SelectStatement11;
import com.csc.pt.svc.rsk.to.Rskdbio068TO;
import com.csc.pt.svc.isl.Cobolsqlca;
import com.csc.pt.svc.nav.Basclt1501;
import com.csc.pt.svc.nav.to.Basclt1501TO;
import com.csc.pt.svc.pas.alr.Bascltb14;
import com.csc.pt.svc.pas.alr.to.Bascltb14TO;

/**
****************************************************************
* This is the Stored procedure to return all the data for the  *
* Additional interest grid corresponding to following          *
* parameters passed from the server-side:                      *
* 1. LOC                                                       *
* 2. MCO                                                       *
* 3. Symbol                                                    *
* 4. Policy                                                    *
* 5. Module Number                                             *
* 6. Insline                                                   *
* 7. Prod                                                      *
* 8. Riskloc                                                   *
* 9. Subloc                                                    *
*10. Unitnum                                                   *
*11. Covg                                                      *
*12. Covgseq                                                   *
*13. Item                                                      *
*14. Itemseq                                                   *
* The following fields are set when the user enters data in the*
* search fields:                                               *
* 6. Interest Type                                             *
* 7. Interest Location                                         *
* 8. Interest Sequence                                         *
* 9. Name                                                      *
* 10. Address                                                  *
* 11. City                                                     *
* 12. State                                                    *
* 13. Client Sequence                                          *
* 14. Address Sequence                                         *
****************************************************************
*/

public class Rskdbio068 extends ProcessInfo<Rskdbio068TO>  {
	protected Cobolsqlca cobolsqlca = new Cobolsqlca();
	protected Ws1SelectStatement11 ws1SelectStatement11 = new Ws1SelectStatement11();
	protected int numOfEntrys;
	protected int numOfEntrysForSql;
	protected char wsIssue0codeFlag = ' ';
	protected BigDecimal defaultIndx = BigDecimal.ZERO;
	protected IABODynamicArray<ItemIntsEntryArr> itemIntsEntryArr =
		ABOCollectionCreator.getReferenceFactory(ItemIntsEntryArr.class).createDynamicArray(1);
	protected PolintRecordRec polintRecordRec = new PolintRecordRec();
	protected ItemIntsRecordRec itemIntsRecordRec = new ItemIntsRecordRec();
	protected Clt1500recRec2 clt1500recRec2 = new Clt1500recRec2();
	protected String wsLocation = "";
	protected String wsMasterco = "";
	protected String wsSymbol = "";
	protected String wsPolicyNum = "";
	protected String wsModule = "";
	protected String wsIntType = "";
	protected String wsIntLoc = "";
	protected String wsIntSeq = "";
	protected String wsName = "";
	protected String wsAddress = "";
	protected String wsCity = "";
	protected String wsState = "";
	protected String wsInsLine = "";
	protected String wsProduct = "";
	protected String wsRiskloc = "";
	protected String wsSubLoc = "";
	protected String wsUnitNum = "";
	protected String wsCovg = "";
	protected String wsCovgSeq = "";
	protected String wsItemCode = "";
	protected String wsItemSeq = "";
	protected BigDecimal wsStatusOwnership = BigDecimal.ZERO;
	protected String wsStatusTitleC = "";
	protected long wsStatusOwnsalary;
	protected String wsLegalEntity = "";
	protected String wsBcState = "";
	protected String wsDeemedCode = "";
	protected char wsDeemedStatus = ' ';
	protected String wsSsnNo = "";
	protected String linkReturnCode = "";
	protected String wsDsort0name = "";
	protected char wsIssue0code = ' ';
	protected int wsItemAddIntLimit;
	protected String wsLineOfBus = "";
	protected String wsId02 = "02";
	protected String wsUse0code = "";
	protected String wsUse0loc = "";
	protected String wsDesc0seq = "";
	protected String wsDesc0line1 = "";
	protected String wsDesc0line3 = "";
	protected String wsSplitCity = "";
	protected String wsSplitState = "";
	protected long linkClientSeqNum;
	protected short linkAddrSeqNum;
	protected String linkLocateName = "";
	protected short sqlIoOk;
	protected LnkParmsRcrtvppi00 lnkParmsRcrtvppi00 = new LnkParmsRcrtvppi00();
	protected int ws1StrPos;

	protected LsWhereClause11 lsWhereClause11 = new LsWhereClause11();
	protected Pmsp0200TO pmsp0200to = new Pmsp0200TO();
	protected Bassys0500TO bassys0500to = new Bassys0500TO();
	protected DynamicSqlDAO sqlCursor_DAO = new DynamicSqlDAO();



	protected Rskdbio068 () {
		this.data = new Rskdbio068TO();

		itemIntsEntryArr.assign((1) - 1, new ItemIntsEntryArr());
	}
	@Override
	protected void mainSubroutine () {
		mainParagraph();
	}

	protected void mainParagraph() {
		// statement is ignored: DECLARECURSOR[IDeclareCursorStatement]
		// @source=BASDBIO068.cbl:line=153
		/*
		 * DECLARE SQL_CURSOR CURSOR FOR SQL_STATEMENT
		 */
		init();
		getIssueLobCode();
		if (wsIssue0codeFlag == 'N') {
			return;
		}
		defaultIndx = BigDecimal.valueOf(1);
		do
		{
			getPolLimitBassys0500();
			defaultIndx = defaultIndx.add(BigDecimal.valueOf(1));
		}
		while (!(defaultIndx.compareTo(BigDecimal.valueOf(5)) >= 0 || cobolsqlca.getSqlCode() == sqlIoOk));
		loadPolints();
		passPolints();
	}

	protected void init() {
		numOfEntrys = 0;
		setWsInputParams(this.data.getLsInputParams());
		initWsInitializeParams();
	}

	protected void getIssueLobCode() {
		wsIssue0codeFlag = 'N';
		pmsp0200to = Pmsp0200DAO.selectById72(pmsp0200to, wsId02, wsSymbol, wsPolicyNum, wsModule, wsMasterco, wsLocation);
		cobolsqlca.setDBAccessStatus(pmsp0200to.getDBAccessStatus());
		if (!cobolsqlca.isEOF()) {
			wsIssue0code = pmsp0200to.getIssue0code();
			wsLineOfBus = pmsp0200to.getLine0bus();
		}
		if (cobolsqlca.getSqlCode() == sqlIoOk) {
			wsIssue0codeFlag = 'Y';
		}
	}

	protected void getPolLimitBassys0500() {
		if (defaultIndx.compareTo(BigDecimal.valueOf(1)) == 0) {
			wsInsLine = this.data.getLsInsLine();
		} else {
			if (defaultIndx.compareTo(BigDecimal.valueOf(2)) == 0) {
				wsInsLine = "*AL";
			} else {
				if (defaultIndx.compareTo(BigDecimal.valueOf(3)) == 0) {
					wsLineOfBus = "*AL";
					wsInsLine = this.data.getLsInsLine();
				} else {
					if (defaultIndx.compareTo(BigDecimal.valueOf(4)) == 0) {
						wsLineOfBus = "*AL";
						wsInsLine = "*AL";
					} else {
						if (defaultIndx.compareTo(BigDecimal.valueOf(5)) == 0) {
							wsItemAddIntLimit = 100;
						}
					}
				}
			}
		}
		bassys0500to = Bassys0500DAO.selectByLob(bassys0500to, wsLineOfBus, wsInsLine);
		cobolsqlca.setDBAccessStatus(bassys0500to.getDBAccessStatus());
		if (!cobolsqlca.isEOF()) {
			wsItemAddIntLimit = bassys0500to.getLimit5();
		}
		if (cobolsqlca.getSqlCode() != sqlIoOk) {
			wsItemAddIntLimit = 100;
		}
	}

	protected void loadPolints() {
		itemIntsRecordRec.initItemIntsRecord();
		ws1SelectStatement11.setWs1SelectStatement("");
		buildSelectStatement();
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.prepWithOverrideCheck(ws1SelectStatement11.getWs1SelectStatement()));
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.open());
		if (cobolsqlca.getSqlCode() == sqlIoOk) {
		} else {
			return;
		}
		fetchRecord();
		numOfEntrys = (1 + numOfEntrys) % 100000;
		getItemIntsEntryArr(numOfEntrys).setPiIssueCode(wsIssue0code);
		numOfEntrys = (1 + numOfEntrys) % 100000;
		if (!(itemIntsRecordRec.getUse0code().compareTo("") == 0 && itemIntsRecordRec.getUse0loc().compareTo("") == 0 && itemIntsRecordRec
			.getDesc0seq().compareTo("") == 0)) {
			cobolsqlca.setSqlCode((short)0);
			numOfEntrys = numOfEntrys;
			while (!(cobolsqlca.getSqlCode() != sqlIoOk || numOfEntrys > wsItemAddIntLimit + 1)) {
				loadRecords();
				numOfEntrys = numOfEntrys + 1;
			}
		}
		numOfEntrys = (numOfEntrys - 1) % 100000;
		getItemIntsEntryArr(2).setPiNumOfRecords(ABOSystem.convertUsingString(numOfEntrys, "S99999"));
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.close());
	}

	protected void buildSelectStatement() {
		lsWhereClause11.setAddressableData(ws1SelectStatement11);
		lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, "SELECT", new String[] { " USE0CODE,",
			" USE0LOC,", " DESC0SEQ", " FROM PMSP1200AT WHERE    (", "\u0000" }));
		// ---------------------------------------------------------------*
		// Set the Address of the LS-WHERE-CLAUSE so that we can *
		// add the restrector fields to the WHERE clause. *
		// ---------------------------------------------------------------*
		// ---------------------------------------------------------------*
		// Set the Address of the LS-WHERE-CLAUSE so that we can *
		// add the restrector fields to the WHERE clause. *
		// ---------------------------------------------------------------*
		setAddress();
		// Location
		if (this.data.getLsLocation().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " LOCATION = \"", new String[] {
				Functions.padBlanks(this.data.getLsLocation(), 2), "\"", "\u0000" }));
			setAddress();
		}
		// Master Company
		if (this.data.getLsMasterco().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND MASTER0CO = \"", new String[] {
				Functions.padBlanks(this.data.getLsMasterco(), 2), "\"", "\u0000" }));
			setAddress();
		}
		// Policy Symbol
		if (this.data.getLsSymbol().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND SYMBOL = \"", new String[] {
				Functions.padBlanks(this.data.getLsSymbol(), 3), "\"", "\u0000" }));
			setAddress();
		}
		// Policy Number
		if (this.data.getLsPolicyNum().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND POLICY0NUM = \"", new String[] {
				Functions.padBlanks(this.data.getLsPolicyNum(), 7), "\"", "\u0000" }));
			setAddress();
		}
		// Module
		if (this.data.getLsModule().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND MODULE = \"", new String[] {
				Functions.padBlanks(this.data.getLsModule(), 2), "\"", "\u0000" }));
			setAddress();
		}
		// Interest Type
		if (this.data.getLsIntType().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND USE0CODE = \"", new String[] {
				Functions.padBlanks(this.data.getLsIntType(), 2), "\"", "\u0000" }));
			setAddress();
		}
		// Interest Location
		if (this.data.getLsIntLoc().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND USE0LOC = \"", new String[] {
				Functions.padBlanks(this.data.getLsIntLoc(), 5), "\"", "\u0000" }));
			setAddress();
		}
		// Interest Sequence
		if (this.data.getLsIntSeq().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND DESC0SEQ = \"", new String[] {
			Functions.padBlanks(this.data.getLsIntSeq(), 5), "\"", "\u0000" }));
			setAddress();
		}
		// Insurance Line
		if (this.data.getLsInsLine().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND INSLINE = \"", new String[] {
				Functions.padBlanks(this.data.getLsInsLine(), 3), "\"", "\u0000" }));
			setAddress();
		}
		// Product
		if (this.data.getLsProduct().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND PRODUCT = \"", new String[] {
				Functions.padBlanks(this.data.getLsProduct(), 6), "\"", "\u0000" }));
			setAddress();
		}
		// Risk Location
		if (this.data.getLsRiskloc().compareTo("") != 0) {
				lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND RLOCNUM = " , new String[] {
					Functions.padBlanks(this.data.getLsRiskloc(), 5),"\u0000" }));
			setAddress();
		}
		// Sub Location
		if (this.data.getLsSubLoc().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND RSUBLOCNUM = ", new String[] {

				Functions.padBlanks(this.data.getLsSubLoc(), 5),"\u0000" }));
			setAddress();
		}
		// Unit Number
		if (this.data.getLsUnitNum().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND UNITNUM = ", new String[] {
				Functions.padBlanks(this.data.getLsUnitNum(), 5),"\u0000" }));
			setAddress();
		}
		// Coverage
		if (this.data.getLsCovg().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND COVERAGE = \"", new String[] {
				Functions.padBlanks(this.data.getLsCovg(), 6), "\"", "\u0000" }));
			setAddress();
		}
		// Coverage Sequence
		if (this.data.getLsCovgSeq().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND COVSEQ = ", new String[] {
			Functions.padBlanks(this.data.getLsCovgSeq(), 5),"\u0000" }));
			setAddress();
		}
		// Item Code
		if (this.data.getLsItemCode().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND ITEMCODE = \"", new String[] {
				Functions.padBlanks(this.data.getLsItemCode(), 6), "\"", "\u0000" }));
			setAddress();
		}
		// Item Sequence
		if (this.data.getLsItemSeq().compareTo("") != 0) {
			lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, " AND ITEMSEQ = ", new String[] {
				Functions.padBlanks(this.data.getLsItemSeq(), 5),"\u0000" }));
			setAddress();
		}
		lsWhereClause11.setLsWhereClause(Functions.concat(lsWhereClause11.getLsWhereClause(), 200, ") ORDER BY", new String[] { " USE0CODE,",
			" USE0LOC,", " DESC0SEQ" }));
	}

	protected void loadRecords() {
		loadRecs();
		// Interest type search
		wsIntType = wsUse0code;
		if (this.data.getLsIntType().compareTo("") != 0) {
			if (wsIntType.compareTo(this.data.getLsIntType()) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// Interest Location search
		wsIntLoc = wsUse0loc;
		if (this.data.getLsIntLoc().compareTo("") != 0) {
			if (wsIntLoc.compareTo(this.data.getLsIntLoc()) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// Interest Sequence search
		wsIntSeq = wsDesc0seq;
		if (this.data.getLsIntSeq().compareTo("") != 0) {
			if (wsIntSeq.compareTo(this.data.getLsIntSeq()) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// Name Search
		wsName = wsDesc0line1;
		if (this.data.getLsName().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(this.data.getLsName(), 31, "%");
			if (ABOSystem.Upper(Functions.subString(wsName, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(this.data.getLsName(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// Address Search
		wsAddress = wsDesc0line3;
		if (this.data.getLsAddress().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(this.data.getLsAddress(), 31, "%");
			if (ABOSystem.Upper(Functions.subString(wsAddress, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(this.data.getLsAddress(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// City Search
		setWsCityState(getWsDesc0line4());
		if (this.data.getLsCity().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(this.data.getLsCity(), 29, "%");
			if (ABOSystem.Upper(Functions.subString(wsCity, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(this.data.getLsCity(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// State Search
		if (this.data.getLsState().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(this.data.getLsState(), 3, "%");
			if (ABOSystem.Upper(Functions.subString(wsState, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(this.data.getLsState(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		getItemIntsEntryArr(numOfEntrys).setPiIntType(itemIntsRecordRec.getUse0code());
		getItemIntsEntryArr(numOfEntrys).setPiIntLocation(itemIntsRecordRec.getUse0loc());
		getItemIntsEntryArr(numOfEntrys).setPiIntSeq(itemIntsRecordRec.getDesc0seq());
		getItemIntsEntryArr(numOfEntrys).setPiName(wsDesc0line1);
		getItemIntsEntryArr(numOfEntrys).setPiAddress(wsDesc0line3);
		getItemIntsEntryArr(numOfEntrys).setPiIssueCode(wsIssue0code);
		setWsCityState(getWsDesc0line4());
		getItemIntsEntryArr(numOfEntrys).setPiCity(wsCity);
		getItemIntsEntryArr(numOfEntrys).setPiState(wsState);
		getItemIntsEntryArr(numOfEntrys).setPiClientSeq(linkClientSeqNum);
		getItemIntsEntryArr(numOfEntrys).setPiAddressSeq(linkAddrSeqNum);
		fetchRecord();
	}

	protected void loadRecs() {
		IProcessInfo<Rcrtvppi00TO> rcrtvppi00;
		Rcrtvppi00TO rcrtvppi00to;
		rtvSeqNum();
		lnkParmsRcrtvppi00.initLowValues();
		lnkParmsRcrtvppi00.setLnkUseCode(itemIntsRecordRec.getUse0code());
		lnkParmsRcrtvppi00.setLnkUseLoc(itemIntsRecordRec.getUse0loc());
		lnkParmsRcrtvppi00.setLnkDescSeq(itemIntsRecordRec.getDesc0seq());
		lnkParmsRcrtvppi00.setLnkSortCode(wsDsort0name);
		lnkParmsRcrtvppi00.setLnkSym(this.data.getLsSymbol());
		itemIntsRecordRec.setSymbol(this.data.getLsSymbol());
		lnkParmsRcrtvppi00.setLnkPol(this.data.getLsPolicyNum());
		itemIntsRecordRec.setPolicy0num(this.data.getLsPolicyNum());
		lnkParmsRcrtvppi00.setLnkMod(this.data.getLsModule());
		itemIntsRecordRec.setModule(this.data.getLsModule());
		lnkParmsRcrtvppi00.setLnkLoc(this.data.getLsLocation());
		itemIntsRecordRec.setLocation(this.data.getLsLocation());
		lnkParmsRcrtvppi00.setLnkMco(this.data.getLsMasterco());
		itemIntsRecordRec.setMaster0co(this.data.getLsMasterco());
		lnkParmsRcrtvppi00.setLnkReadType("GE");
		rcrtvppi00 = DynamicCall.getInfoProcessor(Rcrtvppi00.class);
		rcrtvppi00to =
			new Rcrtvppi00TO(lnkParmsRcrtvppi00, wsStatusOwnership, wsStatusTitleC, wsStatusOwnsalary, wsLegalEntity, wsBcState,
				wsDeemedCode, wsDeemedStatus, wsSsnNo);
		rcrtvppi00to = rcrtvppi00.run(rcrtvppi00to);
		wsStatusOwnership = rcrtvppi00to.getStatusOwnershipPercent();
		wsStatusTitleC = rcrtvppi00to.getStatusTitle();
		wsStatusOwnsalary = rcrtvppi00to.getStatusSalary();
		wsLegalEntity = rcrtvppi00to.getWsLegalEntity();
		wsBcState = rcrtvppi00to.getTbrecu3State();
		wsDeemedCode = rcrtvppi00to.getDeemedCode();
		wsDeemedStatus = rcrtvppi00to.getDeemedStatus();
		wsSsnNo = rcrtvppi00to.getSsnNo();
		linkReturnCode = LiteralGenerator.create("0", 7);
		call1();
		CalledProgList.deleteInst(Bascltb14.class);
		wsUse0code = lnkParmsRcrtvppi00.getLnkUseCode();
		wsUse0loc = lnkParmsRcrtvppi00.getLnkUseLoc();
		wsDesc0seq = lnkParmsRcrtvppi00.getLnkDescSeq();
		wsDesc0line1 = lnkParmsRcrtvppi00.getLnkName();
		wsDesc0line3 = lnkParmsRcrtvppi00.getLnkStreetAddress();
		setWsDesc0line4(DataConverter.toBytes(lnkParmsRcrtvppi00.getLnkCitystAddress(), getWsDesc0line4Size()));
	}

	protected void rtvSeqNum() {
		Basclt1501TO basclt1501to2;
		IProcessInfo<Basclt1501TO> basclt1501;
		clt1500recRec2.setLocation(this.data.getLsLocation());
		clt1500recRec2.setMaster0co(this.data.getLsMasterco());
		clt1500recRec2.setSymbol(this.data.getLsSymbol());
		clt1500recRec2.setPolicy0num(this.data.getLsPolicyNum());
		clt1500recRec2.setModule(this.data.getLsModule());
		clt1500recRec2.setUse0code(itemIntsRecordRec.getUse0code());
		clt1500recRec2.setUse0loc(itemIntsRecordRec.getUse0loc());
		clt1500recRec2.setDesc0seq(itemIntsRecordRec.getDesc0seq());
		basclt1501 = DynamicCall.getInfoProcessor(Basclt1501.class);
		basclt1501to2 = new Basclt1501TO(linkReturnCode, getWsRecord1501());
		basclt1501to2 = basclt1501.run(basclt1501to2);

		linkReturnCode = basclt1501to2.getLinkReturnCode();
		setWsRecord1501(basclt1501to2.getLinkRecord());
		CalledProgList.deleteInst(Basclt1501.class);
		// If the record is found
		if (linkReturnCode.compareTo("0000000") == 0) {
			linkClientSeqNum = clt1500recRec2.getCltseqnum();
			linkAddrSeqNum = clt1500recRec2.getAddrseqnum();
		} else {
			linkClientSeqNum = 0;
			linkAddrSeqNum = 0;
		}
	}

	protected void passPolints() {
		numOfEntrysForSql = numOfEntrys;
		resultSetTO.addResultSet(itemIntsEntryArr, numOfEntrysForSql);
	}

	protected void fetchRecord() {
		Object[] resultSet;
		itemIntsRecordRec.initItemIntsRecord();
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.fetchNext());
		if (cobolsqlca.getSqlCode() == 0) {
			resultSet = cobolsqlca.getResultSet();
			itemIntsRecordRec.setUse0code(TypeCast.getAsStr(resultSet[(1) - 1], 2));
			itemIntsRecordRec.setUse0loc(TypeCast.getAsStr(resultSet[(2) - 1], 5));
			itemIntsRecordRec.setDesc0seq(TypeCast.getAsStr(resultSet[(3) - 1], 5));
		}
		if (cobolsqlca.getSqlCode() == sqlIoOk) {
		} else {
			return;
		}
		polintRecordRec.setUse0code(itemIntsRecordRec.getUse0code());
		this.data.setLsIntType(itemIntsRecordRec.getUse0code());
		polintRecordRec.setUse0loc(itemIntsRecordRec.getUse0loc());
		this.data.setLsIntLoc(itemIntsRecordRec.getUse0loc());
		polintRecordRec.setDesc0seq(itemIntsRecordRec.getDesc0seq());
		this.data.setLsIntSeq(itemIntsRecordRec.getDesc0seq());
	}

	protected void setAddress() {
		ws1StrPos = 1;
		// -------------------------------------------------------------- *
		// Calculate the WS1-STR-POS by getting the number of chars *
		// before the X'00'. *
		// ---------------------------------------------------------------*
		ws1StrPos = ws1StrPos + Functions.countCharsBefore(ws1SelectStatement11.getWs1SelectStatement(), 10000, "\u0000");
		// ---------------------------------------------------------------*
		// The STRING statement can not have a referential modified *
		// field as the INTO fields. So we use a field that has been *
		// defined in linkage named LS-WHERE-CLAUSE. We set the *
		// address of LS-WHERE-CLAUSE to the address of the part of *
		// field that we want to add the where condition to. *
		// ---------------------------------------------------------------*
		lsWhereClause11.setAddressableData(ws1SelectStatement11, ws1StrPos);
		lsWhereClause11.setLsWhereClause("");
	}






	public static Rskdbio068TO getToFromByteArray(byte[] dynamic_lsInputParams) {
		Rskdbio068TO localBasdbio068TO = new Rskdbio068TO();

		if (dynamic_lsInputParams != null) {
			localBasdbio068TO.setLsInputParams(dynamic_lsInputParams);
	}

		return localBasdbio068TO;
	}
	public static void setByteArrayFromTo(Rskdbio068TO localBasdbio068TO, byte[] dynamic_lsInputParams){

		if (dynamic_lsInputParams != null) {
			DataConverter.arrayCopy(localBasdbio068TO.getLsInputParams(), Rskdbio068TO.getLsInputParamsSize(), dynamic_lsInputParams, 1);
		}

		}

	protected void call1() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[PolintRecordRec.getPolintRecordSize()];
		byte[] arr_p2 = new byte[10];
		byte[] arr_p3 = new byte[2];
		byte[] arr_p4 = new byte[30];
		DataConverter.writeString(arr_p0, 1, linkReturnCode, 7);
		DataConverter.arrayCopy(polintRecordRec.getPolintRecord(), PolintRecordRec.getPolintRecordSize(), arr_p1, 1);
		DataConverter.writeLong(arr_p2, 1, linkClientSeqNum, 10, SignType.SIGNED_TRAILING);
		DataConverter.writeShort(arr_p3, 1, linkAddrSeqNum, 2, SignType.SIGNED_TRAILING);
		DataConverter.writeString(arr_p4, 1, linkLocateName, 30);
		IProcessInfo<Bascltb14TO> bascltb14 = DynamicCall.getInfoProcessor(Bascltb14.class);
		Bascltb14TO callingBascltb14TO = Bascltb14.getToFromByteArray(arr_p0, arr_p1, arr_p2, arr_p3, arr_p4);
		callingBascltb14TO = bascltb14.run(callingBascltb14TO);
		Bascltb14.setByteArrayFromTo(callingBascltb14TO, arr_p0, arr_p1, arr_p2, arr_p3, arr_p4);
		linkReturnCode = DataConverter.readString(arr_p0, 1, 7);
		polintRecordRec.setPolintRecord(arr_p1);
		linkClientSeqNum = DataConverter.readLong(arr_p2, 1, 10, SignType.SIGNED_TRAILING);
		linkAddrSeqNum = DataConverter.readShort(arr_p3, 1, 2, SignType.SIGNED_TRAILING);
		linkLocateName = DataConverter.readString(arr_p4, 1, 30);
	}

	protected ItemIntsEntryArr getItemIntsEntryArr(int index) {
		if (index > itemIntsEntryArr.size()) {
			// for an element beyond current array limits, all intermediate
			itemIntsEntryArr.ensureCapacity(index);
			for (int i = itemIntsEntryArr.size() + 1; i <= index; i++) {
				itemIntsEntryArr.append(new ItemIntsEntryArr());
				// assign default value
			}
		}
		return itemIntsEntryArr.get((index) - 1);
	}

	public static int getWsInputParamsSize() {
		return 164;
	}

	protected byte[] getWsInputParams() {
		byte[] buf = new byte[getWsInputParamsSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsMasterco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, wsPolicyNum, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, wsModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsIntType, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsIntLoc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, wsIntSeq, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, wsName, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, wsAddress, 30);
		offset += 30;
		DataConverter.arrayCopy(getWsCityState(), 1, getWsCityStateSize(), buf, offset);
		offset += getWsCityStateSize();
		DataConverter.writeString(buf, offset, wsInsLine, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, wsProduct, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, wsRiskloc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, wsSubLoc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, wsUnitNum, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, wsCovg, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, wsCovgSeq, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, wsItemCode, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, wsItemSeq, 5);
		return buf;
	}

	public static int getWsCityStateSize() {
		return 30;
	}

	protected byte[] getWsCityState() {
		byte[] buf = new byte[getWsCityStateSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsCity, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, wsState, 2);
		return buf;
	}

	protected void setWsInputParams(byte[] buf) {
		int offset = 1;
		wsLocation = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsMasterco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsPolicyNum = DataConverter.readString(buf, offset, 7);
		offset += 7;
		wsModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsIntType = DataConverter.readString(buf, offset, 2);
		offset += 2;
		wsIntLoc = DataConverter.readString(buf, offset, 5);
		offset += 5;
		wsIntSeq = DataConverter.readString(buf, offset, 5);
		offset += 5;
		wsName = DataConverter.readString(buf, offset, 30);
		offset += 30;
		wsAddress = DataConverter.readString(buf, offset, 30);
		offset += 30;
		byte[] wsCityState_buf = DataConverter.allocNcopy(buf, offset, getWsCityStateSize());
		setWsCityState(wsCityState_buf);
		offset += getWsCityStateSize();
		wsInsLine = DataConverter.readString(buf, offset, 3);
		offset += 3;
		wsProduct = DataConverter.readString(buf, offset, 6);
		offset += 6;
		wsRiskloc = DataConverter.readString(buf, offset, 5);
		offset += 5;
		wsSubLoc = DataConverter.readString(buf, offset, 5);
		offset += 5;
		wsUnitNum = DataConverter.readString(buf, offset, 5);
		offset += 5;
		wsCovg = DataConverter.readString(buf, offset, 6);
		offset += 6;
		wsCovgSeq = DataConverter.readString(buf, offset, 5);
		offset += 5;
		wsItemCode = DataConverter.readString(buf, offset, 6);
		offset += 6;
		wsItemSeq = DataConverter.readString(buf, offset, 5);
	}

	protected void setWsCityState(byte[] buf) {
		int offset = 1;
		wsCity = DataConverter.readString(buf, offset, 28);
		offset += 28;
		wsState = DataConverter.readString(buf, offset, 2);
	}

	public static int getWsInitializeParamsSize() {
		return 56;
	}

	protected byte[] getWsInitializeParams() {
		byte[] buf = new byte[getWsInitializeParamsSize()];
		int offset = 1;
		DataConverter.writeBigDecimal(buf, offset, wsStatusOwnership, 6, 3, SignType.SIGNED_TRAILING);
		offset += 9;
		DataConverter.writeString(buf, offset, wsStatusTitleC, 2);
		offset += 2;
		DataConverter.writeLong(buf, offset, wsStatusOwnsalary, 20, SignType.SIGNED_TRAILING);
		offset += 20;
		DataConverter.writeString(buf, offset, wsLegalEntity, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsBcState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, wsDeemedCode, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, wsDeemedStatus);
		offset += 1;
		DataConverter.writeString(buf, offset, wsSsnNo, 11);
		offset += 11;
		DataConverter.writeString(buf, offset, linkReturnCode, 7);
		return buf;
	}

	protected void initWsInitializeParams() {
		wsStatusOwnership = BigDecimal.ZERO;
		wsStatusTitleC = "";
		wsStatusOwnsalary = 0;
		wsLegalEntity = "";
		wsBcState = "";
		wsDeemedCode = "";
		wsDeemedStatus = ' ';
		wsSsnNo = "";
		linkReturnCode = "";
	}

	public static int getWsDesc0line4Size() {
		return 30;
	}

	protected byte[] getWsDesc0line4() {
		byte[] buf = new byte[getWsDesc0line4Size()];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsSplitCity, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, wsSplitState, 2);
		return buf;
	}

	protected void setWsDesc0line4(byte[] buf) {
		int offset = 1;
		wsSplitCity = DataConverter.readString(buf, offset, 28);
		offset += 28;
		wsSplitState = DataConverter.readString(buf, offset, 2);
	}

	public static int getWsRecord1501Size() {
		return 90;
	}

	protected byte[] getWsRecord1501() {
		return clt1500recRec2.getClt1500rec();
	}

	protected void setWsRecord1501(byte[] buf) {
		clt1500recRec2.setClt1500rec(buf);
	}
}
