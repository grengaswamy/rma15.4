// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk;

import com.csc.pt.svc.synon.alr.to.Asb1xf1TO;
import com.csc.pt.svc.synon.alr.Asb1xf1;
import com.csc.pt.svc.common.to.Basasb5201TO;
import com.csc.pt.svc.common.Basasb5201;
import com.csc.pt.svc.nav.to.Basasbv101TO;
import com.csc.pt.svc.nav.Basasbv101;
import com.csc.pt.svc.nav.to.Basasbu101TO;
import com.csc.pt.svc.nav.Basasbu101;
import com.csc.pt.svc.nav.to.Basasbj103TO;
import com.csc.pt.svc.nav.Basasbj103;
import com.csc.pt.svc.nav.to.Basasbq102TO;
import bphx.c2ab.util.IProcessInfo;
import com.csc.pt.svc.nav.Basasbq102;
import bphx.c2ab.util.ProcessInfo;
import bphx.c2ab.sql.KeyType;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DataExtConverter;
import bphx.c2ab.util.DynamicCall;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.Types;
import bphx.sdf.collections.IABODynamicArray;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.util.ABOSystem;

import com.csc.pt.svc.rsk.dao.Bassys0400DAO;
import com.csc.pt.svc.rsk.to.Rskclunit1TO;
import com.csc.pt.svc.common.W1HoldLevel;
import com.csc.pt.svc.common.Wc04Rec2;
import com.csc.pt.svc.common.WsFileStatus28;
import com.csc.pt.svc.data.dao.Asbycpl0DAO;
import com.csc.pt.svc.data.dao.Pmslsa0506DAO;
import com.csc.pt.svc.data.dao.Pmslwc0402DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.dao.Tbcl040DAO;
import com.csc.pt.svc.data.to.Asbycpl0TO;
import com.csc.pt.svc.data.to.Pmslsa0506TO;
import com.csc.pt.svc.data.to.Pmslwc0402TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.data.to.Tbcl040TO;
import com.csc.pt.svc.pas.alr.Cobolsqlca;
import bphx.c2ab.data.Initialize;
import com.csc.pt.svc.data.to.Asb5cpl2TO;
import com.csc.pt.svc.data.to.AsbjcppTO;
import com.csc.pt.svc.data.to.AsbqcppTO;
import com.csc.pt.svc.data.to.AsbvcppTO;
import com.csc.pt.svc.data.to.AsbucppTO;
import com.csc.pt.svc.data.to.Asb1cpl4TO;
import com.csc.pt.svc.data.to.Pmspsa15TO;
import com.csc.pt.svc.data.dao.Pmspsa15DAO;
import com.csc.pt.svc.data.to.Bashlgbj00TO;
import com.csc.pt.svc.data.to.Bashlgbq00TO;
import com.csc.pt.svc.data.to.Bashlgbu00TO;
import com.csc.pt.svc.data.to.Bashlgbv00TO;
import com.csc.pt.svc.data.to.Bashlgb500TO;
import com.csc.pt.svc.data.dao.Bashlgbj00DAO;
import com.csc.pt.svc.data.dao.Bashlgbu00DAO;
import com.csc.pt.svc.data.dao.Bashlgbv00DAO;
import com.csc.pt.svc.data.dao.Bashlgb500DAO;
import com.csc.pt.svc.data.dao.Bashlgbq00DAO;

/**
*
*/

public class Rskclunit1 extends ProcessInfo<Rskclunit1TO>  {

	protected Pmslsa0506TO pmspsa05RecTO2 = Initialize.initSpaces(new Pmslsa0506TO());
	protected Wc04Rec2 pmslwc0402RecTO = new Wc04Rec2();
	protected Tbcl040TO tbcl040RecTO = Initialize.initSpaces(new Tbcl040TO());
	protected Asbycpl0TO asbycpl0RecTO = Initialize.initSpaces(new Asbycpl0TO());
	protected Cobolsqlca cobolsqlca = new Cobolsqlca();
	protected WsFileStatus28 wsFileStatus28 = new WsFileStatus28();
	protected char endOfFileSw = ' ';
	protected static final char END_OF_FILE = 'Y';
	protected static final char INVALID_KEY = 'Y';
	protected char foundTrueSw = ' ';
	protected static final char FOUND_TRUE = 'Y';
	protected String w1CaUnitDesc3 = "";
	protected short numOfEntriesForSql = (short) 1;
	protected String w1PpUnitDesc1 = "";
	protected String w1PpUnitDesc2 = "";
	protected String w1PpUnitDesc3 = "";
	protected String w1PpState = "";
	protected char rskclunit1filler1 = ' ';
	protected String w1PpZipCode = "";
	protected short w1ApvYear;
	protected char rskclunit1filler2 = ' ';
	protected String w1ApvMake = "";
	protected String rskclunit1filler3 = " VIN:";
	protected String w1ApvVinNumber = "";
	protected char rskclunit1filler4 = ' ';
	protected String w1ApvState = "";
	protected String w1CppAddr1 = "";
	protected String w1CaUnitDesc1 = "";
	protected String w1CaUnitDesc2 = "";
	protected String w1WcName = "";
	protected String w1WcAddress1 = "";
	protected String w1WcAddress2 = "";
	protected String w1WcCityState = "";
	protected String w1WcZipCode = "";
	protected String w1MrName = "";
	protected String w1MrUnitDesc1 = "";
	protected String w1MrUnitDesc2 = "";
	protected W1HoldLevel w1HoldLevel = new W1HoldLevel();
	protected String w1HoldAsb1Insline = "";
	protected String w1HoldSa05Lob = "";
	protected String w1HoldAlrUnit = "";
	protected boolean wsClearArray = false;
	protected boolean wsRecordFetched = false;
	protected IABODynamicArray<UnitTableEntryArr1> unitTableEntryArr =
		ABOCollectionCreator.getReferenceFactory(UnitTableEntryArr1.class).createDynamicArray(1);
	protected int wsIndex = 1;
	protected String asb1cpl4ReturnCode = "";
	protected Asb1cpl4TO fb1cpj2Rec = Initialize.initSpaces(new Asb1cpl4TO());
	protected char asb1cpl4CallFunction = ' ';
	protected String asbqcpl2ReturnCode = "";
	protected AsbqcppTO fbocpevRec = Initialize.initSpaces(new AsbqcppTO());
	protected char asbqcpl2CallFunction = ' ';
	protected String asbjcpl2ReturnCode = "";
	protected AsbjcppTO fbjcpemRec = Initialize.initSpaces(new AsbjcppTO());
	protected char asbjcpl2CallFunction = ' ';
	protected String asbucpl1ReturnCode = "";
	protected AsbucppTO fclcphtRec = Initialize.initSpaces(new AsbucppTO());
	protected char asbucpl1CallFunction = ' ';
	protected String asbvcpl1ReturnCode = "";
	protected AsbvcppTO fbvcpjcRec = Initialize.initSpaces(new AsbvcppTO());
	protected char asbvcpl1CallFunction = ' ';
	protected String asb5cpl2ReturnCode = "";
	protected Asb5cpl2TO fb5cpmpRec = Initialize.initSpaces(new Asb5cpl2TO());
	protected char asb5cpl2CallFunction = ' ';

	protected Pmslsa0506DAO pmslsa0506DAO = new Pmslsa0506DAO();
	protected Pmslwc0402DAO pmslwc0402DAO = new Pmslwc0402DAO();
	protected Asbycpl0DAO asbycpl0DAO = new Asbycpl0DAO();
	protected Pmsp0200DAO pmsp0200DAO = new Pmsp0200DAO();

	protected Pmslsa0506TO pmslsa0506TO = new Pmslsa0506TO();
	protected Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
	protected Pmslwc0402TO pmslwc0402TO = new Pmslwc0402TO();

	
	protected Tbcl040TO tbcl040TO = new Tbcl040TO();
	protected Asbycpl0TO asbycpl0TO = new Asbycpl0TO();
	private Pmspsa15DAO pmspsa15dao = new Pmspsa15DAO();
	private Pmspsa15TO pmspsa15to = new Pmspsa15TO();
	private short sqlIoOk;
	protected Bashlgbq00TO bashlgbq00TO = new Bashlgbq00TO();
	protected Bashlgbq00DAO bashlgbq00DAO = new Bashlgbq00DAO();
	protected Bashlgbj00TO bashlgbj00TO = new Bashlgbj00TO();
	protected Bashlgbj00DAO bashlgbj00DAO = new Bashlgbj00DAO();
	protected Bashlgbu00TO bashlgbu00TO = new Bashlgbu00TO();
	protected Bashlgbu00DAO bashlgbu00DAO = new Bashlgbu00DAO();
	protected Bashlgbv00TO bashlgbv00TO = new Bashlgbv00TO();
	protected Bashlgbv00DAO bashlgbv00DAO = new Bashlgbv00DAO();
	protected Bashlgb500TO bashlgb500TO = new Bashlgb500TO();
	protected Bashlgb500DAO bashlgb500DAO = new Bashlgb500DAO();
	protected String w1CppAddr2 = "";
	protected boolean recordFetched= true;
	protected String w1CppState="";
	protected String w1CppZipCode="";
	protected String w1CppCity="";
	
	
	protected Rskclunit1 () {
		this.data = new Rskclunit1TO();

		unitTableEntryArr.assign(1 - 1, new UnitTableEntryArr1());
	}
	@Override
	protected void mainSubroutine () {
		main1();
	}

	protected void main1() {
		housekeeping();
		processRecords();
		rng0300CloseFiles();
	}

	protected void housekeeping() {
		cobolsqlca = new Cobolsqlca();
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		endOfFileSw = ' ';
	}

	protected void processRecords() {
		loadSa05();
		numOfEntriesForSql = (short) wsIndex;
		numOfEntriesForSql = (short) ((numOfEntriesForSql - 1) % 10000);
		resultSetTO.addResultSet(unitTableEntryArr, numOfEntriesForSql);
	}

	protected void closeFiles() {
		cobolsqlca.setDBAccessStatus(pmslsa0506DAO.closeCsr());
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca.setDBAccessStatus(pmslwc0402DAO.closeCsr());
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca = new Cobolsqlca();
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		cobolsqlca.setDBAccessStatus(asbycpl0DAO.closeCsr());
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		if (asbqcpl2CallFunction == 'Y') {
			asbqcpl2CallFunction = 'C';
			call1();
		}
		if (asbjcpl2CallFunction == 'Y') {
			asbjcpl2CallFunction = 'C';
			call2();
		}
		if (asbucpl1CallFunction == 'Y') {
			asbucpl1CallFunction = 'C';
			call3();
		}
		if (asbvcpl1CallFunction == 'Y') {
			asbvcpl1CallFunction = 'C';
			call4();
			if (asb5cpl2CallFunction == 'Y') {
				asb5cpl2CallFunction = 'C';
				call5();
			}
		}
	}

	protected void loadSa05() {
		endOfFileSw = ' ';
		pmspsa05RecTO2.setSymbol(this.data.getSymbol());
		pmspsa05RecTO2.setPolicyno(this.data.getPolicyno());
		pmspsa05RecTO2.setModule(this.data.getModule());
		pmspsa05RecTO2.setLocation(this.data.getLocation());
		pmspsa05RecTO2.setMasterco(this.data.getMasterco());
		pmspsa05RecTO2.setUnitno(Types.LOW_STR_VAL);
		cobolsqlca.setDBAccessStatus(pmslsa0506DAO.openPmslsa0506TOCsr(KeyType.GREATER_OR_EQ, pmspsa05RecTO2.getLocation(),
			pmspsa05RecTO2.getPolicyno(), pmspsa05RecTO2.getSymbol(), pmspsa05RecTO2.getMasterco(),
			pmspsa05RecTO2.getModule(), pmspsa05RecTO2.getUnitno()));
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		if (cobolsqlca.isInvalidKey()) {
			endOfFileSw = 'Y';
		}
		if (!(endOfFileSw == END_OF_FILE)) {
			while (!(endOfFileSw == END_OF_FILE)) {
				readSa05();
			}
		}
	}

	protected void readSa05() {
		pmslsa0506TO = pmslsa0506DAO.fetchNext(pmslsa0506TO);
		pmspsa05RecTO2.setData(pmslsa0506TO.getData());
		wsFileStatus28.setValue(Functions.subString(pmslsa0506TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (pmslsa0506TO.getDBAccessStatus().isEOF()) {
			endOfFileSw = 'Y';
			return;
		}
		if (!wsFileStatus28.isGoodRead()) {
			endOfFileSw = 'Y';
			return;
		} else {
			if (pmspsa05RecTO2.getSymbol().compareTo(this.data.getSymbol()) != 0
				|| pmspsa05RecTO2.getPolicyno().compareTo(this.data.getPolicyno()) != 0
				|| pmspsa05RecTO2.getModule().compareTo(this.data.getModule()) != 0
				|| pmspsa05RecTO2.getMasterco().compareTo(this.data.getMasterco()) != 0
				|| pmspsa05RecTO2.getLocation().compareTo(this.data.getLocation()) != 0) {
				endOfFileSw = 'Y';
				return;
			}
		}
		w1HoldSa05Lob = pmspsa05RecTO2.getLob();
		asb1cpl4ReturnCode = "";
		fb1cpj2Rec.setB1aacd(pmspsa05RecTO2.getLocation());
		fb1cpj2Rec.setB1abcd(pmspsa05RecTO2.getMasterco());
		fb1cpj2Rec.setB1artx(pmspsa05RecTO2.getSymbol());
		fb1cpj2Rec.setB1astx(pmspsa05RecTO2.getPolicyno());
		fb1cpj2Rec.setB1adnb(pmspsa05RecTO2.getModule());
		fb1cpj2Rec.setB1j4tx(pmspsa05RecTO2.getUnitno());
		fb1cpj2Rec.setB1bsnb(((short) 1));
		fb1cpj2Rec.setB1cinb(((short) 1));
		asb1cpl4CallFunction = '1';
		call6();
		wsClearArray = false;
		recordFetched = true;
		// End of Retrofit
		// End of Retrofit
		if (asb1cpl4ReturnCode.compareTo("") == 0) {
			//RSK - Start
			if (fb1cpj2Rec.getB1agtx().compareTo("*AL") == 0 || fb1cpj2Rec.getB1aotx().compareTo("00000") == 0){
				getUnitTableEntryArr(wsIndex).setTypeStat('S');
				getUnitTableEntryArr(wsIndex).setUnitNumber(fb1cpj2Rec.getB1j4tx());
			}
			getUnitTableEntryArr(wsIndex).setPremium(pmspsa05RecTO2.getUnitprem());
			getUnitTableEntryArr(wsIndex).setLob(pmspsa05RecTO2.getLob());
			w1HoldAsb1Insline = fb1cpj2Rec.getB1agtx();
			w1HoldAlrUnit = ABOSystem.convertUsingString(fb1cpj2Rec.getB1aenb(), "S99999");
		}
		readCl040();
		if (asb1cpl4ReturnCode.compareTo("") != 0 && !w1HoldLevel.isW1SiteRecord()) {
			w1HoldLevel.setValue('M');
		}
		if (w1HoldLevel.isW1PersonalProperty()) {
			getPersPropertyUnit();
			getUnitTableEntryArr(wsIndex).setUnitName(getW1PpDescriptionAsStr());
			if ((w1PpUnitDesc1.compareTo("") == 0) && (getUnitTableEntryArr(wsIndex).getTypeStat() != 'S')) {
				w1HoldLevel.setValue('P');
			}
		}
		if (w1HoldLevel.isW1VehicleUnit()) {
			getApvUnit();
			getUnitTableEntryArr(wsIndex).setUnitName(getW1ApvDescriptionAsStr());
			if ((w1ApvYear == 0) && (getUnitTableEntryArr(wsIndex).getTypeStat() != 'S')) {
				w1HoldLevel.setValue('A');
			}
		}
		if (w1HoldLevel.isW1CppLocation()) {
			//RSK - Start
			getUnitTableEntryArr(wsIndex).setLocVehSw('P');
			//RSK - End
			getCppLocation();
			if ((w1CppAddr1.compareTo("") == 0) && (getUnitTableEntryArr(wsIndex).getTypeStat() != 'S')) {
				w1HoldLevel.setValue('C');
			}
		}
		if (w1HoldLevel.isW1CppUnit()) {
			//RSK - Start
			getUnitTableEntryArr(wsIndex).setLocVehSw('V');
			//RSK - End
			getCommAuto();
			getUnitTableEntryArr(wsIndex).setUnitName(getW1CaDescriptionAsStr());
			if ((w1CaUnitDesc1.compareTo("") == 0) && (getUnitTableEntryArr(wsIndex).getTypeStat() != 'S')) {
				w1HoldLevel.setValue('F');
			}
		}
		if (w1HoldLevel.isW1SiteRecord()) {
			rng2500GetWorkersComp2500Exit();
			getUnitTableEntryArr(wsIndex).setUnitName(Functions.subString(getW1WcDescriptionAsStr(), 1, 100));
			if ((w1WcName.compareTo("") == 0) && (getUnitTableEntryArr(wsIndex).getTypeStat() != 'S')) {
				w1HoldLevel.setValue('W');
			}
		}
		if (w1HoldLevel.isW1Manual()) {
			readManual();
			getUnitTableEntryArr(wsIndex).setUnitName(getW1MrDescriptionAsStr());
			if (w1MrName.compareTo("") == 0) {
				w1HoldLevel.setValue('M');
				getUnitTableEntryArr(wsIndex).setTypeStat('S');
				getUnitTableEntryArr(wsIndex).setUnitNumber(getUnitTableEntryArr(wsIndex).getStatUnitNo());
			}
		}
		//RSK - Start
		if(w1HoldLevel.isW1Manual()){
			getUnitTableEntryArr(wsIndex).setTypeStat('S');
			getUnitTableEntryArr(wsIndex).setUnitNumber(getUnitTableEntryArr(wsIndex).getStatUnitNo());
		}
		//RSK - End
		if (wsClearArray) {
			getUnitTableEntryArr(wsIndex).setInsuranceLine("");
			getUnitTableEntryArr(wsIndex).setLob("");
			getUnitTableEntryArr(wsIndex).setLocVehSw(' ');
			getUnitTableEntryArr(wsIndex).setPremium(null);
			getUnitTableEntryArr(wsIndex).setProduct("");
			getUnitTableEntryArr(wsIndex).setPropertyaddress("");
			getUnitTableEntryArr(wsIndex).setRatestate("");
			getUnitTableEntryArr(wsIndex).setRiskLoc("");
			getUnitTableEntryArr(wsIndex).setRiskSubLoc("");
			getUnitTableEntryArr(wsIndex).setSitecity("");
			getUnitTableEntryArr(wsIndex).setSitestate("");
			getUnitTableEntryArr(wsIndex).setSitezip("");
			getUnitTableEntryArr(wsIndex).setSitezip("");
			getUnitTableEntryArr(wsIndex).setStatUnitNo("");
			getUnitTableEntryArr(wsIndex).setSumdesc("");
			getUnitTableEntryArr(wsIndex).setTransStatus(' ');
			getUnitTableEntryArr(wsIndex).setTypeStat(' ');
			getUnitTableEntryArr(wsIndex).setUnitName("");
			getUnitTableEntryArr(wsIndex).setUnitNumber("");
			getUnitTableEntryArr(wsIndex).setVehiclemakeyear(0);
			getUnitTableEntryArr(wsIndex).setVehiclemodel("");
			getUnitTableEntryArr(wsIndex).setVin("");
			wsClearArray = true;
		}
		else 
		 if (recordFetched){
		numOfEntriesForSql = (short) ((1 + numOfEntriesForSql) % 10000);
		wsIndex = numOfEntriesForSql;
		// IF NUM-OF-ENTRIES-FOR-SQL = 500
		// 42299A IF NUM-OF-ENTRIES-FOR-SQL > 500
		if (numOfEntriesForSql > 2000) {
			endOfFileSw = 'Y';
		}
		}
	}
	public void moveSa05Array() {
		if (asb1cpl4ReturnCode.compareTo("") == 0) {
			getUnitTableEntryArr(wsIndex).setStatUnitNo(fb1cpj2Rec.getB1j4tx());
			getUnitTableEntryArr(wsIndex).setUnitName(fb1cpj2Rec.getB1pstx());
			getUnitTableEntryArr(wsIndex).setInsuranceLine(fb1cpj2Rec.getB1agtx());
			getUnitTableEntryArr(wsIndex).setRiskLoc(Integer.toString(fb1cpj2Rec.getB1brnb()));
			getUnitTableEntryArr(wsIndex).setRiskSubLoc(Integer.toString(fb1cpj2Rec.getB1egnb()));
			getUnitTableEntryArr(wsIndex).setProduct(fb1cpj2Rec.getB1antx());
			getUnitTableEntryArr(wsIndex).setUnitNumber(ABOSystem.convertUsingString(fb1cpj2Rec.getB1aenb(), "S99999"));
			if (Functions.isAllZeros(getUnitTableEntryArr(wsIndex).getUnitNumber(), 5)) {
				getUnitTableEntryArr(wsIndex).setUnitNumber("");
			}
			fetchLob();
			fetchBassys0400();
		} else {
			getUnitTableEntryArr(wsIndex).setRiskLoc("");
			getUnitTableEntryArr(wsIndex).setRiskSubLoc("");
			getUnitTableEntryArr(wsIndex).setUnitNumber("");
			getUnitTableEntryArr(wsIndex).setProduct("");
			readSa15Record();
			getUnitTableEntryArr(wsIndex).setInsuranceLine(pmspsa05RecTO2.getLob());
		}
	}
	public void readSa15Record() {
		cobolsqlca.setDBAccessStatus(pmspsa15dao.openPmspsa15C12(this.data.getLocation(), this.data.getMasterco(), this.data.getSymbol(), this.data.getPolicyno(), this.data.getModule(), pmspsa05RecTO2.getUnitno()));
		if (cobolsqlca.getSqlCode() == sqlIoOk) {
		readPmspsa15Record();
		}
		cobolsqlca.setDBAccessStatus(pmspsa15dao.closePmspsa15C12());
	}
	public void readPmspsa15Record() {
		pmspsa15to = pmspsa15dao.fetchPmspsa15C12(pmspsa15to);
		cobolsqlca.setDBAccessStatus(pmspsa15to.getDBAccessStatus());
		if (cobolsqlca.isSuccess()) {
			getUnitTableEntryArr(wsIndex).setInsuranceLine(pmspsa15to.getSublob());
		}
	}
	
	//RSK - Start
	protected void fetchLob(){
		pmsp0200TO = pmsp0200DAO.fetchById(pmsp0200TO, "02", this.data.getLocation() , this.data.getPolicyno(), this.data.getSymbol(), this.data.getMasterco(), this.data.getModule(), 'V');
		if (pmsp0200TO.getDBAccessStatus().isSuccess()){
			getUnitTableEntryArr(wsIndex).setLob(pmsp0200TO.getLine0bus());
		}
	}
	protected void fetchBassys0400(){
		String desc;
		desc = Bassys0400DAO.fetchDesc(pmsp0200TO.getLine0bus(), fb1cpj2Rec.getB1agtx());
		if(desc == null){
			desc = Bassys0400DAO.fetchDesc("*AL", fb1cpj2Rec.getB1agtx());
		}
		if(desc == null){
			desc = Bassys0400DAO.fetchDesc("*AL", "*AL");
		}
		if(desc != null){
			getUnitTableEntryArr(wsIndex).setSumdesc(desc);
		}
	}
	//RSK - End
	
	protected void getPersPropertyUnit() {
		// ****************************************************************
		// ****************************************************************
		asbqcpl2ReturnCode = "";
		initAsbqcpl2LinkRecSpaces();
		fbocpevRec.setBqaacd(pmspsa05RecTO2.getLocation());
		fbocpevRec.setBqabcd(pmspsa05RecTO2.getMasterco());
		fbocpevRec.setBqartx(pmspsa05RecTO2.getSymbol());
		fbocpevRec.setBqastx(pmspsa05RecTO2.getPolicyno());
		fbocpevRec.setBqadnb(pmspsa05RecTO2.getModule());
		fbocpevRec.setBqaenb(ABOSystem.convertUsingInt(w1HoldAlrUnit, "D"));
		asbqcpl2CallFunction = 'Y';
		call7();
		if ((!Functions.isAllZeros(asbqcpl2ReturnCode, 7)) || (!fbocpevRec.getBqaacd().equals(pmspsa05RecTO2.getLocation()))
			|| (!fbocpevRec.getBqabcd().equals(pmspsa05RecTO2.getMasterco()))	
			|| (!fbocpevRec.getBqartx().equals(pmspsa05RecTO2.getSymbol()))
			|| (!fbocpevRec.getBqastx().equals(pmspsa05RecTO2.getPolicyno()))
			|| (!fbocpevRec.getBqadnb().equals(pmspsa05RecTO2.getModule()))
			|| (fbocpevRec.getBqaenb()!=(ABOSystem.convertUsingInt(w1HoldAlrUnit, "D")))
		)
		{
			w1HoldLevel.setValue('M');
			getUnitTableEntryArr(wsIndex).setTypeStat('S');
			getUnitTableEntryArr(wsIndex).setUnitNumber(getUnitTableEntryArr(wsIndex).getStatUnitNo());
			return;
		}
		readHistoryPersPropertyUnit();
	}

	protected void readHistoryPersPropertyUnit() {
		bashlgbq00TO =  bashlgbq00DAO.readHistoryFIle(pmspsa05RecTO2.getLocation(), pmspsa05RecTO2.getPolicyno(), pmspsa05RecTO2.getSymbol(), pmspsa05RecTO2.getMasterco(), pmspsa05RecTO2.getModule(),Integer.parseInt(w1HoldAlrUnit),Integer.parseInt(this.data.getLossDate()));
		if (bashlgbq00TO != null)
		{
			recordFetched = true;
			fetchPersonalUnits(); 
		}
		else{
			recordFetched = false;
			}
		}
	protected void fetchPersonalUnits()
	{
		if (bashlgbq00TO.getRecstatus() == 'H')
		{
			if (bashlgbq00TO.getBqafdt() < (Integer.parseInt(this.getData().getLossDate())))
			{
			wsClearArray = true;
			recordFetched = false;
			}
		}
		moveSa05Array();
		w1PpUnitDesc1 = Functions.subString(bashlgbq00TO.getBqdcnb(), 1, 20);
		getUnitTableEntryArr(wsIndex).setUnitName(bashlgbq00TO.getBqdcnb());
		getUnitTableEntryArr(wsIndex).setPropertyaddress(bashlgbq00TO.getBqdcnb());
		w1PpUnitDesc2 = Functions.subString(bashlgbq00TO.getBqddnb(), 1, 20);
		getUnitTableEntryArr(wsIndex).setSitecity(bashlgbq00TO.getBqddnb());
		w1PpState = bashlgbq00TO.getBqbccd();
		getUnitTableEntryArr(wsIndex).setRatestate(bashlgbq00TO.getBqbccd());
		getUnitTableEntryArr(wsIndex).setSitecity(bashlgbq00TO.getBqbccd());
		w1PpZipCode = Functions.subString(bashlgbq00TO.getBqapnb(), 1, 5);
		getUnitTableEntryArr(wsIndex).setTransStatus(bashlgbq00TO.getRecstatus());
		getUnitTableEntryArr(wsIndex).setUnitNumber(Integer.toString(bashlgbq00TO.getUnitno()));
		
	}

	protected void getApvUnit() {
		// ****************************************************************
		// ****************************************************************
		asbjcpl2ReturnCode = "";
		initAsbjcpl2LinkRecSpaces();
		fbjcpemRec.setBjaacd(pmspsa05RecTO2.getLocation());
		fbjcpemRec.setBjabcd(pmspsa05RecTO2.getMasterco());
		fbjcpemRec.setBjartx(pmspsa05RecTO2.getSymbol());
		fbjcpemRec.setBjastx(pmspsa05RecTO2.getPolicyno());
		fbjcpemRec.setBjadnb(pmspsa05RecTO2.getModule());
		fbjcpemRec.setBjaenb(ABOSystem.convertUsingInt(w1HoldAlrUnit, "D"));
		asbjcpl2CallFunction = 'Y';
		call8();
		if ((!Functions.isAllZeros(asbjcpl2ReturnCode, 7))
			|| (!fbjcpemRec.getBjaacd().equals(pmspsa05RecTO2.getLocation()))
			|| (!fbjcpemRec.getBjabcd().equals(pmspsa05RecTO2.getMasterco()))
			|| (!fbjcpemRec.getBjartx().equals(pmspsa05RecTO2.getSymbol()))
			|| (!fbjcpemRec.getBjastx().equals(pmspsa05RecTO2.getPolicyno()))
			|| (!fbjcpemRec.getBjadnb().equals(pmspsa05RecTO2.getModule()))
			|| (fbjcpemRec.getBjaenb() != ABOSystem.convertUsingInt(w1HoldAlrUnit, "D"))
				)
		{
			w1HoldLevel.setValue('M');
			getUnitTableEntryArr(wsIndex).setTypeStat('S');
			getUnitTableEntryArr(wsIndex).setUnitNumber(getUnitTableEntryArr(wsIndex).getStatUnitNo());
			return;
		}
		readHistoryAPVUnit();
	}
	
	protected void readHistoryAPVUnit()
	{
		      
		bashlgbj00TO =  bashlgbj00DAO.readHistoryFIle(pmspsa05RecTO2.getLocation(), pmspsa05RecTO2.getPolicyno(), pmspsa05RecTO2.getSymbol(), pmspsa05RecTO2.getMasterco(), pmspsa05RecTO2.getModule(), Integer.parseInt(w1HoldAlrUnit),Integer.parseInt(this.data.getLossDate()));
		if (bashlgbj00TO != null)
		{
			recordFetched = true;
			fetchAPVUnits(); 
		}
		else{
			recordFetched = false;
		}
	}
	public void fetchAPVUnits()
	{
		if (bashlgbj00TO.getRecstatus() == 'H')
		{
			if (bashlgbj00TO.getBjafdt() < (Integer.parseInt(this.getData().getLossDate())))
			{
			wsClearArray = true;
			recordFetched = false;
			}
		}
		moveSa05Array();
		w1ApvYear = bashlgbj00TO.getBjahnb();
		getUnitTableEntryArr(wsIndex).setVehiclemakeyear(bashlgbj00TO.getBjahnb());
		w1ApvMake = bashlgbj00TO.getBjaxtx();
		getUnitTableEntryArr(wsIndex).setVehiclemodel(bashlgbj00TO.getBjaxtx());
		w1ApvVinNumber = bashlgbj00TO.getBjaytx();
		getUnitTableEntryArr(wsIndex).setUnitName(bashlgbj00TO.getBjaytx());
		getUnitTableEntryArr(wsIndex).setVin(bashlgbj00TO.getBjaytx());
		w1ApvState = bashlgbj00TO.getBjbccd();
		getUnitTableEntryArr(wsIndex).setRatestate(bashlgbj00TO.getBjbccd());
		getUnitTableEntryArr(wsIndex).setTransStatus(bashlgbj00TO.getRecstatus());
		getUnitTableEntryArr(wsIndex).setUnitNumber(Integer.toString(bashlgbj00TO.getUnitno()));
		getUnitTableEntryArr(wsIndex).setUnitName(getW1ApvDescriptionAsStr());
	}

	protected void getCppLocation() {
		// ****************************************************************
		// ****************************************************************
		asbucpl1ReturnCode = "";
		initAsbucpl1LinkRecSpaces();
		fclcphtRec.setBuaacd(pmspsa05RecTO2.getLocation());
		fclcphtRec.setBuabcd(pmspsa05RecTO2.getMasterco());
		fclcphtRec.setBuartx(pmspsa05RecTO2.getSymbol());
		fclcphtRec.setBuastx(pmspsa05RecTO2.getPolicyno());
		fclcphtRec.setBuadnb(pmspsa05RecTO2.getModule());
		fclcphtRec.setBubrnb(fb1cpj2Rec.getB1brnb());
		asbucpl1CallFunction = 'Y';
		call9();
		if ((!Functions.isAllZeros(asbucpl1ReturnCode, 7)) 
			|| (!fclcphtRec.getBuaacd().equals(pmspsa05RecTO2.getLocation()))
			|| (!fclcphtRec.getBuabcd().equals(pmspsa05RecTO2.getMasterco()))
			|| (!fclcphtRec.getBuartx().equals(pmspsa05RecTO2.getSymbol()))
			|| (!fclcphtRec.getBuastx().equals(pmspsa05RecTO2.getPolicyno()))
			|| (!fclcphtRec.getBuadnb().equals(pmspsa05RecTO2.getModule()))
			|| (fclcphtRec.getBubrnb()!= fb1cpj2Rec.getB1brnb()))
		{
			w1HoldLevel.setValue('M');
			getUnitTableEntryArr(wsIndex).setTypeStat('S');
			getUnitTableEntryArr(wsIndex).setUnitNumber(getUnitTableEntryArr(wsIndex).getStatUnitNo());
			return;
		}
		readHistoryLocation();
		getCppSubloc();
	}

	protected void readHistoryLocation()
	{
		
		bashlgbu00TO =  bashlgbu00DAO.readHistoryFIle(pmspsa05RecTO2.getLocation(), pmspsa05RecTO2.getPolicyno(), pmspsa05RecTO2.getSymbol(), pmspsa05RecTO2.getMasterco(), pmspsa05RecTO2.getModule(), fb1cpj2Rec.getB1brnb(),Integer.parseInt(this.data.getLossDate()));
		if (bashlgbu00TO != null)
		{
			recordFetched = true;
			fetchLocationUnits(); 
		}
		else{
			recordFetched = false;
		}
	}
	
	protected void fetchLocationUnits()
	{
		if (bashlgbu00TO.getRecstatus() == 'H')
		{
			if (bashlgbu00TO.getBuafdt() < (Integer.parseInt(this.getData().getLossDate())))
			{
			wsClearArray = true;
			recordFetched = false;
			}
		}
		moveSa05Array();
		w1CppAddr1 = Functions.subString(bashlgbu00TO.getBueftx(), 1, 20);
		getUnitTableEntryArr(wsIndex).setUnitNameFromReference(bashlgbu00TO.getBueftx(), 1, 30);

		getUnitTableEntryArr(wsIndex).setPropertyaddress(bashlgbu00TO.getBueftx());
		w1CppAddr2 = bashlgbu00TO.getBuegtx();
		w1CppCity = bashlgbu00TO.getBueitx();
		getUnitTableEntryArr(wsIndex).setSitecity(bashlgbu00TO.getBueitx());
		w1CppState = bashlgbu00TO.getBuejtx();
		getUnitTableEntryArr(wsIndex).setSitestate(bashlgbu00TO.getBuejtx());
		getUnitTableEntryArr(wsIndex).setRatestate(bashlgbu00TO.getBuejtx());
		w1CppZipCode = bashlgbu00TO.getBuapnb();
		getUnitTableEntryArr(wsIndex).setSitezip(bashlgbu00TO.getBuapnb());
		getUnitTableEntryArr(wsIndex).setTransStatus(bashlgbu00TO.getRecstatus());
		getUnitTableEntryArr(wsIndex).setUnitNumber("00000");
		//RSk - End
	}

	protected void getCppSubloc() {
		// ****************************************************************
		// ****************************************************************
		asbvcpl1ReturnCode = "";
		initAsbvcpl1LinkRecSpaces();
		fbvcpjcRec.setBvaacd(pmspsa05RecTO2.getLocation());
		fbvcpjcRec.setBvabcd(pmspsa05RecTO2.getMasterco());
		fbvcpjcRec.setBvartx(pmspsa05RecTO2.getSymbol());
		fbvcpjcRec.setBvastx(pmspsa05RecTO2.getPolicyno());
		fbvcpjcRec.setBvadnb(pmspsa05RecTO2.getModule());
		fbvcpjcRec.setBvbrnb(fb1cpj2Rec.getB1brnb());
		fbvcpjcRec.setBvegnb(fb1cpj2Rec.getB1egnb());
		asbvcpl1CallFunction = 'Y';
		call10();
		if ((!Functions.isAllZeros(asbvcpl1ReturnCode, 7))
			|| (!fbvcpjcRec.getBvaacd().equals(pmspsa05RecTO2.getLocation()))	
			|| (!fbvcpjcRec.getBvabcd().equals(pmspsa05RecTO2.getMasterco()))
			|| (!fbvcpjcRec.getBvartx().equals(pmspsa05RecTO2.getSymbol()))
			|| (!fbvcpjcRec.getBvastx().equals(pmspsa05RecTO2.getPolicyno()))
			|| (!fbvcpjcRec.getBvadnb().equals(pmspsa05RecTO2.getModule()))
			|| (fbvcpjcRec.getBvbrnb() != fb1cpj2Rec.getB1brnb())
			|| (fbvcpjcRec.getBvegnb() != fb1cpj2Rec.getB1egnb())
		)
		{
			return;
		}
		readHistorySubloc();
	}

	protected void readHistorySubloc()
	{
		
		bashlgbv00TO =  bashlgbv00DAO.readHistoryFIle(pmspsa05RecTO2.getLocation(), pmspsa05RecTO2.getPolicyno(), pmspsa05RecTO2.getSymbol(), pmspsa05RecTO2.getMasterco(), pmspsa05RecTO2.getModule(), fb1cpj2Rec.getB1brnb(),fb1cpj2Rec.getB1egnb(),Integer.parseInt(this.data.getLossDate()));
		if (bashlgbv00TO != null)
		{
			recordFetched = true;
			fetchSubLocUnits(); 
		}
		else{
			recordFetched = false;
		}
		
	}
	protected void fetchSubLocUnits()
	{
		if (bashlgbv00TO.getRecstatus() == 'H')
		{
			if (bashlgbv00TO.getBvafdt() < (Integer.parseInt(this.getData().getLossDate())))
			{
			wsClearArray = true;
			recordFetched = false;
			}
		}
		moveSa05Array();
		getUnitTableEntryArr(wsIndex).setUnitNameFromReference(",", 30, 31);
		w1CppAddr1 = Functions.subString(bashlgbv00TO.getBveftx(), 1, 20);
		getUnitTableEntryArr(wsIndex).setUnitNameFromReference(bashlgbv00TO.getBveftx(), 32, 62);
		w1CppAddr2 = bashlgbv00TO.getBvegtx();
		w1CppCity = bashlgbv00TO.getBveitx();
		w1CppState = bashlgbv00TO.getBvejtx();
		w1CppZipCode = bashlgbv00TO.getBvapnb();
		getUnitTableEntryArr(wsIndex).setTransStatus(bashlgbv00TO.getRecstatus());
	}

	protected void getCommAuto() {
		// ****************************************************************
		// ****************************************************************
		asb5cpl2ReturnCode = "";
		initAsb5cpl2LinkRecSpaces();
		fb5cpmpRec.setB5aacd(pmspsa05RecTO2.getLocation());
		fb5cpmpRec.setB5abcd(pmspsa05RecTO2.getMasterco());
		fb5cpmpRec.setB5artx(pmspsa05RecTO2.getSymbol());
		fb5cpmpRec.setB5astx(pmspsa05RecTO2.getPolicyno());
		fb5cpmpRec.setB5adnb(pmspsa05RecTO2.getModule());
		fb5cpmpRec.setB5agtx(fb1cpj2Rec.getB1agtx());
		fb5cpmpRec.setB5antx(fb1cpj2Rec.getB1antx());
		fb5cpmpRec.setB5brnb(fb1cpj2Rec.getB1brnb());
		fb5cpmpRec.setB5egnb(fb1cpj2Rec.getB1egnb());
		fb5cpmpRec.setB5aenb(ABOSystem.convertUsingInt(w1HoldAlrUnit, "D"));
		asb5cpl2CallFunction = 'Y';
		call11();
		if ((!Functions.isAllZeros(asb5cpl2ReturnCode, 7)) 
			|| (!fb5cpmpRec.getB5aacd().equals(pmspsa05RecTO2.getLocation()))
			|| (!fb5cpmpRec.getB5abcd().equals(pmspsa05RecTO2.getMasterco()))	
			|| (!fb5cpmpRec.getB5artx().equals(pmspsa05RecTO2.getSymbol()))
			|| (!fb5cpmpRec.getB5astx().equals(pmspsa05RecTO2.getPolicyno()))
			|| (!fb5cpmpRec.getB5adnb().equals(pmspsa05RecTO2.getModule()))
			|| (!fb5cpmpRec.getB5agtx().equals(fb1cpj2Rec.getB1agtx()))
			|| (!fb5cpmpRec.getB5antx().equals(fb1cpj2Rec.getB1antx()))
			|| (fb5cpmpRec.getB5brnb() != fb1cpj2Rec.getB1brnb())
			|| (fb5cpmpRec.getB5egnb() != fb1cpj2Rec.getB1egnb())
			|| (fb5cpmpRec.getB5aenb() != ABOSystem.convertUsingInt(w1HoldAlrUnit, "D"))
			)
		{
			w1HoldLevel.setValue('M');
			getUnitTableEntryArr(wsIndex).setTypeStat('S');
			getUnitTableEntryArr(wsIndex).setUnitNumber(getUnitTableEntryArr(wsIndex).getStatUnitNo());
			return;
		}
		readHistoryCommAuto();
		rng2450ReadAbycppRec();
	}

	protected void readHistoryCommAuto()
	{
		
		bashlgb500TO =  bashlgb500DAO.readHistoryFIle(pmspsa05RecTO2.getLocation(), pmspsa05RecTO2.getPolicyno(), pmspsa05RecTO2.getSymbol(), pmspsa05RecTO2.getMasterco(), pmspsa05RecTO2.getModule(),w1HoldAlrUnit, fb1cpj2Rec.getB1brnb(),fb1cpj2Rec.getB1egnb(),fb1cpj2Rec.getB1agtx(),fb1cpj2Rec.getB1antx(),Integer.parseInt(this.data.getLossDate()));
		if (bashlgb500TO != null)
		{
			recordFetched = true;
			fetchUnits(); 
		}
		else{
			recordFetched = false;
		}
		
	}
	protected void fetchUnits()
	{
		if (bashlgb500TO.getRecstatus() == 'H')
		{
			if (bashlgb500TO.getB5afdt() < (Integer.parseInt(this.getData().getLossDate())))
			{
			wsClearArray = true;
			recordFetched = false;
			}
		}
		moveSa05Array();

		w1CaUnitDesc1 = bashlgb500TO.getB5dcnb();
		w1CaUnitDesc2 = bashlgb500TO.getB5ddnb();
		getUnitTableEntryArr(wsIndex).setUnitName(bashlgb500TO.getB5ddnb());
		//RSK - Start
		getUnitTableEntryArr(wsIndex).setTransStatus(bashlgb500TO.getRecstatus());
		getUnitTableEntryArr(wsIndex).setUnitNumber(Integer.toString(bashlgb500TO.getUnitno()));
		//RSK - End
		getUnitTableEntryArr(wsIndex).setVin(bashlgb500TO.getB5ddnb());
	}

	protected void exit1() {
	// exit
	}

	protected String readAbycppRec() {
		String retcode = "";
		Initialize.initLowValues(asbycpl0RecTO);
		asbycpl0RecTO.setByaacd(pmspsa05RecTO2.getLocation());
		asbycpl0RecTO.setByabcd(pmspsa05RecTO2.getMasterco());
		asbycpl0RecTO.setByartx(pmspsa05RecTO2.getSymbol());
		asbycpl0RecTO.setByastx(pmspsa05RecTO2.getPolicyno());
		asbycpl0RecTO.setByadnb(pmspsa05RecTO2.getModule());
		asbycpl0RecTO.setByagtx(fb1cpj2Rec.getB1agtx());
		asbycpl0RecTO.setByantx(fb1cpj2Rec.getB1antx());
		asbycpl0RecTO.setBybrnb(fb1cpj2Rec.getB1brnb());
		asbycpl0RecTO.setByegnb(fb1cpj2Rec.getB1egnb());
		asbycpl0RecTO.setByaenb(ABOSystem.convertUsingInt(w1HoldAlrUnit, "D"));
		cobolsqlca.setDBAccessStatus(asbycpl0DAO.openAsbycpl0TOCsr(KeyType.GREATER_OR_EQ, asbycpl0RecTO.getByaacd(),
			asbycpl0RecTO.getByabcd(), asbycpl0RecTO.getByartx(), asbycpl0RecTO.getByastx(),
			asbycpl0RecTO.getByadnb(), asbycpl0RecTO.getByagtx(), asbycpl0RecTO.getBybrnb(),
			asbycpl0RecTO.getByegnb(), asbycpl0RecTO.getByantx(), asbycpl0RecTO.getByaenb(),
			asbycpl0RecTO.getByaotx(), asbycpl0RecTO.getByc0nb(), asbycpl0RecTO.getByc6st()));
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		if (cobolsqlca.isInvalidKey()) {
			return "";
		}
		foundTrueSw = ' ';
		while (!(foundTrueSw == FOUND_TRUE)) {
			retcode = rng2460ReadAbycppRec2460ExitM2450Exit();
			if (retcode.compareTo("") != 0) {
				return retcode;
			}
		}
		return "";
	}

	protected void exit2() {
	// exit
	}

	protected String readAbycppRec1() {
		asbycpl0TO = asbycpl0DAO.fetchNext(asbycpl0TO);
		asbycpl0RecTO.setData(asbycpl0TO.getData());
		asbycpl0RecTO.setData(asbycpl0RecTO.getData());
		wsFileStatus28.setValue(Functions.subString(asbycpl0TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (pmspsa05RecTO2.getLocation().compareTo(asbycpl0RecTO.getByaacd()) == 0
			&& pmspsa05RecTO2.getMasterco().compareTo(asbycpl0RecTO.getByabcd()) == 0
			&& pmspsa05RecTO2.getSymbol().compareTo(asbycpl0RecTO.getByartx()) == 0
			&& pmspsa05RecTO2.getPolicyno().compareTo(asbycpl0RecTO.getByastx()) == 0
			&& pmspsa05RecTO2.getModule().compareTo(asbycpl0RecTO.getByadnb()) == 0
			&& fb1cpj2Rec.getB1agtx().compareTo(asbycpl0RecTO.getByagtx()) == 0
			&& fb1cpj2Rec.getB1antx().compareTo(asbycpl0RecTO.getByantx()) == 0
			&& fb1cpj2Rec.getB1brnb() == asbycpl0RecTO.getBybrnb() && fb1cpj2Rec.getB1egnb() == asbycpl0RecTO.getByegnb()
			&& w1HoldAlrUnit.compareTo(ABOSystem.convertUsingString(asbycpl0RecTO.getByaenb(), "S99999")) == 0) {
			foundTrueSw = 'Y';
		} else {
			foundTrueSw = ' ';
			return "2450-EXIT";
		}
		if (foundTrueSw == FOUND_TRUE) {
			//RSK - Start
			getUnitTableEntryArr(wsIndex).setRatestate(asbycpl0RecTO.getBybccd());
			//RSK - End
			if (asbycpl0RecTO.getByeetx().compareTo(" I") == 0 || asbycpl0RecTO.getByeetx().compareTo(" I D") == 0
				|| asbycpl0RecTO.getByeetx().compareTo(" I       G") == 0) {
				initW1CaDescriptionSpaces();
				if (asbycpl0RecTO.getBykktx().compareTo("") == 0) {
					w1CaUnitDesc3 = "Policy Coverages";
				} else {
					w1CaUnitDesc3 = asbycpl0RecTO.getBykktx();
				}
				setW1CaDescription(DataExtConverter.strToBytes(w1CaUnitDesc3, 60));
			}
		}
		return "";
	}

	protected void exit3() {
	// exit
	}

	protected String getWorkersComp() {
		String retcode = "";
		// ****************************************************************
		// ****************************************************************
		pmslwc0402RecTO.setLocation(pmspsa05RecTO2.getLocation());
		pmslwc0402RecTO.setMasterco(pmspsa05RecTO2.getMasterco());
		pmslwc0402RecTO.setSymbol(pmspsa05RecTO2.getSymbol());
		pmslwc0402RecTO.setPolicyno(pmspsa05RecTO2.getPolicyno());
		pmslwc0402RecTO.setModule(pmspsa05RecTO2.getModule());
		pmslwc0402RecTO.setSite(pmspsa05RecTO2.getUnitno());
		pmslwc0402RecTO.setState("00");
		cobolsqlca.setDBAccessStatus(pmslwc0402DAO.openPmslwc0402TOCsr(KeyType.GREATER_OR_EQ, pmslwc0402RecTO.getLocation(),
			pmslwc0402RecTO.getPolicyno(), pmslwc0402RecTO.getSymbol(), pmslwc0402RecTO.getMasterco(),
			pmslwc0402RecTO.getModule(), pmslwc0402RecTO.getSite(), pmslwc0402RecTO.getState()));
		wsFileStatus28.setValue(Functions.subString(cobolsqlca.getFileIOCode(), 1, 2));
		if (cobolsqlca.isInvalidKey()) {
			w1HoldLevel.setValue('M');
			getUnitTableEntryArr(wsIndex).setTypeStat('S');
			getUnitTableEntryArr(wsIndex).setUnitNumber(getUnitTableEntryArr(wsIndex).getStatUnitNo());
			return "";
		}
		retcode = rng2600ReadWorkersComp2600ExitM2450Exit2500Exit();
		if (retcode.compareTo("") != 0) {
			return retcode;
		}
		return "";
	}

	protected void exit4() {
	// exit
	}
	
	protected void exit5() {
		// exit
		}

	protected String readWorkersComp() {
		// ****************************************************************
		// -MOVE THE SITE NUMBER, NAME, ADDRESS, CITY, STATE & ZIP TO
		// ****************************************************************
		pmslwc0402TO = pmslwc0402DAO.fetchNext(pmslwc0402TO);
		pmslwc0402RecTO.setWc04(pmslwc0402TO.getData());
		wsFileStatus28.setValue(Functions.subString(pmslwc0402TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (pmslwc0402TO.getDBAccessStatus().isEOF()) {
			w1HoldLevel.setValue('M');
			getUnitTableEntryArr(wsIndex).setTypeStat('S');
			getUnitTableEntryArr(wsIndex).setUnitNumber(getUnitTableEntryArr(wsIndex).getStatUnitNo());
			return "2400-EXIT";
		}
		//RSK - Start
		getUnitTableEntryArr(wsIndex).setStatUnitNo(pmslwc0402RecTO.getSite());
		getUnitTableEntryArr(wsIndex).setUnitNumber(pmslwc0402RecTO.getSite());
		//RSK - End
		w1WcName = pmslwc0402RecTO.getName();
		//RSK - Start
		getUnitTableEntryArr(wsIndex).setUnitName(pmslwc0402RecTO.getName());
		//RSK - End
		w1WcAddress1 = Functions.subString(pmslwc0402RecTO.getAddress1(), 1, 20);
		w1WcAddress2 = Functions.subString(pmslwc0402RecTO.getAddress2(), 1, 20);
		//RSK - Start
		getUnitTableEntryArr(wsIndex).setPropertyaddress(w1WcAddress2);
		//RSK - End
		w1WcCityState = pmslwc0402RecTO.getCityst();
		//RSK - Start
		if (pmslwc0402RecTO.getCityst().length() == 30){
			
			getUnitTableEntryArr(wsIndex).setSitecity(pmslwc0402RecTO.getCityst().substring(1, 28));
			getUnitTableEntryArr(wsIndex).setSitestate(pmslwc0402RecTO.getCityst().substring(29, 30));
			
		}else{
			getUnitTableEntryArr(wsIndex).setSitecity(pmslwc0402RecTO.getCityst());
		}
		//RSK - End
		w1WcZipCode = Functions.subString(pmslwc0402RecTO.getZip(), 1, 5);
		//RSK - Start
		getUnitTableEntryArr(wsIndex).setSitezip(w1WcZipCode);
		getUnitTableEntryArr(wsIndex).setRatestate(pmslwc0402RecTO.getState());
		getUnitTableEntryArr(wsIndex).setTransStatus('V');
		//RSK - End
		return "";
	}

	protected void readManual() {
		// ****************************************************************
		// ****************************************************************
		//RSK - Start
		getUnitTableEntryArr(wsIndex).setStatUnitNo(pmspsa05RecTO2.getUnitno());
		getUnitTableEntryArr(wsIndex).setTransStatus('V');
		//RSK - End
		getUnitTableEntryArr(wsIndex).setPremium(pmspsa05RecTO2.getUnitprem());
		w1MrName = pmspsa05RecTO2.getName();
		getUnitTableEntryArr(wsIndex).setUnitName(pmspsa05RecTO2.getName());
		w1MrUnitDesc1 = pmspsa05RecTO2.getDescuse1();
		w1MrUnitDesc2 = pmspsa05RecTO2.getDescuse2();
		//RSK - Start
		getUnitTableEntryArr(wsIndex).setPropertyaddress(pmspsa05RecTO2.getDescuse1());
		if (pmspsa05RecTO2.getDescuse2().length() == 30){
			getUnitTableEntryArr(wsIndex).setSitecity(pmspsa05RecTO2.getDescuse2().substring(1, 28));
			getUnitTableEntryArr(wsIndex).setSitestate(pmspsa05RecTO2.getDescuse2().substring(29, 30));
		}else{
			getUnitTableEntryArr(wsIndex).setSitecity(pmspsa05RecTO2.getDescuse2());
		}
		//RSK - End
	}

	protected void readCl040() {
		// ************************************************************
		// ************************************************************
		if (asb1cpl4ReturnCode.compareTo("") == 0) {
			tbcl040RecTO.setLob("");
			tbcl040RecTO.setInsline(w1HoldAsb1Insline);
		} else {
			tbcl040RecTO.setLob(w1HoldSa05Lob);
			tbcl040RecTO.setInsline("");
		}
		tbcl040TO = Tbcl040DAO.selectById(tbcl040TO, tbcl040RecTO.getLob(), tbcl040RecTO.getInsline());
		if (tbcl040TO.getDBAccessStatus().isSuccess()) {
			tbcl040RecTO.setData(tbcl040TO.getData());
		}
		wsFileStatus28.setValue(Functions.subString(tbcl040TO.getDBAccessStatus().getFileIOCode(), 1, 2));
		if (tbcl040TO.getDBAccessStatus().isInvalidKey()) {
		}
		if (!wsFileStatus28.isGoodRead()) {
			tbcl040RecTO.setLob(w1HoldSa05Lob);
			tbcl040TO = Tbcl040DAO.selectById(tbcl040TO, tbcl040RecTO.getLob(), tbcl040RecTO.getInsline());
			if (tbcl040TO.getDBAccessStatus().isSuccess()) {
				tbcl040RecTO.setData(tbcl040TO.getData());
			}
			wsFileStatus28.setValue(Functions.subString(tbcl040TO.getDBAccessStatus().getFileIOCode(), 1, 2));
			if (tbcl040TO.getDBAccessStatus().isInvalidKey()) {
				w1HoldLevel.setValue('M');
			}
		}
		if (wsFileStatus28.isGoodRead()) {
			w1HoldLevel.setValue(tbcl040RecTO.getLevel());
		} else {
			w1HoldLevel.setValue('M');
		}
	}

	protected void rng0300CloseFiles() {
		closeFiles();
	}

	protected void rng2450ReadAbycppRec() {
		readAbycppRec();
		exit2();
	}

	public static Rskclunit1TO getToFromByteArray(byte[] dynamic_claimData)
	{
		Rskclunit1TO localRskclunit1TO = new Rskclunit1TO();
		localRskclunit1TO.setClaimData(dynamic_claimData);
		return localRskclunit1TO;
	}

	public static byte[] setByteArrayFromTo(Rskclunit1TO localRskclunit1TO, byte[] dynamic_claimData) {
		DataConverter.arrayCopy(localRskclunit1TO.getClaimData(), localRskclunit1TO.getClaimDataSize(), dynamic_claimData, 1);
		return localRskclunit1TO.getClaimData();
	}

	protected void call1() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbqcpl2LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asbqcpl2ReturnCode, 7);
		DataConverter.arrayCopy(getAsbqcpl2LinkRec(), getAsbqcpl2LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asbqcpl2CallFunction);
		IProcessInfo<Basasbq102TO> basasbq102 = DynamicCall.getInfoProcessor(Basasbq102.class);
		Basasbq102TO callingBasasbq102TO = Basasbq102.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasbq102TO = basasbq102.run(callingBasasbq102TO);
		Basasbq102.setByteArrayFromTo(callingBasasbq102TO, arr_p0, arr_p1, arr_p2);
		asbqcpl2ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbqcpl2LinkRec(arr_p1);
		asbqcpl2CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call2() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbjcpl2LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asbjcpl2ReturnCode, 7);
		DataConverter.arrayCopy(getAsbjcpl2LinkRec(), getAsbjcpl2LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asbjcpl2CallFunction);
		IProcessInfo<Basasbj103TO> basasbj103 = DynamicCall.getInfoProcessor(Basasbj103.class);
		Basasbj103TO callingBasasbj103TO = Basasbj103.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasbj103TO = basasbj103.run(callingBasasbj103TO);
		Basasbj103.setByteArrayFromTo(callingBasasbj103TO, arr_p0, arr_p1, arr_p2);
		asbjcpl2ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbjcpl2LinkRec(arr_p1);
		asbjcpl2CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call3() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbucpl1LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asbucpl1ReturnCode, 7);
		DataConverter.arrayCopy(getAsbucpl1LinkRec(), getAsbucpl1LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asbucpl1CallFunction);
		IProcessInfo<Basasbu101TO> basasbu101 = DynamicCall.getInfoProcessor(Basasbu101.class);
		Basasbu101TO callingBasasbu101TO = Basasbu101.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasbu101TO = basasbu101.run(callingBasasbu101TO);
		Basasbu101.setByteArrayFromTo(callingBasasbu101TO, arr_p0, arr_p1, arr_p2);
		asbucpl1ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbucpl1LinkRec(arr_p1);
		asbucpl1CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call4() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbvcpl1LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asbvcpl1ReturnCode, 7);
		DataConverter.arrayCopy(getAsbvcpl1LinkRec(), getAsbvcpl1LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asbvcpl1CallFunction);
		IProcessInfo<Basasbv101TO> basasbv101 = DynamicCall.getInfoProcessor(Basasbv101.class);
		Basasbv101TO callingBasasbv101TO = Basasbv101.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasbv101TO = basasbv101.run(callingBasasbv101TO);
		Basasbv101.setByteArrayFromTo(callingBasasbv101TO, arr_p0, arr_p1, arr_p2);
		asbvcpl1ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbvcpl1LinkRec(arr_p1);
		asbvcpl1CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call5() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsb5cpl2LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asb5cpl2ReturnCode, 7);
		DataConverter.arrayCopy(getAsb5cpl2LinkRec(), getAsb5cpl2LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asb5cpl2CallFunction);
		IProcessInfo<Basasb5201TO> basasb5201 = DynamicCall.getInfoProcessor(Basasb5201.class);
		Basasb5201TO callingBasasb5201TO = Basasb5201.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasb5201TO = basasb5201.run(callingBasasb5201TO);
		Basasb5201.setByteArrayFromTo(callingBasasb5201TO, arr_p0, arr_p1, arr_p2);
		asb5cpl2ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsb5cpl2LinkRec(arr_p1);
		asb5cpl2CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call6() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsb1cpl4LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asb1cpl4ReturnCode, 7);
		DataConverter.arrayCopy(getAsb1cpl4LinkRec(), getAsb1cpl4LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asb1cpl4CallFunction);
		IProcessInfo<Asb1xf1TO> asb1xf1 = DynamicCall.getInfoProcessor(Asb1xf1.class);
		Asb1xf1TO callingAsb1xf1TO = Asb1xf1.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingAsb1xf1TO = asb1xf1.run(callingAsb1xf1TO);
		Asb1xf1.setByteArrayFromTo(callingAsb1xf1TO, arr_p0, arr_p1, arr_p2);
		asb1cpl4ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsb1cpl4LinkRec(arr_p1);
		asb1cpl4CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call7() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbqcpl2LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asbqcpl2ReturnCode, 7);
		DataConverter.arrayCopy(getAsbqcpl2LinkRec(), getAsbqcpl2LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asbqcpl2CallFunction);
		IProcessInfo<Basasbq102TO> basasbq102 = DynamicCall.getInfoProcessor(Basasbq102.class);
		Basasbq102TO callingBasasbq102TO = Basasbq102.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasbq102TO = basasbq102.run(callingBasasbq102TO);
		Basasbq102.setByteArrayFromTo(callingBasasbq102TO, arr_p0, arr_p1, arr_p2);
		asbqcpl2ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbqcpl2LinkRec(arr_p1);
		asbqcpl2CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call8() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbjcpl2LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asbjcpl2ReturnCode, 7);
		DataConverter.arrayCopy(getAsbjcpl2LinkRec(), getAsbjcpl2LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asbjcpl2CallFunction);
		IProcessInfo<Basasbj103TO> basasbj103 = DynamicCall.getInfoProcessor(Basasbj103.class);
		Basasbj103TO callingBasasbj103TO = Basasbj103.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasbj103TO = basasbj103.run(callingBasasbj103TO);
		Basasbj103.setByteArrayFromTo(callingBasasbj103TO, arr_p0, arr_p1, arr_p2);
		asbjcpl2ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbjcpl2LinkRec(arr_p1);
		asbjcpl2CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call9() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbucpl1LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asbucpl1ReturnCode, 7);
		DataConverter.arrayCopy(getAsbucpl1LinkRec(), getAsbucpl1LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asbucpl1CallFunction);
		IProcessInfo<Basasbu101TO> basasbu101 = DynamicCall.getInfoProcessor(Basasbu101.class);
		Basasbu101TO callingBasasbu101TO = Basasbu101.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasbu101TO = basasbu101.run(callingBasasbu101TO);
		Basasbu101.setByteArrayFromTo(callingBasasbu101TO, arr_p0, arr_p1, arr_p2);
		asbucpl1ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbucpl1LinkRec(arr_p1);
		asbucpl1CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call10() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsbvcpl1LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asbvcpl1ReturnCode, 7);
		DataConverter.arrayCopy(getAsbvcpl1LinkRec(), getAsbvcpl1LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asbvcpl1CallFunction);
		IProcessInfo<Basasbv101TO> basasbv101 = DynamicCall.getInfoProcessor(Basasbv101.class);
		Basasbv101TO callingBasasbv101TO = Basasbv101.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasbv101TO = basasbv101.run(callingBasasbv101TO);
		Basasbv101.setByteArrayFromTo(callingBasasbv101TO, arr_p0, arr_p1, arr_p2);
		asbvcpl1ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsbvcpl1LinkRec(arr_p1);
		asbvcpl1CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected void call11() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[getAsb5cpl2LinkRecSize()];
		byte[] arr_p2 = new byte[1];
		DataConverter.writeString(arr_p0, 1, asb5cpl2ReturnCode, 7);
		DataConverter.arrayCopy(getAsb5cpl2LinkRec(), getAsb5cpl2LinkRecSize(), arr_p1, 1);
		DataConverter.writeChar(arr_p2, 1, asb5cpl2CallFunction);
		IProcessInfo<Basasb5201TO> basasb5201 = DynamicCall.getInfoProcessor(Basasb5201.class);
		Basasb5201TO callingBasasb5201TO = Basasb5201.getToFromByteArray(arr_p0, arr_p1, arr_p2);
		callingBasasb5201TO = basasb5201.run(callingBasasb5201TO);
		Basasb5201.setByteArrayFromTo(callingBasasb5201TO, arr_p0, arr_p1, arr_p2);
		asb5cpl2ReturnCode = DataConverter.readString(arr_p0, 1, 7);
		setAsb5cpl2LinkRec(arr_p1);
		asb5cpl2CallFunction = DataConverter.readChar(arr_p2, 1);
	}

	protected UnitTableEntryArr1 getUnitTableEntryArr(int index) {
		if (index > unitTableEntryArr.size()) {
			// for an element beyond current array limits, all intermediate
			unitTableEntryArr.ensureCapacity(index);
			for (int i = unitTableEntryArr.size() + 1; i <= index; i++) {
				unitTableEntryArr.append(new UnitTableEntryArr1());
				// assign default value
			}
		}
		return unitTableEntryArr.get(index - 1);
	}

	protected int getAsbqcpl2LinkRecSize() {
		return 687;
	}

	protected byte[] getAsbqcpl2LinkRec() {
		byte[] buf = new byte[getAsbqcpl2LinkRecSize()];
		int offset = 1;
		DataConverter.arrayCopy(fbocpevRec.getData(), 1, AsbqcppTO.getSize(), buf, offset);
		return buf;
	}

	protected void setAsbqcpl2LinkRec(byte[] buf) {
		int offset = 1;
		byte[] fbocpev_buf = DataConverter.allocNcopy(buf, offset, AsbqcppTO.getSize());
		fbocpevRec.setData(fbocpev_buf);
	}

	protected int getAsbjcpl2LinkRecSize() {
		return 462;
	}

	protected byte[] getAsbjcpl2LinkRec() {
		byte[] buf = new byte[getAsbjcpl2LinkRecSize()];
		int offset = 1;
		DataConverter.arrayCopy(fbjcpemRec.getData(), 1, AsbjcppTO.getSize(), buf, offset);
		return buf;
	}

	protected void setAsbjcpl2LinkRec(byte[] buf) {
		int offset = 1;
		byte[] fbjcpem_buf = DataConverter.allocNcopy(buf, offset, AsbjcppTO.getSize());
		fbjcpemRec.setData(fbjcpem_buf);
	}

	protected int getAsbucpl1LinkRecSize() {
		return 148;
	}

	protected byte[] getAsbucpl1LinkRec() {
		byte[] buf = new byte[getAsbucpl1LinkRecSize()];
		int offset = 1;
		DataConverter.arrayCopy(fclcphtRec.getData(), 1, AsbucppTO.getSize(), buf, offset);
		return buf;
	}

	protected void setAsbucpl1LinkRec(byte[] buf) {
		int offset = 1;
		byte[] fclcpht_buf = DataConverter.allocNcopy(buf, offset, AsbucppTO.getSize());
		fclcphtRec.setData(fclcpht_buf);
	}

	protected int getAsbvcpl1LinkRecSize() {
		return 153;
	}

	protected byte[] getAsbvcpl1LinkRec() {
		byte[] buf = new byte[getAsbvcpl1LinkRecSize()];
		int offset = 1;
		DataConverter.arrayCopy(fbvcpjcRec.getData(), 1, AsbvcppTO.getSize(), buf, offset);
		return buf;
	}

	protected void setAsbvcpl1LinkRec(byte[] buf) {
		int offset = 1;
		byte[] fbvcpjc_buf = DataConverter.allocNcopy(buf, offset, AsbvcppTO.getSize());
		fbvcpjcRec.setData(fbvcpjc_buf);
	}

	protected int getAsb5cpl2LinkRecSize() {
		return 1200;
	}

	protected byte[] getAsb5cpl2LinkRec() {
		byte[] buf = new byte[getAsb5cpl2LinkRecSize()];
		int offset = 1;
		DataConverter.arrayCopy(fb5cpmpRec.getData(), 1, Asb5cpl2TO.getSize(), buf, offset);
		return buf;
	}

	protected void setAsb5cpl2LinkRec(byte[] buf) {
		int offset = 1;
		byte[] fb5cpmp_buf = DataConverter.allocNcopy(buf, offset, Asb5cpl2TO.getSize());
		fb5cpmpRec.setData(fb5cpmp_buf);
	}

	protected int getAsb1cpl4LinkRecSize() {
		return 153;
	}

	protected byte[] getAsb1cpl4LinkRec() {
		byte[] buf = new byte[getAsb1cpl4LinkRecSize()];
		int offset = 1;
		DataConverter.arrayCopy(fb1cpj2Rec.getData(), 1, Asb1cpl4TO.getSize(), buf, offset);
		return buf;
	}

	protected void setAsb1cpl4LinkRec(byte[] buf) {
		int offset = 1;
		byte[] fb1cpj2_buf = DataConverter.allocNcopy(buf, offset, Asb1cpl4TO.getSize());
		fb1cpj2Rec.setData(fb1cpj2_buf);
	}

	protected int getW1PpDescriptionSize() {
		return 68;
	}

	protected byte[] getW1PpDescription() {
		byte[] buf = new byte[getW1PpDescriptionSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, w1PpUnitDesc1, 20);
		offset += 20;
		DataConverter.writeString(buf, offset, w1PpUnitDesc2, 20);
		offset += 20;
		DataConverter.writeString(buf, offset, w1PpUnitDesc3, 20);
		offset += 20;
		DataConverter.writeString(buf, offset, w1PpState, 2);
		offset += 2;
		DataConverter.writeChar(buf, offset, rskclunit1filler1);
		offset += 1;
		DataConverter.writeString(buf, offset, w1PpZipCode, 5);
		return buf;
	}

	protected String getW1PpDescriptionAsStr() {
		return DataConverter.readString(getW1PpDescription(), 1, getW1PpDescription().length);
	}

	protected int getW1ApvDescriptionSize() {
		return 56;
	}

	protected byte[] getW1ApvDescription() {
		byte[] buf = new byte[getW1ApvDescriptionSize()];
		int offset = 1;
		DataConverter.writeShort(buf, offset, w1ApvYear, 4);
		offset += 4;
		DataConverter.writeChar(buf, offset, rskclunit1filler2);
		offset += 1;
		DataConverter.writeString(buf, offset, w1ApvMake, 25);
		offset += 25;
		DataConverter.writeString(buf, offset, rskclunit1filler3, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, w1ApvVinNumber, 17);
		offset += 17;
		DataConverter.writeChar(buf, offset, rskclunit1filler4);
		offset += 1;
		DataConverter.writeString(buf, offset, w1ApvState, 2);
		return buf;
	}

	protected String getW1ApvDescriptionAsStr() {
		return DataConverter.readString(getW1ApvDescription(), 1, getW1ApvDescription().length);
	}

	protected int getW1CaDescriptionSize() {
		return 60;
	}

	protected byte[] getW1CaDescription() {
		byte[] buf = new byte[getW1CaDescriptionSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, w1CaUnitDesc1, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, w1CaUnitDesc2, 30);
		return buf;
	}

	protected String getW1CaDescriptionAsStr() {
		return DataConverter.readString(getW1CaDescription(), 1, getW1CaDescription().length);
	}

	protected int getW1WcDescriptionSize() {
		return 105;
	}

	protected byte[] getW1WcDescription() {
		byte[] buf = new byte[getW1WcDescriptionSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, w1WcName, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, w1WcAddress1, 20);
		offset += 20;
		DataConverter.writeString(buf, offset, w1WcAddress2, 20);
		offset += 20;
		DataConverter.writeString(buf, offset, w1WcCityState, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, w1WcZipCode, 5);
		return buf;
	}

	protected String getW1WcDescriptionAsStr() {
		return DataConverter.readString(getW1WcDescription(), 1, getW1WcDescription().length);
	}

	protected int getW1MrDescriptionSize() {
		return 90;
	}

	protected byte[] getW1MrDescription() {
		byte[] buf = new byte[getW1MrDescriptionSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, w1MrName, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, w1MrUnitDesc1, 30);
		offset += 30;
		DataConverter.writeString(buf, offset, w1MrUnitDesc2, 30);
		return buf;
	}

	protected String getW1MrDescriptionAsStr() {
		return DataConverter.readString(getW1MrDescription(), 1, getW1MrDescription().length);
	}

	protected void initAsbqcpl2LinkRecSpaces() {
		Initialize.initSpaces(fbocpevRec);
	}

	protected void initAsbjcpl2LinkRecSpaces() {
		Initialize.initSpaces(fbjcpemRec);
	}

	protected void initAsbucpl1LinkRecSpaces() {
		Initialize.initSpaces(fclcphtRec);
	}

	protected void initAsbvcpl1LinkRecSpaces() {
		Initialize.initSpaces(fbvcpjcRec);
	}

	protected void initAsb5cpl2LinkRecSpaces() {
		Initialize.initSpaces(fb5cpmpRec);
	}

	protected void initW1CaDescriptionSpaces() {
		w1CaUnitDesc1 = "";
		w1CaUnitDesc2 = "";
	}

	protected void setW1CaDescription(byte[] buf) {
		int offset = 1;
		w1CaUnitDesc1 = DataConverter.readString(buf, offset, 30);
		offset += 30;
		w1CaUnitDesc2 = DataConverter.readString(buf, offset, 30);
	}

	protected String rng2460ReadAbycppRec2460ExitM2450Exit() {
		String retCode;
		retCode = readAbycppRec1();
		if (retCode.compareTo("") != 0)
		{
			return retCode;
		}
		exit3();
		return "";
	}

	protected String rng2500GetWorkersComp2500Exit() {
		String retCode = "";
		retCode = getWorkersComp();
		if (retCode.compareTo("2450-EXIT") == 0)
		{
			return retCode;
		}
		exit4();
		return "";
	}

	protected String rng2600ReadWorkersComp2600ExitM2450Exit2500Exit() {
		String retCode = "";
		Boolean goto2400Exit = false;
		do
		{
			if (goto2400Exit)
			{
				goto2400Exit = false;
				exit1();
				retCode = readAbycppRec();
				return "2450-EXIT";
			}
			retCode = readWorkersComp();
			goto2400Exit = (retCode == "2400-EXIT");
		}
		while(goto2400Exit);
		exit5();
		return "";

	}
}
