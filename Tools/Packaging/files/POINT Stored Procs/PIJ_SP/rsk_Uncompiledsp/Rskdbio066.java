// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk;

import com.csc.pt.svc.pas.alr.to.Bascltb14TO;
import com.csc.pt.svc.pas.alr.Bascltb14;


import bphx.c2ab.util.IProcessInfo;
import bphx.c2ab.util.ProcessInfo;
import bphx.c2ab.cobol.CalledProgList;
import bphx.c2ab.sql.TypeCast;
import bphx.c2ab.util.DataConverter;
import bphx.c2ab.util.DataExtConverter;
import bphx.c2ab.util.DynamicCall;
import bphx.c2ab.util.DynamicSqlDAO;
import bphx.c2ab.util.Functions;
import bphx.c2ab.util.SignType;
import bphx.sdf.collections.IABODynamicArray;
import bphx.sdf.collections.creation.ABOCollectionCreator;
import bphx.sdf.util.ABOSystem;

import com.csc.pt.svc.common.LnkParmsRcrtvppi00;
import com.csc.pt.svc.common.Rcrtvppi00;
import com.csc.pt.svc.common.to.Rcrtvppi00TO;
import com.csc.pt.svc.ctrl.Clt1500recRec2;
import com.csc.pt.svc.data.dao.Bassys0500DAO;
import com.csc.pt.svc.data.dao.Pmsp0200DAO;
import com.csc.pt.svc.data.to.Bassys0500TO;
import com.csc.pt.svc.data.to.Pmsp0200TO;
import com.csc.pt.svc.db.LsWhereClause10;
import com.csc.pt.svc.db.PolintRecordRec;
import com.csc.pt.svc.db.Ws1SelectStatement10;
import com.csc.pt.svc.rsk.to.Rskdbio066TO;
import com.csc.pt.svc.isl.Cobolsqlca;
import com.csc.pt.svc.nav.Basclt1501;
import com.csc.pt.svc.nav.to.Basclt1501TO;
import java.math.BigDecimal;
/**
****************************************************************
* This is the Stored procedure to return all the data for the  *
* Policy Interest grid corresponding to following parameters   *
* passed from the server-side:                                 *
* 1. LOC                                                       *
* 2. MCO                                                       *
* 3. Symbol                                                    *
* 4. Policy                                                    *
* 5. Module Number                                             *
* The following fields are set when the user enters data in the*
* search fields:                                               *
* 6. Interest Type                                             *
* 7. Interest Location                                         *
* 8. Interest Sequence                                         *
* 9. Name                                                      *
* 10. Address                                                  *
* 11. City                                                     *
* 12. State                                                    *
****************************************************************
**/

public class Rskdbio066 extends ProcessInfo<Rskdbio066TO>  {
	protected Cobolsqlca cobolsqlca = new Cobolsqlca();
	protected Ws1SelectStatement10 ws1SelectStatement10 = new Ws1SelectStatement10();
	protected int numOfEntrys;
	protected int numOfEntrysForSql;
	protected IABODynamicArray<PolintEntryArr> polintEntryArr = ABOCollectionCreator.getReferenceFactory(PolintEntryArr.class).createDynamicArray(1);
	protected PolintRecordRec polintRecordRec = new PolintRecordRec();
	protected Clt1500recRec2 clt1500recRec2 = new Clt1500recRec2();
	protected String wsLocation = "";
	protected String wsMasterco = "";
	protected String wsSymbol = "";
	protected String wsPolicyNum = "";
	protected String wsModule = "";
	protected char wsIssue0code = ' ';
	protected int wsPolLimit;
	protected String wsLineOfBus = "";
	protected String wsInsLine = "";
	protected String wsName = "";
	protected String wsAddress = "";
	protected String wsCity = "";
	protected String wsState = "";
	protected String wsId02 = "02";
	protected String wsDesc0line1 = "";
	protected String wsDesc0line3 = "";
	protected String wsSplitCity = "";
	protected String wsSplitState = "";
	protected BigDecimal wsStatusOwnership = BigDecimal.ZERO;
	protected String wsStatusTitleC = "";
	protected long wsStatusOwnsalary;
	protected String wsLegalEntity = "";
	protected String wsBcState = "";
	protected String wsDeemedCode = "";
	protected char wsDeemedStatus = ' ';
	protected String wsSsnNo = "";
	protected String linkReturnCode = "";
	protected long linkClientSeqNum;
	protected short linkAddrSeqNum;
	protected String linkLocateName = "";
	protected String wsType = "";
	protected String wsLoc = "";
	protected String wsDescseq = "";
	protected short sqlIoOk;
	protected LnkParmsRcrtvppi00 lnkParmsRcrtvppi00 = new LnkParmsRcrtvppi00();
	protected int ws1StrPos;

	protected LsWhereClause10 lsWhereClause10 = new LsWhereClause10();
	protected Pmsp0200TO pmsp0200to = new Pmsp0200TO();
	protected Bassys0500TO bassys0500to = new Bassys0500TO();
	protected DynamicSqlDAO sqlCursor_DAO = new DynamicSqlDAO();



	protected Rskdbio066 () {
		this.data = new Rskdbio066TO();

		polintEntryArr.assign((1) - 1, new PolintEntryArr());
	}
	@Override
	protected void mainSubroutine () {
		mainParagraph();
	}

	protected void mainParagraph() {
		// statement is ignored: DECLARECURSOR[IDeclareCursorStatement]
		// @source=BASDBIO066.cbl:line=166
		/*
		 * DECLARE SQL_CURSOR CURSOR FOR SQL_STATEMENT
		 */
		init();
		getIssueCode();
		getPolLimit();
		loadPolints();
		passPolints();
	}

	protected void init() {
		numOfEntrys = 0;
		wsLocation = this.data.getLsLocation();
		wsMasterco = this.data.getLsMasterco();
		wsSymbol = this.data.getLsSymbol();
		wsPolicyNum = this.data.getLsPolicyNum();
		wsModule = this.data.getLsModule();
		wsStatusOwnership = BigDecimal.ZERO;
		wsStatusTitleC = "";
		linkReturnCode = "";
		wsLegalEntity = "";
		wsBcState = "";
		wsStatusOwnsalary = 0;
		wsDeemedCode = "";
		wsDeemedStatus = ' ';
		wsSsnNo = "";
	}

	protected void getIssueCode() {
		pmsp0200to = Pmsp0200DAO.selectById62(pmsp0200to, wsId02, wsSymbol, wsPolicyNum, wsModule, wsMasterco, wsLocation);
		cobolsqlca.setDBAccessStatus(pmsp0200to.getDBAccessStatus());
		if (!cobolsqlca.isEOF()) {
			wsIssue0code = pmsp0200to.getIssue0code();
		}
	}

	protected void getPolLimit() {
		wsLineOfBus = "*PL";
		wsInsLine = "*PL";
		readBassys0500();
		if (cobolsqlca.getSqlCode() != sqlIoOk) {
			wsPolLimit = 100;
		}
	}

	protected void readBassys0500() {
		bassys0500to = Bassys0500DAO.selectByLob(bassys0500to, wsLineOfBus, wsInsLine);
		cobolsqlca.setDBAccessStatus(bassys0500to.getDBAccessStatus());
		if (!cobolsqlca.isEOF()) {
			wsPolLimit = bassys0500to.getLimit5();
		}
	}

	protected void loadPolints() {
		ws1SelectStatement10.setWs1SelectStatement("");
		polintRecordRec.initPolintRecord();
		buildSelectStatement();
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.prepWithOverrideCheck(ws1SelectStatement10.getWs1SelectStatement()));
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.open());
		fetchRecord();
		numOfEntrys = (1 + numOfEntrys) % 100000;
		getPolintEntryArr(numOfEntrys).setPiIssueCode(wsIssue0code);
		numOfEntrys = (1 + numOfEntrys) % 100000;
		if (!(polintRecordRec.getUse0code().compareTo("") == 0 && polintRecordRec.getUse0loc().compareTo("") == 0
			&& polintRecordRec.getDesc0seq().compareTo("") == 0 && polintRecordRec.getDsort0name().compareTo("") == 0)) {
			cobolsqlca.setSqlCode((short)0);
			numOfEntrys = numOfEntrys;
			while (!(cobolsqlca.getSqlCode() != sqlIoOk || numOfEntrys > wsPolLimit + 1)) {
				loadRecords();
				numOfEntrys = numOfEntrys + 1;
			}
		}
		numOfEntrys = (numOfEntrys - 1) % 100000;
		getPolintEntryArr(2).setPiNumOfRecords(ABOSystem.convertUsingString(numOfEntrys, "S99999"));
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.close());
	}

	protected void buildSelectStatement() {
		// ---------------------------------------------------------*
		// Set the address of LS-WHERE-CLAUSE to the address of *
		// WS1-SELECT-STATEMENT. This will overlay LS-WHERE-CLAUSE *
		// onto WS1-SELECT-STATEMENT, this is the same as *
		// redefining WS1-SELECT-STATEMENT at run time. *
		// ---------------------------------------------------------*
		lsWhereClause10.setAddressableData(ws1SelectStatement10);
		lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, "SELECT", new String[] { " USE0CODE,",
			" USE0LOC,", " DESC0SEQ,", " DSORT0NAME", " FROM PMSP1200 WHERE (", "\u0000" }));
		// -------------------------------------------------------*
		// Set the Address of the LS-WHERE-CLAUSE so that we can *
		// add the restrector fields to the WHERE clause. *
		// -------------------------------------------------------*
		// -------------------------------------------------------*
		// Set the Address of the LS-WHERE-CLAUSE so that we can *
		// add the restrector fields to the WHERE clause. *
		// -------------------------------------------------------*
		setAddress();
		// Location
		if (this.data.getLsLocation().compareTo("") != 0) {
			lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " LOCATION = \"", new String[] {
				Functions.padBlanks(this.data.getLsLocation(), 2), "\"", "\u0000" }));
			setAddress();
		}
		// Master Company
		if (this.data.getLsMasterco().compareTo("") != 0) {
			lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " AND MASTER0CO = \"", new String[] {
				Functions.padBlanks(this.data.getLsMasterco(), 2), "\"", "\u0000" }));
			setAddress();
		}
		// Policy Symbol
		if (this.data.getLsSymbol().compareTo("") != 0) {
			lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " AND SYMBOL = \"", new String[] {
				Functions.padBlanks(this.data.getLsSymbol(), 3), "\"", "\u0000" }));
			setAddress();
		}
		// Policy Number
		if (this.data.getLsPolicyNum().compareTo("") != 0) {
			lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " AND POLICY0NUM = \"", new String[] {
				Functions.padBlanks(this.data.getLsPolicyNum(), 7), "\"", "\u0000" }));
			setAddress();
		}
		// Module
		if (this.data.getLsModule().compareTo("") != 0) {
			lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " AND MODULE = \"", new String[] {
				Functions.padBlanks(this.data.getLsModule(), 2), "\"", "\u0000" }));
			setAddress();
		}
		// Interest Type
		if (this.data.getLsIntType().compareTo("") != 0) {
			lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " AND USE0CODE >= \"", new String[] {
				Functions.padBlanks(this.data.getLsIntType(), 2), "\"", "\u0000" }));
			setAddress();
		}
		// Interest Location
		if (this.data.getLsIntLocation().compareTo("") != 0) {
			lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " AND USE0LOC >= \"", new String[] {
				Functions.padBlanks(this.data.getLsIntLocation(), 5), "\"", "\u0000" }));
			setAddress();
		}
		// Interest Sequence
		if (this.data.getLsIntSeq().compareTo("") != 0) {
			lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " AND DESC0SEQ >= \"", new String[] {
				Functions.padBlanks(this.data.getLsIntSeq(), 5), "\"", "\u0000" }));
			setAddress();
		}
		// Record Status
		// Future records in out of sequence are stored with record status
		// value as 'H'. These records should not be shown.
		lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " AND TRANS0STAT <> \"H\"", "\u0000"));
		setAddress();
		// Flag. This condition is being added to prevent site interest
		// details from showing on the Policy Level Interest List.
		lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, " AND FLAG != \"Y\"", "\u0000"));
		setAddress();
		// --------------------------*
		// Close the WHERE clause. *
		// Add the ORDER BY clause. *
		// --------------------------*
		lsWhereClause10.setLsWhereClause(Functions.concat(lsWhereClause10.getLsWhereClause(), 200, ") ORDER BY", new String[] { " USE0CODE,",
			" USE0LOC,", " DESC0SEQ" }));
	}

	protected void loadRecords() {
		loadRecs();
		// Name Search
		wsName = wsDesc0line1;
		if (this.data.getLsName().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(this.data.getLsName(), 31, "%");
			if (ABOSystem.Upper(Functions.subString(wsName, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(this.data.getLsName(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// Address Search
		wsAddress = wsDesc0line3;
		if (this.data.getLsAddress().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(this.data.getLsAddress(), 31, "%");
			if (ABOSystem.Upper(Functions.subString(wsAddress, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(this.data.getLsAddress(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// City Search
		setWsCityState(getWsDesc0line4());
		if (this.data.getLsCity().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(this.data.getLsCity(), 29, "%");
			if (ABOSystem.Upper(Functions.subString(wsCity, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(this.data.getLsCity(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// State Search
		if (this.data.getLsState().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(this.data.getLsState(), 3, "%");
			if (ABOSystem.Upper(Functions.subString(wsState, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(this.data.getLsState(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// Type Search
		if (this.data.getLsIntType().compareTo("") != 0) {
			ws1StrPos = 1;
			ws1StrPos = ws1StrPos + Functions.countCharsBefore(this.data.getLsIntType(), 2, "%");
			if (ABOSystem.Upper(Functions.subString(wsType, 1, ws1StrPos - 1)).compareTo(
				Functions.subString(this.data.getLsIntType(), 1, ws1StrPos - 1)) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// Loc Search
		if (this.data.getLsIntLocation().compareTo("") != 0) {
			if (wsLoc.compareTo(this.data.getLsIntLocation()) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		// Seq Search
		if (this.data.getLsIntSeq().compareTo("") != 0) {
			if (wsDescseq.compareTo(this.data.getLsIntSeq()) != 0) {
				numOfEntrys = (numOfEntrys - 1) % 100000;
				fetchRecord();
				return;
			}
		}
		getPolintEntryArr(numOfEntrys).setPiIntType(polintRecordRec.getUse0code());
		getPolintEntryArr(numOfEntrys).setPiIntLocation(polintRecordRec.getUse0loc());
		getPolintEntryArr(numOfEntrys).setPiIntSeq(polintRecordRec.getDesc0seq());
		getPolintEntryArr(numOfEntrys).setPiName(wsDesc0line1);
		getPolintEntryArr(numOfEntrys).setPiAddress(wsDesc0line3);
		getPolintEntryArr(numOfEntrys).setPiIssueCode(wsIssue0code);
		setWsCityState(getWsDesc0line4());
		getPolintEntryArr(numOfEntrys).setPiCity(wsCity);
		getPolintEntryArr(numOfEntrys).setPiState(wsState);
		getPolintEntryArr(numOfEntrys).setPiClientSeq(linkClientSeqNum);
		getPolintEntryArr(numOfEntrys).setPiAddressSeq(linkAddrSeqNum);
		fetchRecord();
	}

	protected void loadRecs() {
		IProcessInfo<Rcrtvppi00TO> rcrtvppi00;
		Rcrtvppi00TO rcrtvppi00to;
		rtvSeqNum();
		lnkParmsRcrtvppi00.initLowValues();
		lnkParmsRcrtvppi00.setLnkUseCode(polintRecordRec.getUse0code());
		lnkParmsRcrtvppi00.setLnkUseLoc(polintRecordRec.getUse0loc());
		lnkParmsRcrtvppi00.setLnkDescSeq(polintRecordRec.getDesc0seq());
		lnkParmsRcrtvppi00.setLnkSortCode(polintRecordRec.getDsort0name());
		polintRecordRec.setId12("12");
		polintRecordRec.setTrans0stat('V');
		lnkParmsRcrtvppi00.setLnkSym(this.data.getLsSymbol());
		polintRecordRec.setSymbol(this.data.getLsSymbol());
		lnkParmsRcrtvppi00.setLnkPol(this.data.getLsPolicyNum());
		polintRecordRec.setPolicy0num(this.data.getLsPolicyNum());
		lnkParmsRcrtvppi00.setLnkMod(this.data.getLsModule());
		polintRecordRec.setModule(this.data.getLsModule());
		lnkParmsRcrtvppi00.setLnkLoc(this.data.getLsLocation());
		polintRecordRec.setLocation(this.data.getLsLocation());
		lnkParmsRcrtvppi00.setLnkMco(this.data.getLsMasterco());
		polintRecordRec.setMaster0co(this.data.getLsMasterco());
		lnkParmsRcrtvppi00.setLnkReadType("GE");
		rcrtvppi00 = DynamicCall.getInfoProcessor(Rcrtvppi00.class);
		rcrtvppi00to =
			new Rcrtvppi00TO(lnkParmsRcrtvppi00, wsStatusOwnership, wsStatusTitleC, wsStatusOwnsalary, wsLegalEntity, wsBcState,
				wsDeemedCode, wsDeemedStatus, wsSsnNo);
		rcrtvppi00to = rcrtvppi00.run(rcrtvppi00to);
		wsStatusOwnership = rcrtvppi00to.getStatusOwnershipPercent();
		wsStatusTitleC = rcrtvppi00to.getStatusTitle();
		wsStatusOwnsalary = rcrtvppi00to.getStatusSalary();
		wsLegalEntity = rcrtvppi00to.getWsLegalEntity();
		wsBcState = rcrtvppi00to.getTbrecu3State();
		wsDeemedCode = rcrtvppi00to.getDeemedCode();
		wsDeemedStatus = rcrtvppi00to.getDeemedStatus();
		wsSsnNo = rcrtvppi00to.getSsnNo();
		call1();
		CalledProgList.deleteInst(Bascltb14.class);
		wsDesc0line1 = lnkParmsRcrtvppi00.getLnkName();
		wsDesc0line3 = lnkParmsRcrtvppi00.getLnkStreetAddress();
		polintRecordRec.setDesc0line4(lnkParmsRcrtvppi00.getLnkCitystAddress());
		wsType = lnkParmsRcrtvppi00.getLnkUseCode();
		wsLoc = lnkParmsRcrtvppi00.getLnkUseLoc();
		wsDescseq = lnkParmsRcrtvppi00.getLnkDescSeq();
		setWsDesc0line4(DataExtConverter.strToBytes(polintRecordRec.getDesc0line4(), 30));
		// IF COMMA-COUNTER = 0
	}

	protected void rtvSeqNum() {
		Basclt1501TO basclt1501to2;
		IProcessInfo<Basclt1501TO> basclt1501;
		clt1500recRec2.setLocation(this.data.getLsLocation());
		clt1500recRec2.setMaster0co(this.data.getLsMasterco());
		clt1500recRec2.setSymbol(this.data.getLsSymbol());
		clt1500recRec2.setPolicy0num(this.data.getLsPolicyNum());
		clt1500recRec2.setModule(this.data.getLsModule());
		clt1500recRec2.setUse0code(polintRecordRec.getUse0code());
		clt1500recRec2.setUse0loc(polintRecordRec.getUse0loc());
		clt1500recRec2.setDesc0seq(polintRecordRec.getDesc0seq());
		basclt1501 = DynamicCall.getInfoProcessor(Basclt1501.class);
		basclt1501to2 = new Basclt1501TO(linkReturnCode, getWsRecord1501());
		basclt1501to2 = basclt1501.run(basclt1501to2);

		linkReturnCode = basclt1501to2.getLinkReturnCode();
		setWsRecord1501(basclt1501to2.getLinkRecord());
		CalledProgList.deleteInst(Basclt1501.class);
		// If the record is found
		if (linkReturnCode.compareTo("0000000") == 0) {
			linkClientSeqNum = clt1500recRec2.getCltseqnum();
			linkAddrSeqNum = clt1500recRec2.getAddrseqnum();
		} else {
			linkClientSeqNum = 0;
			linkAddrSeqNum = 0;
		}
	}

	protected void passPolints() {
		numOfEntrysForSql = numOfEntrys;
		resultSetTO.addResultSet(polintEntryArr, numOfEntrysForSql);
	}

	protected void fetchRecord() {
		Object[] resultSet;
		cobolsqlca.setDBAccessStatus(sqlCursor_DAO.fetchNext());
		if (cobolsqlca.getSqlCode() == 0) {
			resultSet = cobolsqlca.getResultSet();
			polintRecordRec.setUse0code(TypeCast.getAsStr(resultSet[(1) - 1], 2));
			polintRecordRec.setUse0loc(TypeCast.getAsStr(resultSet[(2) - 1], 5));
			polintRecordRec.setDesc0seq(TypeCast.getAsStr(resultSet[(3) - 1], 5));
			polintRecordRec.setDsort0name(TypeCast.getAsStr(resultSet[(4) - 1], 4));
		}
		if (cobolsqlca.getSqlCode() != sqlIoOk) {
			return;
		}
	}

	protected void setAddress() {
		ws1StrPos = 1;
		// -------------------------------------------------------------*
		// Calculate the WS1-STR-POS by getting the number of chars *
		// before the X'00'. *
		// -------------------------------------------------------------*
		ws1StrPos = ws1StrPos + Functions.countCharsBefore(ws1SelectStatement10.getWs1SelectStatement(), 10000, "\u0000");
		// -------------------------------------------------------------*
		// The STRING statement can not have a referential modified *
		// field as the INTO fields. So we use a field that has been *
		// defined in linkage named LS-WHERE-CLAUSE. We set the *
		// address of LS-WHERE-CLAUSE to the address of the part of *
		// field that we want to add the where condition to. *
		// -------------------------------------------------------------*
		lsWhereClause10.setAddressableData(ws1SelectStatement10, ws1StrPos);
		lsWhereClause10.setLsWhereClause("");
	}






	public static Rskdbio066TO getToFromByteArray(byte[] dynamic_lsInputParams) {
		Rskdbio066TO localBasdbio066TO = new Rskdbio066TO();

		if (dynamic_lsInputParams != null) {
			localBasdbio066TO.setLsInputParams(dynamic_lsInputParams);
		}

		return localBasdbio066TO;
	}
	public static void setByteArrayFromTo(Rskdbio066TO localBasdbio066TO, byte[] dynamic_lsInputParams){

		if (dynamic_lsInputParams != null) {
			DataConverter.arrayCopy(localBasdbio066TO.getLsInputParams(), Rskdbio066TO.getLsInputParamsSize(), dynamic_lsInputParams, 1);
		}

	}

	protected void call1() {
		byte[] arr_p0 = new byte[7];
		byte[] arr_p1 = new byte[PolintRecordRec.getPolintRecordSize()];
		byte[] arr_p2 = new byte[10];
		byte[] arr_p3 = new byte[2];
		byte[] arr_p4 = new byte[30];
		DataConverter.writeString(arr_p0, 1, linkReturnCode, 7);
		DataConverter.arrayCopy(polintRecordRec.getPolintRecord(), PolintRecordRec.getPolintRecordSize(), arr_p1, 1);
		DataConverter.writeLong(arr_p2, 1, linkClientSeqNum, 10, SignType.SIGNED_TRAILING);
		DataConverter.writeShort(arr_p3, 1, linkAddrSeqNum, 2, SignType.SIGNED_TRAILING);
		DataConverter.writeString(arr_p4, 1, linkLocateName, 30);
		IProcessInfo<Bascltb14TO> bascltb14 = DynamicCall.getInfoProcessor(Bascltb14.class);
		Bascltb14TO callingBascltb14TO = Bascltb14.getToFromByteArray(arr_p0, arr_p1, arr_p2, arr_p3, arr_p4);
		callingBascltb14TO = bascltb14.run(callingBascltb14TO);
		Bascltb14.setByteArrayFromTo(callingBascltb14TO, arr_p0, arr_p1, arr_p2, arr_p3, arr_p4);
		linkReturnCode = DataConverter.readString(arr_p0, 1, 7);
		polintRecordRec.setPolintRecord(arr_p1);
		linkClientSeqNum = DataConverter.readLong(arr_p2, 1, 10, SignType.SIGNED_TRAILING);
		linkAddrSeqNum = DataConverter.readShort(arr_p3, 1, 2, SignType.SIGNED_TRAILING);
		linkLocateName = DataConverter.readString(arr_p4, 1, 30);
	}

	protected PolintEntryArr getPolintEntryArr(int index) {
		if (index > polintEntryArr.size()) {
			// for an element beyond current array limits, all intermediate
			polintEntryArr.ensureCapacity(index);
			for (int i = polintEntryArr.size() + 1; i <= index; i++) {
				polintEntryArr.append(new PolintEntryArr());
				// assign default value
			}
		}
		return polintEntryArr.get((index) - 1);
	}

	public static int getWsCityStateSize() {
		return 30;
	}

	protected byte[] getWsCityState() {
		byte[] buf = new byte[getWsCityStateSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsCity, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, wsState, 2);
		return buf;
	}

	public static int getWsDesc0line4Size() {
		return 30;
	}

	protected byte[] getWsDesc0line4() {
		byte[] buf = new byte[getWsDesc0line4Size()];
		int offset = 1;
		DataConverter.writeString(buf, offset, wsSplitCity, 28);
		offset += 28;
		DataConverter.writeString(buf, offset, wsSplitState, 2);
		return buf;
	}

	protected void setWsCityState(byte[] buf) {
		int offset = 1;
		wsCity = DataConverter.readString(buf, offset, 28);
		offset += 28;
		wsState = DataConverter.readString(buf, offset, 2);
	}

	protected void setWsDesc0line4(byte[] buf) {
		int offset = 1;
		wsSplitCity = DataConverter.readString(buf, offset, 28);
		offset += 28;
		wsSplitState = DataConverter.readString(buf, offset, 2);
	}

	public static int getWsRecord1501Size() {
		return 90;
	}

	protected byte[] getWsRecord1501() {
		return clt1500recRec2.getClt1500rec();
	}

	protected void setWsRecord1501(byte[] buf) {
		clt1500recRec2.setClt1500rec(buf);
	}
}
