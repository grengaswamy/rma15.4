// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.ProcessInfoObject;
import bphx.c2ab.util.DataConverter;

/*
*/
public class Rskdbio040TO extends ProcessInfoObject {
	private String inUserid = "";
	private String inSymbol = "";
	private String inPolicy0num = "";
	private String inModule = "";
	private String inLastName = "";
	private char inLnBwith = ' ';
	private String inFirstName = "";
	private char inFnBwith = ' ';
	private String inState = "";
	private String inLine0bus = "";
	private String inCust0no = "";
	private String inCity = "";
	private String inZip0post = "";
	private String inSort0name = "";
	private String inLocation = "";
	private String inMaster0co = "";
	private String inAgency = "";
	private String inPhone1 ="";
	private String inSsn ="";
	private String inGroupNo ="";
	private String inAddnlIntType = "";
	private String inAddnlIntName = "";
	private char inSsnSiteInd = ' ';
	private char inSsnCltInd = ' ';
	private String inLegalAdd = "";
	private String inLaCity = "";
	private String inLaState = "";
	private String inLaZip = "";
	private String inMailIngAdd = "";
	private String inLoanNum = "";
	private String inLossdte = "";
	private String inTPASearch = "";
	private short inMaxrecords;
	private String inWildCardPolicySearch="";	
	private String typeAct = "";
	
	
	public String getInFirstName() {
		return inFirstName;
	}
	public void setInFirstName(String inFirstName) {
		this.inFirstName = inFirstName;
	}
	public char getInLnBwith() {
		return inLnBwith;
	}
	public void setInLnBwith(char inLnBwith) {
		this.inLnBwith = inLnBwith;
	}
	public String getInUserid() {
		return inUserid;
	}
	public void setInUserid(String inUserid) {
		this.inUserid = inUserid;
	}
	public String getInSymbol() {
		return inSymbol;
	}
	public void setInSymbol(String inSymbol) {
		this.inSymbol = inSymbol;
	}
	public String getInPolicy0num() {
		return inPolicy0num;
	}
	public void setInPolicy0num(String inPolicy0num) {
		this.inPolicy0num = inPolicy0num;
	}
	public String getInModule() {
		return inModule;
	}
	public void setInModule(String inModule) {
		this.inModule = inModule;
	}
	public String getInLastName() {
		return inLastName;
	}
	public void setInLastName(String inLastName) {
		this.inLastName = inLastName;
	}
	public char getInFnBwith() {
		return inFnBwith;
	}
	public void setInFnBwith(char inFnBwith) {
		this.inFnBwith = inFnBwith;
	}
	public String getInState() {
		return inState;
	}
	public void setInState(String inState) {
		this.inState = inState;
	}
	public String getInLine0bus() {
		return inLine0bus;
	}
	public void setInLine0bus(String inLine0bus) {
		this.inLine0bus = inLine0bus;
	}
	public String getInCust0no() {
		return inCust0no;
	}
	public void setInCust0no(String inCust0no) {
		this.inCust0no = inCust0no;
	}
	public String getInCity() {
		return inCity;
	}
	public void setInCity(String inCity) {
		this.inCity = inCity;
	}
	public String getInZip0post() {
		return inZip0post;
	}
	public void setInZip0post(String inZip0post) {
		this.inZip0post = inZip0post;
	}
	public String getInSort0name() {
		return inSort0name;
	}
	public void setInSort0name(String inSort0name) {
		this.inSort0name = inSort0name;
	}
	public String getInLocation() {
		return inLocation;
	}
	public void setInLocation(String inLocation) {
		this.inLocation = inLocation;
	}
	public String getInMaster0co() {
		return inMaster0co;
	}
	public void setInMaster0co(String inMaster0co) {
		this.inMaster0co = inMaster0co;
	}
	public String getInAgency() {
		return inAgency;
	}
	public void setInAgency(String inAgency) {
		this.inAgency = inAgency;
	}
	public String getInPhone1() {
		return inPhone1;
	}
	public void setInPhone1(String inPhone1) {
		this.inPhone1 = inPhone1;
	}
	public String getInSsn() {
		return inSsn;
	}
	public void setInSsn(String inSsn) {
		this.inSsn = inSsn;
	}
	public String getInGroupNo() {
		return inGroupNo;
	}
	public void setInGroupNo(String inGroupNo) {
		this.inGroupNo = inGroupNo;
	}
	public String getInAddnlIntType() {
		return inAddnlIntType;
	}
	public void setInAddnlIntType(String inAddnlIntType) {
		this.inAddnlIntType = inAddnlIntType;
	}
	public String getInAddnlIntName() {
		return inAddnlIntName;
	}
	public void setInAddnlIntName(String inAddnlIntName) {
		this.inAddnlIntName = inAddnlIntName;
	}
	public char getInSsnSiteInd() {
		return inSsnSiteInd;
	}
	public void setInSsnSiteInd(char inSsnSiteInd) {
		this.inSsnSiteInd = inSsnSiteInd;
	}
	public char getInSsnCltInd() {
		return inSsnCltInd;
	}
	public void setInSsnCltInd(char inSsnCltInd) {
		this.inSsnCltInd = inSsnCltInd;
	}
	public String getInLegalAdd() {
		return inLegalAdd;
	}
	public void setInLegalAdd(String inLegalAdd) {
		this.inLegalAdd = inLegalAdd;
	}
	public String getInLaCity() {
		return inLaCity;
	}
	public void setInLaCity(String inLaCity) {
		this.inLaCity = inLaCity;
	}
	public String getInLaState() {
		return inLaState;
	}
	public void setInLaState(String inLaState) {
		this.inLaState = inLaState;
	}
	public String getInLaZip() {
		return inLaZip;
	}
	public void setInLaZip(String inLaZip) {
		this.inLaZip = inLaZip;
	}
	public String getInMailIngAdd() {
		return inMailIngAdd;
	}
	public void setInMailIngAdd(String inMailIngAdd) {
		this.inMailIngAdd = inMailIngAdd;
	}
	public String getInLoanNum() {
		return inLoanNum;
	}
	public void setInLoanNum(String inLoanNum) {
		this.inLoanNum = inLoanNum;
	}
	public short getInMaxrecords() {
		return inMaxrecords;
	}
	public void setInMaxrecords(short inMaxrecords) {
		this.inMaxrecords = inMaxrecords;
	}
	public String getInWildCardPolicySearch() {
		return this.inWildCardPolicySearch;
	}

	public void setInWildCardPolicySearch(String inWildCardPolicySearch) {
		this.inWildCardPolicySearch = inWildCardPolicySearch;
	}
	public void basicInitialization() {}
	
	public Rskdbio040TO(byte[] inputParameters) {
		basicInitialization();
		setInputParameters(inputParameters);
	}
	
	public Rskdbio040TO() {
		basicInitialization();
	}

	public int getInputParametersSize() {
		return 902;
	}

	public void setInputParameters(byte[] buf) {
		int offset = 1;
		inUserid = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inPolicy0num = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inLastName = DataConverter.readString(buf, offset, 256);
		offset += 256;
		inLnBwith = DataConverter.readChar(buf, offset);
		offset += 1;
		inFirstName = DataConverter.readString(buf, offset, 40);
		offset += 40;
		inFnBwith = DataConverter.readChar(buf, offset);
		offset += 1;
		inState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inLine0bus = DataConverter.readString(buf, offset, 3);
		offset += 3;
		inCust0no = DataConverter.readString(buf, offset, 10);
		offset += 10;
		inCity = DataConverter.readString(buf, offset, 28);
		offset += 28;
		inZip0post = DataConverter.readString(buf, offset, 10);
		offset += 10;		
		inSort0name = DataConverter.readString(buf, offset, 4);
		offset += 4;
		inLocation = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inMaster0co = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inAgency = DataConverter.readString(buf, offset, 7);
		offset += 7;
		inPhone1 = DataConverter.readString(buf, offset, 32);
		offset += 32;
		inSsn = DataConverter.readString(buf, offset, 11);
		offset += 11;
		inGroupNo = DataConverter.readString(buf, offset, 10);
		offset += 10;		
		inAddnlIntType = DataConverter.readString(buf, offset, 2);
		offset += 2;
		inAddnlIntName = DataConverter.readString(buf, offset, 30);
		offset += 30;
		inSsnSiteInd = DataConverter.readChar(buf, offset);
		offset += 1;
		inSsnCltInd = DataConverter.readChar(buf, offset);
		offset += 1;
		inLegalAdd = DataConverter.readString(buf, offset,30);
		offset += 30;
		inLaCity = DataConverter.readString(buf, offset,28);
		offset += 28; 
		inLaState = DataConverter.readString(buf, offset,2);
		offset += 2;
		inLaZip = DataConverter.readString(buf, offset,10);
		offset += 10;
		inMailIngAdd = DataConverter.readString(buf, offset,30);
		offset += 30;
		inLoanNum = DataConverter.readString(buf, offset,25);
		offset += 25;
		inLossdte = DataConverter.readString(buf, offset,7);
		offset += 7;
		inTPASearch = DataConverter.readString(buf, offset,295);
		offset += 295;
		inMaxrecords = DataConverter.readShort(buf, offset, 3);	
		offset += 3;
		inWildCardPolicySearch = DataConverter.readString(buf, offset, 6);
		offset += 6;
		setTypeAct(DataConverter.readString(buf, offset, 2));
		
	}

	public byte[] getInputParameters() {
		byte[] buf = new byte[getInputParametersSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset,inUserid, 10);
		offset += 10;
		DataConverter.writeString(buf, offset,inSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset,inPolicy0num, 7);
		offset += 7;
		DataConverter.writeString(buf, offset,inModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inLastName, 256);
		offset += 256;
		DataConverter.writeChar(buf, offset,inLnBwith);
		offset += 1;
		DataConverter.writeString(buf, offset,inFirstName, 40);
		offset += 40;
		DataConverter.writeChar(buf, offset,inFnBwith);
		offset += 1;
		DataConverter.writeString(buf, offset,inState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inLine0bus, 3);
		offset += 3;
		DataConverter.writeString(buf, offset,inCust0no, 10);
		offset += 10;
		DataConverter.writeString(buf, offset,inCity, 28);
		offset += 28;
		DataConverter.writeString(buf, offset,inZip0post, 10);
		offset += 10;
		DataConverter.writeString(buf, offset,inSort0name, 4);
		offset += 4;
		DataConverter.writeString(buf, offset,inLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inMaster0co, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inAgency, 7);
		offset += 7;
		DataConverter.writeString(buf, offset,inPhone1, 32);
		offset += 32;
		DataConverter.writeString(buf, offset,inSsn, 11);
		offset += 11;
		DataConverter.writeString(buf, offset,inGroupNo, 10);
		offset += 10;		
		DataConverter.writeString(buf, offset,inAddnlIntType, 2);
		offset += 2;
		DataConverter.writeString(buf, offset,inAddnlIntName, 30);
		offset += 30;
		DataConverter.writeChar(buf, offset,inSsnSiteInd);
		offset += 1;
		DataConverter.writeChar(buf, offset,inSsnCltInd);
		offset += 1;
		DataConverter.writeString(buf, offset,inGroupNo, 10);
		offset += 10;
		DataConverter.writeString(buf, offset,inGroupNo, 10);
		offset += 10;
		DataConverter.writeString(buf, offset,inLegalAdd,30);
		offset += 30;
		DataConverter.writeString(buf, offset,inLaCity,28);
		offset += 28;
		DataConverter.writeString(buf, offset,inLaState,2);
		offset += 2;
		DataConverter.writeString(buf, offset,inLaZip,10);
		offset += 10;
		DataConverter.writeString(buf, offset,inMailIngAdd,30);
		offset += 30;
		DataConverter.writeString(buf, offset,inLoanNum,25);
		offset += 25;
		DataConverter.writeString(buf, offset,inLossdte,7);
		offset += 7;
		DataConverter.writeString(buf, offset,inTPASearch,295);
		offset += 295;
		DataConverter.writeShort(buf, offset,inMaxrecords, 3);	
		offset += 3;
		DataConverter.writeString(buf, offset, inWildCardPolicySearch, 6);
		return buf;
	}

	public String getTypeAct() {
		return typeAct;
	}
	
	public void setTypeAct(String typeAct) {
		this.typeAct = typeAct;
	}
	public String getInLossdte() {
		return inLossdte;
	}
	public void setInLossdte(String inLossdte) {
		this.inLossdte = inLossdte;
	}
	public String getInTPASearch() {
		return inTPASearch;
	}
	public void setInTPASearch(String inTPASearch) {
		this.inTPASearch = inTPASearch;
	}
	
}
