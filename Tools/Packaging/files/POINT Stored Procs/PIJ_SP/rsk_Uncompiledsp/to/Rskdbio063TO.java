package com.csc.pt.svc.rsk.to;

import bphx.c2ab.util.DataConverter;

public class Rskdbio063TO {
	private String lsLocation = "";
	private String lsMasterco = "";
	private String lsSymbol = "";
	private String lsPolicyNum = "";
	private String lsModule = "";
	private String lsLineOfBus = "";
	private String lsInsLine = "";
	private String lsProduct = "";
	private String lsState = "";
	private String lsUnitNum = "";
	private String lsRiskLoc = "";
	private String lsSubLoc = "";
	private String lsUnitDesc = "";
	private String lsProductS = "";
	private String lsStateS = "";
	private String lsVinS = "";

	public Rskdbio063TO(byte[] lsInputParams) {
		basicInitialization();
		setLsInputParams(lsInputParams);
	}

	public Rskdbio063TO() {
		basicInitialization();
	}

	public int getLsInputParamsSize() {
		return 148;
	}

	public void setLsInputParams(byte[] buf) {
		int offset = 1;
		lsLocation = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsMasterco = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsSymbol = DataConverter.readString(buf, offset, 3);
		offset += 3;
		lsPolicyNum = DataConverter.readString(buf, offset, 7);
		offset += 7;
		lsModule = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsLineOfBus = DataConverter.readString(buf, offset, 3);
		offset += 3;
		lsInsLine = DataConverter.readString(buf, offset, 3);
		offset += 3;
		lsProduct = DataConverter.readString(buf, offset, 6);
		offset += 6;
		lsState = DataConverter.readString(buf, offset, 2);
		offset += 2;
		lsUnitNum = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsRiskLoc = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsSubLoc = DataConverter.readString(buf, offset, 5);
		offset += 5;
		lsUnitDesc = DataConverter.readString(buf, offset, 62);
		offset += 62;
		lsProductS = DataConverter.readString(buf, offset, 7);
		offset += 7;
		lsStateS = DataConverter.readString(buf, offset, 3);
		offset += 3;
		lsVinS = DataConverter.readString(buf, offset, 31);
	}

	public byte[] getLsInputParams() {
		byte[] buf = new byte[getLsInputParamsSize()];
		int offset = 1;
		DataConverter.writeString(buf, offset, lsLocation, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsMasterco, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsSymbol, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, lsPolicyNum, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, lsModule, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsLineOfBus, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, lsInsLine, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, lsProduct, 6);
		offset += 6;
		DataConverter.writeString(buf, offset, lsState, 2);
		offset += 2;
		DataConverter.writeString(buf, offset, lsUnitNum, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsRiskLoc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsSubLoc, 5);
		offset += 5;
		DataConverter.writeString(buf, offset, lsUnitDesc, 62);
		offset += 62;
		DataConverter.writeString(buf, offset, lsProductS, 7);
		offset += 7;
		DataConverter.writeString(buf, offset, lsStateS, 3);
		offset += 3;
		DataConverter.writeString(buf, offset, lsVinS, 31);
		return buf;
	}

	public void basicInitialization() {}

	public String getLsLocation() {
		return this.lsLocation;
	}

	public String getLsMasterco() {
		return this.lsMasterco;
	}

	public String getLsSymbol() {
		return this.lsSymbol;
	}

	public String getLsPolicyNum() {
		return this.lsPolicyNum;
	}

	public String getLsModule() {
		return this.lsModule;
	}

	public String getLsInsLine() {
		return this.lsInsLine;
	}

	public String getLsProduct() {
		return this.lsProduct;
	}

	public String getLsState() {
		return this.lsState;
	}

	public String getLsLineOfBus() {
		return this.lsLineOfBus;
	}

	public String getLsUnitNum() {
		return this.lsUnitNum;
	}

	public String getLsRiskLoc() {
		return this.lsRiskLoc;
	}

	public String getLsSubLoc() {
		return this.lsSubLoc;
	}

	public String getLsUnitDesc() {
		return this.lsUnitDesc;
	}

	public String getLsProductS() {
		return this.lsProductS;
	}

	public String getLsStateS() {
		return this.lsStateS;
	}

	public String getLsVinS() {
		return this.lsVinS;
	}
}