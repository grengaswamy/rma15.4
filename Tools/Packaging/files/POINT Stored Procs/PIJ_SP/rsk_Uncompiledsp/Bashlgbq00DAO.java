// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.dao;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import bphx.c2ab.hibernate.HibernateSessionFactory;
import bphx.c2ab.util.DBAccessStatus;
import bphx.sdf.sql.util.ABOSqlDB2Conversion;

import com.csc.pt.svc.data.to.Bashlgbq00TO;

public class Bashlgbq00DAO {
	public static DBAccessStatus insertRec(Bashlgbq00TO bashlgbq00TO) {
		DBAccessStatus status = new DBAccessStatus();
		try {
			Session session = HibernateSessionFactory.current().getSession("BASHLGBQ00");
			Query query = session.getNamedQuery("Bashlgbq00DAO_insertRec");
			query.setString("bashlgbq00TO_loguser", bashlgbq00TO.getLoguser());
			query.setDate("bashlgbq00TO_logdate", ABOSqlDB2Conversion.dateToSQL(bashlgbq00TO.getLogdate()));
			query.setTime("bashlgbq00TO_logtime", ABOSqlDB2Conversion.timeToSQL(bashlgbq00TO.getLogtime()));
			query.setInteger("bashlgbq00TO_logseqnum", bashlgbq00TO.getLogseqnum());
			query.setString("bashlgbq00TO_location", bashlgbq00TO.getLocation());
			query.setString("bashlgbq00TO_masterco", bashlgbq00TO.getMasterco());
			query.setString("bashlgbq00TO_symbol", bashlgbq00TO.getSymbol());
			query.setString("bashlgbq00TO_policyno", bashlgbq00TO.getPolicyno());
			query.setString("bashlgbq00TO_module", bashlgbq00TO.getModule());
			query.setString("bashlgbq00TO_insline", bashlgbq00TO.getInsline());
			query.setInteger("bashlgbq00TO_riskloc", bashlgbq00TO.getRiskloc());
			query.setInteger("bashlgbq00TO_risksubloc", bashlgbq00TO.getRisksubloc());
			query.setString("bashlgbq00TO_product", bashlgbq00TO.getProduct());
			query.setInteger("bashlgbq00TO_unitno", bashlgbq00TO.getUnitno());
			query.setCharacter("bashlgbq00TO_recstatus", bashlgbq00TO.getRecstatus());
			query.setCharacter("bashlgbq00TO_bqc7st", bashlgbq00TO.getBqc7st());
			query.setInteger("bashlgbq00TO_bqandt", bashlgbq00TO.getBqandt());
			query.setShort("bashlgbq00TO_bqafnb", bashlgbq00TO.getBqafnb());
			query.setInteger("bashlgbq00TO_bqaldt", bashlgbq00TO.getBqaldt());
			query.setInteger("bashlgbq00TO_bqafdt", bashlgbq00TO.getBqafdt());
			query.setString("bashlgbq00TO_bqtype0act", bashlgbq00TO.getBqtype0act());
			query.setString("bashlgbq00TO_bqbatx", bashlgbq00TO.getBqbatx());
			query.setCharacter("bashlgbq00TO_bqdzst", bashlgbq00TO.getBqdzst());
			query.setCharacter("bashlgbq00TO_bqd0st", bashlgbq00TO.getBqd0st());
			query.setCharacter("bashlgbq00TO_bqd2st", bashlgbq00TO.getBqd2st());
			query.setCharacter("bashlgbq00TO_bqd3st", bashlgbq00TO.getBqd3st());
			query.setBigDecimal("bashlgbq00TO_bqa3va", bashlgbq00TO.getBqa3va());
			query.setBigDecimal("bashlgbq00TO_bqbrva", bashlgbq00TO.getBqbrva());
			query.setBigDecimal("bashlgbq00TO_bqbsva", bashlgbq00TO.getBqbsva());
			query.setBigDecimal("bashlgbq00TO_bqbtva", bashlgbq00TO.getBqbtva());
			query.setBigDecimal("bashlgbq00TO_bqaupc", bashlgbq00TO.getBqaupc());
			query.setString("bashlgbq00TO_bqaqcd", bashlgbq00TO.getBqaqcd());
			query.setString("bashlgbq00TO_bqarcd", bashlgbq00TO.getBqarcd());
			query.setString("bashlgbq00TO_bqascd", bashlgbq00TO.getBqascd());
			query.setString("bashlgbq00TO_bqeetx", bashlgbq00TO.getBqeetx());
			query.setString("bashlgbq00TO_bqbccd", bashlgbq00TO.getBqbccd());
			query.setString("bashlgbq00TO_bqagnb", bashlgbq00TO.getBqagnb());
			query.setBigDecimal("bashlgbq00TO_bqa5va", bashlgbq00TO.getBqa5va());
			query.setString("bashlgbq00TO_bqhwtx", bashlgbq00TO.getBqhwtx());
			query.setCharacter("bashlgbq00TO_bqhxtx", bashlgbq00TO.getBqhxtx());
			query.setCharacter("bashlgbq00TO_bqhytx", bashlgbq00TO.getBqhytx());
			query.setCharacter("bashlgbq00TO_bqhztx", bashlgbq00TO.getBqhztx());
			query.setCharacter("bashlgbq00TO_bqh0tx", bashlgbq00TO.getBqh0tx());
			query.setString("bashlgbq00TO_bqh1tx", bashlgbq00TO.getBqh1tx());
			query.setCharacter("bashlgbq00TO_bqbicd", bashlgbq00TO.getBqbicd());
			query.setString("bashlgbq00TO_bqbjcd", bashlgbq00TO.getBqbjcd());
			query.setString("bashlgbq00TO_bqbkcd", bashlgbq00TO.getBqbkcd());
			query.setString("bashlgbq00TO_bqh2tx", bashlgbq00TO.getBqh2tx());
			query.setString("bashlgbq00TO_bqapnb", bashlgbq00TO.getBqapnb());
			query.setCharacter("bashlgbq00TO_bqblcd", bashlgbq00TO.getBqblcd());
			query.setCharacter("bashlgbq00TO_bqbmcd", bashlgbq00TO.getBqbmcd());
			query.setCharacter("bashlgbq00TO_bqbncd", bashlgbq00TO.getBqbncd());
			query.setCharacter("bashlgbq00TO_bqbocd", bashlgbq00TO.getBqbocd());
			query.setCharacter("bashlgbq00TO_bqbpcd", bashlgbq00TO.getBqbpcd());
			query.setCharacter("bashlgbq00TO_bqbqcd", bashlgbq00TO.getBqbqcd());
			query.setCharacter("bashlgbq00TO_bqbrcd", bashlgbq00TO.getBqbrcd());
			query.setString("bashlgbq00TO_bqbscd", bashlgbq00TO.getBqbscd());
			query.setCharacter("bashlgbq00TO_bqjbtx", bashlgbq00TO.getBqjbtx());
			query.setCharacter("bashlgbq00TO_bqjctx", bashlgbq00TO.getBqjctx());
			query.setString("bashlgbq00TO_bqdbnb", bashlgbq00TO.getBqdbnb());
			query.setString("bashlgbq00TO_bqdcnb", bashlgbq00TO.getBqdcnb());
			query.setString("bashlgbq00TO_bqddnb", bashlgbq00TO.getBqddnb());
			query.setString("bashlgbq00TO_bqdenb", bashlgbq00TO.getBqdenb());
			query.setInteger("bashlgbq00TO_bqdfnb", bashlgbq00TO.getBqdfnb());
			query.setInteger("bashlgbq00TO_bqdgnb", bashlgbq00TO.getBqdgnb());
			query.setInteger("bashlgbq00TO_bqdhnb", bashlgbq00TO.getBqdhnb());
			query.setInteger("bashlgbq00TO_bqdinb", bashlgbq00TO.getBqdinb());
			query.setInteger("bashlgbq00TO_bqabnb", bashlgbq00TO.getBqabnb());
			query.setInteger("bashlgbq00TO_bqdjnb", bashlgbq00TO.getBqdjnb());
			query.setInteger("bashlgbq00TO_bqdknb", bashlgbq00TO.getBqdknb());
			query.setShort("bashlgbq00TO_bqdlnb", bashlgbq00TO.getBqdlnb());
			query.setShort("bashlgbq00TO_bqdmnb", bashlgbq00TO.getBqdmnb());
			query.setShort("bashlgbq00TO_bqdnnb", bashlgbq00TO.getBqdnnb());
			query.setBigDecimal("bashlgbq00TO_bqavpc", bashlgbq00TO.getBqavpc());
			query.setBigDecimal("bashlgbq00TO_bqawpc", bashlgbq00TO.getBqawpc());
			query.setBigDecimal("bashlgbq00TO_bqaxpc", bashlgbq00TO.getBqaxpc());
			query.setBigDecimal("bashlgbq00TO_bqa6va", bashlgbq00TO.getBqa6va());
			query.setBigDecimal("bashlgbq00TO_bqa7va", bashlgbq00TO.getBqa7va());
			query.setBigDecimal("bashlgbq00TO_bqa8va", bashlgbq00TO.getBqa8va());
			query.setBigDecimal("bashlgbq00TO_bqa9va", bashlgbq00TO.getBqa9va());
			query.setBigDecimal("bashlgbq00TO_bqbava", bashlgbq00TO.getBqbava());
			query.setBigDecimal("bashlgbq00TO_bqbbva", bashlgbq00TO.getBqbbva());
			query.setBigDecimal("bashlgbq00TO_bqbcva", bashlgbq00TO.getBqbcva());
			query.setBigDecimal("bashlgbq00TO_bqbdva", bashlgbq00TO.getBqbdva());
			query.setBigDecimal("bashlgbq00TO_bqbeva", bashlgbq00TO.getBqbeva());
			query.setBigDecimal("bashlgbq00TO_bqbfva", bashlgbq00TO.getBqbfva());
			query.setBigDecimal("bashlgbq00TO_bqbgva", bashlgbq00TO.getBqbgva());
			query.setBigDecimal("bashlgbq00TO_bqbhva", bashlgbq00TO.getBqbhva());
			query.setBigDecimal("bashlgbq00TO_bqbiva", bashlgbq00TO.getBqbiva());
			query.setBigDecimal("bashlgbq00TO_bqbjva", bashlgbq00TO.getBqbjva());
			query.setBigDecimal("bashlgbq00TO_bqa9nb", bashlgbq00TO.getBqa9nb());
			query.setCharacter("bashlgbq00TO_bqczst", bashlgbq00TO.getBqczst());
			query.setCharacter("bashlgbq00TO_bqc0st", bashlgbq00TO.getBqc0st());
			query.setCharacter("bashlgbq00TO_bqc1st", bashlgbq00TO.getBqc1st());
			query.setCharacter("bashlgbq00TO_bqc2st", bashlgbq00TO.getBqc2st());
			query.setCharacter("bashlgbq00TO_bqiytx", bashlgbq00TO.getBqiytx());
			query.setCharacter("bashlgbq00TO_bqiztx", bashlgbq00TO.getBqiztx());
			query.setCharacter("bashlgbq00TO_bqi0tx", bashlgbq00TO.getBqi0tx());
			query.setCharacter("bashlgbq00TO_bqi1tx", bashlgbq00TO.getBqi1tx());
			query.setCharacter("bashlgbq00TO_bqi2tx", bashlgbq00TO.getBqi2tx());
			query.setCharacter("bashlgbq00TO_bqi3tx", bashlgbq00TO.getBqi3tx());
			query.setCharacter("bashlgbq00TO_bqi4tx", bashlgbq00TO.getBqi4tx());
			query.setCharacter("bashlgbq00TO_bqi5tx", bashlgbq00TO.getBqi5tx());
			query.setString("bashlgbq00TO_bqakcd", bashlgbq00TO.getBqakcd());
			query.setString("bashlgbq00TO_bqalcd", bashlgbq00TO.getBqalcd());
			query.setString("bashlgbq00TO_bqamcd", bashlgbq00TO.getBqamcd());
			query.setString("bashlgbq00TO_bqancd", bashlgbq00TO.getBqancd());
			query.setBigDecimal("bashlgbq00TO_bqaepc", bashlgbq00TO.getBqaepc());
			query.setBigDecimal("bashlgbq00TO_bqafpc", bashlgbq00TO.getBqafpc());
			query.setInteger("bashlgbq00TO_bqbknb", bashlgbq00TO.getBqbknb());
			query.setInteger("bashlgbq00TO_bqblnb", bashlgbq00TO.getBqblnb());
			query.setBigDecimal("bashlgbq00TO_bqagva", bashlgbq00TO.getBqagva());
			query.setBigDecimal("bashlgbq00TO_bqahva", bashlgbq00TO.getBqahva());
			query.setCharacter("bashlgbq00TO_bqj1tx", bashlgbq00TO.getBqj1tx());
			query.setInteger("bashlgbq00TO_bqaedt", bashlgbq00TO.getBqaedt());
			query.setBigDecimal("bashlgbq00TO_bqbuva", bashlgbq00TO.getBqbuva());
			query.setString("bashlgbq00TO_bqm1tx", bashlgbq00TO.getBqm1tx());
			query.setCharacter("bashlgbq00TO_bqm2tx", bashlgbq00TO.getBqm2tx());
			query.setCharacter("bashlgbq00TO_bqm3tx", bashlgbq00TO.getBqm3tx());
			query.setBigDecimal("bashlgbq00TO_bqb1va", bashlgbq00TO.getBqb1va());
			query.setBigDecimal("bashlgbq00TO_bqb2va", bashlgbq00TO.getBqb2va());

			int result = query.executeUpdate();
			status.setNumOfRows(result);

		} catch (org.hibernate.HibernateException ex) {
			status = new DBAccessStatus(ex);
		}
		return status;
	}

	public Bashlgbq00TO readHistoryFIle(String location, String policyno, String symbol, String masterco, String module,
			int unitno, int lossdate) {
			DBAccessStatus status = new DBAccessStatus();
			Bashlgbq00TO localbashlgbq00TO = null;	
		try{
			Session session = HibernateSessionFactory.current().getSession("BASHLGBQ00");
			Query query = session.getNamedQuery("Bashlgbq00TO_GETHISTORYRECORDS");
			query.setParameter("location", location);
			query.setParameter("masterco", masterco);
			query.setParameter("symbol", symbol);
			query.setParameter("policyno", policyno);			
			query.setParameter("module", module);
			query.setParameter("unitno", unitno);
			query.setParameter("lossdate", lossdate);
			query.setMaxResults(1);
			List resultList = query.list();


			if (resultList != null && resultList.size() > 0) {
				Object obj = resultList.get(0);
				if (obj != null) {
					localbashlgbq00TO = (Bashlgbq00TO)obj;
					
					status = new DBAccessStatus(DBAccessStatus.DB_OK);
					localbashlgbq00TO.setDBAccessStatus(status);
				} 
				
			}
			else status.setSqlCode(DBAccessStatus.DB_EOF);
		}
		 catch (HibernateException ex) {
			status.setException(ex);
		}
		 	
		return localbashlgbq00TO;
		
	}
}
