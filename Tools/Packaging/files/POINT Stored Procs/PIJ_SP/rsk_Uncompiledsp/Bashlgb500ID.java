// Copyright 2011-2014, Computer Sciences Corporation. All right reserved.
package com.csc.pt.svc.data.id;

import java.io.Serializable;

import bphx.sdf.datatype.ABODate;
import bphx.sdf.datatype.ABOTime;

//************************************************************************************************
//* Programmer  : Ankur Gupta							Date       : 10/13/2011                  *
//* Description : This java program will contain all the key fields of BASHLGB500 file.			 *
//************************************************************************************************

public class Bashlgb500ID implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4145976406227187563L;
	private int logSeqNum;
	private String location = new String();
	private String masterco = new String();
	private String symbol = new String();
	private String policyno = new String();
	private String module = new String();
	private String insline = new String();
	private int riskloc;
	private int risksubloc;
	private String product = new String();
	private int unitno;
	private char recstatus;

	public int getLogSeqNum() {
		return logSeqNum;
	}
	public void setLogSeqNum(int logSeqNum) {
		this.logSeqNum = logSeqNum;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getMasterco() {
		return masterco;
	}
	public void setMasterco(String masterco) {
		this.masterco = masterco;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getPolicyno() {
		return policyno;
	}
	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getInsline() {
		return insline;
	}
	public void setInsline(String insline) {
		this.insline = insline;
	}
	public int getRiskloc() {
		return riskloc;
	}
	public void setRiskloc(int riskloc) {
		this.riskloc = riskloc;
	}
	public int getRisksubloc() {
		return risksubloc;
	}
	public void setRisksubloc(int risksubloc) {
		this.risksubloc = risksubloc;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public int getUnitno() {
		return unitno;
	}
	public void setUnitno(int unitno) {
		this.unitno = unitno;
	}
	public char getRecstatus() {
		return recstatus;
	}
	public void setRecstatus(char recstatus) {
		this.recstatus = recstatus;
	}
}
