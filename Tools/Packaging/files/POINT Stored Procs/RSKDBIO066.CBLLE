       PROCESS APOST DATETIME.
       IDENTIFICATION DIVISION.                                                *
       PROGRAM-ID.  RSKDBIO066.                                                4

      ****************************************************************
      * This is the Stored procedure to return all the data for the  *
      * Policy Interest grid corresponding to following parameters   *
      * passed from the server-side:                                 *
      * 1. LOC                                                       *
      * 2. MCO                                                       *
      * 3. Symbol                                                    *
      * 4. Policy                                                    *
      * 5. Module Number                                             *
      * The following fields are set when the user enters data in the*
      * search fields:                                               *
      * 6. Interest Type                                             *
      * 7. Interest Location                                         *
      * 8. Interest Sequence                                         *
      * 9. Name                                                      *
      * 10. Address                                                  *
      * 11. City                                                     *
      * 12. State                                                    *
      * 13. Loss Date - This field has been added for Riskmaster     *
      ****************************************************************
82852A*****************************************************************
82852A* REF#:  82852     PGMR: Nidhi Bhutani       DATE: 12/24/2013   *
82852A* PROJ#: B206838   FSIT#:206838              SRC :  New         *
82852A* Issue: OTH - Additional Interest List is also passing expired *
82852A*        interest to RMA                                        *
82852A* RESOLUTION:                                                   *
82852A*        Excluded additional interest based on loss date passed *
82852A*        by RMA                                                 *
82852A*****************************************************************
86655A* REF#:  86655     PGMR: Chirag Saini        DATE: 04/16/2014   *
86655A* PROJ#: B206838   FSIT#:212407              SRC : BNRDP2OBJ    *
86655A* Issue:OTH - Client Sequence Number and Address Sequence Number*
86655A*        need to be passed for policy interest list.            *
86655A* RESOLUTION:                                                   *
86655A*        Modified the stored procedure to also pass             *
86655A*        clientseqnum and addressseqnum.                        *
86655A*****************************************************************
      **********************
       ENVIRONMENT DIVISION.
      **********************
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-AS400.
       OBJECT-COMPUTER. IBM-AS400.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      ***************
       DATA DIVISION.
      ***************
      *-----------------------*
       WORKING-STORAGE SECTION.
      *-----------------------*
      *------------------------------------*
      *    SQL I-O feedback area: REQUIRED *
      *------------------------------------*
           EXEC SQL
                SET OPTION DATFMT = *ISO
           END-EXEC.

           EXEC SQL
                SET OPTION COMMIT = *NONE
           END-EXEC.

           EXEC SQL
                SET OPTION DATSEP = *DASH
           END-EXEC.

           EXEC SQL
                SET OPTION TIMFMT = *ISO
           END-EXEC.

           EXEC SQL
                SET OPTION TIMSEP = *COLON
           END-EXEC.

           EXEC SQL
              INCLUDE SQLCA
           END-EXEC.
      *--------------------*
      * SQL DECLARE BEGIN: *
      *--------------------*
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
      *
       01  WS1-SELECT-STATEMENT               PIC X(10000).
       01  NUM-OF-ENTRYS                      PIC S9(05).
       01  NUM-OF-ENTRYS-FOR-SQL              PIC S9(09) COMP-4.
       01  POLINT-TABLE.
           05 POLINT-ENTRY OCCURS 5000 TIMES.
              10 PI-INT-TYPE          PIC X(02).
              10 PI-INT-LOCATION      PIC X(05).
              10 PI-INT-SEQ           PIC X(05).
              10 PI-NAME              PIC X(30).
              10 PI-ADDRESS           PIC X(30).
              10 PI-CITY              PIC X(28).
              10 PI-STATE             PIC X(02).
              10 PI-ISSUE-CODE        PIC X(01).
86655A        10 PI-CLIENT-SEQ        PIC X(10).
86655A        10 PI-ADDRESS-SEQ       PIC X(02).
              10 PI-NUM-OF-RECORDS    PIC X(05).
      *
      *   Additional Interests (12)
      *
       01  PMSP1200-REC.
           COPY DDS-ALL-FORMATS OF PMSP1200
              REPLACING REC12FMT  BY POLINT-RECORD.
      *
       01  WS-RECORD-1501.
           COPY DDS-ALL-FORMATS OF BASCLT1500.
      *
       01  WS-LOCATION            PIC X(02).
       01  WS-MASTERCO            PIC X(02).
       01  WS-SYMBOL              PIC X(03).
       01  WS-POLICY-NUM          PIC X(07).
       01  WS-MODULE              PIC X(02).
       01  WS-LOSSDTE             PIC X(07).
       01  WS-ISSUE0CODE          PIC X(01).
       01  WS-POL-LIMIT           PIC S9(05).
       01  WS-LINE-OF-BUS         PIC X(03).
       01  WS-INS-LINE            PIC X(03).
       01  WS-NAME                PIC X(30).
       01  WS-ADDRESS             PIC X(30).
       01  WS-CITY-STATE          PIC X(30).
           05  WS-CITY            PIC X(28).
           05  WS-STATE           PIC X(02).
       01  WS-ID02                PIC X(02) VALUE '02'.
       01  WS-ID12                PIC X(02) VALUE '12'.
       01  WS-DESC0LINE1          PIC X(30).
       01  WS-DESC0LINE3          PIC X(30).
       01  WS-DESC0LINE4          PIC X(30).
           05  WS-SPLIT-CITY      PIC X(28).
           05  WS-SPLIT-STATE     PIC X(02).
       01  WS-STATUS-OWNERSHIP    PIC S9(06)V9(3).
       01  WS-STATUS-TITLE-C      PIC X(2).
       01  WS-STATUS-OWNSALARY    PIC S9(09).
       01  WS-LEGAL-ENTITY        PIC X(02).
       01  WS-BC-STATE            PIC X(2).
       01  WS-DEEMED-CODE         PIC X(2).
       01  WS-DEEMED-STATUS       PIC X(1).
       01  WS-SSN-NO              PIC X(11).
       01  LINK-RETURN-CODE       PIC X(07).
       01  LINK-CLIENT-SEQ-NUM    PIC S9(10) VALUE ZEROS.
       01  LINK-ADDR-SEQ-NUM      PIC S9(02) VALUE ZEROS.
       01  LINK-LOCATE-NAME       PIC X(30).
       01  WS-TYPE                PIC X(2).
       01  WS-LOC                 PIC X(5).
       01  WS-DESCSEQ             PIC X(5).

       01  WS-LOSS-EXP-DATE.
           05  WS-LOSS-EXP-CC      PIC X(02).
           05  WS-LOSS-EXP-YY      PIC X(02).
           05  WS-LOSS-EXP-H1      PIC X(01) VALUE '-'.
           05  WS-LOSS-EXP-MM      PIC X(02).
           05  WS-LOSS-EXP-H2      PIC X(01) VALUE '-'.
           05  WS-LOSS-EXP-DD      PIC X(02).
      *
           EXEC SQL END DECLARE SECTION END-EXEC.
      *-----------------*
      * SQL DECLARE END *
      *-----------------*
      *
      *-------------------------*
      * DECLARE the SQL cursors *
      *-------------------------*----------------------------------*
      *  CHECK_CURSOR is a cursor that is used with dynamically    *
      *          built SELECT statements that are contained in a   *
      *          string, and is needs to be prepared for execution *
      *          using the SQL PREPARE statement, before the cursor*
      *          can be opened.                                    *
      *------------------------------------------------------------*
           EXEC SQL
                DECLARE SQL_CURSOR CURSOR FOR
                        SQL_STATEMENT
           END-EXEC.
      *----------------------*
      *    SQL Return Codes  *
      *----------------------*
           COPY CPYSQLRET.
           COPY CPYLKPPI00.
      *
       01  WS1-STR-POS             PIC 9(05) VALUE ZEROS.
      *
       01  WS-WORK-FIELDS.
           05  WS-RETURN-CODE         PIC X(07).
               88 GOOD-RETURN-FROM-IO   VALUE '0000000'.
           05  WS-CONTROL-SW          PIC X(01).
      *
       LINKAGE SECTION.
       01  LS-INPUT-PARAMS.
           05 LS-LOCATION             PIC X(02).
           05 LS-MASTERCO             PIC X(02).
           05 LS-SYMBOL               PIC X(03).
           05 LS-POLICY-NUM           PIC X(07).
           05 LS-MODULE               PIC X(02).
           05 LS-INT-TYPE             PIC X(02).
           05 LS-INT-LOCATION         PIC X(05).
           05 LS-INT-SEQ              PIC X(05).
           05 LS-NAME                 PIC X(31).
           05 LS-ADDRESS              PIC X(31).
           05 LS-CITY                 PIC X(29).
           05 LS-STATE                PIC X(03).
           05 LS-LOSSDTE              PIC X(07).
      *
       77  LS-WHERE-CLAUSE            PIC X(200).
      *
      ********************
       PROCEDURE DIVISION USING
                 LS-INPUT-PARAMS.
      ********************
       0000-MAIN-PARAGRAPH.
      *
           PERFORM 0100-INIT              THRU 0100-EXIT.
           PERFORM 0500-GET-ISSUE-CODE    THRU 0500-EXIT.
           PERFORM 0600-GET-POL-LIMIT     THRU 0600-EXIT.
           PERFORM 1000-LOAD-POLINTS      THRU 1000-EXIT.
           PERFORM 4000-PASS-POLINTS      THRU 4000-EXIT.
      *
       0000-EXIT-PROGRAM.
           GOBACK.
           EXIT PROGRAM.
      *
       0100-INIT.
            MOVE ZEROS             TO NUM-OF-ENTRYS.
            MOVE LS-LOCATION       TO WS-LOCATION.
            MOVE LS-MASTERCO       TO WS-MASTERCO.
            MOVE LS-SYMBOL         TO WS-SYMBOL.
            MOVE LS-POLICY-NUM     TO WS-POLICY-NUM.
            MOVE LS-MODULE         TO WS-MODULE.
            MOVE LS-LOSSDTE        TO WS-LOSSDTE.
            IF  WS-LOSSDTE NOT =  SPACES
            AND WS-LOSSDTE NOT =  ZEROES
                IF WS-LOSSDTE(1:1) EQUAL TO "1"
                   MOVE "20" TO  WS-LOSS-EXP-CC
                ELSE
                   MOVE "19" TO  WS-LOSS-EXP-CC
                END-IF
                MOVE WS-LOSSDTE(2:2)   TO WS-LOSS-EXP-YY
                MOVE WS-LOSSDTE(4:2)   TO WS-LOSS-EXP-MM
                MOVE WS-LOSSDTE(6:2)   TO WS-LOSS-EXP-DD
            END-IF.

            INITIALIZE  WS-STATUS-OWNERSHIP
                        WS-STATUS-TITLE-C
                        LINK-RETURN-CODE
                        WS-LEGAL-ENTITY
                        WS-BC-STATE
                        WS-STATUS-OWNSALARY
                        WS-DEEMED-CODE
                        WS-DEEMED-STATUS
                        WS-SSN-NO.

       0100-EXIT.
            EXIT.
      *
       0500-GET-ISSUE-CODE.
           EXEC SQL
                SELECT DISTINCT ISSUE0CODE
                       INTO :WS-ISSUE0CODE
                       FROM PMSP0200
                WHERE
                   ID02       = :WS-ID02        AND
                   SYMBOL     = :WS-SYMBOL      AND
                   POLICY0NUM = :WS-POLICY-NUM  AND
                   MODULE     = :WS-MODULE      AND
                   MASTER0CO  = :WS-MASTERCO    AND
                   LOCATION   = :WS-LOCATION
           END-EXEC.
       0500-EXIT.
           EXIT.
      *
       0600-GET-POL-LIMIT.
           MOVE '*PL' TO WS-LINE-OF-BUS.
           MOVE '*PL' TO WS-INS-LINE.
           PERFORM 0750-READ-BASSYS0500 THRU 0750-EXIT
           IF (SQLCODE NOT EQUAL SQL-IO-OK)
              MOVE 100 TO WS-POL-LIMIT
           END-IF.
       0600-EXIT.
           EXIT.

       0750-READ-BASSYS0500.
           EXEC SQL
                SELECT LIMIT5     INTO :WS-POL-LIMIT
                       FROM BASSYS0500
                WHERE
                   LOB        = :WS-LINE-OF-BUS AND
                   INSLINE    = :WS-INS-LINE
           END-EXEC.
       0750-EXIT.
           EXIT.
      *
       1000-LOAD-POLINTS.
      *
           MOVE SPACES TO WS1-SELECT-STATEMENT.
           INITIALIZE POLINT-RECORD.
           PERFORM 2000-BUILD-SELECT-STATEMENT THRU 2000-EXIT.
      *
           EXEC SQL
              PREPARE SQL_STATEMENT FROM :WS1-SELECT-STATEMENT
           END-EXEC.
      *
           EXEC SQL
              OPEN SQL_CURSOR
           END-EXEC.
      *
           PERFORM 8000-FETCH-RECORD THRU 8000-EXIT.
      *
           ADD 1 TO NUM-OF-ENTRYS.
           MOVE WS-ISSUE0CODE TO PI-ISSUE-CODE (NUM-OF-ENTRYS).
           ADD 1 TO NUM-OF-ENTRYS.
           IF NOT (USE0CODE   OF POLINT-RECORD EQUAL SPACES
               AND USE0LOC    OF POLINT-RECORD EQUAL SPACES
               AND DESC0SEQ   OF POLINT-RECORD EQUAL SPACES
               AND DSORT0NAME OF POLINT-RECORD EQUAL SPACES)
                   MOVE ZEROS TO SQLCODE
                   PERFORM 3000-LOAD-RECORDS THRU 3000-EXIT
                   VARYING NUM-OF-ENTRYS
                      FROM NUM-OF-ENTRYS
                        BY 1
                   UNTIL SQLCODE NOT EQUAL SQL-IO-OK
                      OR NUM-OF-ENTRYS > WS-POL-LIMIT + 1
           END-IF.
      *
           SUBTRACT 1 FROM NUM-OF-ENTRYS
           MOVE NUM-OF-ENTRYS TO PI-NUM-OF-RECORDS (2)
      *
           EXEC SQL
              CLOSE SQL_CURSOR
           END-EXEC.
       1000-EXIT.  EXIT.
      *
       2000-BUILD-SELECT-STATEMENT.
      *---------------------------------------------------------*
      * Set the address of LS-WHERE-CLAUSE to the address of    *
      * WS1-SELECT-STATEMENT. This will overlay LS-WHERE-CLAUSE *
      * onto WS1-SELECT-STATEMENT, this is the same as          *
      * redefining WS1-SELECT-STATEMENT at run time.            *
      *---------------------------------------------------------*
           SET ADDRESS OF LS-WHERE-CLAUSE TO
               ADDRESS OF WS1-SELECT-STATEMENT.
           STRING
                  'SELECT'
                  ' USE0CODE,'
                  ' USE0LOC,'
                  ' DESC0SEQ,'
                  ' DSORT0NAME'
                  ' FROM PMSP1200 WHERE ('
                 X'00'                      DELIMITED BY SIZE
                           INTO LS-WHERE-CLAUSE.
      *-------------------------------------------------------*
      * Set the Address of the LS-WHERE-CLAUSE so that we can *
      * add the restrector fields to the WHERE clause.        *
      *-------------------------------------------------------*
           PERFORM 9000-SET-ADDRESS THRU 9000-EXIT.
      *
      * Location
      *
           IF LS-LOCATION NOT EQUAL SPACES
              STRING ' LOCATION = "'            DELIMITED BY SIZE
                  LS-LOCATION                   DELIMITED BY SIZE
                  '"'                           DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
           END-IF.
      *
      * Master Company
      *
           IF LS-MASTERCO NOT EQUAL SPACES
              STRING ' AND MASTER0CO = "'       DELIMITED BY SIZE
                  LS-MASTERCO                   DELIMITED BY SIZE
                  '"'                           DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
           END-IF.
      *
      * Policy Symbol
      *
           IF LS-SYMBOL NOT EQUAL SPACES
              STRING ' AND SYMBOL = "'          DELIMITED BY SIZE
                  LS-SYMBOL                     DELIMITED BY SIZE
                  '"'                           DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
           END-IF.
      *
      * Policy Number
      *
           IF LS-POLICY-NUM NOT EQUAL SPACES
              STRING ' AND POLICY0NUM = "'      DELIMITED BY SIZE
                  LS-POLICY-NUM                 DELIMITED BY SIZE
                  '"'                           DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
           END-IF.
      *
      * Module
      *
           IF LS-MODULE NOT EQUAL SPACES
              STRING ' AND MODULE = "'          DELIMITED BY SIZE
                  LS-MODULE                     DELIMITED BY SIZE
                  '"'                           DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
           END-IF.
      *
      * Interest Location
      *
           IF LS-INT-LOCATION NOT EQUAL SPACES
              STRING ' AND USE0LOC >= "'        DELIMITED BY SIZE
                  LS-INT-LOCATION               DELIMITED BY SIZE
                  '"'                           DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
           END-IF.
      *
      * Interest Sequence
      *
           IF LS-INT-SEQ NOT EQUAL SPACES
              STRING ' AND DESC0SEQ >= "'       DELIMITED BY SIZE
                  LS-INT-SEQ                    DELIMITED BY SIZE
                  '"'                           DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
           END-IF.
      *
      * Expiration Date filtered out by Loss Date
      *
           IF   LS-LOSSDTE NOT = SPACES
           AND  LS-LOSSDTE NOT = ZEROES
              STRING ' AND EXPDATE  >= "'       DELIMITED BY SIZE
                  WS-LOSS-EXP-DATE              DELIMITED BY SIZE
                  '"'                           DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
              STRING ' AND  EFFDATE  <= "'      DELIMITED BY SIZE
                  WS-LOSS-EXP-DATE              DELIMITED BY SIZE
                  '"'                           DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
           END-IF.

      * Record Status
      * Future records in out of sequence are stored with record status
      * value as 'H'. These records should not be shown.
      *
           STRING ' AND TRANS0STAT <> "H"'   DELIMITED BY SIZE
               X'00'                         DELIMITED BY SIZE
               INTO LS-WHERE-CLAUSE
           PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
      *
      * Flag. This condition is being added to prevent site interest
      *       details from showing on the Policy Level Interest List.
      *
              STRING ' AND FLAG != "Y"'         DELIMITED BY SIZE
                  X'00'                         DELIMITED BY SIZE
                  INTO LS-WHERE-CLAUSE
              PERFORM 9000-SET-ADDRESS THRU 9000-EXIT
      *
      *--------------------------*
      * Close the WHERE clause.  *
      * Add the ORDER BY clause. *
      *--------------------------*
      *
           STRING
                ') ORDER BY'
                  ' USE0CODE,'
                  ' USE0LOC,'
                  ' DESC0SEQ'
                                          DELIMITED BY SIZE
                                          INTO LS-WHERE-CLAUSE.
       2000-EXIT.  EXIT.
      *
       3000-LOAD-RECORDS.
           PERFORM 3005-LOAD-RECS THRU 3005-EXIT.
      *
      * Name Search
      *
           MOVE WS-DESC0LINE1 TO WS-NAME.
           IF LS-NAME NOT EQUAL SPACES
              MOVE 1 TO WS1-STR-POS
              INSPECT LS-NAME TALLYING WS1-STR-POS
                      FOR CHARACTERS
                      BEFORE INITIAL '%'
              IF FUNCTION UPPER-CASE(WS-NAME(1:(WS1-STR-POS - 1)))
                 NOT EQUAL TO LS-NAME(1:(WS1-STR-POS - 1))
                      SUBTRACT 1 FROM NUM-OF-ENTRYS
                      PERFORM 8000-FETCH-RECORD THRU 8000-EXIT
                      GO TO 3000-EXIT
              END-IF
           END-IF.
      *
      * Address Search
      *
           MOVE WS-DESC0LINE3 TO WS-ADDRESS.
           IF LS-ADDRESS NOT EQUAL SPACES
              MOVE 1 TO WS1-STR-POS
              INSPECT LS-ADDRESS TALLYING WS1-STR-POS
                      FOR CHARACTERS
                      BEFORE INITIAL '%'
              IF FUNCTION UPPER-CASE(WS-ADDRESS(1:(WS1-STR-POS - 1)))
                 NOT EQUAL TO LS-ADDRESS(1:(WS1-STR-POS - 1))
                      SUBTRACT 1 FROM NUM-OF-ENTRYS
                      PERFORM 8000-FETCH-RECORD THRU 8000-EXIT
                      GO TO 3000-EXIT
              END-IF
           END-IF.
      *
      * City Search
      *
           MOVE WS-DESC0LINE4 TO WS-CITY-STATE.
           IF LS-CITY NOT EQUAL SPACES
              MOVE 1 TO WS1-STR-POS
              INSPECT LS-CITY TALLYING WS1-STR-POS
                      FOR CHARACTERS
                      BEFORE INITIAL '%'
              IF FUNCTION UPPER-CASE(WS-CITY(1:(WS1-STR-POS - 1)))
                 NOT EQUAL TO LS-CITY(1:(WS1-STR-POS - 1))
                      SUBTRACT 1 FROM NUM-OF-ENTRYS
                      PERFORM 8000-FETCH-RECORD THRU 8000-EXIT
                      GO TO 3000-EXIT
              END-IF
           END-IF.
      *
      * State Search
      *
           IF LS-STATE NOT EQUAL SPACES
              MOVE 1 TO WS1-STR-POS
              INSPECT LS-STATE TALLYING WS1-STR-POS
                      FOR CHARACTERS
                      BEFORE INITIAL '%'
              IF FUNCTION UPPER-CASE(WS-STATE(1:(WS1-STR-POS - 1)))
                 NOT EQUAL TO LS-STATE(1:(WS1-STR-POS - 1))
                      SUBTRACT 1 FROM NUM-OF-ENTRYS
                      PERFORM 8000-FETCH-RECORD THRU 8000-EXIT
                      GO TO 3000-EXIT
              END-IF
           END-IF.
      *
      * Type Search
      *
           IF LS-INT-TYPE NOT EQUAL SPACES
              MOVE 1 TO WS1-STR-POS
              INSPECT LS-INT-TYPE TALLYING WS1-STR-POS
                      FOR CHARACTERS
                      BEFORE INITIAL '%'
              IF FUNCTION UPPER-CASE(WS-TYPE(1:(WS1-STR-POS - 1)))
                 NOT EQUAL TO LS-INT-TYPE(1:(WS1-STR-POS - 1))
                      SUBTRACT 1 FROM NUM-OF-ENTRYS
                      PERFORM 8000-FETCH-RECORD THRU 8000-EXIT
                      GO TO 3000-EXIT
              END-IF
           END-IF.
      *
      * Loc Search
      *
           IF LS-INT-LOCATION NOT EQUAL SPACES
              IF WS-LOC NOT EQUAL TO LS-INT-LOCATION
                      SUBTRACT 1 FROM NUM-OF-ENTRYS
                      PERFORM 8000-FETCH-RECORD THRU 8000-EXIT
                      GO TO 3000-EXIT
              END-IF
           END-IF.
      *
      * Seq Search
      *
           IF LS-INT-SEQ NOT EQUAL SPACES
              IF WS-DESCSEQ NOT EQUAL TO LS-INT-SEQ
                      SUBTRACT 1 FROM NUM-OF-ENTRYS
                      PERFORM 8000-FETCH-RECORD THRU 8000-EXIT
                      GO TO 3000-EXIT
              END-IF
           END-IF.
      *
           MOVE USE0CODE      OF POLINT-RECORD
                              TO PI-INT-TYPE     (NUM-OF-ENTRYS).
           MOVE USE0LOC       OF POLINT-RECORD
                              TO PI-INT-LOCATION (NUM-OF-ENTRYS).
           MOVE DESC0SEQ      OF POLINT-RECORD
                              TO PI-INT-SEQ      (NUM-OF-ENTRYS).
           MOVE WS-DESC0LINE1 TO PI-NAME         (NUM-OF-ENTRYS).
           MOVE WS-DESC0LINE3 TO PI-ADDRESS      (NUM-OF-ENTRYS).
           MOVE WS-ISSUE0CODE TO PI-ISSUE-CODE   (NUM-OF-ENTRYS).
      *
           MOVE WS-DESC0LINE4 TO WS-CITY-STATE.
           MOVE WS-CITY       TO PI-CITY         (NUM-OF-ENTRYS).
           MOVE WS-STATE      TO PI-STATE        (NUM-OF-ENTRYS).
86655A     MOVE LINK-CLIENT-SEQ-NUM TO PI-CLIENT-SEQ  (NUM-OF-ENTRYS).
86655A     MOVE LINK-ADDR-SEQ-NUM   TO PI-ADDRESS-SEQ (NUM-OF-ENTRYS).
      *
           PERFORM 8000-FETCH-RECORD THRU 8000-EXIT.
       3000-EXIT.  EXIT.
      *
       3005-LOAD-RECS.
           PERFORM 3010-RTV-SEQ-NUM THRU 3010-EXIT.
           MOVE LOW-VALUES          TO LNK-PARMS-RCRTVPPI00.
           MOVE USE0CODE            OF POLINT-RECORD
                                    TO LNK-USE-CODE.
           MOVE USE0LOC             OF POLINT-RECORD
                                    TO LNK-USE-LOC.
           MOVE DESC0SEQ            OF POLINT-RECORD
                                    TO LNK-DESC-SEQ.
           MOVE DSORT0NAME          OF POLINT-RECORD
                                    TO LNK-SORT-CODE.
           MOVE '12'                TO ID12        OF POLINT-RECORD.
           MOVE 'V'                 TO TRANS0STAT  OF POLINT-RECORD.
           MOVE LS-SYMBOL           TO LNK-SYM
                                       SYMBOL      OF POLINT-RECORD.
           MOVE LS-POLICY-NUM       TO LNK-POL
                                       POLICY0NUM  OF POLINT-RECORD.
           MOVE LS-MODULE           TO LNK-MOD
                                       MODULE      OF POLINT-RECORD.
           MOVE LS-LOCATION         TO LNK-LOC
                                       LOCATION    OF POLINT-RECORD.
           MOVE LS-MASTERCO         TO LNK-MCO
                                       MASTER0CO   OF POLINT-RECORD.
           MOVE 'GE'                TO LNK-READ-TYPE.

           CALL 'RCRTVPPI00' USING     LNK-PARMS-RCRTVPPI00
                                       WS-STATUS-OWNERSHIP
                                       WS-STATUS-TITLE-C
                                       WS-STATUS-OWNSALARY
                                       WS-LEGAL-ENTITY
                                       WS-BC-STATE
                                       WS-DEEMED-CODE
                                       WS-DEEMED-STATUS
                                       WS-SSN-NO.
     *
           CALL 'BASCLTB14'  USING     LINK-RETURN-CODE
                                       POLINT-RECORD
                                       LINK-CLIENT-SEQ-NUM
                                       LINK-ADDR-SEQ-NUM
                                       LINK-LOCATE-NAME
           END-CALL
           CANCEL 'BASCLTB14'.
     *
           MOVE LNK-NAME            TO WS-DESC0LINE1
           MOVE LNK-STREET-ADDRESS  TO WS-DESC0LINE3
           MOVE LNK-CITYST-ADDRESS  TO DESC0LINE4 OF POLINT-RECORD
           MOVE LNK-USE-CODE        TO WS-TYPE
           MOVE LNK-USE-LOC         TO WS-LOC
           MOVE LNK-DESC-SEQ        TO WS-DESCSEQ
           MOVE DESC0LINE4 OF POLINT-RECORD TO WS-DESC0LINE4

       3005-EXIT.  EXIT.
      *
      *  Call to BASCLT1501 to retrieve the Client sequence number and
      *  Address sequence number.
       3010-RTV-SEQ-NUM.
           MOVE LS-LOCATION    TO  LOCATION   OF WS-RECORD-1501.
           MOVE LS-MASTERCO    TO  MASTER0CO  OF WS-RECORD-1501.
           MOVE LS-SYMBOL      TO  SYMBOL     OF WS-RECORD-1501.
           MOVE LS-POLICY-NUM  TO  POLICY0NUM OF WS-RECORD-1501.
           MOVE LS-MODULE      TO  MODULE     OF WS-RECORD-1501.
           MOVE USE0CODE       OF  POLINT-RECORD
                               TO  USE0CODE   OF WS-RECORD-1501.
           MOVE USE0LOC        OF  POLINT-RECORD
                               TO  USE0LOC    OF WS-RECORD-1501.
           MOVE DESC0SEQ       OF  POLINT-RECORD
                               TO  DESC0SEQ   OF WS-RECORD-1501.
           CALL 'BASCLT1501' USING LINK-RETURN-CODE
                                   WS-RECORD-1501
           CANCEL 'BASCLT1501'.
      * If the record is found
           IF  LINK-RETURN-CODE IS EQUAL TO '0000000'
               MOVE CLTSEQNUM  OF WS-RECORD-1501 TO LINK-CLIENT-SEQ-NUM
               MOVE ADDRSEQNUM OF WS-RECORD-1501 TO LINK-ADDR-SEQ-NUM
           ELSE
               MOVE ZEROS TO LINK-CLIENT-SEQ-NUM
               MOVE ZEROS TO LINK-ADDR-SEQ-NUM
           END-IF.
       3010-EXIT.
           EXIT.
      *
       4000-PASS-POLINTS.
           MOVE NUM-OF-ENTRYS TO NUM-OF-ENTRYS-FOR-SQL.
           EXEC SQL
                SET RESULT SETS
                            WITH RETURN TO CLIENT
                           ARRAY :POLINT-ENTRY
                             FOR :NUM-OF-ENTRYS-FOR-SQL ROWS
           END-EXEC.
       4000-EXIT.
           EXIT.
      *
       8000-FETCH-RECORD.
           EXEC SQL
              FETCH SQL_CURSOR  INTO
                                   :POLINT-RECORD.USE0CODE,
                                   :POLINT-RECORD.USE0LOC,
                                   :POLINT-RECORD.DESC0SEQ,
                                   :POLINT-RECORD.DSORT0NAME
           END-EXEC.
      *
           IF SQLCODE NOT EQUAL SQL-IO-OK
              GO TO 8000-EXIT
           END-IF.
      *
       8000-EXIT.  EXIT.
      *
       9000-SET-ADDRESS.
           MOVE 1 TO WS1-STR-POS.
      *-------------------------------------------------------------*
      * Calculate the WS1-STR-POS by getting the number of chars    *
      * before the X'00'.                                           *
      *-------------------------------------------------------------*
           INSPECT WS1-SELECT-STATEMENT TALLYING  WS1-STR-POS
                   FOR CHARACTERS
                   BEFORE INITIAL X'00'.

      *    IF WS-STR-POS GREATER THAN 31000 THEN
      *            SET ERROR-OCCURRED TO TRUE
      *            GO TO 9000-EXIT
      *    END-IF.

      *-------------------------------------------------------------*
      * The STRING statement can not have a referential modified    *
      * field as the INTO fields. So we use a field that has been   *
      * defined in linkage named LS-WHERE-CLAUSE. We set the        *
      * address of LS-WHERE-CLAUSE to the address of the part of    *
      * field that we want to add the where condition to.           *
      *-------------------------------------------------------------*
           SET ADDRESS OF LS-WHERE-CLAUSE TO
               ADDRESS OF WS1-SELECT-STATEMENT(WS1-STR-POS:1).
           MOVE SPACES TO LS-WHERE-CLAUSE.
       9000-EXIT.
           EXIT.
