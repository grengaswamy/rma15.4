<%@ page session="false" %>
<%@ page import="javax.servlet.http.Cookie" %>
<html>
	<head>
		<title>CSC Authentication Service</title>
	</head>
<body>
	<form id='RMXFrmLogin' name='' method='post'>
        <input id='username' name='username' value='' type='hidden' />
    </form>
<%!
private final static String CONFIG_SECURE_LOGIN_URL      = "secure.login.url";
private static final String TGC_ID = "CASTGC";
private static final String PRIVACY_ID = "CASPRIVACY";

%>

<%
 
  String serviceId = (String) request.getParameter("casurl");
  String token = (String) request.getParameter("token");
  String ticket = (String) request.getParameter("ticket");
  String action = (String) request.getParameter("action");
  String uid = (String) request.getParameter("username");
  
  String service = serviceId;
  
     Cookie tgc = new Cookie(TGC_ID, ticket);
    tgc.setMaxAge(-1);
      	            
    response.addCookie(tgc);
    	       
 if (serviceId.indexOf('?') == -1)
    service = serviceId + "?casticket=" + token;
  else
    service = serviceId + "&casticket=" + token;
    
    service = service + "&action=" + action + "&login=true";    
  service =
    edu.yale.its.tp.cas.util.StringUtil.substituteAll(service, "\n", "");
  service = 
    edu.yale.its.tp.cas.util.StringUtil.substituteAll(service, "\r", "");
  service =
    edu.yale.its.tp.cas.util.StringUtil.substituteAll(service, "\"", "");
	System.out.println("New goService URL =" + service);
      	            
//  ?casticket=
%>

 <script>
    document.RMXFrmLogin.action = "<%= service %>";
    document.getElementById('username').value="<%=uid%>";    	
    document.RMXFrmLogin.submit();		
 </script>


 <noscript>
	  <p>Login successful.</p>
	  <p>
	   Click <a href="<%= service %>" />"here</a>
	   to access the service you requested.
	  </p>
 </noscript>
</body>

</html>
