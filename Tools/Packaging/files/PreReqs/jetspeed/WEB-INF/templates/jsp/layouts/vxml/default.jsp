<?xml version="1.0" encoding="iso-8859-1"?>
<!--
Copyright 2004 The Apache Software Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<vxml version="1.0">

    <form id="home">
        <block>
            <prompt>
                Welcome to Jetspeed.
                Jetspeed is an open source enterprise information portal.
            </prompt>
        </block>
    </form>
</vxml>
