<%--
Copyright 2004 The Apache Software Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ taglib uri='/WEB-INF/templates/jsp/tld/template.tld' prefix='jetspeed' %>

<%-- The default error screen that will be displayed if no specific screen is set --%>

<%@ page import = "org.apache.turbine.util.*" %> 
<%@ page import = "java.util.*" %> 


<% RunData rundata = (RunData)request.getAttribute("rundata"); %>

<table border=1 cellpadding="5"> 

<%-- Error Message --%>
  <tr> 
    <td>
      <br>
      <h2>There has been an Error!</h2>
      Reason:
      <pre>
        <%= rundata.getStackTrace() %>
      </pre>
    </td>
  </tr> 


<%-- HTTP Parameters --%>
<% 
  Enumeration keys;
  String key;
  String value;

  keys = rundata.getParameters().keys();
  if (keys.hasMoreElements()) {
%>
    <tr> 
      <td>
        <br>
        <h3>Get/Post Data:</h3>
          <table border=0> 
<%
          keys = rundata.getParameters().keys();
          while ( keys.hasMoreElements() )
          {
            key   = (String) keys.nextElement();
%>
            <tr>
              <td><b> <%= key %> </b></td>
              <td> =  <%= rundata.getParameters().getString(key) %></td>
            <tr>
<%
          }
%>
        </table>
      </td> 
    </tr> 
  <% 
  } 
  %>

<%-- Debug Keys --%>
<%
  keys = rundata.getVarDebug().keys();
  if (keys.hasMoreElements()) {
%>
    <tr> 
      <td>
        <br>
        <h3>Debugging Data:</h3>
          <table border=0> 
<%
          while ( keys.hasMoreElements() )
          {
            key   = (String) keys.nextElement();
%>
            <tr>
              <td><b> <%= key %> </b></td>
              <td> =  <%= rundata.getVarDebug().get(key) %> </td>
            <tr>
<%
          }
%>
        </table>
      </td> 
    </tr> 
<% 
  } 
%>

<%-- Stacktrace --%>
  <tr> 
    <td>
      <br>
      <h3>Stacktrace:</h3>
      <pre>
<%      
        if (rundata.getStackTrace() != null) {
%>
        <%= rundata.getStackTrace() %>
<%      
        }
%>
      </pre>
    </td> 
  </tr> 

</table>