CREATE OR REPLACE PROCEDURE USP_COPY_EXISTING_COMMENTS
AS
/******************************************************************************
   NAME:       USP_COPY_EXISTING_COMMENTS 
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/19/2009          1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     USP_COPY_EXISTING_COMMENTS 
      Sysdate:         9/19/2009
      Date and Time:   9/19/2009, 8:50:25 PM, and 9/19/2009 8:50:25 PM
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
    
    --Clear out the current contents of the Comments_Text table
    --TRUNCATE TABLE comments_text REUSE STORAGE;   
    
    DECLARE EVENT_COMMENTIDvar int; 
    
    --Declare the Event Cursor
    CURSOR EVENT_COMMENT_CURSOR IS
     SELECT EVENT_ID, COMMENTS, HTMLCOMMENTS FROM event e
    WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;
    --AND DBMS_LOB.GETLENGTH(HTMLCOMMENTS) > 0;
    EVENT_COMMENT_VAL EVENT_COMMENT_CURSOR%ROWTYPE;
    
    BEGIN        
         --open the cursor
        FOR EVENT_COMMENT_VAL IN EVENT_COMMENT_CURSOR LOOP
            SELECT NVL(MAX(COMMENT_ID), 0) INTO EVENT_COMMENTIDvar FROM comments_text ct
            WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;
        
            IF EVENT_COMMENTIDvar != 0 THEN
                -- EVENT_COMMENT_VALUE.
               INSERT INTO COMMENTS_TEXT
               (
                     COMMENT_ID,
                     ATTACH_RECORDID,
                     ATTACH_TABLE,
                     COMMENTS,
                     HTMLCOMMENTS
               )
                VALUES
                (
                     EVENT_COMMENTIDvar + 1,
                     EVENT_COMMENT_VAL.EVENT_ID,
                     'EVENT',
                     EVENT_COMMENT_VAL.COMMENTS,
                     EVENT_COMMENT_VAL.HTMLCOMMENTS
                );
            END IF;
            
        END LOOP;
    END;     
    
    DECLARE CLAIM_COMMENTIDvar int;
    
    --Declare the Claim Cursor
    CURSOR CLAIM_COMMENT_CURSOR IS
     SELECT CLAIM_ID, COMMENTS, HTMLCOMMENTS FROM CLAIM 
     WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;
     --AND DBMS_LOB.GETLENGTH(HTMLCOMMENTS) > 0;
    CLAIM_COMMENT_VAL CLAIM_COMMENT_CURSOR%ROWTYPE;
    
    BEGIN
        FOR CLAIM_COMMENT_VALUE IN CLAIM_COMMENT_CURSOR LOOP
            SELECT NVL(MAX(COMMENT_ID), 0) INTO CLAIM_COMMENTIDvar FROM comments_text ct
            WHERE DBMS_LOB.GETLENGTH(COMMENTS) > 0;
        
            IF CLAIM_COMMENTIDvar !=0 THEN
               -- CLAIM COMMENT VALUE
               INSERT INTO COMMENTS_TEXT
               (
                     COMMENT_ID,
                     ATTACH_RECORDID,
                     ATTACH_TABLE,
                     COMMENTS,
                     HTMLCOMMENTS
               )
                VALUES
                (
                     CLAIM_COMMENTIDvar + 1,
                     CLAIM_COMMENT_VAL.CLAIM_ID, 
                     'CLAIM',
                     CLAIM_COMMENT_VAL.COMMENTS,
                     CLAIM_COMMENT_VAL.HTMLCOMMENTS
                ); 
            END IF;
           
        END LOOP;
    END;
    
    DECLARE MAXCOMMENT_IDvar int;
    
    BEGIN
        SELECT MAX(COMMENT_ID) INTO MAXCOMMENT_IDvar FROM COMMENTS_TEXT ct;

        --UPDATE THE GLOSSARY TABLE WITH THE NEXT_UNIQUE_ID
        UPDATE GLOSSARY
        SET NEXT_UNIQUE_ID = MAXCOMMENT_IDvar
        WHERE SYSTEM_TABLE_NAME = 'COMMENTS_TEXT';  
    END;
    
END USP_COPY_EXISTING_COMMENTS;
/
