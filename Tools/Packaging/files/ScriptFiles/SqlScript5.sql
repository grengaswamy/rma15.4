--------------------------------------------------------------------------------------------------
--                                                                            
--  File Name:   sqlscript.sql                                   
--                                                                            
--  Description: Blank SQL script                                          
--                                                                            
--  Comments:    This blank script is intended for advanced users. 
--               To create a script from an existing database with step-by-step  
--		         instructions, use the Database Import Wizard. 
--                                                                                                               
---------------------------------------------------------------------------------------------------
--INSERT INTO THE DATA SOURCE TABLE
INSERT INTO DATA_SOURCE_TABLE (ORGSEC_FLAG, DSN, DSNID, STATUS, RM_USERID, RM_PASSWORD, DBTYPE,CHECKSUM, GLOBAL_DOC_PATH, CONNECTION_STRING,NUM_LICENSES,LIC_UPD_DATE,CRC2,DOC_PATH_TYPE,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES (0 , 'RMXTraining' , 1 , 0 ,'(DB_USERID)' , '(DB_USERPWD)' , (DBTYPE) , '(CHECKSUM)' , NULL ,'(CONN_STRING)' , 1 ,'20090513' , '64c2e39145a5e59049d575b977ff355c' , 0 , NULL ,'20090513150314')
                                   
--INSERT RECORDS INTO THE USER AND USER DETAILS TABLE WITH A HARD-CODED VALUE OF csc/riskmaster
INSERT INTO USER_TABLE (USER_ID , LAST_NAME , FIRST_NAME , MANAGER_ID , NLS_CODE ,CHECKSUM , DTTM_RCD_LAST_UPD,IS_SMS_USER) VALUES( 1 , 'sc' , 'c' , 0 , 1033 , '7f8c3e71a71c966740fcf9b36a867039' , '20090513150216' , 1)
INSERT INTO USER_DETAILS_TABLE (DSNID, USER_ID, LOGIN_NAME, PASSWORD, PRIVS_EXPIRE, PASSWD_EXPIRE,DOC_PATH, SUN_START, SUN_END, MON_START, MON_END, TUES_START, TUES_END, WEDS_START, WEDS_END, THURS_START, THURS_END,FRI_START, FRI_END, SAT_START, SAT_END, CHECKSUM,UPDATED_BY_USER, DTTM_RCD_LAST_UPD, FORCE_CHANGE_PWD) VALUES (1 , 1 , 'csc' , '1e039e62c7039f563411c9ee05799c72' , null , null , null , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , '000000' , 'c8fd9f3cae2abb9587abe9791879ac4e' , 'csc' , '20090513150401', NULL)
