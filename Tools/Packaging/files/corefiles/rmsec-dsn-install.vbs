'==========================================================================
' VBScript Source File
'
' NAME: Script for Creating the DTG Security32 DSN (SQL Server)
'
' AUTHOR: CSC , Computer Sciences Corporation (CSC)
'
' DATE : 03/16/2006
'
' COMMENT: 
'
'==========================================================================
Option Explicit
'On Error Resume Next


Dim oReg        
Dim sDriverDll
Dim sKeyPath
Dim sSearchedValue
Dim PropDSNArray

Dim sDSNName 
Dim sDbServerName 
Dim sDbName 
Dim sUserName
Dim sDriverName 
Dim oArgs
Dim ArgNum

sDSNName = "DTG Security32"
Set oArgs = WScript.Arguments
ArgNum = 0

While ArgNum < oArgs.Count

	If (ArgNum + 1) >= oArgs.Count Then
		Call DisplayUsage
	End If	

	Select Case LCase(oArgs(ArgNum))
		Case "--dbname":
			ArgNum = ArgNum + 1
			sDbName = oArgs(ArgNum)
		Case "--dbservername":
			ArgNum = ArgNum + 1
			sDbServerName = oArgs(ArgNum)
		Case "--username":
			ArgNum = ArgNum + 1
			sUserName = oArgs(ArgNum)
		Case "--drivername":
			ArgNum = ArgNum + 1
			sDriverName = oArgs(ArgNum)
		Case "--help","-?"
			Call DisplayUsage
	End Select	

	ArgNum = ArgNum + 1
Wend



Sub DisplayUsage()
Dim str
str = str + "Usage: rmsec-dsn-install [--dbservername  MYDBMSSERVERNAME] " + vbCrLf
str = str + "                <--dbname mySecurityDBName>"+ vbCrLf
str = str + "                <--username mydbloginname>"+ vbCrLf
str = str + "                <--drivername mydrivername>"+ vbCrLf
str = str + "                [--help|-?]"	+ vbCrLf
str = str + ""+ vbCrLf
str = str + ""+ vbCrLf
str = str + ""+ vbCrLf
str = str + "	Example : rmsec-dsn-install  --dbservername localhost --dbname ""RMSecure""" + vbCrLf
str = str + "           --username ""sa"" --drivername ""SQL Server"""

	WScript.Echo str
	WScript.Quit
End Sub

Dim cRegDSNKey
Dim objRegistry
Dim oWshShell
Dim strKeyPath
Dim strValueName
Dim strValue

Const HKEY_LOCAL_MACHINE = &H80000002  
cRegDSNKey = "HKEY_LOCAL_MACHINE\Software\ODBC\ODBC.INI\" & sDSNName & "\"
Const cRegKey = "HKEY_LOCAL_MACHINE\Software\ODBC\ODBC.INI\ODBC Data Sources\"

'Check if DSN is already Set Up...
Set objRegistry = GetObject("winmgmts:\\.\root\default:StdRegProv")
strKeyPath = "SOFTWARE\ODBC\odbc.ini\" & sDSNName & "\"
strValueName = "Driver"
objRegistry.GetStringValue HKEY_LOCAL_MACHINE, strKeyPath, strValueName, strValue

If IsNull(strValue) Then 'If DSN does not already exist then...
	
	'Get the driver file name
	sKeyPath = "SOFTWARE\ODBC\ODBCINST.INI\" & sDriverName
	Set oReg = GetObject("winmgmts:\\.\root\default:StdRegProv")
	oReg.GetStringValue HKEY_LOCAL_MACHINE,sKeyPath,"Driver",sDriverDll
	
	Set oWshShell = CreateObject("WScript.Shell")
	oWshShell.RegWrite cRegDSNKey & "Driver", sDriverDll
	oWshShell.RegWrite cRegDSNKey & "LastUser", sUserName
	If Instr(sDriverDll, "SQLSRV32") > 0  Then
		oWshShell.RegWrite cRegDSNKey & "Server", sDbServerName
		oWshShell.RegWrite cRegDSNKey & "Database", sDbName
	ElseIf(Instr(sDriverDll, "SQORA32") > 0) Then
		oWshShell.RegWrite  cRegDSNKey & "Attributes", "W"
		oWshShell.RegWrite  cRegDSNKey & "ForceWCHAR", "F"
		oWshShell.RegWrite  cRegDSNKey & "FailoverDelay", "10"
		oWshShell.RegWrite  cRegDSNKey & "FailoverRetryCount", "10"
		oWshShell.RegWrite  cRegDSNKey & "MetadataIdDefault", "F"
		oWshShell.RegWrite  cRegDSNKey & "CloseCursor", "F"
		oWshShell.RegWrite  cRegDSNKey & "EXECSchemaOpt", ""
		oWshShell.RegWrite  cRegDSNKey & "EXECSyntax", "F"
		oWshShell.RegWrite  cRegDSNKey & "Application Attributes", "T"
		oWshShell.RegWrite  cRegDSNKey & "ResultSets", "T"
		oWshShell.RegWrite  cRegDSNKey & "QueryTimeout", "T"
		oWshShell.RegWrite  cRegDSNKey & "Failover", "T"
		oWshShell.RegWrite  cRegDSNKey & "Lobs", "T"
		oWshShell.RegWrite  cRegDSNKey & "DisableMTS", "F"
		oWshShell.RegWrite  cRegDSNKey & "BatchAutocommitMode", "IfAllSuccessful"
		oWshShell.RegWrite  cRegDSNKey & "ServerName", sDbServerName
		oWshShell.RegWrite  cRegDSNKey & "UserID", sUserName
		oWshShell.RegWrite  cRegDSNKey & "DSN", sDSNName
		oWshShell.RegWrite  cRegDSNKey & "FetchBufferSize", "64000"
		oWshShell.RegWrite  cRegDSNKey & "NumericSetting", "NLS"
		oWshShell.RegWrite  cRegDSNKey & "BindAsDATE", "F"
		oWshShell.RegWrite  cRegDSNKey & "DisableDPM", "F"
		oWshShell.RegWrite  cRegDSNKey & "BatchAutocommitMode", "IfAllSuccessful"	
	End If
	oWshShell.RegWrite cRegKey & sDSNName, sDriverName
End If             
Set oWshShell = Nothing  