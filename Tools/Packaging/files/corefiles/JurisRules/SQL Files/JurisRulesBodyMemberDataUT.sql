;*********************************************************************************
;Utah
[ASSIGN %%1=ADD_JURISDICTION(UT,Utah,0,0,0,0,0,0)]
;(3)(a) In the case of the injuries described in Subsections (4) through (6), the compensation shall be 66-2/3% of that employee's average weekly wages at the time of the injury, but not more than a maximum of 66-2/3% of the state average weekly wage at the time of the injury per week and not less than a minimum of $45 per week plus $5 for a dependent spouse and $5 for each dependent child under the age of 18 years, up to a maximum of four dependent children, but not to exceed 66-2/3% of the state average weekly wage at the time of the injury per week. 
;(b) The commission determined under Subsection (3)(a) shall be: 
;(i) paid in routine pay periods not to exceed four weeks for the number of weeks provided for in this section; and 
;(ii) in addition to the compensation provided for temporary total disability and temporary partial disability. 
;(4) For the loss of: Number of Weeks 
;(A) Arm and shoulder (forequarter amputation) 218 
;(B) Arm at shoulder joint, or above deltoid insertion 187 
;(C) Arm between deltoid insertion and elbow joint, at elbow joint, or below elbow joint proximal to insertion of biceps tendon 178 
;(D) Forearm below elbow joint distal to insertion of biceps tendon 168 
;(A) Hand At wrist or midcarpal or midmetacarpal amputation 168 
;(B) Hand All fingers except thumb at metacarpophalangeal joints 101 
;(A) Thumb At metacarpophalangeal joint or with resection of carpometacarpal bone 67 
;(B) Thumb At interphalangeal joint 50 
;(A) Index finger At metacarpophalangeal joint or with resection of metacarpal bone 42 
;(B) Index finger At proximal interphalangeal joint 34 
;(C) Index finger At distal interphalangeal joint 18 
;(A) Middle finger At metacarpophalangeal joint or with resection of metacarpal bone 34 
;(B) Middle finger At proximal interphalangeal joint 27 
;(C) Middle finger At distal interphalangeal joint 15 
;(A) Ring finger At metacarpophalangeal joint or with resection of metacarpal bone 17 
;(B) Ring finger At proximal interphalangeal joint 13 
;(C) Ring finger At distal interphalangeal joint 8 
;(A) Little finger At metacarpophalangeal joint or with resection of metacarpal bone 8 
;(B) Little finger At proximal interphalangeal joint 6 
;(C) Little finger At distal interphalangeal joint 4 
;(A) Leg Hemipelvectomy (leg, hip and pelvis) 156 
;(B) Leg at hip joint or three inches or less below tuberosity of ischium 125 
;                                                                                                                11111111111111111111111111111111111111111111111111
;            11111111112222222222333333333344444444445555555555666666666677777777778888888888999999999900000000001111111111222222222233333333334444444444555555555566666666667
;   12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890            
;(C) Leg above knee with functional stump, at knee joint or Gritti-Stokes amputation or below knee with short stump (three inches or less below intercondylar notch) 112 
;(D) Leg below knee with functional stump 88 
;(ii) Foot 
;(A) Foot at ankle 88 
;(B) Foot partial amputation (Chopart's) 66 
;(C) Foot midmetatarsal amputation 44 
;(iii) Toes 
;(A) Great toe 
;(I) With resection of metatarsal bone 26 
;(II) At metatarsophalangeal joint 16 
;(III) At interphalangeal joint 12 
;(B) Lesser toe (2nd -- 5th) 
;(I) With resection of metatarsal bone 4 
;(II) At metatarsophalangeal joint 3 
;(III) At proximal interphalangeal joint 2 
;(IV) At distal interphalangeal joint 1 
;(C) All toes at metatarsophalangeal joints 26 
;(;iv) Miscellaneous 
;(A) One eye by enucleation 120 
;(B) Total blindness of one eye 100 
;(C) Total loss of binaural hearing 109 
;(5) Permanent and complete loss of use shall be deemed equivalent to loss of the member. Partial loss or partial loss of use shall be a percentage of the complete loss or loss of use of the member. This Subsection (5) does not apply to the items listed in Subsection (4)(b)(iv). 
;(6)(a) For any permanent impairment caused by an industrial accident that is not otherwise provided for in the schedule of losses in this section, permanent partial disability compensation shall be awarded by the commission based on the medical evidence. 
;(b) Compensation for any impairment described in Subsection (6)(a) shall, as closely as possible, be proportionate to the specific losses in the schedule set forth in this section. 
;(c) Permanent partial disability compensation may not: 
;(i) exceed 312 weeks, which shall be considered the period of compensation for permanent total loss of bodily function; and 
;(ii) be paid for any permanent impairment that existed prior to an industrial accident. 
;(7) The amounts specified in this section are all subject to the limitations as to the maximum weekly amount payable as specified in this section, and in no event shall more than a maximum of 66-2/3% of the state average weekly wage at the time of the injury for a total of 312 weeks in compensation be required to be paid. 
; 
