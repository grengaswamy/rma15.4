** REGISTERED VERSION **


This implementation of the MICR font is copyright � 2004 by Covix, LLC

Developers may use and distribute the purchased font without royalties.  Any other sale or distribution is prohibited.

For use of the special characters, use the $, &, -, O and T characters.  See the file chart.bmp for details.


INSTALLATION:
-------------
Extract and save the CovixMICR.ttf file somewhere on your computer's hard disk.  Remember where you saved it.  Install the font like you would any other font from the Windows control panel. 


KNOWN ISSUES:
-------------
On some systems, the font name does not appear when selecting fonts from some programs (such as Word).  The font appears as a blank space in the font list.  You can, however, select and use the font as you would any other font.

