Option Explicit
'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 2009
'
' NAME: UpdateProjectProperties.vbs
'
' AUTHOR:  CSC
' DATE  : 04/28/2010
'
' COMMENT: Use this script during autobuild to:
' 1) Update the path variable for the ISM
' 2) Update the Product Name for the ISM

'==========================================================================
Dim sISMProjectFullPath
Dim strProductName, strPathRoot, strAssemblyRange, strAssemblyVersionNumber
Dim objDictProps, objDictPathVars

'Set the constant for the InstallShield IDE Project library
'InstallShield 2010 support
'Const INSTALLSHIELD_IDE = "ISWiAuto16.ISWiProject"
Const INSTALLSHIELD_IDE = "ISWiAuto20.ISWiProject"

'Create the Scripting Dictionary objects
Set objDictProps = CreateObject("Scripting.Dictionary")
Set objDictPathVars = CreateObject("Scripting.Dictionary")


If (WScript.Arguments.Count = 5) Then
	'Obtain the properties that are passed via command line
	sISMProjectFullPath = WScript.Arguments.Item(0)
	strPathRoot = WScript.Arguments.Item(1)
	strProductName = WScript.Arguments.Item(2)
	strAssemblyRange = WScript.Arguments.Item(3)
	strAssemblyVersionNumber = WScript.Arguments.Item(4)

	'Set the list of Path variables to set
	objDictPathVars.Add "PATH_ROOT", strPathRoot
	
	'Properties required by Data Integration for updating the assemblies.config file
	objDictProps.Add "ASSEMBLY_RANGE", strAssemblyRange
	objDictProps.Add "ASSEMBLY_VERSIONNUMBER", strAssemblyVersionNumber
Else	
	'Obtain the properties that are passed via command line
	sISMProjectFullPath = WScript.Arguments.Item(0)
	strPathRoot = WScript.Arguments.Item(1)
	strProductName = WScript.Arguments.Item(2)	
	
	'Set the list of Path variables to set
	objDictPathVars.Add "PATH_ROOT", strPathRoot
End If

'Call the appropriate methods to update Path Variables and Project Properties
Call UpdatePathVariables(sISMProjectFullPath, objDictPathVars)

Call UpdateProjectProperties(sISMProjectFullPath, objDictProps, strProductName)

Sub UpdateProductVersion(sISMProjectFullPath, sBuildVerSegment)
	' instantiate the Developer Automation interface
	Dim oISM
	Set oISM = CreateObject(INSTALLSHIELD_IDE)
	
	' open a project read-write
	oISM.OpenProject sISMProjectFullPath, False
	
	' ...perform changes...
	
	'Build Modified Product Version string.
	Dim arrVer
	arrVer = Split(oISM.ProductVersion,".")
	'oISM.ISWIProperties
	'WScript.Echo(oISM.ProductVersion & "Bound: " & Ubound(arrVer))
	
	Dim sNewProductVersion
	
	'NOTE: In order for the build number to affect the "Update" mode of InstallShield
	'the Product Version must be changed for the 3rd segment of the Product Version Number
	If(UBound(arrVer)<2) Then
		sNewProductVersion = oISM.ProductVersion & "." & sBuildVerSegment
	Else
		arrVer(2) = sBuildVerSegment
		sNewProductVersion = Join(arrVer,".")
	End If
	
	'Apply new Product version within ISM
	WScript.Echo("Initial Product Version: " & oISM.ProductVersion)
	oISM.ProductVersion = sNewProductVersion 
	WScript.Echo("New Product Version: " & oISM.ProductVersion)
	
	'Apply a new "MSI" package code every time.
	WScript.Echo("Initial MSI Package Code: " & oISM.PackageCode)
	oISM.PackageCode = oISM.GenerateGUID()
	WScript.Echo("New MSI Package Code: " & oISM.PackageCode)
	
	' save changes and clean up
	oISM.SaveProject: oISM.CloseProject: Set oISM = Nothing
	WScript.Echo("Completed Load and Save of ISM")
End Sub

Sub UpdatePathVariables(sISMProjectFullPath, objDictPathVars)
	Dim oISM
	Dim objKeys
	Dim objPathVar
	Dim i
	
	' instantiate the Developer Automation interface
	Set oISM = CreateObject("ISWiAuto20.ISWiProject")
	
	' open a project read-write
	oISM.OpenProject sISMProjectFullPath, False
	
	'Get all of the keys in the Dictionary object
	objKeys = objDictPathVars.Keys
	
	For i=0 To objDictPathVars.Count - 1
		'Get a path variable handle
		Set objPathVar = oISM.ISWiPathVariables.Item(objKeys(i))
		'Set the value of the property
		objPathVar.Value = objDictPathVars.Item(objKeys(i))
	Next
		
	'Save the project
	oISM.SaveProject
	
	'Close the project
	oISM.CloseProject
	
	'Clean up
	Set oISM = Nothing
End Sub

Sub UpdateProjectProperties(sISMProjectFullPath, objDictProps, strProductName)
	Dim oISM	
	Dim objKeys, objProperty
	Dim i
	
	'Create the InstallShield Developer Automation Interface object
	Set oISM = CreateObject("ISWiAuto20.ISWiProject")
	
	'Open the project
	oISM.OpenProject sISMProjectFullPath, False
		
	'Get all of the keys in the Dictionary object
	objKeys = objDictProps.Keys
	
	For i=0 To objDictProps.Count - 1
		'Get a property handle
		Set objProperty = oISM.ISWIProperties.Item(objKeys(i))
		'Set the value of the property
		objProperty.Value = objDictProps.Item(objKeys(i))
	Next
	
	'Set the Product Name
	oISM.ProductName = strProductName
	
	'Save the Project changes
	oISM.SaveProject
	
	'Close the project
	oISM.CloseProject
	
	'Clean up
	Set oISM  = Nothing
End Sub

Sub UpdateProperties(sISMProjectFullPath, strPathRoot)
	Dim oISM
	Dim pProp
	Dim intPropCount
	Dim objProperty
	
	' instantiate the Developer Automation interface
	'Set oISM = CreateObject("ISWiAuto16.ISWiProject")
	Set oISM = CreateObject(INSTALLSHIELD_IDE)
	
	
	' open a project read-write
	oISM.OpenProject sISMProjectFullPath, False
	
	
	'WScript.Echo oISM.ISWiPathVariables.Item("PATH_ROOT").Value
	'Set the path root path variable for the package
	oISM.ISWiPathVariables.Item("PATH_ROOT").Value = strPathRoot
	'WScript.Echo oISM.ISWiPathVariables.Item("PATH_ROOT").Value
	
	
	' Set objProperty = oISM.ISWIProperties.Item(BUILD_VERSION)
' 	objProperty.Value = sBuildVersion
' 	WScript.Echo objProperty.Value
	
' 	For Each pProp In oISM.ISWIProperties
' 		Set objProperty = pProp
' 		'WScript.Echo objProperty.Name
' 		If (objProperty.Name = "PATCHSET_VERSION") Then
' 			WScript.Echo objProperty.Value
' 			objProperty.Value = sBuildVersion
' 			WScript.Echo objProperty.Value
' 		End If
' 	Next
	
	
	' 
' 	For intPropCount=1 To oISM.ISWIProperties.Count -1
' 		Set objProperty = oISM.ISWIProperties(intPropCount)
' 		If (objProperty.Name = "PATCHSET_VERSION") Then
' 			WScript.Echo objProperty.Value
' 		End If
' 	Next
	
	'Save the project
	oISM.SaveProject
	
	'Close the project
	oISM.CloseProject
	
	'Clean up
	Set oISM = Nothing
End Sub
