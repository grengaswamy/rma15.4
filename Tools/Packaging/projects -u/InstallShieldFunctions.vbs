'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 2007
'
' NAME: InstallShieldFunctions
'
' AUTHOR: Computer Sciences Corporation , CSC
' DATE  : 11/7/2008
'
' COMMENT: Contains commonly used InstallShield Functions
'
'==========================================================================
'Set the constant for the InstallShield IDE Project library
Const INSTALLSHIELD_IDE = "ISWiAuto20.ISWiProject"

' instantiate the Developer Automation interface
Dim oISM
'Set oISM = CreateObject(INSTALLSHIELD_IDE)
Set oISM = CreateObject("ISWiAuto20.ISWiProject")

Sub OpenProject(strProjectPath)
	oISM.OpenProject strProjectPath, False
End Sub

Sub SetPackageCode()
	oISM.PackageCode = GetNewGUID()
End Sub

Sub SetProductCode()
	oISM.ProductCode = GetNewGUID()
End Sub

Sub SetProductVersion(dblProductVersion)
	oISM.ProductVersion = dblProductVersion
End Sub

Sub SaveProject()
	oISM.SaveProject 
End Sub

Sub CloseProject()
	oISM.CloseProject
End Sub

Function GetNewGUID()
	GetNewGUID = oISM.GenerateGUID()
End Function

Function GetPackageCode()
	GetPackageCode = oISM.PackageCode
End Function

Function GetProductCode()
	GetProductCode = oISM.ProductCode
End Function

Function GetProductVersion()
	Dim intProductVersion
	
	intProductVersion = oISM.ProductVersion
	
	GetProductVersion = intProductVersion
End Function

Sub SetBuildVersion(sBuildVersion)
	Dim objProperty
	Const BUILD_VERSION = "PATCHSET_VERSION"
	
	Set objProperty = oISM.ISWIProperties.Item(BUILD_VERSION)
	objProperty.Value = sBuildVersion
End Sub
