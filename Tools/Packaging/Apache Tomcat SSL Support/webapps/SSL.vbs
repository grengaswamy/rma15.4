'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 4.0
'
' NAME: VBScript to detect the Java and Tomcat Installation paths and then pass 
' those values as parameters to the required batch files and command lines
' in order to support SSL for encrypted communications for Riskmaster X
'
' AUTHOR: CSC , Computer Sciences Corporation (CSC)
' DATE  : 05/08/2007
'
' COMMENT: Modified to correct issues with updating the cas_jaas.conf
'          and global-parameters.xml files which contain special cases 
'			for replacement
'==========================================================================
Dim strComputer, strComputerValue, strPortValue, strReplaceValue
Dim strTomcatPath, strJavaPath, strTomcatISAPIPath
Dim strJavaSecurityCerts, strJavaKeyStore
Dim intIndex
Const strHTTPPrefix = "http://"
Const strHTTPSPrefix = "https://"
Const strHttpRegExp = "^http://"
Const strPortRegExp = "^(.+):(\d+)"
Const strHttpsRegExp = "^https://"

'Set the value as the local computer
strComputer = "."

'Call the function to retrieve the computer name
'strComputerName = GetComputerName()

'Get the string value for the JDK installation path
strJavaPath = GetJavaPath(strComputer)

'Get the string value for the Apache Tomcat 5.0 installation path
strTomcatInstallPathValue = GetTomcatPath(strComputer)

'Set the path to the Tomcat ISAPI Filter DLL
strTomcatISAPIPath = strTomcatInstallPathValue & "\bin\win32\i386" 

'Obtain the path to the Java Security file
strJavaSecurityCerts = strJavaPath & "\jre\lib\security\cacerts"
strJavaKeyStore = strJavaPath & "\jre\bin\keytool"

'Prompt for the installation location for the website
strWebsiteName = InputBox("Please enter the website installation location", "Website installation location", "Default Web Site")

'Call the subroutine to create the required Tomcat virtual directory in IIS
CreateJakartaVirtualDirectory strTomcatISAPIPath, strWebsiteName
 
'Install the ISAPI Filter for jakarta
AddISAPIFilter strWebsiteName, "jakarta", "Jakarta Tomcat SSL Redirector", """" & strTomcatISAPIPath & "\isapi_redirect.dll" & """"

'Only execute the webservice extension subroutine if the host OS is Windows Server 2003
If (CheckOS()) Then
	'Call the subroutine to create the required Tomcat ISAPI Filter web service extension
	CreateWebSvcExtension strTomcatISAPIPath 
End If

'Call the subroutine to import the SSL certificates
ImportCertificates strTomcatInstallPathValue, strJavaSecurityCerts, strJavaKeyStore

'Call the subroutine to update the registry
UpdateRegistry strTomcatInstallPathValue

'Prompt for the current and replacement values
strPortValue = InputBox("Please enter the current path", "Current Path", LCase(GetComputerName()) & ":8080")

'Loop through to ensure that the user is prompted incessantly
'until they remove the http:// prefix
Do While (TestURL(strPortValue, strHttpRegExp) = True)
	'Prompt the user to re-enter the URL
	strPortValue = InputBox("Please enter the path WITHOUT specifying http://", "Current Path", LCase(GetComputerName()) & ":8080")
Loop

'Loop through to ensure that the user is prompted incessantly
'until they ensure the port value is entered
Do While (TestURL(strPortValue, strPortRegExp) = False)
	'Prompt the user to re-enter the URL to include a Port Number
	strPortValue = InputBox("Please enter the path to include a port number", "Current Path", LCase(GetComputerName()) & ":8080")
Loop

strReplaceValue = InputBox("Please enter the desired path", "Computer/DNS Name", LCase(GetComputerName()))

'Loop through to ensure that the user is prompted incessantly
'until they remove the https:// prefix
Do While (TestURL(strReplaceValue, strHttpsRegExp) = True)
	'Prompt the user to re-enter the URL
	strReplaceValue = InputBox("Please enter the desired path WITHOUT specifying https://", "Computer/DNS Name", LCase(GetComputerName()))
Loop
'Call the subroutine to update the configuration files
UpdateFiles strPortValue, strReplaceValue

'Get the current script path location
Function GetPath()
	' Return path to the current script
	Dim path
	path = WScript.ScriptFullName  ' script file name
	GetPath = Left(path, InStrRev(path, "\"))
End Function

'Tests the URL against a regular expression
'to determine if the URL contains http
Function TestURL(strURL, strRegExpPattern)
	Dim objRegExp
	Dim blnMatch
	
	'Default the match to False
	blnMatch = False
	
	'Create the Regular Expression object
	Set objRegExp = CreateObject("VBScript.RegExp")
	
	objRegExp.Pattern = strRegExpPattern
	objRegExp.IgnoreCase = True
	
	blnMatch = objRegExp.Test(strURL)
	
	'Clean up
	Set objRegExp = Nothing
	
	'Set the function to the boolean
	'to indicate whether or not the URL matched
	TestURL = blnMatch
End Function

'Function to determine the index of a string search expression
'with a specified string
Function FindIndex(strSearch, strExpression)
	Dim lngFoundPos 

	'Find the index of the search expression within the string	
	lngFoundPos = InStr(1, strSearch, strExpression)

	'Return the index at which the specified expression was found
	FindIndex = lngFoundPos
End Function

'Function to extract the left most portion of a String
'based on a specified search index
Function ExtractLeftString(strSearch, intSearchIndex)
	Dim strExtract
	
	'Extract the required string
	strExtract = Left(strSearch, intSearchIndex - 1)
	
	'Return the extracted string
	ExtractLeftString = strExtract	
End Function

'Function to extract the computer name
'from a specified URL
'PSEUDOCODE: Remove http/https from the left portion of the String
'Extract the left portion of the URL containing the computer name/URL and the port
Function ExtractPortURL(strURL, strPrefix)
	Dim strPortURL

	'Retrieve the name of the computer name with the port number
	strPortURL = Right(strURL, Len(strURL) - Len(strPrefix))
	
	'Return the extracted String
	ExtractPortURL = strPortURL
End Function

'Function to extract the computer name
'from a specified URL
'PSEUDOCODE: Retrieve the port URL
'Find the index of the next colon (indicating the port number)
'Assign the index value
'Extract the left portion of the URL containing just the computer name/URL
Function ExtractComputerName(strPortURL)
	Dim strComputerName
	Dim intIndex
	
	intIndex = FindIndex(strPortURL, ":")
	
	'The URL contains port number information
	If (intIndex > 0) Then
		strComputerName = ExtractLeftString(strPortURL, intIndex)
	Else 'The URL does not contain port number information
		strComputerName = strPortURL
	End If
	
	'Return the extracted string containing the computer name
	ExtractComputerName = strComputerName
End Function


'Function to retrieve an array containing all of the relevant file paths
'that are required for editing
Sub UpdateFiles(strSearchValue, strReplaceValue)
	Dim arrFiles(6)
	Dim strScriptPath
	
	'Retrieve the path to the current script location
	strScriptPath = GetPath()
	
	REM SSO files
	arrFiles(0) = strScriptPath & "SSO\META-INF\cas_jaas.conf"
	
	REM oxf files
	arrFiles(1) = strScriptPath & "oxf\WEB-INF\resources\config\global-parameters.xml"
	
	arrFiles(2) = strScriptPath & "oxf\WEB-INF\web.xml"
	
	REM Jetspeed files
	REM Specify the path to the web.xml file
	'TODO: Replace both the http:// path and the :8080 path
	arrFiles(3) = strScriptPath & "jetspeed\WEB-INF\web.xml"
		
	REM Specify the path To the CSC_JetspeedSecurity.properties file
	arrFiles(4) = strScriptPath & "jetspeed\WEB-INF\conf\CSC_JetspeedSecurity.properties"
	
	REM Specify the path To the JSP template for editing
	arrFiles(5) = strScriptPath & "jetspeed\WEB-INF\templates\jsp\navigations\html\top_default.jsp"
	
	REM Open the PSML file for editing
	arrFiles(6) = strScriptPath & "jetspeed\WEB-INF\psml\role\user\html\default.psml"
	
	'Call the StringReplace function to replace the relevant values
	'in each of the files by iterating over all of them
	For Each arrFileName In arrFiles
		Call UpdateFileContents(arrFileName, strSearchValue, strReplaceValue)
	Next
	
	'TODO: Open the Riskmaster.config file for editing by determining the path to the file
	'Notify the user that they must still modify the Riskmaster.config manually with the appropriate URLs
	MsgBox "Do not forget that you must still manually edit the Riskmaster.config file", vbOKOnly, "Riskmaster.config Reminder"
End Sub

Function RegExTest(strSearchValue, strSearchText)
	Dim objRegEx
	Dim blnIsMatch
	
	
	Set objRegEx = CreateObject("VBScript.RegExp")
	
	objRegEx.Global = True
	objRegEx.Pattern = "http[s]*://" & strSearchValue
	objRegEx.IgnoreCase = True
	
	blnIsMatch = objRegEx.Test(strSearchText)
	
	MsgBox blnIsMatch
	
	'Clean up
	Set objRegEx = Nothing
End Function

Function RegExReplace(strRegExprPattern, strSearchText, strReplaceValue)
	Dim objRegEx
	Dim strReplaceText
	
	
	Set objRegEx = CreateObject("VBScript.RegExp")
	
	objRegEx.Global = True
	'objRegEx.Pattern = "http[s]*://" & strSearchValue
	objRegEx.Pattern = strRegExprPattern
	objRegEx.IgnoreCase = True
	
	strReplaceText = objRegEx.Replace(strSearchText, strReplaceValue)
		
	'Clean up
	Set objRegEx = Nothing
	
	RegExReplace = strReplaceText
End Function


Function ReadFile(strFileName)
	Dim objFSO, objFile, objReadFile
	Dim strFileText
	
	'Declare and assign the necessary constants for file processing
	Const ForReading = 1
	Const ForWriting = 2
	Const ForAppending = 8
	
	'Create the FileSystem Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	If (Not objFSO.FileExists(strFileName)) Then
		MsgBox "File path does not exist: " & strFileName
	End If
	'Obtain a handle on the file for reading
	Set objReadFile = objFSO.GetFile(strFileName)
	
	'Begin reading the contents of the file
	Set objFile =objReadFile.OpenAsTextStream(ForReading)
	
	'Read in the entire contents of the file
	strFileText = objFile.ReadAll
	
	'Close the file from further reading
	objFile.Close
	
	'Clean up
	Set objFSO = Nothing
	Set objReadFile = Nothing
	Set objFile = Nothing
	
	ReadFile = strFileText
End Function

Sub WriteFile(strFileName, strFileText)
	Dim objFSO, objFile, objWriteFile
	
	'Declare and assign the necessary constants for file processing
	Const ForReading = 1
	Const ForWriting = 2
	Const ForAppending = 8
	
	'Create the FileSystem Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	'Obtain a handle on the file for reading
	Set objWriteFile = objFSO.GetFile(strFileName)
	
	'Open the file for writing
 	Set objFile = objWriteFile.OpenAsTextStream(ForWriting)

	'Write out the updated contents of the file
	objFile.WriteLine strFileText	

	'Close the file from further writing
	objFile.Close
	
	'Clean up
	Set objFSO = Nothing
	Set objWriteFile = Nothing
	Set objFile = Nothing
End Sub

'Function to determine whether a specific string expression
'contains the specified String
Function ContainsString(strValue, strFindValue)	
	'If the specified string contains the value of http or https
	If InStr(strValue, strFindValue) > 0 Then
		'Indicate that the string contains the specified string
		ContainsString = True
	Else
		'Indicate that the string does not contain the specified string
		ContainsString = False
	End If
End Function


'Perform a string replacement on a specified string in a file
Sub UpdateFileContents(strFileName, strSearchValue, strReplaceValue)
	Dim strOriginalFileText, strReplacementFileText, strComputerName
	Dim APP_REGEXP, PRES_REGEXP, APP_SSL_REGEXP, PRES_SSL_REGEXP
	Const PREFIX_TEXT = ".+"
	Const SUFFIX_TEXT = ".+"
	Const HTTP_PREFIX = "http://"
	Const HTTPS_PREFIX = "https://"
	Const PORT_REGEXPR = "[:]\d+" 'Ex: should contain something similar to :8080 (or any port number)
	Const HTTP_REGEXPR = "http[s]*://" 'Ex: should contain the http:// or https:// prefix
	
	'Get the computer name
	strComputerName = ExtractComputerName(strSearchValue)
	
	'Set the values for the Application Server Regular Expression
	APP_REGEXP = HTTP_PREFIX & strComputerName
	APP_SSL_REGEXP = HTTPS_PREFIX & strReplaceValue
	
	'Set the values for the Presentation Server Regular Expression
	PRES_REGEXP = HTTP_PREFIX & strSearchValue
	PRES_SSL_REGEXP = HTTPS_PREFIX & strReplaceValue
	
	
	'Handle the special case for SSO
	If (ContainsString(strFileName, "cas_jaas.conf") Or ContainsString(strFileName, "global-parameters.xml")) Then
		'NOTE: cas_jass.conf is a unique case which only involves the URL to the CWS 
		'i.e. the Application Server Web Service rather than the URL to the 
		'Presentation Layer server (usually containing Tomcat's port 8080)
		strOriginalFileText = ReadFile(strFileName)
				
		strReplacementFileText = RegExReplace(APP_REGEXP, strOriginalFileText, APP_SSL_REGEXP)
				
		Call WriteFile(strFileName, strReplacementFileText)
	'Handle all files which are not web.xml
	ElseIf (ContainsString(strFileName, "web.xml") = False) Then
		strOriginalFileText = ReadFile(strFileName)
				
		strReplacementFileText = RegExReplace(PRES_REGEXP, strOriginalFileText, PRES_SSL_REGEXP)
		
		Call WriteFile(strFileName, strReplacementFileText)
	Else 'The file name contains web.xml
		'NOTE:web.xml is a unique case which involves replacement of both http(s):// URLs
		'and computer name/DNS URLs
		strOriginalFileText = ReadFile(strFileName)
				
		strReplacementFileText = RegExReplace(PRES_REGEXP, strOriginalFileText, PRES_SSL_REGEXP)
				
		'Perform an additional replacement to handle the case of standalone computer names/DNS names
		'without the http(s):// prefix
		strReplacementFileText = RegExReplace(strSearchValue, strReplacementFileText, strReplaceValue)
				
		Call WriteFile(strFileName, strReplacementFileText)
	End If
End Sub

'Function to determine whether the specific string 
'already contains a value of Http
Function ContainsHttp(strValue)
	'If the specified string contains the value of http or https
	If InStr(strValue, strHTTPPrefix) > 0 Then
		'Indicate that the string contains the Http prefix
		ContainsHttp = True
	Else
		'Indicate that the string does not contain the Http prefix
		ContainsHttp = False
	End If
End Function

'Function to determine whether a specific string expression
'contains the specified string
Function ContainsString(strValue, strFindValue)	
	'If the specified string contains the value of http or https
	If InStr(strValue, strFindValue) > 0 Then
		'Indicate that the string contains the specified string
		ContainsString = True
	Else
		'Indicate that the string does not contain the specified string
		ContainsString = False
	End If
End Function

'Function to determine whether or not the host OS is Server 2003
Function CheckOS()
	Dim objWMIService
	Dim blnIsServer2003
	
	'Initialize the boolean to a false
	blnIsServer2003 = False
	
	'Create the WMI Object to determine the OS
	Set objWMIService = GetObject("winmgmts:" _
	    & "{impersonationLevel=impersonate}!\\" _
	    & strComputer & "\root\cimv2")
	Set colOperatingSystems = objWMIService.ExecQuery _
	    ("Select * from Win32_OperatingSystem")
	For Each objOperatingSystem in colOperatingSystems
	    'Wscript.Echo objOperatingSystem.Caption & _
	    '"  " & objOperatingSystem.Version
	    strOS = objOperatingSystem.Caption
	Next
	
	'Determine if the Operating System is Windows Server 2003
	If (InStr(strOS, "Server 2003")) Then
		'Set the boolean to a value of true
		blnIsServer2003 = True
	End If
	
	'Clean up
	Set objWMIService = Nothing
	
	'Return the boolean variable as the value of the function
	CheckOS = blnIsServer2003
End Function

'Function to retrieve the local computer name
Function GetComputerName()
	Dim SWBemlocator, objWMIService
	Const IS_WORKGROUP_MEMBER = 2
	Const IS_DOMAIN_MEMBER = 3
	Const IS_BACKUP_DOMAIN_CONTROLLER = 4
	Const IS_PRIMARY_DOMAIN_CONTROLLER = 5

	Set SWBemlocator = CreateObject("WbemScripting.SWbemLocator")
	Set objWMIService = SWBemlocator.ConnectServer(strComputer,"root\CIMV2",UserName,Password)
	Set colItems = objWMIService.ExecQuery("Select * from Win32_ComputerSystem",,48)
	For Each objItem in colItems
		'If the server is a member of a workgroup
		If (objItem.DomainRole = IS_WORKGROUP_MEMBER) Then
			'Obtain the DNS Host name of the computer
			strComputerName = objItem.DNSHostName
		ElseIf (objItem.DomainRole = IS_DOMAIN_MEMBER) Then
			'Obtain the FQDN name of the computer
			strComputerName = objItem.DNSHostName & "." & objItem.Domain
		ElseIf (objItem.DomainRole = IS_BACKUP_DOMAIN_CONTROLLER Or objItem.DomainRole = IS_PRIMARY_DOMAIN_CONTROLLER) Then
			MsgBox "Riskmaster X cannot be installed on a Domain Controller.  Please install on another server."
			WScript.Quit
		End If
	Next
	
	'Clean up
	Set SWBemlocator = Nothing
	Set objWMIService = Nothing
	
	'Return the Host Computer Name as the value of the function
	GetComputerName = strComputerName
End Function

'Function to display a prompt to retrieve the host computer name
Function PromptForComputerName()
	Dim strComputerName, strNetBIOSName
	
	'Call the function to retrieve the Host computer name
	strComputerName = GetComputerName()

	'Display a prompt to the user for Computer Name information	
	strNetBIOSName = InputBox("Please enter the computer name, FQDN or IP Address of this web server", "Computer Name", strComputerName)

	'Return the Host Computer Name as the value of the function
	PromptForComputerName = strNetBIOSName
End Function

'Get the path to the Java directory
Function GetJavaPath(strComputer)
	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002
	Dim objReg

	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _
	 strComputer & "\root\default:StdRegProv")
	
	'Set the root path for Java 1.5
	strKeyPath = "SOFTWARE\JavaSoft\Java Development Kit\1.5"
	'Read out the JavaHome registry key
	strEntryName = "JavaHome"
	
	'Get the string value for the JDK Installation path
	objReg.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strEntryName,strValue
	
	'Clean up
	Set objReg = Nothing
	
	'Return the Java installation path as the value of the function
	GetJavaPath = strValue
End Function

'Get the path to the Tomcat directory
Function GetTomcatPath(strComputer)
	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002
	Dim objReg

	'Create the Registry object for reading out from the registry
	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _
	 strComputer & "\root\default:StdRegProv")
	
	'Set the root path for Tomcat 5.0
	strTomcatPath = "SOFTWARE\Apache Software Foundation\Tomcat\5.0"
	
	'Read out the Tomcat 5.0 registry key
	strTomcatInstallPath = "InstallPath"
	
	'Get the string value for the Tomcat installation
	objReg.GetStringValue HKEY_LOCAL_MACHINE, strTomcatPath, strTomcatInstallPath, strTomcatInstallPathValue
	
	'Clean up
	Set objReg = Nothing
	
	'Return the Tomcat installation path as the value of the function
	GetTomcatPath = strTomcatInstallPathValue
End Function

'Subroutine to import the SSL certificates for Java Security Certificate store
Sub ImportCertificates(strTomcatInstallPath, strJavaSecurityCerts, strJavaKeyStore)
	Dim WshShell
	Dim strCmd
	Dim intResponse
	
	'Create the Windows Scripting Host Shell object
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	'Enter the required key store password for importing the required certificates
	strKeyStorePwd = InputBox("Enter the key store password", "Java Key Store Password", "changeit")
	
	'Prompt for a response to determine if this is a VeriSign or similarly purchased SSL Cert
	'to distinguish it from SSL certificates which have been self-generated 
	intResponse = MsgBox("Is this a VeriSign or similarly purchased SSL Cert?", 4, "SSL Certificate Type")
	
	'Allow the user to enter the necessary aliases and certificate paths
	strAlias1 = InputBox("Enter the name of the certificate alias", "Certificate alias")
	strCertPath1 = InputBox("Enter the location of the SSL certificate", "IIS SSL Certificate")
	
	'Determine additional prompting based on MsgBox response
	If (intResponse = 7) Then 'Indicates a response of No
		strAlias2 = InputBox("Enter the name of the certificate authority alias", "Certificate Authority alias")
		strCertPath2 = InputBox("Enter the location of the SSL certificate for the certificate authority", "Certificate Authority SSL Certificate")
	End If
	
	'Concatenate the entire string of the command to be executed
	strCmd = """" & strTomcatInstallPath & "\webapps\cacert-mod.bat" & """"
	strCmd =  strCmd & " """ & strJavaKeyStore & """"
	strCmd = strCmd & " """ & strJavaSecurityCerts & """"
	strCmd = strCmd & " " & strKeyStorePwd & ""
	strCmd = strCmd & " """ & strAlias1 & """"
	strCmd = strCmd & " """ & strCertPath1 & """"
	strCmd = strCmd & " """ & strAlias2 & """"
	strCmd = strCmd & " """ & strCertPath2 & """"
	
	'Pass the value of the registry key path to the batch file
	'as well as the entire string of the command to be executed
	'WScript.Echo strCmd
	WshShell.Run strCmd, 1, True

	'Clean up
	Set WshShell = Nothing

End Sub

'Subroutine to update the registry for the Java Security Certificate store
Sub UpdateRegistry(strTomcatInstallPath)
	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002
	Dim objReg
	Dim strKeyPath
	
	strKeyPath = "SOFTWARE\Apache Software Foundation\Jakarta Isapi Redirector\1.0"
	
	'Create the WMI object to gain access to the registry
	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _
 	strComputer & "\root\default:StdRegProv")
 	
 	'Create the key for the ISAPI Redirector
 	objReg.CreateKey HKEY_LOCAL_MACHINE, strKeyPath
 	
 	'Set the string values required for redirection
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "extension_uri", "/jakarta/isapi_redirect.dll"
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "log_file", strTomcatInstallPath & "\logs\iis_redirect.log"
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "log_level", "emerg"
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "worker_file", strTomcatInstallPath & "\conf\worker.properties"
 	objReg.SetStringValue HKEY_LOCAL_MACHINE, strKeyPath, "worker_mount_file", strTomcatInstallPath & "\conf\uriworkermap.properties"
 	
	'Clean up
	Set objReg = Nothing
End Sub

'Subroutine to edit the configuration files with the SSL path
Sub EditConfigFiles(strTomcatInstallPath)
	Dim WshShell
	Dim strCmd
	Dim strComputerName
	
	'Create the Windows Scripting host Shell object
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	'Retrieve the Host Computer Name
	strComputerName = GetComputerName()
	
	'Display a message box to the user indicating the Host Computer Name
	MsgBox "The name of this computer is: " & strComputerName, 0, "Host computer name"
	
	'Concatenate the entire string of the command to be executed
	strCmd = """" & strTomcatInstallPath & "\webapps\InstallSSL.bat" & """"

	'Run the batch file which opens all of the necessary files for editing
	WshShell.Run strCmd, 1, True
	
	'Clean up
	Set WshShell = Nothing
End Sub

'This subroutine creates the required virtual directory for SSL installations
Sub CreateJakartaVirtualDirectory(strTomcatISAPIPath, strWebsiteName)
	Dim Root
	Dim Dir	
	
	On Error Resume Next

	'Connect to the local server
	Set Root = GetObject("IIS://localhost/W3SVC/" & GetIISWebsiteNumber(strWebsiteName) & "/Root")

 	'Error Handling
 	If (Err.Number <> 0) Then
 		MsgBox "The specified Web site does not exist"
 		WScript.Quit
 	End If
	
	'Begin creating the Virtual Directory
	Set Dir = Root.Create("IIsWebVirtualDir", "jakarta")
	
	'Error Handling
 	If (Err.Number <> 0) Then
 		MsgBox "Virtual directory already exists.  It will not be created at this time."
 		Exit Sub
 	End If
	
	'Set the Tomcat directory path
	Dir.Path = strTomcatISAPIPath

	'Set the IIS virtual directory permissions
	Dir.AccessRead = True
	Dir.AccessExecute = True

	'Specify the friendly display name for the Virtual Directory
	Dir.AppFriendlyName = "jakarta"
	
	'Save all the configuration information
	'and proceed to create the IIS virtual directory
	Dir.AppCreate(True)
	Dir.SetInfo
	
	If Err.Number <> 0 Then
		MsgBox Err.Description
	End If

	'Clean up
	Set Dir = Nothing
	Set Root = Nothing
	
End Sub

'Retrieves the website number for a specified website
Function GetIISWebsiteNumber(strWebsiteName)
	Dim IISObj, IISWebsite
	Dim intWebsiteNumber
	
	'Connect to the web server
	Set IISObj = GetObject("IIS://LocalHost/W3SVC")
	
	'Loop through all of the websites on the web server
	For Each IISWebsite in IISObj
        If (IISWebsite.Class = "IIsWebServer") Then
        	If (strWebsiteName = IISWebsite.ServerComment) Then
        		'Get the website number for the specified Web site
        		intWebsiteNumber = IISWebsite.Name
        	End If
        End If
    Next
    
    'Return the ID of the website as the value of the function
    GetIISWebsiteNumber = intWebsiteNumber
End Function

'Function to create and enable the Jakarta Web Server Extension
Function CreateWebSvcExtension(strTomcatISAPIPath)
	Dim WshShell
	Dim strCmd
	
	'Create the Windows Scripting Host Object
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	'Create the command to be executed
	strCmd = "cscript %SYSTEMROOT%\system32\iisext.vbs /AddFile "
	strCmd = strCmd & """" & strTomcatISAPIPath & "\isapi_redirect.dll" & """ " 'Path to the DLL
	strCmd = strCmd & "1 " 'Enable the extension
	strCmd = strCmd & "jakarta " 'ID of the web service extension
	strCmd = strCmd & "1 " 'Deletable flag
	strCmd = strCmd & "jakarta " 'Short description of the web service extension
	
	'Execute the command to create and enable the Jakarta Web Service Extension
	WshShell.Run strCmd, 7, True
	
	'Clean up
	Set WshShell = Nothing
End Function

'Function to install the ISAPI filter on the relevant web site in IIS
Sub InstallISAPIFilter(strWebsiteName, strISAPIFilterName, strISAPIFilterPath)
	Dim WshShell
	Dim strCmd
	Dim intWebsiteNumber
	
	'Create the Windows Scripting Host Object
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	intWebsiteNumber = GetIISWebsiteNumber(strWebsiteName)
	
	'Create the command to be executed
	strCmd = "cscript FiltTool.js -site:W3SVC/" & intWebsiteNumber &  " -name:" & strISAPIFilterName & " -dll:" & strISAPIFilterPath & " -action:add"
	
	'Execute the command to create the ISAPI filter on the relevant website
	WshShell.Run strCmd, 7, True
	
	'Clean up
	Set WshShell = Nothing
End Sub

'Function to install the ISAPI filter on the relevant web site in IIS
Sub AddISAPIFilter(strWebsiteName, strISAPIFilterName, strISAPIFilterDescription, strISAPIFilterPath)
	Dim FiltersObj 
	Dim FilterObj 
	Dim LoadOrder 
	Dim FilterName 
	Dim FilterPath 
	Dim FilterDesc 
	Dim intWebsiteNumber
	On Error Resume Next

	'Set the ISAPI Filter properties
	FilterName = strISAPIFilterName
	FilterPath = strISAPIFilterPath
	FilterDesc = strISAPIFilterDescription
	
	'Retrieve the Website Number for the selected web site
	intWebsiteNumber = GetIISWebsiteNumber(strWebsiteName)
	
	'Connect to the local computer for setting up the ISAPI filter
	Set FiltersObj = GetObject("IIS://localhost/W3SVC/" & intWebsiteNumber & "/Filters") 
	LoadOrder = FiltersObj.FilterLoadOrder 
	If LoadOrder <> "" Then 
	  LoadOrder = LoadOrder & "," 
	End If 

	'Specify the Load order for the IIS ISAPI filter
	'NOTE: This will install the ISAPI filter if it has not been previously created on the system
	LoadOrder = LoadOrder & FilterName
	FiltersObj.FilterLoadOrder = LoadOrder 
	FiltersObj.SetInfo 
	
	'Configure the ISAPI Filter in IIS if this is the first time 
	'the ISAPI filter is being configured on the machine
	Set FilterObj = FiltersObj.Create("IIsFilter", FilterName) 
	FilterObj.FilterPath = FilterPath 
	FilterObj.FilterDescription = FilterDesc 
	FilterObj.SetInfo 

End Sub

'Function to remove the ISAPI filter on the relevant web site in IIS
Sub RemoveISAPIFilter(strISAPIFilterName)
	 Dim objFilterProps, objFilters
    Dim strLoadOrder
    Dim intStartFilt

    Set objFilters = GetObject("IIS://LocalHost/W3SVC/Filters")
    strLoadOrder = objFilters.FilterLoadOrder
    If strLoadOrder <> "" Then
       If Right(strLoadOrder, 1) <> "," Then
          strLoadOrder = strLoadOrder & ","
       End If
       intStartFilt = InStr(strLoadOrder, strISAPIFilterName)
       strLoadOrder = Mid(strLoadOrder, 1, intStartFilt - 1) & _
          Mid(strLoadOrder, intStartFilt + Len(strFilterName) + 1, _
             Len(strLoadOrder))
       objFilters.FilterLoadOrder= strLoadOrder
       objFilters.SetInfo
       objFilters.Delete "IIsFilter", strISAPIFilterName
    End If
    Set objFilters = Nothing
End Sub