using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections.Generic;

namespace Riskmaster.Tools.C2C
{
    /**************************************************************
	 * $File		: frmC2C.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/18/2008
	 * $Author		: Rahul Solanki 
	 * $Comment		: The tool migrates the comments attached to events and claims into enhanced notes. 	 
	**************************************************************/
    /*
    *	The tool can either be run directly or can be executed via a bacth file/installer in silent mode.
    *   Command line parameters-
    *  Riskmaster.Tools.C2C.exe uid pwd DSN
    *		
    */
    public partial class frmC2C : Form
    {        
        private static long m_CommentsCount = 0;
        private static long m_EnhcNotesCount = 0;
         
        public static  StreamWriter m_sw1;

        private int m_iNoteId = -1;

        private static DbWriter m_CommentdbWriter;
        
        //using the compiled regex now.....
        
        Regex RexLineBreaks = new Regex(@"<br/>", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        char[] m_Breaks = new char[] { '\t', '\n', ' ' };

        public frmC2C()
        {
            InitializeComponent(); 
        }
        
        //Form Load 
        private void frmC2C_Load(object sender, EventArgs e)
        {
            if (AppGlobals.bSilentMode)
            {
                this.Hide();                
            }
            m_sw1 = new StreamWriter("C2C.log", true);
            if (AppGlobals.bSilentMode)
            {             
                writeLog("Running in silent mode; DSN: " + AppGlobals.sDSN);
            }
            writeLog("-----------------------------");
            writeLog("Form Loaded... authenticating");
            writeLog("-----------------------------");
           
            Boolean bCon = false;
            try
            {
                    // enable the follwoing line when running in debug mode.
                    // this will enable the developer to view the extracted notes before they can be saved to db
                    //this.Size = new Size(870, 621);
                    this.FormBorderStyle = FormBorderStyle.Fixed3D;

                    //setting status messages                    
                    txtNewEnhcCode.Text = "";//m_sEnhcNotesShortCode;
                    txtnewEnhcCodeDesc.Text = ""; // m_sEnhcNotesCodeDesc;
                    
                    setStatus("Click on the 'Start' button to start Data Migration...");
                    //loadLOBinfo(); 
                    // if application is called thru the command line then directly proceed to saving the Ench Notes. 
                    if (AppGlobals.bSilentMode)
                    {
                        btnStart_Click(null, null);                                                
                    }
                    
                //MessageBox.Show(getNoteTypeCodeId().ToString()   );
                if (AppGlobals.bSilentMode)
                {
                    this.Hide();
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                if (!AppGlobals.bSilentMode)
                {
                    writeLog(string.Format("ERROR:[form Load error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", ex.Message.ToString(), ex.InnerException.ToString(), ex.StackTrace.ToString()));
                    throw new Exception(ex.Message.ToString(), ex.InnerException);
                }
                       //MessageBox.Show(ex.Message, "C2C error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Application.Exit();
            }
        }

         //appends text to the RTF box
        private void setStatus(string p_status)
        {
            //lblStatus.Text=p_status;
            rtfStatus.AppendText(p_status + "\n");
        }

        //updates the progress bar text box (its just above the progress bar)
        private void setProgressStatus(string p_status)
        {
            txtProgressStatusMessage.Text = p_status;
        }

        
        //closing all connections & disposing 
        private void frmC2C_FormClosing(object sender, FormClosingEventArgs e)
        {
            writeLog("Disposing all objects" );
            if (AppGlobals.m_dsComments!=null) AppGlobals.m_dsComments.Dispose();
            if (AppGlobals.m_dsComment_Temp!=null) AppGlobals.m_dsComment_Temp.Dispose();
            if (m_sw1 != null)
            {                
                m_sw1.Close();
                m_sw1.Dispose();
            }
            
            //if (m_CommentdbWriter!=null)
            //{
            //    m_CommentdbWriter.
            //}
            
        }

        public static void writeLog(string p_strLogText)
        {
            string sMessage = string.Format("[{0}] {1}", System.DateTime.Now.ToString(), p_strLogText);            
            try
            {                
                m_sw1.WriteLine(sMessage);                
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            string sqlLoadComment = string.Empty;
            //string sqlLOBGroup = string.Empty;
            StringBuilder sbSqlLOBGroup = new StringBuilder();
                        
            btnStart.Enabled = false;
            //preparing the enhc Notes dataset.
            //doing an extra table count check; basically usefull in debugging scenario only
            if (AppGlobals.m_dsComment_Temp.Tables.Count == 0)
            {
                AppGlobals.m_dsComment_Temp.Tables.Add("COMMENTS_TEXT_TEMP");
                AppGlobals.m_dsComment_Temp.Tables[0].Columns.Add("ATTACH_RECORDID");
                AppGlobals.m_dsComment_Temp.Tables[0].Columns.Add("ATTACH_TABLE");
                AppGlobals.m_dsComment_Temp.Tables[0].Columns.Add("COMMENTS");
                AppGlobals.m_dsComment_Temp.Tables[0].Columns.Add("HTMLCOMMENTS");
            }

            
            //preparing the dbWriter 
            m_CommentdbWriter = DbFactory.GetDbWriter(m_objDbConnection);
            m_CommentdbWriter.Tables.Add("COMMENTS_TEXT");
                                    
            m_CommentdbWriter.Fields.Add("COMMENT_ID", 0);
            m_CommentdbWriter.Fields.Add("ATTACH_RECORDID", "");
            m_CommentdbWriter.Fields.Add("ATTACH_TABLE", "");
            m_CommentdbWriter.Fields.Add("COMMENTS", "");
            m_CommentdbWriter.Fields.Add("HTMLCOMMENTS", "");

            //getting new value for primary index                 
            
            m_iNoteId = DbFactory.ExecuteScalar(m_objDbConnection.ConnectionString, "SELECT MAX(COMMENT_ID) +1 FROM COMMENTS_TEXT");

            //loading comments attached to events
            sqlLoadComment = "SELECT  EVENT_ID, COMMENTS, HTMLCOMMENTS FROM EVENT WHERE COMMENTS IS NOT NULL ";
            txtProcessingNow.Text = "Events";            
            setProgressStatus(string.Format("Progress for phase 1 of COMMENTS Data Migration"));  
            loadComments(sqlLoadComment, txtProcessingNow.Text);
            // extract enhc notes here....
            writeLog("Extracting Comments from Events");
            ExtractEventComments();

            sqlLoadComment = string.Format("SELECT CLAIM_ID, COMMENTS, HTMLCOMMENTS FROM CLAIM WHERE COMMENTS IS NOT NULL");
            txtProcessingNow.Text = string.Format("Claims ");
            setProgressStatus(string.Format("Progress for phase 2 of COMMENTS Data Migration"));
            loadComments(sqlLoadComment, txtProcessingNow.Text);
            // extracting enhc notes ....   
            writeLog("Extracting Comments from Comments"); 
            ExtractClaimComments();
            // adding up Lob Codes for 
            
            prgBrNotesSave.Value = 100;

            btnStart.Enabled = false;

            // Updating Glossary table
            writeLog("Updating Glossary table with NextUniqueValue");
            UpdateGlossary();

            setStatus("Comments data migration completed.");
            writeLog("Comments data migration completed.");
            if (!AppGlobals.bSilentMode)
            {
                MessageBox.Show("Comments data migration completed.");
            }
            

            // ("comments to Enhanced notes migration completed.");
        }

        private void loadComments(string p_sqlLoadComment, string p_ProcessingNow)
        {
            writeLog(string.Format("Loading comments for: {0}", p_ProcessingNow));
            setStatus(string.Format("Loading comments for: {0}", p_ProcessingNow));
            try
            {
                AppGlobals.m_dsComments.Clear();
                AppGlobals.m_dsComments = DbFactory.GetDataSet(m_objDbConnection, p_sqlLoadComment);
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[Load Comments] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            //writeLog("Comments load operation completed.");            
            writeLog(string.Format("Comment Load operation completed for: {0}", p_ProcessingNow));
            setStatus(string.Format("Comment Load operation completed for: {0}", p_ProcessingNow));
            m_CommentsCount += AppGlobals.m_dsComments.Tables[0].Rows.Count;
            txtCommentCount.Text = m_CommentsCount.ToString();
            writeLog(string.Format("{0} comments loaded so far",txtCommentCount.Text ));            
        }

        // Extracting Notes from comments/event dataset
        private void ExtractEventComments()
        {           
            string CommentLine = string.Empty, HTMLCommentLine = string.Empty;
            string Comment = string.Empty, HTMLComment = string.Empty;
            string Attach_RecordId = string.Empty;
            prgBrNotesSave.Value = 0; //seting the Progress bar to zero.
            string sEventCommentTemp=string.Empty;
            int i = 0;
           
            // notes extracted form claims are appended into dataset 
            // where earlier notes extracted from events were populated 
            for (i = 0; i <= AppGlobals.m_dsComments.Tables[0].Rows.Count - 1; i++)
            {
                Comment = AppGlobals.m_dsComments.Tables[0].Rows[i]["COMMENTS"].ToString();
                HTMLComment = AppGlobals.m_dsComments.Tables[0].Rows[i]["HTMLCOMMENTS"].ToString();
                Attach_RecordId = AppGlobals.m_dsComments.Tables[0].Rows[i]["EVENT_ID"].ToString();

                sEventCommentTemp = RexLineBreaks.Replace(Comment, "").Trim(m_Breaks);
                if (sEventCommentTemp.Length > 0)
                {
                    AppGlobals.m_dsComment_Temp.Tables[0].Rows.Add(Attach_RecordId, "EVENT", Comment, HTMLComment);
                    saveComments();
                }

                if (i%50==0)
                {
                    //writing to log progress afetr every 50 comments have been processed.                    
                    setStatus("Saving Comments....");
                }
                if (i % 10 == 0)
                {
                    //writing the notes count to log file
                    writeLog(string.Format("{0} Comments extracted so far.", txtNotesCount.Text));
                }
                // saving the extracted enhc Notes here.                
                //Thread.Sleep(100);
                Application.DoEvents();
                //saveComments();
                prgBrNotesSave.Value = (i+1)*100/AppGlobals.m_dsComments.Tables[0].Rows.Count;                
                
                AppGlobals.m_dsComment_Temp.Tables[0].Clear();


            }
            //a small value might be left out at the end of the progress bar, so setting ti to 100 as we have already processed all the comments
            //prgBrNotesSave.Value = 100;
        }


        private void ExtractClaimComments()
        {
            string CommentLine = string.Empty, HTMLCommentLine = string.Empty;
            string Comment = string.Empty, HTMLComment = string.Empty;
            string Attach_RecordId = string.Empty;
            prgBrNotesSave.Value = 0; //seting the Progress bar to zero.
            string sCommentTemp = string.Empty;
            int i = 0;
            string sClaimCommentTemp = string.Empty;

            // notes extracted form claims are appended into dataset 
            // where earlier notes extracted from events were populated 
            for (i = 0; i <= AppGlobals.m_dsComments.Tables[0].Rows.Count - 1; i++)
            {
                Comment = AppGlobals.m_dsComments.Tables[0].Rows[i]["COMMENTS"].ToString();
                HTMLComment = AppGlobals.m_dsComments.Tables[0].Rows[i]["HTMLCOMMENTS"].ToString();
                Attach_RecordId = AppGlobals.m_dsComments.Tables[0].Rows[i]["CLAIM_ID"].ToString();

                sClaimCommentTemp = RexLineBreaks.Replace(Comment, "").Trim(m_Breaks);
                if (sClaimCommentTemp.Length > 0)
                {
                    AppGlobals.m_dsComment_Temp.Tables[0].Rows.Add(Attach_RecordId, "CLAIM", Comment, HTMLComment);
                    saveComments();
                }

                if (i % 50 == 0)
                {
                    //writing to log progress afetr every 50 comments have been processed.                    
                    setStatus("Saving Comments....");
                }
                if (i % 10 == 0)
                {
                    //writing the notes count to log file
                    writeLog(string.Format("{0} Comments extracted so far.", txtNotesCount.Text));
                }
                // saving the extracted enhc Notes here.                
                //Thread.Sleep(100);
                Application.DoEvents();

                prgBrNotesSave.Value = (i + 1) * 100 / AppGlobals.m_dsComments.Tables[0].Rows.Count;

                AppGlobals.m_dsComment_Temp.Tables[0].Clear();


            }
            //a small value might be left out at the end of the progress bar, so setting ti to 100 as we have already processed all the comments
            //prgBrNotesSave.Value = 100;
        }

        //saving individual Notes into DB
        private void saveComments()
        {
            int iRowCount = 0,  iEnhNotesCount, iPercentCompleted;
            bool bDoSkip = false; // to be removed after dbupgrade patch
            iEnhNotesCount = AppGlobals.m_dsComment_Temp.Tables[0].Rows.Count;
            string strEnteredByName=string.Empty;
            int iUid = -1, iGroupID = -1;
            string strUid = string.Empty, strGroupID = string.Empty;

            //writeLog("Starting Enhc Notes save operation.");

            //DbReader objDbReader;

            for (iRowCount = 0; iRowCount < iEnhNotesCount; iRowCount++)
            {
                //iRowCount = p_rowNum;                

                //adding values for each enh notes row.           
                m_CommentdbWriter.Fields["COMMENT_ID"].Value = m_iNoteId.ToString();
                m_iNoteId++;
                m_CommentdbWriter.Fields["ATTACH_RECORDID"].Value = AppGlobals.m_dsComment_Temp.Tables[0].Rows[iRowCount][0].ToString();
                m_CommentdbWriter.Fields["ATTACH_TABLE"].Value = AppGlobals.m_dsComment_Temp.Tables[0].Rows[iRowCount][1].ToString();
                m_CommentdbWriter.Fields["COMMENTS"].Value = AppGlobals.m_dsComment_Temp.Tables[0].Rows[iRowCount][2].ToString();
                m_CommentdbWriter.Fields["HTMLCOMMENTS"].Value = AppGlobals.m_dsComment_Temp.Tables[0].Rows[iRowCount][3].ToString();              

                try
                {
                    if (!bDoSkip) m_CommentdbWriter.Execute();
                }
                catch (Exception exp)
                {
                    writeLog(string.Format("ERROR:[Error writing Notes ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                    throw new Exception(exp.Message.ToString(), exp.InnerException);
                }
                m_EnhcNotesCount++;
                txtNotesCount.Text = m_EnhcNotesCount.ToString();
            }
            
        }

        //update NextUniqueValue in Glossary Table
        private void UpdateGlossary()
        {
            int iNoteId = 0, iCodeId=0;
            DbReader objDbReader;
            //DbWriter objDbWriter;
            DbCommand objCmd = null;
            string strSql=string.Empty;
            try
            {
                //updating next_unique_id for enhanced Notes  
                iNoteId = DbFactory.ExecuteScalar(m_objDbConnection.ConnectionString, "SELECT MAX(COMMENT_ID) +1 FROM COMMENTS_TEXT");
                strSql = string.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = {0} WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) = 'COMMENTS_TEXT')", iNoteId);                
                DbFactory.ExecuteNonQuery(string.Empty, strSql, new Dictionary<string, string>());
                               
                
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[Error updating glossary table ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            } 
        
        }
 
    }
}