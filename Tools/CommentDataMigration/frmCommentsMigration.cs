﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using DevComponents.DotNetBar;

using Riskmaster.Db;
using Riskmaster.Security;

namespace Riskmaster.Tools.C2C
{
    public partial class frmCommentsMigration : Office2007RibbonForm
    {
        #region declare global variables
        private int m_iDSNId;
        private int m_iNoteId = -1;
        private static long m_CommentsCount = 0;
        private static long m_EnhcNotesCount = 0;
        private string m_strSelectedDSN = String.Empty;
        private string m_ConnectionString = String.Empty;
        public static StreamWriter m_sw1;
        private static DbWriter m_CommentdbWriter;

        Regex RexLineBreaks = new Regex(@"<br/>", RegexOptions.IgnoreCase | RegexOptions.Compiled); //using the compiled regex
        char[] m_Breaks = new char[] { '\t', '\n', ' ' };
        #endregion

        /// <summary>
        /// frmCommentsMigration
        /// </summary>
        public frmCommentsMigration()
        {
            InitializeComponent();
        }

        /// <summary>
        /// frmCommentsMigration_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmCommentsMigration_Load(object sender, EventArgs e)
        {
            m_sw1 = new StreamWriter("C2C.log", true);
            LoadRiskmasterDatabases(); 
        }

        /// <summary>
        /// LoadRiskmasterDatabases
        /// </summary>
        private void LoadRiskmasterDatabases()
        {
            try
            {
                using (DataTable dtDSNs = RiskmasterDatabase.GetRiskmasterDatabases())
                {
                    //added list view to stay consistent with dB Upgrade Wizard - prg 04/10/09
                    foreach (DataRow dtRow in dtDSNs.Rows)
                    {
                        ListViewItem lvItem = new ListViewItem { Text = dtRow["DSN"].ToString() };

                        lvItem.SubItems.Add(dtRow["DSNID"].ToString());
                        lvItem.SubItems.Add(dtRow["CONNECTION_STRING"].ToString());

                        lvDatabases.Items.Add(lvItem);
                    }//foreach
                }//using
            }
            catch
            {
                MessageBox.Show("Check your connectionStrings.config", "Database Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        /// <summary>
        /// Gets the database information that the user selects
        /// </summary>
        private void GetDSNSelections()
        {
            const int DSN_ID = 1; //Populated as the value for the ListViewSubItem
            const int DSN_CONNECTION_STRING = 2; //Populated as the value for the ListViewSubItem

            //changed to retrieve from list view added - prg 04/10/09
            foreach (ListViewItem lvItem in lvDatabases.SelectedItems)
            {
                m_strSelectedDSN = lvItem.Text.ToString();
                lblDatabase.Text = m_strSelectedDSN;
                Application.DoEvents();

                //Get the individually selected DSN ID
                m_iDSNId = Convert.ToInt32(lvItem.SubItems[DSN_ID].Text.ToString());

                //Get the individually selected connection strings
                m_ConnectionString = lvItem.SubItems[DSN_CONNECTION_STRING].Text.ToString();
            }
        } // method: GetDSNSelections

        /// <summary>
        /// appends text to the RTF box
        /// </summary>
        /// <param name="p_status"></param>
        private void setStatus(string p_status)
        {
            //lblStatus.Text=p_status;
            rtfStatus.AppendText(p_status + "\n");
        }

        /// <summary>
        /// updates the progress bar text box (its just above the progress bar)
        /// </summary>
        /// <param name="p_status"></param>
        private void setProgressStatus(string p_status)
        {
            txtProgressStatusMessage.Text = p_status;
        }

        /// <summary>
        /// writeLog
        /// </summary>
        /// <param name="p_strLogText"></param>
        public static void writeLog(string p_strLogText)
        {
            string sMessage = string.Format("[{0}] {1}", System.DateTime.Now.ToString(), p_strLogText);


            try
            {
                m_sw1.WriteLine(sMessage);
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
        }

        /// <summary>
        /// btnStart_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            string sqlLoadComment = String.Empty;

            //preparing the enhc Notes dataset.
            //doing an extra table count check; basically usefull in debugging scenario only
            if (AppGlobals.m_dsComment_Temp.Tables.Count == 0)
            {
                AppGlobals.m_dsComment_Temp.Tables.Add("COMMENTS_TEXT_TEMP");
                AppGlobals.m_dsComment_Temp.Tables[0].Columns.Add("ATTACH_RECORDID");
                AppGlobals.m_dsComment_Temp.Tables[0].Columns.Add("ATTACH_TABLE");
                AppGlobals.m_dsComment_Temp.Tables[0].Columns.Add("COMMENTS");
                AppGlobals.m_dsComment_Temp.Tables[0].Columns.Add("HTMLCOMMENTS");
            }

            //preparing the dbWriter 
            m_CommentdbWriter = DbFactory.GetDbWriter(m_ConnectionString);
            m_CommentdbWriter.Tables.Add("COMMENTS_TEXT");
            m_CommentdbWriter.Fields.Add("COMMENT_ID", 0);
            m_CommentdbWriter.Fields.Add("ATTACH_RECORDID", "");
            m_CommentdbWriter.Fields.Add("ATTACH_TABLE", "");
            m_CommentdbWriter.Fields.Add("COMMENTS", "");
            m_CommentdbWriter.Fields.Add("HTMLCOMMENTS", "");

            //getting new value for primary index                 
            try
            {
                m_iNoteId = Convert.ToInt32(DbFactory.ExecuteScalar(m_ConnectionString, "SELECT MAX(COMMENT_ID) +1 FROM COMMENTS_TEXT"
    ));
                //loading comments attached to events
                sqlLoadComment = "SELECT  EVENT_ID, COMMENTS, HTMLCOMMENTS FROM EVENT WHERE COMMENTS IS NOT NULL ";
                txtProcessingNow.Text = "Events";
                setProgressStatus("Progress for phase 1 of COMMENTS Data Migration");
                loadComments(sqlLoadComment, txtProcessingNow.Text);

                // extract enhc notes here....
                writeLog("Extracting Comments from Events");
                ExtractEventComments();

                sqlLoadComment = "SELECT CLAIM_ID, COMMENTS, HTMLCOMMENTS FROM CLAIM WHERE COMMENTS IS NOT NULL";
                txtProcessingNow.Text = "Claims ";
                setProgressStatus("Progress for phase 2 of COMMENTS Data Migration");
                loadComments(sqlLoadComment, txtProcessingNow.Text);

                // extracting enhc notes ....   
                writeLog("Extracting Comments from Comments");
                ExtractClaimComments();
                // adding up Lob Codes for 

                prgBrNotesSave.Value = 100;

                btnStart.Enabled = false;

                // Updating Glossary table
                writeLog("Updating Glossary table with NextUniqueValue");
                UpdateGlossary();

                setStatus("Comments data migration completed.");
                writeLog("Comments data migration completed.");
            }
            catch
            {
                //MessageBox.Show("This utility is to be used with RMX R5 PS2 databases only!", "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show("Comments Data Migration Operation Failed!", "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        /// <summary>
        /// loadComments
        /// </summary>
        /// <param name="p_sqlLoadComment"></param>
        /// <param name="p_ProcessingNow"></param>
        private void loadComments(string p_sqlLoadComment, string p_ProcessingNow)
        {
            writeLog(string.Format("Loading comments for: {0}", p_ProcessingNow));
            setStatus(string.Format("Loading comments for: {0}", p_ProcessingNow));

            try
            {
                AppGlobals.m_dsComments = DbFactory.GetDataSet(m_ConnectionString, p_sqlLoadComment);
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[Load Comments] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                //throw new Exception(exp.Message.ToString(), exp.InnerException);  Commented by csingh7 : 9/4/2009
            }

            //writeLog("Comments load operation completed.");            
            writeLog(string.Format("Comment Load operation completed for: {0}", p_ProcessingNow));
            setStatus(string.Format("Comment Load operation completed for: {0}", p_ProcessingNow));
            m_CommentsCount += AppGlobals.m_dsComments.Tables[0].Rows.Count;
            txtCommentCount.Text = m_CommentsCount.ToString();
            writeLog(string.Format("{0} comments loaded so far", txtCommentCount.Text));
        }

        /// <summary>
        /// Extracting Notes from comments/event dataset
        /// </summary>
        private void ExtractEventComments()
        {
            string Comment = string.Empty, HTMLComment = string.Empty;
            string Attach_RecordId = string.Empty;
            prgBrNotesSave.Value = 0; //seting the Progress bar to zero.
            string sEventCommentTemp = string.Empty;
            int i = 0;

            // notes extracted form claims are appended into dataset 
            // where earlier notes extracted from events were populated 
            for (i = 0; i <= AppGlobals.m_dsComments.Tables[0].Rows.Count - 1; i++)
            {
                Comment = AppGlobals.m_dsComments.Tables[0].Rows[i]["COMMENTS"].ToString();
                HTMLComment = AppGlobals.m_dsComments.Tables[0].Rows[i]["HTMLCOMMENTS"].ToString();
                Attach_RecordId = AppGlobals.m_dsComments.Tables[0].Rows[i]["EVENT_ID"].ToString();

                sEventCommentTemp = RexLineBreaks.Replace(Comment, "").Trim(m_Breaks);
                if (sEventCommentTemp.Length > 0)
                {
                    AppGlobals.m_dsComment_Temp.Tables[0].Rows.Add(Attach_RecordId, "EVENT", Comment, HTMLComment);
                    saveComments();
                }

                if (i % 50 == 0)
                {
                    //writing to log progress afetr every 50 comments have been processed.                    
                    setStatus("Saving Comments....");
                }
                if (i % 10 == 0)
                {
                    //writing the notes count to log file
                    writeLog(string.Format("{0} Comments extracted so far.", txtNotesCount.Text));
                }
                // saving the extracted enhc Notes here.                
                //Thread.Sleep(100);
                Application.DoEvents();
                //saveComments();
                prgBrNotesSave.Value = (i + 1) * 100 / AppGlobals.m_dsComments.Tables[0].Rows.Count;

                AppGlobals.m_dsComment_Temp.Tables[0].Clear();
            }

            //a small value might be left out at the end of the progress bar, so setting ti to 100 as we have already processed all the comments
            //prgBrNotesSave.Value = 100;
        }

        /// <summary>
        /// ExtractClaimComments
        /// </summary>
        private void ExtractClaimComments()
        {
            string Comment = string.Empty, HTMLComment = string.Empty;
            string Attach_RecordId = string.Empty;
            prgBrNotesSave.Value = 0; //seting the Progress bar to zero.
            int i = 0;
            string sClaimCommentTemp = string.Empty;

            // notes extracted form claims are appended into dataset 
            // where earlier notes extracted from events were populated 
            for (i = 0; i <= AppGlobals.m_dsComments.Tables[0].Rows.Count - 1; i++)
            {
                Comment = AppGlobals.m_dsComments.Tables[0].Rows[i]["COMMENTS"].ToString();
                HTMLComment = AppGlobals.m_dsComments.Tables[0].Rows[i]["HTMLCOMMENTS"].ToString();
                Attach_RecordId = AppGlobals.m_dsComments.Tables[0].Rows[i]["CLAIM_ID"].ToString();

                sClaimCommentTemp = RexLineBreaks.Replace(Comment, "").Trim(m_Breaks);

                if (sClaimCommentTemp.Length > 0)
                {
                    AppGlobals.m_dsComment_Temp.Tables[0].Rows.Add(Attach_RecordId, "CLAIM", Comment, HTMLComment);
                    saveComments();
                }

                if (i % 50 == 0)
                {
                    //writing to log progress afetr every 50 comments have been processed.                    
                    setStatus("Saving Comments....");
                }

                if (i % 10 == 0)
                {
                    //writing the notes count to log file
                    writeLog(string.Format("{0} Comments extracted so far.", txtNotesCount.Text));
                }
                // saving the extracted enhc Notes here.                
                //Thread.Sleep(100);
                Application.DoEvents();

                prgBrNotesSave.Value = (i + 1) * 100 / AppGlobals.m_dsComments.Tables[0].Rows.Count;

                AppGlobals.m_dsComment_Temp.Tables[0].Clear();
            }

            //a small value might be left out at the end of the progress bar, so setting ti to 100 as we have already processed all the comments
            //prgBrNotesSave.Value = 100;
        }

        /// <summary>
        /// saving individual Notes into DB
        /// </summary>
        private void saveComments()
        {
            int iRowCount = 0, iEnhNotesCount;
            bool bDoSkip = false; // to be removed after dbupgrade patch
            iEnhNotesCount = AppGlobals.m_dsComment_Temp.Tables[0].Rows.Count;

            for (iRowCount = 0; iRowCount < iEnhNotesCount; iRowCount++)
            {
                //iRowCount = p_rowNum;                

                //adding values for each enh notes row.           
                m_CommentdbWriter.Fields["COMMENT_ID"].Value = m_iNoteId.ToString();
                m_iNoteId++;
                m_CommentdbWriter.Fields["ATTACH_RECORDID"].Value = AppGlobals.m_dsComment_Temp.Tables[0].Rows[iRowCount][0].ToString();
                m_CommentdbWriter.Fields["ATTACH_TABLE"].Value = AppGlobals.m_dsComment_Temp.Tables[0].Rows[iRowCount][1].ToString();
                m_CommentdbWriter.Fields["COMMENTS"].Value = AppGlobals.m_dsComment_Temp.Tables[0].Rows[iRowCount][2].ToString();
                m_CommentdbWriter.Fields["HTMLCOMMENTS"].Value = AppGlobals.m_dsComment_Temp.Tables[0].Rows[iRowCount][3].ToString();

                try
                {
                    if (!bDoSkip) m_CommentdbWriter.Execute();
                }
                catch (Exception exp)
                {
                    writeLog(string.Format("ERROR:[Error writing Notes ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                    //throw new Exception(exp.Message.ToString(), exp.InnerException);    Commented by csingh7 : 9/4/2009
                }

                m_EnhcNotesCount++;
                txtNotesCount.Text = m_EnhcNotesCount.ToString();
            }
        }

        /// <summary>
        /// update NextUniqueValue in Glossary Table
        /// </summary>
        private void UpdateGlossary()
        {
            int iNoteId = 0;
            string strSql = string.Empty;

            try
            {
                //updating next_unique_id for enhanced Notes  
                iNoteId = Convert.ToInt32(DbFactory.ExecuteScalar(m_ConnectionString, "SELECT MAX(COMMENT_ID) +1 FROM COMMENTS_TEXT"));
                strSql = string.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = {0} WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) = 'COMMENTS_TEXT')", iNoteId);
                DbFactory.ExecuteNonQuery(m_ConnectionString, strSql, new Dictionary<string, string>());
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[Error updating glossary table ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                //throw new Exception(exp.Message.ToString(), exp.InnerException);  Commented by csingh7 : 9/4/2009
            }
        }

        /// <summary>
        /// frmCommentsMigration_FormClosing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmCommentsMigration_FormClosing(object sender, FormClosingEventArgs e)
        {
            writeLog("Disposing all objects");
            if (AppGlobals.m_dsComments != null) AppGlobals.m_dsComments.Dispose();
            if (AppGlobals.m_dsComment_Temp != null) AppGlobals.m_dsComment_Temp.Dispose();

            if (m_sw1 != null)
            {
                m_sw1.Close();
                m_sw1.Dispose();
            }

            //if (m_CommentdbWriter!=null)
            //{
            //    m_CommentdbWriter.
            //}
        }

        /// <summary>
        /// wizard1_WizardPageChanging
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_WizardPageChanging(object sender, WizardCancelPageChangeEventArgs e)
        {
            if (!String.IsNullOrEmpty(m_strSelectedDSN))
            {
                lblDatabase.Text = m_strSelectedDSN;
                Application.DoEvents();
            }

            //navigating forward from database selection screen
            if (e.OldPage == wpDSNSelection && e.PageChangeSource == eWizardPageChangeSource.NextButton)
            {
                //do not change page if no database has been selected
                if (lvDatabases.SelectedItems.Count == 0)
                {
                    //lblWarningWPDS.Visible = true;
                    e.Cancel = true;
                }
                else
                {
                    //Get the selected DSN information
                    GetDSNSelections();
                    btnStart.Enabled = true;
                } // else
            }//if            

        }

        /// <summary>
        /// Event Handled for when the Cancel button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizard1_CancelButtonClick(object sender, CancelEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        /// <summary>
        /// Event Handled for when the Finish button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizCommentsMigration_FinishButtonClick(object sender, CancelEventArgs e)
        {
            //Close the form
            Close();

            //Remove the application from memory
            System.Windows.Forms.Application.Exit();
        }
    }//class: frmCommentsMigration
}//namespace: Riskmaster.Tools.C2C
