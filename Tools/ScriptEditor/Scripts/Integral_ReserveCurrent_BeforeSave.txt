Sub PSCReserveCurrent( objReserveCurrent As Riskmaster.Datamodel.ReserveCurrent )

	Try
            If objReserveCurrent.Context.LocalCache.GetRelatedCodeId(objReserveCurrent.ReserveTypeCode) = objReserveCurrent.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE") Then
                If (objReserveCurrent.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM RESERVE_CURRENT RC, CODES CD WHERE RC.CLAIM_ID = " & objReserveCurrent.ClaimId & " AND RC.RESERVE_TYPE_CODE = CD.CODE_ID  AND CD.RELATED_CODE_ID <> " & objReserveCurrent.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE")) = 0) Then
                     g_ValErrors.Add(g_ValErrors.Count + 1, "First Reserve on a Claim can not be of Recovery type.")
                End If
            End If
        Catch ex As Exception
            Log.Write(" ***ERROR*** " + ex.ToString() + " in Reserve Current before save for Claim ID - " + Trim(objReserveCurrent.ClaimId), "Scripting")
        Finally
            '
        End Try 
End Sub