Attribute VB_Name = "UTIL_CDS32"
Option Explicit

Declare Function bCodesInit Lib "DTGCSR32.DLL" (ByVal hdbc As Long) As Long

Declare Function bShortCodeExists Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal shortcode As String, ByVal LOB As Long, ByVal codeid As Long, ByVal tableid As Long) As Long
Declare Function bGetCodeDesc Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal codeid As Long, ByVal DESC As String, length As Long) As Long
Declare Function bGetCode Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal codeid As Long, ByVal DESC As String, length As Long) As Long
Declare Function bGetShortCode Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal codeid As Long, ByVal DESC As String, length As Long) As Long
Declare Function bGetCodeWithPart Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal shortcode As String, ByVal tableid As Long, codeid As Long, ByVal LOB As Long, ByVal DESC As String, length As Long) As Long
Declare Function bStartListOfCodes Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal tableid As Long, ByVal LOB As Long, ByVal context As String) As Long
Declare Function bGetListOfCodes Lib "DTGCSR32.DLL" (ByVal context As String, ByVal shortcode As String, len_shortcode As Long, LOB As Long, ByVal DESC As String, len_desc As Long, CODE_ID As Long) As Long

Declare Function lGetCodeIDWithShort Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal shortcode As String, ByVal tableid As Long) As Long
Declare Function lGetRelatedCodeID Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal codeid As Long) As Long
Declare Sub ClearCodes Lib "DTGCSR32.DLL" ()
Declare Function bAddCodeToCache Lib "DTGCSR32.DLL" (ByVal CODE_ID As Long, ByVal TABLE_ID As Long, ByVal DESC As String, ByVal SHORT_CODE As String, ByVal RELATED_CODE_ID As Long, ByVal DELETED_FLAG As Long, ByVal LINE_OF_BUS_CODE As Long, ByVal IND_STANDARD_CODE As Long) As Long
'Declare Function bDeleteCodeFromCache Lib "DTGCSR32.DLL" (ByVal CODE_ID As Long) As Integer
Declare Function bModifyCode Lib "DTGCSR32.DLL" (ByVal CODE_ID As Long, ByVal TABLE_ID As Long, ByVal DESC As String, ByVal SHORT_CODE As String, ByVal RELATED_CODE_ID As Long, ByVal DELETED_FLAG As Long, ByVal LINE_OF_BUS_CODE As Long, ByVal IND_STANDARD_CODE As Long) As Long
Declare Function nNumCodeEntries Lib "DTGCSR32.DLL" () As Long
Declare Function bGetCodeInfo Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal CODE_ID As Long, TABLE_ID As Long, ByVal DESC As String, desc_len As Long, ByVal SHORT_CODE As String, shortcode_len As Long, RELATED_CODE_ID As Long, DELETED_FLAG As Long, LINE_OF_BUS_CODE As Long, IND_STANDARD_CODE As Long) As Long
Declare Function bIsCacheable lib "DTGCSR32.DLL" (ByVal lTableID as Long) as Long

Declare Function bAddStateToCache Lib "DTGCSR32.DLL" (ByVal CODE_ID As Long, ByVal DESC As String, ByVal SHORT_CODE As String, ByVal DELETED_FLAG As Long) As Long
'Declare Function bDeleteStateFromCache Lib "DTGCSR32.DLL" (ByVal STATE_ROW_ID As Long) As Integer
Declare Function bModifyState Lib "DTGCSR32.DLL" (ByVal STATE_ROW_ID As Long, ByVal STATE_NAME As String, ByVal STATE_ID As String, ByVal DELETED_FLAG As Long) As Long
'Declare Function bGetStateInfo Lib "DTGCSR32.DLL" (ByVal STATE_ROW_ID As Long, ByVal STATE_NAME As String, len_desc As Integer, ByVal STATE_ID As String, len_shortcode As Integer, DELETED_FLAG As Integer) As Integer
Declare Function nNumStateEntries Lib "DTGCSR32.DLL" () As Long
Declare Function bGetStateWithPart Lib "DTGCSR32.DLL" (ByVal STATE_ID As String, STATE_ROW_ID As Long) As Long
Declare Sub ClearStates Lib "DTGCSR32.DLL" ()
Declare Function bStatesInit Lib "DTGCSR32.DLL" (ByVal hdbc As Long) As Long
Declare Function bGetStateCode Lib "DTGCSR32.DLL" (ByVal STATE_ROW_ID As Long, ByVal szStateId As String, len_stateid As Long) As Long
Declare Function bGetState Lib "DTGCSR32.DLL" (ByVal STATE_ROW_ID As Long, ByVal szStateId As String, len_stateid As Long) As Long
Declare Function bStartListOfStates Lib "DTGCSR32.DLL" (ByVal pos_context As String) As Long
Declare Function bGetListOfStates Lib "DTGCSR32.DLL" (ByVal pos_context As String, STATE_ROW_ID As Long, ByVal STATE_NAME As String, len_desc As Long, ByVal STATE_ID As String, len_shortcode As Long) As Long
Declare Function bGetStateName Lib "DTGCSR32.DLL" (ByVal STATE_ROW_ID As Long, ByVal szStateName As String, len_statename As Long) As Long
Declare Function lGetStateRowID lib "DTGCSR32.dll" (ByVal STATE_ID as String) as Long

' glossary
Declare Function bInitGloss Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal force As Long) As Long
Declare Function bIsIndStandRqd Lib "DTGCSR32.DLL" (ByVal table As Long) As Long
Declare Function bIsLOBRqrd Lib "DTGCSR32.DLL" (ByVal table As Long) As Long
Declare Function bIsParentRqrd Lib "DTGCSR32.DLL" (ByVal table As Long) As Long
Declare Function bAllowAttachments Lib "DTGCSR32.DLL" (ByVal table As Long) As Long
Declare Function lGetGlssryTypCd Lib "DTGCSR32.DLL" (ByVal table As Long) As Long
Declare Function lGetIndStandTblID Lib "DTGCSR32.DLL" (ByVal table As Long) As Long
Declare Function lGetParentTableID Lib "DTGCSR32.DLL" (ByVal table As Long) As Long
Declare Function lGetOrgChildTableID Lib "DTGCSR32.DLL" (ByVal table As Long) As Long
Declare Function lGetTableIDCache Lib "DTGCSR32.DLL" Alias "lGetTableID" (ByVal sTable As String) As Long
' Declare Function lGetNextUIDEx Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal szTable As String, nErrStatus As Integer) As Long
Declare Function lGetNextUIDEx2 Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal szTable As String, nErrStatus As Long, ByVal sErrText As String, ByVal nErrBufLen As Integer) As Long

Declare Sub GetRltdTblNm Lib "DTGCSR32.DLL" (ByVal sTable As String, ByVal sChild As String)
Declare Sub GetSystemTableName Lib "DTGCSR32.DLL" (ByVal sTable As String, ByVal table As Long)
Declare Sub GetTableName Lib "DTGCSR32.DLL" (ByVal sTable As String, ByVal table As Long)
Declare Sub GetTbl_Prt Lib "DTGCSR32.DLL" (ByVal sTable As String, table As Long, ByVal sShort As String, ByVal lType As Long)
Declare Function GetFirstTable Lib "DTGCSR32.DLL" (ByVal sTable As String, table As Long, ByVal lType As Long) As Long
Declare Function GetNextTable Lib "DTGCSR32.DLL" (ByVal sTable As String, table As Long, ByVal pos As Long, ByVal lType As Long) As Long

' org hierarchy
Declare Function InitOrgHierarchy Lib "DTGCSR32.DLL" (ByVal hdbc As Long) As Long
Declare Sub ClearOrgHierarchy Lib "DTGCSR32.DLL" ()

Declare Function OrgIsEmpty Lib "DTGCSR32.DLL" () As Long

Declare Function bOrgExists Lib "DTGCSR32.DLL" (ByVal lEntityID As Long) As Long
Declare Sub GetOrgInfo Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal sName As String, ByVal sAbbreviation As String, lTableID As Long, lParentEID As Long)
Declare Sub GetOrgName Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal sName As String)
Declare Sub GetOrgAbbreviation Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal sAbbreviation As String)
Declare Function lGetOrgTableID Lib "DTGCSR32.DLL" (ByVal lEntityID As Long) As Long
'Declare Function bIsOrgDeleted Lib "DTGCSR32.DLL" (ByVal lEntityID As Long) As Integer
Declare Function GetPeopleName Lib "DTGCSR32.DLL" (ByVal hdbc As Long, ByVal lEntityID As Long, ByVal szName As String) As Long

Declare Sub SetOrgDeleted Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal bDeleted As Long)

Declare Function lGetOrgParent Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal nLevelsUp As Long) As Long
Declare Function lGetOrgParentByTableID Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal lTableID As Long) As Long

Declare Function FindOrgStart Lib "DTGCSR32.DLL" (ByVal lEntityTableID As Long) As Long
Declare Function FindOrgStartEx Lib "DTGCSR32.DLL" (ByVal lEntityTableID As Long, ByVal lEntityID As Long) As Long
Declare Function lFindOrgNext Lib "DTGCSR32.DLL" (ByVal pContext As Long) As Long
Declare Sub FindOrgEnd Lib "DTGCSR32.DLL" (ByVal pContext As Long)

Declare Function WalkOrgStartDF Lib "DTGCSR32.DLL" (ByVal lStartEID As Long) As Long
Declare Function lWalkOrgNextDF Lib "DTGCSR32.DLL" (ByVal pContext As Long, nLevel As Long) As Long
Declare Sub WalkOrgEndDF Lib "DTGCSR32.DLL" (ByVal pContext As Long)

Declare Function AddOrgItem Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal sName As String, ByVal sAbbreviation As String, ByVal lTableID As Long, ByVal lParentEID As Long) As Long
'Declare Sub RemoveOrgItem Lib "DTGCSR32.DLL" (ByVal lEntityID As Long)
Declare Function ModifyOrgItemName Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal sNewName As String) As Long
Declare Function ModifyOrgItemAbbreviation Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal sNewAbbreviation As String) As Long
Declare Function ModifyOrgItemParentEID Lib "DTGCSR32.DLL" (ByVal lEntityID As Long, ByVal lParentEID As Long) As Long

Declare Function bGetOrgEntryWithPart Lib "DTGCSR32.DLL" (ByVal szPartial As String, ByVal lTableID As Long, lEntityID As Long, ByVal szName As String, ByVal szFullAbbrev As String, lParentEID As Long) As Long

