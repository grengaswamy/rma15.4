Attribute VB_Name = "RSW_PAY_TLDIARY"
Option Explicit

Global m_bUseSupAppReserves As Boolean 'for Supervisory Approval flag of reserve worksheet
Global m_iDaysAppReserves As Integer 'for time limit in days for supervisory approval of reserve worksheet
Global m_iHoursAppReserves As Integer 'for time limit in hours for supervisory approval of reserve worksheet
Global m_bNotifySupReserves As Boolean 'for specifying stepwise or jump functionality
Global m_bUseCurrAdjReserves As Boolean 'to specify to use current adjuster supervisory approval
Global m_iLevel As Integer 'for Supervisory Approval level
Global m_lClaim_ID As Long 'for claim Id
Global m_iLOBCode As Integer 'for line of business code of the claim
Global m_iClmTypeCode As Integer 'for claim type of claim
Global m_iHoldSendDiary As Integer 'for sending diary in case payment is placed on hold
Global m_iDaysForApproval As Integer 'for time limit in days for supervisory approval of payments
Global m_iHoursForApproval As Integer 'for time limit in hours for supervisory approval of payments
Global m_RSWLastUpdatedDate As String 'store last updated date of RserveWorksheet on 12Feb2010
Global m_bDisableEmailNotifyForSuperVsr As Boolean 'store the flag to send the email notification to supervisior for reserve worksheet'
Global m_bDisableDiaryNotifyForSuperVsr As Boolean 'store the flag to send the dairy notification to supervisior for reserve worksheet'
'rsushilaggar MITS 19624 30-Mar-2010'
Global m_bDisableEmailNotifyForPayment As Boolean 'store the flag to send the email notification to supervisior for payment'
        
Public Type structReserves
    iReserveTypeCode As Long 'MITS 28292 Changed by bsharma33
    dTotalPaid As Double
    dTotalCollected As Double
    dTotalIncurred As Double
    dBalance As Double
    sReserveDesc As String
    sStatusCode As String
End Type

Public Type structResFinal
    iReserveTypeCode As Integer
    dBalAmount As Double
    dNewBalAmount As Double
    dNewReserves As Double
End Type

Global m_arrstructReserves() As structReserves
Global m_arrstructResFinal() As structResFinal
Dim m_iArrSize As Integer

Sub GenerateTimeLapseDiaries()

On Error GoTo ERROR_TRAP_GenerateTimeLapseDiaries

    'Check Payment Utilities setting and send diaries accordingly
    Call CheckPaymentTLDiaries
       
    'Check Reserve Worksheet Utilities setting and send diaries accordingly
    Call CheckResWorksheetTLDiaries

    Exit Sub

ERROR_TRAP_GenerateTimeLapseDiaries:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\GenerateTimeLapseDiaries") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub

Sub CheckPaymentTLDiaries()

On Error GoTo ERROR_TRAP_CheckPaymentTLDiaries
                 
                   
    Call ReadCheckOptionsForPayments
    
    Call PayAppTimeLapseDiaries
    
                  
    Exit Sub

ERROR_TRAP_CheckPaymentTLDiaries:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\CheckPaymentTLDiaries") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub

Sub CheckResWorksheetTLDiaries()

On Error GoTo ERROR_TRAP_CheckResWorksheetTLDiaries

    Call ReadCheckOptionsForResWorksheet
    
    Call ResWorksheetTimeLapseDiaries
    
    Exit Sub

ERROR_TRAP_CheckResWorksheetTLDiaries:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\CheckResWorksheetTLDiaries") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub
'smahajan6 - Safeway - Payment Approval Time Lapsed
'Added New
Sub ReadCheckOptionsForPayments()

On Error GoTo ERROR_TRAP_ReadCheckOptionsForPayments

Dim rs1 As Integer

m_iHoldSendDiary = 0
m_iDaysForApproval = 0
m_iHoursForApproval = 0
'rsushilaggar MITS 19624 30-Mar-2010'
m_bDisableEmailNotifyForPayment = False

    UpdateStatus "Getting Utility settings for Payments: Start " & Chr$(34)
    
    rs1 = DB_CreateRecordset(dbGlobalConnect, "SELECT * FROM CHECK_OPTIONS", DB_FORWARD_ONLY, 0)

    If Not DB_EOF(rs1) Then
  
        m_iHoldSendDiary = iAnyVarToInt(vDB_GetData(rs1, "SH_DIARY"))
        m_iDaysForApproval = iAnyVarToInt(vDB_GetData(rs1, "DAYS_FOR_APPROVAL"))
        m_iHoursForApproval = iAnyVarToInt(vDB_GetData(rs1, "HOURS_FOR_APPROVAL"))
        'rsushilaggar MITS 19624 30-Mar-2010'
        m_bDisableEmailNotifyForPayment = vDB_GetData(rs1, "DISABLE_EMAIL_NOTIFY_FOR_PYMT")
                        
        If gProcessStatus = PS_STOP Then
            If CheckStop() Then
                DB_CloseRecordset rs1, DB_DROP
                subHourGlass False
                Exit Sub
            End If
        End If

    End If
    
    DB_CloseRecordset rs1, DB_DROP
    
    UpdateStatus "Getting Utility settings for Payments: Complete " & Chr$(34)

    Exit Sub

ERROR_TRAP_ReadCheckOptionsForPayments:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\ReadCheckOptionsForPayments") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub

Sub ReadCheckOptionsForResWorksheet()

On Error GoTo ERROR_TRAP_ReadCheckOptionsForResWorksheet

Dim rs1 As Integer

m_bUseSupAppReserves = False
m_iDaysAppReserves = 0
m_iHoursAppReserves = 0
m_bNotifySupReserves = False
m_bUseCurrAdjReserves = False
'rsushilaggar MITS 19624 30-Mar-2010'
m_bDisableDiaryNotifyForSuperVsr = False
m_bDisableEmailNotifyForSuperVsr = False


    UpdateStatus "Getting Utility settings for Reserve Worksheet: Start " & Chr$(34)
    
    rs1 = DB_CreateRecordset(dbGlobalConnect, "SELECT * FROM CHECK_OPTIONS", DB_FORWARD_ONLY, 0)

    If Not DB_EOF(rs1) Then
        m_bUseSupAppReserves = vDB_GetData(rs1, "USE_SUP_APP_RESERVES")
        m_iDaysAppReserves = iAnyVarToInt(vDB_GetData(rs1, "DAYS_APP_RESERVES"))
        m_iHoursAppReserves = iAnyVarToInt(vDB_GetData(rs1, "HOURS_APP_RESERVES"))
        m_bNotifySupReserves = vDB_GetData(rs1, "NOTIFY_SUP_RESERVES")
        m_bUseCurrAdjReserves = vDB_GetData(rs1, "USE_CUR_ADJ_RESERVES")
        'rsushilaggar MITS 19624 30-Mar-2010'
        m_bDisableEmailNotifyForSuperVsr = vDB_GetData(rs1, "DISABLE_EMAIL_NOTIFY_FOR_SUPV")
        m_bDisableDiaryNotifyForSuperVsr = vDB_GetData(rs1, "DISABLE_DIARY_NOTIFY_FOR_SUPV")
                        
        If gProcessStatus = PS_STOP Then
            If CheckStop() Then
                DB_CloseRecordset rs1, DB_DROP
                subHourGlass False
                Exit Sub
            End If
        End If

    End If
    
    DB_CloseRecordset rs1, DB_DROP
    
    UpdateStatus "Getting Utility settings for Reserve Worksheet: Complete " & Chr$(34)

    Exit Sub

ERROR_TRAP_ReadCheckOptionsForResWorksheet:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\ReadCheckOptionsForResWorksheet") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub
'smahajan6 - Safeway - Payment Approval Time Lapsed
'Added New
Sub PayAppTimeLapseDiaries()

On Error GoTo ERROR_TRAP_PayAppTimeLapseDiaries

    UpdateStatus "Generating Diaries for Payments: Start " & Chr$(34)
    
    Dim rs As Integer
    Dim rs1 As Integer
    Dim result As Integer
    Dim sSQL As String
    Dim db As Integer
    Dim iCount As Integer
    
    Dim lFundsHoldStatusCode As Long
    Dim dAssDateTime As Date
    Dim dTempDateTime As Date
    Dim sInterval As String
    Dim iNumber As Integer
    Dim iStep As Integer
    Dim lLobLevelUserId As Long
    Dim lTopLevelUserId As Long
    
    Dim lTransId As Long
    Dim sCtlNumber As String
    Dim lApproverId As Long
    Dim sDiaryAssigningUser As String
    Dim sDiaryAssignedUser As String
    
    Dim lRecipientUserId As Long
    Dim sRecipientLoginName As String
    Dim lSenderUserId As Long
    Dim sSenderLoginName As String
    Dim sRegarding As String
    Dim sAttachTable As String
    Dim lAttachRecordID As Long
    Dim sEntryName As String
    Dim sDiaryNotes As String
    
    Dim sUserName As String
    Dim sFromEmail As String
    Dim sToEmail As String
    Dim sSubject As String
    Dim sBody As String
    Dim bSendMailStatus As Boolean    'Add by hsingh61
    Dim bIsMailEntry As Boolean    'Add by hsingh61
    
    db = DB_OpenDatabase(hEnv, sTrimNull(objLogin.SecurityDSN), 0)

    lFundsHoldStatusCode = lGetCodeIDWithShort(DB_GetHdbc(dbLookup), "H", lGetTableID("CHECK_STATUS"))
           
    sSQL = "SELECT * FROM PMT_APPROVAL_HIST WHERE PAH_ROW_ID IN("
    sSQL = sSQL + " SELECT MAX(PAH_ROW_ID) FROM PMT_APPROVAL_HIST"
    sSQL = sSQL + " WHERE TRANS_ID IN (SELECT FUNDS.TRANS_ID FROM FUNDS"
    sSQL = sSQL + " WHERE FUNDS.PAYMENT_FLAG <> 0"
    sSQL = sSQL + " AND FUNDS.STATUS_CODE = " & lFundsHoldStatusCode & " AND FUNDS.VOID_FLAG = 0)"
    sSQL = sSQL + " GROUP BY TRANS_ID) "
    sSQL = sSQL + " ORDER BY  PAH_ROW_ID DESC "
     
    rs = DB_CreateRecordset(dbGlobalConnect, sSQL, DB_FORWARD_ONLY, 0)

    While Not DB_EOF(rs)

        lTransId = lAnyVarToLong(vDB_GetData(rs, "TRANS_ID"))
        lApproverId = lAnyVarToLong(vDB_GetData(rs, "APPROVER_ID"))

        '*******************************************************************************
        
        'Get the Last Diary Assigned User
       
        Select Case g_dbMake
            Case DBMS_IS_SQLSRVR
                sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY "
                sSQL = sSQL + " WHERE ATTACH_TABLE = 'FUNDS' AND ENTRY_NOTES LIKE 'The time for payment approval lapsed for%'"
                sSQL = sSQL + " AND ATTACH_RECORDID = " & lTransId
                sSQL = sSQL + " ORDER BY ENTRY_ID DESC"
            Case DBMS_IS_ORACLE
                sSQL = "SELECT ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY "
                sSQL = sSQL + " WHERE ATTACH_TABLE = 'FUNDS' AND ENTRY_NOTES LIKE 'The time for payment approval lapsed for%'"
                sSQL = sSQL + " And ROWNUM <= 1 AND ATTACH_RECORDID = " & lTransId
                sSQL = sSQL + " ORDER BY ENTRY_ID DESC"
            Case Else
                sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY "
                sSQL = sSQL + " WHERE ATTACH_TABLE = 'FUNDS' AND ENTRY_NOTES LIKE 'The time for payment approval lapsed for%'"
                sSQL = sSQL + " AND ATTACH_RECORDID = " & lTransId
                sSQL = sSQL + " ORDER BY ENTRY_ID DESC"
        End Select
        
        
        rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
        
        If Not DB_EOF(rs1) Then
            sDiaryAssigningUser = vDB_GetData(rs1, "ASSIGNING_USER") & ""
            sDiaryAssignedUser = vDB_GetData(rs1, "ASSIGNED_USER") & ""
        Else
            sDiaryAssigningUser = ""
            sDiaryAssignedUser = ""
        End If
        
        result = DB_CloseRecordset(rs1, DB_DROP)
        
        dAssDateTime = dGetDateTimeFormat(vDB_GetData(rs, "DTTM_APPROVAL_CHGD") & "")

        If (m_iDaysForApproval > 0) Then
            iNumber = m_iDaysForApproval
            sInterval = "d"
        Else
            iNumber = m_iHoursForApproval
            sInterval = "h"
        End If

        dTempDateTime = dAssDateTime

        If (iNumber > 0) Then
            iCount = 0
            While (dTempDateTime < Now)
            
                iCount = iCount + 1
        
                dTempDateTime = DateTime.DateAdd(sInterval, iNumber, dTempDateTime)
                
            Wend
        End If
        
        lRecipientUserId = lApproverId
        lSenderUserId = lApproverId
        
        For iStep = 1 To iCount - 1
            
            If (lRecipientUserId = 0) Then
                Exit For
            Else
                lSenderUserId = lRecipientUserId
            End If
            
            lRecipientUserId = lGetManagerID(lRecipientUserId)
            
        Next
        
        '********************************************************************************
        
        If (lRecipientUserId = 0) Then

            sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL LSA, CLAIM C, FUNDS F"
            sSQL = sSQL + " WHERE LSA.LOB_CODE = C.LINE_OF_BUS_CODE And C.CLAIM_ID = F.CLAIM_ID"
            sSQL = sSQL + " AND F.TRANS_ID = " & lTransId
            rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
            If Not DB_EOF(rs1) Then
                lLobLevelUserId = vDB_GetData(rs1, "USER_ID") 'For getting LOB level User
            Else
                lLobLevelUserId = 0
            End If
            result = DB_CloseRecordset(rs1, DB_DROP)
            
            sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"
            rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
            If Not DB_EOF(rs1) Then
                lTopLevelUserId = vDB_GetData(rs1, "USER_ID") 'For getting Top level User
            Else
               lTopLevelUserId = 0
            End If
            result = DB_CloseRecordset(rs1, DB_DROP)
        

            If (iStep = iCount And lLobLevelUserId <> 0) Then
            
                lRecipientUserId = lLobLevelUserId
                
            Else
            
                'Sender - LOB Level User , if exist else the Highest level manager
                If (lLobLevelUserId <> 0) Then
                
                    lSenderUserId = lLobLevelUserId
                    iStep = iStep + 1
                    
                End If
                
                lRecipientUserId = lTopLevelUserId
                 
            End If
            
        End If

        '********************************************************************************

        sSenderLoginName = sGetUser(db, objUser.DSNID, lSenderUserId)
        sRecipientLoginName = sGetUser(db, objUser.DSNID, lRecipientUserId)
        
        If (lSenderUserId <> lRecipientUserId And lSenderUserId <> lTopLevelUserId) Then
        
            sSQL = "SELECT CTL_NUMBER FROM FUNDS WHERE TRANS_ID = " & lTransId
            rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
            If Not DB_EOF(rs1) Then
                sCtlNumber = vDB_GetData(rs1, "CTL_NUMBER") & ""
            Else
                sCtlNumber = ""
            End If
            result = DB_CloseRecordset(rs1, DB_DROP)
            
            sUserName = sGetUserName(sSenderLoginName) 'Get Sender First Name and Last Name
        
            'Create Diary
            '*********************************************************************************************
            If (m_iHoldSendDiary) Then
                If (sDiaryAssigningUser <> sSenderLoginName Or sDiaryAssignedUser <> sRecipientLoginName) Then
            
                    sRegarding = "Payments: " & sCtlNumber
                    sAttachTable = "FUNDS"
                    lAttachRecordID = lTransId
                    sEntryName = "Check On Hold"
                    sDiaryNotes = "The time for payment approval lapsed for " & sUserName & ". "
                    sDiaryNotes = sDiaryNotes & "The payment has been submitted for your approval."
                    
                    Call GenerateDiary(sSenderLoginName, sRecipientLoginName, sRegarding, sAttachTable, lAttachRecordID, sEntryName, sDiaryNotes)
                
                End If
            End If
            '***************************************************************************************************
    
            'Send Email
            '***************************************************************************************************
            sFromEmail = sGetEmailByLoginName(sSenderLoginName)
            sToEmail = sGetEmailByLoginName(sRecipientLoginName)
            
            'hsingh61 for mits:22561

            If (Trim(sFromEmail) <> "" And Trim(sToEmail) <> "" And m_bDisableEmailNotifyForPayment <> True) Then
            
                sSQL = "SELECT ENTRY_ID FROM WPA_EMAIL_ENTRY"
                sSQL = sSQL + " WHERE ATTACH_REC_ID = '" & sCtlNumber & "'"
                sSQL = sSQL + " AND MAIL_REGARDING = 'Approval Request for Payment: " & sCtlNumber & "'"
                sSQL = sSQL + " AND SENDER_ID = '" & sSenderLoginName & "'"
                sSQL = sSQL + " AND RECEIVER_ID = '" & sRecipientLoginName & "'"
            
                rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
                If Not DB_EOF(rs1) Then
                     bIsMailEntry = False
                Else
                    bIsMailEntry = True
                End If
                result = DB_CloseRecordset(rs1, DB_DROP)
            
                If (bIsMailEntry) Then
                dAssDateTime = DateTime.DateAdd(sInterval, iNumber * (iStep - 1), dAssDateTime)
                
                sSubject = "Approval Request for Payment: " & sCtlNumber

                sBody = "A Payment: " & sCtlNumber & " has been submitted for your approval on " & dAssDateTime & " by " & sUserName & "." & vbCrLf
                sBody = sBody & "Please enter the RISKMASTER system and navigate to Approve Funds Transactions."
                sBody = sBody & " There you will find this and other Requests awaiting your approval." & vbCrLf & vbCrLf
                sBody = sBody & "Reason: The time for payment approval lapsed for " & sUserName & ". " & vbCrLf & vbCrLf
                sBody = sBody & "Thank you," & vbCrLf & "RISKMASTER X"
                
                    'Change by hsingh61 for mits:22561
                    'Call bSendEmail(sUserName, sFromEmail, sToEmail, sSubject, sBody)
                     bSendMailStatus = bSendEmail(sUserName, sFromEmail, sToEmail, sSubject, sBody)
                
                        'Add by hsingh61 for mits:22561
                        If (bSendMailStatus) Then
                            'Need to enter record in database once we send mail for future validation
                            Call EmailEntry(sCtlNumber, sSubject, sSenderLoginName, sRecipientLoginName)
                        End If
                
                End If
            End If
            '***************************************************************************************************
            
        End If
        
        '********************************************************************************
        
        DB_MoveNext rs
        
   Wend
   
   result = DB_CloseRecordset(rs, DB_DROP)
   result = DB_CloseDatabase(db)
   
   UpdateStatus "Generating Diaries for Payments: Complete " & Chr$(34)

Exit Sub

ERROR_TRAP_PayAppTimeLapseDiaries:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\PayAppTimeLapseDiaries") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub

'smahajan6 - Safeway - Payment Approval Time Lapsed
'Added New Function - Get First Name and Last Name
Public Function sGetUserName(sSenderLoginName) As String
  
    On Error GoTo hError:

    Dim sSQL As String
    Dim rs As Integer
    Dim db As Integer
    
    db = DB_OpenDatabase(hEnv, sTrimNull(objLogin.SecurityDSN), 0)
            
    sSQL = "SELECT LAST_NAME, FIRST_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID"
    sSQL = sSQL & " AND USER_DETAILS_TABLE.LOGIN_NAME = '" & sSenderLoginName & "'"
    sSQL = sSQL & " AND DSNID = " & objUser.DSNID

    rs = DB_CreateRecordset(db, sSQL, DB_FORWARD_ONLY, 0)
    
    If Not DB_EOF(rs) Then
        sGetUserName = vDB_GetData(rs, "FIRST_NAME") & " " & vDB_GetData(rs, "LAST_NAME")
    Else
        sGetUserName = ""
    End If
    
    Call DB_CloseRecordset(rs, DB_DROP)
    Call DB_CloseDatabase(db)
    
Exit Function

hError:
    Dim EResult As Integer
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\sGetUserName") = IDRETRY Then Resume
    subHourGlass False
End Function

'smahajan6 - Safeway - Payment Approval Time Lapsed
'Added New Function
Public Function dGetDateTimeFormat(sDbDateTime)

    Dim dDateTime As Date

    If (Len(sDbDateTime) = 14) Then ' For Date Time

        dDateTime = DateSerial(Mid(sDbDateTime, 1, 4), Mid(sDbDateTime, 5, 2), Mid(sDbDateTime, 7, 2))
        dDateTime = DateTime.DateAdd("h", Mid(sDbDateTime, 9, 2), dDateTime)
        dDateTime = DateTime.DateAdd("n", Mid(sDbDateTime, 11, 2), dDateTime)
        dDateTime = DateTime.DateAdd("s", Mid(sDbDateTime, 13, 2), dDateTime)
        
    ElseIf (Len(sDbDateTime) = 8) Then 'For Date Only
    
        dDateTime = DateSerial(Mid(sDbDateTime, 1, 4), Mid(sDbDateTime, 5, 2), Mid(sDbDateTime, 7, 2))
    
    Else
        
        dDateTime = DateTime.Now
        
    End If
    
    dGetDateTimeFormat = dDateTime
        
Exit Function

hError:
    Dim EResult As Integer
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\dGetDateTimeFormat") = IDRETRY Then Resume
    subHourGlass False
End Function
'smahajan6 - Safeway - Payment Approval Time Lapsed
'Added New For Generating Diary
Sub GenerateDiary(sSenderLoginName, sRecipientLoginName, sRegarding, sAttachTable, lAttachRecordID, sEntryName, sDiaryNotes)

On Error GoTo hError:

    Dim sSQL As String

    If (Trim(sRecipientLoginName) <> "") Then
        
        sSQL = "INSERT INTO WPA_DIARY_ENTRY (ENTRY_ID,ENTRY_NAME,ENTRY_NOTES,CREATE_DATE,PRIORITY,"
        sSQL = sSQL & " STATUS_OPEN,AUTO_CONFIRM,ASSIGNED_USER, ASSIGNING_USER, ASSIGNED_GROUP,IS_ATTACHED,ATTACH_TABLE,"
        sSQL = sSQL & " ATT_FORM_CODE,ATTACH_RECORDID,REGARDING,ATT_SEC_REC_ID,COMPLETE_TIME,COMPLETE_DATE,"
        sSQL = sSQL & " DIARY_VOID, DIARY_DELETED,NOTIFY_FLAG,ROUTE_FLAG,ESTIMATE_TIME,AUTO_ID) "
        sSQL = sSQL & " VALUES(" & lGetNextUID("WPA_DIARY_ENTRY") & ",'" & ReplaceStr(sEntryName, "'", "''") & "','"
        sSQL = sSQL & ReplaceStr(sDiaryNotes, "'", "''") & "','" & Format$(Now, "YYYYMMDDHHMMSS") & "',1,1,0,'" & ReplaceStr(sRecipientLoginName, "'", "''") & "','"
        sSQL = sSQL & ReplaceStr(sSenderLoginName, "'", "''") & "','',1,'" & sAttachTable & "',3192,'" & lAttachRecordID & "','" & ReplaceStr(sRegarding, "'", "''")
        sSQL = sSQL & "',0,'235959','" & Format$(Now, "YYYYMMDD") & "',0,0,0,0,0,0)"
        
        DB_SQLExecute dbLookup, sSQL
        
        frmWPAProc.lblTotDiaries.Caption = CInt(frmWPAProc.lblTotDiaries.Caption) + 1

    End If

Exit Sub

hError:
    Dim EResult As Integer
     
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\GenerateDiary") = IDRETRY Then Resume
    subHourGlass False
End Sub
'smahajan6 - Safeway - Payment Approval Time Lapsed
'Added New Function - For Sending Email
Public Function bSendEmail(ByVal sSenderName As String, ByVal sSenderEmail As String, _
                           ByVal sRecipientTo As String, ByVal sSubject As String, ByVal sBody As String) As Boolean
    
    Dim objSMTP         As EasyMailSMTPObj
    Dim iResult         As Integer
    Dim db              As Integer
    Dim rs              As Integer
    Dim lReturn As Long
    Dim sErrString As String
    Dim bValidated As Boolean
On Error GoTo hError

    bSendEmail = False
    bValidated = True
    'Set Status
    Set objSMTP = New EasyMailSMTPObj

    If (sRecipientTo <> "") Then objSMTP.AddRecipient "", sRecipientTo, 1
    If (sRecipientTo = "") Then
        'subHourGlass False
        'MsgBox "Recipient does not have a valid Email Address.", vbInformation
        'subHourGlass True
        sErrString = "-> User does not have a valid Email Address."
        bValidated = False
        GoTo hExit
    End If
    
    ' Connect to security DB for fetching SMTP server.
    db = DB_OpenDatabase(hEnv, sTrimNull(objLogin.SecurityDSN), 0)
    rs = DB_CreateRecordset(db, "SELECT SMTP_SERVER FROM SETTINGS WHERE ID = 1", DB_FORWARD_ONLY, 0)
    objSMTP.MailServer = Trim$(sAnyVarToString(vDB_GetData(rs, "SMTP_SERVER")))
    iResult = DB_CloseRecordset(rs, DB_DROP)
    iResult = DB_CloseDatabase(db)
    If (objSMTP.MailServer = "") Then
        'subHourGlass False
        'MsgBox "Please specify Mail Server using Security Management System.", vbInformation
        'subHourGlass True
        'GoTo hExit
        sErrString = sErrString & " -> Please specify valid Admin Email Address using Security Management System."
        bValidated = False
    End If
    
    If bValidated = False Then
        sErrString = "Export To Email Failed. " & sErrString
        GoTo hValidationError
    End If
    
    objSMTP.From = sSenderName
    objSMTP.FromAddr = sSenderEmail
    objSMTP.Subject = sSubject
    objSMTP.BodyText = sBody
    objSMTP.LicenseKey = "Dorn Technology Group, Inc./30034206149719039930"
    lReturn = objSMTP.Send        'If return value is 0 that means successful
    If lReturn <> 0 Then
        'MsgBox "Error in Sending Mail.", vbInformation
        sErrString = "Error in Sending Mail."
        GoTo hValidationError
    Else
        bSendEmail = True
    End If
hExit:
    subHourGlass False
    Exit Function
hError:

    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\bSendEmail") = vbRetry Then
       Resume
    End If
    Resume hExit
hValidationError:
    Call subWriteErrorLog("GLOBAL.BAS", 0, sErrString)
    Exit Function
End Function
'smahajan6 - Safeway - Payment Approval Time Lapsed
'Added New Function - To Get Manager ID
Public Function lGetManagerID(iRMId As Long) As Long

On Error GoTo hError:

    Dim sSQL As String
    Dim rs As Integer
    Dim db As Integer
    
    db = DB_OpenDatabase(hEnv, sTrimNull(objLogin.SecurityDSN), 0)
            
    sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.MANAGER_ID "
    sSQL = sSQL & " FROM USER_DETAILS_TABLE,USER_TABLE"
    sSQL = sSQL & " WHERE USER_DETAILS_TABLE.DSNID = " & objUser.DSNID
    sSQL = sSQL & " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID"
    sSQL = sSQL & " AND USER_TABLE.USER_ID = " & iRMId

    rs = DB_CreateRecordset(db, sSQL, DB_FORWARD_ONLY, 0)
    
    If Not DB_EOF(rs) Then
        lGetManagerID = lAnyVarToLong(vDB_GetData(rs, "MANAGER_ID"))
    End If
    
    Call DB_CloseRecordset(rs, DB_DROP)
    Call DB_CloseDatabase(db)
    
Exit Function

hError:
    Dim EResult As Integer
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\lGetManagerID") = IDRETRY Then Resume
    subHourGlass False
End Function
'smahajan6 - Safeway - Payment Approval Time Lapsed
'Added New Function - Get Login Name
Public Function bGetLoginNameByUID(ByVal lUserID As Long, sLoginName As String) As Integer
'
'   Given the User ID (from user security table), return the login name and the
'   The restriction is, the entry obtained for this UID will refer to the
'   user entry for the current data source.
'
    Dim nResult As Integer, sTmp As String * 255
    Dim lDSNID As Long
    Dim sDSN As String
    Dim db As Integer
    Dim rs As Integer
    Dim sSQL As String
    Dim result As Integer

    ' Get current DSN ID Number
    lDSNID = objUser.DSNID

    sDSN = objLogin.SecurityDSN

    Rem Connect to security DB
    db = DB_OpenDatabase(hEnv, sDSN, 0)

    Dim var As Variant

    Rem find user in security db
    sSQL = "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE DSNID = " & lDSNID & " AND USER_ID = " & lUserID
    rs = DB_CreateRecordset(db, sSQL, DB_FORWARD_ONLY, 0)
    If Not DB_EOF(rs) Then
       result = DB_GetData(rs, 1, var) & ""
       sLoginName = var
       bGetLoginNameByUID = True
    Else
       sLoginName = ""
       bGetLoginNameByUID = False
    End If

    Rem Shut down
    result = DB_CloseRecordset(rs, DB_DROP)
    result = DB_CloseDatabase(db)

Exit Function

hError:
    Dim EResult As Integer
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\bGetLoginNameByUID") = IDRETRY Then Resume
    subHourGlass False

End Function
'smahajan6 - Safeway - Payment Approval Time Lapsed
'Added New Function - Get Table ID
Function lGetTableID(sTableName As String) As Long

  Dim lTableID As Long
  Dim SQL As String
  Dim rs As Integer
  Dim result As Integer
  
  lGetTableID = 0

  If Trim$(sTableName) = "" Then
    Exit Function
  End If

  lTableID = lGetTableIDCache(sTableName)
  If lTableID = 0 Then
     rs = DB_CreateRecordset(dbLookup, "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'", DB_FORWARD_ONLY, 0)
     If Not DB_EOF(rs) Then
        lTableID = lAnyVarToLong(vDB_GetData(rs, 1))
        DB_CloseRecordset rs, DB_DROP
     Else
        DB_CloseRecordset rs, DB_DROP
     End If
     
     result = bInitGlossary(False, True)
  End If
  lGetTableID = lTableID
  
Exit Function

hError:
    Dim EResult As Integer
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\lGetTableID") = IDRETRY Then Resume
    subHourGlass False
    
End Function

Sub ResWorksheetTimeLapseDiaries()

On Error GoTo ERROR_TRAP_ResWorksheetTimeLapseDiaries

Dim rs1 As Integer
    
    'Check if supervisory approval for reserve worksheet is enabled
    If (m_bUseSupAppReserves) Then
        'Check if there is any valid data in "Days for Approval",
        'if the data is not valid then this function is not required
  If (m_iDaysAppReserves > 0 Or m_iHoursAppReserves > 0) Then
            
            UpdateStatus "Generating Diaries for Reserve Worksheet: Start " & Chr$(34)
            
            Call AssignReserveWorksheet
            UpdateStatus "Generating Diaries for Reserve Worksheet: Complete " & Chr$(34)
        Else
            Exit Sub
        End If
    
    End If

    Exit Sub

ERROR_TRAP_ResWorksheetTimeLapseDiaries:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\ResWorksheetTimeLapseDiaries") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub

Sub AssignReserveWorksheet()

On Error GoTo ERROR_TRAP_AssignReserveWorksheet

Dim rs1 As Integer
Dim rs2 As Integer
Dim lPendingApprovalCode As Long

Dim result As Integer
Dim dtLastUpdatedRecord As String

    lPendingApprovalCode = lGetCodeIDWithShort(DB_GetHdbc(dbLookup), "PA", lGetTableID("RSW_STATUS"))
    
    'Check if supervisory approval for reserve worksheet is enabled
    If (m_bUseSupAppReserves) Then
        'Check if there is any valid data in "Days for Approval",
        'if the data is not valid then this function is not required
         If (m_iDaysAppReserves > 0 Or m_iHoursAppReserves > 0) Then
            
            rs1 = DB_CreateRecordset(dbGlobalConnect, "SELECT * FROM RSW_WORKSHEETS WHERE RSW_STATUS_CODE = " + CStr(lPendingApprovalCode), DB_FORWARD_ONLY, 0)
            
            While Not DB_EOF(rs1)
                'Assign each reserve worksheets to proper user
                m_iLevel = 1
                m_lClaim_ID = vDB_GetData(rs1, "CLAIM_ID")
                
                rs2 = DB_CreateRecordset(dbGlobalConnect2, "SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + CStr(m_lClaim_ID), DB_FORWARD_ONLY, 0)
                If Not DB_EOF(rs2) Then
                    m_iLOBCode = iAnyVarToInt(vDB_GetData(rs2, "LINE_OF_BUS_CODE"))
                    m_iClmTypeCode = iAnyVarToInt(vDB_GetData(rs2, "CLAIM_TYPE_CODE"))
                   
                    If gProcessStatus = PS_STOP Then
                       If CheckStop() Then
                           DB_CloseRecordset rs2, DB_DROP
                           subHourGlass False
                           Exit Sub
                      End If
                    End If
                End If
                result = DB_CloseRecordset(rs2, DB_DROP)

                Dim sDateStr As String
                Dim sTimeStr As String
                Dim sDate As String
                
                sDate = vDB_GetData(rs1, "DTTM_RCD_LAST_UPD")
                sDateStr = Mid(sDate, 5, 2) + "/" + Mid(sDate, 7, 2) + "/" + Mid(sDate, 1, 4)
                sTimeStr = Mid(sDate, 9, 2) + ":" + Mid(sDate, 11, 2) + ":" + Mid(sDate, 13, 2)
                dtLastUpdatedRecord = sDateStr + " " + sTimeStr
                
                Call GetResCategories(m_iLOBCode, m_iClmTypeCode)

                Call GetNewReserveAmts(m_iLOBCode, vDB_GetData(rs1, "RSW_XML"))
                
                If (m_bNotifySupReserves) Then
                    Call StepWiseUpdateRSWForUser(vDB_GetData(rs1, "RSW_ROW_ID"), vDB_GetData(rs1, "SUBMITTED_BY"), vDB_GetData(rs1, "SUBMITTED_TO"), dtLastUpdatedRecord, m_iLevel, vDB_GetData(rs1, "RSW_XML"))
                Else
                    Call JumpUpdateRSWForUser(vDB_GetData(rs1, "RSW_ROW_ID"), vDB_GetData(rs1, "SUBMITTED_BY"), vDB_GetData(rs1, "SUBMITTED_TO"), dtLastUpdatedRecord, m_iLevel, vDB_GetData(rs1, "RSW_XML"))
                End If
                
                If gProcessStatus = PS_STOP Then
                   If CheckStop() Then
                       DB_CloseRecordset rs1, DB_DROP
                       subHourGlass False
                       Exit Sub
                  End If
                End If
                
                DB_MoveNext rs1
            Wend
            DB_CloseRecordset rs1, DB_DROP
        Else
            Exit Sub
        End If
    
    End If

    Exit Sub

ERROR_TRAP_AssignReserveWorksheet:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\AssignReserveWorksheet") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub


Function StepWiseUpdateRSWForUser(p_iRSWID As Long, p_sSubmittedBy As String, p_sSubmittedTo As String, p_dSubmittedOn As String, p_iLevel As Integer, p_sXMLDoc As String)

On Error GoTo ERROR_TRAP_StepWiseUpdateRSWForUser

Dim sDS As String * 255
Dim sDSN As String
Dim db As Integer

Dim iInterval As Integer
iInterval = 1

Dim sSQL As String
sSQL = ""

Dim sSubmittedTo As String
sSubmittedTo = "0"

Dim bReturnValue As Boolean
bReturnValue = True

Dim rs1 As Integer
Dim result As Integer

Dim objDOMDocument As MSXML2.DOMDocument60

Dim sRegarding As String
Dim sPCName As String
Dim sClaimNum As String
Dim sRSWType As String
Dim sSenderLoginName As String
Dim sRecipientLoginName As String
Dim sFromEmail As String
Dim sToEmail As String
Dim sAttachTable As String
Dim lAttachRecordID As Long
Dim sEntryName As String
Dim sUserName As String
Dim sDiaryNotes As String
Dim sDiaryAssigningUser As String
Dim sDiaryAssignedUser As String

Dim iAdjID As Integer

    sDS = objLogin.SecurityDSN 'Security database DSN
    sDSN = sTrimNull(sDS)
    db = DB_OpenDatabase(hEnv, sDSN, 0)

    'iDays = p_iLevel * m_iDaysAppReserves
    If (m_iDaysAppReserves > 0) Then
        iInterval = p_iLevel * m_iDaysAppReserves
    ElseIf (m_iHoursAppReserves > 0) Then
        iInterval = p_iLevel * m_iHoursAppReserves
    Else
        iInterval = 0
    End If
    
    'If worsksheet has lapsed for the user, find its supervisor
    'If (DateAdd("d", CDbl(iDays), p_dSubmittedOn) < Now) Then
     If (AddIntervalToSubmitDate(p_dSubmittedOn, iInterval) < Now) Then
        sSQL = "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + p_sSubmittedTo
        rs1 = DB_CreateRecordset(db, sSQL, DB_FORWARD_ONLY, 0)
        If Not DB_EOF(rs1) Then
            sSubmittedTo = vDB_GetData(rs1, "MANAGER_ID")
        End If
        result = DB_CloseRecordset(rs1, DB_DROP)
            
        'If supervisor exists for this user, determine if the worksheet has lapsed for it too
        'or not otherwise assign worksheet to supervisor
        If (sSubmittedTo <> "0") Then
            'Date submitted updated
            If db <> 0 Then DB_CloseDatabase db
            'p_dSubmittedOn = DateAdd("d", CDbl(m_iDaysAppReserves), p_dSubmittedOn)
            p_dSubmittedOn = AddIntervalToSubmitDate(p_dSubmittedOn, 0)
            Call StepWiseUpdateRSWForUser(p_iRSWID, p_sSubmittedTo, sSubmittedTo, p_dSubmittedOn, p_iLevel, p_sXMLDoc)
        Else 'No supervisor for this user
            
            'check if the worsksheet has lapsed for topmost supervisor too.
            'if yes, then assign it to LOB manager
            
            Dim iLOBCode As Integer
            Dim iClmTypeCode As Integer
            Dim iLOBManagerID As Integer
            Dim iTopID As Integer
        
            'iDays = iDays + m_iDaysAppReserves
            
            If (m_iDaysAppReserves > 0) Then
                iInterval = p_iLevel * m_iDaysAppReserves
            ElseIf (m_iHoursAppReserves > 0) Then
                iInterval = p_iLevel * m_iHoursAppReserves
            Else
                iInterval = 0
            End If
            
            iLOBCode = 0
            iClmTypeCode = 0
            iLOBManagerID = 0
            
            iLOBManagerID = GetLOBManagerID(m_iLOBCode)
            
            sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"
            rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_READ_ONLY, 0)
            If Not DB_EOF(rs1) Then
                iTopID = vDB_GetData(rs1, "USER_ID")
            End If
            result = DB_CloseRecordset(rs1, DB_DROP)

            If (iLOBManagerID = 0) Then
                'Do Nothing
            ElseIf (iTopID = iLOBManagerID) Then
                'Send Diary/Mail to iLOBManagerID from p_sSubmittedTo
                p_sSubmittedBy = p_sSubmittedTo
                p_sSubmittedTo = iLOBManagerID
                sSenderLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedBy))
                sRecipientLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedTo))
            Else
                'Check if time lapsed for LOB Manager
                'If (DateAdd("d", CDbl(iDays), p_dSubmittedOn) < Now) Then
                If (AddIntervalToSubmitDate(p_dSubmittedOn, iInterval) < Now) Then
                    'Send Diary/Mail to iTopID from p_sSubmittedTo
                    p_sSubmittedBy = iLOBManagerID
                    p_sSubmittedTo = iTopID
                    sSenderLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedBy))
                    sRecipientLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedTo))
                Else
                    'Send Diary/Mail to iLOBManagerID from p_sSubmittedTo
                    p_sSubmittedBy = p_sSubmittedTo
                    p_sSubmittedTo = iLOBManagerID
                    sSenderLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedBy))
                    sRecipientLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedTo))
                End If
            End If
        End If
    Else
        'Send Diary/Mail to p_sSubmittedTo from p_sSubmittedBy
        sSenderLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedBy))
        sRecipientLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedTo))
    End If
    
    If (CLng(p_sSubmittedBy) <> CLng(p_sSubmittedTo)) Then
        If (sSenderLoginName <> "" And sRecipientLoginName <> "") Then
        
            Set objDOMDocument = New DOMDocument60
            objDOMDocument.async = False
            objDOMDocument.loadXML (p_sXMLDoc)
                            
            'Add by kuladeep Start
             If (Not (objDOMDocument.selectSingleNode("//control[@name='hdnPCName']") Is Nothing)) Then
                If (objDOMDocument.selectSingleNode("//control[@name='hdnPCName']").Text <> "") Then
                    sPCName = objDOMDocument.selectSingleNode("//control[@name='hdnPCName']").Text
                End If
            End If
            
            If (Not (objDOMDocument.selectSingleNode("//control[@name='hdnpcName']") Is Nothing)) Then
                If (objDOMDocument.selectSingleNode("//control[@name='hdnpcName']").Text <> "") Then
                    sPCName = objDOMDocument.selectSingleNode("//control[@name='hdnpcName']").Text
                End If
            End If
            
            If (objDOMDocument.selectSingleNode("//control[@name='hdnRSWType']").Text <> "") Then
                sRSWType = objDOMDocument.selectSingleNode("//control[@name='hdnRSWType']").Text
            End If
            
            
            If (Not (objDOMDocument.selectSingleNode("//control[@name='hdnClaimNumber']") Is Nothing)) Then
                If (objDOMDocument.selectSingleNode("//control[@name='hdnClaimNumber']").Text <> "") Then
                    sClaimNum = objDOMDocument.selectSingleNode("//control[@name='hdnClaimNumber']").Text
                End If
            End If
            
            If (Not (objDOMDocument.selectSingleNode("//control[@name='claimnum']") Is Nothing)) Then
                 If (objDOMDocument.selectSingleNode("//control[@name='claimnum']").Text <> "") Then
                    sClaimNum = objDOMDocument.selectSingleNode("//control[@name='claimnum']").Text
                End If
            End If
            'Add by kuladeep End
    
            sRegarding = sClaimNum + " * " + sPCName
            sAttachTable = "CLAIM"
            lAttachRecordID = CLng(m_lClaim_ID)
            sUserName = sGetUserName(sSenderLoginName) 'Get Sender First Name and Last Name
            sEntryName = "Approval request for Claim " + sClaimNum + " � " + sPCName + " - Time Lapsed for " + sUserName
    
            'Get the Last Diary Assigned User
            
            'shobhana  29/06/10 Start change location by kuladeep
            sDiaryNotes = GetEmailFormat("RSWSheet", m_lClaim_ID)
                 Dim sDiaryAdj As String
              sDiaryAdj = sDiaryNotes
               If sRSWType = 0 Then
              sRSWType = ""
              End If
              sDiaryAdj = Replace(sDiaryAdj, "{RSW_TYPE}", sAnyVarToString(sRSWType))
              sDiaryAdj = Replace(sDiaryAdj, "{CLAIM_NUMBER}", sClaimNum)
              sDiaryAdj = Replace(sDiaryAdj, "{USER_NAME}", sPCName)
              sDiaryAdj = Replace(sDiaryAdj, "{REASON}", "The time for Reserve Worksheet approval has lapsed for " & sGetUserName(sSenderLoginName))
              sDiaryAdj = Replace(sDiaryAdj, "{SUBMITTED_TO}", sAnyVarToString(sGetUserName(sRecipientLoginName)))
              sDiaryAdj = Replace(sDiaryAdj, "{SUBMITTED_BY}", sAnyVarToString(sGetUserName(sSenderLoginName)))
             
              
              sDiaryNotes = Replace(sDiaryNotes, "{RSW_TYPE}", sAnyVarToString(sRSWType))
              sDiaryNotes = Replace(sDiaryNotes, "{CLAIM_NUMBER}", sClaimNum)
              sDiaryNotes = Replace(sDiaryNotes, "{USER_NAME}", sPCName)
              sDiaryNotes = Replace(sDiaryNotes, "{REASON}", "The time for Reserve Worksheet approval has lapsed for " & sUserName)
              sDiaryNotes = Replace(sDiaryNotes, "{SUBMITTED_TO}", sAnyVarToString(sGetUserName(sRecipientLoginName)))
              sDiaryNotes = Replace(sDiaryNotes, "{SUBMITTED_BY}", sAnyVarToString(sGetUserName(sSenderLoginName)))
    
            'shobhana  29/06/10 End change location by kuladeep
    
            'Get the Last Diary Assigned User
            
        Select Case g_dbMake
            Case DBMS_IS_SQLSRVR
                sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY "
                'Change by kuladeep for mits:28665 Start
                'sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND (ENTRY_NOTES LIKE '%A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted for your approval" + "%'"
                sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + Trim(sDiaryNotes) + " %'"
                'Change by kuladeep for mits:28665 End
                sSQL = sSQL + " AND ATTACH_RECORDID = " & CLng(m_lClaim_ID)
                'smahajan6 - MITS 15584 : Start
                sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " & CStr(p_iRSWID) & ")"
                'smahajan6 - MITS 15584 : End
                'Add by kuladeep for mits:28665 Start
                sSQL = sSQL + " AND ASSIGNED_USER = '" + Trim(sRecipientLoginName) + "' AND ASSIGNING_USER = '" + Trim(sSenderLoginName) + "'"
                'Add by kuladeep for mits:28665 End
                sSQL = sSQL + " ORDER BY ENTRY_ID DESC"
            Case DBMS_IS_ORACLE
                sSQL = "SELECT ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY "
                'Change by kuladeep for mits:28665 Start
                'sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted for your approval" + "%'"
                sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + Trim(sDiaryNotes) + " %'"
                'Change by kuladeep for mits:28665 End
                sSQL = sSQL + " AND ATTACH_RECORDID = " & CLng(m_lClaim_ID)
                'smahajan6 - MITS 15584 : Start
                sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " & CStr(p_iRSWID) & ")"
                'smahajan6 - MITS 15584 : End
                'Add by kuladeep for mits:28665 Start
                sSQL = sSQL + " AND ASSIGNED_USER = '" + Trim(sRecipientLoginName) + "' AND ASSIGNING_USER = '" + Trim(sSenderLoginName) + "'"
                'Add by kuladeep for mits:28665 End
                sSQL = sSQL + " AND ROWNUM <= 1 ORDER BY ENTRY_ID DESC"
            Case Else
                sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY "
                'Change by kuladeep for mits:28665 Start
                'sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted for your approval" + "%'"
                 sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + Trim(sDiaryNotes) + " %'"
                'Change by kuladeep for mits:28665 End
                sSQL = sSQL + " AND ATTACH_RECORDID = " & CLng(m_lClaim_ID)
                'smahajan6 - MITS 15584 : Start
                sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " & CStr(p_iRSWID) & ")"
                'smahajan6 - MITS 15584 : End
                'Add by kuladeep for mits:28665 Start
                sSQL = sSQL + " AND ASSIGNED_USER = '" + Trim(sRecipientLoginName) + "' AND ASSIGNING_USER = '" + Trim(sSenderLoginName) + "'"
                'Add by kuladeep for mits:28665 End
                sSQL = sSQL + " ORDER BY ENTRY_ID DESC"
        End Select
            
            rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
            
            If Not DB_EOF(rs1) Then
                sDiaryAssigningUser = vDB_GetData(rs1, "ASSIGNING_USER") & ""
                sDiaryAssignedUser = vDB_GetData(rs1, "ASSIGNED_USER") & ""
            Else
                sDiaryAssigningUser = ""
                sDiaryAssignedUser = ""
            End If
            result = DB_CloseRecordset(rs1, DB_DROP)
                'shobhana  29/06/10
                
              'sDiaryNotes = GetEmailFormat("RSWSheet", m_lClaim_ID)
                 'Dim sDiaryAdj As String
              'sDiaryAdj = sDiaryNotes
               'If sRSWType = 0 Then
              'sRSWType = ""
              'End If
              'sDiaryAdj = Replace(sDiaryAdj, "{RSW_TYPE}", sAnyVarToString(sRSWType))
              'sDiaryAdj = Replace(sDiaryAdj, "{CLAIM_NUMBER}", sClaimNum)
              'sDiaryAdj = Replace(sDiaryAdj, "{USER_NAME}", sPCName)
              'sDiaryAdj = Replace(sDiaryAdj, "{REASON}", "The time for Reserve Worksheet approval has lapsed for " & sGetUserName(sSenderLoginName))
              'sDiaryAdj = Replace(sDiaryAdj, "{SUBMITTED_TO}", sAnyVarToString(sGetUserName(sRecipientLoginName)))
              'sDiaryAdj = Replace(sDiaryAdj, "{SUBMITTED_BY}", sAnyVarToString(sGetUserName(sSenderLoginName)))
             
              
              'sDiaryNotes = Replace(sDiaryNotes, "{RSW_TYPE}", sAnyVarToString(sRSWType))
              'sDiaryNotes = Replace(sDiaryNotes, "{CLAIM_NUMBER}", sClaimNum)
              'sDiaryNotes = Replace(sDiaryNotes, "{USER_NAME}", sPCName)
              'sDiaryNotes = Replace(sDiaryNotes, "{REASON}", "The time for Reserve Worksheet approval has lapsed for " & sUserName)
              'sDiaryNotes = Replace(sDiaryNotes, "{SUBMITTED_TO}", sAnyVarToString(sGetUserName(sRecipientLoginName)))
              'sDiaryNotes = Replace(sDiaryNotes, "{SUBMITTED_BY}", sAnyVarToString(sGetUserName(sSenderLoginName)))
             
             'End  Shobhana
                
           ' sDiaryNotes = "A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted for your approval." & vbCrLf
           ' sDiaryNotes = sDiaryNotes & vbCrLf & "Reason: The time for Reserve Worksheet approval has lapsed for " & sUserName & ". " & vbCrLf
           ' sDiaryNotes = sDiaryNotes & vbCrLf & "Thank you," & vbCrLf & vbCrLf & "RISKMASTER X"
        
            sFromEmail = sGetEmailByLoginName(sSenderLoginName)
            sToEmail = sGetEmailByLoginName(sRecipientLoginName)
        
            If (sDiaryAssigningUser <> sSenderLoginName Or sDiaryAssignedUser <> sRecipientLoginName) Then
                'rsushilaggar MITS 19624 30-Mar-2010'
                    If (m_bDisableDiaryNotifyForSuperVsr <> True) Then
                        Call GenerateDiary(sSenderLoginName, sRecipientLoginName, sRegarding, sAttachTable, lAttachRecordID, sEntryName, sDiaryNotes)
                    End If
                    If (Trim(sFromEmail) <> "" And Trim(sToEmail) <> "" And m_bDisableEmailNotifyForSuperVsr <> True) Then
                  
                        Call bSendEmail(sUserName, sFromEmail, sToEmail, sEntryName, sDiaryNotes)
                    End If
                'End If comment by kuladeep for mits:28665
                
                'Sending Diary and Email to Adjuster
                iAdjID = iGetAdjusterEID(m_lClaim_ID)
                If ((iAdjID > 0) And (CInt(p_sSubmittedTo) <> iAdjID)) Then
                    Dim sAdjLoginName As String
                    Dim sAdjEmail As String
                 
                    
                    sAdjLoginName = sGetUser(db, objUser.DSNID, CLng(iAdjID))
                    sAdjEmail = sGetEmailByLoginName(sAdjLoginName)
                    
                    
' sDiaryAdj = "A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted to " + sGetUserName(sRecipientLoginName) + " for approval by " + sGetUserName(sSenderLoginName) & vbCrLf
' sDiaryAdj = sDiaryAdj & vbCrLf & "Please enter the RISKMASTER system and navigate to the Claim in order to see this reserve worksheet" & vbCrLf
' sDiaryAdj = sDiaryAdj & vbCrLf & "Reason: The time for Reserve Worksheet approval has lapsed for " & sGetUserName(sSenderLoginName) & ". " & vbCrLf
' sDiaryAdj = sDiaryAdj & vbCrLf & "Thank you," & vbCrLf & vbCrLf & "RISKMASTER X"
                    
                    'rsushilaggar MITS 19624 30-Mar-2010'
                        If (m_bDisableDiaryNotifyForSuperVsr <> True) Then
                            Call GenerateDiary(sSenderLoginName, sAdjLoginName, sRegarding, sAttachTable, lAttachRecordID, sEntryName, sDiaryAdj)
                        End If
                        If (Trim(sFromEmail) <> "" And Trim(sAdjEmail) <> "" And m_bDisableEmailNotifyForSuperVsr <> True) Then
                      
                            Call bSendEmail(sUserName, sFromEmail, sAdjEmail, sEntryName, sDiaryAdj)
                        End If
                    
             End If
            
          End If 'Add/change location by kuladeep for mits:28665
            
        End If
    End If

   If db <> 0 Then DB_CloseDatabase db
   
   Exit Function

ERROR_TRAP_StepWiseUpdateRSWForUser:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\StepWiseUpdateRSWForUser") = IDRETRY Then Resume
  
    subHourGlass False
    Exit Function
Resume

End Function

Function AddIntervalToSubmitDate(p_dSubmittedOn As String, p_iInterval As Integer) As String
    Dim sFinalSubmitDate As String
    
    If (p_iInterval > 0) Then
        If (m_iDaysAppReserves > 0) Then
         sFinalSubmitDate = DateAdd("d", CDbl(p_iInterval), p_dSubmittedOn)
        ElseIf (m_iHoursAppReserves > 0) Then
         sFinalSubmitDate = DateAdd("h", CDbl(p_iInterval), p_dSubmittedOn)
        Else
        sFinalSubmitDate = p_dSubmittedOn
        End If
    Else
        If (m_iDaysAppReserves > 0) Then
         sFinalSubmitDate = DateAdd("d", CDbl(m_iDaysAppReserves), p_dSubmittedOn)
        ElseIf (m_iHoursAppReserves > 0) Then
         sFinalSubmitDate = DateAdd("h", CDbl(m_iHoursAppReserves), p_dSubmittedOn)
        Else
        sFinalSubmitDate = p_dSubmittedOn
        End If
    End If
    AddIntervalToSubmitDate = sFinalSubmitDate
    
End Function
 

Sub GetResCategories(p_iLOBCode As Integer, p_iClmTypeCode As Integer)

On Error GoTo ERROR_TRAP_GetResCategories

Dim sSQL As String

Dim bResByClaimType As Boolean
bResByClaimType = False

Dim iCnt As Integer
Dim rs1 As Integer
Dim result As Integer

            sSQL = "SELECT RES_BY_CLM_TYPE FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + CStr(p_iLOBCode)
            rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
            If Not DB_EOF(rs1) Then
                bResByClaimType = CBool(vDB_GetData(rs1, "RES_BY_CLM_TYPE"))
            End If
            result = DB_CloseRecordset(rs1, DB_DROP)
            
            'Get reserve buckets based on claim type
            sSQL = "SELECT Count(*) as cnt"
            If (bResByClaimType) Then
                sSQL = sSQL + " FROM SYS_CLM_TYPE_RES, CODES_TEXT "
                sSQL = sSQL + " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND "
                sSQL = sSQL + " LINE_OF_BUS_CODE = " + CStr(p_iLOBCode)
                sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + CStr(p_iClmTypeCode)
            Else 'Get all reserve buckets that are available
                sSQL = sSQL + " FROM SYS_LOB_RESERVES, CODES_TEXT "
                sSQL = sSQL + " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND "
                sSQL = sSQL + " LINE_OF_BUS_CODE = " + CStr(p_iLOBCode)
            End If
            
            rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
            If Not DB_EOF(rs1) Then
                m_iArrSize = CInt(vDB_GetData(rs1, "cnt"))
            End If
            result = DB_CloseRecordset(rs1, DB_DROP)
            
            'resizing array
            ReDim m_arrstructReserves(m_iArrSize)
            
            'Get reserve buckets based on claim type
            sSQL = "SELECT RESERVE_TYPE_CODE, CODE_DESC"
            If (bResByClaimType) Then
                sSQL = sSQL + " FROM SYS_CLM_TYPE_RES, CODES_TEXT "
                sSQL = sSQL + " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND "
                sSQL = sSQL + " LINE_OF_BUS_CODE = " + CStr(p_iLOBCode)
                sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + CStr(p_iClmTypeCode)
                sSQL = sSQL + " ORDER BY CODE_DESC "
            Else 'Get all reserve buckets that are available
                sSQL = sSQL + " FROM SYS_LOB_RESERVES, CODES_TEXT "
                sSQL = sSQL + " WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND "
                sSQL = sSQL + " LINE_OF_BUS_CODE = " + CStr(p_iLOBCode)
                sSQL = sSQL + " ORDER BY CODE_DESC "
            End If
            rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
            If Not DB_EOF(rs1) Then
                'Populate the values in Reserves Struct
                iCnt = 0
                While Not DB_EOF(rs1)
                    m_arrstructReserves(iCnt).iReserveTypeCode = vDB_GetData(rs1, "RESERVE_TYPE_CODE")
                    m_arrstructReserves(iCnt).sReserveDesc = vDB_GetData(rs1, "CODE_DESC") & ""
                    iCnt = iCnt + 1
                DB_MoveNext (rs1)
                Wend
            End If
            DB_CloseRecordset rs1, DB_DROP
            
            Exit Sub
            
ERROR_TRAP_GetResCategories:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\GetResCategories") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub

Sub GetNewReserveAmts(p_iLOBCode As Integer, p_sXMLDoc As String)

On Error GoTo ERROR_TRAP_GetNewReserveAmts
    
Dim iCnt As Integer

Dim iReserveTypeCode As Long 'MITS 28292 changed by bsharma33
iReserveTypeCode = 0

Dim sReserveDesc As String
sReserveDesc = ""

Dim sGPName As String
sGPName = ""

Dim sLOB As String
sLOB = ""

Dim sCtrlMName As String
sCtrlMName = ""

Dim objDOMDocument As MSXML2.DOMDocument60

    sLOB = sGetShortCode(CLng(p_iLOBCode))
    
    ReDim m_arrstructResFinal(m_iArrSize)
    
    Set objDOMDocument = New DOMDocument60
    objDOMDocument.async = False
    objDOMDocument.loadXML (p_sXMLDoc)

    For iCnt = 0 To (m_iArrSize - 1)
        iReserveTypeCode = m_arrstructReserves(iCnt).iReserveTypeCode
        sReserveDesc = Trim(m_arrstructReserves(iCnt).sReserveDesc)
        sGPName = "//group[@name='" + sReserveDesc + sLOB + "']"
            
        If (objDOMDocument.selectNodes(sGPName).length > 0) Then
            If (objDOMDocument.selectSingleNode(sGPName).hasChildNodes) Then
                sCtrlMName = Mid(sReserveDesc, 1, 3) + sLOB
                
                m_arrstructResFinal(iCnt).iReserveTypeCode = CStr(iReserveTypeCode)
                
                If (objDOMDocument.selectSingleNode("//control[@name='BalAmt" + sCtrlMName + "']").Text <> "") Then
                    m_arrstructResFinal(iCnt).dBalAmount = objDOMDocument.selectSingleNode("//control[@name='BalAmt" + sCtrlMName + "']").Text
                End If
                
                If (objDOMDocument.selectSingleNode("//control[@name='NewBalAmt" + sCtrlMName + "']").Text <> "") Then
                    m_arrstructResFinal(iCnt).dNewBalAmount = objDOMDocument.selectSingleNode("//control[@name='NewBalAmt" + sCtrlMName + "']").Text
                End If
                
                If (objDOMDocument.selectSingleNode("//control[@name='hdnNewRes" + sCtrlMName + "']").Text <> "") Then
                    m_arrstructResFinal(iCnt).dNewReserves = objDOMDocument.selectSingleNode("//control[@name='hdnNewRes" + sCtrlMName + "']").Text
                End If
            
            End If
        End If
    Next

    Exit Sub

ERROR_TRAP_GetNewReserveAmts:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\GetNewReserveAmts") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub

Function GetLOBManagerID(p_iLOB As Integer) As Integer

On Error GoTo ERROR_TRAP_GetLOBManagerID

Dim sSQL As String
sSQL = ""

Dim lMaxAmount As Long
lMaxAmount = 0

Dim iApproverID As Integer
iApproverID = 0

Dim rs1 As Integer
Dim result As Integer
Dim iCount As Integer

        sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + CStr(p_iLOB)
        rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
        If Not DB_EOF(rs1) Then
            iApproverID = iAnyVarToInt(vDB_GetData(rs1, "USER_ID"))
            lMaxAmount = lAnyVarToLong(vDB_GetData(rs1, "RESERVE_MAX"))
                
            For iCount = 0 To (m_iArrSize - 1)
                If (lMaxAmount < m_arrstructResFinal(iCount).dNewReserves) Then
                    iApproverID = 0
                End If
            Next
                
            If (iApproverID > 0) Then
                GetLOBManagerID = iApproverID
                Exit Function
            End If
        End If
        result = DB_CloseRecordset(rs1, DB_DROP)

        'Get Top level user
        sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"
        rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
        If Not DB_EOF(rs1) Then
            iApproverID = vDB_GetData(rs1, "USER_ID")
            GetLOBManagerID = iApproverID
            result = DB_CloseRecordset(rs1, DB_DROP)
            Exit Function
        End If
        result = DB_CloseRecordset(rs1, DB_DROP)

    GetLOBManagerID = iApproverID
    Exit Function

ERROR_TRAP_GetLOBManagerID:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\GetLOBManagerID") = IDRETRY Then Resume
    subHourGlass False
    Exit Function
Resume

End Function

Function iGetAdjusterEID(p_lClaimId As Long) As Integer

On Error GoTo ERROR_TRAP_iGetAdjusterEID

Dim sSQL As String
sSQL = ""

Dim lCurrentAdjusterID As Long
lCurrentAdjusterID = 0

Dim iRMUserID As Integer
iRMUserID = 0

Dim rs1 As Integer
Dim result As Integer

    sSQL = "SELECT ADJUSTER_EID FROM CLAIM_ADJUSTER WHERE CLAIM_ID = " + CStr(p_lClaimId) + " AND CURRENT_ADJ_FLAG <> 0"
    rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
    
    If Not DB_EOF(rs1) Then
        lCurrentAdjusterID = lAnyVarToLong(vDB_GetData(rs1, "ADJUSTER_EID"))
    End If
    result = DB_CloseRecordset(rs1, DB_DROP)
    
    If (lCurrentAdjusterID > 0) Then
        sSQL = "SELECT RM_USER_ID FROM ENTITY WHERE ENTITY_ID = " + CStr(lCurrentAdjusterID)
        rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
        
        If Not DB_EOF(rs1) Then
            iRMUserID = iAnyVarToInt(vDB_GetData(rs1, "RM_USER_ID"))
            iGetAdjusterEID = iRMUserID
        End If
        result = DB_CloseRecordset(rs1, DB_DROP)
    End If
    
    Exit Function
    
ERROR_TRAP_iGetAdjusterEID:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\iGetAdjusterEID") = IDRETRY Then Resume
    subHourGlass False
    Exit Function

End Function


Sub JumpUpdateRSWForUser(p_iRSWID As Integer, p_sSubmittedBy As String, p_sSubmittedTo As String, p_dSubmittedOn As String, p_iLevel As Integer, p_sXMLDoc As String)

On Error GoTo ERROR_TRAP_JumpUpdateRSWForUser

Dim sDS As String * 255
Dim sDSN As String
Dim db As Integer

Dim rs1 As Integer
Dim result As Integer

Dim iInterval As Integer
iInterval = 1

Dim sSQL As String
sSQL = ""

Dim sSupervisor As String
sSupervisor = "0"

Dim iTopID As Integer
Dim iLOBManagerID As Integer

Dim objDOMDocument As MSXML2.DOMDocument60

Dim sRegarding As String
Dim sPCName As String
Dim sClaimNum As String
Dim sRSWType As String
Dim sSenderLoginName As String
Dim sRecipientLoginName As String
Dim sFromEmail As String
Dim sToEmail As String
Dim sAttachTable As String
Dim lAttachRecordID As Long
Dim sEntryName As String
Dim sDiaryAssigningUser As String
Dim sDiaryAssignedUser As String
Dim sUserName As String
Dim sDiaryNotes As String

Dim iAdjID As Integer

    sDS = objLogin.SecurityDSN 'Security database DSN
    sDSN = sTrimNull(sDS)
    db = DB_OpenDatabase(hEnv, sDSN, 0)

    'iDays = p_iLevel * m_iDaysAppReserves
    If (m_iDaysAppReserves > 0) Then
        iInterval = p_iLevel * m_iDaysAppReserves
    ElseIf (m_iHoursAppReserves > 0) Then
        iInterval = p_iLevel * m_iHoursAppReserves
    Else
        iInterval = 0
    End If
    
    'If worsksheet has lapsed for the user, find its supervisor who lies within the reserve limits
    'If (DateAdd("d", CDbl(iDays), p_dSubmittedOn) < Now) Then
     If (AddIntervalToSubmitDate(p_dSubmittedOn, iInterval) < Now) Then
        'Supervisor within reserve limits
        iLOBManagerID = GetLOBManagerID(m_iLOBCode)
        
        sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"
        rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_READ_ONLY, 0)
        If Not DB_EOF(rs1) Then
            iTopID = vDB_GetData(rs1, "USER_ID")
        End If
        result = DB_CloseRecordset(rs1, DB_DROP)
        
        sSupervisor = CStr(lGetManagerID(CLng(p_sSubmittedTo)))
        sSupervisor = GetApprovalID(p_sXMLDoc, CInt(sSupervisor), m_lClaim_ID, CInt(p_sSubmittedBy), iGetGroupID(CInt(sSupervisor)))
        
        If (CInt(sSupervisor) = CInt(p_sSubmittedTo)) Then
            If ((CInt(p_sSubmittedTo) = iLOBManagerID) And (CInt(p_sSubmittedTo) <> iTopID)) Then
                p_sSubmittedTo = CStr(iTopID)
                p_sSubmittedBy = CStr(iLOBManagerID)
                sSenderLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedBy))
                sRecipientLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedTo))
            ElseIf ((CInt(p_sSubmittedTo) = iLOBManagerID) And (CInt(p_sSubmittedTo) = iTopID)) Then
                Exit Sub
            End If
        Else
            If (sSupervisor <> "0") Then
                If db <> 0 Then DB_CloseDatabase db
                'p_dSubmittedOn = DateAdd("d", CDbl(m_iDaysAppReserves), p_dSubmittedOn)
                p_dSubmittedOn = AddIntervalToSubmitDate(p_dSubmittedOn, 0)
                p_sSubmittedBy = p_sSubmittedTo
                Call JumpUpdateRSWForUser(p_iRSWID, p_sSubmittedBy, sSupervisor, p_dSubmittedOn, p_iLevel, p_sXMLDoc)
            End If
        End If
    Else 'worksheet has not lapsed for this user: assign it to this user
        'Send Diary/Mail to p_sSubmittedTo from p_sSubmittedBy
        sSenderLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedBy))
        sRecipientLoginName = sGetUser(db, objUser.DSNID, CLng(p_sSubmittedTo))
    End If
    
    If (CLng(p_sSubmittedBy) <> CLng(p_sSubmittedTo)) Then
        If (sSenderLoginName <> "" And sRecipientLoginName <> "") Then
    
            Set objDOMDocument = New DOMDocument60
            objDOMDocument.async = False
            objDOMDocument.loadXML (p_sXMLDoc)
                                
            If (objDOMDocument.selectSingleNode("//control[@name='hdnPCName']").Text <> "") Then
                sPCName = objDOMDocument.selectSingleNode("//control[@name='hdnPCName']").Text
            End If
            
            If (objDOMDocument.selectSingleNode("//control[@name='hdnRSWType']").Text <> "") Then
                sRSWType = objDOMDocument.selectSingleNode("//control[@name='hdnRSWType']").Text
            End If
            
            If (objDOMDocument.selectSingleNode("//control[@name='hdnClaimNumber']").Text <> "") Then
                sClaimNum = objDOMDocument.selectSingleNode("//control[@name='hdnClaimNumber']").Text
            End If
        
            sRegarding = sClaimNum + " * " + sPCName
            sAttachTable = "CLAIM"
            lAttachRecordID = CLng(m_lClaim_ID)
            sUserName = sGetUserName(sSenderLoginName) 'Get Sender First Name and Last Name
            sEntryName = "Approval request for Claim " + sClaimNum + " � " + sPCName + " - Time Lapsed for " + sUserName
        
            
             'shobhana 29/06/10 change location by kuladeep
                Dim sDiaryAdj As String
                  sDiaryNotes = GetEmailFormat("RSWSheet", m_lClaim_ID)
              sDiaryAdj = sDiaryNotes
              
              If sRSWType = 0 Then
              sRSWType = ""
              End If
              
              sDiaryAdj = Replace(sDiaryAdj, "{RSW_TYPE}", sAnyVarToString(sRSWType))
              sDiaryAdj = Replace(sDiaryAdj, "{CLAIM_NUMBER}", sClaimNum)
              sDiaryAdj = Replace(sDiaryAdj, "{USER_NAME}", sPCName)
              sDiaryAdj = Replace(sDiaryAdj, "{REASON}", "The time for Reserve Worksheet approval has lapsed for  & sGetUserName(sSenderLoginName) & ")
              sDiaryAdj = Replace(sDiaryAdj, "{SUBMITTED_TO}", sGetUserName(sRecipientLoginName))
              sDiaryAdj = Replace(sDiaryAdj, "{SUBMITTED_BY}", sGetUserName(sSenderLoginName))
             
              
              sDiaryNotes = Replace(sDiaryNotes, "{RSW_TYPE}", sAnyVarToString(sRSWType))
              sDiaryNotes = Replace(sDiaryNotes, "{CLAIM_NUMBER}", sClaimNum)
              sDiaryNotes = Replace(sDiaryNotes, "{USER_NAME}", sPCName)
              sDiaryNotes = Replace(sDiaryNotes, "{REASON}", "The time for Reserve Worksheet approval has lapsed for & sUserName & ")
              sDiaryNotes = Replace(sDiaryNotes, "{SUBMITTED_TO}", sAnyVarToString(sGetUserName(sRecipientLoginName)))
              sDiaryNotes = Replace(sDiaryNotes, "{SUBMITTED_BY}", sAnyVarToString(sGetUserName(sSenderLoginName)))
             
              
             'end shobhana
        
            'Get the Last Diary Assigned User
            
            Select Case g_dbMake
            Case DBMS_IS_SQLSRVR
                sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY "
                'Change by kuladeep for mits:28665 Start
                'sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted for your approval" + "%'"
                sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + Trim(sDiaryNotes) + " %'"
                'Change by kuladeep for mits:28665 End
                sSQL = sSQL + " AND ATTACH_RECORDID = " & CLng(m_lClaim_ID)
                'smahajan6 - MITS 15584 : Start
                sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " & CStr(p_iRSWID) & ")"
                'smahajan6 - MITS 15584 : End
                'Add by kuladeep for mits:28665 Start
                sSQL = sSQL + " AND ASSIGNED_USER = '" + Trim(sRecipientLoginName) + "' AND ASSIGNING_USER = '" + Trim(sSenderLoginName) + "'"
                'Add by kuladeep for mits:28665 End
                sSQL = sSQL + " ORDER BY ENTRY_ID DESC"
            Case DBMS_IS_ORACLE
                sSQL = "SELECT ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY "
                 'Change by kuladeep for mits:28665 Start
                'sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted for your approval" + "%'"
                sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + Trim(sDiaryNotes) + " %'"
                'Change by kuladeep for mits:28665 End
                sSQL = sSQL + " AND ATTACH_RECORDID = " & CLng(m_lClaim_ID)
                'smahajan6 - MITS 15584 : Start
                sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " & CStr(p_iRSWID) & ")"
                'smahajan6 - MITS 15584 : End
                'Add by kuladeep for mits:28665 Start
                sSQL = sSQL + " AND ASSIGNED_USER = '" + Trim(sRecipientLoginName) + "' AND ASSIGNING_USER = '" + Trim(sSenderLoginName) + "'"
                'Add by kuladeep for mits:28665 End
                sSQL = sSQL + " AND ROWNUM <= 1 ORDER BY ENTRY_ID DESC"
            Case Else
                sSQL = "SELECT TOP 1 ASSIGNING_USER,ASSIGNED_USER FROM WPA_DIARY_ENTRY "
                 'Change by kuladeep for mits:28665 Start
                'sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted for your approval" + "%'"
                sSQL = sSQL + " WHERE ATTACH_TABLE = 'CLAIM' AND ENTRY_NOTES LIKE '%" + Trim(sDiaryNotes) + " %'"
                'Change by kuladeep for mits:28665 End
                sSQL = sSQL + " AND ATTACH_RECORDID = " & CLng(m_lClaim_ID)
                'smahajan6 - MITS 15584 : Start
                sSQL = sSQL + " AND CREATE_DATE > (SELECT DTTM_RCD_LAST_UPD FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " & CStr(p_iRSWID) & ")"
                'smahajan6 - MITS 15584 : End
                'Add by kuladeep for mits:28665 Start
                sSQL = sSQL + " AND ASSIGNED_USER = '" + Trim(sRecipientLoginName) + "' AND ASSIGNING_USER = '" + Trim(sSenderLoginName) + "'"
                'Add by kuladeep for mits:28665 End
                sSQL = sSQL + " ORDER BY ENTRY_ID DESC"
            End Select
            
            rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
            
            If Not DB_EOF(rs1) Then
                sDiaryAssigningUser = vDB_GetData(rs1, "ASSIGNING_USER") & ""
                sDiaryAssignedUser = vDB_GetData(rs1, "ASSIGNED_USER") & ""
            Else
                sDiaryAssigningUser = ""
                sDiaryAssignedUser = ""
            End If
            result = DB_CloseRecordset(rs1, DB_DROP)
           ' sDiaryNotes = "A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted for your approval." & vbCrLf
          '  sDiaryNotes = sDiaryNotes & vbCrLf & "Reason: The time for Reserve Worksheet approval has lapsed for " & sUserName & ". " & vbCrLf
          '  sDiaryNotes = sDiaryNotes & vbCrLf & "Thank you," & vbCrLf & "RISKMASTER X"
        
            sFromEmail = sGetEmailByLoginName(sSenderLoginName)
            sToEmail = sGetEmailByLoginName(sRecipientLoginName)
        
            If (sDiaryAssigningUser <> sSenderLoginName Or sDiaryAssignedUser <> sRecipientLoginName) Then
                'rsushilaggar MITS 19624 30-Mar-2010'
                    If (m_bDisableDiaryNotifyForSuperVsr <> True) Then
                        Call GenerateDiary(sSenderLoginName, sRecipientLoginName, sRegarding, sAttachTable, lAttachRecordID, sEntryName, sDiaryNotes)
                    End If
                    If (Trim(sFromEmail) <> "" And Trim(sToEmail) <> "" And m_bDisableEmailNotifyForSuperVsr <> True) Then
                        Call bSendEmail(sUserName, sFromEmail, sToEmail, sEntryName, sDiaryNotes)
                    End If
                'End If Comment by kuladeep for mits:28665
                
                'Sending Diary and Email to Adjuster
                iAdjID = iGetAdjusterEID(m_lClaim_ID)
                If ((iAdjID > 0) And (CInt(p_sSubmittedTo) <> iAdjID)) Then
                    Dim sAdjLoginName As String
                    Dim sAdjEmail As String
                    
                    
                    sAdjLoginName = sGetUser(db, objUser.DSNID, CLng(iAdjID))
                    sAdjEmail = sGetEmailByLoginName(sAdjLoginName)
                    
                 '   sDiaryAdj = "A " + sRSWType + " for Claim " + sClaimNum + " � " + sPCName + " has been submitted to " + sGetUserName(sRecipientLoginName) + " for approval by " + sGetUserName(sSenderLoginName) & vbCrLf
                  '  sDiaryAdj = sDiaryAdj & vbCrLf & "Please enter the RISKMASTER system and navigate to the Claim in order to see this reserve worksheet" & vbCrLf
                  '  sDiaryAdj = sDiaryAdj & vbCrLf & "Reason: The time for Reserve Worksheet approval has lapsed for " & sGetUserName(sSenderLoginName) & ". " & vbCrLf
                 '   sDiaryAdj = sDiaryAdj & vbCrLf & "Thank you," & vbCrLf & vbCrLf & "RISKMASTER X"
                    
                    'rsushilaggar MITS 19624 30-Mar-2010'
                        If (m_bDisableDiaryNotifyForSuperVsr <> True) Then
                            Call GenerateDiary(sSenderLoginName, sAdjLoginName, sRegarding, sAttachTable, lAttachRecordID, sEntryName, sDiaryAdj)
                        End If
                        If (Trim(sFromEmail) <> "" And Trim(sAdjEmail) <> "" And m_bDisableEmailNotifyForSuperVsr <> True) Then
                            Call bSendEmail(sUserName, sFromEmail, sAdjEmail, sEntryName, sDiaryAdj)
                        End If
            End If
          End If 'Add/Change location by kuladeep for mits:28665
        End If
    End If

    If db <> 0 Then DB_CloseDatabase db
    Exit Sub

ERROR_TRAP_JumpUpdateRSWForUser:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\JumpUpdateRSWForUser") = IDRETRY Then Resume
    subHourGlass False
    Exit Sub
Resume

End Sub

Function GetApprovalID(p_sXMLDoc As String, p_iManagerID As Integer, p_lClaimId As Long, p_iUserID As Integer, p_iGroupId As Integer)

On Error GoTo ERROR_TRAP_GetApprovalID

Dim sSQL As String
sSQL = ""

Dim iThisManagerID As Integer
iThisManagerID = 0

Dim iApproverID  As Integer
iApproverID = 0

Dim bUnderLimit As Boolean
bUnderLimit = False

    iApproverID = p_iManagerID
    iThisManagerID = p_iManagerID
    
    If (iThisManagerID = 0) Then
        'No Supervisor Assigned
        iApproverID = GetLOBManagerID(m_iLOBCode)
    Else
        bUnderLimit = UnderUserLimit(p_sXMLDoc, iThisManagerID, p_iGroupId, p_lClaimId)

        If (bUnderLimit) Then
            iApproverID = iThisManagerID
        Else
            'Need to check next manager

            iApproverID = iThisManagerID
            iThisManagerID = lGetManagerID(CLng(iThisManagerID))

            'No Supervisor Assigned
            'If there is no manager assigned
            If (iThisManagerID = 0) Then
                iThisManagerID = GetLOBManagerID(m_iLOBCode)
            Else
                'Not to notify immediate manager
                If Not (m_bNotifySupReserves) Then
                    iThisManagerID = GetApprovalID(p_sXMLDoc, iThisManagerID, p_lClaimId, p_iUserID, p_iGroupId)
                End If
            End If
            iApproverID = iThisManagerID
        End If
    End If
    
    GetApprovalID = iApproverID

    Exit Function

ERROR_TRAP_GetApprovalID:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\GetApprovalID") = IDRETRY Then Resume
    subHourGlass False
    Exit Function
Resume

End Function

Function UnderUserLimit(p_sXMLDoc As String, p_iUserID As Integer, p_iGroupId As Integer, p_lClaimId As Long) As Boolean

On Error GoTo ERROR_TRAP_UnderUserLimit

Dim sSQL As String
sSQL = ""

Dim iCnt As Integer
iCnt = 0

Dim bUnderUserLimit As Boolean
bUnderUserLimit = False

    For iCnt = 0 To (m_iArrSize - 1)
        'Get Reserve Limits and Validate If Reserves fall within that limit
        Dim iCount As Integer
        For iCount = 0 To (m_iArrSize - 1)
            'Query to find reserve limit for all Reserves
            If (m_arrstructResFinal(iCount).iReserveTypeCode = m_arrstructReserves(iCnt).iReserveTypeCode) Then
                If (m_arrstructResFinal(iCount).dNewReserves > 0) Then
                    bUnderUserLimit = CheckReserveLimits(p_lClaimId, m_arrstructReserves(iCnt).iReserveTypeCode, m_arrstructResFinal(iCount).dNewReserves, p_iUserID, p_iGroupId)
                    If (bUnderUserLimit) Then
                        UnderUserLimit = False
                        Exit Function
                    End If
                End If
            End If
        Next
    Next
    UnderUserLimit = True
    Exit Function

ERROR_TRAP_UnderUserLimit:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\UnderUserLimit") = IDRETRY Then Resume
    subHourGlass False
    Exit Function
Resume

End Function

Function CheckReserveLimits(p_lClaimId As Long, p_iReserveTypeCode As Long, p_dAmount As Double, p_iUserID As Integer, p_iGroupId As Integer)
'MITS 28292 bsharma33 changed p_iReserveTypeCode As Integer to p_iReserveTypeCode As Double in parameters above
On Error GoTo ERROR_TRAP_CheckReserveLimits

Dim dMaxAmount As Double
dMaxAmount = 0

Dim sSQL As String
sSQL = ""

Dim bReturnValue As Boolean
bReturnValue = True

Dim rs1 As Integer

    sSQL = "SELECT MAX(MAX_AMOUNT)  MAX_AMT FROM RESERVE_LIMITS,CLAIM "
    sSQL = sSQL + " WHERE (USER_ID = " + CStr(p_iUserID)
    If (p_iGroupId <> 0) Then
        sSQL = sSQL + " OR GROUP_ID = " + CStr(p_iGroupId)
    End If
    sSQL = sSQL + " ) AND CLAIM.CLAIM_ID = " + CStr(p_lClaimId)
    sSQL = sSQL + " AND CLAIM.LINE_OF_BUS_CODE = RESERVE_LIMITS.LINE_OF_BUS_CODE"
    sSQL = sSQL + " AND RESERVE_TYPE_CODE = " + CStr(p_iReserveTypeCode)
    
    rs1 = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
    
    If Not DB_EOF(rs1) Then
        If (IsNull(vDB_GetData(rs1, "MAX_AMT"))) Then
            bReturnValue = False
        End If
            
        If (bReturnValue) Then
            dMaxAmount = vDB_GetData(rs1, "MAX_AMT")
                                
            If (p_dAmount > dMaxAmount) Then
                bReturnValue = True
            Else
                bReturnValue = False
            End If
        Else
            bReturnValue = False
        End If

        If gProcessStatus = PS_STOP Then
            If CheckStop() Then
                DB_CloseRecordset rs1, DB_DROP
                subHourGlass False
                Exit Function
            End If
        End If
    End If
    DB_CloseRecordset rs1, DB_DROP
    
    CheckReserveLimits = bReturnValue

    Exit Function

ERROR_TRAP_CheckReserveLimits:
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\CheckReserveLimits") = IDRETRY Then Resume
    subHourGlass False
    Exit Function
Resume

End Function

Function FileText(ByVal FileName As String) As String
    Dim handle As Integer
    Dim sAppPath As String
    Dim sTextFormat As String

 
       ' ensure that the file exists
    If Len(Dir$(FileName)) = 0 Then
    
        Err.Raise 53  ' File not found
    End If
     
       ' open in binary mode
    handle = FreeFile
    Open FileName$ For Binary As #handle
       ' read the string and close the file
    FileText = Space$(LOF(handle))
    Get #handle, , FileText
    Close #handle
End Function

Function GetEmailFormat(p_sModuleName As String, p_iClaimID As Long) As String

 
  Dim sSQL As String
  Dim rs As Integer
  Dim sAppPath As String
  Dim sTextFormat As String

 Dim strDate As String
 Dim X As Variant
 Dim i As Long
 Dim sTempPath As String

 ' sAppPath = Replace(App.Path, "legacybin", "appfiles\")
  sAppPath = App.Path
 X = Split(sAppPath, "\")
 For i = 0 To UBound(X)
sTempPath = X(i)
 If i = UBound(X) Then
   sAppPath = Replace(App.Path, sTempPath, "appfiles\")
   End If
 Next i
  sAppPath = sAppPath & p_sModuleName & "\"
  
  If (p_sModuleName = "RSWSheet") Then
 
  sAppPath = sAppPath & "RSWSUB_EMAIL.txt"
  
 
  sTextFormat = FileText(sAppPath)

 

  
  If p_iClaimID > 0 Then
  
    sSQL = "SELECT ENTITY.LAST_NAME  LN,ENTITY.FIRST_NAME  FN,CLAIM_NUMBER,CLAIM.EVENT_NUMBER  EN,CLAIM.FILE_NUMBER  FNO,CLAIM.DATE_OF_CLAIM  DOC,CLAIM.TIME_OF_CLAIM  TOC,"
    sSQL = sSQL + " CLAIM.SERVICE_CODE SC ,EVENT.COUNTY_OF_INJURY  COJ,EVENT.ADDR1  ADD1,EVENT.ADDR2  ADD2,EVENT.CITY  CITY,EVENT.LOCATION_AREA_DESC  LAD,"
    sSQL = sSQL + " EVENT.DATE_OF_EVENT   DOE ,EVENT.TIME_OF_EVENT  TOE,EVENT.DATE_REPORTED   DR,"
    sSQL = sSQL + " EVENT.INJURY_FROM_DATE   IFD,EVENT.INJURY_TO_DATE  ITD,"
    sSQL = sSQL + " LOB.CODE_DESC  LIOFB,CT.CODE_DESC  CLAIMTYPE,ET.CODE_DESC  EVENTTYPE"
    sSQL = sSQL + " from CLAIM inner join EVENT on claim.event_id=event.event_id"
    sSQL = sSQL + " INNER JOIN CLAIMANT on CLAIM.CLAIM_ID=CLAIMANT.CLAIM_ID"
    sSQL = sSQL + " inner join ENTITY on CLAIMANT.CLAIMANT_EID=ENTITY.ENTITY_ID"
    sSQL = sSQL + " INNER JOIN CODES_TEXT  LOB ON LOB.CODE_ID=CLAIM.LINE_OF_BUS_CODE"
    sSQL = sSQL + " left outer join CODES_TEXT   CT on CLAIM.CLAIM_TYPE_CODE =CT.CODE_ID"
    sSQL = sSQL + " left outer join CODES_TEXT  ET on EVENT.EVENT_TYPE_CODE=ET.CODE_ID where CLAIM.CLAIM_ID=" & p_iClaimID
    
    rs = DB_CreateRecordset(dbGlobalConnect2, sSQL, DB_FORWARD_ONLY, 0)
    If Not DB_EOF(rs) Then
      'LOB = CStr(vDB_GetData(rs, "LIOFB"))
      Dim sAdd As String
      Dim sClaimantName As String
     
      
      
      sAdd = sAnyVarToString(vDB_GetData(rs, "ADD1")) & "   " & sAnyVarToString(vDB_GetData(rs, "ADD2")) & "   " & sAnyVarToString(vDB_GetData(rs, "CITY"))
     
      sClaimantName = sAnyVarToString(vDB_GetData(rs, "LN")) & "   " & sAnyVarToString(vDB_GetData(rs, "FN"))
      
      
    
      
    
      sTextFormat = Replace(sTextFormat, "{CLAIMANT_NAME}", sClaimantName)
      sTextFormat = Replace(sTextFormat, "{CLAIM_NUMBER}", sAnyVarToString(vDB_GetData(rs, "CLAIM_NUMBER")))
      sTextFormat = Replace(sTextFormat, "{EVENT_NUMBER}", sAnyVarToString(vDB_GetData(rs, "EN")))
      sTextFormat = Replace(sTextFormat, "{DATE_OF_CLAIM}", sAnyVarToString(vDB_GetData(rs, "DOC")))
      sTextFormat = Replace(sTextFormat, "{TIME_OF_CLAIM}", sAnyVarToString(vDB_GetData(rs, "TOC")))
      sTextFormat = Replace(sTextFormat, "{FILE_NO}", sAnyVarToString(vDB_GetData(rs, "FNO")))
      sTextFormat = Replace(sTextFormat, "{SERVICE_CODE}", (vDB_GetData(rs, "SC")))
      sTextFormat = Replace(sTextFormat, "{COUNTY_OF_INJURY}", sAnyVarToString(vDB_GetData(rs, "COJ")))
      sTextFormat = Replace(sTextFormat, "{ADDRESS}", sAdd)
      sTextFormat = Replace(sTextFormat, "{DATE_OF_EVENT}", sAnyVarToString(vDB_GetData(rs, "DOE")))
      sTextFormat = Replace(sTextFormat, "{TIME_OF_EVENT}", sAnyVarToString(vDB_GetData(rs, "TOE")))
      sTextFormat = Replace(sTextFormat, "{DATE_REPORTED}", sAnyVarToString(vDB_GetData(rs, "DR")))
      sTextFormat = Replace(sTextFormat, "{INJURY_FROM_DATE}", sAnyVarToString(vDB_GetData(rs, "IFD")))
      sTextFormat = Replace(sTextFormat, "{INJURY_TO_DATE}", sAnyVarToString(vDB_GetData(rs, "ITD")))
      sTextFormat = Replace(sTextFormat, "{EVENT_LOCATION}", sAnyVarToString(vDB_GetData(rs, "LAD")))
      sTextFormat = Replace(sTextFormat, "{LINE_OF_BUSINESS}", sAnyVarToString(vDB_GetData(rs, "LIOFB")))
      sTextFormat = Replace(sTextFormat, "{CLAIM_TYPE}", sAnyVarToString(vDB_GetData(rs, "CLAIMTYPE")))
      sTextFormat = Replace(sTextFormat, "{EVENT_TYPE}", sAnyVarToString(vDB_GetData(rs, "EVENTTYPE")))
 
         
         
      
        DB_CloseRecordset rs, DB_DROP
     Else
        DB_CloseRecordset rs, DB_DROP
     End If
   End If

End If
  GetEmailFormat = sTextFormat
Exit Function

hError:
    Dim EResult As Integer
   
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\iGetGroupID") = IDRETRY Then Resume
   subHourGlass False
    
End Function

Function iGetGroupID(p_iUserID As Integer) As Integer

  Dim iGroupID As Integer
  Dim SQL As String
  Dim rs As Integer
    
  iGetGroupID = 0

  If p_iUserID > 0 Then
     rs = DB_CreateRecordset(dbLookup, "SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " & CStr(p_iUserID), DB_FORWARD_ONLY, 0)
     If Not DB_EOF(rs) Then
        iGroupID = iAnyVarToInt(vDB_GetData(rs, 1))
        DB_CloseRecordset rs, DB_DROP
     Else
        DB_CloseRecordset rs, DB_DROP
     End If
  End If
  iGetGroupID = iGroupID
  
Exit Function

hError:
    Dim EResult As Integer
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\iGetGroupID") = IDRETRY Then Resume
    subHourGlass False
    
End Function
'Add by hsingh61-for mits:22561
'Function to insert record into table WPA_EMAIL_ENTRY
Public Function EmailEntry(ByVal sCtlNumber As String, ByVal sSubject As String, ByVal sSenderLoginName As String, ByVal sRecipientLoginName As String) As Boolean
      
    Dim bSendEmail As Boolean
    Dim sSQL As String
    
    On Error GoTo hError
    
    bSendEmail = False
    
    sSQL = "INSERT INTO WPA_EMAIL_ENTRY (ENTRY_ID, ATTACH_REC_ID, MAIL_REGARDING, SENDER_ID, RECEIVER_ID, SENDING_DATE)"
    sSQL = sSQL & "VALUES(" & lGetNextUID("WPA_EMAIL_ENTRY") & ", '" & sCtlNumber & "', '" & sSubject & "', '" & sSenderLoginName & "', '" & sRecipientLoginName & "','" & Format$(Now, "YYYYMMDD") & "') "
    DB_SQLExecute dbLookup, sSQL
    bSendEmail = True
    
    Exit Function

hError:
    Dim EResult As Integer
    If iGeneralErrorExt(Err, Error$, "RSW_PAY_TLDIARY.BAS\EmailEntry") = IDRETRY Then Resume
    subHourGlass False
       
End Function
