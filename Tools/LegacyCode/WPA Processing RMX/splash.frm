VERSION 5.00
Begin VB.Form frmSplash 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3735
   ClientLeft      =   1755
   ClientTop       =   2130
   ClientWidth     =   5265
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "splash.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3735
   ScaleWidth      =   5265
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Auto Diary Processing "
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   36
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1755
      Index           =   1
      Left            =   1350
      TabIndex        =   1
      Top             =   960
      Width           =   3795
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblNotice 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "RISKMASTER/World � Risk Management System Version 3.0  Copyright � 1997 DORN Technology Group, Inc."""
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   390
      Left            =   330
      TabIndex        =   2
      Top             =   3270
      Width           =   4605
      WordWrap        =   -1  'True
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "The Leader in Risk Management Solutions"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   15
      TabIndex        =   0
      Top             =   2925
      Width           =   5265
   End
   Begin VB.Image imgSplash 
      Appearance      =   0  'Flat
      Height          =   3750
      Left            =   0
      Picture         =   "splash.frx":000C
      Top             =   0
      Width           =   5250
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'*****************************************************************
'*
'*  SPLASH.FRM
'*
'*  <Enter Form Description Here>
'*
'*  Date Written:             By:
'*
'*  Revisions:
'*
'*  Date          Who     Description
'*  --------      ---     ------------------------------------
'*
'*****************************************************************
Option Explicit

Private Sub Form_Activate()

    subHourGlass False

End Sub

Private Sub Form_Deactivate()

    Unload Me

End Sub

Private Sub Form_Load()
    On Local Error Resume Next

  '  imgSplash.Picture = LoadPicture(sGetSystemPath() & gsSplashPath)
   ' lblNotice.Caption = gsProductCaption & " � Risk Management System Version 3.0 " & Chr$(13) & Chr$(10) & "Copyright � 1997 DORN Technology Group, Inc."
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
Set frmSplash = Nothing
End Sub


