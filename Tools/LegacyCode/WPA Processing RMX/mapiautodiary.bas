Attribute VB_Name = "MAPIVB"

Option Explicit

'****************************************************************************'
'                                                                            '
' Visual Basic declaration for the MAPI functions.                           '
'                                                                            '                                                                           '
'****************************************************************************'


'***************************************************
'   MAPI Message holds information about a message
'***************************************************

Global WPAMapiSession As Long
Const ERR_WPA_MAPI_NOT_INIT = 107

Public Type MAPIMessage
    Reserved As Long
    Subject As String
    NoteText As String
    MessageType As String
    DateReceived As String
    ConversationID As String
    flags As Long
    RecipCount As Long
    FileCount As Long
End Type

'***************************************************
'   DTGMAPISUM Message holds the summary  information about a message
'  ( We greatly simplyfied the summary structure,because this is only a walkaround wchen 3/18/97 )
'***************************************************
'Type DTGMAPISUM
'  MsgReference As String
'  Subject As String
'End Type
Type MsgReference
  length As Integer
  CharArray(0 To 24) As Integer
End Type

'************************************************
'   MAPIRecip holds information about a message
'   originator or recipient
'************************************************
Public Type MapiRecip
    Reserved As Long
    RecipClass As Long
    Name As String
    Address As String
    EIDSize As Long
    EntryID As String
End Type

'******************************************************
'   MapiFile holds information about file attachments
'******************************************************
Public Type MapiFile
    Reserved As Long
    flags As Long
    Position As Long
    PathName As String
    FileName As String
    FileType As String
End Type

'***************************
'   FUNCTION Declarations
'***************************
Declare Function MAPILogon Lib "MAPI32.DLL" (ByVal UIParam&, ByVal User$, ByVal Password$, ByVal flags&, ByVal Reserved&, Session&) As Long
Declare Function MAPILogoff Lib "MAPI32.DLL" (ByVal Session&, ByVal UIParam&, ByVal flags&, ByVal Reserved&) As Long
Declare Function BMAPIReadMail Lib "MAPI32.DLL" (lMsg&, nRecipients&, nFiles&, ByVal Session&, ByVal UIParam&, MessageID$, ByVal Flag&, ByVal Reserved&) As Long
Declare Function BMAPIGetReadMail Lib "MAPI32.DLL" (ByVal lMsg&, Message As MAPIMessage, Recip() As MapiRecip, File() As MapiFile, Originator As MapiRecip) As Long
Declare Function MAPIFindNext Lib "MAPI32.DLL" Alias "BMAPIFindNext" (ByVal Session&, ByVal UIParam&, MsgType$, SeedMsgID$, ByVal Flag&, ByVal Reserved&, MsgID$) As Long
Declare Function MAPIDeleteMail Lib "MAPI32.DLL" (ByVal Session&, ByVal UIParam&, ByVal MsgID$, ByVal flags&, ByVal Reserved&) As Long
Declare Function MAPISendMail Lib "MAPI32.DLL" Alias "BMAPISendMail" (ByVal Session&, ByVal UIParam&, Message As MAPIMessage, Recipient() As MapiRecip, File() As MapiFile, ByVal flags&, ByVal Reserved&) As Long
Declare Function BMAPIAddress Lib "MAPI32.DLL" (lInfo&, ByVal Session&, ByVal UIParam&, Caption$, ByVal nEditFields&, label$, nRecipients&, Recip() As MapiRecip, ByVal flags&, ByVal Reserved&) As Long
Declare Function BMAPIGetAddress Lib "MAPI32.DLL" (ByVal lInfo&, ByVal nRecipients&, Recipients() As MapiRecip) As Long
Declare Function MAPIResolveName Lib "MAPI32.DLL" Alias "BMAPIResolveName" (ByVal Session&, ByVal UIParam&, ByVal UserName$, ByVal flags&, ByVal Reserved&, Recipient As MapiRecip) As Long

'**************************
'   CONSTANT Declarations
'**************************
'CMC Constant
 Global Const LIST_ALL = 0
 Global Const LIST_UNREAD = 1
 Global Const LIST_COUNT = 4

'*********88

'MAPI Constant

Global Const SUCCESS_SUCCESS = 0
'Global Const MAPI_USER_ABORT = 1
'Global Const MAPI_E_FAILURE = 2
'Global Const MAPI_E_LOGIN_FAILURE = 3
'Global Const MAPI_E_DISK_FULL = 4
'Global Const MAPI_E_INSUFFICIENT_MEMORY = 5
'Global Const MAPI_E_BLK_TOO_SMALL = 6
'Global Const MAPI_E_TOO_MANY_SESSIONS = 8
'Global Const MAPI_E_TOO_MANY_FILES = 9
'Global Const MAPI_E_TOO_MANY_RECIPIENTS = 10
'Global Const MAPI_E_ATTACHMENT_NOT_FOUND = 11
'Global Const MAPI_E_ATTACHMENT_OPEN_FAILURE = 12
'Global Const MAPI_E_ATTACHMENT_WRITE_FAILURE = 13
'Global Const MAPI_E_UNKNOWN_RECIPIENT = 14
'Global Const MAPI_E_BAD_RECIPTYPE = 15
Global Const MAPI_E_NO_MESSAGES = 16
'Global Const MAPI_E_INVALID_MESSAGE = 17
'Global Const MAPI_E_TEXT_TOO_LARGE = 18
'Global Const MAPI_E_INVALID_SESSION = 19
'Global Const MAPI_E_TYPE_NOT_SUPPORTED = 20
'Global Const MAPI_E_AMBIGUOUS_RECIPIENT = 21

'Global Const MAPI_ORIG = 0
Global Const MAPI_TO = 1
Global Const MAPI_CC = 2
'Global Const MAPI_BCC = 3


'***********************
'   FLAG Declarations
'***********************

Global Const MAPI_LOGON_UI = &H1
Global Const MAPI_NEW_SESSION = &H2
Global Const MAPI_DIALOG = &H8
Global Const MAPI_UNREAD_ONLY = &H20
Global Const MAPI_ENVELOPE_ONLY = &H40
Global Const MAPI_PEEK = &H80
Global Const MAPI_GUARANTEE_FIFO = &H100
Global Const MAPI_BODY_AS_FILE = &H200
Global Const MAPI_AB_NOMODIFY = &H400
Global Const MAPI_SUPPRESS_ATTACH = &H800
Global Const MAPI_FORCE_DOWNLOAD = &H1000

'Global Const MAPI_OLE = &H1
'Global Const MAPI_OLE_STATIC = &H2


'*****************************************
' Global variables used by MapiDemo only
'*****************************************

Dim MsgID$
'Dim Recips() As MapiRecip, Files() As MapiFile


'*********************************************
' Define global message, recipient and file
' structures for use within the view form
'*********************************************

Dim M As MAPIMessage
Dim Mo As MapiRecip
Dim Mr() As MapiRecip
Dim Mf() As MapiFile

Dim ref() As MsgReference    'CMC reference

Dim toR()  As String
Dim ccR()  As String

Global Const RMWIN_SUBJECT_PREFIX = "[RM/win]"
Global Const RMWIN_REF_PREFIX = "RMREF"
'Global Const RMS_EM_SIZE = 64                   ' Max size of users e-mail address






Function bMailIsRMWINMsg(sSubject, bIsReferenced, sTable, lRecID, lRecID2, lfrmCode, lFrmParentCode, lSecurity, sCaption) As Integer
On Error GoTo ET_bMailIsRMWINMsg

    Dim sTempSubject As String
    Dim iPos As Integer
    'Dim iPos2 As Integer
    Dim sTmp  As String


    If Left$(Trim(sSubject), 8) = RMWIN_SUBJECT_PREFIX Then   ' show only if RM win message
       bMailIsRMWINMsg = True
       bIsReferenced = False

       Rem strictly parse out reference header
       sTempSubject = Mid$(Trim(sSubject), 9)
       If sTempSubject <> "" Then
          sTempSubject = Trim$(sTempSubject)
          If Left$(sTempSubject, 7) = ("[" & RMWIN_REF_PREFIX & ":") Then
             sTempSubject = Mid$(sTempSubject, 8)
             sTempSubject = Trim$(sTempSubject)
             
             iPos = InStr(sTempSubject, ":")
             If iPos > 0 Then
                 sTable = Left$(sTempSubject, iPos - 1)
                 sTempSubject = Mid$(sTempSubject, Len(sTable) + 2)
                 sTempSubject = Trim$(sTempSubject)
                   
                 iPos = InStr(sTempSubject, ":")
                 If iPos > 0 Then
                     sTmp = Left$(sTempSubject, iPos - 1)
                     sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                     sTempSubject = Trim$(sTempSubject)
                     lRecID = Val(sTmp)
             
                     iPos = InStr(sTempSubject, ":")
                     If iPos > 0 Then
                         sTmp = Left$(sTempSubject, iPos - 1)
                         sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                         sTempSubject = Trim$(sTempSubject)
                         lRecID2 = Val(sTmp)
                 
                         iPos = InStr(sTempSubject, ":")
                         If iPos > 0 Then
                             sTmp = Left$(sTempSubject, iPos - 1)
                             sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                             sTempSubject = Trim$(sTempSubject)
                             lfrmCode = Val(sTmp)
                             
                             iPos = InStr(sTempSubject, ":")
                             If iPos > 0 Then
                                 sTmp = Left$(sTempSubject, iPos - 1)
                                 sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                                 sTempSubject = Trim$(sTempSubject)
                                 lFrmParentCode = Val(sTmp)
                 
                                 iPos = InStr(sTempSubject, ":")
                                 If iPos > 0 Then
                                     sTmp = Left$(sTempSubject, iPos - 1)
                                     sTempSubject = Mid$(sTempSubject, Len(sTmp) + 2)
                                     sTempSubject = Trim$(sTempSubject)
                                     lSecurity = Val(sTmp)
                 
                                     iPos = InStr(sTempSubject, "]" & Chr(9))
                                     If iPos > 0 Then
                                         sCaption = Left$(sTempSubject, iPos - 1)
                                         sTempSubject = "Re:" & sCaption
                                         bIsReferenced = True
                                     End If
                                 End If
                            End If
                         End If
                     End If
                 End If
             End If
          End If
       End If
       sSubject = Trim$(sTempSubject)   ' return trimmed subject
    Else
       bMailIsRMWINMsg = False
    End If

    Exit Function

ET_bMailIsRMWINMsg:
     bMailIsRMWINMsg = False
     Exit Function
End Function

Function CopyFiles(MfIn As MapiFile, MfOut As MapiFile) As Long

    MfOut.FileName = MfIn.FileName
    MfOut.PathName = MfIn.PathName
    MfOut.Reserved = MfIn.Reserved
    MfOut.flags = MfIn.flags
    MfOut.Position = MfIn.Position
    MfOut.FileType = MfIn.FileType
    CopyFiles = 1&

End Function

Function CopyRecipient(MrIn As MapiRecip, MrOut As MapiRecip) As Long

    MrOut.Name = MrIn.Name
    MrOut.Address = MrIn.Address
    MrOut.EIDSize = MrIn.EIDSize
    MrOut.EntryID = MrIn.EntryID
    MrOut.Reserved = MrIn.Reserved
    MrOut.RecipClass = MrIn.RecipClass

    CopyRecipient = 1&

End Function

Function MAPIReadMail(Session As Long, MessageID As String, flags As Long, Reserved As Long, Message As MAPIMessage, orig As MapiRecip, RecipsOut() As MapiRecip, FilesOut() As MapiFile) As Long

    Dim Info&
    Dim nFiles&
    Dim nRecips&
    Dim rc&
    Dim i As Integer
    Dim ignore As Long

    rc& = BMAPIReadMail(Info&, nRecips, nFiles, Session, 0, MessageID, flags, Reserved)

    If (rc& = SUCCESS_SUCCESS) Then

        'Message is now read into the handles array.  We have to redim the arrays and read
        'the stuff in

        If (nRecips = 0) Then nRecips = 1
        If (nFiles = 0) Then nFiles = 1

        ReDim Recips(0 To nRecips - 1) As MapiRecip
        ReDim Files(0 To nFiles - 1) As MapiFile

        rc = BMAPIGetReadMail(Info&, Message, Recips(), Files(), orig)
        If rc = SUCCESS_SUCCESS Then

           '*******************************************
           ' Copy Recipient and File structures from
           ' Local structures to those passed as
           ' parameters
           '*******************************************

           ReDim FilesOut(0 To nFiles - 1) As MapiFile
           ReDim RecipsOut(0 To nRecips - 1) As MapiRecip

           For i = 0 To nRecips - 1
               ignore = CopyRecipient(Recips(i), RecipsOut(i))
           Next i

           For i = 0 To nFiles - 1
               ignore = CopyFiles(Files(i), FilesOut(i))
           Next i
        Else
           'Call subDisplayMsg(90, "")
        End If
    Else
      'Call subDisplayMsg(90, "")
    End If

    MAPIReadMail = rc

End Function

Function sGetEmailByLoginName(sUser As String) As String
On Error GoTo ET_sGetEmailByLoginName

    Dim result As Integer
    Dim sTmp As String * 255
    Dim sEmailAddr As String
    Dim nTemp As Integer
    Dim bFound As Integer
    Dim lTemp As Long
    Dim db As Integer
    Dim rs As Integer
    Dim sSQL As String
    Dim sDSN As String

    'result = GetSecurityDSN(sTmp)
    sTmp = objLogin.SecurityDSN
    sDSN = sTrimNull(sTmp)

    Rem Connect to security DB
    db = DB_OpenDatabase(hEnv, sDSN, 0)

    Rem Query
    sSQL = "SELECT DISTINCT EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID =USER_TABLE.USER_ID AND  USER_DETAILS_TABLE.LOGIN_NAME = '" & sUser & "'"
    rs = DB_CreateRecordset(db, sSQL, DB_FORWARD_ONLY, 0)
    If Not DB_EOF(rs) Then
       sEmailAddr = vDB_GetData(rs, 1) & ""
       bFound = True
    Else
       sEmailAddr = ""
       bFound = False
    End If

    Rem Shut down
    result = DB_CloseRecordset(rs, DB_DROP)
    result = DB_CloseDatabase(db)
    
    Rem return email address
    sGetEmailByLoginName = sEmailAddr
    Exit Function

ET_sGetEmailByLoginName:
    Dim EResult As Integer
    EResult = iGeneralErrorExt(Err, Error$, "sGetEmailByLoginName")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            result = DB_CloseRecordset(rs, DB_DROP)
            result = DB_CloseDatabase(db)
            sGetEmailByLoginName = ""
            Exit Function
    End Select
End Function

Function sMailConvertToFaxAddress(sName As String, sNumber As String) As String
' This function turns a name and fax number into a valid fax
'  gateway address. Currently, this routine is hard-coded
'  to convert to Microsoft At Work fax format (included with
'  Microsoft Windows for Workgroups 3.11 and Windows 95).
'  This could be rewritten to be configurable in the future.

On Error GoTo ET_sMailConvertToFaxAddress


    sMailConvertToFaxAddress = "[fax:" & Trim$(sName) & "@" & Trim$(sNumber) & "]"

    Exit Function

ET_sMailConvertToFaxAddress:

     sMailConvertToFaxAddress = ""
  Exit Function


End Function

Function sMailGetEmailName(lUserID As Long) As String

On Error GoTo ET_sMailGetEmailName

    Dim result As Integer
    Dim sTmp As String * 255
    Dim sEmailAddr As String
    Dim nTemp As Integer
    Dim bFound As Integer
    Dim lTemp As Long
    Dim db As Integer
    Dim rs As Integer
    Dim sSQL As String
    Dim sDSN As String

    'result = GetSecurityDSN(sTmp)
    sTmp = objLogin.SecurityDSN
    sDSN = sTrimNull(sTmp)

    Rem Connect to security DB
    db = DB_OpenDatabase(hEnv, sDSN, 0)

    Rem Query
    sSQL = "SELECT EMAIL_ADDR FROM USER_TABLE WHERE USER_ID = " & lUserID
    rs = DB_CreateRecordset(db, sSQL, DB_FORWARD_ONLY, 0)
    If Not DB_EOF(rs) Then
       sEmailAddr = vDB_GetData(rs, 1) & ""
       bFound = True
    Else
       sEmailAddr = ""
       bFound = False
    End If

    Rem Shut down
    result = DB_CloseRecordset(rs, DB_DROP)
    result = DB_CloseDatabase(db)
    
    Rem return email address
    sMailGetEmailName = sEmailAddr
    Exit Function

ET_sMailGetEmailName:

     sMailGetEmailName = ""
  Exit Function

End Function

Function StringToken(iPos%, str1$, delim$) As String

  Dim iPos2 As Integer

'*******************************************************
'   Returns a string from string position "iPos" upto
'   a delimeter character
'*******************************************************

    If (Len(str1$) <> 0) Then

        iPos2% = iPos%

        iPos2% = InStr(iPos%, str1$, delim$) 'Find next delimeter
        If (iPos2% = 0) Then
            StringToken$ = Right$(str1$, Len(str1) - iPos% + 1)
            iPos% = 0
            Exit Function
        Else
            StringToken$ = Mid$(str1$, iPos%, iPos2% - iPos%)
        End If

        iPos% = iPos2% + 1

    Else

        iPos% = 0

    End If

End Function


Function lWPAInitMAPI() As Long
'//Technical Info-----------------------------------------------------------------------------------------------------------
'
'//Description
'
' Logs into MAPI. Session will be visible until subWPAUninitMAPI is called.
'
'//Arguments
'
'//Returns
' 0, if call succeeded
' ERR_WPA_MAPI_NOT_INIT, if the MAPILogon call failed for whatever reason
'
'--------------------------------------------------------------------------------------------------------------------------
On Error GoTo ET_lWPAInitMAPI

  Dim lError As Long


  If WPAMapiSession <> 0 Then
     Rem already logged in
     lWPAInitMAPI = 0  ' success
     Exit Function
  End If
  ' Changed by Denis Basaric 11/11/1997 No CMC anymore
  'lError = DTGMAPILogon(0&, "", "", MAPI_LOGON_UI, 0&, WPAMapiSession)
  lError = MAPILogon(0&, "", "", MAPI_LOGON_UI, 0&, WPAMapiSession)
  If (lError <> SUCCESS_SUCCESS) Then
     lWPAInitMAPI = ERR_WPA_MAPI_NOT_INIT
     WPAMapiSession = 0
     Exit Function
  End If

  lWPAInitMAPI = 0  ' success
  Exit Function

ET_lWPAInitMAPI:
    Dim EResult As Integer
    lWPAInitMAPI = ERR_WPA_MAPI_NOT_INIT
    EResult = iGeneralErrorExt(Err, Error$, "lWPAInitMAPI")
    Select Case EResult
        Case vbRetry
            Resume
        Case Else
            Exit Function
    End Select

End Function

