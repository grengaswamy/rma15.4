Attribute VB_Name = "LOSTWORK"
Option Explicit

Sub CalcLostDays()

    Dim rs As Integer, rs2 As Integer
    Dim sSQL As String
   ' Dim result As Integer
    Dim sDate1 As String, sDate2 As String
    Dim lDays As Long

    Rem Recalc work loss
    ' rs = DB_CreateRecordset(dbGlobalConnect2, "SELECT * FROM PI_X_WORK_LOSS WHERE DATE_RETURNED IS NULL OR DATE_RETURNED = ''", DB_FORWARD_ONLY, 0)
    rs = DB_CreateRecordset(dbGlobalConnect2, "SELECT * FROM PI_X_WORK_LOSS WHERE DATE_RETURNED IS NULL OR DATE_RETURNED = ''", DB_SNAPSHOT, 0)
    If Not DB_EOF(rs) Then
        DB_MoveLast rs
        DB_MoveFirst rs
    End If
    While Not DB_EOF(rs)
        If Not IsNull(vDB_GetData(rs, "DATE_LAST_WORKED")) Then
            sSQL = "SELECT WORK_SUN_FLAG,WORK_MON_FLAG,WORK_TUE_FLAG,WORK_WED_FLAG,WORK_THU_FLAG,WORK_FRI_FLAG,WORK_SAT_FLAG"
            sSQL = sSQL & " FROM PERSON_INVOLVED WHERE PI_ROW_ID = " & vDB_GetData(rs, "PI_ROW_ID")
            rs2 = DB_CreateRecordset(dbLookup, sSQL, DB_FORWARD_ONLY, 0)
            If Not DB_EOF(rs2) Then
                 DB_CloseRecordset rs2, DB_CLOSE
            
                sDate1 = Trim$(vDB_GetData(rs, "DATE_LAST_WORKED") & "")
                If sDate1 <> "" Then
                    sDate1 = Mid$(sDate1, 5, 2) & "/" & Right$(sDate1, 2) & "/" & Left$(sDate1, 4)
                    If IsDate(sDate1) Then
                        sDate2 = Trim$(vDB_GetData(rs, "DATE_RETURNED") & "")
                        If sDate2 <> "" Then
                            sDate2 = Mid$(sDate2, 5, 2) & "/" & Right$(sDate2, 2) & "/" & Left$(sDate2, 4)
                        End If
            
                        If IsDate(sDate2) And sDate2 <> "" Then
                            lDays = iCountEmpDays(rs2, DateAdd("d", 1, sDate1), DateAdd("d", -1, sDate2))
                        Else
                            lDays = iCountEmpDays(rs2, DateAdd("d", 1, sDate1), DateAdd("d", -1, Date))
                        End If
            
                        If lDays <> lAnyVarToLong(vDB_GetData(rs, "DURATION")) Then
                            sSQL = "UPDATE PI_X_WORK_LOSS SET DURATION = " & lDays & " WHERE PI_WL_ROW_ID = " & lAnyVarToLong(vDB_GetData(rs, "PI_WL_ROW_ID"))
                            DB_SQLExecute dbCreate, sSQL
                            DoEvents
                        End If
                    End If
                End If
            End If
            DB_CloseRecordset rs2, DB_DROP
    
        End If
    
        DB_MoveNext rs
    Wend

DB_CloseRecordset rs, DB_DROP

End Sub

Function iCountEmpDays(rs As Integer, sDate1 As String, sDate2 As String) As Integer

    ReDim iDaysOfWeek(1 To 7) As Integer

    iCountEmpDays = 0
    If IsDate(sDate1) Then
        iDaysOfWeek(1) = iAnyVarToInt(vDB_GetData(rs, "WORK_SUN_FLAG"))
        iDaysOfWeek(2) = iAnyVarToInt(vDB_GetData(rs, "WORK_MON_FLAG"))
        iDaysOfWeek(3) = iAnyVarToInt(vDB_GetData(rs, "WORK_TUE_FLAG"))
        iDaysOfWeek(4) = iAnyVarToInt(vDB_GetData(rs, "WORK_WED_FLAG"))
        iDaysOfWeek(5) = iAnyVarToInt(vDB_GetData(rs, "WORK_THU_FLAG"))
        iDaysOfWeek(6) = iAnyVarToInt(vDB_GetData(rs, "WORK_FRI_FLAG"))
        iDaysOfWeek(7) = iAnyVarToInt(vDB_GetData(rs, "WORK_SAT_FLAG"))
        If IsDate(sDate2) Then
            iCountEmpDays = iGetDayCount(sDate1, sDate2, iDaysOfWeek())
        Else
            iCountEmpDays = iGetDayCount(sDate1, Date, iDaysOfWeek())
        End If
    End If

End Function

Function iGetDayCount(sDate1 As String, sDate2 As String, iDaysOfWeek() As Integer) As Integer

On Error GoTo ERROR_TRAP_iGetDayCount

    Dim iDays As Integer
    Dim iNewdays As Integer
    Dim iDayOfWeekStart As Integer
    Dim iDayOfWeekEnd As Integer
    Dim i As Integer

    If IsDate(sDate1) And IsDate(sDate2) Then
        iDays = DateDiff("d", sDate1, sDate2) + 1
        iDayOfWeekStart = Weekday(sDate1)
        iDayOfWeekEnd = Weekday(sDate2)
        iNewdays = 0
        For i = 1 To iDays
            If iDaysOfWeek(Weekday(DateAdd("d", i - 1, sDate1))) Then iNewdays = iNewdays + 1
        Next i
    End If
    iGetDayCount = iNewdays

    Exit Function

ERROR_TRAP_iGetDayCount:
    iGetDayCount = 0
    Exit Function

End Function

