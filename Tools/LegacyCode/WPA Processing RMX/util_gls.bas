Attribute VB_Name = "UTIL_GLS"
'*****************************************************************
'*
'*  UTIL_GLS.BAS
'*
'*  This module will handle all calls and/or routines that have to
'*  do with the GLOSSARY Table in the riskmaster database.
'*
'*  Date Written:             By: David J. Page
'*
'*  Revisions:
'*
'*  Date          Who     Description
'*  --------      ---     ------------------------------------
'*
'*****************************************************************
Option Explicit

Global g_sProgramErrorString As String

Function bInitGlossary(bCheckSyncFlag As Integer, bLogin As Integer) As Integer

On Error GoTo ERROR_TRAP_bInitGlossary

    Static Ignore As Integer
    Dim bDoCache As Integer
    Static sLastRefresh As String

    bInitGlossary = True
    bDoCache = bLogin  ' flags whether caching should occur on this call or not

    ' if sync checking is desired, check cache option settings to see what to do
    If Not Ignore And bCheckSyncFlag And Not bLogin Then bDoCache = bIsCacheNeeded("GLOSSARY", sLastRefresh, Ignore)

    ' cache it in if desired
    If bDoCache Then
        If (bInitGloss(DB_GetHdbc(dbLookup), True) <> 0) Then
            sLastRefresh = Format$(Now, "YYYYMMDDHHNNSS")
        Else
            bInitGlossary = False
        End If
    End If

    Exit Function

ERROR_TRAP_bInitGlossary:
    If iGeneralErrorExt(Err, Error$, "UTIL_GLS.BAS\bInitGlossary") = IDRETRY Then Resume
    bInitGlossary = False
    Exit Function

End Function

Function bIsCacheNeeded(sTableName As String, sDateTime As String, bIgnore As Integer) As Integer

On Error GoTo ERROR_TRAP_bIsCacheNeeded

    Dim sNow As String
    Dim sGlossaryDTTM As String
    Dim result As Integer
    Dim dsGlossary As Integer
    Dim bWarning As Integer
    Dim sTmp As String
    Dim bNeeded As Integer
    
    bNeeded = True
    sNow = Format(Date & " " & Time, "YYYYMMDDHHNNSS")
    dsGlossary = DB_CreateRecordset(dbLookup, "SELECT DTTM_LAST_UPDATE FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & sTableName & "' ", DB_FORWARD_ONLY, 0)
    If Not DB_EOF(dsGlossary) Then
        sGlossaryDTTM = vDB_GetData(dsGlossary, "DTTM_LAST_UPDATE") & ""
        If (sDateTime >= sGlossaryDTTM) Or (Trim$(sDateTime) = "") Then bNeeded = False
        If sGlossaryDTTM > sNow Then
            result = DB_Edit(dsGlossary)
            result = DB_PutData(dsGlossary, "DTTM_LAST_UPDATE", sNow)
            result = DB_Update(dsGlossary)
        End If
    End If
    result = DB_CloseRecordset(dsGlossary, DB_DROP)

    If bNeeded Then
        bWarning = True
        If bRegDBGetValue("Options\Caching", sTmp) Then
            Rem caching enabled - see if we should do it always or at startup
            result = bRegDBGetValue("Options\Caching\" & sTableName & "Allways", sTmp)
            bNeeded = (Val(sTmp) <> 0)
            Rem see if a warning box should be displayed
            result = bRegDBGetValue("Options\Caching\" & sTableName & "Warning", sTmp)
            bWarning = Val(sTmp)
        End If
    End If

    bIsCacheNeeded = bNeeded

    Exit Function

ERROR_TRAP_bIsCacheNeeded:
    If iGeneralErrorExt(Err, Error$, "UTIL_GLS.BAS\bIsCacheNeeded") = IDRETRY Then Resume Else bIsCacheNeeded = False
    Exit Function

End Function

Function lGetNextUID(sTableName As String) As Long

    Dim nErrCode As Long
    Dim sErrBuf As String * 500
    
    lGetNextUID = lGetNextUIDEx2(DB_GetHdbc(dbLookup), sTableName, nErrCode, sErrBuf, 500)
    If nErrCode = -1 Then   ' collision timeout
       g_sProgramErrorString = "Collision timeout. Server load too high. Please wait and try again."
       Error 32001
    ElseIf nErrCode = -2 Then
       g_sProgramErrorString = "Couldn't allocate record id. Server failed. Server error is: " & sTrimNull(sErrBuf)
       Error 32001
    ElseIf nErrCode = 0 Then
       g_sProgramErrorString = "Couldn't allocate record id."
       Error 32001
    End If

End Function

