VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CWorkLoss"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

' Object properties
Private m_PiWlRowId As Long
Private m_PiRowId As Long
Private m_DateLastWorked As String
Private m_DateReturned As String
Private m_Duration As Long
Private m_StateDuration As Long 'mjh 2/20/01
Private m_OSHAWorkLossDays As Long      'MITS 14372

Public Function ClearObject() As Long
    m_PiWlRowId = 0
    m_PiRowId = 0
    m_DateLastWorked = ""
    m_DateReturned = ""
    m_Duration = 0
    m_StateDuration = 0
    m_OSHAWorkLossDays = 0
End Function
Public Property Get PiWlRowId() As Long
   PiWlRowId = m_PiWlRowId
End Property
Public Property Let PiWlRowId(ByVal vData As Long)
   m_PiWlRowId = vData
End Property

Public Property Get PiRowId() As Long
   PiRowId = m_PiRowId
End Property
Public Property Let PiRowId(ByVal vData As Long)
   m_PiRowId = vData
End Property

Public Property Get DateLastWorked() As String
   DateLastWorked = m_DateLastWorked
End Property
Public Property Let DateLastWorked(ByVal vData As String)
   m_DateLastWorked = vData
End Property

Public Property Get DateReturned() As String
   DateReturned = m_DateReturned
End Property
Public Property Let DateReturned(ByVal vData As String)
   m_DateReturned = vData
End Property

Public Property Get Duration() As Long
   Duration = m_Duration
End Property
Public Property Let Duration(ByVal vData As Long)
   m_Duration = vData
End Property

Public Property Get StateDuration() As Long
    StateDuration = m_StateDuration
End Property

Public Property Let StateDuration(ByVal vData As Long)
    m_StateDuration = vData
End Property

Public Property Get OSHAWorkLossDays() As Long
    OSHAWorkLossDays = m_OSHAWorkLossDays
End Property
Public Property Let OSHAWorkLossDays(ByVal vData As Long)
    m_OSHAWorkLossDays = vData
End Property
