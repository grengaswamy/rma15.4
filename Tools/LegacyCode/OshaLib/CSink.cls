VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSink"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IWorkerNotifications

Private Function IWorkerNotifications_CheckAbort() As Boolean
                              'do nothing
End Function

Private Sub IWorkerNotifications_UpdateProgress(ByVal sReportStage As String, ByVal timeElapsed As Long, ByVal timeLeft As Long, ByVal percentComplete As Long)
                              'again do nothing
End Sub
