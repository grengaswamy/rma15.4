VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBroker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'***********************************
'Quick and Dirty Connection Broker for Rocket
'
'Purpose:
'1.) Consolidate the "connections" used in an application.
'2.) Support rudimentary "pooling"
'
' Requires the global rocket variables
'
'
'***********************************

Private Connections As Collection

Private Sub Class_Initialize()
    Set Connections = New Collection
End Sub
Private Sub Class_Terminate()
    Dim tmp As CConnection
    For Each tmp In Connections
        SafeCloseConnection CInt(tmp.Connection)
    Next
    Set Connections = Nothing
End Sub

'BB 02/21/2002
'Note: the IsInUse function cannot determine %100 if a
' connection has been used for any mode other than a DB_FORWARD_ONLY
' recordset.
'  THEREFORE - IF YOU ARE GOING TO USE A DB_SNAPSHOT OR
'  DB_DYNASET CURSOR - OPEN YOUR OWN CONNECTION!!!!!!
Public Function IsInUse(cn As Long) As Boolean
    On Error GoTo hErr
    IsInUse = g_objRocket.DB_ConnInUse(CInt(cn))
hExit:
    On Error GoTo 0
    g_objErr.Bubble
    Exit Function
hErr:
    LogError "CBroker.GetConn", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function
Public Function GetConn(sDSN As String) As Long
    Dim objConn As CConnection
    
    On Error GoTo hErr
    For Each objConn In Connections
    'If connection is open, free and to the proper db.
        If objConn.Connection <> 0 And objConn.DSN = sDSN And Not IsInUse(CInt(objConn.Connection)) Then
            GetConn = CLng(objConn.Connection)
            Exit Function
        End If
    Next
    Set objConn = New CConnection
    objConn.Connection = g_objRocket.DB_OpenDatabase(g_hEnv, sDSN, 0)
    objConn.DSN = sDSN
    GetConn = objConn.Connection
    Connections.Add objConn
hExit:
    Exit Function
hErr:
    LogError "CBroker.GetConn", Erl, Err.Number, Err.Source, Err.Description
    GoTo hExit
End Function
