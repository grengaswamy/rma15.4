VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CErrors"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'---------------------------------------------------------------------------------------
' Module    : CErrors
' DateTime  : 1/8/2002 09:08
' Author    : bsb
' Purpose   : Simply a collection making non-critical errors available to script
'---------------------------------------------------------------------------------------
Option Explicit
Private m_Errors As Collection

Public Function Count() As Long
    Count = m_Errors.Count
End Function
Public Function Add(ByVal sProcedure As String, ByVal ErrLine As Long, ErrNumber As Long, ErrSource As String, ErrDescription As String)
Attribute Add.VB_UserMemId = 0
    Dim objErr As CError
    Set objErr = New CError
    
    With objErr
        .Source = ErrSource & "." & sProcedure
        .Description = ErrDescription
        .Number = ErrNumber
    End With

    m_Errors.Add objErr
End Function

Public Function Clear()
    Set m_Errors = New Collection
End Function
Public Property Get Item(vIdx As Variant) As CError
    Set Item = m_Errors(vIdx)
End Property
Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = m_Errors.[_NewEnum]
End Function

Private Sub Class_Initialize()
    Set m_Errors = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_Errors = Nothing
End Sub
