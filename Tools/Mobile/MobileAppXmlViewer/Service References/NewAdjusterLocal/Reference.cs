﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.235
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MobileAppXmlViewer.NewAdjusterLocal {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RMException", Namespace="http://schemas.datacontract.org/2004/07/Riskmaster.Models")]
    [System.SerializableAttribute()]
    public partial class RMException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ErrorsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Errors {
            get {
                return this.ErrorsField;
            }
            set {
                if ((object.ReferenceEquals(this.ErrorsField, value) != true)) {
                    this.ErrorsField = value;
                    this.RaisePropertyChanged("Errors");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="NewAdjusterLocal.IMobileAdjusterService")]
    public interface IMobileAdjusterService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMobileAdjusterService/DoWork", ReplyAction="http://tempuri.org/IMobileAdjusterService/DoWorkResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(MobileAppXmlViewer.NewAdjusterLocal.RMException), Action="http://tempuri.org/IMobileAdjusterService/DoWorkRMExceptionFault", Name="RMException", Namespace="http://schemas.datacontract.org/2004/07/Riskmaster.Models")]
        void DoWork();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMobileAdjusterService/AdjusterRowId", ReplyAction="http://tempuri.org/IMobileAdjusterService/AdjusterRowIdResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(MobileAppXmlViewer.NewAdjusterLocal.RMException), Action="http://tempuri.org/IMobileAdjusterService/AdjusterRowIdRMExceptionFault", Name="RMException", Namespace="http://schemas.datacontract.org/2004/07/Riskmaster.Models")]
        string AdjusterRowId(string xmlRequest);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMobileAdjusterServiceChannel : MobileAppXmlViewer.NewAdjusterLocal.IMobileAdjusterService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MobileAdjusterServiceClient : System.ServiceModel.ClientBase<MobileAppXmlViewer.NewAdjusterLocal.IMobileAdjusterService>, MobileAppXmlViewer.NewAdjusterLocal.IMobileAdjusterService {
        
        public MobileAdjusterServiceClient() {
        }
        
        public MobileAdjusterServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MobileAdjusterServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MobileAdjusterServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MobileAdjusterServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void DoWork() {
            base.Channel.DoWork();
        }
        
        public string AdjusterRowId(string xmlRequest) {
            return base.Channel.AdjusterRowId(xmlRequest);
        }
    }
}
