﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using System.Data;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Riskmaster.Application.PolicySystemInterface;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.IO;
using System.Web;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using Riskmaster.Settings;
using System.Text.RegularExpressions;
using System.Threading;

namespace ClaimBalancing
{
    public class Program
    {
        private static UserLogin m_objUserLogin = null;
        private static string sPolicySystemId = string.Empty;
        private static string sPTConnectionStr = string.Empty;
        public static string sPolicySystemName = string.Empty;
        public static string m_sClaimTypeCode = string.Empty;
        public static string m_sSendEmail = string.Empty;
        public static string m_sDateofClaim = string.Empty;
        static string m_sDataSource = string.Empty;
        static string m_sLoginName = string.Empty;
        static string m_sLoginPwd = string.Empty;
        static string m_sAdminUserName = string.Empty;
        static string m_sAdminPassword = string.Empty;
        static string m_sDbConnstring = string.Empty;
        static string m_sDbConnstringGlobal = string.Empty;
        static string m_sAdminDbConnstring = string.Empty;
        static string m_sDBOUserId = string.Empty;
        static string m_sDSNID = string.Empty;
        static string m_sDefaultTableSpaceForOracle = string.Empty;
        static string m_sTempTableSpaceForOracle = string.Empty;
        static DbConnection m_objDbConnection = null;
        static eDatabaseType m_sDatabaseType = 0;
        static Mutex mutex = new Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");
        //static bool bIsMultipleInstanceErorr = false;
        static bool bClaimBalancingRunningthroughTM = true;
        static string sShowClosedClaimsInGreyColor = string.Empty;
        static string m_sOnlyPull = string.Empty;
        static string m_sOnlyOOBSheet = string.Empty;
        static int iTotalWhiteRows = 0;
        static int iTotalRedRows = 0;
        static int iTotalYellowRows = 0;
        static int iTotalGreenRows = 0;
        static int iTotalGreyRows = 0;
        static int iTotalRows = 0;
        static int iColorIndexWhite = 0;
        static int iColorIndexGrey = 15;
        static int iColorIndexGreen = 4;
        static int iColorIndexRed = 3;
        static int iColorIndexYellow = 6;
        static string m_sExcludeThisMasterComp = string.Empty;//rupal
        private static int m_iClientId = 0;//sonali
        [STAThread]
        public static void Main(string[] args)
        {
            DbWriter writer = null;
            string sPointLossDate = string.Empty;
            StringBuilder sPointSQL = new StringBuilder();
            LocalCache objCache = null;
            string sFolderPath = string.Empty;
            string sEmailAddr = string.Empty;
            bool bReturn = false;
            Riskmaster.Common.Mailer objMail = null;
            string sAdminEmailAdd = string.Empty;
            string sSMTP = string.Empty;
            StringBuilder sbErrMsg = new StringBuilder();
            AppHelper objAppHelper = null;
            SysSettings objSysSettings = null;
            int idxDSN = 0;
            int idxUID = 1;
            int idxPWD = 2;
            int idxPolSys = 3;
            int idxLossDt = 4;
            int idxClmType = 5;
            int idxSendMail = 6;            
            int idxAdminUid = 7;
            int idxAdminPwd = 8;

            try
            {
                objAppHelper = new AppHelper();
                //check to prevent multiple instance of claim balancing exe
                if (mutex.WaitOne(TimeSpan.Zero, true))
                {
                    try
                    {
                        #region commented
                        //args = new string[9];
                        //args[0] = "-dsRMA141_POLICY"; //dsn
                        //args[1] = "-ruabk"; //uid
                        //args[2] = "NOPWD4TM";//pwd             
                        //args[3] = "2"; //plicy system id
                        // args[4] = "0";//20131024";// Date in yyyyMMdd format
                        // args[5] = "0"; //Claim Type Code
                        // args[6] = "false";//send email?
                        // args[7] = ""; //admin uid
                        // args[8] = "";//admin pwd
                        //Ankit Start
                        #endregion
                        Console.WriteLine("0 ^*^*^ Claim Balancing started ");

                        int argCount = (args == null) ? 0 : args.Length;
                        #region commented
                        //Console.WriteLine(" args[0] " + args[0]);
                        //Console.WriteLine(" args[0] " + args[1]);
                        //Console.WriteLine(" args[0] " + args[2]);
                        //Console.WriteLine(" args[0] " + args[3]);
                        //Console.WriteLine(" args[0] " + args[4]);
                        //Console.WriteLine(" args[0] " + args[5]);
                        //Console.WriteLine(" args[0] " + args[6]);
                        //Console.WriteLine(" args[0] " + args[7]);
                        //Console.WriteLine(" args[0] " + args[8]);
                        #endregion
                        //rupal:start,making the exe stand alone
                        if (argCount == 0)
                        {
                            //if arg count is zero...read input params from config..
                            try
                            {
                                #region Read Input Parms through config and validate them
                                Console.WriteLine("0 ^*^*^ Reading input parameters through configuration file. ");
                                bClaimBalancingRunningthroughTM = false;
                                //Console.Read();
                                args = new string[9];

                                args[idxDSN] = "-ds" + Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["RMXDSNName"]);
                                if (args[idxDSN] == string.Empty)
                                    throw new Exception("RMXDSNName: Data Source Name is left blank in the configuartion file");
                                args[idxUID] = "-ru" + Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["RMXLoginName"]);
                                if (args[idxUID] == string.Empty)
                                    throw new Exception("RMXLoginName: RMA Login Name is left blank in the configuartion file");
                                args[idxPWD] = "-rpNOPWD4TM";
                                args[idxPolSys] = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["PolicySystemName"]);
                                if (args[idxPolSys] == string.Empty)
                                    throw new Exception("PolicySystemName: Policy System Name is left blank in the configuartion file");
                                args[idxLossDt] = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["DateOfLoss"]);
                                if (args[idxLossDt] == string.Empty)
                                    throw new Exception("DateOfLoss: Date of Loss is left blank in the configuartion file. Either provide valid date in(MM/DD/YYYY) format or leave it to default value 0 (Zero). ");
                                args[idxClmType] = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["RMAClaimTypeCodeShortCode"]);
                                if (args[idxClmType] == string.Empty)
                                    throw new Exception("RMAClaimTypeShortCode: RMA Claim Type Short Code is left blank in the configuartion file. Default value should be 0 (Zero) ");
                                args[idxSendMail] = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["SendReportViaEmail"]);
                                if (args[idxSendMail] == string.Empty)
                                    throw new Exception("SendReportViaEmail: Send Report Vial Email is left blank in the configuration file. Values can be either True or False");
                                if (!args[idxSendMail].ToUpper().Equals("TRUE") && !args[6].ToUpper().Equals("FALSE"))
                                    throw new Exception("SendReportViaEmail: Send Report Vial Email parameter can have value either True or False");
                                args[idxAdminUid] = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["AdminUserId"]);
                                if (args[idxAdminUid] != string.Empty)
                                    args[idxAdminUid] = "-au" + args[idxAdminUid];
                                args[idxAdminPwd] = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["AdminPassword"]);
                                if (args[idxAdminPwd] != string.Empty)
                                    args[idxAdminPwd] = "-ap" + args[idxAdminPwd];
                                #region commented
                                //Ankit Start
                                //args[1] = "-ruabk"; //uid
                                //args[2] = "NOPWD4TM";//pwd             
                                //args[3] = "2"; //plicy system id
                                // args[4] = "0";//20131024";// Date in yyyyMMdd format
                                // args[5] = "0"; //Claim Type Code
                                // args[6] = "false";//send email?
                                // args[7] = ""; //admin uid
                                // args[8] = "";//admin pwd
                                #endregion
                                argCount = (args == null) ? 0 : args.Length;
                                #endregion
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("0 ^*^*^ Error occoured while reading data from configuration file. Please provide valid input(s)." + e.Message);
                                sbErrMsg.AppendLine("Error occoured while reading data from configuration file. Please provide valid input(s)." + e.Message);
                                throw new Exception(sbErrMsg.ToString());
                            }
                        }
                        //rupal:end,making the exe stand alone

                        //get the confid setting to show closed claims as grey color
                        try
                        {
                            sShowClosedClaimsInGreyColor = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["ShowClosedClaimsInGrey"]);
                                if (!sShowClosedClaimsInGreyColor.ToUpper().Equals("TRUE") && !sShowClosedClaimsInGreyColor.ToUpper().Equals("FALSE"))
                                    throw new Exception("ShowClosedClaimsInGrey: Invalid input parameter. Values can be either True or False");
                                m_sOnlyOOBSheet = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["OnlyOOBSheetNoPull"]);
                                if (!m_sOnlyOOBSheet.ToUpper().Equals("TRUE") && !m_sOnlyOOBSheet.ToUpper().Equals("FALSE"))
                                    throw new Exception("OnlyOOBSheetNoPull: Invalid input parameter. Values can be either True or False");
                                m_sExcludeThisMasterComp = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("ClaimBalancingInputParams", m_sDbConnstring, m_iClientId)["ExcludeMasterCo"]); 
                        }
                        catch(Exception e)
                        {                            
                            sbErrMsg.Clear();
                            sbErrMsg.AppendLine("Error occoured while reading data from configuration file. Please provide valid input(s)." + e.Message);
                            throw new Exception(sbErrMsg.ToString());
                        }

                        #region get parameters, verify user credentials and prepare RMA Conn Strring
                        if (argCount > 0)//in case of task manager, utility will directly come here
                        {
                            GetParameters(args);
                            m_sDateofClaim = args[idxLossDt];
                            //Console.WriteLine("..convert date to point");
                            sPointLossDate = ConvertDateToPoint(m_sDateofClaim);
                            //Console.WriteLine("sPointLossDate" + sPointLossDate);

                            m_sSendEmail = args[idxSendMail];
                            m_objUserLogin = new UserLogin(m_sLoginName, m_sDataSource,m_iClientId);//sonali
                            Initalize(m_objUserLogin);
                            //Console.WriteLine("after intialize");
                            if (m_objUserLogin.DatabaseId <= 0)
                            {
                                //Console.Write("database id if block");
                                Console.WriteLine("0 ^*^*^ Authentication failure.");
                                sbErrMsg.AppendLine("Authentication failure.");
                                throw new Exception(sbErrMsg.ToString());
                            }
                            //Console.WriteLine("reaced here");
                            //creating connection string using admin userid and password.
                            if (!string.IsNullOrEmpty(m_sAdminUserName) && !string.IsNullOrEmpty(m_sAdminPassword))
                            {
                                //Console.Write("inside admin parms if block");
                                SetAdminParams(m_sAdminUserName, m_sAdminPassword);
                            }
                            else
                            {
                                //Console.Write("inside admin parms else block");
                                m_sAdminDbConnstring = m_sDbConnstring;
                            }
                        }

                        #endregion

                        #region verify policy system and claim type parameter in case of stand alone exe
                        objCache = new LocalCache(m_sAdminDbConnstring,m_iClientId);//sonali
                        if (bClaimBalancingRunningthroughTM)
                        {
                            sPolicySystemId = args[3];
                            m_sClaimTypeCode = args[5];
                        }
                        else
                        {
                            //through config
                            #region verify policy system Id parameter
                            sPolicySystemName = args[3];
                            bool bFinacialUploadFlag = false;
                            GetPolicysystemIdFromPolicysystemName(sPolicySystemName, out sPolicySystemId, out bFinacialUploadFlag);
                            if (sPolicySystemId == string.Empty)
                                throw new Exception("Policy system Name specified in the configuration file is not valid.");
                            //in case of stand alone exe..we need to make sure if policy system's finacial upload flag is true
                            if (!bFinacialUploadFlag)
                                throw new Exception("Finacial upload flag is not enabled for given policy system. Calaim balancing terminated");
                            #endregion

                            #region verify clim type parameter
                            string sRMAClaimTypeShortCode = args[5];
                            //if claim type is mentiones zero in config,it mens no filter criteria is provided for claim type
                            if (sRMAClaimTypeShortCode.Equals("0"))
                                m_sClaimTypeCode = args[5];
                            else
                            {
                                //get code id based on short code
                                int iclaimTypeId = objCache.GetCodeId(sRMAClaimTypeShortCode, "CLAIM_TYPE");
                                if (iclaimTypeId == 0)//short code is not valid,throw exception
                                    throw new Exception("Please specify valid claim type code in the configuration file");
                                else
                                    m_sClaimTypeCode = Convert.ToString(iclaimTypeId);
                            }
                            #endregion
                        }
                        #endregion

                        #region verify policy system name and prepare folder path for excel sheet
                        ////Console.WriteLine("Get Policy system name started");
                        if (bClaimBalancingRunningthroughTM)
                            sPolicySystemName = GetPolicySystemName(objAppHelper, sbErrMsg);
                        if (sPolicySystemName == string.Empty)
                        {
                            Console.WriteLine("0 ^*^*^ Policy system name can not be blank. Please provide valid poliy system ID.");
                            throw new Exception("Policy system name can not be blank. Please provide valid poliy system ID");
                        }
                        //Console.WriteLine("Get Policy system name end");
                        sFolderPath = RMConfigurator.BasePath + "\\userdata\\Claim Balancing";
                        ///Console.WriteLine("Getfolder path" + sFolderPath);
                        #endregion///

                        #region get policy system's claim type short code based on rma short code

                        string sClaimTypeShortCode = string.Empty;
                        if (string.IsNullOrEmpty(m_sClaimTypeCode) == false && m_sClaimTypeCode != "0")
                        {
                            PolicySystemInterface obj = null;
                            try
                            {
                                obj = new PolicySystemInterface(m_sAdminDbConnstring, m_iClientId);
                                //sClaimTypeShortCode = objCache.GetShortCode(Conversion.CastToType<int>(m_sClaimTypeCode, out bReturn));
                                //obj.GetPSMappedCode(Conversion.CastToType<int>(m_sClaimTypeCode, out bReturn),"CLAIM_TYPE",,sPolicySystemId,0
                                //Console.WriteLine("claim type" + sClaimTypeShortCode);
                                string sException = "Claim Balancing : Error occoured while fetching policy system's mapped code for claim type";
                                int iPSCodeID = obj.GetPSMappedCodeIDFromRMXCodeId(Conversion.CastToType<int>(m_sClaimTypeCode, out bReturn), "CLAIM_TYPE", sException, sPolicySystemId);
                                if (iPSCodeID != 0)
                                    sClaimTypeShortCode = objCache.GetShortCode(iPSCodeID);

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("0 ^*^*^ Error occoured while fetching policy system's mapped code for claim type ");

                                throw new Exception("Error occoured while fetching policy system's mapped code for claim type");
                            }
                            finally
                            {
                                obj = null;

                            }
                        }

                        #endregion

                        #region get point connection string and verify the same
                        if (m_sOnlyOOBSheet.ToUpper().Equals("FALSE"))//RUPAL:CLAIM BALANCING ADDITIONAL ENH
                        {
                        //Console.WriteLine("get point conn string started ");
                        GetPointConnStr(objAppHelper, sbErrMsg);
                        //Console.WriteLine("get point conn string end ");
                        if (sPTConnectionStr == string.Empty)
                        {
                            Console.WriteLine("0 ^*^*^ Failed to connect to POINT. Please provide POINT connecion details through RMA Policy system setup");
                            sbErrMsg.AppendLine("Failed to connect to POINT. Please provide POINT connecion details through RMA Policy system setup");
                            throw new Exception(sbErrMsg.ToString());
                        }
                        }
                        #endregion

                        #region if SMTP server is configured
                        if (string.Equals(m_sSendEmail.ToUpper(), "TRUE", StringComparison.InvariantCultureIgnoreCase))
                        {
                            System.Data.DataTable dtSMTPSettings = RMConfigurationSettings.GetSMTPServerSettings(m_iClientId);//sonali

                            sSMTP = dtSMTPSettings.Rows[0]["SMTP_SERVER"].ToString();
                            sAdminEmailAdd = dtSMTPSettings.Rows[0]["ADMIN_EMAIL_ADDR"].ToString();
                            if ((string.IsNullOrEmpty(sAdminEmailAdd) && string.IsNullOrEmpty(sSMTP)))
                            {
                                Console.WriteLine("0 ^*^*^ SMTP Server Not Configured");
                                sbErrMsg.AppendLine("SMTP Server Not Configured");
                                throw new Exception(sbErrMsg.ToString());
                            }
                        }
                        #endregion

                        //Console.WriteLine("truncate");
                        //INSERT DATA INTO CLAIM_BALANCING_PARMS TABLE

                        ////first check if any instance of claim balancing is already running
                        //string sQuery = "SELECT IS_CLAIM_BALANCING_RUNNING FROM CLAIM_BALANCING_PARMS";
                        //int iClaimBalancingRunning = 0;
                        //using (DbReader objReader = DbFactory.GetDbReader(m_sAdminDbConnstring, sQuery))
                        //{
                        //    if (objReader.Read())
                        //    {
                        //        iClaimBalancingRunning = Conversion.ConvertObjToInt(objReader.GetValue(0));
                        //    }
                        //}
                        //if (iClaimBalancingRunning == -1)
                        //{
                        //    throw new Exception("Another instance of Claim Balancing is already running. Please run this utility after some time.");
                        //}


                        #region Truncate data first
                        if (m_sOnlyOOBSheet.ToUpper().Equals("FALSE"))//RUPAL:CLAIM BALANCING ADDITIONAL ENH
                        {
                        string ssQLTruncate = "TRUNCATE TABLE CLAIM_BALANCING_PARMS";
                        DbFactory.ExecuteNonQuery(m_sAdminDbConnstring, ssQLTruncate);
                        objSysSettings = new SysSettings(m_sAdminDbConnstring,m_iClientId);//sonali
                        int iClaimBasedDate = -1;
                        if (int.Equals(objSysSettings.PolicyCvgType, 1))
                            iClaimBasedDate = 0;
                        writer = DbFactory.GetDbWriter(m_sAdminDbConnstring);
                        writer.Tables.Add("CLAIM_BALANCING_PARMS");

                        if (string.IsNullOrEmpty(m_sDateofClaim) == false && m_sDateofClaim != "0")
                            writer.Fields.Add("DATE_OF_CLAIM", Conversion.GetDate(m_sDateofClaim));
                        if (!string.IsNullOrEmpty(m_sClaimTypeCode) && !string.Equals(m_sClaimTypeCode, "0"))
                            writer.Fields.Add("CLAIM_TYPE", m_sClaimTypeCode);
                        writer.Fields.Add("POLICY_SYSTEM_ID", sPolicySystemId);
                        writer.Fields.Add("CLAIM_BASED_DATE", iClaimBasedDate);
                        writer.Execute();
                        writer = null;

                        ssQLTruncate = "TRUNCATE TABLE CLAIM_BALANCING";
                        DbFactory.ExecuteNonQuery(m_sAdminDbConnstring, ssQLTruncate);
                        #endregion

                        #region Pull Data from Point

                        Console.WriteLine("0 ^*^*^ Pulling Data from policy system to RMA.");

                        //rupal:start,
                        sPointSQL.Append("SELECT PMSPCL42.MASTERCO,TBCL006A.RESVTYPE,PMSPCL42.LOSSCAUSE, AllRecords.*,MAXTransSeqRecords.TRANSEQ MAX_TRANS_SEQ FROM ( ");
                        sPointSQL.Append("SELECT PMSPCL50.CLAIM,PMSPCL50.CLMTSEQ,PMSPCL50.POLCOVSEQ,PMSPCL50.RESVNO,MAX(TRANSSEQ) TRANSEQ FROM PMSPCL50,PMSPCL20 ");
                        sPointSQL.Append("WHERE PMSPCL50.CLAIM = PMSPCL20.CLAIM ");
                        if (sPointLossDate != string.Empty)
                            sPointSQL.Append(" AND (PMSPCL20.LOSSDTE >= '" + sPointLossDate + "')");
                        if (sClaimTypeShortCode != string.Empty)
                            sPointSQL.Append(" AND (PMSPCL20.CLAIMTYPE='" + sClaimTypeShortCode + "')");
                        sPointSQL.Append("GROUP BY PMSPCL50.CLAIM,PMSPCL50.CLMTSEQ,PMSPCL50.POLCOVSEQ,PMSPCL50.RESVNO)MAXTransSeqRecords ");
                        sPointSQL.Append("INNER JOIN  ");
                        sPointSQL.Append("(SELECT PMSPCL50.*,PMSPCL20.CLAIMTYPE,PMSPCL20.LOSSDTE FROM PMSPCL50,PMSPCL20 ");
                        sPointSQL.Append("WHERE PMSPCL50.CLAIM = PMSPCL20.CLAIM ");
                        if (sPointLossDate != string.Empty)
                            sPointSQL.Append(" AND (PMSPCL20.LOSSDTE >= '" + sPointLossDate + "')");
                        if (sClaimTypeShortCode != string.Empty)
                            sPointSQL.Append(" AND (PMSPCL20.CLAIMTYPE='" + sClaimTypeShortCode + "')");
                        sPointSQL.Append(" ) AllRecords ");
                        sPointSQL.Append("ON MAXTransSeqRecords.CLAIM=AllRecords.CLAIM AND MAXTransSeqRecords.CLMTSEQ=AllRecords.CLMTSEQ  ");
                        sPointSQL.Append("AND MAXTransSeqRecords.POLCOVSEQ=AllRecords.POLCOVSEQ AND MAXTransSeqRecords.RESVNO=AllRecords.RESVNO ");
                        sPointSQL.Append("AND MAXTransSeqRecords.TRANSEQ=AllRecords.TRANSSEQ ");
                        sPointSQL.Append("INNER JOIN TBCL006A ON AllRecords.RESVNO=TBCL006A.RESVNO ");
                        sPointSQL.Append("INNER JOIN PMSPCL42 ON PMSPCL42.CLAIM=AllRecords.CLAIM AND AllRecords.CLMTSEQ=PMSPCL42.CLMTSEQ AND PMSPCL42.POLCOVSEQ = AllRecords.POLCOVSEQ ");
                        sPointSQL.Append("INNER JOIN PMSPSA15 ON PMSPCL42.SYMBOL=PMSPSA15.SYMBOL AND PMSPCL42.POLICYNO=PMSPSA15.POLICY0NUM AND PMSPCL42.MODULE=PMSPSA15.MODULE ");
                        sPointSQL.Append("AND PMSPCL42.MASTERCO=PMSPSA15.MASTER0CO AND PMSPCL42.LOCATION=PMSPSA15.LOCATION AND PMSPCL42.UNITNO=PMSPSA15.SARUNIT AND ");
                        sPointSQL.Append("PMSPCL42.COVSEQ=PMSPSA15.SARSEQNO AND PMSPCL42.TRANSSEQ=PMSPSA15.SASEQNO ");
                        //rupal:end
                        //Console.WriteLine("before insert into");
                        using (DbReader objDataRdr = DbFactory.GetDbReader(sPTConnectionStr, sPointSQL.ToString()))
                        {
                            while (objDataRdr.Read())
                            {
                                InsertIntoRmaClaimBalancingTable(objDataRdr, objAppHelper, sbErrMsg);     //Ankit Start Added objAppHelper & sbErrMsg parameters
                            }
                        }
                        Console.WriteLine("0 ^*^*^ Data successfully pulled from policy system to RMA.");
                    }
                        #endregion

                        #region write dta to excel
                        //Console.WriteLine("befoe write to excel");

                        StringBuilder sSQL_OOB = new StringBuilder();
                        sSQL_OOB.Append("SELECT * FROM BALANCE_RMA_POINT_OB_V ");
                        if (m_sExcludeThisMasterComp != string.Empty)
                        {
                            string[] sArrMasterComp = m_sExcludeThisMasterComp.Split(',');
                            string sMasterCompNotIn = string.Empty;
                            if (sArrMasterComp != null && sArrMasterComp.Length > 0)
                            {
                                for (int i = 0; i < sArrMasterComp.Length; i++)
                                {
                                    if (sMasterCompNotIn == string.Empty)
                                        sMasterCompNotIn = "'" + sArrMasterComp[i] + "'";
                                    else
                                        sMasterCompNotIn = sMasterCompNotIn + ",'" + sArrMasterComp[i] + "'";
                                }
                                sSQL_OOB.Append(" WHERE P_MASTERCO NOT IN (");
                                sSQL_OOB.Append(sMasterCompNotIn);
                                sSQL_OOB.Append(")");
                            }
                        }
                        sSQL_OOB.Append(" ORDER BY RMA_CLAIM_NUMBER,P_CLAIM_NUMBER ");
                        
                        using (DbReader objRdr = DbFactory.GetDbReader(m_sAdminDbConnstring, sSQL_OOB.ToString()))
                        {
                            WriteDataToExcel(objRdr, sFolderPath, objAppHelper, sbErrMsg);
                            //Ankit Start Added objAppHelper & sbErrMsg parameters                    
                        }
                        Console.WriteLine("0 ^*^*^ Data(if any) exported to excel.");
                        #endregion

                        #region send email
                        if (string.Equals(m_sSendEmail.ToUpper(), "TRUE", StringComparison.InvariantCultureIgnoreCase))
                        {
                            try
                            {
                                sEmailAddr = GetEmailByLoginName(m_objUserLogin.LoginName, SecurityDatabase.Dsn, objAppHelper, sbErrMsg);     //Ankit Start Added objAppHelper & sbErrMsg parameters
                                if (sEmailAddr == string.Empty || sAdminEmailAdd == string.Empty)
                                    throw new Exception("Invalid From/To email address. Mail was not sent!");
                                objMail = new Riskmaster.Common.Mailer(m_iClientId);//sonali
                                objMail.To = sEmailAddr;
                                objMail.From = sAdminEmailAdd;
                                objMail.Subject = "Claim Balancing Report";
                                objMail.IsBodyHtml = true;
                                objMail.Body = "";
                                objMail.SmtpServer = sSMTP;
                                objMail.AddAttachment(sFolderPath + "\\" + sPolicySystemName + "_ClaimBalancing.xls");
                                objMail.SendMail();
                                Console.WriteLine("0 ^*^*^ Mail Sent");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("0 ^*^*^ Error occoured while sending email.");
                                throw e;
                            }
                            finally
                            {
                                objMail = null;
                            }
                        }
                        //ExportToExcel(DTable);
                        #endregion
                        Console.WriteLine("0 ^*^*^ Claim Balancing Completed Successfully.");
                        //mutex.ReleaseMutex();
                        //mutex.Dispose();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        mutex.ReleaseMutex();
                    }
                }//if block ends
                else
                {
                    //bIsMultipleInstanceErorr = true;
                    throw new Exception("Another instance of Claim Balancing is already running. Please try again after some time");
                }
            }//try block ends
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Claim Balancing Completed With Errors");
                Console.WriteLine("1001 ^*^*^ {0} ", e.Message + " " + e.InnerException);
                //Ankit Start
                objAppHelper.WriteErrorToFile(e.Message, e);
                //Ankit End
                //bool rethrow = ExceptionPolicy.HandleException(e, "Logging Policy");

            }
            finally
            {
                if (objAppHelper != null)
                    objAppHelper = null;
                if (sbErrMsg != null)
                    sbErrMsg = null;
                objSysSettings = null;
                writer = null;
                sPointSQL = null;
                objCache = null;
                objMail = null;
                //if (!bIsMultipleInstanceErorr && mutex != null)
                //if (mutex != null)
                //{
                //  mutex.ReleaseMutex();
                //mutex.Dispose();
                //}
                //Console.Read();
            }              
            
        }

        private static void GetPolicysystemIdFromPolicysystemName(string sPolSytemName, out string sPolicySystemId, out bool bFinacialUploadFlag)
        {
            sPolicySystemId = string.Empty;
            bFinacialUploadFlag = false;
            try
            {
                if (sPolSytemName == string.Empty)
                    throw new Exception("Policy system name can not be blank");
                string sSQL = "SELECT POLICY_SYSTEM_ID,FINANCIAL_UPD_FLAG FROM POLICY_X_WEB WHERE UPPER(POLICY_SYSTEM_NAME)='" + sPolSytemName.ToUpper() + "'";
                using (DbReader objReader = DbFactory.GetDbReader(m_sAdminDbConnstring, sSQL))
                {
                    if (objReader.Read())
                    {
                        sPolicySystemId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        bFinacialUploadFlag = Conversion.ConvertObjToBool(objReader.GetValue(1),m_iClientId);//sonali
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }


        //private static bool IsPolicySystemFinacialUploadFlagOn(string sPolicysystemId)
        //{
        //    bool bReturn = false;
        //    try
        //    {
        //        string sSQL = "SELECT FINANCIAL_UPD_FLAG FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID='" + sPolicysystemId + "' AND FINANCIAL_UPD_FLAG=-1";

        //        using (DbReader objReader = DbFactory.GetDbReader(m_sAdminDbConnstring, sSQL))
        //        {
        //            if (objReader.Read())
        //            {
        //                bReturn = true;
        //            }
        //        }
        //        return bReturn;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }            
        //}

        private static void GetParameters(string[] p_args)
        {
//            Console.WriteLine("..Get Parameters Started");
            int iLength = 0;
            string sPrefix = string.Empty;
            string sRebuildAll = string.Empty;
            iLength = p_args.Length;

            for (int i = 0; i < iLength; i++)
            {

                sPrefix = p_args[i].Trim();

                if (sPrefix.Length > 3)
                {
                    sPrefix = sPrefix.Substring(0, 3);
                }
                switch (sPrefix.ToLower())
                {
                    case "-ds":
                        m_sDataSource = p_args[i].Trim().Substring(3);
                        break;
                    case "-ru":
                        m_sLoginName = p_args[i].Trim().Substring(3);
                        break;
                    case "-rp":
                        m_sLoginPwd = p_args[i].Trim().Substring(3);
                        break;
                    case "-au":
                        m_sAdminUserName = p_args[i].Trim().Substring(3);
                        break;
                    case "-ap":
                        m_sAdminPassword = p_args[i].Trim().Substring(3);
                        break;
                }
            }
        //    Console.WriteLine("..Get Parameters end");
        }

        private static void Initalize(UserLogin p_oUserLogin)
        {
       //     Console.WriteLine("..intialize started");
            m_sDSNID = p_oUserLogin.DatabaseId.ToString();
            m_sDBOUserId = p_oUserLogin.objRiskmasterDatabase.RMUserId;
            m_sDbConnstring = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
            m_objDbConnection = DbFactory.GetDbConnection(m_sDbConnstring);
            // m_sDbConnstringForTableSpace = m_sDbConnstring;
            m_sDbConnstringGlobal = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
            try
            {
                m_objDbConnection.Open();
                m_sDatabaseType = m_objDbConnection.DatabaseType;
                m_objDbConnection.Close();
                //if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                //{
                //    SetTableSpaceForOracle();
                //}
//                Console.WriteLine("intialize end");
            }
                
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                m_objDbConnection.Close();
            }

        }

        private static void SetAdminParams(string p_sAdminUserName, string p_sAdminPassword)
        {
            if (!string.IsNullOrEmpty(p_sAdminUserName) && !string.IsNullOrEmpty(p_sAdminPassword))
            {
 //               Console.Write("inside set admin params function");
                m_sDbConnstring = GetBaseConnectionString(m_sDbConnstring);
                m_sAdminDbConnstring = m_sDbConnstring + "UID=" + p_sAdminUserName + ";PWD=" + p_sAdminPassword + ";";

            }
            else
            {
  //              Console.Write("inside set admin params functio - else blockn");
                m_sAdminDbConnstring = m_sDbConnstring;
                m_sAdminUserName = m_sDBOUserId;
            }
//            Console.Write("inside set admin params functio - end");
        }

        private static string GetBaseConnectionString(string strConnectionString)
        {
//            Console.Write("inside get base connection string function");
            string strRegExpr = "^(?<CONN_STR_VALUE>[^;].+)UID=(?<UID_VALUE>[^;]+);PWD=(?<PWD_VALUE>[^;]+)[;]*$";
            const string GROUP_NAME = "CONN_STR_VALUE";

            string strMatchValue = string.Empty;

            //Get the Match for the string
            System.Text.RegularExpressions.Match regExMatch = Regex.Match(strConnectionString, strRegExpr, RegexOptions.IgnoreCase);

            //Get the matching value for the specified group name
            strMatchValue = regExMatch.Groups[GROUP_NAME].Value;
//            Console.Write("base conn string function end " + strMatchValue);
            return strMatchValue;
        }

        private static string GetEmailByLoginName(string p_sUserName, string p_sConn, AppHelper objAppHelper, StringBuilder sbErrMsg)   //Ankit Start Added objAppHelper & sbErrMsg parameters
        {


            string sTmp = string.Empty;
            string sEmailAddr = string.Empty;
            string sTemp = string.Empty;

            string sSQL = string.Empty;
            string sDSN = string.Empty;
            try
            {

                sSQL = "SELECT DISTINCT EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID =USER_TABLE.USER_ID AND  USER_DETAILS_TABLE.LOGIN_NAME = '" + p_sUserName + "'";
                using (DbReader objReader = DbFactory.GetDbReader(p_sConn, sSQL))
                {
                    if (objReader.Read())
                        sEmailAddr = objReader.GetString("EMAIL_ADDR");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Email Id of Assigning User" + p_sUserName + " Not Found.");
                //Ankit Start
                sbErrMsg.AppendLine("Email Id of Assigning User" + p_sUserName + " Not Found.");
                sbErrMsg.AppendLine(e.Message);
                objAppHelper.WriteErrorToFile(sbErrMsg.ToString(), e);
                throw e;
                //Ankit End
            }

            return sEmailAddr;
        }

        //private static void WriteDataToExcel(DbReader p_objRdr, string sFolderPath, AppHelper objAppHelper, StringBuilder sbErrMsg)   //Ankit Start Added objAppHelper & sbErrMsg parameters
        private static void WriteDataToExcel(DbReader p_objRdr, string sFolderPath, AppHelper objAppHelper, StringBuilder sbErrMsg)   //Ankit Start Added objAppHelper & sbErrMsg parameters
        {
            //ColLobSettings objLobSettings = new ColLobSettings(m_sAdminDbConnstring);
            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel._Workbook oWB;
            Microsoft.Office.Interop.Excel._Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range range = null;
            Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
            int iRowNum = 1;
            LocalCache objcache = null;
            try
            {
                if (File.Exists(sFolderPath + "\\" + sPolicySystemName + "_ClaimBalancing.xls"))
                {
                    //Console.WriteLine("Existing File Deleted.");
                    File.Delete(sFolderPath + "\\" + sPolicySystemName + "_ClaimBalancing.xls");
                }
                oXL = new Microsoft.Office.Interop.Excel.Application();
                if (!File.Exists(sFolderPath + "\\" + sPolicySystemName + "_ClaimBalancing.xls"))
                {
                    //Console.WriteLine("New Workbook Add.");
                    oWB = oXL.Workbooks.Add();
                }
                else
                {
                    //Console.WriteLine("New Workbook Open.");
                    oWB = oXL.Workbooks.Open(sFolderPath, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true,
                        false, 0, true, false, false);
                }
                mWorkSheets = oWB.Worksheets;
                #region Prepare OOB Sheet
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)mWorkSheets.get_Item("Sheet1");

                oSheet.Name = "OOB Report";
                //if (bClaimBalancingRunningthroughTM)
                //    cellText = cellText + " : through TM";
                //else
                //    cellText = cellText + " : through config";
                range = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, 1];
                int rowCount = range.Rows.Count;
                //oSheet.Cells[rowCount, 1] = cellText;
                //rowCount++;
                range = oSheet.UsedRange;
                int iColumns = GetHeader(ref oSheet, objAppHelper, sbErrMsg);
                range = oSheet.UsedRange;
                range.Font.Bold = true;
                range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                int colCount = range.Columns.Count;
                rowCount = range.Rows.Count;
                objcache = new LocalCache(m_sAdminDbConnstring,m_iClientId);
                while (p_objRdr.Read())
                {
                        rowCount++;
                    iTotalRows++;
                    GetDataForExport(p_objRdr, ref oSheet, rowCount, colCount, objAppHelper, sbErrMsg, objcache);     //Ankit Start Added objAppHelper & sbErrMsg parameters
                }
                
                //while (p_objRdr.Read())
                //{
                //    rowCount++;
                //    GetDataForExport(p_objRdr, ref oSheet, rowCount, colCount, objAppHelper, sbErrMsg, objcache);     //Ankit Start Added objAppHelper & sbErrMsg parameters
                //}
                

//                Console.WriteLine("getdata for export");
                string sfilename = "\\" + sPolicySystemName + "_ClaimBalancing";
                ((Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, 1]).EntireColumn.ColumnWidth = 20;
                #endregion                
                #region Prepare Summary Sheet
                Microsoft.Office.Interop.Excel._Worksheet oSheetSummary = (Microsoft.Office.Interop.Excel._Worksheet)oWB.Sheets.Add(Type.Missing, Type.Missing, 1, Type.Missing);
                oSheetSummary.Name = "Summary";
                iRowNum = 1;
                oSheetSummary.Cells[iRowNum, 1] = "Summary of the Report";
                ((Microsoft.Office.Interop.Excel.Range)oSheetSummary.Cells[iRowNum++, 1]).Font.Bold = true;
                oSheetSummary.Cells[iRowNum++, 1] = "";
                oSheetSummary.Cells[iRowNum, 1] = "DSN Name ";
                oSheetSummary.Cells[iRowNum++, 2] = m_sDataSource;
                oSheetSummary.Cells[iRowNum, 1] = "Report Run By User ";
                oSheetSummary.Cells[iRowNum++, 2] = m_sLoginName;
                oSheetSummary.Cells[iRowNum, 1] = "Policy System";
                oSheetSummary.Cells[iRowNum++, 2] = sPolicySystemName;
                oSheetSummary.Cells[iRowNum, 1] = "Date of Loss Specified by User";
                if (m_sDateofClaim == "0")
                    oSheetSummary.Cells[iRowNum++, 2] = "Not Specified";
                else
                    oSheetSummary.Cells[iRowNum++, 2] = Conversion.GetDate(m_sDateofClaim);
                oSheetSummary.Cells[iRowNum, 1] = "Claim Type Specified by User";
                if (m_sClaimTypeCode == "0")
                    oSheetSummary.Cells[iRowNum++, 2] = "Not Specified";
                else
                {
                    string sShortcode = string.Empty;
                    string sDesc = string.Empty;
                    bool bout = false;
                    objcache.GetCodeInfo(Conversion.CastToType<int>(m_sClaimTypeCode, out bout), ref sShortcode, ref sDesc);
                    oSheetSummary.Cells[iRowNum++, 2] = sShortcode + "-" + sDesc;
                }
                oSheetSummary.Cells[iRowNum, 1] = "Excluded Master Co";
                oSheetSummary.Cells[iRowNum++, 2] = m_sExcludeThisMasterComp;
                oSheetSummary.Cells[iRowNum++, 1] = "";
                oSheetSummary.Cells[iRowNum, 1] = "Total Rows in Report";
                oSheetSummary.Cells[iRowNum++, 2] = iTotalRows;
                oSheetSummary.Cells[iRowNum, 1] = "Total White Rows(Pending Transactions)";
                oSheetSummary.Cells[iRowNum++, 2] = iTotalWhiteRows;
                oSheetSummary.Cells[iRowNum, 1] = "Total Red Rows(OOB Payments)";
                oSheetSummary.Cells[iRowNum++, 2] = iTotalRedRows;
                oSheetSummary.Cells[iRowNum, 1] = "Total Yellow Rows(OOB Reserves)";
                oSheetSummary.Cells[iRowNum++, 2] = iTotalYellowRows;
                oSheetSummary.Cells[iRowNum, 1] = "Total Green Rows(Unmapped Records)";
                oSheetSummary.Cells[iRowNum++, 2] = iTotalGreenRows;
                if (sShowClosedClaimsInGreyColor.ToUpper() == "TRUE")
                {
                    oSheetSummary.Cells[iRowNum, 1] = "Total Grey Rows(Closed Claims)";
                    oSheetSummary.Cells[iRowNum++, 2] = iTotalGreyRows;
                }
                ((Microsoft.Office.Interop.Excel.Range)oSheetSummary.Cells[1, 1]).EntireColumn.ColumnWidth = 40;
                ((Microsoft.Office.Interop.Excel.Range)oSheetSummary.Cells[1, 2]).EntireColumn.ColumnWidth = 30;
                #endregion                
                #region Prepare Instructions sheet
                Microsoft.Office.Interop.Excel._Worksheet oSheetInstruction = (Microsoft.Office.Interop.Excel._Worksheet)oWB.Sheets.Add(Type.Missing, Type.Missing, 1, Type.Missing);
                oSheetInstruction.Name = "Instructions";
                iRowNum = 1;
                oSheetInstruction.Cells[iRowNum++, 1] = "Key to understand row color";
                oSheetInstruction.Cells.EntireRow.Font.Bold = true;
                oSheetInstruction.Cells[iRowNum++, 1] = "";
                oSheetInstruction.Cells[iRowNum, 1] = "White";
                oSheetInstruction.Cells[iRowNum++, 2] = "In Balance/Pending Transactions";
                if (sShowClosedClaimsInGreyColor.ToUpper() == "TRUE")
                {
                    oSheetInstruction.Cells[iRowNum, 1] = "Grey";
                    ((Microsoft.Office.Interop.Excel.Range)oSheetInstruction.Cells[iRowNum, 1]).Interior.ColorIndex = iColorIndexGrey;
                    oSheetInstruction.Cells[iRowNum++, 2] = "Closed Claims";
                }
                oSheetInstruction.Cells[iRowNum, 1] = "Green";
                ((Microsoft.Office.Interop.Excel.Range)oSheetInstruction.Cells[iRowNum, 1]).Interior.ColorIndex = iColorIndexGreen;
                oSheetInstruction.Cells[iRowNum++, 2] = "Unmapped Records";
                oSheetInstruction.Cells[iRowNum, 1] = "Red";
                ((Microsoft.Office.Interop.Excel.Range)oSheetInstruction.Cells[iRowNum, 1]).Interior.ColorIndex = iColorIndexRed;
                oSheetInstruction.Cells[iRowNum++, 2] = "OOB Payments";
                oSheetInstruction.Cells[iRowNum, 1] = "Yellow";
                ((Microsoft.Office.Interop.Excel.Range)oSheetInstruction.Cells[iRowNum, 1]).Interior.ColorIndex = iColorIndexYellow;
                oSheetInstruction.Cells[iRowNum++, 2] = "OOB Reserves";
                ((Microsoft.Office.Interop.Excel.Range)oSheetInstruction.Cells[1, 1]).EntireColumn.ColumnWidth = 30;
                ((Microsoft.Office.Interop.Excel.Range)oSheetInstruction.Cells[1, 2]).EntireColumn.ColumnWidth = 30;
                #endregion

                oWB.SaveAs(string.Concat(sFolderPath, sfilename), Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
                Missing.Value, Missing.Value, Missing.Value, Missing.Value, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,
                Missing.Value, Missing.Value, Missing.Value,
                Missing.Value, Missing.Value);
                
                ////oWB.SaveAs(string.Concat(sFolderPath, sfilename), Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
                ////Missing.Value, Missing.Value,false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                ////Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlUserResolution,true,Missing.Value, 
                ////Missing.Value, Missing.Value);


                //sfilename = sfilename + ".xlsx";
                //oWB.SaveCopyAs(string.Concat(sFolderPath, sfilename));

                oWB.Close(Missing.Value, Missing.Value, Missing.Value);
                oSheet = null;
                oWB = null;
                oXL.Quit();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                if (File.Exists(sFolderPath + "\\" + sPolicySystemName + "_ClaimBalancing.xls"))
                {
                    if (!Directory.Exists(string.Concat(sFolderPath, "\\History")))
                    {
                        sbErrMsg.AppendLine(string.Concat(sFolderPath, "\\History") + " Folder not found");
                        throw new Exception(sbErrMsg.ToString());
                    }

                    File.Copy(sFolderPath + "\\" + sPolicySystemName + "_ClaimBalancing.xls", sFolderPath + "\\History\\" + sPolicySystemName + "_ClaimBalancing_" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls");
                }                
            }
            catch (Exception e)
            {
                //Ankit Start
                sbErrMsg.AppendLine(e.Message);
                objAppHelper.WriteErrorToFile(sbErrMsg.ToString(), e);
                //Ankit End
                throw e;
            }
            finally
            {
                oXL = null;
                oWB = null;
                oSheet = null;
                range = null;
                mWorkSheets = null;
                if (objcache != null)
                {
                    objcache.Dispose();
                    GC.SuppressFinalize(objcache);
                }
            }
        }
        private static int GetHeader(ref Microsoft.Office.Interop.Excel._Worksheet oSheet, AppHelper objAppHelper, StringBuilder sbErrMsg)   //Ankit Start Added objAppHelper & sbErrMsg parameters
        {
            int iColumns = 0;
            int iHeaderRowNumber = 1;
            try
            {
                oSheet.Cells[iHeaderRowNumber, 1] = "Comments";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 2] = "RMA_CLAIM_NUMBER";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 3] = "P_CLAIM_NUMBER";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 4] = "P_CLMTSEQ";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 5] = "P_POLCOVSEQ";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 6] = "P_RESVNO";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 7] = "RMA_PAID_TOTAL";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 8] = "RMA_CURRESV";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 9] = "RMA_PEND_PAY";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 10] = "RMA_PEND_PAY_DATE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 11] = "RMA_PEND_RESERVE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 12] = "RMA_PEND_RESERVE_DATE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 13] = "RMA_HOLD_RESERVE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 14] = "RMA_HOLD_RESERVE_DATE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 15] = "P_CURRESV";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 16] = "P_PAID_TOTAL";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 17] = "Claim OFFICE";
                iColumns++;
                //oSheet.Cells[2, 17] = "P_Policy LOB";
                //iColumns++;
                oSheet.Cells[iHeaderRowNumber, 18] = "P_CLAIMTYPE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 19] = "P_CLAIM STATUS";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 20] = "P_LOSSDTE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 21] = "DIFF_PAY";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 22] = "DIFF_RES";
                iColumns++;
                //oSheet.Cells[2, 23] = "RMA_Policy LOB";
                //iColumns++;
                oSheet.Cells[iHeaderRowNumber, 23] = "RMA_CLAIMTYPE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 24] = "RMA_CLAIM STATUS";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 25] = "RMA_RC_ROW_ID";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 26] = "RMA_PARENT_RES_TYPE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 27] = "P_LOSSCODE";
                iColumns++;
                oSheet.Cells[iHeaderRowNumber, 28] = "RMA_LOSS_CODE";
                iColumns++;
                //oSheet.Cells[1, 1] = "RMA_TRANS_ID";
                //iColumns++;
            }
            catch (Exception e)
            {
                //Ankit Start
                sbErrMsg.AppendLine("Error occured while adding headers to excel.");
                sbErrMsg.AppendLine(e.Message);
                objAppHelper.WriteErrorToFile(sbErrMsg.ToString(), e);
                throw e;
                //Ankit End
            }
            return iColumns;
        }
        //private static void GetDataForExport(DbReader p_ObjRdr, ref Microsoft.Office.Interop.Excel._Worksheet oSheet, int iRowIndex, int iColumns, AppHelper objAppHelper, StringBuilder sbErrMsg, LocalCache objcache)   //Ankit Start Added objAppHelper & sbErrMsg parameters
        private static void GetDataForExport(DbReader p_ObjRdr, ref Microsoft.Office.Interop.Excel._Worksheet oSheet, int iRowIndex, int iColumns, AppHelper objAppHelper, StringBuilder sbErrMsg, LocalCache objcache)   //Ankit Start Added objAppHelper & sbErrMsg parameters
        {
            #region variable declaration
            string sPolicySytemName = string.Empty;

            string sFolderPath = string.Empty;
            int colIndex = 0;
            //double iRsvAmount = 0;
            //double iPaidTotal = 0;
            //object cellText = null;
            string cellText = string.Empty;
            double iDiffPay = 0;
            double iDiffRes = 0;
            double iSumRes = 0;
            string sParentResType = string.Empty;
            double dPointCurResv = 0;
            double dDiffResWithCollectionInResvBal = 0;
            string sPointClaimNumber = string.Empty;
            string sRMAClaimNumber = string.Empty;
            double dRMACurResv = 0;
            bool bIsRecordNeverUploadedToPoint = false;
            string sPointClaimStatus = string.Empty;
            string sRMAClaimStatus = string.Empty;
            double dRMAPendReserve = 0;
            double dRMAHoldReserve = 0;
            double dRMAPendPay = 0;
            double dRMAPaidTotal = 0;
            double dPointPaidTotal = 0;
            double dRMACollectionTotal = 0;
            bool bRowColorDecided = false;
            int iColorIndex = 0;
            string sRMAParentResStatus = string.Empty;
            //int iLOBCode = 0;
            #endregion
            try
            {
                colIndex = 0;
                #region calculate diff pay

                // RMA_PAID_TOTAL is paid total if collection is 0
                // RMA_PAID_TOTAL is collection total if payment is 0
                // RMA_PAID_TOTAL is payment - collection total if both are not 0
                
                dRMAPendPay = Conversion.ConvertObjToDouble(p_ObjRdr.GetValue("RMA_PEND_PAY"),m_iClientId);//sonali
                dRMAPaidTotal = Conversion.ConvertObjToDouble(p_ObjRdr.GetValue("RMA_PAID_TOTAL"),m_iClientId);
                dPointPaidTotal = Conversion.ConvertObjToDouble(p_ObjRdr.GetValue("P_PAID_TOTAL"),m_iClientId);
                iDiffPay = dRMAPaidTotal - dRMAPendPay - dPointPaidTotal;
                iDiffPay = Math.Round(iDiffPay, 2);
                #endregion

                #region calculate diff resv

                iSumRes = Conversion.ConvertObjToDouble(p_ObjRdr.GetValue("SUMRESV"),m_iClientId);
                dPointCurResv = Conversion.ConvertObjToDouble(p_ObjRdr.GetValue("P_CURRESV"),m_iClientId);
                dRMACurResv = Conversion.ConvertObjToDouble(p_ObjRdr.GetValue("RMA_CURRESV"),m_iClientId);
                sParentResType = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("PARENT_RES_TYPE"));
                cellText = string.Empty;
                sPointClaimNumber = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("P_CLAIM_NUMBER"));
                sRMAClaimNumber = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("RMA_CLAIM_NUMBER"));
                sPointClaimStatus = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("P_CLAIMSTATUS"));
                sRMAClaimStatus = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("RMA_CLAIM_STATUS"));
                dRMAPendReserve = Conversion.ConvertObjToDouble(p_ObjRdr.GetValue("RMA_PEND_RESERVE"),m_iClientId);
                dRMAHoldReserve = Conversion.ConvertObjToDouble(p_ObjRdr.GetValue("RMA_HOLD_RESERVE"),m_iClientId);
                sRMAParentResStatus = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("PARENT_RES_STATUS"));
                if (sPointClaimNumber == string.Empty && sRMAParentResStatus.ToUpper() == "RE" && dRMAPaidTotal == 0)
                {
                    return;
                }

                if (sPointClaimNumber == string.Empty && (dRMAPendReserve != 0 || dRMAHoldReserve != 0 || dRMAPendPay != 0))
                {
                    //first check if a reserve is not uploaded to point. if point claim number is empty, we will assume that the record is not uploaded to point                    
                    iDiffPay = 0;
                    iDiffRes = 0;
                    bIsRecordNeverUploadedToPoint = true;
                }
                else if (iSumRes == 0)// Reserves sums up to 0 for recovery reserves
                {// we don't process RMA_PEND_PAY to compute reserve difference of recovery reserves.
                    iDiffRes = dRMACurResv - dRMAPendReserve - dRMAHoldReserve - dPointCurResv;
                    iDiffRes = Math.Round(iDiffRes, 2);
                    //CALCULATE diff reserve by including collection amount in reserve balance
                    dDiffResWithCollectionInResvBal = (dRMACurResv + dRMACollectionTotal) - dRMAPendReserve - dRMAHoldReserve - dPointCurResv;
                    dDiffResWithCollectionInResvBal = Math.Round(dDiffResWithCollectionInResvBal, 2);
                }
                else
                {
                    iDiffRes = dRMACurResv + dRMAPendPay - dRMAPendReserve - dRMAHoldReserve - dPointCurResv;
                    iDiffRes = Math.Round(iDiffRes, 2);
                    //CALCULATE diff reserve by including collection amount in reserve balance
                    dDiffResWithCollectionInResvBal = (dRMACurResv + dRMACollectionTotal) + dRMAPendPay - dRMAPendReserve - dRMAHoldReserve - dPointCurResv;
                    dDiffResWithCollectionInResvBal = Math.Round(dDiffResWithCollectionInResvBal, 2);
                }
                #endregion

                

                for (int i = 1; i <= iColumns; i++)
                {
                    #region prepare excel cell
                    //write in new column
                    switch (i)
                    {
                        case 1:
                            //if reserves are OOB(iDiffRes!=0) and reserves are not oob if we add up collection amount in reserve balance(dDiffResWithCollectionInResvBal000)
                            if (bIsRecordNeverUploadedToPoint)
                                cellText = "This claim has pending reserves/payments that are never uploaded to point.";
                            else if (iDiffRes != 0.0 && dDiffResWithCollectionInResvBal == 0.0)
                            {
                                cellText = "This reserve might be OOB because collections were not added up in reserve balance as the LOB setting \"Calculate Reserve Balance in Collections\" was not enabled in RMA.";
                            }
                            //if reserves are closed and reserve balance is zero in RMA and not in POINT or if reserve balance is zero in point and not in rma, then it might be 
                            //possible that the after save scripts were not run and therefor the reserve is OOB
                            else if ((sParentResType == "C" && iDiffRes != 0.0 && dPointCurResv == 0 && dRMACurResv != 0) || (sParentResType == "C" && iDiffRes != 0.0 && dPointCurResv != 0 && dRMACurResv == 0))
                            {
                                cellText = "This reserve is OOB because the reserve is closed and the after save scripts were not run to make the reserve amounts zero.";
                            }
                            else
                                cellText = string.Empty;
                            break;
                        case 2:
                            //sRMAClaimNumber = Conversion.ConvertObjToStr( objDR["RMA_CLAIM_NUMBER"));
                            cellText = string.Empty;
                            if (sRMAClaimNumber != string.Empty)
                            {
                                cellText = "'" + sRMAClaimNumber + "'";
                            }
                            break;
                        case 3:
                            //sPointClaimNumber = Conversion.ConvertObjToStr( objDR["P_CLAIM_NUMBER"));
                            cellText = string.Empty;
                            if (sPointClaimNumber != string.Empty)
                            {
                                cellText = "'" + sPointClaimNumber + "'";
                            }
                            break;
                        case 4:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("P_CLMTSEQ"));
                            break;
                        case 5:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("P_POLCOVSEQ"));
                            break;
                        case 6:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("P_RESVNO"));
                            break;
                        case 7:
                            cellText = dRMAPaidTotal.ToString();
                            break;
                        case 8:
                            cellText = dRMACurResv.ToString();
                            break;
                        case 9:
                            cellText = dRMAPendPay.ToString();
                            break;
                        case 10:
                            cellText = Conversion.GetDate(Conversion.ConvertObjToStr(p_ObjRdr.GetValue("RMA_PEND_PAY_DATE")));
                            break;
                        case 11:
                            cellText = dRMAPendReserve.ToString();
                            break;
                        case 12:
                            cellText = Conversion.GetDate(Conversion.ConvertObjToStr(p_ObjRdr.GetValue("RMA_PEND_RESERVE_DATE")));
                            break;
                        case 13:
                            cellText = dRMAHoldReserve.ToString();
                            break;
                        case 14:
                            cellText = Conversion.GetDate(Conversion.ConvertObjToStr(p_ObjRdr.GetValue("RMA_HOLD_RESERVE_DATE")));
                            break;
                        case 15:
                            cellText = dPointCurResv.ToString();
                            break;
                        case 16:
                            cellText = dPointPaidTotal.ToString();
                            break;
                        case 17:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("CLAIM_OFFICE"));
                            break;
                        case 18:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("P_CLAIMTYPE"));
                            break;
                        case 19:
                            cellText = sPointClaimStatus;
                            break;
                        case 20:
                            cellText = Conversion.GetDate(ConvertPointDateToRMA(Conversion.ConvertObjToStr(p_ObjRdr.GetValue("P_LOSSDTE"))));
                            break;
                        case 21:
                            cellText = iDiffPay.ToString();
                            break;
                        case 22:
                            cellText = iDiffRes.ToString();
                            break;
                        case 23:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("RMA_CLAIM_TYPE"));
                            break;
                        case 24:
                            cellText = sRMAClaimStatus;
                            break;
                        case 25:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("RMA_RC_ROW_ID"));
                            break;
                        case 26:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("PARENT_RES_TYPE"));
                            break;
                        case 27:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("P_LOSSCODE"));
                            break;
                        case 28:
                            cellText = Conversion.ConvertObjToStr(p_ObjRdr.GetValue("RMA_LOSS_SHORT_CODE"));
                            break;
                    }
                    #endregion

                    oSheet.Cells[iRowIndex, i] = cellText;
                    Microsoft.Office.Interop.Excel.Range range = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[iRowIndex, i];

                    #region decide color of the row
                    if (bRowColorDecided)
                    {
                        range.Interior.ColorIndex = iColorIndex;
                    }
                    else if (bIsRecordNeverUploadedToPoint)
                    {
                        bRowColorDecided = true;
                        iColorIndex = iColorIndexWhite;
                        //white row.comment is already defined above
                        iTotalWhiteRows++;
                    }
                    else
                    {
                        if (sShowClosedClaimsInGreyColor.ToUpper() == "TRUE")
                        {
                            string sRMAClaimStatusRelatedCode = objcache.GetShortCode(objcache.GetRelatedCodeId(objcache.GetCodeId(sRMAClaimStatus, "CLAIM_STATUS")));
                            if ((string.IsNullOrEmpty(sRMAClaimNumber) && (sPointClaimStatus == "C")) || (sRMAClaimStatusRelatedCode == "C") || (sRMAClaimStatusRelatedCode == "C" && sPointClaimStatus == "C"))
                            {
                                range.Interior.ColorIndex = iColorIndexGrey;//if record is closed, grey color
                                iColorIndex = iColorIndexGrey;
                                bRowColorDecided = true;
                                iTotalGreyRows++;
                            }
                        }
                        if (!bRowColorDecided && (string.IsNullOrEmpty(sRMAClaimNumber) || string.IsNullOrEmpty(sPointClaimNumber)))
                        {
                            //if row color is not decided yet and rma claim number is blank or point claim number is blank
                            range.Interior.ColorIndex = iColorIndexGreen;//Green, record is not mapped between rma and point
                            bRowColorDecided = true;
                            iColorIndex = iColorIndexGreen;
                            iTotalGreenRows++;
                        }
                        else if (!bRowColorDecided)
                        {
                            //row color is not decided yet
                            if (iDiffPay != 0.0)
                            {
                                range.Interior.ColorIndex = iColorIndexRed; //if payment id OOB ,red color
                                bRowColorDecided = true;
                                iColorIndex = iColorIndexRed;
                                iTotalRedRows++;
                            }
                            else if (iDiffRes != 0.0)
                            {
                                range.Interior.ColorIndex = iColorIndexYellow; //if reserves are OOB, yellow color
                                bRowColorDecided = true;
                                iColorIndex = iColorIndexYellow;
                                iTotalYellowRows++;
                            }
                            else
                            {
                                //white row, claim record exists on both rma and point but rma has some pending transactions and the record will be balanced when it will be uploaded to point
                                oSheet.Cells[iRowIndex, 1] = "RMA has pending reserves/payments.";
                                bRowColorDecided = true;
                                iColorIndex = iColorIndexWhite;
                                iTotalWhiteRows++;
                            }
                        }
                    }
                    #endregion
                }//end for loop

                
            }
            catch (Exception e)
            {
                //Ankit Start
                sbErrMsg.AppendLine("Could not export data to Excel.");
                sbErrMsg.AppendLine(e.Message);
                objAppHelper.WriteErrorToFile(sbErrMsg.ToString(), e);
                throw e;
                //Ankit End
            }
        }

        private static void GetPointConnStr(AppHelper objAppHelper, StringBuilder sbErrMsg)   //Ankit Start Added objAppHelper & sbErrMsg parameters
        {
            PolicySystemInterface objManager = null;
            string sDSN = string.Empty;
            string sUser = string.Empty;
            string sPwd = string.Empty;

            try
            {

                objManager = new PolicySystemInterface(m_sAdminDbConnstring, m_iClientId);
                
                //objManager.GetDataDSNDetails(Conversion.ConvertStrToInteger(sPolicySystemId), ref sDSN, ref sUser, ref sPwd);
                //sPTConnectionStr = "DSN=PIJ_SK;Uid=sqlinst1;Pwd=sqlinst1;";

                ///sPTConnectionStr = "DSN=BNRDP2DAT;Uid=LAB07;Pwd=sangeet#4;";


                string[] sDSNDetails = null;

                SetUpPolicySystem objPolInt = new SetUpPolicySystem(m_objUserLogin.LoginName, m_sAdminDbConnstring,m_iClientId);
                objPolInt.GetPolicyDSNDetails(ref sDSNDetails, Conversion.ConvertStrToInteger(sPolicySystemId));
                sPTConnectionStr = sDSNDetails[7];

            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Could not load  POINT DSN details ");
                //Ankit Start
                sbErrMsg.AppendLine("Could not load  POINT DSN details.");
                sbErrMsg.AppendLine(e.Message);
                objAppHelper.WriteErrorToFile(sbErrMsg.ToString(), e);
                //Ankit End
                throw e;
            }
            finally
            {
                objManager = null;
            }
        }

        private static string GetPolicySystemName(AppHelper objAppHelper, StringBuilder sbErrMsg)   //Ankit Start Added objAppHelper & sbErrMsg parameters
        {
            string sPolicySystemName = string.Empty;
            try
            {
                sPolicySystemName = Riskmaster.Common.Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sAdminDbConnstring, "SELECT POLICY_SYSTEM_NAME FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + sPolicySystemId));
  //              Console.WriteLine("policy system name " + sPolicySystemName);
            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Policy System not found ");
                //Ankit Start
                sbErrMsg.AppendLine("Policy System not found.");
                sbErrMsg.AppendLine(e.Message);
                objAppHelper.WriteErrorToFile(sbErrMsg.ToString(), e);
                //Ankit End
                throw e;
            }
            return sPolicySystemName;
        }


        //private static int GetRCRowId(int iClaimId, string sRMAClaimNum, string sCLMTSeq, string sPolCvgSeq, string sResvNo, string sPOINTClaimType)
        //{
        //    string sSQL = string.Empty;
        //    string sPTReserveType = string.Empty;
        //    int iRMAReserveTypeCode = 0;
        //    PolicySystemInterface objManager = null;
        //    int iRcRowId = 0;
        //    AppHelper objHelper = new AppHelper();
        //    LocalCache objLocalCache = null;
        //    try
        //    {
        //        sSQL = "SELECT RESVTYPE FROM TBCL006 WHERE CLAIMTYPE='" + sPOINTClaimType + "' AND RESVNO=" + sResvNo;
        //        sPTReserveType = Riskmaster.Common.Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(sPTConnectionStr, sSQL));
        //        if (string.IsNullOrEmpty(sPTReserveType))
        //        {
        //            objHelper.WriteErrorToFile("No Reserve Type found for Point Claim Type = " + sPOINTClaimType + " and ResvNo = " + sResvNo + " on point database. Claim Number " + sRMAClaimNum + " will not be added for balancing");
        //            return 0;
        //        }
        //        objManager = new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString);
        //        iRMAReserveTypeCode = objManager.GetRMXCodeIdFromPSMappedCode(sPTReserveType + "(" + sResvNo + ")", "RESERVE_TYPE", "ClaimBalancing.GetRcRowId", sPolicySystemId);
        //        if (iRMAReserveTypeCode == 0)
        //        {
        //            objHelper.WriteErrorToFile("No mapping found in RMA for Point Reserve Type = " + "(" + sResvNo + ")" + " Claim Number " + sRMAClaimNum + " will not be added for balancing");
        //            return 0;
        //        }
        //        sSQL = "SELECT RC_ROW_ID FROM RESERVE_CURRENT,CLAIM,CLAIMANT WHERE RESERVE_CURRENT.CLAIM_ID = " + iClaimId + " AND RESERVE_CURRENT.RESERVE_TYPE_CODE=" + iRMAReserveTypeCode + " AND CLAIMANT.CLAIMANT_EID= RESERVE_CURRENT.CLAIMANT_EID AND CLAIMANT.CLAIMANT_NUMBER= " + sCLMTSeq + " AND CLAIMANT.CLAIM_ID=CLAIM.CLAIM_ID AND RESERVE_CURRENT.CLAIM_ID = CLAIM.CLAIM_ID AND POLICY_CVG_SEQNO=" + sPolCvgSeq;
        //        iRcRowId = Riskmaster.Common.Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL));
        //        if (iRcRowId == 0)
        //        {
        //            objLocalCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString);
        //            objHelper.WriteErrorToFile("No reserve found in RMA for claim number= " + sRMAClaimNum + ", RMA Reserve Type = " + objLocalCache.GetCodeDesc(iRMAReserveTypeCode) + ", ClaimSeqNo=" + sCLMTSeq + "POLICY_CVG_SEQNO" + sPolCvgSeq + ". Claim number" + sRMAClaimNum + " will not be added for balancing");
        //            return 0;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("0 ^*^*^ Error while getting reserves for claim  " + iClaimId + " and reserve number  " + sResvNo);
        //        throw e;
        //    }
        //    finally
        //    {
        //        objManager = null;
        //        objHelper = null;
        //        if (objLocalCache != null)
        //            objLocalCache.Dispose();
        //    }

        //    return iRcRowId;
        //}
        //private static int GetSplitRowId(int iRcRowId, string sPOINTTransType, string sCtlNumber)
        //{
        //    PolicySystemInterface objManager = null;
        //    int iTransTypeCode = 0;
        //    int iSplitRowId = 0;
        //    string sSQL = string.Empty;
        //    try
        //    {
        //        objManager = new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString);
        //        iTransTypeCode = objManager.GetRMXCodeIdFromPSMappedCode(sPOINTTransType, "TRANS_TYPES", "ClaimBalancing.GetSplitRowId", sPolicySystemId);

        //        //sSQL = "SELECT SPLIT_ROW_ID FROM FUNDS_TRANS_SPLIT,FUNDS  WHERE RC_ROW_ID = " + iRcRowId + " AND TRANS_TYPE_CODE=" + iTransTypeCode + " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.CTL_NUMBER='"+sCtlNumber+"'";
        //        sSQL = "SELECT TRANS_ID FROM FUNDS_TRANS_SPLIT WHERE RC_ROW_ID = " + iRcRowId;
        //        iSplitRowId = Riskmaster.Common.Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL));
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("0 ^*^*^ Error while getting financial " + sCtlNumber);
        //        throw e;
        //    }
        //    finally
        //    {
        //        objManager = null;
        //    }
        //    return iSplitRowId;
        //}


        //private static int GetTransId(int iRcRowId)
        //{
        //    int iTransId = 0;
        //    string sSQL = string.Empty;
        //    try
        //    {
        //        sSQL = "SELECT TOP 1 TRANS_ID FROM FUNDS_TRANS_SPLIT WHERE RC_ROW_ID = " + iRcRowId;
        //        iTransId = Riskmaster.Common.Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL));
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("0 ^*^*^ Error while getting TRANS ID ");
        //        throw e;
        //    }
        //    return iTransId;
        //}


        //private static int GetTransId(string sCtlNumber)
        //{
        //    int iTranId = 0;
        //    string sSQL = string.Empty;
        //    try
        //    {

        //        sSQL = "SELECT TRANS_ID FROM FUNDS  WHERE CTL_NUMBER = '" + sCtlNumber + "'";
        //        iTranId = Riskmaster.Common.Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL));
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("0 ^*^*^ Error while getting transaction  " + sCtlNumber);
        //        throw e;
        //    }
        //    return iTranId;
        //}


        private static void InsertIntoRmaClaimBalancingTable(DbReader objDataRdr, AppHelper objAppHelper, StringBuilder sbErrMsg)   //Ankit Start Added objAppHelper & sbErrMsg parameters
        {
            string sSQL = string.Empty;
            DbWriter dbWriter = null;
            int iRowId = 0;
            //int iSplitRowId = 0;
            //int iRcRowId = 0;
            string sPointClaimNum = string.Empty;
            //bool breturn = false;
            string sClaimTrans = string.Empty;
            string sClmtSeq = string.Empty;
            string sPolCovSeq = string.Empty;
            string sResNo = string.Empty;
            string sClaimType = string.Empty;
            string sPaymentTyp = string.Empty;
            string sItemNo = string.Empty;
            string sTransSeqNum = string.Empty;
            //int iRMAClaimId = 0;
            string sRMAClaimNum = string.Empty;
            //int iTransId = 0;
            string sRMAPendingPayDate = string.Empty;
            string sRMAPendingReserveDate = string.Empty;
            string sClaimStatus = string.Empty;
            string sPointRecStatus = string.Empty;
            //PolicySystemInterface objManager = null;            
            //int iRMACLaimTypeCode = 0;
            //int iRMACLaimStatusCode = 0;
            try
            {
                sPointClaimNum = Conversion.ConvertObjToStr(objDataRdr.GetValue("CLAIM"));

                //sSQL = "SELECT CLAIM_ID,CLAIM_NUMBER,CLAIM_TYPE_CODE,CLAIM_STATUS_CODE  FROM CLAIM WHERE CLAIM_NUMBER LIKE '%" + sPointClaimNum.TrimStart('0') + "'";
                //using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                //{
                //    if (objReader.Read())
                //    {
                //        iRMAClaimId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"));
                //        sRMAClaimNum = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER"));
                //        iRMACLaimTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_TYPE_CODE"));
                //        iRMACLaimStatusCode = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_STATUS_CODE"));
                //    }
                //}
                //if (objDataRdr != null && objDataRdr.Read())
                //{
                sClaimTrans = Conversion.ConvertObjToStr(objDataRdr.GetValue("CLAIMTRANS"));
                sClmtSeq = Conversion.ConvertObjToStr(objDataRdr.GetValue("CLMTSEQ"));
                sPolCovSeq = Conversion.ConvertObjToStr(objDataRdr.GetValue("POLCOVSEQ"));
                sResNo = Conversion.ConvertObjToStr(objDataRdr.GetValue("RESVNO"));
                sClaimType = Conversion.ConvertObjToStr(objDataRdr.GetValue("CLAIMTYPE"));
                sTransSeqNum = Conversion.ConvertObjToStr(objDataRdr.GetValue("TRANSSEQ"));
                sPaymentTyp = Conversion.ConvertObjToStr(objDataRdr.GetValue("PAYMENTTYP"));
                sPointRecStatus = Conversion.ConvertObjToStr(objDataRdr.GetValue("RECSTATUS"));                
                //sItemNo = Conversion.ConvertObjToStr(objDataRdr.GetValue("ITEMNO"));
                //}


                //if (string.Equals(sClaimTrans, "NC") || string.Equals(sClaimTrans, "OC") || string.Equals(sClaimTrans, "RO") || string.Equals(sClaimTrans, "CL") || string.Equals(sClaimTrans.Trim(), "RC"))
                //    iRcRowId = GetRCRowId(iRMAClaimId, sRMAClaimNum, sClmtSeq, sPolCovSeq, sResNo, sClaimType);
                //else if (string.Equals(sClaimTrans, "OA") || string.Equals(sClaimTrans, "PP") || string.Equals(sClaimTrans, "FF") || string.Equals(sClaimTrans, "FP") || string.Equals(sClaimTrans, "SP"))
                //{
                //    iRcRowId = GetRCRowId(iRMAClaimId,sRMAClaimNum, sClmtSeq, sPolCovSeq, sResNo, sClaimType);
                //    if (iRcRowId > 0)
                //        //iSplitRowId = GetSplitRowId(iRcRowId, sPaymentTyp, sItemNo);
                //        iTransId = GetTransId(iRcRowId);                   
                //}
                //else
                //    return;

                //sSQL = "SELECT ROW_ID FROM CLAIM_BALANCING WHERE P_CLAIM_NUMBER  = '" + sPointClaimNum + "' AND P_CLMTSEQ = " + sClmtSeq + " AND P_POLCOVSEQ =" + sPolCovSeq + " AND P_RESVNO   = " + sResNo + "   AND P_TRANS_SEQ = " + sTransSeqNum;
                //iRowId = Riskmaster.Common.Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sAdminDbConnstring, sSQL));
                
                dbWriter = DbFactory.GetDbWriter(m_sAdminDbConnstring);
                dbWriter.Tables.Add("CLAIM_BALANCING");
                dbWriter.Fields.Add("CLAIM_NUMBER", sRMAClaimNum);
                //dbWriter.Fields.Add("CLAIM_ID", iRMAClaimId);
                dbWriter.Fields.Add("P_CLAIM_NUMBER", sPointClaimNum);
                dbWriter.Fields.Add("P_CLMTSEQ", sClmtSeq);
                dbWriter.Fields.Add("P_POLCOVSEQ", sPolCovSeq);
                dbWriter.Fields.Add("P_RESVNO", sResNo);
                dbWriter.Fields.Add("P_PAID_TOTAL", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("TOTPAYMENT")));
                dbWriter.Fields.Add("P_CURRESV", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("CURRRESV")));
                dbWriter.Fields.Add("P_OFFICE", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("CLMOFFICE")));
                dbWriter.Fields.Add("P_TRANS_SEQ", sTransSeqNum);
                //dbWriter.Fields.Add("RC_ROW_ID", iRcRowId);
                //dbWriter.Fields.Add("POLICY_SYSTEM_ID", sPolicySystemId);
                //dbWriter.Fields.Add("RMA_PEND_PAY", GetRMAPendingPay(iRcRowId, out sRMAPendingPayDate));
                //dbWriter.Fields.Add("RMA_PEND_PAY_DATE", sRMAPendingPayDate);
                //dbWriter.Fields.Add("RMA_PEND_RESERVE", GetRMAPendingReserve(iRcRowId, out sRMAPendingReserveDate));
                //dbWriter.Fields.Add("RMA_PEND_RESERVE_DATE", sRMAPendingReserveDate);
                dbWriter.Fields.Add("P_CLAIMTYPE", sClaimType);
                dbWriter.Fields.Add("P_CLAIM_STATUS", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("RECSTATUS")));
                dbWriter.Fields.Add("P_LOSSCODE", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("LOSSCAUSE")));
                dbWriter.Fields.Add("P_RESVTYPE", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("RESVTYPE")));
                dbWriter.Fields.Add("P_LOSSDATE", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("LOSSDTE")));
                dbWriter.Fields.Add("P_RECSTATUS", Conversion.ConvertObjToStr(objDataRdr.GetValue("RECSTATUS")));
                //objCache = new LocalCache(m_sAdminDbConnstring);
                //dbWriter.Fields.Add("RMA_CLAIM_TYPE", objCache.GetShortCode(iRMACLaimTypeCode));
                dbWriter.Fields.Add("P_MASTERCO", Conversion.ConvertObjToStr(objDataRdr.GetValue("MASTERCO")));
                //dbWriter.Fields.Add("EXPORTED_TO_EXCEL", 0);

                //if (iRowId == 0)
                //{
                iRowId = Riskmaster.Common.Utilities.GetNextUID(m_sAdminDbConnstring, "CLAIM_BALANCING",m_iClientId);//sonali
                dbWriter.Fields.Add("ROW_ID", iRowId);
                dbWriter.Fields.Add("DTTM_ADDED", System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                //}
                //else
                //    dbWriter.Where.Add("ROW_ID = " + iRowId);
                //dbWriter.Fields.Add("TRANS_ID", iTransId);

                dbWriter.Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Error while Inserting data into Claim Balancing table ");
                //Ankit Start
                sbErrMsg.AppendLine("Error while Inserting data into Claim Balancing table");
                sbErrMsg.AppendLine(e.Message);
                objAppHelper.WriteErrorToFile(sbErrMsg.ToString(), e);
                //Ankit End
                throw e;
            }
            finally
            {
                dbWriter = null;
            }
        }

        //private static void InsertRMAClaimsIntoClaimsBalancingTable()
        //{

        //}

        //private static void InsertIntoRmaCheckBalancingTable(DbReader objDataRdr)
        //{
        //    string sSQL = string.Empty;
        //    DbWriter dbWriter = null;
        //    int iRowId = 0;
        //    int iTransId = 0;
        //    try
        //    {

        //        iTransId = GetTransId(Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("ITEMNO")));

        //        sSQL = "SELECT ROW_ID FROM CHECK_BALANCING WHERE POINT_CTL_NUMBER  = '" + Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("ITEMNO")) + "'";
        //        iRowId = Riskmaster.Common.Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL));
        //        dbWriter = DbFactory.GetDbWriter(m_objUserLogin.objRiskmasterDatabase.ConnectionString);

        //        dbWriter.Tables.Add("CHECK_BALANCING");
        //        dbWriter.Fields.Add("POLICY_SYSTEM_ID", sPolicySystemId);
        //        dbWriter.Fields.Add("P_BANKCODE", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("BANKCODE")).TrimStart('0'));
        //        dbWriter.Fields.Add("POINT_CHECKNO", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("CHECKNO")));
        //        dbWriter.Fields.Add("POINT_CTL_NUMBER", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("ITEMNO")));
        //        dbWriter.Fields.Add("POINT_CHECKDATE", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("DRFTPRTDTE")));
        //        dbWriter.Fields.Add("POINT_TRANS_DATE", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("CRTDATE")));
        //        dbWriter.Fields.Add("CLAIM", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("CLAIM")).TrimStart('0'));

        //        dbWriter.Fields.Add("P_PAYEENAME1", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("PAYEENAME1")));
        //        dbWriter.Fields.Add("P_PAYEENAME2", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("PAYEENAME2")));
        //        dbWriter.Fields.Add("P_PAYEENAME3", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("PAYEENAME3")));
        //        dbWriter.Fields.Add("P_MAILTO", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("MAILTONAME")));

        //        dbWriter.Fields.Add("P_AMOUNT", Riskmaster.Common.Conversion.ConvertObjToStr(objDataRdr.GetValue("DRFTAMT")));
        //        dbWriter.Fields.Add("TRANS_ID", iTransId);
        //        if (iRowId == 0)
        //        {
        //            iRowId = Riskmaster.Common.Utilities.GetNextUID(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "CHECK_BALANCING");
        //            dbWriter.Fields.Add("ROW_ID", iRowId);
        //        }
        //        else
        //            dbWriter.Where.Add("ROW_ID = " + iRowId);

        //        dbWriter.Execute();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("0 ^*^*^ Error while saving check Balancing ");
        //        throw e;
        //    }
        //}

        private static string ConvertPointYear(string strPointYr)
        {
            string strTemp = string.Empty;
            if (string.Equals(strPointYr.Substring(0, 1), "1")) //If StrComp(Left(strPointYr, 1), "1") = 0 
                return "20" + strPointYr.Substring(strPointYr.Length - 2 - 1);// Right(strPointYr, 2)
            else
                return "19" + strPointYr.Substring(strPointYr.Length - 2);// Right(strPointYr, 2)
        }

        private static string ConvertYearToPoint(string strPointYr)
        {
            string strTemp = string.Empty;
            if (string.Equals(strPointYr.Substring(0, 1), "2"))  //if StrComp(Left(strPointYr, 1), "2") = 0 Then
                strTemp = "1" + strPointYr.Substring(2, 2); // Mid(strPointYr, 3, 2)
            else
                strTemp = "0" + strPointYr.Substring(2, 2);// Mid(strPointYr, 3, 2)

            return strTemp;
        }

        private static string ConvertDateToPoint(string strPointYYYYMMDD)
        {
            string sYYYY = string.Empty;
            string sCYY = string.Empty;
            string sReturnString = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(strPointYYYYMMDD) == false && strPointYYYYMMDD != "0")
                {
                    if (Conversion.GetDate(strPointYYYYMMDD) == string.Empty)
                        throw new Exception();
                    sYYYY = strPointYYYYMMDD.Substring(0, 4);// Left(strPointYYYYMMDD, 4)
                    sCYY = ConvertYearToPoint(sYYYY);
                    sReturnString = sCYY + strPointYYYYMMDD.Substring(strPointYYYYMMDD.Length - 4); // Right(strPointYYYYMMDD, 4)
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Invalid input parameter : Date of Loss. Date should be in YYYYMMDD format.");
                throw new Exception("Invalid input parameter : Date of Loss. Date should be in YYYYMMDD format.");
            }
            return sReturnString;
        }

        private static string ConvertPointDateToRMA(string sPointYYMMDD)
        {
            string strTemp = string.Empty;
            try
            {                
                if (!string.IsNullOrEmpty(sPointYYMMDD))
                {
                    if (string.Equals(sPointYYMMDD.Substring(0, 1), "1"))  //if StrComp(Left(strPointYr, 1), "2") = 0 Then
                        strTemp = "20" + sPointYYMMDD.Substring(1, 2); // Mid(strPointYr, 3, 2)
                    else
                        strTemp = "19" + sPointYYMMDD.Substring(1, 2);// Mid(strPointYr, 3, 2)

                    strTemp = strTemp + sPointYYMMDD.Substring(3);
                }
            }
            catch (Exception e)
            {
                //if point loss date is not in correct format then we will return it as it is
                strTemp = sPointYYMMDD;
            }
            return strTemp;
        }

        //private static double GetRMAPendingPay(int iRCRowID, out string sRMAPendingPayDate)
        //{
        //    double dRMAPendingPay = 0;
        //    sRMAPendingPayDate = string.Empty;
        //    string sSQL = "SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) RMA_PENDING_PAY ,MIN(ACTIVITY_TRACK.DTTM_RCD_ADDED) RMA_PENDING_PAY_DATE FROM FUNDS_TRANS_SPLIT,ACTIVITY_TRACK WHERE FUNDS_TRANS_SPLIT.RC_ROW_ID=" + iRCRowID + " AND ACTIVITY_TRACK.FOREIGN_TABLE_KEY = FUNDS_TRANS_SPLIT.TRANS_ID AND ACTIVITY_TRACK.FOREIGN_TABLE_ID=85 AND UPLOAD_FLAG=-1";
        //    using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
        //    {
        //        if (objReader.Read())
        //        {
        //            dRMAPendingPay = Conversion.ConvertObjToDouble(objReader.GetValue("RMA_PENDING_PAY"));
        //            sRMAPendingPayDate = Conversion.ConvertObjToStr(objReader.GetValue("RMA_PENDING_PAY_DATE"));
        //        }
        //    }
        //    return dRMAPendingPay;
        //}

        //private static double GetRMAPendingReserve(int iRCRowID, out string sRMAPendingReserveDate)
        //{
        //    double dRMAPendingReserve = 0;
        //    sRMAPendingReserveDate = string.Empty;
        //    string sSQL = "SELECT SUM(RESERVE_CURRENT.RESERVE_AMOUNT)RMA_PENDING_RESERVE ,MIN(ACTIVITY_TRACK.DTTM_RCD_ADDED) RMA_PENDING_RESERVE_DATE FROM RESERVE_CURRENT,ACTIVITY_TRACK WHERE RESERVE_CURRENT.RC_ROW_ID=" + iRCRowID + " AND ACTIVITY_TRACK.FOREIGN_TABLE_KEY = RESERVE_CURRENT.RC_ROW_ID AND ACTIVITY_TRACK.FOREIGN_TABLE_ID=74 AND UPLOAD_FLAG=-1";
        //    using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
        //    {
        //        if (objReader.Read())
        //        {
        //            dRMAPendingReserve = Conversion.ConvertObjToDouble(objReader.GetValue("RMA_PENDING_RESERVE"));
        //            sRMAPendingReserveDate = Conversion.ConvertObjToStr(objReader.GetValue("RMA_PENDING_RESERVE_DATE"));
        //        }
        //    }
        //    return dRMAPendingReserve;
        //}

    }
}
