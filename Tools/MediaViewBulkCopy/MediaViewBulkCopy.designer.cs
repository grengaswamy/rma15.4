namespace Riskmaster.Tools.MediaViewBulkCopy
{
    partial class FormMediaViewBulkCopy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label20 = new System.Windows.Forms.Label();
            this.txtDocPath = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDocPathType = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtRemainingFilesCount = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnStartTransfer = new System.Windows.Forms.Button();
            this.txtProgressStatusMessage = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDocsCount = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTotalSize = new System.Windows.Forms.TextBox();
            this.bgWorkerPopulateFileinfo = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.btnPauseResume = new System.Windows.Forms.Button();
            this.bgWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(394, 26);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(114, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "Global Document Path";
            // 
            // txtDocPath
            // 
            this.txtDocPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDocPath.Location = new System.Drawing.Point(530, 23);
            this.txtDocPath.Name = "txtDocPath";
            this.txtDocPath.ReadOnly = true;
            this.txtDocPath.Size = new System.Drawing.Size(250, 20);
            this.txtDocPath.TabIndex = 23;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 26);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(108, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "Document Path Type";
            // 
            // txtDocPathType
            // 
            this.txtDocPathType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDocPathType.Location = new System.Drawing.Point(139, 23);
            this.txtDocPathType.Name = "txtDocPathType";
            this.txtDocPathType.ReadOnly = true;
            this.txtDocPathType.Size = new System.Drawing.Size(249, 20);
            this.txtDocPathType.TabIndex = 21;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(394, 52);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = "Files processed";
            // 
            // txtRemainingFilesCount
            // 
            this.txtRemainingFilesCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRemainingFilesCount.Location = new System.Drawing.Point(530, 49);
            this.txtRemainingFilesCount.Name = "txtRemainingFilesCount";
            this.txtRemainingFilesCount.ReadOnly = true;
            this.txtRemainingFilesCount.Size = new System.Drawing.Size(250, 20);
            this.txtRemainingFilesCount.TabIndex = 19;
            this.txtRemainingFilesCount.Text = "0";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.progressBar1, 4);
            this.progressBar1.Location = new System.Drawing.Point(3, 125);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(777, 23);
            this.progressBar1.TabIndex = 2;
            // 
            // btnStartTransfer
            // 
            this.btnStartTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartTransfer.Location = new System.Drawing.Point(689, 165);
            this.btnStartTransfer.Name = "btnStartTransfer";
            this.btnStartTransfer.Size = new System.Drawing.Size(97, 32);
            this.btnStartTransfer.TabIndex = 18;
            this.btnStartTransfer.Text = "Start";
            this.btnStartTransfer.UseVisualStyleBackColor = true;
            this.btnStartTransfer.Click += new System.EventHandler(this.btnStartTransfer_Click);
            // 
            // txtProgressStatusMessage
            // 
            this.txtProgressStatusMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProgressStatusMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tableLayoutPanel1.SetColumnSpan(this.txtProgressStatusMessage, 2);
            this.txtProgressStatusMessage.Location = new System.Drawing.Point(3, 101);
            this.txtProgressStatusMessage.Name = "txtProgressStatusMessage";
            this.txtProgressStatusMessage.ReadOnly = true;
            this.txtProgressStatusMessage.Size = new System.Drawing.Size(385, 13);
            this.txtProgressStatusMessage.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Total files count";
            // 
            // txtDocsCount
            // 
            this.txtDocsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDocsCount.Location = new System.Drawing.Point(139, 49);
            this.txtDocsCount.Name = "txtDocsCount";
            this.txtDocsCount.ReadOnly = true;
            this.txtDocsCount.Size = new System.Drawing.Size(249, 20);
            this.txtDocsCount.TabIndex = 15;
            this.txtDocsCount.Text = "0";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 78);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(128, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "Total size (to be migrated)";
            // 
            // txtTotalSize
            // 
            this.txtTotalSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalSize.Location = new System.Drawing.Point(139, 75);
            this.txtTotalSize.Name = "txtTotalSize";
            this.txtTotalSize.ReadOnly = true;
            this.txtTotalSize.Size = new System.Drawing.Size(249, 20);
            this.txtTotalSize.TabIndex = 10;
            this.txtTotalSize.Text = "0";
            this.txtTotalSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // bgWorkerPopulateFileinfo
            // 
            this.bgWorkerPopulateFileinfo.WorkerReportsProgress = true;
            this.bgWorkerPopulateFileinfo.WorkerSupportsCancellation = true;
            this.bgWorkerPopulateFileinfo.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerPopulateFileinfo_DoWork);
            this.bgWorkerPopulateFileinfo.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorkerPopulateFileinfo_reportProgress);
            this.bgWorkerPopulateFileinfo.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerPopulateFileinfo_RunWorkerCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 218);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(811, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(796, 17);
            this.toolStripStatusLabel2.Spring = true;
            this.toolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanelMain.SetColumnSpan(this.tableLayoutPanel1, 2);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 136F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 136F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label21, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtDocPathType, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label20, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtDocPath, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtRemainingFilesCount, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label19, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtTotalSize, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtDocsCount, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.progressBar1, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtProgressStatusMessage, 0, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(783, 156);
            this.tableLayoutPanel1.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Doc details";
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.btnStartTransfer, 1, 1);
            this.tableLayoutPanelMain.Controls.Add(this.btnPauseResume, 0, 1);
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(12, 9);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 3;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 162F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(789, 200);
            this.tableLayoutPanelMain.TabIndex = 26;
            // 
            // btnPauseResume
            // 
            this.btnPauseResume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPauseResume.Enabled = false;
            this.btnPauseResume.Location = new System.Drawing.Point(294, 165);
            this.btnPauseResume.Name = "btnPauseResume";
            this.btnPauseResume.Size = new System.Drawing.Size(97, 32);
            this.btnPauseResume.TabIndex = 24;
            this.btnPauseResume.Text = "Pause";
            this.btnPauseResume.UseVisualStyleBackColor = true;
            this.btnPauseResume.Click += new System.EventHandler(this.btnPauseResume_Click);
            // 
            // bgWorkerMain
            // 
            this.bgWorkerMain.WorkerReportsProgress = true;
            this.bgWorkerMain.WorkerSupportsCancellation = true;
            this.bgWorkerMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerMain_DoWork);
            this.bgWorkerMain.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorkerMain_ProgressChanged);
            this.bgWorkerMain.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerMain_RunWorkerCompleted);
            // 
            // FormMediaViewBulkCopy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(811, 240);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Controls.Add(this.statusStrip1);
            this.MaximumSize = new System.Drawing.Size(1024, 269);
            this.MinimumSize = new System.Drawing.Size(600, 269);
            this.Name = "FormMediaViewBulkCopy";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "MediaView Bulk Copy";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMediaView_FormClosing);
            this.Load += new System.EventHandler(this.frmMediaView_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn Text2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnStartTransfer;
        private System.Windows.Forms.TextBox txtProgressStatusMessage;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDocsCount;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTotalSize;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtRemainingFilesCount;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDocPath;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtDocPathType;
        private System.ComponentModel.BackgroundWorker bgWorkerPopulateFileinfo;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.Label label5;
        private System.ComponentModel.BackgroundWorker bgWorkerMain;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Button btnPauseResume;
    }
}

