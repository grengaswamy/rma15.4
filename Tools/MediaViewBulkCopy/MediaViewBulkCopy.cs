using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Timers;
//using Riskmaster.DataModel;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.Common.Win32;
using Riskmaster.Db;
using Riskmaster.Application.FileStorage;
using Riskmaster.Common;
using Riskmaster.Application.MediaViewWrapper;
using Riskmaster.Settings;
using Oracle.DataAccess;
using System.Configuration;

namespace Riskmaster.Tools.MediaViewBulkCopy
{
    /**************************************************************
	 * $File		: frmMediaView.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 04/01/2008
	 * $Author		: Rahul Solanki 
	 * $Comment		: The tool migrates the docs attached(and mail merge templates) into MediaView. 	 
	*************************************************************   
    *	The tool can either be run directly or can be executed via a bacth file/installer in silent mode.
    *   Command line parameters-
    *  Riskmaster.Tools.MediaView.exe uid pwd DSN
    * 
    *  NOTE: DSN is Case sensitive.    
    */

    public partial class FormMediaViewBulkCopy : Form
    {        
        private Login m_objLogin = new Login();                
        private Riskmaster.Db.DbConnection m_objDbConnection ;

        private DbReader m_objDbReaderRetriveRc = null;

        public static  TextWriter m_sw1;   
        //public static  StreamWriter m_sw1;   
               
        private string m_nowDateTime = Conversion.ToDbDate(System.DateTime.Now);
        private string m_nowTime = Conversion.ToDbTime(System.DateTime.Now);
        private static Boolean m_bIsDebugMode = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["debug"]);
        private static int m_iresetIISInterval = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["resetIISInterval"]);
        private ManualResetEvent locker = new ManualResetEvent(true);

        private static System.Timers.Timer aTimer;
        
       
     //   DataModelFactory m_objDataModelFactory = null;
        RMConfigurator objConfig = null;
        FileStorageManager objFileStorageManager = null;
        FileStorageManager objFileStorageManagerIndividualUser = null;

        StorageType m_enmDocumentStorageType = 0;
        string m_sDestinationStoragePath = string.Empty;

        //  -1 = Transmitted
        //   0 or null = Not transmitted

        public FormMediaViewBulkCopy()
        {
            InitializeComponent(); 
        }
        
        
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            //Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime);

            locker.Set();

            Process pr = new Process();
            pr.StartInfo.FileName = "iisreset";
            //pr.StartInfo.Arguments = "test.dat";
            pr.Start();
            pr.WaitForExit();
            //while (pr.HasExited == false)
            //    if ((DateTime.Now.Second % 5) == 0)
            //    { // Show a tick every five seconds.
            //        System.Threading.Thread.Sleep(1000);
            //    }
            locker.Reset();

        }

        private void frmMediaView_Load(object sender, EventArgs e)
        {  
            if (AppGlobals.bSilentMode)
            {
                this.Hide();
            }
            try
            {
                m_sw1 = new StreamWriter("MediaView.log", true);                
            }
            catch (Exception exp)
            {
                //writeLog(string.Format("ERROR:[Log file Open error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            if (AppGlobals.bSilentMode)
            {
                writeLog("Running in silent mode; DSN: " + AppGlobals.sDSN);
            }
            writeLog("-----------------------------");
            writeLog("Form Loaded... authenticating");
            writeLog("-----------------------------");

            Boolean bCon = false;
            try
            {
                if (!AppGlobals.bSilentMode)
                {
                    bCon = m_objLogin.RegisterApplication(this.Handle.ToInt32(), 0, ref AppGlobals.Userlogin);
                }

                bool bConversionSuccess = false;
                string sDummyFileMode = System.Configuration.ConfigurationManager.AppSettings["UseDummyFiles"];
                AppGlobals.bUseDummyFiles = Conversion.CastToType<Boolean>(sDummyFileMode, out bConversionSuccess);
                
                

                if (!AppGlobals.bSilentMode && !bCon)
                {
                    // for invalid Login
                    if (m_objLogin != null) m_objLogin.Dispose();
                    writeLog("Invalid Logon. terminating");
                    System.Windows.Forms.Application.Exit();
                }
                else
                {                    
                    writeLog("Authentication successful");
                    AppGlobals.ConnectionString = AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString;
                    m_objDbConnection = DbFactory.GetDbConnection(AppGlobals.ConnectionString);                    

                    txtDocPathType.Text = (Convert.ToBoolean(AppGlobals.Userlogin.objRiskmasterDatabase.DocPathType))?"Database":"File System";
                    txtDocPath.Text = AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath;
                    m_enmDocumentStorageType = StorageType.FileSystemStorage;
                    m_sDestinationStoragePath = string.Empty;

                    // checking to see if MediaView is ON or NOT
                    writeLog("checking to see if MediaView is Enabled");
                    SysSettings oSettings = new SysSettings(AppGlobals.ConnectionString);


                    if (!oSettings.UseMediaViewInterface)
                    { 
                        // throw error if MediaView is disabled & exit.
                        if (!AppGlobals.bSilentMode)
                        {
                            MessageBox.Show("MediaView is disabled; \nDoc migration can continue..."); 
                        }
                        writeLog("MediaView is disabled; cannot proceed");
                        //todo r5
                        //System.Windows.Forms.Application.Exit();                        
                    }
                    writeLog("MediaView is Enabled.");


                    // Loading Document details...
                    setStatus("Loading Documents info...");
                    writeLog("Loading Documents info...");
                    PopulateDocInfoGrid();

                    AppGlobals.m_dsDocs.Tables[0].Columns.Add("Size",typeof(long));
                    AppGlobals.m_dsMergeDocs.Tables[0].Columns.Add("Size",typeof(long));
                    
                    setStatus("Click on the 'Start' button to start porting Documents to MediaView...");
                    
                    if (AppGlobals.bUseDummyFiles || this.m_enmDocumentStorageType.ToString()!="0" )
                    {
                        // hiding the fields for "total file size" in case dummy file transfer is used
                        txtTotalSize.Hide();
                        label15.Hide();
                    }

                    //retriving file size info
                    writeLog("Loading Documents size info...");
                    bgWorkerPopulateFileinfo.RunWorkerAsync();
                                        
                    

                    if (AppGlobals.bSilentMode)
                    {
                        btnStartTransfer_Click(null, null);
                    }
                }                
                if (AppGlobals.bSilentMode)
                {
                    this.Hide();
                    System.Windows.Forms.Application.Exit();
                }
            }
            catch (Exception ex)
            {
                if (!AppGlobals.bSilentMode)
                {
                    writeLog(string.Format("ERROR:[form Load error] \nmessage:{0}\nStackTrace{1}  ", ex.Message.ToString(), ex.StackTrace.ToString()));
                    throw new Exception(ex.Message.ToString(), ex.InnerException);
                }                               
            }           
        }

        // Populate Doc Info Grid
        private void PopulateDocInfoGrid()
        {

           int i = 0;           
            
           // use stringbuilder here 
            string sqlClaims = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOCUMENT_NAME,"
                    + "DOCUMENT_ATTACH.TABLE_NAME,DOCUMENT_ATTACH.RECORD_ID,"
                    + "DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.CREATE_DATE, DOCUMENT.FOLDER_ID,"
                    + "DOCUMENT.USER_ID, DOCUMENT.DOCUMENT_TYPE,  DOCUMENT.MEDIAVIEW_TRANSFER_STATUS FROM DOCUMENT LEFT OUTER JOIN DOCUMENT_ATTACH"
                    + " ON DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID WHERE NOT(MEDIAVIEW_TRANSFER_STATUS=-1) OR DOCUMENT.MEDIAVIEW_TRANSFER_STATUS IS NULL "
                    + " ORDER BY DOCUMENT.USER_ID";
            //ORDER BY TABLE_NAME

            string sqlMerge = "SELECT DISTINCT MERGE_FORM.FORM_ID,FORM_NAME,FORM_FILE_NAME, MEDIAVIEW_TRANSFER_STATUS FROM MERGE_FORM WHERE NOT(MEDIAVIEW_TRANSFER_STATUS=-1) OR MEDIAVIEW_TRANSFER_STATUS IS NULL";

            writeLog("Loading doc info...");
            try
            {

                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objDbConnection, sqlClaims);
                AppGlobals.m_dsDocs.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsDocs);                

                ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objDbConnection, sqlMerge);
                AppGlobals.m_dsMergeDocs.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsMergeDocs);

                writeLog("Load complete..datasets populated...");

                // only executed if debug is set to true in config file
                if (m_bIsDebugMode)
                {
                    AppGlobals.m_dsDocs.WriteXml(Directory.GetCurrentDirectory() + "\\AttachDocInfo_" + DateTime.Now.Ticks.ToString() + ".xml");
                    AppGlobals.m_dsMergeDocs.WriteXml(Directory.GetCurrentDirectory() + "\\MergeDocInfo_" + DateTime.Now.Ticks.ToString() + ".xml");
                }
              
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[PopulateDocInfoGrid] \nmessage:{0}\n\nStackTrace{1}  ", exp.Message.ToString(),  exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            writeLog("Load operation completed.");

            txtDocsCount.Text = (AppGlobals.m_dsDocs.Tables[0].Rows.Count + AppGlobals.m_dsMergeDocs.Tables[0].Rows.Count).ToString();
        }

        // sets text for the status panel box
        private void setStatus(string p_status)
        {
            toolStripStatusLabel1.Text = p_status;
        }
                
        private void setBgStatus(string p_status)
        {
            //toolStripStatusLabel1.Text = p_status;
            AppGlobals.sStatusMessage = p_status;
            bgWorkerMain.ReportProgress(1);
        }
        
        private void frmMediaView_FormClosing(object sender, FormClosingEventArgs e)
        {
            writeLog("Disposing all objects" );
            if (m_objLogin!=null)   m_objLogin.Dispose();
            if (AppGlobals.m_dsDocs!=null) AppGlobals.m_dsDocs.Dispose();
            //if (AppGlobals.m_dsEnhNotes!=null) AppGlobals.m_dsEnhNotes.Dispose();
            if (m_objDbConnection!=null) m_objDbConnection.Dispose();
            if (m_sw1 != null)
            {                
                m_sw1.Close();
                m_sw1.Dispose();
            }
            
            // cancel all bg workers here
            bgWorkerPopulateFileinfo.CancelAsync();
            bgWorkerMain.CancelAsync();
        }


        //skhare7   //skhare7 MITS 21874
        public struct RecordCollection
        {
            public string EventNumber;
            public string PolicyNumber;
            public string ClaimNumber;
          
        }

     //skhare7 MITS 21874
        public RecordCollection GetRecordValues(string sTableName, int iPrimaryKey)
        {
            string sSQL = string.Empty;
            
            //RecordCollection rc = new RecordCollection();
            try
            {

                switch (sTableName.ToLower())
                {

                    case "event":
                        sSQL = "SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                           return new RecordCollection {EventNumber=  Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }

                        break;

                    case "claim":

                        sSQL = "SELECT E.EVENT_NUMBER,C.CLAIM_NUMBER FROM CLAIM C,EVENT E WHERE C.EVENT_ID=E.EVENT_ID AND C.CLAIM_ID=" + iPrimaryKey;
                        
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]), ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1]) };

                          
                        }
                        break;

                    case "pipatient":
                    case "piwitness":
                    case "piemployee":
                        sSQL = "select E.EVENT_NUMBER,C.CLAIM_NUMBER from PERSON_INVOLVED PI,EVENT E,CLAIM C " 
                            + "where E.EVENT_ID=PI.EVENT_ID and C.EVENT_ID=E.EVENT_ID and PI.PI_ROW_ID = " 
                            + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                                , ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1]) };

                        
                        }
                        break;

                    case "policyenh":
                        sSQL = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID=" + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection {PolicyNumber=  Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }

                        break;

                    case "eventdatedtext":
                          sSQL = "select E.EVENT_NUMBER  from EVENT_X_DATED_TEXT ED,EVENT E "
                              +"where ED.EVENT_ID=E.EVENT_ID and ED.EV_DT_ROW_ID=" 
                              + iPrimaryKey;
                         m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }

                        break;

                    case "claimant":
                         sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER  from CLAIMANT CL,EVENT E,CLAIM C where cl.CLAIM_ID=C.CLAIM_ID and "
                         + "E.EVENT_ID=C.EVENT_ID and CL.CLAIMANT_ROW_ID=" + iPrimaryKey;
                         m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                , ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }

                        break;

                    case "defendant":
                         sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER  from DEFENDANT D,EVENT E,CLAIM C where D.CLAIM_ID=C.CLAIM_ID and "
                        + "E.EVENT_ID=C.EVENT_ID and D.DEFENDANT_ROW_ID =" + iPrimaryKey;
                         m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                , ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }
                        break;

                    case "policy":
                         sSQL = "SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID=" + iPrimaryKey;
                         m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection { PolicyNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0]) };
                           
                        }

                        break;
                    //skhare7:MITS 22743
                    case "litigation":
                        sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER from CLAIM_X_LITIGATION CL ,CLAIM C,EVENT E where C.CLAIM_ID=CL.CLAIM_ID and C.EVENT_ID=E.EVENT_ID and CL.LITIGATION_ROW_ID ="
                            + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;
                    case "expert":
                        sSQL = "select C.CLAIM_NUMBER,E.EVENT_NUMBER from EXPERT EP ,CLAIM_X_LITIGATION CL ,CLAIM C,EVENT E  where C.CLAIM_ID=CL.CLAIM_ID and E.EVENT_ID=C.EVENT_ID"
                                 + "and CL.LITIGATION_ROW_ID=EP.LITIGATION_ROW_ID and EP.EXPERT_ROW_ID ="
                                 + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;
                    case "adjuster dated text":
                        sSQL = "select  C.CLAIM_NUMBER,E.EVENT_NUMBER from ADJUST_DATED_TEXT AT,CLAIM_ADJUSTER CA,CLAIM C,EVENT E where "
                                +"AT.ADJ_ROW_ID=CA.ADJ_ROW_ID and C.CLAIM_ID=CA.CLAIM_ID and C.EVENT_ID=E.EVENT_ID"
                                 +"and AT.ADJ_DTTEXT_ROW_ID ="
                                    + iPrimaryKey;
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                            };

                        }

                        break;
                    case "plan":
                        sSQL = "select E.EVENT_NUMBER,c.CLAIM_NUMBER from CLAIM c , DISABILITY_PLAN DP,EVENT E "
                               + "where  c.CLAIM_ID=DP.PLAN_ID and E.EVENT_ID=c.EVENT_ID and "
                               + "DP.PLAN_ID=" + iPrimaryKey;
                              
                        m_objDbReaderRetriveRc = DbFactory.GetDbReader(m_objDbConnection.ConnectionString, sSQL);
                        if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                        if (m_objDbReaderRetriveRc.Read())
                        {

                            return new RecordCollection
                            {
                                EventNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[0])
                                ,
                                ClaimNumber = Conversion.ConvertObjToStr(m_objDbReaderRetriveRc[1])
                            };

                        }

                        break;
                    //skhare7:MITS 22743 End
                    default:
                        return new RecordCollection();

                }

                return new RecordCollection();
              

            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(), p_oException.StackTrace.ToString()));
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                //m_objDbReaderRetriveRc.Dispose();
            }
          
        }
        //skhare7 MITS 21874 End
        public static string GetSingleValue_Sql(string sSQL, string strConnectionString)
        {
            string sValue = string.Empty;
            DbReader objReader = null;
            try
            {
                objReader = DbFactory.GetDbReader(strConnectionString, sSQL);
                if (m_bIsDebugMode) writeLog("GetSingleValue_Sql: executing sql " + sSQL);
                if (objReader.Read())
                {
                    sValue = Conversion.ConvertObjToStr(objReader[0]);
                }
            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(),  p_oException.StackTrace.ToString()));               
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                objReader.Dispose();
            }
            return sValue;
        }

      

       
        public void ExecuteSql(string p_sSQL)
        {
            if (m_bIsDebugMode) writeLog("ExecuteSql: executing sql " + p_sSQL);
            try
            {   
                m_objDbConnection.ExecuteNonQuery(p_sSQL);
            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[ExecuteSql] \nmessage:{0}\nStackTrace{1}  ", p_oException.Message.ToString(), p_oException.StackTrace.ToString()));
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {   
                //objDbConnection.Dispose();
            }            
        }

        public static void writeLog(string p_strLogText)
        {
            string sMessage = string.Format("[{0}] {1}", System.DateTime.Now.ToString(), p_strLogText);            
            try
            {                
                m_sw1.WriteLine(sMessage);
                m_sw1.Flush();                
            }
            catch (Exception exp)
            {
                throw new Exception("Error Logging Exception :" + exp.Message.ToString(), exp.InnerException);
            }
            
        }

        private void btnStartTransfer_Click(object sender, EventArgs e)
        {
            if (m_iresetIISInterval >= 0)
            {
                aTimer = new System.Timers.Timer(m_iresetIISInterval * 60000);
                
                // Hook up the Elapsed event for the timer.
                aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                aTimer.AutoReset = true;
                aTimer.Enabled = true;
            }
            else
            {
                //toolStripStatusLabel3.Text = "Timer disabled";
                aTimer.Enabled = false;
            }
            
            
            btnPauseResume.Enabled = true;
            btnStartTransfer.Enabled = false;
            setStatus("Starting doc transfer");
            writeLog("Starting doc transfer");

            try
            {
                bgWorkerMain.RunWorkerAsync();
            }
            catch (Exception exp)
            {                
                writeLog(string.Format("ERROR:[StartTransfer] \nmessage:{0}\nStackTrace{1}  ", exp.Message.ToString(),  exp.StackTrace.ToString()));
            }
        }

        private void bgWorkerMain_DoWork(object sender, DoWorkEventArgs e)
        {           
            #region Declarations
            string sIndiUserPath = "";
            MemoryStream objFileContent = null;
            string sSQL = string.Empty;
            string sDocId = string.Empty;
            string sTableName = string.Empty;
            string sEventNumber = string.Empty;
            string sClaimNumber = string.Empty;
            string sPolicyName = string.Empty;
            string sEventId = string.Empty;
            string sClaimId = string.Empty;
            string sFileName = string.Empty;
            int iRetCd = 0;
            byte[] bFileContent = null;
            string sDocTitle = string.Empty;
            string sCreateTs = string.Empty;
            string sModifiedCreateTs = string.Empty;
            MediaView objMediaView = null;
            string sAppExcpXml = string.Empty;
            string sSessionId = string.Empty;
            string sReturnXmlstring = string.Empty;
            string sAttachedRecord = String.Empty;
            string sPIEmployeeId = string.Empty;
            string sPIPatientId = string.Empty;
            string sPIWitnessId = string.Empty;
            string sEventDatedText = string.Empty;
            string sClaimant = string.Empty;
            string sDefendant = string.Empty;
            string sPolicyId = string.Empty;
            string sFormName = string.Empty;
            string sFormFileName = string.Empty;
            string sAuthor = string.Empty;
            string sPlan = string.Empty;
            string sLitigation = string.Empty;
            string sExpert = string.Empty;
            string sAdjusterText = string.Empty;

            string sEventFolderFriendlyName = "";
            string sClaimFolderFriendlyName = "";
            string sPolicyFolderFriendlyName = "";
            string sUsersFolderFriendlyName = "";
            string sGeneralFolderFriendlyName = "";
            string sUserDocLoc="";
            string sMigrationStatus = "";
            bool bMigrationStatus = false;

            int iRowsCount = AppGlobals.m_dsDocs.Tables[0].Rows.Count;
            int i=0;
            long iSize = 0;

            #endregion

            try
            {
               
                m_enmDocumentStorageType = (StorageType)AppGlobals.Userlogin.objRiskmasterDatabase.DocPathType;
                m_sDestinationStoragePath = AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath;
                objFileStorageManager = new FileStorageManager(this.m_enmDocumentStorageType, this.m_sDestinationStoragePath);
                objFileStorageManagerIndividualUser = new FileStorageManager(this.m_enmDocumentStorageType, this.m_sDestinationStoragePath);
                
                objFileContent = new MemoryStream();
              
                Boolean bSuccess = false;
              
                objMediaView = new MediaView();
                for (i = 0; i < iRowsCount; i++)
                {

                    bSuccess = false;
                    locker.WaitOne();

                    sMigrationStatus = AppGlobals.m_dsDocs.Tables[0].Rows[i]["MEDIAVIEW_TRANSFER_STATUS"].ToString();
                    //doing a null check
                    if (sMigrationStatus.Length > 0)
                    {
                        
                        bMigrationStatus = (Convert.ToInt32(sMigrationStatus.Trim())==-1)?true:false;
                            //Convert.ToBoolean(Convert.ToInt32(sMigrationStatus.Trim()));
                    }
                    else
                    {
                        bMigrationStatus = false;
                    }

                    // this check is unrequired in case we load only doc with migration status !=0 but anyways 
                    // implementing it as in case any changes are made in future
                    if (!bMigrationStatus)
                    {
                        sFileName = AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_FILENAME"].ToString();
                        sDocTitle = AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_NAME"].ToString();
                        try
                        {
                            #region uploading attachment to MediaView

                            writeLog("---------------");
                            iRetCd = 0;                            
                            sTableName = AppGlobals.m_dsDocs.Tables[0].Rows[i]["TABLE_NAME"].ToString();                            
                            sCreateTs = AppGlobals.m_dsDocs.Tables[0].Rows[i]["CREATE_DATE"].ToString();
                            sAuthor = AppGlobals.m_dsDocs.Tables[0].Rows[i]["USER_ID"].ToString();
                            sDocId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_ID"].ToString();

                            if (AppGlobals.bUseDummyFiles || this.m_enmDocumentStorageType.ToString() != "FileSystemStorage")
                            {
                                iSize = 1;
                            }
                            else if (AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"] != null && AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"].ToString().Length > 0)
                            {
                                iSize = Convert.ToInt64(AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"]);
                            }
                            else
                            {
                                // means that the utility was unable to get the size of the file in the first place..
                                // the utility will do a re-check ont he file....
                                // instead we could also have skiped on this file.
                                iSize = 0;
                                writeLog("possibly erroneous file: " + sFileName);
                            }

                            //checking individual doc path here.
                            if (!AppGlobals.sdUserDocPaths.ContainsKey(sAuthor))
                            {
                                sIndiUserPath = GetSingleValue_Sql(string.Format("SELECT DOC_PATH FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '{0}'", sAuthor).Trim(), m_objLogin.SecurityDsn);
                                AppGlobals.sdUserDocPaths.Add(sAuthor, sIndiUserPath);
                            }
                            else
                            {
                                sIndiUserPath = AppGlobals.sdUserDocPaths[sAuthor];
                            }

                            if (sIndiUserPath != "" && objFileStorageManagerIndividualUser.DestinationStoragePath.ToLower() != sIndiUserPath.ToLower())
                            {
                                objFileStorageManagerIndividualUser = new FileStorageManager(this.m_enmDocumentStorageType, sIndiUserPath);
                            }

                            // value for updating progress bar here
                            AppGlobals.lProcessedFileSize += iSize;

                            //checking individual doc path here.
                            //if (!AppGlobals.sdUserDocPaths.ContainsKey(sAuthor))
                            //{                         
                            //    //sAuthor 
                            //    //GetSingleValue_Sql
                            //    sUserDocLoc = GetSingleValue_Sql(string.Format("SELECT DOC_PATH FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '{0}'", sAuthor), AppGlobals.Userlogin.ToString());
                            //    AppGlobals.sdUserDocPaths.Add("sAuthor", sUserDocLoc);
                            //}
                                                        
                            setBgStatus("attempting file transfer for:" + sFileName);
                            writeLog(string.Format("attempting file transfer for:{0}\n FileType:{1}\nDocument Name:{2}\nauthor:{4} ", sFileName, sTableName, sDocTitle, sCreateTs, sAuthor));

                            sAttachedRecord = "";

                            if (sCreateTs != "")
                            {
                                sModifiedCreateTs = sCreateTs.Substring(0, 4) + "/" + sCreateTs.Substring(4, 2) + "/"
                                    + sCreateTs.Substring(6, 2) + " " + sCreateTs.Substring(8, 2) + ":"
                                    + sCreateTs.Substring(10, 2);
                            }
                            if (AppGlobals.bUseDummyFiles)
                            {
                                iRetCd = 1;
                                objFileContent = new MemoryStream(FileAsByteArray(@System.IO.Path.GetFullPath("dummy.txt")));
                            }

                            RecordCollection rcTemp;
                            
                            switch (sTableName.ToLower())
                            {
                                case "event": sEventId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    rcTemp = GetRecordValues("event", Conversion.ConvertStrToInteger(sEventId));
                                    sEventNumber = rcTemp.EventNumber;

                                    writeLog("extracting object info.");
                                   // sEventNumber = objEvent.EventNumber;
                                    sAttachedRecord = sEventNumber;
                                    //skhare7 MITS 21874 End
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("EVENT: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                            //writeLog(string.Format("OutXML:{0}", objFileContent.to));
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("EVENT: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }


                                        if (iRetCd == 1)
                                        { writeLog("EVENT: File retrival successfull"); }
                                        else
                                        { writeLog("EVENT: File retrival failure"); }
                                    }

                                    if (iRetCd == 1)
                                    {

                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sEventNumber , "Event" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);
                                        
                                    }
                                    break;
                                    
                                case "claim": sClaimId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                   
                                    rcTemp = GetRecordValues("claim", Conversion.ConvertStrToInteger(sClaimId));
                                    sEventNumber = rcTemp.EventNumber;

                                    sClaimNumber = rcTemp.ClaimNumber;


                                    writeLog("extracting object info.");
                                   
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("CLAIM: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("CLAIM: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("CLAIM: File retrival successfull"); }
                                        else
                                        { writeLog("CLAIM: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sClaimNumber, "Claim", sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;
                                
                                case "piemployee": sPIEmployeeId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    //skhare7 MITS 21874
                                   // writeLog("creating DM object-piemployee");
                                  //  objPI = (PiEmployee)m_objDataModelFactory.GetDataModelObject("PiEmployee", false);
                                   // writeLog("navigating...");
                                   // objPI.MoveTo(Conversion.ConvertStrToInteger(sPIEmployeeId));                                    
                                  //  sEventId = objPI.EventId.ToString();
                                   // writeLog("creating DM object-event");
                                   // objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                                   // writeLog("navigating...");
                                  //  objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));
                                    sEventNumber = GetRecordValues("piemployee", Conversion.ConvertStrToInteger(sPIEmployeeId)).EventNumber;

                                   // sClaimNumber = GetRecordValues(sTableName, Conversion.ConvertStrToInteger(sClaimId)).ClaimNumber;
                                    writeLog("extracting object info.");
                                   //sEventNumber = objEvent.EventNumber;
                                    sAttachedRecord = sEventNumber;
                                    //skhare7 MITS 21874 End
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("piemployee: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("piemployee: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("piemployee: File retrival successfull"); }
                                        else
                                        { writeLog("piemployee: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sEventNumber , "Event" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;

                                case "pipatient": sPIPatientId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    rcTemp = GetRecordValues("pipatient", Conversion.ConvertStrToInteger(sPIPatientId));
                                    sClaimNumber = rcTemp.ClaimNumber;
                                    sEventNumber = rcTemp.EventNumber;

                                    writeLog("extracting object info.");
                                  //  sEventNumber = objEvent.EventNumber;
                                    //skhare7 MITS 21874 End
                                    sAttachedRecord = sEventNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("pipatient: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("pipatient: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("pipatient: File retrival successfull"); }
                                        else
                                        { writeLog("pipatient: File retrival failure"); }
                                    }

                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sEventNumber , "Event" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;

                                case "piwitness": sPIWitnessId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                 

                                     rcTemp = GetRecordValues("piwitness", Conversion.ConvertStrToInteger(sPIWitnessId));

                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                   writeLog("extracting object info.");
                                    sAttachedRecord = sEventNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("piwitness: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("piwitness: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("piwitness: File retrival successfull"); }
                                        else
                                        { writeLog("piwitness: File retrival failure"); }
                                    }

                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sEventNumber , "Event" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);
                                    }


                                    break;

                                case "eventdatedtext": sEventDatedText = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                    sEventNumber = GetRecordValues("eventdatedtext", Conversion.ConvertStrToInteger(sEventDatedText)).EventNumber;
                                    writeLog("extracting object info.");
                                    sAttachedRecord = sEventNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("eventdatedtext: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("eventdatedtext: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("eventdatedtext: File retrival successfull"); }
                                        else
                                        { writeLog("eventdatedtext: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                       importList oList = new importList(sEventNumber , "Event" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;

                                case "claimant": sClaimant = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                  
                                    rcTemp = GetRecordValues("claimant", Conversion.ConvertStrToInteger(sClaimant));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;


                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("claimant: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("claimant: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("claimant: File retrival successfull"); }
                                        else
                                        { writeLog("claimant: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sClaimNumber , "Claim" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;

                                case "defendant": sDefendant = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                
                                    rcTemp= GetRecordValues("defendant", Conversion.ConvertStrToInteger(sDefendant));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                    writeLog("extracting object info.");
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("defendant: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("defendant: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("defendant: File retrival successfull"); }
                                        else
                                        { writeLog("defendant: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sClaimNumber , "Claim" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;

                                //skhare7:MITS 22743
                                case "expert": sExpert = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                  
                                   rcTemp = GetRecordValues("expert", Conversion.ConvertStrToInteger(sExpert));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                    writeLog("extracting object info.");
                                   
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("defendant: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("defendant: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("defendant: File retrival successfull"); }
                                        else
                                        { writeLog("defendant: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                       importList oList = new importList(sClaimNumber , "Claim" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;
                               
                                case "plan": sPlan = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                  
                     
                                    rcTemp = GetRecordValues("plan", Conversion.ConvertStrToInteger(sPlan));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                    writeLog("extracting object info.");
                                    
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("plan: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("plan: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("defendant: File retrival successfull"); }
                                        else
                                        { writeLog("defendant: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sClaimNumber , "Claim" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;
                                case "litigation": sLitigation = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();

                                    rcTemp = GetRecordValues("litigation", Conversion.ConvertStrToInteger(sLitigation));

                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;


                                    writeLog("extracting object info.");
                                    
                                    //skhare7 MITS 21874 End
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("litigation: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("litigation: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("litigation: File retrival successfull"); }
                                        else
                                        { writeLog("litigation: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sClaimNumber , "Claim" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;
                                case "adjuster dated text": sAdjusterText = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                 
                                    rcTemp= GetRecordValues("adjuster dated text", Conversion.ConvertStrToInteger(sAdjusterText));
                                    sEventNumber = rcTemp.EventNumber;
                                    sClaimNumber = rcTemp.ClaimNumber;

                                    writeLog("extracting object info.");
                                   
                                    sAttachedRecord = sClaimNumber;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("adjuster dated text: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("adjuster dated text: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("defendant: File retrival successfull"); }
                                        else
                                        { writeLog("defendant: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                        importList oList = new importList(sClaimNumber , "Claim" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;
                                //skhare7:MITS 22743 End
                                case "policy": sPolicyId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                 sPolicyName = GetRecordValues("policy", Conversion.ConvertStrToInteger(sPolicyId)).PolicyNumber;
                                   
                                    writeLog("extracting object info.");
                                  //  sPolicyName = objPolicy.PolicyName;
                                    //skhare7 MITS 21874 End
                                    sAttachedRecord = sPolicyName;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("policy: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("policy: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("policy: File retrival successfull"); }
                                        else
                                        { writeLog("policy: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                       importList oList = new importList(sPolicyName , "Policy" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;
                                
                              
                                case "policyenh":
                                case "policyenhgl":
                                case "policyenhpc":
                                case "policyenhwc":
                                case "policyenhal": 
                                    sPolicyId = AppGlobals.m_dsDocs.Tables[0].Rows[i]["RECORD_ID"].ToString();
                                   sPolicyName = GetRecordValues("policyenh", Conversion.ConvertStrToInteger(sPolicyId)).PolicyNumber;
                                    writeLog("extracting object info.");
                                    //sPolicyName = objPolicyEnh.PolicyName;
                                    //skhare7 MITS 21874 End
                                    sAttachedRecord = sPolicyName;
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("policy: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("policy: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("policy: File retrival successfull"); }
                                        else
                                        { writeLog("policy: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                       importList oList = new importList(sPolicyName , "Policy" , sDocTitle);
                                       bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }


                                    break;
                           
                         
                        

                                //skhare7 MITS 21889 
                                default:

                                    sTableName = "OrphanDocuments";
                                    if (!AppGlobals.bUseDummyFiles)
                                    {
                                        if (sIndiUserPath.Length == 0)
                                        {
                                            if (m_bIsDebugMode) writeLog("OrphanDocuments: retriving File thru Global doc location ");
                                            iRetCd = objFileStorageManager.RetrieveFile(sFileName, out objFileContent);
                                        }
                                        else
                                        {
                                            if (m_bIsDebugMode) writeLog("OrphanDocuments: retriving File thru individual doc location ");
                                            iRetCd = objFileStorageManagerIndividualUser.RetrieveFile(sFileName, out objFileContent);
                                        }

                                        if (iRetCd == 1)
                                        { writeLog("OrphanDocuments: File retrival successfull"); }
                                        else
                                        { writeLog("OrphanDocuments: File retrival failure"); }
                                    }
                                    if (iRetCd == 1)
                                    {
                                        bFileContent = objFileContent.ToArray();
                                       importList oList = new importList(sAuthor , "User" , sDocTitle);
                                        bSuccess = objMediaView.MMUploadDocument(sFileName, bFileContent, oList);

                                    }

                                    break;
                                    
                            } // switch

                            writeLog("MediaView upload output xml :-" + sAppExcpXml);
                            writeLog("Updating Doc transfer status in db");
                            if (bSuccess)
                            {
                                // updating processed file count in GUI
                                AppGlobals.iProcessedFileCount++;

                                //udating status in db
                                ExecuteSql(string.Format("UPDATE DOCUMENT SET MEDIAVIEW_TRANSFER_STATUS=-1 WHERE DOCUMENT_ID={0}", sDocId)); //, m_objDbConnection                                    
                            }
                            bgWorkerMain.ReportProgress(0);

                            

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            writeLog(string.Format("ERROR:[bgWorkerMain_moving_attachments -inner block] \nFileName:{0} \nDocName{1}\n Message:{2}", sFileName, sDocTitle, ex.Message.ToString()));
                        }
                    }
                }

            }
            catch (Exception exp)
            {
                //objTempElement = objXmlDoc.CreateElement("DocumentName");
                //objTempElement.InnerText = sFileName;
                //objTempElement.SetAttribute("Exported", "False");
                
                //Filename to be writeLog here  
                writeLog(string.Format("ERROR:[bgWorkerMain_moving_attachments: outer block] \nmessage:{0}\nStackTrace{1}\nFileName{2}  ", exp.Message.ToString(),  exp.StackTrace.ToString(), sFileName));
              
            }
            finally
            {
                //objTempElement.SetAttribute("AttachedRecord", sAttachedRecord);
                //objParentElement = (XmlElement)objXmlDoc.SelectSingleNode("//" + sTableName.ToLower());
                //objParentElement.AppendChild(objTempElement);
            }

            //clearing dataset holding details of Atathcments (this is unrequired now as we havealready moved them to MediaView)
            writeLog("--------------------------------\nAttachment transfer completed. initiating mail merge files transfer\n--------------------------------");
            AppGlobals.m_dsDocs.Tables[0].Clear();

            #region uploading mail merge templates
            if (objMediaView == null)
            {
                objMediaView = new MediaView();
            }
            
            for (i = 0; i < AppGlobals.m_dsMergeDocs.Tables[0].Rows.Count;i++ )
            {
                locker.WaitOne();
                bool bSuccess = false;
                iRetCd = 0;

                sMigrationStatus = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["MEDIAVIEW_TRANSFER_STATUS"].ToString();
                //doing a null check
                if (sMigrationStatus.Length > 0)
                {
                    bMigrationStatus = (Convert.ToInt32(sMigrationStatus.Trim()) == -1) ? true : false;
                    //bMigrationStatus = Convert.ToBoolean(sMigrationStatus.Trim());
                }
                else
                {
                    bMigrationStatus = false;
                }

                // this check is unrequired in case we load only doc with migration status !=0 but anyways 
                // implementing it as in case any changes are made in future
                if (!bMigrationStatus)
                {
                    try
                    {
                        sFormName = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_NAME"].ToString();
                        sFormFileName = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_FILE_NAME"].ToString();
                        sDocId = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_ID"].ToString();
                        

                        if (AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["Size"] != null && AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["Size"].ToString().Length >0)
                        {
                            iSize = Convert.ToInt64(AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["Size"]);
                        }
                        else
                        {
                            // means that the utility was unable to get the size of the file in the first place..
                            // the utility will do a re-check ont he file....
                            // instead we could also have skiped on this file.
                            iSize = 0;
                            writeLog("possibly erroneous file: " + sFormFileName);
                        }
                        
                        writeLog("---------------");
                        setBgStatus("attempting file transfer :" + sFormFileName);
                        writeLog(string.Format("attempting file transfer :{0}\nDocument Name:{1}", sFormFileName, sFormName));

                        // value for updating progress bar here
                        AppGlobals.lProcessedFileSize += iSize;
                        
                        // we are now updating the merge file count only when both the merge file & its HDR
                        // quivalent has got uploaded.
                        //AppGlobals.iProcessedFileCount++;

                        iRetCd = objFileStorageManager.RetrieveFile(sFormFileName, out objFileContent);
                        
                        if (iRetCd == 1)
                        { writeLog("Merge: File retrival successfull"); }
                        else
                        { writeLog("Merge: File retrival failure"); }

                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            

                            //Posting document to Media View
                            importList oList = new importList("MailMergeTemplates", "Misc", sFormFileName);
                            bSuccess = objMediaView.MMUploadDocument(sFormFileName, bFileContent, oList);
                            

                        }
                    }
                    catch (Exception exp)
                    {
                        writeLog(string.Format("ERROR:[bgWorkerMain_moving_merge_files] \nmessage:{0}\nStackTrace{1}  ", exp.Message.ToString(), exp.StackTrace.ToString()));

                    }
                    finally
                    {
                        //objParentElement = (XmlElement)objXmlDoc.SelectSingleNode("//Templates");
                        //objParentElement.AppendChild(objTempElement);
                    }

                    //Posting hdr files
                    try
                    {
                        //sFormName = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_NAME"].ToString();
                        iRetCd = 0;
                        sFormFileName = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_FILE_NAME"].ToString();

                        sFormFileName = sFormFileName.Replace(sFormFileName.Substring(sFormFileName.Length - 4, 4), ".HDR");
                        
                        writeLog("---------------");
                        setBgStatus("attempting HDR file transfer for:" + sFormFileName);
                        writeLog(string.Format("attempting HDR file transfer :{0}", sFormFileName));
                        
                        iRetCd = objFileStorageManager.RetrieveFile(sFormFileName, out objFileContent);
                        
                        if (iRetCd == 1)
                        { writeLog("Merge-hdr: File retrival successfull"); }
                        else
                        { writeLog("Merge-hdr: File retrival failure"); }
                        
                        
                        if (iRetCd == 1)
                        {
                            bFileContent = objFileContent.ToArray();
                            

                            //Posting document to Media View
                            importList oList = new importList("MailMergeTemplates", "Misc", sFormFileName);
                            bSuccess = objMediaView.MMUploadDocument(sFormFileName, bFileContent, oList);

                        }
                    
                        //updating status in db only when both template & hdr file ahs got uploaded to MediaView
                        if (bSuccess)
                        {
                            AppGlobals.iProcessedFileCount++;
                            ExecuteSql(string.Format("UPDATE MERGE_FORM SET MEDIAVIEW_TRANSFER_STATUS=-1 WHERE FORM_ID={0}", sDocId)); //, m_objDbConnection
                        }
                        
                    }
                    catch (Exception exp)
                    {
                        //objTempElement = objXmlDoc.CreateElement("HeaderName");
                        //objTempElement.InnerText = sFormFileName;
                        //objTempElement.SetAttribute("Exported", "False");

                        //toDO : write filename here
                        writeLog(string.Format("ERROR:[bgWorkerMain_moving_merge_hdr_files] \nmessage:{0}\nStackTrace:{1}\nFileName:{2}  ", exp.Message.ToString(), exp.StackTrace.ToString(), sFormFileName));
                    }
                    finally
                    {
                        //objParentElement = (XmlElement)objXmlDoc.SelectSingleNode("//TemplateHeaders");
                        //objParentElement.AppendChild(objTempElement);
                    }

                    bgWorkerMain.ReportProgress(0);
                }
            }
            
            #endregion

          
            //if (objEvent != null)
            //    objEvent.Dispose();
            //if (objClaim != null)
            //    objClaim.Dispose();
            //if (objPI != null)
            //    objPI.Dispose();
            //if (objPolicy != null)
            //    objPolicy.Dispose();
            //if (objClaimant != null)
            //    objClaimant.Dispose();
            //if (objDefendant != null)
            //    objDefendant.Dispose();
            //if (objDatedText != null)
            //    objDatedText.Dispose();
            //if (objWitness != null)
            //    objWitness.Dispose();
            //if (objPatient != null)
            //    objPatient.Dispose();
            //if (objFileContent != null)
            //    objFileContent.Dispose();
            //if (objPolicyEnh != null)
            //    objPolicyEnh.Dispose();
            
            btnStartTransfer.Enabled = false;

        }

        private void bgWorkerPopulateFileinfo_DoWork(object sender, DoWorkEventArgs e)
        {
            //try catch to be put in retrive file info here
            int i=0;            
            long size=0;
            string sStatus = string.Empty;
            string sAuthor = "";
            string sIndiUserPath = "";
            
            System.IO.DirectoryInfo dirInfo = new DirectoryInfo(AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath.ToString());
            System.IO.DirectoryInfo dirInfoIndi = new DirectoryInfo(AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath.ToString());            
            System.IO.FileInfo[] fileNames;



            if (!AppGlobals.bUseDummyFiles && this.m_enmDocumentStorageType.ToString() == "FileSystemStorage")
            {
                #region regular transfer

                for (; i < AppGlobals.m_dsDocs.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        sStatus = AppGlobals.m_dsDocs.Tables[0].Rows[i]["MEDIAVIEW_TRANSFER_STATUS"].ToString();
                        sAuthor = AppGlobals.m_dsDocs.Tables[0].Rows[i]["USER_ID"].ToString();


                        //checking individual doc path here.
                        if (!AppGlobals.sdUserDocPaths.ContainsKey(sAuthor))
                        {
                            sIndiUserPath = GetSingleValue_Sql(string.Format("SELECT DOC_PATH FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '{0}'", sAuthor), m_objLogin.SecurityDsn);
                            AppGlobals.sdUserDocPaths.Add(sAuthor, sIndiUserPath);
                        }
                        else
                        {
                            sIndiUserPath = AppGlobals.sdUserDocPaths[sAuthor];
                        }

                        if (sIndiUserPath != "" && dirInfoIndi.FullName.ToLower() != @sIndiUserPath.ToLower())
                        {
                            dirInfoIndi = new DirectoryInfo(@sIndiUserPath);
                        }

                        // updating file size info here 
                        if (sStatus.Length == 0 || sStatus == "" || sStatus == "0")
                        {
                            //TO DO: additional checks for individual user's upload loc here...
                            if (sIndiUserPath != "")
                            {
                                fileNames = dirInfoIndi.GetFiles(AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_FILENAME"].ToString());
                            }
                            else
                            {
                                fileNames = dirInfo.GetFiles(AppGlobals.m_dsDocs.Tables[0].Rows[i]["DOCUMENT_FILENAME"].ToString());
                            }

                            //we assume that only 1 file would be returned by the doc name
                            if (fileNames.GetLength(0) > 0)
                            {
                                size = size + fileNames[0].Length;
                                AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"] = fileNames[0].Length;

                                // rsolanki2: we are setting the value of lFileSize in each iteration coz it might be used in another thread in parallel
                                AppGlobals.lTotalFileSize = size;
                                bgWorkerPopulateFileinfo.ReportProgress(0, null);
                            }
                        }
                        else
                        {
                            AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"] = -1;
                        }
                    }
                    catch (Exception ex)
                    {
                        //skipping on file errors
                    }
                }

                // updating file info for Mail merge files
                for (i = 0; i < AppGlobals.m_dsMergeDocs.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        sStatus = AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["MEDIAVIEW_TRANSFER_STATUS"].ToString();

                        // updating file size info here 
                        if (sStatus.Length == 0 || sStatus == "" || sStatus == "0")
                        {
                            //TO DO: additional checks for individual user's upload loc here...
                            fileNames = dirInfo.GetFiles(AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["FORM_FILE_NAME"].ToString());

                            //we assume that only 1 file would be returned by the doc name
                            if (fileNames.GetLength(0) > 0)
                            {

                                size = size + fileNames[0].Length;
                                AppGlobals.m_dsMergeDocs.Tables[0].Rows[i]["Size"] = fileNames[0].Length;

                                // rsolanki2: we are setting the value of lFileSize in each iteration coz it might be used in another thread in parallel
                                AppGlobals.lTotalFileSize = size;
                                bgWorkerPopulateFileinfo.ReportProgress(0, null);
                            }
                        }
                        else
                        {
                            AppGlobals.m_dsDocs.Tables[0].Rows[i]["Size"] = -1;
                        }
                    }
                    catch (Exception ex)
                    {
                        //skipping on file errors 
                    }
                }
                #endregion
            }
            else
            {
                #region "Document db" Or "the Dummy file transfer mode"

                
                //progressBar1.Maximum = AppGlobals.m_dsDocs.Tables[0].Rows.Count
                //    + AppGlobals.m_dsMergeDocs.Tables[0].Rows.Count;

                foreach (DataRow row in AppGlobals.m_dsDocs.Tables[0].Rows)
                {
                    row["Size"] = 1;                
                }
                foreach (DataRow row in AppGlobals.m_dsMergeDocs.Tables[0].Rows)
                {
                    row["Size"] = 1;
                }

                //txtTotalSize.Hide();
                //label15.Hide();
                
                #endregion
            }
            
        }

        private void bgWorkerPopulateFileinfo_reportProgress(object sender, ProgressChangedEventArgs e)
        {
         
            //if (AppGlobals.lTotalFileSize > 1073741824)
            //{
            //    txtTotalSize.Text = (AppGlobals.lProcessedFileSize / 1073741824).ToString() + "  GB";
            //}
            //else 
            if (AppGlobals.lTotalFileSize > 1048576)
            {
                txtTotalSize.Text = (AppGlobals.lTotalFileSize / 1048576).ToString() + "  MB";
            }
            else if (AppGlobals.lTotalFileSize > 1024)
            {
                txtTotalSize.Text = (AppGlobals.lTotalFileSize / 1024).ToString() + "  KB";
            }
            else
            {
                txtTotalSize.Text = AppGlobals.lTotalFileSize.ToString() + "  Bytes";
            }
            
            txtRemainingFilesCount.Text = AppGlobals.iProcessedFileCount.ToString();
          
        }

        private void bgWorkerMain_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int iProgressbarvalue = 0;
            if (e.ProgressPercentage == 1)
            {
                toolStripStatusLabel1.Text = AppGlobals.sStatusMessage; 
            }
            else
            {
                txtRemainingFilesCount.Text = AppGlobals.iProcessedFileCount.ToString();
                iProgressbarvalue = Convert.ToInt32(AppGlobals.lProcessedFileSize * 100 / AppGlobals.lTotalFileSize);
                if (iProgressbarvalue<=100)
	            {
                    progressBar1.Value = iProgressbarvalue;
                    toolStripStatusLabel2.Text = progressBar1.Value.ToString() + " % completed";
	            }
                writeLog("\nprocessed file count : " + AppGlobals.iProcessedFileCount);
            }

            //updating timer info
            //toolStripStatusLabel3.Text = aTimer.Interval.ToString();
        }

        private void bgWorkerMain_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            aTimer.Enabled = false;

            // if Not in silent mode then show success box
            if (!AppGlobals.bSilentMode)
            {
                btnPauseResume.Enabled = false;

                progressBar1.Value = 100;
                toolStripStatusLabel2.Text = "100 % completed";
                MessageBox.Show("Migration of docs to MediaView completed.");
                setStatus("Migration completed");                
            }            
            writeLog("Migration of docs to MediaView completed.");

        }

        private void bgWorkerPopulateFileinfo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // only executed if debug is set to true in config file
            if (m_bIsDebugMode)
            {
                AppGlobals.m_dsDocs.WriteXml(Directory.GetCurrentDirectory() + "\\AttachDocInfoWithSizeInfo_" + DateTime.Now.Ticks.ToString() + ".xml");
                AppGlobals.m_dsMergeDocs.WriteXml(Directory.GetCurrentDirectory() + "\\MergeDocInfoWithSizeInfo_" + DateTime.Now.Ticks.ToString() + ".xml");
            }
        }
        private byte[] FileAsByteArray(string p_sFileName)
        {
            byte[] arrRet = null;
            FileStream objFStream = null;
            BinaryReader objBReader = null;
            try
            {
                objFStream = new FileStream(p_sFileName, FileMode.Open);
                objBReader = new BinaryReader(objFStream);
                arrRet = objBReader.ReadBytes((int)objFStream.Length);
            }
            catch (Exception p_objException)
            {
                writeLog("error retriving dummy file");
            }
            finally
            {
                if (objFStream != null)
                {
                    objFStream.Close();
                    objFStream = null;
                }
                if (objBReader != null)
                {
                    objBReader.Close();
                    objBReader = null;
                }
            }
            return arrRet;
        }

        private void btnPauseResume_Click(object sender, EventArgs e)
        {
            if (btnPauseResume.Text == "Pause")
            {
                locker.Reset();
                btnPauseResume.Text = "Resume";
            }
            else
            {
                locker.Set();
                btnPauseResume.Text = "Pause";
            }
        }
        private void WriteBinaryFile(string fileName, byte[] fileBuffer)
        {
            FileStream fileStream;
            BinaryWriter binaryWrtr;

            try
            {
                // create new or overwrite existing
                fileStream = new FileStream(fileName, FileMode.Create);
                binaryWrtr = new BinaryWriter(fileStream);
                binaryWrtr.Write(fileBuffer);

                binaryWrtr.Close();
                fileStream.Close();
            }
            catch (Exception ex)
            {

            }
        }
    
    }
}