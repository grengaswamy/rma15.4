;***********************************************************************************
; DB Upgrade Script for Comments 2 notes tool
;***********************************************************************************
;The script is valid for Oracle 9i r2 and above (As rename command is not supported in oracle 8i)
;checking to see if the enhc Notes column is of varchar type, if yes, then convert it to ClOB type
[Oracle][ASSIGN %%1=QUERY(SELECT COUNT(*) FROM USER_TAB_COLUMNS WHERE UPPER(COLUMN_NAME) = 'NOTE_MEMO_CARETECH' AND UPPER(TABLE_NAME)='CLAIM_PRG_NOTE' AND (UPPER(DATA_TYPE)='VARCHAR' OR UPPER(DATA_TYPE)='VARCHAR2'))]
[Oracle][IF %%1=1]
; adding temporary column
[Oracle]ALTER TABLE CLAIM_PRG_NOTE ADD (temp CLOB)
; copy data from Notes column to temporary column
[Oracle]UPDATE CLAIM_PRG_NOTE SET TEMP=NOTE_MEMO_CARETECH WHERE 1=1
; delete Notes column
[Oracle]ALTER TABLE CLAIM_PRG_NOTE DROP COLUMN NOTE_MEMO_CARETECH
;renaming the temp column to Notes column name
[Oracle]ALTER TABLE CLAIM_PRG_NOTE RENAME COLUMN TEMP TO NOTE_MEMO_CARETECH
[Oracle][ENDIF]
;----------------------------------------------------------------------------------
