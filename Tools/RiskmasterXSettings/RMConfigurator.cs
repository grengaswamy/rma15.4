using System;
using System.IO;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;

namespace Riskmaster.Application.RiskmasterSettings
{
    /// <summary>
    /// Provides read and write access
    /// to Riskmaster.config file information
    /// </summary>
    public class RMConfigurator : IRMConfigurator
    {

        #region Private Member Variables
            private string m_strRiskmasterConfigPath = string.Empty;
            private bool m_IsSecurityEnabled = false; 
        #endregion

        #region Class constructors
        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <remarks>Hidden (i.e. private) so that users cannot 
        /// instantiate the object using the default constructor</remarks>
        private RMConfigurator()
        {
        } // constructor


        /// <summary>
        /// OVERLOADED: Class constructor which initializes
        /// the absolute file path to the Riskmaster.config file
        /// </summary>
        /// <param name="strRiskmasterConfigPath"></param>
        public RMConfigurator(string strRiskmasterConfigPath)
        {
            //Initialize the Riskmaster.config file path
            m_strRiskmasterConfigPath = strRiskmasterConfigPath;

            //Initialize and populate the SecuritySettings property
            CheckSecuritySettings();
        } // constructor 
        #endregion

        /// <summary>
        /// Gets whether or not the Security Settings
        /// have been enabled within the Riskmaster.config file
        /// </summary>
        public bool IsSecuritySettingsEnabled
        {
            get 
            {
                return m_IsSecurityEnabled;
            }//get
        }

        /// <summary>
        /// Retrieves the Security Settings from the Riskmaster.config file
        /// </summary>
        /// <returns>NameValueCollection containing the Security Settings
        /// specified in the Riskmaster.config file</returns>
        public NameValueCollection GetSecuritySettings()
        {
            NameValueCollection nvSecuritySettings = new NameValueCollection();

            //Check whether or not the Security Settings are present
            //within the Riskmaster.config file
            if (CheckSecuritySettings())
            {
                //Populate the NameValueCollection with the values
                //from the Security Settings section of the Riskmaster.config file
                nvSecuritySettings = GetNameValueCollection("SecuritySettings");
            } // if
            else
            {
                string strErrorMsg = "The SecuritySettings section has not been enabled within the Riskmaster.config file.\n";
                strErrorMsg += "Please ensure that they are present in an uncommented form before attempting to retrieve this information.";

                throw new ApplicationException(strErrorMsg);
            } // else


            return nvSecuritySettings;
        } // method: GetSecuritySettings

        /// <summary>
        /// Checks whether or not the Security Settings section
        /// has been enabled or uncommented within the Riskmaster.config file
        /// </summary>
        /// <returns>boolean indicating whether or not the Security Settings
        /// elements are uncommented or present in the Riskmaster.config file</returns>
        private bool CheckSecuritySettings()
        {
            bool blnIsSecuritySettingsEnabled = false;


            NameValueCollection nvSecuritySettings = new NameValueCollection();

            try
            {
                nvSecuritySettings = GetNameValueCollection("SecuritySettings");

                //If there are 1 or more elements
                //present in the NameValueCollection
                if (nvSecuritySettings.Count > 0)
                {
                    //Set the boolean variable to true
                    blnIsSecuritySettingsEnabled = true;
                } // if
            }
            //The elements are commented out within the Riskmaster.config file
            //or do not exist
            catch (Exception)
            {
                blnIsSecuritySettingsEnabled = false;
            }
            finally
            {
                //Clean up
                nvSecuritySettings = null;
            } // finally

            
            //Set whether or not the Security Settings have been enabled
            m_IsSecurityEnabled = blnIsSecuritySettingsEnabled;

            //return a boolean variable 
            //indicating whether or not Security Settings have been enabled
            return blnIsSecuritySettingsEnabled;
        } // method: CheckSecuritySettings



        #region IRMConfigurator Members

        /// <summary>
        /// Gets or sets the absolute file path to the
        /// Riskmaster.config file
        /// </summary>
        public string RiskmasterConfigPath
        {
            get
            {
                return m_strRiskmasterConfigPath;
            }
            set
            {
                m_strRiskmasterConfigPath = value;
            }
        }

        /// <summary>
        /// Reads the value of the specified Xml Node
        /// </summary>
        /// <param name="strXmlNode">string containing the XPath to the 
        /// specified XmlNode contained in Riskmaster.config</param>
        /// <returns>string value of the specified XmlNode in the Riskmaster.config file</returns>
        public string Value(string strXmlNode)
        {
            string strXmlNodeValue = string.Empty;

            XmlDocument objXmlDoc = new XmlDocument();

            objXmlDoc.Load(m_strRiskmasterConfigPath);

            //Obtain a handle to the specified node
            XmlNode objXmlValueNode = objXmlDoc.SelectSingleNode("//" + strXmlNode);

            //Retrieve the inner text of the content contained in the node
            strXmlNodeValue = objXmlValueNode.InnerText;

            //If the Inner Text property is empty
            if (String.IsNullOrEmpty(strXmlNodeValue))
            {
                strXmlNodeValue = objXmlValueNode.InnerText;
            } // if

            //Clean up
            objXmlDoc = null;
            objXmlValueNode = null;

            //return the value of the specified Xml Node
            return strXmlNodeValue;
        }//method: Value()

        /// <summary>
        /// Sets the value for the specified Xml Node in the Riskmaster.config file
        /// </summary>
        /// <param name="p_strXmlNode">string containing XPath query to the specified XmlNode</param>
        /// <param name="p_strXmlNodeValue">string containing the value for the specified XmlNode</param>
        public void Value(string p_strXmlNode, string p_strXmlNodeValue)
        {
            XmlDocument objXmlDoc = new XmlDocument();

            try
            {
                //Load the Riskmaster.config file into the XmlDocument object
                objXmlDoc.Load(m_strRiskmasterConfigPath);

                XmlNode objRMConfigNode = objXmlDoc.SelectSingleNode("//" + p_strXmlNode);

                //Set the value for the Xml Node
                objRMConfigNode.InnerText = p_strXmlNodeValue;

                //Save back the changes to the Riskmaster.config file
                objXmlDoc.Save(m_strRiskmasterConfigPath);
            } // try
            catch (IOException)
            {
                string strErrorMsg = "The specified file path is incorrect or the file is currently locked.\n";
                strErrorMsg += "Please ensure that IIS has been stopped or reset prior to making your changes.";
                throw new ApplicationException(strErrorMsg);
            } // catch
            catch (XmlException)
            {
                string strErrorMsg = "Could not save the specified value given the current Xml Node Path (XPath)";
                
                throw new ApplicationException(strErrorMsg);    
            } // catch
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message, ex.InnerException);
            } // catch
            finally
            {
                //Clean up
                objXmlDoc = null;
            } // finally
        }//method: Value()

        /// <summary>
        /// Sets the value for the specified Xml Node in the Riskmaster.config file
        /// </summary>
        /// <param name="p_strXmlNode">string containing XPath query to the specified XmlNode</param>
        /// <param name="dstXmlNodeValue">DataSet containing the required Xml for replacement</param>
        public void Value(string p_strXmlNode, DataSet dstXmlNodeValue)
        {
            XmlDocument objXmlDoc = new XmlDocument();

            try
            {
                //Load the Riskmaster.config file into the XmlDocument object
                objXmlDoc.Load(m_strRiskmasterConfigPath);

                XmlNode objRMConfigNode = objXmlDoc.SelectSingleNode("//" + p_strXmlNode);

                objRMConfigNode.InnerXml = GetDataSetXml(dstXmlNodeValue);

                
                //Save back the changes to the Riskmaster.config file
                objXmlDoc.Save(m_strRiskmasterConfigPath);
            } // try
            catch (IOException)
            {
                string strErrorMsg = "The specified file path is incorrect or the file is currently locked.\n";
                strErrorMsg += "Please ensure that IIS has been stopped or reset prior to making your changes.";
                throw new ApplicationException(strErrorMsg);
            } // catch
            catch (XmlException)
            {
                string strErrorMsg = "Could not save the specified value given the current Xml Node Path (XPath)";

                throw new ApplicationException(strErrorMsg);
            } // catch
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message, ex.InnerException);
            } // catch
            finally
            {
                //Clean up
                objXmlDoc = null;
            } // finally
        }


        /// <summary>
        /// Parses the Xml Contents of a DataSet in order to retrieve only the
        /// relevant child nodes as Xml
        /// </summary>
        /// <param name="dstXmlNodeValue">DataSet containing the required Xml to be updated</param>
        /// <returns>string containing the Xml Child nodes of the DataSet</returns>
        private string GetDataSetXml(DataSet dstXmlNodeValue)
        {
            string strDataSetXml = string.Empty;
            StringBuilder strXmlBuilder = new StringBuilder();

            XmlDataDocument objXmlDataDoc = new XmlDataDocument();

            //Load the DataSet Xml into the XmlDataDocument object
            objXmlDataDoc.LoadXml(dstXmlNodeValue.GetXml());

            //Determine if there are any child elements
            if (objXmlDataDoc.DocumentElement.HasChildNodes)
            {
                //Loop through each of the child elements and build the appropriate Xml structure
                foreach (XmlNode objXmlChildNode in objXmlDataDoc.DocumentElement.ChildNodes)
                {
                    strXmlBuilder.Append(objXmlChildNode.InnerXml);
                } // foreach

            } // if

            //Retrieve the XML from the StringBuilder
            strDataSetXml = strXmlBuilder.ToString();

            //Clean up
            objXmlDataDoc = null;
            strXmlBuilder = null;

            return strDataSetXml;
        }//method: Value()


        /// <summary>
        /// Currently not implemented
        /// </summary>
        /// <param name="p_strXmlNode"></param>
        /// <param name="p_strXmlAttributeName"></param>
        /// <returns></returns>
        public string AttributeValue(string p_strXmlNode, string p_strXmlAttributeName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Currently not implemented
        /// </summary>
        /// <param name="p_strXmlNode"></param>
        /// <param name="p_strXmlAttributeName"></param>
        /// <param name="p_strXmlAttributeValue"></param>
        public void AttributeValue(string p_strXmlNode, string p_strXmlAttributeName, string p_strXmlAttributeValue)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Currently not implemented
        /// </summary>
        /// <param name="p_strXmlNode"></param>
        /// <returns></returns>
        public XmlNode NamedNode(string p_strXmlNode)
        {
            throw new Exception("THe method or operation is not implemented.");
        }
        #endregion

        /// <summary>
        /// Returns a NameValueCollection based on a specified XmlNode Collection
        /// in the Riskmaster.config file
        /// </summary>
        /// <param name="p_strXmlNode">string containing the XPath Query 
        /// to the required Xml Node</param>
        /// <returns>NameValueCollection containing the values for the XmlNode Collection</returns>
        /// <remarks>This method is particularly useful for Node Collections
        /// which may have multiple child nodes with different node names</remarks>
        public NameValueCollection XmlNodeCollection(string p_strXmlNode)
        {
            //Create a new XmlDocument object
            XmlDocument objXmlDoc = new XmlDocument();
            NameValueCollection nvXmlNodes = new NameValueCollection();

            //Load the context of the Riskmaster.config file
            //into the XmlDocument
            objXmlDoc.Load(m_strRiskmasterConfigPath);
            
            //Obtain a handle to the XmlNode object
            //based on the specified XPath Query
            XmlNode objXmlRMNode = objXmlDoc.SelectSingleNode("//" + p_strXmlNode);


            //Loop through each of the Child Nodes
            //and add the values to the DataTable
            foreach (XmlNode objXmlChildNode in objXmlRMNode.ChildNodes)
            {
                //Add the Child Node information to the NameValueCollection
                //Specify the Node Name as the Key for the collection
                //Specify the Node Value as the value for the collection
                nvXmlNodes.Add(objXmlChildNode.Name, objXmlChildNode.Value);

            } // foreach

            //Clean up
            objXmlDoc = null;
            objXmlRMNode = null;

            //return the NameValueCollection with the populated XmlNode Collection data
            return nvXmlNodes;
        } // method: XmlNodeCollection

        /// <summary>
        /// Returns a DataSet based on a specified XmlNode Collection
        /// in the Riskmaster.config file
        /// </summary>
        /// <param name="p_strXmlNode">string containing the XPath Query 
        /// to the required Xml Node</param>
        /// <returns>DataSet containing the values for the XmlNode Collection</returns>
        /// <remarks>This method is particularly useful for Node Collections
        /// such as the TrustedIPPool nodes which consist of multiple
        /// child nodes that all have the same name (i.e. IP) </remarks>
        public DataSet XmlDataNodeCollection(string p_strXmlNode)
        {
            //Create a new empty XmlDataDocument object
            XmlDataDocument objXmlDataDoc = new XmlDataDocument();
            //Create a new empty DataSet object
            DataSet dstRMNodeData = new DataSet();

            //Load the context of the Riskmaster.config file
            //into the XmlDataDocument
            objXmlDataDoc.Load(m_strRiskmasterConfigPath);

            //Obtain a handle to the XmlNode object
            //based on the specified XPath Query
            XmlNode objXmlRMNode = objXmlDataDoc.SelectSingleNode("//" + p_strXmlNode);

            //Create a new DataTable object
            //with the specified Node Name
            DataTable dtbRMNodes = new DataTable(objXmlRMNode.Name);

            //Create a column name based on the first instance
            //of the child node
            string strColumnName = objXmlRMNode.FirstChild.Name;

            //Add the specified column to the DataTable
            dtbRMNodes.Columns.Add(strColumnName);

            //Loop through each of the Child Nodes
            //and add the values to the DataTable
            foreach (XmlNode objXmlChildNode in objXmlRMNode.ChildNodes)
            {
                //Create a New DataRow to be added to the DataTable
                DataRow dtrNewRow = dtbRMNodes.NewRow();

                //Populate the DataRow with the specified node value
                dtrNewRow[strColumnName] = objXmlChildNode.InnerText;

                //Add the new DataRow to the DataTable
                dtbRMNodes.Rows.Add(dtrNewRow);
            } // foreach

            //Add the DataTable to the DataSet
            dstRMNodeData.Tables.Add(dtbRMNodes);

            //Clean up
            objXmlDataDoc = null;
            objXmlRMNode = null;

            //return the DataSet with the populated XmlNode Collection data
            return dstRMNodeData;
        } // method: XmlDataNodeCollection

        /// <summary>
        /// Reads through a set of nodes that have been specified as NameValueCollection
        /// in the Riskmaster.config file
        /// </summary>
        /// <param name="strXmlNode"></param>
        /// <returns></returns>
        private NameValueCollection GetNameValueCollection(string strXmlNode)
        {

            NameValueCollection objCol = new NameValueCollection();
            XmlDocument objXmlDoc = new XmlDocument();

            //Load the specified XML Document
            objXmlDoc.Load(m_strRiskmasterConfigPath);

            XmlNodeList nvNodes = objXmlDoc.SelectNodes("//" + strXmlNode + "[@type='NameValueCollection']/add");

            //Loop through each of the nodes and add them to the collection
            foreach (XmlNode node in nvNodes)
            {
                objCol.Add(node.Attributes["key"].Value, node.Attributes["value"].Value);
            } // foreach

            //Clean up
            nvNodes = null;
            objXmlDoc = null;

            //return the NameValueCollection
            return objCol;
        } // method: GetNameValueCollection()
    }
}
