using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    public class FileProcessing
    {
        /// <summary>
        /// Reads the contents of a file into a single long string
        /// </summary>
        /// <param name="strFilePath">string containing the path to the file</param>
        /// <returns>string containing the entire contents of the file</returns>
        public static string ReadFile(string strFilePath)
        {
            string strFileText = string.Empty;

            try
            {
                strFileText = File.ReadAllText(strFilePath);
            }
            catch (IOException)
            {

                throw new IOException("File could not be found.");
            }

            return strFileText;
        }//method: ReadFile

        /// <summary>
        /// Reads the contents of a file into a string array of elements
        /// containing each line of the file
        /// </summary>
        /// <param name="strFilePath">string containing the path to the file</param>
        /// <returns>string array containing the entire contents of the file line-by-line</returns>
        public static string[] ReadFileContents(string strFilePath)
        {

            string[] arrFileText = null;

            try
            {
                arrFileText = File.ReadAllLines(strFilePath);
            }
            catch (IOException)
            {

                throw new IOException("File could not be found.");
            }

            return arrFileText;
        }//method: ReadFileContent()

        /// <summary>
        /// Writes out the contents of a file when passed a single long string
        /// </summary>
        /// <param name="strFilePath">string containing the path to the file</param>
        /// <param name="strFileText">string containing the text to be written out to the file</param>
        public static void WriteFile(string strFilePath, string strFileText)
        {
            try
            {
                //Write the entire contents of the string
                //to the specified file
                File.WriteAllText(strFilePath, strFileText);
            }
            catch (IOException)
            {

                throw new IOException("File could not be found.");
            }
        }//method: WriteFile()

        /// <summary>
        /// Writes out the contents of a file when passed a string array
        /// of elements containing each line of the file to be written
        /// </summary>
        /// <param name="strFilePath">string containing the path to the file</param>
        /// <param name="arrFileText">string array containing each line of the file to be written</param>
        public static void WriteFile(string strFilePath, string[] arrFileText)
        {
            try
            {
                File.WriteAllLines(strFilePath, arrFileText);
            }
            catch (IOException)
            {

                throw new IOException("File could not be found.");
            }
        }//method: WriteFile()




    }
}
