using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Application.RiskmasterSettings
{
    public class GlobalASAConfiguration: RiskmasterSettings, IRiskmasterSettings
    {
        private string m_strGlobalASAPath = string.Empty;

        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <remarks>Hidden (i.e. private) so that users cannot 
        /// instantiate the object using the default constructor</remarks>
        private GlobalASAConfiguration()
        {
            
        } // constructor

        /// <summary>
        /// OVERLOADED: class constructor
        /// to initialize the Global.asa path
        /// with the specified file path
        /// </summary>
        /// <param name="strGlobalASAPath">string containing file path to the Global.asa file</param>
        public GlobalASAConfiguration(string strGlobalASAPath)
        {
            m_strGlobalASAPath = strGlobalASAPath;
        } // constructor



        #region IRiskmasterSettings Members

        public string SettingType
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// Gets and sets the path to the Global.asa file
        /// </summary>
        public string GlobalASAPath
        {
            get
            {
                return m_strGlobalASAPath;
            }
            set
            {
                m_strGlobalASAPath = value;
            }//set
        } // property GlobalASAPath

        /// <summary>
        /// Reads a particular Application Setting from the Global.asa file
        /// and returns the value of the specified Application Setting
        /// </summary>
        /// <param name="nvCollSettings">Name Value Collection indicating the name of the Application 
        /// Setting to be read</param>
        /// <returns>the value of the Application Setting in the Global.asa file</returns>
        public string ReadSetting(System.Collections.Specialized.NameValueCollection nvCollSettings)
        {
            string strSettingValue = string.Empty;
            const int CONN_STRING_MATCH = 1;
            const string DOUBLE_QUOTE_REPLACEMENT = @"""";

            //Retrieve the Generic String Collection
            List<string> arrGlobalASAStrings = GetNVCollectionValues(nvCollSettings);

            //Retrieve the appropriate Registry Key and corresponding Key Value
            string strSettingName = GetListString(arrGlobalASAStrings);

            //Read all the lines of the Global.asa file
            string[] arrFileLines = FileProcessing.ReadFileContents(m_strGlobalASAPath);

            //Loop through each line of the Global.asa file
            foreach (string strLine in arrFileLines)
            {
                //Find the matching string for the Global.asa setting
                if (RegExManager.IsMatch(strLine, RMRegularExpressions.strApplication))
                {
                    //Perform an additional comparison
                    //to verify that the Application Setting 
                    //actually contains the specified setting
                    if (strLine.Contains(strSettingName))
                    {
                        string[] strMatches = RegExManager.MatchGroups(strLine, RMRegularExpressions.strApplicationConnString);

                        //Update the entire value of the connection string
                        strSettingValue = strMatches[CONN_STRING_MATCH];

                        //NOTES: If the string contains double quotes (as it probably does in most instances)
                        //Replace the double quotes and return the standalone string value
                        strSettingValue.Replace(DOUBLE_QUOTE_REPLACEMENT, string.Empty);

                        break; //exit the loop after the 1st match
                    }//if
                }//if
            }//foreach

            //Return the string value of the respective Global.asa setting
            
            return strSettingValue;
        }

        /// <summary>
        /// Writes a specified new setting value to the Global.asa file to replace an 
        /// existing Application Setting value currently present in the file
        /// </summary>
        /// <param name="nvCollSettings">Name Value collection containing the name of the Application Setting 
        /// to be replaced in the Global.asa file</param>
        /// <param name="strSettingValue">value to be replaced for the specified Application Setting in the Global.asa file</param>
        public void WriteSetting(System.Collections.Specialized.NameValueCollection nvCollSettings, string strSettingValue)
        {
            const int CONN_STRING_MATCHES = 2;
            const string DOUBLE_QUOTE_REPLACEMENT = @"""";

            //Retrieve the Generic String Collection
            List<string> arrGlobalASAStrings = GetNVCollectionValues(nvCollSettings);

            //Retrieve the appropriate Global.asa Application Settings
            string strSettingName = GetListString(arrGlobalASAStrings);

            //Read all the lines of the Global.asa file
            string[] arrFileLines = FileProcessing.ReadFileContents(m_strGlobalASAPath);

            //Create a Generic String ArrayList for holding the contents to be written back out
            //to the Global.asa file
            List<string> arrWriteLines = new List<string>();

            //Declare variables for setting values and replacements
            string strCurrentSettingValue = string.Empty;            


            //Surround the existing replacement string with double quotes
            //NOTE: No longer needed because this is handled by the Regular Expression matches
            //strSettingValue = DOUBLE_QUOTE_REPLACEMENT + strSettingValue + DOUBLE_QUOTE_REPLACEMENT;

            //Loop through each line of the Global.asa file
            foreach (string strLine in arrFileLines)
            {
                string strWriteLine = strLine;

                //Find the matching string for the Global.asa setting
                if (RegExManager.IsMatch(strWriteLine, RMRegularExpressions.strApplication))
                {
                    //Perform an additional comparison
                    //to verify that the Application Setting 
                    //actually contains the specified setting
                    if (strWriteLine.Contains(strSettingName))
                    {
                        string[] strMatches = RegExManager.MatchGroups(strWriteLine, RMRegularExpressions.strApplicationConnString);

                        //There should be the specified number of matches
                        //in order to ensure that the connection string expression
                        //has been parsed out correctly
                        if (strMatches.Length == CONN_STRING_MATCHES)
                        {
                            //Read out the current setting value
                            strCurrentSettingValue = strMatches[CONN_STRING_MATCHES - 1];

                            //Perform the string replacement in the line with the new setting value
                            strWriteLine = strWriteLine.Replace(strCurrentSettingValue, strSettingValue);
                        }//if
                    }//if
                }//if

                //Add the output line to the ArrayList
                arrWriteLines.Add(strWriteLine);
            }//foreach

            //Update the Global.asa setting through use of a string replacement or Regular Expression replacement
            //with the specified setting value
            FileProcessing.WriteFile(m_strGlobalASAPath, arrWriteLines.ToArray());

        }//method: WriteSetting()

        #endregion
    }
}
