using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Microsoft.Win32;
using System.Collections;

namespace Riskmaster.Application.RiskmasterSettings
{
    public class RegistryConfiguration: RiskmasterSettings, IRiskmasterSettings
    {
        private string m_strSettingType = string.Empty;


        #region IRiskmasterSettings Properties
        public string SettingType
        {
            get
            {
                return m_strSettingType;
            }
            set
            {
                m_strSettingType = value;
            }
        } 
        #endregion

        #region IRiskmasterSettings Members

        /// <summary>
        /// Reads a setting from the Registry based on the Registry Key
        /// and the Registry Key value specified in the Name Value Collection
        /// </summary>
        /// <param name="nvCollSettings">Name Value Collection containing the combintaion
        /// of the Registry Key and the specified Registry Key value to be retrieved</param>
        /// <returns></returns>
        public string ReadSetting(System.Collections.Specialized.NameValueCollection nvCollSettings)
        {
            string strRegistryKey = string.Empty;
            string strRegistryKeyString = string.Empty;
            string strRegistryKeyValue = string.Empty;

            //Retrieve the Generic String Collection
            List<string> arrRKStrings = GetNVCollectionValues(nvCollSettings);

            //Retrieve the appropriate Registry Key and corresponding Key Value
            GetListString(arrRKStrings, ref strRegistryKey, ref strRegistryKeyString);

            RegistryKey objRK = null;

            try
            {
                //Read the specified Registry Key
                objRK = Registry.LocalMachine.OpenSubKey(strRegistryKey);

                //Read the specified Registry Key string
                //and extract the specified value for the Registry Key String
                strRegistryKeyValue = objRK.GetValue(strRegistryKeyString).ToString();
            }
            catch (Exception ex)
            {

                throw new ApplicationException(ex.Message, ex.InnerException);
            }
            finally
            {
                //Clean up
                arrRKStrings = null;
                objRK = null;
            } // finally


            //return the Registry Key value as the value of the function
            return strRegistryKeyValue;
        }

        /// <summary>
        /// Writes a setting to the Registry based on the Registry Key
        /// and the Registry Key value specified in the Name Value Collection
        /// </summary>
        /// <param name="nvCollSettings">Name Value Collection containing the combintaion
        /// of the Registry Key and the specified Registry Key value to be written</param>
        /// <returns></returns>
        public void WriteSetting(System.Collections.Specialized.NameValueCollection nvCollSettings, string strSettingValue)
        {
            string strRegistryKey = string.Empty;
            string strRegistryKeyString = string.Empty;
            string strRegistryKeyValue = string.Empty;

            //Retrieve the Generic String Collection
            List<string> arrRKStrings = GetNVCollectionValues(nvCollSettings);

            //Retrieve the appropriate Registry Key and corresponding Key Value
            GetListString(arrRKStrings, ref strRegistryKey, ref strRegistryKeyString);

            RegistryKey objRK = null;

            try
            {
                //Create the specified Registry Key
                objRK = Registry.LocalMachine.CreateSubKey(strRegistryKey);

                //Read the specified Registry Key string
                //and extract the specified value for the Registry Key String
                objRK.SetValue(strRegistryKeyString, strSettingValue);
            }
            catch (Exception ex)
            {

                throw new ApplicationException(ex.Message, ex.InnerException);
            }
            finally
            {
                //Clean up
                arrRKStrings = null;
                objRK = null;
            } // finally

            
        }

        #endregion

       

    }//class
}
