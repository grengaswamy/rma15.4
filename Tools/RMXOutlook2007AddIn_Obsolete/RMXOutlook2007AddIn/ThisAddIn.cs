﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Configuration;
using System.Collections;

namespace RMXOutlook2007AddIn
{
    public partial class ThisAddIn
    {
        //Class variables
        Outlook.MailItem eMail = null; 
        Office.CommandBar newToolBar;
        Office.CommandBarButton firstButton;
        Office.CommandBarButton secondButton;
        Outlook.Explorers selectExplorers;
        Outlook.Inspectors Inspectors; //jramkumar for MITS 35398
        TextWriterTraceListener oTraceListener = null;
        TraceSwitch oTraceSwitch = null;

        //Formats in which the emails can be uploaded
        private const string MSG = ".msg";
        private const string TXT = ".txt";
        private const string DOC = ".doc";
        private const string HTML = ".html";

        /// <summary>
        /// This function is called when Outlook starts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {

          
            try
            {
                //Initialize trace switch
                string sLogFile = String.Format("{0}" + ConfigurationManager.AppSettings["LogFileName"], ConfigurationManager.AppSettings["TemplatePath"]);
                oTraceListener = new TextWriterTraceListener(sLogFile);
                Trace.Listeners.Add(oTraceListener);
                Trace.AutoFlush = true;
                oTraceSwitch = new TraceSwitch("mySwitch", "Entire application");
                                
                //event handlers
                this.Application.ItemSend += new Outlook.ApplicationEvents_11_ItemSendEventHandler(MyItemSendEventHandler);
                selectExplorers = this.Application.Explorers;
                selectExplorers.NewExplorer += new Outlook
                    .ExplorersEvents_NewExplorerEventHandler(newExplorer_Event);

                //Adding the new toolbar and its buttons
                AddToolbar();

                //jramkumar for MITS 35398
                Inspectors = this.Application.Inspectors;
                Inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(AddToolbar);

            }
            catch (Exception ex)
            {
                if (oTraceListener != null)
                {
                    oTraceListener.WriteLine(String.Format("{0}. {1}", "TimeStamp:", DateTime.Now.ToString()));
                    oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));
                }
                
                MessageBox.Show("An error has occurred while starting RMX plug-in.");
            }
            finally
            {
               
            }

        }

        //jramkumar for MITS 35398
        private void AddToolbar(Outlook.Inspector Inspector)
        {
            try
            {
                foreach (Office.CommandBar ObjCmdBar in Inspector.CommandBars)
                {
                    if (ObjCmdBar.Name == "NewToolBar")
                    {
                        ObjCmdBar.Delete();
                    }
                }

                newToolBar = (Office.CommandBar)Inspector.CommandBars.Add("NewToolBar", Office.MsoBarPosition.msoBarTop, false, true);

                Office.CommandBarButton button_1 = (Office.CommandBarButton)newToolBar.Controls.Add(1, missing, missing, missing, missing);
                button_1.Style = Office.MsoButtonStyle.msoButtonCaption;
                button_1.Caption = "New RMX Email";
                button_1.Tag = "Button1";

                Office.CommandBarButton button_2 = (Office.CommandBarButton)newToolBar.Controls.Add(1, missing, missing, missing, missing);
                button_2.Style = Office.MsoButtonStyle.msoButtonCaption;
                button_2.Caption = "Upload Mail Item(s) To RMX";
                button_2.Tag = "Button2";

                newToolBar.Visible = true;
            }
            catch (Exception ex)
            {
                if (oTraceListener != null)
                {
                    oTraceListener.WriteLine(String.Format("{0}. {1}", "TimeStamp:", DateTime.Now.ToString()));
                    oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));
                }
                MessageBox.Show("An error has occurred while creating the toolbar for RMX plug-in.");
            }
            finally
            {

            }
        }
		
        private void newExplorer_Event(Outlook.Explorer new_Explorer)
        {
            try
            {
                ((Outlook._Explorer)new_Explorer).Activate();
                newToolBar = null;

                //Adding the new toolbar and its buttons
                AddToolbar();

                //jramkumar for MITS 35398
                Inspectors = this.Application.Inspectors;
                Inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(AddToolbar);
            }
            catch (Exception ex)
            {
                if (oTraceListener != null)
                {
                    oTraceListener.WriteLine(String.Format("{0}. {1}", "TimeStamp:", DateTime.Now.ToString()));
                    oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));
                }
                MessageBox.Show("An error has occurred while creating the toolbar for RMX plug-in.");
            }
            finally
            {
               
            }
        }
        /// <summary>
        /// Adds custom buttons to new Outlook toolbar
        /// </summary>
        private void AddToolbar()
        {
            try
            {
                //Add the toolbar if it does not already exist
                if (newToolBar == null)
                {
                    Office.CommandBars cmdBars =
                        this.Application.ActiveExplorer().CommandBars;
                    newToolBar = cmdBars.Add("NewToolBar",
                        Office.MsoBarPosition.msoBarTop, false, true);
                }
           
                //Add button to the new toolbar. Clicking this button will open the
                //custom RMX send email template
                Office.CommandBarButton button_1 =
                    (Office.CommandBarButton)newToolBar.Controls
                    .Add(1, missing, missing, missing, missing);
                button_1.Style = Office
                    .MsoButtonStyle.msoButtonCaption;
                button_1.Caption = "New RMX Email";
                button_1.Tag = "Button1";
                if (this.firstButton == null)
                {
                    this.firstButton = button_1;
                    firstButton.Click += new Office.
                        _CommandBarButtonEvents_ClickEventHandler
                        (ButtonClick);
                }


                //Add button to the new toolbar.User can select email items
                // from the inbox.Clicking this button will open the
                //the claim info form
                Office.CommandBarButton button_2 = (Office
                    .CommandBarButton)newToolBar.Controls.Add
                    (1, missing, missing, missing, missing);
                button_2.Style = Office
                    .MsoButtonStyle.msoButtonCaption;
                button_2.Caption = "Upload Mail Item(s) To RMX";
                button_2.Tag = "Button2";
                newToolBar.Visible = true;
                if (this.secondButton == null)
                {
                    this.secondButton = button_2;
                    secondButton.Click += new Office.
                        _CommandBarButtonEvents_ClickEventHandler
                        (Button2Click);
                }
            }
            catch (Exception ex)
            {
                if (oTraceListener != null)
                {
                    oTraceListener.WriteLine(String.Format("{0}. {1}", "TimeStamp:", DateTime.Now.ToString()));
                    oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));
                }
                MessageBox.Show("An error has occurred while creating the toolbar for RMX plug-in.");
            }
            finally
            {
                
            }
        }
        /// <summary>
        /// Upload Mail Item to RMX button click event handler
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="cancel"></param>
        private void Button2Click(Office.CommandBarButton ctrl,
                ref bool cancel)
        {

            try
            {
                Outlook.Selection sel;
                try
                {
                    sel = this.Application.ActiveExplorer().Selection;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please select at least one email item to upload");
                    return;
                }
                if (sel.Count > 0)
                {
                    ClaimInfo frm = new ClaimInfo(this.Application.ActiveExplorer().Selection);

                    frm.Show();
                }
                else
                {
                    MessageBox.Show("Please select at least one email item to upload");
                    return;
                }
            }
            catch (Exception ex)
            {
                if (oTraceListener != null)
                {
                    oTraceListener.WriteLine(String.Format("{0}. {1}", "TimeStamp:", DateTime.Now.ToString()));
                    oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));
                }
                
                MessageBox.Show("An error has occurred while opening the Claim Information Form for RMX plug-in.");
            }
            finally
            {
                
            }
        }
        /// <summary>
        /// New RMX Email button click event handler
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="cancel"></param>
        private void ButtonClick(Office.CommandBarButton ctrl,
                ref bool cancel)
        {
            
            try
            {
                Outlook.MAPIFolder folder =
                this.Application.Session.GetDefaultFolder(
                Outlook.OlDefaultFolders.olFolderDrafts) as Outlook.MAPIFolder;
                
                //Get the custom template from the path in config
                string sTemplatePath = String.Format("{0}" + ConfigurationManager.AppSettings["TemplateFileName"], ConfigurationManager.AppSettings["TemplatePath"]);
                Outlook.MailItem TemplateItem = this.Application.CreateItemFromTemplate(sTemplatePath, folder) as Outlook.MailItem;
                TemplateItem.Display(false);
            }
            catch (Exception ex)
            {
                if (oTraceListener != null)
                {
                    oTraceListener.WriteLine(String.Format("{0}. {1}", "TimeStamp:", DateTime.Now.ToString()));
                    oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));
                }
                MessageBox.Show("An error has occurred while opening the custom template for RMX plug-in.");
            }
            finally
            {
                
            }
        }


        public void MyItemSendEventHandler(object Item, ref bool Cancel)
        {

            bool bAttachToRMXTemplate = false;
            bool bSendAttachmentsToRMX = false;
            bool bDoNotAttachToRMXStandard = false;
            string sFormat = string.Empty;
            int iEmailUploadPromptLevel = 1;
            string sClaimNumber = string.Empty;
            string sTempEmailPath = null;
            string Labeltext = string.Empty;
            
            
                
            
            try
            {
                if (Item is Outlook.MailItem)
                {
                    eMail = (Outlook.MailItem)Item;

                    for (int i = 1; i <= eMail.UserProperties.Count; i++)
                    {
                        
                        if (eMail.UserProperties[i] != null && string.Compare(eMail.UserProperties[i].Name, "Send Attachments To RMX") == 0 && eMail.UserProperties[i].Value != null && string.Compare(eMail.UserProperties[i].Value.ToString().ToLower(), "true") == 0)
                        {
                            bSendAttachmentsToRMX = true;
                            
                           
                        }
                        else if (eMail.UserProperties[i] != null && string.Compare(eMail.UserProperties[i].Name, "Claim Number") == 0)
                        {
                            bDoNotAttachToRMXStandard = true;
                            if (eMail.UserProperties[i].Value != null && string.Compare(eMail.UserProperties[i].Value.ToString(), "") != 0)
                            {
                                bAttachToRMXTemplate = true;
                                sClaimNumber = eMail.UserProperties[i].Value.ToString();

                            }
                        }
                        else if (eMail.UserProperties[i] != null && string.Compare(eMail.UserProperties[i].Name, "ListBoxField") == 0 && eMail.UserProperties[i].Value != null && string.Compare(eMail.UserProperties[i].Value.ToString(), "") != 0)
                        {
                            sFormat = eMail.UserProperties[i].Value.ToString();
                            
                        }
						//rsharma220 RMA-136
                        else if (eMail.UserProperties[i] != null && string.Compare(eMail.UserProperties[i].Name, "Type") == 0)
                        {
                            if (eMail.UserProperties[i].Value != null && string.Compare(eMail.UserProperties[i].Value.ToString(), "True") == 0)
                            {
                                Labeltext = "Claim Number";
                            }
                            else
                            {
                                Labeltext = "Event Number";
                            }
                        }
                    }
                    if (bAttachToRMXTemplate)//When the item_send event is called from RMX template
                    {
                        if (string.IsNullOrEmpty(sClaimNumber) || string.IsNullOrEmpty(sFormat))
                        {
                            return;
                        }
                        else
                        {
                            int iNumberOfUserProperties = eMail.UserProperties.Count;
                            for (int i = 1; i <= iNumberOfUserProperties; i++)
                            {
                                eMail.UserProperties.Remove(1);
                            }
                            if (string.Compare(sFormat, MSG) == 0 && !bSendAttachmentsToRMX)
                            {
                                if (MessageBox.Show("Email Item in .msg format cannot be uploaded without attachments. Do you want to upload it with attachments ?", "Confirm Upload", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    bSendAttachmentsToRMX = true;
                                }
                                else
                                {
                                    return;
                                }
                                
                            }
                            DocumentAttachWrapper objDocAttach = new DocumentAttachWrapper();
                            //rsharma220 RMA-136
							objDocAttach.CreateDocDetails(sFormat, eMail, bSendAttachmentsToRMX, sClaimNumber, Labeltext);
                        }
                        MessageBox.Show("Email Attached To RMX");
                        return;
                    }
                    else if(!bDoNotAttachToRMXStandard)//if the call is from standard template
                    {
                        iEmailUploadPromptLevel = Convert.ToInt16(ConfigurationManager.AppSettings["EmailUploadPromptLevel"]);
                        switch (iEmailUploadPromptLevel)
                        {
                            case 1://Never Prompt to ask for upload
                                return;

                            case 2://Prompt only when subject has the word "claim"
                                string sSubjectSearchWord = ConfigurationManager.AppSettings["SubjectSearchWord"].ToUpper();
                                if (eMail.Subject.ToUpper().IndexOf(sSubjectSearchWord) > -1)
                                {
                                    if (MessageBox.Show("Upload Email To RMX ?", "Confirm Upload", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                    {
                                        sTempEmailPath = ConfigurationManager.AppSettings["TemplatePath"] + "TempMailItem.msg";
                                        Outlook.MAPIFolder folder = this.Application.Session.GetDefaultFolder(
                                                                     Outlook.OlDefaultFolders.olFolderDrafts) as Outlook.MAPIFolder;
                                        if (File.Exists(sTempEmailPath))
                                        {
                                            File.Delete(sTempEmailPath);
                                        }
                                        eMail.SaveAs(ConfigurationManager.AppSettings["TemplatePath"] + "TempMailItem.msg", Outlook.OlSaveAsType.olMSG);
                                        Outlook.MailItem savedEmail = this.Application.CreateItemFromTemplate(ConfigurationManager.AppSettings["TemplatePath"] + "TempMailItem.msg", folder) as Outlook.MailItem;
                                        ClaimInfo frm = new ClaimInfo(savedEmail);
                                        frm.Show();
                                        File.Delete(sTempEmailPath);
                                        return;
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                                else
                                {
                                    return;
                                }

                            case 3://Always prompt to ask for upload
                                if (MessageBox.Show("Upload Email To RMX ?", "Confirm Upload", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    sTempEmailPath = ConfigurationManager.AppSettings["TemplatePath"] + "TempMailItem.msg";
                                    Outlook.MAPIFolder folder = this.Application.Session.GetDefaultFolder(
                                                                       Outlook.OlDefaultFolders.olFolderDrafts) as Outlook.MAPIFolder;
                                    if (File.Exists(sTempEmailPath))
                                    {
                                        File.Delete(sTempEmailPath);
                                    }
                                    eMail.SaveAs(sTempEmailPath, Outlook.OlSaveAsType.olMSG);
                                    Outlook.MailItem savedEmail = this.Application.CreateItemFromTemplate(sTempEmailPath, folder) as Outlook.MailItem;
                                    ClaimInfo frm = new ClaimInfo(savedEmail);
                                    frm.Show();
                                    File.Delete(sTempEmailPath);
                                    return;
                                }
                                else
                                {
                                    return;
                                }

                            default://Don't prompt to ask for upload
                                return;

                        }
                    }

                    

                }

                else
                {
                    return;
                }
                

            }
            catch (Exception ex)
            {
                if (oTraceListener != null)
                {
                    oTraceListener.WriteLine(String.Format("{0}. {1}", "TimeStamp:", DateTime.Now.ToString()));
                    oTraceListener.WriteLine(String.Format("{0}. {1}", ex.Message, ex.StackTrace));
                }
                MessageBox.Show("An error has occurred while sending the email item to RMX.Please check whether the claim exists and then try again.");
            }
            finally
            {
                File.Delete(sTempEmailPath);
            }

        }
        
        
        /// <summary>
        /// This function is called when Outlook is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            if (oTraceListener != null)
                oTraceListener.Close();
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
