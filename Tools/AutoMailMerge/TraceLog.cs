﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AutoMailMergeScheduler
{
     public static class TraceLog
    {
         public static StringBuilder sb = new StringBuilder();

         public static StringBuilder tempsb = new StringBuilder();

         public static void WriteFile(string FilePath, string FileContent, String FileName)
         {
             FileStream errorFileStream = null;
            
             if (!File.Exists(FilePath))
             {
                 errorFileStream = File.Create(FilePath);
                 
             }
             else
             {
                 errorFileStream = File.Open(FilePath, FileMode.Open);
             }

             using (StreamWriter outfile = new StreamWriter(errorFileStream))
             {
                 outfile.Write(FileContent);
             }

         }
    }
    //Added the class for error Handling : Anu Tennyson Starts
     public static class ErrorLog
     {
         public static StringBuilder sbTempErrorLog = new StringBuilder();
         public static string sErrorFilePath = string.Empty;
         public static string sJobId = string.Empty;
              
              
         public static void WriteErrorFile()
         {
             FileStream errorFileStream = null;

             if (sbTempErrorLog.Length > 0)
             {
                 if (!string.IsNullOrEmpty(sbTempErrorLog.ToString()))
                 {
                     if (!File.Exists(sErrorFilePath + "_" + sJobId + "_EXE_ERROR.txt"))
                     {
                         errorFileStream = File.Create(sErrorFilePath + "_" + sJobId + "_EXE_ERROR.txt");

                     }

                     using (StreamWriter outfile = new StreamWriter(errorFileStream))
                     {
                         outfile.Write(sbTempErrorLog.ToString());
                     }
                 }
             }

         }
     }
    //Ends
}
