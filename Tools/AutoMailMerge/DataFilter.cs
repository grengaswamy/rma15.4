﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Settings;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.ExceptionTypes;
using System.Text.RegularExpressions;
using Riskmaster.Application.MailMerge;
using System.Collections;
using Riskmaster.Security;
using System.Reflection;
using Microsoft.Office.Interop.Word;
using C1.C1Zip;
using System.Data;
using System.Drawing.Printing;
using System.Drawing;


namespace AutoMailMergeScheduler
{
    public class Constants
    {
        public static string sFilePath = string.Empty;

    }

    public class DataFilter
    {
        #region "Member Variables"
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = string.Empty;
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = string.Empty;
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = string.Empty;
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>		
        private string m_sDsnName = string.Empty;
        /// <summary>
        /// Private variable to store Document Path Name
        /// </summary>		
        private string m_sDocPath = string.Empty;
        /// <summary>
        /// Private variable to store Document Path Type
        /// </summary>		
        private int m_iDocPathType = 0;
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;

        ///// <summary>
        ///// Info setting Object
        ///// </summary>
        private InfoSetting objInfoSet;

        /// <summary>
        /// Info definition Object
        /// </summary>
        private InfoDefinition objInfoDef;

        /// <summary>
        /// Database type
        /// </summary>
        private int m_iDbType = 0;

        private UserLogin OuserLoginvalue = null;

        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_Tracelogfilename = string.Empty;
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>		
        private string m_TracelogfilePath = string.Empty;
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>		
        public string m_JobId = string.Empty;

        private int m_iClientId = 0;//rkaur27
       

        #endregion

        #region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value.
		/// </summary>
        /// <param name="p_sConnectionString">Connection String</param>
        /// <param name="p_iDBType">DB Type</param>
        /// <param name="p_sDocumentPath">Document Path</param>
        /// <param name="p_iDocumentType">Document Type</param>
        /// <param name="p_sDSNName">DSN Name</param>
        /// <param name="p_sUsername">User Name</param>
        /// <param name="p_spassword">Password</param>
        /// <param name="ouserLogin">UserLogin</param>
        /// <param name="Tracelogfilename">Tracelogfilename</param>
        /// <param name="TracelogfilePath">TracelogfilePath</param>
        /// <param name="p_sJobID">JobID</param>
        public DataFilter(string p_sConnectionString, int p_iDBType, string p_sDocumentPath, int p_iDocumentType, string p_sDSNName, string p_sUsername, string p_spassword, UserLogin ouserLogin, string Tracelogfilename, string TracelogfilePath,string p_sJobID, int p_iClientId)//rkaur27
		{
            try
            {
                m_sConnectionString = p_sConnectionString;
                m_iDbType = Convert.ToInt32(p_iDBType);
                m_sDocPath = p_sDocumentPath;
                m_iDocPathType = p_iDocumentType;
                m_sDsnName = p_sDSNName;
                m_sUserName = p_sUsername;
                m_sPassword = p_spassword;
                OuserLoginvalue = ouserLogin;
                m_Tracelogfilename = Tracelogfilename;
                m_TracelogfilePath = TracelogfilePath;
                m_JobId = p_sJobID;
                m_iClientId = p_iClientId;//rkaur27

            }
            catch (Exception exc)
            {
                throw exc;
            }
		}
		#endregion
        public void Data()
        {
            LocalCache objLocalCache = null;
            DbReader objRdr = null;
            DbReader objFilterReader = null;
            int nFilters;
            int nTempFilters;
            Int64 iSinceLast;
            string ssql = string.Empty;
            string sMergeSQL = string.Empty;
            try
            {
                Console.WriteLine("0 ^*^*^ fetching data From Database");
                TraceLog.sb.AppendLine(DateTime.Now + " " + "fetching data From Database");
                TraceLog.WriteFile(m_TracelogfilePath, TraceLog.sb.ToString(), m_Tracelogfilename);

                string templateTracelogfilename = string.Empty;
                string templateTracelogfilePath = string.Empty;
                string FolderPath = string.Empty;
                using (objRdr = DbFactory.GetDbReader(m_sConnectionString, "SELECT * FROM MERGE_AUTOSET"))
                {

                    if (objRdr != null)
                    {
                        while (objRdr.Read())
                        {
                            objInfoSet.RowID = objRdr.GetInt32("AUTO_ROW_ID");
                            objInfoSet.Name = objRdr.GetString("AUTO_NAME");
                            objInfoSet.Index = objRdr.GetInt32("DEF_INDEX");
                            objInfoSet.DefName = objRdr.GetString("DEF_NAME");
                            
                            LoadInfoDef(objInfoSet.Index, objInfoSet.DefName);
                            if (!string.IsNullOrEmpty(objInfoDef.Name))
                            {
                                objInfoSet.DefName = objInfoDef.Name;
                                objInfoSet.ProcessDate = objRdr.GetString("PROCESS_DATE");

                                objInfoSet.TemplateID = objRdr.GetInt32("LETTER_ID");
                                objInfoSet.TemplateName = objRdr.GetString("LETTER_NAME");
                                objInfoSet.PrinterName = objRdr.GetString("PRINTER_NAME");
                                objInfoSet.PaperBin = objRdr.GetString("LETTER_PAPER_BIN");
                                objInfoSet.LastRun = objRdr.GetString("LAST_RUN");

                                objInfoSet.FilterSet = new FilterSetting[100];
                                sMergeSQL = "SELECT * FROM MERGE_AUTO_FILTER WHERE AUTO_ID = " + objInfoSet.RowID;
                                nFilters = 0;
                                using (objFilterReader = DbFactory.GetDbReader(m_sConnectionString, sMergeSQL))
                                {
                                    if (objFilterReader != null)
                                    {
                                        while (objFilterReader.Read())
                                        {
                                            objInfoSet.FilterSet[nFilters].Number = objFilterReader.GetInt32("FILTER_INDEX");
                                            objInfoSet.FilterSet[nFilters].Data = objFilterReader.GetString("FILTER_DATA");
                                            nFilters = nFilters + 1;
                                        }
                                    }
                                }

                                objInfoSet.NumFilters = nFilters;
                                objFilterReader.Close();

                                nTempFilters = 0;
                                objInfoSet.TemplateFilterSet = new TemplateFilterSetting[100];
                                using (objFilterReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT * FROM MERGE_TBR_FILTER WHERE AUTO_ID = " + objInfoSet.RowID))
                                {
                                    if (objFilterReader != null)
                                    {
                                        while (objFilterReader.Read())
                                        {
                                            objInfoSet.TemplateFilterSet[nTempFilters].Number = objFilterReader.GetInt32("CATEGORY_ID");
                                            objInfoSet.TemplateFilterSet[nTempFilters].Data = objFilterReader.GetString("TBR_FILTER_DATA");
                                            nTempFilters = nTempFilters + 1;
                                        }
                                    }
                                }

                                objInfoSet.TempNumFilters = nTempFilters;
                                objFilterReader.Close();

                                using (objFilterReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT * FROM MERGE_ENTITY_ROLE WHERE AUTO_ID = " + objInfoSet.RowID))
                                {
                                    if (objFilterReader != null)
                                    {
                                        while (objFilterReader.Read())
                                        {
                                            objInfoSet.PersonInvolvedValue = objFilterReader.GetString("PERSON_INV_ROLE");
                                            objInfoSet.AdjusterValue = objFilterReader.GetString("ADJUSTER_ROLE");
                                            objInfoSet.CasemanagerValue = objFilterReader.GetString("CASEMANAGER_ROLE");
                                            objInfoSet.DefendantValue = objFilterReader.GetString("DEFENDANT_TYPE");
                                            objInfoSet.ClaimantValue = objFilterReader.GetString("CLAIMANT_TYPE");
                                            objInfoSet.ExpertWitnessValue = objFilterReader.GetString("EXPERTWITNESS_TYPE");
                                        }
                                    }

                                }
                            
                                objFilterReader.Close();

                                if (string.IsNullOrEmpty(objRdr.GetString("LAST_RUN")))
                                {
                                    iSinceLast = 0;
                                }
                                else
                                {
                                  DateTime yy = Convert.ToDateTime(Conversion.GetDBDateFormat(objRdr.GetString("LAST_RUN"), "d"));
                                  DateTime mm = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                                  TimeSpan cc =   mm-yy;
                                  iSinceLast = Convert.ToInt64(cc.TotalDays);
                                  if (iSinceLast > 0)
                                  {
                                      iSinceLast = iSinceLast - 1;
                                  }
                                }

                                FolderPath = Constants.sFilePath + m_JobId + "_" + objInfoSet.RowID;

                                templateTracelogfilename = "ProcessLog.txt";
                                if (!System.IO.Directory.Exists(FolderPath)) 
                                {
                                    System.IO.Directory.CreateDirectory(FolderPath); 
                                }

                                templateTracelogfilePath = FolderPath + "\\" + templateTracelogfilename;

                                TraceLog.tempsb.AppendLine(TraceLog.sb.ToString());
                                TraceLog.WriteFile(templateTracelogfilePath, TraceLog.tempsb.ToString(), templateTracelogfilename);

                                InfoRun(iSinceLast, templateTracelogfilePath,templateTracelogfilename);
                                using (DbConnection dbConnection = DbFactory.GetDbConnection(m_sConnectionString))
                                {
                                    dbConnection.Open();
                                    DbCommand cmdInsert = dbConnection.CreateCommand();
                                    cmdInsert.CommandText = "UPDATE MERGE_AUTOSET SET LAST_RUN = '" + DateTime.Now.ToString("yyyyMMddHHmmss") + "' WHERE AUTO_ROW_ID = " + objInfoSet.RowID;
                                    cmdInsert.ExecuteNonQuery();
                                    dbConnection.Close();
                                }
                        }

                        TraceLog.sb.AppendLine(TraceLog.tempsb.ToString());
                        TraceLog.WriteFile(m_TracelogfilePath, TraceLog.sb.ToString(), m_Tracelogfilename);

                        }

                    }
                }

                objRdr.Close();
                CreateZipFinal(Constants.sFilePath, m_JobId);
                FileToDatabase(m_JobId + ".zip", Constants.sFilePath + m_JobId + ".zip");

                CleanFolder();

                Console.WriteLine("0 ^*^*^Folder/Zip Files Deleted From AutomailMergeFolder");
                TraceLog.sb.AppendLine(DateTime.Now + " " + "Folder/Zip Files Deleted From AutomailMergeFolder");
                TraceLog.WriteFile(m_TracelogfilePath, TraceLog.sb.ToString(), m_Tracelogfilename);

                Console.WriteLine("0 ^*^*^ fetching data From Database compeleted");
                TraceLog.sb.AppendLine(DateTime.Now + " " + "fetching data From Database compeleted");
                TraceLog.WriteFile(m_TracelogfilePath, TraceLog.sb.ToString(), m_Tracelogfilename);
            }
            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                //Log.Write(exc.Message);  
                ErrorLog.sbTempErrorLog.AppendLine(exc.Message);
                throw;
            }           
        }



        /// <summary>
        /// Load the Info Definition data
        /// </summary>
        /// <param name="ID">Input xml document with data</param>
        /// <param name="Defname">row id</param>
        /// <returns>xml document with data</returns>
        public void LoadInfoDef(int ID, string Defname)
        {
            int iPtr = 0;
            int iInfoNum = 0;
            int iFilterNum = -1;
            int iNumFilters = 0;
            int iTmplateNo = 0;
            int iDbType = 0;
            bool bEnd = false;
            ColLobSettings objLobSettings = null;
            string sFileName = "";
            string sKeyWord = "";
            string sValue = "";
            string sTmp = "";
            RMConfigurator objConfig = null;
            StreamReader objSRdr = null;
            XmlElement objTmplate = null;
            XmlElement objNewTmplate = null;
            XmlElement objMailTemp = null;
            XmlElement objAvlFltrs = null;
            Riskmaster.Settings.SysSettings objSys = null;

            try
            {
                Console.WriteLine("0 ^*^*^ fetching data From Definition File");
                TraceLog.sb.AppendLine(DateTime.Now + " " + "fetching data From Definition File");
                TraceLog.WriteFile(m_TracelogfilePath, TraceLog.sb.ToString(), m_Tracelogfilename);

                objInfoDef.NumFilters = 0;
                objSys = new Riskmaster.Settings.SysSettings(m_sConnectionString, m_iClientId);//rkaur27
                objLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);//rkaur27
                objSys = null;
                sFileName = RMConfigurator.BasePath + "\\appfiles\\MailMerge\\mailmergeinfo.def";

                objInfoDef.FilterDef = new FilterDefinition[182];

                if (File.Exists(sFileName))
                {
                    using (objSRdr = new StreamReader(sFileName))
                    {
                        string sLine = "";
                        while ((sLine = objSRdr.ReadLine()) != null)
                        {
                            sLine = sLine.Trim();
                            if (sLine != "")
                            {
                                if (sLine.Substring(0, 1) == "[")
                                {
                                    sKeyWord = sLine.Substring(1, sLine.Length - 2);
                                    if (sKeyWord.ToUpper() == "FILTER")
                                    {
                                        iFilterNum += 1;
                                        iNumFilters += 1;
                                        objInfoDef.NumFilters++;
                                    }
                                    else
                                    {
                                        if (bEnd)
                                            break;
                                        if (ID > 0)
                                        {
                                            iFilterNum = -1;
                                            objInfoDef.NumFilters = 0;
                                        }
                                        iNumFilters = -1;
                                        iInfoNum += 1;
                                        objInfoDef.Name = sKeyWord;
                                    }
                                }
                                else
                                {
                                    iPtr = sLine.IndexOf("=");
                                    if (sLine.Substring(0, 2) == "//" || iPtr == 0)
                                        iPtr = 1;
                                    sKeyWord = sLine.Substring(0, iPtr);
                                    sValue = sLine.Substring(iPtr + 1, sLine.Length - iPtr - 1).Trim();
                                    switch (sKeyWord.ToUpper())
                                    {
                                        //rupal:start,r8 auto diary setup enh
                                        case "DATABASE":
                                            objInfoDef.FilterDef[iFilterNum].Database = sValue;
                                            break;
                                        //rupal:end
                                        case "NAME":
                                            objInfoDef.FilterDef[iFilterNum].Name = sValue;
                                            break;
                                        case "ID":
                                            if (iNumFilters == -1)
                                            {
                                                if (ID == Conversion.ConvertStrToInteger(sValue))
                                                    bEnd = true;
                                                objInfoDef.ID = Conversion.ConvertStrToInteger(sValue);
                                            }
                                            else
                                            {
                                                objInfoDef.FilterDef[iFilterNum].ID = Conversion.ConvertStrToInteger(sValue);
                                            }
                                            break;
                                        case "ATTACH_TABLE":
                                            objInfoDef.AttachTable = sValue;
                                            break;
                                        case "PARENT_TABLE":
                                            objInfoDef.ParentTable = sValue;
                                            break;
                                        case "ATTACH_COLUMN":
                                            objInfoDef.AttachCol = sValue;
                                            break; 
                                        case "ATTACH_DESC":
                                            objInfoDef.AttachDesc = sValue;
                                            break;
                                        case "SQL":
                                            if (sValue.Substring(0, 1) == "[")
                                            {
                                                iPtr = sValue.IndexOf("]");
                                                sTmp = sValue.Substring(2, iPtr - 2);
                                                sValue = sValue.Substring(0, iPtr + 1);
                                                switch (sTmp.ToUpper())
                                                {
                                                    case "SQLSERVER":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
                                                        break;
                                                    case "SYBASE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
                                                        break;
                                                    case "INFORMIX":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
                                                        break;
                                                    case "ORACLE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
                                                        break;
                                                    case "DB2":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_DB2;
                                                        break;
                                                    default:
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
                                                        break;
                                                }
                                                if (iDbType == m_iDbType)
                                                {
                                                    objInfoDef.SQL = sValue;
                                                }
                                            }
                                            else
                                            {
                                                objInfoDef.SQL = sValue;
                                            }
                                            break;
                                        case "TYPE":
                                            switch (sValue.ToUpper())
                                            {
                                                case "CHECKBOX":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 1;
                                                    break;
                                                case "LIST":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 2;
                                                    break;
                                                case "TOPX":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 3;
                                                    break;
                                                case "COMBO":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 4;
                                                    break;
                                                case "DURATION":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 5;
                                                    break;
                                                case "NO_OPTION":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 6;
                                                    break;
                                                default:
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 0;
                                                    break;
                                            }
                                            break;
                                        case "MIN":
                                            // MIN and DEFAULT have the same logic
                                            switch (sValue)
                                            {
                                                case "CURRENT_YEAR":
                                                    objInfoDef.FilterDef[iFilterNum].FilterMin = System.DateTime.Now.Year;
                                                    break;
                                                default:
                                                    objInfoDef.FilterDef[iFilterNum].FilterMin = Conversion.ConvertStrToLong(sValue);
                                                    break;
                                            }
                                            break;
                                        case "MAX":
                                            objInfoDef.FilterDef[iFilterNum].FilterMax = Conversion.ConvertStrToLong(sValue);
                                            break;
                                        case "DEFAULT":
                                            objInfoDef.FilterDef[iFilterNum].DefValue = sValue;
                                            break;
                                        case "TABLE":
                                            objInfoDef.FilterDef[iFilterNum].table = sValue;
                                            break;
                                        case "SQLFILL":
                                            if (sValue.Substring(0, 1) == "[")
                                            {
                                                iPtr = sValue.IndexOf("]");
                                                sTmp = sValue.Substring(1, iPtr - 2);
                                                sValue = sValue.Substring(0, iPtr + 1);
                                                switch (sTmp.ToUpper())
                                                {
                                                    case "SQLSERVER":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
                                                        break;
                                                    case "SYBASE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
                                                        break;
                                                    case "INFORMIX":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
                                                        break;
                                                    case "ORACLE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
                                                        break;
                                                    case "DB2":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_DB2;
                                                        break;
                                                    default:
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
                                                        break;
                                                }
                                                if (iDbType == m_iDbType)
                                                {
                                                    objInfoDef.FilterDef[iFilterNum].SQLFill = sValue;
                                                }
                                            }
                                            else
                                            {
                                                objInfoDef.FilterDef[iFilterNum].SQLFill = sValue;
                                            }
                                            break;
                                        case "SQLFROM":
                                            if (sValue.Substring(0, 1) == "[")
                                            {
                                                iPtr = sValue.IndexOf("]");
                                                sTmp = sValue.Substring(1, iPtr - 2);
                                                sValue = sValue.Substring(0, iPtr + 1);
                                                switch (sTmp.ToUpper())
                                                {
                                                    case "SQLSERVER":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
                                                        break;
                                                    case "SYBASE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
                                                        break;
                                                    case "INFORMIX":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
                                                        break;
                                                    case "ORACLE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
                                                        break;
                                                    case "DB2":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_DB2;
                                                        break;
                                                    default:
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
                                                        break;
                                                }
                                                if (iDbType == m_iDbType)
                                                {
                                                    objInfoDef.FilterDef[iFilterNum].SQLFrom = sValue;
                                                }
                                            }
                                            else
                                            {
                                                objInfoDef.FilterDef[iFilterNum].SQLFrom = sValue;
                                            }
                                            break;
                                        case "SQLWHERE":
                                            if (sValue.Substring(0, 1) == "[")
                                            {
                                                iPtr = sValue.IndexOf("]", 1);
                                                sTmp = sValue.Substring(1, iPtr - 1);
                                                sValue = sValue.Substring(iPtr + 1);
                                                switch (sTmp.ToUpper())
                                                {
                                                    case "SQLSERVER":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
                                                        break;
                                                    case "SYBASE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
                                                        break;
                                                    case "INFORMIX":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
                                                        break;
                                                    case "ORACLE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
                                                        break;
                                                    case "DB2":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_DB2;
                                                        break;
                                                    default:
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
                                                        break;
                                                }
                                                if (iDbType == m_iDbType)
                                                {
                                                    objInfoDef.FilterDef[iFilterNum].SQLWhere = sValue;
                                                }
                                            }
                                            else
                                            {
                                                objInfoDef.FilterDef[iFilterNum].SQLWhere = sValue;
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    Console.WriteLine("0 ^*^*^ fetching data From Definition File compeleted succesfully");
                    TraceLog.sb.AppendLine(DateTime.Now + " " + "fetching data From Definition File compeleted succesfully");
                    TraceLog.WriteFile(m_TracelogfilePath, TraceLog.sb.ToString(), m_Tracelogfilename);
                }

                else
                {
                    Console.WriteLine("0 ^*^*^ Definition File Not Found");
                    TraceLog.sb.AppendLine(DateTime.Now + " " + "Definition File Not Found");
                    TraceLog.WriteFile(m_TracelogfilePath, TraceLog.sb.ToString(), m_Tracelogfilename);
                }

               
            }

            catch (Exception p_objEx)
            {
                ErrorLog.sbTempErrorLog.AppendLine(p_objEx.Message);
                throw;
                //throw new RMAppException(Globalization.GetString("DataFilter.LoadInfoDef.Err"), p_objEx);
            }
            finally
            {
                if (objSRdr != null)
                    objSRdr.Dispose();
                objSys = null;
            }
        }

        public void InfoRun(Int64 iSinceLast,string templateTracelogfilePath, string templateTracelogfilename)
        {
            Application wordApp = new Application();
            Document doc = null;
            object missingValue = Type.Missing;
            try
            {
                Console.WriteLine("0 ^*^*^ Running InfoRun Procedure for " + objInfoSet.Name + " AutoMailMerge SetUp" );
                TraceLog.tempsb.AppendLine(DateTime.Now + " " + "Running InfoRun Procedure for " + objInfoSet.Name + " AutoMailMerge SetUp");
                TraceLog.WriteFile(templateTracelogfilePath, TraceLog.tempsb.ToString(), templateTracelogfilename);

                string sTmp = "";
                string sSQL = string.Empty;
                string sSQLInsert = string.Empty;
                string sSQLValue = string.Empty;
                string sSQLSelect = string.Empty;
                string sSQLFrom = string.Empty;
                string sSQLWhere = string.Empty;
                string sSQLGroup = string.Empty;
                string sSQLOrder = string.Empty;
                string sDummy = string.Empty;
                string sKey = string.Empty;
                string sFinalstring = string.Empty;
                string sTempName = string.Empty;
                string sSelectList = string.Empty;
                string[] sSplitString = null;
                List<string> tmpSQL = new List<string>();

                int iStrt;
                int iPtr;
                int iPtr2;
                int fidx;
                int iIndex;

                if (!string.IsNullOrEmpty(objInfoDef.tmpSQL))
                {
                    sTmp = objInfoDef.tmpSQL;
                    do
                    {
                        iPtr = sTmp.IndexOf("|", 0);
                        if (iPtr == -1)
                        {
                            iPtr = sTmp.Length + 1;
                        }

                        tmpSQL.Add(sTmp.Substring(0, iPtr - 1));
                        sTmp = sTmp.Substring(iPtr + 1);
                    }
                    while (sTmp == "");
                }


                sSQL = objInfoDef.SQL;
                iStrt = 0;
                iPtr = sSQL.IndexOf(" FROM ", 0);
                sSQLSelect = (sSQL.Substring(iStrt, iPtr)).Trim();

                iStrt = iPtr + 1;
                iPtr = sSQL.IndexOf(" WHERE ", iStrt);
                if (iPtr == -1)
                {
                    iPtr = sSQL.IndexOf(" GROUP ", iStrt);
                }
                if (iPtr == -1)
                {
                    iPtr = sSQL.Length;
                }
                sSQLFrom = (sSQL.Substring(iStrt, iPtr - iStrt)).Trim();
                if (sSQL.IndexOf(" WHERE ", 0) != -1)
                {
                    iStrt = iPtr + 1;
                    iPtr = sSQL.IndexOf(" GROUP ", iStrt);
                    if (iPtr == -1)
                    {
                        iPtr = sSQL.IndexOf(" ORDER BY ", iStrt);
                    }
                    if (iPtr == -1)
                    {
                        iPtr = sSQL.Length;
                    }
                    sSQLWhere = (sSQL.Substring(iStrt, iPtr - iStrt)).Trim();
                }
                else
                {
                    sSQLWhere = "";
                }

                if (sSQL.IndexOf(" GROUP ", 0) != -1)
                {
                    iStrt = iPtr + 1;
                    iPtr = sSQL.IndexOf(" ORDER BY ", iStrt);
                    if (iPtr == -1)
                    {
                        iPtr = sSQL.Length;
                    }
                    sSQLGroup = (sSQL.Substring(iStrt, iPtr - iStrt)).Trim();
                }

                if (sSQL.IndexOf(" ORDER BY ", 0) != -1)
                {
                    sSQLOrder = sSQL.Substring(sSQL.IndexOf(" ORDER BY ") + 1).Trim();
                }
                else
                {
                    sSQLOrder = "";
                }

                for (int j = 0; j <= objInfoSet.NumFilters - 1; j++)
                {
                    fidx = GetFilterIndex(objInfoSet.FilterSet[j].Number);
                    switch (objInfoDef.FilterDef[fidx].FilterType)
                    {
                        case 1:
                            if (objInfoSet.FilterSet[j].Data != "")
                            {
                                if ((objInfoDef.FilterDef[fidx].SQLFrom).Trim() != "")
                                {
                                    sDummy = sGetTablesNotInSQLFrom(sSQLFrom, (objInfoDef.FilterDef[fidx].SQLFrom).Trim());
                                    if (sDummy != "")
                                    {
                                        sSQLFrom = sSQLFrom + "," + sDummy;
                                    }
                                }

                                sTmp = objInfoDef.FilterDef[fidx].SQLWhere;
                                iPtr = sTmp.IndexOf("[", 0);
                                if (iPtr != -1)
                                {
                                    sKey = sTmp.Substring(iPtr, sTmp.IndexOf("]", iPtr) + 1 - iPtr).ToUpper();
                                    switch (sKey.ToUpper())
                                    {
                                        case "[OPTION]":
                                            if (objInfoSet.FilterSet[j].Data == "" || objInfoSet.FilterSet[j].Data == null)
                                            {
                                                sTmp = sTmp.Substring(RInStr(iPtr, sTmp, "{") - 1) + sTmp.Substring((sTmp.IndexOf("}", iPtr) + 1));
                                            }
                                            else
                                            {
                                                sTmp = sTmp.Substring(0, iPtr) + objInfoSet.FilterSet[j].Data + sTmp.Substring(iPtr + 8);
                                            }
                                            break;
                                    }
                                }

                                if (sTmp.Trim() != "")
                                {
                                    if (sSQLWhere == "")
                                    {
                                        sSQLWhere = "WHERE ";
                                    }

                                    else
                                    {
                                        sSQLWhere = sSQLWhere + " AND ";
                                    }

                                    sSQLWhere = sSQLWhere + sTmp;
                                }
                            }
                            break;

                        case 2:
                        case 4:
                            if (!string.IsNullOrEmpty((objInfoDef.FilterDef[fidx].SQLFrom)))
                            //if ((objInfoDef.FilterDef[fidx].SQLFrom).Trim() != "" || (objInfoDef.FilterDef[fidx].SQLFrom).Trim() != null)
                            {
                                sDummy = sGetTablesNotInSQLFrom(sSQLFrom, (objInfoDef.FilterDef[fidx].SQLFrom).Trim());
                                if (sDummy != "")
                                {
                                    sSQLFrom = sSQLFrom + "," + sDummy;
                                }
                            }

                            sTmp = objInfoDef.FilterDef[fidx].SQLWhere;
                            sFinalstring = "";
                            iPtr = sTmp.IndexOf("[", 0);
                            if (iPtr != -1)
                            {
                                sKey = sTmp.Substring(iPtr, sTmp.IndexOf("]", iPtr) + 1 - iPtr).ToUpper();
                                switch (sKey.ToUpper())
                                {
                                    case "[OPTION]":
                                        if (objInfoSet.FilterSet[j].Data == "" || objInfoSet.FilterSet[j].Data == null || objInfoSet.FilterSet[j].Data == ",")
                                        {
                                            sTmp = sTmp.Substring(RInStr(iPtr, sTmp, "{") - 1) + sTmp.Substring((sTmp.IndexOf("}", iPtr) + 1));
                                        }
                                        else
                                        {
                                            string sOptionString = string.Empty;
                                            //string sString = objInfoSet.FilterSet[j].Data.ToString();

                                            sOptionString = objInfoSet.FilterSet[j].Data.ToString().Substring(1, objInfoSet.FilterSet[j].Data.ToString().Length - 2);
                                            //sOptionString = sOptionString.Substring(1, sOptionString.Length - 2);
                                            bool bIsAlphaNumeric;
                                            bIsAlphaNumeric = checkAlpha(sOptionString);
                                            if (bIsAlphaNumeric)
                                            {
                                                sSplitString = sOptionString.Split(',');
                                                for (iIndex = 0; iIndex < sSplitString.Count(); iIndex++)
                                                {
                                                    if (sFinalstring != "")
                                                    {
                                                        sFinalstring = sFinalstring + ",'" + sSplitString[iIndex] + "'";
                                                    }
                                                    else
                                                    {
                                                        sFinalstring = "'" + sSplitString[iIndex] + "'";
                                                    }
                                                }

                                                if (sFinalstring == "")
                                                {
                                                    sFinalstring = sOptionString;
                                                }

                                                sTmp = sTmp.Substring(0, iPtr) + sFinalstring + sTmp.Substring(iPtr + 8);
                                            }

                                            else
                                            {

                                                sTmp = sTmp.Substring(0, iPtr) + objInfoSet.FilterSet[j].Data.ToString().Substring(1, objInfoSet.FilterSet[j].Data.ToString().Length - 2) + sTmp.Substring(iPtr + 8);
                                            }
                                        }
                                        break;
                                }
                            }

                            if (sTmp.Trim() != "")
                            {
                                if (sSQLWhere == "")
                                {
                                    sSQLWhere = "WHERE ";
                                }
                                else
                                {
                                    sSQLWhere = sSQLWhere + " AND ";
                                }

                                sSQLWhere = sSQLWhere + sTmp;
                            }
                            break;

                        case 3:
                        case 5:
                            if (!string.IsNullOrEmpty((objInfoDef.FilterDef[fidx].SQLFrom)))
                            //if (!string.IsNullOrEmpty(objInfoDef.FilterDef[fidx].SQLFrom.Trim()))
                            {
                                sDummy = sGetTablesNotInSQLFrom(sSQLFrom, (objInfoDef.FilterDef[fidx].SQLFrom).Trim());
                                if (sDummy != "")
                                {
                                    sSQLFrom = sSQLFrom + "," + sDummy;
                                }
                            }


                            sTmp = objInfoDef.FilterDef[fidx].SQLWhere;
                            do
                            {
                                iPtr = sTmp.IndexOf("[", 0);
                                if (iPtr != -1)
                                {
                                    sKey = sTmp.Substring(iPtr, sTmp.IndexOf("]", iPtr) + 1 - iPtr).ToUpper();
                                    switch (sKey.ToUpper())
                                    {
                                        case "[OPTION]":
                                            if (objInfoSet.FilterSet[j].Data == "" || objInfoSet.FilterSet[j].Data == null)
                                            {
                                                sTmp = sTmp.Substring(RInStr(iPtr, sTmp, "{") - 1) + sTmp.Substring((sTmp.IndexOf("}", iPtr) + 1));
                                            }
                                            else
                                            {
                                                object Data = objInfoSet.FilterSet[j].Data;
                                                if (objInfoDef.FilterDef[fidx].FilterType == 5 && iSinceLast != 0)
                                                {
                                                    if (sTmp.IndexOf("<=", 0) != -1)
                                                    {
                                                        iPtr2 = RInStr(iPtr, sTmp, "<=");
                                                    }

                                                    else
                                                    {
                                                        iPtr2 = RInStr(iPtr, sTmp, ">=");
                                                    }

                                                    if (iPtr2 > 0)
                                                    {
                                                        iPtr2 = 0;
                                                    }
                                                    else
                                                    {
                                                        iPtr2 = RInStr(iPtr, sTmp, "=");
                                                    }

                                                    if (iPtr2 != 0)
                                                    {

                                                        sTmp = sTmp.Substring(iPtr2 - 1) + "BETWEEN " + objInfoSet.FilterSet[j].Data + " AND " + (Convert.ToInt32(Data) + iSinceLast) + sTmp.Substring(iPtr + 8);
                                                    }
                                                    else
                                                    {
                                                        sTmp = sTmp.Substring(0, iPtr - 1) + Data.ToString() + sTmp.Substring(iPtr + 8);
                                                    }
                                                }

                                                else
                                                {
                                                    sTmp = sTmp.Substring(0, iPtr - 1) + Data.ToString() + sTmp.Substring(iPtr + 8);
                                                }
                                            }

                                            break;

                                        case "[TODAY]":
                                            //sTmp = sTmp.Substring(0, iPtr - 1) + "'" + Conversion.get + "'" + sTmp.Substring(iPtr + 7);
                                            sTmp = sTmp.Substring(0, iPtr) + "'" + DateTime.Today.ToString("MM/dd/yyyy") + "'" + sTmp.Substring(iPtr + 7);
                                            break;

                                        case "[DTG_TODAY]":
                                            sTmp = sTmp.Substring(0, iPtr) + "'" + Conversion.ToDbDate(DateTime.Now) + "'" + sTmp.Substring(iPtr + 11);
                                            break;

                                    }
                                }
                            }
                            while (iPtr != -1);


                            if (sTmp.Trim() != "")
                            {
                                if (sSQLWhere == "")
                                {
                                    sSQLWhere = "WHERE ";
                                }
                                else
                                {
                                    sSQLWhere = sSQLWhere + " AND ";
                                }

                                sSQLWhere = sSQLWhere + sTmp;
                            }

                            break;

                    }
                }

                iPtr = sSQLSelect.IndexOf("#TMP_", 0);
                if (iPtr != -1)
                {
                    sTempName = sSQLSelect.Substring(iPtr, sSQLSelect.IndexOf(".", iPtr) - iPtr);
                }
                else
                {
                    sTempName = "";
                }

                if (!string.IsNullOrEmpty(objInfoDef.tmpSQL) && !string.IsNullOrEmpty(sSelectList) && !string.IsNullOrEmpty(sTempName))
                {
                    if (objInfoDef.tmpSQL.IndexOf("[SELECTLIST]", 0) != -1 && sSelectList != "" && sTempName != "")
                    {
                        sDummy = sGetTablesNotInSQLFrom(sSQLFrom, sTempName);
                        if (sDummy != "")
                        {
                            if (sSQLFrom == "")
                            {
                                sSQLFrom = "FROM ";
                            }
                            else
                            {
                                sSQLFrom = sSQLFrom + ",";
                            }
                            sSQLFrom = sSQLFrom + sDummy;
                        }

                        sTmp = sSelectList;
                        do
                        {
                            iPtr = sSelectList.IndexOf(",", 0);
                            if (iPtr == -1)
                            {
                                iPtr = sTmp.Length + 1;
                            }
                            sKey = sTmp.Substring(0, iPtr - 1);

                            if (sSQLWhere.IndexOf(sTempName + sKey.Substring(sKey.IndexOf(".", 0)) + "=" + sKey, 0) == -1)
                            {
                                if (sSQLWhere == "")
                                {
                                    sSQLWhere = "WHERE";
                                }
                                else
                                {
                                    sSQLWhere = sSQLWhere + " AND ";
                                }

                                sSQLWhere = sSQLWhere + sTempName + sKey.Substring(sKey.IndexOf(".", 0)) + " = " + sKey;
                            }

                            sTmp = sTmp.Substring(iPtr + 1);
                        }
                        while (sTmp == "");
                    }
                }

                iStrt = 0;
                do
                {
                    iPtr = sSQLSelect.IndexOf("#TMP_", iStrt);
                    if (iPtr == -1)
                    {
                        break;
                    }
                    else
                    {
                        iPtr2 = sSQLSelect.IndexOf(",", iPtr) - iPtr;
                        if (sSQLGroup == "")
                        {
                            sSQLGroup = "GROUP BY ";
                        }

                        else
                        {
                            sSQLGroup = sSQLGroup + ",";
                        }

                        sSQLGroup = sSQLGroup + sTmp;
                    }

                    iStrt = iPtr + 1;
                }
                while (iStrt > 0);


                if (!string.IsNullOrEmpty(objInfoSet.LastRun))
                {
                    sSQLWhere = sSQLWhere + " " + "AND" + " " + "(CLAIM.DTTM_RCD_ADDED >" + " " + "'" + objInfoSet.LastRun + "'" + " " + "OR CLAIM.DTTM_RCD_LAST_UPD >" + " " + "'" + objInfoSet.LastRun + "')";
                }
				
				// akaushik5 Added for RMA-5982 Starts 
                if (!string.IsNullOrEmpty(objInfoSet.ProcessDate))
                {
                    sSQLWhere += string.Format(" AND (CLAIM.DTTM_RCD_ADDED >= '{0}' OR CLAIM.DTTM_RCD_LAST_UPD >= '{0}')", objInfoSet.ProcessDate);
                }
				// akaushik5 Added for RMA-5982 Ends

                DbReader objReader = null;
                DbReader objmergeReader = null;
                string sMergeSQL = "";
                string SligitationType = "";
                string Litigationtype = string.Empty;
                string LitigationStatus = string.Empty;
                string stablename = string.Empty;
                string sColumnname = string.Empty;
                string stablename1 = string.Empty;
                string sColumnname1 = string.Empty;


                if (objInfoSet.Index == 1 && objInfoSet.TempNumFilters > 0)
                {
                    for (int i = 0; i < objInfoSet.TempNumFilters; i++)
                    {
                        int Number = objInfoSet.TemplateFilterSet[i].Number;
                        string TempFilterData = string.Empty;
                        TempFilterData = Conversion.ConvertObjToStr(objInfoSet.TemplateFilterSet[i].Data);

                        if (Number == 1)
                        {
                            stablename = "CLAIM_X_LITIGATION";
                            sColumnname = "LIT_TYPE_CODE";
                            if (sSQL.Contains("CLAIM_X_LITIGATION"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 2)
                        {
                            stablename = "CLAIM_X_LITIGATION";
                            sColumnname = "LIT_STATUS_CODE";
                            if (sSQL.Contains("CLAIM_X_LITIGATION"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }

                            }
                        }

                        if (Number == 3)
                        {
                            stablename = "CLAIM_X_SUBRO";
                            sColumnname = "SUB_TYPE_CODE";
                            if (sSQL.Contains("CLAIM_X_SUBRO"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 4)
                        {
                            stablename = "CLAIM_X_SUBRO";
                            sColumnname = "SUB_STATUS_CODE";
                            if (sSQL.Contains("CLAIM_X_SUBRO"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }

                        }

                        if (Number == 5)
                        {
                            stablename = "CLAIM_X_SUBRO";
                            sColumnname = "SUB_STATUS_DESC_CODE";
                            if (sSQL.Contains("CLAIM_X_SUBRO"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 6)
                        {
                            stablename = "CLAIM_X_LIABILITYLOSS";
                            sColumnname = "LIABILITY_TYPE_CODE";
                            if (sSQL.Contains("CLAIM_X_LIABILITYLOSS"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 7)
                        {
                            stablename = "CASE_MANAGEMENT";
                            sColumnname = "CASEMGT_ROW_ID";
                            stablename1 = "CM_X_TREATMENT_PLN";
                            sColumnname1 = "PLAN_STATUS_CODE";
                            if (sSQL.Contains("CASE_MANAGEMENT") && sSQL.Contains("CM_X_TREATMENT_PLN"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname1 + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }

                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename + " " + "," + " " + stablename1;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname1 + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + stablename + "." + sColumnname + "=" + stablename1 + "." + sColumnname;
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }

                            }
                        }

                        if (Number == 8)
                        {
                            stablename = "CLAIM_X_PROPERTYLOSS";
                            sColumnname = "PROPERTYTYPE";
                            if (sSQL.Contains("CLAIM_X_PROPERTYLOSS"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 9)
                        {
                            stablename = "CLAIMANT";
                            sColumnname = "CLAIMANT_TYPE_CODE";
                            if (sSQL.Contains("CLAIMANT"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 10)
                        {
                            stablename = "DEFENDANT";
                            sColumnname = "DFNDNT_TYPE_CODE";
                            if (sSQL.Contains("DEFENDANT"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }


                    }

                    sSQL = sSQL.Replace("{", "");
                    sSQL = sSQL.Replace("}", "");
                }


                if (objInfoSet.Index == 2 & objInfoSet.TempNumFilters > 0)
                {
                    for (int i = 0; i < objInfoSet.TempNumFilters; i++)
                    {
                        int Number = objInfoSet.TemplateFilterSet[i].Number;
                        string TempFilterData = string.Empty;
                        TempFilterData = Conversion.ConvertObjToStr(objInfoSet.TemplateFilterSet[i].Data);

                        if (Number == 1)
                        {
                            stablename = "CLAIM_X_LITIGATION";
                            sColumnname = "LIT_TYPE_CODE";
                            if (sSQL.Contains("CLAIM_X_LITIGATION"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;

                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIMANT.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;

                                }

                            }
                        }

                        if (Number == 2)
                        {
                            stablename = "CLAIM_X_LITIGATION";
                            sColumnname = "LIT_STATUS_CODE";
                            if (sSQL.Contains("CLAIM_X_LITIGATION"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIMANT.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }

                        }

                        if (Number == 3)
                        {
                            stablename = "CLAIM_X_SUBRO";
                            sColumnname = "SUB_TYPE_CODE";
                            if (sSQL.Contains("CLAIM_X_SUBRO"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIMANT.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }

                        }

                        if (Number == 4)
                        {
                            stablename = "CLAIM_X_SUBRO";
                            sColumnname = "SUB_STATUS_CODE";
                            if (sSQL.Contains("CLAIM_X_SUBRO"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIMANT.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 5)
                        {
                            stablename = "CLAIM_X_SUBRO";
                            sColumnname = "SUB_STATUS_DESC_CODE";
                            if (sSQL.Contains("CLAIM_X_SUBRO"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIMANT.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 6)
                        {
                            stablename = "CLAIM_X_LIABILITYLOSS";
                            sColumnname = "LIABILITY_TYPE_CODE";
                            if (sSQL.Contains("CLAIM_X_LIABILITYLOSS"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIMANT.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 7)
                        {
                            stablename = "CASE_MANAGEMENT";
                            sColumnname = "CASEMGT_ROW_ID";
                            stablename1 = "CM_X_TREATMENT_PLN";
                            sColumnname1 = "PLAN_STATUS_CODE";
                            if (sSQL.Contains("CASE_MANAGEMENT") && sSQL.Contains("CM_X_TREATMENT_PLN"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname1 + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename + " " + "," + " " + stablename1;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname1 + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIMANT.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + stablename + "." + sColumnname + "=" + stablename1 + "." + sColumnname;
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }

                            }
                        }

                        if (Number == 8)
                        {
                            stablename = "CLAIM_X_PROPERTYLOSS";
                            sColumnname = "PROPERTYTYPE";
                            if (sSQL.Contains("CLAIM_X_PROPERTYLOSS"))
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(TempFilterData))
                                {
                                    TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                    sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                    sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIMANT.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                                }
                            }
                        }

                        if (Number == 9)
                        {
                            stablename = "CLAIMANT";
                            sColumnname = "CLAIMANT_TYPE_CODE";
                            if (!string.IsNullOrEmpty(TempFilterData))
                            {
                                TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                sSQLFrom = sSQLFrom;
                                sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")";
                                sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                            }
                        }

                        if (Number == 10)
                        {
                            stablename = "DEFENDANT";
                            sColumnname = "DFNDNT_TYPE_CODE";
                            if (!string.IsNullOrEmpty(TempFilterData))
                            {
                                TempFilterData = TempFilterData.Substring(1, TempFilterData.Length - 2);
                                sSQLFrom = sSQLFrom + " " + "," + " " + stablename;
                                sSQLWhere = sSQLWhere + " " + "AND" + " " + sColumnname + " " + "IN" + " " + "(" + TempFilterData + ")" + " " + "AND" + " " + "CLAIMANT.CLAIM_ID" + "=" + stablename + ".CLAIM_ID" + " " + "AND" + " " + "CLAIM.CLAIM_ID" + "=" + stablename + ".CLAIM_ID";
                                sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                            }
                        }


                    }

                    sSQL = sSQL.Replace("{", "");
                    sSQL = sSQL.Replace("}", "");
                }

                if (objInfoSet.TempNumFilters == 0)
                {
                    sSQL = sSQLSelect + " " + sSQLFrom + " " + sSQLWhere + " " + sSQLGroup + " " + sSQLOrder;
                    sSQL = sSQL.Replace("{", "");
                    sSQL = sSQL.Replace("}", "");
                }

                DbReader objFilterRead = null;
                MergeManager objMergeManager = null;
                string ClaimID = string.Empty;
                objFilterRead = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                Claim objClaim = null;
                Event objEvent = null;
                PersonInvolved objPersonInvolved = null;
                IDictionaryEnumerator objDictionaryEnumerator = null;
                TemplateField objField = null;
                bool blnSuccess = false;



                int counter = 0;

                if (objFilterRead != null)
                {
                    while (objFilterRead.Read())
                    {
                        ClaimID = Conversion.ConvertObjToStr(objFilterRead.GetInt("CLAIM_ID"));
                        //ClaimID = Conversion.ConvertObjToStr(objFilterRead.GetInt32("CLAIM_ID"));

                        m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);//rkaur27

                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);

                        //objClaim.MoveTo(Convert.ToInt16(ClaimID));
                        objClaim.MoveTo(Riskmaster.Common.Conversion.CastToType<int>(ClaimID, out blnSuccess));

                        objMergeManager = new MergeManager(OuserLoginvalue, Riskmaster.Security.SecurityDatabase.Dsn, OuserLoginvalue.UserId.ToString(), m_iClientId);//rkaur27

                        objMergeManager.DSN = m_sConnectionString;
                        objMergeManager.UID = OuserLoginvalue.UserId.ToString();

                        if (m_sDocPath != "")
                        {
                            objMergeManager.TemplatePath = m_sDocPath;
                        }

                        objMergeManager.DocStorageDSN = m_sDocPath;
                        objMergeManager.DocPathType = m_iDocPathType;
                        objMergeManager.MoveTo(Conversion.ConvertStrToLong(objInfoSet.TemplateID.ToString()));
                        objMergeManager.CurMerge.RecordId = Conversion.ConvertStrToLong(ClaimID);

                        objDictionaryEnumerator = objMergeManager.CurMerge.Template.Fields.GetEnumerator();

                        if (!string.IsNullOrEmpty(objInfoSet.DefendantValue))
                        {
                            objMergeManager.CurMerge.sDefendantType = objInfoSet.DefendantValue.Substring(0, objInfoSet.DefendantValue.Length - 1);
                        }

                        if (!string.IsNullOrEmpty(objInfoSet.ClaimantValue))
                        {
                            objMergeManager.CurMerge.sClaimantType = objInfoSet.ClaimantValue.Substring(0, objInfoSet.ClaimantValue.Length - 1);
                        }

                        if (!string.IsNullOrEmpty(objInfoSet.ExpertWitnessValue))
                        {
                            objMergeManager.CurMerge.sExpertType = objInfoSet.ExpertWitnessValue.Substring(0, objInfoSet.ExpertWitnessValue.Length - 1);
                        }

                        if (!string.IsNullOrEmpty(objInfoSet.PersonInvolvedValue))
                        {
                            objMergeManager.CurMerge.sPersonInvolvedType = objInfoSet.PersonInvolvedValue.Substring(0, objInfoSet.PersonInvolvedValue.Length - 1);
                        }

                        if (!string.IsNullOrEmpty(objInfoSet.AdjusterValue))
                        {
                            objMergeManager.CurMerge.sCurrentAdjuster = objInfoSet.AdjusterValue;
                        }

                        if (!string.IsNullOrEmpty(objInfoSet.CasemanagerValue))
                        {
                            objMergeManager.CurMerge.sPrimaryCasemanager = objInfoSet.CasemanagerValue;
                        }


                        objMergeManager.CurMerge.FetchData();

                        string sRowid = string.Empty;
                        int icount = objMergeManager.CurMerge.GetRecordCount();
                        if (icount > 0)
                        {
                            for (int i = 1; i <= icount; i++)
                            {
                                string sRow = i.ToString();
                                sRowid = sRowid + "," + sRow;
                            }
                        }
                        //AA 19423
                        if(!String.IsNullOrEmpty(sRowid))
                        sRowid = sRowid.Substring(1, sRowid.Length - 1);

                        XmlDocument p_objXmlOut = new XmlDocument();
                        XmlNode objNode = null;

                        objNode = p_objXmlOut.CreateElement("Template");
                        p_objXmlOut.AppendChild(objNode);
                        XmlElement e = p_objXmlOut.CreateElement("File");
                        e.SetAttribute("Filename", Generic.GetServerUniqueFileName("mmt", "doc", m_iClientId));//rkaur27
                        e.InnerText = objMergeManager.CurMerge.Template.FormFileContent;
                        objNode.AppendChild(e);

                        //get the datasource if user is doing a merge (PreFab means user is creating a merge template)
                        //when user is creating template the merge fields are passed by wizard in a hidden field
                        if (!objMergeManager.CurMerge.PreFab)
                        {
                            e = p_objXmlOut.CreateElement("DataSource");
                            e.InnerText = objMergeManager.CurMerge.GetMergeDataSource(sRowid);
                            objNode.AppendChild(e);
                        }

                        //get the datasource if user is doing a merge (PreFab means user is creating a merge template)
                        //when user is creating template the merge fields are passed by wizard in a hidden field
                        if (!objMergeManager.CurMerge.PreFab)
                        {
                            e = p_objXmlOut.CreateElement("HeaderContent");
                            e.InnerText = objMergeManager.CurMerge.Template.HeaderFileContent;
                            objNode.AppendChild(e);
                        }

                        if (!objMergeManager.CurMerge.PreFab)
                        {
                            string[] arrRowid = (sRowid.ToString()).Split(',');
                            int iNumRecords = arrRowid.Length;
                            e = p_objXmlOut.CreateElement("NumRecords");
                            e.InnerText = iNumRecords.ToString();
                            objNode.AppendChild(e);
                        }

                        string sTempFileName = "";
                        string sRTFContent = "";
                        string sHeaderContent = "";
                        string sDataSource = "";
                        string strform = "";
                        string sheaderFileName = "";
                        string sDataSourceFileName = "";
                        string sDataFileName = "";

                        string sSecFile = "";

                        string SNumRecords = "";
                        string strBasepath = string.Empty;
                        string strZipfilename = string.Empty;

                        sTempFileName = p_objXmlOut.SelectSingleNode("//File").Attributes["Filename"].Value;
                        sRTFContent = p_objXmlOut.SelectSingleNode("//File").InnerText;
                        sHeaderContent = p_objXmlOut.SelectSingleNode("//HeaderContent").InnerText;
                        sDataSource = p_objXmlOut.SelectSingleNode("//DataSource").InnerText;
                        SNumRecords = p_objXmlOut.SelectSingleNode("//NumRecords").InnerText;

                        strZipfilename = m_JobId + "_" + objInfoSet.RowID;

                        strBasepath = Constants.sFilePath + m_JobId + "_" + objInfoSet.RowID + "\\";

                        //strBasepath = RMConfigurator.BasePath  + "\\temp\\AutoMailMerge\\" + m_JobId + "_" + objInfoSet.RowID + "\\";

                        strform = strBasepath + sTempFileName;
                        this.WriteFile(strform, sRTFContent);
                        sheaderFileName = strform.Substring(0, strform.Length - 4) + ".hdr";
                        this.WriteFile(sheaderFileName, sHeaderContent);
                        sDataFileName = string.Concat(new object[] { strBasepath, sTempFileName.Replace(".doc", ""), ".doc" });
                        sDataSourceFileName = string.Concat(new object[] { strBasepath, "DataSource", sTempFileName });

                        sSecFile = string.Concat(new object[] { strBasepath, sTempFileName.Replace(".doc", ""), "Sec.doc" });

                        this.WriteFile(sDataSourceFileName, sDataSource);
                        this.WordMerge(strform, sheaderFileName, sDataSourceFileName, sDataFileName, sSecFile, SNumRecords);

                        Console.WriteLine("0 ^*^*^ Document has been successsfully generated for Claim Number: " + objClaim.ClaimNumber);
                        TraceLog.tempsb.AppendLine(DateTime.Now + " " + "Document has been successsfully generated for Claim Number : " + objClaim.ClaimNumber);
                        TraceLog.WriteFile(templateTracelogfilePath, TraceLog.tempsb.ToString(), templateTracelogfilename);

                        if (File.Exists(sheaderFileName))
                        {
                            File.Delete(sheaderFileName);
                        }
                        if (File.Exists(sDataSourceFileName))
                        {
                            File.Delete(sDataSourceFileName);
                        }

                        MemoryStream objMemory = null;
                        string sCatid = "";
                        string sSubject = "";
                        string sName = "";
                        string sNotes = "";
                        string sKeywords = string.Empty;
                        string sClass = "";
                        string sCategory = "";
                        string sType = "";
                        string sRecordId = "";
                        string sFileName = "";
                        string sFormName = "";
                        int iPsid = 0;
                        string sTemplateId = string.Empty;
                        string sTable = string.Empty;
                        string sSendEmail = string.Empty;
                        string sMailRecipient = string.Empty;
                        string sRetMes = string.Empty;
                        DbReader objRdr = null;
                        sFileName = strform;
                        FileStream fs = new FileStream(sFileName, FileMode.Open, FileAccess.Read);
                        byte[] fileData = new byte[fs.Length];
                        fs.Read(fileData, 0, Convert.ToInt32(fs.Length));
                        fs.Close();
                        objMemory = new MemoryStream(fileData);

                        sCatid = objInfoSet.Index.ToString();
                        sRecordId = ClaimID;

                        string sSQLParentID = string.Empty;
                        string sFunctionName = string.Empty;

                        switch (objClaim.LineOfBusCode)
                        {
                            case 241:
                                sFunctionName = "General Claims";
                                break;
                            case 242:
                                sFunctionName = "Vehicle Accident";
                                break;
                            case 243:
                                sFunctionName = "Workers'' Compensation Claims";
                                break;
                            case 844:
                                sFunctionName = "Non-Occupational Claims";
                                break;
                            case 845:
                                sFunctionName = "Property Claims";
                                break;
                        }

                      

                        string sSecurityConnection = SecurityDatabase.Dsn;

                        if (!string.IsNullOrEmpty(sFunctionName))
                        {
                            sSQLParentID = "SELECT FUNC_ID from FUNCTION_LIST where FUNCTION_NAME = '" + sFunctionName + "'";
                            using (objRdr = DbFactory.GetDbReader(sSecurityConnection, sSQLParentID))
                            {
                                if (objRdr.Read())
                                {
                                    //iPsid = objRdr.GetInt16("FUNC_ID");
                                    iPsid = objRdr.GetInt("FUNC_ID");
                                }
                            }

                        objRdr.Close();
                        }

                        sTemplateId = objInfoSet.TemplateID.ToString();
                        sTable = "claim";

                        objMergeManager.AttachDocument(sFileName, objMemory, sCatid, sSubject, sName, sNotes, sKeywords, sClass, sCategory, sType, sRecordId, iPsid, sFormName);

                        Console.WriteLine("0 ^*^*^ Document has been successsfully attached for Claim Number : " + objClaim.ClaimNumber);
                        TraceLog.tempsb.AppendLine(DateTime.Now + " " + "Document has been successsfully attached for Claim Number : " + objClaim.ClaimNumber);
                        TraceLog.WriteFile(templateTracelogfilePath, TraceLog.tempsb.ToString(), templateTracelogfilename);

                        if (File.Exists(sSecFile))
                        {
                            File.Delete(sSecFile);
                        }

                        //changed by swati for AIC Gap 8 MITS # 36930 11/14/2014
                        //using (objRdr = DbFactory.GetDbReader(m_sConnectionString, "SELECT * from MERGE_FORM where FORM_ID =" + objInfoSet.TemplateID.ToString()))
                        //{
                        //    if (objRdr.Read())
                        //    {
                        //        sSendEmail = objRdr.GetInt16("SEND_MAIL_FLAG").ToString();
                        //        sMailRecipient = objRdr.GetInt32("DESIGNATED_RECIPIENT").ToString();
                        //    }
                        //}
                        using (objRdr = DbFactory.GetDbReader(m_sConnectionString, "SELECT SEND_MAIL_FLAG, FR.DESIGNATED_RECIPIENT_ID FROM MERGE_FORM MF, FORM_RECIPIENT FR WHERE MF.FORM_ID = FR.FORM_ID AND FR.FORM_ID = " + objInfoSet.TemplateID.ToString()))
                        {
                            while (objRdr.Read())
                            {
                                sSendEmail = objRdr.GetInt16("SEND_MAIL_FLAG").ToString();
                                if (string.IsNullOrEmpty(sMailRecipient))
                                {
                                    sMailRecipient = objRdr.GetInt32("DESIGNATED_RECIPIENT").ToString();
                                }
                                else
                                {
                                    sMailRecipient = sMailRecipient + "," + objRdr.GetInt32("DESIGNATED_RECIPIENT").ToString();
                                }
                            }
                        }
                        //change end here by swati
                        objRdr.Close();

                        if (string.Compare(sSendEmail, "-1", true) == 0 && string.Compare(sMailRecipient, "", true) != 0)
                        {
                            sRetMes = objMergeManager.SendMailRTF(sFileName, objMemory, sTemplateId, sName, sTable, sType, sRecordId, iPsid, sFormName, sSendEmail, sMailRecipient);
                            Console.WriteLine("0 ^*^*^ " + sRetMes + "for Claim Number : " + objClaim.ClaimNumber);
                            TraceLog.tempsb.AppendLine(DateTime.Now + " " + sRetMes + "for Claim Number : " + objClaim.ClaimNumber);
                            TraceLog.WriteFile(templateTracelogfilePath, TraceLog.tempsb.ToString(), templateTracelogfilename);
                        }

                        string PaperBin = string.Empty;
                        string PrinterName = string.Empty;

                        if (!string.IsNullOrEmpty(objInfoSet.PaperBin) && !string.IsNullOrEmpty(objInfoSet.PrinterName))
                        {
                            PaperBin = objInfoSet.PaperBin;
                            PrinterName = objInfoSet.PrinterName;
                        }
                        else
                        {
                            using (objRdr = DbFactory.GetDbReader(m_sConnectionString, "SELECT * from SYS_PARMS_LOB where LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode))
                            {
                                if (objRdr.Read())
                                {
                                    PaperBin = objRdr.GetString("LETTER_PAPER_BIN");
                                    PrinterName = objRdr.GetString("PRINTER_NAME");
                                }
                            }

                            objRdr.Close();
                        }

                        if (!string.IsNullOrEmpty(PaperBin) && !string.IsNullOrEmpty(PrinterName))
                        {
                            if (PrinterName != "Microsoft XPS Document Writer")
                            {
                                //Application wordApp = new Application();

                                object fileName = strform;
                                object confirmConversions = Type.Missing;
                                object readOnly = false;
                                object addToRecentFiles = Type.Missing;
                                object passwordDoc = Type.Missing;
                                object passwordTemplate = Type.Missing;
                                object revert = Type.Missing;
                                object writepwdoc = Type.Missing;
                                object writepwTemplate = Type.Missing;
                                object format = Type.Missing;
                                object encoding = Type.Missing;
                                object visible = Type.Missing;
                                object openRepair = Type.Missing;
                                object docDirection = Type.Missing;
                                object notEncoding = Type.Missing;
                                object xmlTransform = Type.Missing;


                                doc = wordApp.Documents.Open(
                                    ref fileName, ref confirmConversions, ref readOnly, ref addToRecentFiles,
                                    ref passwordDoc, ref passwordTemplate, ref revert, ref writepwdoc,
                                    ref writepwTemplate, ref format, ref encoding, ref visible, ref openRepair,
                                    ref docDirection, ref notEncoding, ref xmlTransform);

                                object myTrue = true; // Print in background
                                object myFalse = false;

                                

                                wordApp.ActivePrinter = PrinterName;
                           
                                wordApp.Visible = false;

                                //selecting Tray start

                                WdPaperTray wdFirst;
                                wdFirst = (WdPaperTray)Convert.ToInt32(PaperBin);

                                doc.PageSetup.FirstPageTray = wdFirst;

                                doc.PrintOut(ref myTrue,
                                ref myFalse, ref missingValue, ref missingValue, ref missingValue,
                                       ref missingValue, ref missingValue,
                                    ref myTrue, ref missingValue, ref missingValue, ref myFalse,
                                    ref missingValue, ref missingValue, ref missingValue, ref missingValue, ref missingValue, ref missingValue,ref missingValue);

                                doc.Close(ref missingValue, ref missingValue, ref missingValue);
                                wordApp.ActiveDocument.Close(ref missingValue, ref missingValue, ref missingValue);
                                wordApp.Quit(ref missingValue, ref missingValue, ref missingValue);
                                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wordApp);
                              
                                Console.WriteLine("0 ^*^*^ Document has been successsfully printed for Claim Number : " + objClaim.ClaimNumber);
                                TraceLog.tempsb.AppendLine(DateTime.Now + " " + "Document has been successsfully printed for Claim Number : " + objClaim.ClaimNumber);
                                TraceLog.WriteFile(templateTracelogfilePath, TraceLog.tempsb.ToString(), templateTracelogfilename);
                        }
                        else
                        {
                            Console.WriteLine("0 ^*^*^ Document has not been printed for Claim Number : " + objClaim.ClaimNumber + " as Microsoft XPS Printer is attached");
                            TraceLog.tempsb.AppendLine(DateTime.Now + " " + "Document has not been printed for Claim Number  : " + objClaim.ClaimNumber + " as Microsoft XPS Printer is attached");
                            TraceLog.WriteFile(templateTracelogfilePath, TraceLog.tempsb.ToString(), templateTracelogfilename);
                        }
                    }



                  

                        counter++;
                    }

                }

                objFilterRead.Close();
                CreateZip(Constants.sFilePath, m_JobId + "_" + objInfoSet.RowID);

                if (counter == 0)
                {
                    Console.WriteLine("0 ^*^*^ No Criteria has been satisfied for " + objInfoSet.Name + " AutoMailMerge SetUp");
                    TraceLog.tempsb.AppendLine(DateTime.Now + " " + "No Criteria has been satisfied for " + objInfoSet.Name + " AutoMailMerge SetUp");
                    TraceLog.WriteFile(templateTracelogfilePath, TraceLog.tempsb.ToString(), templateTracelogfilename);
                }

                Console.WriteLine("0 ^*^*^ Running InfoRun Procedure for " + objInfoSet.Name + " AutoMailMerge SetUp executed succesfully");
                TraceLog.tempsb.AppendLine(DateTime.Now + " " + "Running InfoRun Procedure for " + objInfoSet.Name + " AutoMailMerge SetUp executed succesfully");
                TraceLog.WriteFile(templateTracelogfilePath, TraceLog.tempsb.ToString(), templateTracelogfilename);
            }

            catch(Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                ErrorLog.sbTempErrorLog.AppendLine(exc.Message);
                throw;
            }

            finally
            {
                Console.WriteLine("0 ^*^*^ Console1");
                if (wordApp != null)
                {
                    Console.WriteLine("0 ^*^*^ Console2");
                    if (doc != null)
                    {
                        Console.WriteLine("0 ^*^*^ Console3");
                        // objWordDoc.Close(ref misValue, ref misValue, ref misValue);
                        doc = null;
                        Console.WriteLine("0 ^*^*^ Console4");
                    }
                    Console.WriteLine("0 ^*^*^ Console5");
                    if (wordApp.Documents.Count > 0)
                    {
                        Console.WriteLine("0 ^*^*^ Console6");
                        if (wordApp.ActiveDocument != null)
                        {
                            Console.WriteLine("0 ^*^*^ Console7");
                            object bSaveChanges = false;
                            Console.WriteLine("0 ^*^*^ Console8");
                            wordApp.ActiveDocument.Close(ref bSaveChanges, ref missingValue, ref missingValue);
                            Console.WriteLine("0 ^*^*^ Console9");
                        }
                    }
                    Console.WriteLine("0 ^*^*^ Console10");
                    wordApp.Quit(ref missingValue, ref missingValue, ref missingValue);
                    Console.WriteLine("0 ^*^*^ Console11");
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wordApp);
                    Console.WriteLine("0 ^*^*^ Console12");
                    wordApp = null;
                    Console.WriteLine("0 ^*^*^ Console13");
                }

            }



        }


        public void WordMerge(string strform, string sheaderFileName, string sDataSourceFileName, string sDataFileName, string sSecFile, string SNumRecords)
        {
            //Start: Neha Suresh Jain, 03/21/2013, make changes to allow saving a doc with fillin and ask fields.
            object fileName = strform;
            object objdataFileName = sDataFileName;
            object pause = false;
            object objsSecFile = sSecFile;
            //object objPassword = sPassword;
            object misValue = Missing.Value;
            //make doc invisible 
            object visible = false;
            object readOnly = false;
            Microsoft.Office.Interop.Word.Application objWordApp = new Application();
            Document objWordDoc = null;            
            bool flag = false;
            try
            {               
                objWordDoc = objWordApp.Documents.Open(ref fileName, ref misValue, ref readOnly, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref visible, ref misValue, ref misValue, ref misValue, ref misValue);
                            
                if (objWordApp.Documents.Count == 1)
                {     
                    //As the document is opened in invisible mode, activate the document.
                    objWordDoc.Activate();                    
                    objWordApp.DisplayAlerts = WdAlertLevel.wdAlertsNone;
                    objWordApp.ActiveDocument.SaveAs(ref fileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);                  
                    objWordDoc.Select();
                    objWordDoc.MailMerge.OpenHeaderSource(sheaderFileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
                    objWordDoc.MailMerge.OpenDataSource(sDataSourceFileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
                    objWordDoc.MailMerge.DataSource.FirstRecord = 1;
                    objWordDoc.MailMerge.DataSource.LastRecord = 1;
                    if (!string.IsNullOrEmpty(SNumRecords))
                    {
                        objWordDoc.MailMerge.DataSource.LastRecord = int.Parse(SNumRecords);
                    }

                    objWordDoc.MailMerge.Destination = WdMailMergeDestination.wdSendToNewDocument;
                    //find fillin and Ask fields..and fill in their default values..
                    foreach (Field afield in objWordDoc.Fields)
                    {
                        //if (afield.Type == WdFieldType.wdFieldFillIn || afield.Type == WdFieldType.wdFieldAsk || afield.Type == WdFieldType.wdFieldMergeField || afield.Type == WdFieldType.wdFieldSet)
                        if (afield.Type == WdFieldType.wdFieldFillIn || afield.Type == WdFieldType.wdFieldAsk)
                        {
                            afield.Select();
                            if (!string.IsNullOrEmpty(afield.Result.Text))
                            {
                                objWordApp.Selection.TypeText(afield.Result.Text);
                            }
                            else
                            {
                                afield.Result.Text = "0";
                                objWordApp.Selection.TypeText("0");
                            }
                        }
                    }
                    objWordDoc.MailMerge.Execute(ref pause);
                    // akaushik5 Added for RMA-5982 Starts
                    objWordDoc.Saved = true;
                    // akaushik5 Added for RMA-5982 Ends
                    objWordDoc.Close(ref misValue, ref misValue, ref misValue);
                    objWordApp.ActiveDocument.Select();
                    objWordApp.ActiveDocument.SaveAs(ref objdataFileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
                    objWordApp.ActiveDocument.SaveAs(ref objsSecFile, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
                    objWordApp.ActiveDocument.Close(ref misValue, ref misValue, ref misValue);
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                ErrorLog.sbTempErrorLog.AppendLine(exc.Message);
            }
            finally
            {                
                
                if (objWordApp != null)
                {
                    if (objWordDoc != null)
                    {
                        
                       // objWordDoc.Close(ref misValue, ref misValue, ref misValue);
                        objWordDoc = null;
                    }
                    if (objWordApp.Documents.Count > 0)
                    {
                        if (objWordApp.ActiveDocument != null)
                        {
                            //replace the error file generated in execute with data file
                            objWordApp.ActiveDocument.SaveAs(ref objdataFileName, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue, ref misValue);
                            object bSaveChanges = false;
                            objWordApp.ActiveDocument.Close(ref bSaveChanges, ref misValue, ref misValue);
                        }
                    }
                    objWordApp.DisplayAlerts = WdAlertLevel.wdAlertsAll;
                    objWordApp.Quit(ref misValue, ref misValue, ref misValue);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(objWordApp);
                    objWordApp = null;
                }

            }
        }
        public int GetFilterIndex(int Index)
        {
            int i ;
            int FilterIndex = -1;
            for( i =0 ; i <= objInfoDef.NumFilters - 1 ; i++)
            {
                if (Index == objInfoDef.FilterDef[i].ID)
                {
                    FilterIndex = i;
                    break;
                }
            }

            return FilterIndex;

        }

        public string sGetTablesNotInSQLFrom(string sSQLFrom, string sTableNames)
        {
            int i;
            int iLastPosition;
            string s;
            string sReturn = string.Empty;
            const string SEPARATOR = ",";
            string sGetTablesNotInSQLFrom = string.Empty;

            sTableNames = sTableNames.Trim();
            sSQLFrom = sSQLFrom.Trim();
            if (sTableNames == "")
            {
                sGetTablesNotInSQLFrom = sReturn;
            }
            if (sSQLFrom == "")
            {
                sReturn = sTableNames;
                sGetTablesNotInSQLFrom = sReturn;
                
            }

            i = sTableNames.IndexOf(SEPARATOR, 0);
            if (i == -1)
            {
                if (!IsTableInSQLFrom(sSQLFrom, sTableNames))
                {
                    sReturn = sTableNames;
                }

                sGetTablesNotInSQLFrom = sReturn;
            }

            else
            {
                iLastPosition = 0;
                while (i > -1)
                {
                    s = sTableNames.Substring(iLastPosition, i - iLastPosition);
                    if (!IsTableInSQLFrom(sSQLFrom, s))
                    {
                        if (sReturn != "")
                        {
                            sReturn = sReturn + SEPARATOR;
                            sReturn = sReturn + s;
                        }
                    }

                    iLastPosition = i + 1;
                    i = sTableNames.IndexOf(SEPARATOR, iLastPosition);

                    if (i == -1)
                    {
                        if (iLastPosition < sTableNames.Length)
                        {
                            s = sTableNames.Substring(iLastPosition);
                            if (!IsTableInSQLFrom(sSQLFrom, s))
                            {
                                if (sReturn != "")
                                {
                                    sReturn = sReturn + SEPARATOR;
                                    sReturn = sReturn + s;
                                }
                            }
                        }
                    }
                }

                sGetTablesNotInSQLFrom = sReturn;
            }

            return sGetTablesNotInSQLFrom;
        }

        public bool IsTableInSQLFrom(string sSQLFrom, string sTableName)
        {
            const string SQL_FROM = "FROM ";
            int i;
            bool IsTableInSQLFrom = false;
            if (sSQLFrom == "" || sTableName == "")
            {
                return false;
            }

            sSQLFrom = sSQLFrom.ToUpper().Trim();
            sTableName = sTableName.ToUpper().Trim();

            i = sTableName.Length;

            if (sSQLFrom.Contains(sTableName))
            {
                if (sSQLFrom.Substring(0, i) == sTableName)
                {
                    IsTableInSQLFrom = true;
                    return IsTableInSQLFrom;
                }
                else if (sSQLFrom.Substring(sSQLFrom.Length - i) == sTableName)
                {
                    IsTableInSQLFrom = true;
                    return IsTableInSQLFrom;
                }
                else if (sSQLFrom.Substring(0, i + SQL_FROM.Length) == SQL_FROM + sSQLFrom)
                {
                    IsTableInSQLFrom = true;
                    return IsTableInSQLFrom;
                }

                else if (sSQLFrom.IndexOf(sTableName + ",", 0) > -1)
                {
                    IsTableInSQLFrom = true;
                    return IsTableInSQLFrom;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


        }

        public int RInStr(int spos, string sStr, string Char)
        {
            int i;
            int RInStr = 0;
            for (i = spos; i >= 1; i--)
            {
                 if (sStr.Substring(i, Char.Length) == Char)
                 {
                     RInStr = i;
                     break;
                 }
            }

            return RInStr;
         }

        public bool checkAlpha(string strInputText)
        {
            int intCounter;
            string strCompare = string.Empty;
            string strInput = string.Empty;
            bool checkAlpha = false;

            for (intCounter = 0; intCounter < strInputText.Length; intCounter++)
            {
                strCompare = strInputText.Substring(intCounter, 1);
                //strInput = strInputText.Substring(intCounter + 1, strInputText.Length - 1);
                if (Regex.IsMatch(strCompare, "[a-zA-Z]$"))
                {
                    checkAlpha = true;
                    return checkAlpha;
                }
                else
                {
                    checkAlpha = false;
                }
            }
            return checkAlpha;
        }

        public void WriteFile(string FilePath, string FileContent)
        {
            byte[] bytes;
            bytes = bytes = Convert.FromBase64String(FileContent);
            using (FileStream fs = new FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (new BinaryWriter(fs))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }
                fs.Close();
            }
        }
		
        public void CreateZip(string FilePath, string strZipfilename)
        {
            /*if (!m_UseTaskMan)
            {
                return;
            } */
            try
            {
                int fileCount = 0;

                C1ZipFile ZipFile = new C1ZipFile(FilePath + strZipfilename + ".zip");
                foreach (string file in Directory.GetFiles(FilePath + strZipfilename))
                {
                    if (!file.EndsWith(".zip"))
                    {
                        if (!(file.EndsWith(".tmp") || file.StartsWith("~")))
                        {
                            fileCount++;
                            ZipFile.Entries.Add(file);
                        }
                    }
                }
                //If there is password
                if (m_sPassword.Length > 0)
                {
                    ZipFile.Password = m_sPassword;
                }
                ZipFile.Close();
            }
            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                //Log.Write(exc.Message + " " + exc.InnerException);
                ErrorLog.sbTempErrorLog.AppendLine(exc.Message);
                throw;
            }
        }
        public void CreateZipFinal(string FilePath, string strZipfilename)
        {
            /*if (!m_UseTaskMan)
            {
                return;
            } */
            bool flag = false;
            C1ZipFile ZipFile = new C1ZipFile(FilePath + strZipfilename + ".zip");
            try
            {
                int fileCount = 0;
               
                foreach (string file in Directory.GetFiles(FilePath, strZipfilename + "*"))
                {
                    if (file.EndsWith(".zip"))
                    {
                        if (string.Compare(file, FilePath + strZipfilename + ".zip", true) != 0)
                        {
                            fileCount++;
                            ZipFile.Entries.Add(file);
                        }
                    }
                }
                //If there is password
                if (m_sPassword.Length > 0)
                {
                    ZipFile.Password = m_sPassword;
                }
                ZipFile.Close();
                flag = true;
            }
            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                //Log.Write(exc.Message + " " + exc.InnerException);
                ErrorLog.sbTempErrorLog.AppendLine(exc.Message);
                throw;
            }
            finally
            {
                if (!flag)
                {
                    ZipFile.Close();
                    ZipFile = null;
                }
            }
        }

        public void FileToDatabase(string fileName, string strZipfilePathName)
        {
            FileStream stream = null;
            byte[] fileData = null;
            if (File.Exists(strZipfilePathName))
            {
                try
                {
                    stream = new FileStream(strZipfilePathName, FileMode.Open, FileAccess.Read);
                    fileData = new byte[stream.Length];
                    stream.Read(fileData, 0, (int)stream.Length);
                    stream.Close();
                }
                catch (Exception exc)
                {
                    Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                    //Log.Write(exc.Message + " " + exc.InnerException);
                    ErrorLog.sbTempErrorLog.AppendLine(exc.Message);
                    throw;
                }
                finally
                {
                    stream.Close();
                    stream.Dispose();
                }
                try
                {
                    using (DbConnection dbConnection = DbFactory.GetDbConnection(RMConfigurationManager.GetConnectionString("TaskManagerDataSource", m_iClientId)))//rkaur27
                    {
                        dbConnection.Open();
                        DbCommand cmdInsert = dbConnection.CreateCommand();
                        DbParameter paramFileId = cmdInsert.CreateParameter();
                        DbParameter paramJobId = cmdInsert.CreateParameter();
                        DbParameter paramFileName = cmdInsert.CreateParameter();
                        DbParameter paramFileData = cmdInsert.CreateParameter();
                        DbParameter paramContentType = cmdInsert.CreateParameter();
                        DbParameter paramLength = cmdInsert.CreateParameter();

                        cmdInsert.CommandText = "INSERT INTO TM_JOBS_DOCUMENT " +
                            "(TM_FILE_ID, JOB_ID, FILE_NAME, FILE_DATA, CONTENT_TYPE, CONTENT_LENGTH) VALUES " +
                            "(~FileId~, ~JobId~, ~FileName~, ~FileData~, ~ContentType~, ~ContentLength~)";

                        paramFileId.Direction = ParameterDirection.Input;
                        paramFileId.Value = int.Parse(m_JobId);
                        //paramFileId.Value = JobId;
                        paramFileId.ParameterName = "FileId";
                        paramFileId.SourceColumn = "TM_FILE_ID";
                        cmdInsert.Parameters.Add(paramFileId);

                        paramJobId.Direction = ParameterDirection.Input;
                        paramJobId.Value = int.Parse(m_JobId);
                        //paramJobId.Value = JobId;
                        paramJobId.ParameterName = "JobId";
                        paramJobId.SourceColumn = "JOB_ID";
                        cmdInsert.Parameters.Add(paramJobId);

                        paramFileName.Direction = ParameterDirection.Input;
                        paramFileName.Value = fileName;
                        paramFileName.ParameterName = "FileName";
                        paramFileName.SourceColumn = "FILE_NAME";
                        cmdInsert.Parameters.Add(paramFileName);

                        paramFileData.Direction = ParameterDirection.Input;
                        paramFileData.Value = fileData;
                        paramFileData.ParameterName = "FileData";
                        paramFileData.SourceColumn = "FILE_DATA";
                        cmdInsert.Parameters.Add(paramFileData);

                        paramContentType.Direction = ParameterDirection.Input;
                        paramContentType.Value = "";
                        paramContentType.ParameterName = "ContentType";
                        paramContentType.SourceColumn = "CONTENT_TYPE";
                        cmdInsert.Parameters.Add(paramContentType);

                        paramLength.Direction = ParameterDirection.Input;
                        paramLength.Value = fileData.Length;
                        paramLength.ParameterName = "ContentLength";
                        paramLength.SourceColumn = "CONTENT_LENGTH";
                        cmdInsert.Parameters.Add(paramLength);

                        cmdInsert.ExecuteNonQuery();

                    }
                    
                }
                catch (Exception exc)
                {
                    Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                    //Log.Write(exc.Message + " " + exc.InnerException);
                    ErrorLog.sbTempErrorLog.AppendLine(exc.Message);
                    throw;
                }

                finally
                {
                }
            }
        }


        public void CleanFolder()
        {
            try
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(Constants.sFilePath);
                foreach (DirectoryInfo subfolder in directoryInfo.GetDirectories(m_JobId + "*"))
                {
                    //Neha Suresh Jain, 03/21/2013: check before deletion
                    if (subfolder.Exists)
                    {
                        subfolder.Delete(true);
                    }
                }

                foreach (string file in Directory.GetFiles(Constants.sFilePath, m_JobId + "*" + ".zip"))
                {
                    //Neha Suresh Jain, 03/21/2013: check before deletion
                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                //Log.Write(exc.Message + " " + exc.InnerException);
                ErrorLog.sbTempErrorLog.AppendLine(exc.Message);
                throw;
            }
        }

   
    }
}
