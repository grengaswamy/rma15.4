﻿using System.Net.Mail;
using System;
using System.Xml;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.Security;
using Riskmaster.Common;
using System.Configuration;
using Riskmaster.Db;
using Riskmaster.Security.Encryption;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Settings;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.IO;
using Riskmaster.Application.FAS;
using System.Net;
using System.Threading;
using Riskmaster.BusinessAdaptor.FASDataAdaptor;


namespace Riskmaster.Tools.FASScheduler
{
    /// Name		: Riskmaster.Tools.FASScheduler
    /// Author		: Anshul Verma
    /// Date Created: 24 June 2013
    ///************************************************************
    /// Amendment History
    ///************************************************************
    /// Date Amended   *   Amendment   *    Author
    ///************************************************************
    /// <summary>
    /// This will run to create an ACORD xml file with all the claim related information which will be sent to FAS (Fraud Analytics Suite).
    /// </summary>
    /// <remarks>*	The tool can either be run directly or can be executed via taskmanager in silent mode.
    /// Command line parameters-
    /// FASScheduler.exe uid pwd DSN adminUid,adminPwd</remarks>
   public class Program
    {
        #region declarations

        static string m_sDataSource = string.Empty;
        static string m_sLoginName = string.Empty;
        static int m_iLoginID = 0;
        static string m_sLoginPwd = string.Empty;
        static string m_sModuleName = string.Empty;
        static string m_sDbConnstring = string.Empty;
        static string m_sDSNID = string.Empty;
        static string m_sDBOUserId = string.Empty;
        static DbConnection m_objDbConnection = null;
        static DbCommand m_objCmd = null;
        static int m_iModuleId;
        static string m_sTableName = string.Empty;
        static int m_DocPathType;
        static string m_sPdfFileName = string.Empty;
        static string m_sAcordPdfFileName = string.Empty;
        static string m_sPdfBasePath = string.Empty;
        static string m_sTempDirPath = string.Empty;
        static string m_sAcordTempDirName = string.Empty;
        static string m_sExecSummaryTempDirName = string.Empty;
        static Riskmaster.Settings.SysSettings objSettings;


        private static string m_sClaimType;
        private static string m_sClaimStatus;
        private static string m_sFromDateOfClaim;
        private static string m_sToDateOfClaim;
        private static string m_sFromClaimUpdateDate;
        private static string m_sToClaimUpdateDate;

        private static bool m_bIsDebugMode = false;

        private static DbReader m_objDbReaderRetriveRc = null;

        static int m_iClientId = 0;//Add by kuladeep for Cloud Changes.
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            UserLogin oUserLogin = null; 
            string smsg = string.Empty;
            string sRm_UserId = string.Empty;
            bool blnSuccess = false;//Add by kuladeep for Cloud Changes.
            try
            {

                int argCount = (args == null) ? 0 : args.Length;

                //default set from appsetting. Value is overwritten if passed in args below.
                m_iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                
                //Add & Change by kuladeep for Cloud----End

                if (argCount > 0)//Entry from TaskManager
                {
                    GetCommandLineParameters(args);

                    if (string.IsNullOrEmpty(m_sDataSource)
                       || string.IsNullOrEmpty(m_sLoginName)
                       || string.IsNullOrEmpty(m_sLoginPwd))
                    {
                        throw new Exception("parameters missing.");
                    }

                    writeLog("\nAuthenticating.... \n");
                    //Thread.Sleep(15000); //debug 

                    //AppGlobals.Userlogin = new UserLogin(m_sLoginName, m_sLoginPwd, m_sDataSource);
                    AppGlobals.Userlogin = new UserLogin(m_sLoginName, m_sDataSource,m_iClientId);//Add clientId by kuladeep for Cloud.

                    if (AppGlobals.Userlogin == null
                        || AppGlobals.Userlogin.DatabaseId == 0
                        || AppGlobals.Userlogin.LoginName == string.Empty)
                    {
                        throw new Exception("Authentication failure.");
                    }
                    Initalize(AppGlobals.Userlogin);
                }
                else//Entry for direct run
                {
                    // Direct run just for debug purposes right now... The cliam ID etc paremeter still need to be provided.

                    Login objLogin = new Login(m_iClientId);//Add clientId by kuladeep for Cloud.
                    bool bCon = false;

                    bCon = objLogin.RegisterApplication(0, 0, ref AppGlobals.Userlogin, m_iClientId);//Add clientId by kuladeep for Cloud.
                    if (bCon)
                    {
                        Initalize(AppGlobals.Userlogin);
                    }
                    else
                    {
                        throw new Exception("Unauthenticated");
                    }

                }

                AppGlobals.ConnectionString = AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString;
                m_objDbConnection = DbFactory.GetDbConnection(AppGlobals.ConnectionString);
             
                StartFASSchedulerProcess();
            }
            catch (Exception ex)
            {
                writeLog("ERROR [Main]" + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                if (m_objDbConnection != null)
                {
                    m_objDbConnection.Close();
                    m_objDbConnection.Dispose();

                }

                if (m_objCmd != null)
                    m_objCmd = null;

                if (m_objDbConnection != null)
                    m_objDbConnection.Dispose();

            }

        }

        public static void writeLog(string p_strLogText)
        {
            Console.WriteLine("\n0 ^*^*^ " + p_strLogText.Replace("\n", "\n0 ^*^*^ "));
        }
        private static void GetCommandLineParameters(string[] p_args)
        {
            int iLength = 0;
            string sPrefix = string.Empty;
            string sRebuildAll = string.Empty;
            iLength = p_args.Length;
            bool blnSuccess=false;

            for (int i = 0; i < iLength; i++)
            {
                sPrefix = p_args[i].Trim();

                if (sPrefix.Length > 3)
                {
                    sPrefix = sPrefix.Substring(0, 3);
                }
                switch (sPrefix.ToLower())
                {
                    case "-ds":
                        m_sDataSource = p_args[i].Trim().Substring(3);
                        break;
                    case "-ru":
                        m_sLoginName = p_args[i].Trim().Substring(3);
                        break;
                    case "-rp":
                        m_sLoginPwd = p_args[i].Trim().Substring(3);
                        m_sLoginPwd = XmlConvert.DecodeName(m_sLoginPwd);
                        break;

                    case "-ny": m_sClaimType = p_args[i].Trim().Substring(3);
                        break;
                    case "-ns": m_sClaimStatus = p_args[i].Trim().Substring(3);
                        break;
                    case "-nf": m_sFromDateOfClaim = p_args[i].Trim().Substring(3);
                        break;
                    case "-nt": m_sToDateOfClaim = p_args[i].Trim().Substring(3);
                        break;
                    case "-nm": m_sFromClaimUpdateDate = p_args[i].Trim().Substring(3);
                        break;
                    case "-nn": m_sToClaimUpdateDate = p_args[i].Trim().Substring(3);
                        break; ;
                    case "-ci": m_iClientId = Conversion.CastToType<int>(p_args[i].Trim().Substring(3), out blnSuccess);//Add clientId by kuladeep for Cloud.
                        break; ;
                }
            }
        }
        /// <summary>
        /// Initialze Global Varriables.
        /// </summary>
        /// <param name="p_oUserLogin">User Login Object</param>
        private static void Initalize(UserLogin p_oUserLogin)
        {
            m_sDSNID = p_oUserLogin.DatabaseId.ToString();
            m_sDBOUserId = p_oUserLogin.objRiskmasterDatabase.RMUserId;
            m_sDbConnstring = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
            m_DocPathType = p_oUserLogin.objRiskmasterDatabase.DocPathType;
            m_iLoginID = p_oUserLogin.UserId;
        }

      
        private static bool StartFASSchedulerProcess()
        {
            string sPdfPath = string.Empty;

            try
            {
                writeLog("\nProcessing FAS Claim Data XML");
                sPdfPath = CreateFASClaimData();
            }

            catch (Exception ex)
            {
                AppGlobals.htErrors.Add(ex.Message, ex.StackTrace);
                writeLog("ERROR: [nProcessing]" + ex.Message + " " + ex.StackTrace);
            }

            return true;

        }
        public static string CreateFASClaimData()
        {
            FASAdaptor objFASAdaptor = null;
            string sfilename = string.Empty;
            string sFileName=string.Empty;

            try
            {
                objFASAdaptor = new FASAdaptor(AppGlobals.Userlogin.LoginName, AppGlobals.Userlogin.Password, AppGlobals.Userlogin.objRiskmasterDatabase.DataSourceName, AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
                objFASAdaptor.SetSecurityInfo(AppGlobals.Userlogin, m_iClientId);                
                objFASAdaptor.CreateFASClaimData(m_sClaimStatus, m_sClaimType, m_sFromDateOfClaim, m_sToDateOfClaim, m_sToClaimUpdateDate, m_sFromClaimUpdateDate,ref sFileName);
                writeLog("\n " + sFileName);
            }
            catch (Exception ex)
            {   
                AppGlobals.htErrors.Add(ex.Message, ex.StackTrace);
                writeLog("ERROR [FASData]:" + ex.Message + " " + ex.StackTrace);
            }
            return sFileName;

        }
   }
}
