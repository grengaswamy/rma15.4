using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using Riskmaster.Security;
using Riskmaster.Application.EnhancePolicy.Billing;
using Riskmaster.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Configuration;

namespace BillingScheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            UserLogin oUserLogin = null;
            NoticeManager objNoticeManager = null;
            InvoiceManager objInvoiceManager = null;
            GenerateNextInstallment objGenerateInstallment = null;
            int iClientId = 0;
            bool blnSuccess = false;
            string sGenerateType = string.Empty;//Invoice/Notice/Installment

            try
            {
                AppDomain.CurrentDomain.AppendPrivatePath(AppDomain.CurrentDomain.BaseDirectory);

               //args = new string[5];
               //args[0] = "rmACloud";
               //args[1] = "cloud";
               //args[2] = "cloud";
               //args[3] = "1";
               //args[4] = "Invoice";
                              
                //args[0] = "Installment";
                //args[1] = "Mississippi";
                //args[2] = "csc";
                //args[3] = "csc";  

                //Add & Change by kuladeep for Cloud----Start
                if (args.Length == 5  && (string.IsNullOrEmpty(args[3]) == false))
                {

                    iClientId = Convert.ToInt32(args[3]);
                    sGenerateType = args[4];
                }
                //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
                //By this we can run Tool through exe for different client by change ClientId in appSetting.config.
                else
                {
                    sGenerateType = args[3];
                    iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                }
                //Add & Change by kuladeep for Cloud----End

                if (args.Length <= 0)
                {
                    throw new Exception("Please specify the  command line parameters correctly ! ");
                }

                //MITS 22996 Ignore password when called from TaskManager
                oUserLogin = new UserLogin(args[1], args[0],iClientId);//Add ClientId by kuladeep for Cloud.
                if (oUserLogin.DatabaseId <= 0)
                {
                    throw new Exception("Authentication failure.");
                    //throw new System.IO.IOException("IO Exception");
                }

                if (sGenerateType == "Invoice")
                {
                    using (objInvoiceManager = new InvoiceManager(oUserLogin.objRiskmasterDatabase.DataSourceName,
                        oUserLogin.LoginName, oUserLogin.Password, iClientId))//Add ClientId by kuladeep for Cloud.
                    {
                        // Call the method
                        Console.WriteLine("0 ^*^*^ Generating Invoices");
                        objInvoiceManager.PrintInvoice();
                        Console.WriteLine("0 ^*^*^ Generation successful");
                    }
                }
                else if (sGenerateType == "Notice")
                {
                    using(objNoticeManager = new NoticeManager(oUserLogin.objRiskmasterDatabase.DataSourceName,
                        oUserLogin.LoginName, oUserLogin.Password,iClientId))//Add ClientId by kuladeep for Cloud.
                    {
                        // Call the method
                        Console.WriteLine("0 ^*^*^ Generating Notices");
                        objNoticeManager.PrintNotice();
                        Console.WriteLine("0 ^*^*^ Generation successful");
                    }
                }
                else if (sGenerateType == "Installment")
                {
                    using (objGenerateInstallment = new GenerateNextInstallment(oUserLogin.objRiskmasterDatabase.DataSourceName,
                                                                oUserLogin.LoginName, oUserLogin.Password,iClientId))//Add ClientId by kuladeep for Cloud.
                    {
                        // Call the method
                        Console.WriteLine("0 ^*^*^ Generating Next Installment");
                        objGenerateInstallment.GenerateInstallment();
                        Console.WriteLine("0 ^*^*^ Generation successful");
                    }
                }
                else
                    throw new Exception("Please specify either Invoice, Notice or Installment as the first parameter");

            
            }
            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                //Log.Write(exc.Message, "CommonWebServiceLog");  

                //Simply log the exception
                bool rethrow = ExceptionPolicy.HandleException(exc, "Logging Policy");
            }           
        }
    }
}
