﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BatchFroiAndAcord;

namespace BatchFroiAndAcord
{
    public partial class FroiAndAcordPrompt : Form
    {
        public FroiAndAcordPrompt()
        {
            InitializeComponent();
            if (AppGlobals.bDisableControls)
            {
                
                lblMessage.Visible = false;
               

                this.Size = new Size(294, 150);

                this.chkBox1.Location = new Point(this.chkBox1.Location.X+40, 30);

                this.lblFROI.Location = new Point(this.lblFROI.Location.X+40, 30);

                this.btnOk.Location = new Point(this.btnOk.Location.X, 70);
                this.btnCancel.Location = new Point(this.btnCancel.Location.X, 70);
            }
        }
        internal delegate void ReturnEventHandler(bool bCheckvalueFroi,bool bCheckValueAcord);

        internal event ReturnEventHandler ReturnValue;

        private void btnOk_Click(object sender, EventArgs e)
        {
            ReturnValue(chkBox1.Checked, chkBox2.Checked);
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ReturnValue(false,false);
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
