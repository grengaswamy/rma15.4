﻿using System;
using System.Reflection;

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: CLSCompliant(true)]
[assembly: AssemblyProduct("RISKMASTER X r6SP1")]
[assembly: AssemblyCompany("CSC")]

[assembly: AssemblyCopyright("Copyright (c) 2010, CSC")]
[assembly: AssemblyTrademark("CSC")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("3.1.467.0")]
[assembly: AssemblyFileVersion("3.1.391.0")]
[assembly: AssemblyInformationalVersion("3.1.467.0")]

/*using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("BatchFroiAndAcord")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Computer Sciences Corporation (CSC)")]
[assembly: AssemblyProduct("BatchFroiAndAcord")]
[assembly: AssemblyCopyright("Copyright © Computer Sciences Corporation (CSC) 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("16479cf0-56f4-4b22-85d9-22342cb3b936")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.0.467.0")]
[assembly: AssemblyFileVersion("3.0.467.0")]*/
