﻿using System;
using System.Collections;
using System.IO;
using System.Xml;
using Amyuni.PDFCreator;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.FROINet6;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;
using System.Configuration;
using Riskmaster.Models;
namespace BatchFroiAndAcord
{
    /// Name		: BatchFroiAndAcord
    /// Author		: Nadim Zafar
    /// Date Created: 20 july 2010
    ///************************************************************
    /// Amendment History
    ///************************************************************
    /// Date Amended   *   Amendment   *    Author
    ///************************************************************
    /// <summary>
    /// This will run to Print Froi and acord in batch,it can run from taskmanager as well as silently
    /// </summary>
    /// <remarks>*	The tool can either be run directly or can be executed via taskmanager in silent mode.
    /// Command line parameters-
    /// BatchFroiAndAcord.exe uid pwd DSN adminUid,adminPwd</remarks>
    public class Program
    {
        static string m_sDataSource = string.Empty;
        static string m_sLoginName = string.Empty;
        static string m_sLoginPwd = string.Empty;
        static string m_sAdminUserName = string.Empty;
        static string m_sAdminPassword = string.Empty;
        static string m_sDbConnstring = string.Empty;
        static string m_sAdminDbConnstring = string.Empty;
        static string m_sDSNID = string.Empty;
        static string m_sDBOUserId = string.Empty;
        static string m_strPDFTemplatePath = string.Empty;
        static string m_strPDFOutputPath = string.Empty;
        static string m_strAcordPDFOutputPath = string.Empty;
        static string m_strPDFUrlPath = string.Empty;
        static DbConnection m_objDbConnection = null;
        static DbCommand m_objCmd = null;
        static int m_iClaimId;
        static int m_iFormId;
        static int m_iBatchId;
        static string sStateId;
        static int m_DocPathType;
        static string m_sPdfFileName = string.Empty;
        static string m_sAcordPdfFileName = string.Empty;
        static string m_sPdfBasePath = string.Empty;
        static string m_sAcordPdfBasePath = string.Empty;
        static int m_iAcordBatchId;
        static int m_iAcordClaimId;
        static int m_iAcordFormId;
        static string m_sTempDirPath = string.Empty;
        static string m_sAcordTempDirName = string.Empty;
        static string m_sFroiTempDirName = string.Empty;
        static bool m_bCheckvalueFroi = false;
        static bool m_bCheckvalueAcord = false;
        
        //Deb:MITS 25330

        /// <summary>
        /// FROIFails Enums
        /// </summary>
        public enum FROIFails : int
        {
            FF_NCCIBodyParts = 2,
            FF_NCCICauseCode = 4,
            FF_NCCIClassCode = 8,
            FF_NCCIIllness = 16,
            FF_NoDepartmentAssigned = 32,
            FF_NoClaimant = 64,
            FF_LoadFROIOptionsFailed = 128,
            FF_NonNumericClassCode = 256
        };
        static string sClaimNumber = string.Empty;
        static int m_iClientId = 0;//rkaur27
        //Deb:MITS 25330
        //static string m_sAcordPdfFileName;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            Login objLogin = null;
            string smsg = string.Empty;
            string sRm_UserId = string.Empty;
            string sTempClientId = string.Empty;
            bool blnSuccess = false;
            //args = new string[5];
            //args[0] = "rmACloud";
            //args[1] = "cloud";
            //args[2] = "cloud";
            //args[3] = "True";
            try
            {
                //string[] arrFields;
                if (args != null && args.Length > 5 && (args[5] != null))
                {
                    sTempClientId = args[5] ?? string.Empty;
                    m_iClientId = Convert.ToInt32(sTempClientId);
                }
                //this value by default will be zero and can be change as per requirements through appSetting.config for different client.
                //By this we can run Tool through exe for different client by change ClientId in appSetting.config.
                else
                {
                    m_iClientId = Conversion.CastToType<int>(ConfigurationManager.AppSettings["ClientId"], out blnSuccess);
                }
                //Add & Change by rkaur27 for Cloud----End

                int argCount = (args == null) ? 0 : args.Length;
                if (argCount > 0)//Entry from TaskManager
                {
                    //GetParameters(args);
                    //m_sDataSource = "Z_R6SP1_MAY16";
                    //args = new string[3];
                    //args[0] = "Z_R6SP1_MAY16";
                    //args[1]="csc";
                    // args[2] = "csc";
                    m_sLoginName = args[1];
                    m_sDataSource = args[0];
                    //MITS 22996 Ignore password when called from TaskManager
                    AppGlobals.Userlogin = new UserLogin(m_sLoginName, m_sDataSource, m_iClientId);
                
                    if (AppGlobals.Userlogin.DatabaseId <= 0)
                    {
                        throw new Exception("Authentication failure.");
                    }
                    Initalize(AppGlobals.Userlogin);
                    m_bCheckvalueFroi = Convert.ToBoolean(args[3]);
                    m_bCheckvalueAcord = Convert.ToBoolean(args[4]);
                }
                else//Entry for direct run
                {
                    objLogin = new Login(m_iClientId);
                    bool bCon = false;
                    bCon = objLogin.RegisterApplication(0, 0, ref AppGlobals.Userlogin, m_iClientId);
                    if (bCon)
                    {
                        Initalize(AppGlobals.Userlogin);
                    }
                    GetSettingsFroiAcord();
                }
                ProcessBatchPrint();
                }
            catch (Exception exc)
            {
                smsg = exc.Message;
                FormatAndDisplayExcMsg(smsg);
                Log.Write(exc.Message, "CommonWebServiceLog", m_iClientId);
            }
            finally
            {
                if (m_objDbConnection != null)
                {
                    m_objDbConnection.Close();
                    m_objDbConnection.Dispose();
                }
                if (m_objCmd != null)
                    m_objCmd = null;

                if (m_objDbConnection != null)
                    m_objDbConnection.Dispose();
            }
        }

        /// <summary>
        /// Format Exception Message returned from Stored Procedure.
        /// </summary>
        /// <param name="smsg">Exception Message</param>
        private static void FormatAndDisplayExcMsg(string smsg)
        {
            string[] sMsgArray = smsg.Split('\n');

            int iLengthofArray = sMsgArray.Length;
            for (int i = 0; i < iLengthofArray; i++)
            {
                sMsgArray[i] = sMsgArray[i].Replace("'", "");
                sMsgArray[i] = sMsgArray[i].Replace("-", " ");
                Console.WriteLine("1001 ^*^*^ {0} ", sMsgArray[i]);
            }
        }

        /// <summary>
        /// Initialze Global Varriables.
        /// </summary>
        /// <param name="p_oUserLogin">User Login Object</param>
        private static void Initalize(UserLogin p_oUserLogin)
        {
            m_sDSNID = p_oUserLogin.DatabaseId.ToString();
            m_sDBOUserId = p_oUserLogin.objRiskmasterDatabase.RMUserId;
            m_sDbConnstring = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
            m_DocPathType = p_oUserLogin.objRiskmasterDatabase.DocPathType;
        }

        /// Name		: GetAcrosoftSetting
        /// Author		: Nadim Zafar
        /// Date Created: 		20 july 2010
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function get acrosoft settings.
        /// </summary>
        private static void GetAcrosoftSetting()
        {
            RMConfigurationManager.GetAcrosoftSettings();
            m_strPDFTemplatePath = Riskmaster.Common.RMConfigurator.CombineFilePath(Riskmaster.Common.RMConfigurator.AppFilesPath, @"pdf-forms\");
            m_strPDFOutputPath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, @"froi");
            //rkaur27 : As FROI settings will be same for all the clients in cloud environment,
            // so they are fetched from appsetting.config and not from DB.
            m_strPDFUrlPath = RMConfigurationManager.GetNameValueSectionSettings("FROI", string.Empty, 0)["FROI_PdfUrlPath"];
        }

        /// <summary>
        /// Gets the PDF path.
        /// </summary>
        /// <returns></returns>
        private static string GetPdfPath()
        {
            string sBasepath = string.Empty;
            string sTemp = string.Empty;
            try
            {
                sTemp = "RiskmasterUI\\UI\\pdf-forms\\";
                sBasepath = Riskmaster.Common.RMConfigurator.BasePath;
                sBasepath = sBasepath.Remove((sBasepath.Length - 10), 10);
                sBasepath = sBasepath + sTemp;
            }
            catch (Exception exc)
            {
                throw exc;
            }
            return sBasepath;
        }

        /// <summary>
        /// Gets the acord PDF path.
        /// </summary>
        /// <returns></returns>
        private static string GetAcordPdfPath()
        {
            string sBasepath = string.Empty;
            string sTemp = string.Empty;
            try
            {
                sTemp = "RiskmasterUI\\UI\\pdf-forms\\ClaimForms\\AC\\";
                sBasepath = Riskmaster.Common.RMConfigurator.BasePath;
                sBasepath = sBasepath.Remove((sBasepath.Length - 10), 10);
                sBasepath = sBasepath + sTemp;
            }
            catch (Exception exc)
            {
                throw exc;
            }
            return sBasepath;
        }

        /// <summary>
        /// Creates the froi temporary dir.
        /// </summary>
        private static void CreateFroiTempDir()
        {
            string sBasepath = string.Empty;
            string sTemp = string.Empty;
            try
            {
                sTemp = "RiskmasterUI\\UI\\pdf-forms\\ClaimForms\\AC\\";
                sBasepath = Riskmaster.Common.RMConfigurator.BasePath;
                sBasepath = sBasepath.Remove((sBasepath.Length - 10), 10);
                sBasepath = sBasepath + sTemp;

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// Creates the and delete acord temporary dir.
        /// </summary>
        /// <param name="bType">if set to <c>true</c> [b type].</param>
        private static void CreateAndDeleteAcordTempDir(bool bType)
        {
            string sBasepath = string.Empty;
            string sTemp = string.Empty;
            try
            {
                if (bType)
                {
                    sBasepath = m_strPDFOutputPath.Substring(0, (m_strPDFOutputPath.IndexOf("userdata")));
                    sBasepath = sBasepath + "temp";
                    Random rd = new Random();
                    sTemp = rd.Next().ToString();
                    m_sAcordTempDirName = sBasepath + "\\ACORD_BATCH_" + sTemp;
                    System.IO.Directory.CreateDirectory(@m_sAcordTempDirName);
                }
                else
                {
                    //delete folder as well as file recursively
                    System.IO.Directory.Delete(@m_sAcordTempDirName, true);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// Name		: CreateAndDeleteFroiTempDir
        /// Author		: Nadim Zafar
        /// Date Created: 		20 july 2010
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function create and delete froi temp directory.
        /// </summary>
        /// <returns>Instance of FROI interop class.</returns>
        private static void CreateAndDeleteFroiTempDir(bool bType)
        {
            string sBasepath = string.Empty;
            string sTemp = string.Empty;
            try
            {
                if (bType)
                {
                    sBasepath = m_strPDFOutputPath.Substring(0, (m_strPDFOutputPath.IndexOf("userdata")));
                    sBasepath = sBasepath + "temp";
                    Random rd = new Random();
                    sTemp = rd.Next().ToString();
                    m_sFroiTempDirName = sBasepath + "\\FROI_BATCH_" + sTemp;
                    Directory.CreateDirectory(@m_sFroiTempDirName);
                }
                else
                {
                    //delete folder as well as file recursively
                    Directory.Delete(@m_sFroiTempDirName, true);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// Name		: InitializeFROIInterOp
        /// Author		: Nadim Zafar
        /// Date Created: 		20 july 2010
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// This function instanstiate FROI interop class.
        /// </summary>
        /// <returns>Instance of FROI interop class.</returns>
        //private static FROINet6.CFROIFormsClass InitializeFROI(ref XmlDocument p_objXmlDoc, ref BusinessAdaptorErrors p_objErr)
        private static FROIForms InitializeFROI(ref XmlDocument p_objXmlDoc, ref BusinessAdaptorErrors p_objErr)
        {
            // akaushik5 Changed for JIRA 3631 Starts
            //FROINet6.CFROIFormsClass objFroi = null;
            FROIForms objFroi = null;
            // akaushik5 Changed for JIRA 3631 Ends
            int iUserId;
            
            try
            {
                p_objXmlDoc = new XmlDocument();
                p_objErr = new BusinessAdaptorErrors(m_iClientId);
                // akaushik5 Changed for JIRA 3631 Starts
                //objFroi = new FROINet6.CFROIFormsClass();
                objFroi = new FROIForms(AppGlobals.Userlogin, m_iClientId);
                // akaushik5 Changed for JIRA 3631 Ends
                objFroi.DSN = m_sDbConnstring;
                iUserId = AppGlobals.Userlogin.UserId;
                // akaushik5 Commented for JIRA 3631 Starts
                //objFroi.set_LoginId(ref iUserId);
                // akaushik5 Commented for JIRA 3631 Ends
                objFroi.LoginName = AppGlobals.Userlogin.LoginName;
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            return objFroi;
        }

        /// <summary>
        /// Processes the batch print.
        /// </summary>
        private static void ProcessBatchPrint()
        {
            GetAcrosoftSetting();
            // GetSettingsFroiAcord();
            if (m_bCheckvalueFroi)
            {
                Console.WriteLine("0 ^*^*^ Processing of FROIs pdf-started");
                ProcessFROI();
                Console.WriteLine("0 ^*^*^ Processing of FROIs pdf-completed");
            }
            if (m_bCheckvalueAcord)
            {
                Console.WriteLine("0 ^*^*^ Processing of ACORDs pdf-started");
                ProcessAcord();
                Console.WriteLine("0 ^*^*^ Processing of ACORDs pdf-completed");
            }
        }

        /// <summary>
        /// Objects the admin_ return value.
        /// </summary>
        /// <param name="bCheckvalueFroi">if set to <c>true</c> [b checkvalue froi].</param>
        /// <param name="bCheckvalueAcord">if set to <c>true</c> [b checkvalue acord].</param>
        private static void objAdmin_ReturnValue(bool bCheckvalueFroi, bool bCheckvalueAcord)
        {
            m_bCheckvalueFroi = bCheckvalueFroi;
            m_bCheckvalueAcord = bCheckvalueAcord;
        }

        /// <summary>
        /// Gets the settings froi acord.
        /// </summary>
        private static void GetSettingsFroiAcord()
        {
            FroiAndAcordPrompt objAdmin = new FroiAndAcordPrompt();
            objAdmin.ReturnValue += new FroiAndAcordPrompt.ReturnEventHandler(objAdmin_ReturnValue);
            objAdmin.ShowDialog();
            AppGlobals.bShowDialog = false;
            objAdmin.ReturnValue -= new FroiAndAcordPrompt.ReturnEventHandler(objAdmin_ReturnValue);
        }

        /// <summary>
        /// Appends the slash.
        /// </summary>
        /// <param name="p_surl">The p_surl.</param>
        /// <returns></returns>
        public static string AppendSlash(string p_surl)
        {
            if (!p_surl.EndsWith("/"))
                p_surl = p_surl + "/";
            return p_surl;
        }

        /// Name		: SetProperties
        /// Author		: Nadim Zafar
        /// Date Created: 		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 09 July 2010  *   Added check for global Attachform flag
        ///************************************************************
        /// <summary>
        /// This function will set some common properties of FROI interop object.
        /// </summary>
        /// <param name="p_objDoc">Document containing the various properties to set.</param>
        /// <param name="p_objFroi">FROI interop object on which properties will be set.</param>
        // akaushik5 Changed for JIRA 3631 Starts
        //private void SetProperties(ref XmlDocument p_objDoc, ref FROINet6.CFROIFormsClass p_objFroi)
        private void SetProperties(XmlDocument p_objDoc, FROIForms p_objFroi)
        // akaushik5 Changed for JIRA 3631 Ends
        {
            string sFilePath = string.Empty;
            string sRequestHost = string.Empty;
            try
            {
                sRequestHost = "http://localhost/RiskmasterUI";
                p_objFroi.PDFPath = m_strPDFTemplatePath;
                p_objFroi.OutputPath = m_strPDFOutputPath;
                p_objFroi.PDFUrl = AppendSlash(sRequestHost).Replace(":443/", "/").Replace(":80/", "/") + AppendSlash(m_strPDFUrlPath);
                p_objFroi.DocStorageType = (short)m_DocPathType;

                if (AppGlobals.Userlogin.DocumentPath.Length > 0)
                {
                    p_objFroi.DocumentPath = AppGlobals.Userlogin.DocumentPath;
                }
                else
                {
                    p_objFroi.DocumentPath = AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath;
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
        }

        /// <summary>
        /// Name		: GetAttachFromValueFromFroiSettings
        /// Author		: Nadim Zafar
        /// Date Created: 	9 july 2010
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************		
        /// </summary>
        /// <returns>returns values from Global froi settings 0 - True, 1-False
        /// </returns>
        private static bool GetAttachFormValueFromFroiSettings()
        {
            using (DbReader objRdr = DbFactory.GetDbReader(m_sDbConnstring, "SELECT ATTACH_TO_CLAIM FROM FROI_OPTIONS"))
            {
                if (objRdr.Read())
                {
                    return (objRdr.GetInt32(0).Equals(0) ? true : false);
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the name of the froi PDF.
        /// </summary>
        /// <param name="iMergeCount">The i merge count.</param>
        /// <param name="sPdfCreated">The s PDF created.</param>
        /// <returns></returns>
        public static string GetFroiPdfName(int iMergeCount, string sPdfCreated)
        {
            string sFroiPdfname = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;
            sTemp = "FROI_FINAL_" + iMergeCount + "_" + m_iBatchId + ".pdf";
            sFdfPathTemp = sPdfCreated.Remove(sPdfCreated.LastIndexOf("\\") + 1);
            sFdfPathTemp = sFdfPathTemp + sTemp;
            sFroiPdfname = sFdfPathTemp;
            return sFroiPdfname;
        }

        /// <summary>
        /// Gets the name of the acord PDF.
        /// </summary>
        /// <param name="iMergeCount">The i merge count.</param>
        /// <param name="sPdfCreated">The s PDF created.</param>
        /// <returns></returns>
        public static string GetAcordPdfName(int iMergeCount, string sPdfCreated)
        {
            string sFroiPdfname = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;

            sTemp = "ACORD_FINAL_" + iMergeCount + "_" + m_iAcordBatchId + ".pdf";
            sFdfPathTemp = sPdfCreated.Remove(sPdfCreated.LastIndexOf("\\") + 1);
            sFdfPathTemp = sFdfPathTemp + sTemp;
            sFroiPdfname = sFdfPathTemp;

            return sFroiPdfname;
        }

        /// <summary>
        /// Gets the froi PDF nam previous.
        /// </summary>
        /// <param name="iMergeCount">The i merge count.</param>
        /// <param name="sPdfCreated">The s PDF created.</param>
        /// <returns></returns>
        public static string GetFroiPdfNamPrevious(int iMergeCount, string sPdfCreated)
        {
            string sFroiPdfname = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;
            iMergeCount = iMergeCount - 1;
            sTemp = "FROI_FINAL_" + iMergeCount + "_" + m_iBatchId + ".pdf";
            sFdfPathTemp = sPdfCreated.Remove(sPdfCreated.LastIndexOf("\\") + 1);
            sFdfPathTemp = sFdfPathTemp + sTemp;
            sFroiPdfname = sFdfPathTemp;

            return sFroiPdfname;
        }

        /// <summary>
        /// Gets the acord PDF nam previous.
        /// </summary>
        /// <param name="iMergeCount">The i merge count.</param>
        /// <param name="sPdfCreated">The s PDF created.</param>
        /// <returns></returns>
        public static string GetAcordPdfNamPrevious(int iMergeCount, string sPdfCreated)
        {
            string sFroiPdfname = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;
            iMergeCount = iMergeCount - 1;
            sTemp = "ACORD_FINAL_" + iMergeCount + "_" + m_iAcordBatchId + ".pdf";
            sFdfPathTemp = sPdfCreated.Remove(sPdfCreated.LastIndexOf("\\") + 1);
            sFdfPathTemp = sFdfPathTemp + sTemp;
            sFroiPdfname = sFdfPathTemp;
            string sFinalPdf = string.Empty;
            return sFroiPdfname;
        }

        /// <summary>
        /// Processes the froi.
        /// </summary>
        /// <returns></returns>
        private static bool ProcessFROI()
        {
            XmlDocument p_objXmlOut = null;
            XmlDocument p_objXmlIn = null;
            // akaushik5 Changed for JIRA 3631 Starts
            //FROINet6.CFROIFormsClass objFroi = null;
            FROIForms objFroi = null;
            // akaushik5 Changed for JIRA 3631 Ends
            BusinessAdaptorErrors p_objErrOut = null;
            SysSettings objSettings = null;
            string sFilePath = string.Empty;
            string sRequestHost = string.Empty;
            int iFormCount = 0;
            string sPdfPath = string.Empty;
            string sFdfPath = string.Empty;
            string sPdfMergedPath = string.Empty;
            int iRowcount = 0;
            string sPdfCreated = string.Empty;
            int iMergeCount = 0;
            string sFinalPdf = string.Empty;
            //Deb:MITS 25330
            int iErrorType = 0;
            bool bParse = false;
            //Deb:MITS 25330
            try
            {
                BatchFroiAndAcord.Program objBatch = new Program();
                objFroi = InitializeFROI(ref p_objXmlOut, ref p_objErrOut);

                objBatch.SetProperties(p_objXmlIn, objFroi);
                objFroi.AttachForm = GetAttachFormValueFromFroiSettings();
                
                iRowcount = getRowCount();
                
                for (int iOuterCnt = 0; iOuterCnt < iRowcount; iOuterCnt++)
                {
                    iMergeCount = 0;
                    iFormCount = getFormCount();
                    if (iFormCount > 0)
                    {
                        CreateAndDeleteFroiTempDir(true);//true for create directory,false to delete directory
                    }

                    for (int iInnerCount = 0; iInnerCount < iFormCount; iInnerCount++)
                    {
                        GetForms();
                        // arrForms = (System.Array)objFroi.GetFROIForms(m_iClaimId);
                        objSettings = new SysSettings(m_sDbConnstring, m_iClientId);
                        objFroi.FroiPrintType = "BATCH";//Add by kuladeep for for froi issue 4/4/2012 mits:28100
                        if (objFroi.AttachForm && objSettings.UseAcrosoftInterface)
                        {
                            objFroi.AttachForm = false;
                            objFroi.DocumentPath = "";
                            sFilePath = objFroi.InvokeFROI(m_iClaimId, m_iFormId);
                            objFroi.AttachForm = true;
                        } // if
                        else
                        {
                            sFilePath = objFroi.InvokeFROI(m_iClaimId, m_iFormId);
                        } // else

                        if (sFilePath != null && sFilePath.Trim() != "")
                        {
                            // objBatch.MakeOutputDocumentWithFileContent("Froi", ref p_objXmlOut, sFilePath);
                            //Deb:MITS 25330,Start
                            iMergeCount = iMergeCount + 1;
                            bParse = int.TryParse(sFilePath, out iErrorType);
                            if (bParse)
                            {
                                Console.WriteLine("0 ^*^*^ Merging of FROIs pdf-fdf(" + iMergeCount + ")-started");
                                switch (iErrorType)
                                {
                                    case (int)FROIFails.FF_NCCIBodyParts:
                                        Console.WriteLine("0 ^*^*^NCCIBodyParts-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCICauseCode:
                                        Console.WriteLine("0 ^*^*^NCCICause-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIIllness:
                                        Console.WriteLine("0 ^*^*^NCCIInjury/Illness-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NonNumericClassCode:
                                        Console.WriteLine("0 ^*^*^NonNumericClassCode-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCICauseCode:
                                        Console.WriteLine("0 ^*^*^NCCIBodyParts-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCICause-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCIIllness:
                                        Console.WriteLine("0 ^*^*^NCCIBodyParts-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCIInjury/Illness-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NonNumericClassCode:
                                        Console.WriteLine("0 ^*^*^NCCIBodyParts-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NonNumericClassCode-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NCCIIllness:
                                        Console.WriteLine("0 ^*^*^NCCICause-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCIInjury/Illness-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NonNumericClassCode:
                                        Console.WriteLine("0 ^*^*^NCCICause-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NonNumericClassCode-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIIllness + (int)FROIFails.FF_NonNumericClassCode:
                                        Console.WriteLine("0 ^*^*^NCCIInjury/Illness-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NonNumericClassCode-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NCCIIllness:
                                        Console.WriteLine("0 ^*^*^NCCIBodyParts-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCICause-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCIInjury/Illness-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCIIllness + (int)FROIFails.FF_NonNumericClassCode:
                                        Console.WriteLine("0 ^*^*^NCCIBodyParts-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCIInjury/Illness-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NonNumericClassCode-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NCCIIllness + (int)FROIFails.FF_NonNumericClassCode:
                                        Console.WriteLine("0 ^*^*^NCCICause-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCIInjury/Illness-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NonNumericClassCode-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NCCIIllness + (int)FROIFails.FF_NonNumericClassCode:
                                        Console.WriteLine("0 ^*^*^NCCIBodyParts-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCICause-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCIInjury/Illness-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIInjury/Illness", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NonNumericClassCode-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIBodyParts + (int)FROIFails.FF_NCCICauseCode + (int)FROIFails.FF_NonNumericClassCode:
                                        Console.WriteLine("0 ^*^*^NCCIBodyParts-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIBodyParts", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NCCICause-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCICause", m_iClientId));
                                        Console.WriteLine("0 ^*^*^NonNumericClassCode-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NonNumericClassCode", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NCCIClassCode:
                                        Console.WriteLine("0 ^*^*^NCCIClass-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NCCIClass", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NoDepartmentAssigned:
                                        Console.WriteLine("0 ^*^*^NoDepartmentAssigned-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NoDepartmentAssigned", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_NoClaimant:
                                        Console.WriteLine("0 ^*^*^NoClaimant-" + Globalization.GetString("FROIAdaptor.InvokeFROI.NoClaimant", m_iClientId));
                                        break;
                                    case (int)FROIFails.FF_LoadFROIOptionsFailed:
                                        Console.WriteLine("0 ^*^*^LoadFROIOptionsFailed-" + Globalization.GetString("FROIAdaptor.InvokeFROI.LoadFROIOptionsFailed", m_iClientId));
                                        break;
                                }

                                CancelUpdatePrintedStatus();
                                Console.WriteLine("0 ^*^*^ Merging of FROIs pdf-fdf(" + iMergeCount + ")-completed with error for claim number=" + sClaimNumber + " and  jurisdiction=" + sStateId);
                                break;
                            }
                            else
                            {
                                Console.WriteLine("0 ^*^*^ Merging of FROIs pdf-fdf(" + iMergeCount + ")-started");
                                sPdfCreated = MergeFdfPdf(sFilePath);
                                Console.WriteLine("0 ^*^*^ Merging of FROIs pdf-fdf(" + iMergeCount + ")-completed");
                                Console.WriteLine("0 ^*^*^ Append to FROIs pdf-pdf(" + iMergeCount + ")-started");
                                sFinalPdf = AppendTwoPDF(sPdfCreated, iMergeCount);
                                Console.WriteLine("0 ^*^*^ Append to FROIs pdf-pdf(" + iMergeCount + ")-completed");
                                UpdatePrintedStatus();
                            }
                            //Deb:MITS 25330,End
                        }
                        //UpdatePrintedStatus();
                    }
                    if (sFinalPdf != null && sFinalPdf.Trim() != "" && !bParse)//Deb:MITS 25330
                    {
                        CopyFinalFileToDir(sFinalPdf, -1);//-1 for froi and 0 for acord
                    }
                }
                
                // Delete the file
                if (iRowcount != 0 && sFilePath != null && sFilePath.Trim() != "")
                {
                    File.Delete(sFilePath);
                }

                if (iRowcount != 0)
                {
                    CreateAndDeleteFroiTempDir(false);//true for create directory,false to delete directory
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                if (objFroi != null)
                {
                    objFroi = null;
                }
            }

            return true;
        }

        /// <summary>
        /// Cancels the update printed status.
        /// </summary>
        private static void CancelUpdatePrintedStatus()
        {
            string sSQL = string.Empty;
            try
            {
                m_objDbConnection = DbFactory.GetDbConnection(m_sDbConnstring);
                m_objDbConnection.Open();
                sSQL = "UPDATE BATCH_PRINT SET PRINTED_STATUS=0 WHERE BATCH_ID= " + m_iBatchId;
                m_objDbConnection.ExecuteNonQuery(sSQL);
                sSQL = "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + m_iClaimId;
                using (DbReader objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null && objDbReader.Read())
                    {
                        sClaimNumber = objDbReader.GetString("CLAIM_NUMBER");
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                if (m_objDbConnection != null)
                {
                    m_objDbConnection.Dispose();
                }
            }
        }

        /// <summary>
        /// Processes the acord.
        /// </summary>
        /// <returns></returns>
        private static bool ProcessAcord()
        {
            XmlDocument p_objXmlOut = null;
            // akaushik5 Chaged for JIRA 3631 Starts
            ////FROINet6.CFROIFormsClass objFroi = null;
            FROIForms objFroi = null;
            // akaushik5 Chaged for JIRA 3631 Ends
            BusinessAdaptorErrors p_objErrOut = null;
            SysSettings objSettings = null;
            string sFilePath = string.Empty;
            string sRequestHost = string.Empty;
            int iFormCount = 0;
            string sPdfPath = string.Empty;
            string sFdfPath = string.Empty;
            string sPdfMergedPath = string.Empty;
            int iRowcount = 0;
            string sPdfCreated = string.Empty;
            int iMergeCount = 0;
            string sFinalPdf = string.Empty;
            try
            {

                BatchFroiAndAcord.Program objBatch = new Program();
                objFroi = InitializeFROI(ref p_objXmlOut, ref p_objErrOut);

                sRequestHost = "http://localhost/RiskmasterUI";

                objFroi.PDFPath = m_strPDFTemplatePath + "ClaimForms\\AC\\";
                objFroi.PDFUrl = AppendSlash(sRequestHost).Replace(":443/", "/").Replace(":80/", "/") + AppendSlash(m_strPDFUrlPath);
                objFroi.OutputPath = m_strPDFOutputPath;

                if (AppGlobals.Userlogin.DocumentPath.Length > 0)
                {
                    objFroi.DocumentPath = AppGlobals.Userlogin.DocumentPath;
                }
                else
                {
                    objFroi.DocumentPath = AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath;
                }

                objFroi.AttachForm = true;

                iRowcount = getAccordRowCount();

                for (int iOuterCnt = 0; iOuterCnt < iRowcount; iOuterCnt++)
                {
                    iMergeCount = 0;
                    iFormCount = getAccordFormCount();

                    if (iFormCount > 0)
                    {
                        CreateAndDeleteAcordTempDir(true);//true for create ,false for delete
                    }

                    for (int iInnerCount = 0; iInnerCount < iFormCount; iInnerCount++)
                    {
                        GetAcordForms();
                        objSettings = new SysSettings(m_sDbConnstring, m_iClientId);
                        
                        if (objFroi.AttachForm && objSettings.UseAcrosoftInterface)
                        {
                            objFroi.AttachForm = false;
                            objFroi.DocumentPath = "";
                            // sFilePath = objFroi.InvokeFROI(m_iClaimId, m_iFormId);
                            sFilePath = objFroi.InvokeAcordForms(m_iAcordClaimId, m_iAcordFormId);
                            objFroi.AttachForm = true;

                        } // if
                        else
                        {
                            sFilePath = objFroi.InvokeAcordForms(m_iAcordClaimId, m_iAcordFormId);
                        } // else

                        if (sFilePath != null)
                        {
                            iMergeCount = iMergeCount + 1;
                            Console.WriteLine("0 ^*^*^ Merging of ACORDs pdf-fdf(" + iMergeCount + ")-started");
                            sPdfCreated = MergeAcordFdfPdf(sFilePath);
                            Console.WriteLine("0 ^*^*^ Merging of ACORDs pdf-fdf(" + iMergeCount + ")-completed");
                            Console.WriteLine("0 ^*^*^ Append to ACORDs pdf-pdf(" + iMergeCount + ")-started");
                            sFinalPdf = AppendTwoAcordPDF(sPdfCreated, iMergeCount);
                            Console.WriteLine("0 ^*^*^ Append to ACORDs pdf-pdf(" + iMergeCount + ")-completed");
                            //UpdateAcordPrintedStatus();
                        }
                        UpdateAcordPrintedStatus();
                    }
                    CopyFinalFileToDir(sFinalPdf, 0);//-1 for froi and 0 for acord                    
                }

                // Delete the file
                if (iRowcount != 0 && sFilePath != null && sFilePath.Trim() != "")
                {
                    File.Delete(sFilePath);
                }

                if (iRowcount != 0)
                {
                    CreateAndDeleteAcordTempDir(false);//true for create ,false for delete
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            return true;
        }

        /// <summary>
        /// Creates a memory stream of given file
        /// </summary>
        /// <param name="p_sFilePath">Complete File Path</param>
        /// <returns>Memory stream of the given file</returns>
        public static MemoryStream CreateStream(string p_sFilePath)
        {
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            BinaryReader objBinaryReader = null;
            Byte[] arrByte = null;

            try
            {
                if (!File.Exists(p_sFilePath))
                {
                    throw new FileNotFoundException(Globalization.GetString("Function.FileNotFound.Error", m_iClientId));
                }

                objFileStream = new FileStream(p_sFilePath, FileMode.Open, FileAccess.Read);
                objMemoryStream = new MemoryStream((int)objFileStream.Length);
                objBinaryReader = new BinaryReader(objFileStream);
                arrByte = objBinaryReader.ReadBytes((int)objFileStream.Length);
                objMemoryStream.Write(arrByte, 0, (int)objFileStream.Length);
                return (objMemoryStream);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.CreateStream.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objFileStream != null)
                {
                    objFileStream.Close();
                    objFileStream.Dispose();
                }
                if (objBinaryReader != null)
                {
                    objBinaryReader.Close();
                    objBinaryReader = null;
                }
                arrByte = null;
                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    objMemoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Copies the final file to dir.
        /// </summary>
        /// <param name="sSourceDir">The s source dir.</param>
        /// <param name="iType">Type of the i.</param>
        /// <exception cref="System.IO.FileNotFoundException"></exception>
        public static void CopyFinalFileToDir(string sSourceDir, int iType)
        {
            string sName = string.Empty;
            string sDestName = string.Empty;
            string sGlobalDocPath = string.Empty;
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            FileStorageManager objmanager = null;
            string sRMXUserName = string.Empty;
            string sDSNName = string.Empty;
            try
            {
                if (sSourceDir != null && sSourceDir.Trim() != "")
                {
                    sName = sSourceDir.Substring((sSourceDir.LastIndexOf("\\") + 1));
                    sName = sName.Replace(".pdf", "_.pdf");
                    sDestName = m_strPDFOutputPath + "\\" + sName;

                    objmanager = new FileStorageManager((StorageType)AppGlobals.Userlogin.objRiskmasterDatabase.DocPathType, AppGlobals.Userlogin.objRiskmasterDatabase.GlobalDocPath, m_iClientId);
                    System.IO.File.Copy(sSourceDir, sDestName, true);

                    if (!File.Exists(sDestName))
                        throw new FileNotFoundException(Globalization.GetString("Function.FileNotFound.Error", m_iClientId));

                    objFileStream = File.Open(sDestName, FileMode.Open);

                    DataStreamingService.DataStreamingServiceClient rmservice = null;
                    rmservice = new DataStreamingService.DataStreamingServiceClient();
                    //DataStreamingService.StreamedDocumentType document = null;
                    //document = new DataStreamingService.StreamedDocumentType();

                    RMServiceType oDocumentType = new RMServiceType();

                    oDocumentType.ClientId = m_iClientId;

                    DataStreamingService.StreamedDocumentType oStreamedDocumentType = null;
                    oStreamedDocumentType = new DataStreamingService.StreamedDocumentType();

                    //AuthenticationService 
                    AuthenticationServiceReference.AuthenticationServiceClient auService = null;
                    auService = new AuthenticationServiceReference.AuthenticationServiceClient();
                    AuthData oAuthData = new AuthData();

                    oAuthData.ClientId = m_iClientId;
                    oAuthData.UserName = m_sLoginName;
                    oAuthData.DsnName = m_sDataSource;
                    sRMXUserName = AppGlobals.Userlogin.LoginName;
                    sDSNName = AppGlobals.Userlogin.objRiskmasterDatabase.DataSourceName;

                    sRMXUserName = m_sLoginName;
                    sDSNName = m_sDataSource;

                    oStreamedDocumentType.Class = new DataStreamingService.StreamedCodeType();
                    oStreamedDocumentType.Type = new DataStreamingService.StreamedCodeType();
                    oStreamedDocumentType.Category = new DataStreamingService.StreamedCodeType();
                    oStreamedDocumentType.Token = auService.GetUserSessionID(sRMXUserName, sDSNName);
                   // oStreamedDocumentType.Token = AppGlobals.GetResponse<string>("RMService/Authenticate/session", AppGlobals.HttpVerb.POST, "application/json", oAuthData);
                    oStreamedDocumentType.ClientId = m_iClientId;

                    if (iType == -1)
                    {
                        oStreamedDocumentType.Title = "FROI";
                        oStreamedDocumentType.FolderId = "FROI FORM";
                        oStreamedDocumentType.Subject = "FROI SUBJECT";
                        oStreamedDocumentType.AttachTable = "BATCH_PRINT";
                    }
                    else
                    {
                        oStreamedDocumentType.Title = "ACORD";
                        oStreamedDocumentType.FolderId = "ACORD FORM";
                        oStreamedDocumentType.Subject = "ACORD SUBJECT";
                        oStreamedDocumentType.AttachTable = "BATCH_PRINT_ACORD";
                    }

                    oStreamedDocumentType.PsId = 3000;
                    oStreamedDocumentType.FilePath = string.Empty;
                    // document.AttachTable = "claim";
                    oStreamedDocumentType.AttachRecordId = -3;
                    oStreamedDocumentType.Notes = string.Empty;
                    oStreamedDocumentType.Keywords = string.Empty;
                    //document.FileName = sDocDetails["FileName"];
                    oStreamedDocumentType.FileName = sName;
                    oStreamedDocumentType.fileStream = objFileStream;
                    //oStreamedDocumentType.fileStream = objFileStream;
                    //byte[] file = new byte[objFileStream.Length];
                    //int len = Convert.ToInt32(objFileStream.Length);
                    //objFileStream.Read(file, 0, len);
                    //oStreamedDocumentType.FileContents = file;
                    oStreamedDocumentType.FileSize = objFileStream.Length.ToString();
                    //Add by kuladeep for mits:27740
                    //oStreamedDocumentType.FileSize = Convert.ToString(objFileStream.Length);
                    //AppGlobals.GetResponse<RMServiceType>("RMService/DataStream/create", AppGlobals.HttpVerb.POST, "application/json", oStreamedDocumentType);
                    rmservice.CreateDocument(oStreamedDocumentType.AttachRecordId,
                        oStreamedDocumentType.AttachTable,
                        oStreamedDocumentType.Category,
                        oStreamedDocumentType.Claimnumber,
                        oStreamedDocumentType.Class,
                        oStreamedDocumentType.ClientId,
                        oStreamedDocumentType.Copy,
                        oStreamedDocumentType.Create,
                        oStreamedDocumentType.CreateDate,
                        oStreamedDocumentType.Delete,
                        oStreamedDocumentType.DocInternalType,
                        oStreamedDocumentType.DocumentCategory,
                        oStreamedDocumentType.DocumentClass,
                        oStreamedDocumentType.DocumentId,
                        oStreamedDocumentType.DocumentsType,
                        oStreamedDocumentType.Download,
                        oStreamedDocumentType.Edit,
                        oStreamedDocumentType.Email,
                        oStreamedDocumentType.Errors,
                        oStreamedDocumentType.FileContents,
                        oStreamedDocumentType.FileName,
                        oStreamedDocumentType.FilePath,
                        oStreamedDocumentType.FileSize,
                        oStreamedDocumentType.FolderId,
                        oStreamedDocumentType.FolderName,
                        oStreamedDocumentType.FormName,
                        oStreamedDocumentType.Keywords,
                        oStreamedDocumentType.Move,
                        oStreamedDocumentType.Notes,
                        oStreamedDocumentType.Pid,
                        oStreamedDocumentType.PsId,
                        oStreamedDocumentType.Readonly,
                        oStreamedDocumentType.ScreenFlag,
                        oStreamedDocumentType.Subject,
                        oStreamedDocumentType.Title,
                        oStreamedDocumentType.Token,
                        oStreamedDocumentType.Transfer,
                        oStreamedDocumentType.Type,
                        oStreamedDocumentType.UserId,
                        oStreamedDocumentType.UserName,
                        oStreamedDocumentType.View,
                        oStreamedDocumentType.fileStream);
                    objFileStream.Close();
                    System.IO.File.Delete(sDestName);
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                if (objFileStream != null)
                {
                    objFileStream.Close();
                    objFileStream.Dispose();
                }

                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    objMemoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Lock_objectses the specified filenameandpath.
        /// </summary>
        /// <param name="filenameandpath">The filenameandpath.</param>
        /// <param name="name">The name.</param>
        public static void Lock_objects(string filenameandpath, string name)
        {
            FileStream file1 = new FileStream(filenameandpath, FileMode.Open, FileAccess.Read);
            IacDocument PdfDoc = new IacDocument(null);
            PdfDoc.Open(file1, "");
            //need to loop through all the field objects and make them non editable.
            try
            {
                for (int i = 1; i <= PdfDoc.PageCount; i++)
                {
                    PdfDoc.CurrentPage = PdfDoc.GetPage(i);
                    //Declare ArrayList and fill it with all of the objects in the 
                    //current PDF document
                    ArrayList arList = (ArrayList)PdfDoc.GetPage(i).Attribute("Objects").Value;
                    foreach (IacObject obj in arList)
                    {
                        //MGaba2:MITS 27740
                        //  obj.Attribute("Annotation").Value = false;
                        obj.Annotation = false;
                    }
                }

                // Open the PDF document file.
                string sFilePathAndNameSave = name;
                //save doc to new file
                FileStream fs2 = new FileStream(sFilePathAndNameSave, FileMode.Create, FileAccess.Write, FileShare.Read);
                PdfDoc.Save(fs2, IacFileSaveOption.acFileSaveAll);
                fs2.Close();
                file1.Dispose();
            }
            catch (Exception exc)
            {
                throw (exc);
            }
        }

        /// <summary>
        /// Lock_s the acord objects.
        /// </summary>
        /// <param name="filenameandpath">The filenameandpath.</param>
        /// <param name="name">The name.</param>
        public static void Lock_AcordObjects(string filenameandpath, string name)
        {
            FileStream file1 = new FileStream(filenameandpath, FileMode.Open, FileAccess.Read);
            IacDocument PdfDoc = new IacDocument(null);
            PdfDoc.Open(file1, "");
            //need to loop through all the field objects and make them non editable.
            try
            {
                for (int i = 1; i <= PdfDoc.PageCount; i++)
                {
                    PdfDoc.CurrentPage = PdfDoc.GetPage(i);
                    //Declare ArrayList and fill it with all of the objects in the 
                    //current PDF document
                    ArrayList arList = (ArrayList)PdfDoc.GetPage(i).Attribute("Objects").Value;
                    foreach (IacObject obj in arList)
                    {
                        //MGaba2:MITS 27740
                        //  obj.Attribute("Annotation").Value = false;
                        obj.Annotation = false;
                    }
                }

                // Open the PDF document file.
                string sFilePathAndNameSave = name;
                //save doc to new file
                System.IO.FileStream fs2 = new System.IO.FileStream(sFilePathAndNameSave, FileMode.Create, FileAccess.Write, FileShare.Read);
                PdfDoc.Save(fs2, Amyuni.PDFCreator.IacFileSaveOption.acFileSaveAll);
                //PdfDoc.Save(fs2);
                fs2.Close();
                file1.Dispose();
                //   System.IO.File.Delete(filenameandpath);
            }
            catch (Exception exc)
            {
                throw (exc);
            }
        }

        /// <summary>
        /// Merges the FDF PDF.
        /// </summary>
        /// <param name="sFdfPath">The s FDF path.</param>
        /// <returns></returns>
        public static string MergeFdfPdf(string sFdfPath)
        {
            string sMergeFinalPDF = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;
            string sPdfPathReturn = string.Empty;
            try
            {
                m_sPdfBasePath = GetPdfPath();
                m_sPdfBasePath = m_sPdfBasePath + m_sPdfFileName;
                sPdfPathReturn = m_sFroiTempDirName + "\\" + (sFdfPath.Substring((sFdfPath.LastIndexOf("\\") + 1), (sFdfPath.Length - sFdfPath.LastIndexOf("\\") - 1))).Replace(".fdf", ".pdf");
                // sPdfPathReturn = sFdfPath.Replace(".fdf", ".pdf");
                acPDFCreatorLib.Initialize();
                acPDFCreatorLib.SetLicenseKey("CSC Financial Services Group", "07EFCDAB01000100C9D1AB2346F36D68B637892D59E2B2D416C18F0397811F5604016685DBC54D7928B23578A87E09BFEB3D9FC4FE9F");
                // Open the first PDF document from file
                System.IO.FileStream file1 = new System.IO.FileStream(m_sPdfBasePath, FileMode.Open, FileAccess.Read);
                Amyuni.PDFCreator.IacDocument PdfDoc1 = new Amyuni.PDFCreator.IacDocument(null);
                PdfDoc1.Open(file1, "");
                // Open the second fdf document from file
                System.IO.FileStream file2 = new System.IO.FileStream(sFdfPath, FileMode.Open, FileAccess.Read);
                Amyuni.PDFCreator.IacDocument PdfDoc2 = new Amyuni.PDFCreator.IacDocument(null);
                PdfDoc2.Open(file2, "");
                PdfDoc2.LockAllObjects(true);
                PdfDoc1.Merge(PdfDoc2, 1);
                Random rd = new Random();
                string sTempfilename = rd.Next().ToString();
                sTempfilename = m_sFroiTempDirName + "\\" + sTempfilename + ".pdf";
                // save the result to a third file
                System.IO.FileStream file3 = new System.IO.FileStream(sTempfilename, FileMode.Create, FileAccess.Write);
                PdfDoc1.Save(file3);
                file3.Close();
                file1.Close();
                file1.Dispose();
                file2.Close();
                file2.Dispose();
                file3.Close();
                file3.Dispose();
                PdfDoc1.LockAllObjects(true);
                PdfDoc1.Dispose();
                PdfDoc2.Dispose();
                Lock_objects(sTempfilename, sPdfPathReturn);
                //Delete the file
                System.IO.File.Delete(sFdfPath);
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                acPDFCreatorLib.Terminate();
            }
            return sPdfPathReturn;
        }

        /// <summary>
        /// Merges the acord FDF PDF.
        /// </summary>
        /// <param name="sFdfPath">The s FDF path.</param>
        /// <returns></returns>
        public static string MergeAcordFdfPdf(string sFdfPath)
        {
            string sMergeFinalPDF = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;
            string sPdfPathReturn = string.Empty;
            try
            {
                m_sAcordPdfBasePath = GetAcordPdfPath();
                m_sAcordPdfBasePath = m_sAcordPdfBasePath + m_sAcordPdfFileName;
                sPdfPathReturn = m_sAcordTempDirName + "\\" + (sFdfPath.Substring((sFdfPath.LastIndexOf("\\") + 1), (sFdfPath.Length - sFdfPath.LastIndexOf("\\") - 1))).Replace(".fdf", ".pdf");
                //sPdfPathReturn = sFdfPath.Replace(".fdf", ".pdf");
                acPDFCreatorLib.Initialize();
                acPDFCreatorLib.SetLicenseKey("CSC Financial Services Group", "07EFCDAB01000100C9D1AB2346F36D68B637892D59E2B2D416C18F0397811F5604016685DBC54D7928B23578A87E09BFEB3D9FC4FE9F");
                // Open the first PDF document from file
                System.IO.FileStream file1 = new System.IO.FileStream(m_sAcordPdfBasePath, FileMode.Open, FileAccess.Read);
                Amyuni.PDFCreator.IacDocument PdfDoc1 = new Amyuni.PDFCreator.IacDocument(null);
                PdfDoc1.Open(file1, "");
                // Open the second fdf document from file
                System.IO.FileStream file2 = new System.IO.FileStream(sFdfPath, FileMode.Open, FileAccess.Read);
                Amyuni.PDFCreator.IacDocument PdfDoc2 = new Amyuni.PDFCreator.IacDocument(null);
                PdfDoc2.Open(file2, "");
                PdfDoc2.LockAllObjects(true);
                PdfDoc1.Merge(PdfDoc2, 1);
                Random rd = new Random();
                string sTempfilename = rd.Next().ToString();
                // sTempfilename = m_strPDFOutputPath + "\\" + sTempfilename + ".pdf";
                sTempfilename = m_sAcordTempDirName + "\\" + sTempfilename + ".pdf";
                // save the result to a third file
                System.IO.FileStream file3 = new System.IO.FileStream(sTempfilename, FileMode.Create, FileAccess.Write);
                PdfDoc1.Save(file3);
                file3.Close();
                file1.Close();
                file1.Dispose();
                file2.Close();
                file2.Dispose();
                file3.Close();
                file3.Dispose();
                PdfDoc1.LockAllObjects(true);
                PdfDoc1.Dispose();
                PdfDoc2.Dispose();
                Lock_AcordObjects(sTempfilename, sPdfPathReturn);
                //Delete the file
                System.IO.File.Delete(sFdfPath);
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                acPDFCreatorLib.Terminate();
            }
            return sPdfPathReturn;
        }

        /// <summary>
        /// Appends the two PDF.
        /// </summary>
        /// <param name="sPdfCreated">The s PDF created.</param>
        /// <param name="iMergeCount">The i merge count.</param>
        /// <returns></returns>
        public static string AppendTwoPDF(string sPdfCreated, int iMergeCount)
        {
            string sMergeFinalPDF = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;
            string sFinalPath = string.Empty;
            string sPreviousPath = string.Empty;
            try
            {
                //
                acPDFCreatorLib.Initialize();
                acPDFCreatorLib.SetLicenseKey("CSC Financial Services Group", "07EFCDAB01000100C9D1AB2346F36D68B637892D59E2B2D416C18F0397811F5604016685DBC54D7928B23578A87E09BFEB3D9FC4FE9F");
                if (iMergeCount > 1)
                {
                    // Open the first PDF document from file
                    sFinalPath = GetFroiPdfName(iMergeCount, sPdfCreated);
                    sPreviousPath = GetFroiPdfNamPrevious(iMergeCount, sPdfCreated);
                    System.IO.FileStream file1 = new System.IO.FileStream(sPdfCreated, FileMode.Open, FileAccess.Read);
                    Amyuni.PDFCreator.IacDocument PdfDoc1 = new Amyuni.PDFCreator.IacDocument(null);
                    PdfDoc1.Open(file1, "");
                    // Open the second fdf document from file
                    System.IO.FileStream file2 = new System.IO.FileStream(sPreviousPath, FileMode.Open, FileAccess.Read);
                    Amyuni.PDFCreator.IacDocument PdfDoc2 = new Amyuni.PDFCreator.IacDocument(null);
                    PdfDoc2.Open(file2, "");
                    // PdfDoc1.LockAllObjects(true);
                    PdfDoc1.Append(PdfDoc2);
                    System.IO.FileStream file3 = new System.IO.FileStream(sFinalPath, FileMode.Create, FileAccess.Write);
                    PdfDoc1.Save(file3);
                    // save the result to a third file                    
                    file2.Close();
                    file2.Dispose();
                    PdfDoc2.Dispose();
                    file1.Close();
                    file1.Dispose();
                    file2.Close();
                    file2.Dispose();
                    file3.Close();
                    file3.Dispose();
                    // file3.Dispose();
                    PdfDoc1.Dispose();
                    PdfDoc2.Dispose();
                }
                else
                {
                    // Open the first PDF document from file
                    sFinalPath = GetFroiPdfName(iMergeCount, sPdfCreated);
                    System.IO.FileStream file1 = new System.IO.FileStream(sPdfCreated, FileMode.Open, FileAccess.Read);
                    Amyuni.PDFCreator.IacDocument PdfDoc1 = new Amyuni.PDFCreator.IacDocument(null);
                    PdfDoc1.Open(file1, "");
                    // save the result to a third file
                    System.IO.FileStream file3 = new System.IO.FileStream(sFinalPath, FileMode.Create, FileAccess.Write);
                    PdfDoc1.Save(file3);
                    file1.Close();
                    file3.Close();
                    file1.Dispose();
                    file3.Dispose();
                    PdfDoc1.Dispose();
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                acPDFCreatorLib.Terminate();
            }
            return sFinalPath;
        }

        /// <summary>
        /// Appends the two acord PDF.
        /// </summary>
        /// <param name="sPdfCreated">The s PDF created.</param>
        /// <param name="iMergeCount">The i merge count.</param>
        /// <returns></returns>
        public static string AppendTwoAcordPDF(string sPdfCreated, int iMergeCount)
        {
            string sMergeFinalPDF = string.Empty;
            string sTemp = string.Empty;
            string sFdfPathTemp = string.Empty;
            string sFinalPath = string.Empty;
            string sPreviousPath = string.Empty;
            try
            {
                //
                acPDFCreatorLib.Initialize();
                acPDFCreatorLib.SetLicenseKey("CSC Financial Services Group", "07EFCDAB01000100C9D1AB2346F36D68B637892D59E2B2D416C18F0397811F5604016685DBC54D7928B23578A87E09BFEB3D9FC4FE9F");
                if (iMergeCount > 1)
                {
                    // Open the first PDF document from file
                    sFinalPath = GetAcordPdfName(iMergeCount, sPdfCreated);
                    sPreviousPath = GetAcordPdfNamPrevious(iMergeCount, sPdfCreated);
                    System.IO.FileStream file1 = new System.IO.FileStream(sPdfCreated, FileMode.Open, FileAccess.Read);
                    Amyuni.PDFCreator.IacDocument PdfDoc1 = new Amyuni.PDFCreator.IacDocument(null);
                    PdfDoc1.Open(file1, "");
                    // Open the second fdf document from file
                    System.IO.FileStream file2 = new System.IO.FileStream(sPreviousPath, FileMode.Open, FileAccess.Read);
                    Amyuni.PDFCreator.IacDocument PdfDoc2 = new Amyuni.PDFCreator.IacDocument(null);
                    PdfDoc2.Open(file2, "");
                    // PdfDoc1.LockAllObjects(true);
                    PdfDoc1.Append(PdfDoc2);
                    System.IO.FileStream file3 = new System.IO.FileStream(sFinalPath, FileMode.Create, FileAccess.Write);
                    PdfDoc1.Save(file3);
                    // save the result to a third file                    
                    file2.Close();
                    file2.Dispose();
                    PdfDoc2.Dispose();
                    file1.Close();
                    file1.Dispose();
                    file2.Close();
                    file2.Dispose();
                    file3.Close();
                    file3.Dispose();
                    // file3.Dispose();
                    PdfDoc1.Dispose();
                    PdfDoc2.Dispose();
                }
                else
                {
                    // Open the first PDF document from file
                    sFinalPath = GetAcordPdfName(iMergeCount, sPdfCreated);
                    System.IO.FileStream file1 = new System.IO.FileStream(sPdfCreated, FileMode.Open, FileAccess.Read);
                    Amyuni.PDFCreator.IacDocument PdfDoc1 = new Amyuni.PDFCreator.IacDocument(null);
                    PdfDoc1.Open(file1, "");
                    // save the result to a third file
                    System.IO.FileStream file3 = new System.IO.FileStream(sFinalPath, FileMode.Create, FileAccess.Write);
                    PdfDoc1.Save(file3);
                    file1.Close();
                    file3.Close();
                    file1.Dispose();
                    file3.Dispose();
                    PdfDoc1.Dispose();
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                acPDFCreatorLib.Terminate();
            }
            return sFinalPath;
        }

        /// <summary>
        /// Updates the printed status.
        /// </summary>
        private static void UpdatePrintedStatus()
        {
            string sSQL = string.Empty;
            try
            {
                m_objDbConnection = DbFactory.GetDbConnection(m_sDbConnstring);
                m_objDbConnection.Open();
                sSQL = "UPDATE BATCH_PRINT SET PRINTED_STATUS=-1 WHERE BATCH_ID= " + m_iBatchId + " AND  CLAIM_ID= " + m_iClaimId;
                m_objDbConnection.ExecuteNonQuery(sSQL);
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                if (m_objDbConnection != null)
                {
                    m_objDbConnection.Dispose();
                }
            }
        }

        /// <summary>
        /// Updates the acord printed status.
        /// </summary>
        private static void UpdateAcordPrintedStatus()
        {
            string sSQL = string.Empty;
            try
            {
                m_objDbConnection = DbFactory.GetDbConnection(m_sDbConnstring);
                m_objDbConnection.Open();
                sSQL = "UPDATE BATCH_PRINT_ACORD SET PRINTED_STATUS=-1 WHERE BATCH_ID= " + m_iAcordBatchId + " AND  CLAIM_ID= " + m_iAcordClaimId + " AND FORM_ID= " + m_iAcordFormId;
                m_objDbConnection.ExecuteNonQuery(sSQL);
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                if (m_objDbConnection != null)
                    m_objDbConnection.Dispose();
            }
        }

        /// <summary>
        /// Gets the claim identifier and state identifier.
        /// </summary>
        /// <param name="iClaimId">The i claim identifier.</param>
        /// <param name="sStateId">The s state identifier.</param>
        private static void GetClaimIdAndStateId(ref int iClaimId, ref  string sStateId)
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            try
            {
                sSQL = "SELECT CLAIM_ID,JURISDICTION FROM BATCH_PRINT WHERE BATCH_ID= " + m_iBatchId + " AND  PRINTED_STATUS<>-1";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            iClaimId = objDbReader.GetInt32("CLAIM_ID");
                            sStateId = objDbReader.GetString("JURISDICTION");
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
        }

        /// <summary>
        /// Gets the acord claim identifier.
        /// </summary>
        /// <param name="iClaimId">The i claim identifier.</param>
        private static void GetAcordClaimId(ref int iClaimId)
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            try
            {
                sSQL = "SELECT CLAIM_ID FROM BATCH_PRINT_ACORD WHERE BATCH_ID= " + m_iAcordBatchId + " AND  PRINTED_STATUS<>-1";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            iClaimId = objDbReader.GetInt32("CLAIM_ID");
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
        }

        /// <summary>
        /// Gets the forms.
        /// </summary>
        private static void GetForms()
        {
            try
            {
                // m_iBatchId = getMaxBatchId();
                GetClaimIdAndStateId(ref m_iClaimId, ref sStateId);
                m_iFormId = GetFormIdAndPdfFileName(sStateId);
            }
            catch (Exception exc)
            {
                throw (exc);
            }
        }

        /// <summary>
        /// Gets the acord forms.
        /// </summary>
        private static void GetAcordForms()
        {
            try
            {
                GetAcordClaimId(ref m_iAcordClaimId);
                m_iAcordFormId = GetAcordFormId();
                m_sAcordPdfFileName = GetAcordPdf();
            }
            catch (Exception exc)
            {
                throw (exc);
            }
        }

        /// <summary>
        /// Gets the maximum batch identifier.
        /// </summary>
        /// <returns></returns>
        private static int getMaxBatchId()
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sStateId = string.Empty;
            try
            {
                sSQL = "SELECT MAX(BATCH_ID) FROM BATCH_PRINT WHERE PRINTED_STATUS <>-1";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            m_iBatchId = Convert.ToInt32(objDbReader[0].ToString());
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }

            return m_iBatchId;
        }

        /// <summary>
        /// Gets the form count.
        /// </summary>
        /// <returns></returns>
        private static int getFormCount()
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sStateId = string.Empty;
            int iFormCount = 0;
            try
            {
                sSQL = "SELECT COUNT(*),BATCH_ID FROM  BATCH_PRINT WHERE PRINTED_STATUS<>-1 GROUP BY BATCH_ID";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            iFormCount = Convert.ToInt32(objDbReader[0].ToString());
                            m_iBatchId = Convert.ToInt32(objDbReader[1].ToString());
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }

            return iFormCount;
        }

        /// <summary>
        /// Gets the accord form count.
        /// </summary>
        /// <returns></returns>
        private static int getAccordFormCount()
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sStateId = string.Empty;
            int iFormCount = 0;
            try
            {
                sSQL = "SELECT COUNT(*),BATCH_ID FROM  BATCH_PRINT_ACORD WHERE PRINTED_STATUS<>-1 GROUP BY BATCH_ID";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            iFormCount = Convert.ToInt32(objDbReader[0].ToString());
                            m_iAcordBatchId = Convert.ToInt32(objDbReader[1].ToString());
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }

            return iFormCount;
        }

        /// <summary>
        /// Gets the row count.
        /// </summary>
        /// <returns></returns>
        private static int getRowCount()
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            int iRowCount = 0;
            try
            {
                sSQL = "SELECT COUNT(*),BATCH_ID FROM  BATCH_PRINT WHERE PRINTED_STATUS<>-1 GROUP BY BATCH_ID";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            iRowCount = iRowCount + 1;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            return iRowCount;
        }

        /// <summary>
        /// Gets the accord row count.
        /// </summary>
        /// <returns></returns>
        private static int getAccordRowCount()
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            int iRowCount = 0;
            try
            {
                sSQL = "SELECT COUNT(*),BATCH_ID FROM  BATCH_PRINT_ACORD WHERE PRINTED_STATUS<>-1 GROUP BY BATCH_ID";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            iRowCount = iRowCount + 1;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            return iRowCount;
        }

        /// <summary>
        /// Gets the acord form identifier.
        /// </summary>
        /// <returns></returns>
        private static int GetAcordFormId()
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            try
            {
                sSQL = "SELECT FORM_ID FROM BATCH_PRINT_ACORD WHERE BATCH_ID=" + m_iAcordBatchId + " AND CLAIM_ID=" + m_iAcordClaimId + " AND PRINTED_STATUS<>-1";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            m_iAcordFormId = Convert.ToInt32(objDbReader[0].ToString());
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            return m_iAcordFormId;
        }

        /// <summary>
        /// Gets the acord PDF.
        /// </summary>
        /// <returns></returns>
        private static string GetAcordPdf()
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            try
            {
                sSQL = "SELECT FILE_NAME FROM CL_FORMS WHERE FORM_ID=" + m_iAcordFormId;
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            m_sAcordPdfFileName = objDbReader.GetString("FILE_NAME");
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            return m_sAcordPdfFileName;
        }

        /// <summary>
        /// Gets the name of the form identifier and PDF file.
        /// </summary>
        /// <param name="sStateId">The s state identifier.</param>
        /// <returns></returns>
        private static int GetFormIdAndPdfFileName(string sStateId)
        {
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            int m_iFormId = 0;
            try
            {
                sSQL = "SELECT FORM_ID,PDF_FILE_NAME FROM JURIS_FORMS,STATES WHERE JURIS_FORMS.STATE_ROW_ID = STATES.STATE_ROW_ID AND STATES.STATE_ID = '" + sStateId + "' AND PRIMARY_FORM_FLAG = -1";
                using (objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            m_iFormId = objDbReader.GetInt("FORM_ID");
                            m_sPdfFileName = objDbReader.GetString("PDF_FILE_NAME");
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw (exc);
            }
            return m_iFormId;
        }
    }
}
