using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Xml;

using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Security;

namespace Riskmaster.Tools.PvUpgrade
{
	/// <summary>
	/// Summary description for powerView.
	/// </summary>
	public class PowerView
    {
        #region declare class objects

        const string XHTML_NAMESPACE = "http://www.w3.org/1999/xhtml";
        const string MESSAGE_TYPE = "message";
        const string PREFIX_NAME = "RMX UPG of ";

        bool m_IsRMNetView = true;

        int m_iDSNId = 0;
        int m_ViewID = 0;
        int m_NewViewID = 0;

        string m_DSN = String.Empty;
        string m_User = String.Empty;
        string m_Password = String.Empty;
        string m_ViewName = String.Empty;
		string m_ViewDesc = String.Empty;
		string m_ViewHome = String.Empty;
		string m_PageMenu = String.Empty;
        string m_NewViewHome = String.Empty;
        string m_ConnectionString = String.Empty;
        string m_RMXPageConnectionString = String.Empty;
		
		ArrayList m_oFormList = null;
		DataSet m_DSBaseView = null;
		XmlDocument m_ControlMapDoc = null;
		XmlDocument m_FormListDoc = null;
        TextWriterTraceListener m_TraceListener = null;
		TraceSwitch m_TraceSwitch = null;

        /// <summary>
        /// FormCount
        /// </summary>
        public int FormCount
        {
            get { return m_oFormList.Count; }
        }

        /// <summary>
        /// ViewName
        /// </summary>
        public string ViewName
        {
            get { return m_ViewName; }
        }

        /// <summary>
        /// DSNID
        /// </summary>
        public int DSNID
        {
            set
            {
                m_iDSNId = value;
            }
        }

        /// <summary>
        /// IsRMNetView
        /// </summary>
        public bool IsRMNetView
        {
            get { return m_IsRMNetView; }
            set { m_IsRMNetView = value; }
        }

        #endregion

        /// <summary>
        /// PowerView
        /// </summary>
        /// <param name="oTraceListener"></param>
        /// <param name="oTraceSwitch"></param>
		public PowerView(TextWriterTraceListener oTraceListener, TraceSwitch oTraceSwitch)
		{
			m_oFormList = new ArrayList();
			m_TraceListener = oTraceListener;
			m_TraceSwitch = oTraceSwitch;
		}

		/// <summary>
		/// Initialize the power view object
		/// </summary>
		/// <param name="iViewID"></param>
		/// <param name="sViewName"></param>
		/// <param name="sDSN"></param>
		/// <param name="sUser"></param>
		/// <param name="sPassword"></param>
		/// <param name="sConnectionString"></param>
		/// <param name="oFormListDoc"></param>
		/// <param name="oControlMapDoc"></param>
		/// <param name="oDSBaseView"></param>
		/// <param name="sViewHome"></param>
		/// <param name="sViewDesc"></param>
		/// <param name="sPageMenu"></param>
		public void Initialization(int iViewID, string sViewName, string sDSN, string sUser, string sPassword, string sConnectionString, XmlDocument oFormListDoc, XmlDocument oControlMapDoc, DataSet oDSBaseView, string sViewHome, string sViewDesc, string sPageMenu)
		{
			m_ViewID = iViewID;
			m_ViewName = sViewName;
			m_ViewHome = sViewHome;
			m_ViewDesc = sViewDesc;
			m_PageMenu = sPageMenu;
			m_DSN = sDSN; 
			m_User = sUser;
			m_Password = sPassword;
			m_ConnectionString = sConnectionString;
			m_FormListDoc = oFormListDoc;
			m_ControlMapDoc = oControlMapDoc;
			m_DSBaseView = oDSBaseView;
		}

		/// <summary>
		/// Get powerview compatable form xml definition
		/// </summary>
		public void GetExistingFormXMLs(string m_iDSNId)
		{
            int iFormCount = 0;
            string sSQL = String.Empty;
            string sTest = String.Empty;
            string sOldFormXml = String.Empty;
            string sIgnoreChangeFile = String.Empty;

			DbReader oReader = null;
            FormBase oForm = null;
            ArrayList alIgnoreFormNames = new ArrayList();
            
            try
			{				
                //Modified by Navdeep - added DSN Id to query
                m_RMXPageConnectionString = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;                
                sSQL = String.Format("SELECT * FROM NET_VIEW_FORMS WHERE VIEW_ID='{0}' AND DATA_SOURCE_ID = {1}", m_ViewID, m_iDSNId);                
                //oReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);
                //NAVDEEP CHANGED THE CONNECTION STRING
                oReader = DbFactory.GetDbReader(m_RMXPageConnectionString, sSQL);
                sIgnoreChangeFile = String.Format(@"{0}\Ignore_Change.xml", AppDomain.CurrentDomain.BaseDirectory);
                GetIgnoreFormName(sIgnoreChangeFile, ref alIgnoreFormNames);

				while(oReader.Read())
				{
                    try
                    {
                        string sFormName = oReader.GetString("FORM_NAME");

                        if (alIgnoreFormNames.Contains(sFormName))
                        {
                            continue;
                        }

                        iFormCount++;
                        sOldFormXml = oReader["VIEW_XML"].ToString();

                        //Raman 06/19/2009 : We do not need to create duplicate view for RMNet now as our page database is different
                        //We would be upgrading the existing view id for RMNet too

                        //GetNewViewID(sOldFormXml , m_ViewID);
                        m_NewViewID = m_ViewID;

                        if (m_IsRMNetView)
                            oForm = new RMNetForm(this, m_FormListDoc, m_TraceListener, m_TraceSwitch);
                        else
                            oForm = new RMXForm(this, m_FormListDoc, m_TraceListener, m_TraceSwitch);

                        oForm.OldFormXml = sOldFormXml;
                        oForm.ViewID = m_NewViewID;
                        oForm.ViewName = m_ViewName;
                        oForm.IsRMNetView = m_IsRMNetView;
                        oForm.FormName = oReader["FORM_NAME"].ToString();

                        //non-powerview form's xml definition could get 
                        //from default base view
                        if (oForm.IsPowerviewForm || oForm.IsAdminTrackingForm)
                        {
                            oForm.TopLevel = int.Parse(oReader["TOPLEVEL"].ToString());
                            oForm.Caption = oReader["CAPTION"].ToString();
                            oForm.DSBaseView = m_DSBaseView;
                            oForm.ControlMapDoc = m_ControlMapDoc;
                            oForm.DSN = m_DSN;
                            oForm.User = m_User;
                            oForm.Password = m_Password;
                            oForm.ConnectionString = m_ConnectionString;
                            m_oFormList.Add(oForm);
                        }
                    }
                    catch (Exception)
                    {
                    }
				}
				
				//If there is no Forms in the view, assume it's a RMX view
				if( iFormCount == 0 )
				{                    
                    m_NewViewID = Utilities.GetNextUID(m_ConnectionString, "NET_VIEWS",0);                                 
					m_IsRMNetView = true;
				}
			}
			catch(Exception)
			{
			}
			finally
			{
				if (oReader != null)
					oReader.Close();
			}
		}

        /// <summary>
        /// GetIgnoreFormName
        /// </summary>
        /// <param name="p_sIgnoreChangeFile"></param>
        /// <param name="p_alIgnoreForms"></param>
        private void GetIgnoreFormName(string p_sIgnoreChangeFile, ref ArrayList p_alIgnoreForms)
        {
            XmlDocument objIgnoreChange = new XmlDocument();
            XmlNodeList objIgnoreList = null;
            objIgnoreChange.Load(p_sIgnoreChangeFile);
            objIgnoreList = objIgnoreChange.SelectNodes("/Ignore_Change/Ignore/formname");

            foreach (XmlNode objIgnore in objIgnoreList)
            {
                p_alIgnoreForms.Add(objIgnore.InnerText);
            }
        }

		/// <summary>
		/// Get a new view id for an RMNet version power view
		/// </summary>
		/// <param name="sXMLDoc"></param>
		private void GetNewViewID(string sXMLDoc , int p_iExistingViewID)
		{
			string sViewID = string.Empty;
			XmlDocument oDoc = null;
			XmlElement oForm = null;

			//Try to get new view id if needed
			if( m_NewViewID == 0 )
			{
				oDoc = new XmlDocument();
				oDoc.LoadXml( sXMLDoc );
				oForm = (XmlElement)oDoc.SelectSingleNode("//form");
				if( oForm != null )
				{
					sViewID = oForm.GetAttribute("viewid");
					if( sViewID == string.Empty)
					{
                        //Raman 06/19/2009 : We do not need to create duplicate view for RMNet now as our page database is different
                        //We would be upgrading the existing view id for RMNet too

                        //m_NewViewID = Utilities.GetNextUID(m_ConnectionString, "NET_VIEWS");                        
                        m_NewViewID = p_iExistingViewID;
						m_IsRMNetView = true;
					}
					else
					{
						m_NewViewID = int.Parse(sViewID);
						m_IsRMNetView = false;
					}
				}
			}
		}

		/// <summary>
		/// Get the new home url
		/// </summary>
		private void GetNewHomeURL()
		{
			if (m_IsRMNetView)
			{
				m_NewViewHome = GetMappedControl(m_ViewHome, "homepage");
			}
			else
			{
				//URL changed from RMX SP1 to RM Xr2
				m_NewViewHome = m_ViewHome.Replace("home?pg=riskmaster/fdm/fdm&amp;", "fdm?");
			}

            //added by NAVDEEP
            //Change URL to R5 Standard
            m_NewViewHome = GetR5URL(m_NewViewHome);
		}

        /// <summary>
        /// GetR5URL - added by NAVDEEP
        /// </summary>
        /// <param name="sOldUrl"></param>
        /// <returns></returns>
        private string GetR5URL(string sOldUrl)
        {
            string sHomeUrl = sOldUrl;
            if (sOldUrl.Contains("fdm?") || sOldUrl.Contains("home?"))
            {
                switch (sOldUrl)
                {
                    case "fdm?SysFormName=event":
                        sHomeUrl = "event";
                        break;
                    case "fdm?SysFormName=claimgc":
                        sHomeUrl = "claimgc";
                        break;
                    case "fdm?SysFormName=claimwc":
                        sHomeUrl = "claimwc";
                        break;
                    case "fdm?SysFormName=claimva":
                        sHomeUrl = "claimva";
                        break;
                    case "fdm?SysFormName=claimdi":
                        sHomeUrl = "claimdi";
                        break;
                    case "fdm?SysFormName=policy":
                        sHomeUrl = "policy";
                        break;
                    case "fdm?SysFormName=patient":
                        sHomeUrl = "patient";
                        break;
                    case "fdm?SysFormName=vehicle":
                        sHomeUrl = "vehicle";
                        break;
                    case "fdm?SysFormName=people":
                        sHomeUrl = "people";
                        break;
                    case "fdm?SysFormName=employee":
                        sHomeUrl = "employee";
                        break;
                    case "fdm?SysFormName=entitymaint":
                        sHomeUrl = "entitymaint";
                        break;
                    case "fdm?SysFormName=physician":
                        sHomeUrl = "physician";
                        break;
                    case "fdm?SysFormName=leaveplan":
                        sHomeUrl = "leaveplan";
                        break;
                    case "fdm?SysFormName=plan":
                        sHomeUrl = "plan";
                        break;
                    case "fdm?SysFormName=staff":
                        sHomeUrl = "staff";
                        break;
                    case "fdm?SysFormName=bankaccount":
                        sHomeUrl = "bankaccount";
                        break;
                    case "fdm?SysFormName=deposit":
                        sHomeUrl = "deposit";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=claim":
                        sHomeUrl = "sclaim";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=event":
                        sHomeUrl = "sevent";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=employee":
                        sHomeUrl = "semployee";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=entity":
                        sHomeUrl = "sentity";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=vehicle":
                        sHomeUrl = "svehicle";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=policy":
                        sHomeUrl = "spolicy";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=payment":
                        sHomeUrl = "spayment";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=patient":
                        sHomeUrl = "spatient";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=physician":
                        sHomeUrl = "sphysician";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=staff":
                        sHomeUrl = "smedstaff";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=dp":
                        sHomeUrl = "splan";
                        break;
                    case "home?pg=riskmaster/Search/MainPage&formname=leaveplan":
                        sHomeUrl = "sleaveplan";
                        break;
                    case "home?pg=riskmaster/Diaries/DiaryList":
                        sHomeUrl = "DiaryList";
                        break;
                    case "home?pg=riskmaster/DocumentManagement/file-listing&flag=Files":
                        sHomeUrl = "DocumentList";
                        break;
                    case "home?pg=riskmaster/DCC/reports-listing":
                        sHomeUrl = "reports-listing";
                        break;
                    case "home?pg=riskmaster/Reports/JobQueue":
                        sHomeUrl = "smrepqueue";
                        break;
                    case "fdm?SysFormName=admintrackinglist&SysViewType=controlsonly":
                        sHomeUrl = "admintrackinglist";
                        break;
                    default:
                        sHomeUrl = "DiaryList";
                        break;
                }
            }
            return sHomeUrl;
        }

		/// <summary>
		/// Upgrade a powerview
		/// </summary>
		public void UpgradeView(bool p_bRMXViewUpgrade)
		{
			FormBase oForm = null;

			//Upgrade the home url for the power-view
			GetNewHomeURL();

			if( m_oFormList.Count == 0 )
				return;

			for(int iForm=0; iForm< m_oFormList.Count; iForm++)
			{
				oForm = (FormBase)m_oFormList[iForm];	
				if(p_bRMXViewUpgrade)
                    oForm.UpgradeForm(m_User, m_Password, m_DSN, m_iDSNId, m_ConnectionString, m_ViewID);
                else
                    oForm.UpgradeForm();
			}
		}
		
		/// <summary>
		/// Save upgraded view definitions back to database
		/// </summary>
		/// <param name="oCommand"></param>
        public void SaveUpgradedView(DbCommand oCommand)
		{
            int iForm = 0;
            string sSQL = String.Empty;
            string sNewHome = String.Empty;
			string sNewViewName = String.Empty;

            #region commented out code
            //Raman 06/19/2009 : We do not need to create duplicate view for RMNet now as our page database is different
            //We would be upgrading the existing view id for RMNet too
            
            //if( m_IsRMNetView )
			//{
				//Insert into NET_VIEWS and VIEW_NAME field is limited to 50
            /*  
            sNewViewName = m_ViewName;
                if (sNewViewName.Length > 50)
                    sNewViewName = sNewViewName.Substring(0, 50);

				sNewHome = m_NewViewHome;

                //Modified by Navdeep -  added DSN ID in query 
                //sSQL = "INSERT INTO NET_VIEWS(VIEW_ID,VIEW_NAME,VIEW_DESC,HOME_PAGE,PAGE_MENU) " +
                //sSQL = "INSERT INTO NET_VIEWS(DATA_SOURCE_ID,VIEW_ID,VIEW_NAME,VIEW_DESC,HOME_PAGE,PAGE_MENU) " +
                //    "VALUES(" + m_iDSNId + "," + m_NewViewID + ",'" + sNewViewName.Replace("'", "''") + "','" +
                //    m_ViewDesc.Replace("'", "''") + "','" + sNewHome.Replace("'", "''") + 
                //    "','" + m_PageMenu.Replace("'", "''") + "')" ;
                //oCommand.CommandText = sSQL;
                //oCommand.ExecuteNonQuery(); //New View Created


                //commented for R5..Navdeep
                //sSQL = "INSERT INTO NET_VIEWS_MEMBERS(VIEW_ID,MEMBER_ID,ISGROUP) SELECT " +
                //    m_NewViewID + ",MEMBER_ID,ISGROUP FROM NET_VIEWS_MEMBERS " +
                //    "WHERE NET_VIEWS_MEMBERS.VIEW_ID=" + m_ViewID;
               
                //Include DATA_SOURCE_ID in SQL Query - Navdeep
                //sSQL = "INSERT INTO NET_VIEWS_MEMBERS(VIEW_ID,MEMBER_ID,ISGROUP,DATA_SOURCE_ID) SELECT " +
                //    m_NewViewID + ",MEMBER_ID,ISGROUP,DATA_SOURCE_ID FROM NET_VIEWS_MEMBERS " +
                //    "WHERE NET_VIEWS_MEMBERS.VIEW_ID=" + m_ViewID;
                //oCommand.CommandText = sSQL;
                //oCommand.ExecuteNonQuery();
			}
			else
			{
             */
            #endregion

            //Insert into NET_VIEWS and VIEW_NAME field is limited to 50
            sNewViewName = m_ViewName;
            if (sNewViewName.Length > 50)
                sNewViewName = sNewViewName.Substring(0, 50);

            sNewHome = m_NewViewHome;
            
            // If home url changed, update it
            if( m_ViewHome != m_NewViewHome )
            {
                sNewHome = m_NewViewHome;
                sNewHome = sNewHome.Replace("'", "''");
                //added DSN ID to SQL Query - Navdeep
                sSQL = "UPDATE NET_VIEWS SET HOME_PAGE='" + sNewHome + "' WHERE VIEW_ID=" + m_ViewID + " AND DATA_SOURCE_ID = " + m_iDSNId;
                oCommand.CommandText = sSQL;
                oCommand.ExecuteNonQuery();
            }
            //}

			InsertDefaultViews( oCommand );

			try
			{
                for (iForm = 0; iForm < m_oFormList.Count; iForm++)
                {
                    //for R5..Raman Bhatia
                    //We need to set DSNID
                    ((FormBase)m_oFormList[iForm]).DSNID = m_iDSNId;

                    ((FormBase)m_oFormList[iForm]).SaveUpgradeForm(oCommand);
                }
			}
			catch(Exception e)
			{
				string sError = e.Message;
			}
		}

        /// <summary>
        /// SaveUpgradedView
        /// </summary>
        public void SaveUpgradedView()
        {
            int iForm = 0;
            string sSQL = String.Empty;
            string sNewHome = String.Empty;
            string sNewViewName = String.Empty;
            DbCommand oCommand = null;
            DbConnection oConnection = null;

            try
            {
                oConnection = DbFactory.GetDbConnection(m_RMXPageConnectionString);
                sNewViewName = m_ViewName;
                if (sNewViewName.Length > 50)
                    sNewViewName = sNewViewName.Substring(0, 50);

                sNewHome = m_NewViewHome;

                // If home url changed, updage it
                if (m_ViewHome != m_NewViewHome)
                {
                    oConnection.Open();
                    oCommand = oConnection.CreateCommand();
                    sNewHome = m_NewViewHome;
                    sNewHome = sNewHome.Replace("'", "''");
                    //added DSN ID to SQL Query - Navdeep
                    sSQL = "UPDATE NET_VIEWS SET HOME_PAGE='" + sNewHome + "' WHERE VIEW_ID=" + m_ViewID + " AND DATA_SOURCE_ID = " + m_iDSNId;
                    oCommand.CommandText = sSQL;
                    oCommand.ExecuteNonQuery();
                }

                for (iForm = 0; iForm < m_oFormList.Count; iForm++)
                {
                    //for R5..Raman Bhatia
                    //We need to set DSNID
                    ((FormBase)m_oFormList[iForm]).DSNID = m_iDSNId;
                    ((FormBase)m_oFormList[iForm]).SaveUpgradeForm(m_User, m_Password, m_DSN, m_iDSNId, m_ConnectionString);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (oConnection != null)
                    oConnection.Close();
            }
        }

		/// <summary>
		/// Inserts the default forms for the newly created view if upgraded from RMNet view. 
		/// These are the forms which cannot be edited using powerviews. 
		/// So they need to be inserted by default for each of the existing view
		/// This will take care of new screens situation. they would be inserted by default even if not in rmnet
		/// </summary>
		private void InsertDefaultViews(DbCommand oCommand )
		{
            string sSQL = String.Empty;
            string sCaption = String.Empty;
            string sFormName = String.Empty;
			XmlNode objNode = null;
            XmlNamespaceManager nsNamespace = null;
            XmlDocument objFormDoc = new XmlDocument();

			try
			{
				foreach(DataRow oRow in m_DSBaseView.Tables[0].Rows)
				{
					sFormName = oRow["FORM_NAME"].ToString();
					if(!IsPVForm( sFormName ))
					{
						objFormDoc.LoadXml(oRow["VIEW_XML"].ToString());
						objNode = objFormDoc.SelectSingleNode("//form");
						if(objNode == null)
						{
							nsNamespace = new XmlNamespaceManager(objFormDoc.NameTable);
							nsNamespace.AddNamespace("xhtml", XHTML_NAMESPACE);
							//Try the xhtml:body for the other list kind of forms
							objNode = objFormDoc.SelectSingleNode("//xhtml:body",nsNamespace);
							if( objNode == null )
								continue;
						}

						if(objNode != null)
							((XmlElement)objNode).SetAttribute("viewid", m_NewViewID.ToString());  

						//Need to be inserted as default in the views table.
						//If the original is already RMX view, try to delete it first and insert again.
						sCaption = oRow["CAPTION"].ToString();
						sFormName = oRow["FORM_NAME"].ToString();						
						if( !m_IsRMNetView )
						{
							//to be checked - Navdeep add DSN here also
                            sSQL = "DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID=" + m_NewViewID.ToString() +
								" AND DATA_SOURCE_ID = " + m_iDSNId + " AND FORM_NAME='" + sFormName.Replace("'", "''") + "'";
							oCommand.CommandText = sSQL;
							oCommand.ExecuteNonQuery();
						}

						DbParameter objParam = null;
                        
                        //NAVDEEP 
                        //Query changed to add DataSourceID information
//						sSQL = "INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML) VALUES (" + 
                        sSQL = "INSERT INTO NET_VIEW_FORMS(DATA_SOURCE_ID , VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML) VALUES (" + m_iDSNId + " , " +
							m_NewViewID.ToString() + ",'" + sFormName.Replace("'", "''") +
							"'," + oRow["TOPLEVEL"].ToString() + ",'" +
							sCaption.Replace("'", "''") + "',~VXML~)";
						oCommand.Parameters.Clear();
						objParam = oCommand.CreateParameter();
						objParam.Direction = ParameterDirection.Input;
						objParam.Value = objFormDoc.OuterXml ;
						objParam.ParameterName = "VXML";
						objParam.SourceColumn = "VIEW_XML";
						oCommand.Parameters.Add(objParam);
						oCommand.CommandText = sSQL;
						oCommand.ExecuteNonQuery();
					}
				}
			}
			catch(Exception e)
			{
				string sError = e.Message;
			}
		}

        /// <summary>
        /// CHECK if its a PV compatible form
        /// </summary>
        /// <param name="p_sForm"></param>
        /// <returns></returns>
		public bool IsPVForm(string p_sForm)
		{
			bool bIsPVForm = false;

			if(m_FormListDoc.SelectSingleNode("//form[@file='" + p_sForm + "']")!=null)
				bIsPVForm = true;
			else
			{
				//check for all low case name, i.e. AdjusterDatedText.xml
				p_sForm = p_sForm.ToLower();
				if(m_FormListDoc.SelectSingleNode("//form[@file='" + p_sForm + "']")!=null)
					bIsPVForm = true;
			}

			return bIsPVForm;
		}

		/// <summary>
		/// Find RMX control name for a RMNet control from the mapping file
		/// </summary>
		/// <param name="p_sCtrlName"></param>
		/// <param name="p_sForm"></param>
		/// <returns></returns>
		public string GetMappedControl(string p_sCtrlName,string p_sForm)
		{
			XmlNode objNode = null;
			
			objNode = m_ControlMapDoc.SelectSingleNode("//data[@name='" + p_sCtrlName + "' and @form='" + p_sForm + "']/value");
			if(objNode != null)
				return objNode.InnerText; 
			else
				return "";
		}
	}
}
