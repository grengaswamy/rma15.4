﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Configuration;

namespace Riskmaster.Tools.PvUpgrade
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            string strView = String.Empty;
            string strSecurity = String.Empty;

            if (args.Length == 0)
            {
                System.Windows.Forms.Application.EnableVisualStyles();
                System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
                
                strSecurity = SecurityConnectionString();
                strView = ViewConnectionString();

                if (!String.IsNullOrEmpty(strSecurity) && !String.IsNullOrEmpty(strView))
                {
                    System.Windows.Forms.Application.Run(new frmPVUpgradeWizard(strSecurity, strView));
                }
            } // if
            else
            {
                //Run the program in silent mode without displaying the UI
            } // else
        }

        /// <summary>
        /// Gets the View Database Connection String
        /// </summary>
        /// <returns>RMX View Database connection string</returns>
        static string ViewConnectionString()
        {
            string strView = String.Empty;

            try
            {
                strView = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
            }
            catch(Exception ex)
            {

                MessageBox.Show("Missing or invalid connection in the connectionStrings.config. " + ex.Message, "View Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            return strView;
        } // method: ViewConnectionString

        /// <summary>
        /// Gets the Security Database Connection String
        /// </summary>
        /// <returns>RMX Security Database connection string</returns>
        static string SecurityConnectionString()
        {
            string strSecurity = String.Empty;

            try
            {
                strSecurity = ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Missing or invalid connection in the connectionStrings.config. " + ex.Message, "Security Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            return strSecurity;
        } // method: SecurityConnectionString
    }
}
