using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Riskmaster.Security; 
using Riskmaster.Db; 
using System.Xml; 
using Riskmaster.Common; 
using System.IO; 
using Riskmaster.DataModel;
using Riskmaster.Application.AdminTracking;
using System.Diagnostics; 
using System.Configuration;

namespace  Riskmaster.Tools.PvUpgrade
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmPVUpgrade : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListBox lstDsnList;
		private System.Windows.Forms.Label lblPick;
		private System.Windows.Forms.Button btnUpgrade;
		private System.Windows.Forms.Button btnCancel;
        private const string PREFIX_NAME = "RMX UPG of ";

		private static string m_DSN = string.Empty;
		private bool m_IsCmdLine = false;
		private static string sLogXml = string.Empty;
		private XmlDocument errXml = null;
        private int m_iDSNId = 0;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label lblViewName;
        private RadioButton radioRMX;
        private RadioButton radioRMNet;
        private CheckBox checkBox1;
        //R5 changes ..Raman Bhatia
        private  string m_RMXPageConnectionString = "";
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmPVUpgrade()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lstDsnList = new System.Windows.Forms.ListBox();
            this.lblPick = new System.Windows.Forms.Label();
            this.btnUpgrade = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblViewName = new System.Windows.Forms.Label();
            this.radioRMX = new System.Windows.Forms.RadioButton();
            this.radioRMNet = new System.Windows.Forms.RadioButton();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lstDsnList
            // 
            this.lstDsnList.Location = new System.Drawing.Point(8, 32);
            this.lstDsnList.Name = "lstDsnList";
            this.lstDsnList.Size = new System.Drawing.Size(537, 186);
            this.lstDsnList.TabIndex = 0;
            // 
            // lblPick
            // 
            this.lblPick.Location = new System.Drawing.Point(8, 8);
            this.lblPick.Name = "lblPick";
            this.lblPick.Size = new System.Drawing.Size(208, 16);
            this.lblPick.TabIndex = 1;
            this.lblPick.Text = "Pick Database to Upgrade";
            // 
            // btnUpgrade
            // 
            this.btnUpgrade.Location = new System.Drawing.Point(71, 339);
            this.btnUpgrade.Name = "btnUpgrade";
            this.btnUpgrade.Size = new System.Drawing.Size(112, 32);
            this.btnUpgrade.TabIndex = 2;
            this.btnUpgrade.Text = "Upgrade";
            this.btnUpgrade.Click += new System.EventHandler(this.btnUpgrade_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(263, 339);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(112, 32);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Exit";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(8, 307);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(537, 24);
            this.progressBar1.TabIndex = 5;
            this.progressBar1.Visible = false;
            // 
            // lblViewName
            // 
            this.lblViewName.Location = new System.Drawing.Point(5, 265);
            this.lblViewName.Name = "lblViewName";
            this.lblViewName.Size = new System.Drawing.Size(537, 27);
            this.lblViewName.TabIndex = 6;
            // 
            // radioRMX
            // 
            this.radioRMX.AutoSize = true;
            this.radioRMX.Checked = true;
            this.radioRMX.Location = new System.Drawing.Point(8, 234);
            this.radioRMX.Name = "radioRMX";
            this.radioRMX.Size = new System.Drawing.Size(124, 17);
            this.radioRMX.TabIndex = 7;
            this.radioRMX.TabStop = true;
            this.radioRMX.Text = "Upgrade RMX Views";
            this.radioRMX.UseVisualStyleBackColor = true;
            // 
            // radioRMNet
            // 
            this.radioRMNet.AutoSize = true;
            this.radioRMNet.Location = new System.Drawing.Point(190, 234);
            this.radioRMNet.Name = "radioRMNet";
            this.radioRMNet.Size = new System.Drawing.Size(134, 17);
            this.radioRMNet.TabIndex = 8;
            this.radioRMNet.Text = "Upgrade RMNet Views";
            this.radioRMNet.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(395, 234);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(81, 17);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "Ignore Error";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // frmPVUpgrade
            // 
            this.AcceptButton = this.btnUpgrade;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(557, 374);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.radioRMNet);
            this.Controls.Add(this.radioRMX);
            this.Controls.Add(this.lblViewName);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnUpgrade);
            this.Controls.Add(this.lblPick);
            this.Controls.Add(this.lstDsnList);
            this.MaximizeBox = false;
            this.Name = "frmPVUpgrade";
            this.Text = "RISKMASTER X Powerview Upgrade";
            this.Load += new System.EventHandler(this.frmPVUpgrade_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args) 
		{
			System.Windows.Forms.Application.Run(new frmPVUpgrade());
		}

		private void frmPVUpgrade_Load(object sender, System.EventArgs e)
		{
			try
			{
				string[] arrDb;
				Login objLogin = new Login();
				arrDb = objLogin.GetDatabases();
				lstDsnList.DataSource = arrDb;
			}
			catch(Exception ex){MessageBox.Show(ex.Message);};	
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close(); 
		}

		private void btnUpgrade_Click(object sender, System.EventArgs e)
		{
			string sConnectionString = string.Empty;
			string sDSN = string.Empty;
			string sSQL = string.Empty;
			string sUser = string.Empty;
			string sPassword = string.Empty;
			DbReader oReader = null;
			int iViewID = 0;
			string sViewName = string.Empty;
			string sViewHome = string.Empty;
			string sViewDesc = string.Empty;
			string sPageMenu = string.Empty;
			string sMapFile = string.Empty;
			string sListFile = string.Empty;
			DataSet oDSBaseView = null;
			DbTransaction oTransaction = null;
			DbCommand oCommand = null;
			DbConnection  oConnection = null;
			XmlDocument oFormListDoc = null;
			XmlDocument oControlMapDoc = null;

			bool bErr=false;
            bool isRMXViewUpgrade = true;
			string sControlType = string.Empty;

			TextWriterTraceListener oTraceListener = null;
			TraceSwitch oTraceSwitch = null;

			PowerView oPView = null;
			ArrayList oPViewList = null;

			try
			{
                if (radioRMNet.Checked)
                    isRMXViewUpgrade = false;

				//Check for the mapping files
				string sPath=AppDomain.CurrentDomain.BaseDirectory;
				sMapFile = AppDomain.CurrentDomain.BaseDirectory + @"\Mapping.xml";
				sListFile = AppDomain.CurrentDomain.BaseDirectory + @"\formlist.xml";
				if(!File.Exists(sMapFile) || !File.Exists(sListFile))
				{
					if(!m_IsCmdLine)
					{
						MessageBox.Show("Mapping.xml or formlist.xml file missing");
						return;
					}
					else
					{
						throw new Exception ("Mapping.xml or formlist.xml file missing");
					}
				}
				
                //R5 changes ..Raman Bhatia
                //We need to use RMX_Page as defined in config file for writing new views

                m_RMXPageConnectionString = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
				//Incase its not executed from command line prompt for a back up
				sDSN = lstDsnList.SelectedItem.ToString();

                frmLogin oLoginForm = new frmLogin();
                oLoginForm.Dsn = sDSN;
                oLoginForm.ShowDialog();
                if (!oLoginForm.LoginSuccess)
                {
                    MessageBox.Show("You must login first before you can run the PowerView upgrade tool.");
                    return;
                }

                sUser = oLoginForm.UserID;
                sPassword = oLoginForm.Password;
                sConnectionString = oLoginForm.ConnectionString;

				if (MessageBox.Show("Please be ABSOLUTELY certain that a backup of your database or database server has been performed before proceeding. " +  "\n" +
					"Are you sure you want to proceed with the upgrade process? " + "\n" + 
					"Computer Sciences Corporation cannot be held responsible for unauthorized or unapproved use of this program.  " + "\n" + "\n" +
					"Yes will upgrade the existing POWERVIEWS in " + sDSN + " to RISKMASTER X compatible form. " , "Confirm Upgrade",
					MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.No)  
					return;


				//Initialize trace switch
				string sLogFile = AppDomain.CurrentDomain.BaseDirectory + "PvUpgrade_" + sDSN  + ".log";
				oTraceListener = new TextWriterTraceListener( sLogFile );
				Trace.Listeners.Add(oTraceListener);
				Trace.AutoFlush = true;
				oTraceSwitch = new TraceSwitch("mySwitch", "Entire application");

				this.Cursor = Cursors.WaitCursor;  
				progressBar1.Visible=true;

				//Get base views for the database				
                //Raman Bhatia: Base Views now reside in RMX_Page

                UserLogin oLogin = new UserLogin(sUser,sPassword,sDSN);
                m_iDSNId = oLogin.DatabaseId;
                if (oLogin != null)
                {
                    //oLogin.Dispose();
                }

				//oDSBaseView = DbFactory.GetDataSet(sConnectionString, "SELECT * FROM NET_VIEW_FORMS WHERE VIEW_ID = 0");
                oDSBaseView = DbFactory.GetDataSet(m_RMXPageConnectionString, "SELECT * FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND DATA_SOURCE_ID = " + m_iDSNId.ToString() + " AND FORM_NAME NOT LIKE 'admintracking|%'");

				oFormListDoc = GetFormList(sListFile, sDSN, sUser, sPassword);
				oControlMapDoc = new XmlDocument();
				oControlMapDoc.Load( sMapFile );

				//Get the View List. If it's RMNet view upgrade, exclude RMNet views which were upgraded before 
                //(There exist views with name beginning with "RMX Upgrade of...")
				oPViewList = new ArrayList();
                if (isRMXViewUpgrade)
                {
                    //Modified by Navdeep -  added DSN ID in query 
                    sSQL = "SELECT * FROM NET_VIEWS WHERE VIEW_ID > 0 AND DATA_SOURCE_ID = " + m_iDSNId.ToString() + " ORDER BY VIEW_NAME";
                }
                else
                {
                    //Modified by Navdeep -  added DSN ID in query 
                    sSQL = "SELECT * FROM NET_VIEWS nv1 WHERE NOT EXISTS " +
                        "(SELECT * FROM NET_VIEWS nv2 WHERE nv2.VIEW_NAME = '" + PREFIX_NAME + "' + nv1.VIEW_NAME) AND DATA_SOURCE_ID = " + m_iDSNId.ToString() + " ORDER BY nv1.VIEW_NAME";
                }
                oReader = DbFactory.GetDbReader(m_RMXPageConnectionString, sSQL);
				while(oReader.Read())
				{
					progressBar1.Value = 0;
					sViewName = oReader.GetString("VIEW_NAME");
					sViewHome = oReader.GetString("HOME_PAGE");
					sViewDesc = oReader.GetString("VIEW_DESC");
					sPageMenu = oReader.GetString("PAGE_MENU");
					iViewID = oReader.GetInt32("VIEW_ID");
					oPView = new PowerView(oTraceListener, oTraceSwitch);
					oPView.Initialization(iViewID, sViewName, sDSN, sUser, sPassword, 
						sConnectionString, oFormListDoc, oControlMapDoc, oDSBaseView,
						sViewHome, sViewDesc, sPageMenu);
                    oPViewList.Add(oPView);
				}
				oReader.Close();

				progressBar1.Maximum = oPViewList.Count;
				for(int i=0; i< oPViewList.Count; i++)
				{
					oPView = (PowerView)oPViewList[i];
					progressBar1.Value = i + 1;
					lblViewName.Text = "Updating view: " + oPView.ViewName;
					this.Refresh();

                    oPView.GetExistingFormXMLs(m_iDSNId.ToString());
                    if ((isRMXViewUpgrade && (!oPView.IsRMNetView)) || ((!isRMXViewUpgrade) && oPView.IsRMNetView))
                    {
                        oPView.UpgradeView();
                    }
				}

                //R5.. Set views for the database				
                //Navdeep: Views now reside in RMX_Page
                oConnection = DbFactory.GetDbConnection(m_RMXPageConnectionString);
				oConnection.Open();
				oCommand = oConnection.CreateCommand();
				oTransaction = oConnection.BeginTransaction();
				oCommand.Transaction = oTransaction;
				for(int i=0; i< oPViewList.Count; i++)
				{
					oPView = (PowerView)oPViewList[i];
					progressBar1.Value = i + 1;
					lblViewName.Text = "Updating database for view: " + oPView.ViewName;
					this.Refresh();

                    if ((isRMXViewUpgrade && (!oPView.IsRMNetView)) || ((!isRMXViewUpgrade) && oPView.IsRMNetView))
                    {
                        //R5: We also need to set DSNID now
                        oPView.DSNID = m_iDSNId;
                        oPView.SaveUpgradedView(oCommand);
                    }
				}

				//Commit Transaction
				if(!bErr || this.checkBox1.Checked)
				{					
					//Insert default views for non-pv forms					
					oCommand.Transaction.Commit();
				}
				else
				{
					if(oCommand.Transaction != null)
						oCommand.Transaction.Rollback();
				}

				progressBar1.Value = 0;
				//Write to xml.
				if(bErr)
					errXml.Save(sLogXml);       
				
				this.Cursor = Cursors.Default;
				progressBar1.Visible=false;
				if(!m_IsCmdLine)
				{
					if(!bErr || this.checkBox1.Checked)
						MessageBox.Show("PowerViews Upgraded Successfully.", "Powerview Upgrade");  
					else
						MessageBox.Show("FAILED to Upgrade PowerViews. Please see the log file " + sLogXml, "PowerView Upgrade" );  
				}
			}
			catch(Exception ex)
			{
				if(oCommand!=null) 
					if(oCommand.Transaction != null)
					{
						oCommand.Transaction.Rollback();
					}

                oTraceListener.WriteLine(ex.Message + ". " + ex.StackTrace);

				if(!m_IsCmdLine)
					MessageBox.Show(ex.Message);   
			}
			finally
			{
				progressBar1.Visible=false;
				this.Cursor = Cursors.Default;  
				if(oReader!=null)
					oReader.Close(); 
				
				if(oConnection!=null)
					oConnection.Close();

                if(oTraceListener != null)
				    oTraceListener.Close();
			}
		}

		private XmlDocument GetFormList(string sListFile, string sDSN, string sUser, string sPassword)
		{
			XmlDocument objXMLDocFormList = null;
			XmlElement objSelectElement = null;
			XmlElement objRowText = null; 
			DataModelFactory objDmf = null; 
			ADMTableList objAdmLst=null;
			ArrayList arrAdm=null;
			string[] retItem = new string[3];

			//Retrieve form list from file
			objXMLDocFormList = new XmlDocument();
			objXMLDocFormList.Load(sListFile);		
			objSelectElement = (XmlElement)objXMLDocFormList.SelectSingleNode("//forms"); 

			//retrieve all admin tracking table names
			objDmf = new DataModelFactory(sDSN, sUser, sPassword);
			objAdmLst= (ADMTableList)objDmf.GetDataModelObject("ADMTableList",false);
			arrAdm = objAdmLst.TableUserNames();
   
			foreach(string[] arrItem in arrAdm)
			{
				objRowText = objXMLDocFormList.CreateElement("form");
				objRowText.SetAttribute("name",arrItem[1]);
				objRowText.SetAttribute("file","admintracking" + "%" + arrItem[0] + ".xml");
				objRowText.SetAttribute("toplevel","1");
				objSelectElement.AppendChild((XmlNode)objRowText);
			}

			return objXMLDocFormList;
		}

		private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
		{
			if(checkBox1.Checked && !m_IsCmdLine)  
				MessageBox.Show("Selecting this option would ignore all errors and upgrade the PowerViews","Ignore",
					System.Windows.Forms.MessageBoxButtons.OK , System.Windows.Forms.MessageBoxIcon.Warning ); 
		}
        /// <summary>
        /// Public Property to fetch the RMX Page connection string
        /// </summary>
        public string RMXPageConnectionString
        {
            get
            {
                return m_RMXPageConnectionString;
            }
        }
	}
}