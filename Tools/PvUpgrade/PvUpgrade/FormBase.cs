using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO; 
using System.Xml; 

using Riskmaster.Common; 
using Riskmaster.DataModel;
using Riskmaster.Db; 

namespace Riskmaster.Tools.PvUpgrade
{
	/// <summary>
	/// Summary description for FormBase.
	/// </summary>
	public class FormBase
    {
        #region declare class objects
        protected int m_ViewID = 0;
        protected int m_DSNID = 0;
		protected string m_ViewName = string.Empty;
		protected string m_FormName = string.Empty;
		protected string m_ADTName = string.Empty;
		protected int m_iTopLevel = 0;
		protected string m_Caption = string.Empty;
		protected string m_NewFormXml = string.Empty;
		protected string m_OldFormXml = string.Empty;
		protected bool m_IsADT = false;
		protected bool m_IsRMNetView = false;
		protected bool m_hasFormElement = false;
		protected bool m_IsPowerViewForm = false;
		protected XmlDocument m_OldFormXmlDoc = null;
		protected XmlDocument m_NewFormXmlDoc = null;
		protected XmlDocument m_ControlMapDoc = null;
		protected XmlDocument m_FormListDoc = null;
		protected string m_DSN = string.Empty;
		protected string m_User = string.Empty;
		protected string m_Password = string.Empty;
		protected string m_ConnectionString = string.Empty;
		protected DataSet m_DSBaseView = null;
		protected const string XHTML_NAMESPACE="http://www.w3.org/1999/xhtml";
		protected const string MESSAGE_TYPE="message";
		protected const string PREFIX_NAME="RMX UPG of ";
		protected TextWriterTraceListener m_TraceListener = null;
		protected TraceSwitch m_TraceSwitch = null;
		protected PowerView m_parent = null;
		protected const string PV_ATTRLIST = "|title|required|helpmsg|";
        private int m_iClientId=0;
        #endregion

        /// <summary>
        /// FormBase
        /// </summary>
        public FormBase()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#region Properties
		public int ViewID
		{
			get{ return m_ViewID; }
			set{ m_ViewID = value; }
		}
        //for R5
        public int DSNID
        {
            get { return m_DSNID; }
            set { m_DSNID = value; }
		}

		public string ViewName
		{
			get{ return m_ViewName; }
			set{ m_ViewName = value; }
		}

		public string DSN
		{
			get	{ return m_DSN; }
			set { m_DSN = value; }
		}

		public string User 
		{
			get	{ return m_User; }
			set { m_User = value; }
		}

		public string Password
		{
			get	{ return m_Password; }
			set { m_Password = value; }
		}

		public string ConnectionString
		{
			get	{ return m_ConnectionString; }
			set { m_ConnectionString = value; }
		}

		public virtual string FormName
		{
			get {return m_FormName;}
			set {}
		}

		public int TopLevel
		{
			get{ return m_iTopLevel; }
			set{ m_iTopLevel = value; }
		}

		public string Caption
		{
			get{ return m_Caption; }
			set{ m_Caption = value; }
		}

		public string NewFormXml
		{
			get{ return m_NewFormXml; }
			set{ m_NewFormXml = value; }
		}

		public string OldFormXml
		{
			get{ return m_OldFormXml; }
			set
			{
				m_OldFormXml = value;
				m_OldFormXmlDoc = new XmlDocument();
				m_OldFormXmlDoc.LoadXml( m_OldFormXml );

				XmlElement oForm = (XmlElement)m_OldFormXmlDoc.SelectSingleNode("//form");
				if(oForm == null)
					m_hasFormElement = false;
				else
					m_hasFormElement = true;
			}
		}

		public bool IsRMNetView
		{
			get{ return m_IsRMNetView; }
			set{ m_IsRMNetView = value; }
		}

		public bool IsPowerviewForm
		{
			get{ return m_IsPowerViewForm; }
			set{ m_IsPowerViewForm = value; }
		}

		public bool IsAdminTrackingForm
		{
			get{ return m_IsADT; }
			set{ m_IsADT = value; }
		}

		public DataSet DSBaseView
		{
			set{ m_DSBaseView = value; }
		}

		public XmlDocument ControlMapDoc
		{
			set{ m_ControlMapDoc = value; }
		}

		public XmlDocument FormListDoc
		{
			set{ m_FormListDoc = value; }
		}
		#endregion Property

		/// <summary>
		/// Wrapper function for upgrade power view form
		/// </summary>
		public virtual void UpgradeForm()
		{
		}

        /// <summary>
        /// UpgradeForm
        /// </summary>
        /// <param name="sUser"></param>
        /// <param name="sPassword"></param>
        /// <param name="sDSN"></param>
        /// <param name="iDSNId"></param>
        /// <param name="sConnString"></param>
        /// <param name="iViewId"></param>
        public virtual void UpgradeForm(string sUser, string sPassword, string sDSN, int iDSNId, string sConnString, int iViewId)
        {
        }

		/// <summary>
		/// Save upgraded powerview form back to database
		/// </summary>
		/// <param name="oCommand"></param>
		public virtual void SaveUpgradeForm(DbCommand oCommand )
		{
		}

        /// <summary>
        /// SaveUpgradeForm
        /// </summary>
        /// <param name="sUser"></param>
        /// <param name="sPassword"></param>
        /// <param name="sDSN"></param>
        /// <param name="iDSNId"></param>
        /// <param name="sConnString"></param>
        public virtual void SaveUpgradeForm(string sUser, string sPassword, string sDSN, int iDSNId, string sConnString)
        {
        }

		/// <summary>
		/// Get default AdminTracking table form xml
		/// </summary>
		/// <param name="p_sAdm">AdminTracking table name</param>
		/// <returns></returns>
		protected XmlDocument GetDefaultAdmView(string p_sAdm)
		{
			XmlDocument objGrp = new XmlDocument(); 
			XmlDocument objXMLFormDoc = new XmlDocument();
			string sAdm = string.Empty;
			string sViewXML = string.Empty;
			string[] arr = p_sAdm.Split('%');

			if(arr.Length==1)
				arr = p_sAdm.Split('|');

			if(arr[1].EndsWith(".xml"))
				sAdm=arr[1].Substring(0,arr[1].Length-4);
			else
				sAdm = arr[1];

			m_DSBaseView.Tables[0].DefaultView.RowFilter = "FORM_NAME='admintracking.xml'"; 
			if(m_DSBaseView.Tables[0].DefaultView.Count >0)
			{
				sViewXML = m_DSBaseView.Tables[0].DefaultView[0].Row["VIEW_XML"].ToString();
			}
			objXMLFormDoc.LoadXml(sViewXML);

			//Retrieve Admin Tracking table specific XML
            DataModelFactory objDmf = DataModelFactory.CreateDataModelFactory(m_ConnectionString, m_iClientId);
			ADMTable objAdm = (ADMTable)objDmf.GetDataModelObject("ADMTable",false);

            //added by Navdeep
            objAdm.PowerViewUpgrade = true;
			
            objAdm.TableName = sAdm;
			sViewXML = objAdm.ViewXml.OuterXml;  
			objAdm.Dispose();
			objDmf.Dispose();  
			objGrp.LoadXml(sViewXML);
 
			//Add specific xml to admintracking.xml
			XmlElement newGrp = objXMLFormDoc.CreateElement("group");
			foreach(XmlAttribute atr in ((XmlElement)objGrp.SelectSingleNode("//group")).Attributes) 
				newGrp.SetAttribute(atr.Name,atr.Value);   

			objXMLFormDoc.SelectSingleNode("//form").AppendChild(newGrp);
			newGrp.InnerXml=objGrp.SelectSingleNode("//group").InnerXml;

			return objXMLFormDoc;
		}

		/// <summary>
		/// Add supp fields to the base form xml
		/// </summary>
		/// <param name="p_sUserId">login user</param>
		/// <param name="p_sPassword">password</param>
		/// <param name="p_sConnString">connection string to RMX database</param>
		/// <param name="p_sDSNName">Data source name</param>
		/// <param name="oListFieldsDoc">base view form xml</param>
		protected void AddSuppDefinition(string p_sUserId, string p_sPassword, string p_sConnString, string p_sDSNName, XmlDocument oListFieldsDoc)
		{
            string sTableName = String.Empty;
			DataModelFactory objDataModelFactory = null;
			SupplementalObject objSupp = null;
			XmlElement objSuppGrp = null;
			XmlDocument objSuppDoc= null;

			try
			{
				sTableName = ((XmlElement)oListFieldsDoc.GetElementsByTagName("form")[0]).GetAttribute("supp");
				if(sTableName.Trim() == string.Empty)
					return; 

				objSuppDoc = new XmlDocument();
                objDataModelFactory = DataModelFactory.CreateDataModelFactory(p_sConnString, m_iClientId);

				foreach(XmlNode objSection in oListFieldsDoc.SelectNodes("//section"))
				{
					switch(((XmlElement)objSection).GetAttribute("name"))
					{
						case "suppdata":
							objSupp = (Supplementals)objDataModelFactory.GetDataModelObject("Supplementals",false);
                            objSupp.PowerViewUpgrade = true; //added by Navdeep
                            objSupp.TableName= sTableName; 
							break;
						case "jurisdata":
							//							objSupp = (Jurisdictionals)objDataModelFactory.GetDataModelObject("Jurisdictionals",false);
							//							objSupp.TableName= sTableName;
							break;
						case "acorddata":
							objSupp = (Acord)objDataModelFactory.GetDataModelObject("Acord",false);
                            objSupp.PowerViewUpgrade = true; //added by Navdeep
                            objSupp.TableName="CLAIM_ACCORD_SUPP";
							break;
					}
					if(objSupp!=null)
					{
						objSuppDoc.LoadXml(objSupp.ViewXml.OuterXml);  
						objSuppGrp = oListFieldsDoc.CreateElement("group");
						foreach(XmlAttribute attr in objSuppDoc.SelectSingleNode("//group").Attributes)
						{
							objSuppGrp.SetAttribute(attr.Name, attr.Value);  
						}
						objSuppGrp.InnerXml= objSuppDoc.SelectSingleNode("//group").InnerXml;  
						oListFieldsDoc.SelectSingleNode("//form").InsertAfter(objSuppGrp, oListFieldsDoc.SelectSingleNode("//form/group[last()]"));
					}
				}
			}
			catch(Exception e)
			{
				m_TraceListener.WriteLine("FormBase:AddSuppDefinition:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
				throw e;				
			}
			finally
			{
				if(objSupp != null)
					objSupp.Dispose();
				if(objDataModelFactory != null)
					objDataModelFactory.Dispose();
				objSuppDoc=null;
				objSuppGrp = null;
			}
		}

		/// <summary>
		/// Set the tab index for all the controls in the form
		/// </summary>
		protected void SetTabIndex()
		{
			XmlNodeList oControlList = null; 
			int iRightTabIndex = 0;
			int iLeftTabIndex = 0;
			XmlNodeList oGroupList = null;
			XmlNode oGroupNode = null;
			XmlNodeList oDispColumnList = null;
			XmlNode oDispColumnNode = null;
			XmlElement oControlElem = null;
			XmlNodeList oSubControlList = null;
			XmlElement oSubControlElem = null;
			string sType = string.Empty;
			string sSingleRow = string.Empty;

            try
            {
                oGroupList = m_NewFormXmlDoc.SelectNodes("//form/group");
                for (int iGroup = 0; iGroup < oGroupList.Count; iGroup++)
                {
                    oGroupNode = oGroupList[iGroup];
                    string sGroupName = string.Empty;
                    XmlAttribute attrGroupName = oGroupNode.Attributes["name"];
                    //For the views from RMNet, the group name attribute maybe missing. It needs to be there.
                    if (attrGroupName == null)
                    {
                        sGroupName = GetGroupName(m_NewFormXmlDoc, iGroup);
                        ((XmlElement)oGroupNode).SetAttribute("name", sGroupName);
                    }

                    //Get the start tab index for the right column
                    oControlList = oGroupNode.SelectNodes(".//control");
                    iRightTabIndex = iLeftTabIndex + (oControlList.Count / 2 + 1) * 10;
                    oControlList = oGroupNode.SelectNodes(".//control[@type='controlgroup']//control");
                    iRightTabIndex += oControlList.Count * 10;

                    oDispColumnList = oGroupNode.SelectNodes("./displaycolumn");
                    for (int iDispColumn = 0; iDispColumn < oDispColumnList.Count; iDispColumn++)
                    {
                        oDispColumnNode = oDispColumnList[iDispColumn];
                        oControlList = oDispColumnNode.ChildNodes;
                        for (int iControl = 0; iControl < oControlList.Count; iControl++)
                        {
                            //Some node type, i.e. comment, can not be converted to XmlElement
                            if (oControlList[iControl].NodeType != XmlNodeType.Element)
                                continue;

                            oControlElem = (XmlElement)oControlList[iControl];
                            sType = oControlElem.GetAttribute("type");
                            switch (sType)
                            {
                                //No tab index needed for message type control
                                case "message":
                                    break;
                                //control groups
                                case "controlgroup":
                                    oSubControlList = oControlElem.ChildNodes; ;
                                    sSingleRow = oControlElem.GetAttribute("singlerow");
                                    for (int iSubControl = 0; iSubControl < oSubControlList.Count; iSubControl++)
                                    {
                                        //Some node type, i.e. comment, can not be converted to XmlElement
                                        if (oSubControlList[iSubControl].NodeType != XmlNodeType.Element)
                                            continue;

                                        oSubControlElem = (XmlElement)oSubControlList[iSubControl];
                                        if (sSingleRow == "yes")
                                        {
                                            iLeftTabIndex += 10;
                                            oSubControlElem.SetAttribute("tabindex", iLeftTabIndex.ToString());
                                        }
                                        else
                                        {
                                            if (iSubControl % 2 == 0)
                                            {
                                                iLeftTabIndex += 10;
                                                oSubControlElem.SetAttribute("tabindex", iLeftTabIndex.ToString());
                                            }
                                            else
                                            {
                                                iRightTabIndex += 10;
                                                oSubControlElem.SetAttribute("tabindex", iRightTabIndex.ToString());
                                            }
                                        }
                                    }
                                    break;
                                //Regular controls
                                default:
                                    if (iControl == 0)
                                    {
                                        iLeftTabIndex += 10;
                                        oControlElem.SetAttribute("tabindex", iLeftTabIndex.ToString());
                                    }
                                    else
                                    {
                                        iRightTabIndex += 10;
                                        oControlElem.SetAttribute("tabindex", iRightTabIndex.ToString());
                                    }
                                    break;
                            }
                        }
                    }

                    iLeftTabIndex = iRightTabIndex + 10;
                }
            }
            catch (Exception e)
            {
                m_TraceListener.WriteLine("FormBase:SetTabIndex:: View: " + m_ViewName + " Form: " + m_FormName + ". Error: " + e.Message);
                return;
            }		
		}

        /// <summary>
        /// Get the group name value for RMNet Views.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="groupIndex"></param>
        /// <returns></returns>
        private string GetGroupName(XmlDocument xmlDoc, int groupIndex)
        {
            XmlNode groupNode = null;
            string groupName = "group" + groupIndex.ToString();
            groupNode = m_NewFormXmlDoc.SelectSingleNode("//group[@name='" + groupName + "']");
            while (groupNode != null)
            {
                groupName += "_1";
                groupNode = m_NewFormXmlDoc.SelectSingleNode("//group[@name='" + groupName + "']");
            }

            return groupName;
        }

        /// <summary>
        /// responsible for logging all errors as they occur during the database upgrade
        /// </summary>
        /// <param name="strLogFilePath"></param>
        /// <param name="sMsg"></param>
        public static void LogErrors(string strLogFilePath, string sMsg)
        {

            File.AppendAllText(strLogFilePath, sMsg);

            //Insert a blank line for readability
            File.AppendAllText(strLogFilePath, Environment.NewLine);
        }

        public class FormNames
        {
            string m_FormName;
            bool m_bPowerViewAvailable;
            string m_OldName;//amitosh
            string m_Caption;
            public FormNames(string p_sFormName, bool p_bPowerviewAvailable,string p_sOldName,string p_sCaption)//amitosh
            {
                m_FormName = p_sFormName;
                m_bPowerViewAvailable = p_bPowerviewAvailable;
                m_OldName = p_sOldName;
                m_Caption = p_sCaption;
            }
            public FormNames(string p_sFormName, bool p_bPowerviewAvailable)
            {
                m_FormName = p_sFormName;
                m_bPowerViewAvailable = p_bPowerviewAvailable;
                m_OldName = string.Empty;
                m_Caption = string.Empty;
            }

            public string FormName
            {
                get
                {
                    return m_FormName;
                }
                set
                {
                    m_FormName = value;
                }
            }

            public bool PowerViewAvailable
            {
                get
                {
                    return m_bPowerViewAvailable;
                }
                set
                {
                    m_bPowerViewAvailable = value;
                }
            }

            public string OldName//amitosh
            {
                get
                {
                    return m_OldName;
                }
                set
                {
                    m_OldName = value;
                }
            }
            public string Caption//amitosh
            {
                get
                {
                    return m_Caption;
                }
                set
                {
                    m_Caption = value;
                }
            }

        }
	}
}
