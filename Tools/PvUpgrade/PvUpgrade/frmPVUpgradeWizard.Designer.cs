﻿namespace Riskmaster.Tools.PvUpgrade
{
    public partial class frmPVUpgradeWizard
    {
        private DevComponents.DotNetBar.WizardPage wpDSNSelection;
        private DevComponents.DotNetBar.WizardPage wpConfirmation;
        private System.Windows.Forms.ListBox lstDsnList;
        private DevComponents.DotNetBar.WizardPage wpLegacy;
        private System.Windows.Forms.Label lblViewName;
        private DevComponents.DotNetBar.LabelX labelX1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.LabelX labelX8;
        public DevComponents.DotNetBar.Controls.ListViewEx lvDatabases;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevComponents.DotNetBar.WizardPage wpSelectProcess;
        private DevComponents.DotNetBar.WizardPage wpRMXCurrent;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbRMX;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbLegacy;
        private DevComponents.DotNetBar.LabelX lblWarningWPDS;
        private DevComponents.DotNetBar.LabelX lblWarningWPSP;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX10;
        private System.Windows.Forms.PictureBox pictureBox4;
        private DevComponents.DotNetBar.WizardPage wpStatus;
        public DevComponents.DotNetBar.Controls.ProgressBarX progressBar1;
        private DevComponents.DotNetBar.LabelX labelX11;
        private System.Windows.Forms.PictureBox pictureBox5;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.Controls.CheckBoxX radioRMNet;
        private DevComponents.DotNetBar.Controls.CheckBoxX checkBox1;
        private DevComponents.DotNetBar.Controls.CheckBoxX radioRMX;
        private DevComponents.DotNetBar.LabelX labelX13;
        private DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.Controls.CheckBoxX rdbPV;
        private DevComponents.DotNetBar.Controls.CheckBoxX rdbJurisData;
        private DevComponents.DotNetBar.Controls.CheckBoxX rdbPVJuris;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.LabelX labelX18;
        private DevComponents.DotNetBar.LabelX lblStatus;
        private DevComponents.DotNetBar.LabelX labelX16;
        private System.Windows.Forms.PictureBox pictureBox6;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.LabelX lblWarningWPLGCY;
        private DevComponents.DotNetBar.LabelX lblWarningWPRMX;
        private DevComponents.DotNetBar.Controls.CheckBoxX rdbSingleForm;
        private System.Windows.Forms.TextBox txtFormName;
        private System.Windows.Forms.ColumnHeader dsn;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.Label LblDsnList;
        private System.Windows.Forms.Label LblDsnListHeader;
        private DevComponents.DotNetBar.Wizard wizard1;
    }
}