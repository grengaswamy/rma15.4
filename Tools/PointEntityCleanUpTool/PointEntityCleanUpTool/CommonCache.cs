﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;

namespace PointEntityCleanUpTool
{
    public static class CommonCache
    {
        #region "Common Variables"
        public static string ss_DSNConnectionString;
        public static int si_UId;
        public static int si_DsnId;
        public static string ss_UName;
        public static string ss_DsnName;
        public static string ss_MainDatabaseName;
        public static string ss_Session;

        public static int si_PolicySystemID;
        public static int si_ClientFileFlag;
        public static bool sb_IsPolicyInterestSelected;
        public static bool sb_IsUnitInterestSelected;
        public static bool sb_IsPolicyInsuredSelected;
        public static bool sb_IsDriverSelected;
        public static string ss_LastSelectedVal;
        public static string ss_OutputXML;

        static string ss_LogFilePath;
        #endregion

        #region "Constants"
        private const string cs_Issue = "********************PCT_ISSUE*****************";
        private const string cs_Message = "****************PCT_MESSAGE*******************";
        private const string cs_Warning = "****************PCT_WARNING*******************";
        private const string cs_CatchError = "****************PCT_CATCH_ERROR***************";
        private const string cs_EndString = "**************************************************";

        #region "Database"
        #region "Tables"
        public const string cst_EntityXAddress = "ENTITY_X_ADDRESSES";
        #endregion
        #region "Columns"
        public const string csc_Addr1 = "ADDR1";
        public const string csc_Addr2 = "ADDR2";
        #endregion
        #endregion
        enum LogType
        {
            Issue = 1,
            Message = 2,
            Warning = 3,
            Catch_Error = 4
        }
        #endregion

        #region "Common Functions"
        /// <summary>
        /// Used for writing logs
        /// </summary>
        /// <param name="fileName">aspx Page Name</param>
        /// <param name="methodName">Name of Method from where writing the Log</param>
        /// <param name="message">Log Message</param>
        /// <param name="iLogType">1 --> Update/Insert Issue;  2 --> Message; 3 --> Warning; 4 --> Catch Error</param>
        /// <returns>Nothing</returns>
        public static void WriteLogFile(string fileName, string methodName, string message, int iLogType)
        {
            try
            {
                if (!string.IsNullOrEmpty(message))
                {
                    if (string.IsNullOrEmpty(ss_LogFilePath))
                        ss_LogFilePath = string.Concat(Application.StartupPath, "\\PointEntityCleanUpTool_", DateTime.Now.ToString("MMM_dd_yyyy_hh_mm_ss_tt"), ".log");

                    StreamWriter log;
                    if (!File.Exists(ss_LogFilePath))
                        log = new StreamWriter(ss_LogFilePath);
                    else
                        log = File.AppendText(ss_LogFilePath);

                    // Write to the file:
                    switch (iLogType)
                    {
                        case (Int32)LogType.Issue:
                            log.WriteLine(cs_Issue);
                            break;
                        case (Int32)LogType.Message:
                            log.WriteLine(cs_Message);
                            break;
                        case (Int32)LogType.Warning:
                            log.WriteLine(cs_Warning);
                            break;
                        case (Int32)LogType.Catch_Error:
                            log.WriteLine(cs_CatchError);
                            break;
                    }

                    log.WriteLine(message);
                    log.WriteLine(cs_EndString);
                    log.WriteLine();
                    // Close the stream:
                    log.Close();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static bool ColumnExists(DataTable dt, string columnName)
        {
            if (dt != null)
            {
                return dt.Columns.Contains(columnName);
            }
            else
                return false;
        }

        /// <summary>
        /// Returns the database type after connecting to the database
        /// </summary>
        /// <param name="strConnString">connection string</param>
        /// <returns>returns an integer representing the DatabaseType</returns>
        public static int DbType(string strConnString)
        {
            // DBMS_IS_ACCESS   = 0,
            // DBMS_IS_SQLSRVR  = 1,
            // DBMS_IS_SYBASE   = 2,
            // DBMS_IS_INFORMIX = 3,
            // DBMS_IS_ORACLE   = 4,
            // DBMS_IS_ODBC     = 5,
            // DBMS_IS_DB2      = 6,

            var dbConn = DbFactory.GetDbConnection(strConnString);

            dbConn.Open();
            var DB_MAKE = dbConn.DatabaseType;
            dbConn.Close();
            dbConn.Dispose();

            return Convert.ToInt32(DB_MAKE);
        }

        public static int GetDBColumnLength(string sTableName, string sColumnName)
        {
            int iReturnValue = 0;
            if (string.Equals(sTableName.ToUpper(), cst_EntityXAddress) && (string.Equals(sColumnName.ToUpper(), csc_Addr1) || string.Equals(sColumnName.ToUpper(), csc_Addr2)))
                iReturnValue = 50;

            return iReturnValue;
        }
        #endregion
    }
}
