using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.Common.Win32;
using Riskmaster.Db;
using Riskmaster.Common;
using Oracle.DataAccess;

namespace Riskmaster.Tools.MCMUpdateAcroStoreUtil
{
    /**************************************************************
	 * $File		: frmMCMUpdateAcroStoreUtil.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 10/25/2009
	 * $Author		: Rahul Solanki 
	 * $Comment		: 
	**************************************************************/
    /*
    *	The tool can either be run directly or can be executed via a bacth file/installer in silent mode.
    *   Command line parameters-
    *  Riskmaster.Tools.MCMUpdateAcroStoreUtil.exe uid pwd DSN
    *		
    */
    public partial class frmMCMUpdateAcroStoreUtil : Form
    {
        #region Declarations

        private Login m_objLogin = new Login();        
        private Riskmaster.Db.DbConnection m_objRMDbConnection ;
        private Riskmaster.Db.DbConnection m_objMCMDbConnection;

        public DirectoryInfo m_diRmx;

        private static long m_MovedFilesCount = 0;
        private static long m_ProcessedFilesCount = 0;
        
        //private static string[] m_LOBinfo = new string[20]; //assuming max 10 LOB division 
        //private static int m_LOBCount ;    
        public static  StreamWriter m_sw1;

        private string m_nowDateTime = Conversion.ToDbDate(System.DateTime.Now);
        private string m_nowTime = Conversion.ToDbTime(System.DateTime.Now);

        private static DbWriter m_RMDocsdbWriter;
        private static DbWriter m_ClaimHtmlCommentsdbWriter;

    

        public frmMCMUpdateAcroStoreUtil()
        {
            InitializeComponent();
        }

        #endregion

        #region debugging only Code

        //Loading comments attached to claims
        private void btnLoadMCMDocsInfo_Click(object sender, EventArgs e)
        {
            string sqlDocs = "SELECT MD_ID, MD_EXT_MEDIA_DESC, MD_TITLE_TXT , MD_CREATE_TS FROM MEDIA_DESCRIPTION ";
            writeLog("Loading MCM docs info.");
            try
            {
                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objMCMDbConnection, sqlDocs);
                AppGlobals.m_dsDocuments.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsDocuments);
                //dataGridView1.DataSource = AppGlobals.m_dsComments.Tables[0];
                //dataGridView1.Refresh();
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            
            //m_strDisplay = 2;
            //btnExtractNotes.Enabled = true;
            txtNoClaimsComments.Text = AppGlobals.m_dsDocuments.Tables[0].Rows.Count.ToString();
        }

        //Loading comments attached to events
        private void btnLoadCommentsEvent_Click(object sender, EventArgs e)
        {
            string sqlEvents = "SELECT  EVENT_ID, EVENT_NUMBER, COMMENTS, HTMLCOMMENTS FROM EVENT WHERE COMMENTS IS NOT NULL ";
            writeLog("Loading comments attached to events");
            try
            {
                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objRMDbConnection, sqlEvents);
                AppGlobals.m_dsDocuments.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsDocuments);
                //dataGridView1.DataSource = AppGlobals.m_dsComments.Tables[0];
                //dataGridView1.Refresh();
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            writeLog("comments attached to events Load operation completed.");
            //m_strDisplay = 1;
//            btnExtractNotes.Enabled = true;
            txtNoEventComments.Text = AppGlobals.m_dsDocuments.Tables[0].Rows.Count.ToString();

        }

        private void btnBindCommentGrid_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = AppGlobals.m_dsDocuments.Tables[0];
            dataGridView1.Refresh();
        }

        private void btnBindEnhcNotesGrid_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = AppGlobals.m_dsDocuments.Tables[0];
            dataGridView2.Refresh();
        }

        //reading note_types  
        private void button4_Click(object sender, EventArgs e)
        {                           
            string sqlNotes = "SELECT C.CODE_ID,CODE_DESC FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID=CT.CODE_ID WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME)='NOTE_TYPE_CODE') ORDER BY 2";
            DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objRMDbConnection, sqlNotes);            
            DataSet ds2 = new DataSet();            
            ObjDbDataAdapter.Fill(ds2);            
            grdNoteType.DataSource = ds2.Tables[0];
            grdNoteType.Refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowLoc=Convert.ToInt32(e.RowIndex.ToString());
            int ColumnLoc = Convert.ToInt32(e.ColumnIndex.ToString());
            string celltext = dataGridView1.Rows[rowLoc].Cells[ColumnLoc].Value.ToString();
            MessageBox.Show(celltext,"cell text");
        }

        private void dataGridView2_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int rowLoc = Convert.ToInt32(e.RowIndex.ToString());
            int ColumnLoc = Convert.ToInt32(e.ColumnIndex.ToString());
            string celltext = dataGridView2.Rows[rowLoc].Cells[ColumnLoc].Value.ToString();
            MessageBox.Show(celltext, "cell text");
        }

        // delete new saved enhanced c2n 
        private void btnDeleteC2NenchNotes_Click(object sender, EventArgs e)
        {
            //string sqlDelNotes = string.Format("DELETE FROM CLAIM_PRG_NOTE WHERE NOTE_TYPE_CODE IN (SELECT CODE_ID FROM CODES WHERE UPPER(SHORT_CODE) = '{0}')", m_sEnhcNotesShortCode);
            //DbCommand objDbCommand;
            ////MessageBox.Show(m_objRMDbConnection.State.ToString());
            //m_objRMDbConnection.Open();
            //objDbCommand = m_objRMDbConnection.CreateCommand();            
            //objDbCommand.CommandText = sqlDelNotes;
            //try
            //{
            //    objDbCommand.ExecuteNonQuery();
            //}
            //catch (Exception p_oException)
            //{
            //    writeLog(string.Format("ERROR:[C2N notes deletion error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", p_oException.Message.ToString(), p_oException.InnerException.ToString(), p_oException.StackTrace.ToString()));
            //    throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            //}
            //finally
            //{
            //    m_objRMDbConnection.Close();
            //}
        }

        private void ExecuteNonQuery(string sqlQuery)
        {            
            DbCommand objDbCommand;
            //m_objMCMDbConnection.Open();
            Riskmaster.Db.DbConnection objTempConn = DbFactory.GetDbConnection(AppGlobals.MCMConnectionString);
            //objTempConn.ConnectionString = AppGlobals.MCMConnectionString;
            objTempConn.Open();
            objDbCommand = objTempConn.CreateCommand();
            objDbCommand.CommandText = sqlQuery;
            try
            {
                objDbCommand.ExecuteNonQuery();
            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[ExecuteNonQuery ] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", p_oException.Message.ToString(), p_oException.InnerException.ToString(), p_oException.StackTrace.ToString()));
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                objTempConn.Close();
            }
        }

        // this button wil noe NOT be used
        // Extracting Notes from comments/event dataset
        //updates the RTF box wih the provided text

        #endregion

        //Form Load 
        private void frmMCMUpdateAcroStoreUtil_Load(object sender, EventArgs e)
        {
            if (AppGlobals.bSilentMode)
            {
                this.Hide();                
            }
            try
            {
                if (System.IO.File.Exists("MCMUpdateAcroStoreUtil.log"))
                {
                    System.IO.File.Move("MCMUpdateAcroStoreUtil.log", "MCMUpdateAcroStoreUtil." + System.DateTime.Now.ToString("dd-MM-yy-HH-mm-ss") + ".log");
                }
                m_sw1 = new StreamWriter("MCMUpdateAcroStoreUtil.log", true);

            }
            catch (Exception exp)
            {
                //writeLog(string.Format("ERROR:[Log file Open error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            if (AppGlobals.bSilentMode)
            {             
                writeLog("Running in silent mode; DSN: " + AppGlobals.sDSN);
            }
            writeLog("-----------------------------");
            writeLog("Form Loaded... authenticating");
            writeLog("-----------------------------");
           
            Boolean bCon = false;
            try
            {
                if (!AppGlobals.bSilentMode)
                {
                    bCon = m_objLogin.RegisterApplication(this.Handle.ToInt32(), 0, ref AppGlobals.Userlogin);    
                }                
                if (!AppGlobals.bSilentMode && !bCon)
                { 
                    // for invalid Login
                    if (m_objLogin!=null) m_objLogin.Dispose();
                    writeLog("Invalid Logon. terminating");
                    Application.Exit();
                }
                else
                {
                    writeLog("Authentication successful");

                    //    McmDbConnString
                    //  McmUploadLoc
                    //          c:\program files\acosoft\acrostore\obecjts\repository
                    //  RmxFilesLoc
                    //          c:\mcmdocs

                    writeLog("retriving connection & filestoc loc...");
                    AppGlobals.ConnectionString = AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString;
                    m_objRMDbConnection = DbFactory.GetDbConnection(AppGlobals.ConnectionString);
                    
                    AppGlobals.MCMConnectionString = System.Configuration.ConfigurationSettings.AppSettings["McmDbConnString"];
                    m_objMCMDbConnection = DbFactory.GetDbConnection(AppGlobals.MCMConnectionString);

                    AppGlobals.strMcmUploadLoc = System.Configuration.ConfigurationSettings.AppSettings["McmUploadLoc"];

                    AppGlobals.strRmxFilesLoc = System.Configuration.ConfigurationSettings.AppSettings["RmxFilesLoc"];

                    if (string.IsNullOrEmpty(AppGlobals.strMcmUploadLoc ) || string.IsNullOrEmpty(AppGlobals.MCMConnectionString) || string.IsNullOrEmpty(AppGlobals.strRmxFilesLoc ))
                    {
                        writeLog("Either MCM / RMC folder info or conenction string info missing in config file");
                        MessageBox.Show("Either MCM / RMC folder info or conenction string info missing in config file");
                        Application.Exit();
                    }
                    
                    if (!System.IO.Directory.Exists(AppGlobals.strMcmUploadLoc))
                    {
                        //System.IO.Directory.CreateDirectory(targetPath);
                        writeLog("MCM folder loc not accessible");
                        MessageBox.Show("MCM folder loc not accessible");
                        Application.Exit();
                    }

                    if (!System.IO.Directory.Exists(AppGlobals.strRmxFilesLoc))
                    {
                        //System.IO.Directory.CreateDirectory(targetPath);
                        writeLog("RMX folder loc not accessible");
                        MessageBox.Show("RMX folder loc not accessible");
                        Application.Exit();
                    }                    


                    writeLog("filestoc loc info retrival successfull...");

                    // enable the follwoing line when running in debug mode.
                    // this will enable the developer to view the extracted notes before they can be saved to db
                    //this.Size = new Size(870, 621);
                    this.FormBorderStyle = FormBorderStyle.Fixed3D;

                    //setting status messages                    
                    //txtNewEnhcCode.Text = m_sEnhcNotesShortCode;
                    //txtnewEnhcCodeDesc.Text = m_sEnhcNotesCodeDesc;
                    
                    setStatus("Click on the 'Start' button to start the Filestore update process...");
                    
                    //loadLOBinfo(); 
                    // if application is called thru the command line then directly proceed to saving the Ench Notes. 
                    if (AppGlobals.bSilentMode)
                    {
                        btnStart_Click(null, null);                                                
                    }
                    
                }
                //MessageBox.Show(getNoteTypeCodeId().ToString()   );
                if (AppGlobals.bSilentMode)
                {
                    this.Hide();
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                if (!AppGlobals.bSilentMode)
                {
                    writeLog(string.Format("ERROR:[form Load error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", ex.Message.ToString(), ex.InnerException.ToString(), ex.StackTrace.ToString()));
                    throw new Exception(ex.Message.ToString(), ex.InnerException);
                }
                       //MessageBox.Show(ex.Message, "C2n error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Application.Exit();
            }
        }

        // appends text to the RTF box
        private void setStatus(string p_status)
        {
            txtProgressStatusMessage.Text = p_status;
            //lblStatus.Text=p_status;
            //rtfStatus.AppendText(p_status + "\n");
        }

        //updates the progress bar text box (its just above the progress bar)
        private void setProgressStatus(string p_status)
        {
            txtProgressStatusMessage.Text=p_status;            
        }

        //returns the Code Type of 'Comments2EnhNotes' if presents otherwise creates one
        //private int getNoteTypeCodeId()
        //{
        //    int iNodeTypeCodeId = -1, iCodeIdFromCodesTable=-1, iCodeIdFromCodesTextTable=-1, iNotesGlossaryTableId=-1;
        //    // checking to see if the 'C2N' code type already exits, in which case directly retunr its Code_id
        //    string sqlNotes = string.Format("SELECT C.CODE_ID FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID=CT.CODE_ID WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME)='NOTE_TYPE_CODE') AND UPPER(CT.CODE_DESC) = '{0}' ", m_sEnhcNotesCodeDesc.ToUpper());
        //    DbReader objDbReader; 
        //    //objDbReader = DbFactory.GetDbReader(m_objRMDbConnection.ConnectionString, sqlNotes);
        //    //objDbReader.Read();
        //    //m_objRMDbConnection.ExecuteScalar
        //    DbCommand objDbCommand;
        //    m_objRMDbConnection.Open();
        //    objDbCommand = m_objRMDbConnection.CreateCommand();
        //    objDbCommand.CommandText = sqlNotes;
        //    object objobject = objDbCommand.ExecuteScalar();
                  
        //    //if (!objDbReader.IsDBNull(0))
        //    iNodeTypeCodeId = Convert.ToInt32(objobject);            

        //    // if the Code type already exits then returning the same.  
        //    if (!(iNodeTypeCodeId == -1 || iNodeTypeCodeId == 0)) return iNodeTypeCodeId;

        //    //getting Table id for enhanced note to be put in codes table
        //    sqlNotes = "SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME)='NOTE_TYPE_CODE'";
            
        //    objDbReader = DbFactory.GetDbReader(m_objRMDbConnection.ConnectionString, sqlNotes);
        //    objDbReader.Read();
        //    if (!objDbReader.IsDBNull(0))
        //        iNotesGlossaryTableId = objDbReader.GetInt32(0);

        //    // TO DO : encapsulate the following block within a transaction
            
        //    // getting new code_id from codes table
        //    sqlNotes = "SELECT MAX(CODE_ID)+1 FROM CODES ";
            
        //    objDbReader = DbFactory.GetDbReader(m_objRMDbConnection.ConnectionString, sqlNotes);
        //    objDbReader.Read();
        //    if (!objDbReader.IsDBNull(0))
        //        iCodeIdFromCodesTable = objDbReader.GetInt32(0);

        //    // getting new code_id from codes_text table
        //    sqlNotes = "SELECT MAX(CODE_ID)+1 FROM CODES_TEXT ";

        //    objDbReader = DbFactory.GetDbReader(m_objRMDbConnection.ConnectionString, sqlNotes);
        //    objDbReader.Read();
        //    if (!objDbReader.IsDBNull(0))
        //        iCodeIdFromCodesTextTable = objDbReader.GetInt32(0);

        //    // normally the two tables (Codes & Codes_text would be in sync)... just in case
        //    // taking the maximum of the two for our Comments2EnhNotes codes type
        //    iNodeTypeCodeId = (iCodeIdFromCodesTable > iCodeIdFromCodesTextTable) ? iCodeIdFromCodesTable : iCodeIdFromCodesTextTable;

        //    //saving our new Codes into the table
        //    DbWriter objWriter;
            
        //    // Saving into the codes table
        //    objWriter = DbFactory.GetDbWriter(m_objRMDbConnection);
        //    objWriter.Tables.Add("CODES");
        //    objWriter.Fields.Add("CODE_ID", iNodeTypeCodeId.ToString());
        //    objWriter.Fields.Add("TABLE_ID", iNotesGlossaryTableId.ToString());
        //    objWriter.Fields.Add("SHORT_CODE", m_sEnhcNotesShortCode);
        //    objWriter.Fields.Add("DELETED_FLAG", "0");
        //    objWriter.Execute();

        //    // Saving into the codes_text table
        //    //sqlNotes = "insert into codes_text (code_id,short_code,code_desc) values (9999, 'C2N','Comm2EnhNotes')";
                    
        //    // Saving into the codes table
        //    objWriter = DbFactory.GetDbWriter(m_objRMDbConnection);
        //    objWriter.Tables.Add("CODES_TEXT");
        //    objWriter.Fields.Add("CODE_ID", iNodeTypeCodeId.ToString());
        //    objWriter.Fields.Add("SHORT_CODE", m_sEnhcNotesShortCode);
        //    objWriter.Fields.Add("CODE_DESC", m_sEnhcNotesCodeDesc);
        //    objWriter.Execute();

        //    return iNodeTypeCodeId;
        //}

        //getting Uid for login id
        //private int getUid(string p_loginId)
        //{
        //    string sqlQuery,strResult=string.Empty;
        //    int iUid = -1;
        //    if (m_UidCache.ContainsKey(p_loginId))
        //        return  Convert.ToInt32(m_UidCache[p_loginId]);

        //    writeLog("Retriving uid for login "+ p_loginId);
        //    sqlQuery = string.Format("SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME='{0}' AND DSNID='{1}'", p_loginId, AppGlobals.Userlogin.DatabaseId);
        //    //sqlQuery = string.Format("SELECT * FROM USER_DETAILS_TABLE UDT LEFT JOIN DATA_SOURCE_TABLE DST ON UDT.DSNID=DST.DSNID WHERE UPPER(UDT.LOGIN_NAME)='{0}' AND UPPER(UDT.CONNECTION_STRING)='{1}''", p_loginId.ToUpper(), AppGlobals.ConnectionString.ToUpper());
        //    //SELECT * FROM USER_DETAILS_TABLE udt left Join DATA_SOURCE_TABLE dst on udt.dsnid=dst.dsnid WHERE upper(udt.LOGIN_NAME)='{0}' and upper(udt.connection_string)='{1}'
        //    strResult = GetSingleValue_Sql(sqlQuery, m_objLogin.SecurityDsn);
        //    if (strResult==string.Empty)
        //    {
        //        m_UidCache.Add(p_loginId, -1);
        //        return iUid;
        //    }
        //    iUid = Convert.ToInt32(strResult);
        //    m_UidCache.Add(p_loginId,iUid);
        
        //    return iUid;
        //}

        //getting GroupId (of the group from sms) for the uid 
        //private int getGroupId(string p_uId)
        //{
        //    string sqlQuery,strResult=string.Empty;
        //    int iGroupId=-1;
        //    if (m_GroupidCache.ContainsKey(p_uId))
        //        return Convert.ToInt32(m_GroupidCache[p_uId]);

        //    writeLog("Retriving groupid for uid " + p_uId);
        //    sqlQuery = string.Format("SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID={0}", p_uId);
        //    strResult = GetSingleValue_Sql(sqlQuery, AppGlobals.ConnectionString);
        //    if (strResult == string.Empty)
        //    {
        //        m_GroupidCache.Add(p_uId, -1);
        //        return iGroupId;
        //    }
        //    iGroupId = Convert.ToInt32(strResult);
        //    m_GroupidCache.Add(p_uId, iGroupId);

        //    return iGroupId;
        //}

        //closing all connections & disposing 
                
        private void frmMCMUpdateAcroStoreUtil_FormClosing(object sender, FormClosingEventArgs e)
        {
            writeLog("Disposing all objects" );
            if (m_objLogin!=null)   m_objLogin.Dispose();
            if (AppGlobals.m_dsDocuments!=null) AppGlobals.m_dsDocuments.Dispose();
            //if (AppGlobals.m_dsEnhNotes!=null) AppGlobals.m_dsEnhNotes.Dispose();
            if (m_objRMDbConnection!=null) m_objRMDbConnection.Dispose();
            if (m_sw1 != null)
            {                
                m_sw1.Close();
                m_sw1.Dispose();
            }

            //Todo: additiona dispose here for new collections
            //m_sbHtmlComment.Length = 0;
            

            //if (m_RMDocsdbWriter!=null)
            //{
            //    m_RMDocsdbWriter.
            //}
            
        }

        public static string GetSingleValue_Sql(string sSQL, string strConnectionString)
        {
            string sValue = string.Empty;
            DbReader objReader = null;
            try
            {
                objReader = DbFactory.GetDbReader(strConnectionString, sSQL);
                if (objReader.Read())
                {
                    sValue = Conversion.ConvertObjToStr(objReader[0]);
                }
            }
            catch (Exception p_oException)
            {
                writeLog(string.Format("ERROR:[GetSingleValue_Sql] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", p_oException.Message.ToString(), p_oException.InnerException.ToString(), p_oException.StackTrace.ToString()));
                throw new Exception(p_oException.Message.ToString(), p_oException.InnerException);
            }
            finally
            {
                objReader.Dispose();
            }
            return sValue;
        }

        public static void writeLog(string p_strLogText)
        {
            string sMessage = string.Format("[{0}] {1}", System.DateTime.Now.ToString(), p_strLogText);            
            try
            {                
                m_sw1.WriteLine(sMessage);                
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            
        }

        //public void loadLOBinfo()
        // {
        //     int i = 0;
        //     string sqlNotes = "SELECT C.CODE_ID,CT.CODE_DESC FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID IN ( SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) LIKE '%LINE_OF_BUSINESS%' ) ";
        //     DbReader objDbReader;
        //     m_objRMDbConnection = DbFactory.GetDbConnection(AppGlobals.ConnectionString);
        //     objDbReader = DbFactory.GetDbReader(m_objRMDbConnection.ConnectionString, sqlNotes);
        //     while (objDbReader.Read())             
        //     if (!objDbReader.IsDBNull(0))
        //     {
        //         m_LOBinfo[i] =   Convert.ToString(objDbReader.GetInt32(0));
        //         m_LOBinfo[i+1] = objDbReader.GetString(1);
        //         i += 2;
        //     }
        //     // m_LOBCount would have twice the number of LOB the actualy number of LOB's
        //     // their  Code & code_desc are stored in consecutive values.
        //     m_LOBCount = i;

        // }

        public void ProcessDirectory(DirectoryInfo di) 
        {
            
            FileInfo[] aFiles = di.GetFiles();// Directory.GetFiles(System.IO.Path.GetFullPath(di.Name));

            //there sudn't be any file in the root mcm folder but adding the file check just in case.
            foreach (FileInfo s in aFiles)
            {
                //   MessageBox.Show("file: " + s.Name);
                ProcessFile(s);
            }

            foreach (DirectoryInfo diChild in di.GetDirectories())
              ProcessDirectory(diChild);
        }

        public void ProcessFile(FileInfo fi)
        {
            string sSourceFilename, sDestFilename;
            DataView dv = new DataView(AppGlobals.m_dsAcrosoft.Tables[0], "MD_ID = '" + fi.Name  +"'" , "", DataViewRowState.CurrentRows);
            dv.AllowEdit = true;
            //dv.AllowNew = true;
            //dv.AllowDelete = true;
            //mkaran2 - MITS 31645 - start
            DataView dvDocument = null;
            string sDocFileName;           
            //mkaran2 - MITS 31645 - end
            
            foreach (DataRowView drv in dv)
            {
                try
                {
                    //sSourceFilename = Path.Combine(AppGlobals.strRmxFilesLoc, (string)drv["MD_EXT_MEDIA_DESC"]);
                    //sDestFilename = fi.FullName;

                    //mkaran2 - MITS 31645 - start
                    sDocFileName = (string)drv["MD_EXT_MEDIA_DESC"];
                    if (sDocFileName.Contains("_"))
                    {
                        dvDocument = new DataView(AppGlobals.m_dsDocuments.Tables[0], "DOCUMENT_ID = '" + sDocFileName.Substring(0, sDocFileName.IndexOf("_", sDocFileName.IndexOf("") + 1)) + "'", "", DataViewRowState.CurrentRows);
                        foreach (DataRowView drvDoc in dvDocument)
                        {
                            if (!DBNull.Value.Equals(drvDoc["DOCUMENT_FILEPATH"]))
                                AppGlobals.strRmxFilesLoc = (string)drvDoc["DOCUMENT_FILEPATH"];
                            else
                                AppGlobals.strRmxFilesLoc = System.Configuration.ConfigurationSettings.AppSettings["RmxFilesLoc"];
                        }

                        sSourceFilename = Path.Combine(AppGlobals.strRmxFilesLoc, sDocFileName.Substring(sDocFileName.IndexOf("_") + 1));
                        sDestFilename = fi.FullName;
                    }
                    else
                    {
                        sSourceFilename = Path.Combine(AppGlobals.strRmxFilesLoc, sDocFileName);
                        sDestFilename = fi.FullName;
                    }                  

                    //mkaran2 - MITS 31645 - end
                    //check the presence in MD_status

                    // script for removing the mail merge files

                    //update md_status
                    //set md_status.file_update_status = 4
                    //where md_status.md_id in ( select md.md_id from media_description md, QA_R5PS1.dbo.merge_form mf
                    //where md.md_title_txt=mf.form_file_name )




                    //if absent then 
                    //    locate mapped RMX file - if unable to locate update status in MD_Status
                    //    rename the acro file 

                    //renaming dest file to something else 
                    //fi.MoveTo(sDestFilename + ".bak");                    


                    //    move/rename the RMX file
                    //    update status in md_status
                    //end if

                    //m_diRmx.move
                    m_ProcessedFilesCount++;

                    
                    if (System.IO.File.Exists(sSourceFilename))// cechk the presence of this file in RMX file store...
                    {
                        writeLog(string.Format("moving file {0} to {1}", sSourceFilename, sDestFilename));
                        // add a file exit clause here.. > before rename >
                        if (System.IO.File.Exists(@sDestFilename + ".bak"))
                        {
                            //running this twice will result in overwriting/deleting the previous backup.
                            System.IO.File.Delete(@sDestFilename + ".bak");
                        }

                        System.IO.File.Move(@sDestFilename, @sDestFilename + ".bak");
                        //System.IO.File.Move(@sSourceFilename, @sDestFilename);

                        //mkaran2 - MITS 31645 - start                       
                            System.IO.File.Copy(@sSourceFilename, @sDestFilename);                       
                        //mkaran2 - MITS 31645 - end
                        
                        //updating status in MCM db
                        if (drv["FILE_UPDATE_STATUS"] != System.DBNull.Value)
                        {// entry exist in md_status
                                                        
                            ExecuteNonQuery(string.Format("UPDATE MD_STATUS SET MD_STATUS.FILE_UPDATE_STATUS = 2 WHERE MD_ID = '{0}'", fi.Name));
                            //AppGlobals.                            

                        }
                        else
                        { // entry needs to be added in md_status                            
                            ExecuteNonQuery(string.Format("INSERT INTO MD_STATUS  (MD_ID, FILE_UPDATE_STATUS) VALUES  ({0},2)", fi.Name));
                        }
                        //update the dateset here 
                        //drv["FILE_UPDATE_STATUS"] = 2;

                    }
                    else
                    {
                        // update status in md_status on error.
                        //int temp = (int)drv["FILE_UPDATE_STATUS"];
                        if (drv["FILE_UPDATE_STATUS"] != System.DBNull.Value)
                        {// entry exist in md_status
                            ExecuteNonQuery(string.Format("UPDATE MD_STATUS SET MD_STATUS.FILE_UPDATE_STATUS = 3 WHERE MD_ID = '{0}'", fi.Name));

                        }
                        else
                        { // entry needs to be added in md_status                            
                            ExecuteNonQuery(string.Format("INSERT INTO MD_STATUS  (MD_ID, FILE_UPDATE_STATUS) VALUES  ({0},3)", fi.Name));                            
                        }
                        //update the dateset here 
                        //drv["FILE_UPDATE_STATUS"] = 3;
                    }


                   
                    txtProcessedFilesCount.Text = m_ProcessedFilesCount.ToString();
                    m_MovedFilesCount++;
                    
                    //prgBrUpdateStore

                    //txtProcessedFilesCount
                    //txtTotalFilesCount
                    
                }
                catch (Exception ex)
                {
                    //AppGlobals.m_dsAcrosoft.AcceptChanges();

                    writeLog(string.Format("ERROR:[File Move error] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", ex.Message.ToString(), ex.InnerException.ToString(), ex.StackTrace.ToString()));
                    throw new Exception(ex.Message.ToString(), ex.InnerException);
                }
                Application.DoEvents();
            }

            //query the retrived file in acrosoft ds
             //> if found then locate it in RMX doc folder
            // >> if found > move / rename it to MCM folder
              // and update status in log / documents db

        }

        private void btnStart_Click(object sender, EventArgs e)
        {            
            string sqlLoadComment = string.Empty;                  
            //StringBuilder sbSqlLOBGroup = new StringBuilder();
            btnStart.Enabled = false;
            //string sqlDocs = "SELECT MD_ID, MD_EXT_MEDIA_DESC, MD_TITLE_TXT , MD_CREATE_TS FROM MEDIA_DESCRIPTION ";

            string sqlDocs = "SELECT MD.MD_ID, MD_EXT_MEDIA_DESC, MD_TITLE_TXT, MS.FILE_UPDATE_STATUS FROM MEDIA_DESCRIPTION MD LEFT JOIN  MD_STATUS MS ON MD.MD_ID = MS.MD_ID WHERE FILE_UPDATE_STATUS IS NULL OR ( NOT FILE_UPDATE_STATUS IN (2,3,4 ))";

            //CREATE TABLE MD_STATUS ( MD_ID CHAR(11), FILE_UPDATE_STATUS INT NULL) 

            //--null = added afterwards
            //--1 = initail
            //--2 = migrated
            //--3 = error
            //--4 = Mail merge doc


            //m_ProcessedFilesCount 

            // Loading RMX doc info
            //writeLog(string.Format("Loading Docs info from RMX"));            
            writeLog("Loading MCM docs info.");
            setStatus(string.Format("Loading MCM docs info."));
            try
            {
                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objMCMDbConnection, sqlDocs);
                AppGlobals.m_dsAcrosoft.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsAcrosoft);
                txtTotalFilesCount.Text = AppGlobals.m_dsAcrosoft.Tables[0].Rows.Count.ToString();

                if (System.IO.File.Exists("AcrosoftDocInfo.xml"))
                {
                    System.IO.File.Move("AcrosoftDocInfo.xml", "AcrosoftDocInfo." + System.DateTime.Now.ToString("dd-MM-yy-HH-mm-ss") + ".xml");
                }

                AppGlobals.m_dsAcrosoft.WriteXml(Directory.GetCurrentDirectory() + "\\AcrosoftDocInfo.xml");

            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[load mcm doc info operation] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            writeLog("Loading MCM docs info operation completed.");

            // txtNoClaimsComments.Text = AppGlobals.m_dsAcrosoft.Tables[0].Rows.Count.ToString();

            // Loading RMX doc info
            writeLog("Loading Docs info from RMX");

            sqlDocs = "SELECT DOCUMENT_ID, DOCUMENT_NAME, DOCUMENT_FILENAME FROM DOCUMENT";            
            setStatus("Loading RMX docs info.");
            try
            {
                DbDataAdapter ObjDbDataAdapter = DbFactory.GetDataAdapter(m_objRMDbConnection, sqlDocs);
                AppGlobals.m_dsDocuments.Clear();
                ObjDbDataAdapter.Fill(AppGlobals.m_dsDocuments);
                if (System.IO.File.Exists("RMXDocInfo.xml"))
                {
                    System.IO.File.Move("RMXDocInfo.xml", "RMXDocInfo." + System.DateTime.Now.ToString("dd-MM-yy-HH-mm-ss") + ".xml");
                }
                AppGlobals.m_dsDocuments.WriteXml(Directory.GetCurrentDirectory() + "\\RMXDocInfo.xml");
                //dataGridView1.DataSource = AppGlobals.m_dsComments.Tables[0];
                //dataGridView1.Refresh();
            }
            catch (Exception exp)
            {
                writeLog(string.Format("ERROR:[load rmx doc info operation] \nmessage:{0}\nInnerexception:{1}\nStackTrace{2}  ", exp.Message.ToString(), exp.InnerException.ToString(), exp.StackTrace.ToString()));
                throw new Exception(exp.Message.ToString(), exp.InnerException);
            }
            writeLog("Loading RMX docs info operation completed.");

            //m_strDisplay = 2;
            //btnExtractNotes.Enabled = true;
            //txtNoClaimsComments.Text = AppGlobals.m_dsDocuments.Tables[0].Rows.Count.ToString();

            DirectoryInfo diMCMDir = new DirectoryInfo(AppGlobals.strMcmUploadLoc);
            m_diRmx = new DirectoryInfo(AppGlobals.strRmxFilesLoc);


            //MessageBox.Show("yellow");
            ProcessDirectory(diMCMDir);

            setStatus("Rmx to MCM doc migration completed.");
            //writeLog("Comments to Enhanced notes migration completed.");
            if (!AppGlobals.bSilentMode)
            {
                //MessageBox.Show("Rmx to MCM doc migration completed.");
                writeLog(string.Format("Rmx to MCM doc migration completed. total {0} repository files were got updated ", m_MovedFilesCount));
                MessageBox.Show(string.Format("Rmx to MCM doc migration completed.\ntotal {0} repository files were got updated ", m_MovedFilesCount));

            }
           
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = AppGlobals.m_dsDocuments.Tables[0];
            dataGridView1.Refresh();
        }
 
    }
}