using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Security; 


namespace Riskmaster.Tools.MCMUpdateAcroStoreUtil
{
    public class AppGlobals
    {
        public static UserLogin Userlogin;
        public static string ConnectionString;
        public static string MCMConnectionString;

        public static string strMcmUploadLoc;
        public static string strRmxFilesLoc;

        public static string sUser = ""; //user login
        public static string sPwd = ""; //password
        public static string sDSN = ""; //database name
        public static bool bSilentMode = false;        

        // for storing the comments
        public static DataSet m_dsDocuments = new DataSet();
        public static DataSet m_dsAcrosoft = new DataSet();
                
        // for storing the extracted Notes
        //public static DataSet m_dsEnhNotes = new DataSet();
        
    }
}
