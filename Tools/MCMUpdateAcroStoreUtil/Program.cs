using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Riskmaster.Security;
using Riskmaster.Tools.MCMUpdateAcroStoreUtil;


namespace Riskmaster.Tools.MCMUpdateAcroStoreUtil
{
    /**************************************************************
	 * $File		: Program.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/18/2008
	 * $Author		: Rahul Solanki 
	 * $Comment		: The tool migrates the comments attached to events and claims into enhanced notes. 	 
	**************************************************************/
    /*
    *	The tool can either be run directly or can be executed via a batch file/installer in silent mode.
    *   Command line parameters-
    *  Riskmaster.Tools.MCMUpdateAcroStoreUtil.exe uid pwd DSN
    *		
    */
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] Args )
        {
           Login m_objLogin = null;
            try
            {
                //for getting parameters from command line in case app is run in silent mode               
                if (Args.Length>0)  //handle running automatically
                {
                    m_objLogin = new Login();                    
                    //Disabling Silent run until exception catching for all errors in Userlogin control is implemented
                    AppGlobals.bSilentMode = true;
                    //AppGlobals.bSilentMode = false;

                    AppGlobals.sUser = Args[0];
                    AppGlobals.sPwd = Args[1];
                    AppGlobals.sDSN = Args[2];
                    //AppGlobals.sParam = Args[3].ToUpper() ;

                    //AppGlobals.Userlogin = m_objLogin.AuthenticateUser(AppGlobals.sDSN, AppGlobals.sUser, AppGlobals.sPwd);
                    m_objLogin.AuthenticateUser(AppGlobals.sDSN, AppGlobals.sUser, AppGlobals.sPwd, out  AppGlobals.Userlogin);

                    AppGlobals.ConnectionString = AppGlobals.Userlogin.objRiskmasterDatabase.ConnectionString;                     
                }
                else
                {
                                        
                    AppGlobals.bSilentMode = false;
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);                    
                }
                Application.Run(new frmMCMUpdateAcroStoreUtil());  
            }
            catch (Exception p_oExp)
            {
                string strErrMessage = "There seems to be some problem. \nError description:\n\n";
                //if (!AppGlobals.bSilentMode)
                MessageBox.Show(strErrMessage + p_oExp.Message, "MCMUpdateAcroStoreUtil: Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
            finally
            {
                if (m_objLogin != null)
                    m_objLogin.Dispose();                
            }
        }
    }   
}