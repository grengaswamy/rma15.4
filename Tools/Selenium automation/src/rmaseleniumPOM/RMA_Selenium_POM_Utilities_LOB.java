package rmaseleniumPOM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//Default Package Import Completed

public class RMA_Selenium_POM_Utilities_LOB {
public static WebElement Element = null;

//============================================================================================
//FunctionName 			: RMAApp_LOB_Tab_ClaimOptions
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Claim Options Tab On RMA Application Line Of Business Parameter Set Up Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-19-2015                                 
// ============================================================================================
public static WebElement RMAApp_LOB_Tab_ClaimOptions(WebDriver driver)
{
	Element = driver.findElement(By.linkText("Claim Options")); //Unique Id  Of Claim Options Tab On RMA Application Line Of Business Parameter Set Up Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_LOB_Lst_LineOfBusiness
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which LineOfBusiness Dropdown On Line Of Business Parameter Set Up Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-19-2015                                 
// ============================================================================================
public static WebElement RMAApp_LOB_Lst_LineOfBusiness(WebDriver driver)
{
	Element = driver.findElement(By.id("lstLOB")); //Unique Id  Of  LineOfBusiness Dropdown On Line Of Business Parameter Set Up Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_LOB_Chk_ClaimType
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which ClaimType Check Box On Line Of Business Parameter Set Up Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-19-2015                                 
// ============================================================================================
public static WebElement RMAApp_LOB_Chk_ClaimType(WebDriver driver)
{
	Element = driver.findElement(By.id("chkCType")); //Unique Id  Of  ClaimType Check Box On Line Of Business Parameter Set Up Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_LOB_Btn_Save
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Save Button On Line Of Business Parameter Set Up Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-19-2015                                 
// ============================================================================================
public static WebElement RMAApp_LOB_Btn_Save(WebDriver driver)
{
	Element = driver.findElement(By.id("Save")); //Unique Id  Of  Save Button On Line Of Business Parameter Set Up Page Is Fetched
	return Element;
}
}
