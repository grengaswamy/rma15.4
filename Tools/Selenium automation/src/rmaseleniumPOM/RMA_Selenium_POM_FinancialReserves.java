package rmaseleniumPOM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//Default Package Import Completed

public class RMA_Selenium_POM_FinancialReserves {
public static WebElement Element = null;

//============================================================================================
//FunctionName 			: RMAApp_ReserveCreation_Txt_ReserveAmount
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Reserve Amount TextBox On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_ReserveCreation_Txt_ReserveAmount(WebDriver driver)
{
	Element = driver.findElement(By.id("txtAmount")); //Unique Id  Of Reserve Amount TextBox On RMA Application Financial Reserves Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_ReserveCreation_Btn_Modify
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Modify Button On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_ReserveCreation_Btn_Modify(WebDriver driver)
{
	Element = driver.findElement(By.id("btnModify")); //Unique Id  Of Modify Button On RMA Application Financial Reserves Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_ReserveCreation_Lst_Status
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which ReserveStatus DropDown On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_ReserveCreation_Lst_Status (WebDriver driver)
{
	Element = driver.findElement(By.xpath(".//*[@id='lstStatus']")); //Unique Id  Of Reserve Status DropDown On RMA Application Financial Reserves Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_FinReserves_Btn_AddPayment
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Add Payment Button On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_FinReserves_Btn_AddPayment(WebDriver driver)
{
	Element = driver.findElement(By.id("btnAddPayment")); //Unique Id  Of Add Payment Button On RMA Application Financial Reserves Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_Payment_Tab_TransactionDetail
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Transaction Detail Tab On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Tab_TransactionDetail(WebDriver driver)
{
	Element = driver.findElement(By.id("TABStransactiondetail")); //Unique Id  Of Transaction Detail Tab On RMA Application Payments Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_Payment_Lst_BankAccount
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Bank Account Drop Down  On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Lst_BankAccount(WebDriver driver)
{
	Element = driver.findElement(By.id("bankaccount")); //Unique Id  Of Bank Account Drop Down On RMA Application Payments Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_Payment_Lst_PayeeType
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Payee Type Drop Down  On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Lst_PayeeType(WebDriver driver)
{
	Element = driver.findElement(By.id("cbopayeetype")); //Unique Id  Of Payee Type Drop Down  On RMA Application Payments Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_Payment_Lst_TransactionDate
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Transaction Date Drop Down On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Lst_TransactionDate(WebDriver driver)
{
	Element = driver.findElement(By.id("transdate")); //Unique Id  Of Transaction Date Drop Down  On RMA Application Payments Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_Payment_Txt_DistributionType
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Distribution Type Text Box  On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Txt_DistributionType(WebDriver driver)
{
	Element = driver.findElement(By.id("distributiontype_codelookup")); //Unique Id  Of Distribution Text Box  On RMA Application Payments Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_Payment_Txt_LastName
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Last Name Text Box On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Txt_LastName(WebDriver driver)
{
	Element = driver.findElement(By.id("pye_lastname")); //Unique Id  Of Last Name Combo Box  On RMA Application Payments Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_LastName_Lst_LastNameSelection
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which LastName Selector On LastName Window On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-26-2015                                 
// ============================================================================================
public static WebElement RMAApp_LastName_Lst_LastNameSelection(WebDriver driver)
{
	Element = driver.findElement(By.id("optResults")); //Unique Id  Of LastName Selector On LastName Window On RMA Application Payments Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_LastName_Btn_ChooseHighLighted
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Choose HighLighted Button On LastName Window On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-26-2015                                 
// ============================================================================================
public static WebElement RMAApp_LastName_Btn_ChooseHighLighted(WebDriver driver)
{
	Element = driver.findElement(By.id("cmdSubmit1")); //Unique Id  Of Choose HighLighted Button On LastName Window On RMA Application Payments Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_Payment_Btn_AddNewPayment
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Add New Payment Button On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Btn_AddNewPayment(WebDriver driver)
{
	Element = driver.findElement(By.id("FundsSplitsGrid_New")); //Unique Id  Of Add New Button On RMA Application Payments Page Is Fetched
	return Element;
	
}

//============================================================================================
//FunctionName 			: RMAApp_Payment_Tab_Transaction
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Transaction Tab On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Tab_Transaction(WebDriver driver)
{
	Element = driver.findElement(By.id("LINKTABStransaction")); //Unique Id  Of Transaction Tab On RMA Application Payments Page Is Fetched
	return Element;
		
}

//============================================================================================
//FunctionName 			: RMAApp_Payment_Txt_ControlNumber
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Control Number TextBox On RMA Application Payments Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Txt_ControlNumber(WebDriver driver)
{
	Element = driver.findElement(By.id("ctlnumber")); //Unique Id  Of Control Number TextBox  On RMA Application Payments Page Is Fetched
	return Element;
			
}

//============================================================================================
//FunctionName 			: RMAApp_FinReserves_Lnk_M_Medical
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Medical Link On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-25-2015                                 
// ============================================================================================
public static WebElement RMAApp_FinReserves_Lnk_M_Medical(WebDriver driver)
{
	Element = driver.findElement(By.linkText("M Medical")); //Unique Id  Of Medical Link On RMA Application Financial Reserves Page
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_FundsSplitDetails_Lst_TransactionType
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which TransactionType DropDown On RMA Application FundsSplitDetails Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-26-2015                                 
// ============================================================================================
public static WebElement RMAApp_FundsSplitDetails_Lst_TransactionType (WebDriver driver)
{
	Element = driver.findElement(By.id("TransTypeCode_codelookup")); //Unique Id  Of TransactionType DropDown On RMA Application FundsSplitDetails Page Is Fetched
	return Element;	
}

//============================================================================================
//FunctionName 			: RMAApp_FundsSplitDetails_Txt_Amount
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Amount TextBox On RMA Application FundsSplitDetails Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-26-2015                                 
// ============================================================================================
public static WebElement RMAApp_FundsSplitDetails_Txt_Amount(WebDriver driver)
{
	Element = driver.findElement(By.id("Amount")); //Unique Id  Of Amount TextBox On RMA Application FundsSplitDetails Page Is Fetched
	return Element;	
}

//============================================================================================
//FunctionName 			:RMAApp_FundsSplitDetails_Btn_OK
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which OK Button On RMA Application FundsSplitDetails Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-26-2015                                 
// ============================================================================================
public static WebElement RMAApp_FundsSplitDetails_Btn_OK(WebDriver driver)
{
	Element = driver.findElement(By.id("btnOk")); //Unique Id  Of OK Button On RMA Application FundsSplitDetails Page Is Fetched
	return Element;	
}

//============================================================================================
//FunctionName 			:RRMAApp_Payment_Btn_Save
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Save Button On RMA Application FinancialReserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-26-2015                                 
// ============================================================================================
public static WebElement RMAApp_Payment_Btn_Save(WebDriver driver)
{
		Element = driver.findElement(By.id("save")); //Unique Id  Of Save Button On RMA Application FinancialReserves Page Is Fetched
		return Element;	
}

//============================================================================================
//FunctionName 			: RMAApp_ReserveCreation_Tbl_Reserve
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Reserve Table On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-11-03-2015                                 
//============================================================================================
public static WebElement RMAApp_ReserveCreation_Tbl_Reserve(WebDriver driver)
{
	Element = driver.findElement(By.id("grdReserve")); //Unique Id  Of Reserve Table On RMA Application Financial Reserves Page Is Fetched
	return Element;
}


//============================================================================================
//FunctionName 			: RMAApp_FinReserves_Lnk_L_Litigation
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Litigation Link On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-25-2015                                 
//============================================================================================
public static WebElement RMAApp_FinReserves_Lnk_L_Litigation(WebDriver driver)
{
	Element = driver.findElement(By.linkText("L Litigation")); //Unique Id  Of Litigation Link On RMA Application Financial Reserves Page
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_FinReserves_Lnk_I_Indemnity
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Indemnity Link On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-25-2015                                 
//============================================================================================
public static WebElement RMAApp_FinReserves_Lnk_I_Indemnity(WebDriver driver)
{
	Element = driver.findElement(By.linkText("I Indemnity")); //Unique Id  Of Indemnity Link On RMA Application Financial Reserves Page
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_FinReserves_Lnk_E_Expense
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Expense Link On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-25-2015                                 
//============================================================================================
public static WebElement RMAApp_FinReserves_Lnk_E_Expense(WebDriver driver)
{
	Element = driver.findElement(By.linkText("E Expense")); //Unique Id  Of Expense Link On RMA Application Financial Reserves Page
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_FinReserves_Lnk_RC_REC_Recovery
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Recovery Link On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-25-2015                                 
//============================================================================================
public static WebElement RMAApp_FinReserves_Lnk_RC_REC_Recovery(WebDriver driver)
{
	Element = driver.findElement(By.linkText("RC REC Recovery")); //Unique Id Of Recovery Link On RMA Application Financial Reserves Page
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_FinReserves_Lnk_BI_Bodily_Injury
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Recovery Link On RMA Application Financial Reserves Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-25-2015                                 
//============================================================================================
public static WebElement RMAApp_FinReserves_Lnk_BI_Bodily_Injury(WebDriver driver)
{
	Element = driver.findElement(By.linkText("BI Bodily Injury")); //Unique Id Of Recovery Link On RMA Application Financial Reserves Page
	return Element;
}
}

