package rmaseleniumPOM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//Default Package Import Completed

public class RMA_Selenium_POM_Login_DSNSelect_Frames {
public static WebElement Element = null;

//============================================================================================
//FunctionName 		: RMAApp_Login_Txt_UserName
//Description  		: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which UserName TextBox On RMA Application Login Page Can Be Identified
//Input Parameter 	: Driver Variable Of The Type WebDriver		 
//Revision			: 0.0 - KumudNaithani-10-12-2015                                 
// ============================================================================================
public static WebElement RMAApp_Login_Txt_UserName(WebDriver driver)
{
	Element = driver.findElement(By.id("cphMainBody_Login1_UserName")); //Unique Id  Of UserName Textbox on Login Page Of RMA Application Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 		: RMAApp_Login_Txt_PassWord
//Description  		: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which PassWord TextBox On RMA Application Login Page Can Be Identified
//Input Parameter 	: Driver Variable Of The Type WebDriver		 
//Revision			: 0.0 - KumudNaithani-10-12-2015                                 
// ============================================================================================
public static WebElement RMAApp_Login_Txt_PassWord(WebDriver driver)
{
	Element = driver.findElement(By.id("cphMainBody_Login1_Password")); //Unique Id  Of PassWord Textbox On Login Page Of RMA Application Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_Login_Btn_Login
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Login Button On RMA Application Login Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-12-2015                                 
// ============================================================================================
public static WebElement RMAApp_Login_Btn_Login(WebDriver driver)
{
	Element = driver.findElement(By.xpath(".//*[@id='cphMainBody_Login1_Button1']")); //Unique Id  Of Login Button On Login Page Of RMA Application Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DSNSelect_Lst_DataSourceName
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which DataSourceName ListBox On RMA Application DSN Selection Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-16-2015                                 
// ============================================================================================
public static WebElement RMAApp_DSNSelect_Lst_DataSourceName(WebDriver driver)
{
	Element = driver.findElement(By.id("cphMainBody_ddlDataSources")); //Unique Id  Of DataSourceName ListBox On RMA Application DSN Selection Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DSNSelect_Btn_Continue
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Continue Button On RMA Application DSN Selection Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-16-2015                                 
// ============================================================================================
public static WebElement RMAApp_DSNSelect_Btn_Continue(WebDriver driver)
{
	Element = driver.findElement(By.id("cphMainBody_btnLogin")); //Unique Id  Of Continue Button On RMA Application DSN Selection Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DefaultView_Lbl_DataSource
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which DataSource Label On RMA Application Default View Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-16-2015                                 
// ============================================================================================
public static WebElement RMAApp_DefaultView_Lbl_DataSource(WebDriver driver)
{
	Element = driver.findElement(By.xpath(".//*[@id='cphMainBody_lblDSN']")); //Unique Id  Of DataSource Label On RMA Application Default View Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DefaultView_Frm_MenuOptionFrame
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of DefaultFrame (containing menu options) On RMA Application Default View Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-16-2015                                 
// ============================================================================================
public static WebElement RMAApp_DefaultView_Frm_MenuOptionFrame(WebDriver driver)
{
	Element = driver.findElement(By.id("cphMainBody_uwtPortal_frame0")); //Unique Id  Of DefaultFrame (containing menu options) On RMA Application Default View Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DefaultView_SubMnu_Options
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which GeneralClaim Menu Option On RMA Application Default View Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-16-2015                                 
// ===========================================================================================
public static WebElement RMAApp_DefaultView_SubMnu_Options(WebDriver driver, int x, int y)
{
	Element = driver.findElement(By.xpath(".//*[@id='MDIMenu_"+x+"_"+y+"']"+"/tbody/tr/td[2]/nobr")); //Unique Id  Of SubMenu Options On RMA Application Default View Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DefaultView_FrmEventCreation
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Event Creation Frame On RMA Application Default View Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-19-2015                                 
// ============================================================================================
public static WebElement RMAApp_DefaultView_FrmEventCreation(WebDriver driver)
{
	Element = driver.findElement(By.id("Document-1eventEventEvent (New)FalseFalse")); //Unique Id  Of Event Creation Frame On RMA Application Default View Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DefaultView_Mnu_GeneralClaim
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Document Menu Option On RMA Application Default View Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-16-2015                                 
// ============================================================================================
public static WebElement RMAApp_DefaultView_Mnu_Options(WebDriver driver, int x)
{
	Element = driver.findElement(By.xpath(".//*[@id='MDIMenu_"+x+"']/tbody/tr/td[2]/nobr")); //Unique Id  Of Menu Options On RMA Application Default View Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DefaultView_ContextMnu_Options
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Context Menu Option On RMA Application Default View Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-16-2015                                 
//============================================================================================
public static WebElement RMAApp_DefaultView_ContextMnu_Options(WebDriver driver, int x, int y, int z)
{
	Element = driver.findElement(By.xpath(".//*[@id='MDIMenu_"+x+"_"+y+"_"+z+"']"+"/tbody/tr/td[2]/nobr")); //Unique Id  Of Context Menu Options On RMA Application Default View Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DefaultView_Frm_GeneralClaim
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which General Claim Frame On RMA Application Default View Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_DefaultView_Frm_GeneralClaim(WebDriver driver)
{
	Element = driver.findElement(By.id("Document-2-1claimgcGeneral ClaimGeneral Claim (New)FalseFalse")); //Unique Id  Of General Claim Frame On RMA Application Default View Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DefaultView_Frm_LineOfBusinessParameterSetUp
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Line Of Business Parameter SetUp Frame On RMA Application Default View Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-19-2015                                 
// ============================================================================================
public static WebElement RMAApp_DefaultView_Frm_LineOfBusinessParameterSetUp(WebDriver driver)
{
	Element = driver.findElement(By.id("UtilitieszLOBParametersLOBParametersLine Of Business Parameter SetupUI/Utilities/Manager/Line Of Business Parameter SetupFalseFalse")); //Unique Id  Of  Line Of Business Parameter SetUp Frame On RMA Application Default View Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_DefaultView_Lnk_LogOut
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which LogOut Link On RMA Application Default View Page Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-20-2015                                 
// ============================================================================================
public static WebElement RMAApp_DefaultView_Lnk_LogOut(WebDriver driver)
{
	Element = driver.findElement(By.id("cphMainBody_Loginstatus1")); //Unique Id  Of  LogOut Link On RMA Application Default View Page Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_LeftHandNavTree_Img_GCExpander
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which GC Expander On Left Hand Navigation Tree Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static WebElement RMAApp_LeftHandNavTree_Img_GCExpander(WebDriver driver)
{
	Element = driver.findElement(By.xpath(".//*[@id='navTree']/table[6]/tbody/tr[2]/td[3]/a/img")); // Unique Id Of Expander On Left Hand Navigation Tree Is Fetched
	return Element;
}

//============================================================================================
//FunctionName 			: RMAApp_LeftHandNavTree_Lnk_FinancialReserves
//Description  			: To Fetch Unique Property (Such As Id, Xpath, Name ) On The Basis Of Which Financial/Reserves Link On Left Hand Navigation Tree Can Be Identified
//Input Parameter 		: Driver Variable Of The Type WebDriver		 
//Revision				: 0.0 - KumudNaithani-10-24-2015                                 
// ============================================================================================
public static WebElement RMAApp_LeftHandNavTree_Lnk_FinancialReserves(WebDriver driver)
{
	Element = driver.findElement(By.xpath(".//*[@id='navTreet10']/span")); // Unique Id Of Financial/ Reserves Link On Left Hand Navigation Tree Is Fetched
	return Element;
}
}
