package rmaseleniumutilties;

import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;
//Default Package Import Completed

public class RMA_RandomCharGenerate_Utility {
static int IntRandomNum;
static String StrGeneratedUCaseRandomString;

//============================================================================================
//FunctionName 		: RMA_RandomIntGeneration_Utility
//Description  		: To Generate A Random Number Based On The Maximum And Minimum Integer Limit Provided
//Input Parameter 	: IntMinRange, IntMaxRange Of The Type Integer
//Revision			: 0.0 - KumudNaithani-11-02-2015                                 
//============================================================================================
public static int RMA_RandomIntGeneration_Utility(int IntMinRange, int IntMaxRange) 
{
	// To Concatenate The Return Value To A Fixed Desired Starting Value User Needs To Call This Function Store The Value In A Variable And Then Concatenate Using + Operator
try 
{
	 Random rand = new Random();
	 IntRandomNum = rand.nextInt((IntMaxRange - IntMinRange) + 1) + IntMinRange; // Since nextInt Is Exclusive Of The Maximum Value So 1 Is Added To Make It Inclusive	
} catch (Exception e) {
	System.out.println("Random Number Was Not Generated As" + e.getMessage()); //Try Catch Statement Is Used To Handle Any Type Of Exception And Print Log Of It
}
return IntRandomNum;
}

//============================================================================================
//FunctionName 		: RMA_RandomStringGeneration_Utility
//Description  		: To Generate A Random String
//Input Parameter 	: IntStringLength Of The Type Integer
//Revision			: 0.0 - KumudNaithani-11-02-2015                                 
//============================================================================================
public static String RMA_RandomStringGeneration_Utility(int IntStringLength) 
{	
//To Concatenate The Return Value To A Fixed Desired Starting Value User Needs To Call This Function Store The Value In A Variable And Then Concatenate Using + Operator
try
{
	 String GeneratedRandomString = RandomStringUtils.random(IntStringLength, true, false);
	 StrGeneratedUCaseRandomString = GeneratedRandomString.toUpperCase();
} catch (Exception e) {
	System.out.println("Random String Was Not Generated As" + e.getMessage()); //Try Catch Statement Is Used To Handle Any Type Of Exception And Print Log Of It
}
return StrGeneratedUCaseRandomString;
}
}


