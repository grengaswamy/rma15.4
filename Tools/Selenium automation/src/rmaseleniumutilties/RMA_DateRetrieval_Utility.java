package rmaseleniumutilties;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
//Default Package Import Completed

public class RMA_DateRetrieval_Utility {
	
//============================================================================================
//FunctionName 		: RMA_CurrentDateInfo_Utility
//Description  		: To Retrieve The Current System Date in "yyyyMMdd"
//Input Parameter 	: None 
//Revision			: 0.0 - KumudNaithani-10-21-2015                                 
// ============================================================================================
public static String RMA_CurrentDateInfo_Utility()
{
	DateFormat dateformat = new SimpleDateFormat("yyyyMMdd"); //Date Format Is Defined
	Date date = new Date();
    String Strdated = dateformat.format(date); //Retrieved Date Is Formatted
    return Strdated;
}

//============================================================================================
//FunctionName 		: RMA_CurrentTimeInfo_Utility
//Description  		: To Retrieve The Current System Time in "hhmmss" format 
//Input Parameter 	: None 
//Revision			: 0.0 - KumudNaithani-10-22-2015                                 
//============================================================================================
public static String RMA_CurrentTimeInfo_Utility()
{
DateFormat dateformat = new SimpleDateFormat("hhmmss"); //Date Format Is Defined
Date date = new Date();
String Strdated = dateformat.format(date); //Retrieved Date Is Formatted
return Strdated;
}
}




 
