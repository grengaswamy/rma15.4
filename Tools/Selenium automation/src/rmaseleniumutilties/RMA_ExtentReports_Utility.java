package rmaseleniumutilties;

import com.relevantcodes.extentreports.ExtentReports;
//Default Package Import Completed

public class RMA_ExtentReports_Utility
//============================================================================================
//FunctionName 		: RMA_ExtentReportsInstance_Utility
//Description  		: To Generate Test Case Execution Report Using Extent Report Reporting FrameWork And Store It In Desired Path
//Input Parameter 	: None
//Revision			: 0.0 - KumudNaithani-10-16-2015                                 
//============================================================================================
{
public static ExtentReports RMA_ExtentReportsInstance_Utility()
{
	ExtentReports extent;
	String RMAApp_ExtentReportsLocation;
	String RMAApp_ExtentReportsTitle;
	String RMAApp_ExtentReportsName;
	String RMAApp_ExtentReportsHeadLine;
	String RMAApp_ExtentReportsHtmlName;
	String dtmCurrentTimeInfo;
	String dtmCurrentDateInfo;
		
	dtmCurrentDateInfo = RMA_DateRetrieval_Utility.RMA_CurrentDateInfo_Utility();
	dtmCurrentTimeInfo = RMA_DateRetrieval_Utility.RMA_CurrentTimeInfo_Utility();
		
	RMA_ExcelDataRetrieval_Utility ExcelData = new RMA_ExcelDataRetrieval_Utility("D:\\RMASeleniumAutomationTestData.xlsx"); //Excel WorkBook RMASeleniumAutomationTestData IS Fetched To Retrieve Data 
	RMAApp_ExtentReportsHtmlName = ExcelData.RMA_ExcelDataRead_Utility(0, 1, 5); //ExtentReportHTMLName Fetched From DataSheet RMA_Utilities

	RMAApp_ExtentReportsLocation = ExcelData.RMA_ExcelDataRead_Utility(0, 1, 1) + RMAApp_ExtentReportsHtmlName + "_"+ dtmCurrentDateInfo + "_" + dtmCurrentTimeInfo +  ".html"; //ExtentReport Location Fetched From DataSheet RMA_Utilities
	RMAApp_ExtentReportsTitle = ExcelData.RMA_ExcelDataRead_Utility(0, 1, 2); //ExtentReport Title Fetched From DataSheet RMA_Utilities
	RMAApp_ExtentReportsName = ExcelData.RMA_ExcelDataRead_Utility(0, 1, 3); //ExtentReport Name Fetched From DataSheet RMA_Utilities
	RMAApp_ExtentReportsHeadLine = ExcelData.RMA_ExcelDataRead_Utility(0, 1, 4); //ExtentReport HeadLine Fetched From DataSheet RMA_Utilities
	RMAApp_ExtentReportsHtmlName = ExcelData.RMA_ExcelDataRead_Utility(0, 1, 5); //ExtentReportHTMLName Fetched From DataSheet RMA_Utilities
		
	extent = new ExtentReports (RMAApp_ExtentReportsLocation, true);
	extent.config().documentTitle(RMAApp_ExtentReportsTitle).reportName(RMAApp_ExtentReportsName).reportHeadline(RMAApp_ExtentReportsHeadLine);
	return extent;	
	}
}
