package rmaseleniumutilties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
//Default Package Import Completed

public class RMA_WindowsPopHandler_Utility
{
//============================================================================================
//FunctionName 		: RMA_WindowsMessageHandler_Utility
//Description  		: Handles Any Type Of Windows Pop Up And Accespts Or Rejects The Same (Say Clicking On Yes Or No Button) 
//Input Parameter 	: Instance Of The WebDriver Class 'Driver' And Value Of The String 'StrAccept'
//Revision			: 0.0 - KumudNaithani-10-20-2015                                 
// ============================================================================================
public static void RMA_WindowsMessageHandler_Utility(WebDriver driver, String StrAccept) 
{
	Alert alert;
	try {
		alert = driver.switchTo().alert(); // A Switch To The Displayed Alert Message Is Done
		if (StrAccept == "Yes") // If The User Chooses To Accept The Alert That Is The Positive Option Say "Yes", "OK" Or "Leave The Page" Then Alert Window Is Accepted And Desired Action Is Done Like Refereshing The Page
		{
			//Thread.sleep(10000);
			alert.accept(); // If The User Chooses To Decline The Alert That Is The Negative Option Say "No", "Cancel" Or "Stay On The Page" Then Alert Window Is Dismissed And Desired Action Is Done Like Staying On The Page
			//Thread.sleep(10000);
				
		}
		else
		{
			alert.dismiss(); // If The User Chooses To Decline The Alert That Is The Negative Option Say "No", "Cancel" Or "Stay On The Page" Then Alert Window Is Dismissed And Desired Action Is Done Like Staying On The Page
		} 
		}
	catch (Exception e) 
		{
			System.out.println("Exception Occurred While Handling The Windows PopUp" + e.getMessage()); //Try Catch Statement Is Used To Handle Any Type Of Unhandled Exception And Print Log Of It
		}
		
	}
}
