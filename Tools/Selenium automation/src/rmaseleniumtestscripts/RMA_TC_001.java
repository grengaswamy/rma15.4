package rmaseleniumtestscripts;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
//Default Package Import Completed

import rmaseleniumPOM.RMA_Selenium_POM_Login_DSNSelect_Frames;
import rmaseleniumutilties.RMA_ExcelDataRetrieval_Utility;
import rmaseleniumutilties.RMA_FailureScreenCapture_Utility;
//RMA Package Import Completed

public class RMA_TC_001 extends rmaseleniumtestscripts.RMA_TC_BaseTest{	
static String ExceptionRecorded;
static String []ErrorMessage;
static String FinalErrorMessage;
@Test
public void RMAApp_login() throws Exception 
{
try{
	logger = reports.startTest("TC_001_RiskMaster Title Verification", "Application Is Logged And Title Is Verfied");
	String RMAApp_Login_Txt_UserName;
	String RMAApp_Login_Txt_Password;
	String StrRMAApplicationExpectedTitle;
	String StrRMAApplicationActualTitle;
	//Local Variable Declaration

	StrRMAApplicationExpectedTitle = "RISKMASTER Database and View Selection";
				
	RMA_ExcelDataRetrieval_Utility ExcelData = new RMA_ExcelDataRetrieval_Utility(System.getProperty("user.dir")+"\\RMASeleniumTestDataSheets\\RMASeleniumAutomationTestData.xlsx"); //Excel WorkBook RMASeleniumAutomationTestData IS Fetched To Retrieve Data 
	RMAApp_Login_Txt_UserName = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_001", 1, 0); //UserName Fetched From DataSheet RMA_TC_001
	RMAApp_Login_Txt_Password = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_001", 1, 1); //Password Fetched From DataSheet RMA_TC_001
				
	logger.log(LogStatus.INFO, "Rismaster Application URL Is Provided And Application Is Started");
	RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_Login_Txt_UserName(driver).sendKeys(RMAApp_Login_Txt_UserName); //Enter UserName In UserName TextBox Of RMA Application Login Page
	logger.log(LogStatus.INFO, "UserName Provided In UserName TextBox On RMA Application Login Page Is" + " "+ "::" + " " +  RMAApp_Login_Txt_UserName.toUpperCase());		
				
	RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_Login_Txt_PassWord(driver).sendKeys(RMAApp_Login_Txt_Password); //Enter PassWord In PassWord TextBox Of RMA Application Login Page
	logger.log(LogStatus.INFO, "PassWord Provided In PassWord TextBox On RMA Application Login Page Is" + " " + "::"+ " "+  RMAApp_Login_Txt_Password.toUpperCase() );
				
	RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_Login_Btn_Login(driver).click(); // Click On The Login Button On RMA Application Login Page
	logger.log(LogStatus.INFO, "Login Button Is Clicked On RMA Application Login Page");
	Thread.sleep(10000);
	StrRMAApplicationActualTitle =  driver.getTitle();  //Derieve The Title Of The RMA Application Login Page
				
	Assert.assertEquals( StrRMAApplicationExpectedTitle,StrRMAApplicationActualTitle);
	logger.log(LogStatus.PASS, "Title Of The RMA Application Is As The Expected Title RISKMASTER Database and View Selection");
	reports.endTest(logger);
	
	}catch(Exception e){
		ExceptionRecorded = e.getMessage();	//Try Catch Statement Is Used To Handle Any Type Of Unhandled Exception And Print Log Of It
		if (ExceptionRecorded.contains("Command"))
		{
		ErrorMessage = ExceptionRecorded.split("Command");
		FinalErrorMessage = ErrorMessage[0];
		}
		else
		{
			FinalErrorMessage = ExceptionRecorded;
		}
		throw (e);
	}
	}

@AfterMethod
public void RMA_FailureReport(ITestResult result) //All The Information Associated With The Test Case Is Stored In Result Variable
{
	String StrScreenShotLocation;
	String StrScreenShotTCName;
	StrScreenShotTCName = "RMA_TC_001";
	if (ITestResult.FAILURE == result.getStatus())
	{
		StrScreenShotLocation = RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName );
		logger.log(LogStatus.FAIL, "Following Error Occurred While Exceuting TestCase - RMA_TC_001 And Hence The Test Case Is A Fail::" + FinalErrorMessage);
		logger.log(LogStatus.INFO, "ScreenShot Of The Application Is Stored At Following Path In Local Machine::" + " " + StrScreenShotLocation);
		logger.log(LogStatus.INFO, logger.addScreenCapture(RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName)));
	}
}
}
