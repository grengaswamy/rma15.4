package rmaseleniumtestscripts;

import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
//Default Package Import Completed
import rmaseleniumPOM.RMA_Selenium_POM_Login_DSNSelect_Frames;
import rmaseleniumPOM.RMA_Selenium_POM_FinancialReserves;
import rmaseleniumutilties.RMA_ExcelDataRetrieval_Utility;
import rmaseleniumutilties.RMA_FailureScreenCapture_Utility;
import rmaseleniumutilties.RMA_WindowsPopHandler_Utility;
//RMA Package Import Completed

public class RMA_TC_006 extends rmaseleniumtestscripts.RMA_TC_BaseTest{	
	static String ExceptionRecorded;
	static String []ErrorMessage;
	static String FinalErrorMessage;
@Test
public void PaymentAddition() throws Exception
{
	try {
		logger = reports.startTest("TC_006_Payment Addition", "User Makes A New  Payment");
		String RMAApp_Payment_Lst_BankAccount;
		String RMAApp_Payment_Lst_PayeeType;
		String RMAApp_FundsSplitDetails_Lst_TransactionType;
		int RMAApp_FundsSplitDetails_Txt_Amount;
		String StrPrimaryWindowHandle;
		String RMAApp_Payment_Txt_LastName;
		String StrSecPrimaryWindowHandle;
		String StrControlNumber;
		String StrAccept;
		//Local Variable Declaration
		
		StrAccept = "Yes";
		
		RMA_ExcelDataRetrieval_Utility ExcelData = new RMA_ExcelDataRetrieval_Utility(System.getProperty("user.dir")+"\\RMASeleniumTestDataSheets\\RMASeleniumAutomationTestData.xlsx"); //Excel WorkBook RMASeleniumAutomationTestData IS Fetched To Retrieve Data 
		RMAApp_Payment_Lst_BankAccount = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_006", 1, 0); //Bank Account Is Fetched From DataSheet RMA_TC_006
		RMAApp_Payment_Lst_PayeeType = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_006", 1, 1); // Payee Type Is Fetched From DataSheet RMA_TC_006
		RMAApp_FundsSplitDetails_Lst_TransactionType = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_006", 1, 2); //Transaction Type Is Fetched From DataSheet RMA_TC_006
		RMAApp_FundsSplitDetails_Txt_Amount = ExcelData.RMA_ExcelNumberDataRead_Utility("RMA_TC_006", 1, 3); //Amount Is Fetched From DataSheet RMA_TC_006
		RMAApp_Payment_Txt_LastName = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_006", 1, 4); //LastName Is Fetched From DataSheet RMA_TC_006

		RMA_Selenium_POM_FinancialReserves.RMAApp_FinReserves_Btn_AddPayment(driver).click(); //Add Payment Button On Financial Reserves Screen Is Created
		logger.log(LogStatus.INFO, "Add Payment Button Is Clicked On Reserve Creation Page");
		
		Select bankdropdown = new Select (RMA_Selenium_POM_FinancialReserves.RMAApp_Payment_Lst_BankAccount(driver));
		bankdropdown.selectByVisibleText(RMAApp_Payment_Lst_BankAccount); //Value Is Entered In Bank Account Drop Down List On Payment Creation Page
		logger.log(LogStatus.INFO, "Value Entered In Bank Account Drop Down List On Payment Creation Page Is" + " " + "_" + " " + RMAApp_Payment_Lst_BankAccount.toUpperCase());
		
		Thread.sleep(3000);
		Select payeedropdown = new Select (RMA_Selenium_POM_FinancialReserves.RMAApp_Payment_Lst_PayeeType(driver)); //Value Is Entered In Payee Type Drop Down List On Payment Creation Page
		payeedropdown.selectByVisibleText(RMAApp_Payment_Lst_PayeeType);
		logger.log(LogStatus.INFO, "Value Entered In Payee Type Drop Down List On Payment Creation Page Is" + " " + "_" + " "  + RMAApp_Payment_Lst_PayeeType.toUpperCase());
		
		Thread.sleep(7000);
		RMA_Selenium_POM_FinancialReserves.RMAApp_Payment_Txt_LastName(driver).sendKeys(RMAApp_Payment_Txt_LastName); //Value Is Entered In Last Name Field Of Paymnet Creation Page
		RMA_Selenium_POM_FinancialReserves.RMAApp_Payment_Txt_LastName(driver).sendKeys(Keys.TAB);
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "Value Entered In Last Name Text Box On Payment Creation Page Is" + " " + "_" + " "  + RMAApp_Payment_Txt_LastName.toUpperCase());
		
		StrSecPrimaryWindowHandle = driver.getWindowHandle(); //Window Handle Of The Current Window Is Fetched
		Set<String> StrSecHandles = driver.getWindowHandles(); //Entire Set Of The Present Window Handles Is Fetched
		Iterator<String>seciterator = StrSecHandles.iterator(); //Entire Set Of The Present Window Handles Is Iterated
		while (seciterator.hasNext())
		{
			String SecChildWindow = seciterator.next();
			if (!StrSecPrimaryWindowHandle.equalsIgnoreCase(SecChildWindow)) //If The Fetched Child Window Is Not Equal To the Parent Window Then A Switch To The Child Window Is Done
			{
				driver.switchTo().window(SecChildWindow);
				Thread.sleep(10000);
			}
		}
		
		RMA_Selenium_POM_FinancialReserves.RMAApp_LastName_Lst_LastNameSelection(driver).sendKeys(Keys.ARROW_DOWN); //Intended Last Name Is Selected From LastName Selector On Last Name Selection Window On Payment Creation Page
		logger.log(LogStatus.INFO, "Intended Last Name Selected From LastName Selector On Last Name Selection Window On Payment Creation Page Is" + " " + "_" + " "  + RMAApp_Payment_Txt_LastName.toUpperCase());
		
		RMA_Selenium_POM_FinancialReserves.RMAApp_LastName_Btn_ChooseHighLighted(driver).click(); //Choose Highlighted Button Is Clicked On Last Name Selection Window On Payment Creation Page
		Thread.sleep(5000);
		logger.log(LogStatus.INFO, "Choose Highlighted Button Is Clicked On Last Name Selection Window On Payment Creation Page");
		
		driver.switchTo().window(StrSecPrimaryWindowHandle); //A Switch To The Parent Window Is Done
		driver.switchTo().frame(RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Frm_MenuOptionFrame(driver)); //A Switch To The Frame Containing RMA Application Menu Option Is Done
		Thread.sleep(10000);
		driver.switchTo().frame(2); //A Switch To The Frame Containing Financial/Reserves Control Is Done
		Thread.sleep(15000);
		RMA_Selenium_POM_FinancialReserves.RMAApp_Payment_Tab_TransactionDetail(driver).click(); //Transaction Details Tab On Payment Creation Page Is Clicked
		logger.log(LogStatus.INFO, "Transaction Details Tab On On Payment Creation Page Is Clicked");
		
		Thread.sleep(5000);
		RMA_Selenium_POM_FinancialReserves.RMAApp_Payment_Btn_AddNewPayment(driver).click(); //Add New Payment Button On Transaction Details Tab Of Payment Creation Page Is Clicked
		Thread.sleep(5000);
		StrPrimaryWindowHandle = driver.getWindowHandle(); //Window Handle Of The Current Window Is Fetched
		Set<String> strWindowHandles = driver.getWindowHandles(); //Entire Set Of The Present Window Handles Is Fetched
		Iterator<String>windowiterator = strWindowHandles.iterator(); //Entire Set Of The Present Window Handles Is Iterated
		while (windowiterator.hasNext())
		{
			String ChildWindow = windowiterator.next();
			if (!StrPrimaryWindowHandle.equalsIgnoreCase(ChildWindow)) //If The Fetched Child Window Is Not Equal To the Parent Window Then A Switch To The Child Window Is Done
			{
				driver.switchTo().window(ChildWindow);
				Thread.sleep(10000);
			}
		}
			
		RMA_Selenium_POM_FinancialReserves.RMAApp_FundsSplitDetails_Lst_TransactionType(driver).sendKeys(RMAApp_FundsSplitDetails_Lst_TransactionType); //Value Is Entered In Transaction Type Drop Down On Funds Details Window On Payment Creation Page
		RMA_Selenium_POM_FinancialReserves.RMAApp_FundsSplitDetails_Lst_TransactionType(driver).sendKeys(Keys.TAB);
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "Value Entered In Transaction Type DropDown On Funds Details Window On Payment Creation Page Is" + " " + "_" + " "  + RMAApp_FundsSplitDetails_Lst_TransactionType);
		
		RMA_Selenium_POM_FinancialReserves.RMAApp_FundsSplitDetails_Txt_Amount(driver).clear();
		RMA_Selenium_POM_FinancialReserves.RMAApp_FundsSplitDetails_Txt_Amount(driver).sendKeys(String.valueOf(RMAApp_FundsSplitDetails_Txt_Amount)); //Value Is Entered In Amount TextBox On Funds Details Window On Payment Creation Page
		logger.log(LogStatus.INFO, "Value Entered In Amount TextBox On Funds Details Window On Payment Creation Page Is" + " " + "_" + " "  + RMAApp_FundsSplitDetails_Txt_Amount);
		
		RMA_Selenium_POM_FinancialReserves.RMAApp_FundsSplitDetails_Btn_OK(driver).click(); // OK Button Is Clicked  On Funds Details Window On Payment Creation Page
		logger.log(LogStatus.INFO, "OK Button Is Clicked  On Funds Details Window On Payment Creation Page");
		
		Thread.sleep(10000);
		driver.switchTo().window(StrPrimaryWindowHandle);
		driver.switchTo().frame(RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Frm_MenuOptionFrame(driver)); //A Switch To The Frame Containing RMA Application Menu Option Is Done
		Thread.sleep(5000);
		driver.switchTo().frame(2); //A Switch To The Frame Containing Financial/Reserves Control Is Done
		Thread.sleep(5000);
		RMA_Selenium_POM_FinancialReserves.RMAApp_Payment_Btn_Save(driver).click(); // Save Button Is Clicked On Payment Creation Page
		logger.log(LogStatus.INFO, "Save Button Is Clicked On Payment Creation Page");
		
		RMA_Selenium_POM_FinancialReserves.RMAApp_Payment_Tab_Transaction(driver).click(); // Transaction Tab On Payment Creation Page Is Clicked
		logger.log(LogStatus.INFO, "Transaction Tab On Payment Creation Page Is Clicked");
		StrControlNumber = RMA_Selenium_POM_FinancialReserves.RMAApp_Payment_Txt_ControlNumber(driver).getAttribute("value"); //Generated Control Number Is Stored In Variable StrControlNumber
		
		Assert.assertFalse(StrControlNumber.isEmpty());
		logger.log(LogStatus.PASS, "Payment Is Suceessfully Done And The Generated Control Number Is" + " " + "_"+ " " + StrControlNumber);

		RMA_ExcelDataRetrieval_Utility.WriteDataToExcel("D:\\RMASeleniumAutomationTestData.xlsx","RMA_TC_006", 10, 0, StrControlNumber);;  //Control Number Is Written In RMA_TC_006 Excel Sheet
		logger.log(LogStatus.INFO, "Generated ControlNumber:" + " " + StrControlNumber + " " + "Is Also Written In The Corresponding Excel Data Sheet RMA_TC_006");
		driver.navigate().refresh(); //Web page Is Refeshed
		logger.log(LogStatus.INFO, "Web Page Is Refeshed");
		
		Thread.sleep(10000);	
		RMA_WindowsPopHandler_Utility.RMA_WindowsMessageHandler_Utility(driver, StrAccept); //Alert Window Is Refeshed
		logger.log(LogStatus.INFO, "Windows PopUp That Appears On Web Page Refresh Is Accepted And WebPage Is Leaved");
		
		Thread.sleep(10000);
		driver.switchTo().parentFrame(); // A Switch To The Parent Frame Containing Menu Options Is Created 
		reports.endTest(logger);
	} catch (Exception e) {
		ExceptionRecorded = e.getMessage();	//Try Catch Statement Is Used To Handle Any Type Of Unhandled Exception And Print Log Of It
		if (ExceptionRecorded.contains("Command"))
		{
		ErrorMessage = ExceptionRecorded.split("Command");
		FinalErrorMessage = ErrorMessage[0];
		}
		else
		{
			FinalErrorMessage = ExceptionRecorded;
		}
		throw (e);
	}
}

@AfterMethod
public void RMA_FailureReport(ITestResult result) //All The Information Associated With The Test Case Is Stored In Result Variable
{
	String StrScreenShotLocation; 
	String StrScreenShotTCName = "RMA_TC_006";
	if (ITestResult.FAILURE == result.getStatus())
	{
		StrScreenShotLocation = RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName);
		logger.log(LogStatus.FAIL, "Following Error Occurred While Executing Test Case - RMA_TC_006 And Hence The TestCase Is A Fail  " + "::" + FinalErrorMessage);
		logger.log(LogStatus.INFO, "ScreenShot Of The Application Is Stored At Following Path In Local Machine:" + " " +  StrScreenShotLocation);
		logger.log(LogStatus.INFO, logger.addScreenCapture(RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName)));
	}
}
}
