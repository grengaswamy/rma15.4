package rmaseleniumtestscripts;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
//Default Package Import Completed

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import rmaseleniumutilties.RMA_ExtentReports_Utility;
//RMA Package Import Completed

public class RMA_TC_BaseTest {
public static WebDriver driver;
public static ExtentReports reports;
public static ExtentTest logger; 
	
@BeforeSuite
 public void SetUp()
	{
	try {
		System.setProperty("webdriver.ie.driver", "C:\\Selenium Softwares\\IEDriverServer_Win32_2.48.0\\IEDriverServer.exe");
		driver = new InternetExplorerDriver(); // Driver Instance Creation
		reports = RMA_ExtentReports_Utility.RMA_ExtentReportsInstance_Utility(); 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //Implicit Wait Added For The Page To Load Successfully
		driver.navigate().to("http://20.201.110.254/RiskmasterUI/UI/Home/Login.aspx/"); //Navigation To The RMA Application Login Page
		driver.manage().window().maximize(); //Maximizing The RMA Application Login Page		
	} catch (Exception e) {
		System.out.println("Exception Occurred While Initilaizing The Application URL " + e.getMessage()); //Try Catch Statement Is Used To Handle Any Type Of Unhandled Exception And Print Log Of It
	}
	}

@AfterSuite
	public void ExitSetUp()
	{
		reports.flush();
		reports.close();
		driver.quit();
	}
}