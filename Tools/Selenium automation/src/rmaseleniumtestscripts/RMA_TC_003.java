package rmaseleniumtestscripts;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
//Default Package Import Completed
import rmaseleniumPOM.RMA_Selenium_POM_Document_Event;
import rmaseleniumPOM.RMA_Selenium_POM_Login_DSNSelect_Frames;
import rmaseleniumutilties.RMA_ExcelDataRetrieval_Utility;
import rmaseleniumutilties.RMA_FailureScreenCapture_Utility;
import rmaseleniumutilties.RMA_WindowsPopHandler_Utility;
//RMA Package Import Completed

public class RMA_TC_003 extends rmaseleniumtestscripts.RMA_TC_BaseTest {
	static String ExceptionRecorded;
	static String []ErrorMessage;
	static String FinalErrorMessage;
@Test 
public void EventCreation() throws Exception 
{
	try {
		logger = reports.startTest("TC_003_Event Creation", "A New Event Is Created And The Created Event's Event Number Is Stored In A Global Variable");
			
		String RMAApp_EventCreation_Txt_EventType;
		String RMAApp_EventCreation_Txt_DepteId;
		int RMAApp_EventCreation_Txt_TimeOfEvent;
		String RMAApp_EventCreation_Txt_EventStatus;
		int RMAApp_EventCreation_Txt_DateReported;
		int RMAApp_EventCreation_Txt_TimeReported;
		int RMAApp_EventCreation_Txt_DateOfEvent;
		String StrEventNumber; 
		String StrAccept;
		//Local Variable Declaration
			
		RMA_ExcelDataRetrieval_Utility ExcelData = new RMA_ExcelDataRetrieval_Utility(System.getProperty("user.dir")+"\\RMASeleniumTestDataSheets\\RMASeleniumAutomationTestData.xlsx"); //Excel WorkBook RMASeleniumAutomationTestData IS Fetched To Retrieve Data 
		RMAApp_EventCreation_Txt_EventType = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_003", 1, 0); //TypeOfEvent Is Fetched From DataSheet RMA_TC_003
		RMAApp_EventCreation_Txt_DepteId = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_003", 1, 6); //Dept Id Is Fetched From DataSheet RMA_TC_003
		RMAApp_EventCreation_Txt_EventStatus =  ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_003", 1, 3); //Event Status Is Fetched From DataSheet RMA_TC_003
		RMAApp_EventCreation_Txt_TimeOfEvent = ExcelData.RMA_ExcelNumberDataRead_Utility("RMA_TC_003", 1, 2); //TimeOfEvent Is Fetched From DataSheet RMA_TC_003
		RMAApp_EventCreation_Txt_DateReported = ExcelData.RMA_ExcelNumberDataRead_Utility("RMA_TC_003", 1, 4); //DateOfReporting Is Fetched From DataSheet RMA_TC_003
		RMAApp_EventCreation_Txt_TimeReported = ExcelData.RMA_ExcelNumberDataRead_Utility("RMA_TC_003", 1, 5); //TimeOfReporting Is Fetched From DataSheet RMA_TC_003
		RMAApp_EventCreation_Txt_DateOfEvent = ExcelData.RMA_ExcelNumberDataRead_Utility("RMA_TC_003", 1, 1); //DateOfEvent Is Fetched From DataSheer RMA_TC_003
		
		StrAccept = "Yes";
		driver.switchTo().frame(RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Frm_MenuOptionFrame(driver)); //A Switch To The Frame Containing RMA Application Menu Option Is Done
		RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Mnu_Options(driver, 1).click(); //Document Menu Option Is Selected
		logger.log(LogStatus.INFO, "Document Menu Option Is Clicked On RISKMASTER Default View Page");
			
		RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_SubMnu_Options(driver, 1, 1).click(); //Event Menu Option Is Selected
		logger.log(LogStatus.INFO, "Document-->Event Menu Option Is Clicked On RISKMASTER Default View Page");
			
		driver.switchTo().frame(RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_FrmEventCreation(driver)); //A Switch To The Frame Containing Event Creation Controls IS Done
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_EventType(driver).sendKeys(RMAApp_EventCreation_Txt_EventType);
		Thread.sleep(5000);
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_EventType(driver).sendKeys(Keys.TAB);
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "Event Type Is Typed In Event Type TextBox On Event Creation Page" + " "+ "::" + " "+ RMAApp_EventCreation_Txt_EventType.toUpperCase() );
			
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_DepteId(driver).sendKeys(RMAApp_EventCreation_Txt_DepteId);
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_DepteId(driver).sendKeys(Keys.TAB);
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "DeptId Is Typed In DeptId TextBox On Event Creation Page" + " " + "::" + " " + RMAApp_EventCreation_Txt_DepteId.toUpperCase());
			
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_TimeOfEvent(driver).sendKeys(String.valueOf(RMAApp_EventCreation_Txt_TimeOfEvent));
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "TimeOfEvent Is Typed In TimeOfEvent TextBox On Event Creation Page" + " " + "::"+ " "+ RMAApp_EventCreation_Txt_TimeOfEvent);
			
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_EventStatus(driver).sendKeys(RMAApp_EventCreation_Txt_EventStatus);
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_EventStatus(driver).sendKeys(Keys.TAB);
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "EventStatus Is Typed In EventStatus TextBox On Event Creation Page" +" " + "::" + "" + RMAApp_EventCreation_Txt_EventStatus.toUpperCase());
			
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_DateReported(driver).sendKeys(String.valueOf(RMAApp_EventCreation_Txt_DateReported));
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "DateReported Is Typed In DateReported TextBox On Event Creation Page" + "" + "::" + RMAApp_EventCreation_Txt_DateReported);
			
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_TimeReported(driver).sendKeys(String.valueOf(RMAApp_EventCreation_Txt_TimeReported));
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "TimeReported Is Typed In TimeReported TextBox On Event Creation Page" + " " + "::" + " "+ RMAApp_EventCreation_Txt_TimeReported );
			
		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_DateOfEvent(driver).sendKeys(String.valueOf(RMAApp_EventCreation_Txt_DateOfEvent));
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "DateOfEvent Is Typed In DateOfEvent TextBox On Event Creation Page" + " " + "::"+ " "+ RMAApp_EventCreation_Txt_DateOfEvent );

		RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Btn_Save(driver).click();
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "Save Button Is Clicked On Event Creation Page");
			
		StrEventNumber = RMA_Selenium_POM_Document_Event.RMAApp_EventCreation_Txt_EvntNumber(driver).getAttribute("value"); //Value Of the Generated Event Number Is Fetched In The Variable StrEventNumber
		logger.log(LogStatus.INFO, "Value Of the Generated Event Number Is" + " " + "::"+ " "+ StrEventNumber );

		Assert.assertTrue(StrEventNumber.contains("EVT"));
		logger.log(LogStatus.PASS, "Valid Event Number Is Generated That Is" + " " + "::"+ " "+ StrEventNumber );
			
		driver.navigate().refresh(); //Web page Is Refeshed
		logger.log(LogStatus.INFO, "Web Page Is Refeshed");
		
		Thread.sleep(10000);	
		RMA_WindowsPopHandler_Utility.RMA_WindowsMessageHandler_Utility(driver, StrAccept); //Alert Window Is Refeshed
		logger.log(LogStatus.INFO, "Windows PopUp That Appears On Web Page Refresh Is Accepted And WebPage Is Leaved");
		
		Thread.sleep(10000);	
		driver.switchTo().parentFrame(); // A Switch To The Parent Frame Containing Menu Options Is Created 
		RMA_ExcelDataRetrieval_Utility.WriteDataToExcel("D:\\RMASeleniumAutomationTestData.xlsx","RMA_TC_003", 10, 0, StrEventNumber);
		logger.log(LogStatus.INFO, "Generated Event Number:" + " " + StrEventNumber + " " + "Is Also Written In The Corresponding Excel Data Sheet RMA_TC_003");
		reports.endTest(logger);
		
	} catch (Exception e) {
		ExceptionRecorded = e.getMessage();	//Try Catch Statement Is Used To Handle Any Type Of Unhandled Exception And Print Log Of It
		if (ExceptionRecorded.contains("Command"))
		{
		ErrorMessage = ExceptionRecorded.split("Command");
		FinalErrorMessage = ErrorMessage[0];
		}
		else
		{
			FinalErrorMessage = ExceptionRecorded;
		}
		throw (e);
	}
	}

@AfterMethod
public void RMA_FailureReport(ITestResult result) //All The Information Associated With The Test Case Is Stored In Result Variable
{
	String StrScreenShotLocation; 
	String StrScreenShotTCName = "RMA_TC_003";
	if (ITestResult.FAILURE == result.getStatus())
	{
		StrScreenShotLocation = RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName);
		logger.log(LogStatus.FAIL, "Following Error Occurred While Executing Test Case - RMA_TC_003 And Hence The TestCase Is A Fail  " + "::" + FinalErrorMessage);
		logger.log(LogStatus.INFO, "ScreenShot Of The Application Is Stored At Path In Local Machine", StrScreenShotLocation);
		logger.log(LogStatus.INFO, logger.addScreenCapture(RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName)));
	}
}
}
