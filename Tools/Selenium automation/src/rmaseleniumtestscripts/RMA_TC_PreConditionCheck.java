package rmaseleniumtestscripts;

import org.openqa.selenium.support.ui.Select;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
//Default Package Import Completed

import rmaseleniumPOM.RMA_Selenium_POM_Utilities_LOB;
import rmaseleniumPOM.RMA_Selenium_POM_Login_DSNSelect_Frames;
import rmaseleniumutilties.RMA_ExcelDataRetrieval_Utility;
import rmaseleniumutilties.RMA_FailureScreenCapture_Utility;
import rmaseleniumutilties.RMA_WindowsPopHandler_Utility;
//RMA Package Import Completed

public class RMA_TC_PreConditionCheck extends rmaseleniumtestscripts.RMA_TC_BaseTest
{
	static String ExceptionRecorded;
	static String []ErrorMessage;
	static String FinalErrorMessage;
@Test
public void RMAApp_TC_PreCondition() throws Exception
{
	try {
		logger = reports.startTest("RMAApp_TC_PreConditionCheck_PreCondition Enabling", "PreConditions That Are Required To Execute And Test A Particular Set Of TestConditions Are Enabled");
		String StrRMAApp_LOB_Lst_LineOfBusiness;
		String StrAccept;
		//Local Variable Declaration
				
		StrAccept = "Yes";
				
		RMA_ExcelDataRetrieval_Utility ExcelData = new RMA_ExcelDataRetrieval_Utility(System.getProperty("user.dir")+"\\RMASeleniumTestDataSheets\\RMASeleniumAutomationTestData.xlsx"); //Excel WorkBook RMASeleniumAutomationTestData IS Fetched To Retrieve Data 
		StrRMAApp_LOB_Lst_LineOfBusiness = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_PreConditionsCheck", 1, 0); //Line Of Business Name Is Fetched From DataSheet RMA_TC_PreCheck		
		
		driver.switchTo().frame(RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Frm_MenuOptionFrame(driver)); //A Switch To The Frame Containing RMA Application Menu Option Is Done
		RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Mnu_Options(driver, 10).click(); // Utilities Menu Option Is Clicked
		logger.log(LogStatus.INFO, "Utilities Menu Option Is Clicked On RISKMASTER Default View Page");

		RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_SubMnu_Options(driver, 10, 3).click(); //Utilities-->System Parameters Menu Option Is Clicked
		logger.log(LogStatus.INFO, "Utilities-->System Parameters Menu Option Is Clicked On RISKMASTER Default View Page");
				
		RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_ContextMnu_Options(driver, 10, 3, 2).click(); //Utilities-->System Parameters-->Line Of Business Parameter Set Up Menu Option Is Clicked
		logger.log(LogStatus.INFO, "Utilities-->System Parameters-->Line Of Business Parmeter Set Up Menu Option Is Clicked On RISKMASTER Default View Page");

		driver.switchTo().frame(RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Frm_LineOfBusinessParameterSetUp(driver)); //A Switch To The Frame Containing Line Of Business Parameter Set Up Page Is Done
		Select dropdown = new Select (RMA_Selenium_POM_Utilities_LOB.RMAApp_LOB_Lst_LineOfBusiness(driver)); //Required Line Of Business Is Selected In Line Of Business List Box On Line Of Business Parameter Set Up Page
		dropdown.selectByVisibleText(StrRMAApp_LOB_Lst_LineOfBusiness);
		logger.log(LogStatus.INFO, "Line Of Business Selected In Line Of Business ListBox Is" +  " " + "::"+ " "+ StrRMAApp_LOB_Lst_LineOfBusiness.toUpperCase() );
		logger.log(LogStatus.INFO, "HTML", "Line Of Business Selected In Line Of Business ListBox Is: <span style='font-weight:bold;'> '+ StrRMAApp_LOB_Lst_LineOfBusiness +' </span>");
		Thread.sleep(3000); //Sleep Statement Is Introduced To Handle The Delay That Browser Takes To Select General Claim As Options And Then Load The Corresponding UI 
				
		RMA_Selenium_POM_Utilities_LOB.RMAApp_LOB_Tab_ClaimOptions(driver).click(); //Claim Options Tab On Line Of Business Parameter Set Up Page Is Clicked
		logger.log(LogStatus.INFO, "Claim Options Tab On Line Of Business Parameter Set Up Page Is Clicked");
		Thread.sleep(3000); //Sleep Statement Is Introduced To Handle The Delay That Browser Takes To Click On Claim Options Tab  And Then Load The Corresponding UI 
				
		RMA_Selenium_POM_Utilities_LOB.RMAApp_LOB_Chk_ClaimType(driver).click(); //Claim Type CheckBox Is Checked In Line Of Business Parameter Set Up Page
		logger.log(LogStatus.INFO, "Claim Type CheckBox Is Checked In Line Of Business Parameter Set Up Page");
		Thread.sleep(3000); //Sleep Statement Is Introduced To Make The UI Movement FRom Selection Of Options To Save Clear
				
		RMA_Selenium_POM_Utilities_LOB.RMAApp_LOB_Btn_Save(driver).click();  //Save Button Is Checked In Line Of Business Parameter Set Up Page
		logger.log(LogStatus.INFO, "Save Button Is Checked In Line Of Business Parameter Set Up Page");
				
		driver.navigate().refresh(); //Web page Is Refeshed
		logger.log(LogStatus.INFO, "Web Page Is Refeshed");
		
		Thread.sleep(10000);	
		RMA_WindowsPopHandler_Utility.RMA_WindowsMessageHandler_Utility(driver, StrAccept); //Alert Window Is Refeshed
		logger.log(LogStatus.INFO, "Windows PopUp That Appears On Web Page Refresh Is Accepted And WebPage Is Leaved");
		
		Thread.sleep(10000);
		driver.switchTo().parentFrame(); // A Switch To The Parent Frame Containing Menu Options Is Created 
		reports.endTest(logger);
	} catch (Exception e) {
		ExceptionRecorded = e.getMessage();	//Try Catch Statement Is Used To Handle Any Type Of Unhandled Exception And Print Log Of It
		if (ExceptionRecorded.contains("Command"))
		{
		ErrorMessage = ExceptionRecorded.split("Command");
		FinalErrorMessage = ErrorMessage[0];
		}
		else
		{
			FinalErrorMessage = ExceptionRecorded;
		}
		throw (e);
	}
	}
	
@AfterMethod
public void RMA_FailureReport(ITestResult result) //All The Information Associated With The Test Case Is Stored In Result Variable
{
	String StrScreenShotLocation; 
	String StrScreenShotTCName = "RMA_TC_PreConditionCheck";
	if (ITestResult.FAILURE == result.getStatus())
	{
		StrScreenShotLocation = RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName);
		logger.log(LogStatus.FAIL, "Following Error Occurred While Executing Test Case - RMA_TC_PreConditionCheck And Hence The TestCase Is A Fail  " + "::" + ErrorMessage[0]);
		logger.log(LogStatus.INFO, "ScreenShot Of The Application Is Stored At Following Path In Local Machine:" + " " +  StrScreenShotLocation);
		logger.log(LogStatus.INFO, logger.addScreenCapture(RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName)));
	}
	}	
}
