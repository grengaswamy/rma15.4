package rmaseleniumtestscripts;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import rmaseleniumPOM.RMA_Selenium_POM_Login_DSNSelect_Frames;
import rmaseleniumPOM.RMA_Selenium_POM_Document_GeneralClaims;
import rmaseleniumutilties.RMA_ExcelDataRetrieval_Utility;
import rmaseleniumutilties.RMA_FailureScreenCapture_Utility;

public class RMA_TC_004 extends rmaseleniumtestscripts.RMA_TC_BaseTest
{
static String ExceptionRecorded;
static String []ErrorMessage;
static String FinalErrorMessage;
@Test 
public void GeneralClaimCreation() throws Exception
{
	try {
		logger = reports.startTest("TC_004_General Claim Creation", "A New General Claim Is Created");
		String RMAApp_GeneralClaimCreation_Txt_Claimype;
		String RMAApp_GeneralClaimCreation_Txt_DepteId;
		int RMAApp_GeneralClaimCreation_Txt_TimeOfEvent;
		String RMAApp_GeneralClaimCreation_Txt_ClaimStatus;
		int RMAApp_GeneralClaimCreation_Txt_DateOfClaim;
		int RMAApp_GeneralClaimCreation_Txt_TimeOfClaim;
		int RMAApp_GeneralClaimCreation_Txt_DateOfEvent;
		String StrEventNumber; 
		String StrClaimNumber;
		//Local Variable Declaration
				
		RMA_ExcelDataRetrieval_Utility ExcelData = new RMA_ExcelDataRetrieval_Utility(System.getProperty("user.dir")+"\\RMASeleniumTestDataSheets\\RMASeleniumAutomationTestData.xlsx"); //Excel WorkBook RMASeleniumAutomationTestData IS Fetched To Retrieve Data 
		RMAApp_GeneralClaimCreation_Txt_Claimype = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_004", 1, 0); //ClaimType Is Fetched From DataSheet RMA_TC_004
		RMAApp_GeneralClaimCreation_Txt_DepteId = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_004", 1, 6); //Dept Id Is Fetched From DataSheet RMA_TC_004
		RMAApp_GeneralClaimCreation_Txt_TimeOfEvent = ExcelData.RMA_ExcelNumberDataRead_Utility("RMA_TC_004", 1, 2); //TimeOfEvent Is Fetched From DataSheet RMA_TC_004
		RMAApp_GeneralClaimCreation_Txt_ClaimStatus =  ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_004", 1, 3); //Claim Status Is Fetched From DataSheet RMA_TC_004
		RMAApp_GeneralClaimCreation_Txt_DateOfClaim = ExcelData.RMA_ExcelNumberDataRead_Utility("RMA_TC_004", 1, 4); //DateOfReporting Is Fetched From DataSheet RMA_TC_004
		RMAApp_GeneralClaimCreation_Txt_TimeOfClaim = ExcelData.RMA_ExcelNumberDataRead_Utility("RMA_TC_004", 1, 5); //TimeOfReporting Is Fetched From DataSheet RMA_TC_004
		RMAApp_GeneralClaimCreation_Txt_DateOfEvent = ExcelData.RMA_ExcelNumberDataRead_Utility("RMA_TC_004", 1, 1); //DateOfEvent Is Fetched From DataSheer RMA_TC_004		
		
		driver.switchTo().frame(RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Frm_MenuOptionFrame(driver)); //A Switch To The Frame Containing RMA Application Menu Option Is Done
		RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Mnu_Options(driver, 1).click(); //Document Menu Option Is Selected
		logger.log(LogStatus.INFO, "Document Menu Option Is Clicked On RISKMASTER Default View Page");
				
		RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_SubMnu_Options(driver, 1, 2).click(); //General Claim Menu Option Is Selected
		logger.log(LogStatus.INFO, "Document-->General Claim Menu Option Is Clicked On RISKMASTER Default View Page");
				
		driver.switchTo().frame(RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Frm_GeneralClaim(driver)); //A Switch To The Frame Containing General Claim Creation Controls IS Done
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_Dept_Id(driver).sendKeys(RMAApp_GeneralClaimCreation_Txt_DepteId);
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_Dept_Id(driver).sendKeys(Keys.TAB);
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "DeptId Is Typed In DeptId TextBox On General Claim Creation Page" + " " + "::" + " " + RMAApp_GeneralClaimCreation_Txt_DepteId.toUpperCase());
				
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_TimeOfEvent(driver).sendKeys(String.valueOf(RMAApp_GeneralClaimCreation_Txt_TimeOfEvent));
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "TimeOfEvent Is Typed In TimeOfEvent TextBox On General Claim Creation Page" + " " + "::"+ " "+ RMAApp_GeneralClaimCreation_Txt_TimeOfEvent);
				
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_ClaimStatus(driver).sendKeys(RMAApp_GeneralClaimCreation_Txt_ClaimStatus);
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_ClaimStatus(driver).sendKeys(Keys.TAB);
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "ClaimStatus Is Typed In ClaimStatus TextBox On GeneralClaim Creation Page" +" " + "::" + "" + RMAApp_GeneralClaimCreation_Txt_ClaimStatus.toUpperCase());
				
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_DateOfClaim(driver).sendKeys(String.valueOf(RMAApp_GeneralClaimCreation_Txt_DateOfClaim));
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "DateOfClaim Is Typed In DateOfClaim TextBox On GeneralClaim Creation Page" + "" + "::" + RMAApp_GeneralClaimCreation_Txt_DateOfClaim);
				
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_ClaimType(driver).sendKeys(RMAApp_GeneralClaimCreation_Txt_Claimype);
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_ClaimType(driver).sendKeys(Keys.TAB);
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "Claim Type Is Typed In Claim Type TextBox On General Claim Creation Page" + " "+ "::" + " "+ RMAApp_GeneralClaimCreation_Txt_Claimype.toUpperCase() );
				
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_TimeOfClaim(driver).sendKeys(String.valueOf(RMAApp_GeneralClaimCreation_Txt_TimeOfClaim));
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "TimeOfClaim Is Typed In TimeOfClaim TextBox On GeneralClaim Creation Page" + " " + "::" + " "+ RMAApp_GeneralClaimCreation_Txt_TimeOfClaim );
				
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_DateOfEvent(driver).sendKeys(String.valueOf(RMAApp_GeneralClaimCreation_Txt_DateOfEvent));
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "DateOfEvent Is Typed In DateOfEvent TextBox On General Claim Creation Page" + " " + "::"+ " "+ RMAApp_GeneralClaimCreation_Txt_DateOfEvent);
				
		RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Btn_Save(driver).click();
		Thread.sleep(7000);
		logger.log(LogStatus.INFO, "Save Button Is Clicked On General Claim Creation Page");
				
		StrEventNumber = RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_EventNumber(driver).getAttribute("value"); //Value Of the Generated Event Number Is Fetched In The Variable StrEventNumber
		logger.log(LogStatus.INFO, "Value Of the Generated Event Number Is" + " " + "::"+ " "+ StrEventNumber );
		StrClaimNumber = RMA_Selenium_POM_Document_GeneralClaims.RMAApp_GeneralClaim_Txt_ClaimNumber(driver).getAttribute("value"); //Value Of the Generated Claim Number Is Fetched In The Variable StrClaimNumber
		logger.log(LogStatus.INFO, "Value Of the Generated Claim Number Is" + " " + "::"+ " "+ StrClaimNumber );
		Assert.assertTrue(StrEventNumber.contains("EVT"));
		logger.log(LogStatus.PASS, "Valid Event Number Is Generated That Is" + " " + "::"+ " "+ StrEventNumber );
		Assert.assertTrue(StrClaimNumber.contains("GC"));
		logger.log(LogStatus.PASS, "Valid Claim Number Is Generated That Is" + " " + "::"+ " "+ StrClaimNumber );
				
		RMA_ExcelDataRetrieval_Utility.WriteDataToExcel("D:\\RMASeleniumAutomationTestData.xlsx","RMA_TC_004", 10, 0, StrEventNumber);
		logger.log(LogStatus.INFO, "Generated Event Number:" +  " " + StrEventNumber + " "  + "Is Also Written In The Corresponding Excel Data Sheet RMA_TC_004");
		
		RMA_ExcelDataRetrieval_Utility.WriteDataToExcel("D:\\RMASeleniumAutomationTestData.xlsx","RMA_TC_004", 10, 1, StrClaimNumber);
		logger.log(LogStatus.INFO, "Generated Claim Number:" + " " + StrClaimNumber + " " + "Is Also Written In The Corresponding Excel Data Sheet RMA_TC_004");
		
		driver.switchTo().parentFrame(); //A Switch To The Parent Frame That Contains Menu Options And Left Hand Navigation Tree Is Done
		reports.endTest(logger);
	} catch (Exception e) {
		ExceptionRecorded = e.getMessage();	//Try Catch Statement Is Used To Handle Any Type Of Unhandled Exception And Print Log Of It
		if (ExceptionRecorded.contains("Command"))
		{
		ErrorMessage = ExceptionRecorded.split("Command");
		FinalErrorMessage = ErrorMessage[0];
		}
		else
		{
			FinalErrorMessage = ExceptionRecorded;
		}
		throw (e);
	}
	}
	
@AfterMethod
public void RMA_FailureReport(ITestResult result) //All The Information Associated With The Test Case Is Stored In Result Variable
{
	String StrScreenShotLocation; 
	String StrScreenShotTCName = "RMA_TC_004";
	if (ITestResult.FAILURE == result.getStatus())
	{
		StrScreenShotLocation = RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName);
		logger.log(LogStatus.FAIL, "Following Error Occurred While Executing Test Case - RMA_TC_004 And Hence The TestCase Is A Fail  " + "::" + FinalErrorMessage);
		logger.log(LogStatus.INFO, "ScreenShot Of The Application Is Stored At Path In Local Machine", StrScreenShotLocation);
		logger.log(LogStatus.INFO, logger.addScreenCapture(RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName)));
	}
}
}



