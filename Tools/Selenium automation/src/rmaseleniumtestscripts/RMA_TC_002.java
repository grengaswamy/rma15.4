package rmaseleniumtestscripts;

import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
//Default Package Import Completed

import rmaseleniumPOM.RMA_Selenium_POM_Login_DSNSelect_Frames;
import rmaseleniumutilties.RMA_ExcelDataRetrieval_Utility;
import rmaseleniumutilties.RMA_FailureScreenCapture_Utility;
//RMA Package Import Completed

public class RMA_TC_002 extends rmaseleniumtestscripts.RMA_TC_BaseTest
{
static String ExceptionRecorded;
static String []ErrorMessage;
static String FinalErrorMessage;
@Test
public void RMAApp_DSNSelection() throws Exception
{
	try {
		logger = reports.startTest("TC_002_RiskMaster DSN Selection", "Required DSN Is Selected And DataSource Label On the Default View Page Is Verified");
		String RMAApp_DSNSelect_Lst_DataSourceName;
		String StrDataSourceLabelActual;
		//Local Variable Declaration
				
		RMA_ExcelDataRetrieval_Utility ExcelData = new RMA_ExcelDataRetrieval_Utility(System.getProperty("user.dir")+"\\RMASeleniumTestDataSheets\\RMASeleniumAutomationTestData.xlsx"); //Excel WorkBook RMASeleniumAutomationTestData IS Fetched To Retrieve Data 
		RMAApp_DSNSelect_Lst_DataSourceName = ExcelData.RMA_ExcelStringDataRead_Utility("RMA_TC_002", 1, 0); //Data Source Name Is Fetched From DataSheet RMA_TC_002
		
		Select dropdown = new Select (RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DSNSelect_Lst_DataSourceName(driver));
		dropdown.selectByVisibleText(RMAApp_DSNSelect_Lst_DataSourceName);
		logger.log(LogStatus.INFO, "Required Data Source Name Is Selected In Data Source Name List Box On RISKMASTER Database and View Selection Page" + " "+ "::" + " " +  RMAApp_DSNSelect_Lst_DataSourceName.toUpperCase());
		
		Thread.sleep(3000); //Sleep Statement Is Introduced To Handle The Delay That IE Browser Takes To Click On Continue Button Though Implicit Wait Is There But Sleep Introduction Makes The UI Movement More Clear		
		RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DSNSelect_Btn_Continue(driver).click();
		logger.log(LogStatus.INFO, "Continue Button Is Clicked On RISKMASTER Database and View Selection Page");
		
		StrDataSourceLabelActual = RMA_Selenium_POM_Login_DSNSelect_Frames.RMAApp_DefaultView_Lbl_DataSource(driver).getText();
		
		Assert.assertEquals(RMAApp_DSNSelect_Lst_DataSourceName,StrDataSourceLabelActual);
		logger.log(LogStatus.PASS, "DataSource Name Label On Default View Page Displays The Same Name As Selected In DataSourceName ListBox RISKMASTER Database and View Selection");
		reports.endTest(logger);
		
	} catch (Exception e) {
		ExceptionRecorded = e.getMessage();	//Try Catch Statement Is Used To Handle Any Type Of Unhandled Exception And Print Log Of It
		if (ExceptionRecorded.contains("Command"))
		{
		ErrorMessage = ExceptionRecorded.split("Command");
		FinalErrorMessage = ErrorMessage[0];
		}
		else
		{
			FinalErrorMessage = ExceptionRecorded;
		}
		throw (e);
	}
	}
	
@AfterMethod
public void RMA_FailureReport(ITestResult result) //All The Information Associated With The Test Case Is Stored In Result Variable
{
	String StrScreenShotLocation;
	String StrScreenShotTCName;
	StrScreenShotTCName = "RMA_TC_002";
	if (ITestResult.FAILURE == result.getStatus())
	{
		StrScreenShotLocation = RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName );
		logger.log(LogStatus.FAIL, "Following Error Occurred While Exceuting Test Case - RMA_TC_002 And Hence The Test Case Is a Fail::" + FinalErrorMessage);
		logger.log(LogStatus.INFO, "ScreenShot Of The Application Is Stored At Following Path In Local Machine::" + " " + StrScreenShotLocation);
		logger.log(LogStatus.INFO, logger.addScreenCapture(RMA_FailureScreenCapture_Utility.RMA_ScreenShotCaptureOnFailure_Utility(driver, result.getName(), StrScreenShotTCName)));
	}
}
}
