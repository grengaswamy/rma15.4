using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Riskmaster.Security;

namespace Riskmaster.Tools.QueryTool
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class dlgConnect : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button cmdOk;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.TextBox txtConnString;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox chkRMLogins;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public dlgConnect()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public void Quit()
		{
			this.Close();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdOk = new System.Windows.Forms.Button();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.txtConnString = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.chkRMLogins = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// cmdOk
			// 
			this.cmdOk.Location = new System.Drawing.Point(208, 72);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Size = new System.Drawing.Size(96, 24);
			this.cmdOk.TabIndex = 0;
			this.cmdOk.Text = "Connect";
			this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.Location = new System.Drawing.Point(320, 72);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(96, 24);
			this.cmdCancel.TabIndex = 1;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// txtConnString
			// 
			this.txtConnString.Location = new System.Drawing.Point(16, 40);
			this.txtConnString.Name = "txtConnString";
			this.txtConnString.Size = new System.Drawing.Size(384, 20);
			this.txtConnString.TabIndex = 2;
			this.txtConnString.Text = "Server=SODBNT01;Database=Wizard_BSB;UID=sa;PWD=riskmaster;";
			this.txtConnString.TextChanged += new System.EventHandler(this.txtConnString_TextChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(208, 16);
			this.label1.TabIndex = 3;
			this.label1.Text = "Database Connection String";
			// 
			// chkRMLogins
			// 
			this.chkRMLogins.Location = new System.Drawing.Point(16, 72);
			this.chkRMLogins.Name = "chkRMLogins";
			this.chkRMLogins.Size = new System.Drawing.Size(128, 16);
			this.chkRMLogins.TabIndex = 4;
			this.chkRMLogins.Text = "Use RM Login";
			this.chkRMLogins.CheckedChanged += new System.EventHandler(this.chkRMLogins_CheckedChanged);
			// 
			// dlgConnect
			// 
			this.AcceptButton = this.cmdOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cmdCancel;
			this.ClientSize = new System.Drawing.Size(432, 109);
			this.Controls.Add(this.chkRMLogins);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtConnString);
			this.Controls.Add(this.cmdCancel);
			this.Controls.Add(this.cmdOk);
			this.Name = "dlgConnect";
			this.Text = "Riskmaster Query Tool";
			this.ResumeLayout(false);

		}
		#endregion
//driver=rmoracle;User Id=remrisk;Password=remrisk;Data Source=MONSTER;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Quit();
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{

			if (this.chkRMLogins.Checked)
			{
				UserLogin user = new UserLogin();
				Login login = new Login();
				try{	login.RegisterApplication(this.Handle.ToInt32(),0,ref user);}catch{}
				try{this.txtConnString.Text = user.objRiskmasterDatabase.ConnectionString;}catch{}
			}	
			frmMain frm = this.Owner as frmMain;
            frm.m_connectionString = this.txtConnString.Text;
			this.Close();
		}

		private void chkRMLogins_CheckedChanged(object sender, System.EventArgs e)
		{
//			if (this.chkRMLogins.Checked)
//                this.Height  = this.Height + 300;
//			else
//				this.Height = this.Height - 300;
		}

		private void txtConnString_TextChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}
