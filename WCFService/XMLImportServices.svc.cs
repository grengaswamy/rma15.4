﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Models;
using Riskmaster.Security;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "XMLImportServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select XMLImportServices.svc or XMLImportServices.svc.cs at the Solution Explorer and start debugging.
    //public class XMLImportServices : IXMLImportServices
    //{
    //    public void DoWork()
    //    {
    //    }
    //}
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class XMLImportServices : RMService, IXMLImportServices
    {

        public XMLOutPut ProcessImportXML(XMLInput oXMLInput)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            //XMLImport objReturn = null;
            ImportXMLAdaptor objxmlimport = null;
            BusinessAdaptorErrors systemErrors = null;
            string sFunctionName = "ProcessXML";
            //Added by Amitosh
            bool bresult = false;
            XMLOutPut objOutPut = null;
            try
            {
                systemErrors = new BusinessAdaptorErrors(oXMLInput.ClientId);
                //  objReturn = new ProgressNotesType();
                objxmlimport = new ImportXMLAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                // InitiateServiceProcess(oXMLInput, out xmlRequest, sFunctionName, objxmlimport, out oUserLogin, ref systemErrors);
                InitiateServiceProcess(oXMLInput, out xmlRequest, sFunctionName, objxmlimport, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                systemErrors = new BusinessAdaptorErrors(oUserLogin, oXMLInput.ClientId);
                bresult = objxmlimport.XMLImport(oXMLInput.GetXMLInput, ref systemErrors, out objOutPut, oXMLInput.bVal);
                //bresult = objxmlimport.XMLImport(oXMLInput.GetXMLInput, ref systemErrors, out objOutPut, oXMLInput.bVal);//logFileContent Added for MITS 29711 by bsharma33
                objOutPut.bResult = bresult;
                // oXMLInput.bVal = bresult;
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, systemErrors);

            if (systemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objOutPut;
        }

        // public XMLImport GetSuppTemplate(XMLTemplate oXMLInput, out XMLTemplate objReturn)
        public XMLTemplate GetSuppTemplate(XMLTemplate oXMLTemplate)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            ImportXMLAdaptor objXmlImport = null;
            BusinessAdaptorErrors systemErrors = null;
            string sFunctionName = "GetSuppTemplate";
            XMLTemplate objReturn = null;
            //Added by Amitosh
            bool bresult = false;
            try
            {
                systemErrors = new BusinessAdaptorErrors(oXMLTemplate.ClientId);
                //  objReturn = new ProgressNotesType();
                objXmlImport = new ImportXMLAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oXMLTemplate, out xmlRequest, sFunctionName, objXmlImport, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                systemErrors = new BusinessAdaptorErrors(oUserLogin, oXMLTemplate.ClientId);
                bresult = objXmlImport.GetSupplementals(oXMLTemplate, out objReturn, ref systemErrors);

            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                //logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            logErrors(sFunctionName, xmlRequest, bresult, systemErrors);

            if (systemErrors.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bresult, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

            return objReturn;

        }
    }
}
