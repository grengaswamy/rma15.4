﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
//using Riskmaster.Models;
using System.ServiceModel.Activation;
using System.Configuration;
namespace RiskmasterService.DataStreamingServiceForDA
{
    // NOTE: If you change the interface name "IDataStreamingService" here, you must also update the reference to "IDataStreamingService" in Web.config.
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    // NOTE: If you change the class name "RMService" here, you must also update the reference to "RMService" in Web.config and in the associated .svc file.
    public class DataStreamingService : RMService,IDataStreamingService
    {
        //public Stream RetrieveDocument(StreamedDocumentType request)
        public Stream RetrieveDocument(string request1, string request2, string screenflag, string token,int iClientId, string sFormName)
        {
            XmlDocument xmlRequest = null;
            string functionName = "RetrieveDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            Riskmaster.Models.StreamDocType objReturn = null;
            Riskmaster.Models.StreamDocType request = null;
            Stream downloadedStream = null;
            bool bResult = false;
            bool bIsStorageFileSystem = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                request = new Riskmaster.Models.StreamDocType();
                request.DocumentId = Conversion.ConvertStrToInteger(request1);
                request.Token = token;
                request.PsId = Conversion.ConvertStrToInteger(request2);
                request.ScreenFlag=screenflag;
                request.FormName = sFormName;
                request.ClientId = iClientId;
                systemErrors = new BusinessAdaptorErrors(iClientId);
                objReturn = new Riskmaster.Models.StreamDocType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, iClientId);
                objDocument.RetrieveLargeDocument(request,out objReturn,ref bIsStorageFileSystem,ref errOut);

                //downloadedStream = objReturn.fileStream;
                downloadedStream = new System.IO.FileStream(objReturn.FilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                if (!bIsStorageFileSystem)
                {
                    OperationContext clientContext = OperationContext.Current;
                    clientContext.OperationCompleted += new EventHandler(delegate(object sender, EventArgs args)
                    {
                        if (downloadedStream != null)
                            downloadedStream.Dispose();
                        File.Delete(objReturn.FilePath);
                    });
                }
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return downloadedStream;

        }

        public void UploadBRSFeeScheuleFile(StreamedBRSDocumentType request)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            Riskmaster.Models.StreamBRSDocType objReq = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                //objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                objReq = new Riskmaster.Models.StreamBRSDocType();
                request.CopyProperties(objReq);
                objReq.fileStream = request.fileStream;
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(objReq, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.UploadBRSFeeScheuldeFile(objReq, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void CreateDocument(StreamedDocumentType request)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            Riskmaster.Models.StreamDocType objReq = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                //objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                objReq = new Riskmaster.Models.StreamDocType();
                request.CopyProperties(objReq);
                objReq.fileStream = request.fileStream;
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(objReq, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.AddDocument(objReq, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void UploadFile(StreamedUploadDocumentType request)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            Riskmaster.Models.StreamUploadDocType objReq = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                //objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                objReq = new Riskmaster.Models.StreamUploadDocType();
                request.CopyProperties(objReq);
                objReq.fileStream = request.fileStream; // TODO:aaggarwal29
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(objReq, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.UploadFile(objReq, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                Riskmaster.Models.RMException theFault = new Riskmaster.Models.RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<Riskmaster.Models.RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
    }
    /// <summary>
    /// Reflection
    /// </summary>
    public static class Reflection
    {
        public static void CopyProperties(this object source, object destination)
        {
            Type typeDest = destination.GetType();
            Type typeSrc = source.GetType();

            PropertyInfo[] srcProps = typeSrc.GetProperties();
            foreach (PropertyInfo srcProp in srcProps)
            {
                PropertyInfo targetProperty = typeDest.GetProperty(srcProp.Name);
                targetProperty.SetValue(destination, srcProp.GetValue(source, null), null);
            }
        }
    }
    /// <summary>
    /// RMStreamedServiceType
    /// </summary>
    [MessageContract]
    public class RMStreamedServiceType
    {
        public RMStreamedServiceType()
        {

        }

        [MessageHeader]
        public string Token
        {
            get;
            set;
        }
        [MessageHeader]
        public string Errors;
        [MessageHeader]
        public int ClientId
        {
            get;
            set;
        }
    }

    /// <summary>
    /// StreamedBRSDocumentType
    /// </summary>
    [MessageContract]
    public class StreamedBRSDocumentType : RMStreamedServiceType, IDisposable
    {
        [MessageBodyMember]
        [XmlIgnore]
        public Stream fileStream;

        [MessageHeader]
        public string FileName
        {
            get;
            set;
        }


        public void Dispose()
        {

            if (fileStream != null)
            {
                fileStream.Dispose();
                fileStream = null;
            }
        }
    }

    /// <summary>
    /// StreamedDocumentType
    /// </summary>
    [MessageContract]
    public class StreamedDocumentType : RMStreamedServiceType, IDisposable
    {
        public StreamedDocumentType()
        {
            this.Class = new Riskmaster.Models.StreamedCodeType();
            this.Category = new Riskmaster.Models.StreamedCodeType();
            this.Type = new Riskmaster.Models.StreamedCodeType();
        }
        public void Dispose()
        {

            if (fileStream != null)
            {
                fileStream.Dispose();
                fileStream = null;
            }
        }

        [MessageBodyMember]
        [XmlIgnore]
        public Stream fileStream;

        [MessageHeader]
        public string UserId
        {
            get;
            set;
        }
        [MessageHeader]
        public int Readonly
        {
            get;
            set;
        }
        [MessageHeader]
        public string Pid
        {
            get;
            set;
        }
        [MessageHeader]
        public int View
        {
            get;
            set;
        }
        [MessageHeader]
        public int Create
        {
            get;
            set;
        }
        [MessageHeader]
        public int Edit
        {
            get;
            set;
        }
        [MessageHeader]
        public int Delete
        {
            get;
            set;
        }
        [MessageHeader]
        public int Transfer
        {
            get;
            set;
        }
        [MessageHeader]
        public int Copy
        {
            get;
            set;
        }
        [MessageHeader]
        public int Download
        {
            get;
            set;
        }
        [MessageHeader]
        public int Move
        {
            get;
            set;
        }
        [MessageHeader]
        public int Email
        {
            get;
            set;
        }
        [MessageHeader]
        public string FolderId
        {
            get;
            set;
        }
        [MessageHeader]
        public string FolderName
        {
            get;
            set;
        }
        [MessageHeader]
        public string CreateDate
        {
            get;
            set;
        }
        [MessageHeader]
        public string UserName
        {
            get;
            set;
        }
        [MessageHeader]
        public string FilePath
        {
            get;
            set;
        }
        [MessageHeader]
        public string AttachTable
        {
            get;
            set;
        }
        [MessageHeader]
        public string DocInternalType
        {
            get;
            set;
        }
        [MessageHeader]
        public int AttachRecordId
        {
            get;
            set;
        }
        [MessageHeader]
        public int DocumentId
        {
            get;
            set;
        }
        [MessageHeader]
        public string ScreenFlag
        {
            get;
            set;
        }
        [MessageHeader]
        public string FormName
        {
            get;
            set;
        }
        [MessageHeader]
        public int PsId
        {
            get;
            set;
        }
        [MessageHeader]
        public string Title
        {
            get;
            set;
        }
        [MessageHeader]
        public string Subject
        {
            get;
            set;
        }
        [MessageHeader]
        public Riskmaster.Models.StreamedCodeType Type
        {
            get;
            set;
        }
        [MessageHeader]
        public string DocumentsType
        {
            get;
            set;
        }
        [MessageHeader]
        public string DocumentClass
        {
            get;
            set;
        }
        [MessageHeader]
        public Riskmaster.Models.StreamedCodeType Class
        {
            get;
            set;
        }
        [MessageHeader]
        public string DocumentCategory
        {
            get;
            set;
        }
        [MessageHeader]
        public Riskmaster.Models.StreamedCodeType Category
        {
            get;
            set;
        }
        [MessageHeader]
        public string FileSize
        {
            get;
            set;
        }
        [MessageHeader]
        public string Keywords
        {
            get;
            set;
        }
        [MessageHeader]
        public string Notes
        {
            get;
            set;
        }
        [MessageHeader]
        public string FileName
        {
            get;
            set;
        }
        [MessageHeader]
        [XmlIgnore]
        public byte[] FileContents
        {
            get;
            set;
        }
        //Amandeep Mobile Adjuster
        [MessageHeader]
        public string Claimnumber
        {
            get;
            set;
        }
        [DataMember]
        public bool bMobileAdj
        {
            get;
            set;
        }
        //mbahl3 
        [DataMember]
        public bool bMobilityAdj
        {
            get;
            set;
        }
        [DataMember]
        public string Eventnumber
        {
            get;
            set;
        }

    }

    /// <summary>
    /// StreamedUploadDocumentType
    /// </summary>
    [MessageContract]
    public class StreamedUploadDocumentType : RMStreamedServiceType, IDisposable
    {
        [MessageBodyMember]
        [XmlIgnore]
        public Stream fileStream;


        [MessageHeader]
        public string FileName
        {
            get;
            set;
        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // We need filePath, ModuleName, RelatedId (optionsetid) and DocumentType while uploading the Import Files or attachments to DB
        [MessageHeader]
        public string filePath
        {
            get;
            set;
        }

        [MessageHeader]
        public string ModuleName
        {
            get;
            set;
        }

        [MessageHeader]
        public string DocumentType
        {
            get;
            set;
        }


        [MessageHeader]
        public int RelatedId
        {
            get;
            set;
        }


        public void Dispose()
        {
            if (fileStream != null)
            {
                fileStream = null;
            }

        }
    
    }
}
