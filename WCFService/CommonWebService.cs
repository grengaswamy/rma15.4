﻿//#define DEBUG //used for Debugging purposes only
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Security;
using System.ServiceModel;
using System.Runtime.Serialization.Formatters.Binary;

namespace RiskmasterService
{

    /// <summary>
    /// Partial class to offer functionality currently provided by CWS
    /// but to hide all classic .Net Web Service functionality behind
    /// WCF Services
    /// </summary>
    public partial class CommonWCFService
    {
        public const string XML_NAMESPACE = "http://csc.com/Riskmaster/Webservice/Common";
        private static Hashtable m_AdaptorCache = new Hashtable();

        /// <summary>
        /// Static constructor for the Web Service to load all Business Adaptors
        /// initialize Session values etc.
        /// </summary>
        static CommonWCFService()
        {
            try
            {
                ////Build the Riskmaster Adaptors collection
                RMAdaptersCollection rmAdapters = RMConfigurationManager.GetRMAdaptors();
                RMAdaptersCollection rmExtensibilityAdapters = RMConfigurationManager.GetRMExtensibilityAdaptors();

                BuildAdaptorCache(rmAdapters, false);
                BuildAdaptorCache(rmExtensibilityAdapters, true);
            }
            catch (Exception ex)
            {
                throw new FaultException(ex.Message);
            }  
        }//static constructor

        /// <summary>
        /// Instance based constructor for the Web Service 
        /// to initiate Session information
        /// </summary>
        public CommonWCFService()
        {
        }

        /// <summary>
        /// Builds the Adaptor Hash Table based on the currently configured 
        /// RISKMASTER Adaptors in RiskmasterAdaptors.config
        /// </summary>
        /// <param name="rmAdapters">collection of RMAdapter Elements</param>
        /// <param name="IsExtensibility">boolean indicating whether or not the adapters
        /// collection are Extensibility adapters</param>
        private static void BuildAdaptorCache(RMAdaptersCollection rmAdapters, bool IsExtensibility)
        {
            AdaptorCacheRecord adaptor;
            foreach (RMAdapterElement rmAdapter in rmAdapters)
            {
                adaptor = new AdaptorCacheRecord();
                adaptor.Name = rmAdapter.Name;
                adaptor.Assembly = rmAdapter.Assembly;
                adaptor.Class = rmAdapter.Class;
                adaptor.Method = rmAdapter.Method;
                adaptor.BypassSecurity = rmAdapter.BypassSecurity;

                //If the collection consists of Extensibility Adapters
                if (IsExtensibility)
                {
                    //Allow for "replacement\override" of system Adaptor entries.
                    if (m_AdaptorCache.Contains(adaptor.Name))
                    {
                        m_AdaptorCache.Remove(adaptor.Name);
                    } // if
                } // if

                //Add the AdaptorCache Record to the HashTable
                m_AdaptorCache.Add(adaptor.Name, adaptor);
            } // foreach
        }//method: BuildAdaptorCache()

        




        




        private XmlDocument formatOutputXML(XmlDocument xmlOut, bool bCallResult, BusinessAdaptorErrors errors)
        {
            // Create output envelope doc and root node (ResultMessage)
            XmlDocument xmlOutEnv = new XmlDocument();
            // JP TMP 01.18.2006   XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage", XML_NAMESPACE);
            XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage");
            xmlOutEnv.AppendChild(xmlRoot);

            // Add errors
            formatErrorXML(xmlOutEnv, xmlRoot, bCallResult, errors);

            // Add output document
            // JP TMP 01.18.2006   XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document", XML_NAMESPACE);
            XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document");
            xmlRoot.AppendChild(xmlDocEnv);

            if (xmlOut != null)  // output doc can be null if error occurred - but <Document/> element still needs to be in place
                xmlDocEnv.InnerXml = xmlOut.OuterXml;

            // Return output envelope
            return xmlOutEnv;
        }

        private void formatErrorXML(XmlDocument xmlDoc, XmlElement xmlRoot, bool bCallResult, BusinessAdaptorErrors errors)
        {
            XmlElement xmlElement = null;
            XmlElement xmlMsgsRoot = null;

            // Create error root element (MsgStatus)
            // JP TMP 01.18.2006   XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus", XML_NAMESPACE);
            XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus");
            xmlRoot.AppendChild(xmlErrRoot);

            // Add overall result status
            // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("MsgStatusCd", XML_NAMESPACE);
            xmlElement = xmlDoc.CreateElement("MsgStatusCd");
            if (bCallResult)
                xmlElement.InnerText = "Success";
            else
                xmlElement.InnerText = "Error";

            xmlErrRoot.AppendChild(xmlElement);

            if (errors != null)
            {
                foreach (BusinessAdaptorError err in errors)
                {
                    // ... add individual error messages/warnings (if no errors, there will still be an empty ExtendedStatus element)
                    // JP TMP 01.18.2006   xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus", XML_NAMESPACE);
                    xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus");
                    xmlErrRoot.AppendChild(xmlMsgsRoot);

                    if (err.oException == null)
                    {  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
                        // ... add error code/number
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = err.ErrorCode;
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                        // RMA 6444 - Remove the token if present
                        xmlElement.InnerText = err.ErrorDescription.Replace("~~BlankKey","");
                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // non-Exception case
                    else
                    {  // Exception case
                        // Determine error code - assembly + exception type
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = String.Format("{0}.{1}", err.oException.Source, err.oException.GetType().Name);
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");

                        // RMA 6444 - In case the Key is not present in DB, and while throwing the error back to CommonWebService, if an exception is also sent, then it used to check if the 
                        // the Error Description is blank, then it used to display the exception message. But now as we are sending the key, it was failing that condition.
                        // So now checking if the message ends with this token we are displaying the Exception Message
                        if (!string.IsNullOrEmpty(err.ErrorDescription) && ! err.ErrorDescription.EndsWith("~~BlankKey"))
                            xmlElement.InnerText = err.ErrorDescription;
                        else
                            xmlElement.InnerText = err.oException.Message.Replace("~~BlankKey", "");

                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // Exception case

                    // ...add error type
                    // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedMsgType", XML_NAMESPACE);
                    xmlElement = xmlDoc.CreateElement("ExtendedMsgType");
                    switch (err.ErrorType)
                    {
                        case BusinessAdaptorErrorType.SystemError:
                            xmlElement.InnerText = "SystemError";
                            break;
                        case BusinessAdaptorErrorType.Error:
                            xmlElement.InnerText = "Error";
                            break;
                        case BusinessAdaptorErrorType.Warning:
                            xmlElement.InnerText = "Warning";
                            break;
                        case BusinessAdaptorErrorType.Message:
                            xmlElement.InnerText = "Message";
                            break;
                        case BusinessAdaptorErrorType.PopupMessage:
                            xmlElement.InnerText = "PopupMessage";
                            break;
                        default:
                            // TODO   - What to do if not a standard error code?
                            break;
                    };
                    xmlMsgsRoot.AppendChild(xmlElement);
                }
            }

        }

        #region .Net/WCF Web Service Method
        /// <summary>
        /// Processes the input from the Presentation Layer and generates the appropriate output
        /// </summary>
        /// <param name="xmlRequest">XML Document containing the presentation layer request content</param>
        /// <returns>XMLDocument containing all of the appropriate logic to be used by the Presentation Layer</returns>
        internal XmlDocument ProcessRequest(XmlDocument xmlRequest)
        {
            XmlDocument xmlOutEnvelope = null;       // Will point to outgoing message envelope
            XmlElement xmlIn = null;                 // Will point to incoming XML document (embedded in envelope)
            XmlDocument xmlOut = null;				 // Will point to outgoing XML document from business adaptor
            XmlElement xmlAuth = null;               // Will point to authorization token
            XmlElement xmlFunction = null;           // Will point to function in call
            XmlNamespaceManager nsNamespace = null;  // Used for xpath queries using common envelope namespace
            string functionName;                     // Holds name of business adaptor function to look up
            string sessionKeySupplied = string.Empty;
            UserLogin oUserLogin = null;             // User login retrieved from session and passed to business adaptor functions.
            

            // Create a namespace manager so we can query XPath on the envelope
            nsNamespace = new XmlNamespaceManager(xmlRequest.NameTable);
            nsNamespace.AddNamespace("env", XML_NAMESPACE);

            //Move this code from other location & commented that part also.
            XmlElement xmlClient = (XmlElement)xmlRequest.SelectSingleNode("/Message/ClientId");
            string sClientId = "0";
            int iClientId = 0;
            if (xmlClient != null)
            {
                sClientId = xmlClient.InnerText;
            }
            iClientId = int.Parse(sClientId);
            BusinessAdaptorErrors systemErrors =
                new BusinessAdaptorErrors(iClientId);         // Collection for code to use to store system errors that need to be thrown back.
            // Crack the <call> element and lookup the assembly/function
            //  in the configuration file.
            //   If can't find the call, return a "Bad Call" .
            //    Be sure to document in Web Services Guidelines.doc.
            // JP TMP 01.18.2006    xmlFunction = (XmlElement) xmlRequest.SelectSingleNode("/env:Message/env:Call/env:Function", nsNamespace);
            xmlFunction = (XmlElement)xmlRequest.SelectSingleNode("/Message/Call/Function");
            if (xmlFunction == null)
            {
                // Throw back system error if part of envelope not there.
                systemErrors.Add("Riskmaster.WebService.Common.MalformedEnvelope",
                    Globalization.GetString("Riskmaster.WebService.Common.MalformedEnvelope", iClientId),
                    BusinessAdaptorErrorType.SystemError);

                ServiceLogger.LogErrors("", xmlRequest, false, systemErrors, iClientId);

                return formatOutputXML(null, false, systemErrors);
            }
            functionName = xmlFunction.InnerText;

            // Extract inbound XML message for business adaptor
            // JP 01.18.2006   xmlIn = (XmlElement) xmlRequest.SelectSingleNode("/env:Message/env:Document/*", nsNamespace);
            xmlIn = (XmlElement)xmlRequest.SelectSingleNode("/Message/Document/*");
            if (xmlIn == null)
            {
                // Throw back system error if part of envelope not there.
                systemErrors.Add("Riskmaster.WebService.Common.MalformedEnvelope",
                    Globalization.GetString("Riskmaster.WebService.Common.MalformedEnvelope", iClientId),
                    BusinessAdaptorErrorType.SystemError);

                ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                return formatOutputXML(null, false, systemErrors);
            }

            // Extract authorization key from inbound message
            // JP TMP 01.18.2006   xmlAuth = (XmlElement) xmlRequest.SelectSingleNode("/env:Message/env:Authorization", nsNamespace);
            xmlAuth = (XmlElement)xmlRequest.SelectSingleNode("/Message/Authorization");
            if (xmlAuth == null)
            {
                // Throw back system error if part of envelope not there.
                systemErrors.Add("Riskmaster.WebService.Common.MalformedEnvelope",
                    Globalization.GetString("Riskmaster.WebService.Common.MalformedEnvelope",iClientId),
                    BusinessAdaptorErrorType.SystemError);

                ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                return formatOutputXML(null, false, systemErrors);
            }


            // Lookup function in adaptor cache (cached at startup from config file)
            if (!m_AdaptorCache.Contains(functionName))
            {
                // Throw back system error if part of envelope not there.
                systemErrors.Add("Riskmaster.WebService.Common.AdaptorLookupFailed",
                    Globalization.GetString("Riskmaster.WebService.Common.AdaptorLookupFailed",iClientId),
                    BusinessAdaptorErrorType.SystemError);

                ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                return formatOutputXML(null, false, systemErrors);
            }
            AdaptorCacheRecord adaptor = (AdaptorCacheRecord)m_AdaptorCache[functionName];

            BusinessAdaptorErrors errOut = null;
            // Obtain an instance of the business adaptor class.
            Object obj = null;
            try
            {
                // Load the assembly
                Assembly a = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "\\bin\\" + adaptor.Assembly);   // TO DO: Hardwired to go to bin subdirectory right now. Probably needs to change.
                if (a == null)
                {
                    // Throw back system error if cannot load assembly.
                    systemErrors.Add("Riskmaster.WebService.Common.ErrorLoadingAssembly",
                        Globalization.GetString("Riskmaster.WebService.Common.ErrorLoadingAssembly",iClientId),
                        BusinessAdaptorErrorType.SystemError);

                    ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                    return formatOutputXML(null, false, systemErrors);
                }

                // Get the type of the class
                Type t = a.GetType(adaptor.Class);
                if (t == null)
                {
                    // Throw back system error if cannot find class in assembly.
                    systemErrors.Add("Riskmaster.WebService.Common.ClassNotFound",
                        Globalization.GetString("Riskmaster.WebService.Common.ClassNotFound",iClientId),
                        BusinessAdaptorErrorType.SystemError);

                    ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                    return formatOutputXML(null, false, systemErrors);
                }

                // Create an instance of the class
                obj = Activator.CreateInstance(t);
                if (obj == null)
                {
                    // Throw back system error if cannot create instance of class.
                    systemErrors.Add("Riskmaster.WebService.Common.CreateInstanceFailed",
                        Globalization.GetString("Riskmaster.WebService.Common.CreateInstanceFailed",iClientId),
                        BusinessAdaptorErrorType.SystemError);

                    ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                    return formatOutputXML(null, false, systemErrors);
                }
            }
            catch (Exception e)
            {
                // Throw back system error if exception is thrown during load/create class process.
                systemErrors.Add("Riskmaster.WebService.Common.ClassCreationException",
                    Globalization.GetString("Riskmaster.WebService.Common.ClassCreationException",iClientId),
                    BusinessAdaptorErrorType.SystemError);

                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError); //Throw exception in as well as a secondary error message.

                ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                return formatOutputXML(null, false, systemErrors);
            }

            // Check to see if the assembly has the "bypass security" flag
            //  set. If so, it can be called without security session validation.
            if (!adaptor.BypassSecurity)
            {

                // Validate security session key and retrieve UserLogin object
                //  (unless "bypass security" flag set).
                sessionKeySupplied = xmlAuth.InnerText;

                SessionManager oSession = SessionManager.LoadSession(sessionKeySupplied, iClientId);
                if (oSession == null) // If user not in session'
                {
                    throw new Exception(Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized",iClientId));
                }
                else //Session Exists - unpack UserLogin Object
                {
                    oUserLogin = new UserLogin(iClientId);
                    oUserLogin = (UserLogin)Utilities.BinaryDeserialize(oSession.GetBinaryItem(AppConstants.SESSION_OBJ_USER));

                    systemErrors = new BusinessAdaptorErrors(oUserLogin,iClientId); //Apply Login Info for possible later uncaught execptions.

                    //Initiates the Business Adaptor based on the Interface definition
                    IBusinessAdaptor pBusAdaptor = obj as IBusinessAdaptor;

                    //Only initiate the Business Adaptor if it is exists and can be instantiated
                    if (pBusAdaptor != null)
                    {
                        if (oUserLogin != null) //BSB Protect for "bypass security" case.
                            pBusAdaptor.SetSecurityInfo(oUserLogin, iClientId);

                        // Pass session object to business adaptor
                        if (oSession != null)
                        {
                            pBusAdaptor.SetSessionObject(oSession);
                        }//if
                    }//if
                }//else
            }//if
            else
            {
               //Send the client forcefully for use in appplication layer
               //Initiates the Business Adaptor based on the Interface definition
               IBusinessAdaptor pBusAdaptor = obj as IBusinessAdaptor;
               //Only initiate the Business Adaptor if it is exists and can be instantiated
               if (pBusAdaptor != null)
               {
                   pBusAdaptor.SetSecurityInfo(null, iClientId);
               }
            }
            // Create a delegate and bind it to the method we want to call
            BusinessAdaptorMethod methodDelegate = null;
            try
            {
                methodDelegate =
                    (BusinessAdaptorMethod)BusinessAdaptorMethod.CreateDelegate(typeof(BusinessAdaptorMethod), obj, adaptor.Method, true);
                if (methodDelegate == null)
                {
                    // Throw back system error if can't create a delegate instance.
                    systemErrors.Add("Riskmaster.WebService.Common.CannotCreateDelegate",
                        Globalization.GetString("Riskmaster.WebService.Common.CannotCreateDelegate",iClientId),
                        BusinessAdaptorErrorType.SystemError);

                    ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                    return formatOutputXML(null, false, systemErrors);
                }
            }
            catch (Exception e)
            {
                // Throw back system error if can't create a delegate instance.
                systemErrors.Add("Riskmaster.WebService.Common.CannotCreateDelegate",
                    Globalization.GetString("Riskmaster.WebService.Common.CannotCreateDelegate",iClientId),
                    BusinessAdaptorErrorType.SystemError);

                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError); //Throw exception in as well as a secondary error message.

                ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                return formatOutputXML(null, false, systemErrors);
            }

            // Call the business adaptor method
            bool bResult = false;
            try
            {
                errOut = new BusinessAdaptorErrors(oUserLogin,iClientId); // 8.24.2005  JP   Some business adaptors weren't allocating this on their own. Put this in to avoid rework on all of those adaptors.
                xmlOut = new XmlDocument();           // 8.25.2005  JP   Many business adaptors apparently don't allocate their output DOMs - even though it was clearly documented that this was a requirement. So, do this to let them off easy.

                XmlDocument xmlTmp = new XmlDocument();
                xmlTmp.LoadXml(xmlIn.OuterXml);

                bResult = methodDelegate(xmlTmp, ref xmlOut, ref errOut);

            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);

                ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                return formatOutputXML(null, false, systemErrors);
            }

            // Format the return message
            try
            {
                xmlOutEnvelope = formatOutputXML(xmlOut, bResult, errOut);
            }
            catch (Exception e)
            {
                // Throw back system error if there was a problem formatting the return envelope.
                systemErrors.Add("Riskmaster.WebService.Common.ErrorFormattingResultEnvelope",
                    Globalization.GetString("Riskmaster.WebService.Common.ErrorFormattingResultEnvelope",iClientId),
                    BusinessAdaptorErrorType.SystemError);

                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError); //Throw exception in as well as a secondary error message.

                ServiceLogger.LogErrors(functionName, xmlRequest, false, systemErrors, iClientId);

                return formatOutputXML(null, false, systemErrors);
            }

            //Only compile the code if the debug directive is defined
            #if DEBUG
                // Final log if all went well
            ServiceLogger.LogErrors(functionName, xmlRequest, bResult, errOut, iClientId);
            #endif

            // Return results
            return xmlOutEnvelope;
        }
        #endregion

        #region Adaptor class
        private class AdaptorCacheRecord
        {
            public string Name;			// Name key. This is what the common envelope uses to reference the function.
            public string Assembly;		// Assembly the class lives in. Filename of DLL.
            public string Class;			// Class method lives in.
            public string Method;			// Method to call in class.
            public bool BypassSecurity;	// If true, bypass AuthorizationToken check.
        }
        #endregion

    }
}
