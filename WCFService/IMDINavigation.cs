﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using Riskmaster.Models;
namespace RiskmasterService
{

    [ServiceContract]
    public interface IMDINavigation
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/profile", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MDINavigationTreeNodeProfileResponse GetMDINavigationTreeNodeProfileData(MDINavigationTreeNodeProfileRequest oMDINavigationTreeNodeProfileRequest);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/profilebyparent", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MDINavigationTreeNodeProfileResponse GetMDINavigationTreeNodeProfileDataByParent(MDINavigationTreeNodeProfileRequestByParent oMDINavigationTreeNodeProfileRequestByParent);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/init", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        InitMDIResponse InitMDI(InitMDIRequest oInitMDIRequest);
        //nkaranam2 - 34408
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/settings", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GetSysSettingsResponse GetSysSettings(GetSysSettingsRequest oGetSysSettingsRequest);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/GetClaimantDetails", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MDIClaimantFinancialRes GetClaimantDetails(MDIClaimantFinancialReq oMDIClaimantFinancialReq);
    }
   
}
