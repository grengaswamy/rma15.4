﻿using Newtonsoft.Json;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Models;
using Riskmaster.Security;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Riskmaster.Cache;
using Riskmaster.Security.Authentication;
using Riskmaster.Db;
using Riskmaster.Security.RMApp;
using System.ServiceModel.Activation;
using System.Web;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Mobile" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Mobile.svc or Mobile.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Mobile : RMService, IMobile
    {
        #region Authentication & Security functions are handled directly
      
        public bool Authenticate(Stream authData)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AuthData));
            AuthData user = (AuthData)serializer.ReadObject(authData);
            string strMessage = string.Empty;
            user.ClientId = int.Parse(HttpContext.Current.Request.Headers["ClientId"].ToString());
            bool blnIsAuthenticated = MembershipProviderFactory.AuthenticateUser(ConfigurationInfo.GetSecurityConnectionString(user.ClientId), user.UserName, user.Password, out strMessage, user.ClientId);
            return blnIsAuthenticated;
        }

        public Dictionary<string, string> GetUserDSNs(Stream authData)
        {

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AuthData));
            AuthData user = (AuthData)serializer.ReadObject(authData);
            int iClientId = int.Parse(HttpContext.Current.Request.Headers["ClientId"].ToString());
            user.ClientId = iClientId;
            Authenticate authenticate = new Authenticate();
            return authenticate.GetUserDSNs(user);
        }

        public string GetUserSessionID(Stream authData)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AuthData));
            AuthData user = (AuthData)serializer.ReadObject(authData);
            int iClientId = int.Parse(HttpContext.Current.Request.Headers["ClientId"].ToString());
            user.ClientId = iClientId;
            Authenticate authenticate = new Authenticate();
            return authenticate.GetUserSessionID(user);
        }

        public void LogOut()
        {
            int iClientId = int.Parse(HttpContext.Current.Request.Headers["ClientId"].ToString());
            string strSessionID = HttpContext.Current.Request.Headers["session"];
            RMSessionManager.RemoveSession(ConfigurationInfo.GetSessionConnectionString(iClientId), strSessionID, string.Empty);
        }

        public bool ChangePassword(Stream authData)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AuthData));
            AuthData user = (AuthData)serializer.ReadObject(authData);
            Authenticate authenticate = new Authenticate();
            return authenticate.ChangePassword(user);
        }

        public string GetUsername(Stream authData)
        {
            int iClientId = int.Parse(HttpContext.Current.Request.Headers["ClientId"].ToString());
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AuthData));
            AuthData user = (AuthData)serializer.ReadObject(authData);
            user.ClientId = iClientId;
            Authenticate authenticate = new Authenticate();
            return authenticate.GetUserName(user);
        }
        
        #endregion

        #region Adjuster data functions calling CommonWCF service

        public Stream Claims()
        {
            CommonWCFService commonWCFService = new CommonWCFService();
            XmlDocument xmlDocResponse = new XmlDocument();
            string session = HttpContext.Current.Request.Headers["session"];
            string sClientID = HttpContext.Current.Request.Headers["ClientId"];
            session = session.Replace("\"", "");
            //RMA-17041
            string claimsRequest = "<Message><Authorization>" + session + "</Authorization><ClientId>" + sClientID + "</ClientId><Call><Function>SearchAdaptor.SearchRun</Function></Call><Document><Search catid=\"2\"><caller>MobilityAdjusterClaimList</caller><PageNo>1</PageNo><TotalRecords /><SortColPass /><LookUpId>0</LookUpId><PageSize>100</PageSize><MaxResults>500</MaxResults><SortColumn /><Order>ascending</Order><IfBack /><SearchMain><LangId>1033</LangId><IfLookUp>0</IfLookUp><InProcess>0</InProcess><ScreenFlag>1</ScreenFlag><ViewId>-1000</ViewId><FormName>claim</FormName><FormNameTemp /><TableRestrict /><codefieldrestrict /><codefieldrestrictid /><tablename /><rowid /><eventdate /><claimdate /><policydate /><filter /><SysEx /><Settings /><TableID /><SoundEx>-1</SoundEx><DefaultSearch /><DefaultViewId /><Views /><CatId>2</CatId><DisplayFields><OrderBy1 /><OrderBy2 /><OrderBy3 /></DisplayFields><SearchCat /><AdmTable /><EntityTableId /><Type /><LookUpType /><SearchFields><field fieldtype=\"1\" id=\"FLD100\" issupp=\"0\" table=\"CLAIM\" name=\"CLAIM_NUMBER\" type=\"text\" tabindex=\"1\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD8501\" issupp=\"0\" table=\"POLICY\" name=\"POLICY_NAME\" type=\"text\" tabindex=\"2\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD1001\" issupp=\"0\" table=\"CLAIMANT_ENTITY\" name=\"FIRST_NAME\" type=\"text\" tabindex=\"3\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD1000\" issupp=\"0\" table=\"CLAIMANT_ENTITY\" name=\"LAST_NAME\" type=\"text\" tabindex=\"4\"><Operation>=</Operation></field><field fieldtype=\"7\" id=\"FLD101\" issupp=\"0\" table=\"CLAIM\" name=\"DATE_OF_CLAIM\" type=\"date\" tabindex=\"5\"><Operation>between</Operation><StartDate></StartDate><EndDate></EndDate></field><field fieldtype=\"3\" id=\"FLD104\" issupp=\"0\" table=\"CLAIM\" name=\"CLAIM_STATUS_CODE\" codetable=\"CLAIM_STATUS\" type=\"code\" tabindex=\"6\"><Operation>=</Operation><_cid /><_tableid>CLAIM</_tableid></field><field fieldtype=\"1\" id=\"FLD2001\" issupp=\"0\" table=\"ADJUSTER_ENTITY\" name=\"FIRST_NAME\" type=\"text\" tabindex=\"7\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD2000\" issupp=\"0\" table=\"ADJUSTER_ENTITY\" name=\"LAST_NAME\" type=\"text\" tabindex=\"8\"><Operation>=</Operation></field></SearchFields><SelOrgLevels /><SettoDefault>-1</SettoDefault><AllowPolicySearch /></SearchMain></Search></Document></Message>";           
            xmlDocResponse.LoadXml(commonWCFService.ProcessRequest(claimsRequest));
            return XmlToJson(xmlDocResponse);

        }

        public Stream Claim(string claimNumber)
        {

            CommonWCFService commonWCFService = new CommonWCFService();
            XmlDocument xmlDocResponse = new XmlDocument();
            string session = HttpContext.Current.Request.Headers["session"];
            string sClientID = HttpContext.Current.Request.Headers["ClientId"];
            session = session.Replace("\"", "");
            string claimsDetail = "<Message><Authorization>" + session + "</Authorization><ClientId>" + sClientID + "</ClientId><Call><Function>ExecutiveSummaryAdaptor.ClaimExecSumm</Function></Call><Document><ExecutiveSummaryReport><caller>MobilityAdjuster</caller><ClaimNumber>" + claimNumber + "</ClaimNumber><File></File></ExecutiveSummaryReport></Document></Message>";

            xmlDocResponse.LoadXml(commonWCFService.ProcessRequest(claimsDetail));
            return XmlToJson(xmlDocResponse);

        }

        public Stream Events()
        {
            CommonWCFService commonWCFService = new CommonWCFService();
            XmlDocument xmlDocResponse = new XmlDocument();
            string session = HttpContext.Current.Request.Headers["session"];
            string sClientID = HttpContext.Current.Request.Headers["ClientId"];
            session = session.Replace("\"", "");
            //RMA-17041
            string eventsList = "<Message><Authorization>" + session + "</Authorization><ClientId>" + sClientID + "</ClientId><Call><Function>SearchAdaptor.SearchRun</Function></Call><Document><Search><caller>MobilityAdjusterEventList</caller><PageNo>1</PageNo><TotalRecords /><SortColPass /><LookUpId>0</LookUpId><PageSize>100</PageSize><MaxResults>500</MaxResults><SortColumn /><Order>ascending</Order><IfBack /><SearchMain><LangId>1033</LangId><IfLookUp>0</IfLookUp><InProcess>0</InProcess><ScreenFlag>1</ScreenFlag><ViewId>-1002</ViewId><FormName>event</FormName><FormNameTemp /><TableRestrict></TableRestrict><codefieldrestrict></codefieldrestrict><codefieldrestrictid></codefieldrestrictid><tablename></tablename><rowid></rowid><eventdate></eventdate><claimdate></claimdate><policydate></policydate><filter></filter><SysEx /><Settings /><TableID /><SoundEx>-1</SoundEx><DefaultSearch /><DefaultViewId /><Views /><CatId>2</CatId><DisplayFields><OrderBy1></OrderBy1><OrderBy2></OrderBy2><OrderBy3></OrderBy3></DisplayFields><SearchCat /><AdmTable /><EntityTableId /><Type /><LookUpType /><SearchFields><field fieldtype=\"1\" id=\"FLD70375\" issupp=\"0\" table=\"ADJUSTER_ENTITY\" name=\"FIRST_NAME\" type=\"text\" tabindex=\"1\"><Operation>=</Operation></field><field fieldtype=\"1\" id=\"FLD70376\" issupp=\"0\" table=\"ADJUSTER_ENTITY\" name=\"LAST_NAME\" type=\"text\" tabindex=\"2\"><Operation>=</Operation></field><field fieldtype=\"3\" id=\"FLD70374\" issupp=\"0\" table=\"CLAIM\" name=\"CLAIM_STATUS_CODE\" codetable=\"\" type=\"code\" tabindex=\"3\"><Operation>=</Operation><_cid></_cid><_tableid>CLAIM</_tableid></field></SearchFields><SelOrgLevels></SelOrgLevels><SettoDefault>-1</SettoDefault><AllowPolicySearch></AllowPolicySearch></SearchMain></Search></Document></Message>";           
            xmlDocResponse.LoadXml(commonWCFService.ProcessRequest(eventsList));
            return XmlToJson(xmlDocResponse);

        }

        public Stream CreateTask(Stream task)
        {

            CommonWCFService commonWCFService = new CommonWCFService();
            XmlDocument xmlDocResponse = new XmlDocument();
            string session = HttpContext.Current.Request.Headers["session"];
            string sClientID = HttpContext.Current.Request.Headers["ClientId"];
            session = session.Replace("\"", "");
            string user = HttpContext.Current.Request.Headers["user"];
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Riskmaster.Models.Diary));
            Riskmaster.Models.Diary mobileTask = (Riskmaster.Models.Diary)serializer.ReadObject(task);

            string createTaskRequest = "<Message><Authorization>" + session + "</Authorization><ClientId>" + sClientID + "</ClientId><Call><Function>WPAAdaptor.SaveDiary</Function></Call><Document><SaveDiary><caller>MobilityAdjuster</caller><EntryId /><TaskSubject>" + mobileTask.name + "</TaskSubject><AssigningUser>" + user + "</AssigningUser><AssignedUser>" + user + "</AssignedUser><StatusOpen>1</StatusOpen><TeTracked /><TeTotalHours>56</TeTotalHours><TeExpenses /><TeStartTime /><TeEndTime /><ResponseDate /><Response /><TaskNotes>" + mobileTask.description + "</TaskNotes><DueDate>" + mobileTask.dueDate + "</DueDate><CompleteTime>" + mobileTask.dueTime + "</CompleteTime><EstimateTime /><Priority>2</Priority><ActivityString>" + mobileTask.activity + "</ActivityString><AutoConfirm /><NotifyFlag /><EmailNotifyFlag /><OverDueDiaryNotify /><AttachRecordId>" + mobileTask.recordId + "</AttachRecordId><AttachTable>" + mobileTask.table + "</AttachTable><Action>Create</Action></SaveDiary></Document></Message>";

            xmlDocResponse.LoadXml(commonWCFService.ProcessRequest(createTaskRequest));
            return XmlToJson(xmlDocResponse);
        }

        #endregion

        public Stream Tasks()
        {
            DiaryList diaryList = new DiaryList();
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(ProcessTasks(diaryList)));
            return memoryStream;

        }

        public Stream SyncTasks(String lastSyncDateTime)
        {
            DiaryList diaryList = new DiaryList();
            diaryList.lastSyncDateTime = lastSyncDateTime; //setting last sync stamp
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(ProcessTasks(diaryList)));
            return memoryStream;

        }

        private string ProcessTasks(DiaryList diaryList)
        {
            string session = HttpContext.Current.Request.Headers["session"];
            string sClientID = HttpContext.Current.Request.Headers["ClientId"];
            string user = HttpContext.Current.Request.Headers["user"];
            session = session.Replace("\"", "");

            diaryList.ClientId = Int32.Parse(sClientID);
            diaryList.Token = session;

            //Requesting Diaries
            Diary diaryService = new Diary();
            diaryList = diaryService.GetDiaries(diaryList, user);

            //Making JSON of response
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string jsonString = javaScriptSerializer.Serialize(diaryList);
            return jsonString;
        }

        #region Adjuster data functions calling REST services

        //Notes
        public Stream Notes(Stream note)
        {
            string session = HttpContext.Current.Request.Headers["session"];
            string sClientID = HttpContext.Current.Request.Headers["ClientId"];
            session = session.Replace("\"", "");

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ProgressNote));
            ProgressNote progressNote = (ProgressNote)serializer.ReadObject(note);
            ProgressNote n = new ProgressNote();
            progressNote.ClientId = Int32.Parse(sClientID);
            progressNote.Token = session;

            ProgressNotes progressNotes = new ProgressNotes();
            var result = progressNotes.SaveNotes(progressNote);

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string jsonString = javaScriptSerializer.Serialize(result);
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));

            return memoryStream;
        }

        //Codes        
        public Stream Codes()
        {
            string session = HttpContext.Current.Request.Headers["session"];
            int iClientId = int.Parse(HttpContext.Current.Request.Headers["ClientId"].ToString());
            session = session.Replace("\"", "");

            //List of Codes
            string[] codeTablesRequest = { "CLAIM_STATUS", "UNIT_TYPE_CODE", "STATES", "NOTE_TYPE_CODE", "WPA_ACTIVITIES", "PERSON_INV_TYPE", "ENTITY", "CLAIM_TYPE", "OTHER_PEOPLE", "DOCUMENT_TYPE", "POLICY_CLAIM_LOB" }; //RMA-10039

            Codes codes = new Codes();
            CodeTypeRequest objCode = null;
            BusinessAdaptorErrors systemErrors = null;
            XmlDocument xmlResponse = new XmlDocument();
            XmlDocument xmlRequest = new XmlDocument();
            CodeListType objList = null;
            List<CodeListType> result = new List<CodeListType>();

            try
            {
                objCode = new CodeTypeRequest();
                objCode.ClientId = iClientId;
                foreach (string table in codeTablesRequest)
                {
                    objCode.TableName = table;
                    objCode.FormName = "search";
                    objCode.Token = session;
                    objCode.bIsMobileAdjuster = true;
                    objCode.TriggerDate = "";
                    objCode.TypeLimits = "";
                    objCode.DeptEId = "";
                    objCode.SessionLOB = "";
                    objCode.EventDate = "";
                    objCode.SessionClaimId = "";
                    objCode.RecordCount = "";
                    objCode.PageNumber = "";
                    objCode.Title = "";
                    objCode.InsuredEid = "";
                    objCode.EventId = "";
                    objCode.ParentCodeID = "";
                    objCode.Filter = "";
                    objCode.ShowCheckBox = "";
                    objCode.LOB = "";
                    if (objCode.TableName.Equals("ENTITY"))
                    {
                        objCode.SortColumn = "4";
                    }
                    else
                    {
                        objCode.SortColumn = "1";
                    }
                    objCode.SortOrder = "Asc";
                    objList = codes.GetCodes(objCode);
                    result.Add(objList);
                }
            }

            catch (Exception e)
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            string json = JsonConvert.SerializeObject(result);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(json));
            return memoryStream;
        }

        // Attachments
        public bool Attachment(Stream attachment)
        {
            DataContractJsonSerializer serializer = null;
            DocumentType document = null;
            Document documentService = null;
            RMServiceType returnVal = null;

            bool bReturn = false;

            string session = HttpContext.Current.Request.Headers["session"];
            int iClientId = int.Parse(HttpContext.Current.Request.Headers["ClientId"].ToString());
            session = session.Replace("\"", "");
            try
            {

                serializer = new DataContractJsonSerializer(typeof(DocumentType));
                document = (DocumentType)serializer.ReadObject(attachment);
                documentService = new Document();
                document.Token = session;
                document.ClientId = iClientId;
                document.Class = new CodeType();
                document.Category = new CodeType();
                document.Type = new CodeType();

                document.FileContents = format(document.base64EncodedFileContents, iClientId);
                document.Type.Id = Int32.Parse(document.typeId);
                document.FolderId = "";
                document.PsId = 0;
                document.FilePath = "";
                //document.Class.Id = 0;
                //document.Class.Desc = "";
                //document.Category.Id = 0;
                //document.Category.Desc = "";
                document.ScreenFlag = "";
                document.Keywords = "";
                document.FormName = "";
                document.AttachRecordId = 0;


                if (!string.IsNullOrEmpty(document.Claimnumber))
                {
                    document.AttachTable = "CLAIM";
                    document.ClaimId = -1;
                    document.EventId = 0;
                }
                else if (!string.IsNullOrEmpty(document.Eventnumber))
                {
                    document.AttachTable = "EVENT";
                    document.ClaimId = 0;
                    document.EventId = -1;
                }
                document.bMobileAdj = true;
                document.bMobilityAdj = true;

                returnVal = documentService.CreateDocument(document);
                if (returnVal.Errors == null)
                {
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), iClientId);

            }

            return bReturn;
        }

        public Stream GetReportees()
        {
            int clientId = int.Parse(HttpContext.Current.Request.Headers["ClientId"].ToString());
            string session = HttpContext.Current.Request.Headers["session"];
            session = session.Replace("\"", "");

            SessionManager oSession = SessionManager.LoadSession(session, clientId);
            UserLogin auth = new UserLogin(clientId);
            auth = (UserLogin)Utilities.BinaryDeserialize(oSession.GetBinaryItem(AppConstants.SESSION_OBJ_USER));

            UserLogin user = new UserLogin(auth.LoginName, auth.Password, auth.objRiskmasterDatabase.DataSourceName, clientId);

            WPAAdaptor diaryAdapter = new WPAAdaptor();
            diaryAdapter.SetSecurityInfo(user, clientId);
            diaryAdapter.ClientId = clientId;
            string peekList = string.Empty;
            BusinessAdaptorErrors error = new BusinessAdaptorErrors(clientId);

            String response = string.Empty;
            try
            {
                if (diaryAdapter.GetAvailablePeekList(ref peekList, ref error))
                {
                    response = peekList;
                }
                else
                {
                    response = "error";
                }
            }
            catch (Exception e)
            {
                Log.Write(e.ToString(), clientId);
                response = "error";
            }
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(response));
            return memoryStream;
        }

        #endregion

        //XML to json conversion
        Stream XmlToJson(XmlDocument xmlDoc)
        {

            //convert XML Object to json object
            String jsonResponse = JsonConvert.SerializeXmlNode(xmlDoc);
            jsonResponse = jsonResponse.Replace(@"@", "");
            //jsonResponse = jsonResponse.Replace(@"\", "");
            jsonResponse = jsonResponse.Replace(@"#", "");
            jsonResponse = jsonResponse.Replace("null", "\"\"");

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(jsonResponse));

            return memoryStream;
        }



        // Convert the Base64 UUEncoded input into binary output.
        byte[] format(string base64String, int iClientId)
        {
            byte[] binaryData;
            try
            {
                binaryData =
                   System.Convert.FromBase64String(base64String);
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), iClientId);
                return null;
            }
            return binaryData;
        }

    }
}
