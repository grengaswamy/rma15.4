﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Xml;

namespace RiskmasterService
{
    // NOTE: If you change the class name "CommonWCFService" here, you must also update the reference to "CommonWCFService" in Web.config.
    /// <summary>
    /// Common Windows Communication Foundation Service used for handling RMX 
    /// </summary>
    [AspNetCompatibilityRequirements(
     RequirementsMode=AspNetCompatibilityRequirementsMode.Required)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public partial class CommonWCFService : ICommonWCFService
    {
        /// <summary>
        /// Processes requests and responses that are cross-platform compliant
        /// using Windows Communication Foundation
        /// </summary>
        /// <param name="xmlRequest"></param>
        /// <returns></returns>
        public string ProcessRequest(string xmlRequest)
        {
            XmlDocument xmlDocRequest = new XmlDocument();
            XmlDocument xmlDocResponse = new XmlDocument();

            //Load the specified XML
            xmlDocRequest.LoadXml(xmlRequest);

            xmlDocResponse = ProcessRequest(xmlDocRequest);

            //return the resultant xml output
            return xmlDocResponse.OuterXml;
            
        }
    }
}
