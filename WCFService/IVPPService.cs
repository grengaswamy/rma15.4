﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: If you change the interface name "IVPPService" here, you must also update the reference to "IVPPService" in Web.config.
    [ServiceContract]
    public interface IVPPService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetFileContent(VPPFileRequest request, out VPPFileResponse response);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetInitialTimeStamp(RMServiceType request, out VPPTimeStampResponse response);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetFileTimeStamp(VPPFileRequest request, out VPPTimeStampResponse response);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetModifiedPageList(VPPModifiedPagesRequest request, out VPPModifiedPagesResponse response);
 
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetDataSourceID(RMServiceType request, out VPPGetDataSourceIDResponse response);
    }
}
