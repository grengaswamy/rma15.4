﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPortalService" in both code and config file together.
    /// <summary>
    /// IPortal
    /// </summary>
    [ServiceContract]
    public interface IPortal
    {
        /// <summary>
        /// Get ContentInfo
        /// </summary>
        /// <param name="oPortalData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/ContentInfo", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string ContentInfo(PortalData oPortalData);

        /// <summary>
        /// HomeContentInfo
        /// </summary>
        /// <param name="oPortalData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/homeinfo", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PortalData HomeContentInfo(PortalData oPortalData);
        
        /// <summary>
        /// ContentInfo
        /// </summary>
        /// <param name="oPortalData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/filecontent", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        byte[] FileContent(PortalData oPortalData);

        /// <summary>
        /// update content
        /// </summary>
        /// <param name="oPortalData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "/Content", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void ContentUpdate(PortalData oPortalData);

        /// <summary>
        /// Insert content
        /// </summary>
        /// <param name="oPortalData"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Content", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void ContentInsert(PortalData oPortalData);
    }
}
