﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;


namespace RiskmasterService
{
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    // NOTE: If you change the class name "DiaryCalendar" here, you must also update the reference to "DiaryCalendar" in Web.config.
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class DiaryCalendar : RMService, IDiaryCalendar
    {
        /// <summary>
        /// Gets the diaries
        /// </summary>
        /// <param name="DiaryInputObject">DiaryCalendarModels.DiaryInput</param>
        /// <param name="DiaryOutputObject">ref DiaryCalendarModels.DiaryOutput</param>
        public DiaryOutput GetDiaryList(DiaryInput DiaryInputObject)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            WPAAdaptor objDiaries = null;
            DiaryOutput DiaryOutputObject = new DiaryOutput();
             
            BusinessAdaptorErrors systemErrors = null;
            XmlDocument outPutGetDiaryDoc = null;
            XmlDocument inputGetDiaryDoc = null;
            string functionName = "GetDiaryList";

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(DiaryInputObject.ClientId);
                objDiaries = new WPAAdaptor();

                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(DiaryInputObject, out xmlRequest, functionName, objDiaries, out oUserLogin, ref  systemErrors);

                outPutGetDiaryDoc = new XmlDocument();
                string userName = string.Empty;

                inputGetDiaryDoc = new XmlDocument();
                inputGetDiaryDoc.LoadXml(DiaryInputObject.GetDiaryInput);

                objDiaries.GetDairiesForDairyCalender(inputGetDiaryDoc, ref outPutGetDiaryDoc, ref systemErrors);
                

                DiaryOutputObject.UserName = oUserLogin.LoginName;
                DiaryOutputObject.GetDiaryOutput = outPutGetDiaryDoc.OuterXml;
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                if (oUserLogin != null)
                {
                    oUserLogin = null;
                }
            }

            return DiaryOutputObject;
        }
        /// <summary>
        /// Gets the Calendar view
        /// </summary>
        /// <param name="DiaryInputObject">DiaryCalendarModels.DiaryInput</param>
        /// <param name="DiaryOutputObject">ref DiaryCalendarModels.DiaryOutput</param>
        public DiaryOutput GetCalendarView(DiaryInput DiaryInputObject)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            WPAAdaptor objDairies = null;
            BusinessAdaptorErrors systemErrors = null;
            XmlDocument outPutGetDiaryDoc = null;
            XmlDocument inputGetDiaryDoc = null;
            string functionName = "GetCalendarView";
            DiaryOutput DiaryOutputObject = new DiaryOutput(); 
            try
            {
                systemErrors = new BusinessAdaptorErrors(DiaryInputObject.ClientId);
                objDairies = new WPAAdaptor();

                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(DiaryInputObject, out xmlRequest, functionName, objDairies, out oUserLogin, ref  systemErrors);

                outPutGetDiaryDoc = new XmlDocument();
                string userName = string.Empty;

                inputGetDiaryDoc = new XmlDocument();
                //inputGetDairyDoc.LoadXml(DiaryInputObject.GetDairyInput);

                objDairies.GetCalendarView(inputGetDiaryDoc, ref outPutGetDiaryDoc, ref systemErrors);

                DiaryOutputObject.UserName = oUserLogin.LoginName;
                DiaryOutputObject.GetDiaryOutput = outPutGetDiaryDoc.OuterXml;
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                if (oUserLogin != null)
                {
                    oUserLogin = null;
                }
            }

            return DiaryOutputObject;
        }
        /// <summary>
        /// Gets the information at the time of Diary Calendar load
        /// </summary>
        /// <param name="DiaryInputObject">DiaryCalendarModels.DiaryInput</param>
        /// <param name="DiaryOutputObject">ref DiaryCalendarModels.DiaryOutput</param>
        public DiaryOutput GetOnLoadInformation(DiaryInput DiaryInputObject)
        {
            XmlDocument xmlRequest = null;
            UserLogin oUserLogin = null;
            WPAAdaptor objDiaries = null;
            BusinessAdaptorErrors systemErrors = null;
            XmlDocument outPutGetDiaryDoc = null;
            XmlDocument inputGetDiaryDoc = null;
            string functionName = "GetOnLoadInformation";
            DiaryOutput DiaryOutputObject = new DiaryOutput();
            try
            {
                systemErrors = new BusinessAdaptorErrors(DiaryInputObject.ClientId);
                objDiaries = new WPAAdaptor();

                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(DiaryInputObject, out xmlRequest, functionName, objDiaries, out oUserLogin, ref  systemErrors);

                outPutGetDiaryDoc = new XmlDocument();
                string userName = string.Empty;

                inputGetDiaryDoc = new XmlDocument();
                //inputGetDairyDoc.LoadXml(DiaryInputObject.GetDairyInput);

                objDiaries.GetOnLoadInformation(inputGetDiaryDoc, ref outPutGetDiaryDoc, ref systemErrors);

                DiaryOutputObject.UserName = oUserLogin.LoginName;
                DiaryOutputObject.GetDiaryOutput = outPutGetDiaryDoc.OuterXml;
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            finally
            {
                if (oUserLogin != null)
                {
                    oUserLogin = null;
                }
            }

            return DiaryOutputObject;
        }
    }
}
