﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Riskmaster.Models;
using System.Linq;
using System.Xml.Linq;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IXMLInput" in both code and config file together.
    [ServiceContract]
    public interface IXMLImport
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        //bool ProcessXML(XMLInput oXMLInput, out string logFileContent);//logFileContent Added for MITS 29711 by bsharma33
        bool ProcessXML(out XMLOutPut objOutPut, XMLInput oXMLInput, bool rVal); //prashbiharis 

        [OperationContract]
        [FaultContract(typeof(RMException))]
               void GetSuppTemplate(XMLTemplate oXML,out XMLTemplate objReturn);

    }
}
