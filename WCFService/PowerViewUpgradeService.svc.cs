﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;


namespace RiskmasterService
{
    // NOTE: If you change the class name "PowerViewUpgradeService" here, you must also update the reference to "PowerViewUpgradeService" in Web.config.
    [AspNetCompatibilityRequirements(
RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class PowerViewUpgradeService : RMService,IPowerViewUpgradeService
    {
        /// <summary>
        /// 
        /// </summary>
        public void DoWork()
        {
            
        } // method: DoWork

        public void UpgradeXmlTagToAspxForm(WCPowerViewUpgrade request , out WCPowerViewUpgrade objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "UpgradeXmlTagToAspxForm";
            UserLogin oUserLogin = null;
            PowerViewUpgradeAdaptor objPowerViewUpgradeAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                //convertedAspx = "";
                objPowerViewUpgradeAdaptor = new PowerViewUpgradeAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objPowerViewUpgradeAdaptor, out oUserLogin, ref systemErrors);
            
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(request.ClientId);
                bResult = objPowerViewUpgradeAdaptor.UpgradeXmlTagToAspxForm(request , out objReturn, ref errOut);//request, out  objReturn,
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
        public void FetchJurisdictionalData(WCPowerViewUpgrade request, out WCPowerViewUpgrade objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "FetchJurisdictionalData";
            UserLogin oUserLogin = null;
            PowerViewUpgradeAdaptor objPowerViewUpgradeAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                //jurisdictionalAspx = "";
                objPowerViewUpgradeAdaptor = new PowerViewUpgradeAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objPowerViewUpgradeAdaptor, out oUserLogin, ref systemErrors);

                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(request.ClientId);
                bResult = objPowerViewUpgradeAdaptor.FetchJurisdictionalData(request , out objReturn, ref errOut);//request, out  objReturn,
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }
    }
}
