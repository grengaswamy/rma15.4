﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;
namespace RiskmasterService
{
    // NOTE: If you change the interface name "IDocumentService" here, you must also update the reference to "IDocumentService" in Web.config.
    [ServiceContract]
    public interface IMobileDocumentService
    {                
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void CreateDocTypeFromXml_MobAdj(string request);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string MobileDocumentUploadRequest(string request);
    }

}
