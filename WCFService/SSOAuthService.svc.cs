﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Cache;
using System.Data;
using Riskmaster.Security.Encryption;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SSOAuthService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SSOAuthService.svc or SSOAuthService.svc.cs at the Solution Explorer and start debugging.
    // Service used for Active dir and LDAP -
    // Add by kuladeep for Jira-156
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SSOAuthService : RMService,ISSOAuthService
    {
        #region ISSOAuthService Attributes

        private const string LDAP_SSL = "LDAPS://";
        private const string LDAP_BASIC = "LDAP://";

        /// <summary>
        /// Holds value types for 
        /// connection string types to SSO Authentication Providers
        /// </summary>
        struct ConnectionTypes
        {
            public static string STANDARD = "1"; //Basic connection type w/o SSL
            public static string SSL = "2"; //connection type over SSL
        }

        #endregion // ISSOAuthService Attributes
        #region ISSOAuthService Members
        /// <summary>
        /// Gets the various types of authentication types available for SSO providers
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetSSLAuthTypes(RMServiceType oRMServiceType)
        {
            Dictionary<string, string> dictSSLAuthTypes = new Dictionary<string, string>();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strConnTypeBldr = new StringBuilder();

            strConnTypeBldr.Append("SELECT AUTH_PROVIDER_CONN_TYPE_ID,");
            strConnTypeBldr.Append("AUTH_PROVIDER_CONN_DESC ");
            strConnTypeBldr.Append("FROM AUTH_PROVIDER_CONN_TYPE");

            using (DbReader idr = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(oRMServiceType.ClientId), strConnTypeBldr.ToString(), dictParams))
            {
                //Loop through the DataReader
                while (idr.Read())
                {
                    //Add the record to the Dictionary collection
                    //NOTE: There are only 2 values to replace, therefore, only 2 columns required
                    dictSSLAuthTypes.Add(idr.GetValue(0).ToString(), idr.GetValue(1).ToString());
                }//while

            }//using

            return dictSSLAuthTypes;
        }//method: GetSSLAuthTypes()

        #region Select Statements
        /// <summary>
        /// Gets the specified Active Directory Provider information from the database
        /// </summary>
        /// <returns></returns>
        public DataSet GetADProvider(RMServiceType oRMServiceType)
        {

            StringBuilder strADBldr = new StringBuilder();

            strADBldr.Append("SELECT AUTH_PROVIDER_ID ProviderID,");
            strADBldr.Append("NAME HostName,");
            strADBldr.Append("ENABLED IsEnabled,");
            strADBldr.Append("CONN_STRING ConnectionString,");
            strADBldr.Append("ap.AUTH_PROVIDER_CONN_TYPE_ID ConnectionType,");
            strADBldr.Append("USER_ATTRIBUTE_MAPPING UserAttribute,");
            strADBldr.Append("ADMIN_UID AdminUserName,");
            strADBldr.Append("ADMIN_PWD AdminPwd ");
            strADBldr.Append("from auth_provider ap ");
            strADBldr.Append("inner join AUTH_PROVIDER_TYPE apt ");
            strADBldr.Append("on ap.AUTH_PROVIDER_TYPE_ID = apt.AUTH_PROVIDER_TYPE_ID ");
            strADBldr.Append("inner join AUTH_PROVIDER_CONN_TYPE apct ");
            strADBldr.Append("on ap.AUTH_PROVIDER_CONN_TYPE_ID = apct.AUTH_PROVIDER_CONN_TYPE_ID ");
            strADBldr.Append("where AUTH_PROVIDER_TYPE_DESC = ~AUTH_PROVIDER_TYPE~ "); //'Active Directory'

            Dictionary<string, object> dictParams = new Dictionary<string, object>();

            dictParams.Add("AUTH_PROVIDER_TYPE", "Active Directory");

            DataSet dstADProvider = DbFactory.ExecuteDataSet(ConfigurationInfo.GetSecurityConnectionString(oRMServiceType.ClientId), strADBldr.ToString(), dictParams, 0);
            DataTable dtADProvider = dstADProvider.Tables[0];

            if (dstADProvider.Tables[0].Rows.Count != 0)
            {
                Boolean bIsEnabled = Convert.ToBoolean(dstADProvider.Tables[0].Rows[0]["IsEnabled"]);
                dtADProvider.Columns.Remove("IsEnabled");
                dtADProvider.Columns.Add("IsEnabled", typeof(Boolean));
                dtADProvider.Rows[0]["IsEnabled"] = bIsEnabled;

            }
            //return dstADProvider.GetXml();

            return dstADProvider;

        }


        /// <summary>
        /// Gets the specified LDAP provider from the database
        /// </summary>
        /// <returns></returns>
        public DataSet GetLDAPProvider(RMServiceType oRMServiceType)
        {
            StringBuilder strADBldr = new StringBuilder();

            strADBldr.Append("SELECT AUTH_PROVIDER_ID ProviderID,");
            strADBldr.Append("NAME HostName,");
            strADBldr.Append("ENABLED IsEnabled,");
            strADBldr.Append("CONN_STRING ConnectionString,");
            strADBldr.Append("ap.AUTH_PROVIDER_CONN_TYPE_ID ConnectionType,");
            strADBldr.Append("USER_ATTRIBUTE_MAPPING UserAttribute,");
            strADBldr.Append("ADMIN_UID AdminUserName,");
            strADBldr.Append("ADMIN_PWD AdminPwd ");
            strADBldr.Append("from auth_provider ap ");
            strADBldr.Append("inner join AUTH_PROVIDER_TYPE apt ");
            strADBldr.Append("on ap.AUTH_PROVIDER_TYPE_ID = apt.AUTH_PROVIDER_TYPE_ID ");
            strADBldr.Append("inner join AUTH_PROVIDER_CONN_TYPE apct ");
            strADBldr.Append("on ap.AUTH_PROVIDER_CONN_TYPE_ID = apct.AUTH_PROVIDER_CONN_TYPE_ID ");
            strADBldr.Append("where AUTH_PROVIDER_TYPE_DESC = ~AUTH_PROVIDER_TYPE~ "); //'Active Directory'

            Dictionary<string, object> dictParams = new Dictionary<string, object>();

            dictParams.Add("AUTH_PROVIDER_TYPE", "LDAP");

            DataSet dstLDAPProvider = DbFactory.ExecuteDataSet(ConfigurationInfo.GetSecurityConnectionString(oRMServiceType.ClientId), strADBldr.ToString(), dictParams, 0);
            DataTable dtLDAPProvider = dstLDAPProvider.Tables[0];

            string sLDAPConn = String.Empty;
            string[] sLDAPConnParse;
            Boolean bIsEnabled;

            // Parse out the BaseLDAPDn information from the connection string and insert into the data set,
            // also convert the IsEnabled data to boolean so the forms work properly.
            if (dstLDAPProvider.Tables[0].Rows.Count != 0)
            {
                bIsEnabled = Convert.ToBoolean(dtLDAPProvider.Rows[0]["IsEnabled"]);

                dtLDAPProvider.Columns.Add("BaseDN");
                dtLDAPProvider.Columns.Remove("IsEnabled");
                dtLDAPProvider.Columns.Add("IsEnabled", typeof(Boolean));
                dtLDAPProvider.Rows[0]["IsEnabled"] = bIsEnabled;

                sLDAPConn = dtLDAPProvider.Rows[0]["ConnectionString"].ToString();
                sLDAPConnParse = sLDAPConn.Split('/');
                dtLDAPProvider.Rows[0]["BaseDN"] = sLDAPConnParse[3];

            } // if



            //return dstLDAPProvider.GetXml();
            return dstLDAPProvider;
        }
        #endregion

        #region Insert Statements
        /// <summary>
        /// Inserts a new Active Directory record into the database
        /// </summary>
        /// <param name="objADProvider"></param>
        public void InsertADProvider(RiskmasterMembershipProvider objADProvider)
        {
            StringBuilder strSQL = new StringBuilder();
            string sSecurityConnectionString = ConfigurationInfo.GetSecurityConnectionString(objADProvider.ClientId);
            eDatabaseType eSecurityDbType = DbFactory.GetDatabaseType(sSecurityConnectionString);

            // If the AD source will be enabled, disable all other sources
            if (objADProvider.IsEnabled)
            {
                strSQL.Append("UPDATE AUTH_PROVIDER ");
                if (eSecurityDbType == eDatabaseType.DBMS_IS_ORACLE)
                    strSQL.Append("SET ENABLED = 0");
                else
                    strSQL.Append("SET [ENABLED] = 0");

                DbFactory.ExecuteScalar(sSecurityConnectionString, strSQL.ToString());
                strSQL = new StringBuilder();
                //MITS 24086
                strSQL.Append("UPDATE SETTINGS ");
                strSQL.Append("SET DOMAIN_LOGIN_ENABLED = -1");

                DbFactory.ExecuteScalar(sSecurityConnectionString, strSQL.ToString());
                strSQL = new StringBuilder();
                //MITS 24086
            } // if
            else
            {
                //MITS 24086
                strSQL.Append("UPDATE SETTINGS ");
                strSQL.Append("SET DOMAIN_LOGIN_ENABLED = 0");

                DbFactory.ExecuteScalar(sSecurityConnectionString, strSQL.ToString());
                strSQL = new StringBuilder();
                //MITS 24086
            }
            strSQL.Append("INSERT INTO AUTH_PROVIDER ");
            strSQL.Append("(AUTH_PROVIDER_ID, ");
            strSQL.Append("AUTH_PROVIDER_TYPE_ID, ");
            strSQL.Append("AUTH_PROVIDER_CONN_TYPE_ID, ");
            strSQL.Append("NAME, ");
            strSQL.Append("ENABLED, ");
            strSQL.Append("CONN_STRING, ");
            strSQL.Append("ADMIN_UID, ");
            strSQL.Append("ADMIN_PWD)");
            strSQL.Append("VALUES ");
            strSQL.Append("(");
            strSQL.Append("~AUTH_PROVIDER_ID~,");
            strSQL.Append("~AUTH_PROVIDER_TYPE_ID~,");
            strSQL.Append("~CONN_TYPE_ID~,");
            strSQL.Append("~HOSTNAME~,");
            strSQL.Append("~ENABLED~,");
            strSQL.Append("~CONN_STRING~,");
            strSQL.Append("~ADMIN_UID~,");
            strSQL.Append("~ADMIN_PWD~");
            strSQL.Append(")");

            Dictionary<string, object> dictParams = new Dictionary<string, object>();

            //dictParams.Add("AUTH_PROVIDER_TYPE_ID", objADProvider.ProviderID);
            dictParams.Add("AUTH_PROVIDER_ID", GetNextProviderID(objADProvider.ClientId));
            dictParams.Add("AUTH_PROVIDER_TYPE_ID", 1); //TODO: Hook in the Provider Type ID as part of the Data Source Provider
            dictParams.Add("CONN_TYPE_ID", objADProvider.ConnectionType);
            dictParams.Add("HOSTNAME", objADProvider.HostName);
            dictParams.Add("ENABLED", Convert.ToInt16(objADProvider.IsEnabled));
            dictParams.Add("CONN_STRING", GenerateADConnectionString(objADProvider.HostName, objADProvider.ConnectionType));
            dictParams.Add("ADMIN_UID", objADProvider.AdminUserName);
            dictParams.Add("ADMIN_PWD", RMCryptography.EncryptString(objADProvider.AdminPwd));//rsushilaggar MITS 23841

            // dictParams.Add("AUTH_PROVIDER_ID", objADProvider.ProviderID);

            DbFactory.ExecuteNonQuery(sSecurityConnectionString, strSQL.ToString(), dictParams);
            //MITS 24086
            if (!GetCountDom(objADProvider.ClientId))
            {
                strSQL = new StringBuilder();
                strSQL.Append("INSERT INTO DOMAIN ");
                strSQL.Append("(NAME, ");
                strSQL.Append("PROVIDER, ");
                strSQL.Append("ENABLED, ");
                strSQL.Append("DOMAINID, ");
                strSQL.Append("DTTM_RCD_LAST_UPD, ");
                strSQL.Append("UPDATED_BY_USER)");
                strSQL.Append("VALUES ");
                strSQL.Append("(");
                strSQL.Append("~NAME~,");
                strSQL.Append("~PROVIDER~,");
                strSQL.Append("~ENABLED~,");
                strSQL.Append("~DOMAINID~,");
                strSQL.Append("~DTTM_RCD_LAST_UPD~,");
                strSQL.Append("~UPDATED_BY_USER~");
                strSQL.Append(")");

                dictParams = new Dictionary<string, object>();

                dictParams.Add("NAME", objADProvider.HostName);
                dictParams.Add("PROVIDER", 0);
                dictParams.Add("ENABLED", Convert.ToInt16(objADProvider.IsEnabled));
                dictParams.Add("DOMAINID", GetNextDomainID(objADProvider.ClientId));
                dictParams.Add("DTTM_RCD_LAST_UPD", "");
                dictParams.Add("UPDATED_BY_USER", "");

                DbFactory.ExecuteNonQuery(sSecurityConnectionString, strSQL.ToString(), dictParams);
            }
            else
            {
                strSQL = new StringBuilder();
                strSQL.Append("UPDATE DOMAIN ");

                if (eSecurityDbType == eDatabaseType.DBMS_IS_ORACLE)
                    strSQL.AppendFormat("SET ENABLED =~ENABLED~,");
                else
                    strSQL.AppendFormat("SET [ENABLED] =~ENABLED~,");

                strSQL.Append("NAME = ~HOSTNAME~,");
                strSQL.AppendFormat("PROVIDER= ~PROVIDER~,");
                strSQL.Append("DTTM_RCD_LAST_UPD = ~DTTM_RCD_LAST_UPD~, ");
                strSQL.Append("UPDATED_BY_USER = ~UPDATED_BY_USER~ ");

                dictParams = new Dictionary<string, object>();

                dictParams.Add("NAME", objADProvider.HostName);
                dictParams.Add("PROVIDER", 0);
                dictParams.Add("ENABLED", Convert.ToInt16(objADProvider.IsEnabled));
                dictParams.Add("DTTM_RCD_LAST_UPD", "");
                dictParams.Add("UPDATED_BY_USER", "");

                DbFactory.ExecuteNonQuery(sSecurityConnectionString, strSQL.ToString(), dictParams);
            }
            //MITS 24086
        }

        /// <summary>
        /// Inserts a new LDAP provider into the database
        /// </summary>
        /// <param name="objLDAPProvider"></param>
        public void InsertLDAPProvider(RiskmasterMembershipProvider objLDAPProvider)
        {
            StringBuilder strSQL = new StringBuilder();
            string sSecurityConnectionString = ConfigurationInfo.GetSecurityConnectionString(objLDAPProvider.ClientId);
            eDatabaseType eSecurityDbType = DbFactory.GetDatabaseType(sSecurityConnectionString);

            // If the LDAP source will be enabled, disable all other sources
            if (objLDAPProvider.IsEnabled)
            {
                strSQL.Append("UPDATE AUTH_PROVIDER ");
                if (eSecurityDbType == eDatabaseType.DBMS_IS_ORACLE)
                    strSQL.Append("SET ENABLED = 0");
                else
                    strSQL.Append("SET [ENABLED] = 0");

                DbFactory.ExecuteScalar(sSecurityConnectionString, strSQL.ToString());
                strSQL = new StringBuilder();
            } // if

            strSQL.Append("INSERT INTO AUTH_PROVIDER ");
            strSQL.Append("(AUTH_PROVIDER_ID, ");
            strSQL.Append("AUTH_PROVIDER_TYPE_ID, ");
            strSQL.Append("AUTH_PROVIDER_CONN_TYPE_ID, ");
            strSQL.Append("NAME, ");
            strSQL.Append("ENABLED, ");
            strSQL.Append("CONN_STRING, ");
            strSQL.Append("USER_ATTRIBUTE_MAPPING, ");
            strSQL.Append("ADMIN_UID, ");
            strSQL.Append("ADMIN_PWD) ");

            strSQL.Append("VALUES ");
            strSQL.Append("(");
            strSQL.Append("~AUTH_PROVIDER_ID~,");
            strSQL.Append("~AUTH_PROVIDER_TYPE_ID~,");
            strSQL.Append("~CONN_TYPE_ID~,"); // AUTH_PROVIDER_CONN_TYPE_ID
            strSQL.Append("~HOSTNAME~,"); // NAME
            strSQL.Append("~ENABLED~,"); // ENABLED
            strSQL.Append("~CONN_STRING~,"); // CONN_STRING
            strSQL.Append("~USER_ATTRIBUTE_MAPPING~,"); // USER_ATTRIBUTE_MAPPING
            strSQL.Append("~ADMIN_UID~,");
            strSQL.Append("~ADMIN_PWD~");
            strSQL.Append(")");

            Dictionary<string, object> dictParams = new Dictionary<string, object>();

            dictParams.Add("AUTH_PROVIDER_ID", GetNextProviderID(objLDAPProvider.ClientId));
            dictParams.Add("AUTH_PROVIDER_TYPE_ID", 2); //TODO: Hook in the Provider Type ID as part of the Data Source Provider
            dictParams.Add("CONN_TYPE_ID", objLDAPProvider.ConnectionType);
            dictParams.Add("HOSTNAME", objLDAPProvider.HostName);
            dictParams.Add("ENABLED", Convert.ToInt16(objLDAPProvider.IsEnabled));
            dictParams.Add("CONN_STRING", GenerateLDAPConnectionString(objLDAPProvider.HostName, objLDAPProvider.BaseDN, objLDAPProvider.ConnectionType));
            dictParams.Add("USER_ATTRIBUTE_MAPPING", objLDAPProvider.UserAttribute);
            dictParams.Add("ADMIN_UID", objLDAPProvider.AdminUserName);
            dictParams.Add("ADMIN_PWD", RMCryptography.EncryptString(objLDAPProvider.AdminPwd));//rsushilaggar MITS 23841

            DbFactory.ExecuteNonQuery(sSecurityConnectionString, strSQL.ToString(), dictParams);
        }
        #endregion

        #region Update Statements
        /// <summary>
        /// Updates the Active Directory provider information in the database
        /// </summary>
        /// <param name="objADProvider"></param>
        public void UpdateADProvider(RiskmasterMembershipProvider objADProvider)
        {

            StringBuilder strSQL = new StringBuilder();
            string sSecurityConnectionString = ConfigurationInfo.GetSecurityConnectionString(objADProvider.ClientId);
            eDatabaseType eSecurityDbType = DbFactory.GetDatabaseType(sSecurityConnectionString);

            // If the AD source will be enabled, disable all other sources
            if (objADProvider.IsEnabled)
            {
                strSQL.Append("UPDATE AUTH_PROVIDER ");
                if (eSecurityDbType == eDatabaseType.DBMS_IS_ORACLE)
                    strSQL.Append("SET ENABLED = 0");
                else
                    strSQL.Append("SET [ENABLED] = 0");

                DbFactory.ExecuteScalar(sSecurityConnectionString, strSQL.ToString());
                strSQL = new StringBuilder();
                //MITS 24086
                strSQL.Append("UPDATE SETTINGS ");
                strSQL.Append("SET DOMAIN_LOGIN_ENABLED = -1");

                DbFactory.ExecuteScalar(sSecurityConnectionString, strSQL.ToString());
                strSQL = new StringBuilder();
                //MITS 24086
            } // if
            else
            {
                //MITS 24086
                strSQL.Append("UPDATE SETTINGS ");
                strSQL.Append("SET DOMAIN_LOGIN_ENABLED = 0");

                DbFactory.ExecuteScalar(sSecurityConnectionString, strSQL.ToString());
                strSQL = new StringBuilder();
                //MITS 24086
            }

            strSQL.Append("UPDATE AUTH_PROVIDER ");

            if (eSecurityDbType == eDatabaseType.DBMS_IS_ORACLE)
                strSQL.AppendFormat("SET ENABLED =~ENABLED~,");
            else
                strSQL.AppendFormat("SET [ENABLED] =~ENABLED~,");

            strSQL.Append("NAME = ~HOSTNAME~,");
            strSQL.AppendFormat("CONN_STRING= ~CONN_STRING~,");
            strSQL.AppendFormat("AUTH_PROVIDER_CONN_TYPE_ID= ~CONN_TYPE_ID~,");
            strSQL.Append("ADMIN_UID = ~ADMIN_UID~,");
            strSQL.Append("ADMIN_PWD = ~ADMIN_PWD~ ");
            strSQL.Append("WHERE AUTH_PROVIDER_ID = ~AUTH_PROVIDER_ID~");

            Dictionary<string, object> dictParams = new Dictionary<string, object>();

            //MITS 21982 Be careful, Oracle binding according to parameter POSITION not by NAME -- xhu
            dictParams.Add("ENABLED", Convert.ToInt16(objADProvider.IsEnabled));
            dictParams.Add("HOSTNAME", objADProvider.HostName);
            dictParams.Add("CONN_STRING", GenerateADConnectionString(objADProvider.HostName, objADProvider.ConnectionType));
            dictParams.Add("CONN_TYPE_ID", objADProvider.ConnectionType);
            dictParams.Add("ADMIN_UID", objADProvider.AdminUserName);
            dictParams.Add("ADMIN_PWD", RMCryptography.EncryptString(objADProvider.AdminPwd));//rsushilaggar MITS 23841
            dictParams.Add("AUTH_PROVIDER_ID", objADProvider.ProviderID);

            DbFactory.ExecuteNonQuery(sSecurityConnectionString, strSQL.ToString(), dictParams);

            //MITS 24086
            strSQL = new StringBuilder();
            strSQL.Append("UPDATE DOMAIN ");

            if (eSecurityDbType == eDatabaseType.DBMS_IS_ORACLE)
                strSQL.AppendFormat("SET ENABLED =~ENABLED~,");
            else
                strSQL.AppendFormat("SET [ENABLED] =~ENABLED~,");

            strSQL.Append("NAME = ~HOSTNAME~,");
            strSQL.AppendFormat("PROVIDER= ~PROVIDER~,");
            strSQL.Append("DTTM_RCD_LAST_UPD = ~DTTM_RCD_LAST_UPD~, ");
            strSQL.Append("UPDATED_BY_USER = ~UPDATED_BY_USER~ ");

            dictParams = new Dictionary<string, object>();

            dictParams.Add("HOSTNAME", objADProvider.HostName);
            dictParams.Add("PROVIDER", 0);
            dictParams.Add("ENABLED", Convert.ToInt16(objADProvider.IsEnabled));
            dictParams.Add("DTTM_RCD_LAST_UPD", "");
            dictParams.Add("UPDATED_BY_USER", "");

            DbFactory.ExecuteNonQuery(sSecurityConnectionString, strSQL.ToString(), dictParams);
            //MITS 24086

        }


        /// <summary>
        /// Updates the LDAP provider information in the database
        /// </summary>
        /// <param name="objLDAPProvider"></param>
        public void UpdateLDAPProvider(RiskmasterMembershipProvider objLDAPProvider)
        {
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            string sSecurityConnectionString = ConfigurationInfo.GetSecurityConnectionString(objLDAPProvider.ClientId);
            eDatabaseType eSecurityDbType = DbFactory.GetDatabaseType(sSecurityConnectionString);

            // If the LDAP source will be enabled, disable all other sources
            if (objLDAPProvider.IsEnabled)
            {
                strSQL.Append("UPDATE AUTH_PROVIDER ");
                if (eSecurityDbType == eDatabaseType.DBMS_IS_ORACLE)
                    strSQL.Append("SET ENABLED = 0");
                else
                    strSQL.Append("SET [ENABLED] = 0");

                DbFactory.ExecuteScalar(sSecurityConnectionString, strSQL.ToString());
                strSQL = new StringBuilder();
            } // if

            // Create SQL update statement
            strSQL.Append("UPDATE AUTH_PROVIDER ");

            if (eSecurityDbType == eDatabaseType.DBMS_IS_ORACLE)
                strSQL.AppendFormat("SET ENABLED =~ENABLED~,");
            else
                strSQL.AppendFormat("SET [ENABLED] =~ENABLED~,");

            strSQL.AppendFormat("NAME = ~HOST_NAME~,");
            strSQL.AppendFormat("AUTH_PROVIDER_CONN_TYPE_ID =~CONN_TYPE_ID~,");
            strSQL.AppendFormat("ADMIN_UID = ~ADMIN_UID~,");
            strSQL.AppendFormat("CONN_STRING = ~CONN_STR~,");
            strSQL.AppendFormat("USER_ATTRIBUTE_MAPPING = ~USER_ATTRIBUTE_MAPPING~,");
            strSQL.AppendFormat("ADMIN_PWD = ~ADMIN_PWD~");
            strSQL.AppendFormat(" WHERE AUTH_PROVIDER_ID= ~AUTH_PROVIDER_ID~");

            // Add Parameters. 
            //MITS 21982 Be careful, Oracle binding according to parameter POSITION not by NAME -- xhu
            dictParams.Add("ENABLED", Convert.ToInt16(objLDAPProvider.IsEnabled));
            dictParams.Add("HOST_NAME", objLDAPProvider.HostName);
            dictParams.Add("CONN_TYPE_ID", objLDAPProvider.ConnectionType);
            dictParams.Add("ADMIN_UID", objLDAPProvider.AdminUserName);
            dictParams.Add("CONN_STR", GenerateLDAPConnectionString(objLDAPProvider.HostName, objLDAPProvider.BaseDN, objLDAPProvider.ConnectionType));
            dictParams.Add("USER_ATTRIBUTE_MAPPING", objLDAPProvider.UserAttribute);
            dictParams.Add("ADMIN_PWD", RMCryptography.EncryptString(objLDAPProvider.AdminPwd));//rsushilaggar MITS 23841
            dictParams.Add("AUTH_PROVIDER_ID", objLDAPProvider.ProviderID);

            DbFactory.ExecuteNonQuery(sSecurityConnectionString, strSQL.ToString(), dictParams);
        }
        #endregion

        #region Misc

        private static DataColumn CreateDataColumn(string strColumnName, string strTypeName)
        {
            DataColumn dtColumn = new DataColumn();
            dtColumn.DataType = Type.GetType(strTypeName);
            dtColumn.ColumnName = strColumnName;

            return dtColumn;
        }

        /// <summary>
        /// Creates the appropriate LDAP connection string
        /// based on user's entered information such as connection type etc.
        /// </summary>
        /// <param name="Hostname"></param>
        /// <param name="BaseDN"></param>
        /// <param name="ConnectionType"></param>
        /// <returns></returns>
        private static String GenerateLDAPConnectionString(string Hostname, string BaseDN, string ConnectionType)
        {
            StringBuilder sConnStr = new StringBuilder();

            if (ConnectionType.Equals(ConnectionTypes.SSL))
            {
                sConnStr.AppendFormat(string.Format(LDAP_SSL));
            }
            else
            {
                sConnStr.AppendFormat(string.Format(LDAP_BASIC));
            }

            sConnStr.AppendFormat(string.Format(Hostname));
            sConnStr.AppendFormat(string.Format("/"));
            sConnStr.AppendFormat(string.Format(BaseDN));

            return sConnStr.ToString();
        }//method: GenerateLDAPConnectionString()

        /// <summary>
        /// Creates the appopriate Active Directory connection string
        /// based on user's entered information such as connection type etc.
        /// </summary>
        /// <param name="Hostname"></param>
        /// <param name="ConnectionType"></param>
        /// <returns></returns>
        private static String GenerateADConnectionString(string Hostname, string ConnectionType)
        {
            StringBuilder sConnStr = new StringBuilder();

            if (ConnectionType.Equals(ConnectionTypes.SSL))
            {
                sConnStr.AppendFormat(string.Format(LDAP_SSL));
            }
            else
            {
                sConnStr.AppendFormat(string.Format(LDAP_BASIC));
            }

            sConnStr.AppendFormat(string.Format(Hostname));

            return sConnStr.ToString();
        } // method: GenerateADConnectionString

        /// <summary>
        /// Selects the max current authentication provider ID then returns the next increment.
        /// </summary>
        /// <returns></returns>
        private static String GetNextProviderID(int p_iClientId)
        {
            String sNextProviderId = string.Empty;
            StringBuilder sGetNextIDSQL = new StringBuilder();

            sGetNextIDSQL.Append("SELECT MAX(AUTH_PROVIDER_ID)");
            sGetNextIDSQL.Append("FROM AUTH_PROVIDER");

            using (DbReader dbrNextAuth = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(p_iClientId), sGetNextIDSQL.ToString()))
            {
                dbrNextAuth.Read();
                sNextProviderId = dbrNextAuth.GetValue(0).ToString();
            }

            if (sNextProviderId == "")
            {
                sNextProviderId = "0";
            }
            else
            {
                sNextProviderId = (Convert.ToInt32(sNextProviderId) + 1).ToString();
            } // else


            return sNextProviderId;
        } // method: GetNextProviderID

        /// <summary>
        /// Selects the max current DOMAINID then returns the next increment.
        /// </summary>
        /// <returns></returns>
        private static String GetNextDomainID(int p_iClientId)
        {
            String sNextDomainId = string.Empty;
            StringBuilder sGetNextIDSQL = new StringBuilder();

            sGetNextIDSQL.Append("SELECT MAX(DOMAINID)");
            sGetNextIDSQL.Append("FROM DOMAIN");

            using (DbReader dbrNextDom = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(p_iClientId), sGetNextIDSQL.ToString()))
            {
                dbrNextDom.Read();
                sNextDomainId = dbrNextDom.GetValue(0).ToString();
            }

            if (string.IsNullOrEmpty(sNextDomainId))
            {
                sNextDomainId = "0";
            }
            else
            {
                sNextDomainId = (Convert.ToInt32(sNextDomainId) + 1).ToString();
            } // else


            return sNextDomainId;
        }

        /// <summary>
        /// Get number of rows from DOMAIN.
        /// </summary>
        /// <returns></returns>
        private static bool GetCountDom(int p_iClientId)
        {
            String sCount = string.Empty;
            StringBuilder sGetSQL = new StringBuilder();

            sGetSQL.Append("SELECT COUNT(*)");
            sGetSQL.Append("FROM DOMAIN");

            using (DbReader dbrNextAuth = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(p_iClientId), sGetSQL.ToString()))
            {
                dbrNextAuth.Read();
                sCount = dbrNextAuth.GetValue(0).ToString();
            }
            if (Convert.ToInt32(sCount) > 0)
                return true;
            else
                return false;
        }
        #endregion
        #endregion
    }
}
