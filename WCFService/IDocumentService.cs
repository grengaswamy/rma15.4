﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;
namespace RiskmasterService
{
    // NOTE: If you change the interface name "IDocumentService" here, you must also update the reference to "IDocumentService" in Web.config.
    [ServiceContract]
    public interface IDocumentService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        bool CopyDocuments(TransferDocumentsRequest request);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void CreateDocument(DocumentType request, out RMServiceType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        bool CreateFolder(FolderTypeRequest request);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        bool Delete(DocumentsDeleteRequest request);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        bool EmailDocuments(EmailDocumentsRequest request);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetAttachments(DocumentListRequest request, out DocumentList objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetDocument(DocumentType request, out DocumentType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetDocumentsList(DocumentListRequest request, out DocumentList objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetFolders(Folder request, out System.Collections.Generic.List<Folder> objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetUsers(DocumentUserRequest request, out UsersList objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        bool MoveDocuments(MoveDocumentsRequest request);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void RetrieveDocument(DocumentType request, out DocumentType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        bool TransferDocuments(TransferDocumentsRequest request);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        bool UpdateDocument(DocumentType request);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void UpdateDocumentFile(DocumentType request, out DocumentType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void CreateDocumentDetail(DocumentType request, out DocumentType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetExecutiveSummaryFileDetails(DocumentType request, out DocumentType objReturn);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void CheckOutlookSetting(DocumentType request, out bool bIsUseOutlookEnabled);
        //rbhatia4:R8: Use Media View Setting : July 08 2011
        //Restructuring Document Management Code for choosing the selected vendor
        /*
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string GetPaperVisionSetting(DocumentType objDocReq);
         */
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetDocumentManagementVendorDetails(DocumentVendorDetails objDocReq, out DocumentVendorDetails objReturn);
        
    }

}
