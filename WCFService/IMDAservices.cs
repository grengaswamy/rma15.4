﻿using System.ServiceModel;
//mbahl3 
using Riskmaster.Models; //mbahl3 
using System.ServiceModel.Web; //mbahl3 


namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMDAservices" in both code and config file together.
    [ServiceContract]
    public interface IMDAservices
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Topic" ,BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MDATopic GetMDATopics(MDATopic oMDATopic);
    }
}
