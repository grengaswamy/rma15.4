﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMobileUploadService" in both code and config file together.
    [ServiceContract]
    public interface IMobileUploadService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void DoWork();
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string UploadRequest(string xmlRequest);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string ProcessRequest(string xmlRequest);
    }
}
