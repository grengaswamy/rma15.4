﻿//#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;
using System.Collections.Specialized;
using System.Web;
using Riskmaster.Common;
using System.Collections;

namespace RiskmasterService
{
    // NOTE: If you change the class name "RMService" here, you must also update the reference to "RMService" in Web.config and in the associated .svc file.
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class MobileDocumentService : RMService, IMobileDocumentService
    {
        // 05/14/2013 MITS 32589 kkaur8 Starts
        bool docUploaded = false;
        ArrayList sFilelist = new ArrayList();
               
        public string MobileDocumentUploadRequest(string request)
        {
            XmlDocument xmlDocRequest = null;
            xmlDocRequest = new XmlDocument();
            xmlDocRequest.LoadXml(request);
            XmlDocument xmlOutEnv = new XmlDocument();
            XmlElement xmlElement = null;
            XmlElement xmlMsgsRoot = null;
            XmlElement xmlFilelists = null;
                         
            //Amandeep Mobile Adjuster--start
            DocumentVendorDetails objDocReq = null;
            DocumentVendorDetails objResponse = null;

            // Mobile over Cloud 
            XmlElement xmlClient = (XmlElement)xmlDocRequest.SelectSingleNode("/Message/ClientId");
            string sClientId = "0";
            int iClientId = 0;
            if (xmlClient != null)
                sClientId = xmlClient.InnerText;
            iClientId = int.Parse(sClientId);

            objDocReq = new DocumentVendorDetails();
            objDocReq.AssociatedRecordInfo = new AssociatedRecordInfo();
            objDocReq.Token = xmlDocRequest.SelectSingleNode("//Authorization").InnerText;
            objDocReq.ClientId = iClientId;
            objDocReq.AssociatedRecordInfo.FormName ="";
            objDocReq.AssociatedRecordInfo.RecordId = "0";
            GetDocumentManagementVendorDetails(objDocReq, out objResponse);

            switch (objResponse.DocumentManagementVendorName)
            {
                case "RiskmasterX":
                    CreateDocTypeFromXml_MobAdj(request);
                    break;
                case "Acrosoft":
                    CreateStreamedDocTypeFromXml_MobAdj(request);
                    break;
                default:
                    CreateDocTypeFromXml_MobAdj(request);
                    break;
            }

            //Amandeep Mobile Adjuster--end
            XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage");
            xmlOutEnv.AppendChild(xmlRoot);
            XmlElement xmlErrRoot = xmlOutEnv.CreateElement("MsgStatus");
            xmlRoot.AppendChild(xmlErrRoot);
            xmlMsgsRoot = xmlOutEnv.CreateElement("MsgStatusCd");
            if (docUploaded)
               xmlMsgsRoot.InnerText = "Success";
            else
               xmlMsgsRoot.InnerText = "Error";
                           
            xmlErrRoot.AppendChild(xmlMsgsRoot);
            foreach (string sfilenam in sFilelist)
            {
                XmlElement xmlFilelist = xmlOutEnv.CreateElement("File");
                xmlFilelist.InnerText = sfilenam;
                xmlErrRoot.AppendChild(xmlFilelist);
             }
             return xmlOutEnv.OuterXml;
        }
        //Amandeep Molbile Adjuster
        public void GetDocumentManagementVendorDetails(DocumentVendorDetails request, out DocumentVendorDetails objReturn)
        {

            DocumentManagementAdaptor objDocument = null;
            UserLogin oUserLogin = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            string functionName = "GetDocumentManagementVendorName";
            XmlDocument xmlRequest = null;
            bool bUsePaperVision = false; //mbahl3 mits 29236
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);

                objDocument.GetDocumentManagementVendorDetails(request, out objReturn, ref errOut, ref bUsePaperVision); //mbahl3 mits 29236
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
           
            if (bUsePaperVision || (errOut.Count > 0) || (systemErrors.Count > 0)) //mbahl3 mits 29236
            {
                logErrors(functionName, xmlRequest, bResult, errOut);
            }
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

        }


        public void CreateStreamedDocTypeFromXml_MobAdj(string request)
        {           
            StreamedDocumentType document = null;
            XmlNodeList documentlist;
            XmlDocument objrequest = new XmlDocument();         
            RMServiceType returnVal = null;

            objrequest.LoadXml(request);

            documentlist = objrequest.SelectNodes("//DocumentUpload");

            document = new StreamedDocumentType();
            document.Class = new StreamedCodeType();
            document.Category = new StreamedCodeType();
            document.Type = new StreamedCodeType();
            document.Title = ""; 
            document.FolderId = "";
            document.PsId = 0;
            document.FilePath = "";
            document.Subject = "";            
            document.Token = objrequest.SelectSingleNode("//Authorization").InnerText;             
            document.Type.Id = 0;
            document.Type.Desc = "";
            document.Class.Id = 0;
            document.Class.Desc = "";
            document.Category.Id = 0;
            document.Category.Desc = "";            
            document.ScreenFlag = "";
            document.Keywords = "";
            document.Notes = "";
           // document.AttachTable = "CLAIM"; 
            document.FormName = "";
            document.AttachRecordId = 0;
            //rsharma220 MITS 35387
            //  document.Claimnumber = objrequest.SelectSingleNode("//CLAIM_NUMBER").InnerText;
            if (objrequest.SelectSingleNode("//CLAIM_NUMBER") != null)
            {
                document.Claimnumber = objrequest.SelectSingleNode("//CLAIM_NUMBER").InnerText;
                document.AttachTable = "CLAIM";
            }
            else if (objrequest.SelectSingleNode("//EVENT_NUMBER") != null)  //rsharma220: Added for mobile changes
            {
                document.Eventnumber = objrequest.SelectSingleNode("//EVENT_NUMBER").InnerText;
                document.AttachTable = "EVENT";
            }
            if (objrequest.SelectSingleNode("//MobAdjuster") != null && objrequest.SelectSingleNode("//MobAdjuster").InnerText == "true")
            {
                document.bMobileAdj = true;
            }

            foreach (XmlNode objitem in documentlist)
            {
                document.FileName = objitem.Attributes["filename"].Value;                
                document.Title = objitem.Attributes["title"].Value;
                document.Subject = objitem.Attributes["subject"].Value;
                if (objitem.Attributes["type_id"] != null)
                    document.Type.Id = Conversion.ConvertStrToInteger(objitem.Attributes["type_id"].Value);
                if (objitem.Attributes["type"] != null)  
                    document.Type.Desc = objitem.Attributes["type"].Value;
                if (objitem.Attributes["notes"] != null)     
                    document.Notes = objitem.Attributes["notes"].Value;               
                if (objitem.Attributes["class_id"] != null)
                    document.Class.Id = Conversion.ConvertStrToInteger(objitem.Attributes["class_id"].Value);
                if (objitem.Attributes["category_id"] != null)
                    document.Category.Id = Conversion.ConvertStrToInteger(objitem.Attributes["category_id"].Value);
                if (objitem.Attributes["keywords"] != null)
                    document.Keywords = objitem.Attributes["keywords"].Value;
               
                Byte[] bytes = System.Convert.FromBase64String(objitem.Attributes["content"].Value.ToString());
                document.FileContents = bytes;

                CreateDocument(document);                
                if (document.Errors == null)
                {
                    docUploaded = true;
                    sFilelist.Add(document.FileName);
                }
                else
                {
                    docUploaded = false;
                    break;
                }                
            }
        }
        // 05/14/2013 MITS 32589 kkaur8 Ends

        //function to create document type from the Input XML
        //additions for Mobile App - yatharth START
      
        public void CreateDocTypeFromXml_MobAdj(string request)
        {
            //Here we shall read the values of documents node by node and then upload them
            DocumentType document = null;
            XmlNodeList documentlist;
            XmlDocument objrequest = new XmlDocument();
            //byte[] file = null;
            RMServiceType returnVal = null;

            objrequest.LoadXml(request);

            documentlist = objrequest.SelectNodes("//DocumentUpload");

            document = new DocumentType();
            document.Class = new CodeType();
            document.Category = new CodeType();
            document.Type = new CodeType();
            document.Title = ""; //AppHelper.GetFormValue("Title");
            document.FolderId = "";//AppHelper.GetFormValue("FolderId").ToString();
            document.PsId = 0;//Conversion.ConvertStrToInteger(AppHelper.GetFormValue("Psid").ToString());
            document.FilePath = "";
            document.Subject = "";// AppHelper.GetFormValue("Subject");
            //Start HardCoded need to change values
            document.Token = objrequest.SelectSingleNode("//Authorization").InnerText; //AppHelper.GetSessionId();
            //End HardCoded need to change values
            document.Type.Id = 0;//Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documenttypecode_cid"));
            document.Type.Desc = "";// AppHelper.GetFormValue("documenttypecode");
            document.Class.Id = 0;//Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentclasscode_cid"));
            document.Class.Desc = "";// AppHelper.GetFormValue("documentclasscode");
            document.Category.Id = 0;// Conversion.ConvertStrToInteger(AppHelper.GetFormValue("documentcategorycode_cid"));
            document.Category.Desc = "";// AppHelper.GetFormValue("documentcategorycode");
            //HardCoded need to change values
            document.ScreenFlag = "";// AppHelper.GetFormValue("flag").ToString();
            document.Keywords = "";// AppHelper.GetFormValue("Keywords");
            document.Notes = "";// AppHelper.GetFormValue("Notes");
           // document.AttachTable = "CLAIM"; //AppHelper.GetFormValue("AttachTableName").ToString();
            document.FormName = "";// AppHelper.GetFormValue("FormName").ToString();
            document.AttachRecordId = 0;// This should be claimid

            document.ClientId = Int32.Parse(objrequest.SelectSingleNode("//ClientId").InnerText.ToString()); //nsharma202 : for mobile uploads
            if (objrequest.SelectSingleNode("//CLAIM_NUMBER") != null)
            {
                document.Claimnumber = objrequest.SelectSingleNode("//CLAIM_NUMBER").InnerText;
                document.AttachTable = "CLAIM";
                document.ClaimId = -1;
                document.EventId = 0;
            }
            else if (objrequest.SelectSingleNode("//EVENT_NUMBER") != null)  //rsharma220: Added for mobile changes
            {
                document.Eventnumber = objrequest.SelectSingleNode("//EVENT_NUMBER").InnerText;
                document.AttachTable = "EVENT";
                document.ClaimId = 0;
                document.EventId = -1;
            }
            
            if (objrequest.SelectSingleNode("//MobAdjuster") != null && objrequest.SelectSingleNode("//MobAdjuster").InnerText == "true")
            {
                document.bMobileAdj = true;
                document.bMobilityAdj = true;
            }

            foreach (XmlNode objitem in documentlist)
            {
                
                document.FileName = objitem.Attributes["filename"].Value;
                //Ijha: Adding nodes to Document upload
                document.Title = objitem.Attributes["title"].Value;
                document.Subject = objitem.Attributes["subject"].Value;
                if (objitem.Attributes["type_id"] != null)  // 05/14/2013 MITS 32589 kkaur8
                    document.Type.Id = Conversion.ConvertStrToInteger(objitem.Attributes["type_id"].Value);
                if (objitem.Attributes["type"] != null)   // 05/14/2013 MITS 32589 kkaur8
                    document.Type.Desc = objitem.Attributes["type"].Value;
                if (objitem.Attributes["notes"] != null)     // 05/14/2013 MITS 32589 kkaur8
                    document.Notes = objitem.Attributes["notes"].Value;   
                //Ijha end
                // 05/14/2013 MITS 32589 kkaur8 Starts
                if (objitem.Attributes["class_id"]!=null)
                    document.Class.Id = Conversion.ConvertStrToInteger(objitem.Attributes["class_id"].Value);
                if (objitem.Attributes["category_id"]!= null)
                    document.Category.Id = Conversion.ConvertStrToInteger(objitem.Attributes["category_id"].Value);
                if (objitem.Attributes["keywords"]!= null)
                    document.Keywords = objitem.Attributes["keywords"].Value;
                // 05/14/2013 MITS 32589 kkaur8 Ends

                //System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                //Byte[] bytes = encoding.GetBytes(objitem.Attributes["content"].Value);
                // Ijha : conversion of base64 into bytes
                Byte[] bytes = System.Convert.FromBase64String(objitem.Attributes["content"].Value.ToString());
                document.FileContents = bytes;
                
                CreateDocument(document, out returnVal);
                // 05/14/2013 MITS 32589 kkaur8 Starts 
                if (returnVal.Errors == null)
                {
                   docUploaded = true;
                   sFilelist.Add(document.FileName);
                }
                else
                {
                   docUploaded = false;
                   break;
                }
                // 05/14/2013 MITS 32589 kkaur8 Ends
              }
         }

        //Amandeep Mobile Adjuster
        public void CreateDocument(StreamedDocumentType request)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                //objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.AddDocument(request, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
        public void CreateDocument(DocumentType request, out RMServiceType objReturn)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DocumentManagementAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(request.ClientId);
                objReturn = new RMServiceType();
                objDocument = new DocumentManagementAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(request, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, request.ClientId);
                bResult = objDocument.AddDocument(request, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }
        //additions for Mobile App - yatharth END        
    }
}
