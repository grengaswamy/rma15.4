﻿using System;
using System.ServiceModel;
using System.Xml;
using System.Text.RegularExpressions;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using System.ServiceModel.Activation;
using System.Xml.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Riskmaster.Common;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RiskmasterService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ProgressNotes : RMService, IProgressNotes
    {
        public ProgressNotesType LoadProgressNotesPartial(ProgressNotesType oProgressNotesType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "LoadProgressNotesPartialWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNotesType objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNotesType.ClientId);
                objReturn = new ProgressNotesType();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNotesType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNotesType.ClientId);
                bResult = objDocument.OnLoadIntegratePartial(oProgressNotesType, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }

        public ProgressNotesTypeChange GetNotesCaption(ProgressNotesTypeChange oProgressNotesTypeChange)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetNotesCaption";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNotesTypeChange objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNotesTypeChange.ClientId);
                objReturn = new ProgressNotesTypeChange();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNotesTypeChange, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNotesTypeChange.ClientId);
                bResult = objDocument.GetNotesCaption(oProgressNotesTypeChange, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }

        public ProgressNotesType LoadProgressNotes(ProgressNotesType oProgressNotesType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "LoadProgressNotesWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNotesType objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNotesType.ClientId);
                objReturn = new ProgressNotesType();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNotesType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNotesType.ClientId);
                bResult = objDocument.OnLoadIntegrate(oProgressNotesType, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public ProgressNotesType DeleteNote(ProgressNotesType oProgressNotesType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "DeleteNoteWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNotesType objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNotesType.ClientId);
                objReturn = new ProgressNotesType();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNotesType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNotesType.ClientId);
                bResult = objDocument.DeleteProgressNoteIntegrate(oProgressNotesType, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;
        }
        public ProgressNotesType PrintProgressNotes(ProgressNotesType oProgressNotesType)
        {
            XmlDocument xmlRequest = null;
            string functionName = "PrintProgressNotesWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNotesType objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNotesType.ClientId);
                objReturn = new ProgressNotesType();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNotesType, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNotesType.ClientId);
                bResult = objDocument.PrintProgressNotesIntegrate(oProgressNotesType, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public GetNoteDetailsObject GetNoteDetails(GetNoteDetailsObject oGetNoteDetailsObject)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetNoteDetailsWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            GetNoteDetailsObject objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oGetNoteDetailsObject.ClientId);
                objReturn = new GetNoteDetailsObject();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oGetNoteDetailsObject, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oGetNoteDetailsObject.ClientId);
                bResult = objDocument.GetNoteDetailsIntegrate(oGetNoteDetailsObject, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public SelectClaimObject SelectClaim(SelectClaimObject oSelectClaimObject)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SelectClaimWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            SelectClaimObject objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oSelectClaimObject.ClientId);
                objReturn = new SelectClaimObject();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oSelectClaimObject, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oSelectClaimObject.ClientId);
                bResult = objDocument.SelectClaimIntegrate(oSelectClaimObject, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        /*gbindra MITS#34104 WWIG GAP15 02042014 START*/
        public SelectClaimantObject SelectClaimant(SelectClaimantObject oSelectClaimantObject)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SelectClaimantWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            SelectClaimantObject objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oSelectClaimantObject.ClientId);
                objReturn = new SelectClaimantObject();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oSelectClaimantObject, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oSelectClaimantObject.ClientId);
                bResult = objDocument.SelectClaimantIntegrate(oSelectClaimantObject, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        /*gbindra MITS#34104 WWIG GAP15 02042014 END*/
        public ProgressNote SaveNotes(ProgressNote oProgressNote)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SaveNotesWCF";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNote objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNote.ClientId);
                objReturn = new ProgressNote();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNote, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNote.ClientId);
                bResult = objDocument.SaveNotesIntegrate(oProgressNote, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public ProgressNoteTemplates LoadTemplates(ProgressNoteTemplates oProgressNoteTemplates)
        {
            XmlDocument xmlRequest = null;
            string functionName = "LoadTemplates";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNoteTemplates objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNoteTemplates.ClientId);
                objReturn = new ProgressNoteTemplates();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNoteTemplates, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNoteTemplates.ClientId);
                bResult = objDocument.LoadTemplates(oProgressNoteTemplates, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public ProgressNoteTemplates SaveTemplates(ProgressNoteTemplates oProgressNoteTemplates)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SaveTemplates";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNoteTemplates objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNoteTemplates.ClientId);
                objReturn = new ProgressNoteTemplates();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNoteTemplates, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNoteTemplates.ClientId);
                bResult = objDocument.SaveTemplates(oProgressNoteTemplates, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public ProgressNoteTemplates DeleteTemplate(ProgressNoteTemplates oProgressNoteTemplates)
        {
            XmlDocument xmlRequest = null;
            string functionName = "DeleteTemplate";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNoteTemplates objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNoteTemplates.ClientId);
                objReturn = new ProgressNoteTemplates();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNoteTemplates, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNoteTemplates.ClientId);
                bResult = objDocument.DeleteTemplate(oProgressNoteTemplates, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public ProgressNoteTemplates GetTemplateDetails(ProgressNoteTemplates oProgressNoteTemplates)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetTemplateDetails";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNoteTemplates objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNoteTemplates.ClientId);
                objReturn = new ProgressNoteTemplates();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNoteTemplates, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNoteTemplates.ClientId);
                bResult = objDocument.GetTemplateDetails(oProgressNoteTemplates, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public ProgressNoteSettings GetNotesHeaderOrder(ProgressNoteSettings oProgressNoteSettings)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetNotesHeaderOrder";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNoteSettings objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNoteSettings.ClientId);
                objReturn = new ProgressNoteSettings();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNoteSettings, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNoteSettings.ClientId);
                bResult = objDocument.GetNotesHeaderOrder(oProgressNoteSettings, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
        public ProgressNoteSettings SaveNotesSettings(ProgressNoteSettings oProgressNoteSettings)
        {
            XmlDocument xmlRequest = null;
            string functionName = "SaveNotesSettings";
            UserLogin oUserLogin = null;
            ProgressNotesAdaptor objDocument = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            ProgressNoteSettings objReturn = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oProgressNoteSettings.ClientId);
                objReturn = new ProgressNoteSettings();
                objDocument = new ProgressNotesAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oProgressNoteSettings, out xmlRequest, functionName, objDocument, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oProgressNoteSettings.ClientId);
                bResult = objDocument.SaveNotesSettings(oProgressNoteSettings, out  objReturn, ref errOut);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objReturn;

        }
    }
}
