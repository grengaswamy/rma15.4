﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;
using System.ServiceModel.Web;


namespace RiskmasterService
{
    [ServiceContract]
    public interface IPVUpgrade
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/upgrade", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PVUpgradeData UpgradeXmlTagToAspxForm(PVUpgradeData oPVUpgradeData);
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/fetch", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PVUpgradeData FetchJurisdictionalData(PVUpgradeData oPVUpgradeData);
    }
}
