﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using Riskmaster.Db;
using System.Configuration;
using Riskmaster.Db;
using System.Data;
using Riskmaster.Cache;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PortalService" in code, svc and config file together.
    [AspNetCompatibilityRequirements(
     RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
            [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class PortalService : IPortalService
    {
        // Rsolanki2: mits 23786: RMX portal setting should be saved to database instead of web server

        //string strConn = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
        string strConn = ConfigurationInfo.GetSessionConnectionString(0);   //ConfigurationManager.ConnectionStrings["SessionDataSource"].ConnectionString;
        //string strConn = @"Driver={SQL Server};Server=NDACOL\RISKMASTER;Database=R6A_VIEW_PSARIN;UID=sa;PWD=rmserver_123";

        public string ContentInfo(string strSql)
        {
            string Content = string.Empty;
            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(strConn, strSql))
                {
                    objReader.Read();
                    Content = objReader[0].ToString();
                }
                return Content;
            }
            catch (Exception)
            {
                return string.Empty;
            }            
        }

        private string getNextCustomizeId()
        {
            return DbFactory.ExecuteScalar(strConn, "SELECT MAX(ID) + 1 FROM CUSTOMIZE").ToString();            
        }

        public void ContentUpdate(string strSql, string strRow)
        {
            //strSql = "UPDATE SETTINGS SET CONTENT = '" + strSql + "' WHERE NAME='" + strRow + "'";

            //Ankit Start : Worked on MITS - 33537
            //strSql = "UPDATE CUSTOMIZE SET CONTENT = '" + strSql + "' WHERE UPPER(FILENAME)=UPPER('" + strRow + "')";
            //DbFactory.ExecuteNonQuery(strConn, strSql);
            
            using (DbConnection objConn = DbFactory.GetDbConnection(strConn))
            {
                objConn.Open();
                
                using (DbCommand objCmd = objConn.CreateCommand())
                {
                    DbParameter objParam = null;
                    string sSql = "UPDATE CUSTOMIZE SET CONTENT = ~XML~ WHERE UPPER(FILENAME)=UPPER('" + strRow + "')";

                    objParam = objCmd.CreateParameter();
                    objParam.Direction = ParameterDirection.Input;
                    objParam.Value = strSql;
                    objParam.ParameterName = "XML";
                    objParam.SourceColumn = "CONTENT";
                    objCmd.CommandText = sSql;
                    objCmd.Parameters.Add(objParam);
                    objCmd.ExecuteNonQuery();
                }
            }
            //Ankit End
        }

        public void ContentInsert(string strContent, string strFileName)
        {
            //strSql = "UPDATE SETTINGS SET CONTENT = '" + strSql + "' WHERE NAME='" + strRow + "'";
            //strSql = "UPDATE CUSTOMIZE SET CONTENT = '" + strSql + "' WHERE UPPER(FILENAME)=UPPER('" + strRow + "')";                       

            DbConnection objConn = DbFactory.GetDbConnection(strConn); ; 
            //DataSet objDataSet = null;
            //DbReader objRdr1 = null;
            DbCommand objCmd = null;
            DbParameter objParam = null;

            objConn.Open();
            //sbSql = new StringBuilder();

            string sNextCustomizeId = getNextCustomizeId();

            string sSql = "INSERT INTO CUSTOMIZE( ID ,FILENAME ,CONTENT, TYPE, IS_BINARY ) VALUES ("
                + sNextCustomizeId 
                + ", '" + strFileName   +"'" //;'customize_Homepage' ,"
                + ",~XML~"
                + ",0,0)";
                //+ ",~XML~)";

            objCmd = objConn.CreateCommand();
            objParam = objCmd.CreateParameter();
            objParam.Direction = ParameterDirection.Input;
            objParam.Value = strContent;
            objParam.ParameterName = "XML";
            objParam.SourceColumn = "CONTENT";

            objCmd.CommandText = sSql;            
            objCmd.Parameters.Add(objParam);
            objCmd.ExecuteNonQuery();
            //DbFactory.ExecuteNonQuery(strConn, strSql);

            //need to update next val in DB
            UpdateId(sNextCustomizeId); 

        }

        private void UpdateId(string sId)
        {
            int iNextCustomizeId= Convert.ToInt32(sId) + 1;
            string sSql = "UPDATE SESSION_IDS SET NEXT_UNIQUE_ID = " 
                + iNextCustomizeId.ToString() 
                + " WHERE UPPER(SYSTEM_TABLE_NAME) =UPPER('CUSTOMIZE')";
            DbFactory.ExecuteNonQuery(strConn, sSql);
        }



    }
}
