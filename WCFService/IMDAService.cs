﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.Models;

namespace RiskmasterService
{
    [ServiceContract]
    public interface IMDAService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        string GetMDATopics(string medicalcode, string jobclass);
    }
}

