﻿#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PageCacheService" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class PageCache : RMService, IPageCache
    {
        public string GetConnectionstringFromDSNId(PageData oPageData)
        {
            string sConnectionString = string.Empty;
            RiskmasterDatabase oRMDatabase = new RiskmasterDatabase( oPageData.DsnId ,oPageData.ClientId);
            sConnectionString = oRMDatabase.ConnectionString;

            return sConnectionString;
        }

        public string GetTableTimestamps(PageData oPageData)
        {
            string sTableTimestampList = string.Empty;
            XDocument oTableListConfig = XDocument.Parse(oPageData.TableListConfig);
            foreach (XElement oPage in oTableListConfig.XPathSelectElements("//pages/page"))
            {
                string sTable = oPage.Attribute("table").Value;
                string sColumn = oPage.Attribute("timecolumn").Value;
                string sSQL = string.Format("SELECT MAX({0}) FROM {1}", sColumn, sTable);
                string sTime = DbFactory.ExecuteScalar(oPageData.ConnKey, sSQL).ToString();
                oPage.Attribute("timestamp").Value = sTime;
            }

            return oTableListConfig.ToString();
        }

        public string GetTimestampForOrgHierarchy(PageData oPageData)
        {
            string sTime = DbFactory.ExecuteScalar(oPageData.ConnKey, "SELECT MAX(DTTM_RCD_LAST_UPD) FROM ENTITY WHERE ENTITY_TABLE_ID >=1005 AND ENTITY_TABLE_ID <= 1012").ToString();

            return sTime;
        }

        public Dictionary<string, string> GetUserLoginInfo(PageData oPageData)
        {
            Dictionary<string, string> dictUserProp = new Dictionary<string, string>();
            UserLogin objuser = new UserLogin(oPageData.User, oPageData.DsnName, oPageData.ClientId);
            dictUserProp.Add("iBesGroupId",objuser.BESGroupId.ToString());
            dictUserProp.Add("connectionString", objuser.objRiskmasterDatabase.ConnectionString);
            dictUserProp.Add("iDsnId", objuser.DatabaseId.ToString());
            dictUserProp.Add("iOrgSecFlag", objuser.objRiskmasterDatabase.OrgSecFlag.ToString());

            return dictUserProp;
        }
    }
}
