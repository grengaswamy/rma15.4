﻿using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;

namespace RiskmasterService
{
    public class Global1 : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterServiceRoutes();
        }
        private void RegisterServiceRoutes()
        {
            RouteTable.Routes.Add(new ServiceRoute("Authenticate", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.Authenticate)));
            RouteTable.Routes.Add(new ServiceRoute("Portal", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.Portal)));
            RouteTable.Routes.Add(new ServiceRoute("Resource", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.RMXResource)));
            RouteTable.Routes.Add(new ServiceRoute("MDI", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.MDINavigation)));
            RouteTable.Routes.Add(new ServiceRoute("PVUpgrade", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.PVUpgrade)));
            RouteTable.Routes.Add(new ServiceRoute("Codes", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.Codes)));
            RouteTable.Routes.Add(new ServiceRoute("RSW", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.RSWService)));
            RouteTable.Routes.Add(new ServiceRoute("Diary", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.Diary)));
            RouteTable.Routes.Add(new ServiceRoute("SSO", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.SSOAuthService)));
            RouteTable.Routes.Add(new ServiceRoute("Page", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.PageCache)));
            RouteTable.Routes.Add(new ServiceRoute("VPP", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.VPP)));
            RouteTable.Routes.Add(new ServiceRoute("MDA", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.MDAservices))); //mbahl3 jira [RMACLOUD-159]
            RouteTable.Routes.Add(new ServiceRoute("DataStream", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.DataStreaming)));
            RouteTable.Routes.Add(new ServiceRoute("Document", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.Document)));
            RouteTable.Routes.Add(new ServiceRoute("XMLImports", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.XMLImportServices))); //mbahl3 jira [RMACLOUD-158]
            RouteTable.Routes.Add(new ServiceRoute("ProgressNotes", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.ProgressNotes)));
            RouteTable.Routes.Add(new ServiceRoute("DAIntegration", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.Dataintegrator)));
            RouteTable.Routes.Add(new ServiceRoute("Mobile", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.Mobile)));
            RouteTable.Routes.Add(new ServiceRoute("LogMessage", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.Logs)));
            RouteTable.Routes.Add(new ServiceRoute("CustomLogin", new System.ServiceModel.Activation.WebServiceHostFactory(), typeof(RiskmasterService.CustomLogin))); //ygoyal3 jira [RMA 5504]
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpResponse response = HttpContext.Current.Response;
            response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
            response.AddHeader("Access-Control-Allow-Origin", "*");
            response.AddHeader("Access-Control-Allow-Headers", "session,ClientId,Content-Type,user,pid");

            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                response.End();
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}