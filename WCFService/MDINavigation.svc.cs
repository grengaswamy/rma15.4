﻿//#define DEBUG
#undef DEBUG
using System;
using System.ServiceModel;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.BusinessAdaptor.MDINavigationTreeNodeProfile;
using Riskmaster.Security;
using Riskmaster.Models;
using System.ServiceModel.Activation;

namespace RiskmasterService
{


    // NOTE: If you change the class name "MDINavigationTreeNodeProfileService" here, you must also update the reference to "MDINavigationTreeNodeProfileService" in Web.config.
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    /// <summary>
    /// Manages MDI Navigation
    /// </summary>
    public class MDINavigation  : RMService, IMDINavigation
    {
        /// <summary>
        /// Describe your member here.
        /// </summary>
        public MDINavigationTreeNodeProfileResponse GetMDINavigationTreeNodeProfileData(MDINavigationTreeNodeProfileRequest oMDINavigationTreeNodeProfileRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetMDINavigationTreeNodeProfileData";
            MDINavigationTreeNodeProfileAdaptor objMDINavigationTreeNodeProfile = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oMDINavigationTreeNodeProfileRequest.ClientId);
                objMDINavigationTreeNodeProfile = new MDINavigationTreeNodeProfileAdaptor();

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oMDINavigationTreeNodeProfileRequest, out xmlRequest, functionName, objMDINavigationTreeNodeProfile, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                return objMDINavigationTreeNodeProfile.QueryProfile(oMDINavigationTreeNodeProfileRequest);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public MDINavigationTreeNodeProfileResponse GetMDINavigationTreeNodeProfileDataByParent(MDINavigationTreeNodeProfileRequestByParent oMDINavigationTreeNodeProfileRequestByParent)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetMDINavigationTreeNodeProfileDataByParent";
            MDINavigationTreeNodeProfileAdaptor objMDINavigationTreeNodeProfile = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oMDINavigationTreeNodeProfileRequestByParent.ClientId);
                objMDINavigationTreeNodeProfile = new MDINavigationTreeNodeProfileAdaptor();

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oMDINavigationTreeNodeProfileRequestByParent, out xmlRequest, functionName, objMDINavigationTreeNodeProfile, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                return objMDINavigationTreeNodeProfile.QueryProfile(oMDINavigationTreeNodeProfileRequestByParent);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public InitMDIResponse InitMDI(InitMDIRequest oInitMDIRequest)
        {
            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "InitMDI";
            MDINavigationTreeNodeProfileAdaptor objMDINavigationTreeNodeProfile = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oInitMDIRequest.ClientId);
                objMDINavigationTreeNodeProfile = new MDINavigationTreeNodeProfileAdaptor();

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oInitMDIRequest, out xmlRequest, functionName, objMDINavigationTreeNodeProfile, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                return objMDINavigationTreeNodeProfile.InitMDI(oInitMDIRequest);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }
        //nkaranam2 - 34408
        public GetSysSettingsResponse GetSysSettings(GetSysSettingsRequest oGetSysSettingsRequest)
        {

            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetSysSettings";
            MDINavigationTreeNodeProfileAdaptor objMDINavigationTreeNodeProfile = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oGetSysSettingsRequest.ClientId);
                objMDINavigationTreeNodeProfile = new MDINavigationTreeNodeProfileAdaptor();

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oGetSysSettingsRequest, out xmlRequest, functionName, objMDINavigationTreeNodeProfile, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                return objMDINavigationTreeNodeProfile.GetSysSettings(oGetSysSettingsRequest);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public MDIClaimantFinancialRes GetClaimantDetails(MDIClaimantFinancialReq oMDIClaimantFinancialReq)
        {

            UserLogin oUserLogin = null;
            XmlDocument xmlRequest = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetClaimantDetails";
            MDINavigationTreeNodeProfileAdaptor objMDINavigationTreeNodeProfile = null;

            // Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oMDIClaimantFinancialReq.ClientId);
                objMDINavigationTreeNodeProfile = new MDINavigationTreeNodeProfileAdaptor();

                // This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oMDIClaimantFinancialReq, out xmlRequest, functionName, objMDINavigationTreeNodeProfile, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                return objMDINavigationTreeNodeProfile.GetClaimantDetails(oMDIClaimantFinancialReq);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

    }
}
