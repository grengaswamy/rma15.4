﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml.Linq;
using Riskmaster.Models;

namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPolicyInterfaceService" in both code and config file together.
    [ServiceContract]
    public interface IPolicyInterfaceService
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicySystemList(PolicySystemList oSearchRequest, out PolicySystemList oSearchResponse);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetXml(RMServiceType oRMServiceType, out DisplayFileData oDisplayFileData);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetDuplicateOptions(SaveDownloadOptions oSaveDownloadOptions, out DisplayFileData oDisplayFileData);
		//RMA-12047
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void CheckDuplicatePolicy(CheckDuplicateReq oCheckDupReq, out CheckDuplicateRes oCheckDupRes);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        bool SaveOptions(ref SaveDownloadOptions oSaveDownloadOptions);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void DeleteOldFiles(DisplayMode oMode);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicySearchResult(PolicySearch oSearchRequest, out PolicySearch oSearchResponse);
        
        //// Pradyumna - MITS 36013 - Start
        //[OperationContract]
        //[FaultContract(typeof(RMException))]
        //void GetStgPolicySearchResult(PolicySearch oSearchRequest, out PolicySearch oSearchResponse);

        //[OperationContract]
        //[FaultContract(typeof(RMException))]
        //void SavePolicyFrmStaging(PolicySaveRequest oSaveRequest, out PolicySaveRequest oSaveResponse);

        //[OperationContract]
        //[FaultContract(typeof(RMException))]
        //bool SaveOptionsFrmStaging(ref SaveDownloadOptions oSaveDownloadOptions);
				
        //[OperationContract]
        //[FaultContract(typeof(RMException))]
        //void PolicyValidationStgng(PolicyValidateInput oPolicyRequest, out PolicyValidateResponse oPolicyResponse);
        //// Pradyumna - MITS 36013 - ends
		
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicyEnquiryResult(PolicyEnquiry oSearchRequest, out PolicyEnquiry oSearchResponse);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetUnitDetailResult(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetCoverageDetailResult(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicyInterestDetailResult(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetEndorsementData(PolicyEnquiry oRequest, out PolicyEnquiry oResponse);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SaveExternalPolicy(PolicySaveRequest oSaveRequest, out PolicySaveRequest oSaveResponse);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void PolicyValidation(PolicyValidateInput oPolicyRequest, out PolicyValidateResponse oPolicyResponse);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicyDriverDetailResult(PolicyEnquiry oRequest, out PolicyEnquiry oResponse);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void SaveEndorsementData(Object oRequest, string sMode, int iPolicyId, int iRecordRowId);
        
        [OperationContract]
		[FaultContract(typeof(RMException))]
        void GetDownLoadXMLData(PolicyDownload oRequest, out PolicyDownload oResponse);
        
        [OperationContract]
		[FaultContract(typeof(RMException))]
        void GetUnitCoverageListResult(PolicyEnquiry oRequest, out PolicyEnquiry oResponse);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetEndorsementDataForTracking(PolicyDownload oRequest, out PolicyDownload oResponse);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetUnitInterestListResult(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        void GetPolicyUnitInterestList(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        bool SavePolicyDataForUtility(ref SaveDownloadOptions oSaveDownloadOptions);
         [OperationContract]
         [FaultContract(typeof(RMException))]
         void GetUnitXml(RMServiceType oRMServiceType, out DisplayFileData oDisplayFileData);

         [OperationContract]
         [FaultContract(typeof(RMException))]
         void SearchPolicyUnits(UnitSearch oUnitSearch, out UnitListing oUnitListing);

         [OperationContract]
         [FaultContract(typeof(RMException))]
         void GetPolicyInterestList(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse);

         [OperationContract]
         [FaultContract(typeof(RMException))]
         void GetPolicyDriverList(PolicyEnquiry oEnquiryRequest, out PolicyEnquiry oEnquiryResponse);

         [OperationContract]
         [FaultContract(typeof(RMException))]
         void GetPolicyDetails(FetchPolicyDetails oFetchPolReq, out FetchPolicyDetails oFetchPolRes);
         [OperationContract]
         [FaultContract(typeof(RMException))]
         void ImportMappingFromFile(LossCodeMappingFileContent oRequest, out LossCodeMappingOutput oResponse);
         [OperationContract]
         [FaultContract(typeof(RMException))]
         string ExportMapping(ExportLossCodeMapping oRequest);

         [OperationContract]
         [FaultContract(typeof(RMException))]
         void ReplicateLossCodeMapping(ReplicateLossCodeMapping oRequest, out LossCodeMappingOutput oResponse);

        
        [OperationContract]
         [FaultContract(typeof(RMException))]
         XElement GetDiminishingEvaluationDate(XElement xDoc, PolicyEnquiry oRequest);
    }
}


