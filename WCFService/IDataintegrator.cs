﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Riskmaster.Models;

namespace RiskmasterService
{
    // npadhy Converting the WCF service to WCF Rest for Cloud
    [ServiceContract]
    public interface IDataintegrator
    {
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/savesettings", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel SaveSettings(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/retrievesettings", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel RetrieveSettings(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getaccountlist", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PPAccountList GetAccountList(PPAccountList oPPAccountList);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/retrievejobfiles", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        JobFile RetrieveJobFiles(JobFile oJobFile);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/setclaimforreplacement", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        int SetClaimForReplacement(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/resetclaimforinitialsubmit", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        int ResetClaimForInitialSubmit(DataIntegratorModel oDataIntegratorModel);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getisolossmappings", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetISOLossMappings(DataIntegratorModel oDataIntegratorModel);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getrmclaimtypemapping", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetRMClaimTypeMapping(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getcoveragetypemapping", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetCoverageTypeMapping(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getlosstypemapping", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetLossTypeMapping(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/savermlosstypemapping", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel SaveRMLossTypeMapping(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getclaimanttypemapping", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetClaimantTypeMapping(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/saveclaimanttypemapping", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel SaveClaimantTypeMapping(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getadmintrackfields", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetAdminTrackFields(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getbankaccountlist", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetBankAccountList(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getreservelist4lob", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetReserveList4LOB(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/updatedastagingdatabase", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UpdateDAStagingDatabase(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/peekdastagingdatabase", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void PeekDAStagingDatabase(DataIntegratorModel oDataIntegratorModel);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/RetrieveFile", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void RetrieveFile(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getrmconfiguratordatemppath", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetRMConfiguratorDATempPath(DataIntegratorModel objDIModel);
        
        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/uploaddaimportfile", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UploadDAImportFile(DAImportFile oDAImportFile);

        /// Vsoni5 : 01/29/2011 : MITS 23347, 23357, 23358,23364, 23365, 23439, 23046
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/savesettingscleanup", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void SaveSettingsCleanup(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/gettaxids", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        System.Data.DataSet GetTaxIds(DataIntegratorModel oDataIntegratorModel);

        //mihtesham
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getpolicylob", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetPolicyLOB(DataIntegratorModel oDataIntegratorModel);

        //Developer – abharti5 |MITS 36676 | start
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getpolicysystemnames", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetPolicySystemNames(DataIntegratorModel oDataIntegratorModel);
        //Developer – abharti5 |MITS 36676 | end

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getpolicyclaimtype", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetPolicyClaimType(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getwcpolicytype", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetWCPolicyType(DataIntegratorModel oDataIntegratorModel);

        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getpolicyclaimlobtype", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        int GetPolicyClaimLOBType(DataIntegratorModel oDataIntegratorModel);
        
        [OperationContract]
        [FaultContract(typeof(RMException))]
        [WebInvoke(Method = "POST", UriTemplate = "/getpolicycoveragetype", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        DataIntegratorModel GetPolicyCoverageType(DataIntegratorModel oDataIntegratorModel);
        
        //sagarwal54 MITS 35386
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/getrmaproptypeandisoproptypeservicefunc", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetrmAPropTypeAndISOPropTypeServiceFunc(DataIntegratorModel oDataIntegratorModel);

        //mihtesham MITS 35711
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/getrmasproleandisosproleservicefunc", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetrmASPRoleAndISOSPRoleServiceFunc(DataIntegratorModel oDataIntegratorModel);

        //sagarwal54 MITS 35704
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/getadditionalclaimantroletype", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetAdditionalClaimantRoleType(DataIntegratorModel oDataIntegratorModel);
    }
}
