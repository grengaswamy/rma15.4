﻿//#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Models;
using Riskmaster.Security;
using Riskmaster.Security.Authentication;
using Riskmaster.Security.RMApp;
using Riskmaster.Settings;
using System.Xml;
using System.Xml.Linq;
using System.Data;//asharma326 MITS 27586
using Riskmaster.Cache;

namespace RiskmasterService
{
    // NOTE: If you change the class name "AuthenticationService" here, you must also update the reference to "AuthenticationService" in Web.config.
    /// <summary>
    /// Provides the ability to authenticate users against a specified
    /// RISKMASTER Authentication/Authorization repository
    /// </summary>
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    #if DEBUG
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    #else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
    #endif
    public class AuthenticationService : RMService, IAuthenticationService
    {
        #region IAuthenticationService Members

        /// <summary>
        /// Authenticates the user using the Default RISKMASTER Membership Provider
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <param name="strPassword">string containing the user's login password</param>
        /// <param name="strMessage">string containing the exception message if any</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public bool AuthenticateUser(string strUserName, string strPassword, out string strMessage)
        {
            string strDecodedUserName = string.Empty, strDecodedPassword = string.Empty;

            strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(strUserName);
            strDecodedPassword = System.Web.HttpContext.Current.Server.HtmlDecode(strPassword);

            bool blnIsAuthenticated = MembershipProviderFactory.AuthenticateUser(ConfigurationInfo.GetSecurityConnectionString(0), strDecodedUserName, strDecodedPassword, out strMessage, 0);

            return blnIsAuthenticated;
        }//method: AuthenticateUser()

        /// <summary>
        /// Authenticate Mobile User
        /// </summary>
        /// <param name="xmlRequest"></param>
        /// <param name="xmlRepsonse"></param>
        /// <param name="strMessage"></param>
        /// <returns></returns>
        public string AuthenticateMobileUser(string xmlRequest)
        {
            XmlDocument objXmlRequest = new XmlDocument();
            
            string strDecodedUserName = string.Empty;
            string strDecodedDSNName = string.Empty;
            string strUserSessionId = string.Empty;
            string strResponse = string.Empty;
            XmlNode objXmlUserName  = null;
            XmlNode objXmlDSNName = null;
                        
            try
            {
                objXmlRequest.LoadXml(xmlRequest);

                objXmlUserName = objXmlRequest.SelectSingleNode("//UserName");
                objXmlDSNName = objXmlRequest.SelectSingleNode("//DSNName");
            
                if (objXmlUserName != null)
                    strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(objXmlUserName.InnerText);

                if (objXmlDSNName != null)
                    strDecodedDSNName = System.Web.HttpContext.Current.Server.HtmlDecode(objXmlDSNName.InnerText); 
                
                strUserSessionId = GetUserSessionID(strDecodedUserName.Trim(), strDecodedDSNName.Trim());
                strResponse = "<ResultMessage><MsgStatus><MsgStatusCd>Success</MsgStatusCd></MsgStatus><Document><result category=\"Authorization\"><Token>" + strUserSessionId + "</Token><LoggedUser>" + strDecodedUserName.Trim() + "</LoggedUser><LoginTime>" + DateTime.Now.ToString() + "</LoginTime></result></Document></ResultMessage>";                
            }
            catch (Exception ex)
            {  
                strResponse = "<ResultMessage><MsgStatus><MsgStatusCd>Error</MsgStatusCd><ExtendedStatus><ExtendedStatusCd></ExtendedStatusCd><ExtendedStatusDesc>" + ex.Message + "</ExtendedStatusDesc><ExtendedMsgType>Error</ExtendedMsgType></ExtendedStatus></MsgStatus></ResultMessage>";          
            }

            return strResponse;
            

            
        }

        /// <summary>
        /// Authenticates the SMS user using the Default RISKMASTER Membership Provider
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <param name="strPassword">string containing the user's login password</param>
        /// <param name="strMessage">string containing the exception message if any</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public bool AuthenticateSMSUser(string strUserName, string strPassword, out string strMessage)
        {
            string strDecodedUserName = string.Empty, strDecodedPassword = string.Empty;

            strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(strUserName);
            strDecodedPassword = System.Web.HttpContext.Current.Server.HtmlDecode(strPassword);

            bool blnIsAuthenticated = MembershipProviderFactory.AuthenticateSMSUser(ConfigurationInfo.GetSecurityConnectionString(0), strDecodedUserName, strDecodedPassword, out strMessage, 0);

            return blnIsAuthenticated;
        }
        /// <summary>
        /// Authenticates the user using the Default RISKMASTER Membership Provider
        /// </summary>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public bool SingleSignOnUser(string strUserName)
        {
            string strDecodedUserName = string.Empty;

            strDecodedUserName = System.Web.HttpContext.Current.Server.HtmlDecode(strUserName);

            bool blnIsAuthenticated = MembershipProviderFactory.SingleSignOnUser(ConfigurationInfo.GetSecurityConnectionString(0), strDecodedUserName, 0);
            return blnIsAuthenticated;
        }//method: SingleSignOnUser()


       


        /// <summary>
        /// Gets a collection of all available and configured authentication 
        /// providers
        /// </summary>
        /// <returns>Generic Dictionary collection of all available and configured
        /// Authentication Providers</returns>
        public Dictionary<string, string> GetAuthenticationProviders()
        {
            Dictionary<string, string> dictAuthProviders = new Dictionary<string, string>();
            return dictAuthProviders;
            //return MembershipProviderFactory.GetAuthenticationProviders();
        }

        /// <summary>
        /// Retrieves the list of DSNs for a specific user
        /// </summary>
        /// <param name="strUserName">string containing the logged in user name</param>
        /// <returns>Generic String Dictionary collection containing all of the User DSNs</returns>
        public Dictionary<string, string> GetUserDSNs(string strUserName)
        {
            Dictionary<string, string> dictUserDbs = new Dictionary<string,string>();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            StringBuilder strSQL = new StringBuilder();

            strSQL.Append("SELECT DATA_SOURCE_TABLE.DSN, DATA_SOURCE_TABLE.DSNID ");
            strSQL.Append("FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE ");
            strSQL.Append("WHERE USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID ");
            strSQL.AppendFormat("AND USER_DETAILS_TABLE.LOGIN_NAME={0} ", "~USERNAME~");
            strSQL.Append("ORDER BY DATA_SOURCE_TABLE.DSN");

            dictParams.Add("USERNAME", strUserName);

            //TODO: Migrate this code over to the Riskmaster.Security.Authentication library
            using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(0), strSQL.ToString(), dictParams))
            {
                while (reader.Read())
                {
                    string strDSNKey = reader["DSN"].ToString();
                    string strDSNID = reader.GetInt32("DSNID").ToString();

                    //Ensure that an ArgumentException is not thrown
                    //by adding the same DSN to the Dictionary Collection
                    if (! dictUserDbs.ContainsKey(strDSNKey))
                    {
                        dictUserDbs.Add(strDSNKey, strDSNID);
                    } // if

                } // while

                //Close the DataReader
                reader.Close();
            } // using

            return dictUserDbs;
        }//method: GetUserDSNs

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strUserName"></param>
        /// <param name="strDatabaseName"></param>
        /// <param name="strSelectedDsnId"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetUserViews(string strUserName, string strDatabaseName, string strSelectedDsnId)
        {
            StringBuilder strSQL = new StringBuilder();
            int selectedDSNId = 0;
            int iUserId = 0;
            int iGroupId = 0;
            string sConnectionString = string.Empty;
            
            selectedDSNId = System.Convert.ToInt32(strSelectedDsnId);

            //Create a new UserLogin object that can be utilized for various values
            //smishra25:Start by Shivendu for performance updates
           //UserLogin objUserLogin = new UserLogin(strUserName, strDatabaseName);
            UserLogin objUserLogin = new UserLogin(0);
            iUserId = objUserLogin.GetUserIdAndGroupId(strUserName, strDatabaseName, out iGroupId);
            
            //smishra25:End by Shivendu for performance updates

            strSQL.Append("SELECT NET_VIEWS.VIEW_NAME,NET_VIEWS.VIEW_ID ");
            strSQL.Append("FROM NET_VIEWS_MEMBERS,NET_VIEWS ");
            strSQL.AppendFormat("WHERE ((MEMBER_ID={0} ", "~USERID~");
            strSQL.AppendFormat("AND ISGROUP=0) OR (MEMBER_ID={0} ", "~GROUPID~");
            strSQL.Append("AND ISGROUP<>0)) ");
            strSQL.Append("AND NET_VIEWS_MEMBERS.VIEW_ID=NET_VIEWS.VIEW_ID ");
            strSQL.AppendFormat("AND NET_VIEWS_MEMBERS.DATA_SOURCE_ID={0} ", "~DSNID~"); 
            strSQL.AppendFormat("AND NET_VIEWS.DATA_SOURCE_ID={0} ", "~DSNID~");
            strSQL.Append("ORDER BY ISGROUP");

            Dictionary<string, string> dictUserViews = new Dictionary<string, string>();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //dictParams.Add("USERID", objUserLogin.UserId);
            //dictParams.Add("GROUPID", objUserLogin.GroupId);
            dictParams.Add("USERID", iUserId);
            dictParams.Add("GROUPID", iGroupId);
            dictParams.Add("DSNID", selectedDSNId);

            using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetConnectionString("ViewDataSource", 0), strSQL.ToString(), dictParams))
		    {
                while (reader.Read())
                {
                    string strViewKey = reader["VIEW_NAME"].ToString();
                    string strViewID = reader["VIEW_ID"].ToString();

                    //Ensure that an ArgumentException is not thrown
                    //by adding the same View to the Dictionary Collection
                    if (! dictUserViews.ContainsKey(strViewKey))
                    {
                        dictUserViews.Add(strViewKey, strViewID);
                    } // if

                } // while

		    } // using

            //Clean up
            objUserLogin = null;

            return dictUserViews;
        }//method: GetUserViews

        /// <summary>
        ///  returns the GetCustomViews
        /// </summary>
        /// <param name="strUserName"></param>
        /// <param name="strDatabaseName"></param>
        /// <param name="p_strSelectedDsnId"></param>
        /// <returns></returns>
        public string GetCustomViews(string strSelectedDsnId)
        {
            //rsolanki2(may 23,2010): extensibility updates start - retriving views with viewid at -1

            StringBuilder strSQL = new StringBuilder();
            
            //int selectedDSNId = 0;
            //selectedDSNId = System.Convert.ToInt32(strSelectedDsnId);           
            //Dictionary<string, string> dictUserViews = new Dictionary<string, string>();                        
                        
            strSQL.Append("|");

            using (DbReader reader = DbFactory.ExecuteReader
                (ConfigurationInfo.GetConnectionString("ViewDataSource", 0)
                , "SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID=-1 AND DATA_SOURCE_ID ="
                    + strSelectedDsnId))
            {
                while (reader.Read())
                {
                    strSQL.Append(reader["FORM_NAME"].ToString());
                    strSQL.Append("|");  
                } // while

            } // using
            //dictUserViews.Add("ViewList", strSQL.ToString());
            
            if (strSQL.Length==1)
            {
                return string.Empty;
            }                                    
            return strSQL.ToString();            
            
        }//method: GetCustomViews


        /// <summary>
        /// Gets the user's session ID based on their authentication credentials
        /// </summary>
        /// <param name="strUserName">string containing the user's Login Name</param>
        /// <param name="strDSNName">string containing the selected User's DSN</param>
        /// <returns>string containing the user's authenticated Session ID</returns>
        public string GetUserSessionID(string strUserName, string strDSNName)
        {
            string strSessionID = string.Empty;
            strSessionID = GetSessionDBID(ConfigurationInfo.GetSessionConnectionString(0), strUserName, strDSNName);

            return strSessionID;
        }//method: GetUserSessionID()

        //PSARIN2 R8 Perf Imp. New Session Info Get Method
        /// <summary>
        /// Gets the user's session info based on their authentication credentials
        /// </summary>
        /// <param name="strUserName">string containing the user's Login Name</param>
        /// <param name="strDSNName">string containing the selected User's DSN</param>
        /// /// <param name="strDsnId">string containing the selected User's DSN id</param>
        /// <returns>Dictionary<string, string> containing the user's authenticated Session Info</returns>
        public Dictionary<string, string> GetUserSessionInfo(string strUserName, string strDSNName,string strDsnId)
        {
            Dictionary<string, string> dictSessionInfo = new Dictionary<string, string>();
            string strSessionID = string.Empty;
            string strCustomViews = string.Empty;
            string sLangCode = string.Empty;
            string sEnableVSS = string.Empty;
            string sCurrentDateSetting = string.Empty; //igupta3
            string sAdjAssignmentAutoDiary = string.Empty;      //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            //Ankit Start : Worked on MITS - 32386 - FAS Integration
            StringBuilder strParmNames = new StringBuilder();
            string strReturnVal = string.Empty;
            string[] strArrReturnVal = null;
            string sEnableFAS = string.Empty;
            string sFileLocationSelection = string.Empty;
            string sSharedLocation = string.Empty;
            string sFASServer = string.Empty;
            string sFASUserId = string.Empty;
            string sFASPassword = string.Empty;
            string sFASFolder = string.Empty;
            //Ankit End
            bool bBOBSetting = false;
            strSessionID = GetSessionDBID(ConfigurationInfo.GetSessionConnectionString(0), strUserName, strDSNName);
            dictSessionInfo.Add("SessionId", strSessionID);
            bBOBSetting = GetBOBSetting(strUserName, strDSNName);
            dictSessionInfo.Add("BOBSetting", bBOBSetting.ToString());
            strCustomViews=GetCustomViews(strDsnId);
            dictSessionInfo.Add("CustomViews", strCustomViews);
            //Deb: Multi Language Changes
            sLangCode = GetLanguageCode(strUserName, strDSNName);

            //Ankit Start : Worked on MITS - 32386 - FAS Integration
            //sEnableFAS = GetFASSettings(strDSNName, strUserName); remove function call
            //sEnableVSS = GetVSSSettings(strDSNName, strUserName);remove function call
            sEnableFAS = "ENABLE_FAS";
            sEnableVSS = "ENABLE_VSS";
            sFileLocationSelection = "FILE_LOCATION";
            sSharedLocation = "SHARED_LOCATION";
            sFASServer = "FAS_SERVER";
            sFASUserId = "FAS_USER_ID";
            sFASPassword = "FAS_PASSWORD";
            sFASFolder = "FAS_FOLDER";
            strParmNames.Append(string.Concat("'", sEnableVSS, "', "));
            strParmNames.Append(string.Concat("'", sEnableFAS, "', "));
            strParmNames.Append(string.Concat("'", sFileLocationSelection, "', "));
            strParmNames.Append(string.Concat("'", sSharedLocation, "', "));
            strParmNames.Append(string.Concat("'", sFASServer, "', "));
            strParmNames.Append(string.Concat("'", sFASUserId, "', "));
            strParmNames.Append(string.Concat("'", sFASPassword, "', "));
            strParmNames.Append(string.Concat("'", sFASFolder, "'"));
            strReturnVal = GetAllSettings(strDSNName, strUserName, strParmNames.ToString());
            strArrReturnVal = strReturnVal.Split(new string[] { "~!@" }, StringSplitOptions.None);

            for (int i = 0; i < strArrReturnVal.Length; i++)
            {
                if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sEnableVSS, "@!~").ToUpper()))
                    sEnableVSS = strArrReturnVal[i].Replace(string.Concat(sEnableVSS, "@!~"), string.Empty);
                else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sEnableFAS, "@!~").ToUpper()))
                    sEnableFAS = strArrReturnVal[i].Replace(string.Concat(sEnableFAS, "@!~"), string.Empty);
                else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFileLocationSelection, "@!~").ToUpper()))
                    sFileLocationSelection = strArrReturnVal[i].Replace(string.Concat(sFileLocationSelection, "@!~"), string.Empty);
                else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sSharedLocation, "@!~").ToUpper()))
                    sSharedLocation = strArrReturnVal[i].Replace(string.Concat(sSharedLocation, "@!~"), string.Empty);
                else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFASServer, "@!~").ToUpper()))
                    sFASServer = strArrReturnVal[i].Replace(string.Concat(sFASServer, "@!~"), string.Empty);
                else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFASUserId, "@!~").ToUpper()))
                    sFASUserId = strArrReturnVal[i].Replace(string.Concat(sFASUserId, "@!~"), string.Empty);
                else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFASPassword, "@!~").ToUpper()))
                    sFASPassword = strArrReturnVal[i].Replace(string.Concat(sFASPassword, "@!~"), string.Empty);
                else if (string.Concat(strArrReturnVal[i], "@!~").ToUpper().StartsWith(string.Concat(sFASFolder, "@!~").ToUpper()))
                    sFASFolder = strArrReturnVal[i].Replace(string.Concat(sFASFolder, "@!~"), string.Empty);
            }
            //Ankit End
            sCurrentDateSetting = GetCurrentDateSetting(strDSNName, strUserName); //igupta3
            sAdjAssignmentAutoDiary = GetAAdjAssignmentAutoDiarySettings(strDSNName, strUserName);      //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            dictSessionInfo.Add("LanguageCode", sLangCode);
            dictSessionInfo.Add("BaseLanguageCode", RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString());
            dictSessionInfo.Add("EnableVSS", sEnableVSS);
            dictSessionInfo.Add("CurrentDateSetting", sCurrentDateSetting); //igupta3
            dictSessionInfo.Add("AdjAssignmentAutoDiary", sAdjAssignmentAutoDiary);     //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            //Ankit Start : Worked on MITS - 32386 - FAS Integration
            //Check FAS Access
            if (sEnableFAS == "-1")
            {
                UserLogin objUserLogin = new UserLogin(strUserName, strDSNName, 0);
                const int FAS_SMS_PERMISSION = 450000;
                if (objUserLogin.IsAllowedEx(FAS_SMS_PERMISSION))
                {
                    sEnableFAS = "-1";
                }
                else
                {
                    sEnableFAS = "0";
                }
                objUserLogin = null;
            }
            dictSessionInfo.Add("EnableFAS", sEnableFAS);
            dictSessionInfo.Add("FileLocationSelection", sFileLocationSelection);
            dictSessionInfo.Add("SharedLocation", sSharedLocation);
            dictSessionInfo.Add("FASServer", sFASServer);
            dictSessionInfo.Add("FASUserId", sFASUserId);
            dictSessionInfo.Add("FASPassword", sFASPassword);
            dictSessionInfo.Add("FASFolder", sFASFolder);
            //Ankit End
            //Deb: Multi Language Changes
            return dictSessionInfo;
        }

        //Ankit Start : Worked on MITS - 32386 - Made a Generic Function for All Settings
        /// <summary>
        /// Use this Fucntion in order to extract any value from DB via authservervice Don't create new fucntion
        /// </summary>
        /// <param name="p_DSNName"></param>
        /// <param name="p_Username"></param>
        /// <param name="sWhereCondition"></param>
        /// <returns></returns>
        private string GetAllSettings(string p_DSNName, string p_Username, string sWhereCondition)
        {
            string sSQL = string.Empty;
            DataSet objDataSet = null;
            string strReturnValues = string.Empty;
            UserLogin objUserLogin=null;
            try
            {
                objUserLogin = new UserLogin(p_Username, p_DSNName, 0);
                sSQL = string.Concat("SELECT PARM_NAME, STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME IN (", sWhereCondition, ")");

                objDataSet = DbFactory.GetDataSet(objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL, 0);
                if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drMgr in objDataSet.Tables[0].Rows)
                    {
                        if (string.IsNullOrEmpty(strReturnValues))
                            strReturnValues = string.Concat(strReturnValues, string.Concat(drMgr["PARM_NAME"].ToString(), "@!~", drMgr["STR_PARM_VALUE"].ToString()));
                        else
                            strReturnValues = string.Concat(strReturnValues, "~!@", string.Concat(drMgr["PARM_NAME"].ToString(), "@!~", drMgr["STR_PARM_VALUE"].ToString()));
                    }
                }

                return strReturnValues;
            }
            finally
            {
                //clean up
                objUserLogin = null;
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }
            }
        }
        //Ankit End
        //averma62: FAS Enable setting
        /// <summary>
        /// GetFASSettings for the Username
        /// </summary>
        /// <param name="p_Username">p_Username</param>
        /// <param name="p_DSNName">p_Username</param>
        /// <returns>FASSettings</returns>
        //public string GetFASSettings(string p_DSNName, string p_Username)
        //{
        //    const int FAS_SMS_PERMISSION = 450000;
        //    string sEnableFAS = string.Empty;
        //    UserLogin objUserLogin = new UserLogin(p_Username, p_DSNName);
        //    sEnableFAS = Convert.ToString(DbFactory.ExecuteScalar(objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ENABLE_FAS'"));
        //    if (sEnableFAS == "-1")
        //    {
        //        if (objUserLogin.IsAllowedEx(FAS_SMS_PERMISSION))
        //        {
        //            sEnableFAS = "-1";
        //        }
        //        else
        //        {
        //            sEnableFAS = "0";
        //        }
        //    }
        //    objUserLogin = null;
        //    return sEnableFAS;
        //}
        /// <summary>
        /// Gets the necessary Session Database ID based on User Credentials
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="sUserName">string containing the logged in user name</param>
        ///<param name="sDSN">string containing the name of the DSN</param> 
        /// <returns>string containing the Session ID from the RISKMASTER Session database</returns>
        private string GetSessionDBID(string strConnString, string sUserName, string sDSN)
        {
            UserLogin objUserLogin = null;
            BusinessAdaptorErrors systemErrors = null;
            /***************************************************
                Kick user out if a Data Source not specified.
            **************************************************/
            if (string.IsNullOrEmpty(sDSN))
            {
                throw new FaultException("An existing DSN was not found for the specified user", new FaultCode("Sender"));
            }

            /***************************************************
                Authenticate against the new Data Source
             **************************************************/

            objUserLogin = new UserLogin(sUserName, sDSN, 0);
            //Raman 12/18/2009 : Since LSS bypasses our authentication (it is running in trusted mode)
            //hence if the loginname or database name is not correct still userlogin object gets created
            //adding check for loginname and databaseid for raising the exception

            if (objUserLogin == null || objUserLogin.LoginName == string.Empty || objUserLogin.DatabaseId == 0)
            {
                throw new FaultException("A user session was not able to be established.", new FaultCode("Sender"));
            }

            /***************************************************    
                Is Module Level Security Active?
             **************************************************/
            if (objUserLogin.objRiskmasterDatabase.Status)
            {
                if (objUserLogin.GroupId == 0 || !objUserLogin.IsAllowedEx(1))//User is not in a	 valid group? Or Group has no permission? 
                {
                    RMException theFault = new RMException();
                    theFault.Errors = formatOutputXML(null, false, systemErrors);
                    throw new FaultException<RMException>(theFault, new FaultReason(Globalization.GetString("LoginAdaptor.NoPermission", 0)), new FaultCode("Sender"));
                }
            } // if

            //Raman Bhatia 04/22/2009: Current Date and expiry date validations should be a part of AUTHORIZATION and not AUTHENTICATION
            //These settings work at a DataSource level and NOT during Authentication
            //Implementing logic here

            if (objUserLogin.PrivilegesExpire != DateTime.MinValue && objUserLogin.PrivilegesExpire < DateTime.Now)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(Globalization.GetString("Login.AuthUser.ForcedPwdExpired", 0)), new FaultCode("Sender"));
            }

            bool blnIsExpiredLogin = ValidateLoginPermission(objUserLogin);

            if (blnIsExpiredLogin)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(Globalization.GetString("Login.AuthUser.DateTimeExpired", 0)), new FaultCode("Sender"));
            }

            List<byte[]> arrSessionBinary = new List<byte[]>();
            
            arrSessionBinary.Add(Utilities.BinarySerialize(objUserLogin));
            
            // JP 6/15/2005   *Added - Begin*
            // Create and serialize other objects that RISKMASTER specifically needs. AC will have its own list and it will be different in the AC LoginAdaptor.   JP 6/15/2005
            // ... create UserLoginLimits object
            UserLoginLimits objLimits = new UserLoginLimits(objUserLogin, 0);

            arrSessionBinary.Add(Utilities.BinarySerialize(objLimits));

            return RMSessionManager.CreateSession(strConnString, sUserName, arrSessionBinary,0);
        }//method: GetSessionDBID()

        //Deb: Multi Language Changes
        /// <summary>
        /// GetLanguageCode for the Username
        /// </summary>
        /// <param name="p_Username">p_Username</param>
        /// <param name="p_DSNName">p_Username</param>
        /// <returns>LanguageCode</returns>
        public string GetLanguageCode(string p_Username,string  p_DSNName)
        {
            UserLogin objUserLogin = null;
            string sLangCodeAndCulture = string.Empty;
            string sBaseLangCode = "1033|en-US";
            try
            {
                sBaseLangCode = GetBaseLanguageCode();
                objUserLogin = new UserLogin(p_Username, p_DSNName, 0);
                
                if (objUserLogin.objUser.NlsCode != int.Parse(sBaseLangCode.Split('|')[0]))
                {
                    sLangCodeAndCulture = Convert.ToString(DbFactory.ExecuteScalar(Riskmaster.Security.SecurityDatabase.Dsn, "SELECT CULTURE FROM SYS_LANGUAGES WHERE LANG_ID=" + objUserLogin.objUser.NlsCode));
                }
                else
                {
                    sLangCodeAndCulture = sBaseLangCode.Split('|')[1];
                }
                return Convert.ToString(objUserLogin.objUser.NlsCode + "|" + sLangCodeAndCulture);
            }
            catch (Exception ee)
            {
                return sBaseLangCode;
            }
            finally
            {
                objUserLogin = null;
            }
        }

        //averma62: VSS Enable setting
        /// <summary>
        /// GetVSSSettings for the Username
        /// </summary>
        /// <param name="p_Username">p_Username</param>
        /// <param name="p_DSNName">p_Username</param>
        /// <returns>LanguageCode</returns>
        //public string GetVSSSettings(string p_DSNName,string p_Username)
        //{
        //    string sEnableVSS = string.Empty;
        //    UserLogin objUserLogin = new UserLogin(p_Username, p_DSNName);
        //    sEnableVSS = Convert.ToString(DbFactory.ExecuteScalar(objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ENABLE_VSS'"));
        //    objUserLogin = null;
        //    return sEnableVSS;
        //}

        //igupta3 Mits:32846
        /// <summary>
        /// GetCurrentDateSetting for the Username
        /// </summary>
        /// <param name="p_Username">p_Username</param>
        /// <param name="p_DSNName">p_Username</param>
        /// <returns>string for CurrentDateSetting enable or not</returns>
        public string GetCurrentDateSetting(string p_DSNName, string p_Username)
        {
            string sCurrentDateSetting = string.Empty;
            UserLogin objUserLogin = new UserLogin(p_Username, p_DSNName, 0);
            sCurrentDateSetting = Convert.ToString(DbFactory.ExecuteScalar(objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='USE_CUR_DATE_DIARY'"));
            objUserLogin = null;
            return sCurrentDateSetting;
        }

        //avipinsrivas: Adjuster Assignment Auto Diary setting for MITS - 29721
        /// <summary>
        /// GetAAdjAssignmentAutoDiarySettings for the Username
        /// </summary>
        public string GetAAdjAssignmentAutoDiarySettings(string p_DSNName, string p_Username)
        {
            string sAdjAssignmentAutoDiary = string.Empty;
            UserLogin objUserLogin = new UserLogin(p_Username, p_DSNName, 0);
            sAdjAssignmentAutoDiary = Convert.ToString(DbFactory.ExecuteScalar(objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ADJ_ASIGN_AUTO_DIARY'"));
            objUserLogin = null;
            return sAdjAssignmentAutoDiary;
        }

        /// <summary>
        /// GetBaseLanguageCode
        /// </summary>
        /// <returns>BaseLangCode</returns>
        public string GetBaseLanguageCode()
        {
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString();
            return sBaseLangCode;
        }
        //Deb: Multi Language Changes
        /// <summary>
        /// Verifies that the user is permitted to log into the system
        /// </summary>
        /// <param name="objUserLogin">User Login object</param>
        /// <returns>boolean indicating whether or not the user is permitted
        /// to log into the system at the current time</returns>
        private bool ValidateLoginPermission(UserLogin objUserLogin)
        {
            string strCurrentTime = DateTime.Now.ToString("HHmmss");
            string strBeginTime = String.Empty, strEndTime = string.Empty;
            bool bSuccess = false;
            bool bDateTimeExpired = false;
            const string PERMITTED_TIME = "000000";

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    strBeginTime = objUserLogin.SunStart;
                    strEndTime = objUserLogin.SunEnd;
                    break;
                case DayOfWeek.Monday:
                    strBeginTime = objUserLogin.MonStart;
                    strEndTime = objUserLogin.MonEnd;
                    break;
                case DayOfWeek.Tuesday:
                    strBeginTime = objUserLogin.TueStart;
                    strEndTime = objUserLogin.TueEnd;
                    break;
                case DayOfWeek.Wednesday:
                    strBeginTime = objUserLogin.WedStart;
                    strEndTime = objUserLogin.WedEnd;
                    break;
                case DayOfWeek.Thursday:
                    strBeginTime = objUserLogin.ThuStart;
                    strEndTime = objUserLogin.ThuEnd;
                    break;
                case DayOfWeek.Friday:
                    strBeginTime = objUserLogin.FriStart;
                    strEndTime = objUserLogin.FriEnd;
                    break;
                case DayOfWeek.Saturday:
                    strBeginTime = objUserLogin.SatStart;
                    strEndTime = objUserLogin.SatEnd;
                    break;
            }

            if (! strBeginTime.Equals(PERMITTED_TIME) || ! strEndTime.Equals(PERMITTED_TIME))
            {
                if (strEndTime.Equals(PERMITTED_TIME))
                {
                    if (Conversion.CastToType<long>(strCurrentTime, out bSuccess) < Conversion.CastToType<long>(strBeginTime, out bSuccess))
                    {
                        bDateTimeExpired = true;
                    }//if
                }//if
                else if ((Conversion.CastToType<long>(strCurrentTime, out bSuccess) < Conversion.CastToType<long>(strBeginTime, out bSuccess)) || (Conversion.CastToType<long>(strCurrentTime, out bSuccess) > Conversion.CastToType<long>(strEndTime, out bSuccess)))
                {
                    bDateTimeExpired = true;
                }//else if
            }//if

            return bDateTimeExpired;
        }//method: ValidateLoginPermission()

        /// <summary>
        /// Provides the ability to change a user's password
        /// </summary>
        /// <param name="p_sLoginName"></param>
        /// <param name="p_sOldPassword"></param>
        /// <param name="p_sNewPassword"></param>
        /// <returns>boolean indicating whether or not the user's password was successfully changed</returns>
        public bool ChangePassword(string p_sLoginName, string p_sOldPassword, string p_sNewPassword)
        {
            bool bIsPasswordChanged = false;

            CustomMembershipProvider membProvider = MembershipProviderFactory.CreateMembershipProvider(ConfigurationInfo.GetSecurityConnectionString(0), 0);

            bIsPasswordChanged = membProvider.ChangePassword(p_sLoginName, p_sOldPassword, p_sNewPassword);

            return bIsPasswordChanged;
        }

        /// <summary>
        /// Logs out a user and removes their session information from the database
        /// </summary>
        /// <param name="strSessionID">string containing the current user's Session ID</param>
        public void LogOutUser(string strSessionID)
        {
            RMSessionManager.RemoveSession(ConfigurationInfo.GetSessionConnectionString(0), strSessionID, string.Empty);
        } // method: LogOutUser

        //added by Amitosh for Adding carrier claim settings in session
        public bool GetBOBSetting(string p_sloginUserName, string sDataSourceName)
        {
            bool bReturnValue = false;
            SysSettings objSettings = null;
            UserLogin objUserLogin = null;
            try
            {
                objUserLogin = new UserLogin(p_sloginUserName, sDataSourceName, 0);
                objSettings = new SysSettings(objUserLogin.objRiskmasterDatabase.ConnectionString, 0);

                bReturnValue = Conversion.ConvertObjToBool(objSettings.MultiCovgPerClm,0);

            }

            catch
            {
                if (objUserLogin != null)
                    objUserLogin = null;
                if (objSettings != null)
                    objSettings = null;

            }


            return bReturnValue;
        }

        //Asharma326 MITS 27586 Starts
        /// <summary>
        /// Fucntion for checking whether Email address is configured for userid or not.
        /// </summary>
        /// <param name="p_userId"></param>
        /// <returns></returns>
        public ForgotPasswordGenericResponse ForgotPasswordSendMail(string s_UserId)
        {
            string sretunrvalue = string.Empty;
            Mailer objMailer = null;

            string s_TempPwd = CommonFunctions.GetRandomPassword(5);
            ForgotPasswordGenericResponse objForgotPasswordGenericResponse = new ForgotPasswordGenericResponse();
            string s_EmailID = string.Empty;
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT DISTINCT USER_TABLE.EMAIL_ADDR FROM USER_DETAILS_TABLE,USER_TABLE ");
                sbSQL.Append("WHERE USER_DETAILS_TABLE.USER_ID=USER_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME= '" + s_UserId + "'");
                using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(0), sbSQL.ToString()))
                {
                    if (reader.Read())
                    {
                        s_EmailID = reader["EMAIL_ADDR"].ToString();
                    }

                }
                if (!string.IsNullOrEmpty(s_EmailID))
                {
                    try
                    {
                        //Update password to DB.
                        //StringBuilder sb_update = new StringBuilder();
                        sbSQL.Clear();
                        sbSQL.Append("UPDATE USER_DETAILS_TABLE Set PASSWORD= '" + Riskmaster.Security.Encryption.RMCryptography.EncryptString(s_TempPwd) + "' ,IS_PWD_RESET=-1 where LOGIN_NAME= '" + s_UserId + "'");
                        DbFactory.ExecuteScalar(ConfigurationInfo.GetSecurityConnectionString(0).ToString(), sbSQL.ToString());

                        //Send Mail to User.
                        //StringBuilder sb_body = new StringBuilder();
                        //sb_body.Append("Hi " + s_UserId + ",");
                        //sb_body.Append("<br/>");
                        //sb_body.Append("<br/>");
                        //sb_body.Append("Your passworwd has been reset, Please use the below temporary password for Login.");
                        //sb_body.Append("<br/>");
                        //sb_body.Append("<br/>");
                        //sb_body.Append("Your Temporary Password : " + "<b>" + s_TempPwd + "</b>");
                        //sb_body.Append("<br/>");
                        //sb_body.Append("<br/>");
                        //sb_body.Append("Thanks,");
                        //sb_body.Append("<br/>");
                        //sb_body.Append("RiskMasterSupport Team");

                        objMailer = new Mailer(0);
                        objMailer.To = s_EmailID;
                        objMailer.From = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtpwdfromMailid", 0));
                        objMailer.Subject = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtpwdSubject", 0));
                        //objMailer.Body = sb_body.ToString();
                        objMailer.Body = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtPwdMailBody", 0).Replace("User_id", s_UserId).Replace("s_TempPwd", s_TempPwd));
                        objMailer.IsBodyHtml = true;
                        objMailer.SendMail();
                        //       CommonFunctions.SendEmail(s_EmailID, "RiskMasterSupport@CSC.Com", "RMA CREDENTIALS", sb_body.ToString(), true);
                        objForgotPasswordGenericResponse.Status = "Success";
                        objForgotPasswordGenericResponse.Message = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtpwdSuccessMessage", 0));
                    }
                    catch (Exception exe)
                    {

                        objForgotPasswordGenericResponse.Status = "Error";
                        objForgotPasswordGenericResponse.Message = CommonFunctions.FilterBusinessMessage(exe.Message.ToString());
                    }
                }
                else
                {
                    objForgotPasswordGenericResponse.Status = "Error";
                    objForgotPasswordGenericResponse.Message = CommonFunctions.FilterBusinessMessage(Globalization.GetString("frgtpwdErrorMessage", 0));
                }
            }
            catch (Exception exe)
            {
                exe.ToString();
            }
            return objForgotPasswordGenericResponse;
        }
        /// <summary>
        /// check Whether password has been reset for user or not.
        /// </summary>
        /// <param name="p_UserId"></param>
        /// <returns></returns>
        public bool IsPasswordReset(string p_UserId,int iClientId)
        {
            try
            {
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT DISTINCT IS_PWD_RESET FROM USER_DETAILS_TABLE WHERE LOGIN_NAME= '" + p_UserId + "'");
                using (DbReader reader = DbFactory.ExecuteReader(ConfigurationInfo.GetSecurityConnectionString(iClientId), sbSQL.ToString()))
                {
                    if (reader.Read())
                    {
                        return Conversion.ConvertObjToBool(reader["IS_PWD_RESET"].ToString(),iClientId);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception exe)
            {
                exe.ToString();
            }
            return false;
        }
        //Asharma326 MITS 27586 Ends

        
		
		//Rakhel Multi Language Changes
        /// <summary>
        /// GetDateFormat for the Username,DSNName
        /// </summary>
        /// <param name="p_Username">p_Username</param>
        /// <param name="p_DSNName">p_DSNName</param>
        /// <returns>DateFormat</returns>
        public string GetDateFormatFromLoginDetails(string p_Username, string p_DSNName)
        {
            string sDateFormat = string.Empty;
            string sLangCode = string.Empty;
            try
            {
                sLangCode = GetLanguageCode(p_Username, p_DSNName);
                
                if (sLangCode != string.Empty)
                {
                    sLangCode = sLangCode.Split('|')[0].ToString();
                    sDateFormat = Convert.ToString(DbFactory.ExecuteScalar(Riskmaster.Security.SecurityDatabase.Dsn, "SELECT DATEFORMAT FROM SYS_LANGUAGES WHERE LANG_ID=" + sLangCode));
                }
                return sDateFormat;
            }
            catch (Exception ee)
            {
                return sDateFormat;
            }
            finally
            {
                sDateFormat = null;
            }
        }

        //Rakhel Multi Language Changes
        /// <summary>
        /// GetDateFormat for the Languade Code
        /// </summary>
        /// <param name="p_Username">p_LangCode</param>
        /// <returns>DateFormat</returns>
        public string GetDateFormatFromLangCode(string p_LangCode,int iClientId)
        {
            string sDateFormat = string.Empty;
            string sLangCode = string.Empty;
            try
            {
                if (p_LangCode != string.Empty)
                {
                    sDateFormat = Convert.ToString(DbFactory.ExecuteScalar(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(iClientId), "SELECT DATEFORMAT FROM SYS_LANGUAGES WHERE LANG_ID=" + p_LangCode));
                }
                return sDateFormat;
            }
            catch (Exception ee)
            {
                return sDateFormat;
            }
            finally
            {
                sDateFormat = null;
            }
        }
        #endregion
    }
}
