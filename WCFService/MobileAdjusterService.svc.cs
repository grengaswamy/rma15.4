﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Models;
using Riskmaster.Common;
using Riskmaster.Security;
using System.Collections;
using System.ServiceModel.Activation;
using System.Reflection;
using Riskmaster.Cache;
namespace RiskmasterService
{
    // NOTE: If you change the class name "ProgressNoteService" here, you must also update the reference to "ProgressNoteService" in Web.config.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
#if DEBUG
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
#else
        [ServiceBehavior(AddressFilterMode = AddressFilterMode.Prefix)]
#endif
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AdjusterService" in code, svc and config file together.
    public class MobileAdjusterService : IMobileAdjusterService
    {
        
        private static Hashtable m_AdaptorCache = new Hashtable();
        public void DoWork()
        {
        }

        public string AdjusterRowId(string xmlRequest)
        {
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            XmlDocument xmlDocRequest = null;
            XmlDocument xmlDocResponse = null;
            XmlDocument xmlOutEnvelope = null;
            XmlElement xmlIn = null;
            XmlElement xmlAuth = null;
            XmlElement xmlFunction = null;           // Will point to function in call
            XmlDocument xmlTmp = null;
            bool bResult = false;
            string sessionKeySupplied = string.Empty;
            UserLogin oUserLogin = null;
            int iClientId = 0;
            MobileAdjusterAdaptor objMobAdjuster = null;
            string functionName = "GetAdjusterRowIds";

            try
            {
                xmlDocRequest = new XmlDocument();
                xmlDocResponse = new XmlDocument();
                systemErrors = new BusinessAdaptorErrors(iClientId);
                string ret = string.Empty;
                xmlTmp = new XmlDocument();
                xmlDocRequest.LoadXml(xmlRequest);

                xmlIn = (XmlElement)xmlDocRequest.SelectSingleNode("/Message/Document/*");

                systemErrors = new BusinessAdaptorErrors(iClientId);

                objMobAdjuster = new MobileAdjusterAdaptor();

                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(xmlDocRequest, functionName, objMobAdjuster, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                //logErrors(functionName, xmlRequest, false, systemErrors);
                //RMException theFault = new RMException();
                xmlOutEnvelope = formatOutputXML(null, false, systemErrors);
                //throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
                return xmlOutEnvelope.OuterXml;
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, iClientId);
                xmlTmp.LoadXml(xmlIn.OuterXml);
                bResult = objMobAdjuster.GetAdjusterRowId(xmlTmp, ref xmlDocResponse, ref errOut);
                xmlOutEnvelope = formatOutputXML(xmlDocResponse, bResult, errOut);
                return xmlOutEnvelope.OuterXml;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                //logErrors(functionName, xmlRequest, false, systemErrors);
                //RMException theFault = new RMException();
                xmlOutEnvelope = formatOutputXML(null, false, systemErrors);
                //throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
                return xmlOutEnvelope.OuterXml;
            }


        }

        #region Primary Service initialization method
        /// <summary>
        /// This method initialize the Common tasks to be done for each service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="xmlRequest"></param>
        /// <param name="functionName"></param>
        /// <param name="adaptor"></param>
        /// <param name="oUserLogin"></param>
        /// <param name="systemErrors"></param>
        /// <returns></returns>
        protected bool InitiateServiceProcess(XmlDocument xmlDocRequest, string functionName, BusinessAdaptorBase adaptor,
            out UserLogin oUserLogin, ref BusinessAdaptorErrors systemErrors)
        {

            XmlElement xmlAuth = null;
            string sessionKeySupplied = string.Empty;
            xmlAuth = (XmlElement)xmlDocRequest.SelectSingleNode("/Message/Authorization");
            int iClientId = 0;
            sessionKeySupplied = xmlAuth.InnerText;

            //xmlDocResponse.Load(xmlDocRequest.InnerXml);
            if (!RMSessionManager.SessionExists(ConfigurationInfo.GetSessionConnectionString(iClientId), sessionKeySupplied, iClientId)) // If user not in session'
            {
                throw new Exception(Globalization.GetString("Riskmaster.WebService.Common.CallerNotAuthorized", iClientId));
            }
            else //Session Exists - unpack UserLogin Object
            {
                UserSession objUserSession = RMSessionManager.GetSession(ConfigurationInfo.GetSessionConnectionString(iClientId), sessionKeySupplied, string.Empty);
                SessionManager m_oSession = SessionManager.LoadSession(sessionKeySupplied, iClientId);

                oUserLogin = new UserLogin(iClientId);
                oUserLogin = (UserLogin)Utilities.BinaryDeserialize(objUserSession.BinaryItems[AppConstants.SESSION_OBJ_USER].BinaryValue);
                systemErrors = new BusinessAdaptorErrors(oUserLogin,iClientId); //Apply Login Info for possible later uncaught execptions.

                if (oUserLogin != null) //BSB Protect for "bypass security" case.
                    adaptor.SetSecurityInfo(oUserLogin, iClientId);

                // Pass session object to business adaptor
                if (m_oSession != null)
                {
                    adaptor.SetSessionObject(m_oSession);
                }//if
            }//else



            return true;
        }
        #endregion

        private XmlDocument formatOutputXML(XmlDocument xmlOut, bool bCallResult, BusinessAdaptorErrors errors)
        {
            // Create output envelope doc and root node (ResultMessage)
            XmlDocument xmlOutEnv = new XmlDocument();
            // JP TMP 01.18.2006   XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage", XML_NAMESPACE);
            XmlElement xmlRoot = xmlOutEnv.CreateElement("ResultMessage");
            xmlOutEnv.AppendChild(xmlRoot);

            // Add errors
            formatErrorXML(xmlOutEnv, xmlRoot, bCallResult, errors);

            // Add output document
            // JP TMP 01.18.2006   XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document", XML_NAMESPACE);
            XmlElement xmlDocEnv = xmlOutEnv.CreateElement("Document");
            xmlRoot.AppendChild(xmlDocEnv);

            if (xmlOut != null)  // output doc can be null if error occurred - but <Document/> element still needs to be in place
                xmlDocEnv.InnerXml = xmlOut.OuterXml;

            // Return output envelope
            return xmlOutEnv;

        }
        private void formatErrorXML(XmlDocument xmlDoc, XmlElement xmlRoot, bool bCallResult, BusinessAdaptorErrors errors)
        {
            XmlElement xmlElement = null;
            XmlElement xmlMsgsRoot = null;

            // Create error root element (MsgStatus)
            // JP TMP 01.18.2006   XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus", XML_NAMESPACE);
            XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus");
            xmlRoot.AppendChild(xmlErrRoot);

            // Add overall result status
            // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("MsgStatusCd", XML_NAMESPACE);
            xmlElement = xmlDoc.CreateElement("MsgStatusCd");
            if (bCallResult)
                xmlElement.InnerText = "Success";
            else
                xmlElement.InnerText = "Error";

            xmlErrRoot.AppendChild(xmlElement);

            if (errors != null)
            {
                foreach (BusinessAdaptorError err in errors)
                {
                    // ... add individual error messages/warnings (if no errors, there will still be an empty ExtendedStatus element)
                    // JP TMP 01.18.2006   xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus", XML_NAMESPACE);
                    xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus");
                    xmlErrRoot.AppendChild(xmlMsgsRoot);

                    if (err.oException == null)
                    {  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
                        // ... add error code/number
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = err.ErrorCode;
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                        xmlElement.InnerText = err.ErrorDescription;
                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // non-Exception case
                    else
                    {  // Exception case
                        // Determine error code - assembly + exception type
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusCd", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                        xmlElement.InnerText = String.Format("{0}.{1}", err.oException.Source, err.oException.GetType().Name);
                        xmlMsgsRoot.AppendChild(xmlElement);

                        // ... add error description
                        // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc", XML_NAMESPACE);
                        xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                        if (!string.IsNullOrEmpty(err.ErrorDescription))
                            xmlElement.InnerText = err.ErrorDescription;
                        else
                            xmlElement.InnerText = err.oException.Message;

                        xmlMsgsRoot.AppendChild(xmlElement);
                    }  // Exception case

                    // ...add error type
                    // JP TMP 01.18.2006   xmlElement = xmlDoc.CreateElement("ExtendedMsgType", XML_NAMESPACE);
                    xmlElement = xmlDoc.CreateElement("ExtendedMsgType");
                    switch (err.ErrorType)
                    {
                        case BusinessAdaptorErrorType.SystemError:
                            xmlElement.InnerText = "SystemError";
                            break;
                        case BusinessAdaptorErrorType.Error:
                            xmlElement.InnerText = "Error";
                            break;
                        case BusinessAdaptorErrorType.Warning:
                            xmlElement.InnerText = "Warning";
                            break;
                        case BusinessAdaptorErrorType.Message:
                            xmlElement.InnerText = "Message";
                            break;
                        case BusinessAdaptorErrorType.PopupMessage:
                            xmlElement.InnerText = "PopupMessage";
                            break;
                        default:
                            // TODO   - What to do if not a standard error code?
                            break;
                    };
                    xmlMsgsRoot.AppendChild(xmlElement);
                }
            }

        }

        #region Adaptor class
        private class AdaptorCacheRecord
        {
            public string Name;			// Name key. This is what the common envelope uses to reference the function.
            public string Assembly;		// Assembly the class lives in. Filename of DLL.
            public string Class;			// Class method lives in.
            public string Method;			// Method to call in class.
            public bool BypassSecurity;	// If true, bypass AuthorizationToken check.
        }
        #endregion


    }
}
