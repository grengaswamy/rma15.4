﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Riskmaster.Models;


namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPageCacheService" in both code and config file together.
    /// <summary>
    /// WCF Service to retrieving latest time stamp for database table records 
    /// </summary>
    [ServiceContract]
    public interface IPageCache
    {
        /// <summary>
        /// Get connectionstring value which will be used to retrive timestamp
        /// </summary>
        /// <param name="DataSourceId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/connkeybydsnid", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetConnectionstringFromDSNId(PageData oPageData);

        /// <summary>
        /// Retrieve latest time stamp to decide if a page cache should be expired
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="sTableListConfig"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/tabletimestamps", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetTableTimestamps(PageData oPageData);

        /// <summary>
        /// Retrieve latest time stamp of particular table to decide if a page cache should be expired
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/timestampfororg", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetTimestampForOrgHierarchy(PageData oPageData);

        /// <summary>
        /// Retrieve userlogin info
        /// </summary>
        /// <param name="sUser"></param>
        /// <param name="sDsnName"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/logininfo", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetUserLoginInfo(PageData oPageData);
    }
}
