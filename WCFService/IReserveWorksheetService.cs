﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Riskmaster.Models;
namespace RiskmasterService
{
    // NOTE: If you change the interface name "IReserveWorksheetService" here, you must also update the reference to "IReserveWorksheetService" in Web.config.
    [ServiceContract]
    public interface IReserveWorksheetService
    {
        [OperationContract]
        ReserveWorksheetResponse GetReserveWorkSheet(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse OnLoadInformation(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse SaveReserveWorkSheet(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse SubmitReserveWorkSheet(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse ApproveReserveWorkSheet(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse RejectReserveWorkSheet(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse DeleteReserveWorkSheet(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse PrintReserveWorkSheet(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse ListFundsTransactions(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse ApproveReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest request);

        [OperationContract]
        ReserveWorksheetResponse RejectReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest request);
    }
}
