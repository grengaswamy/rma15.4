﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Models;
using Riskmaster.Common;
using System.ServiceModel.Activation;

namespace RiskmasterService
{
    // npadhy Converting the WCF service to WCF Rest for Cloud
    [AspNetCompatibilityRequirements(
    RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Dataintegrator : RMService, IDataintegrator
    {
        public DataIntegratorModel SaveSettings(DataIntegratorModel oDataIntegratorModel)
        {
            XmlDocument xmlRequest = null;
            string sFunctionName = "SaveSettings";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, sFunctionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objDIModelResponse = new DataIntegratorModel();
                errOut = new BusinessAdaptorErrors(oUserLogin, oDataIntegratorModel.ClientId);
                objDIModelResponse = objDataIntegratorAdaptor.SaveSettings(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(sFunctionName, xmlRequest, true, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, true, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel RetrieveSettings(DataIntegratorModel oDataIntegratorModel)
        {
            XmlDocument xmlRequest = null;
            string functionName = "RetrieveSettings";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objErrOut = null;
            DataIntegratorModel objDIModelResponse = null;
            bool bResult = false;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                //InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objSettings, out oUserLogin, ref systemErrors);
                //oDataIntegratorModel = objSettings.RetrieveSettings();
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            //Vsoni5 : MITS 22565 : Exception handling done for Error display on UI and error logging in service error log.
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objErrOut = new BusinessAdaptorErrors(oUserLogin, oDataIntegratorModel.ClientId);
                objDIModelResponse = objDataIntegratorAdaptor.RetrieveSettings(oDataIntegratorModel, ref objErrOut);
            }
            catch (Exception e)
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            if (objErrOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, objErrOut);
                logErrors(functionName, xmlRequest, bResult, objErrOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objDIModelResponse;
        }

        public PPAccountList GetAccountList(PPAccountList oPPAccountList)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetAccountList";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            PPAccountList objPPAccountListResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oPPAccountList.ClientId);
                objPPAccountListResponse = new PPAccountList();
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oPPAccountList, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oPPAccountList.ClientId);
                //          bResult = objDataIntegratorAdaptor.test(request);
                bResult = objDataIntegratorAdaptor.GetPositivePayAccountList(oPPAccountList);
                objPPAccountListResponse = oPPAccountList;
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objPPAccountListResponse;
        }

        public JobFile RetrieveJobFiles(JobFile oJobFile)
        {
            XmlDocument xmlRequest = null;
            string functionName = "RetrieveJobFiles";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            JobFile objJobFileResponse = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oJobFile.ClientId);
                objJobFileResponse = new JobFile();
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oJobFile, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oJobFile.ClientId);
                bResult = objDataIntegratorAdaptor.RetrieveJobFile(oJobFile, out  objJobFileResponse);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objJobFileResponse;
        }

        public int SetClaimForReplacement(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            int iResult = 0;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                iResult = objDataIntegratorAdaptor.SetClaimForReplacement(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return iResult;
        }

        public int ResetClaimForInitialSubmit(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            int iResult = 0;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                iResult = objDataIntegratorAdaptor.ResetClaimForInitialSubmit(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return iResult;
        }

        public DataIntegratorModel GetISOLossMappings(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetISOLossMappings(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel GetRMClaimTypeMapping(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetRMClaimTypeMapping(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel GetCoverageTypeMapping(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetCoverageTypeMapping(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel GetLossTypeMapping(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetLossTypeMapping(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel SaveRMLossTypeMapping(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.SaveRMLossTypeMapping(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel GetClaimantTypeMapping(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetClaimantTypeMapping(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel SaveClaimantTypeMapping(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.SaveClaimantTypeMapping(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel GetAdminTrackFields(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetAdminTrackFields(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel GetBankAccountList(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetBankAccountList(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel GetReserveList4LOB(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetReserveList4LOB(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public void UpdateDAStagingDatabase(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "UpdateDAStagingDatabase";
            XmlDocument xmlRequest = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);
                objDataIntegratorAdaptor.UpdateDAStagingDatabase(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }


	//kkaur25 start UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder
        public void RetrieveFile(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "RetrieveFile";
            XmlDocument xmlRequest = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);
                objDataIntegratorAdaptor.RetrieveFile(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

	//kkaur25 end UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder

        public void PeekDAStagingDatabase(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "PeekDAStagingDatabase";
            XmlDocument xmlRequest = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);
                objDataIntegratorAdaptor.PeekDAStagingDatabase(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public string GetRMConfiguratorDATempPath(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            string functionName = "GetRMConfiguratorDATempPath";
            XmlDocument xmlRequest = null;
            string sRMConfiguratorDATempPath = string.Empty;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                sRMConfiguratorDATempPath = objDataIntegratorAdaptor.GetRMConfiguratorDATempPath();
            }
            catch (Exception e)
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            return sRMConfiguratorDATempPath;
        }

        public void UploadDAImportFile(DAImportFile oDAImportFile)
        {
            XmlDocument xmlRequest = null;
            string functionName = "CreateDocument";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors errOut = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDAImportFile.ClientId);
                //objReturn = new RMServiceType();
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateStreamedServiceProcess(oDAImportFile, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                errOut = new BusinessAdaptorErrors(oUserLogin, oDAImportFile.ClientId);
                objDataIntegratorAdaptor.UploadDAImportFile(oDAImportFile);
            }
            catch (Exception e)
            {
                // Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            // Final log if all went well
            logErrors(functionName, xmlRequest, bResult, errOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (errOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, errOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public void SaveSettingsCleanup(DataIntegratorModel oDataIntegratorModel)
        {
            XmlDocument xmlRequest = null;
            string sFunctionName = "SaveSettingsCleanup";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objerrOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, sFunctionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objerrOut = new BusinessAdaptorErrors(oUserLogin, oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor.SaveSettingsCleanup(oDataIntegratorModel, ref objerrOut);
            }
            catch (Exception e)
            {
                //Throw back error if business adaptor isn't trapping its own exceptions.
                //systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, objerrOut);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, objerrOut);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            // Final log if all went well
            logErrors(sFunctionName, xmlRequest, true, objerrOut);
            //Explicitly throw out the error in case we have the Business/System/Warnings errors from BA Layer
            if (objerrOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, true, objerrOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
        }

        public System.Data.DataSet GetTaxIds(DataIntegratorModel oDataIntegratorModel)
        {
            XmlDocument xmlRequest = null;
            string sFunctionName = "GetTaxIds";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objErrOut = null;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, sFunctionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objErrOut = new BusinessAdaptorErrors(oUserLogin, oDataIntegratorModel.ClientId);
                return objDataIntegratorAdaptor.GetTaxIds(oDataIntegratorModel.EntityList);
            }
            catch (Exception e)
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(sFunctionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
        }

        public DataIntegratorModel GetPolicyLOB(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetPolicyLOB(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel GetPolicySystemNames(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                oDataIntegratorModel = objDataIntegratorAdaptor.GetPolicySystemNames(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return oDataIntegratorModel;
        }

        public DataIntegratorModel GetPolicyClaimType(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetPolicyClaimType(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public DataIntegratorModel GetWCPolicyType(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            //BusinessAdaptorErrors errOut = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetWCPolicyType(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }

        public int GetPolicyClaimLOBType(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            //BusinessAdaptorErrors errOut = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);

                return objDataIntegratorAdaptor.GetPolicyClaimLOBType(oDataIntegratorModel.iPolicyType);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

        }

        public DataIntegratorModel GetPolicyCoverageType(DataIntegratorModel oDataIntegratorModel)
        {
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            //BusinessAdaptorErrors errOut = null;
            DataIntegratorModel objDIModelResponse = null;

            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                //This is a common function which intialises/perform the common tasks on each service call
                InitiateServiceProcess(oDataIntegratorModel, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                objDIModelResponse = objDataIntegratorAdaptor.GetPolicyCoverageType(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            return objDIModelResponse;
        }
		//aramkumarsha: JIRA RMA-16817
        public string GetrmASPRoleAndISOSPRoleServiceFunc(DataIntegratorModel oDataIntegratorModel)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetrmASPRoleAndISOSPRole";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objErrOut = null;
            DataIntegratorModel objDIModelResponse = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                //This is a common function which intialises/perform the common tasks on each service call
                //InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                //oDataIntegratorModel = objDataIntegratorAdaptor.RetrieveSettings();
                //aramkumarsha: JIRA RMA-16817
				InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);


            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objErrOut = new BusinessAdaptorErrors(oUserLogin, oDataIntegratorModel.ClientId);
                objDIModelResponse = objDataIntegratorAdaptor.GetrmASPRoleAndISOSPRole(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            if (objErrOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, objErrOut);
                logErrors(functionName, xmlRequest, bResult, objErrOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objDIModelResponse.dsrmASPRoleCodes.GetXml();
        }
		//aramkumarsha: JIRA RMA-16817
        public string GetrmAPropTypeAndISOPropTypeServiceFunc(DataIntegratorModel oDataIntegratorModel)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetrmAPropTypeAndISOPropType";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objErrOut = null;
            DataIntegratorModel objDIModelResponse = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                //This is a common function which intialises/perform the common tasks on each service call
                //InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                //oDataIntegratorModel = objDataIntegratorAdaptor.RetrieveSettings();
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);


            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objErrOut = new BusinessAdaptorErrors(oUserLogin, oDataIntegratorModel.ClientId);
                objDIModelResponse = objDataIntegratorAdaptor.GetrmAPropTypeAndISOPropType(oDataIntegratorModel);
            }
            catch (Exception e)
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            if (objErrOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, objErrOut);
                logErrors(functionName, xmlRequest, bResult, objErrOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objDIModelResponse.dsrmAPropCodes.GetXml();
        }

        public string GetAdditionalClaimantRoleType(DataIntegratorModel oDataIntegratorModel)
        {
            XmlDocument xmlRequest = null;
            string functionName = "GetISOClaimPartyType";
            UserLogin oUserLogin = null;
            DataIntegratorAdaptor objDataIntegratorAdaptor = null;
            BusinessAdaptorErrors systemErrors = null;
            BusinessAdaptorErrors objErrOut = null;
            DataIntegratorModel objDIModelResponse = null;
            bool bResult = false;
            //Initialize the Error Collection for handling the error on Service Layer
            try
            {
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                objDataIntegratorAdaptor = new DataIntegratorAdaptor();
                objDIModelResponse = new DataIntegratorModel();
                systemErrors = new BusinessAdaptorErrors(oDataIntegratorModel.ClientId);
                //This is a common function which intialises/perform the common tasks on each service call
                //InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors);
                //oDataIntegratorModel = objDataIntegratorAdaptor.RetrieveSettings();
                InitiateServiceProcess(oDataIntegratorModel, out xmlRequest, functionName, objDataIntegratorAdaptor, out oUserLogin, ref systemErrors, Utilities.SerializationType.DataContractSerializer);


            }
            catch (Exception e)
            {
                // Throw back error if there is ne error in intialization process.
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }

            try
            {
                //Initialize the Error Collection to be passed to the respective BA Layer for error handling
                objErrOut = new BusinessAdaptorErrors(oUserLogin, oDataIntegratorModel.ClientId);
                objDIModelResponse = objDataIntegratorAdaptor.GetAdditionalClaimantRoleCodes(oDataIntegratorModel);
            }
             catch (Exception e) 
            {
                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);
                logErrors(functionName, xmlRequest, false, systemErrors);
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, false, systemErrors);
                throw new FaultException<RMException>(theFault, new FaultReason(e.Message), new FaultCode("Sender"));
            }
            if (objErrOut.Count > 0)
            {
                RMException theFault = new RMException();
                theFault.Errors = formatOutputXML(null, bResult, objErrOut);
                logErrors(functionName, xmlRequest, bResult, objErrOut);
                throw new FaultException<RMException>(theFault, new FaultReason(theFault.Errors), new FaultCode("Sender"));
            }
            return objDIModelResponse.dsAdditionalClaimantCodes.GetXml();
        }
    }
}
