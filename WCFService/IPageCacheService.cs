﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;


namespace RiskmasterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPageCacheService" in both code and config file together.
    /// <summary>
    /// WCF Service to retrieving latest time stamp for database table records 
    /// </summary>
    [ServiceContract]
    public interface IPageCacheService
    {
        /// <summary>
        /// Get connectionstring value which will be used to retrive timestamp
        /// </summary>
        /// <param name="DataSourceId"></param>
        /// <returns></returns>
        [OperationContract]
        string GetConnectionstringFromDSNId(int DataSourceId);

        /// <summary>
        /// Retrieve latest time stamp to decide if a page cache should be expired
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="sTableListConfig"></param>
        /// <returns></returns>
        [OperationContract]
        string GetTableTimestamps(string connectionString, string sTableListConfig);

        /// <summary>
        /// Retrieve latest time stamp of particular table to decide if a page cache should be expired
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        [OperationContract]
        string GetTimestampForOrgHierarchy(string connectionString);

        /// <summary>
        /// Retrieve userlogin info
        /// </summary>
        /// <param name="sUser"></param>
        /// <param name="sDsnName"></param>
        /// <returns></returns>
        [OperationContract]
        Dictionary<string, string> GetUserLoginInfo(string sUser, string sDsnName, int iClientId);
    }
}
