﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.DirectoryServices;
using System.Web;
using System.Web.Security;
using System.Web.Configuration;


/// <summary>
/// Summary description for RiskmasterLDAPMembershipProvider
/// </summary>
public class RiskmasterLDAPMembershipProvider: MembershipProvider
{
	public RiskmasterLDAPMembershipProvider()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public override string ApplicationName
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public override bool ChangePassword(string username, string oldPassword, string newPassword)
    {
        throw new NotImplementedException();
    }

    public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
    {
        throw new NotImplementedException();
    }

    public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
    {
        throw new NotImplementedException();
    }

    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    {
        throw new NotImplementedException();
    }

    public override bool EnablePasswordReset
    {
        get { throw new NotImplementedException(); }
    }

    public override bool EnablePasswordRetrieval
    {
        get { throw new NotImplementedException(); }
    }

    public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
        throw new NotImplementedException();
    }

    public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
        throw new NotImplementedException();
    }

    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    {
        throw new NotImplementedException();
    }

    public override int GetNumberOfUsersOnline()
    {
        throw new NotImplementedException();
    }

    public override string GetPassword(string username, string answer)
    {
        throw new NotImplementedException();
    }

    public override MembershipUser GetUser(string username, bool userIsOnline)
    {
        throw new NotImplementedException();
    }

    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    {
        throw new NotImplementedException();
    }

    public override string GetUserNameByEmail(string email)
    {
        throw new NotImplementedException();
    }

    public override int MaxInvalidPasswordAttempts
    {
        get { throw new NotImplementedException(); }
    }

    public override int MinRequiredNonAlphanumericCharacters
    {
        get { throw new NotImplementedException(); }
    }

    public override int MinRequiredPasswordLength
    {
        get { throw new NotImplementedException(); }
    }

    public override int PasswordAttemptWindow
    {
        get { throw new NotImplementedException(); }
    }

    public override MembershipPasswordFormat PasswordFormat
    {
        get { throw new NotImplementedException(); }
    }

    public override string PasswordStrengthRegularExpression
    {
        get { throw new NotImplementedException(); }
    }

    public override bool RequiresQuestionAndAnswer
    {
        get { throw new NotImplementedException(); }
    }

    public override bool RequiresUniqueEmail
    {
        get { throw new NotImplementedException(); }
    }

    public override string ResetPassword(string username, string answer)
    {
        throw new NotImplementedException();
    }

    public override bool UnlockUser(string userName)
    {
        throw new NotImplementedException();
    }

    public override void UpdateUser(MembershipUser user)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// Validates a user in the LDAP repository/store
    /// </summary>
    /// <param name="username">string containing the LDAP username</param>
    /// <param name="password">string containing the LDAP user's password</param>
    /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
    public override bool ValidateUser(string username, string password)
    {
        string strLDAPConnString = WebConfigurationManager.ConnectionStrings["LDAPConnectionString"].ConnectionString;
        bool blnIsAuthenticated = false;

        try
        {
            DirectoryEntry dirEntryLDAP = new DirectoryEntry(strLDAPConnString, username, password, AuthenticationTypes.None);

            //NOTE: This can be expressed more succintly in code through the ternary operator, but makes the code less readable
            //Example: blnIsAuthenticated = !string.IsNullOrEmpty(dirEntryLDAP.Name) ? true : false;
            if (! string.IsNullOrEmpty(dirEntryLDAP.Name))
            {
                blnIsAuthenticated = true;
            } // if
            else
            {
                blnIsAuthenticated = false;
            } // else

            
        } // try
        catch (Exception)
        {
            blnIsAuthenticated = false;
        } // catch

        return blnIsAuthenticated;

    }//method: ValidateUser()

    //public override bool ValidateUser(string username, string password)
    //{
    //    string strLDAPConnString = WebConfigurationManager.ConnectionStrings["LDAPConnectionString"].ConnectionString;

    //    //Find the person in the directory to determine their distinct name 

    //    try
    //    {


    //        DirectoryEntry root = new DirectoryEntry(strLDAPConnString, null, null, AuthenticationTypes.None);

    //        DirectorySearcher searcher = new DirectorySearcher(root);
    //        searcher.SearchScope = SearchScope.Subtree;
    //        searcher.Filter = "uid=" + username;

    //        SearchResult findResult = searcher.FindOne();

    //        string distinctName = "uid=" + username;

    //        //// Inverse the ou order found in LDAP to build distinct name 

    //        //for (int i = findResult.Properties["ou"].Count - 1; i >= 0; i--)
    //        //{

    //        //    distinctName += ",ou=" + findResult.Properties["ou"][i];
    //        //}

    //        //distinctName += ",o=YourCompanyName.com,c=US";

    //        // Find the person as Employee 

    //        DirectoryEntry root2 = new DirectoryEntry(strLDAPConnString,distinctName, password, AuthenticationTypes.ServerBind);

    //        DirectorySearcher searcher2 = new DirectorySearcher(root2);
    //        searcher2.SearchScope = SearchScope.Subtree;
    //        searcher2.Filter = "uid=" + username;

    //        try
    //        {


    //            SearchResult resultEmployee = searcher2.FindOne();

    //            if (resultEmployee.Properties["uid"].Count == 1) { return true; } else { return false; }
    //        }


    //        catch (Exception ex)
    //        {
    //            return false;
    //        }//catch

    //    }


    //    catch (Exception ex)
    //    {
    //        return false;
    //    }

    //} 
}
