﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

// NOTE: If you change the interface name "IAuthenticationService" here, you must also update the reference to "IAuthenticationService" in Web.config.
[ServiceContract]
public interface IAuthenticationService
{
    [OperationContract]
    bool AuthenticateUser(string MembershipProviderName);
}
