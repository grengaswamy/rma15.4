﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Security.Principal;

public partial class Login : System.Web.UI.Page
{
    private DropDownList ddlSSOProviders; 
    private RMAuthenticationService.AuthenticationServiceClient authService = new RMAuthenticationService.AuthenticationServiceClient();

    /// <summary>
    /// Handles the Page Load event for the screen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (! Page.IsPostBack)
        {
            ddlSSOProviders = Login1.FindControl("ddlSSOProviders") as DropDownList;

            //Bind to the DataSource
            ddlSSOProviders.DataSource = GetSSOProviders();
            ddlSSOProviders.DataTextField = "text";
            ddlSSOProviders.DataValueField = "value";
            ddlSSOProviders.DataBind();
        } // if

    }

    /// <summary>
    /// Authenticates the user using the specified credentials
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnAuthenticate(object sender, AuthenticateEventArgs e)
    {
        e.Authenticated = false;
        bool blnIsAuthenticated = false;

        ddlSSOProviders = Login1.FindControl("ddlSSOProviders") as DropDownList;
        string strSSOProvider = ddlSSOProviders.SelectedItem.Text;

        //RMAuthenticationService.AuthenticationServiceClient authService = new RMAuthenticationService.AuthenticationServiceClient("WSHttpBinding_IAuthenticationService");
        //authService.ClientCredentials.UserName.UserName = Login1.UserName;
        //authService.ClientCredentials.UserName.Password = Login1.Password;

        try
        {
            blnIsAuthenticated = authService.AuthenticateUser(strSSOProvider, Login1.UserName, Login1.Password);


            RMAuthenticationService.RiskmasterUser objRMUser = authService.GetRMUser(strSSOProvider, Login1.UserName);
            
            

            //HttpContext.Current.User = objRMUser;
            HttpContext.Current.Items.Add("RMUser", objRMUser);

            RMAuthenticationService.RiskmasterUser myObject = HttpContext.Current.Items["RMUser"] as RMAuthenticationService.RiskmasterUser;

            //object myObject = HttpContext.Current.Items["RMUser"] as RiskmasterUser;
            //object myObject = (Riskmaster)HttpContext.Current.Items["RMUser"];

            
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message, ex);
        }
        finally
        {
            authService.Close();
        } // finally

        //TODO: Need to determine if the service call needs to be closed

        e.Authenticated = blnIsAuthenticated;
    } // method: OnAuthenticate


    /// <summary>
    /// Gets the list of SSO Providers from the Web.config file
    /// </summary>
    /// <returns>ListItemCollection containing all of the currently configured SSO Providers</returns>
    private ListItemCollection GetSSOProviders()
    {
        //ListItemCollection objLiColl = new ListItemCollection();

        // //Get the connectionStrings key,value pairs collection.
        ////ConnectionStringSettingsCollection connectionStrings =
        ////    WebConfigurationManager.ConnectionStrings
        ////    as ConnectionStringSettingsCollection;

        //MembershipSection membSection = WebConfigurationManager.GetWebApplicationSection("system.web/membership") as MembershipSection;

        //ProviderSettingsCollection membProviders = membSection.Providers;
        
        //// Get the collection enumerator.
        //IEnumerator membProvidersEnum = membProviders.GetEnumerator();

        //// Loop through the collection and 
        //// display the connectionStrings key, value pairs.
        //int i = 0;
        //while (membProvidersEnum.MoveNext())
        //{
        //    string name = membProviders[i].Name;

        //    //Create a new ListItem and add it to the ListItemCollection
        //    ListItem objLi = new ListItem(membProviders[i].Name, membProviders[i].Type);
        //    objLiColl.Add(objLi);
        //    i += 1;
        //}

        ListItemCollection objLiColl = new ListItemCollection();

        Dictionary<string, string> objDictProviders = new Dictionary<string, string>();

        objDictProviders = authService.GetAuthenticationProviders();

        foreach (string strKey in objDictProviders.Keys)
        {
            ListItem objLiItem = new ListItem();

            objLiItem.Text = strKey;
            objLiItem.Value = objDictProviders[strKey];

            objLiColl.Add(objLiItem);
        } // foreach


        return objLiColl;
    } // method: GetSSOProviders


    /// <summary>
    /// Handles the Unload event of the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Unload(object sender, EventArgs e)
    {
        //Ensure that the call to the WCF service is closed upon termination of the page
        authService.Close();
    }
}
