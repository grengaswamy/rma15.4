<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/*">		
	<TABLE cellspacing="0"  width="100%">
        <colgroup>
           <col width ="40%" />
           <col width ="20%" />
           <col width ="40%" />
        </colgroup>
           <TR bgcolor="#6fb3e7" >                
                <td bgcolor="#6fb3e7" colspan="3" align="center">
                   <FONT face="Ariel" size="4">
                     <B>
                       <xsl:value-of select="name(.)" /> Information
                     </B>
                   </FONT>                 
                </td>   
           </TR>
	</TABLE>  
<TABLE>
 <xsl:for-each select="@*|*">
    <xsl:choose>
	   <xsl:when test="*">	   
	   <xsl:if test="not(name(.)='Risks' or name(.)='Parties' or name(.)='Coverages')">
	   <xsl:variable name="AttributeParent" select="name(.)"/>
	    <xsl:call-template name="ChildTemplate">
        <xsl:with-param name="NodeName" select="$AttributeParent"/>
        </xsl:call-template>		
	   </xsl:if>
	 </xsl:when>	 
	<xsl:otherwise>
	  <xsl:choose>
		<xsl:when test="@code">	
			<TR>
				<td>
					<B>						
						<xsl:value-of select="name(.)"/>						
					</B>
				</td>
				<td width="50px"></td>
				<td>
						<xsl:value-of select="@code"/> -
						<xsl:value-of select="@description"/>
				</td>
			</TR>
		</xsl:when>
		<xsl:otherwise>	
			<TR>
				<td>
					<B>						
						<xsl:value-of select="name(.)"/>						
					</B>
				</td>
				<td width="50px"></td>
				<td>
						<xsl:value-of select="."/>
				</td>
			</TR>	
		</xsl:otherwise>
	 </xsl:choose>	 
	</xsl:otherwise>
  </xsl:choose>     
 </xsl:for-each>
</TABLE>
</xsl:template >
<xsl:template name="ChildTemplate">
 <xsl:param name="NodeName"/>
	<xsl:for-each select="@*|*">
		<xsl:choose>
	          <xsl:when test="*">
					<xsl:variable name="AttributeParent" select="name(.)"/>
					<xsl:call-template name="ChildTemplate">
					<xsl:with-param name="NodeName" select="$AttributeParent"/>
					</xsl:call-template>
              </xsl:when>
			 <xsl:otherwise>	
			 <xsl:choose>
		    <xsl:when test="@code">	
			   <TR>
				  <td>
					 <B>						
						<xsl:value-of select="name(.)"/>						
					 </B>
				 </td>
				 <td width="50px"></td>
				 <td>
						<xsl:value-of select="@code"/> -
						<xsl:value-of select="@description"/>
				 </td>
			</TR>
		</xsl:when>
		<xsl:otherwise>
					<TR>
						<TD>
							<B>
								<xsl:if test="not($NodeName='Premium' or $NodeName='Name' or $NodeName='Address' or $NodeName='CoverageType')">
								<xsl:value-of select="$NodeName"/> 									
								&#160;
								</xsl:if>		
                                								
								<xsl:value-of select="name(.)"/>								
							</B>
						</TD>
						<td width="50px"></td>
						<TD>
								<xsl:value-of select="."/>
						</TD>
					</TR>					
		</xsl:otherwise>
	 </xsl:choose>
			</xsl:otherwise>
		</xsl:choose>	
	</xsl:for-each>
</xsl:template>
</xsl:stylesheet>