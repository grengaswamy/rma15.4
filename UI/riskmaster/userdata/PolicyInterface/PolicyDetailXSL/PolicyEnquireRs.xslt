﻿<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
          <FONT face="Ariel" size="2">

          <TABLE cellspacing="0"  width="100%">
            <colgroup>
              <col width ="40%" />
              <col width ="20%" />
              <col width ="40%" />
            </colgroup>
              <TR bgcolor="#6fb3e7" >

                
                <td bgcolor="#6fb3e7" colspan="3" align="center">
                   <FONT face="Ariel" size="4">
                     <B>
                       Policy Information

                     </B>
                   </FONT>
                 
                </td>
                
              
              </TR>
            <BR></BR>
              <TR >
                <TD >
                  <B>
                    <u>PolicyNumber: </u>
                  </B>
                    <xsl:value-of select="//PolicyNumber"/>
                  <BR></BR>
                </TD>
           
                <TD >

                </TD>

                <td>

                  <B>
                    <u>  PolicyStatus :</u>
                  </B>
                  <xsl:value-of select="//PolicyStatusCd"/>

                  <BR></BR>

                </td>
              </TR>
            <tr>

              <TD >
                <B>
                  <u>Policy Effective Date: </u>
                </B>
                <xsl:value-of select="//EffectiveDt"/>

                <BR></BR>

              </TD>
              <td></td>
              <TD >
                <B>
                  <u>Policy Expiration Date: </u>
                </B>
                <xsl:value-of select="//ExpirationDt"/>

                <BR></BR>
                
              </TD>
            </tr>
            <tr>

              <TD >
                <B>
                  <u>Agency Number/Name: </u>
                </B>
                <xsl:value-of select="//ContractNumber"/>
                <xsl:value-of select="' '"/>
                <xsl:value-of select="//com.csc_AgentName "/>
                <BR></BR>
                

              </TD>
              <td></td>
              <TD >
                <B>
                  <u>Policy Cancellation Date: </u>
                </B>
                <xsl:value-of select="//com.csc_CancellationDt"/>

                <BR></BR>

              </TD>
            </tr>
            <tr>
              <td>

                <B>
                  <u>Location Company: </u>
                </B>
                <xsl:value-of select="//com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId"/>
                <BR></BR>

              </td>
              <td></td>
              <td>
                <B>
                  <u>Master  Company: </u>
                </B>
                <xsl:value-of select="//com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId"/>
                <BR></BR>

              </td>
            </tr>
            
            <tr>

              <td>
              <B>
                <u>Policy State : </u>
              </B>
              <xsl:value-of select="//StateProvCd"/>
                <BR></BR>
              </td>
              <td></td>
              <td>
                <B>
                  <u>Line Of Business :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd"/>


                <BR></BR>
                

              </td>

            </tr>
            <tr>

              <td>
                <B>
                  <u>Issue Description: </u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_IssueCodeDesc"/>


                <BR></BR>
                

              </td>
              <td></td>
              <td>
                <B>
                  <u>Customer No :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/CustPermId"/>


                <BR></BR>
                

              </td>

            </tr>
            <tr>

              <td>
                <B>
                  <u>Insured Name : </u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>


                <BR></BR>
                

              </td>
              <td></td>
              <td>
                <B>
                  <u>Sort Name :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/NickName"/>


                <BR></BR>
               

              </td>

            </tr>
            <tr>
              <td>
                <B>
                  <u>Insured Tax ID :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity"/>

                <BR></BR>


              </td>
              <td></td>
              <td>
                <B>
                  <u>Insured Birth Date :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/BirthDt"/>

                <BR></BR>


              </td>
            </tr>
            <tr>

              <td>
                <B>
                  <u>Address : </u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1"/>
                <xsl:value-of select="' '"/>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2"/>

                <BR></BR>
                

              </td>
              <td></td>
              <td>
                <B>
                  <u>City/State/Postal Code :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/City"/>
                <xsl:value-of select="' '"/>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd"/>
                <xsl:value-of select="' '"/>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode"/>
                <BR></BR>
                

              </td>

            </tr>
            <tr>

              <td>
                <B>
                  <u>Premium : </u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_AgentPremium"/>


                <BR></BR>
                

              </td>
              <td></td>
              <td>
                <B>
                  <u>Branch : </u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_BranchCd"/>


                <BR></BR>
                

              </td>
             

            </tr>
            <tr>
              <td>
                <B>
                  <u>Profit Center : </u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ProfitCenter"/>

                <BR></BR>


              </td>
              <td></td>
              <td>
                <B>
                  <u>Name Type : </u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/NameType"/>
                <BR></BR>
              </td>
            </tr>
            <tr>

              <td>
                <B>
                  <u>Policy Company : </u>
                </B>
                <xsl:value-of select="//com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId"/>
                


                <BR></BR>
                

              </td>
              <td></td>
              <td>
                <B>
                  <u>Audit Frequency :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/City"/>
                
                <BR></BR>
                

              </td>

            </tr>
            <tr>

              <td>
                <B>
                  <u>Pay Plan : </u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_PayByModeDesc"/>


                <BR></BR>
                

              </td>
              <td></td>
              <td>
                <B>
                  <u>Renewal Option :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_RenewalCd"/>

                <BR></BR>


              </td>

            </tr>
           
            
            <tr>

              <td>
                <B>
                  <u>Renewal Of Policy : </u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_RenewPayByDesc"/>


                <BR></BR>
                

              </td>
              <td></td>
              <td>
                <B>
                  <u>Reason Amended:</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ReasonAmdDesc"/>

                <BR></BR>
               

              </td>

            </tr>
            <tr>
              <td>
                <B>
                  <u>Insurer Name :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>

                <BR></BR>


              </td>
              <td></td>
              <td>
                <B>
                  <u>Insurer Address :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/Addr/Addr1"/>

                <BR></BR>


              </td>
            </tr>
            <tr>
              <td>
                <B>
                  <u>Insurer City :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/Addr/City"/>

                <BR></BR>


              </td>
              <td></td>
              <td>
                <B>
                  <u>Insurer Postal Code :</u>
                </B>
                <xsl:value-of select="ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/Addr/PostalCode"/>

                <BR></BR>


              </td>
            </tr>
          </TABLE>
        </FONT>
     
  </xsl:template>
</xsl:stylesheet>
