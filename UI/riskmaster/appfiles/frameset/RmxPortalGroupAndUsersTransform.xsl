<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:variable name="NodesUniqueGroups" select="//Users[not(preceding-sibling::Users/GROUP_NAME=GROUP_NAME) ]/GROUP_NAME"/>
		<GroupAndUser name="Group And Users">
			<xsl:for-each select="$NodesUniqueGroups">
				<xsl:sort select="."/>
				<Group>
					<xsl:attribute name="name"><xsl:value-of select="."/></xsl:attribute>
					<xsl:variable name="sUniqueGroupName" select="."/>
					<xsl:for-each select="//Users[GROUP_NAME=$sUniqueGroupName]/LOGIN_NAME">
						<xsl:sort select="."/>
						<User>
							<xsl:attribute name="login_name"><xsl:value-of select="."/></xsl:attribute>
							<xsl:attribute name="user_id"><xsl:value-of select="../USER_ID"/></xsl:attribute>
						</User>
					</xsl:for-each>
				</Group>
			</xsl:for-each>
			<!--for users not beloging to any groups-->
			<xsl:for-each select="//Users[not(GROUP_NAME)]/LOGIN_NAME">
				<xsl:sort select="."/>
				<User>
					<xsl:attribute name="login_name"><xsl:value-of select="."/></xsl:attribute>
					<xsl:attribute name="user_id"><xsl:value-of select="../USER_ID"/></xsl:attribute>
				</User>
			</xsl:for-each>
		</GroupAndUser>
	</xsl:template>
</xsl:stylesheet>