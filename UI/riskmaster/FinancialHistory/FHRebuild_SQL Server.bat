echo off

REM ***************************************************
REM Mandatory parameters:
REM -d<database type> : database type will be sqlsvr
REM -s<server name> : server name
REM -n<database name> : database DSN name
REM -u<SQL login ID> : user/login id
REM -p<password> : password

REM Optional Parameters:
REM -verbose : add this for interactive log at the command window
REM -log : to generate a log file in the user's temp directory
REM -zerobasedfinhist : to create zero based financial history
REM -full : to recreate financial history instead of update financial history
REM zerobasedcrit=<claim/event>: use 'zerobasedcrit=claim' if DATE_OF_CLAIM is to be used for zero based fin. hist records.
REM				 use 'zerobasedcrit=event' if DATE_OF_EVENT is to be used for zero based fin. hist. records.
REM ****************************************************

finhistrebuild -dsqlsvr -s(SERVERNAME) -n(DATABASENAME) -u(USERNAME) -p(PASSWORD) (-sqlverbose) (-sqllog) (-sqlzerobasedfinhist) (-sqlfull) zerobasedcrit=(BASIS)
if errorlevel 200 goto 200
if errorlevel 100 goto 100
if errorlevel 2 goto 2
if errorlevel 1 goto 1
echo Exit Code 0
goto Done

:200
echo Exit Code 200 - Full rebuild required.
goto done

:100
echo Exit Code 100 - Generic FH DLL Error
goto Done

:2
echo Exit Code 2 - FINHIST.DLL not registered.
goto Done

:1
echo Exit Code 1 - Parameter Error
goto Done

:Done
echo Complete
