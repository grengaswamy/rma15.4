strPolicyFolderName = InputBox( "Please enter Policy Folder name(Logs wil be created here):","Policy Upload Configuration")
If Len(strPolicyFolderName) > 0 Then
	Set filesys = CreateObject("Scripting.FileSystemObject")
	varPathCurrent = filesys.GetParentFolderName(WScript.ScriptFullName)
	folderpath = varPathCurrent & "\" & strPolicyFolderName
	If filesys.FolderExists(folderpath)Then
		MsgBox "The Folder already exists"
	Else
		filesys.CreateFolder folderpath 
	End if
	Set filesys = Nothing
End If

strRMAdsn = InputBox( "Please enter RMA dsn name(Data source used in RMA):","Policy Upload Configuration")
If Len(strRMAdsn) > 0 Then
	Set filesys = CreateObject("Scripting.FileSystemObject")
	varPathCurrent = filesys.GetParentFolderName(WScript.ScriptFullName)
	strTemplateFile = varPathCurrent & "\R6SP1_OLD.dat"
	strIniFilepath = varPathCurrent & "\" & strRMAdsn & ".dat"
If filesys.FileExists(strIniFilepath)Then
Else
	filesys.CopyFile strTemplateFile,strIniFilepath
End If

strSection = strPolicyFolderName
  If Len(strSection) > 0 Then
	strRelease = InputBox( "Please enter the release version","Policy Upload Configuration")
	If Len(strRelease) > 0 Then
		WriteIni strIniFilepath,strSection,"Release",strRelease
	End If
	strADJ = InputBox( "Please enter numeric values for setting ADJASSIGN values 1 - CLAIMANT.EXAMINER  2 - CLAIM.EXAMINER 3 - CLAIM ","Policy Upload Configuration")
	If strADJ = 1 Then
 		WriteIni strIniFilepath,strSection,"ADJASSIGN","CLAIMANT.EXAMINER"
	End If
	If strADJ = 2 Then
 		WriteIni strIniFilepath,strSection,"ADJASSIGN","CLAIM.EXAMINER"
	End If
	If strADJ = 3 Then
 		WriteIni strIniFilepath,strSection,"ADJASSIGN","CLAIM"
	End If
	' strUPD = InputBox( "Please enter numeric values for setting UPDATEVENDOR values 1 - UPDATE  2 - ADD ","Policy Upload Configuration")
' 	If strUPD = 1 Then
'  		WriteIni strIniFilepath,strSection,"UPDATEVENDOR","UPDATE"
' 	End If
' 	If strUPD = 2 Then
'  		WriteIni strIniFilepath,strSection,"UPDATEVENDOR","ADD"
' 	End If
	strDataDSN = InputBox( "Please enter Data DSN name:","Policy Upload Configuration")
  	If Len(strDataDSN) > 0 Then	
  		strDataDSNUser = InputBox( "Please enter Data DSN user:","Policy Upload Configuration") 
  		If Len(strDataDSNUser) > 0 Then	
  			strDataDSNPass = InputBox( "Please enter Data DSN password:","Policy Upload Configuration") 
  			If Len(strDataDSNPass) > 0 Then
  				strDataDSNServer = InputBox( "Please enter Data DSN server:","Policy Upload Configuration") 
  				If Len(strDataDSNServer) > 0 Then
  					strDataDSNLibrary = InputBox( "Please enter Data DSN library list:","Policy Upload Configuration") 
  					If Len(strDataDSNLibrary) > 0 Then
  						WriteIni strIniFilepath,strSection,"DataDSN",strDataDSN
  						WriteIni strIniFilepath,strSection,"DataUser",strDataDSNUser
  						WriteIni strIniFilepath,strSection,"DataPswd",strDataDSNPass
  						If DSNExists(strDataDSN) Then
  							DeleteDSN strDataDSN
  						End If
  						CreateDSN strDataDSN,strDataDSNServer,strDataDSNLibrary
  						'Section Starts for Support DSN
  						intAnswer = Msgbox("Do you want to use same credentials for Support DSN",vbYesNo, "Policy Upload Configuration")

						If intAnswer = vbYes Then
    						WriteIni strIniFilepath,strSection,"SupportDSN",strDataDSN
	  						WriteIni strIniFilepath,strSection,"SupportUser",strDataDSNUser
	  						WriteIni strIniFilepath,strSection,"SupportPswd",strDataDSNPass
						Else
							strSupportDSN = InputBox( "Please enter Support DSN name:","Policy Upload Configuration")
						  	If Len(strSupportDSN) > 0 Then	
									strSupportDSNUser = InputBox( "Please enter Support DSN user:","Policy Upload Configuration") 
							  		If Len(strSupportDSNUser) > 0 Then	
						  				strSupportDSNPass = InputBox( "Please enter Support DSN password:","Policy Upload Configuration") 
							  			If Len(strSupportDSNPass) > 0 Then
						  					strSupportDSNServer = InputBox( "Please enter Support DSN server:","Policy Upload Configuration") 
						  					If Len(strSupportDSNServer) > 0 Then
						  						strSupportDSNLibrary = InputBox( "Please enter Support DSN library list:","Policy Upload Configuration") 
						  						If Len(strSupportDSNLibrary) > 0 Then
  													WriteIni strIniFilepath,strSection,"SupportDSN",strSupportDSN
						  							WriteIni strIniFilepath,strSection,"SupportUser",strSupportDSNUser
						  							WriteIni strIniFilepath,strSection,"SupportPswd",strSupportDSNPass
						  							If DSNExists(strSupportDSN) Then
  														DeleteDSN strSupportDSN
  													End If
						  							CreateDSN strSupportDSN,strSupportDSNServer,strSupportDSNLibrary
												Else
    												MsgBox "You must enter a valid Support DSN library list"
												End If
											Else 
												MsgBox "You must enter a valid Support DSN server"				
											End If
										Else 
											MsgBox "You must enter a valid Support DSN password"
										End If
									Else 
										MsgBox "You must enter a valid Support DSN user"
									End If
								Else 
									MsgBox "You must enter a valid Support DSN name"
								End If
						End If
						'Section Ends for Support DSN
						
						'Section Starts for Vendor DSN
						
						' intAnswer2 = Msgbox("Do you want to use same credentials for Vendor DSN",vbYesNo, "Policy Upload Configuration")
' 
' 						If intAnswer2 = vbYes Then
'     						WriteIni strIniFilepath,strSection,"VendorDSN",strDataDSN
' 	  						WriteIni strIniFilepath,strSection,"VendorUser",strDataDSNUser
' 	  						WriteIni strIniFilepath,strSection,"VendorPswd",strDataDSNPass
' 						Else
' 							strVendorDSN = InputBox( "Please enter Vendor DSN name:","Policy Upload Configuration")
' 						  	If Len(strVendorDSN) > 0 Then	
' 									strVendorDSNUser = InputBox( "Please enter Vendor DSN user:","Policy Upload Configuration") 
' 							  		If Len(strVendorDSNUser) > 0 Then	
' 						  				strVendorDSNPass = InputBox( "Please enter Vendor DSN password:","Policy Upload Configuration") 
' 							  			If Len(strVendorDSNPass) > 0 Then
' 						  					strVendorDSNServer = InputBox( "Please enter Vendor DSN server:","Policy Upload Configuration") 
' 						  					If Len(strVendorDSNServer) > 0 Then
' 						  						strVendorDSNLibrary = InputBox( "Please enter Vendor DSN library list:","Policy Upload Configuration") 
' 						  						If Len(strVendorDSNLibrary) > 0 Then
'   													WriteIni strIniFilepath,strSection,"VendorDSN",strVendorDSN
' 						  							WriteIni strIniFilepath,strSection,"VendorUser",strVendorDSNUser
' 						  							WriteIni strIniFilepath,strSection,"VendorPswd",strVendorDSNPass
' 						  							If DSNExists(strVendorDSN) Then
'   														DeleteDSN strVendorDSN
'   													End If
' 						  							CreateDSN strVendorDSN,strVendorDSNServer,strVendorDSNLibrary
' 												Else
'     												MsgBox "You must enter a valid Vendor DSN library list"
' 												End If
' 											Else 
' 												MsgBox "You must enter a valid Vendor DSN server"				
' 											End If
' 										Else 
' 											MsgBox "You must enter a valid Vendor DSN password"
' 										End If
' 									Else 
' 										MsgBox "You must enter a valid Vendor DSN user"
' 									End If
' 								Else 
' 									MsgBox "You must enter a valid Vendor DSN name"
' 								End If
' 						End If
						'Section Ends For Vendor DSN
						
						'Section Starts for Client DSN
						intAnswer3 = Msgbox("Do you want to use POINTCLIENT",vbYesNo, "Policy Upload Configuration")
							If intAnswer3 = vbYes Then
							WriteIni strIniFilepath,strSection,"POINTCLIENT ","YES"
							intAnswer4 = Msgbox("Do you want to use same credentials for Client DSN",vbYesNo, "Policy Upload Configuration")
			
							If intAnswer4 = vbYes Then
	    						WriteIni strIniFilepath,strSection,"ClientDSN",strDataDSN
	  							WriteIni strIniFilepath,strSection,"ClientUser",strDataDSNUser
	  							WriteIni strIniFilepath,strSection,"ClientPswd",strDataDSNPass
							Else
								strClientDSN = InputBox( "Please enter Client DSN name:","Policy Upload Configuration")
						  		If Len(strClientDSN) > 0 Then	
										strClientDSNUser = InputBox( "Please enter Client DSN user:","Policy Upload Configuration") 
							  			If Len(strClientDSNUser) > 0 Then	
						  					strClientDSNPass = InputBox( "Please enter Client DSN password:","Policy Upload Configuration") 
							  				If Len(strClientDSNPass) > 0 Then
						  						strClientDSNServer = InputBox( "Please enter Client DSN server:","Policy Upload Configuration") 
						  						If Len(strClientDSNServer) > 0 Then
						  							strClientDSNLibrary = InputBox( "Please enter Client DSN library list:","Policy Upload Configuration") 
						  							If Len(strClientDSNLibrary) > 0 Then
  														WriteIni strIniFilepath,strSection,"ClientDSN",strClientDSN
						  								WriteIni strIniFilepath,strSection,"ClientUser",strClientDSNUser
						  								WriteIni strIniFilepath,strSection,"ClientPswd",strClientDSNPass
						  								If DSNExists(strClientDSN) Then
  															DeleteDSN strClientDSN
  														End If
						  								CreateDSN strClientDSN,strClientDSNServer,strClientDSNLibrary
													Else
    													MsgBox "You must enter a valid Client DSN library list"
													End If
												Else 
													MsgBox "You must enter a valid Client DSN server"				
												End If
											Else 
												MsgBox "You must enter a valid Client DSN password"
											End If
										Else 
											MsgBox "You must enter a valid Client DSN user"
										End If
									Else 
										MsgBox "You must enter a valid Client DSN name"
									End If
							End If
						Else
							WriteIni strIniFilepath,strSection,"POINTCLIENT ","NO"
						End If
						'Section Ends For Client DSN
						
  					Else
  						MsgBox "You must enter a valid Data DSN library list"	
  					End If
  				Else 
  					MsgBox "You must enter a valid Data DSN server"	
  				End If
  			Else
  				MsgBox "You must enter a valid Data DSN password"
  			End If	
  		Else
  			MsgBox "You must enter a valid Data DSN user"
  		End If	
  	Else
  		MsgBox "You must enter a valid Data DSN name"
  	End IF
  Else
  	MsgBox "You must enter a valid Section name"
  End If
Else 
  MsgBox "You must enter a valid RMA dsn name"
End If

Set oFSO = CreateObject("Scripting.FileSystemObject")
Set oShell = CreateObject("WScript.Shell")
sSysDir = oFSO.GetSpecialFolder(1).Path
If Right(sSysDir,1) <> "\" Then sSysDir = sSysDir & "\"
Calcds = sSysDir & "cacls.exe" 
FoldPerm = """" & Calcds &"""" & """" & strIniFilepath & """" & " /E /G " & """Network Service""" & ":C" 
oShell.Run FoldPerm, 1 ,True

Sub WriteIni( myFilePath, mySection, myKey, myValue )
    Const ForReading   = 1
    Const ForWriting   = 2
    Const ForAppending = 8

    Dim blnInSection, blnKeyExists, blnSectionExists, blnWritten
    Dim intEqualPos
    Dim objFSO, objNewIni, objOrgIni, wshShell
    Dim strFilePath, strFolderPath, strKey, strLeftString
    Dim strLine, strSection, strTempDir, strTempFile, strValue

    strFilePath = Trim( myFilePath )
    strSection  = Trim( mySection )
    strKey      = Trim( myKey )
    strValue    = Trim( myValue )

    Set objFSO   = CreateObject( "Scripting.FileSystemObject" )
    Set wshShell = CreateObject( "WScript.Shell" )

    strTempDir  = wshShell.ExpandEnvironmentStrings( "%TEMP%" )
    strTempFile = objFSO.BuildPath( strTempDir, objFSO.GetTempName )

    Set objOrgIni = objFSO.OpenTextFile( strFilePath, ForReading, True )
    Set objNewIni = objFSO.CreateTextFile( strTempFile, False, False )

    blnInSection     = False
    blnSectionExists = False
    ' Check if the specified key already exists
    blnKeyExists     = ( ReadIni( strFilePath, strSection, strKey ) <> "" )
    blnWritten       = False

    ' Check if path to INI file exists, quit if not
    strFolderPath = Mid( strFilePath, 1, InStrRev( strFilePath, "\" ) )
    If Not objFSO.FolderExists ( strFolderPath ) Then
        WScript.Echo "Error: WriteIni failed, folder path (" _
                   & strFolderPath & ") to ini file " _
                   & strFilePath & " not found!"
        Set objOrgIni = Nothing
        Set objNewIni = Nothing
        Set objFSO    = Nothing
        WScript.Quit 1
    End If

    While objOrgIni.AtEndOfStream = False
        strLine = Trim( objOrgIni.ReadLine )
        If blnWritten = False Then
            If LCase( strLine ) = "[" & LCase( strSection ) & "]" Then
                blnSectionExists = True
                blnInSection = True
            ElseIf InStr( strLine, "[" ) = 1 Then
                blnInSection = False
            End If
        End If

        If blnInSection Then
            If blnKeyExists Then
                intEqualPos = InStr( 1, strLine, "=", vbTextCompare )
                If intEqualPos > 0 Then
                    strLeftString = Trim( Left( strLine, intEqualPos - 1 ) )
                    If LCase( strLeftString ) = LCase( strKey ) Then
                        ' Only write the key if the value isn't empty
                        ' Modification by Johan Pol
                        If strValue <> "<DELETE_THIS_VALUE>" Then
                            objNewIni.WriteLine strKey & " = " & strValue
                        End If
                        blnWritten   = True
                        blnInSection = False
                    End If
                End If
                If Not blnWritten Then
                    objNewIni.WriteLine strLine
                End If
            Else
                objNewIni.WriteLine strLine
                    ' Only write the key if the value isn't empty
                    ' Modification by Johan Pol
                    If strValue <> "<DELETE_THIS_VALUE>" Then
                        objNewIni.WriteLine strKey & "=" & strValue
                    End If
                blnWritten   = True
                blnInSection = False
            End If
        Else
            objNewIni.WriteLine strLine
        End If
    Wend

    If blnSectionExists = False Then ' section doesn't exist
        objNewIni.WriteLine
        objNewIni.WriteLine "[" & strSection & "]"
            ' Only write the key if the value isn't empty
            ' Modification by Johan Pol
            If strValue <> "<DELETE_THIS_VALUE>" Then
                objNewIni.WriteLine strKey & "=" & strValue
            End If
    End If

    objOrgIni.Close
    objNewIni.Close

    ' Delete old INI file
    objFSO.DeleteFile strFilePath, True
    ' Rename new INI file
    objFSO.MoveFile strTempFile, strFilePath

    Set objOrgIni = Nothing
    Set objNewIni = Nothing
    Set objFSO    = Nothing
    Set wshShell  = Nothing
End Sub
Function ReadIni( myFilePath, mySection, myKey )
    
    Const ForReading   = 1
    Const ForWriting   = 2
    Const ForAppending = 8

    Dim intEqualPos
    Dim objFSO, objIniFile
    Dim strFilePath, strKey, strLeftString, strLine, strSection

    Set objFSO = CreateObject( "Scripting.FileSystemObject" )

    ReadIni     = ""
    strFilePath = Trim( myFilePath )
    strSection  = Trim( mySection )
    strKey      = Trim( myKey )

    If objFSO.FileExists( strFilePath ) Then
        Set objIniFile = objFSO.OpenTextFile( strFilePath, ForReading, False )
        Do While objIniFile.AtEndOfStream = False
            strLine = Trim( objIniFile.ReadLine )

            ' Check if section is found in the current line
            If LCase( strLine ) = "[" & LCase( strSection ) & "]" Then
                strLine = Trim( objIniFile.ReadLine )

                ' Parse lines until the next section is reached
                Do While Left( strLine, 1 ) <> "["
                    ' Find position of equal sign in the line
                    intEqualPos = InStr( 1, strLine, "=", 1 )
                    If intEqualPos > 0 Then
                        strLeftString = Trim( Left( strLine, intEqualPos - 1 ) )
                        ' Check if item is found in the current line
                        If LCase( strLeftString ) = LCase( strKey ) Then
                            ReadIni = Trim( Mid( strLine, intEqualPos + 1 ) )
                            ' In case the item exists but value is blank
                            If ReadIni = "" Then
                                ReadIni = " "
                            End If
                            ' Abort loop when item is found
                            Exit Do
                        End If
                    End If

                    ' Abort if the end of the INI file is reached
                    If objIniFile.AtEndOfStream Then Exit Do

                    ' Continue with next line
                    strLine = Trim( objIniFile.ReadLine )
                Loop
            Exit Do
            End If
        Loop
        objIniFile.Close
    Else
        WScript.Echo strFilePath & " doesn't exists. Exiting..."
        Wscript.Quit 1
    End If
End Function
Sub CreateDSN(myDSN,mySystem,myLibrary)
	Const HKEY_LOCAL_MACHINE = &H80000002
	strComputer = "."
	strDSN = myDSN
	strSystem = mySystem
	strLibrary = strDSN & " " & myLibrary
	Dim WshShell
	Dim OsType
	Set WshShell = CreateObject("WScript.Shell")
	OsType = WshShell.RegRead("HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment\PROCESSOR_ARCHITECTURE")
	
	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _ 
	    strComputer & "\root\default:StdRegProv")
 	
 	If OsType = "x86" Then
		strKeyPath = "SOFTWARE\ODBC\ODBC.INI\ODBC Data Sources"
	Elseif OsType = "AMD64" Then
		strKeyPath = "SOFTWARE\Wow6432Node\ODBC\ODBC.INI\ODBC Data Sources"
	End If
	
	strValueName = myDSN
	strValue = "iSeries Access ODBC Driver"

	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	If OsType = "x86" Then
 		strKeyPath = "SOFTWARE\ODBC\ODBC.INI\" & strDSN
 	Elseif OsType = "AMD64" Then
 	 	strKeyPath = "SOFTWARE\Wow6432Node\ODBC\ODBC.INI\" & strDSN
 	End If
	objReg.CreateKey HKEY_LOCAL_MACHINE,strKeyPath

	If OsType = "x86" Then
 		strKeyPath = "SOFTWARE\ODBC\ODBC.INI\" & strDSN
 	Elseif OsType = "AMD64" Then
 	 	strKeyPath = "SOFTWARE\Wow6432Node\ODBC\ODBC.INI\" & strDSN
 	End If

	set shell = WScript.CreateObject("WScript.Shell")
	windowsdir = shell.ExpandEnvironmentStrings("%windir%")
	
	strValueName = "Driver"
	If OsType = "x86" Then
 		strValue = windowsdir & "\system32\cwbodbc.dll"
 	Elseif OsType = "AMD64" Then
 	 	strValue = windowsdir & "\SysWOW64\cwbodbc.dll"
 	End If
	
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "Description"
	strValue = ""
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "System"
	strValue = strSystem
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "Signon"
	strValue = "3"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "UserID"
	strValue = ""
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "SSL"
	strValue = "2"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "SQLConnectPromptMode"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "Naming"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "CommitMode"
	strValue = "2"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "DefaultLibraries"
	strValue = strLibrary
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "Database"
	strValue = ""
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "ConnectionType"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "AllowProcCalls"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "DateFormat"
	strValue = "5"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "DateSeparator"
	strValue = "1"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "TimeFormat"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "TimeSeparator"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "Decimal"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "MaximumDecimalPrecision"
	strValue = "31"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "MaximumDecimalScale"
	strValue = "31"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "MinimumDivideScale"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "ConvertDateTimeToChar"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "Graphic"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "DecFloatRoundMode"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "MapDecimalFloatDescribe"
	strValue = "1"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "ExtendedDynamic"
	strValue = "1"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "DefaultPkgLibrary"
	strValue = "QGPL"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "DefaultPackage"
	strValue = "QGPL/DEFAULT(IBM),2,0,1,0,512"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "LazyClose"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "PreFetch"
	strValue = "1"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "AllowDataCompression"
	strValue = "1"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "Concurrency"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "ExtendedColInfo"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "QueryTimeout"
	strValue = "1"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "BlockSizeKB"
	strValue = "256"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "MaxFieldLength"
	strValue = "32"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "BlockFetch"
	strValue = "1"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "CursorSensitivity"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "QueryOptimizeGoal"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "QueryStorageLimit"
	strValue = "-1"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "TrueAutoCommit"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "SortSequence"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "SortTable"
	strValue = ""
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "SortWeight"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "LanguageID"
	strValue = "ENU"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "LibraryView"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "ODBCRemarks"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "SearchPattern"
	strValue = "1"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "CatalogOptions"
	strValue = "3"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "DelimitNames"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "ForceTranslation"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "TranslationDLL"
	strValue = ""
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "TranslationOption"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "CCSID"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "AllowUnsupportedChar"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	strValueName = "UnicodeSQL"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "HexParserOpt"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "Trace"
	strValue = "0"
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "QAQQINILibrary"
	strValue = ""
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue

	strValueName = "SQDiagCode"
	strValue = ""
	objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
	If OsType = "AMD64" Then
		strValueName = "PackageCCPPCPOINT"
		strValue = "QGPL/CCPPCP(AFBA),2,0,1,0,512"
		objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue
	
		strValueName = "PackageAS400QUERY"
		strValue = "QGPL/AS400Q(AFBA),2,0,1,0,512"
		objReg.SetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strValue	
	End if
	
End Sub

Function DSNExists(strValueName) 
      Dim strComputer: strComputer = "." 
      Dim objReg: Set objReg = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & _ 
          "\root\default:StdRegProv") 
      Dim strKeyPath
      Dim WshShell
	  Dim OsType
	  Set WshShell = CreateObject("WScript.Shell")
	  OsType = WshShell.RegRead("HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment\PROCESSOR_ARCHITECTURE")
	  If OsType = "x86" Then
		strKeyPath = "SOFTWARE\ODBC\ODBC.INI\ODBC Data Sources" 
	  elseif OsType = "AMD64" Then
	  	strKeyPath = "SOFTWARE\Wow6432Node\ODBC\ODBC.INI\ODBC Data Sources" 
	  end if
       
      Const HKEY_LOCAL_MACHINE = &H80000002 
      Dim strDWValue 
      objReg.GetStringValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName,strDWValue 
      If strDWValue <> "" Then 
            DSNExists = vbTrue 
      Else  
            DSNExists = VBFalse 
      End If 
End Function 

Sub DeleteDSN(DSNName)
	Const HKEY_LOCAL_MACHINE = &H80000002
	strComputer = "."
	Dim WshShell
	Dim OsType
	Set WshShell = CreateObject("WScript.Shell")
	OsType = WshShell.RegRead("HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment\PROCESSOR_ARCHITECTURE")
	
	Set objReg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & _ 
    strComputer & "\root\default:StdRegProv")
 	If OsType = "x86" Then
		strKeyPath = "SOFTWARE\ODBC\ODBC.INI\" & DSNName	
	Elseif OsType = "AMD64" Then
		strKeyPath = "SOFTWARE\Wow6432Node\ODBC\ODBC.INI\" & DSNName
	End If
	
	objReg.DeleteKey HKEY_LOCAL_MACHINE, strKeyPath
	If OsType = "x86" Then
		strKeyPath = "SOFTWARE\ODBC\ODBC.INI\ODBC Data Sources"
	Elseif OsType = "AMD64" Then
		strKeyPath = "SOFTWARE\Wow6432Node\ODBC\ODBC.INI\ODBC Data Sources"
	End if
	strValueName = DSNName
	objReg.DeleteValue HKEY_LOCAL_MACHINE,strKeyPath,strValueName
End Sub
