<html>
<head>    
    <link href="images/default.css" type="text/css" rel="stylesheet">
    <link href="images/skins.css" type="text/css" rel="stylesheet">    
</head>
<script language="JavaScript">
var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
var oXMLDoc=new ActiveXObject("Microsoft.XMLDOM");
function LoadHomePage()
{ 	
	var objHomeFrame = document.getElementById ("HomeFrame");   
	ReSizeiFrame();
	loadXML("HomePage.xml");
	try
	{    
		var doc=xmlDoc.documentElement;
		if (doc !=null)
		{	 
		    objHomeFrame.src= doc.childNodes(0).text;	    
		}		
	}catch (e)
	{
		objHomeFrame.src = "htmlFiles/CSC%20Self-Insured%20Customer%20Support%20Center.htm";	    
	}
}

function loadXML(xmlFile) 
{
        xmlDoc.async="false";
        xmlDoc.onreadystatechange=verify;
        xmlDoc.load(xmlFile);
}

function verify() 
{ 
        if(xmlDoc.readyState!=4)
                return false; 
}
function ReSizeiFrame()
{
    var x,y;        
    if (self.innerHeight) // all except Explorer
    {
	    x = self.innerWidth;
	    y = self.innerHeight;
    }
    else if (document.documentElement && document.documentElement.clientHeight)
	    // Explorer 6 Strict Mode
    {
	    x = document.documentElement.clientWidth;
	    y = document.documentElement.clientHeight;
    }
    else if (document.body) // other Explorers
    {
	    x = document.body.clientWidth;
	    y = document.body.clientHeight;
    }
    
	try
    {
       var objElts = document.getElementById("HomeFrame"); 
       if(objElts.id != null)       
       {
           objElts.style.height =y-107 ;
           objElts.style.width = x-45 ;                   
       }
	 }
	 catch (e)
	 {
	 }
}


</script>

<body bgcolor="#0d4c7d" onload="LoadHomePage();" onresize="ReSizeiFrame();">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td>
                    <div>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr valign="middle">
                                    <td align="left" valign="top">
                                        <img height="54" src="images/weblogox4.gif"></td>
                                    <td style="background-repeat: repeat-x; background-position: center top;" align="center" 
                                        background="images/welcome_tile.gif" valign="top" width="100%">
                                    </td>                                    
                                    <td align="right" valign="bottom" width="30%" style="background-repeat: repeat-x; background-position: center top;" background="images/welcome_tile.gif">
                                        <form style="margin: 0pt;" method="post" action="pass-thru.jsp" enctype="application/x-www-form-urlencoded">
                                            
                                            <table cellpadding="0" cellspacing="2">
                                                <tbody>
                                                    <tr>
                                                        <td style="color: white; font-size: 10px;">
                                                            Username:</td>
                                                        <td>
                                                            <input value="" name="loginname" maxlength="25" tabindex="1" style="color: black;
                                                                font-size: 10px; width:120px;" type="text">
                                                        </td>
                                                        <td rowspan="2" align="center">
                                                            <input name="submit" value="Login" tabindex="4" style="color: black; font-size: 10px;"
                                                                type="submit">
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="color: white; font-size: 10px;">
                                                            Password:</td>
                                                        <td>
                                                            <input value="" name="password" maxlength="25" tabindex="2" style="font-size: 10px;width:120px;"
                                                                type="password">
                                                        </td>
                                                    </tr>                                                    
                                                </tbody>
                                            </table>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td>
                    <table border="0" cellpadding="2" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td valign="top" >
                                    <table border="0" cellpadding="0" cellspacing="2" width="100%">
                                        <tbody>
                                            <tr>
                                                <td width="100%">
                                                    <div class="Jetspeed">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="PTitleLeft" style="font-size: 1pt;" nowrap="true">
                                                                        &nbsp;</td>
                                                                    <td id ="HomeFrameTitle" class="PTitle" align="left" valign="middle">
                                                                    </td>
                                                                    <td class="PTitle" align="right" valign="middle">
                                                                    </td>
                                                                    <td class="PTitleRight" style="font-size: 1pt;" nowrap="true">
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="PContentLeft" style="font-size: 1pt;" nowrap="true">
                                                                        &nbsp;</td>
                                                                    <td colspan="2" class="PContent" style="background-color: rgb(255, 255, 255);">
                                                                        <iframe src="about:blank" id = "HomeFrame" name = "HomeFrame" frameborder="1" height="480" scrolling="auto" width="800" security="restricted"></iframe>
                                                                    </td>
                                                                    <td class="PContentRight" style="font-size: 1pt;" nowrap="true">
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="PBottomLeft" style="font-size: 1pt;" nowrap="true">
                                                                        &nbsp;</td>
                                                                    <td colspan="2" class="PBottom" style="font-size: 1pt;" nowrap="true">
                                                                        &nbsp;</td>
                                                                    <td class="PBottomRight" style="font-size: 1pt;" nowrap="true">
                                                                        &nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td valign="top" width="2px">
                                    <table border="0" cellpadding="0" cellspacing="2" width="100%">
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td bgcolor="#ffffff" valign="bottom">
                  
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
