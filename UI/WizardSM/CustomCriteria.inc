<!-- #include file ="criteriautils.inc" -->
<!-- #include file ="oshareport.inc" -->
<!-- #include file ="executivesummaryreport.inc" -->

<script language="VBScript" runat="server">
Public objXML, objXSL
Public Const OSHA_300_REPORT = 1

'Response.ExpiresAbsolute=DateAdd("h",-24,Now)
'Server.ScriptTimeout = 24*60*60*60 'run for a full day 24 *hr *min * sec
'Session.Timeout = 24*60 'already specified in minutes.

Function GetReportType(lReportId)
	Dim sWhere, sXML, objXML, objNode
	'Fetch this Report XML
	sWhere = "SM_REPORTS_PERM.USER_ID = " & objSessionStr("UserID") & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND SM_REPORTS.REPORT_ID = " & lReportID
  sXML = GetSingleSMValue("REPORT_XML","SM_REPORTS,SM_REPORTS_PERM",sWhere)
  If sXML = "" Then
  Response.Write "Error: Selected report could not be found on the server."
  Response.End
  end if

  'Load this Report XML
  Set objXML = CreateObject("Microsoft.XMLDOM")
  if (objXML.loadXML(sXML) = false) then
  Response.Write "Error: Selected report could not be loaded.  Please try uploading it again."
  Response.End
  end if

  'Determine what type of report this is. (And what engine to run.)
  Set objNode = objXML.selectSingleNode("/report")
  If IsEmpty(objNode) or IsNull(objNode) or objNode is nothing then
  Response.Write "Error: Selected report does not have a top level report element with the 'type' attribute.  Please try uploading it again."
  Response.End
  end if
  GetReportType = 0
  If not IsNull(objNode.getAttribute("type")) Then GetReportType = clng(objNode.getAttribute("type"))
  End Function

  Function IsChecked(sVarName)
  If Request(sVarName) = "1" then
  IsChecked = 1
  Else
  IsChecked = 0
  End If
  End Function

  Function GetReportTypeForAdmin(lReportId)
  Dim sWhere, sXML, objXML, objNode
  'Fetch this Report XML
  sWhere = "SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND SM_REPORTS.REPORT_ID = " & lReportID
  sXML = GetSingleSMValue("REPORT_XML","SM_REPORTS,SM_REPORTS_PERM",sWhere)
  If sXML = "" Then
  Response.Write "Error: Selected report could not be found on the server."
  Response.End
  end if

  'Load this Report XML
  Set objXML = CreateObject("Microsoft.XMLDOM")
  if (objXML.loadXML(sXML) = false) then
  Response.Write "Error: Selected report could not be loaded.  Please try uploading it again."
  Response.End
  end if

  'Determine what type of report this is. (And what engine to run.)
  Set objNode = objXML.selectSingleNode("/report")
  If IsEmpty(objNode) or IsNull(objNode) or objNode is nothing then
  Response.Write "Error: Selected report does not have a top level report element with the 'type' attribute.  Please try uploading it again."
  Response.End
  end if
  GetReportTypeForAdmin = 0
  If not IsNull(objNode.getAttribute("type")) Then GetReportTypeForAdmin = clng(objNode.getAttribute("type"))
  End Function

  Function IsChecked(sVarName)
  If Request(sVarName) = "1" then
  IsChecked = 1
  Else
  IsChecked = 0
  End If
  End Function

  'Sortmaster reports WILL come through this function since the
  'SMI object only generates the criteria section.
  Function SaveScheduledReport(sUserName, lUserID, sConnectString, sSMConnectString, sOutputPath, sOutputPathURL, errorHTML)

  Dim lReportType
  SaveScheduledReport = false
  lReportType = GetReportType(CLng(Request("reportid")))
  If lReportType = 0 then
  Set objSMI = CreateObject("SMInterface.SMI")
  If objSMI.SaveScheduledReport (objSessionStr(SESSION_LOGINNAME), objSessionStr(SESSION_USERID), objSessionStr(SESSION_DSN), Application(APP_SMDSN), objSessionStr(SESSION_DOCPATH), sOutputPathURL) Then
  SaveScheduledReport = true
  Exit Function
  End If
  End If
  Dim sWhere 'as string
  Dim sJobName 'As String
  Dim sJobDesc 'As String
  Dim sOutputType 'As String
  Dim sStartDate 'As String
  Dim sStartTime 'As String
  Dim sNotificationType 'As String
  Dim sNotifyEmail 'As String
  Dim sNotifyMsg 'As String
  Dim sXML 'As String
  Dim rs 'As Integer
  Dim lReportId 'As Long
  Dim sReportName 'As String
  Dim lScheduleType 'As Long
  Dim sNextRun 'As String
  Dim lScheduleId 'As Long
  'Start: Add by kuladeep for update mits:27574
  Dim sXMLRpt 'As String
  Dim lReportTypeId 'As String 0- for old/1- for new report


  lScheduleId = 0
  If not IsEmpty(Request("scheduleid")) then lScheduleId = Clng(Request("scheduleid"))
  lReportId = 0
  If not IsEmpty(Request("reportid")) then lReportId = Clng(Request("reportid"))
  If lReportId = 0 Then Exit Function
  If not IsEmpty(Request("scheduletype")) then lScheduleType = Clng(Request("scheduletype"))
  If lScheduleType = 0 Then lScheduleType = 1

  'Pick up the ReportName
  OpenSMDatabase
  sWhere = "SM_REPORTS.REPORT_ID = " & lReportId & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND SM_REPORTS_PERM.USER_ID = " & lUserID
  	sReportName = GetSingleSMValue("REPORT_NAME","SM_REPORTS,SM_REPORTS_PERM",sWhere)
  	
  	' Let Report engine validate the user entered criteria
	If (Request("editscheduleonly") & "") = "" Then
  If Not ApplyCriteria(sXML, errorHTML) Then
  CloseSMDatabase
  Exit Function
  Else
  ApplyPostedXMLJobParams sXML
  End if
  End If

  'Start: Add by kuladeep for update mits:27574
  If (Request("editscheduleonly") & "") = "1" Then
  lReportTypeId=0
  rs = objSMRocket.DB_CreateRecordset(hSMDb, "SELECT * FROM SM_SCHEDULE WHERE SCHEDULE_ID=" & lScheduleId, 3, 0)

  If Not objSMRocket.DB_EOF(rs) Then
  objSMRocket.DB_GetData rs,"REPORT_XML", v
  sXMLRpt = v
  End if

  Dim sPrivaqcyRptDom
  Set sPrivaqcyRptDom = CreateObject("Microsoft.XMLDOM")
  sPrivaqcyRptDom.loadxml(sXMLRpt)
  For Each RptDataNode In sPrivaqcyRptDom.selectNodes("/report/form/group")
  If(RptDataNode.getAttribute("title") = "OSHA Information [By OSHA Establishment]") Then

  If(RptDataNode.selectNodes("control").[0].getAttribute("id")="byoshaestablishmentflag1") Then
  lReportTypeId = 1
  End If
  End If
  Next


  If(lReportTypeId=1) Then
  If Not ApplyCriteria(sXML, errorHTML) Then
  CloseSMDatabase
  Exit Function
  Else
  ApplyPostedXMLJobParams sXML
  End if
  End If

  End if

  'End: Add by kuladeep for update mits:27574


  ' Get job parameters
  sJobName = Request("jobname") & ""
	sJobDesc = Request("jobdesc") & ""
	If sJobName = "" Then
		sJobName = sReportName & "-" & sUserName & "-" & Year(Now) & PadNumeric(Month(Now)) & PadNumeric(Day(Now))& PadNumeric(Hour(Now)) & PadNumeric(Minute(Now)) & PadNumeric(Second(Now))'  FormatDateTime("YYYYMMDDHHMMSS",Now)
	End If
	If sJobDesc = "" Then
		sJobDesc = "Report " & sReportName & " submitted on " & Date & " at " & Time & " by user " & sUserName & "."
	End If
		
	sOutputType = Request("outputtype") & ""
		
	sNotificationType = Request("notifytype") & ""
	If sNotificationType <> "notifyonly" And sNotificationType <> "notifylink" And _
		sNotificationType <> "notifyembed" And sNotificationType <> "none" Then
		sNotificationType = "none"
	End If
		
	sNotifyEmail = Request("notifyemail") & ""
	sNotifyMsg = Request("notifymsg") & ""
		
	' Start time
	sStartTime = Request("starttime")
	If sStartTime <> "" And IsDate(sStartTime) Then
		sStartTime = GetDBTime(sStartTime) ' PadNumeric(Hour(CDate(sStartTime))) & PadNumeric(Minute(CDate(sStartTime))) & PadNumeric(Second(CDate(sStartTime))) 'Format$(CDate(sStartTime), "hhnnss")
	Else
		sStartTime = "010000"
	End If
		
	' Start Date
	sStartDate = Request("startdate")
	If sStartDate <> "" And IsDate(sStartDate) Then
		sStartDate = GetDBDate(sStartDate) 'Year(CDate(sStartDate)) & PadNumeric(Month(CDate(sStartDate))) & PadNumeric(Day(CDate(sStartDate)))	'Format$(CDate(sStartDate), "yyyymmdd")
	Else
		sStartDate = GetDBDate(Now) 'Year(Now) & PadNumeric(Month(Now)) & PadNumeric(Day(Now)) ' Format$(Date, "yyyymmdd")
	End If
	
	' Calculate Next Run Date
	Dim v, lIndex 'As Long
	v = DateSerial(Left(sStartDate, 4), Mid(sStartDate, 5, 2), Mid(sStartDate, 7, 2))
	sNextRun = GetDBDate(v) 'Format$(v, "yyyymmdd")

	If lScheduleType = 1 Then
		For lIndex = 1 To 7
			If (Weekday(v) = 2 And Request("MON_RUN") = "1") Or (Weekday(v) = 3 And Request("TUE_RUN") = "1") Or (Weekday(v) = 4 And Request("WED_RUN") = "1") Or (Weekday(v) = 5 And Request("THU_RUN") = "1") Or (Weekday(v) = 6 And Request("FRI_RUN") = "1") Or (Weekday(v) = 7 And Request("SAT_RUN") = "1") Or (Weekday(v) = 1 And Request("SUN_RUN") = "1") Then
				sNextRun = GetDBDate(v) 'Format$(v, "yyyymmdd")
				Exit For
			End If
			v = DateAdd("d", 1, v)
		Next
	Else
		On Error Resume Next
		v = DateSerial(Year(v), Month(v), Request("startdate"))
		If Err.Number > 0 Then
			v = DateSerial(Left(sStartDate, 4), Mid(sStartDate, 5, 2), Mid(sStartDate, 7, 2))
		Else
			If Date > v Then v = DateAdd("m", 1, v)
		End If
		On Error GoTo 0
		For lIndex = 1 To 12
			If (Month(v) = 1 And Request("JAN_RUN") = "1") Or (Month(v) = 2 And Request("FEB_RUN") = "1") Or (Month(v) = 3 And Request("MAR_RUN") = "1") Or (Month(v) = 4 And Request("APR_RUN") = "1") Or (Month(v) = 5 And Request("MAY_RUN") = "1") Or (Month(v) = 6 And Request("JUN_RUN") = "1") Then
				sNextRun = GetDBDate(v) 'Format$(v, "yyyymmdd")
				Exit For
			ElseIf (Month(v) = 7 And Request("JUL_RUN") = "1") Or (Month(v) = 8 And Request("AUG_RUN") = "1") Or (Month(v) = 9 And Request("SEP_RUN") = "1") Or (Month(v) = 10 And Request("OCT_RUN") = "1") Or (Month(v) = 11 And Request("NOV_RUN") = "1") Or (Month(v) = 12 And Request("DEC_RUN") = "1") Then
				sNextRun = GetDBDate(v) 'Format$(v, "yyyymmdd")
				Exit For
			End If
			If Date > v Then v = DateAdd("m", 1, v)
		Next
	End If
		
	If lScheduleId = 0 Then
		lScheduleId = lGetNextSMUID("SM_SCHEDULE", hSMDb)
	End If
	' Schedule Report
	rs = objSMRocket.DB_CreateRecordset(hSMDb, "SELECT * FROM SM_SCHEDULE WHERE SCHEDULE_ID=" & lScheduleId, 3, 0)

	If Not objSMRocket.DB_EOF(rs) Then 
		If  Request("editscheduleonly")& "" <> "" Then 'Update schedule only - NO REPORT CHANGES.
      'sXML = GetSMData(rs,"REPORT_XML")
      'Start:Comment by kuladeep for update the schedule mits:27574
      If lReportType = 1 or lReportType= 3 or lReportType= 4 then
      If(lReportTypeId=0) Then
      objSMRocket.DB_GetData rs,"REPORT_XML", v
      sXML = v
      End If
      else
      objSMRocket.DB_GetData rs,"REPORT_XML", v
      sXML = v
      End If
      'End:Comment by kuladeep for update the schedul mits:27574

      End If
      objSMRocket.DB_CloseRecordset Cint(rs), 1
      objSMRocket.DB_Edit rs
      Else
      objSMRocket.DB_CloseRecordset Cint(rs), 1
      objSMRocket.DB_AddNew rs
      End If

      objSMRocket.DB_PutData rs, "SCHEDULE_ID", lScheduleId
      objSMRocket.DB_PutData rs, "REPORT_ID", lReportId
      objSMRocket.DB_PutData rs, "SCHEDULE_TYPE", lScheduleType
      objSMRocket.DB_PutData rs, "NEXT_RUN_DATE", sNextRun
      objSMRocket.DB_PutData rs, "START_DATE", sNextRun
      objSMRocket.DB_PutData rs, "START_TIME", sStartTime
      If lScheduleType = 1 Then
      objSMRocket.DB_PutData rs, "MON_RUN", IsChecked("MON_RUN")
      objSMRocket.DB_PutData rs, "TUE_RUN", IsChecked("TUE_RUN")
      objSMRocket.DB_PutData rs, "WED_RUN", IsChecked("WED_RUN")
      objSMRocket.DB_PutData rs, "THU_RUN", IsChecked("THU_RUN")
      objSMRocket.DB_PutData rs, "FRI_RUN", IsChecked("FRI_RUN")
      objSMRocket.DB_PutData rs, "SAT_RUN", IsChecked("SAT_RUN")
      objSMRocket.DB_PutData rs, "SUN_RUN", IsChecked("SUN_RUN")
      Else
      Dim lDay
      lDay = 1
      If not IsEmpty(Request("startday")) Then lDay = Clng(Request("startday"))
      objSMRocket.DB_PutData rs, "DAYOFMONTH_RUN", lDay
      objSMRocket.DB_PutData rs, "JAN_RUN", IsChecked("JAN_RUN")
      objSMRocket.DB_PutData rs, "FEB_RUN", IsChecked("FEB_RUN")
      objSMRocket.DB_PutData rs, "MAR_RUN", IsChecked("MAR_RUN")
      objSMRocket.DB_PutData rs, "APR_RUN", IsChecked("APR_RUN")
      objSMRocket.DB_PutData rs, "MAY_RUN", IsChecked("MAY_RUN")
      objSMRocket.DB_PutData rs, "JUN_RUN", IsChecked("JUN_RUN")
      objSMRocket.DB_PutData rs, "JUL_RUN", IsChecked("JUL_RUN")
      objSMRocket.DB_PutData rs, "AUG_RUN", IsChecked("AUG_RUN")
      objSMRocket.DB_PutData rs, "SEP_RUN", IsChecked("SEP_RUN")
      objSMRocket.DB_PutData rs, "OCT_RUN", IsChecked("OCT_RUN")
      objSMRocket.DB_PutData rs, "NOV_RUN", IsChecked("NOV_RUN")
      objSMRocket.DB_PutData rs, "DEC_RUN", IsChecked("DEC_RUN")
      End If

      objSMRocket.DB_PutData rs, "USER_ID", lUserID
      objSMRocket.DB_PutData rs, "JOB_NAME", sJobName
      objSMRocket.DB_PutData rs, "JOB_DESC", sJobDesc
      objSMRocket.DB_PutData rs, "DSN", sConnectString
      objSMRocket.DB_PutData rs, "OUTPUT_TYPE", sOutputType
      objSMRocket.DB_PutData rs, "OUTPUT_PATH", sOutputPath
      objSMRocket.DB_PutData rs, "OUTPUT_PATH_URL", sOutputPathURL
      objSMRocket.DB_PutData rs, "REPORT_XML", sXML
      objSMRocket.DB_PutData rs, "NOTIFICATION_TYPE", sNotificationType
      objSMRocket.DB_PutData rs, "NOTIFY_EMAIL", sNotifyEmail
      objSMRocket.DB_PutData rs, "NOTIFY_MSG", sNotifyMsg

      objSMRocket.DB_Update cint(rs)

      objSMRocket.DB_CloseRecordset cint(rs), 2

      SaveScheduledReport = true
      CloseSMDatabase
      End Function

      Function SaveScheduledReportForAdmin(sUserName, lUserID, sConnectString, sSMConnectString, sOutputPath, sOutputPathURL, errorHTML)

      Dim lReportType
      SaveScheduledReportForAdmin = false
      lReportType = GetReportTypeForAdmin(CLng(Request("reportid")))
      If lReportType = 0 then
      Set objSMI = CreateObject("SMInterface.SMI")
      If objSMI.SaveScheduledReport (objSessionStr(SESSION_LOGINNAME), objSessionStr(SESSION_USERID), objSessionStr(SESSION_DSN), Application(APP_SMDSN), objSessionStr(SESSION_DOCPATH), sOutputPathURL) Then
      SaveScheduledReportForAdmin = true
      Exit Function
      End If
      End If
      Dim sWhere 'as string
      Dim sJobName 'As String
      Dim sJobDesc 'As String
      Dim sOutputType 'As String
      Dim sStartDate 'As String
      Dim sStartTime 'As String
      Dim sNotificationType 'As String
      Dim sNotifyEmail 'As String
      Dim sNotifyMsg 'As String
      Dim sXML 'As String
      Dim rs 'As Integer
      Dim lReportId 'As Long
      Dim sReportName 'As String
      Dim lScheduleType 'As Long
      Dim sNextRun 'As String
      Dim lScheduleId 'As Long
      'Start: Add by kuladeep for update mits:27574
      Dim sXMLRpt 'As String
      Dim lReportTypeId 'As String 0- for old/1- for new report


      lScheduleId = 0
      If not IsEmpty(Request("scheduleid")) then lScheduleId = Clng(Request("scheduleid"))
      lReportId = 0
      If not IsEmpty(Request("reportid")) then lReportId = Clng(Request("reportid"))
      If lReportId = 0 Then Exit Function
      If not IsEmpty(Request("scheduletype")) then lScheduleType = Clng(Request("scheduletype"))
      If lScheduleType = 0 Then lScheduleType = 1

      'Pick up the ReportName
      OpenSMDatabase
      sWhere = "SM_REPORTS.REPORT_ID = " & lReportId & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND SM_REPORTS_PERM.USER_ID = " & lUserID
      sReportName = GetSingleSMValue("REPORT_NAME","SM_REPORTS,SM_REPORTS_PERM",sWhere)

      ' Let Report engine validate the user entered criteria
      If (Request("editscheduleonly") & "") = "" Then
      If Not ApplyCriteriaForAdmin(sXML, errorHTML) Then
      CloseSMDatabase
      Exit Function
      Else
      ApplyPostedXMLJobParams sXML
      End if
      End If

      'Start: Add by kuladeep for update mits:27574
      If (Request("editscheduleonly") & "") = "1" Then
      lReportTypeId=0
      rs = objSMRocket.DB_CreateRecordset(hSMDb, "SELECT * FROM SM_SCHEDULE WHERE SCHEDULE_ID=" & lScheduleId, 3, 0)

      If Not objSMRocket.DB_EOF(rs) Then
      objSMRocket.DB_GetData rs,"REPORT_XML", v
      sXMLRpt = v
      End if

      Dim sPrivaqcyRptDom
      Set sPrivaqcyRptDom = CreateObject("Microsoft.XMLDOM")
      sPrivaqcyRptDom.loadxml(sXMLRpt)
      For Each RptDataNode In sPrivaqcyRptDom.selectNodes("/report/form/group")
      If(RptDataNode.getAttribute("title") = "OSHA Information [By OSHA Establishment]") Then

      If(RptDataNode.selectNodes("control").[0].getAttribute("id")="byoshaestablishmentflag1") Then
      lReportTypeId = 1
      End If
      End If
      Next


      If(lReportTypeId=1) Then
      If Not ApplyCriteriaForAdmin(sXML, errorHTML) Then
      CloseSMDatabase
      Exit Function
      Else
      ApplyPostedXMLJobParams sXML
      End if
      End If

      End if

      'End: Add by kuladeep for update mits:27574


      ' Get job parameters
      sJobName = Request("jobname") & ""
      sJobDesc = Request("jobdesc") & ""
      If sJobName = "" Then
      sJobName = sReportName & "-" & sUserName & "-" & Year(Now) & PadNumeric(Month(Now)) & PadNumeric(Day(Now))& PadNumeric(Hour(Now)) & PadNumeric(Minute(Now)) & PadNumeric(Second(Now))'  FormatDateTime("YYYYMMDDHHMMSS",Now)
      End If
      If sJobDesc = "" Then
      sJobDesc = "Report " & sReportName & " submitted on " & Date & " at " & Time & " by user " & sUserName & "."
      End If

      sOutputType = Request("outputtype") & ""

      sNotificationType = Request("notifytype") & ""
      If sNotificationType <> "notifyonly" And sNotificationType <> "notifylink" And _
      sNotificationType <> "notifyembed" And sNotificationType <> "none" Then
      sNotificationType = "none"
      End If

      sNotifyEmail = Request("notifyemail") & ""
      sNotifyMsg = Request("notifymsg") & ""

      ' Start time
      sStartTime = Request("starttime")
      If sStartTime <> "" And IsDate(sStartTime) Then
      sStartTime = GetDBTime(sStartTime) ' PadNumeric(Hour(CDate(sStartTime))) & PadNumeric(Minute(CDate(sStartTime))) & PadNumeric(Second(CDate(sStartTime))) 'Format$(CDate(sStartTime), "hhnnss")
      Else
      sStartTime = "010000"
      End If

      ' Start Date
      sStartDate = Request("startdate")
      If sStartDate <> "" And IsDate(sStartDate) Then
      sStartDate = GetDBDate(sStartDate) 'Year(CDate(sStartDate)) & PadNumeric(Month(CDate(sStartDate))) & PadNumeric(Day(CDate(sStartDate)))	'Format$(CDate(sStartDate), "yyyymmdd")
      Else
      sStartDate = GetDBDate(Now) 'Year(Now) & PadNumeric(Month(Now)) & PadNumeric(Day(Now)) ' Format$(Date, "yyyymmdd")
      End If

      ' Calculate Next Run Date
      Dim v, lIndex 'As Long
      v = DateSerial(Left(sStartDate, 4), Mid(sStartDate, 5, 2), Mid(sStartDate, 7, 2))
      sNextRun = GetDBDate(v) 'Format$(v, "yyyymmdd")

      If lScheduleType = 1 Then
      For lIndex = 1 To 7
      If (Weekday(v) = 2 And Request("MON_RUN") = "1") Or (Weekday(v) = 3 And Request("TUE_RUN") = "1") Or (Weekday(v) = 4 And Request("WED_RUN") = "1") Or (Weekday(v) = 5 And Request("THU_RUN") = "1") Or (Weekday(v) = 6 And Request("FRI_RUN") = "1") Or (Weekday(v) = 7 And Request("SAT_RUN") = "1") Or (Weekday(v) = 1 And Request("SUN_RUN") = "1") Then
      sNextRun = GetDBDate(v) 'Format$(v, "yyyymmdd")
      Exit For
      End If
      v = DateAdd("d", 1, v)
      Next
      Else
      On Error Resume Next
      v = DateSerial(Year(v), Month(v), Request("startdate"))
      If Err.Number > 0 Then
      v = DateSerial(Left(sStartDate, 4), Mid(sStartDate, 5, 2), Mid(sStartDate, 7, 2))
      Else
      If Date > v Then v = DateAdd("m", 1, v)
      End If
      On Error GoTo 0
      For lIndex = 1 To 12
      If (Month(v) = 1 And Request("JAN_RUN") = "1") Or (Month(v) = 2 And Request("FEB_RUN") = "1") Or (Month(v) = 3 And Request("MAR_RUN") = "1") Or (Month(v) = 4 And Request("APR_RUN") = "1") Or (Month(v) = 5 And Request("MAY_RUN") = "1") Or (Month(v) = 6 And Request("JUN_RUN") = "1") Then
      sNextRun = GetDBDate(v) 'Format$(v, "yyyymmdd")
      Exit For
      ElseIf (Month(v) = 7 And Request("JUL_RUN") = "1") Or (Month(v) = 8 And Request("AUG_RUN") = "1") Or (Month(v) = 9 And Request("SEP_RUN") = "1") Or (Month(v) = 10 And Request("OCT_RUN") = "1") Or (Month(v) = 11 And Request("NOV_RUN") = "1") Or (Month(v) = 12 And Request("DEC_RUN") = "1") Then
      sNextRun = GetDBDate(v) 'Format$(v, "yyyymmdd")
      Exit For
      End If
      If Date > v Then v = DateAdd("m", 1, v)
      Next
      End If

      If lScheduleId = 0 Then
      lScheduleId = lGetNextSMUID("SM_SCHEDULE", hSMDb)
      End If
      ' Schedule Report
      rs = objSMRocket.DB_CreateRecordset(hSMDb, "SELECT * FROM SM_SCHEDULE WHERE SCHEDULE_ID=" & lScheduleId, 3, 0)

      If Not objSMRocket.DB_EOF(rs) Then
      If  Request("editscheduleonly")& "" <> "" Then 'Update schedule only - NO REPORT CHANGES.
        'sXML = GetSMData(rs,"REPORT_XML")
        'Start:Comment by kuladeep for update the schedule mits:27574
        If lReportType = 1 or lReportType= 3 or lReportType= 4 then
        If(lReportTypeId=0) Then
        objSMRocket.DB_GetData rs,"REPORT_XML", v
        sXML = v
        End If
        else
        objSMRocket.DB_GetData rs,"REPORT_XML", v
        sXML = v
        End If
        'End:Comment by kuladeep for update the schedul mits:27574

        End If
        objSMRocket.DB_CloseRecordset Cint(rs), 1
        objSMRocket.DB_Edit rs
        Else
        objSMRocket.DB_CloseRecordset Cint(rs), 1
        objSMRocket.DB_AddNew rs
        End If

        objSMRocket.DB_PutData rs, "SCHEDULE_ID", lScheduleId
        objSMRocket.DB_PutData rs, "REPORT_ID", lReportId
        objSMRocket.DB_PutData rs, "SCHEDULE_TYPE", lScheduleType
        objSMRocket.DB_PutData rs, "NEXT_RUN_DATE", sNextRun
        objSMRocket.DB_PutData rs, "START_DATE", sNextRun
        objSMRocket.DB_PutData rs, "START_TIME", sStartTime
        If lScheduleType = 1 Then
        objSMRocket.DB_PutData rs, "MON_RUN", IsChecked("MON_RUN")
        objSMRocket.DB_PutData rs, "TUE_RUN", IsChecked("TUE_RUN")
        objSMRocket.DB_PutData rs, "WED_RUN", IsChecked("WED_RUN")
        objSMRocket.DB_PutData rs, "THU_RUN", IsChecked("THU_RUN")
        objSMRocket.DB_PutData rs, "FRI_RUN", IsChecked("FRI_RUN")
        objSMRocket.DB_PutData rs, "SAT_RUN", IsChecked("SAT_RUN")
        objSMRocket.DB_PutData rs, "SUN_RUN", IsChecked("SUN_RUN")
        Else
        Dim lDay
        lDay = 1
        If not IsEmpty(Request("startday")) Then lDay = Clng(Request("startday"))
        objSMRocket.DB_PutData rs, "DAYOFMONTH_RUN", lDay
        objSMRocket.DB_PutData rs, "JAN_RUN", IsChecked("JAN_RUN")
        objSMRocket.DB_PutData rs, "FEB_RUN", IsChecked("FEB_RUN")
        objSMRocket.DB_PutData rs, "MAR_RUN", IsChecked("MAR_RUN")
        objSMRocket.DB_PutData rs, "APR_RUN", IsChecked("APR_RUN")
        objSMRocket.DB_PutData rs, "MAY_RUN", IsChecked("MAY_RUN")
        objSMRocket.DB_PutData rs, "JUN_RUN", IsChecked("JUN_RUN")
        objSMRocket.DB_PutData rs, "JUL_RUN", IsChecked("JUL_RUN")
        objSMRocket.DB_PutData rs, "AUG_RUN", IsChecked("AUG_RUN")
        objSMRocket.DB_PutData rs, "SEP_RUN", IsChecked("SEP_RUN")
        objSMRocket.DB_PutData rs, "OCT_RUN", IsChecked("OCT_RUN")
        objSMRocket.DB_PutData rs, "NOV_RUN", IsChecked("NOV_RUN")
        objSMRocket.DB_PutData rs, "DEC_RUN", IsChecked("DEC_RUN")
        End If

        objSMRocket.DB_PutData rs, "USER_ID", lUserID
        objSMRocket.DB_PutData rs, "JOB_NAME", sJobName
        objSMRocket.DB_PutData rs, "JOB_DESC", sJobDesc
        objSMRocket.DB_PutData rs, "DSN", sConnectString
        objSMRocket.DB_PutData rs, "OUTPUT_TYPE", sOutputType
        objSMRocket.DB_PutData rs, "OUTPUT_PATH", sOutputPath
        objSMRocket.DB_PutData rs, "OUTPUT_PATH_URL", sOutputPathURL
        objSMRocket.DB_PutData rs, "REPORT_XML", sXML
        objSMRocket.DB_PutData rs, "NOTIFICATION_TYPE", sNotificationType
        objSMRocket.DB_PutData rs, "NOTIFY_EMAIL", sNotifyEmail
        objSMRocket.DB_PutData rs, "NOTIFY_MSG", sNotifyMsg

        objSMRocket.DB_Update cint(rs)

        objSMRocket.DB_CloseRecordset cint(rs), 2

        SaveScheduledReportForAdmin = true
        CloseSMDatabase
        End Function

        'Note: Because of the SMI object generating an equivalent page,
        ' this function will never be called for any sortmaster report.
        Function runReportQueued(sUserName,  lUserID ,  sConnectString ,  sSMConnectString,  sOutputPath,  sOutputPathURL, errorHTML )
        Dim sJobName 'As String
        Dim sJobDesc 'As String
        Dim sOutputType 'As String
        Dim sStartDate 'As String
        Dim sStartTime 'As String
        Dim sNotificationType 'As String
        Dim sNotifyEmail 'As String
        Dim sNotifyMsg 'As String
        Dim sXML 'As String
        Dim rs 'As Integer
        '   Dim sReportXML 'As String
        Dim lReportID 'As Long
        Dim sReportName 'As String
        Dim sReportDesc 'As String
        Dim lReportType 'As Long
        Dim lJobID
        dim sURL
        errorHTML = ""

        OpenSMDatabase

        ' Get report id and lookup report in  database
        If not IsEmpty(Request("reportid")) then
        lReportID = clng(Request("reportid") & "")
    End If
    
    If lReportID = 0 Then
        errorHTML = errorHTML & "<li>Invalid page. No Report ID specified.</li>"
		CloseDatabase
		Exit Function
    End If
    
    rs = objSMRocket.DB_CreateRecordset(hSMDb, "SELECT * FROM SM_REPORTS,SM_REPORTS_PERM WHERE SM_REPORTS.REPORT_ID = " & _
                                lReportID & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND " & _
                                "SM_REPORTS_PERM.USER_ID = " & lUserID, 3, 0)
    If objSMRocket.DB_EOF(rs) Then
        errorHTML = errorHTML & "<li>Report unavailable. Report may not exist or user lacks permission to run report.</li>"
        objSMRocket.DB_CloseRecordset rs, 2
        CloseSMDatabase
		Exit Function
    End If
    
    objSMRocket.DB_GetData rs, "REPORT_NAME", sReportName
    objSMRocket.DB_GetData rs, "REPORT_DESC", sReportDesc 
'    objSMRocket.DB_GetData rs, "REPORT_XML", sReportXML
    
    objSMRocket.DB_CloseRecordset CInt(rs), 2
    
    
	' Let Report engine validate the user entered criteria
	lReportType = GetReportType(CLng(Request("reportid")))
	If Not ApplyCriteria(sXML, errorHTML) Then 
		CloseSMDatabase
		Exit Function
	Else
		ApplyPostedXMLJobParams sXML
	End if
	            
    ' Get job parameters
    sJobName = Request("jobname") & ""
    sJobDesc = Request("jobdesc") & ""
    If sJobName = "" Then
        sJobName = sReportName & "-" & sUserName & "-" & Year(Now) & PadNumeric(Month(Now)) & PadNumeric(Day(Now))& PadNumeric(Hour(Now)) & PadNumeric(Minute(Now)) & PadNumeric(Second(Now))'  FormatDateTime("YYYYMMDDHHMMSS",Now)
    End If
    If sJobDesc = "" Then
        sJobDesc = "Report " & sReportName & " submitted on " & Date & " at " & Time & " by user " & sUserName & "."
    End If
    
    sOutputType = Request("outputtype") & ""
    
    If Request("execreport") & "" = "dttm" Then
        sStartDate = Request("execdate") & ""
        sStartTime = Request("exectime") & ""
        
        If sStartDate <> "" And IsDate(sStartDate) Then
            sStartDate = Year(sStartDate) & PadNumeric(Month(sStartDate)) & PadNumeric(Day(sStartDate))
        Else
            sStartDate = ""
            errorHTML = errorHTML & "<li>Start Date entered is invalid. Please reenter.</li>"
        End If
    
        If sStartTime <> "" And IsDate(sStartTime) Then
            sStartTime = PadNumeric(Hour(sStartTime)) & PadNumeric(Minute(sStartTime)) & PadNumeric(Second(sStartTime))
        Else
            sStartTime = ""
            errorHTML = errorHTML & "<li>Start Time entered is invalid. Please reenter.</li>"
        End If
    End If
    
    sNotificationType = Request("notifytype") & ""
    If sNotificationType <> "notifyonly" And sNotificationType <> "notifylink" And _
       sNotificationType <> "notifyembed" And sNotificationType <> "none" Then
            
       errorHTML = errorHTML & "<li>Notification Type entered is invalid. Please reenter.</li>"
    End If
    
    sNotifyEmail = Request("notifyemail") & ""
    sNotifyMsg = Request("notifymsg") & ""

    '----------------------------------------------------------------
    'SafeWay Atul - OSHA Privacy Case
    Dim pRoot 
    Dim sPrivaqcyDom 
    Dim objAttrib            
    Set sPrivaqcyDom = CreateObject("Microsoft.XMLDOM")
    sPrivaqcyDom.loadxml(sXML)
    Set pRoot = sPrivaqcyDom.selectSingleNode("//report")		   
	Set objAttrib = sPRivaqcyDom.createAttribute("PrivacyCaseFlag")
	objAttrib.Text = Request("PrivacyCaseFlag")
	pRoot.Attributes.setNamedItem objAttrib	
	sXML = sPrivaqcyDom.xml
	'Safeway Atul Code End
	
	'------------------------------------------------------------------

    ' Either submit report to queue manager or display error page if errors occurred
    If Len(errorHTML) > 0 Then
        ' Error occurred - display error page
        errorHTML = "<TR><TD colSpan=4>There were problems with the information you entered. Please correct the listed items.<UL>" & errorHTML & "</ul>"
        runReportQueued = false
    Else
        ' Submit job to queue manager
        rs = objSMRocket.DB_CreateRecordset(hSMDb, "SELECT * FROM SM_JOBS WHERE 0=1", 3, 0)
        objSMRocket.DB_CloseRecordset CInt(rs), 1
        objSMRocket.DB_AddNew rs
        
        lJobID = lGetNextSMUID("SM_JOBS", hSMDb)
        objSMRocket.DB_PutData rs, "JOB_ID", lJobID
        objSMRocket.DB_PutData rs, "USER_ID", lUserID
        objSMRocket.DB_PutData rs, "JOB_PRIORITY", 0
        objSMRocket.DB_PutData rs, "JOB_NAME", sJobName
        objSMRocket.DB_PutData rs, "JOB_DESC", sJobDesc
        objSMRocket.DB_PutData rs, "DSN", sConnectString
        objSMRocket.DB_PutData rs, "OUTPUT_TYPE", sOutputType
        objSMRocket.DB_PutData rs, "OUTPUT_PATH", sOutputPath
        sURL = sOutputPathURL & "?JobID=" & lJobID & "&DPT=" & objSessionStr(SESSION_DOCPATHTYPE) & "&file="
        objSMRocket.DB_PutData rs, "OUTPUT_PATH_URL", sURL
        objSMRocket.DB_PutData rs, "REPORT_XML", sXML
        If sStartDate <> "" Then
            objSMRocket.DB_PutData rs, "START_DTTM", sStartDate & sStartTime
        End If
        objSMRocket.DB_PutData rs, "NOTIFICATION_TYPE", sNotificationType
        objSMRocket.DB_PutData rs, "NOTIFY_EMAIL", sNotifyEmail
        objSMRocket.DB_PutData rs, "NOTIFY_MSG", sNotifyMsg
        
        objSMRocket.DB_PutData rs, "COMPLETE", 0
        objSMRocket.DB_PutData rs, "ASSIGNED", 0
        objSMRocket.DB_PutData rs, "ARCHIVED", 0
        
        objSMRocket.DB_Update CInt(rs)
        objSMRocket.DB_CloseRecordset CInt(rs), 2
        runReportQueued = True
    End If

	If len(errorHTML) <> 0 Then runReportQueued = false
	CloseSMDatabase
End Function

'Get document path
Function GetFileDocPath(ByVal lJobID, ByRef sOutputPath, ByRef sOutputPathURL)
	Dim sTemp, i
	Dim rs
	
	OpenSMDatabase

    rs = objSMRocket.DB_CreateRecordset(hSMDb, "SELECT OUTPUT_PATH, OUTPUT_PATH_URL FROM SM_JOBS WHERE JOB_ID = " & lJobID, 3, 0)
    If Not objSMRocket.DB_EOF(rs) Then
		objSMRocket.DB_GetData rs, "OUTPUT_PATH_URL", sOutputPathURL
		objSMRocket.DB_GetData rs, "OUTPUT_PATH", sOutputPath
    End If
    
    objSMRocket.DB_CloseRecordset CInt(rs), 2
    CloseSMDatabase

End Function

Function PadNumeric(lNum)
	if lNum >= 10 then 
		PadNumeric = cstr(lNum)
	else
		PadNumeric = "0" & cstr(lNum)
	end if
End Function


'******************************************
' The following functions switch based 
' on report type and call the appropriate 
' custom reporting function.
'******************************************

'******************************************
' Build Criteria Page.
'******************************************
Function GenerateCriteriaHTML()
	Dim lType 'As Long
	Dim lReportId

    If not IsEmpty(Request("reportid")) then
        lReportId = clng(Request("reportid") & "")
    End If
   
    lType = GetReportType(lReportId)
    Select Case lType
    Case 1,3,4,5 'Osha Report
    GenerateCriteriaHTML = GenerateOSHACriteriaHTML()
    Case 2 'Executive Summary Report
    GenerateCriteriaHTML = GenerateExecCriteriaHTML()
    Case Else 'Your report Criteria HTML generating function (call) here.
    End Select

    End Function

    '******************************************
    ' Test submitted criteria by applying them.
    '******************************************
    Function ApplyCriteria(ByRef sXML,ByRef errorHTML)
    Dim lType 'As Long
    Dim lReportId

    If not IsEmpty(Request("reportid")) then
    lReportId = clng(Request("reportid") & "")
    End If

    lType = GetReportType(lReportId)
    Select Case lType
    Case 1,3,4,5 'Osha300 Report
    ApplyCriteria = ApplyOSHACriteria(sXML, errorHTML)
    Case 2 'Executive Summary Report
    ApplyCriteria = ApplyExecCriteria(sXML, errorHTML)
    Case Else 'Your report Criteria HTML generating function (call) here.
    End Select

    End Function

    Function GenerateCriteriaHTMLForAdmin()
    Dim lType 'As Long
    Dim lReportId

    If not IsEmpty(Request("reportid")) then
    lReportId = clng(Request("reportid") & "")
    End If

    lType = GetReportTypeForAdmin(lReportId)
    Select Case lType
    Case 1,3,4,5 'Osha Report
    GenerateCriteriaHTMLForAdmin = GenerateOSHACriteriaHTMLForAdmin()
    Case 2 'Executive Summary Report
    GenerateCriteriaHTMLForAdmin = GenerateExecCriteriaHTML()
    Case Else 'Your report Criteria HTML generating function (call) here.
    End Select

    End Function

    '******************************************
    ' Test submitted criteria by applying them.
    '******************************************
    Function ApplyCriteria(ByRef sXML,ByRef errorHTML)
    Dim lType 'As Long
    Dim lReportId

    If not IsEmpty(Request("reportid")) then
    lReportId = clng(Request("reportid") & "")
    End If

    lType = GetReportType(lReportId)
    Select Case lType
    Case 1,3,4,5 'Osha300 Report
    ApplyCriteria = ApplyOSHACriteria(sXML, errorHTML)
    Case 2 'Executive Summary Report
    ApplyCriteria = ApplyExecCriteria(sXML, errorHTML)
    Case Else 'Your report Criteria HTML generating function (call) here.
    End Select

    End Function

    '******************************************
    ' Test submitted criteria by applying them.
    '******************************************
    Function ApplyCriteriaForAdmin(ByRef sXML,ByRef errorHTML)
    Dim lType 'As Long
    Dim lReportId

    If not IsEmpty(Request("reportid")) then
    lReportId = clng(Request("reportid") & "")
    End If

    lType = GetReportTypeForAdmin(lReportId)
    Select Case lType
    Case 1,3,4,5 'Osha300 Report
    ApplyCriteriaForAdmin = ApplyOSHACriteriaForAdmin(sXML, errorHTML)
    Case 2 'Executive Summary Report
    ApplyCriteriaForAdmin = ApplyExecCriteria(sXML, errorHTML)
    Case Else 'Your report Criteria HTML generating function (call) here.
    End Select

    End Function

    '******************************************
    ' Determine available Engine output formats.
    '******************************************
    Function GenerateAvailFormats(lReportId)
    Dim lType ' as Long
    Dim s ' As string
    Dim lSupportedOutputs 'As Long (BitPattern)
    Dim objMgr 'Object type to hold the created object from which we can get
    ' an IReportEngine pointer.
    Dim pEngine 'Pointer to IReportEngine
    Dim bitResult 'The And operator result.
    Set pEngine = nothing

    lType = GetReportType(lReportId)

    Select Case lType
    Case 1,3,4,5
    Set objMgr = CreateObject("OSHALib.COSHAManager")
    objMgr.DSN = objSessionStr(SESSION_DSN)
    objMgr.Init
    'Safewy Atul  Start  - OSHA Privacy case
    objMgr.PrivacyFlag = Request("PrivacyCaseFlag")
    'Safeway Code End
    Set pEngine = objMgr.GetIReportEngine
    Case 2
    Set objMgr = CreateObject("ExecutiveSummary.CManager")
    objMgr.DSN = objSessionStr(SESSION_DSN)
    objMgr.Init
    Set pEngine = objMgr.GetIReportEngine
    Case Else 'SortMaster or other unknown custom report
    End Select

    If not pEngine is nothing then
    lSupportedOutputs = pEngine.OutputsSupported
    bitResult = lSupportedOutputs And 2 'PDF
    If bitResult then s = s & "<option value='pdf' selected=''>PDF</option>"
		bitResult = lSupportedOutputs And 1 'HTML
		If bitResult then s = s & "<option value='html' >HTML</option>"
		bitResult = lSupportedOutputs And 4 'XML
		If bitResult then s = s & "<option value='xml' >XML</option>"
		bitResult = lSupportedOutputs And 8 'TEXT
		If bitResult then s = s & "<option value='txt' >Plain Text</option>"
		bitResult = lSupportedOutputs And 16 'VSVIEW
		If bitResult then s = s & "<option value='vsview' >VSView</option>"
		bitResult = lSupportedOutputs And 32 'CRYSTAL REPORTS
		If bitResult then s = s & "<option value='crystal' >Crystal Reports</option>"
	Else
		s = s & "<option value='pdf' selected=''>PDF</option>"
		s = s &	"<option value='html'>HTML</option>"
		s = s &	"<option value='xml'>XML</option>"
		'MITS 9517  Modified By: Ankur Saxena 06/05/2007
		s = s &	"<option value='csv'>CSV</option>"
		s = s &	"<option value='xls'>XLS</option>"
	End If
	
	'Clean Up
	Set objMgr = nothing
	Set pEngine = nothing
	
	GenerateAvailFormats = "<select name='outputtype' height='1' ID='Select1'>" & s & "</select>"
    End Function

    Function GenerateAvailFormatsForAdmin(lReportId)
    Dim lType ' as Long
    Dim s ' As string
    Dim lSupportedOutputs 'As Long (BitPattern)
    Dim objMgr 'Object type to hold the created object from which we can get
    ' an IReportEngine pointer.
    Dim pEngine 'Pointer to IReportEngine
    Dim bitResult 'The And operator result.
    Set pEngine = nothing

    lType = GetReportTypeForAdmin(lReportId)

    Select Case lType
    Case 1,3,4,5
    Set objMgr = CreateObject("OSHALib.COSHAManager")
    objMgr.DSN = objSessionStr(SESSION_DSN)
    objMgr.Init
    'Safewy Atul  Start  - OSHA Privacy case
    objMgr.PrivacyFlag = Request("PrivacyCaseFlag")
    'Safeway Code End
    Set pEngine = objMgr.GetIReportEngine
    Case 2
    Set objMgr = CreateObject("ExecutiveSummary.CManager")
    objMgr.DSN = objSessionStr(SESSION_DSN)
    objMgr.Init
    Set pEngine = objMgr.GetIReportEngine
    Case Else 'SortMaster or other unknown custom report
    End Select

    If not pEngine is nothing then
    lSupportedOutputs = pEngine.OutputsSupported
    bitResult = lSupportedOutputs And 2 'PDF
    If bitResult then s = s & "<option value='pdf' selected=''>PDF</option>"
    bitResult = lSupportedOutputs And 1 'HTML
    If bitResult then s = s & "<option value='html' >HTML</option>"
    bitResult = lSupportedOutputs And 4 'XML
    If bitResult then s = s & "<option value='xml' >XML</option>"
    bitResult = lSupportedOutputs And 8 'TEXT
    If bitResult then s = s & "<option value='txt' >Plain Text</option>"
    bitResult = lSupportedOutputs And 16 'VSVIEW
    If bitResult then s = s & "<option value='vsview' >VSView</option>"
    bitResult = lSupportedOutputs And 32 'CRYSTAL REPORTS
    If bitResult then s = s & "<option value='crystal' >Crystal Reports</option>"
    Else
    s = s & "<option value='pdf' selected=''>PDF</option>"
    s = s &	"<option value='html'>HTML</option>"
    s = s &	"<option value='xml'>XML</option>"
    'MITS 9517  Modified By: Ankur Saxena 06/05/2007
    s = s &	"<option value='csv'>CSV</option>"
    s = s &	"<option value='xls'>XLS</option>"
    End If

    'Clean Up
    Set objMgr = nothing
    Set pEngine = nothing

    GenerateAvailFormatsForAdmin = "<select name='outputtype' height='1' ID='Select1'>" & s & "</select>"
    End Function
  </script>

