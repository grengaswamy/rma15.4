<% 'By neelima %>
<!-- #include file ="SessionInitialize.asp" -->
<!-- #include file ="generic.asp" -->
<html>
	<head>
		<title>Post Draft Reports</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
<%
'-------------------------------------------------------------------------------------------
'-- Author	: Rajeev Chauhan
'-- Dated	: 08-Jul-2003
'-------------------------------------------------------------------------------------------
Dim objUtil, m, pageError

'-- Delete the temporary report if the user redirects to post the report
Call CleanUp()

	
Set objUtil = Nothing

On Error Resume Next
Set objUtil = Server.CreateObject("InitSMList.CUtility")
On Error Goto 0

If objUtil Is Nothing Then
	Call ShowError(pageError, "Call to CUtility class failed.", True)
End If

If LCase(Request.ServerVariables("REQUEST_METHOD")) = "post" Then
	'-- Component call : records replicated from SMWD tables to SM tables
	If Not objUtil.PostReports(Application("SM_DSN"), Request.Form("reportlist"), Request.Form("userlist")) Then
		Call ShowError(pageError, "Report(s) couldn't be posted.", False)
	Else
		If Request.Form("hdPostedID") <> "" Then ChangeTab Request.Form("hdTabID"), "<center><b><font color=blue>Report Posted Successfully!</font></b></center>"
%>
		<center><b><font color=blue>Report Posted Successfully!</font></b></br></center>
<%
	End if
End If
%>

<html>
	<head>
		<title>Post Draft Reports</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<script language="JavaScript" SRC="SMD.js"></script>
		<script>
			var m_FormSubmitted=false;
		
			function getSelUsers()
			{
				var sUsers=new String();
				for(var i=0;i<document.frmData.users.options.length;i++)
				{
					if (document.frmData.users.options[i].selected)
					{
						var sName=new String(document.frmData.users.options[i].value);
						if(sUsers!="")
							sUsers=sUsers + ",";
						sUsers=sUsers + sName;
					}
				}
				return sUsers;
			}

			function getSelReports()
			{
				var sReports = new String();
			
				if (document.frmData.hdPostedID.value != '')
					sReports = document.frmData.hdPostedID.value;
				else
					{
					for(var i=0;i<document.frmData.elements.length;i++)
					{
						if(document.frmData.elements[i].type == 'checkbox' && document.frmData.elements[i].checked)
						{
							var sReportID = new String(document.frmData.elements[i].value);
							if(sReports != "")
								sReports = sReports + ",";
							sReports = sReports + sReportID;
						}
					}
				}
				return sReports;
			}
					
			function UserSubmit()
			{
				if(m_FormSubmitted)
				{
					alert("You already submitted this form. Please wait for server to respond.");
					return false;
				}
				
				if(getSelUsers()=="")
				{
					alert("Please select at least one user allowed to use this report.");
					return false;
				}
				if(getSelReports()=="")
				{
					alert("Please select at least one report to post.");
					return false;
				}
				var sUsers=getSelUsers();
				document.frmData.userlist.value = sUsers;
				
				var sReports=getSelReports();
				document.frmData.reportlist.value = sReports;
								
				m_FormSubmitted=true;			
				return true;
			}

			function window_onLoad()
			{
			
			}
			var ns, ie, ieversion;
			var browserName = navigator.appName;                   // detect browser 
			var browserVersion = navigator.appVersion;
			if (browserName == "Netscape")
			{
				ie=0;
				ns=1;
			}
			else		//Assume IE
			{
				ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
				ie=1;
				ns=0;
			}

			function onPageLoaded()
			{
				if (ie)
				{
					if ((eval("document.all.divForms")!=null) && (ieversion>=6))
						divForms.style.height=window.frames.frameElement.Height*0.75;
				}
				else
				{
					var o_divforms;
					o_divforms=document.getElementById("divForms");
					if (o_divforms!=null)
					{
						o_divforms.style.height=window.frames.innerHeight*0.72;
						o_divforms.style.width=window.frames.innerWidth*0.995;
					}
				}
			}
		</script>
	</head>

	<body class="10pt" onload1="onPageLoaded()">
	<form name="frmData" method="post" topmargin="0" bottommargin="0" action="<%= Request.ServerVariables("SCRIPT_NAME") %>">
		<%If Request.QueryString("REPORTID") = "" Then %>
				
				
					<table width=100% border="0" cellspacing="0" cellpadding="4">
					<tr>
						<td class="ctrlgroup2" colspan="8">Post Draft Reports</td>
					</tr>
					<tr>
						<td width="5%" class="colheader3">&nbsp;</td>
						<td width="25%" class="colheader3">Report Name</td>
						<td width="70%" class="colheader3">Report Description</td>
					</tr>
					</table>
					<DIV id="divForms" class="divScroll">
					<table width=100% border="0" cellspacing="0" cellpadding="4">
											<%
					Dim i
					i = 0

					Dim arrReportIDs(), arrNames(), arrDescs()
					'-- Component call : results returned in array format
					objUtil.GetReports Application("SM_DSN"), objSessionStr("UserID"), arrReportIDs, arrNames, arrDescs


					On Error Resume Next
					err.Clear() 
					m = -1
					m = UBound(arrReportIDs)
					If Err.number <> 9 And m <> "" And m >= 0 Then
						For i = 0 To UBound(arrReportIDs)
							If i Mod 2 = 0 Then 
						%>
								<tr class="rowlight1">
						<%	Else %>
								<tr class="rowdark1">
						<%	End If %>
									<td width="5%"><input type="Checkbox" name=<%="chkPost" & i%> value="<%=arrReportIDs(i)%>"></td>
									<td width="25%"><a class="Blue" href="rptfields.asp?REPORTID=<%=arrReportIDs(i)%>"><% If "" & arrNames(i)="" Then Response.Write "Report Nr. " & arrReportIDs(i) Else Response.Write arrNames(i) %></a></td>
									<td width="70%"><%If "" & arrDescs(i)="" Then Response.Write "Not Specified" Else Response.Write arrDescs(i) %></td>
								</tr>
						<%
						Next
					End If
					On Error Goto 0						
					%>						
					<tr>
						<td class="colheader3" colspan="8">&nbsp;</td>
					</tr>
				</table>
				</DIV>
			<%Else%>
			<h3>Report saved successfuly. Please select user(s) to post report.</h3>
			<%End If%>
			<table  width="100%" height1="30%" align="center">
				<tr>
					<td  class="datatd" width="20%"><b>Users:</b></td>
					<td class="datatd">
						<i>Note: To select multiple users, hold down Ctrl key while clicking on user.</i><br />
						<select name="users" multiple="">
							<%
							Dim bLoadAllUsers, arrUserIDs(), arrFirstNames(), arrLastNames(), arrLoginNames()
							bLoadAllUsers=True
							If objSessionStr(SESSION_MODULESECURITY)="1" Then
								If objSessionStr(SESSION_ADMINRIGHTS)<>"1" Then
									bLoadAllUsers=False
								End If
							End If
		
							If bLoadAllUsers Then
								'-- Component call : users returned in array
								objUtil.LoadAllUsers Application(APP_SECURITYDSN), objSessionStr("DSNID"), arrUserIDs, arrFirstNames, arrLastNames, arrLoginNames
								Err.Clear()
								On Error Resume Next
								m = -1
								m = UBound(arrUserIDs)
								If Err.number <> 9 And m <> "" And m >= 0 Then
									For iCount = 0 To UBound(arrUserIDs)
										%>
										<option value="<% = arrUserIDs(iCount) %>"><% = arrFirstNames(iCount) & " " & arrLastNames(iCount) & " (" & arrLoginNames(iCount) & ")" %></option>
										<%
									Next
								End If
								On Error Goto 0
							Else
								Response.Write "<option value=""" & objSessionStr(SESSION_USERID) & """>" & objSessionStr(SESSION_FIRSTNAME) & " " & objSessionStr(SESSION_LASTNAME) & " (" & objSessionStr(SESSION_LOGINNAME) & ")</option>"
							End If
							%>								
						</select>
					</td>
				</tr>
								<tr>
					<td colspan="2" align="center" class="datatd">
						<center>
						<input type="submit" class="button" name="btnSave" value="  Post  " onClick="return UserSubmit();"/>
						<input type="hidden" name="userlist" value=""/></center>
						<input type="hidden" name="reportlist" value=""/></center>
						<input type="hidden" name="hdPostedID" value="<%= Request.QueryString("REPORTID") %>"/></center>
						<input type="hidden" name="hdTabID" value="<%= Request.QueryString("TABID") %>"/></center>
					</td>
				</tr>
				<tr>
					<td class="ctrlgroup2" colspan="8"> </td>
				</tr>
			</table>
		</form>
	</body>
</html>
<%
Set objUtil = Nothing

Function ChangeTab(pTabID, pMsg)

	'-- Tabbing redirection
	Dim link
		
	link = ""
	Select Case pTabID
		Case "0"
			link="rptfields.asp"
		Case "1"
			link="rptcriteria.asp"
		Case "2"
			link="rptoptions.asp"
		Case "3"
			link="graphoptions.asp"
		Case "4"
			link="colhierarchy.asp"
	End Select

	If link <> "" Then Response.Redirect link & "?MSG=" & pMsg

End Function
%>