<!-- #include file ="session.inc" -->
<!-- #include file ="dbutil.inc" -->
<%Option Explicit

Public objSession
initSession objSession
dim path,objXml,objXmlStyle,xcode,xparent,isAddrBar,searchIN,xid,entityID,xnodcat,xexpand,xTable,ipAdr
ipAdr=Request.ServerVariables("REMOTE_ADDR")
'Response.Write Request.QueryString("mode")
'Response.End
entityID=0
path=Application(APP_DATAPATH)
	'objSession("table")="claim"
	'objSession("rowid")="1"
	Set xcode = server.CreateObject("orgHierarchy.orgDisplay")
	set objXml=CreateObject("Microsoft.XMLDOM")
	set objXmlStyle=CreateObject("Microsoft.XMLDOM")
	
	if Request.QueryString("tablename")<>"" then
		objSession("table")=Request.QueryString("tablename")
	elseif objSessionStr("table")="" then
		objSession("table")="nothing"
	else
	
	end if
	
	if clng(objSessionStr("isAddrBar"))=1 or clng(objSessionStr("isAddrBar"))=-1 then
		Response.write("<script language=""javascript"">var m_AddrSearch=1;</script>")
		isAddrBar=1
	else
		Response.write("<script language=""javascript"">var m_AddrSearch=0;</script>")
		isAddrBar=0
	end if
		'for addrbar search
		if Request.QueryString("searchin")<>"" then
			searchIN=Request.QueryString("searchin")
		else
			searchIN="DT"
		end if
	if Request.QueryString("entityid")<>"" then
		entityID=Request.QueryString("entityid")
	end if
	'sense the section for which this hierarchy used
	if Request.QueryString("section")="admin" then%>
		<script language="javascript">var m_section="admin";</script>
	<%end if
	'used to set the filter option
	
	if Request.QueryString("chkFilter")="" then
		xTable=objSessionStr("table")%>
	<script language="javascript"> var m_filter='1';</script>
	<%
	elseif Request.QueryString("chkFilter")="1" then
		xTable=objSessionStr("table")%>
	<script language="javascript"> var m_filter='1';</script>
	<%
	elseif Request.QueryString("chkFilter")="0" then
		xTable="nothing"%>
	<script language="javascript"> var m_filter='0';</script>
	<%end if
	
	if Request.QueryString("lob")<>"" then
		objSession("lob")=Request.QueryString("lob")
	elseif objSessionStr("lob")="" then
		objSession("lob")="0"
	end if
	
	
	if Request.QueryString("rowid")<>"" then
		objSession("rowid")=Request.QueryString("rowid")
	elseif objSessionStr("rowid")="" then
		objSession("rowid")="0"
	end if
	if Request.QueryString("parent")="" then
		xparent= "0"
	else
		xparent=Request.QueryString("parent")
	end if
	xid=Request.QueryString("id")
	xnodcat=Request.QueryString("code")
	if Request.QueryString("expand")="" then
		xexpand=true
	else
		xexpand=Request.QueryString("expand")
	end if
	if xid="" then
		objSession("objxml")=""
	end if
	if Request.QueryString("search")<>"" then
		objSession("objxml")=""
	elseif Request.QueryString("reset")="1" then 
		objSession("objxml")=""
	end if
	if objSessionStr("objxml")="" then
		
		objXml.load path & "orgTree.xml"
	else
		objxml.loadxml objSessionStr("objxml")
	end if
	if Request.QueryString("section")="admin" then
		objXmlStyle.load path & "orgTreeAdmin.xsl"
	else
		objXmlStyle.load path & "orgTree.xsl"
	end if
	xcode.conString=objSessionStr("DSN")
	xcode.tabName=objSessionStr("table")
	xcode.row=objSessionStr("rowid")
	
	sub setValue(obj,ctrname) 
		dim xElem
		
		set xElem=obj.selectSingleNode("//form/group/control[@name='" & ctrname & "']") 
		if not xElem is nothing then
			xElem.text="" & Request.QueryString("dlevel")
			xElem.setAttribute "userid",objSessionStr(SESSION_USERID)
			
		end if
		
	end sub
	'this method used to set the starting level of tree with specific organization
	sub getStartWithThisOrg(orgid) 'decide level to make enabled
		dim treeNode
		
	select case objSessionStr("lob")
		
		case "1"
		treeNode="C"
		case "2"
		treeNode="CO"
		case "3"
		treeNode="O"
		case "4"
		treeNode="R"
		case "5"
		treeNode="D"
		case "6"
		treeNode="L"
		case "7"
		treeNode="F"
		case "8"
		treeNode="DT"
	end select
		
	xcode.getSpecificOrgHierarchy cstr(treeNode),objXml,clng(objSessionStr("lob")),clng(orgid),cstr(treeNode),cstr(xTable),clng(objSessionStr("rowid"))
	
	end sub
	
	setValue objXml,"dlevel"	'set the default list level
	if Request.QueryString("search")="" then 'if not search
		
		if objSessionStr("searchOrgId")<>"" and Request.QueryString("currentorg")="1" then 'to set the at current org
			getStartWithThisOrg objSessionStr("searchOrgId")
			'objSession("searchOrgId")=""
		else
		if xid="" then 'first time this proc
			
			'Response.End
			xcode.getorgHierarchyXml cbool(xexpand),objXml,clng(objSessionStr("lob")),,,,cstr(xTable),clng(objSessionStr("rowid"))
		else
			Response.Write("<script language='javascript'>var m_node ='" & xid & "';var m_code='" & xnodcat & "';var m_parent='" & xparent & "';</script>")
			
			xcode.getorgHierarchyXml cbool(xexpand),objXml,clng(objSessionStr("lob")),clng(xid),cstr(xnodcat),cstr(xparent),cstr(xTable),clng(objSessionStr("rowid"))
		end if
		end if
	else
	
		if isAddrBar=0 then
			Response.Write("<script language='javascript'>var m_search = true;</script>")
			if Request.QueryString("selectedlevel")="1" then 'for perticular level search
				'M_RM_SEARCH Pankaj C 25 May,2004 trim before search
				'xcode.getorgHierarchybyLevel cstr(Request.QueryString("name")),cstr(xnodcat),objXml,clng(objSessionStr("lob")),clng(xid),cstr(Request.QueryString("level")),cstr(xTable),clng(objSessionStr("rowid"))
				xcode.getorgHierarchybyLevel trim(cstr(Request.QueryString("name"))),cstr(xnodcat),objXml,clng(objSessionStr("lob")),clng(xid),cstr(Request.QueryString("level")),cstr(xTable),clng(objSessionStr("rowid"))
			elseif Request.QueryString("selectedlevel")="0" then
				'Response.Write(Request.QueryString("name") & "-" & cstr(xnodcat) & "-" & clng(objSessionStr("lob")) & "-" & xTable & "-" & objSessionStr("rowid"))
				'M_RM_SEARCH Pankaj C 25 May,2004 trim before search
				'xcode.getorgHierarchybyLevel cstr(Request.QueryString("name")),cstr(xnodcat),objXml,clng(objSessionStr("lob")),,,cstr(xTable),clng(objSessionStr("rowid"))
				xcode.getorgHierarchybyLevel trim(cstr(Request.QueryString("name"))),cstr(xnodcat),objXml,clng(objSessionStr("lob")),,,cstr(xTable),clng(objSessionStr("rowid"))
			end if
		elseif isAddrBar=1 then
			xcode.getSpecificOrgHierarchy cstr(searchIN),objXml,clng(objSessionStr("lob")),clng(entityID),cstr(searchIN),cstr(xTable),clng(objSessionStr("rowid"))
		end if
	end if
	
	objSession("objxml")=objXml.xml 'xml object remain same for a session
	
	Response.Write objXml.transformNode(objXmlStyle.documentElement)
	
	set objXml=nothing
	set objXmlStyle=nothing
	set xcode=nothing
	%>