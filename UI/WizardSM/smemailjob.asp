<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<!-- #include file ="dcom.inc" -->
<!-- #include file ="dbutil.inc" -->
<!-- #include file ="criteriautils.inc" -->
<%'Option Explicit
' Author: Denis Basaric, 03/28/2000
' Move documents passed in doc variable
' Id's are comma delimited
%>
<%'Public objSession
'initSession objSession
%> 
<script language="vbscript" runat="server">
Function IIf(bBool, bTrue, bFalse)
	if bBool then IIf = bTrue else IIf = bFalse
End Function

Sub CancelRequest()
	Response.Redirect "smrepqueue.asp"
	Response.End
End Sub

Function GetLong(lNum)
	GetLong=0
	If IsNumeric(lNum) Then
		on Error resume next
		GetLong=CLng(lNum)
	End If
End Function

' Return all child users for the current user
Function getChildUsers()
	Dim sRet
	Dim objThisUser, objUser
	Dim lChildUserId
	dim r, henv, db, rs,rs1,db1
	dim lUserID, sFirstName, sLastName, sEmail,bLoadGroupUsers
	
	sRet=""
	bLoadGroupUsers=ObjSessionStr(SESSION_USERSBYGROUP) 'changed by ravi on 06-2004
	if cbool(bLoadGroupUsers)=false then
		set r = CreateObject("DTGRocket")
		henv = r.DB_InitEnvironment()
		db = r.DB_OpenDatabase(henv, Application("SECURITY_DSN"), 0)
		rs = r.DB_CreateRecordset(db, "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_TABLE.EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.DSNID = " & objSessionStr("DSNID") & " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME", 3, 0)
		while not r.DB_EOF(rs)
			r.DB_GetData rs, 1, lUserID
			r.DB_GetData rs, 2, sLastName
			r.DB_GetData rs, 3, sFirstName
			r.DB_GetData rs, 4, sEmail
				
			if sEmail & "" <> "" then   ' skip any users that don't have an email address
				sRet = sRet & "<option value=""" & lUserID & """>" & sFirstName & " " & sLastName & " (" & sEmail & ")</option>"
			end if
				
			r.DB_MoveNext rs
		wend
		r.DB_CloseRecordset CInt(rs), 2
		r.DB_CloseDatabase CInt(db)
		r.DB_FreeEnvironment CLng(henv)
		set r = Nothing
	else
	'*************modified by ravi on24 june 04 to filter out user lists according to group
		set r = CreateObject("DTGRocket")
		henv = r.DB_InitEnvironment()
		db1 = r.DB_OpenDatabase(henv, ObjSessionStr(SESSION_DSN), 0)
		rs1 = r.DB_CreateRecordset(db1, "SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID=" & objSessionStr(SESSION_GROUPID), 3, 0)
				
		while not r.DB_EOF(rs1)
			r.DB_GetData rs1, 1, lUserID
				
			db = r.DB_OpenDatabase(henv,Application(APP_SECURITYDSN) , 0)
			rs = r.DB_CreateRecordset(db, "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_TABLE.EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " & lUserID & " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME", 3, 0)
			if not r.DB_EOF(rs) then
				r.DB_GetData rs, 1, lUserID
				r.DB_GetData rs, 2, sLastName
				r.DB_GetData rs, 3, sFirstName
				r.DB_GetData rs, 4, sEmail
			end if	
		if sEmail & "" <> "" then  			
			sRet = sRet & "<option value=""" & lUserID & """>" & sFirstName & " " & sLastName & " (" & sEmail & ")</option>"
		end if	
			r.DB_MoveNext rs1
			r.DB_CloseRecordset CInt(rs), 2
			r.DB_CloseDatabase CInt(db)
		wend
		r.DB_CloseRecordset CInt(rs), 2
		r.DB_CloseRecordset CInt(rs1), 2
		r.DB_CloseDatabase CInt(db)
		r.DB_CloseDatabase CInt(db1)
		r.DB_FreeEnvironment CLng(henv)
		set r =nothing
	end if	
		
	If sRet="" Then
		sRet="<option value=""0"">No Users Defined.</option>"
	End If

	getChildUsers=sRet
End Function
</script>

<%
Dim sJobList
Dim f, bOk, sTmp, vArrRec
Dim sMoveTo
Dim objDocHandler, objDoc
dim r, h_env, db, rs
dim sFilename
dim lUserID, sFirstName, sLastName, sEmail

sJobList=Request("jobs")
If sJobList="" Then
	CancelRequest
End If

If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" Then
	' Make sure we have email address to send documents to
	If Request("lstUsers")="" And Request("txtEmailTo")="" Then
		Response.Redirect "smemailjob.asp?jobs=" & Server.URLEncode(sJobList)
		Response.End
	End If
	' More Validation
	If Request("lstUsers").Count>0 And Request("txtEmailTo")="" Then
		If Request("lstUsers")(1)="0" Then
			Response.Redirect "smemailjob.asp?jobs=" & Server.URLEncode(sJobList)
			Response.End
		End If
	End If
	
	' Should be OK but we still don't know are those email addresses valid
	If Request("txtEmailTo")<>"" Then
		vArrRec=Split(Request("txtEmailTo"),";")
	End If
	
	Dim objSmtp, sFrom, sFromAddr, sMsg,fso 
	Dim sEmail_From, sEmail_Fromaddr,sWhere,sXML,objXML
	sEmail_From = EMAIL_FROM
	sEmail_Fromaddr = EMAIL_FROMADDR
	
	' 01/17/2007 REM UMESH
		OpenSessDatabase
		sWhere = "FILENAME='customize_reports'"
		sXML = GetSingleSessValue("CONTENT","CUSTOMIZE",sWhere)
		CloseSessDatabase
		if sXML<>"" then
			Set objXML = CreateObject("Microsoft.XMLDOM")
			if (objXML.loadXML(sXML) = false) then
			Response.Write "Error: Selected Customize File could not be loaded.  Please try uploading it again."
			Response.End
			end if
			sEmail_From = objXML.SelectSingleNode("//RMAdminSettings/ReportEmail/From").text
			sEmail_Fromaddr = objXML.SelectSingleNode("//RMAdminSettings/ReportEmail/FromAddr").text
		end if	
	' END REM 
	Set objSmtp = CreateObject("EasyMail.SMTP.4")

	sFrom=IIF(Trim(sEmail_From)="",objSessionStr("FirstName") & " " & objSessionStr("LastName"),sEmail_From)
	sFromAddr=IIF(Trim(sEmail_Fromaddr)="",objSessionStr("EMail"),sEmail_Fromaddr)

	If Trim(sFromAddr & "") = "" Then sFromAddr = REPORTAPP_TITLE & "@here.com"
	sMsg="Report(s) in this message has been sent to you by " & sFrom & " (" & sFromAddr & ")" & Chr(13) & Chr(10) & Chr(13) & Chr(10)
	sMsg=sMsg & Request("txtMessage")

	' Add Custom Recipients from text...
	If IsArray(vArrRec) Then
		For f=0 to UBound(vArrRec)
			objSmtp.AddRecipient vArrRec(f), vArrRec(f),3
		Next
	End If
	
	' Add Any Selected from list
	If Request("lstUsers").Count>0 Then
		set r = CreateObject("DTGRocket")
		h_env = r.DB_InitEnvironment()
		db = r.DB_OpenDatabase(h_env, Application("SECURITY_DSN"), 0)
	
		For f=1 To Request("lstUsers").Count
			rs = r.DB_CreateRecordset(db, "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_TABLE.EMAIL_ADDR FROM USER_TABLE WHERE USER_TABLE.USER_ID = " & Request("lstUsers")(f), 3, 0)
			if not r.DB_EOF(rs) then
				r.DB_GetData rs, 1, lUserID
				r.DB_GetData rs, 2, sLastName
				r.DB_GetData rs, 3, sFirstName
				r.DB_GetData rs, 4, sEmail
					
				if sEmail & "" <> "" then   ' skip any users that don't have an email address
					objSmtp.AddRecipient sFirstName & " " & sLastName, sEmail,3
				end if
			end if
			r.DB_CloseRecordset CInt(rs), 2
		Next
		
		r.DB_CloseDatabase CInt(db)
		r.DB_FreeEnvironment CLng(h_env)
		set r = Nothing
	End If


	' Add all attachments
	Dim sPath, colCleanUpFiles
	Set colCleanUpFiles = CreateObject("Scripting.Dictionary")
	Set fso=CreateObject("Scripting.FilesystemObject")
	sPath=objSessionStr("DocPath")
	If Right(sPath,1)<>"\" Then sPath=sPath & "\"
	
	set r= CreateObject("DTGRocket")
	h_env = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(h_env, Application("SM_DSN"), 0)
	rs = r.DB_CreateRecordset(db, "SELECT * FROM SM_JOBS WHERE COMPLETE <> 0 AND JOB_ID IN (" & sJobList & ") AND USER_ID = " & objSessionStr("UserID"), 3, 0)
	while Not r.DB_EOF(rs)
		r.DB_GetData rs, "OUTPUT_PATH", sFilename
		
		if sFilename <> "" and sFilename <> "none" then
			If Left(Trim(sFilename),1)="{" or UCASE(Left(Trim(sFilename),6))="DRIVER" Then 'DB System Storage
				Dim objCSCFileStorage, vArr
				Set objCSCFileStorage = CreateObject("CSCFileStorage.CCSCFileStorage")
				objCSCFileStorage.Init objSessionStr(SESSION_DSN), CSCStoreDatabase, objSessionStr(SESSION_DOCPATH), false
				vArr=Split(sFilename,"\")
				sFilename = vArr(ubound(vArr))
				sFilename = objCSCFileStorage.RetrieveFile(sFilename)
				If USE_DCOM Then sFileName = PullFileFromDCOMServer(sFileName)
				If Not colCleanUpFiles.Exists(sFilename) Then
					colCleanUpFiles.Add sFilename,sFilename
				End If
			End If
			If Not fso.FileExists(sFilename) Then
				Response.Write("No Email Sent - could not find output file[" + sFilename + "] to attach.")
				Response.End
			End If
			objSmtp.AddAttachment sFilename,0
		End if
	
		r.DB_MoveNext rs
	wend
	r.DB_CloseRecordset CInt(rs), 2
	set r = Nothing
	objSmtp.MailServer=Application("SMTP_SERVER")
    objSmtp.From=sFrom
    objSmtp.FromAddr=sFromAddr
    objSmtp.Subject=REPORTAPP_TITLE & " Report Mail"
    objSmtp.LicenseKey="Dorn Technology Group, Inc./30034206149719039930"
    objSmtp.BodyText=sMsg
	'objSmtp.LogFile = "c:\home\Docs\DebugEmail.Log"
	'objsmtp.EventLogging = true
	objSmtp.Send
	
	'CleanUp
	On Error Resume Next
	Dim vFile
	For Each vFile In colCleanUpFiles
		fso.DeleteFile vFile, True
	Next
	On Error Goto 0
	
	Response.Redirect "smrepqueue.asp"
	Response.End
End If

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>SORTMASTER Report Email</title>
	<!-- [ Rajeev 11/12/2002 -- CSS Consolidation] link rel="stylesheet" href="forms.css" type="text/css"/-->
	<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<script language="JavaScript">
<!--
function ValForm()
{
	var boolRet=false;
	var objUsers=eval("document.frmData.lstUsers");
	if(objUsers!=null)
	{
		if(objUsers.selectedIndex<0 && document.frmData.txtEmailTo.value=="" || objUsers.selectedIndex==1 && objUsers.options[1].value=="0")
			alert("Please select the E-Mail Recipients from the list or enter them manually.");
		else
			boolRet=true;
		return boolRet;
	}
	
	if(document.frmData.txtEmailTo=="")
		alert("Please enter the E-Mail Recipients.");
	else
		boolRet=true;
		
	return boolRet;
}
//Set Focus to the first Field
function pageLoaded()
{

	 var i; 
	 for (i=0;i<document.frmData.length;i++)			
		{	 				
 
          if((document.frmData.item(i).type=="text") || (document.frmData.item(i).type=="select-one") || (document.frmData.item(i).type=="textarea"))
			{
				document.frmData.item(i).focus();
				break;
			}
		}
}
-->
</script>
</head>
<body class="10pt" onload=pageLoaded()>
<form name="frmData" method="post" action="smemailjob.asp" onSubmit="return ValForm()">
<input type="hidden" name="jobs" value="<%=sJobList%>" />
<table class="singleborder" align="center">
	<tr><td class="ctrlgroup">E-Mail Report Jobs</td></tr>
	<tr>
		<td nowrap="" class="datatd"><b>Please select the Users to E-Mail to:</b></td>
	</tr>
	<%sTmp=getChildUsers()
	If sTmp<>"" Then%>
		<tr>
			<td class="datatd"><p align="center"><select name="lstUsers" size="15"  multiple="true"><%=sTmp%></select><br />
			<span class="smallnote">(Press the <b>Ctrl</b> key to select multiple recipients)</span></p></td>
		</tr>
	<%End If%>
	<tr>
		<td class="datatd">Also E-Mail to:<br />
		<input type="text" size="45" name="txtEmailTo" /><br>
		<span class="smallnote">(Separate multiple E-Mail addresses with the semicolon)</span>
		</td>
	</tr>
	<tr>
		<td class="datatd">Message for the Recipients:<br />
		<textarea name="txtMessage" rows="5" cols="36"></textarea>
		</td>
	</tr>
	<tr>
		<td align="center" nowrap="">
			<input type="submit" class="button" name="btnView" value="  E-Mail  ">
			<input type="button" class="button" name="btnView" value=" Cancel " onClick="document.location='smrepqueue.asp'">
		</td>
	</tr>
</table>
</form>

</body>
</html>