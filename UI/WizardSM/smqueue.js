// Author: Denis Basaric, 03/28/2000
// Last Modified by: J. Partin 7/10/2000    Hacked for use in SM Report Queue

function getSelJobs()
{
	var sJobs=new String();
	for(var i=0;i<document.frmData.length;i++)
	{
		var objElem=document.frmData.elements[i];
		var sName=new String(objElem.name);
		if(sName.substring(0,6)=="seljob" && objElem.checked)
		{
			if(sJobs!="")
				sJobs=sJobs + ",";
			sJobs=sJobs + sName.substring(6,sName.length);
		}
	}
	return sJobs;
}

function ArchiveJob()
{
	var sJobs=getSelJobs();

	if(sJobs=="")
	{
		alert("Please select report jobs you would like to archive.");
		return true;
	}
		
	// Send request to the server
	document.location="smarchivejob.asp?jobs=" + escape(sJobs);
	return true;
}

function DeleteJob()
{
	var sJobs=getSelJobs();
	if(sJobs=="")
	{
		alert("Please select report jobs to delete.");
		return true;
	}
	//document.location="smdeletejob.asp?jobs=" + escape(sJobs);
	document.frmData.jobs.value=sJobs;
	document.frmData.action='smdeletejob.asp';
	document.frmData.method='post';
	document.frmData.submit();	
	return true;

}

function EMailJob()
{
	var sJobs=getSelJobs();
	if(sJobs=="")
	{
		alert("Please select report jobs you would like to e-mail.");
		return true;
	}
	document.location="smemailjob.asp?jobs=" + escape(sJobs);
	return true;
}
