<%'Option Explicit%>
<!-- #include file ="session.inc" -->
<%
' This code is run before the login page is returned and it just checks
' is this the user that has been already logged in
'if Request.QueryString("strSessionID") <> "" then
   ' sSessionId = Request.QueryString("strSessionID")
'end if
' Deb -Pen Testing
if Request.QueryString("strID") <> "" then
    sSessionId = Request.QueryString("strID")
end if

If sSessionId = "" Then
    sSessionId=Request.Cookies("RMNETSESSION")
End If    

Set objSession = CreateSessionObj() 
If Application("SESSION_DSN")<>"" Then
	objSession.ConnectionString = Application("SESSION_DSN")
End If


sSessionId=objSession.Init(sSessionId)


'Rsolanki2: security updates 
'Response.AddHeader "Set-Cookie", "RMNETSESSION=" & sSessionId & "; HttpOnly"

Response.Cookies("RMNETSESSION")=sSessionId
'Response.Cookies("RMNETSESSION").Expires = DateAdd("h",24,Now)
Response.Cookies("RMNETSESSION").Path = "/"

Response.ExpiresAbsolute=DateAdd("h",-24,Now)

objSession(SESSION_AUTHENTICATED) = "1"
objSession("OPENPROPERTY")="-2"

'Comment by kuldeep for mits:32377 Start
'if Request.QueryString("UserId") <> "" then
	'objSession("UserID")  =Request.QueryString("UserId") 
'end if

'if Request.QueryString("DSNId") <> "" then
	'objSession("DSNID")=Request.QueryString("DSNId")
'end if

'if Request.QueryString("DocStorageType") <> "" then
	'objSession(SESSION_DOCPATHTYPE)=Request.QueryString("DocStorageType") 
'end if

'if Request.QueryString("DocStoragePath") <> "" then
	'objSession(SESSION_DOCPATH)=Request.QueryString("DocStoragePath") 
'end if

'if Request.QueryString("ConnectionString") <> "" then
	'objSession(SESSION_DSN) = Request.QueryString("ConnectionString") 
'end if

'if Request.QueryString("GroupId") <> "" then
	'objSession(SESSION_GROUPID) = Request.QueryString("GroupId") 
'end if

'if Request.QueryString("DSNName") <> "" then
	'objSession(SESSION_DBNAME) = Request.QueryString("DSNName")  
'end if

'if Request.QueryString("FirstName") <> "" then
	'objSession(SESSION_FIRSTNAME) = Request.QueryString("FirstName")
'end if

'if Request.QueryString("LastName") <> "" then
	'objSession(SESSION_LASTNAME) = Request.QueryString("LastName") 
'end if

'if Request.QueryString("LoginName") <> "" then
	'objSession(SESSION_LOGINNAME) = Request.QueryString("LoginName") 
'end if

'if Request.QueryString("Email") <> "" then
	'objSession(SESSION_EMAIL) = Request.QueryString("Email")
'end if

'if Request.QueryString("AdminRights") <> "" then
	'objSession(SESSION_ADMINRIGHTS) = Request.QueryString("AdminRights") 
'end if
'Comment by kuldeep for mits:32377 End

'Add by kuladeep for SortMaster change Start
if Request.QueryString("SMKeyId") <> "" then
	objSession(SESSION_SMKeyId) = Request.QueryString("SMKeyId")
end if
'Add by kuladeep for SortMaster change End

objSession(SESSION_MODULESECURITY)="0"
'If (Request.QueryString("DBStatus") <> "")  Then
'	if Request.QueryString("DBStatus") <> "0" then
'		objSession(SESSION_MODULESECURITY)="1"
'	end if
'end if
GetSMQueryData() 'Add by kuldeep for mits:32377
objSession(SESSION_USERSBYGROUP)=IsUserListByGroup()


Public Function IsUserListByGroup()
	On Error Resume Next
	dim blnVal,r,db1,iRS1,lEnv1
	blnVal=0
	Set r = CreateObject("DTGRocket")
	lEnv1 = r.DB_InitEnvironment
	db1 = r.DB_OpenDatabase(lEnv1, objSessionStr(SESSION_DSN), 0)
	iRS1=r.DB_CreateRecordSet(db1, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='USER_BY_GROUP_FLG'" , 3, 0)
	if not r.DB_Eof(iRS1) then
		'r.DB_GetData iRS1,1,blnVal
		r.DB_GetData iRS1, 1, blnVal: if IsNull(blnVal) Then blnVal=0
		
	end if
	'Response.Write blnVal
	'Response.End
	blnVal=CBool(blnVal)
	IsUserListByGroup = blnVal
	r.DB_CloseRecordset CInt(iRS1), 2
	r.DB_CloseDatabase CLng(db1)
	r.DB_FreeEnvironment CLng(lEnv1)
	set r = Nothing
End Function

'Add by kuladeep for SortMaster change  mits:32377 Start
Public Function GetSMQueryData()
	On Error Resume Next
   dim blnVal,r,db1,iRS1,lEnv1
    Set r = CreateObject("DTGRocket")
    lEnv1 = r.DB_InitEnvironment
	db1 = r.DB_OpenDatabase(lEnv1,  Application("SESSION_DSN"), 0)
	iRS1=r.DB_CreateRecordSet(db1, "SELECT USERID,DSNID,CONNECTIONSTRING,DOCSTORAGETYPE,DOCSTORAGEPATH,GROUPID,DSNNAME,FIRSTNAME,LASTNAME,LOGINNAME,EMAIL,ADMINRIGHTS,DBSTATUS FROM SM_USER_DATA WHERE SM_ROW_ID=" & objSession(SESSION_SMKeyId), 3, 0)
	if not r.DB_Eof(iRS1) then
     'r.DB_GetData iRS1, "USERID", objSession("UserID")
     r.DB_GetData iRS1, "USERID", blnVal
     objSession("UserID")=blnVal

     'r.DB_GetData iRS1, "DSNID", objSession("DSNID")
     r.DB_GetData iRS1, "DSNID", blnVal
     objSession("DSNID")=blnVal

    'r.DB_GetData iRS1, "CONNECTIONSTRING", objSessionStr(SESSION_DSN)
     r.DB_GetData iRS1, "CONNECTIONSTRING", blnVal
    objSession(SESSION_DSN)=blnVal

     'r.DB_GetData iRS1, "DOCSTORAGETYPE", objSession(SESSION_DOCPATHTYPE)
     r.DB_GetData iRS1, "DOCSTORAGETYPE", blnVal
     objSession(SESSION_DOCPATHTYPE)=blnVal

     'r.DB_GetData iRS1, "DOCSTORAGEPATH", objSession(SESSION_DOCPATH)
     r.DB_GetData iRS1, "DOCSTORAGEPATH", blnVal
     objSession(SESSION_DOCPATH)=blnVal

    'r.DB_GetData iRS1, "GROUPID", objSession(SESSION_GROUPID)
     r.DB_GetData iRS1, "GROUPID", blnVal
    objSession(SESSION_GROUPID)=blnVal

    'r.DB_GetData iRS1, "DSNNAME", objSession(SESSION_DBNAME)
     r.DB_GetData iRS1, "DSNNAME", blnVal
    objSession(SESSION_DBNAME)=blnVal

    'r.DB_GetData iRS1, "FIRSTNAME", objSession(SESSION_FIRSTNAME)
     r.DB_GetData iRS1, "FIRSTNAME", blnVal
    objSession(SESSION_FIRSTNAME)=blnVal

    ' r.DB_GetData iRS1, "LASTNAME", objSession(SESSION_LASTNAME)
     r.DB_GetData iRS1, "LASTNAME", blnVal
    objSession(SESSION_LASTNAME)=blnVal

    ' r.DB_GetData iRS1, "LOGINNAME", objSession(SESSION_LOGINNAME)
     r.DB_GetData iRS1, "LOGINNAME", blnVal
    objSession(SESSION_LOGINNAME)=blnVal

     ' r.DB_GetData iRS1, "EMAIL", objSession(SESSION_EMAIL)
     r.DB_GetData iRS1, "EMAIL", blnVal
    objSession(SESSION_EMAIL)=blnVal

    ' r.DB_GetData iRS1, "ADMINRIGHTS", objSession(SESSION_ADMINRIGHTS)
     r.DB_GetData iRS1, "ADMINRIGHTS", blnVal
    objSession(SESSION_ADMINRIGHTS)=blnVal

	end if
    GetSMQueryData = blnVal
End Function
'Add by kuladeep for SortMaster change  mits:32377 End


%>