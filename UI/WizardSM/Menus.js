//tkr 4/2003 enforce "save changes?" when leave frame.  does not work with Netscape 4

var ns, ie;
var browserName = navigator.appName;                   // detect browser 
if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ie=1;
	ns=0;
}
for(var i=0;i<self.parent.frames.length;i++){
	if(self.parent.frames[i].name=="docframe")
		var iDoc=i;
	else if(self.parent.frames[i].name=="nav")
		var iNav=i;
}

function bDataToSave()
{
	//confirm presence of 3 elements needed run the ConfirmSave() function
	if(eval("parent.docframe.ConfirmSave")==null)	
		return false;
	if(eval("parent.docframe.document.frmData")==null)
		return false;
	if(eval("parent.docframe.document.frmData.sysCmdConfirmSave")==null)
		return false;
	
	//run the ConfirmSave() function
	return parent.docframe.ConfirmSave()	
	
}

function onPageLoaded()
{
	var i;
	
	if (ie)
	{
		for (i=0;i<document.all.length;i++)
		{
			if (document.all.item(i).tagName=="A")				//Change the color of the fisrt <A> tag
			{
				document.all.item(i).style.color="DarkRed";
				i=document.all.length+1;
			}
		}
	}
	else	//Netscape
	{
		var elemlist;
		elemlist=document.getElementsByTagName("A");
		for (i=0;i<elemlist.length;i++)
			if (elemlist[i].tagName=="A")						//Change the color of the fisrt <A> tag
			{
				elemlist[i].style.color="DarkRed";
				break;
			}
	}
}

function linkClick(obj,sURL,sTarget)
{
	var i;
	if (ie)
	{
		for (i=0;i<document.all.length;i++)
			if (document.all.item(i).tagName=="A")
				document.all.item(i).style.color="";
	}
	else
	{
		var elemlist;
		elemlist=document.getElementsByTagName("A");
		for (i=0;i<elemlist.length;i++)
			if (elemlist[i].tagName=="A")
				elemlist[i].style.color="";
	}
		
	obj.style.color="DarkRed";

	/*if(bDataToSave()){
		parent.docframe.document.frmData.sysCmdConfirmSave.value=1;
		parent.docframe.document.frmData.sysCmdQueue.value="urlNavigate('"+sURL+"')";
		window.setTimeout("parent.docframe.Navigate(5)",10);
		return true;
	}
	else{*/
		window.setTimeout("self.parent.frames["+iDoc+"].location.href='"+sURL+"'",10);
		return true;
	/*}*/
	
}

function Documents(obj,qentrypage)
{

	var i;
	if (ie)
	{
		for (i=0;i<document.all.length;i++)
			if (document.all.item(i).tagName=="A")
				document.all.item(i).style.color="";
	}
	else
	{
		var elemlist;
		elemlist=document.getElementsByTagName("A");
		for (i=0;i<elemlist.length;i++)
			if (elemlist[i].tagName=="A")
				elemlist[i].style.color="";
	}
	obj.style.color="DarkRed";
	
	if(bDataToSave()){
		parent.docframe.document.frmData.sysCmdConfirmSave.value=1;
		if(qentrypage=="")
			parent.docframe.document.frmData.sysCmdQueue.value="urlNavigate('forms.asp?formname=event')";
		else
			parent.docframe.document.frmData.sysCmdQueue.value="urlNavigate('"+qentrypage+"')";
		window.setTimeout("parent.docframe.Navigate(5)",10);
		window.setTimeout("self.parent.frames["+iNav+"].location.href='documentsmenu.asp'",10);
		return true;
	}
	else{
		if (qentrypage=="")
			window.setTimeout("self.parent.frames["+iDoc+"].location.href='forms.asp?formname=event'",10);
		else
			window.setTimeout("self.parent.frames["+iDoc+"].location.href='"+qentrypage+"'",10);
		window.setTimeout("self.parent.frames["+iNav+"].location.href='documentsmenu.asp'",10);
	}
}