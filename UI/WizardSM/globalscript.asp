<%'Option Explicit%>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<%

Dim xmlDoc, pageError
Set xmlDoc = Nothing
On Error Resume Next
Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument")
On Error Goto 0

If xmlDoc Is Nothing Then
	Call ShowError(pageError, "XML Parser is not available.", True)
End If
xmlDoc.async = False

Set objData = Nothing

On Error Resume Next
Set objData = Server.CreateObject("InitSMList.CDataManager")
On Error Goto 0

If objData Is Nothing Then
	Call ShowError(pageError, "Call to CDataManager class failed.", True)
End If

objData.m_RMConnectStr = Application("SM_DSN")
objData.ReportID = objSessionStr("REPORTID")
If Not objData.GetReport Then
	objData.TempReportID = objSessionStr("REPORTID")
	If Not objData.GetTempReport Then
		Response.Write "Error in Report..."
		Response.End 
	End If
End If

xmlDoc.loadXML objData.XML
	
If Not xmlDoc.getElementsByTagName("report").item(0).getAttributeNode("globalscript") Is Nothing Then
	sGlobalScript = xmlDoc.getElementsByTagName("report").item(0).getAttributeNode("globalscript").value
End If
	
%>

<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Global Script]</title>
		<script>
			function dialogModal()
			{
				var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) >= 4));
				
				if (Nav4)
					window.focus();
				
				return true;
			}	
			
			function OK_click()
			{
				window.opener.window.document.frmData.hdDerGScript.value = window.document.frmData.txtUsers.value;
				window.opener.tabNavigation(window.document.frmData.hdTabID.value,window.document.frmData.hdTabID.value);
				window.close(); 
			}
				
		</script>	
	</head>

	<body onBlur="dialogModal()">
	<form name="frmData" topmargin="0" leftmargin="0">
		<table class="formsubtitle" border="0" width="98%" align="center">
			<tr>
				<td class="ctrlgroup">Global Script</td>
			</tr>
		</table>

		<br>

		<table width="100%" border="0">
			<tr>
				<td><TEXTAREA style="WIDTH:99%" name=txtUsers rows=5 cols=40><%=sGlobalScript%></TEXTAREA></td>
			</tr>
		</table>
		<hr width="98%">
		
		<table width="100%" border="0">
			<tr>
				<td width="50%" align="right"><input type="button" class="button" name="up" value="OK"    onClick ="OK_click();" style="width:30%"></td>
				<td align="left"><input type="button" class="button" name="up1" value="Cancel" onClick="window.close();" style="width:30%"></td>
			</tr>
		</table>

		<input type="hidden" name="hdTabID" value="<%=Request.QueryString("TABID")%>">
		
	</form>
	</body>
</html>