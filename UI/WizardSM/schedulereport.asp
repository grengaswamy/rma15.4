<!-- #include file="CustomCriteria.inc" -->
<!-- #include file="dbutil.inc" -->
<!-- #include file ="sessionInitialize.asp" -->

<%
Public objSession
initSession objSession
' Allow browser to cache page in case of errors 
' Don't want to clear all inputs w/ new request.)
Response.Expires = 10
'ExpirePage
Dim lScheduleType, lReportId, hRS, l
Dim objSMI

Set objSMRocket=Nothing

lScheduleType=0
lReportId=0

Public Function GenerateSchedulePage(lReportID, lScheduleType,lUserID, sEmail,sConnectString ,sSMConnectString)
   Dim sm 'As New SMEngine
   Dim sXML 'As String
   Dim xmlRep 'As New DOMDocument
   Dim nodes 'As IXMLDOMNodeList
   Dim node 'As IXMLDOMElement
   Dim pTmp 'As IXMLDOMElement
   Dim pList 'As IXMLDOMNodeList
   Dim pRoot 'As IXMLDOMElement
   Dim sTmp 'As String
   Dim i 'As Long
   Dim j 'As Long
   Dim sHTML 'As String
   Dim sAsOf 'As String
   Dim henv 'As Long
   Dim db 'As Integer
   Dim rs 'As Integer
   Dim sReportName 'As String
   Dim sReportDesc 'As String
   Dim sReportXML 'As String
   
   ' Get report from SM database
   henv = objSMRocket.DB_InitEnvironment()
   db = objSMRocket.DB_OpenDatabase(cint(henv), sSMConnectString, 0)
   rs = objSMRocket.DB_CreateRecordset(cint(db), "SELECT * FROM SM_REPORTS WHERE SM_REPORTS.REPORT_ID = " & lReportID, 3, 0)
   If objSMRocket.DB_EOF(rs) Then
      'errorHTML = errorHTML & "<li>Report unavailable. Report may not exist or user lacks permission to run report.</li>"
      objSMRocket.DB_CloseRecordset cint(rs), 2
	  objSMRocket.DB_CloseDatabase cint(db)
	  objSMRocket.DB_FreeEnvironment cint(henv)
	  Exit Function
   End If
   
   objSMRocket.DB_GetData rs, "REPORT_NAME",sReportName
   objSMRocket.DB_GetData rs, "REPORT_DESC",sReportDesc 
   objSMRocket.DB_GetData rs, "REPORT_XML",sReportXML
   objSMRocket.DB_CloseRecordset cint(rs), 2
   
   
   ' Load report into DOM
   Set xmlRep = CreateObject("Microsoft.XMLDOM")
   xmlRep.loadXML sReportXML
   Set pRoot = xmlRep.documentElement
  
  'Check for standard Sortmaster Report (Short Circuit's this function)
   On Error Resume Next
   Dim lType, objSMI
   lType = 0
   lType = clng(pRoot.getAttribute("type"))
   On Error Goto 0
   If lType = 0 then 'SortMaster Report (type attribute does not exist - uses reporttype instead) 
		Set objSMI = CreateObject("SMInterface.SMI")
		objSMI.NotifyLink = CBool(bNOTIFY_LINK):	objSMI.NotifyLink_Text = NOTIFYLINK_TEXT
		objSMI.NotifyAttach = CBool(bNOTIFY_EMBED):	objSMI.NotifyEmbed_Text = NOTIFYEMBED_TEXT
		objSMI.NotifyEmail = CBool(bNOTIFY_ONLY):	objSMI.NotifyOnly_Text = NOTIFYONLY_TEXT
		objSMI.NotifyNone_Text = NOTIFYNONE_TEXT
		objSMI.NotifyDefault = CInt(NOTIFY_DEFAULT)
		GenerateSchedulePage =objSMI.GenerateSchedulePage(lReportID, lScheduleType, objSessionStr("UserID"), objSessionStr("EMail"), objSessionStr("DSN"), Application("SM_DSN"))
		Exit Function
   End If
   
     ' Write HTML header
   sHTML = "Schedule Type <b>"
   If lScheduleType = 1 Then sHTML = sHTML & "Daily" Else sHTML = sHTML & "Monthly"
   sHTML = sHTML & "</b><br />"
   
   sHTML = sHTML & "Report Name: <b>" & sReportName & "</b><br />"
   
   sHTML = sHTML & "<table border=""0"" cellspacing=""4"" cellpadding=""2"">" & vbCrLf
   sHTML = sHTML & "<tr><td class=""ctrlgroup"" colspan=""2"">Enter Start Date and Time:</td></tr>" & vbCrLf
   sHTML = sHTML & "<tr>" & vbCrLf
   sHTML = sHTML & "<td>Start Date:</td>" & vbCrLf
   sHTML = sHTML & "<td><input type=""text"" name=""startdate"" value="""" onblur=""dateLostFocus(this.name)"" size=""10""/></td>" & vbCrLf
   sHTML = sHTML & "</tr>" & vbCrLf
   sHTML = sHTML & "<tr>" & vbCrLf
   sHTML = sHTML & "<td><b>Start Time:</b></td>" & vbCrLf
   sHTML = sHTML & "<td><input type=""text"" name=""starttime"" value="""" onblur=""timeLostFocus(this.name)"" size=""10""/></td>" & vbCrLf
   sHTML = sHTML & "</tr>" & vbCrLf
    
   If lScheduleType = 1 Then
      sHTML = sHTML & "<tr>" & vbCrLf
      sHTML = sHTML & "<td class=""ctrlgroup"" colspan=""2"">Select the days you would like to Run Report(s) on:</td>" & vbCrLf
      sHTML = sHTML & "</tr>" & vbCrLf
      sHTML = sHTML & "<tr>" & vbCrLf
      sHTML = sHTML & "<td />" & vbCrLf
      sHTML = sHTML & "<td><input type=""checkbox"" name=""mon_run"" checked=""1"" value=""1"" /> Monday<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""tue_run"" checked=""1"" value=""1"" /> Tuesday<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""wed_run"" checked=""1"" value=""1"" /> Wednesday<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""thu_run"" checked=""1"" value=""1"" /> Thursday<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""fri_run"" checked=""1"" value=""1"" /> Friday<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""sat_run"" value=""1"" /> Saturday<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""sun_run"" value=""1"" /> Sunday<br />" & vbCrLf
      sHTML = sHTML & "</td>" & vbCrLf
      sHTML = sHTML & "</tr>" & vbCrLf
   Else
      sHTML = sHTML & "<tr>" & vbCrLf
      sHTML = sHTML & "<td><b>Run on Day:</b></td>" & vbCrLf
      sHTML = sHTML & "<td><input type=""text"" name=""startday"" value=""1"" onblur=""numLostFocus(this)"" size=""2"" maxlength=""2""/></td>" & vbCrLf
      sHTML = sHTML & "</tr>" & vbCrLf
      sHTML = sHTML & "<tr>" & vbCrLf
      sHTML = sHTML & "<td class=""ctrlgroup"" colspan=""2"">Select the Month(s) you would like to Run Report(s) on:</td>" & vbCrLf
      sHTML = sHTML & "</tr>" & vbCrLf
      sHTML = sHTML & "<tr>" & vbCrLf
      sHTML = sHTML & "<td />" & vbCrLf
      sHTML = sHTML & "<td><input type=""checkbox"" name=""jan_run"" checked=""1"" value=""1"" /> January<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""feb_run"" checked=""1"" value=""1"" /> February<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""mar_run"" checked=""1"" value=""1"" /> March<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""apr_run"" checked=""1"" value=""1"" /> April<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""may_run"" checked=""1"" value=""1"" /> May<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""jun_run"" checked=""1"" value=""1"" /> June<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""jul_run"" checked=""1"" value=""1"" /> July<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""aug_run"" checked=""1"" value=""1"" /> August<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""sep_run"" checked=""1"" value=""1"" /> September<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""oct_run"" checked=""1"" value=""1"" /> October<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""nov_run"" checked=""1"" value=""1"" /> November<br />" & vbCrLf
      sHTML = sHTML & "<input type=""checkbox"" name=""dec_run"" checked=""1"" value=""1"" /> December<br />" & vbCrLf
      sHTML = sHTML & "</td>" & vbCrLf
      sHTML = sHTML & "</tr>" & vbCrLf
   End If
      
   sHTML = sHTML & "<tr><td colspan=""2"" class=""ctrlgroup"">Report Info</td></tr>"
    
   Set pList = xmlRep.selectNodes("/report/reporttitle")
   sHTML = sHTML & "<tr>"
   sHTML = sHTML & "<td>Report Title</td>"
   
   sHTML = sHTML & "<td><input name=""reporttitle"" type=""text"" size=""80"" value="""
   If pList.length > 0 Then sHTML = sHTML & pList.Item(0).Text
   sHTML = sHTML & """></input></td>"
   sHTML = sHTML & "</tr>"
   
   Set pList = xmlRep.selectNodes("/report/reporttitle/subtitle")
   sHTML = sHTML & "<tr>"
   sHTML = sHTML & "<td>Report Subtitle</td>"
   sHTML = sHTML & "<td><input name=""reportsubtitle"" type=""text"" size=""80"" value="""
   If pList.length > 0 Then sHTML = sHTML & pList.Item(0).Text
   sHTML = sHTML & """></input></td>"
   sHTML = sHTML & "</tr>"
   
     ' ... as of date
   sTmp = pRoot.getAttribute("evaldate") & ""
   If sTmp <> "" Then
      sHTML = sHTML & "<tr>"
      sHTML = sHTML & "<td>Report As-Of Date</td>"
      sHTML = sHTML & "<td><input name=""asofdate"" type=""text"" size=""15"" value="""
      sHTML = sHTML & sParseDate(sTmp)
      sHTML = sHTML & """></input></td>"
      sHTML = sHTML & "</tr>"
   End If
    
   ' Generate Job parameters
   	sHTML = sHTML & "<tr>"
	sHTML = sHTML & "<td valign='top'>Optional Output Name</td>"
	sHTML = sHTML & "<td><input name='outputfilename' type='text' size='20' value=''></input><br /><i>(Note: OutputFileName=[OutputName]+[UniqueId]+[.Extension])</i></td>"
	sHTML = sHTML & "</tr>"

   sHTML = sHTML & "<tr>"
   sHTML = sHTML & "<td>Output Type</td>"
   sHTML = sHTML & "<td>"  & GenerateAvailFormats(lReportId)
   sHTML = sHTML & "</td>"
   sHTML = sHTML & "</tr>"
        
   sHTML = sHTML & "<tr>"
   sHTML = sHTML & "<td>Notification Type</td>"
   sHTML = sHTML & "<td><select name=""notifytype"" height=""1"">"
   If CBool(bNOTIFY_LINK) Then sHTML = sHTML & "<option value=""notifylink"" selected="""">" & NOTIFYLINK_TEXT & "</option>"
   If CBool(bNOTIFY_EMBED) Then sHTML = sHTML & "<option value=""notifyembed"">" & NOTIFYEMBED_TEXT & "</option>"
   If CBool(bNOTIFY_ONLY) Then sHTML = sHTML & "<option value=""notifyonly"">" & NOTIFYONLY_TEXT & "</option>"
   sHTML = sHTML & "<option value=""none"">" & NOTIFYNONE_TEXT & "</option>"
   sHTML = sHTML & "</select>"
   sHTML = sHTML & "</td>"
   sHTML = sHTML & "</tr>"
    
   sHTML = sHTML & "<tr>"
   sHTML = sHTML & "<td>Email To</td>"
   sHTML = sHTML & "<td><textarea cols=""30"" rows=""2"" name=""notifyemail"">" & sEmail & "</textarea>&nbsp;&nbsp;<i>(Note: separate multiple email addresses with semicolons)</i>"
   sHTML = sHTML & "</td>"
   sHTML = sHTML & "</tr>"
    
   sHTML = sHTML & "<tr>"
   sHTML = sHTML & "<td>Optional Email Message</td>"
   sHTML = sHTML & "<td><textarea cols=""30"" rows=""2"" name=""notifymsg""></textarea>"
   sHTML = sHTML & "</td>"
   sHTML = sHTML & "</tr>"
   
   sHTML = sHTML & "</table><br />"
   
    
   ' Generate criteria
   'SELECT CASE BASED ON REPORT TYPE HERE
   sHTML = sHTML & GenerateCriteriaHTML()

   ' return HTML
   GenerateSchedulePage = sHTML
   objSMRocket.DB_CloseDatabase cint(db)
   objSMRocket.DB_FreeEnvironment cint(henv)

 
End Function

Function IIf(bBool, bTrue, bFalse)
	if bBool then IIf = bTrue else IIf = bFalse
End Function

Function lGetNextUID(sTableName)
	Dim rs
	Dim sSQL
	Dim lNextUID
	Dim lOrigUID
	Dim lRows
	Dim lCollisionRetryCount
	Dim lErrRetryCount
	Dim sSaveError
 
   Do 
		rs = objSMRocket.DB_CreateRecordset(hSMDB, "SELECT NEXT_ID FROM SM_IDS WHERE TABLE_NAME = '" & sTableName & "'", 3, 0)
		If objSMRocket.DB_EOF(rs) Then
			objSMRocket.DB_CloseRecordset Cint(rs), 2			
			Err.Raise 32001, "lGetNextUID(" & sTableName & ")", "Specified table does not exist in glossary."
			lGetNextUID = 0
		End If
       
		objSMRocket.DB_GetData rs, "NEXT_ID", lNextUID
		objSMRocket.DB_CloseRecordset CInt(rs), 2

		' Compute next id
		lOrigUID = lNextUID
		
		If lOrigUID Then lNextUID = lNextUID + 1 Else lNextUID = 2
		   
		' try to reserve id (searched update)
		sSQL = "UPDATE SM_IDS SET NEXT_ID = " & lNextUID & " WHERE TABLE_NAME = '" & sTableName & "'"
		
		' only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
		If lOrigUID Then sSQL = sSQL & " AND NEXT_ID = " & lOrigUID

		' Try update
		On Error Resume Next
		lRows = objSMRocket.DB_SQLExecute(hSMDB, sSQL)
		
		If Err.Number <> 0 Then
			sSaveError = Err.Description
			
			lErrRetryCount = lErrRetryCount + 1
			
			If lErrRetryCount >= 5 Then
				On Error GoTo 0
				Response.Write "lGetNextUID(" & sTableName & "), Could not save Record, please try and resubmit your application."
				Exit Function
			End If
		Else
			' if success, return
			If lRows = 1 Then
				lGetNextUID = lNextUID - 1
				Exit Function   ' success
			Else
				lCollisionRetryCount = lCollisionRetryCount + 1   ' collided with another user - try again (up to 1000 times)
			End If
		End If
		
		On Error Goto 0
        
	Loop While (lErrRetryCount < 5) And (lCollisionRetryCount < 1000)
     
	If lCollisionRetryCount >= 1000 Then
		Response.Write "GlobalFunctions.lGetNextUID(" & sTableName & "), Collision timeout. Server load too high. Please wait and try again."
		Exit Function
	End If

	lGetNextUID = lNextUID
End Function

Function GetDBDate(sDate)
	Dim vDate
	GetDBDate=""
	
	If Not IsDate(sDate) Then Exit Function
	
	vDate=CDate(sDate)
	
	GetDBDate=Year(vDate)
	If Len("" & Month(vDate))=1 Then GetDBDate=GetDBDate & "0"
	GetDBDate=GetDBDate & Month(vDate)
	
	If Len("" & Day(vDate))=1 Then GetDBDate=GetDBDate & "0"
	GetDBDate=GetDBDate & Day(vDate)
End Function

Function GetDBTime(sTime)
	Dim vTime
	GetDBTime=""
	
	If sTime="" Or Not IsDate(sTime) Then Exit Function
	
	On Error Resume Next
	vTime=CDate(sTime)
	If Err.Number>0 Then Exit Function
	On Error Goto 0
	
	If Len("" & Hour(vTime))=1 Then GetDBTime=GetDBTime & "0"
	GetDBTime=GetDBTime & Hour(vTime)
	
	If Len("" & Minute(vTime))=1 Then GetDBTime=GetDBTime & "0"
	GetDBTime=GetDBTime & Minute(vTime)
	
	If Len("" & Second(vTime))=1 Then GetDBTime=GetDBTime & "0"
	GetDBTime=GetDBTime & Second(vTime)
	
End Function

Sub WriteReports()
	Dim sSQL
	Dim vName, vDesc, lReportID
	
	OpenSMDatabase
	sSQL = "SELECT * FROM SM_REPORTS,SM_REPORTS_PERM WHERE "
	sSQL = sSQL & "SM_REPORTS_PERM.USER_ID = " & objSessionStr("UserID") & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID"
	sSQL = sSQL & " ORDER BY SM_REPORTS.REPORT_NAME"
	hRS = objSMRocket.DB_CreateRecordset(hSMDB, sSQL, 3, 0)
	Do While Not objSMRocket.DB_EOF(hRS)
		objSMRocket.DB_GetData hRS, "REPORT_ID", lReportID
		objSMRocket.DB_GetData hRS, "REPORT_NAME", vName
		objSMRocket.DB_GetData hRS, "REPORT_DESC", vDesc
		Response.Write "<option value=""" & lReportID & """>"
		If "" & vName="" Then Response.Write "Report Nr. " & lReportID Else Response.Write vName
		Response.Write "</option>"
		objSMRocket.DB_MoveNext hRS
	Loop
	objSMRocket.DB_CloseRecordset CInt(hRS), 2
End Sub

If Request("scheduletype")="1" Then
	lScheduleType=1
ElseIf Request("scheduletype")="2" Then
	lScheduleType=2
End If

If Request("reportid")<>"" And IsNumeric(Request("reportid")) Then lReportId=CLng(Request("reportid"))

If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" Then
	Dim sPath, sHTTP, errorHTML, sURL
	Set objSMI = CreateObject("SMInterface.SMI")

	If ucase(Request.ServerVariables("HTTPS")) = "ON" then	sHTTP = "https://" else	sHTTP = "http://"
	
	sPath=Request.ServerVariables("SCRIPT_NAME")
	l=InStrRev(sPath,"/")
	If l>0 Then
		sPath=Left(sPath,l)
	Else
		sPath="/"
	End If
	sURL = sHTTP & Request.ServerVariables("SERVER_NAME") & sPath & "smrepserve.asp?DPT=" & objSessionStr(SESSION_DOCPATHTYPE)& ""
	If SaveScheduledReport( objSessionStr(SESSION_LOGINNAME), objSessionStr(SESSION_USERID), objSessionStr(SESSION_DSN), Application(APP_SMDSN), objSessionStr(SESSION_DOCPATH), sURL, errorHTML) Then
		Response.Redirect "schedulelist.asp"
	Else 'Handle form submission error.%>
		<html><head><title></title>
		<!--[Rajeev 11/15/2002 CSS Consolidation] link rel="stylesheet" href="sm.css" type="text/css"/-->
		<link rel="stylesheet" href="RMNet.css" type="text/css"> 
		</head>
		<body class="10pt">
		<b><font color="red">There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br/><br/></font></b>
		<%="<ul>" & errorHTML & "</ul>"%>
		</body></html>
<% 	End If	
	Response.End
End If

%>
<html>
<head>
	<title>Schedule Report</title>
	<!--[Rajeev 11/15/2002 CSS Consolidation] link rel="stylesheet" href="forms.css" type="text/css"/-->
		<link rel="stylesheet" href="RMNet.css" type="text/css"> 
	<script language="JavaScript" SRC="form.js"></script>
	<script language="JavaScript" SRC="smi.js"></script>
<script language="JavaScript">
function ScheduleTypeChange()
{
	if(document.frmData.scheduletype.selectedIndex<=0)
		return false;
	document.frmData.submit();
}
function ReportChange()
{
	if(document.frmData.reportid.selectedIndex<=0)
		return false;
	document.frmData.submit();
}
function Validate()
{
	if(document.frmData.reportid.selectedIndex<0)
	{
		alert("Please select report(s) you would like to schedule.");
		return false;
	}
	if(document.frmData.starttime.value=="")
	{
		alert("Please enter Start Time.");
		document.frmData.starttime.focus();
		return false;
	}
	
	if(document.frmData.startdate.value!="")
	{
		var tmpDate = new Date();
		tmpDate.setHours(0); 
		tmpDate.setMinutes(0);
		tmpDate.setSeconds(0);
		tmpDate.setMilliseconds(0);
		if(Date.parse(document.frmData.startdate.value)<Date.parse(tmpDate))
		{
			alert("Start Date must be >= than today's date.");
			return false;
		}
	}
	
	if(document.frmData.scheduletype.value=="1")
	{
		var f=document.frmData;
		if(!f.mon_run.checked && !f.tue_run.checked && !f.wed_run.checked && !f.thu_run.checked && 
		!f.fri_run.checked && !f.sat_run.checked && !f.sun_run.checked)
		{
			alert("Please select at least one day on which selected report(s) should run.");
			return false;
		}
	}
	else if(document.frmData.scheduletype.value=="2")
	{
		var f=document.frmData;
		if(!f.jan_run.checked && !f.feb_run.checked && !f.mar_run.checked && !f.apr_run.checked && !f.may_run.checked && !f.jun_run.checked && 
		!f.jul_run.checked && !f.aug_run.checked && !f.sep_run.checked && !f.oct_run.checked && !f.nov_run.checked && !f.dec_run.checked)
		{
			alert("Please select at least one month on which selected report(s) should run.");
			return false;
		}
	}
	return ValidateCriteria();
}
function ValidateCriteria()
{
	// This stub is intended to be over-ridden by the custom report's 
	// ValidateCriteria() function which should be defined in their own 
	// included .js file.  It will be called after validating the 
	// scheduling portion of the form.
	// In the case that no further ValidateCriteria() routine is defined, 
	// simply blaze on ahead...
	return true;
}
//Asif MITS 12570 
function TrimSpaces()
{
var TextBoxValue=document.frmData.notifyemail.value;
var temp = new Array();
var finalString='';
var TempString;

temp = TextBoxValue.split(';');
for(var i=0;i<temp.length;i++)
{
TempString=temp[i].replace(/^\s*|\s*$/g,"");
finalString=finalString+TempString+';';
}
finalString=finalString.substring(0,finalString.length-1);
document.frmData.notifyemail.value=finalString;
}
//MITS 12570 END
</script>
</head>
<body class="10pt" onload="return pageLoaded();">
		<table width="100%">
			<tr>
				<td class="msgheader">Schedule a Report</td>
			</tr>
		</table>
<% Dim sSQL
If lScheduleType<>0 Then
	If lReportId<>0 Then
		OpenSMDatabase
		sSQL="SELECT * FROM SM_REPORTS WHERE REPORT_ID=" & lReportId
		hRS = objSMRocket.DB_CreateRecordset(hSMDB, sSQL, 3, 0)
		If objSMRocket.DB_EOF(hRS) Then
			objSMRocket.DB_CloseRecordset CInt(hRS), 2
			CloseSMDatabase
			Response.Write "Report missing. ID: " & lReportId
			Response.End
		End If%>
		<form name="frmData" method="post" action"schedulereport.asp" onSubmit="return Validate()">
		<input type="hidden" name="scheduletype" value="<%=lScheduleType%>" />
		<input type="hidden" name="reportid" value="<%=lReportId%>" />
		
		<%=GenerateSchedulePage(clng(lReportID), clng(lScheduleType), clng(objSessionStr("UserID")), cstr(objSessionStr("EMail")), cstr(objSessionStr("DSN")), cstr(Application("SM_DSN")))%>
					
		<br /><input class="button" type="submit" onclick="TrimSpaces();" value="  Save  " /></td>
	<%Else%>
		<form name="frmData" method="get" action"schedulereport.asp">
		<input type="hidden" name="scheduletype" value="<%=lScheduleType%>" />
		Schedule Type: <b><%If lScheduleType=1 Then Response.Write "Run Report Daily" Else Response.Write "Run Report Monthly"%></b><br />
		Select Report that you would like to Schedule:
		<select name="reportid" onchange="ReportChange()">
			<option />
			<%WriteReports%>
		</select>
	<%End If%>
<%Else%>
<form name="frmData" method="get" action="schedulereport.asp">
Please select schedule type you would like to create:
<select name="scheduletype" size="1" onchange="ScheduleTypeChange()">
	<option />
	<option value="1">Daily  </option>
	<option value="2">Monthly</option>
</select>
<%End If%>
<!--Junk Values expected by form.js code.-->
<input type="hidden" name="sys_formidname" value="eventid" ID="Hidden1"/>
<input type="hidden" name="sys_formpidname" value="eventid,claimid" ID="Hidden2"/>
<input type="hidden" name="formname" value="oshacriteria" ID="Hidden6" />
</form>
<%CloseSMDatabase%>
</html>