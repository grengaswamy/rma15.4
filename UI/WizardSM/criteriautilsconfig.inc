<script language="VBScript" runat="server">

'report email settings
Public Const NOTIFY_DEFAULT="2"
Public Const bNOTIFY_LINK="-1"
Public Const NOTIFYLINK_TEXT="Send Email with Link to Report Output"
Public Const NOTIFYLINK_DEFAULT="Send Email with Link to Report Output"
Public Const bNOTIFY_EMBED="-1"
Public Const NOTIFYEMBED_TEXT="Send Email with Report Output Attached"
Public Const NOTIFYEMBED_DEFAULT="Send Email with Report Output Attached"
Public Const bNOTIFY_ONLY="-1"
Public Const NOTIFYONLY_TEXT="Send Email Only (no report output attached or linked)"
Public Const NOTIFYONLY_DEFAULT="Send Email Only (no report output attached or linked)"
'Public Const bNOTIFY_NONE=-1
Public Const NOTIFYNONE_TEXT="None"
Public Const NOTIFYNONE_DEFAULT="None"
Public Const EMAIL_FROM=""
Public Const EMAIL_FROMADDR=""

'report run settings
Public Const RUN_DEFAULT="0"
Public Const bRUN_IMMEDIATE="-1"
Public Const RUNIMMEDIATE_TEXT="Immediately"
Public Const RUNIMMEDIATE_DEFAULT="Immediately"
Public Const bRUN_DTTM="-1"
Public Const RUNDTTM_TEXT="At Specific Date/Time ->"
Public Const RUNDTTM_DEFAULT="At Specific Date/Time ->"

'reportsmenu.asp links ("0" means hide the link)
Public Const sREPORT_TITLE="Reports"
Public Const sREPORT_TITLE_DEFAULT="Reports"
Public Const bREPORT_TITLE="-1"
Public Const bAVAILABLE_RPT="-1"
Public Const bJOBQUEUE_RPT="-1"
Public Const bPOSTNEW_RPT="-1"
Public Const bDELETE_RPT="-1"
Public Const bSCHED_RPT="-1"
Public Const bVIEWSCHED_RPT="-1"

Public Const sSMNET_TITLE="SM"
Public Const sSMNET_TITLE_DEFAULT="SM"
Public Const bSMNET_TITLE="-1"
Public Const bDESIGNER_RPT="-1"
Public Const bDRAFT_RPT="-1"
Public Const bPOSTDRAFT_RPT="-1"

Public Const sEXECSUMM_TITLE="Exec. Summary"
Public Const sEXECSUMM_TITLE_DEFAULT="Exec. Summary"
Public Const bEXECSUMM_TITLE="-1"
Public Const bCONFIG_RPT="-1"
Public Const bCLAIM_RPT="-1"
Public Const bEVENT_RPT="-1"

Public Const sOTHERRPT_TITLE="Other Reports"
Public Const sOTHERRPT_TITLE_DEFAULT="Other Reports"
Public Const bOTHERRPT_TITLE="-1"
Public Const bOSHA200_RPT="-1"
Public Const bDCC_RPT="-1"
Public Const sDCC_LABEL="DCC"
Public Const sDCC_LABEL_DEFAULT="DCC"

'smrepqueue.asp buttons
Public Const bARCHIVE_Q="-1"
Public Const bEMAIL_Q="-1"
Public Const bDELETE_Q="-1"

</script>