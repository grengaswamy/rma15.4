<!-- #include file ="appconstants.inc" -->
<!-- #include file ="permissionscache.inc" -->
<!-- #include file ="appconstantsconfig.inc" -->

<script language="VBScript" runat="server">
' Author: Denis Basaric, 03/16/2000
Function objSessionStr(s)' as String
	objSessionStr = Trim(objSession(s) & "")
End Function

'BSB Called from index.asp and logout.asp
Function CreateSessionObj()
	Dim Retry
	Dim objSess
	On Error Resume Next
	Set objSess = Nothing
	Retry = 0
	While objSess is Nothing And Retry < 10
		Set objSess=CreateObject("WFSM.CWFSession")
		Retry = Retry + 1
	Wend
	On Error Goto 0
'	objSess.ConnectionString = Application("SESSION_DSN")
'	Response.Write objSess.ConnectionString
	If objSess is Nothing Then
		Response.Write "There has been a communications problem  Please right click and select ""Refresh"" to retry the communication and continue working.<br /><br />If this problem persists please contact technical support."
		Response.End
	End If
	Set CreateSessionObj = objSess
End Function 

Sub initSession(ByRef objSession)
	Dim sSessionId, sAbortURL,Retry
	' Try to retrive Session from Cookifs
	sSessionId=Request.Cookies("RMNETSESSION")
	' Try to Initialize The Session
	On Error Resume Next
	Err.Clear
	Set objSession = Nothing
	Retry = 0
	Do 
		Err.Clear
		Set objSession = Nothing
		Set objSession=CreateObject("WFSM.CWFSession")
	
		if Application(APP_SESSIONDSN)<>"" Then
			objSession.ConnectionString = Application(APP_SESSIONDSN)
		End If
		sSessionId=objSession.Init(sSessionId)

		'Test if we're having communications problem - if so, prepare for retry.
		If Err.Number <> 0 Then Set objSession = Nothing

		' Record the Cookie on the Client it could the new one
		Response.Cookies("RMNETSESSION")=sSessionId
'		Response.Cookies("RMNETSESSION").Expires = DateAdd("h",24,Now)
		Response.Cookies("RMNETSESSION").Path = "/"
		If Not objSession is Nothing Then
			If objSessionStr(SESSION_AUTHENTICATED)<>"1" Then
				If InStr(LCase(Request.ServerVariables("PATH_INFO")),"/admin/") > 0 Then sAbortURL = "../"
				Response.Redirect sAbortURL & "logout.asp"
				Response.End
			Else
				' Check for module security
				If objSessionStr(SESSION_MODULESECURITY)="1" Then
					If "" & Application(APP_PERMLASTCHANGED & objSessionStr(SESSION_DSNID))="" Then
						LoadPermissions objSessionStr(SESSION_DSNID),  objSessionStr(SESSION_DSN)
					End If
				End If
			End If
		End If
		Err.Clear
		objSession(SESSION_SID)=sSessionId
		'Test if we're having communications problem - if so, prepare for retry.
		If Err.Number <> 0 Then Set objSession = Nothing

		Retry = Retry + 1	
	Loop While objSession is Nothing And Retry < 10
	
	On Error Goto 0
	If objSession is Nothing Then
		Response.Write "There has been a communications problem  Please right click and select ""Refresh"" to retry the communication and continue working.<br /><br />If this problem persists please contact technical support."
		Response.End
	End If

End Sub

Public Sub ExpirePage()
	Response.Expires = 60
	Response.Expiresabsolute = Now() - 1
	Response.AddHeader "pragma","no-cache"
	Response.AddHeader "cache-control","private"
	Response.CacheControl = "no-cache"
End Sub
</script>