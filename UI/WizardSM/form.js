// Author: Denis Basaric, 08/10/2000

var m_FormID="";
var m_FormParentID="";
var m_codeWindow=null;
var m_sFieldName="";
var m_LookupType=0;
var m_FormName="";
var m_Wnd=null;
var m_AllowRestore=false;
var m_sDupes="";
var EXISTREC_ALERT="You are working on a new record and this functionality is available for existing records only. Please save the data and try again.";

// BSB 01.18.2003
// Re-work the quicklookup stuff.
// Record the current value in the onchange

//Ramkumar--  MITS4009 20/01/2002
var m_policyWindow=null;
var m_DataChanged=false;
var m_policyLoad=false;
var m_LookupTextChanged=false;
var m_policyFieldName="";
//--

var m_LookupTextChanged=false; //Denotes that a field has been changed.
var m_LookupCancelFieldList = ""; //Store the list of fields that may need to be cancelled for the input currently being looked up.
var m_LookupBusy = false;  //Serialize Access to inputs - one at a time.

var NS4 = (document.layers);
var IE4 = (document.all);
var OPERA=(navigator.userAgent.toLowerCase().indexOf('opera') != -1);
var m_AutoLookup=false;

function ConfirmSave()
{
	var ret = false;

	if(document.frmData.formname.value == 'activitylog')
	return;
			
	if(m_DataChanged){
		ret = confirm('Data has changed. Do you want to save changes?');
	}
	return ret;
}

function CloseProgressWindow()
{
	if(self.parent!=null)
	{
		if(haveProperty(self.parent,"wndProgress") && self.parent.wndProgress!=null)
		{
			self.parent.wndProgress.close();
			self.parent.wndProgress=null;
		}
	}
}

function pageLoaded()
{
	document.codeSelected=codeSelected;
	document.SetClaimPolicy=SetClaimPolicy;
	document.onCodeClose=onCodeClose;
	document.dateSelected=dateSelected;
	document.entitySelected=entitySelected;
	document.policySelected=policySelected;
	document.setFieldName=setFieldName;	
	document.formHandler=formHandler;
	document.OnFormSubmit=OnFormSubmit;
	document.Navigate=Navigate;
	document.FilteredDiary=FilteredDiary;
	document.doSearch=doSearch;
	document.doLookup=doLookup;
	document.showSupp=showSupp;
	document.showJuris=showJuris;
	document.ConfirmSave=ConfirmSave;
	document.goToDoc=goToDoc;
	document.urlNavigate=urlNavigate;
	//document.DeleteConfirmed=DeleteConfirmed;
	self.onfocus=onWindowFocus;
	
	m_FormID=document.frmData.sys_formidname.value;
	m_FormParentID=document.frmData.sys_formpidname.value;
	m_FormName=document.frmData.formname.value;

	if(self.parent!=null)
	{
		if(haveProperty(self.parent,"wndProgress") && self.parent.wndProgress!=null)
		{
			self.parent.wndProgress.close();
			self.parent.wndProgress=null;
		}
	}
	
	if(IE4)
	{
		if(navigator.userAgent.indexOf("MSIE 4.0")<0)
		{
			if(eval("document.all.toolbardrift")!=null)
			{
				document.all.toolbardrift.style.visibility = "visible";
				document.all.toolbardrift.style.position="absolute";
				startDrift(document.all.toolbardrift);
				document.all.formtitle.style.marginTop="48px";
			}
		}
	}
	
	self.onunload=pageUnload;
	
	//tkr 1/2003 check if user action was interrupted by "do you want to save your changes" prompt
	//sysCmdQueue, if it exists, is the interrupted function call
	var obj=eval("document.frmData.sysCmdQueue");
	if(obj!=null){
		var s = new String(document.frmData.sysCmdQueue.value);
		if(s.length>0){
			s='document.'+s;
			document.frmData.sysCmdConfirmSave.value=0;
			document.frmData.sysCmdQueue.value='';
			//self.setTimeout(s,200);
			eval(s);
			return true;
		}
	}
	
	//Set focus to first field
	/* var i; 
	 for (i=0;i<document.frmData.length;i++)			
		{	 				
 
          if((document.frmData[i].type=="text") || (document.frmData[i].type=="select-one") || (document.frmData[i].type=="textarea"))
			{
				document.frmData[i].focus();
				break;
			}
		}*/

	var i =0, bFocused = 0, iCount = 0; 

	for(i=0;i<document.all.length;i++)
	{
		if(document.all[i].name != null)
		{
			if(document.all[i].name.substring(0,7) == 'FORMTAB')
			{
				if (document.all[i].style.display=="")
				{
					bFocused = 1;
					if(document.frmData.sys_focusfields.value != '')
					{
						var sFocusField=new String(document.frmData.sys_focusfields.value);
						var arr=sFocusField.split("|");
						try // if more tabs then provided in XML -- to be handled along with the origin of Tab
						{	
							objElem=eval('document.frmData.'+arr[iCount]);
					
							if((objElem.type=="text") || (objElem.type=="select-one") || (objElem.type=="textarea"))
							{
								if(objElem.disabled==false)
									objElem.focus();
							}
						}
						catch(e){}
					}
					break;
				}
				iCount++;
			}
		}
	}

	if(bFocused == 0)
	{
	 for (i=0;i<document.frmData.length;i++)			
		{	 				
 
          if((document.frmData[i].type=="text") || (document.frmData[i].type=="select-one") || (document.frmData[i].type=="textarea"))
			{
				if(document.frmData[i].disabled==false  && document.frmData[i].visible)
					document.frmData[i].focus();
				break;
			}
		}
	}
		
	//tkr 6/2003 popup if potential dupe claim
	if(m_sDupes.length>0){
		m_Wnd=window.open('ad.html','progressWnd',
				'scrollbars=yes,resizable=yes,width=640,height=350'+',top='+(screen.availHeight-350)/2+',left='+(screen.availWidth-640)/2);
		try
		{
			m_Wnd.document.write(m_sDupes);
		}
		catch ( exception )
		{
			m_sDupes = "Duplicates could not be displayed. Error was: " + String(exception.description);
		}
		finally
		{
		  m_Wnd.document.write(m_sDupes);// this code is always executed
		}
		m_Wnd.document.close();
	}
	
	return true;
}


function pageUnload()
{
	if(m_codeWindow !=null)
		m_codeWindow.close();
	m_codeWindow=null;
	if(m_Wnd!=null)
		m_Wnd.close();
	m_Wnd=null;
	return true;
}

function OnFormSubmit(sCmdQueue)
{
	var ret=true;
	
	if(!m_DataChanged && document.frmData.sysCmd.value=='5')
		return false;
		
	if(document.frmData.sysCmd.value=='5')
		ret=validateForm();
	else if(m_DataChanged)
		{
		if(ConfirmSave())
		 {
			if(!validateForm())
				return false;
			else
				{
					if(sCmdQueue==null)
						sCmdQueue='';
					document.frmData.sysCmdConfirmSave.value=1;
					document.frmData.sysCmdQueue.value=sCmdQueue;
					self.setTimeout('document.frmData.submit();',200);
					return false;
				}
		}	

			var wnd=window.open('progress.html','progressWnd',
				'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
			self.parent.wndProgress=wnd;
			
			return true;
		}
	
	self.onerror=scripterror;
	if(ret && self.parent!=null && self.parent!=self && !OPERA)
	{
		var wnd=window.open('progress.html','progressWnd',
			'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
		self.parent.wndProgress=wnd;
	}
	self.onerror=null;
	
	return ret;
}

function scripterror()
{
	self.parent.wndProgress=null;
	self.setTimeout('document.frmData.submit();',200);
	return true;
}
function FilteredDiary()
{

	if(ConfirmSave()){
		if(!validateForm())
			return false
			else{
				document.frmData.sysCmdConfirmSave.value=1;
				document.frmData.sysCmdQueue.value="FilteredDiary()";
				self.setTimeout('document.frmData.submit();',200);
				return false;
			}
	}
	
	var sId=getCurrentID();
	if(sId<=0 || sId=="")
	{
		self.alert(EXISTREC_ALERT);
		return false;
	}
	//tkr 4/2003 to conform with rmworld must pass claimantentityid when table is claimaint, not claimantrowid
	if(haveProperty(document.frmData,"clmntentityid") && m_FormName=='claimant')
		sId=document.frmData.clmntentityid.value;
	window.location = 'wpamessages.asp?sid=19000&attformname='+m_FormName+'&attrecordid='+sId;
		
	return true;
}
function setDataChanged(b)
{
	m_DataChanged=b;
	return b;
}

function getCurrentID()
{
	if(m_FormID=="")
		return 0;
	var objElem=eval('document.frmData.'+m_FormID);
	if(objElem!=null)
		return objElem.value;
	return 0;
}

function setCurrentID(id)
{
	if(m_FormID=="")
		return false;
	var objElem=eval('document.frmData.'+m_FormID);
	if(objElem!=null)
	{
		objElem.value=id;
		return true;
	}
	return false;
}
function getFormValue(sElementName)
{
	if(sElementName==null || sElementName=="")
		return null;
	var objElem=eval('document.frmData.'+sElementName);
	if(objElem!=null)
		return objElem.value;
	return null;	
}
function getFormDataPage()
{
	var objElem=eval('document.frmData.sys_formdatapage');
	if(objElem!=null)
		return objElem.value;
	return null;
}
function getSaveDataPage()
{
	var objElem=eval('document.frmData.sys_savedatapage');
	if(objElem!=null)
		return objElem.value;
	return null;
}

// Hides code popup window
function onWindowFocus()
{
	if(m_codeWindow!=null)
	{	m_codeWindow.close();
		m_codeWindow=null;
	}
	if(m_policyWindow!=null && m_policyLoad==true)
	{	
 	 	m_policyWindow.close();
	    m_policyWindow=null;
	    m_policyLoad = false;
    }    	
	return true;
}

function onCodeClose()
{
	m_codeWindow=null;
	return true;
}

function Navigate(direction)
{
	document.frmData.sysCmd.value=direction;
	
	//-- Rajeev -- 04/06/04 -- Tab Implementation
	for(i=0;i<document.all.length;i++)
	{
		if(document.all[i].name != null)
		{
			if((document.all[i].name.substring(0,4) == 'TABS') && (direction == ''))
			{
				document.frmData.hTabName.value=document.all[i].id.substring(4,document.all[i].id.length);
				break;
			}
			if((document.all[i].name.substring(0,4) == 'TABS') && document.all[i].className == "Selected")
			{
				document.frmData.hTabName.value=document.all[i].id.substring(4,document.all[i].id.length);
				break;
			}
		}
	}

	if(direction=='')  //tkr need single quotes for sysCmdQueue value
		direction="''";
	if(OnFormSubmit("Navigate("+direction+");"))
		self.setTimeout('document.frmData.submit();',100);

	return true;
}

function validateForm()
{
	//return true;
	var v=null;
	v=eval("document.frmData.sys_notreqnew");
	var sNotReqNew=new String("");
	if(v!=null)
		sNotReqNew=new String(document.frmData.sys_notreqnew.value);
	var arrNR=sNotReqNew.split("|");
	var sReq=new String(document.frmData.sys_required.value);
	var bIsNew=false;
	if(getCurrentID()==0)
		bIsNew=true;
	
	var arr=sReq.split("|");
	var c=arr.length;
	var objElem=null;
	var bFocused = 0;
	for(var i=0;i<=c;i++)
	{
		if(arr[i]!="")
			objElem=eval('document.frmData.'+arr[i]);
		if(objElem!=null)
		{
			var s=new String(objElem.value);
			if(replace(s," ","")=="" || arr[i].substring(arr[i].length-4,arr[i].length)=="_cid" && s=="0")
			{
				if(!bIsNew || (bIsNew && !isInArray(arrNR,arr[i])))
				{
					if(arr[i].substring(arr[i].length-4,arr[i].length)=="_cid")
					{
						objElem=eval('document.frmData.'+arr[i].substring(0,arr[i].length-4));
					}
					alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
					if(objElem!=null && (objElem.type=="textarea" || objElem.type=="text"))
					{
						//-- Rajeev -- 04/06/04 -- Tab Implementation
						for(i=0;i<document.all.length;i++)
						{
							if(document.all[i].name != null)
							{
								if(document.all[i].name.substring(0,7) == 'FORMTAB')
								{
									bFocused = 1;
									try 
									{
										tabChange(document.all[i].name.substring(7,document.all[i].name.length));
										objElem.focus();
										break;
									}
									catch(e) // if required field is on invisible tab area
									{
										continue;
									}
								}
							}
						}
						
						if(bFocused == 0)
						{
							objElem.focus();
						}			
					}
					document.frmData.sysCmdConfirmSave.value=0;
					document.frmData.sysCmdQueue.value='';
					return false;
				}
			}
			
		}
		objElem=null;
	}
	return true;

}


function isInArray(arr, sValue)
{
	var c=arr.length;
	for(var i=0;i<=c;i++)
	{
		if(arr[i]==sValue)
			return true;
	}
	return false;
}

//CodeWithDetail is a field type that has two buttons. It behaves
// in general like a code except that an extra button is present 
// which allows for the return of additional detail fields via
// a function and it's call-back indicated by the DetailTypeId.
// Created for ClaimStatusHistory pop-up.
function selectCodeWithDetail(sFieldName,DetailTypeId)
{
	switch(DetailTypeId)
	{
		case 1:
			selectStatusHistoryDetail(sFieldName);
			break;
		default:
			//does nothing
	}
	return true;
}

function selectStatusHistoryDetail(sFieldName)
{
	var frm;
	var curStatus,curDate,curApprovedBy,curReason,curParams;
	frm = document.frmData;
	
	//Pick up defaults for detail screen.
	if (haveProperty(frm, "statuschangedate"))
		curDate = document.frmData.statuschangedate.value;
	if (haveProperty(frm, "statuschangereason"))
		curReason = document.frmData.statuschangereason.value;
	if (haveProperty(frm, "statuschangeapprovedby"))
		curApprovedBy = document.frmData.statuschangeapprovedby.value;
	if (haveProperty(frm, "claimstatuscode_cid"))
		curStatus = document.frmData.claimstatuscode_cid.value;
	
	curParams = "&curstatus=" + escape(curStatus);
	curParams = curParams + "&curdate=" + escape(curDate);
	curParams = curParams + "&curapprovedby=" + escape(curApprovedBy);
	curParams = curParams + "&curreason=" + escape(curReason);
	
	
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	m_Wnd=window.open('claimstatushistory.asp?claimid='+getCurrentID() + curParams,'statusWnd',
			'width=500,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	m_LookupTextChanged=false;
	return true;
}
function GetLOB()
{
	if (!haveProperty(document.frmData, "formname"))
		return '';
	
	sFormName = String(eval('document.frmData.formname.value'));
	switch(sFormName.substr(sFormName.length-2,2))
	{
		case 'gc':
			return "241";
		case 'va':
			return "242";
		case 'wc':
			return "243";
		case 'di':
			return "844";
	}
	return '';
}
function selectCode(sCodeTable,sFieldName,fieldtitle)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	
	if(m_policyWindow!=null && m_policyLoad==true)
	  {
		   m_policyWindow.close();
	       m_policyWindow=null;
		   m_policyLoad =false;
	  }			
	m_sFieldName=sFieldName;
	
	var sFormName;
	
	if (eval('document.frmData.formname')!=null)
		sFormName=document.frmData.formname.value;
	else
		sFormName='';
	
	if(m_LookupTextChanged)
	{
		var objFormElem=eval('document.frmData.'+sFieldName);
		var sFind=objFormElem.value;
		
		objFormElem.value="";
		var objFormElem=eval('document.frmData.'+sFieldName+"_cid");
		objFormElem.value="0";
		
		//Track the Fields to restore in case of cancel.
		m_LookupCancelFieldList=sFieldName + "|"+sFieldName+"_cid";

		m_codeWindow=window.open('quicklookup1.asp?type=code.'+sCodeTable+"&find="+sFind+"&lob="+GetLOB(),'codeWnd',
			'width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		m_LookupTextChanged=false;
	}
	else
	{
	//added by ravi on 12th feb 03
		if(sCodeTable=='orgh')
		{
			var orglevel;
			var objFormElem=eval('document.frmData.'+sFieldName+'_cid');
			var sFind=objFormElem.value;

			orglevel=getOrgLevel(fieldtitle);
			
			if(m_codeWindow!=null)
				m_codeWindow.close();
			if(sFind=='0')
			sFind='';
			//alert('org.asp?tablename='+orgfor+'&rowid='+getCurrentID()+'&lob='+orglevel+'&searchOrgId='+sFind);
			m_codeWindow=window.open('org.asp?tablename='+sFormName+'&rowid='+getCurrentID()+'&lob='+orglevel+'&searchOrgId='+sFind,'Table','width=700,height=600'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');			
		}
		else
		{
			var sTriggerDate;
			var sOrgEid;
			
			if (sFormName!='')
			{
				switch (sFormName)
				{
					case 'event':
						sTriggerDate=getdbDate(document.frmData.dateofevent.value);
						sOrgEid=document.frmData.depteid_cid.value;
						break;
						
					case 'claimgc':
					case 'claimwc':
					case 'claimva':
					case 'claimdi':
						sTriggerDate=getdbDate(document.frmData.dateofclaim.value);
						sOrgEid=document.frmData.ev_depteid_cid.value;
						break;
						
					case 'policy':
						sTriggerDate=getdbDate(document.frmData.effectivedate.value);
						sOrgEid=0;
						break;
					
					case 'employee':
						sOrgEid=document.frmData.deptassignedeid_cid.value;
						break;
						
					default:
						sTriggerDate='';
						sOrgEid=0;
				}
			}
			m_codeWindow=window.open('getcode.asp?code='+sCodeTable+"&lob="+GetLOB()+"&formname="+sFormName+"&triggerdate="+sTriggerDate+"&orgeid="+sOrgEid,'codeWnd',
				'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		}
	}
	
	m_LookupTextChanged=false;
	return false;
}

function getOrgLevel(fieldtitle)
{
var orglevel;
newstr=new String(fieldtitle);
	switch(newstr.toUpperCase())
			{
				case 'DEPARTMENT':
					orglevel='8';
					break;
				case 'FACILITY':
					orglevel='7';
					break;
				case 'LOCATION':
					orglevel='6';
					break;
				case 'DIVISION':
					orglevel='5';
					break;
				case 'REGION':
					orglevel='4';
					break;
				case 'OPERATION':
					orglevel='3';
					break;
				case 'COMPANY':
					orglevel='2';
					break;
				case 'CLIENT':
					orglevel='1';
					break;
				default:
					orglevel='0';
			}
		return orglevel;
	
}
function statusSelected()
{
	var elt;
	var frm;
	elt = null;
	frm = null;
	//Test for each src and target.
	if (m_Wnd==null) return false;
	frm = m_Wnd.document.frmData;
	if (frm == null) return false;

	//Doesn't bring back status changed by since this is auto generated by the objects.
	if (haveProperty(frm, "statuschangedate") && haveProperty(document.frmData, "statuschangedate"))
	{
		document.frmData.statuschangedate.value = frm.statuschangedate.value;
		setDataChanged(true);
	}
	if (haveProperty(frm, "statuschangereason") && haveProperty(document.frmData, "statuschangereason"))
	{
		document.frmData.statuschangereason.value = frm.statuschangereason.value;
		setDataChanged(true);
	}
	if (haveProperty(frm, "statuschangeapprovedby") && haveProperty(document.frmData, "statuschangeapprovedby"))
	{
		document.frmData.statuschangeapprovedby.value = frm.statuschangeapprovedby.value;
		setDataChanged(true);
	}
	if (haveProperty(frm, "claimstatuscode") && haveProperty(document.frmData, "claimstatuscode"))
	{
		elt = frm.claimstatuscode;
		elt = elt.options[elt.selectedIndex];
		document.frmData.claimstatuscode.value=elt.text;
		document.frmData.claimstatuscode_cid.value = elt.value;
		//tkr 4/2003  8 means the claim was closed, must adjust dttmclosed
		if(elt.relatedcodeid==8){
			var sStatusChangeDate=document.frmData.statuschangedate.value;
			var d = new Date();
			var iHours = d.getHours();
			if(iHours<10)
				iHours='0'+iHours;
			var iMinutes = d.getMinutes();
			if(iMinutes<10)
				iMinutes='0'+iMinutes;
			var sTime = ' '+iHours+':'+iMinutes;
			document.frmData.dttmclosed.value=sStatusChangeDate+sTime;	
		}
		else
			document.frmData.dttmclosed.value='';
		setDataChanged(true);
	}
	m_Wnd.close();
	m_Wnd = null;
	return true;
}

function selectCodeLevel(sCodeTable,sFieldName, sOrgLevel)
{
var orglevel;
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	orglevel=getOrgLevel(sOrgLevel);
	m_codeWindow=window.open('org.asp?lob='+orglevel,'Table','width=500,height=450'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	return false;
}

function selectAccountList(sFieldName)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	
	m_codeWindow=window.open('getaccountlist.asp','codeWnd',
		'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	return false;
}

function setFieldName(){

 	m_policyLoad = true;
}
function codeSelected(sCodeText, lCodeId,bCloseWindow)
{
	var objCtrl=null;
	if (m_codeWindow != null && bCloseWindow)
		m_codeWindow.close();
		
 	if(m_policyWindow!=null && m_policyLoad==true)
	  {
	    m_policyWindow.close();
	    m_policyWindow=null;
	    m_policyLoad =false;
	  }					
	objCtrl=eval('document.frmData.'+m_sFieldName);
	if(objCtrl!=null)
	{
		if(objCtrl.type=="textarea")
		{
			objCtrl.value=objCtrl.value + sCodeText + "\n";
		}
		else if(objCtrl.type=="select-one" || objCtrl.type=="select-multiple")
		{
			var bAdd=true;
			for(var i=0;i<objCtrl.length;i++)
			{
				if(objCtrl.options[i].value==lCodeId)
					bAdd=false;
			}
			if(bAdd)
			{
				var objOption = new Option(sCodeText, lCodeId, false, false);
				objCtrl.options[objCtrl.length] = objOption;
				objCtrl=null;
				objCtrl=eval("document.frmData." + m_sFieldName+"_lst");
				if(objCtrl!=null)
				{
					if(objCtrl.value!="" && objCtrl.value.substring(objCtrl.value.length,1)!=",")
						objCtrl.value=objCtrl.value+",";
					objCtrl.value=objCtrl.value+lCodeId;
				}
			}
		}
		else
		{
			//BSB 01.18.2003
			//All quicklookup fields are input type="text".
			//Quicklookup hack - record this in the cancelledvalue
			// attribute so that we track what to restore in case of 
			// "no matching codes found".
			objCtrl.cancelledvalue=sCodeText; 
			objCtrl.value=sCodeText;
			objCtrl=eval('document.frmData.'+m_sFieldName+"_cid");
			objCtrl.value=lCodeId;			
			objCtrl.cancelledvalue=lCodeId;			
		}
		setDataChanged(true);
		setPolicyDataChanged(m_sFieldName);
	}
	if (bCloseWindow) {
	    m_sFieldName="";
	    m_codeWindow=null;
	}
	m_LookupBusy=false;
	return true;
}

function deleteSelCode(sFieldName)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.frmData.'+sFieldName);
	if(objCtrl==null)
		return false;
	if(objCtrl.selectedIndex<0)
		return false;
	
	var bRepeat=true;
	while(bRepeat)
	{
		bRepeat=false;
		for(var i=0;i<objCtrl.length;i++)
		{
			// remove selected elements
			if(objCtrl.options[i].selected)
			{
				objCtrl.options[i]=null;
				bRepeat=true;
				break;
			}
		}
	}
	// Now create ids list
	var sId="";
	for(var i=0;i<objCtrl.length;i++)
	{
			if(sId!="")
				sId=sId+",";
			sId=sId+objCtrl.options[i].value;
	}
	objCtrl=null;
	objCtrl=eval("document.frmData." + sFieldName+"_lst");
	if(objCtrl!=null)
		objCtrl.value=sId;
	
	setDataChanged(true);

	return true;
}

function formatDate(sParamDate)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}

function formatTime(sParamTime)
{
	if(sParamTime=="")
		return "";
	var sTime=new String(sParamTime);
	return sTime.substr(0,2) + ":" + sTime.substr(2,2);
}

function getdbDate(sDate)
{
	if(sDate=="")
		return "";
	var d=new Date(sDate);
	var sYear=new String(d.getFullYear());
	var sMonth=new String(d.getMonth()+1);
	var sDay=String(d.getDate());
	if(sMonth.length==1)
		sMonth="0" + sMonth;
	if(sDay.length==1)
		sDay="0" + sDay;
	
	return sYear+sMonth+sDay;
}

function getdbTime(sTime)
{
	if(sTime=="")
		return "";
	var arr=sTime.split(":");
	if(arr.length!=2)
		return "";
	
	return arr[0]+""+arr[1]+"00";
}

function dateLostFocus(sCtrlName)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var objFormElem=eval('document.frmData.'+sCtrlName);
	var sDate=new String(objFormElem.value);
	var iMonth=0, iDay=0, iYear=0;
	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	var sArr=sDate.split(sDateSeparator);
	if(sArr.length==3)
	{
		sArr[0]=new String(parseInt(sArr[0],10));
		sArr[1]=new String(parseInt(sArr[1],10));
		sArr[2]=new String(parseInt(sArr[2],10));
		// Classic leap year calculation
		if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
			monthDays[1] = 29;
		if(iDayPos<iMonthPos)
		{
			// Date should be as dd/mm/yyyy
			if(parseInt(sArr[1],10)<1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
				objFormElem.value="";
		}
		else
		{
			// Date is something like mm/dd/yyyy
			if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
				objFormElem.value="";
		}
		// Check the year
		if(parseInt(sArr[2],10)<10 || (sArr[2].length!=4 && sArr[2].length!=2))
			objFormElem.value="";
		// If date has been accepted
		if(objFormElem.value!="")
		{
			// Format the date
			if(sArr[0].length==1)
				sArr[0]="0" + sArr[0];
			if(sArr[1].length==1)
				sArr[1]="0" + sArr[1];
			if(sArr[2].length==2)
				sArr[2]="19"+sArr[2];
			if(iDayPos<iMonthPos)
				objFormElem.value=formatDate(sArr[2] + sArr[1] + sArr[0]);
			else
				objFormElem.value=formatDate(sArr[2] + sArr[0] + sArr[1]);
		}
	}
	else
		objFormElem.value="";
	return true;
}

function timeLostFocus(sCtrlName)
{
	var objFormElem=eval('document.frmData.'+sCtrlName);
	var sTime=new String(objFormElem.value);
	if(sTime=="")
		return true;
	sTime=sTime.toUpperCase();
	var sArr=sTime.split(":");
	if(sArr.length!=2)
	{
		objFormElem.value="";
		return true;
	}
	if(sArr[0]=="" || sArr[1]=="" || isNaN(parseInt(sArr[0])) || isNaN(parseInt(sArr[1])))
	{
		objFormElem.value="";
		return true;
	}
	sArr[0]=new String(parseInt(sArr[0],10));
	sArr[1]=new String(parseInt(sArr[1],10));
	if(sTime.indexOf("AM")<0 && sTime.indexOf("PM")<0)
	{
		if(parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>23 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
		{
			objFormElem.value="";
			return true;
		}
		if(sArr[0].length==1)
			sArr[0]="0" + sArr[0];
		if(sArr[1].length==1)
			sArr[1]="0" + sArr[1];
		objFormElem.value=formatTime(sArr[0]+sArr[1]);
	}
	else
	{
		if(parseInt(sArr[0],10)<=0 || parseInt(sArr[0],10)>12 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
		{
			objFormElem.value="";
			return true;
		}
		if(sArr[0].length==1)
			sArr[0]="0" + sArr[0];
		if(sArr[1].length==1)
			sArr[1]="0" + sArr[1];
		objFormElem.value=sArr[0]+":"+sArr[1];
		if(sTime.indexOf("AM")>=0)
			objFormElem.value=objFormElem.value+" AM";
		else
			objFormElem.value=objFormElem.value+" PM";
	}
	return true;
}

function codeLostFocus(sCtrlName)
{
	var objFormElem=eval('document.frmData.'+sCtrlName);
	if(objFormElem.value=="")
	{
		objFormElem=eval('document.frmData.'+sCtrlName+"_cid");
		objFormElem.value="0";
		m_LookupTextChanged=false;
	}
	else
	{
		if(m_LookupTextChanged)
		{
			var objFormElemButton=eval('document.frmData.'+sCtrlName+"btn");
			objFormElemButton.click()
		}
		else
		{
			objFormElem=eval('document.frmData.'+sCtrlName+"_cid");
			if(objFormElem.value=="0" || objFormElem.value=="")
			{
				var objFormElem=eval('document.frmData.'+sCtrlName);
				objFormElem.value="";
			}
		}
	}
	
	return true;
}

function tlbButton(sParam)
{
	if(sParam=="")
		return false;
	var ParamArr=sParam.split(";");
	if(ParamArr.length==0)
		return false;
	var arr=ParamArr[0].split("=");
	if(arr.length==2 && arr[0]=="form" )
		sForm=arr[1];
	setCookie("linkto","event.asp");
	setCookie("id",getCurrentID());
	setCookie("cmd","0");
	if(sForm=="froi")
	{
		window.setTimeout("urlNavigate('http://jpartin_2000/es/mi.pdf')",10);
	}
	else if(sForm=="esumm")
	{
		if(getCurrentID()==0)
		{
			alert('Please select a record to view the Executive Summary.');
			return false;
		}
		s=replace(ParamArr[1],"%currentid%",getCurrentID());
		s=replace(s,"%currentidname%",m_FormID);
		
		//ravi on 04/30/03
		wnd1=window.open("load.htm","es",
		"width="+screen.availWidth+",height="+screen.availHeight+",top=0,left=0,resizable=yes,scrollbars=yes'");
		self.parent.wndProgress=wnd1;

		wnd=window.open("execsummary.asp?" + s + "&formname="+escape(m_FormName),"es",
		"width="+screen.availWidth+",height="+screen.availHeight+",top=0,left=0,resizable=yes,scrollbars=yes'");
				//wnd.document.write('<table align="center" width="100%" border="0"><tr><td valign="center"><img src="img/anglobe.gif"></td><td width="100%" valign="middle" align="center"><font name="Verdana,Arial" size="16"><b>Please wait...</b></font></td></tr></table>');		
	}
	return true;
}

function urlNavigate(sUrl)
{
	window.location.href = sUrl;
	return true;
}

function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p==sPropName)
			return true;
	}
	return false;
}

function formHandler(sLinkTo,sParams,sEnableForNew,sType)
{
	var sLinkParams=new String();
	var bCookies=supportCookies();
	
	if(sType == 'actlog')
	{
		document.frmData.tracklog.value = 1;
		self.setTimeout('document.frmData.submit();',200);
		return false;
	}	
	
	if(sParams!="")
	{	
		if(ConfirmSave()) {
			if(!validateForm())
				return false;
			else{
				document.frmData.sysCmdConfirmSave.value=1;
				var obj=eval("document.frmData.sysCmdQueue");
				if(obj!=null)
					obj.value="formHandler('"+sLinkTo+"','"+sParams+"','"+sEnableForNew+"','"+sType+"');";
				self.setTimeout('document.frmData.submit();',200);
				return false;
			}
		}		

		CloseProgressWindow();
		
		if(sEnableForNew!="1" && (getCurrentID()<=0 || getCurrentID()==""))
		{
			self.alert(EXISTREC_ALERT);
			return false;
		}
		var sp=new String(sParams);
		var arrParams=sp.split("&");
		for(var i=0;i<arrParams.length;i++)
		{
			var arrNameVal=arrParams[i].split("=");
			if(arrNameVal.length==2)
			{
				if(arrNameVal[1].charAt(0)=="%" && arrNameVal[1].charAt(arrNameVal[1].length-1)=="%")
				{
					// Try to replace it with real value
					var sName = replace(arrNameVal[1],"%","");
					var objFormField=null;
					objFormField=eval("document.frmData."+sName);
					if(objFormField!=null)
						arrNameVal[1]=objFormField.value;
					else
						arrNameVal[1]="";
				}
				if(sLinkParams!="")
					sLinkParams=sLinkParams+"&";
				sLinkParams=sLinkParams+arrNameVal[0] + "=" + escape(arrNameVal[1]);
			}
			else
			{
				if(sLinkParams!="")
					sLinkParams=sLinkParams+"&";
				sLinkParams=sLinkParams+arrParams[i];
			}
		}
	}

	if(sType!="back")
	{
		var objTmp=eval("document.frmData.sys_sid");
		if(objTmp!=null)
		{
			if(sLinkParams!="")
				sLinkParams=sLinkParams+"&";
			sLinkParams=sLinkParams+"psid=" + escape(objTmp.value);
		}
	}
	else
	{
		var objTmp=eval("document.frmData.sys_psid");
		if(objTmp!=null)
		{
			if(sLinkParams!="")
				sLinkParams=sLinkParams+"&";
			sLinkParams=sLinkParams+"sid=" + escape(objTmp.value);
		}
	}
	
	if(sLinkParams!="")
		sLinkTo=sLinkTo + "?" + sLinkParams;
	sLinkTo=replace(sLinkTo,"&amp;","&");
	
	if (sType=="new")
		window.open(sLinkTo,'','resizable=yes,scrollbars=yes');
	else
		self.setTimeout("urlNavigate('"+sLinkTo+"',10)");
		
	return true;
}

function replace(sSource, sSearchFor, sReplaceWith)
{
	var arr = new Array();
	arr=sSource.split(sSearchFor);
	return arr.join(sReplaceWith);
}

function supportCookies()
{
	document.cookie="cookietest=1;";
	if(getCookie("cookietest")=="1")
	{
		deleteCookie("cookietest");
		return true;
	}
	return false;	
}

function getCookie(sCookie)
{
  // cookies are separated by semicolons
  var aCookie = document.cookie.split(";");
  for (var i=0; i < aCookie.length; i++)
  {
    // a name/value pair (a crumb) is separated by an equal sign
    var aCrumb = aCookie[i].split("=");
	 var sName=replace(aCrumb[0]," ","");
    if (sCookie == sName) 
      return unescape(aCrumb[1]);
  }
  // a cookie with the requested name does not exist
  return null;
}

function deleteCookie(sCookie)
{
	var dExp=new Date();
	dExp.setFullYear(dExp.getFullYear()-1);
	document.cookie=sCookie+"=;expires=Mon, 01 Dec 1980 23:00:00 UTC;";
	return true;
}

function setCookie(sName, sValue)
{
	document.cookie = sName + "=" + escape(sValue)+";";
	return true;
}

// EID
function eidLostFocus(sCtrlName)
{
	var objFormElem=eval('document.frmData.'+sCtrlName);
	if(objFormElem.value=="")
	{
		objFormElem=null;
		objFormElem=eval('document.frmData.'+sCtrlName+"_eid");
		if(objFormElem!=null)
			objFormElem.value="";
	}
	return true;
}

function lookupData(sFieldName, sTableId, sViewId, sFieldMark, lookupType)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	
	m_AllowRestore=false;
	m_sFieldName=sFieldMark;
	m_LookupType=lookupType;
	
	if(sTableId=="0" || sTableId=="")
		sTableId="-1";
	
	if(lookupType>=6 && lookupType<=8)
		self.lookupCallback="numLookupCallback";
	else if (lookupType==10)
		self.lookupCallback="VehicleLookupCallback";
	else if (lookupType==11)
		self.lookupCallback="EventLookupCallback";
	else
	
		self.lookupCallback="entitySelected";
	
	if(m_LookupTextChanged)
	{
		if(lookupType==1 || lookupType==2)
		{
			// Entity
			if(sTableId!="-1")
				sTableId="entity."+sTableId;
			else
				sTableId="entity";
			
		}//BSB Hack(s) to allow restoring of existing quicklookup field data.
		var objField=eval("window.document.frmData."+sFieldName);
		if(eval("window.document.frmData."+objField.name+"_cid")!=null)
			m_LookupCancelFieldList = objField.name + "|" + objField.name + "_cid";
		else
			m_LookupCancelFieldList = objField.name;
				
		//BSB 01.20.2003 Netscape 4.x hack - Javascript inside the browser does not recognize
		// "extra" attributes added directly from the html.  Hence we have to toss in 
		// a hidden field to hold what should simply be a ".creatable" property of the 
		// input. (Hack started in .xsl)
		var sCreatable = "";
		if (haveProperty(document.frmData,objField.name + "_creatable"))
			if (eval("document.frmData." + objField.name + "_creatable.value") == "1")
			{
				sCreatable = "&creatable=1"
				objField.Previous = objField.value;
			}
		m_LookupBusy = true;
		var s = 'quicklookup1.asp?find=' + escape(objField.value)+"&type="+sTableId+sCreatable;
		
		m_codeWindow=window.open(s,'codeWnd',
			'width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		//if(sTableId!="employees" && !m_AutoLookup)
		//	objField.value="";
		m_AutoLookup=false;
	}
	else
	{	//changed by ravi on 24th march 03 to show org hierarchy for tableid='organizations'
		if(sTableId=='ORGANIZATIONS')
		{
			selectCodeLevel(sFieldName,sFieldName,'');
		}
		else{
		m_codeWindow=window.open('searchpopup.asp?viewid='+sViewId+'&tableid='+sTableId,'searchWnd',
		'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
		}
	}
	
	m_LookupTextChanged=false;
		
	return false;
}

function EventLookupCallback(sId)
{
	var obj=null;
	
	if (m_codeWindow!=null)
	    m_codeWindow.close();
	m_codeWindow=null;
	self.lookupCallback=null;
	
	// Event Lookup
	if (sId==-1)
	{
		var objElements=m_Wnd.document.frmData.elements;
		for(i=0;i<objElements.length;i++)
		{
			obj=null;
			obj=eval("document.frmData."+m_sFieldName+objElements[i].name);
			if(obj!=null)
			{
				obj.value=objElements[i].value;
				obj.cancelledvalue=objElements[i].value;
			}
		}
		m_Wnd.close();
		m_Wnd=null;
		setDataChanged(true);
	}
	else
	{
		self.lookupCallback="EventLookupCallback";
		self.setTimeout("m_Wnd=window.open('geteventdata.asp?eventid="+sId+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
	}
	m_AllowRestore= true;
	return true;
}


function VehicleLookupCallback(sId)
{
	var obj=null;
	
	if (m_codeWindow!=null)
	    m_codeWindow.close();
	m_codeWindow=null;
	self.lookupCallback=null;
	
	if (m_LookupType==10)
	{
		// Vehicle Lookup
		if (sId==-1)
		{
			var objElements=m_Wnd.document.frmData.elements;
			for(i=0;i<objElements.length;i++)
			{
				obj=null;
				obj=eval("document.frmData."+m_sFieldName+objElements[i].name);
				if(obj!=null)
				{
					obj.value=objElements[i].value;
					obj.cancelledvalue=objElements[i].value;
				}
			}
			m_Wnd.close();
			m_Wnd=null;
			setDataChanged(true);
			m_AllowRestore= true;
		}
		else
		{
			self.lookupCallback="VehicleLookupCallback";
			
			self.setTimeout("m_Wnd=window.open('getvehicledata.asp?unitid="+sId+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
		}
	}
	
	return true;
}

function numLookupCallback(sId)
{
	var obj=null;
	
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	self.lookupCallback=null;

	if(m_LookupType==6)
	{
		// Claim Number Lookup
		if(sId==-1)
		{
			obj=eval("document.frmData."+m_sFieldName);
			if(obj!=null)
			{	
				obj.value=m_Wnd.document.frmData.claimnumber.value;
				obj.cancelledvalue=m_Wnd.document.frmData.claimnumber.value;
			}
			
			obj=eval("document.frmData."+"claimid");//Hack to get the claim id instead of using a whole call back routine
			if(obj!=null)
			{	
				obj.value=m_Wnd.document.frmData.claimid.value;
				obj.cancelledvalue=m_Wnd.document.frmData.claimid.value;
			}
			
			m_Wnd.close();
			m_Wnd=null;
			setDataChanged(true);
		}
		else
		{
			self.lookupCallback="numLookupCallback";
			
			self.setTimeout("m_Wnd=window.open('getclaimdata.asp?claimid="+sId+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
			//m_Wnd=window.open('getclaimdata.asp?claimid='+sId,'progressWnd',
			//		'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
		}
	}
	else if(m_LookupType==7)
	{
		// Event Number Lookup
		if(sId==-1)
		{
			obj=eval("document.frmData."+m_sFieldName);
			if(obj!=null)
			{	
				obj.value=m_Wnd.document.frmData.eventnumber.value;
				obj.cancelledvalue=m_Wnd.document.frmData.eventnumber.value;
			}
			m_Wnd.close();
			m_Wnd=null;
			setDataChanged(true);
		}
		else
		{
			self.lookupCallback="numLookupCallback";
			
			self.setTimeout("m_Wnd=window.open('geteventdata.asp?eventid="+sId+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
			//m_Wnd=window.open('geteventdata.asp?eventid='+sId,'progressWnd',
			//		'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
		}
	}
	else if(m_LookupType==8)
	{
		// Vehicle Number Lookup
		if(sId==-1)
		{
			obj=eval("document.frmData."+m_sFieldName);
			
			if(obj!=null)
			{	
				obj.value=m_Wnd.document.frmData.vin.value;
				obj.cancelledvalue=m_Wnd.document.frmData.vin.value;
			}
			m_Wnd.close();
			m_Wnd=null;
			setDataChanged(true);
		}
		else
		{
			self.lookupCallback="numLookupCallback";
			self.setTimeout("m_Wnd=window.open('getvehicledata.asp?unitid="+sId+
				"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
				",left="+((screen.availWidth-400)/2)+"');",100);
			//m_Wnd=window.open('getvehicledata.asp?unitid='+sId,'progressWnd',
			//		'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
		}
	}
	else if(m_LookupType==9)
	{
		// Policy Number Lookup
		if(sId==-1)
		{
			obj=eval("document.frmData."+m_sFieldName);
			if(obj!=null)
			{	
				obj.value=m_Wnd.document.frmData.policynumber.value;
				obj.cancelledvalue=m_Wnd.document.frmData.policynumber.value;
			}
			m_Wnd.close();
			m_Wnd=null;
			setDataChanged(true);
		}
		else
		{
			self.lookupCallback="numLookupCallback";
			
			self.setTimeout("m_Wnd=window.open('getpolicydata.asp?policyid="+sId+
				"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
				",left="+((screen.availWidth-400)/2)+"');",100);
			//m_Wnd=window.open('getpolicydata.asp?policyid='+sId,'progressWnd',
			//		'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
		}
	}
	m_AllowRestore= true;
	return true;
}

//tkr 6/2003 no policy on short term dis; use for plan
function lookupClaimPolicy(sFieldName,sLookup)
{
	if(sLookup=='' || sLookup==null)
		sLookup='lookupclaimpolicy.asp';
	if(m_codeWindow!=null)
		m_codeWindow.close();
	if(m_policyWindow!=null && m_policyLoad==true)
	  {
		
		   m_policyWindow.close();
	       m_policyWindow=null;
		   m_policyLoad =false;
	  }

		m_policyFieldName=sFieldName;
	
 		var objClaimtypecode=eval("document.frmData.claimtypecode_cid");
		var objDateofEvent=eval("document.frmData.ev_dateofevent");	
		var objDateofClaim=eval("document.frmData.dateofclaim");		
		var objDeptID=eval("document.frmData.ev_depteid_cid");		

 		if (objClaimtypecode!=null)
 			sClaimtypecode=objClaimtypecode.value;
 		if (objDateofEvent!=null)
			var sDateofEvent=objDateofEvent.value;
 		if (objDateofClaim!=null)
			var sDateofClaim=objDateofClaim.value;
 		if (objDeptID!=null)
			var sDeptID=objDeptID.value;
		
		var objpolicyCntrl=eval('document.frmData.'+m_policyFieldName)
		//objpolicyCntrl.value="";
		
		if(NS4)
		{
			var sTarget=new String(document.location);
			if(sTarget.lastIndexOf("/")>=0)
				sTarget=sTarget.substring(0,sTarget.lastIndexOf("/"));
				sTarget=sTarget + "/" ;
		
			m_policyWindow=window.open(sTarget + sLookup + '?claimtype='+sClaimtypecode+'&evdate='+sDateofEvent+'&claimdate='+sDateofClaim+'&deptid='+sDeptID,'lookupWnd',
 	  		'width=600,height=250,top='+(screen.availHeight-250)/2+',left='+(screen.availWidth-600)/2+',resizable=yes,scrollbars=yes');
		} 
		else
		{

			m_policyWindow=window.open(sLookup+'?claimtype='+sClaimtypecode+'&evdate='+sDateofEvent+'&claimdate='+sDateofClaim+'&deptid='+sDeptID,'lookupWnd',
 	  		'width=600,height=250,top='+(screen.availHeight-250)/2+',left='+(screen.availWidth-600)/2+',resizable=yes,scrollbars=yes');
		}	
		 
	return false;
}

function setPolicyDataChanged(sFieldName)
{	
  	var objPolicyChangeField=eval('document.frmData.sys_claimpolicychange');
	var sFldname=new String(sFieldName);
	if (objPolicyChangeField!=null)
	{	
		var objCntrlPrimaryPolicy= eval('document.frmData.sys_policyid');
		if (objCntrlPrimaryPolicy!=null)
		{
			var objCntrlpolicyName=eval('document.frmData.'+objCntrlPrimaryPolicy.value);	
 			if (objCntrlpolicyName!=null)
 			{	
				var objElem=eval('document.frmData.'+m_FormID);
				if(objElem!=null)
				{					 
  					if((objPolicyChangeField.value==sFldname)&& (objElem.value!=0))		
 					{ 
 					  if(confirm('Do You want to change attached Policy?'))
					   {					
							lookupClaimPolicy(objCntrlPrimaryPolicy.value);
						}								
					}
				}	
			}
 		}
 	}
	return true;  

}

function searchPolicies(sViewId, sFieldName)
{
	var obj, sPolicyId;
	
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
		
	obj=eval("document.frmData.policyid");
	if(obj!=null)
		sPolicyId=obj.value;
	
	m_codeWindow=window.open('searchpopup.asp?viewid='+sViewId+'&tableid='+sPolicyId,'searchWnd',
		'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');

	return false;
}

function SetClaimPolicy(lPolicyId,sPolicyName,sClassNames)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	if (m_policyWindow!= null && m_policyLoad == true)
	{
	  m_policyWindow.close();		
	  m_policyWindow=null;
	  m_policyLoad =false;
	}   		
	objCtrl=eval('document.frmData.'+m_policyFieldName);
	if(objCtrl!=null)
	{	
		objCtrl.value=sPolicyName;
		objCtrl=eval('document.frmData.'+ m_policyFieldName+"_cid");
		objCtrl.value=lPolicyId;
	}
	
	if (sClassNames!=null)   //called by lookupclaimplan.asp.  must replace class names options
	{
		objCtrl=eval('document.frmData.classname');
		if(objCtrl!=null)
		{
			objCtrl.options.length=0;
			var opt;
			var arr = sClassNames.split("|");
			var TextValue;
			for (var i=0; i<arr.length;i++)
			{
				TextValue=arr[i].split(";");
				opt=new Option(TextValue[1],TextValue[0]);
				objCtrl.options[objCtrl.options.length]=opt;	
			}
		}
	}
	setDataChanged(true);
	
	
	m_policyFieldName="";
	m_codeWindow=null;
	
	return true;
}

function RequestCancel()
{
	var i;
	var obj;
	var arrElements;
	//alert("Cancel Requested: "+m_LookupCancelFieldList+"\nbusy?"+window.m_LookupBusy);
	if(self.document.frmData == null)
		return false;
	arrElements=String(m_LookupCancelFieldList).split("|");
	
	for(i=0;i<arrElements.length;i++)
	{
		obj=null;
		
		if (arrElements[i]!="")
		{
			obj = eval('window.document.frmData.'+arrElements[i]);
			if (obj!=null)
				waitFor("(!window.m_LookupBusy)",'window.DoCancel("'+obj.name+'");');
		}
	}
	return false;
}

function DoCancel(sName)
{
	var obj=eval("document.frmData."+sName);
	
	if (obj.cancelledvalue==null)
		obj.value=obj.defaultValue;
	else
		obj.value=obj.cancelledvalue;
	return true;
}
function RequestRestore(sVal)
{
	var i;
	var obj;
	var objElements;
	if(self.document.frmData == null)
		return false;
	objElements=self.document.frmData.elements;
	//alert("Restore Requested:" + sVal);
	
	//Reset the "wait" object.
	m_AllowRestore = false;
	
	for(i=0;i<objElements.length;i++)
	{
		obj=null;
		obj = objElements[i];
		if (haveProperty(obj,"Previous"))
			if( String(obj.Previous) == String(sVal))
			{	
				obj.cancelledvalue = obj.Previous;//changed by ravi to keep storing previous value 
				waitFor("window.m_AllowRestore",'window.DoRestore('+i+');');
				return true;
			}
	}
		
	return false;
}
function DoRestore(Idx)
{
	//alert("In DoRestore");
	var obj=document.frmData.elements[Idx];
	obj.value=obj.Previous;
	
	//obj.Previous=null;//changed by ravi to set new value during addnew
	return true;
}

function entitySelected(sEntityId,sType)
{
// Move the closing of the codewindow to AFTER the 
// data is loaded.  This allows for correct reset\restore
// of data by the codewindow if user wants to create a 
// new entity\value.  
// Example: Allow create of a new employee record 
// directly from the claim screen when quick-lookup 
// does not find the employee.
//	if(m_codeWindow!=null)
//		m_codeWindow.close();
//	return true;
	
	if(sEntityId!="")
	{
		if(sEntityId!="-1")
		{
			if(m_LookupType<4)
			{
				self.setTimeout("m_Wnd=window.open('getentitydata.asp?entityid="+sEntityId+"&type="+sType+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
				//m_Wnd=window.open('getentitydata.asp?entityid='+sEntityId,'progressWnd',
				//	'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
				document.entityLoaded=entitySelected;
			}
			else if(m_LookupType==4)
			{	self.setTimeout("m_Wnd=window.open('getemployeedata.asp?entityid="+sEntityId+
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
				//m_Wnd=window.open('getemployeedata.asp?entityid='+sEntityId,'progressWnd',
				//	'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
				document.entityLoaded=entitySelected;
			}
			return true;
		}
		else
		{
			var obj=null, obj2=null;
			if(m_LookupType==1 || m_LookupType==4)
			{
				var objElements=m_Wnd.document.frmData.elements;
				// Entity Lookup
				for(i=0;i<objElements.length;i++)
				{
					obj=null;
					obj=eval("document.frmData."+m_sFieldName+objElements[i].name);
					if(obj!=null)
					{
						obj.value=objElements[i].value;
						//BSB 01.18.2003
						//All quicklookup fields are input type="text".
						//Quicklookup hack - record this in the cancelledvalue
						// attribute so that we track what to restore in case of 
						// "no matching codes found".
						obj.cancelledvalue=objElements[i].value; 

					}
				}
			}
			else if (m_LookupType==2)
			{
				// EID Lookup
				obj=eval("m_Wnd.document.frmData.lastfirstname");
				if(obj!=null)
					obj2=eval("document.frmData." + m_sFieldName);
				if(obj2!=null)
				{
					obj2.value=obj.value;
					//BSB 01.18.2003
					//All quicklookup fields are input type="text".
					//Quicklookup hack - record this in the cancelledvalue
					// attribute so that we track what to restore in case of 
					// "no matching codes found".
					obj2.cancelledvalue=obj.value; 
				}
			// Record Id
				obj2=null;
				obj=null;
				obj=eval("m_Wnd.document.frmData.entityid");
				if(obj!=null)
					obj2=eval("document.frmData." + m_sFieldName + "_cid");
				if(obj2!=null)
				{
					obj2.value=obj.value;
					obj2.cancelledvalue=obj.value; 
				}
			}
			else if(m_LookupType==3)//No quick lookup for this..
			{
				var sEntityName, sEntityId;
				obj=eval("m_Wnd.document.frmData.lastfirstname");
				if(obj!=null)
					sEntityName=obj.value;
				obj=null;				
				obj=eval("m_Wnd.document.frmData.entityid");
				if(obj!=null)
					sEntityId=obj.value;
				
				obj2=eval("document.frmData." + m_sFieldName);
				if(obj2!=null)
				{
					var bAdd=true;
					for(var i=0;i<obj2.length;i++)
					{
							if(obj2.options[i].value==sEntityId)
							bAdd=false;
					}
					if(bAdd)
					{
						var objOption = new Option(sEntityName, sEntityId, false, false);
						obj2.options[obj2.length] = objOption;
						obj2=null;
						obj2=eval("document.frmData." + m_sFieldName+"_lst");
						if(obj2!=null)
						{
							if(obj2.value!="" && obj2.value.substring(obj2.value.length-1,1)!=",")
								obj2.value=obj2.value+",";
							obj2.value=obj2.value+sEntityId;
						}
					}					
				}
										
			}
			
			setDataChanged(true);
		}
	}
	if (m_Wnd!=null)
		m_Wnd.close();
	m_Wnd=null;
	m_sFieldName="";
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	m_AllowRestore= true;
	return true;
}

function waitFor(cond,block)
{
	if(eval(cond))
		eval(block);
	else
		self.setTimeout("waitFor('" +cond+"','"+block+"');",10);
	return true;
}
function policySelected(sPolicyId)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	if(sPolicyId!="")
	{
		
		if(sPolicyId!="-1")
		{
			m_Wnd=window.open('getpolicydata.asp?policyid='+sPolicyId,'progressWnd',
				'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
			document.entityLoaded=policySelected;
			return true;
		}
		else
		{
			var obj=null, obj2=null;
			
			// EID Lookup
			obj=eval("m_Wnd.document.frmData.policyname");
			if(obj!=null)
				obj2=eval("document.frmData." + m_sFieldName);
			if(obj2!=null)
				obj2.value=obj.value;
				
			
			// Record Id
			obj2=null;
			obj=null;
			obj=eval("m_Wnd.document.frmData.policyid");
			if(obj!=null)
				obj2=eval("document.frmData." + m_sFieldName + "_cid");
			if(obj2!=null)
				obj2.value=obj.value;
			
			setDataChanged(true);
		}
	}
	m_AllowRestore= true;
	m_Wnd.close();
	m_sFieldName="";
	m_codeWindow=null;
	return true;
}

function doSearch()
{
	if(ConfirmSave()){
		if(!validateForm())
			return false
		else{
			document.frmData.sysCmdConfirmSave.value=1;
			document.frmData.sysCmdQueue.value="doSearch()";
			self.setTimeout('document.frmData.submit();',200);
			return false;
			}
	}
		
	if(m_FormName=="claimgc")
		window.setTimeout("urlNavigate('searchgen.asp?formname="+escape(m_FormName)+"&tableid=gc')", 10);
	else if(m_FormName=="claimwc")
		window.setTimeout("urlNavigate('searchgen.asp?formname="+escape(m_FormName)+"&tableid=wc')", 10);
	else if(m_FormName=="claimva")
		window.setTimeout("urlNavigate('searchgen.asp?formname="+escape(m_FormName)+"&tableid=va')", 10);
	else if(m_FormName=="employee")
		window.setTimeout("urlNavigate('searchgen.asp?formname="+escape(m_FormName)+"')", 10);
	else if(m_FormName=="people")
	{		//BSB 10.21.2002 Pass in enough info to:
			//1.) Filter entity lookup type
			//2.) Come back to correct "people maint" screen
		var sys_ex = document.frmData.peopletype.value; 
		window.setTimeout("urlNavigate('searchgen.asp?formname="+escape(m_FormName)+"&tableid="+escape(document.frmData.entitytableid.value)+"&sys_ex=" + sys_ex + "')", 10);
	}
	else if(m_FormName=="entitymaint")
	{	//tkr 1/2003 add filter of entity lookup type
		window.setTimeout("urlNavigate('searchgen.asp?formname="+escape(m_FormName)+"&tableid="+escape(document.frmData.entitytableid.value)+ "')", 10);
	}
	else
	{
		window.setTimeout("urlNavigate('searchgen.asp?formname="+escape(m_FormName)+"')", 10);
	}
	return true;
}

function doLookup()
{
	if(ConfirmSave()){
		if(!validateForm())
			return false
		else{
			document.frmData.sysCmdConfirmSave.value=1;
			document.frmData.sysCmdQueue.value="doLookup()";
			self.setTimeout('document.frmData.submit();',200);
			return false;
		}
	}

	CloseProgressWindow();
	
	//window.setTimeout("urlNavigate('lookuprun.asp?formname="+escape(m_FormName)+"')", 10);
	var sUrl="urlNavigate('lookup.asp?formname="+escape(m_FormName)
	if(m_FormParentID!="" && m_FormParentID!=null)
	{
		var arrParentId;
		arrParentId=m_FormParentID.split(",");
		for (var i=0; i<arrParentId.length; i++)
			sUrl=sUrl+"&"+arrParentId[i]+"="+getFormValue(arrParentId[i]);
		sUrl=sUrl + "&fpid=" + m_FormParentID;
	}
	
	var sTarget=document.frmData.action;
	if(sTarget.indexOf("?")>=0)
		sTarget=sTarget.substring(0,sTarget.indexOf("?"));
	if(sTarget.indexOf("#")>=0)
		sTarget=sTarget.substring(0,sTarget.indexOf("#"));
	
	sUrl = sUrl + "&aspname=" + sTarget;
	var objTmp=eval("document.frmData.sys_psid");
	if(objTmp!=null)
		sUrl=sUrl+"&psid="+objTmp.value;

	objTmp=eval("document.frmData.sys_sid");
	if(objTmp!=null)
		sUrl=sUrl+"&sid=" + escape(objTmp.value);

	objTmp=eval("document.frmData.sys_ex");
	if(objTmp!=null)
	{
		var s=objTmp.value;
		sUrl=sUrl+"&sys_ex="+s;

		var arr=s.split(",");
		if(arr.length>0)
		{
			for(var i=0;i<arr.length;i++)
			{
				var objFormField=eval("document.frmData."+arr[i]);
				if(objFormField!=null && sUrl.indexOf(arr[i]+"=")<0)
					sUrl=sUrl+"&"+arr[i]+"="+objFormField.value;
			}
		}
	}
	sUrl=sUrl+"')";
	
	window.setTimeout(sUrl, 10);
	return true;
}

function attach()
{
	var sid="";
	var objTmp=eval("document.frmData.sys_sid");
	
	if(objTmp!=null)
		sid=escape(objTmp.value);

	var lCurrentId=getCurrentID();
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	{
		alert("Documents can be attached only to the saved records.");
		return true;
	}
	var sUrl="docatt.asp?tn=" + escape(m_FormName) + "&pid=" + lCurrentId;
	if(sid!="")
		sUrl=sUrl+"&sid=" + sid;
	
	objTmp=eval("document.frmData.sys_deletepage");
	if(objTmp!=null)
		sUrl=sUrl+"&at=1";
	
	var wnd=window.open(sUrl,'attachWnd',
			'width=500,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2 + ',resizable=yes,scrollbars=yes');
	return true;
}

function DeleteRecord()
{	
	var lCurrentId=getCurrentID();
	var objTmp=eval("document.frmData.sys_sid");
	var sid="";

	if(objTmp!=null)
		sid=escape(objTmp.value);
	
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	{
		alert("Please select record to delete.");
		return true;
	}
	
	if(!self.confirm("Are you sure you want to delete this record?"))
		return false;
	
	objElem=eval("document.frmData.sys_deletepage");
	if(objElem!=null && objElem.value!="")
		document.frmData.action=objElem.value;
				
	document.frmData.sysCmd.value=6; // Delete
	self.setTimeout('document.frmData.submit();',200);
	
	return true;
}

function showSummary()
{
	// JP 4.5.2004    var lCurrentId=getCurrentID();
	// JP 4.5.2004    if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	// JP 4.5.2004    {
	// JP 4.5.2004    	alert("To display the summary you have to select or create a record first.");
	// JP 4.5.2004    	return true;
	// JP 4.5.2004    }
	
	// JP 4.5.2004    window.open('admSummary.asp?admtable='+escape(m_FormName)+'&rid='+lCurrentId,'summaryWnd',
	// JP 4.5.2004    		'width=640,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-640)/2 + ',resizable=yes,toolbar=yes,scrollbars=yes');

	// JP 4.5.2004    return true;
			
	return tlbButton('form=esumm;%currentidname%=%currentid%');   // JP 4.5.2004   Adm exec summary now goes to standard exec summary not HTML version.
}

function showSupp()
{
	if(ConfirmSave()){
		if(!validateForm())
			return false
		else{
			document.frmData.sysCmdConfirmSave.value=1;
			document.frmData.sysCmdQueue.value="showSupp()";
			self.setTimeout('document.frmData.submit();',200);
			return false;
		}
	}
	
	var lCurrentId=getCurrentID();
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	{
		alert("To display the supplemental data you have to select or create a record first.");
		return true;
	}
	
	var obj=eval("document.frmData.sys_supplemental");
	var sParams, sLinkParams;	
	if(obj!=null)
		sParams=new String(obj.value);
	else
		return false;
	
	sLinkParams=processURLParams(sParams);
	//Hack To Support People Type on Return from Supp Screen.
	//BSB 02.25.2003
	if(m_FormName=="people")
	{
		var s;
		var obj;
		obj = eval("document.frmData.peopletype");
		if (obj!=null)
			 s = obj.value;
			 
		obj = eval("document.frmData.sys_ex");
		if(obj!=null) //Add to sys_ex as a normal extra param.
 			obj.value = obj.value + ",peopletype";// + document.frmData.peopletype.value;
		else //Must add directly to url.
		{
			if(sLinkParams!="")
				sLinkParams=sLinkParams+"&";

			sLinkParams=sLinkParams+"sys_ex=peopletype&peopletype="+s;
		}
	}
	
	var obj=eval("document.frmData.sys_ex");
	if(obj!=null)
	{
		var s=obj.value;
		if(sLinkParams!="") sLinkParams=sLinkParams+"&";
		sLinkParams=sLinkParams+"sys_ex="+s;
				
		var arr=s.split(",");
		if(arr.length>0)
		{
			for(var i=0;i<arr.length;i++)
			{
				var objFormField=eval("document.frmData."+arr[i]);
				if(objFormField!=null)
				{
					if(sLinkParams!="") sLinkParams=sLinkParams+"&";
					sLinkParams=sLinkParams+arr[i]+"="+objFormField.value;
				}
			}
		}
	}
	
	var objTmp=eval("document.frmData.sys_sid");

	if(objTmp!=null)
	{
		if(sLinkParams!="")
			sLinkParams=sLinkParams+"&";
		sLinkParams=sLinkParams+"sys_psid=" + escape(objTmp.value);
	}

	if(sLinkParams!="")
	{
		sLinkParams=replace(sLinkParams,"&amp;","&");
		window.setTimeout("urlNavigate('supp.asp?"+sLinkParams+"')", 10);
	}
	return true;
}

function dateSelected(sDay, sMonth, sYear)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.frmData.'+m_sFieldName);
	if(objCtrl!=null)
	{
		sDay=new String(sDay);
		if(sDay.length==1)
			sDay="0"+sDay;
		sMonth=new String(sMonth);
		if(sMonth.length==1)
			sMonth="0"+sMonth;
		sYear=new String(sYear);
			
		objCtrl.value=formatDate(sYear+sMonth+sDay);
	}
	m_sFieldName="";
	m_codeWindow=null;
	setDataChanged(true);
	return true;
}

function selectDate(sFieldName)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	
	m_codeWindow=window.open('calendar.html','codeWnd',
		'width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
	return false;
}
/* This function should be dead now...
   I have left it in simply in case I missed a "quick diary" button.
   anywhere in the product. BB 08.16.2002
*/
function QuickDiary()
{
	var sId=getCurrentID();
	if(sId<=0 || sId=="")
	{
		self.alert(EXISTREC_ALERT);
		return false;
	}
	window.open('quickdiary.asp?formname='+m_FormName+"&recordid="+sId,'qdWnd',
		'width=400,height=260'+',top='+(screen.availHeight-260)/2+',left='+(screen.availWidth-400)/2+',resizable=yes,scrollbars=no');
	return true;
}

function Diary()
{
	var sAttSecRecId='';
	var sId=getCurrentID();
	if(sId<=0 || sId=="")
	{
		self.alert(EXISTREC_ALERT);
		return false;
	}
	//tkr 4/2003 make compat with rmworld
	if(m_FormName=='claimant'){
		sAttSecRecId=document.frmData.claimid.value;
		sId=document.frmData.clmntentityid.value;
	}
	if(m_FormName=='piemployee' || m_FormName=='piwitness' || m_FormName=='piother' || m_FormName=='piqpatient' || m_FormName=='pipatient')
		sAttSecRecId=document.frmData.eventid.value;
	
	window.open('attachdiary.asp?formname='+m_FormName+"&recordid="+sId+"&attsecrecid="+sAttSecRecId,'adWnd',
		'width=500,height=500'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	return true;
}

//tkr 1/2003 add NS 4 & 7 compatibility
function eatKeystrokes(evt) {
	if(IE4) {
		if(window.event.keyCode!=9) {
			window.event.returnValue=false;
			return true;
		}
	}
	else {
		evt = (evt) ? evt : ((event) ? event : null);
		if (evt) {
		    var charCode = (evt.charCode || evt.charCode == 0) ? evt.charCode : 
		                   ((evt.keyCode) ? evt.keyCode : evt.which);
		    if (charCode!=9 && charCode!=0) {
		        if (evt.returnValue) {
		            evt.returnValue = false;
		        } else if (evt.preventDefault) {
		            evt.preventDefault();
		        } else {
		            return false;
		        }
		    }
		}
	}
}


// Validates/Formats the phone number field after it loses focus
function phoneLostFocus(objCtrl)
{
	if(objCtrl.value.length==0)
			return false;
	
	var sValue=new String(objCtrl.value);
	sValue=stripNonDigits(sValue);
	
	if(sValue.length<10)
	{
		// Invalid phone number
		objCtrl.value="";
		alert("Invalid phone number. Please enter valid phone number. Example: (734) 462-5800 or (734) 462-580 Ext:9999");
		objCtrl.focus();
		return false;
	}
	
	// Reformat (###) ###-####  Ext:#####
	var sExt=sValue.substr(10);
	if(sExt!="")
		sExt=" Ext:" + sExt;
	sValue="("+sValue.substr(0,3)+")"+" "+sValue.substr(3,3)+"-"+sValue.substr(6,4)+sExt;
	
	objCtrl.value=sValue;
	return true;
}

function phoneGotFocus(objCtrl)
{
	if(objCtrl.value.length==0)
			return false;
	
	var sValue=new String(objCtrl.value);
	sValue=stripNonDigits(sValue);
	var sExt=sValue.substr(10);
	if(sExt!="")
		sExt=" Ext:" + sExt;
	objCtrl.value=sValue.substr(0,10)+sExt;
	objCtrl.select();

}

function stripNonDigits(str) {
	return str.replace(/[^0-9]/g,"")
}

function zipLostFocus(objCtrl)                                                                                                                                       
{                                                                                                                                                                    
	// #####-#### or A#A-#A#(canada) Zip Format	   edited by ravi on 5/2/3                                                                                           
	                                                                                                                                                                 
	if(objCtrl.value.length==0)                                                                                                                                      
			return false;                                                                                                                                            
	if(canadaZip(objCtrl)==true)                                                                                                                                     
	{                                                                                                                                                                
		return true;                                                                                                                                                 
	}                                                                                                                                                                
	                                                                                                                                                                 
	var sValue=new String(objCtrl.value);                                                                                                                            
	var sLetter="";                                                                                                                                                  
	if(sValue.substr(0,1)<'0' || sValue.substr(0,1)>'9')                                                                                                             
		sLetter=sValue.substr(0,1);                                                                                                                                  
		                                                                                                                                                             
		                                                                                                                                                             
	sValue=stripNonDigits(sValue);                                                                                                                                   
	//edited by ravi 05/02/03                                                                                                                                        
	if(((sValue.length==5 || sValue.length==9) && sLetter==""))                                                                                
	{                                                                                                                                                                
		if(sValue.length>5)                                                                                                                                          
		{                                                                                                                                                            
			if(sLetter=="")                                                                                                                                          
				sValue=sValue.substr(0,5)+"-"+sValue.substr(5);                                                                                                      
			//else	ravi                                                                                                                                             
			//sValue=sLetter+sValue.substr(0,4)+"-"+sValue.substr(4);                                                                                                
		}                                                                                                                                                            
		else                                                                                                                                                         
		{                                                                                                                                                            
			if(sLetter!="")                                                                                                                                          
				sValue=sLetter+sValue;                                                                                                                               
		}                                                                                                                                                            
		objCtrl.value=sValue;                                                                                                                                        
		return true;                                                                                                                                                 
	}                                                                                                                                                                
	else                                                                                                                                                             
	{                                                                                                                                                                
		alert("Invalid ZIP code entry. Please enter valid ZIP code.");                                                                                               
		objCtrl.value="";                                                                                                                                            
		objCtrl.focus();                                                                                                                                             
		return false;                                                                                                                                                
	}                                                                                                                                                                
}                                                                                                                                                                    
function canadaZip(obj)                                                                                                                                              
{                                                                                                                                                                    
	if(obj.value.length==0)                                                                                                                                          
	return false;                                                                                                                                                    
	var str=obj.value;                                                                                                                                               
	                                                                                                                                                                 
	if(str.replace(/[a-z][0-9][a-z]-[0-9][a-z][0-9]/i,"")=="")                                                                                                       
		{                                                                                                                                                            
		                                                                                                                                                             
		return true;                                                                                                                                                 
		                                                                                                                                                             
		}                                                                                                                                                            
	else                                                                                                                                                             
		{return false;}                                                                                                                                              
}                                                                                                                                                                    

function ssnLostFocus(objCtrl)
{
	// ###-##-####
	/* 
	if(objCtrl.value.length==0)
			return false;
	var sValue=new String(objCtrl.value);
	sValue=stripNonDigits(sValue);
	if(sValue.length!=9)
	{
		alert("Please enter valid SSN number.");
		objCtrl.value="";
		objCtrl.focus();
		return false;
	}
	objCtrl.value=sValue.substr(0,3)+"-"+sValue.substr(3,2)+"-"+sValue.substr(5,4);
	return true;
	*/
	if(objCtrl.value.length==0)
			return false;
	var sValue=new String(objCtrl.value);
	var sCheck=new String();
	//check is 9 digits
	sCheck=stripNonDigits(sValue);
	if(sCheck.length!=9)
	{
		alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
		objCtrl.value="";
		objCtrl.focus();
		return false;
	}
	//user entered SSN with dashes
	if(sValue.length==11)
	{
		if(sValue.charAt(3)!="-" || sValue.charAt(6)!="-")
		{
			alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
			objCtrl.value="";
			objCtrl.focus();
			return false;
		}
		return true;	
	}		
	
	//user entered a tax id with dashes
	if(sValue.length==10)
	{
		if(sValue.charAt(2)!="-")
		{
			alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
			objCtrl.value="";
			objCtrl.focus();
			return false;
		}
		return true;
	}
	//fall through:  if user did not add dashes, default to SSN ###-##-####
	objCtrl.value=sCheck.substr(0,3)+"-"+sCheck.substr(3,2)+"-"+sCheck.substr(5,4);
	return true;
	
}

function taxidLostFocus(objCtrl)
{
	// ##-#######
	/*
	if(objCtrl.value.length==0)
			return false;
	var sValue=new String(objCtrl.value);
	sValue=stripNonDigits(sValue);
	if(sValue.length!=9)
	{
		alert("Please enter valid Tax ID.");
		objCtrl.value="";
		objCtrl.focus();
		return false;
	}
	objCtrl.value=sValue.substr(0,2)+"-"+sValue.substr(2,7);
	return true;
	*/
	if(objCtrl.value.length==0)
			return false;
	var sValue=new String(objCtrl.value);
	var sCheck=new String();
	//check is 9 digits
	sCheck=stripNonDigits(sValue);
	if(sCheck.length!=9)
	{
		alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
		objCtrl.value="";
		objCtrl.focus();
		return false;
	}
	//user entered SSN with dashes
	if(sValue.length==11)
	{
		if(sValue.charAt(3)!="-" || sValue.charAt(6)!="-")
		{
			alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
			objCtrl.value="";
			objCtrl.focus();
			return false;
		}
		return true;	
	}		
	
	//user entered a tax id with dashes
	if(sValue.length==10)
	{
		if(sValue.charAt(2)!="-")
		{
			alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
			objCtrl.value="";
			objCtrl.focus();
			return false;
		}
		return true;
	}
	//fall through:  if user did not add dashes, default to SSN ###-##-####
	objCtrl.value=sValue.substr(0,2)+"-"+sValue.substr(2,7);
	return true;
}

function numLostFocus(objCtrl)
{
	if(objCtrl.value.length==0)
			return false;
	if(isNaN(parseFloat(objCtrl.value)))
		objCtrl.value="";
	else
		objCtrl.value=parseFloat(objCtrl.value);
	return true;
}

function currencyLostFocus(objCtrl)
{
	var dbl=objCtrl.value;
	
	if(dbl.length==0)
			return false;
			
	if(isNaN(parseFloat(dbl))){
		objCtrl.value="";
		return false;
	}
	
	//round to 2 places
	dbl=parseFloat(dbl);
	dbl=dbl.toFixed(2);
	objCtrl.value=dbl;
	return true;
}

function Comments()
{
	var sId=getCurrentID();
	if(sId<=0 || sId=="")
	{
		self.alert(EXISTREC_ALERT);
		return false;
	}
	var sLink='notes.asp?formname='+m_FormName+"&recordid="+sId;
	
	var objTmp=eval("document.frmData.sys_sid");
	if(objTmp!=null)
		sLink=sLink+"&psid=" + escape(objTmp.value);
	
	window.open(sLink,'qdWnd',
		'width=620,height=450'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	return true;
}

function showJuris()
{
	if(ConfirmSave()){
		if(!validateForm())
			return false
		else{
			document.frmData.sysCmdConfirmSave.value=1;
			document.frmData.sysCmdQueue.value="showJuris()";
			self.setTimeout('document.frmData.submit();',200);
			return false;
		}
	}

	CloseProgressWindow();

	var lCurrentId=getCurrentID();
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	{
		alert("To display the Jurisdictional data you have to select or create a record first.");
		return true;
	}
	
	sLinkParams="syscmd=0&sys_formidname=claim_id&claim_id="+lCurrentId;
		
	var objTmp=eval("document.frmData.sys_sid");
	if(objTmp!=null)
	{
		if(sLinkParams!="")
			sLinkParams=sLinkParams+"&";
		sLinkParams=sLinkParams+"sys_psid=" + escape(objTmp.value);
	}
	
	if(sLinkParams!="")
	{
		sLinkParams=replace(sLinkParams,"&amp;","&");
		window.setTimeout("urlNavigate('juris.asp?"+sLinkParams+"')", 10);
	}
	return true;
}

function EditMemo(sFieldName)
{
	if(sFieldName=="")
		return false;
		
	document.OnEditMemoSave=OnEditMemoSave;
	document.OnEditMemoLoaded=OnEditMemoLoaded;
		
	var objWnd=window.open("memoedit.asp?loaded=OnEditMemoLoaded&fieldname="+escape(sFieldName),"memoWnd",
		'width=620,height=450'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	return true;
}

function OnEditMemoLoaded(objWnd)
{
	var objField=eval("document.frmData."+objWnd.document.frmData.fieldname.value);
	if(objField==null)
		return false;

	objWnd.document.frmData.callback.value="OnEditMemoSave";
	if(objField.value == '')
		objWnd.document.frmData.txtMemo.value= objWnd.document.frmData.txtMemo.value;
	else
		objWnd.document.frmData.txtMemo.value=objField.value + '\n' + objWnd.document.frmData.txtMemo.value;
	
	return true;
}

function OnEditMemoSave(objWnd)
{
	var objNewMemo, objDateTimeStamp;
	
	if(objWnd==null)
		return false;
		
	var objField=eval("document.frmData."+objWnd.document.frmData.fieldname.value);
	if(objField!=null)
	{
		objNewMemo = eval(objWnd.document.frmData.txtNewMemo);
		objDateTimeStamp=eval(objWnd.document.frmData.DateTimeStamp);
		if (objNewMemo!=null)
		{
			if (objDateTimeStamp!=null)
			{
				if(objWnd.document.frmData.txtMemo.value == '' )
					objField.value=objWnd.document.frmData.DateTimeStamp.value + ' ' + objWnd.document.frmData.txtNewMemo.value;
				else
					objField.value=objWnd.document.frmData.txtMemo.value + '\n' + objWnd.document.frmData.DateTimeStamp.value + ' ' + objWnd.document.frmData.txtNewMemo.value;
			}	
			else
				objField.value=objWnd.document.frmData.txtMemo.value + objWnd.document.frmData.txtNewMemo.value;
		}
		else
			objField.value=objWnd.document.frmData.txtMemo.value;
	}
	setDataChanged(true);
	objWnd.close();
	return true;
}

function MailMerge()
{
	//self.alert("Running Mail Merge Test");
	var lCurrentId=getCurrentID();
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	{
		self.alert("To begin a Mail Merge you have to select or create a record first.");
		return false;
	}
	var objWnd=window.open("MergeTemplate1.asp?formname="+escape(m_FormName)+"&recordid="+escape(getCurrentID()),"mergeWnd",'width=620,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	
	return true;
}

function processURLParams(sParams)
{
	var arrParams=sParams.split("&");
	var sLinkParams="";
	for(var i=0;i<arrParams.length;i++)
	{
		var arrNameVal=arrParams[i].split("=");
		if(arrNameVal.length==2)
		{
			if(arrNameVal[1].charAt(0)=="%" && arrNameVal[1].charAt(arrNameVal[1].length-1)=="%")
			{
				// Try to replace it with real value
				var sName = replace(arrNameVal[1],"%","");
				var objFormField=null;
				objFormField=eval("document.frmData."+sName);
				if(objFormField!=null)
					arrNameVal[1]=objFormField.value;
			}
			if(sLinkParams!="")
				sLinkParams=sLinkParams+"&";
			sLinkParams=sLinkParams+arrNameVal[0] + "=" + arrNameVal[1];
		}
		else
		{
			if(sLinkParams!="")
				sLinkParams=sLinkParams+"&";
			sLinkParams=sLinkParams+arrParams[i];
		}
	}
	return sLinkParams;
}

function lookupTextChanged(objField)
{
//	alert("lookupTextChanged");
	if (m_LookupBusy)
	{
		window.alert("The application is busy looking up data for a previous field.\nPlease wait for the results before modifying additional fields.");
		objField.value= objField.cancelledvalue;
		return false;
	}
	setDataChanged(true);
	m_LookupTextChanged=true;
	return true;
}

function lookupLostFocus(objField)
{
	if(m_LookupTextChanged && objField.value!="")
	{
		var objButton=eval("document.frmData."+objField.name+"btn");
		if(objButton!=null)
		{
			m_AutoLookup=true;
			objButton.click();
		}
	}
	else
		m_LookupTextChanged=false;
	
	return true;
}

function recordSummary()
{
	var s = '';
	var lCurrentId=getCurrentID();
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	{
		alert("To print the current record you have to select or create a record first.");
		return true;
	}
	
	//this fills the 3 Request variable used by RequestToParam() in forms.inc
	var sLink='forms.asp?formname='+m_FormName+'&syscmd=0&sys_formpidname='+m_FormID+'&sys_formidname='+m_FormParentID+'&'+m_FormID+'='+lCurrentId+'&RecordSummary=1';
	var objTmp=eval("document.frmData.sys_sid");
	if(objTmp!=null)
	{
		if(sLink!="")
			sLink=sLink+"&";
		sLink=sLink+"sid=" + escape(objTmp.value);
	}
	window.open(sLink,'qdWnd',
		'width=620,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	return true;

}

function CommentSummary()
{
	var lCurrentId=getCurrentID();
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	{
		self.alert("To view a Comment Summary you have to select or create a record first.");
		return false;
	}
	var objWnd=window.open("commentsummary.asp?formname="+escape(m_FormName)+"&recordid="+escape(getCurrentID()),"commentsummaryWnd",'width=620,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	
	return true;
}

function PrintAdjusterText()
{
	var lCurrentId=getCurrentID();
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	{
		self.alert("To view a Adjuster Dated Text you have to select or create a record first.");
		return false;
	}
	if(haveProperty(document.frmData,m_FormParentID))
		var objWnd=window.open("adjustertext.asp?recordid="+escape(document.frmData.adjrowid.value),"adjustertextWnd",'width=620,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	else
	{	self.alert("To view a Adjuster Dated Text you must be on an Adjuster Dated Text type of record.");
		return false;
	}
	
	return true;

}

function EventExplorer()
{
	var lCurrentId=getCurrentID();
	if(lCurrentId==null || lCurrentId=="" || lCurrentId=="0" || m_FormName=="")
	{
		self.alert("To view the Quick Summary you have to select or create a record first.");
		return false;
	}
	
	switch(m_FormName)
	{
		case 'event':

			var objWnd=window.open("eventexplorer.asp?eventid="+escape(getCurrentID()),"eventexplorerWnd",'width=620,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
			return true;
			break;	

		default:

			var lEventId = document.frmData.ev_eventid.value;
			var objWnd2=window.open("eventexplorer.asp?eventid="+lEventId+"&claimid="+lCurrentId,"eventexplorerWnd",'width=620,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
			return true;
			break;
	}
}

function ClaimInstructions()
{
	var l;
	var objElem=eval('document.frmData.ev_depteid_cid');
	if(objElem!=null)
		l = objElem.value;
		
	if(l==0 || isNaN(l) || l==null)
		return false;
	
	var objWnd2=window.open("instructions.asp?lEid="+l,"instrWnd",'width=620,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	return true;
	
}

function goToDoc(sURL,sTarget)
{
	document.frmData.target=sTarget;
	document.frmData.action=sURL;
	window.setTimeout("document.frmData.submit()", 10);
}

//Pankaj- 01/Apr/2004 PatriotProtector check
//function to check if the mandatory fields are filled and then
//opens a new window with the new page if the mandatory field has value

function LinkHandler(sLinkTo,sParams)
{
	var sLinkParams=new String();
	var sp=new String(sParams);
	var arrParams=sp.split("&");
		
	for(var i=0;i<arrParams.length;i++)
	{
		var arrNameVal=arrParams[i].split("=");
		if(arrNameVal.length==2)
		{
			if(arrNameVal[1].charAt(0)=="%" && arrNameVal[1].charAt(arrNameVal[1].length-1)=="%")
			{
				// Try to replace it with real value
				var sName = replace(arrNameVal[1],"%","");
				var objFormField=null;
				objFormField=eval("document.frmData."+sName);
				if (objFormField.required=="yes" && objFormField.value=="")
				{
					alert(objFormField.article1 + " " + objFormField.title + " is required to perform " + objFormField.article2 + " " + objFormField.LinkTitle + ".  \nPlease enter a "+ objFormField.title + " and try again.");
					return false;
				}
				if(objFormField!=null)
					{
					arrNameVal[1]=objFormField.value;					    
					}
				else
					arrNameVal[1]="";
			}
			if (objFormField.required=="yes" && objFormField.value=="")
				{
					alert(objFormField.article1 + " " + objFormField.title + " is required to perform " +  objFormField.article2 + " " + objFormField.LinkTitle + ".  \nPlease enter a "+ objFormField.title + " and try again.");
					return false;
				}				
			if(sLinkParams!="")
				sLinkParams=sLinkParams+"&";
			sLinkParams=sLinkParams+arrNameVal[0] + "=" + escape(arrNameVal[1]);
		}
		else
		{
			if (objFormField.required=="yes" && objFormField.value=="")
				{
					alert(objFormField.article1 + " " + objFormField.title + " is required to perform " +  objFormField.article2 + " " + objFormField.LinkTitle + ".  \nPlease enter a "+ objFormField.title + " and try again.");
					return false;
				}			
			if(sLinkParams!="")
				sLinkParams=sLinkParams+"&";
			sLinkParams=sLinkParams+arrParams[i];
		}
	}

	if(sLinkParams!="")
			sLinkTo=sLinkTo + "?" + sLinkParams;
	sLinkTo=replace(sLinkTo,"&amp;","&");		
	//open new window
	window.open(sLinkTo,'','resizable=yes,scrollbars=yes');
}

//-- Rajeev -- 04/06/04 -- Tab Implementation
function tabChange(objname)
{
	document.frmData.hTabName.value = objname;	
	for(i=0;i<document.all.length;i++)
	{
		if(document.all[i].name != null)
			{
			if(document.all[i].name.substring(0,7) == 'FORMTAB')
				document.all[i].style.display = "none";
			if(document.all[i].name.substring(0,4) == 'TABS')
				document.all[i].className = "NotSelected";
			if(document.all[i].id.substring(0,8) == 'LINKTABS')
				document.all[i].className = "NotSelected1";
			}
	}
	eval('document.all.FORMTAB'+objname+'.style.display=""');
	eval('document.all.TABS'+objname+'.className="Selected"');
	eval('document.all.LINKTABS'+objname+'.className="Selected"');

	var iCount = 0;
	for(i=0;i<document.all.length;i++)
	{
		if(document.all[i].name != null)
		{
			if(document.all[i].name.substring(0,7) == 'FORMTAB')
			{
				if (document.all[i].style.display=="")
				{
					if(document.frmData.sys_focusfields.value != '')
					{
						var sFocusField=new String(document.frmData.sys_focusfields.value);
						var arr=sFocusField.split("|");
						try // if more tabs then provided in XML -- to be handled along with the origin of Tab
						{	
							objElem=eval('document.frmData.'+arr[iCount]);
					
							if((objElem.type=="text") || (objElem.type=="select-one") || (objElem.type=="textarea"))
							{
								if(objElem.disabled==false)
									objElem.focus();
							}
						}
						catch(e){}
					}
					break;
				}
				iCount++;
			}
		}
	}

	return true;
}

function onLeaseVehicle()
{
	if(document.frmData.leaseflag.checked)
	{
		document.frmData.leasingcoeid.readOnly = false;
		document.frmData.leasenumber.readOnly = false;
		document.frmData.leaseterm.readOnly = false;
		document.frmData.leaseexpiredate.readOnly = false;
		document.frmData.leaseamount.readOnly = false;		
	}
	else
	{
		document.frmData.leasingcoeid.value = "";
		document.frmData.leasingcoeid.readOnly = true;
		
		document.frmData.leasenumber.value = "";
		document.frmData.leasenumber.readOnly = true;
		
		document.frmData.leaseterm.value = "";
		document.frmData.leaseterm.readOnly = true;
		
		document.frmData.leaseexpiredate.value = "";
		document.frmData.leaseexpiredate.readOnly = true;
		
		document.frmData.leaseamount.value = "";		
		document.frmData.leaseamount.readOnly = true;		
	}
}

function OSHARecordabilityWizard()
{
	if(m_DataChanged)
	{
		alert("Please save the current record to use the OSHA Recordability Wizard.");
		return false;
	}
	
	var sEventID = document.frmData.eventid.value;
	var sRecordableFlag = "0";
	if( document.frmData.recordableflag.checked )
	{
		sRecordableFlag = "1";
	}
	var sURL = "osharecordabilitywizard.asp?eventid=" + sEventID + "&recordableflag=" + sRecordableFlag;
	var objWnd=window.open(sURL, "OshaRcdWizardWnd",'width=620,height=500'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-620)/2+',resizable=yes,scrollbars=yes');
	
	return true;
}

function OpenAddDialog()
{

	

}

function GetQueryString(szName)
// Get querystring variable with Javascript
{
	var szQueryString = window.location.search.substring(1); // Get rid of the quesion mark ?
	if(szQueryString.length > 0)
	{
		szQueryString = szQueryString.split("&");
		
		for (i=0; i < szQueryString.length; i++)
		{
			var temp = szQueryString[i].split("=");
			if (temp[0].toUpperCase() == szName.toUpperCase())
			{
				return temp[1].replace("+", " ");
			}
		}
    }
    return "";
}

function padZero(val)
{
	if (val < 10)
	{
		val = "0" + val;
	}
	return val.toString();
}


function GetDttm()
{
	var DttmRcd = new String();
	dt = new Date();
	DttmRcd = dt.getFullYear();

	DttmRcd += (padZero(dt.getMonth() + 1));
	DttmRcd += (padZero(dt.getDate()));
	DttmRcd += (padZero(dt.getHours()));
	DttmRcd += (padZero(dt.getMinutes()));
	DttmRcd += (padZero(dt.getSeconds()));

	return DttmRcd;
}

//AP - 12/22/2004, MITS 5229
function OnFormSubmitOrg()
{
	var ret=true;
	
	if(!m_DataChanged && document.frmData.sysCmd.value=='5')
		return false;








		
	if(document.frmData.sysCmd.value=='5')
		ret=validateForm();
	else if(m_DataChanged)
		{
			if(!validateForm())
				return false;
			else
				{
					return true;
				}			
			return false;								
		}
	self.onerror=null;	
	return ret;
}

function DeleteOrgHierarchy()
{			
	var EntityID;
	var ret;
	ret = confirm("Do you really want to delete the record");	
	if(ret)
	{
		EntityID=document.frmData.EntityID.value;			
		location.href="DeleteOrgHierarchy.asp?EntityID=" + EntityID;				
		return ret;
	}

}

function submitReport()
{		
	if(OnFormSubmitOrg())
		{			
		var EntityID;	
		document.frmData.identify.value=1 ;	
		validateForm();
		document.frmData.submit();	
		EntityID=document.frmData.EntityID.value;		
		return true;
		}
}

function addSubmitReport()
{
	if(OnFormSubmitOrg())
		{			
		var EntityID,EntityTableID;	
		EntityTableID=document.frmData.EntityTableID.value;			
		if(EntityTableID=="1005")	
		{
			if (frmData.departmentname.value=="" || frmData.abbreviation.value=="")
				{
				alert("Please enter all the required fields and save the data.");
				return;
				}
		}			
		else
		{
			if (frmData.departmentname.value=="" || frmData.abbreviation.value=="" || frmData.parentoperation.value==0)
				{
				alert("Please enter all the required fields and save the data.");
				return;
				}
		}	
		document.frmData.identify.value=1 ;	
		validateForm();
		document.frmData.submit();	
		EntityID=document.frmData.EntityID.value;		
		return true;
		}
}
