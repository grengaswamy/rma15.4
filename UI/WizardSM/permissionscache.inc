<script language="VBScript" runat="server">
' Author: Denis Basaric, 01/2001

' Refresh the permissions lookup table. Call this function after user logs in
' Caller should determine if the call is needed (Module Security turned on or not)
' Cache will be refreshed only
Public Sub LoadPermissions(lDSNID, sDSN)
	Dim objLookup
	Dim objFso, objFile, sFileName
	Dim objDB, lEnv, iDB, iRS
	Dim lGroupId, lFuncId
	Dim sLastChanged, s
	
	sFileName=Application(APP_DATAPATH)
	sLastChanged="" & Application(APP_PERMLASTCHANGED & lDSNID)
	
	Application.Lock
	
	If Right(sFileName,1)<>"\" Then sFileName=sFileName & "\"
	sFileName=sFileName & "tmpperm" & lDSNID & ".txt"
	
	Set objDB = CreateObject("DTGRocket")
	lEnv = objDB.DB_InitEnvironment()
	iDB = objDB.DB_OpenDatabase(lEnv, sDSN, 0)
	
	' Make sure that we need to refresh the cache	
	' Query Glossary
	iRS = objDB.DB_CreateRecordset(iDB,"SELECT DTTM_LAST_UPDATE FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='GROUP_PERMISSIONS'", 3, 0)
	If Not objDB.DB_Eof(iRS) Then
		objDB.DB_GetData iRS, 1, s
		s="" & s
		If IsNumeric(s) And s<>"" And IsNumeric(sLastChanged) And sLastChanged<>"" Then
			If s<=sLastChanged Then
				' Clean up and exit
				Application.Unlock
				objDB.DB_CloseRecordset CInt(iRS), 2
				objDB.DB_CloseDatabase CInt(iDB)
				objDB.DB_FreeEnvironment CLng(lEnv)
				Set objDB = Nothing
				Exit Sub
			End If
		End If
		Application(APP_PERMLASTCHANGED & lDSNID)=s
	End If
	objDB.DB_CloseRecordset CInt(iRS), 2
	iRS=0	
	
	Set objFso = CreateObject("Scripting.FileSystemObject")
	Set objFile = objFso.CreateTextFile(sFileName,True)
	
	iRS = objDB.DB_CreateRecordset(iDB,"SELECT GROUP_ID,FUNC_ID FROM GROUP_PERMISSIONS", 3, 0)
	Do While Not objDB.DB_Eof(iRS)
		' Write them to file
		objDB.DB_GetData iRS, 1, lGroupId
		objDB.DB_GetData iRS, 2, lFuncId
		
		objFile.WriteLine lGroupId & "-" & lFuncId & ",1"
		
		objDB.DB_MoveNext iRS
	Loop
	
	objDB.DB_CloseRecordset CInt(iRS), 2
	
	' Close file
	objFile.Close
	Set objFile=Nothing
	
	objDB.DB_CloseDatabase CInt(iDB)
	objDB.DB_FreeEnvironment CLng(lEnv)
	Set objDB = Nothing
	
	Set objLookup=CreateObject("IISSample.LookupTable")
	
	' String keys, Int values
	objLookup.LoadValues sFileName,1
	
	Set Application(APP_PERMCACHE & lDSNID)=objLookup
	Application.Unlock
	
	On Error Resume Next
	objFso.DeleteFile sFileName
	
End Sub

</script>