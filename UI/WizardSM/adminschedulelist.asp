<!-- #include file ="sessionInitialize.asp" -->
<!-- #include file ="dbutil.inc" -->
<%
Public objSession
initSession objSession
ExpirePage
Dim lScheduleType
Public hRS
Dim sSQL, v, lScheduleId, lReportId, sClass,sUserName,sUser,lUserId 'Add by Rakhel for editable report mits:33563 
Dim lCount, sAction
Dim sOrderBy, bChecked
lCount=0
sAction=""

If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" And Request("scheduleids")<>"" And Request("action")& "" = "delete" Then
	If LCase(Request("action"))="delete" Then
		sSQL="DELETE FROM SM_SCHEDULE WHERE SCHEDULE_ID IN (" & Request("scheduleids") & ")"
		OpenSMDatabase
		objSMRocket.DB_SQLExecute hSMDB,sSQL
	End If
End If

Dim vArrScheduleIds
Function IsChecked(lScheduleId)
	Dim Count
	IsChecked=false
	'Build Selected Schedule Id Array if necessary.
	If Not IsArray(vArrScheduleIds) Then vArrScheduleIds = Split(Replace(Request("scheduleids")," ",""),",")
	'If no results then nothing is selected and we can say "not checked"
	If Not IsArray(vArrScheduleIds) Then Exit Function
	For Count = LBound(vArrScheduleIds) to Ubound(vArrScheduleIds)
		If lScheduleId =  Clng("0" & vArrScheduleIds(Count)) Then IsChecked=true : Exit Function
	Next	
	
End Function 
%>
<html>
<head>
	<title>Scheduled Reports</title>
	<!--[Rajeev 11/20/2002 CSS Consolidation] link rel="stylesheet" href="../forms.css" type="text/css"/-->
	<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<script language="JavaScript">
var ns, ie, ieversion;
var browserName = navigator.appName;                   // detect browser 
var browserVersion = navigator.appVersion;
if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}

function ValidateDelete()
{
	if(document.frmData.schedulecount.value==0)
		return false;
	if(document.frmData.schedulecount.value==1)
	{
		if(document.frmData.scheduleids.checked)
		{
			document.frmData.action.value="delete";
			return self.confirm("Are you sure you want to delete selected schedule(s)?");
			//var ret = confirm("Are you sure you want to delete selected schedule(s)?");
			//if(ret == false)
			//{
			//	document.location="adminschedulelist.asp"
			//}
			//else
			//{
			//	return true;
			//}
		}
		return false;
	}
	
	for(var i=0;i<document.frmData.scheduleids.length;i++)
	{
		if(document.frmData.scheduleids[i].checked)
		{
			document.frmData.action.value="delete";
			return self.confirm("Are you sure you want to delete selected schedule(s)?");
		}
	}
	alert("Please select the schedule you would like to delete.");
	return false;
}

function onClickEdit()
{
	var selected, selectedindex;

	selected=0;	
	selectedindex=0;
	
	if(document.frmData.schedulecount.value==0)
		return false;

	for(var i=0;i<document.frmData.scheduleids.length;i++)
	{
		if(document.frmData.scheduleids[i].checked)
		{
			selected=selected + 1;
			selectedindex = i;
		}
	}

	if(selected==1)
	{
		if(document.frmData.scheduleids[selectedindex].checked)
		{
			window.setTimeout("window.location.href='adminscheduledetail.asp?scheduleid=" + document.frmData.scheduleids[selectedindex].value + "&viewonly=0'",100);
			return true;
		}
		return false;
	}
	else
	{
		if (selected==0)
		{
			if(document.frmData.scheduleids.checked)
			{
				window.setTimeout("window.location.href='adminscheduledetail.asp?scheduleid=" + document.frmData.scheduleid.value + "&viewonly=0'",100);
				return true;
			}
		}
		else
			alert("Cannot Edit multiple schedules.");
			
		return false;
	}
}

function onPageLoaded()
{
	if (ie)
	{
		if ((eval("document.all.divForms")!=null) && (ieversion>=6))
			divForms.style.height=window.frames.frameElement.Height*0.75;
	}
	else
	{
		var o_divforms;
		o_divforms=document.getElementById("divForms");
		if (o_divforms!=null)
		{
			o_divforms.style.height=window.frames.innerHeight*0.72;
			o_divforms.style.width=window.frames.innerWidth*0.995;
		}
	}
}
</script>
</head>
<body class="Margin0" onload="onPageLoaded()">
<form name="frmData" method="post" action="adminschedulelist.asp">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr><td class="msgheader" colspan="8">Reports Administration</td></tr>
<tr><td class="ctrlgroup" colspan="8">Scheduled Reports</td></tr>
</table>
<div id="divForms" class="divScroll">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr class="colheader3">
	<td><a onclick="document.frmData.orderby.value='report_name';document.frmData.submit();" class="tablink" href="#">Report Name</a></td>
	<td><a onclick="document.frmData.orderby.value='schedule_type';document.frmData.submit();" class="tablink" href="#">Schedule Type</a></td>
	<td><a onclick="document.frmData.orderby.value='next_run_date';document.frmData.submit();" class="tablink" href="#">Next Run</a></td>
	<td><a onclick="document.frmData.orderby.value='last_run_dttm';document.frmData.submit();" class="tablink" href="#">Last Run</a></td>
    <td><a onclick="document.frmData.orderby.value='USER_ID';document.frmData.submit();" class="tablink" href="#">UserId (User)</a></td><!-- Add by Rakhel for editable report mits:33563 End !-->
	<td></TD>
</tr>

<%

OpenSMDatabase

sSQL="SELECT SCHEDULE_ID, SM_SCHEDULE.REPORT_ID, SM_REPORTS.REPORT_NAME, SCHEDULE_TYPE, LAST_RUN_DTTM, NEXT_RUN_DATE, START_TIME,JOB_DESC,USER_ID FROM SM_SCHEDULE,SM_REPORTS " 'Add by Rakhel for editable report mits:33563
sSQL = sSQL & " WHERE SM_SCHEDULE.REPORT_ID=SM_REPORTS.REPORT_ID "
If UCASE(Request("orderby")) & "" = "" Then 
	sSQL = sSQL & " ORDER BY SM_REPORTS.REPORT_NAME" ' NEXT_RUN_DATE"
Else
	sSQL = sSQL & " ORDER BY " & uCase(Request("orderby")) & ""
End If

hRS = objSMRocket.DB_CreateRecordset(hSMDB, sSQL, 3, 0)

If objSMRocket.DB_Eof(hRS) Then
	Response.Write "<tr><td colspan=""4"" align=""center""><br /><br /><br /><br /><i>There are no Reports scheduled to run</i><br /><br /><br /><br /><br /></td></tr>"
End If

sClass="datatd"
Do While Not objSMRocket.DB_Eof(hRS)
	objSMRocket.DB_GetData hRS, "SCHEDULE_ID", v
	lScheduleId=CLng(v)
	bChecked = IsChecked(lScheduleId)
	
    objSMRocket.DB_GetData hRS, "USER_ID", v
	lUserId=CLng(v)
    objSMRocket.DB_GetData hRS, "JOB_DESC", v
    If "" & v<>"" Then
        sUserName=Split(v,"by user")
        sUser=Split(sUserName(1),".")
	End If
	
	objSMRocket.DB_GetData hRS, "REPORT_ID", v
	lReportId=CLng(v)
	objSMRocket.DB_GetData hRS, "REPORT_NAME", v
	If "" & v="" Then v="Report Nr. " & lReportId
	Response.Write "<tr class=""" & sClass & """>"
	Response.Write "<td><input type=""checkbox"" " 
	If bChecked  Then Response.Write "checked" 
	Response.Write " name=""scheduleids"" value=""" & lScheduleId & """ /> <a class=""LIGHTBOLD"" href=""adminscheduledetail.asp?scheduleid=" & lScheduleId &"&UserIdForAdmin="& lUserId &"&ReportUser="& CStr(sUser(0)) & "&viewonly=0&reportid=" & lReportId & """>" & v & "</a></td>" 'Add by Rakhel for editable report mits:33563 
	objSMRocket.DB_GetData hRS, "SCHEDULE_TYPE", v
	If v=1 Then Response.Write "<td>Daily</td>" Else Response.Write "<td>Monthly</td>"
	objSMRocket.DB_GetData hRS, "NEXT_RUN_DATE", v
	Response.Write "<td>"
	if "" & v<>"" Then
		Response.Write FormatDateTime(DateSerial(Left(v,4),Mid(v,5,2),Mid(v,7,2)),vbGeneralDate) & " "
		objSMRocket.DB_GetData hRS, "START_TIME", v
		If "" & v<>"" Then Response.Write FormatDateTime(TimeSerial(Left(v,2),Mid(v,3,2),Mid(v,5,2)),vbLongTime)
	End If
	Response.Write "</td>"
	objSMRocket.DB_GetData hRS, "LAST_RUN_DTTM", v
	
	Response.Write "<td>"
	If "" & v<>"" Then
		Response.Write FormatDateTime(DateSerial(Left(v,4),Mid(v,5,2),Mid(v,7,2)),vbGeneralDate) & " " & FormatDateTime(TimeSerial(Mid(v,9,2),Mid(v,11,2),Mid(v,13,2)),vbLongTime)
	End If
	Response.Write "</td>"
	
    'Add by Rakhel for editable report mits:33563 start
	Response.Write "<td>"
	If "" & sUser(0)<>"" Then
        Response.Write lUserId & " ("& Trim(sUser(0)) & ")" 
	End If
	Response.Write "</td>"
   ' Add by Rakhel for editable report mits:33563 End

	'Response.Write "<td>"
	'Response.Write "<a href=""scheduledetail.asp?scheduleid=" & lScheduleid & "&viewonly=0""" & ">Edit</a>"
	'Response.Write "</tr>"	
	objSMRocket.DB_MoveNext hRS
	If sClass="datatd" Then sClass="datatd1" Else sClass="datatd"
	lCount=lCount+1
Loop
objSMRocket.DB_CloseRecordset CInt(hRS), 2

CloseSMDatabase
%>

</table>
</div>
<input type="hidden" name="orderby" value="" />
<input type="hidden" name="schedulecount" value="<%=lCount%>" />
<input type="hidden" name="action" value="<%=sAction%>" />
<input type="hidden" name="scheduleid" value="<%=lScheduleId%>" />
<%If lCount>0 Then%>
	<input type="submit" class="button" ONCLICK="return ValidateDelete()" value=" Delete " / id=submit1 name=submit1>
<%End If%>
</form>
</body>
</html>