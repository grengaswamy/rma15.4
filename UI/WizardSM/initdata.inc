<script language="VBScript" runat="server">
' These procedures are factored out here to ensure that 
' they are uniform across all usages and that thereby, the 
' VB Code behind them is always fully initialized.
' We have been running into intermittent (difficult to reproduce)
' situations where code works or fails because
' a previous page on the same thread already initialized bas module
' (thread local storage) variables and they are assumed to be valid 
' in the next request - a definite no-no.  This leads to customer complaints
' about farcical security warnings and other strange behavior.
' After cleaning up the VB Object(s) to zero out these variables on each object 
' creation, it has become obvious that many pages are missing full initialization 
' code.
' Author: Brian Battah, 11/06/2003


'This one is in use from forms.asp and all GetXXXData.asp pages.
Sub initCData(ByRef objData)
	objData.DataPath=Application(APP_DATAPATH)
	objData.DSN=objSessionStr(SESSION_DSN)
	objData.SecurityDSN=Application(APP_SECURITYDSN)
	objData.UserLoginName=objSessionStr(SESSION_LOGINNAME)
	If objSessionStr(SESSION_MODULESECURITY)="1" Then
		objData.ModuleSecurity=True
		Set objData.SecurityCache=Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID))
		objData.SecurityGroupId=objSessionStr(SESSION_GROUPID)
		On Error Resume Next 'If these do not get set I'm not overly concerned. They are carried in XML as well...
		objData.SecurityId=lSecurityId
		objData.ParentSecurityId=lParentSecurityId
		If Request("sid")<>"" And IsNumeric(Request("sid")) And lSecurityId=0 Then objData.Sid=CLng(Request("sid"))
		On Error Goto 0
	End If
End Sub

' These are currently 1-off's so there isn't alot of point moving them here.
' However, if use of these VB components spreads to more ASP pages, centralizing these 
' will become more important. 
Sub initCSuppForms(ByRef objData)
End Sub

Sub initCJuris(ByRef objData)
End Sub

Sub initCADMForms(ByRef objData)
End Sub
</script>