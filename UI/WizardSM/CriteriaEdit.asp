<%@ Language=VBScript %>
<!-- #include file ="session.inc" -->
<!-- #include file ="CustomCriteria.inc" -->
<!-- #include file ="dbutil.inc" -->

<%OPTION EXPLICIT
Public objSession
initSession objSession
Response.Expires = 10
'ExpirePage

Function IIf(bBool, bTrue, bFalse)
	if bBool then IIf = bTrue else IIf = bFalse
End Function

'Handle form submission.
If UCase(Request.ServerVariables("REQUEST_METHOD")) = "POST" then
	Dim sHTTP,sPath,l, errorHTML, sURL
	OpenSMDatabase
	If ucase(Request.ServerVariables("HTTPS")) = "ON" then
		sHTTP = "https://" 
	Else
		sHTTP = "http://" 
	End If
	sPath=Request.ServerVariables("SCRIPT_NAME")
	l=InStrRev(sPath,"/")
	If l>0 Then
		sPath=Left(sPath,l)
	Else
		sPath="/"
	End If

	'SELECT CASE BASED ON REPORT TYPE HERE. [FORM SUBMISSION]
	sURL = sHTTP & Request.ServerVariables("SERVER_NAME") & sPath & "smrepserve.asp"
	If runReportQueued(objSessionStr("LoginName")& "", objSessionStr("UserID") & "", objSessionStr("DSN")& "", Application("SM_DSN")& "",objSessionStr(SESSION_DOCPATH)& "", sURL, errorHTML) then
		Response.Redirect "smrepqueue.asp"
	Else 'Handle form submission error.%>
		<html><head><title></title>
		<!--[Rajeev 11/25/2002 CSS COnsolidation] link rel="stylesheet" href="sm.css" type="text/css"/-->
		<link rel="stylesheet" href="RMNet.css" type="text/css">
		</head>
		<body class="10pt">
		<b><font color="red">There were errors in the submitted report criteria. Please hit the Back button on your browser to correct the mistakes and submit again.<br/><br/></font></b>
		<%=errorHTML%>
		</body></html>
<%	End If
	CloseSMDatabase
	Response.End
End If
Dim sWhere, sReportName, sReportXML
'Not a form submission (Generate the Criteria Edit Form)
sWhere = "SM_REPORTS.REPORT_ID = " & Request("reportid") & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND SM_REPORTS_PERM.USER_ID = " & objSessionStr("UserID")
sReportName = GetSingleSMValue("REPORT_NAME","SM_REPORTS, SM_REPORTS_PERM",sWhere)
sReportXML = GetSingleSMValue("REPORT_XML ","SM_REPORTS, SM_REPORTS_PERM",sWhere)

'01/04/07 REM   UMESH
		Dim	 objectXML
		Dim  bNotifyLink, bNotifyAttach, bNotifyEmail, iNotifyDefault, bRunDTTM
		Dim  sXML, sNotifyNone_Text,   sRunDTTM_Text, sNotifyLink_Text, sNotifyEmbed_Text, sNotifyOnly_Text,  sRunImmediate_Text, sRunDefault
		
		 bNotifyLink=bNOTIFY_LINK 
		 bNotifyAttach=bNOTIFY_EMBED 
		 bNotifyEmail=bNOTIFY_ONLY 
		 iNotifyDefault=NOTIFY_DEFAULT 
		 bRunDTTM=bRUN_DTTM
		 sNotifyNone_Text=NOTIFYNONE_TEXT   
		 sRunDTTM_Text=RUNDTTM_TEXT 
		 sNotifyLink_Text=NOTIFYLINK_TEXT 
		 sNotifyEmbed_Text=NOTIFYEMBED_TEXT 
		 sNotifyOnly_Text=NOTIFYONLY_TEXT 
		 sRunImmediate_Text=RUNIMMEDIATE_TEXT
		 sRunDefault=RUN_DEFAULT
		
		OpenSessDatabase
		sWhere = "FILENAME='customize_custom'"
		sXML = GetSingleSessValue("CONTENT","CUSTOMIZE",sWhere)
		CloseSessDatabase
		if sXML<>"" then
			Set objectXML = CreateObject("Microsoft.XMLDOM")
			if (objectXML.loadXML(sXML) = false) then
			Response.Write "Error: Selected Customize File could not be loaded.  Please try uploading it again."
			Response.End
			end if
			
			bNotifyLink = objectXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailwithLink/show").text
			if bNotifyLink ="" then bNotifyLink="0"
			sNotifyLink_Text = objectXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailwithLink/text").text
			
			bNotifyAttach = objectXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailwithReport/show").text
			if bNotifyAttach ="" then bNotifyAttach="0"
			sNotifyEmbed_Text= objectXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailwithReport/text").text
			
			bNotifyEmail = objectXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailOnly/show").text
			if bNotifyEmail ="" then bNotifyEmail="0"
			sNotifyOnly_Text = objectXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailOnly/text").text
			
			sNotifyNone_Text = objectXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/None/text").text
			
			bRunDTTM = objectXML.SelectSingleNode("//RMAdminSettings/ReportRunOptions/AtSpecificDate/show").text
			if bRunDTTM ="" then bRunDTTM="0"
			sRunDTTM_Text = objectXML.SelectSingleNode("//RMAdminSettings/ReportRunOptions/AtSpecificDate/text").text
			
			sRunImmediate_Text = objectXML.SelectSingleNode("//RMAdminSettings/ReportRunOptions/Immediately/text").text
			iNotifyDefault = objectXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/DefaultOption").text
			if iNotifyDefault="" then iNotifyDefault="3"
			sRunDefault = objectXML.SelectSingleNode("//RMAdminSettings/ReportRunOptions/DefaultOption").text
			if sRunDefault="" then sRunDefault="0"
		end if
		'END REM   UMESH
%>
<html>
	<head>
		<title><%=sReportName%></title>
		<!--[Rajeev 11/25/2002 CSS COnsolidation] link rel="stylesheet" href="forms.css" type="text/css"/-->
		<link rel="stylesheet" href="RMNet.css" type="text/css" />
		<script language="JavaScript" SRC="smi.js"></script>
		<script language="JavaScript" SRC="form.js"></script>
	</head>
	<body class="10pt" onload="return pageLoaded();">
		<form name="frmData" action="criteriaedit.asp" method="post" onsubmit="return OnSubmitForm();" ID="Form1">
			<table border="0" cellspacing="0" cellpadding="4" width="100%" ID="Table1">
				<tr>
					<td colspan="4" class="ctrlgroup"><%=sReportName%></td>
				</tr>
			</table>
			<table border="0" width="100%" ID="Table2">
				<tr>
					<td align="center"><input type="submit" value="Run Report" class="button" ID="Submit1" NAME="Submit1"></input></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" width="100%" ID="Table3">
				<tr>
					<td colspan="2" class="ctrlgroup">Report Info</td>
				</tr>
				<tr>
					<td>Report Title</td>
					<td><input name="reporttitle" type="text" size="100" value="" ID="Text1"></input></td>
				</tr>
				<tr>
					<td>Report Subtitle</td>
					<td><input name="reportsubtitle" type="text" size="100" value="" ID="Text2"></input></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" width="100%" ID="Table4">
				<tr>
					<td valign="top">Optional Output Name</td>
					<td><input name="outputfilename" type="text" size="20" value=""></input><br /><i>(Note: OutputFileName=[OutputName]+[UniqueId]+[.Extension])</i></td>
				</tr>
				<tr>
					<td>Output Type</td>
					<td><%=GenerateAvailFormats(Clng(Request("reportid")))%></td>
				</tr>
				
				<tr>
					<td>Run Report</td>
					<td><select name="execreport" height="1" ID="Select2">
						<option value="now" <%=iif(sRunDefault=0,"selected=""""","")%>><%=sRunImmediate_Text%></option>
						<% If CBool(bRunDTTM) Then %>
							<option value="dttm" <%=iif(sRunDefault=1,"selected=""""","")%>><%=sRunDTTM_Text%></option>
						</select>&nbsp;&nbsp;&nbsp;Start Date:<input name="execdate" size="15" type="text" ID="Text3"></input>&nbsp;&nbsp;Start 
						Time:<input name="exectime" size="15" type="text" ID="Text4"></input></td>
						<% End If %>
				</tr>
				<tr>
					<td>Notification Type</td>
					<td><select name="notifytype" height="1" ID="Select3">
						<%If CBool(bNotifyLink) Then %> 
						<option value="notifylink" <%=iif(iNotifyDefault=0,"selected=""""","")%>><%=sNotifyLink_Text%></option>
						<% End If%>
						<%If CBool(bNotifyAttach) Then %> 
						<option value="notifyembed" <%=iif(iNotifyDefault=1,"selected=""""","")%>><%=sNotifyEmbed_Text%></option>
						<% End If%>
						<%If CBool(bNotifyEmail) Then %> 
						<option value="notifyonly" <%=iif(iNotifyDefault=2,"selected=""""","")%>><%=sNotifyOnly_Text%></option>
						<% End If%>
						<option value="none" <%=iif(iNotifyDefault=3,"selected=""""","")%>><%=sNotifyNone_Text%></option>
					</select></td>
				</tr>
				<tr>
					<td>Email To</td>
					<td><textarea cols="30" rows="2" name="notifyemail" ID="Textarea1"></textarea>&nbsp;&nbsp;<i>(Note: 
							separate multiple email addresses with semicolons)</i></td>
				</tr>
				<tr>
					<td>Optional Email Message</td>
					<td><textarea cols="30" rows="2" name="notifymsg" ID="Textarea2"></textarea></td>
				</tr>
			</table>
			<br />
			<!-- Use ASP function from an "include" file to generate the custom report criteria -->
			<!-- SELECT CASE HERE BASED ON REPORT TYPE [CUSTOM CRITERIA]-->
			<%=  GenerateCriteriaHTML()%>
			<table border="0" width="100%" ID="Table5">
				<tr>
					<td align="center"><input type="submit" value="Run Report" class="button" ID="Submit2" NAME="Submit2"></input></td>
				</tr>
			</table><input name="reportid" type="hidden" value=<%=Request("reportid")%> ID="Hidden1"></input>
			<!--Safeway Atul for OSHA Privacy case-->
			<input type="hidden" value="<%=Request("PrivacyCaseFlag")%>" name="PrivacyCaseFlag">
		</form>
	</body>
</html>
