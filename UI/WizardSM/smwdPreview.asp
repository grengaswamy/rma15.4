<% Option Explicit %>

<!-- #include file ="session.inc" -->
<!-- #include file ="criteriautils.inc" -->
<%Public objSession
initSession objSession
%> 


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<META HTTP-EQUIV="Refresh" CONTENT="5">
<html>
	<head>
		<script LANGUAGE="JavaScript">
		
		</script>
	</head>
<body>
<form name="frmData">
<input type="hidden" name="hdJobID" value="<%=Request.QueryString("JOBID")%>">	
<%
dim r
dim sSQL
dim henv
dim db, db2
dim rs
dim bComplete, lJobID, vOutputPathURL, vMsg, bErrorFlag
dim pos

sSQL = "SELECT OUTPUT_PATH_URL,COMPLETE FROM SM_JOBS WHERE "
sSQL = sSQL & " SM_JOBS.JOB_ID = " & Request.QueryString("JOBID") & " AND SM_JOBS.USER_ID = " & objSessionStr("UserID") & " AND (ARCHIVED = 0 OR ARCHIVED IS NULL)"
sSQL = sSQL & " ORDER BY JOB_ID DESC"

Set r = CreateObject("DTGRocket")

henv = r.DB_InitEnvironment()

db = r.DB_OpenDatabase(henv, Application("SM_DSN"), 0)

rs = r.DB_CreateRecordset(db, sSQL, 3, 0)
If Not r.DB_EOF(rs) Then
	r.DB_GetData rs, "OUTPUT_PATH_URL", vOutputPathURL
	r.DB_GetData rs, "COMPLETE", bComplete
	
	If bComplete And vOutputPathURL<>"" Then
		' Adjust for right server name etc.
		pos=InstrRev(vOutputPathURL,"/")
		vOutputPathURL=Mid(vOutputPathURL,pos+1)
	End If


	If bComplete Then ' complete 
		r.DB_CloseRecordset CInt(rs), 2
		r.DB_CloseDatabase CInt(db)
		r.DB_FreeEnvironment CLng(henv)
		Set r = Nothing

		objSession("PreviewJobID") = "0"
		Response.Redirect "smwdPreviewReturn.asp?URL=" & vOutputPathURL & "&TABID=" & Request.QueryString("TABID")
		%>
		<%
		Response.End 
	End If 
	r.DB_CloseRecordset CInt(rs), 2
End If

r.DB_CloseDatabase CInt(db)
r.DB_FreeEnvironment CLng(henv)

Set r = Nothing
%>
</form>
</body>	
</html>
