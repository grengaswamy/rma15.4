<% 'Option Explicit %>
<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->

<html>
	<head>
		<title>Report Options</title>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<script language="JavaScript" SRC="SMD.js"></script>

<%
Dim sColProperties, pageError

Dim Title, SubTitle, ColHeading, ReportFont, sGlobalScript

Dim	sRptFont, sRptStyle1, sRptStyle2, sRptSize, sRptTitleFont, sRptTitleStyle1, sRptTitleStyle2
Dim sRptTitleSize, sUnderLine, sStrikeOut, sHeaderLeft, sHeaderCenter, sHeaderRight
Dim sFooterLeft, sFooterCenter, sFooterRight, sTitleColor, sPrintSerialNum, b
Dim sHtmlNumLinesPerPage, sHtmlPageFormat, sHtmlPageBreak, sHtmlExportGraph, sHtmlGraphSameFile, sHtmlGraphTopOfPage

Dim rptName, rptDesc

Dim myArrayP, bArrayP
'-- Rajeev -- 03/09/2004
'''myArrayP = Split(objSessionStr("ArrayP"), ",")
myArrayP = Split(objSessionStr("ArrayP"), "*!^")

bArrayP = IsValidArray(myArrayP)

If bArrayP Then
	For i = 0 To UBound(myArrayP)
		myArrayP(i) = Right(myArrayP(i),Len(myArrayP(i))-InStr(myArrayP(i),":"))
	Next
End If

Dim tempXML

If Not GetValidObject("Msxml2.DOMDocument", tempXML) Then
	Call ShowError(pageError, "XML Parser is not available.", True)
End If

Dim arrFields(), arrFieldWidth(), arrUserColNames()

Dim myArrayPXML
Dim sDefaultWidth, sUserColNames
sDefaultWidth = ""
sUserColNames = ""

Dim m_objSMList

If Not GetValidObject("InitSMList.CInitSM", m_objSMList) Then
	Call ShowError(pageError, "Call to CInitSM class failed.", True)
End If

m_objSMList.m_RMConnectStr = objSessionStr(SESSION_DSN) 
m_objSMList.m_SMConnectStr = Application(APP_SMDDSN) 

If objSessionStr("ArrayPXML") <> "-1" And objSessionStr("ArrayPXML") <> "" Then
	m_objSMList.GetUserColName objSessionStr("ArrayPXML"), arrUserColNames, arrFieldWidth

	If IsValidArray(arrUserColNames) Then
		For i = 0 To UBound(arrUserColNames)
			If sUserColNames = "" Then
				sUserColNames = arrUserColNames(i)
			Else
'-- Rajeev -- 03/09/2004			
'''				sUserColNames = sUserColNames & "," & arrUserColNames(i)
				sUserColNames = sUserColNames & "*!^" & arrUserColNames(i)
			End If
		Next
	End If

	If IsValidArray(arrFieldWidth) Then
		For i = 0 To UBound(arrFieldWidth)
			If sDefaultWidth = "" Then
				sDefaultWidth = arrFieldWidth(i)
			Else
				sDefaultWidth = sDefaultWidth & "," & arrFieldWidth(i)
			End If
		Next
	End If
	
End If

'-- Create an object of MSXML Dom Document
Dim xmlDoc 
Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument")
xmlDoc.async = false

Dim sXML, objData

'-- Initialize variables
Dim sReportTitle, sSubTitle
sReportTitle = ""
sSubTitle = ""

Dim sRptAlignLeft, sRptAlignCenter, sRptAlignRight

'--MITS 9521 Ankur Saxena 07/11/2007 - Declared for report orientation
Dim sRptOrientPortrait, sRptOrientLandscape

'^ Rajeev -- Retrieve report from database
'-- User opens an existing report or continues using saved report
If objSessionStr("REPORTID") <> "-1" And objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "1" Then

	If Not GetValidObject("InitSMList.CDataManager", objData) Then
		Call ShowError(pageError, "Call to CDataManager class failed.", True)
	End If
	
	objData.m_RMConnectStr = Application("SM_DSN")
	objData.ReportID = objSessionStr("REPORTID")
	objData.GetReport

	sXML = objData.XML
		
	xmlDoc.loadXML sXML

	Set objData = Nothing
	
	'-- Parse XML for populating on GUI
	Call DisplayOptions()
	
End if

'-- User continues tabbing but has not clicked on Save yet
If objSessionStr("REPORTID") <> "-1"  And objSessionStr("Save") = "0" And objSessionStr("SaveFinal") = "0" Then
	
	If Not GetValidObject("InitSMList.CDataManager", objData) Then
		Call ShowError(pageError, "Call to CDataManager class failed.", True)
	End If
	
	objData.m_RMConnectStr = Application("SM_DSN")
	objData.TempReportID = objSessionStr("REPORTID")
	objData.GetTempReport

	sXML = objData.XML
	xmlDoc.loadXML sXML

	Set objData = Nothing
	'-- Parse XML for populating on GUI
	Call DisplayOptions()

End If
'^^ Rajeev 06/05/2003 -- Retrieve report from database	

If LCase(Request.ServerVariables("REQUEST_METHOD")) = "post" Then

	Call SubmitReport()

End If
%>

		<Script>
			var pOptions = new Array();
			pOptions.length = 0;
				
			var globalIndex = -1;
			var m_DataChange = false;

			function ColNameChange(pIndex)
			{
				globalIndex = pIndex;
					
				for(i=0,n=0;i<pOptions.length;i++)
				{
					var tempArray = pOptions[i].split("+^+^+")
							
					var selIndex = pOptions[i].substring(0,pOptions[i].indexOf("+^+^+"));

					if (selIndex != '')
					{
						if (parseInt(selIndex) == pIndex)
						{
							document.frmData.txtColHeading.value = tempArray[1];
							//document.frmData.txtColHeading.value = unescape(tempArray[1]);
									
							document.frmData.cboRptColWidth.selectedIndex = tempArray[3]/40;
									
							if(tempArray[4] == 1)
								document.frmData.chkBorderTop.checked = true;
							else
								document.frmData.chkBorderTop.checked = false;
									
							if(tempArray[5] == 1)
								document.frmData.chkBorderLeft.checked = true;
							else
								document.frmData.chkBorderLeft.checked = false;
									
							if(tempArray[6] == 1)
								document.frmData.chkBorderRight.checked = true;
							else
								document.frmData.chkBorderRight.checked = false;

							if(tempArray[7] == 1)
								document.frmData.chkBorderBottom.checked = true;
							else
								document.frmData.chkBorderBottom.checked = false;

							if(tempArray[8] == 1)
								document.frmData.chkBoldText.checked = true;
							else
								document.frmData.chkBoldText.checked = false;

							if(tempArray[9] == 1)
								document.frmData.chkItalicText.checked = true;
							else
								document.frmData.chkItalicText.checked = false;

							if(tempArray[10] == 1)
								document.frmData.chkGrayShading.checked = true;
							else
								document.frmData.chkGrayShading.checked = false;

							if(tempArray[11] == 'left')
								document.frmData.optRptColAlign[0].checked = true;
							else if(tempArray[11] == 'center')
								document.frmData.optRptColAlign[1].checked = true;
							else
								document.frmData.optRptColAlign[2].checked = true;

							break;
						}
					}
				}
						
				var index = document.frmData.cboFieldName.selectedIndex;
						
				///--- var temp = document.frmData.hdSelFieldsXML.value.split(',');

				var temp = document.frmData.hdSelFieldsXML.value.split("</column>,");
				for(iCounter=0;iCounter<temp.length-1;iCounter++)
					temp[iCounter] = temp[iCounter] + "</column>"					

				if((index != -1) && (temp[index].indexOf("aggcount") != -1 || temp[index].indexOf("aggsum") != -1 || temp[index].indexOf("aggaverage") != -1 || temp[index].indexOf("aggmin") != -1 || temp[index].indexOf("aggmax") != -1 || temp[index].indexOf("agguniquecount") != -1))
					document.frmData.btnAggPrefix.disabled = false;
				else
					document.frmData.btnAggPrefix.disabled = true;
			}
				
			function window_onLoad()
			{
				m_DataChange = false;
				var tempArray = document.frmData.hdSelFieldsXML.value.split("</column>,");
				for(iCounter=0;iCounter<tempArray.length-1;iCounter++)
					tempArray[iCounter] = tempArray[iCounter] + "</column>"					
					
				if(tempArray[0].indexOf("aggcount") != -1 || tempArray[0].indexOf("aggsum") != -1 || tempArray[0].indexOf("aggaverage") != -1 || tempArray[0].indexOf("aggmin") != -1 || tempArray[0].indexOf("aggmax") != -1 || tempArray[0].indexOf("agguniquecount") != -1)
					document.frmData.btnAggPrefix.disabled = false;
				else
					document.frmData.btnAggPrefix.disabled = true;
						
				if(document.frmData.hdPWidth.value != '')
					pWidth = document.frmData.hdPWidth.value.split(',');
					
				if(document.frmData.hdPUserColName.value != '')
//					pUserColName = document.frmData.hdPUserColName.value.split(',');
					pUserColName = document.frmData.hdPUserColName.value.split('*!^');
						
				fillWidthCombo();
					
				if(document.frmData.hdPOptions.value != '-1' || document.frmData.hdPOptions.value != '')
				{
					pOptions = document.frmData.hdPOptions.value.split('`````,');
						
					for(i=0;i<pOptions.length-1;i++)
						pOptions[i] = pOptions[i] + '`````';
							
					ColNameChange(0);
					globalIndex = 0; 
				}
					
				if (document.frmData.cboFieldName.length == 0)
					document.frmData.btnColor.disabled = true;
					
				return true;
			}
				
			function fillWidthCombo()
			{
				var cStr = ' Default';
				var oOption;
					
				oOption = new Option(cStr, cStr);
				window.document.frmData.cboRptColWidth.options[window.document.frmData.cboRptColWidth.options.length] = oOption;
				   			
				var ix, inch, num, denom, g;
				ix = 0;
				inch = 0;
				for(ix = 2; ix <= 288; ix += 2)
				{
					inch = ix / 72;
				    num = ix % 72;
				    if(num > 0)
				    {
				        g = gcd(num, 72);
				        num /= g;
				        denom = 72 / g;
				    }
				    if(parseInt(inch) == 0)
						cStr = ' ' + parseInt(num) + '/' + parseInt(denom) + ' in.';
				    else
				    {
						if(parseInt(num) == 0)
							cStr = ' ' + parseInt(inch) + ' in.';
						else
							cStr = ' ' + parseInt(inch) + ' ' + parseInt(num) + '/' + parseInt(denom) + ' in.';
				    }
					      
			   		oOption = new Option(cStr, cStr);
					window.document.frmData.cboRptColWidth.options[window.document.frmData.cboRptColWidth.options.length] = oOption;
				}
			}

			function gcd(a, b)
			{
			    var c;
			    if(a > 0  &&  b > 0)
			    {
					if (a > b) return gcd(b,a);
					if (a == 1 ||  b == 1) return 1;
					if ((c = b % a) == 0) return a;
				}
			    return gcd(c,a);
			}	

			function OnSelChangeCbWidth()
			{
				try{
				if (index != -1)
				{
					var index = document.frmData.cboFieldName.selectedIndex;
					var tempArray = pOptions[index].split('+^+^+');
			
					var iCount;
					var tempStr = '';
			
					for(iCount=0;iCount<tempArray.length;iCount++)
					{				
						if(tempStr == '')
							tempStr =  tempArray[iCount];
						else
						{
							if(iCount == 2)
							{
								if (window.document.frmData.cboFieldName.length != 0)
								{
								    var ix = window.document.frmData.cboRptColWidth.selectedIndex;
								    if (ix == 0)
								        tempStr =  tempStr + '+^+^+' + 'chars';
								    else
								        tempStr =  tempStr + '+^+^+' + 'twips';
								 }
							}
							else if(iCount == 3)
							{
								if (window.document.frmData.cboFieldName.length != 0)
								{
								    var ix = window.document.frmData.cboRptColWidth.selectedIndex;
								    if (ix == 0)
								        tempStr =  tempStr + '+^+^+' + pWidth[window.document.frmData.cboFieldName.selectedIndex];
								    else
								        tempStr =  tempStr + '+^+^+' + (ix * 2 * 20);  // Convert Pts. to TWIPS
								 }
							}							
							else
								tempStr =  tempStr + '+^+^+' + tempArray[iCount];
						}
					}
			
					pOptions[index] = tempStr;
					document.frmData.hdPOptions.value = pOptions;
				}
				}
				catch(e)
				{}
			}
				
			function changeCheckBox(pObject, pIndex)
			{
				var index = document.frmData.cboFieldName.selectedIndex;
				if (index != -1)
				{
					var tempArray = pOptions[index].split('+^+^+');
											
					var iCount;
					var tempStr = '';
				
					for(iCount=0;iCount<tempArray.length;iCount++)
					{				
						if(tempStr == '')
							tempStr =  tempArray[iCount];
						else
						{
							if(iCount == pIndex)
							{
								if(eval(pObject.checked))
									tempStr =  tempStr + '+^+^+' + '1';
								else
									tempStr =  tempStr + '+^+^+' + '';
							}
							else
								tempStr =  tempStr + '+^+^+' + tempArray[iCount];
						}
					}
				
					pOptions[index] = tempStr;
					document.frmData.hdPOptions.value = pOptions;
				}
			}

			function changeColumnAlign()
			{
				var index = document.frmData.cboFieldName.selectedIndex;
				if (index != -1)
				{
					var tempArray = pOptions[index].split('+^+^+');
				
					var iCount;
					var tempStr = '';
				
					for(iCount=0;iCount<tempArray.length;iCount++)
					{				
						if(tempStr == '')
							tempStr =  tempArray[iCount];
						else
						{
							if(iCount == 11)
							{
								if(document.frmData.optRptColAlign[0].checked)
									tempStr =  tempStr + '+^+^+' + 'left';
								else if(document.frmData.optRptColAlign[1].checked)
									tempStr =  tempStr + '+^+^+' + 'center';
								else
									tempStr =  tempStr + '+^+^+' + 'right';
							}
							else
								tempStr =  tempStr + '+^+^+' + tempArray[iCount];
						}
					}
					pOptions[index] = tempStr;
					document.frmData.hdPOptions.value = pOptions;
				}
			}

			function onEmptyColHeading()
			{
				if (globalIndex != -1)
				{
					if(trimIt(document.frmData.txtColHeading.value).length == 0)
						document.frmData.txtColHeading.value = pUserColName[globalIndex];
						//document.frmData.txtColHeading.value = unescape(pUserColName[globalIndex]);

					var tempArray = pOptions[globalIndex].split('+^+^+');
					
					var iCount;
					var tempStr = '';
											
					for(iCount=0;iCount<tempArray.length;iCount++)
					{				
						if(tempStr == '')
							tempStr =  tempArray[iCount];
						else
						{
							if(iCount == 1)
								tempStr =  tempStr + '+^+^+' + fixDoubleQuote(document.frmData.txtColHeading.value);
							else
								tempStr =  tempStr + '+^+^+' + tempArray[iCount];
						}
					}
					pOptions[globalIndex] = tempStr;	
					document.frmData.hdPOptions.value = pOptions;			
				}
			}
		
			function updateColHeading()
			{
				if(globalIndex != -1)
				{
					if(trimIt(document.frmData.txtColHeading.value).length == 0)
						document.frmData.txtColHeading.value = pUserColName[globalIndex];
						//document.frmData.txtColHeading.value = unescape(pUserColName[globalIndex]);
		
						var tempArray = pOptions[globalIndex].split('+^+^+');
				
						var iCount;
						var tempStr = '';
									
						for(iCount=0;iCount<tempArray.length;iCount++)
						{				
							if(tempStr == '')
								tempStr =  tempArray[iCount];
							else
							{
								if(iCount == 1)
									tempStr =  tempStr + '+^+^+' + fixDoubleQuote(document.frmData.txtColHeading.value);
								else
									tempStr =  tempStr + '+^+^+' + tempArray[iCount];
							}
						}
						pOptions[globalIndex] = tempStr;
						document.frmData.hdPOptions.value = pOptions;
				}
			}
				
			function changeAggPrefix()
			{
				var tempArrayXML = document.frmData.hdSelFieldsXML.value.split("</column>,");
				for(iCounter=0;iCounter<tempArrayXML.length-1;iCounter++)
					tempArrayXML[iCounter] = tempArrayXML[iCounter] + "</column>"					

				var index = document.frmData.cboFieldName.selectedIndex;
				var tempAgg = '';
					
				if(tempArrayXML[index].indexOf("aggcount") != -1)
					tempAgg = tempAgg + "^^^^^" + "Count";
				if(tempArrayXML[index].indexOf("agguniquecount") != -1)
					tempAgg = tempAgg + "^^^^^" + "Unique Count";
				if(tempArrayXML[index].indexOf("aggsum") != -1)
					tempAgg = tempAgg + "^^^^^" + "Sum";
				if(tempArrayXML[index].indexOf("aggaverage") != -1)
					tempAgg = tempAgg + "^^^^^" + "Average";
				if(tempArrayXML[index].indexOf("aggmin") != -1)
					tempAgg = tempAgg + "^^^^^" + "Minimum";
				if(tempArrayXML[index].indexOf("aggmax") != -1)
					tempAgg = tempAgg + "^^^^^" + "Maximum";
					
				var tempArray = pOptions[globalIndex].split('+^+^+');
				tempAgg = tempAgg.substring(5,tempAgg.length);
					
				window.open('aggprefix.asp?a='+tempAgg+'&b='+tempArray[13],'aggprefix','resizable=yes,Width=450,Height=200,top='+(screen.availHeight-200)/2+',left='+(screen.availWidth-450)/2+'');
			}
		
			function openColor()
			{
				window.open('colorpicker.asp?a='+pOptions[globalIndex]+'&CHECKWINDOW='+'rptoptions'+'&GLOBALINDEX='+globalIndex,'colpick','resizable=yes,Width=340,Height=360,top='+(screen.availHeight-360)/2+',left='+(screen.availWidth-340)/2+'');		
			}

			function updateAggPrefix(gAggPrefix)
			{
				var tempArray = pOptions[globalIndex].split('+^+^+');
				var iCount;
				var tempStr = '';
								
				for(iCount=0;iCount<tempArray.length;iCount++)
				{				
					if(tempStr == '')
						tempStr =  tempArray[iCount];
					else
					{
						if(iCount == 13)
							tempStr =  tempStr + '+^+^+' + gAggPrefix;
						else
							tempStr =  tempStr + '+^+^+' + tempArray[iCount];
					}
				}
				pOptions[globalIndex] = tempStr;
				document.frmData.hdPOptions.value = pOptions;
			}
		
			function changeColor(sColor,pSampleColor)
			{
				var tempArray = pOptions[globalIndex].split('+^+^+');
				var iCount;
				var tempStr = '';
								
				for(iCount=0;iCount<tempArray.length;iCount++)
				{				
					if(tempStr == '')
						tempStr =  tempArray[iCount];
					else
					{
						if(iCount == 12)
							tempStr =  tempStr + '+^+^+' + sColor;
						else
							tempStr =  tempStr + '+^+^+' + tempArray[iCount];
					}
				}
				pOptions[globalIndex] = tempStr;
				document.frmData.hdPOptions.value = pOptions;
			}	
		
			function checkSerialNum(pIndex)
			{
				if(pIndex == 0)
				{
					m_DataChange = true;
					if(document.frmData.chkPrintRowNo.checked == true)
						document.frmData.hdPrintRowNum.value = 1;
					else
						document.frmData.hdPrintRowNum.value = 0;
				}
				else
				{
					if(m_DataChange == true)
					{
						if(document.frmData.chkPrintRowNo.checked == true)
							document.frmData.hdPrintRowNum.value = 1;
						else
							document.frmData.hdPrintRowNum.value = 0;
					}
				}
										
			}
		
			function checkSerialNumForColumnProperties(pIndex)
			{
				if(document.frmData.cboFieldName.length > 0)
				{
					if(pIndex == 0)
					{
						m_DataChange = true;
						if(document.frmData.chkPrintRowNo.checked == true)
							document.frmData.hdPrintRowNum.value = 1;
						else
							document.frmData.hdPrintRowNum.value = 0;
					}
					else
					{
						if(m_DataChange == true)
						{
							if(document.frmData.chkPrintRowNo.checked == true)
								document.frmData.hdPrintRowNum.value = 1;
							else
								document.frmData.hdPrintRowNum.value = 0;
						}
					}
				}
			}		

			function openDerivedField2(pText,pBtnClick,pTabID)
			{
				window.document.frmData.hdDerFieldName.value = pText;
				window.open('derivedfields_2.asp?usercolname='+pText+'&UpdateMode='+pBtnClick+'&TABID='+pTabID,'der2','resizable=yes,Width=575,Height=465,top='+(screen.availHeight-465)/2+',left='+(screen.availWidth-575)/2+'');
			}	
	
			function editDerivedField2(pText,pBtnClick,pTabID)
			{
				window.document.frmData.hdDerFieldName.value = pText;
				window.open('derivedfields_2.asp?usercolname='+pText+'&UpdateMode='+pBtnClick+'&TABID='+pTabID,'der2','resizable=yes,Width=575,Height=465,top='+(screen.availHeight-465)/2+',left='+(screen.availWidth-575)/2+'');
			}	

			function derived_click()
			{
				if(document.frmData.hdSelFields.value != -1)
					openDerivedField1(true, 2);
				else
					openDerivedField1(false, 2);
			}

			function openGScript(pTabID)
			{
				window.open('globalscript.asp?TABID='+pTabID,'glob','resizable=yes,Width=500,Height=225,top='+(screen.availHeight-225)/2+',left='+(screen.availWidth-500)/2+'');				
			}			
				
		</Script>
	</head>

	<body class="10pt" leftmargin="0" onLoad="pageLoaded();window_onLoad();" >
	<form name="frmData" method="post"  topmargin="0" bottommargin="0">
	<%If Request("ERROR") <> "" Then
		Response.Write "<center><b><font color=red>Error saving Derived Field: " & Request("ERROR") & "</font></b></center>"
	End If
	%>
	
	<%If Request("MSG") <> "" Then
		Response.Write Request("MSG")
	End If
	%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
			<tr height="4%">
				<td width="30%">
					<table border="0" class="toolbar" height="90%" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="newReport();return false;"><img src="img/new.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/new2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/new.gif';this.style.zoom='100%'" title="New" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick= "uploadReport();return false;"> <img src="img/open.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/open2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/open.gif';this.style.zoom='100%'" title="Open"/></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(2,0);return false;"><img src="img/smwd_save.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/smwd_save2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/smwd_save.gif';this.style.zoom='100%'" title="Save" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(2,3);return false;"><img src="img/saveas.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/saveas2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/saveas.gif';this.style.zoom='100%'" title="Save As..." /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="saveReport(2,1);return false;"><img src="img/savepost.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/savepost2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/savepost.gif';this.style.zoom='100%'" title="Save and Post" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openPreview('pdf');saveReport(2,2);return false;"><img src="img/adobe_prev1.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/adobe_prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/adobe_prev1.gif';this.style.zoom='100%'"  title="Preview in PDF" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openPreview('xls');saveReport(2,2);return false;"><img src="img/excel_prev1.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/excel_prev2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/excel_prev1.gif';this.style.zoom='100%'"  title="Preview in XLS" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="derived_click();return false;"><img src="img/derfield.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/derfield2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/derfield.gif';this.style.zoom='100%'" title="Derived Fields" /></a></td>
							<td align="center" valign="middle" height="32" width="28"><a class="LightBold" href="#" onClick="openRelDate(2);return false;"><img src="img/rel_date.gif" width="28" height="28" border="0" alt="" onMouseOver="this.src='img/rel_date2.gif';this.style.zoom='110%'" onMouseOut="this.src='img/rel_date.gif';this.style.zoom='100%'" title="Relative Date" /></a></td>
						</tr>
					</table>
				</td>
				<% If objSession("REPORTNAME") <> "-1" Then %> 
					<td width="70%" class="msgheader" align="right"><% Response.Write "<b><font>" & "Report Name : " & LEFT(objSession("REPORTNAME"), 50) & "</font></b>" %>&nbsp;&nbsp;&nbsp;</td>
				<% End If %>
			</tr>
			<tr height="6%">
				<td colspan="2">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td id="tool_rptfields" width="24%" class="NotSelected">
								<a class="NotSelected1" href="JavaScript:tabNavigation(0,2)"><span style="{text-decoration:none}">Report Fields</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
							<td id="tool_rptcriteria" width="24%" class="NotSelected">
								<a class="NotSelected1" href="JavaScript:tabNavigation(1,2)"><span style="{text-decoration:none}">Report Criteria</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
							<td id="tool_rptoptions" width="24%" class="Selected">
								<a class="Selected" href="#"><span style="{text-decoration:none}">Report Options</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
							<td id="tool_graphoptions" width="24%" class="NotSelected">
								<a class="NotSelected1" href="javascript:tabNavigation(3,2)"><span style="{text-decoration:none}">Graph Options</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td>
							<!--td id="tool_hier" width="25%" class="NotSelected">
							<a class="NotSelected1" href1="javascript:tabNavigation('4')"><span style="{text-decoration:none}">Column Hierarchy</span></a>
							</td>
							<td style="border-bottom:none;border-left-style:solid;border-left-color:#999999;border-width:2px;border-top-style:solid;border-top-width:2px;border-top-color:#FFFFFF;">&nbsp;&nbsp;</td-->
						</tr>
					</table>
				</td>
			</tr>
	
			<tr height="90%"><td  colspan="2">
				<table id="reportoptions" style1="display:none" width="100%" class="background" border="0" height="100%">
					<tr class="background" VALIGN="top">
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%" bgcolor="white">
								<tr>
									<td>
										<table class="formsubtitle" border="0" width="100%" bgcolor="white" height="10%" >
											<tr>
												<td width="70%" colspan="2">&nbsp;</td>
												<td width="5%" align="center" >&nbsp;</td>
												<td width="25%" colspan="2" align="left"><b>Report Orientation:</b></td>
											</tr>
											<tr>
												<td width="70%" class="ctrlgroup" colspan="2">&nbsp;&nbsp;Report Title</td>
												<td width="5%" align="center" >&nbsp;</td>
												<td width="25%" align="left" colspan="2"><input type="radio" name="optRptOrientation" value="0" <%=sRptOrientPortrait%> onClick="checkSerialNum(0);">Portrait&nbsp;&nbsp;<input type="radio" name="optRptOrientation" value="1" <%=sRptOrientLandscape%> onClick="checkSerialNum(0);">Landscape</td>
											</tr>
										</table>					
										<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="white">
											<tr>
												<td width="100%" align="center" colspan="5">&nbsp;</td>
											</tr>
											<tr>
												<td width="65%" align="left" colspan="2">&nbsp;<input type="text" size="94" style1="width:99%"  name="txtRptTitle"  value="<%=fixDoubleQuote(sReportTitle)%>" onChange="checkSerialNum(0);"></td>
												<td width="1%" align="center" >&nbsp;</td>
												<td width="10%" align="left">&nbsp;&nbsp;<input class="button" type="button" value="  HTML  " onClick="window.open('html.asp','html','resizable=yes,Width=600,Height=250,top='+(screen.availHeight-250)/2+',left='+(screen.availWidth-600)/2+'')"  name="btnRptHTML">&nbsp;&nbsp;</td>
												<td width="24%" align="left"><input class="button" type="button"  style="width:95%" value="Headers & Footers" onClick="window.open('headfoot.asp','headfoot','resizable=yes,Width=500,Height=250,top='+(screen.availHeight-250)/2+',left='+(screen.availWidth-500)/2+'')"  name="btnHeaderFooter"></td>
											</tr>
											<tr>
												<td width="100%" align="center" colspan="5">&nbsp;</td>
											</tr>
											<tr>
												<td width="65%" align="left" colspan="2">&nbsp;<input type="text" size="94" style1="width:99%" name="txtRptSubTitle" value="<%= fixDoubleQuote(sSubTitle)%>" onChange="checkSerialNum(0);"></td>
												<td width="1%" align="center" >&nbsp;</td>
												<td width="10%" align="left">&nbsp;
													<input class="button" type="button" value="Report Font" onClick="window.open('rptFont.asp','ReportFont','resizable=yes,Width=500,Height=280,top='+(screen.availHeight-280)/2+',left='+(screen.availWidth-500)/2+'')"  name="btnRptFont">
												</td>
												<td width="24%" align="left"></td>
											</tr>
											<tr>
												<td width="100%" align="center" colspan="5">&nbsp;</td>
											</tr>
											<tr>
												<td width="30%" align="left"><input type="radio" name="optRptAlign" value="0" <%=sRptAlignLeft%> onClick="checkSerialNum(0);">Left&nbsp;&nbsp;<input type="radio" name="optRptAlign" value="1" <%=sRptAlignCenter%> onClick="checkSerialNum(0);">Center&nbsp;&nbsp;<input type="radio" name="optRptAlign" value="2" <%=sRptAlignRight%> onClick="checkSerialNum(0);">Right</td>
												
												<td width="40%" align="right"> 
													<input class="button" type="button" value="Title Font" onClick="window.open('rptTitleFont.asp','TitleFont','resizable=yes,Width=500,Height=375,top='+(screen.availHeight-350)/2+',left='+(screen.availWidth-500)/2+'')"  name="btnRptTitleFont">
												</td>
												<td width="5%" align="center" >&nbsp;</td>
												<td width="30%" align="left" colspan="2">&nbsp;<input type="checkbox"  name="chkPrintRowNo" <%=sPrintSerialNum%> onClick="checkSerialNum(1);checkSerialNumForColumnProperties(1);"> Print Row Number</td>
											</tr>
											
										</table>

										<table class="formsubtitle" border="0" width="100%" bgcolor="white">
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td class="ctrlgroup">&nbsp;&nbsp;Column Properties</td>
											</tr>
										</table>	
	
										<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="white">
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td width="70%" align="center">&nbsp;</td>				
												<td width="15%" align="left"><b>Effects</b></td>				
												<td width="15%"align="center"><b>Borders</b></td>				
											</tr>
											<tr>
												<td width="100%" align="center" colspan="3">&nbsp;</td>				
											</tr>
											<tr>
												<td width="70%" align="left">&nbsp;
													<select  size="1" style="width:90%"  name="cboFieldName" onChange="ColNameChange(this.selectedIndex);checkSerialNumForColumnProperties(0);">
															<%	If objSessionStr("ArrayP") <> "-1" Then
																	Dim i
																	If bArrayP Then
																		For i = 0 to UBound(myArrayP)
																			If i=0 then 
																				Response.Write "<option selected>" & myArrayP(i) & "</option>"
																			Else
																				Response.Write "<option>" & myArrayP(i) & "</option>"
																			End If
																		Next
																	End If
																End If
																
															%>
													</select>
												</td>				
												<td width="15%" align="left"><input type="checkbox" name="chkGrayShading" value="1" onClick="changeCheckBox(this, 10);checkSerialNumForColumnProperties(0);">Gray Shading</td>				
												<td width="15%"align="center"><input type="checkbox"  name="chkBorderTop" value="1" onClick="changeCheckBox(this, 4);checkSerialNumForColumnProperties(0);">Top&nbsp;&nbsp;&nbsp;&nbsp;</td>				
											</tr>
											<tr>
												<td width="100%" align="center" colspan="3">&nbsp;</td>				
											</tr>
											<tr>
												<td width="70%" align="left">&nbsp;&nbsp;Column Heading</td>				
												<td width="15%" align="left"><input type="checkbox"  name="chkBoldText" value="1" onClick="changeCheckBox(this, 8);checkSerialNumForColumnProperties(0);">Bold Text</td>				
												<td width="15%"align="center"><input type="checkbox" name="chkBorderLeft" value="1" onClick="changeCheckBox(this, 5);checkSerialNumForColumnProperties(0);">Left&nbsp;<input type="checkbox" name=chkBorderRight value="1" onClick="changeCheckBox(this, 6);">Right</td>				
											</tr>
											<tr>
												<td width="100%" align="center" colspan="3">&nbsp;</td>				
											</tr>
											<tr>
												<td width="70%" align="left">&nbsp;&nbsp;<input type="text" name="txtColHeading" size="70" onKeyUp="onEmptyColHeading();" onChange="updateColHeading();checkSerialNumForColumnProperties(0);"></td>				
												<td width="15%" align="left"><input type="checkbox"  name="chkItalicText" value="1" onClick="changeCheckBox(this, 9);checkSerialNumForColumnProperties(0);">Italic Text</td>				
												<td width="15%"align="center"><input type="checkbox"  name="chkBorderBottom" value="1" onClick="changeCheckBox(this, 7);checkSerialNumForColumnProperties(0);">Bottom</td>				
											</tr>
											<tr>
												<td width="100%" align="center" colspan="3">&nbsp;</td>				
											</tr>
											<tr>
												<td width="70%" align="left">&nbsp;&nbsp;Width:&nbsp;
													<select  size="1" style="width:20%" name="cboRptColWidth" onChange="OnSelChangeCbWidth();checkSerialNumForColumnProperties(0);">
													</select>&nbsp;&nbsp;
													
													<input type="radio" name=optRptColAlign value="0" onClick="changeColumnAlign();checkSerialNumForColumnProperties(0);">Left &nbsp;&nbsp;
													<input type="radio" name=optRptColAlign value="1" onClick="changeColumnAlign();checkSerialNumForColumnProperties(0);">Center &nbsp;&nbsp;
													<input type="radio" name=optRptColAlign value="2" onClick="changeColumnAlign();checkSerialNumForColumnProperties(0);">Right &nbsp;&nbsp;
												</td>				
												<td width="15%" align="left"><input class="button" type="button" value="Agg. Prefix"  name="btnAggPrefix" onClick="changeAggPrefix();"></td>				
												<td width="15%"align="center"><input class="button" type="button" value="Color" onClick="openColor()" name="btnColor"></td>				
											</tr>
										</table>
									</td>
								</tr>
							</table>		
						</td>
					</tr>
			 </table>

			<input type="hidden" name="hdTabID" value="">
			<input type="hidden" name="hdSave" value="<%=objSessionStr("Save")%>">
			<input type="hidden" name="hdSaveFinal" value="<%=objSessionStr("SaveFinal")%>">
			<input type="hidden" name="hdRptName" value="" >
			<input type="hidden" name="hdRptDesc" value="" >
			<input type="hidden" name="hdSelFields" value="<%=objSessionStr("ArrayP")%>">
			<input type="hidden" name="hdSelFieldsLength" value="" >
			<input type="hidden" name="hdArrayTF" value="">
			<input type="hidden" name="hdArrayPFields" value="">			
			<input type="hidden" name="hdDataType" value="">
			<input type="hidden" name="hdColProp" value="">
			<input type="hidden" name="hdArrayCurrentFields" value=''>
			<input type="hidden" name="hdSelFieldsXML" value="<%=fixDoubleQuote(objSessionStr("ArrayPXML"))%>">
			<input type="hidden" name="hdRptFont" value="<%=sRptFont%>">
			<input type="hidden" name="hdRptStyle1" value="<%=sRptStyle1%>">
			<input type="hidden" name="hdRptStyle2" value="<%=sRptStyle2%>">
			<input type="hidden" name="hdRptSize" value="<%=sRptSize%>">
			<input type="hidden" name="hdRptTitleFont" value="<%=sRptTitleFont%>">
			<input type="hidden" name="hdRptTitleStyle1" value="<%=sRptTitleStyle1%>">
			<input type="hidden" name="hdRptTitleStyle2" value="<%=sRptTitleStyle2%>">
			<input type="hidden" name="hdRptTitleSize" value="<%=sRptTitleSize%>">
			<input type="hidden" name="hdStrikeOut" value="<%=sStrikeOut%>">
			<input type="hidden" name="hdUnderLine" value="<%=sUnderLine%>">
			<input type="hidden" name="hdHeaderLeft" value="<%=sHeaderLeft%>">
			<input type="hidden" name="hdHeaderCenter" value="<%=sHeaderCenter%>">
			<input type="hidden" name="hdHeaderRight" value="<%=sHeaderRight%>">
			<input type="hidden" name="hdFooterLeft" value="<%=sFooterLeft%>">
			<input type="hidden" name="hdFooterCenter" value="<%=sFooterCenter%>">
			<input type="hidden" name="hdFooterRight" value="<%=sFooterRight%>">
			
			<input type="hidden" name="hdHtmlPageFormat" value="<%=sHtmlPageFormat%>">
			<input type="hidden" name="hdHtmlPageBreak" value="<%=sHtmlPageBreak%>">
			<input type="hidden" name="hdHtmlExportGraph" value="<%=sHtmlExportGraph%>">
			<input type="hidden" name="hdHtmlGraphSameFile" value="<%=sHtmlGraphSameFile%>">
			<input type="hidden" name="hdHtmlGraphTopOfPage" value="<%=sHtmlGraphTopOfPage%>">
			<input type="hidden" name="hdHtmlNumLinesPerPage" value="<%=sHtmlNumLinesPerPage%>">

			<input type=hidden name="hdColor" value="">
			<input type=hidden name="hdTitleFontColor" value="<%=sTitleColor%>">
			<input type=hidden name="hdTitleFontRColor" value="">
			<input type=hidden name="hdTitleFontGColor" value="">
			<input type=hidden name="hdTitleFontBColor" value="">
			
			<input type=hidden name="hdRColor" value="">
			<input type=hidden name="hdGColor" value="">
			<input type=hidden name="hdBColor" value="">
			
			<input type=hidden name="hdPOptions" value="<%=sColProperties%>">
			<input type="hidden" name="hdPWidth" value="<%=sDefaultWidth%>">			
			<input type="hidden" name="hdPUserColName" value="<%=sUserColNames%>">			
			<input type="hidden" name="hdPrintRowNum" value="">			
			 
			<input type="hidden" name="hdSubmit" value="">

			<input type="hidden" name="hdDerFieldType" value="">
			<input type="hidden" name="hdDerFieldName" value="">
			<input type="hidden" name="hdDerFieldWidth" value="">
			<input type="hidden" name="hdDerDetailFormula" value="">
			<input type="hidden" name="hdDerIndex" value=""> 
			<input type="hidden" name="hdDerAddEdit" value=""> 
			<input type="hidden" name="hdDerAggregate" value=""> 
			<input type="hidden" name="hdDerAggregateFormula" value="">
			<input type="hidden" name="hdDerColumnInvolved" value="<%=fixDoubleQuote(objSessionStr("ColumnsInvolved"))%>"> 
			<input type="hidden" name="hdDerError" value='<%=sError%>'> 
			<input type="hidden" name="hdDerGScript" value="<%=sGlobalScript%>">
		
			<input type="hidden" name="hdRelDate" value=""> 
			<input type="hidden" name="hdRelDateFormula" value=""> 
			<input type="hidden" name="hdRelativeDate" value=""> 
			<input type="hidden" name="hdRelMode" value=""> 
			<input type="hidden" name="hdRelID" value=""> 

			<input type="hidden" name="hdMode1" value="<%=objSessionStr("MODE1")%>">
			<input type="hidden" name="hdPost" value="0"> 
			<input type="hidden" name="hdOutputType" value="0">
			<input type="hidden" name="hdPreview" value="0">
		</form>
	</body>
</html>

<%

Function SaveReport()

	'-- objSession("MODE1") will always be 1 here
	'-- User continues using tabbing but hasn't clicked on Save button
	If objSessionStr("Save") = "0" Then
	
		rptName = "Temp"
		sXML = UpdateXMLReport()
		
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
	
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptName
		objData.XML = sXML
		objData.UpdateTempReport
		
		Set objData = Nothing
	
	'-- User copies the report
	ElseIf objSessionStr("MODE1") = "1" And objSessionStr("Save") = "3" Then
		rptName = Request.Form("hdRptName")
		rptDesc = Request.Form("hdRptDesc")
				
		sXML = UpdateXMLReport()

		'-- Delete corresponding temporary report from database
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptName
		objData.ReportDesc = rptDesc
		objData.XML = sXML
		objData.DeleteTempReport
		objData.AddReport objSessionStr("UserID")

		objSession("REPORTID") = objData.ReportID
		objSession("REPORTNAME") = objData.REPORTNAME

		Set objData = Nothing
			
		objSession("Save") = "1"
		objSession("SaveFinal") = "1" 

		Set objData = Nothing
			
	'-- User continues using tabbing but has clicked on Save button first time
	ElseIf objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "0" Then

		rptName=Request.Form("hdRptName")
		rptDesc=Request.Form("hdRptDesc")

		sXML = UpdateXMLReport()

		'-- Delete corresponding temporary report from database
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
		
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.TempReportID = objSessionStr("REPORTID")
		objData.ReportName = rptName
		objData.ReportDesc = rptDesc
		objData.XML = sXML
		objData.DeleteTempReport
		objData.AddReport objSessionStr("UserID")

		objSession("REPORTID") = objData.ReportID
		Set objData = Nothing
			
		objSession("SaveFinal") = "1" 
				
	'-- User has already clicked on Save button and again clicks save button or uses tabbing
	ElseIf objSessionStr("Save") = "1" And objSessionStr("SaveFinal") = "1" Then

		sXML = UpdateXMLReport()
		
		If Not GetValidObject("InitSMList.CDataManager", objData) Then
			Call ShowError(pageError, "Call to CDataManager class failed.", True)
		End If
	
		objData.m_RMConnectStr = Application("SM_DSN")
		objData.ReportID = objSessionStr("REPORTID")
		objData.XML = sXML
		objData.UpdateReport
		Set objData = Nothing
		
	End If 

End Function


Function UpdateXMLReport()

	'-- Add/Delete XML tags/attributes for Options
	Dim objXML
	On Error Resume Next
	Set objXML = Server.CreateObject("Msxml2.DOMDocument")
	objXML.async = false

	'--Load the existing XML
	objXML.loadXML sXML
	
	'-- Print Serial Number
	Dim b
	Set b = objXML.getElementsByTagName("report").item(0)

	If Request.Form("hdDerGScript") <> "" Then

		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If

		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If

		script.StartEngine

		script.RegisterGlobalObject "", evobj, False

		On Error Resume Next
		script.AddScript Request.Form("hdDerGScript")
		
		sError = script.LastError

		If sError = "" Then
			b.setAttribute "globalscript", Request.Form("hdDerGScript")
		Else
			script.StopEngine
			Set evobj = Nothing
			Set script = Nothing
		End If
	Else
		If Not b.getAttributeNode("globalscript") Is Nothing Then b.removeAttribute("globalscript")
	End If

	If Request.Form("hdPrintRowNum") = "1"  Then
		b.setAttribute "printserialnumber",Request.Form("hdPrintRowNum")
	ElseIf Request.Form("hdPrintRowNum") = "0"  Then
		If Not b.getAttributeNode("printserialnumber") Is Nothing Then b.removeAttribute("printserialnumber")
	End if
	
	'-- Report Title/Subtittle
	If Request.Form("txtRptTitle") <> "" Then
		If objXML.getElementsByTagName("reporttitle").item(0).childNodes.length = 1 Then
			objXML.getElementsByTagName("reporttitle").item(0).removeChild objXML.getElementsByTagName("reporttitle").item(0).childNodes.item(0)
			objXML.getElementsByTagName("reporttitle").item(0).appendChild objXML.createTextNode(Request.Form("txtRptTitle"))
				 
			Dim x
			Set x = objXML.getElementsByTagName("reporttitle").item(0).appendChild(objXML.createElement("subtitle"))
			x.appendChild objXML.createTextNode(Request.Form("txtRptSubTitle"))
		Else
			objXML.getElementsByTagName("reporttitle").Item(0).childNodes(0).Text = Request.Form("txtRptTitle")
			objXML.getElementsByTagName("reporttitle").Item(0).childNodes(1).Text = Request.Form("txtRptSubTitle")
		End If
		sReportTitle = Request.Form("txtRptTitle")
		sSubTitle = Request.Form("txtRptSubTitle")
	End If
		
	If Request.Form("txtRptTitle") = "" Then
		If objXML.getElementsByTagName("reporttitle").item(0).childNodes.length = 1 Then
		Else
			objXML.getElementsByTagName("reporttitle").item(0).removeChild objXML.getElementsByTagName("reporttitle").item(0).childNodes.item(0)
			objXML.getElementsByTagName("reporttitle").Item(0).childNodes(0).Text = Request.Form("txtRptSubTitle")
		End If
		objXML.getElementsByTagName("reporttitle").Item(0).childNodes(0).Text = Request.Form("txtRptSubTitle")
		sReportTitle = ""
		sSubTitle = Request.Form("txtRptSubTitle")
	End If
				
	'-- Report Title alignment

	If Request.Form("optRptAlign") = "0" Then
		objXML.getElementsByTagName("reporttitle").item(0).attributes.item(0).value = "left"
		sRptAlignLeft = "checked" 
		sRptAlignCenter = ""
		sRptAlignRight = ""
	ElseIf Request.Form("optRptAlign") = "1" Then
		objXML.getElementsByTagName("reporttitle").item(0).attributes.item(0).value = "center"
		sRptAlignCenter = "checked" 
		sRptAlignLeft = ""
		sRptAlignRight = ""
	ElseIf Request.Form("optRptAlign") = "2" Then
		objXML.getElementsByTagName("reporttitle").item(0).attributes.item(0).value = "right"
		sRptAlignRight = "checked"
		sRptAlignLeft = ""
		sRptAlignCenter = ""
	End if
				
	'--Start MITS 9521
	'--Ankur Saxena 07/11/2007
	'--Setting the report orientation
	If Request.Form("optRptOrientation") = "0" Then
		objXML.getElementsByTagName("report").item(0).attributes.item(2).value = "portrait"
		sRptOrientPortrait = "checked" 
		sRptOrientLandscape = ""
	ElseIf Request.Form("optRptOrientation") = "1" Then
		objXML.getElementsByTagName("report").item(0).attributes.item(2).value = "landscape"
		sRptOrientPortrait =  ""
		sRptOrientLandscape = "checked"
	End if
	'--End MITS 9521
	
	'-- Font pop-ups
	objXML.getElementsByTagName("reportfont").item(0).attributes.item(0).text = Request.Form("hdRptFont")
	objXML.getElementsByTagName("reportfont").item(0).attributes.item(1).text = Request.Form("hdRptSize")
	objXML.getElementsByTagName("reportfont").item(0).attributes.item(2).text = Request.Form("hdRptStyle1")
	objXML.getElementsByTagName("reportfont").item(0).attributes.item(3).text = Request.Form("hdRptStyle2")
	
	objXML.getElementsByTagName("reporttitle").item(0).attributes.item(1).text = Request.Form("hdTitleFontColor")
	objXML.getElementsByTagName("reporttitle").item(0).attributes.item(2).text = Request.Form("hdRptTitleFont")
	objXML.getElementsByTagName("reporttitle").item(0).attributes.item(3).text = Request.Form("hdRptTitleSize")
	objXML.getElementsByTagName("reporttitle").item(0).attributes.item(4).text = Request.Form("hdRptTitleStyle1")
	objXML.getElementsByTagName("reporttitle").item(0).attributes.item(5).text = Request.Form("hdRptTitleStyle2")
	objXML.getElementsByTagName("reporttitle").item(0).attributes.item(6).text = Request.Form("hdUnderLine")
	objXML.getElementsByTagName("reporttitle").item(0).attributes.item(7).text = Request.Form("hdStrikeOut")
	
	sRptFont = objXML.getElementsByTagName("reportfont").item(0).attributes.item(0).text
	sRptStyle1 = objXML.getElementsByTagName("reportfont").item(0).attributes.item(2).text
	sRptStyle2 = objXML.getElementsByTagName("reportfont").item(0).attributes.item(3).text
	sRptSize = objXML.getElementsByTagName("reportfont").item(0).attributes.item(1).text
	
	sRptTitleFont = objXML.getElementsByTagName("reporttitle").item(0).attributes.item(2).text
	sRptTitleStyle1 = objXML.getElementsByTagName("reporttitle").item(0).attributes.item(4).text
	sRptTitleStyle2 = objXML.getElementsByTagName("reporttitle").item(0).attributes.item(5).text
	sRptTitleSize = objXML.getElementsByTagName("reporttitle").item(0).attributes.item(3).text
	sUnderLine = objXML.getElementsByTagName("reporttitle").item(0).attributes.item(6).text 
	sStrikeOut = objXML.getElementsByTagName("reporttitle").item(0).attributes.item(7).text 
	sTitleColor = objXML.getElementsByTagName("reporttitle").item(0).attributes.item(1).text
	
	'--Handling headers & footers 
	objXML.getElementsByTagName("headerleft").item(0).text = Request.Form("hdHeaderLeft")
	objXML.getElementsByTagName("headercenter").item(0).text = Request.Form("hdHeaderCenter")
	objXML.getElementsByTagName("headerright").item(0).text = Request.Form("hdHeaderRight")
	objXML.getElementsByTagName("footerleft").item(0).text = Request.Form("hdFooterLeft")
	objXML.getElementsByTagName("footercenter").item(0).text = Request.Form("hdFooterCenter")
	objXML.getElementsByTagName("footerright").item(0).text = Request.Form("hdFooterRight")
	
	
	sHeaderLeft = objXML.getElementsByTagName("headerleft").item(0).text
	sHeaderCenter = objXML.getElementsByTagName("headercenter").item(0).text
	sHeaderRight = objXML.getElementsByTagName("headerright").item(0).text
	sFooterLeft = objXML.getElementsByTagName("footerleft").item(0).text
	sFooterCenter = objXML.getElementsByTagName("footercenter").item(0).text
	sFooterRight = objXML.getElementsByTagName("footerright").item(0).text

	'--Handling HTML page
	objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(0).text = Request.Form("hdHtmlNumLinesPerPage")
	objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(1).text = Request.Form("hdHtmlPageFormat")
	objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(2).text = Request.Form("hdHtmlPageBreak")
	objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(3).text = Request.Form("hdHtmlExportGraph")
	objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(4).text = Request.Form("hdHtmlGraphSameFile")
	objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(5).text = Request.Form("hdHtmlGraphTopOfPage")
	
	sHtmlNumLinesPerPage = objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(0).text
	sHtmlPageFormat = objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(1).text
	sHtmlPageBreak = objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(2).text
	sHtmlExportGraph = objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(3).text
	sHtmlGraphSameFile = objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(4).text
	sHtmlGraphTopOfPage = objXML.getElementsByTagName("htmlsettings").item(0).attributes.item(5).text

	Dim arrColProperties, m, arrOptionParts
	'-- Coding for column attributes 
	
	If Request.Form("hdPOptions") <> "" And Request.Form("hdPOptions") <> "-1" Then
		
		arrColProperties = Split(Request.Form("hdPOptions"), "`````,")
		
		Dim i

		If IsValidArray(arrColProperties) Then
			If UBound(arrColProperties) = 0 Then
				arrColProperties(0) = Replace(arrColProperties(0), "`````", "")
			Else
				For i = 0 To UBound(arrColProperties)
					If Right(arrColProperties(i), 5) = "`````" Then	arrColProperties(i) = Left(arrColProperties(i), Len(arrColProperties(i))-5 )
				Next
			End If
		End If
				
		Dim objTemp, iCount
		
		If IsValidArray(arrColProperties) Then
			For iCount = 0 To UBound(arrColProperties)
				arrOptionParts = Split(arrColProperties(iCount), "+^+^+")
				
				If IsValidArray(arrOptionParts) Then
					'-- Usercolname
					objXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(3).text = arrOptionParts(1)				
					'-- Width unit
					objXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(4).text = arrOptionParts(2)
					'-- Width
					objXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount).attributes.item(5).text = arrOptionParts(3)
				
					'-- Borders and effects
					Set objTemp = objXML.getElementsByTagName("columnlist").item(0).childNodes.item(iCount)
					If arrOptionParts(4) <> "" Then 
						objTemp.setAttribute "bordertop", arrOptionParts(4)
					Else
						If Not objTemp.getAttributeNode("bordertop") Is Nothing Then objTemp.removeAttribute("bordertop")
					End If
					If arrOptionParts(5) <> "" Then 
						objTemp.setAttribute "borderleft", arrOptionParts(5)
					Else
						If Not objTemp.getAttributeNode("borderleft") Is Nothing Then objTemp.removeAttribute("borderleft")
					End If
					If arrOptionParts(6) <> "" Then 
						objTemp.setAttribute "borderright", arrOptionParts(6)
					Else
						If Not objTemp.getAttributeNode("borderright") Is Nothing Then objTemp.removeAttribute("borderright")
					End If
					If arrOptionParts(7) <> "" Then 
						objTemp.setAttribute "borderbottom", arrOptionParts(7)
					Else
						If Not objTemp.getAttributeNode("borderbottom") Is Nothing Then objTemp.removeAttribute("borderbottom")
					End If
					If arrOptionParts(8) <> "" Then 
						objTemp.setAttribute "bold", arrOptionParts(8)
					Else
						If Not objTemp.getAttributeNode("bold") Is Nothing Then objTemp.removeAttribute("bold")
					End If
					If arrOptionParts(9) <> "" Then 
						objTemp.setAttribute "italic", arrOptionParts(9)
					Else
						If Not objTemp.getAttributeNode("italic") Is Nothing Then objTemp.removeAttribute("italic")
					End If
					If arrOptionParts(10) <> "" Then 
						objTemp.setAttribute "shade", arrOptionParts(10)
					Else
						If Not objTemp.getAttributeNode("shade") Is Nothing Then objTemp.removeAttribute("shade")
					End If
				
					If arrOptionParts(11) <> "" Then objTemp.setAttribute "justify", arrOptionParts(11)
					If arrOptionParts(12) <> "" Then objTemp.setAttribute "color", arrOptionParts(12)

					If arrOptionParts(13) <> "" Then 
						
						Dim objAggPrefixes
						Set objAggPrefixes = objTemp.getElementsByTagName("aggregatelabels").item(0)
						objTemp.removeChild(objAggPrefixes)
						
						Set objAggPrefixes = objTemp.appendChild(objXML.createElement("aggregatelabels"))

						Dim arrAggLabels, objLabel, arrAggLabel
						arrAggLabels = Split(arrOptionParts(13), "^^^^^")
						
						If IsValidArray(arrAggLabels) Then
							For i = 0 To UBound(arrAggLabels)
								arrAggLabel = Split(arrAggLabels(i), "!!!!!")
								Set objLabel = objAggPrefixes.appendChild(objXML.createElement("aggregatelabel"))
								If IsValidArray(arrAggLabel) Then objTemp.getElementsByTagName("aggregatelabels").item(0).childNodes.item(i).appendChild(objXML.createTextNode( arrAggLabel(1)))
							Next
						End If
					End If
				End If
			Next
		End If
	End If

	If Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "ADD" Then
				
		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
			Dim objData
			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If
			
			objData.m_RMConnectStr = Application("SM_DSN")
		
			objData.AddDateFormula Request.Form("hdRelDate"), Request.Form("hdRelDateFormula")
			Set objData = Nothing
		End If
		
	ElseIf Request.Form("hdSubmit") = "RELATIVEDATE" And Request.Form("hdRelMode") = "UPDATE" Then

		If Request.Form("hdRelDate") <> "" And Request.Form("hdRelDateFormula") <> "" Then
			If Not GetValidObject("InitSMList.CDataManager", objData) Then
				Call ShowError(pageError, "Call to CDataManager class failed.", True)
			End If
			objData.m_RMConnectStr = Application("SM_DSN")
						
			objData.UpdateDateFormula CInt(Request.Form("hdRelID")),Request.Form("hdRelDateFormula")
			Set objData = Nothing
		End If
	
	End If

	Dim objDerived, objDetailFormula, objAggregateFormula, a1, a2, a3
	
	'-- Add mode
	If Request.Form("hdSubmit") = "DERIVED" And Request.Form("hdDerAddEdit") = "Add" Then

		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If

		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If

		script.StartEngine

		script.RegisterGlobalObject "", evobj, False

		On Error Resume Next
		script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & " end function"
		sError = script.LastError
		If sError = "" Then
			script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerDetailFormula") & Chr(13) & Chr(13) & " end function"
			sError = script.LastError
			If sError = "" Then
				script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerAggregateFormula") & Chr(13) & Chr(13) & " end function"
				sError = script.LastError
				If sError = "" Then
					'-- Add column tag in columnorder tag and its value
					Set z = objXML.getElementsByTagName("columnorder").item(0).appendChild(objXML.createElement("column"))
						z.appendChild(objXML.createTextNode(objXML.getElementsByTagName("columnorder").item(0).childNodes.length - 1))
					For k = objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1 To 0 Step -1
						Set a1 = objXML.getElementsByTagName("columnlist").item(0).childNodes.item(k)
						Set a2 = a1.getElementsByTagName("derived").item(0)
						
						a4 = Cstr(CInt(a1.getAttributeNode("fieldnumber").value) + 1)
						
						If Not a2 Is Nothing Then
							a3 = a1.getAttributeNode("tableid").value
							a3 = CStr(CInt(a3) - 1)
							Exit For 
						Else
							a3 = "-1"
						End If
					
					Next

					'-- Add column in columnlist tag and its children
					Set z = objXML.getElementsByTagName("columnlist").item(0).appendChild(objXML.createElement("column"))
						z.setAttribute "fieldnumber", a4
						z.setAttribute "tableid", a3
						z.setAttribute "fieldid", a3
						z.setAttribute "usercolname", Request.Form("hdDerFieldName")
						z.setAttribute "colwidthunits", "chars"
						z.setAttribute "colwidth", Request.Form("hdDerFieldWidth")
						z.setAttribute "justify", "left"
						z.setAttribute "color", "0"
			
		
					Set objDerived = z.appendChild(objXML.createElement("derived"))
						objDerived.setAttribute "fieldtype", Request.Form("hdDerFieldType")
						objDerived.setAttribute "fieldname", Request.Form("hdDerFieldName")
						objDerived.setAttribute "defaultfieldwidth", Request.Form("hdDerFieldWidth")
						Set objDetailFormula = objDerived.appendChild(objXML.createElement("detailformula"))
						
					objDetailFormula.appendChild(objXML.createTextNode(Request.Form("hdDerDetailFormula")))			
							
					'-- Check for Aggregate Formula
					If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
						Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
							objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
					End If
				
					objAggregateFormula.appendChild(objXML.createTextNode(Request.Form("hdDerAggregateFormula")))

					'-- Set parenttables tag
					z.appendChild(objXML.createElement("parenttables"))

					'-- Update p and pXML array with new added derived field and its attributes and children
					objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1).xml)
'-- Rajeev -- 03/09/2004					
'''					objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "," & "Derived : " & Request.Form("hdDerFieldName")
					objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "*!^" & "Derived : " & Request.Form("hdDerFieldName")
				Else
					script.StopEngine
		
					Set evobj = Nothing
					Set script = Nothing 
				End If
				
			Else
				script.StopEngine
		
				Set evobj = Nothing
				Set script = Nothing 
			End If
		Else
			script.StopEngine
		
			Set evobj = Nothing
			Set script = Nothing 
		End If

	ElseIf Request.Form("hdSubmit") = "DERIVED" And Request.Form("hdDerAddEdit") = "Edit" Then '-- Edit mode
		
		Set z = objXML.getElementsByTagName("columnlist").item(0).childNodes.item(CInt(Request.Form("hdDerIndex")))

		z.setAttribute "colwidth", Request.Form("hdDerFieldWidth")

		Set objDerived = z.getElementsByTagName("derived").item(0)

			objDerived.setAttribute "fieldtype", Request.Form("hdDerFieldType")
			objDerived.setAttribute "fieldname", Request.Form("hdDerFieldName")
			objDerived.setAttribute "defaultfieldwidth", Request.Form("hdDerFieldWidth")
		
		'-- Check for Aggregate Formula
		Dim objTempAgg, p, sAggFormula
		Set objTempAgg = objDerived.getElementsByTagName("aggregateformula").item(0)
		
		If Not objTempAgg Is Nothing Then
			sAggFormula = objTempAgg.text 
		Else
			sAggFormula = ""
		End If
		
		Set p = z.childNodes.item(0).getAttributeNode("aggregatecalcmethod")

		If Not p Is Nothing Then 
			objDerived.removeAttribute("aggregatecalcmethod")
		End If

		If Not objTempAgg Is Nothing Then
			''objTempAgg.removeAll
			objDerived.removeChild(objTempAgg)
		End If
		
		If Not GetValidObject("DTGScript.ScriptEngine", script) Then
			Call ShowError(pageError, "Call to DTG ScriptEngine class failed.", True)
		End If

		If Not GetValidObject("InitSMList.CEventSink", evobj) Then
			Call ShowError(pageError, "Call to CEventSink class failed.", True)
		End If

		script.StartEngine

		script.RegisterGlobalObject "ScriptEvent", evobj, False
		
		On Error Resume Next
		script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerDetailFormula") & Chr(13) & Chr(13) & " end function"
   
		sError = script.LastError
		
		If sError = "" Then
			Set objDetailFormula = objDerived.getElementsByTagName("detailformula").item(0)
				objDetailFormula.text = Request.Form("hdDerDetailFormula")

			'-- Check for Aggregate Formula
			If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
				Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
					objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
			End If
					
			script.AddScript "function " & Request.Form("hdDerFieldName") & "()" & Chr(13) & Request.Form("hdDerAggregateFormula") & Chr(13) & Chr(13) & " end function"
   
			sError = script.LastError
				
			Set objAggregateFormula = objDerived.getElementsByTagName("aggregateformula").item(0)
				
			If sError = "" Then
				objAggregateFormula.text = Request.Form("hdDerAggregateFormula")
			Else
				objAggregateFormula.text = sAggFormula
			End If
		Else
			'-- Check for Aggregate Formula
			If Request.Form("hdDerAggregate") = "sameformula" Or Request.Form("hdDerAggregate") = "differentformula" Then
				Set objAggregateFormula = objDerived.appendChild(objXML.createElement("aggregateformula"))
					objDerived.setAttribute "aggregatecalcmethod", Request.Form("hdDerAggregate")
			End If
			Set objAggregateFormula = objDerived.getElementsByTagName("aggregateformula").item(0)
				
			objAggregateFormula.text = sAggFormula	
		
		End If

		script.StopEngine
		
		Set evobj = Nothing
		Set script = Nothing 

		objSession("ArrayPXML") = "-1"
		
		For j = 0  To objXML.getElementsByTagName("columnlist").item(0).childNodes.length - 1
			If objSessionStr("ArrayPXML") = "-1" Then
				objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			Else
				objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			End If
		Next
		
	ElseIf Request.Form("hdDerAddEdit") = "Delete" Then '-- Delete Derived Field

		Set z = objXML.getElementsByTagName("columnlist").item(0)
			z.removeChild z.childNodes.item(CInt(Request.Form("hdDerIndex")))
		
		Set z = objXML.getElementsByTagName("columnorder").item(0)
			z.removeChild z.childNodes.item(CInt(Request.Form("hdDerIndex")))

		
		objSession("ArrayPXML") = "-1"
		Dim arrColOrder()
		For j = 0  To CInt(z.childNodes.length) - 1
			If objSessionStr("ArrayPXML") = "-1" Then
				objSession("ArrayPXML") = Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			Else
				objSession("ArrayPXML") = Trim(objSessionStr("ArrayPXML")) & "," & Trim(objXML.getElementsByTagName("columnlist").item(0).childNodes.item(j).xml)
			End If
			
			Redim Preserve arrColOrder(j)
			If CInt(Request.Form("hdDerIndex")) < Cint(Trim(objXML.getElementsByTagName("columnorder").item(0).childNodes.item(j).text)) Then
				arrColOrder(j) = Cint(Trim(objXML.getElementsByTagName("columnorder").item(0).childNodes.item(j).text)) - 1
			Else
				arrColOrder(j) = Cint(Trim(objXML.getElementsByTagName("columnorder").item(0).childNodes.item(j).text))
			End If
		Next

		Set z = objXML.selectNodes("//columns//columnorder")
		z.removeAll
			
		Set z = objXML.getElementsByTagName("columns").item(0)
		
		Set z1 = z.insertBefore(objXML.createElement("columnorder"), z.getElementsByTagName("columnlist").item(0))
		
		If IsValidArray(arrColOrder) Then
			For j = 0  To UBound(arrColOrder)
				Set z2 = z1.appendChild(objXML.createElement("column"))
					z2.appendChild(objXML.createTextNode(arrColOrder(j)))
			Next			
		End If
		
		Dim myArrayP
'-- Rajeev -- 03/09/2004
'''		myArrayP = Split(objSessionStr("ArrayP"), ",")
		myArrayP = Split(objSessionStr("ArrayP"), "*!^")

		objSession("ArrayP") = "-1"

		If IsValidArray(myArrayP) Then
			For j = 0  To  UBound(myArrayP)
				If j <> CInt(Request.Form("hdDerIndex")) Then
					If objSessionStr("ArrayP") = "-1" Then
						objSession("ArrayP") = myArrayP(j)
					Else
'-- Rajeev -- 03/09/2004
'''						objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "," & myArrayP(j)
						objSession("ArrayP") = Trim(objSessionStr("ArrayP")) & "*!^" & myArrayP(j)
					End If
				End If
			Next
		End If
						
		'-- Updating columns involved 
		Dim arrXML, b1, b2, b3
		Set xmlDoc = Server.CreateObject("Msxml2.DOMDocument")
		xmlDoc.async = false
		'''--- arrXML = Split(objSessionStr("ArrayPXML"),",")
		
		arrXML = Split(objSessionStr("ArrayPXML"), "</column>,")
		
		If IsValidArray(arrXML) Then
			For iCounter = 0 To UBound(arrXML) - 1
				arrXML(iCounter) = arrXML(iCounter) & "</column>"
			Next
		
			Dim sColumnInvolved
			sColumnInvolved = "-1"
			For i = 0 To UBound(arrXML)
				xmlDoc.loadXML arrXML(i)
				'-- Fetching DataType for derived fields
				Set b1 = xmlDoc.getElementsByTagName("column").item(0).childNodes.item(0) 
				Set b2 = xmlDoc.getElementsByTagName("derived").item(0)
				If Not b2 Is Nothing Then
					'-- Handling for columns involved
						Set b3 = b1.getElementsByTagName("detailformula").item(0)
						If sColumnInvolved = "-1" Then
							sColumnInvolved = b3.text
						Else
							sColumnInvolved = sColumnInvolved  & b3.text
						End If
				End If		
			Next
		End If
		
		objSession("ColumnsInvolved") = "-1"
		If Not (sColumnInvolved = "-1" Or sColumnInvolved = "") Then
			objSession("ColumnsInvolved") = sColumnInvolved
		End If
		
	End If			
	UpdateXMLReport = objXML.XML

	Set objXML = Nothing	

End Function

Function DisplayOptions()
	On Error Resume Next
	If Not xmlDoc.getElementsByTagName("report").item(0).getAttributeNode("globalscript") Is Nothing Then
		sGlobalScript = xmlDoc.getElementsByTagName("report").item(0).getAttributeNode("globalscript").value
	End If
	
	'-- Report Title/Subtitle
	If xmlDoc.getElementsByTagName("reporttitle").item(0).childNodes.length = 1 then
		sReportTitle = ""
		sSubTitle = xmlDoc.getElementsByTagName("reporttitle").item(0).childNodes(0).text 
	Else
		sReportTitle = xmlDoc.getElementsByTagName("reporttitle").item(0).childNodes(0).text 
		sSubTitle = xmlDoc.getElementsByTagName("reporttitle").item(0).childNodes(1).text 
	End If

	
	If xmlDoc.getElementsByTagName("report").item(0).attributes.item(xmlDoc.getElementsByTagName("report").item(0).attributes.length-1).name="printserialnumber" Then
		sPrintSerialNum = "checked"
	End If 

	
	'-- Report title alignment
	sRptAlignLeft = "" 
	sRptAlignCenter = ""
	sRptAlignRight = ""

	If xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(0).value = "left" Then
		sRptAlignLeft = "checked" 
		sRptAlignCenter = ""
		sRptAlignRight = ""
	ElseIf xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(0).value = "center" Then
		sRptAlignCenter = "checked" 
		sRptAlignLeft = ""
		sRptAlignRight = ""
	ElseIf xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(0).value = "right" Then
		sRptAlignRight = "checked"
		sRptAlignLeft = ""
		sRptAlignCenter = ""
	End if
	
	'--Start MITS 9521
	'--Ankur Saxena 07/11/2007
	'--Setting the report orientation
	sRptOrientPortrait = "" 
	sRptOrientLandscape = ""
	If xmlDoc.getElementsByTagName("report").item(0).attributes.item(2).value = "portrait" Then
		sRptOrientPortrait = "checked" 
		sRptOrientLandscape = ""
	ElseIf xmlDoc.getElementsByTagName("report").item(0).attributes.item(2).value = "landscape" Then
		sRptOrientPortrait = ""
		sRptOrientLandscape = "checked"
	End if
	'--End MITS 9521

	'-- Font Pop-Ups
	sRptFont = xmlDoc.getElementsByTagName("reportfont").item(0).attributes.item(0).text
	sRptStyle1 = xmlDoc.getElementsByTagName("reportfont").item(0).attributes.item(2).text
	sRptStyle2 = xmlDoc.getElementsByTagName("reportfont").item(0).attributes.item(3).text
	sRptSize = xmlDoc.getElementsByTagName("reportfont").item(0).attributes.item(1).text
	sRptTitleFont = xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(2).text
	sRptTitleStyle1 = xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(4).text
	sRptTitleStyle2 = xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(5).text
	sRptTitleSize = xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(3).text
	sUnderLine = xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(6).text 
	sStrikeOut = xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(7).text 
	sTitleColor = xmlDoc.getElementsByTagName("reporttitle").item(0).attributes.item(1).text 
	
	'-- Handling headers & footers
	sHeaderLeft = xmlDoc.getElementsByTagName("headerleft").item(0).text
	sHeaderCenter = xmlDoc.getElementsByTagName("headercenter").item(0).text
	sHeaderRight = xmlDoc.getElementsByTagName("headerright").item(0).text
	sFooterLeft = xmlDoc.getElementsByTagName("footerleft").item(0).text
	sFooterCenter = xmlDoc.getElementsByTagName("footercenter").item(0).text
	sFooterRight = xmlDoc.getElementsByTagName("footerright").item(0).text

	'-- Handling HTML page
	sHtmlNumLinesPerPage = xmlDoc.getElementsByTagName("htmlsettings").item(0).attributes.item(0).text
	sHtmlPageFormat = xmlDoc.getElementsByTagName("htmlsettings").item(0).attributes.item(1).text
	sHtmlPageBreak = xmlDoc.getElementsByTagName("htmlsettings").item(0).attributes.item(2).text
	sHtmlExportGraph = xmlDoc.getElementsByTagName("htmlsettings").item(0).attributes.item(3).text
	sHtmlGraphSameFile = xmlDoc.getElementsByTagName("htmlsettings").item(0).attributes.item(4).text
	sHtmlGraphTopOfPage = xmlDoc.getElementsByTagName("htmlsettings").item(0).attributes.item(5).text

	'-- Build array for column properties
	sColProperties = "-1"

	Dim objColumnList, iCount
	Set objColumnList = xmlDoc.getElementsByTagName("columnlist").item(0) 
	
	Dim objUtil

	If Not GetValidObject("InitSMList.CUtility", objUtil) Then
		Call ShowError(pageError, "Call to CUtility class failed.", True)
	End If

	Dim objColumnXML

	Dim sColName, sColWidthUnit, sColWidth, sBorderTop, sBorderLeft, sBorderRight
	Dim sBorderBottom, sBold, sItalic, sShade, sJustify, sColor, sAggPrefix
	
	For iCount = 0 To objColumnList.childNodes.length - 1
		Set objColumnXML = objColumnList.childNodes.item(iCount)
		Call objUtil.GetOptionsProperties(objColumnXML.XML,sColName, sColWidthUnit, sColWidth, _
				sBorderTop, sBorderLeft, sBorderRight,sBorderBottom, sBold, sItalic, sShade, _
				sJustify, sColor, sAggPrefix)
		
		
		If sColProperties = "-1" Then
			sColProperties = iCount & "+^+^+" & sColName & "+^+^+" & sColWidthUnit & "+^+^+" & sColWidth & "+^+^+" & sBorderTop & "+^+^+" & sBorderLeft & "+^+^+" & sBorderRight & "+^+^+" & sBorderBottom & "+^+^+" & sBold & "+^+^+" & sItalic & "+^+^+" & sShade & "+^+^+" & sJustify & "+^+^+" & sColor & "+^+^+" & sAggPrefix & "`````"
		Else
			sColProperties = sColProperties & "," & iCount & "+^+^+" & sColName & "+^+^+" & sColWidthUnit & "+^+^+" & sColWidth & "+^+^+" & sBorderTop & "+^+^+" & sBorderLeft & "+^+^+" & sBorderRight & "+^+^+" & sBorderBottom & "+^+^+" & sBold & "+^+^+" & sItalic & "+^+^+" & sShade & "+^+^+" & sJustify & "+^+^+" & sColor & "+^+^+" & sAggPrefix & "`````"
		End If
	Next	
	
End Function

Function RefreshSessionVariables()

	If Request.Form("hdSave") <> "-1" And Request.Form("hdSave") <> "" then
		objSession("Save") = Request.Form("hdSave")
	Else
		objSession("Save") = "-1"
	End If
	
	If Request.Form("hdSaveFinal") <> "-1" And Request.Form("hdSaveFinal") <> "" Then
		objSession("SaveFinal") = Request.Form("hdSaveFinal")
	Else
		objSession("SaveFinal") = "0"
	End If

	If Request.Form("hdArrayTF") <> "-1" And Request.Form("hdArrayTF") <> "" Then
		objSession("ArrayTF") = Request.Form("hdArrayTF")
	Else 
		objSession("ArrayTF") = "-1"
	End If
	
	If Request.Form("hdSelFields") <> "-1" And Request.Form("hdSelFields") <> "" Then
		objSession("ArrayP") = Request.Form("hdSelFields")
	Else 
		objSession("ArrayP") = "-1"
	End If
	
	If Request.Form("hdSelFieldsXML") <> "-1" And Request.Form("hdSelFieldsXML") <> "" Then
		objSession("ArrayPXML") = Request.Form("hdSelFieldsXML")
	Else
		objSession("ArrayPXML") = "-1"
	End If

	If Request.Form("hdPOptions") <> "-1" And Request.Form("hdPOptions") <> "" Then
		sColProperties = Request.Form("hdPOptions")
	Else
		sColProperties = "-1"
	End If	

	If Request.Form("hdDerError") <> "-1" And Request.Form("hdDerError") <> "" Then
		sError = Request.Form("hdDerError")
	Else
		sError = ""
	End If
		
End Function

%>
