<%@ Language=VBScript %>
<!-- #include file ="SessionInitialize.asp" -->
<!-- #include file ="customexecsummcode.inc" -->
<%
Public objSession
initSession objSession
ExpirePage

Dim lReportType 'As Long
Dim errorHTML 'As String
  
' Get report id and lookup report in  database
If Not IsEmpty(Request("reporttype")) then
    lReportType = clng(Request("reporttype") & "")
End If

UploadReport errorHTML 'If successfull, redirects to postreport.asp

'Otherwise Handle form submission error.%>
	<html><head><title></title>
	<!-- [Rajeev 11/12/2002 -- CSS Consolidation] link rel="stylesheet" href="../sm.css" type="text/css"/-->
<link rel="stylesheet" href="../RMNet.css" type="text/css"/>
	</head>
	<body class="MarginLeft10Font10">
	<b><font color="red">The following errors were encountered while submitting the report. Please try again. <br />If the error continues, please record the error message and <%=ERR_CONTACT%><br/><br/></font></b>
	<%="<ul class=""Margin0"">" & errorHTML & "</ul>"%>
	</body></html><%
Response.End

Function UploadReport(errorHTML)
	Dim sName, sDesc, sXML, objXML
	
	UploadReport = false
	Set objXML = CreateObject("Microsoft.XMLDOM")
	
	Select Case lReportType
		Case 1 'OSHA300 Report - Pull a blank template
			If Not objXML.load( Application(APP_DATAPATH) & "/oshareport.xml") Then  
				errorHTML = "<li>Osha 300 Report Template could not be loaded.</li><li>"& objXML.parseError.reason & "</li>"
				Exit Function 
			End If
			objXML.selectSingleNode("/report").setAttribute "type", "1" 
			sXML = objXML.xml
			sName = "OSHA 300 Report"
			sDesc = "Not Specified"
		Case 2 ' Executive Summary - Pull a blank template
			If Not objXML.load( Application(APP_DATAPATH) & "/executivesummary.xml") Then  
				errorHTML = "<li>Template could not be loaded.</li><li>"& objXML.parseError.reason & "</li>"
				Exit Function 
			End If
			objXML.selectSingleNode("/report").setAttribute "type", "2" 
			sXML = objXML.xml
			sName = objXML.selectSingleNode("/report").getAttribute("title")
			CustomGetName sName
			sDesc = "Not Specified"
		Case 3 'OSHA300A Report - Pull a blank template
			If Not objXML.load( Application(APP_DATAPATH) & "/osha300Areport.xml") Then  
				errorHTML = "<li>Osha 300A Report Template could not be loaded.</li><li>"& objXML.parseError.reason & "</li>"
				Exit Function 
			End If
			objXML.selectSingleNode("/report").setAttribute "type", "3" 
			sXML = objXML.xml
			sName = "OSHA 300A Report"
			sDesc = "Not Specified"
		Case 4 'OSHA301 Report - Pull a blank template
			If Not objXML.load( Application(APP_DATAPATH) & "/osha301report.xml") Then  
				errorHTML = "<li>Osha 300A Report Template could not be loaded.</li><li>"& objXML.parseError.reason & "</li>"
				Exit Function 
			End If
			objXML.selectSingleNode("/report").setAttribute "type", "4" 
			sXML = objXML.xml
			sName = "OSHA 301 Report"
			sDesc = "Not Specified"
		Case 5 'OSHA Sharps Log Report - Pull a blank template
			If Not objXML.load( Application(APP_DATAPATH) & "/oshasharpslog.xml") Then  
				errorHTML = "<li>Osha Sharps Log Report Template could not be loaded.</li><li>"& objXML.parseError.reason & "</li>"
				Exit Function 
			End If
			objXML.selectSingleNode("/report").setAttribute "type", "5" 
			sXML = objXML.xml
			sName = "OSHA Sharps Log"
			sDesc = "Not Specified"
	
		Case Else 'Your Report Type Here
			errorHTML = "<li>Unknown Report Type Requested.</li>"
			Exit Function
	End Select


	'Pass to smpostreport.asp for posting. 
	'Note: xml passes through the objSession because:
	'1.) Can't safely put the xml in a urlencoded string.
	'2.) objSession manages state automatically rather than the file system.
	objSession("reportxml") = sXML 'Will be retrieved by smpostreport.asp
	Response.Redirect "smpostreport.asp?" & "txtName=" & sName & "&txtDescription=" & sDesc
	Response.End
	
	UploadReport = true
End Function
%>