<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
<xsl:template match="/">
<html><head><title>Code Selection</title>
<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<script language="JavaScript" SRC="getcode.js"></script>
</head>
	<body class="Center32" onUnload="handleUnload()">
	<form name="frmdata" method="post">
	<xsl:attribute name="action"><xsl:choose><xsl:when test="codes/@postpage"><xsl:value-of select="codes/@postpage"/></xsl:when><xsl:otherwise>getcode.asp</xsl:otherwise></xsl:choose></xsl:attribute>
		<input type="hidden" name="bUnload" value="false"/>
		<input type="hidden" name="PageNumber" value="1"/>
		<input type="hidden" name="RecordCount"><xsl:attribute name="value"><xsl:value-of select="codes/@recordcount"/></xsl:attribute></input>
		<input type="hidden" name="PageCount"><xsl:attribute name="value"><xsl:value-of select="codes/@pagecount"/></xsl:attribute></input>
		<input type="hidden" name="code"><xsl:attribute name="value"><xsl:value-of select="codes/@tablename"/></xsl:attribute></input>
		<input type="hidden" name="LOB"><xsl:attribute name="value"><xsl:value-of select="codes/@lob"/></xsl:attribute></input>
		<input type="hidden" name="feesched"><xsl:attribute name="value"><xsl:value-of select="codes/@feesched"/></xsl:attribute></input>
		<input type="hidden" name="feesched2"><xsl:attribute name="value"><xsl:value-of select="codes/@feesched2"/></xsl:attribute></input>
		<input type="hidden" name="find"><xsl:attribute name="value"><xsl:value-of select="codes/@find"/></xsl:attribute></input>
		<input type="hidden" name="triggerdate"><xsl:attribute name="value"><xsl:value-of select="codes/@triggerdate"/></xsl:attribute></input>
		<input type="hidden" name="FormName"><xsl:attribute name="value"><xsl:value-of select="codes/@FormName"/></xsl:attribute></input>
		

		<xsl:if test="codes/@pagecount[.!='1']">
		<table width="95%" cellspacing="0" cellpadding="1" border="0">
		<tr bgcolor="#330099">
			<td colspan="4" class="headertext1"><xsl:if test="codes/@thispage">Page <xsl:value-of select="codes/@thispage"/> of <xsl:value-of select="codes/@pagecount"/></xsl:if></td>
			<td nowrap="" colspan="2" align="right" class="headertext1">
			&#160;&#160;&#160;&#160;&#160;&#160;|&#160;&#160;&#160;
			&#160;<xsl:choose><xsl:when test="codes/@firstpage"><a class="headerlink" href="#" onclick="getPage(1)">First</a></xsl:when><xsl:otherwise>First</xsl:otherwise></xsl:choose>&#160;&#160;|&#160;
			<xsl:choose><xsl:when test="codes/@previouspage"><a class="headerlink" href="#"><xsl:attribute name="onclick" >getPage(<xsl:value-of select="codes/@previouspage"/>)</xsl:attribute>&lt; Prev</a></xsl:when><xsl:otherwise>&lt; Prev</xsl:otherwise></xsl:choose> &#160;&#160;
			<xsl:choose><xsl:when test="codes/@nextpage"><a class="headerlink" href="#"><xsl:attribute name="onclick" >getPage(<xsl:value-of select="codes/@nextpage"/>)</xsl:attribute>Next &gt;</a></xsl:when><xsl:otherwise>Next &gt;</xsl:otherwise></xsl:choose> &#160;&#160;
			|&#160;&#160;
			<xsl:choose><xsl:when test="codes/@lastpage"><a class="headerlink" href="#"><xsl:attribute name="onclick">getPage(<xsl:value-of select="codes/@lastpage"/>)</xsl:attribute>Last</a></xsl:when><xsl:otherwise>Last</xsl:otherwise></xsl:choose>&#160;
			</td>
		</tr>
		</table>
		</xsl:if>
		<table border="0">
			<!--changed by ravi on 8th april 03 to show parent code-->
			<xsl:choose>
				<xsl:when test="codes/@tablename[.!='states']">
					<tr>
					<td class="Bold2">Code</td>
					<td class="Bold2">Description</td>
					<td class="Bold2">Parent Code</td>
					</tr>
					<xsl:for-each select="codes/code">
						<tr><td class="Bold2"><xsl:value-of select="@shortcode"/></td>
						<td class="Bold2"><a class="HeaderNavy" href="#"><xsl:attribute name="onClick">selCode(&quot;<xsl:value-of select="@shortcode"/>&#32;<xsl:value-of select="text()"/>&quot;,&quot;<xsl:value-of select="@id"/>&quot;)</xsl:attribute><xsl:value-of /></a></td><td  class="Bold2"><xsl:value-of select="@parentcode"/></td>
						</tr>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<tr>
					<td class="Bold2">Code</td>
					<td class="Bold2">Description</td>
					</tr>
					<xsl:for-each select="codes/code">
						<tr><td class="Bold2"><xsl:value-of select="@shortcode"/></td>
						<td class="Bold2"><a class="HeaderNavy" href="#"><xsl:attribute name="onClick">selCode(&quot;<xsl:value-of select="@shortcode"/>&#32;<xsl:value-of select="text()"/>&quot;,&quot;<xsl:value-of select="@id"/>&quot;)</xsl:attribute><xsl:value-of /></a></td>
						</tr>
					</xsl:for-each>				
				</xsl:otherwise>
			</xsl:choose>
		</table>
		<!--end-->
		<xsl:if test="codes/@pagecount[.!='1']">
		<table width="95%" cellspacing="0" cellpadding="1" border="0">
		<tr bgcolor="#330099">
			<td colspan="4" class="headertext1"><xsl:if test="codes/@thispage">Page <xsl:value-of select="codes/@thispage"/> of <xsl:value-of select="codes/@pagecount"/></xsl:if></td>
			<td nowrap="" colspan="2" align="right" class="headertext1">
			&#160;&#160;&#160;&#160;&#160;&#160;|&#160;&#160;&#160;
			&#160;<xsl:choose><xsl:when test="codes/@firstpage"><a class="headerlink" href="#" onclick="getPage(1)">First</a></xsl:when><xsl:otherwise>First</xsl:otherwise></xsl:choose>&#160;&#160;|&#160;
			<xsl:choose><xsl:when test="codes/@previouspage"><a class="headerlink" href="#"><xsl:attribute name="onclick" >getPage(<xsl:value-of select="codes/@previouspage"/>)</xsl:attribute>&lt; Prev</a></xsl:when><xsl:otherwise>&lt; Prev</xsl:otherwise></xsl:choose> &#160;&#160;
			<xsl:choose><xsl:when test="codes/@nextpage"><a class="headerlink" href="#"><xsl:attribute name="onclick" >getPage(<xsl:value-of select="codes/@nextpage"/>)</xsl:attribute>Next &gt;</a></xsl:when><xsl:otherwise>Next &gt;</xsl:otherwise></xsl:choose> &#160;&#160;
			|&#160;&#160;
			<xsl:choose><xsl:when test="codes/@lastpage"><a class="headerlink" href="#"><xsl:attribute name="onclick">getPage(<xsl:value-of select="codes/@lastpage"/>)</xsl:attribute>Last</a></xsl:when><xsl:otherwise>Last</xsl:otherwise></xsl:choose>&#160;
			</td>
		</tr>
		</table>
		</xsl:if>
	</form>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>