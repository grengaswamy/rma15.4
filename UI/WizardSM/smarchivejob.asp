<!-- #include file ="session.inc" -->
<%Option Explicit
' Author: Denis Basaric, 03/28/2000
' Move documents passed in doc variable
' Id's are comma delimited
%>
<%Public objSession
initSession objSession
%> 
<script language="vbscript" runat="server">
Sub CancelRequest()
	Response.Redirect "smrepqueue.asp"
	Response.End
End Sub
</script>

<%
Dim sJobList
Dim vArrJob, f, bOk
Dim sMoveTo
Dim objDocHandler
dim objNewDoc 
Dim iDocPathType
dim r, henv, db, rs
dim sCompleteFilename, sFilename, iPos, sJobName, sJobDesc

sJobList=Request("jobs")
If sJobList="" Then
	CancelRequest
End If
iDocPathType=objSessionStr(SESSION_DOCPATHTYPE)

Set objDocHandler=CreateObject("RMDoc.CDocHandler")	
objDocHandler.DSN = objSessionStr("DSN")
objDocHandler.UserId=objSessionStr(SESSION_USERID)
objDocHandler.UserLoginName=objSessionStr(SESSION_LOGINNAME)
objDocHandler.AppDataPath = Application(APP_DATAPATH)
objDocHandler.UserDataPath=Application(APP_USERDIRECTORYHOME)
objDocHandler.DocPathType=CSCStoreFileSystem  'if database storage must override need to retrieve file from CSCFileStorage object

'Response.Write "<br>iDocPathType=" & objSessionStr(SESSION_DOCPATHTYPE)
'Response.Write "<br>objDocHandler.DSN=" & objSessionStr("DSN")
'Response.Write "<br>objDocHandler.UserId=" & objSessionStr(SESSION_USERID)
'Response.Write "<br>objDocHandler.UserLoginName=" & objSessionStr(SESSION_LOGINNAME)
'Response.Write "<br>objDocHandler.AppDataPath=" & Application(APP_DATAPATH)
'Response.Write "<br>objDocHandler.UserDataPath=" & Application(APP_USERDIRECTORYHOME)
'Response.Write "<br>objDocHandler.DocPathType=" & iDocPathType

If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" Then
	sMoveTo=Trim(Request("lstFolders"))
	' Move documents to selected folder
	If Not IsNumeric(sMoveTo) Or sMoveTo="" Then
		' Go back to the source
		Set objDocHandler=Nothing
		Response.Redirect "smrepqueue.asp"
		Response.End
	Else
		set r= CreateObject("DTGRocket")
		henv = r.DB_InitEnvironment()
		db = r.DB_OpenDatabase(henv, Application("SM_DSN"), 0)
		
		rs = r.DB_CreateRecordset(db, "SELECT * FROM SM_JOBS WHERE COMPLETE <> 0 AND JOB_ID IN (" & sJobList & ") AND USER_ID = " & objSessionStr("UserID"), 3, 0)
		while Not r.DB_EOF(rs)
			sFileName=""
			
			if iDocPathType = CSCStoreFileSystem  Then
				r.DB_GetData rs, "OUTPUT_PATH", sCompleteFilename
				if sCompleteFilename & "" <> "none" then
					iPos = InStrRev(sCompleteFilename, "\")
					if iPos > 0 then
						sFilename = mid(sCompleteFilename, iPos + 1)
					else
						sFilename = sCompleteFilename
					end if
				end if
			else
				'output_path is the connection string to document database, so need to grab file name off of the URL
				r.DB_GetData rs, "OUTPUT_PATH_URL", sFilename
				If Len(Trim(sFilename & "")) > 0 then sFileName = Mid(sFileName, Instr(1, sFileName, "file=") + 5)
			end if
			
			if len(trim(sFileName)) > 0 Then
				Set objNewDoc = CreateObject("RMDoc.CDocument")
				objNewDoc.FileName=sFilename
				objNewDoc.FolderId = sMoveTo
				r.DB_GetData rs, "JOB_NAME", sJobName
				objNewDoc.Title = sJobName & ""
				r.DB_GetData rs, "JOB_DESC", sJobDesc
				objNewDoc.Notes = sJobDesc & ""
					'Response.Write "<br>sFileName=" & sFileName
					'Response.Write "<br>objNewDoc.FileName = " & sFileName
					'Response.Write "<br>objNewDoc.FolderId = " & sMoveTo
					'Response.Write "<br>objNewDoc.Title = " & sJobName & ""
					'Response.Write "<br>objNewDoc.Notes = " & sJobDesc & ""
					'Response.Write "objDocHandler.DocPathType= " & CSCStoreFileSystem	
					'Response.Write "objDocHandler.AddDocument objNewDoc "
					'Response.End
				objDocHandler.AddDocument objNewDoc
				set objNewDoc = Nothing
			End if
			
			r.DB_MoveNext rs
		wend
		Set objDocHandler=Nothing
		'set objCSCFileStorage = nothing
		r.DB_CloseRecordset CInt(rs), 2
		
		' Now, mark all the archived jobs as ARCHIVED so they don't show up in the report job list anymore
		r.DB_SQLExecute db, "UPDATE SM_JOBS SET ARCHIVED = -1 WHERE COMPLETE <> 0 AND JOB_ID IN (" & sJobList & ") AND USER_ID = " & objSessionStr("UserID")
		
		r.DB_CloseDatabase CInt(db)
		r.DB_FreeEnvironment CLng(henv)
		
		set r = Nothing
		Response.Redirect "smrepqueue.asp"
		Response.End
	End If
End If

Response.ExpiresAbsolute=DateAdd("h",-24,Now)
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>SORTMASTER Report Archive</title>
	<!--[Rajeev 11/19/2002 CSS Consolidation] link rel="stylesheet" href="forms.css" type="text/css"/-->
	<link rel="stylesheet" href="RMNet.css" type="text/css"/>
<script language="JavaScript">
<!--
function ValForm()
{
	var boolRet=false;
	if(document.frmData.lstFolders.selectedIndex<0)
		alert("Please select the Folder.");
	else
		boolRet=true;
		
	return boolRet;
}

//Set Focus to the first field
function pageLoaded()
{
	//Set focus to first field
	 var i; 
	 for (i=0;i<document.frmData.length;i++)			
		{	 				
 
          if((document.frmData.item(i).type=="text") || (document.frmData.item(i).type=="select-one") || (document.frmData.item(i).type=="textarea"))
			{
				document.frmData.item(i).focus();
				break;
			}
		}
}
-->

</script>
</head>
<body class="10pt" onload="pageLoaded();">
<form name="frmData" method="post" action="smarchivejob.asp" onSubmit="return ValForm()">
<input type="hidden" name="jobs" value="<%=sJobList%>" />
<table class="singleborder" align="center">
	<tr><td class="ctrlgroup">Archive Report Jobs</td></tr>
	<tr>
		<td nowrap="" class="datatd"><b>Please select the folder to archive job(s) to:</b></td>
	</tr>
	<tr>
		<td class="datatd"><p align="center"><select name="lstFolders" size="15"><%=objDocHandler.getFoldersAsOption(0)%></select></p></td>
	</tr>
	<tr>
		<td align="center" nowrap="">
			<input type="submit" class="button" name="btnView" value="  Archive  ">
			<input type="button" class="button" name="btnView" value=" Cancel " onClick="document.location='smrepqueue.asp'">
		</td>
	</tr>
</table>
</form>
<%Set objDocHandler=Nothing%>

</body>
</html>