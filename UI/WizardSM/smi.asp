<% OPTION EXPLICIT %>
<!-- #include file ="session.inc" -->
<!-- #include file ="dbutil.inc" -->
<!-- #include file ="criteriautils.inc" -->
<%Public objSession
initSession objSession
%> 
<SCRIPT LANGUAGE="VBScript" RUNAT=SERVER>

dim smi
dim lReportID
Dim sXML, objXML, objNode, sWhere
'Safeway Atul OSHA Privacy Case
Dim sPrivacyCase
sPrivacyCase = Request("PrivacyCaseFlag")
'Safeway Code End
lReportID = CLng(Request("reportid"))

'Fetch this Report XML
OpenSMDatabase
sWhere = "SM_REPORTS_PERM.USER_ID = " & objSession("UserID") & " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND SM_REPORTS.REPORT_ID = " & lReportID
sXML = GetSingleSMValue("REPORT_XML","SM_REPORTS,SM_REPORTS_PERM",sWhere)
CloseSMDatabase
If sXML = "" Then
	Response.Write "Error: Selected report could not be found on the server."
	Response.End
end if

'Load this Report XML 
Set objXML = CreateObject("Microsoft.XMLDOM")
if (objXML.loadXML(sXML) = false) then
	Response.Write "Error: Selected report could not be loaded.  Please try uploading it again."
	Response.End
end if

'Determine what type of report this is. (And what engine to run.)
Set objNode = objXML.selectSingleNode("/report")
If IsEmpty(objNode) or objNode is nothing then
	Response.Write "Error: Selected report does not have a top level report element with the 'type' attribute.  Please try uploading it again."
	Response.End
end if	
Select Case objNode.getAttribute("type")
	Case "1","2","3","4","5"	' Known Custom Reports (OSHA & Exec Summary)
		objSession("reportcriteriaxml") = sXML
		'Safeway Atul 
		Response.Redirect "CriteriaEdit.asp?reportid=" & cstr(lreportID) & "&PrivacyCaseFlag=" &  sPrivacyCase
		Response.End
	Case Else	' Assume SortMaster
		' Generate criteria page
		'tkr 10/2003.  my design could be better.  these only need to be set if they are customized
		'01/04/07 REM   UMESH
		Dim bNotifyLink, bNotifyAttach, bNotifyEmail, iNotifyDefault, bRunDTTM
		Dim  sNotifyNone_Text,   sRunDTTM_Text, sNotifyLink_Text, sNotifyEmbed_Text, sNotifyOnly_Text,  sRunImmediate_Text, sRunDefault
		
		
		 bNotifyLink=bNOTIFY_LINK 
		 bNotifyAttach=bNOTIFY_EMBED 
		 bNotifyEmail=bNOTIFY_ONLY 
		 iNotifyDefault=NOTIFY_DEFAULT 
		 bRunDTTM=bRUN_DTTM
		 sNotifyNone_Text=NOTIFYNONE_TEXT   
		 sRunDTTM_Text=RUNDTTM_TEXT 
		 sNotifyLink_Text=NOTIFYLINK_TEXT 
		 sNotifyEmbed_Text=NOTIFYEMBED_TEXT 
		 sNotifyOnly_Text=NOTIFYONLY_TEXT 
		 sRunImmediate_Text=RUNIMMEDIATE_TEXT
		 sRunDefault=RUN_DEFAULT
		
		OpenSessDatabase
		sWhere = "FILENAME='customize_custom'"
		sXML = GetSingleSessValue("CONTENT","CUSTOMIZE",sWhere)
		CloseSessDatabase
		if sXML<>"" then
			Set objXML = CreateObject("Microsoft.XMLDOM")
			if (objXML.loadXML(sXML) = false) then
				Response.Write "Error: Selected Customize File could not be loaded.  Please try uploading it again."
				Response.End
			end if
			
			bNotifyLink = objXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailwithLink/show").text
			if bNotifyLink ="" then bNotifyLink="0"
			sNotifyLink_Text = objXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailwithLink/text").text
			
			bNotifyAttach = objXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailwithReport/show").text
			if bNotifyAttach ="" then bNotifyAttach="0"
			sNotifyEmbed_Text= objXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailwithReport/text").text
			
			bNotifyEmail = objXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailOnly/show").text
			if bNotifyEmail ="" then bNotifyEmail="0"
			sNotifyOnly_Text = objXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/SendEmailOnly/text").text
			
			sNotifyNone_Text = objXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/None/text").text
			
			bRunDTTM = objXML.SelectSingleNode("//RMAdminSettings/ReportRunOptions/AtSpecificDate/show").text
			if bRunDTTM ="" then bRunDTTM="0"
			sRunDTTM_Text = objXML.SelectSingleNode("//RMAdminSettings/ReportRunOptions/AtSpecificDate/text").text
			
			sRunImmediate_Text = objXML.SelectSingleNode("//RMAdminSettings/ReportRunOptions/Immediately/text").text
			iNotifyDefault = objXML.SelectSingleNode("//RMAdminSettings/ReportEmailOptions/DefaultOption").text
			if iNotifyDefault="" then iNotifyDefault="3"
			sRunDefault = objXML.SelectSingleNode("//RMAdminSettings/ReportRunOptions/DefaultOption").text
			if sRunDefault="" then sRunDefault="0"
		end if
		
		
		Set smi = CreateObject("SMInterface.SMI")
		smi.NotifyLink = CBool(bNotifyLink):		smi.NotifyLink_Text = sNotifyLink_Text
		smi.NotifyAttach = CBool(bNotifyAttach):	smi.NotifyEmbed_Text = sNotifyEmbed_Text
		smi.NotifyEmail = CBool(bNotifyEmail):		smi.NotifyOnly_Text = sNotifyOnly_Text
		smi.NotifyNone_Text = sNotifyNone_Text:		smi.RunDTTM = CBool(bRunDTTM)
		smi.NotifyDefault = CInt(iNotifyDefault):	smi.RunImmediate_Text = sRunImmediate_Text
		smi.RunDTTM_Text = sRunDTTM_Text:			smi.RunDefault = sRunDefault
		
		'END   REM Umesh
		Response.Write smi.generateReportPage(lReportID, objSession("UserID"), objSession("EMail"), objSession("DSN"), Application("SM_DSN"))
		Set smi = Nothing
End Select


</SCRIPT>