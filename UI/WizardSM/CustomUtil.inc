<!-- #include file ="dbutil.inc" -->
<script language="VBScript" runat="server">
Public Sub AppStart()
	'pull configuration files from the database and overwrite the files on the web server
	on error resume next
	Application.Lock
	Dim sPath, sExistingRecAlert
	Dim iTextMlCols, iTextMlRows, iFreecodeCols, iFreeCodeRows, iReadOnlyMemoCols, iReadOnlyMemoRows, iMemoCols, iMemoRows
	Dim sAppConstants, sCriteriaUtilsConfig
	
	sPath = Application(APP_PATH)
	if Right(sPath, 1) <> "\" Then sPath = sPath & "\"
	
	sAppConstants = sPath & "appconstantsconfig.inc"
	sCriteriaUtilsConfig = sPath & "CriteriaUtilsConfig.inc"
	
	FetchFile "appconstantsconfig.inc", sPath, NULL
	FetchFile "criteriautilsconfig.inc", sPath, NULL
	FetchFile "customformscode.inc", sPath, NULL
	FetchFile "customexecsummcode.inc", sPath, NULL
	FetchFile "customdocscode.inc", sPath, NULL
	FetchFile "customlookupcode.inc", sPath, NULL
	FetchFile "customizecode.inc", sPath, NULL
	FetchFile "top1.gif", sPath & "img\", NULL
	FetchFile "top2.gif", sPath & "img\", NULL
	FetchFile "top3.gif", sPath & "img\", NULL
	'this value is in form.js.  do not store form.js in database, just replace the one line
	sExistingRecAlert=Trim(ReplaceValue(sPath & "appconstantsconfig.inc", "EXISTREC_ALERT", null) & "")
	ReplaceValue sPath & "form.js", "EXISTREC_ALERT", sExistingRecAlert

	iTextMlCols=CInt("0" & Trim(ReplaceValue(sAppConstants, "TEXTML_COLS", null)))
	iTextMlRows=CInt("0" & Trim(ReplaceValue(sAppConstants, "TEXTML_ROWS", null)))
	iFREECODECols=CInt("0" & Trim(ReplaceValue(sAppConstants, "FREECODE_COLS", null)))
	iFREECODERows=CInt("0" & Trim(ReplaceValue(sAppConstants, "FREECODE_ROWS", null)))
	iREADONLYMEMOCols=CInt("0" & Trim(ReplaceValue(sAppConstants, "READONLYMEMO_COLS", null)))
	iREADONLYMEMORows=CInt("0" & Trim(ReplaceValue(sAppConstants, "READONLYMEMO_ROWS", null)))
	iMEMOCols=CInt("0" & Trim(ReplaceValue(sAppConstants, "MEMO_COLS", null)))
	iMEMORows=CInt("0" & Trim(ReplaceValue(sAppConstants, "MEMO_ROWS", null)))
	
'tkr 10/2003
'--ReplaceValue() will now add booleans to appconstantsconfig.inc or criteriautilsconfig.inc if the boolean does not exist.  Call ReplaceValue() in the AppStart() function on all booleans added to appconstantsconfig.inc or criteriautilsconfig.inc since the first install of the customization module, 
'--Booleans in the other custom files that can contain configurable boolean values (customformscode.inc,customexecsummcode.inc,customdocscode.inc,customlookupcode.inc) will be unique for each customer.
'--Customizecode.inc does NOT contain any boolean settings or any settings that the customer can change.  It contains the page reached by the link "Custom For [Customer Name]".
'--A new "Customization Files" link will allow users to overwrite customization files.
'All custom files are overwritten from database on app startup.  Booleans in appconstantsconfig.inc or criteriautilsconfig.inc are handled with the ReplaceValue() call above, but to update code in other custom files you must either:
'A. Have the customer use the "Customization Files" interface in the customization module;
'B. Insert the custom files into the database programmatically with an installation program;
'C. Have the customer do the following:
	'1.  login to Riskmaster (this will overwrite the custom files on the disk from the database if AppStart is triggered from Index.asp).
	'2.  copy the new custom files into the RMNet and/or RMNet\Admin folders
	'3.  press "submit" button from any customization screen (this will update the database with files from disk)

	'below are booleans added after the first install of the customization module:
	
	'show/hide buttons in hometop.asp
	ReplaceValue sAppConstants, "bSHOW_ADMINPAGE", NULL
	ReplaceValue sAppConstants, "bSHOW_DOCUMENTS", NULL
	ReplaceValue sAppConstants, "bSHOW_SEARCH", NULL
	ReplaceValue sAppConstants, "bSHOW_FILES", NULL
	ReplaceValue sAppConstants, "bSHOW_DIARIES", NULL
	ReplaceValue sAppConstants, "bSHOW_REPORTS", NULL
	ReplaceValue sAppConstants, "bSHOW_LOGOUT", NULL
	ReplaceValue sAppConstants, "bSHOW_HELP", NULL
	
	'show/hide links in searchmenu.asp
	ReplaceValue sAppConstants, "bSEARCH_CLAIMS", NULL
	ReplaceValue sAppConstants, "bSEARCH_EVENTS", NULL
	ReplaceValue sAppConstants, "bSEARCH_EMP", NULL
	ReplaceValue sAppConstants, "bSEARCH_ENT", NULL
	ReplaceValue sAppConstants, "bSEARCH_VEH", NULL
	ReplaceValue sAppConstants, "bSEARCH_POL", NULL
	ReplaceValue sAppConstants, "bSEARCH_FUNDS", NULL
	ReplaceValue sAppConstants, "bSEARCH_PAT", NULL
	ReplaceValue sAppConstants, "bSEARCH_PHY", NULL
	
	'show/hide Add Payment/Add Collection button in reserves.asp
	ReplaceValue sAppConstants, "bADD_PAYM", NULL
	ReplaceValue sAppConstants, "bADD_COLL", NULL
	
	ReplaceValue sAppConstants, "bOFAC_CHECK", "-1"
	
	'show/hide links in reportsmenu.asp
	ReplaceValue sCriteriaUtilsConfig, "sREPORT_TITLE", "Reports"
	ReplaceValue sCriteriaUtilsConfig, "sREPORT_TITLE_DEFAULT", "Reports"
	ReplaceValue sCriteriaUtilsConfig, "bREPORT_TITLE", "-1"

	ReplaceValue sCriteriaUtilsConfig, "sSMNET_TITLE", "SM"
	ReplaceValue sCriteriaUtilsConfig, "sSMNET_TITLE_DEFAULT", "SM"
	ReplaceValue sCriteriaUtilsConfig, "bSMNET_TITLE", "-1"
	ReplaceValue sCriteriaUtilsConfig, "bDESIGNER_RPT", "-1"
	ReplaceValue sCriteriaUtilsConfig, "bDRAFT_RPT", "-1"
	ReplaceValue sCriteriaUtilsConfig, "bPOSTDRAFT_RPT", "-1"

	ReplaceValue sCriteriaUtilsConfig, "sEXECSUMM_TITLE", "Exec. Summary"
	ReplaceValue sCriteriaUtilsConfig, "sEXECSUMM_TITLE_DEFAULT", "Exec. Summary"
	ReplaceValue sCriteriaUtilsConfig, "bEXECSUMM_TITLE", "-1"
	
	ReplaceValue sCriteriaUtilsConfig, "sOTHERRPT_TITLE", "Other Reports"
	ReplaceValue sCriteriaUtilsConfig, "sOTHERRPT_TITLE_DEFAULT", "Other Reports"
	ReplaceValue sCriteriaUtilsConfig, "bOTHERRPT_TITLE", "-1"

	ReplaceValue sCriteriaUtilsConfig, "sDCC_LABEL", "DCC"
	ReplaceValue sCriteriaUtilsConfig, "sDCC_LABEL_DEFAULT", "DCC"

	'end booleans added after the first install of the customization module
	sPath = Application(APP_PATH)
	if Right(sPath, 1) <> "\" Then sPath = sPath & "\"

	InsertFile "appconstantsconfig.inc", sPath, NULL, false
	InsertFile "criteriautilsconfig.inc", sPath, NULL, false
	
	UpdateTextArea_Powerviews iTextMlCols, iTextMlRows, iFreeCodeCols, iFreeCodeRows, iReadOnlyMemoCols, iReadOnlyMemoRows, iMemoCols, iMemoRows
	UpdateTextArea_Disk iTextMlCols, iTextMlRows, iFreeCodeCols, iFreeCodeRows, iReadOnlyMemoCols, iReadOnlyMemoRows, iMemoCols, iMemoRows
Application(APP_SMDDSN) = "DSN=SM;UID=;PWD=;"
'-- Delete the reports in SMWD_TEMP_REPORTS table, if any
Dim objDataManager
On Error Resume Next
Set objDataManager = Server.CreateObject("InitSMList.CDataManager")
objDataManager.m_RMConnectStr = Application("SM_DSN")
objDataManager.DeleteAllTempReports
Set objDataManager = Nothing
On Error Goto 0

	Application(APP_APPSTART)=true
	Application.Unlock
End Sub

'replace or fetch value from text file of construct: ValueName="Value"
'if sNewValue = null then just fetch current value
Public Function ReplaceValue(sFile, sValueName, sNewValue)
    Dim objFSO, txs, fl
    Dim sNewLine, sCurrentValue, sFileName, s, sTmp
    Dim vArray, sLine
    Dim vError(2)
    Dim b
    Dim sSemiColon, sScript
    Const sDefaultValue = "-1"
    
    sScript = Chr(60) & "/script" & Chr(62)
          
    'if this is a javacript file, append semicolon to the end of the constant after the closing quote
    If Right(sFile, 2) = "js" Then
        sSemiColon = ";"
    Else
        sSemiColon = ""
    End If
       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    sFileName = Mid(sFile, InStr(1, sFile, "\") + 1)
    
    If Not objFSO.FileExists(sFile) Then
        Set objFSO = Nothing
        Err.Raise vbObjectError + 1000, "customize.asp", sFileName & " does not exist"
    End If
        
    'user wants to replace a value
    If Not IsNull(sNewValue) Then
        'no double-quotes in new value
        sNewValue = Replace(sNewValue, Chr(34), "'")
    
        'make backup copy to restore in case of error
        If objFSO.FileExists(sFile & ".backup") Then
            Set fl = objFSO.GetFile(sFile & ".backup")
            If fl.Attributes And 1 Then fl.Attributes = fl.Attributes - 1  'clear ReadOnly
            Set fl = Nothing
        End If
        objFSO.CopyFile sFile, sFile & ".backup"
        If Not objFSO.FileExists(sFile & ".backup") Then Err.Raise vbObjectError + 1000, "customize.asp", "Unable to create backup file."
        
        b = False
        Set txs = objFSO.OpenTextFile(sFile, 1)
        s = txs.ReadAll
        txs.Close
        Set txs = Nothing
        vArray = Split(s, vbCrLf)
        For Each sLine In vArray
            sLine = CStr(sLine & "")
            If InStr(1, sLine, sValueName, 1) Then
                sCurrentValue = sLine
                b = GetCurrentValueFromLine(sValueName, sCurrentValue)
                If b Then Exit For
            End If
        Next
        
        If Not b Then
            'Err.Raise vbObjectError + 1000, "customize.asp", "'" & sValueName & "' not found in " & sFileName
            'tkr 10/2003 if value not exist insert it if this is NOT a javascript page.  raise an error if this is a .js page.  sSemiColon tells if this is .js page
            If sSemiColon = ";" Then Err.Raise vbObjectError + 1000, "customize.asp", "'" & sValueName & "' not found in " & sFileName
            'remove the closing script tag, append new value, re-append the closing script tag
            s = Replace(s, sScript, "Public Const " & sValueName & "=" & Chr(34) & sNewValue & Chr(34) & vbCrLf & sScript)
        Else
            sTmp = Replace(sLine, Chr(34), "")   'double quotes interfere with Replace() function
            If Trim(sCurrentValue) = "" Then
                sNewLine = sTmp & Chr(34) & sNewValue & Chr(34) & sSemiColon
            Else
                sNewLine = Replace(sTmp, "=" & sCurrentValue, "=" & Chr(34) & sNewValue & Chr(34)) & sSemiColon
            End If
            s = Replace(s, sLine, sNewLine)
        End If
        
        Set fl = objFSO.GetFile(sFile)
        If fl.Attributes And 1 Then fl.Attributes = fl.Attributes - 1  'clear ReadOnly
        
        On Error Resume Next
        Set txs = fl.OpenAsTextStream(2)
        If Err.Number <> 0 Then
            With Err
                vError(0) = .Number: vError(1) = .Source: vError(2) = .Description
            End With
            txs.Close
            Set txs = Nothing
            On Error GoTo 0
            HandleError objFSO, sFile, vError(0), vError(1), vError(2)
        End If
        txs.Write s
        txs.Close
        Set txs = Nothing
        If Err.Number <> 0 Then
            On Error GoTo 0
            HandleError objFSO, sFile, Err.Number, Err.Source, Err.Description
        End If
        On Error GoTo 0
    End If
    
    b = False
    Set txs = objFSO.OpenTextFile(sFile, 1)
    Do Until txs.AtEndOfStream
        sCurrentValue = txs.ReadLine
        If InStr(1, sCurrentValue, sValueName, 1) Then
            b = GetCurrentValueFromLine(sValueName, sCurrentValue)
            If b Then Exit Do
        End If
    Loop
    
    If Not b Then
        'tkr 10/2003 if value not exist insert it if this is NOT a javascript page.  raise an error if this is a .js page.  sSemiColon tells if this is .js page
        If sSemiColon = ";" Then Err.Raise vbObjectError + 1000, "customize.asp", "'" & sValueName & "' not found in " & sFileName
        txs.Close   '(must close and re-open file to get to top and do ReadAll)
        Set txs = objFSO.OpenTextFile(sFile, 1)
        s = txs.ReadAll
        txs.Close
        'remove the closing script tag, append new value, re-append the closing script tag
        s = Replace(s, sScript, "Public Const " & sValueName & "=" & Chr(34) & sDefaultValue & Chr(34) & vbCrLf & sScript)
        Set txs = objFSO.OpenTextFile(sFile, 2)
        txs.Write s
    End If
    
    txs.Close
    Set txs = Nothing
   
    Set objFSO = Nothing
    sCurrentValue = Replace(sCurrentValue, "\", "\\")
    sCurrentValue = Replace(sCurrentValue, "'", "\'")
    ReplaceValue = sCurrentValue
    
End Function

'check that the value name is identical to the value name desired (not just share some characters)
'if identical, separate out the current value and return true
Private Function GetCurrentValueFromLine(sDesiredValueName, sLine)
    Dim iQuote, iClosingQuote, iSingleQuote, iEquals, sValueName, v
    GetCurrentValueFromLine = False
    
    iQuote = InStr(1, sLine, Chr(34))
    iEquals = InStr(1, sLine, "=")
    iClosingQuote = InStr(iQuote + 1, sLine, Chr(34))  'closing quote.  don't do InStrRev, could be a quote in the comment
    
    If iClosingQuote > 0 Then  'the value is surrounded by quotes
        iSingleQuote = InStr(iClosingQuote, sLine, "'")   'find the comment, if any
    Else   'no quotes around value (numeric)
        iSingleQuote = InStr(1, sLine, "'")
    End If
        
    If iQuote > iEquals And iClosingQuote < iSingleQuote Then   'the value is surrounded by quotes and the line has a comment
        If Len(Trim(sLine)) > iClosingQuote Then sLine = Mid(sLine, 1, iClosingQuote)  'something exists after the closing quote, lop it off
    ElseIf iSingleQuote > iEquals Then   'value is not surrounded by comments, there is a comment on the line, lop it off
        sLine = Mid(sLine, 1, iSingleQuote - 1)
    End If
    
    v = Split(sLine, "=")
    sValueName = Trim(v(0))
    If InStr(1, sValueName, " ") Then sValueName = Trim(Mid(sValueName, InStrRev(sValueName, " ")))  'assume value name is everything between first blank character and the "=" sign
    If LCase(sValueName) = LCase(sDesiredValueName) Then
        sLine = Replace(Mid(sLine, InStr(1, sLine, "=") + 1), Chr(34), "")  'don't use v(1); the value might contain the "=" sign
        GetCurrentValueFromLine = True
    End If
        
End Function

Private Function HandleError(objFSO, sFile, lNumber, sSrc, sDesc)
   
    On Error Resume Next
    If objFSO.FileExists(sFile & ".backup") Then objFSO.CopyFile sFile & ".backup", sFile
    
    On Error GoTo 0
    Err.Raise lNumber, sSrc, sDesc
    
End Function

Private Function HandleError2(txs, objFSO, sMsg)
    Dim vError(2)
    vError(0) = err.Number
    vError(1) = err.Source
    vError(2) = err.Description
    On Error Resume Next
    txs.Close
    Set txs = Nothing
    Set objFSO = Nothing
    On Error GoTo 0
    err.Raise vError(0), vError(1), sMsg & ", Err.Description=" & vError(2)
End Function

'uploads the file into the Temp directory and returns full filename in sFileName
Public Function UploadFile(sFileName,lErrNum,sErrSrc,sErrDesc)
	Dim objUpload, objFSO
	Dim s, sFolder
	
	UploadFile=false

	Set objFSO=CreateObject("Scripting.FileSystemObject")
	sFolder= objFSO.GetSpecialFolder(2)' TemporaryFolder
	set objFSO=nothing
	
	Set objUpload=CreateObject("IISUpload.CUpload")
	objUpload.UploadFolder=sFolder
	s=objUpload.Upload()
	
	If Right(sFolder,1) <> "\" Then sFolder=sFolder & "\"
	
	If objUpload.ErrNumber = 0 And objUpload.FileName<>"" Then
		sFileName=sFolder & objUpload.FileName
	elseif objUpload.ErrNumber=0 And objUpload.FileName="" Then
		sFileName=""
		lErrNum=0
		sErrSrc=""
		sErrDesc=""
		set objUpload=nothing
		Exit Function
	else
		lErrNum=objUpload.ErrNumber
		sErrSrc="objUpload"
		sErrDesc=objUpload.ErrDesc
		set objUpload=nothing
		Exit Function
	end if
	
	set objUpload=nothing
	UploadFile=true
	
End Function

Public Function ReplaceFile(sFile, sOverwrite, bErase)
    Dim objFSO
    Dim fl
    Dim sBackup, sExt, i
    
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    If Not objFSO.FileExists(sFile) Then 
		set objFSO=nothing
		err.Raise vbObjectError + 1000, "ReplaceFile()", "The file you attempted to copy into the application was not copied into the appropriate folder or you lack permission to view it, unable to proceed."
	end if
	
    'make backup copy
    If objFSO.FileExists(sOverwrite & ".backup") Then
        Set fl = objFSO.GetFile(sOverwrite & ".backup")
        If fl.Attributes And 1 Then fl.Attributes = fl.Attributes - 1  'clear ReadOnly
        Set fl = Nothing
    End If
    objFSO.CopyFile sOverwrite, sOverwrite & ".backup"
    If Not objFSO.FileExists(sOverwrite & ".backup") Then Err.Raise vbObjectError + 1000, "customize.asp", "Unable to create backup file."

    'do overwrite
    If objFSO.FileExists(sOverwrite) Then
		Set fl = objFSO.GetFile(sOverwrite)
		If fl.Attributes And 1 Then fl.Attributes = fl.Attributes - 1  'clear ReadOnly
		Set fl = Nothing
    End If
    objFSO.CopyFile sFile, sOverWrite, True
       
    'delete sFile (usually sFile created in temp folder using IISUpload or CZip component)
    If bErase Then
        On Error Resume Next
        Set fl = objFSO.GetFile(sFile)
        If fl.Attributes And 1 Then fl.Attributes = fl.Attributes - 1  'clear ReadOnly
        Set fl = Nothing
        objFSO.DeleteFile sFile, True
    End If
    
    set objFSO=nothing
        
End Function
'insert sPath & sFileName into database.
'sFolder is optional arg to identify file in addition to filename, is not part of physical path
Public Function InsertFile(sFileName, sPath, sFolder, bBinary)
    Dim sSQL, rs
    Dim objFSO, oZip
    Dim txs
    Dim s, sValue
    Dim ID
    Dim v
    Const iType = 0     'this is type for entire file.  1 is type for partial file
    Dim iBinary
    
    sFolder = Trim(sFolder & "")
    sFileName = Trim(sFileName & "")
    sPath = Trim(sPath & "")
    If CBool(bBinary & "") Then
        iBinary = 1
    Else
        iBinary = 0
    End If
    If Len(Trim(sFileName)) = 0 Then err.Raise vbObjectError + 1000, "InsertFile()", "Required parameter FileName missing."
    If Len(Trim(sPath)) = 0 Then err.Raise vbObjectError + 1000, "InsertFile()", "Required parameter Path missing."
    If Right(sPath, 1) <> "\" Then sPath = sPath & "\"

    Set objFSO = CreateObject("Scripting.FileSystemObject")
    If Not objFSO.FileExists(sPath & sFileName) Then
        Set objFSO = Nothing
        err.Raise vbObjectError + 1000, "InsertFile()", "File Not Exist: " & sFileName
    End If
    
    On Error Resume Next
    If iBinary Then
        Set oZip = CreateObject("ZipUtil.CZipUtil")
        s = oZip.ZIPFile(CStr(sPath & sFileName))
        Set oZip = Nothing
    Else
        Set txs = objFSO.OpenTextFile(sPath & sFileName, 1)
        If err.Number <> 0 Then HandleError2 txs, objFSO, "InsertFile(), Error opening file: " & sFileName
        s = txs.ReadAll
        If err.Number <> 0 Then HandleError2 txs, objFSO, "InsertFile(), Error reading file: " & sFileName
        txs.Close
        Set txs = Nothing
        Set objFSO = Nothing
    End If
    On Error GoTo 0
    
    OpenSessDatabase
    
    If Len(sFolder) = 0 Then
        sSQL = "SELECT ID FROM CUSTOMIZE WHERE FILENAME = '" & sFileName & "' AND TYPE = " & iType
    Else
        sSQL = "SELECT ID FROM CUSTOMIZE WHERE FILENAME = '" & sFileName & "' AND FOLDER = '" & sFolder & "' AND TYPE = " & iType
    End If
    rs = CInt(objRocketSess.DB_CreateRecordset(hDBSess, sSQL, 3, 0))
    If objRocketSess.DB_EOF(rs) Then
        objRocketSess.DB_CloseRecordset CInt(rs), 2
        sSQL = "SELECT MAX(ID) FROM CUSTOMIZE"
        rs = CInt(objRocketSess.DB_CreateRecordset(hDBSess, sSQL, 3, 0))
        If Not objRocketSess.DB_EOF(rs) Then
            objRocketSess.DB_GetData rs, 1, v
            ID = CInt("0" & v) + 1
        Else
            ID = 1
        End If
        objRocketSess.DB_CloseRecordset CInt(rs), 2
        If Len(sFolder) = 0 Then
            sSQL = "INSERT INTO CUSTOMIZE(ID,FILENAME,TYPE,IS_BINARY) VALUES(" & ID & ",'" & sFileName & "'," & iType & "," & iBinary & ")"
        Else
            sSQL = "INSERT INTO CUSTOMIZE(ID,FILENAME,FOLDER,IS_BINARY) VALUES(" & ID & ",'" & sFileName & "','" & sFolder & "'," & iType & "," & iBinary & ")"
        End If
        objRocketSess.DB_SQLExecute hDBSess, sSQL
    Else
        objRocketSess.DB_GetData rs, 1, v
        ID = CInt(v)
        objRocketSess.DB_CloseRecordset CInt(rs), 2
    End If

    sSQL = "SELECT CONTENT FROM CUSTOMIZE WHERE ID = " & ID
    rs = objRocketSess.DB_CreateRecordset(hDBSess, sSQL, 3, 0)
    objRocketSess.DB_CloseRecordset CInt(rs), 1
    objRocketSess.DB_Edit rs
    objRocketSess.DB_PutData rs, "CONTENT", s
    objRocketSess.DB_Update rs
    objRocketSess.DB_CloseRecordset CInt(rs), 2
    
    CloseSessDatabase
    
   
End Function
'file is written to sPath & sFileName
Public Function FetchFile(sFileName, sPath, sFolder)
    Dim sSQL, rs
    Dim objFSO, oZip
    Dim txs
    Dim s, sTmp
    Const iType = 0     'this is type for entire file.  1 is type for partial file
    Dim bBinary
    
    sFolder = Trim(sFolder & "")
    sFileName = Trim(sFileName & "")
    sPath = Trim(sPath & "")
    If Len(Trim(sFileName)) = 0 Then err.Raise vbObjectError + 1000, "InsertFile()", "Required parameter FileName missing."
    If Len(Trim(sPath)) = 0 Then err.Raise vbObjectError + 1000, "InsertFile()", "Required parameter Path missing."
    If Right(sPath, 1) <> "\" Then sPath = sPath & "\"

    Set objFSO = CreateObject("Scripting.FileSystemObject")
    If Not objFSO.FolderExists(sPath) Then
        Set objFSO = Nothing
        err.Raise vbObjectError + 1000, "FetchFile()", "Folder Not Exist: " & sPath
    End If
        
    OpenSessDatabase
    sSQL = "SELECT CONTENT, IS_BINARY FROM CUSTOMIZE WHERE FILENAME='" & sFileName & "' AND TYPE=" & iType
    rs = CInt(objRocketSess.DB_CreateRecordset(hDBSess, sSQL, 3, 0))
    If objRocketSess.DB_EOF(rs) Then
		set objFSO=nothing
        objRocketSess.DB_CloseRecordset CInt(rs), 2
        CloseSessDatabase
        'err.Raise vbObjectError + 1000, "FetchFile()", "Record not exist where Filename=" & sFileName & " And Type=" & iType
        exit function
    End If
    objRocketSess.DB_GetData rs, 1, s
    s = CStr(s & "")
    objRocketSess.DB_GetData rs, 2, bBinary
    If IsNull(bBinary) Then bBinary = 0
    objRocketSess.DB_CloseRecordset CInt(rs), 2
    CloseSessDatabase
        
    If bBinary Then
        Set oZip = CreateObject("ZipUtil.CZipUtil")
        sTmp = oZip.UnZIPBufferToTempFile(CStr(s))
        Set oZip = Nothing
    Else
        sTmp = objFSO.GetSpecialFolder(2) ' TemporaryFolder
        If Right(sTmp, 1) <> "\" Then sTmp = sTmp & "\"
        sTmp = sTmp & sFileName
        Set txs = objFSO.CreateTextFile(sTmp, 1)
        txs.Write s
        txs.Close
        Set txs = Nothing
    End If
    
    Set objFSO = Nothing

    ReplaceFile sTmp, sPath & sFileName, True

End Function

'update the "cols" and "rows" attributes for all controls that use textarea
Function UpdateTextArea_Powerviews(iTextMlCols, iTextMlRows, iFreeCodeCols, iFreeCodeRows, iReadOnlyMemoCols, iReadOnlyMemoRows, iMemoCols, iMemoRows)
    Dim sSQL, iRS, arrViews, iCount, i, b, lViewId, sFormName, sViewXML
    Dim dom
    
    Set dom = CreateObject("Microsoft.XMLDOM")
    OpenDatabase
    
    sSQL = "SELECT COUNT(*) FROM NET_VIEW_FORMS"
    iRS = objRocket.DB_CreateRecordset(hDB, sSQL, 3, 0)
    iCount = GetData(iRS, 1)
    objRocket.DB_CloseRecordset CInt(iRS), 2
    ReDim arrViews(2, iCount - 1)
    
    i=0
    sSQL = "SELECT VIEW_ID, FORM_NAME, VIEW_XML FROM NET_VIEW_FORMS"
    iRS = objRocket.DB_CreateRecordset(hDB, sSQL, 3, 0)
    Do Until objRocket.DB_EOF(iRS)
        arrViews(0, i) = GetData(iRS, 1)
        arrViews(1, i) = GetData(iRS, 2)
        arrViews(2, i) = Trim(GetData(iRS, 3) & "")
        i = i + 1
		objRocket.DB_MoveNext iRS
    Loop
    objRocket.DB_CloseRecordset CInt(iRS), 2
    
    For i = 0 To iCount - 1
        sViewXML = arrViews(2, i)
        If Len(sViewXML) > 0 Then
			dom.loadXML sViewXML
			b = False
			SetColsRows dom, b, iTextMlCols, iTextMlRows, iFreeCodeCols, iFreeCodeRows, iReadOnlyMemoCols, iReadOnlyMemoRows, iMemoCols, iMemoRows
			If b Then
			    lViewId = CLng("0" & arrViews(0, i))
			    sFormName = Trim("" & arrViews(1, i))
			    sViewXML = dom.xml
			    sSQL = "SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE FORM_NAME = '" & sFormName & "' AND VIEW_ID = " & lViewId
			    iRS = objRocket.DB_CreateRecordset(hDB, sSQL, 3, 0)
			    objRocket.DB_CloseRecordset CInt(iRS), 1
			    objRocket.DB_Edit iRS
			    objRocket.DB_PutData iRS, "VIEW_XML", sViewXML
			    objRocket.DB_Update iRS
			    objRocket.DB_CloseRecordset CInt(iRS), 2
			End If
        End If
    Next
    
    Set dom = Nothing
    CloseDatabase
    
End Function

Function UpdateTextArea_Disk(iTextMlCols, iTextMlRows, iFreeCodeCols, iFreeCodeRows, iReadOnlyMemoCols, iReadOnlyMemoRows, iMemoCols, iMemoRows)
    Dim sAppDataPath
    Dim dom
    Dim objFso, txs
    Dim fl, fld
    Dim sErrMsg
    Dim b
    Dim sViewXML
    
    sAppDataPath = Application(APP_DATAPATH)
    
    Set objFso = CreateObject("Scripting.FileSystemObject")
    Set fld = objFso.GetFolder(sAppDataPath)
    Set dom = CreateObject("Microsoft.XMLDOM")
    
    For Each fl In fld.Files
        If Right(fl.Name, 3) = "xml" Then
            If fl.Attributes And 1 Then fl.Attributes = fl.Attributes - 1  'clear ReadOnly
            Set txs = fl.OpenAsTextStream(1)
            sViewXML = txs.ReadAll
            txs.Close
            Set txs = Nothing
            If Len(Trim(sViewXML)) > 0 Then
                dom.loadXML sViewXML
                b = 0
                SetColsRows dom, b, iTextMlCols, iTextMlRows, iFreeCodeCols, iFreeCodeRows, iReadOnlyMemoCols, iReadOnlyMemoRows, iMemoCols, iMemoRows
                If b <> 0 Then
                    Set txs = fl.OpenAsTextStream(2)
                    txs.Write dom.xml
                    txs.Close
                    Set txs = Nothing
                End If
            End If
        End If
    Next
    Set objFso = Nothing
    Set dom = Nothing
    Set fld = Nothing
    Set fl = Nothing
    Set txs = Nothing
End Function

Private Sub SetColsRows(dom, b, iTextMlCols, iTextMlRows, iFreeCodeCols, iFreeCodeRows, iReadOnlyMemoCols, iReadOnlyMemoRows, iMemoCols, iMemoRows)
    Dim objNodeList
    Dim xmlElem
   
    Set objNodeList = dom.selectNodes("//control[@type='textml']")
    If b = 0 Then b = objNodeList.length
    For Each xmlElem In objNodeList
        xmlElem.setAttribute "cols", iTextMlCols
        xmlElem.setAttribute "rows", iTextMlRows
    Next
    
    Set objNodeList = dom.selectNodes("//control[@type='freecode']")
    If b = 0 Then b = objNodeList.length
    For Each xmlElem In objNodeList
        xmlElem.setAttribute "cols", iFreeCodeCols
        xmlElem.setAttribute "rows", iFreeCodeRows
    Next
    
    Set objNodeList = dom.selectNodes("//control[@type='readonlymemo']")
    If b = 0 Then b = objNodeList.length
    For Each xmlElem In objNodeList
        xmlElem.setAttribute "cols", iReadOnlyMemoCols
        xmlElem.setAttribute "rows", iReadOnlyMemoRows
    Next
    
    Set objNodeList = dom.selectNodes("//control[@type='memo']")
    If b = 0 Then b = objNodeList.length
    For Each xmlElem In objNodeList
        xmlElem.setAttribute "cols", iMemoCols
        xmlElem.setAttribute "rows", iMemoRows
    Next
    
    Set objNodeList = Nothing
    Set xmlElem = Nothing
End Sub
</script>