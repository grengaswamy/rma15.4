'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 4.0
'
' NAME: Update IIS 6 Metabase Properties for both the entire Web Server
'       and the IIS 6 Virtual Directories
'
' AUTHOR: CSC , CSC
' DATE  : 6/27/2007
'
' COMMENT: Script to configure IIS 6 to correctly run the ASP Sortmaster application
'
'
'NOTES: This script will modify the following settings in the IIS 6 Metabase
'  1.  Increase the file size limit for uploads
'  2.  Increase the Response Buffer limit for rendering file attachment content
'  3.  Enable the ASP Web Service extension in IIS 6 (if already installed)
'  4.  Enable the ASP.Net 2.0 Web Service extension in IIS 6 (if already installed)
'  5.  Update the properties for the rmnet Virtual Directory
'  		a.  Uncheck the checkbox for Enable Session State
'		b.  Check the checkbox for Enable Parent Paths
'
' REFERENCES: http://www.microsoft.com/technet/prodtechnol/WindowsServer2003/Library/IIS/cde669f1-5714-4159-af95-f334251c8cbd.mspx
'==========================================================================
Dim strComputer, strOS, strVDirName
Dim intWebsite, intErrCode
Dim providerObj, vDirObj

'Set the computer name to the local computer
strComputer = "."

'Specify the name of the IIS Virtual Directory
strVDirName = "WIZARDSM"

'Get the Website ID for the selected Website
intWebsite = Property("CustomActionData")

'Create the WMI Object to determine the OS
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" _
    & strComputer & "\root\cimv2")
Set colOperatingSystems = objWMIService.ExecQuery _
    ("Select * from Win32_OperatingSystem")
For Each objOperatingSystem in colOperatingSystems
    'Wscript.Echo objOperatingSystem.Caption & _
    '"  " & objOperatingSystem.Version
    strOS = objOperatingSystem.Caption
Next

'Determine if the Operating System is Windows Server 2003
If (InStr(strOS, "Server 2003")) Then
	On Error Resume Next

	'Get the Provider Object
	Set providerObj=GetObject("winmgmts:/root/MicrosoftIISv2")
	
	'Enable the ASP Web Service Extension
	intErrCode = EnableWebSvcExtensions("ASP")
	
	If intErrCode <> 0 Then
		MsgBox("Unable to enable the ASP Web Service Extension.  Please check your IIS Settings.")
	End If
	
	'Enable the ASP.Net 2.0 Web Service Extension
	intErrCode = EnableWebSvcExtensions("ASP.NET v2.0.50727")
	
	If intErrCode <> 0 Then
		MsgBox("Unable to enable the ASP.Net 2.0 Web Service Extension.  Please check your IIS Settings.")
	End If
	
	'Enable the ASP.Net 2.0 Framework Web Server Application
	intErrCode = EnableWebSvcApps("ASP.Net v2.0.50727")
	
	If intErrCode <> 0 Then
		MsgBox("Unable to enable the ASP.Net Web Service Application.  Please check your IIS Settings.")
	End If
	
	'Call the function to update the Web Server Settings
	UpdateWebServerProps providerObj

	'Call the function to update the Virtual Directory properties
	UpdateVDirProps providerObj, intWebsite, strVDirName
		
	'Clean up
	Set providerObj = Nothing
End If

'Function to update the settings on the Virtual Directories
Function UpdateVDirProps(objWMIService, intWebsite, vDirName)
	'Get the Virtual Directory object from the Default Web Site
	'Append the name of the specific Virtual Directory to the Default Web Site path
	Set vdirObj=objWMIService.get("IIsWebVirtualDirSetting='W3SVC/" & intWebsite & "/ROOT/" & vDirName & "'")
	
	' Set some properties:
	'Enables Parent Paths so that relative paths in RMNet code 
	'that refers to directories functions properly
	vDirObj.AspEnableParentPaths = True
	'Disables ASP Session State so that RMNet functions under its 
	'own management of Session State
	vDirObj.AspAllowSessionState = False
	
	' Save the property changes in the metabase:
	vdirObj.Put_()
	
	'Clean up
	Set vDirObj = Nothing
End Function

'Function to update the Web Server Settings for IIS 6
Function UpdateWebServerProps(objWMIService)
	Dim objItem, colItems

	'Get the IIS Web Server Settings
	Set colItems = objWMIService.ExecQuery("SELECT * FROM IIsWebServerSetting", "WQL", _
                                          wbemFlagReturnImmediately + wbemFlagForwardOnly)
                                          
	'Loop through and set the necessary properties for IIS
   	For Each objItem In colItems
   		'Update the AspMaxRequestEntityAllowed property 
   		'which specifies the maximum number of bytes allowed in the 
   		'entity body of an ASP request.
   		objItem.AspMaxRequestEntityAllowed = "1073741824"
   		
   		'Update the AspBufferingLimit property 
   		'which sets the maximum size of the ASP buffer
   		'If response buffering is turned on, 
   		'this property controls the maximum number of bytes that an ASP page 
   		'can write to the response buffer before a flush occurs.
   		objItem.AspBufferingLimit = "1073741824"
   		
   		'Enable logging of ASP Error messages to the Windows NT Event Log
   		objItem.AspLogErrorRequests = True
   		objItem.AspErrorsToNTLog = False
   	
   		'Save the property changes to the IIS Metabase
   		objItem.Put_()
   	Next
   	
   	'Clean up
   	Set colItems = Nothing
End Function

'Function to Enable ASP Web Server Extensions
Function EnableWebSvcExtensions(strWebSvcExtension)
	Dim objWSH
	Dim intReturnErrCode
	
	'Create the Windows Scripting Host Object
	Set objWSH = WScript.CreateObject("WScript.Shell")
	
	'Enable ASP Extensions
	intReturnErrCode = objWSH.Run("cscript %SYSTEMROOT%\system32\iisext.vbs /enext " & strWebSvcExtension,7,True)
	
	'Clean up
	Set objWSH = Nothing
	
	EnableWebSvcExtensions = intReturnErrCode
End Function

'Function to Enable IIS 6 Applications
Function EnableWebSvcApps(strWebSvcApp)
	Dim objWSH
	Dim intReturnErrCode
	
	'Create the Windows Scripting Host Object
	Set objWSH = WScript.CreateObject("WScript.Shell")
	
	'Enable the IIS 6 Application
	intReturnErrCode = objWSH.Run("cscript %SYSTEMROOT%\system32\iisext.vbs /EnApp """ & strWebSvcApp & """",7,True)
	
	'Clean up
	Set objWSH = Nothing
	
	EnableWebSvcApps = intReturnErrCode
End Function
                                          
	
	
