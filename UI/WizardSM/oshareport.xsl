<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
<xsl:template match="/">
	<div>
		<script language="JavaScript" SRC="oshareport.js"></script>
		<input type="hidden" name="sysCmd" />
		<xsl:if test="form/toolbar">
			<div id="toolbardrift" name="toolbardrift" class="toolbardrift">
				<table border="0" class="toolbar"><tr>
				<xsl:apply-templates select="report/form/toolbar/button" />
				<td width="100%">&#160;</td>
				</tr></table>
			</div>
			<br />
		</xsl:if>
		<table border="0" width="100%">
		<xsl:if test="report/form/@topbuttons[.='1']">
			<xsl:if test="report/form/button">
				<tr><td>
				<table>
					<tr><td>
					<xsl:for-each select="report/form/button">
						<input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute>
							<xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute>
						</input>
					</xsl:for-each>
					</td></tr>
				</table>
				</td></tr>
			</xsl:if>
		</xsl:if>
		<tr><td>
		<xsl:if test="report/form/errors">
			<br />
			<div class="errtextheader">Following errors were reported:</div>
			<ul>
				<xsl:apply-templates select="report/form/errors/error" />	
			</ul>
		</xsl:if>
		<xsl:apply-templates select="report/form/control" />
		<table border="0" width="100%">
			<xsl:for-each select="report/form/group">
				<tr><td colspan="3" class="ctrlgroup"><xsl:value-of select="@title"/></td></tr>
				<xsl:apply-templates select="control" />
			</xsl:for-each>
		</table>
		<table>
			<!-- Buttons Area -->			
			<tr>
			<td>
			<xsl:for-each select="report/form/button">
				<input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute>
					<xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute>
				</input>
			</xsl:for-each>
			</td>
			</tr>			
		</table>
		<xsl:apply-templates select="//internal"/>
		<input type="hidden" name="formname"><xsl:attribute name="value"><xsl:for-each select="//form"><xsl:value-of select="@name"/></xsl:for-each></xsl:attribute></input>
		<input type="hidden" name="sys_required"><xsl:attribute name="value"><xsl:for-each select=".//control"><xsl:if test="@required[.='yes']"><xsl:value-of select="@name"/><xsl:if test="@type[.='code']">_cid</xsl:if><xsl:if test="@type[.='orgh']">_cid</xsl:if>|</xsl:if></xsl:for-each></xsl:attribute></input>
	</td>
	<td valign="top"><xsl:if test="report/form/@image"><img border="0" align="right"><xsl:attribute name="src">img/<xsl:value-of select="report/form/@image"/></xsl:attribute></img></xsl:if></td>
	</tr></table></div>
</xsl:template>

<!--<xsl:template match="button">
	<input type="button"><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute>
		<xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute>
	</input>
</xsl:template>-->

<xsl:template match="control">
<xsl:choose>
	<xsl:when test="@type[.='id']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of /></xsl:attribute></input></xsl:when>
	<xsl:otherwise>
	<tr>
		<xsl:choose>
			<xsl:when test="@type[.='button']">
				<td colspan="2">
				<input type="button" class="button"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@title"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input>
				</td>
			</xsl:when>
				<!-- New Radio Type -->
			<xsl:when test="@type[.='radio']">
				<td colspan="2">
				<input type="radio" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:if test="@checked"><xsl:attribute name="checked"></xsl:attribute></xsl:if><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute></input><xsl:value-of select="@title"/>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='href']">
				<td colspan="2">
				<a href="#"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute><xsl:attribute name="class"><xsl:value-of select="@style"/></xsl:attribute><xsl:value-of select="@title"/></a>
				</td>
			</xsl:when>
			<xsl:when test="@type[.='textlabel']">
				<td colspan="2">
					<xsl:value-of select="text()"/>
				</td>
			</xsl:when>
		<xsl:otherwise>
			<td>
				<xsl:choose>
					<xsl:when test="@required[.='yes']"><b><u><xsl:value-of select="@title"/></u></b></xsl:when>
					<xsl:otherwise><xsl:value-of select="@title"/></xsl:otherwise>
				</xsl:choose>
			</td>
			<td>
			<xsl:choose>
				<xsl:when test="@type[.='text']"><input type="text" size="30" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input>
				<xsl:if test="@href"><a href="#"><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute><xsl:value-of select="@href"/></a></xsl:if>
				<xsl:if test="@button"><input type="button" class="button"><xsl:attribute name="value"><xsl:value-of select="@button"/></xsl:attribute><xsl:attribute name="onClick">formHandler('<xsl:value-of select="@linkto"/>','<xsl:value-of select="@param"/>','<xsl:value-of select="@enablefornew"/>','<xsl:value-of select="@type"/>')</xsl:attribute></input></xsl:if>
				</xsl:when>
				<xsl:when test="@type[.='hidden']"><input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='textml']"><xsl:element name="textarea" xml:space="preserve"><xsl:attribute name="cols"><xsl:value-of select="@cols"/></xsl:attribute><xsl:attribute name="rows"><xsl:value-of select="@rows"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute><xsl:value-of select="text()"/></xsl:element>
				<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btnMemo</xsl:attribute><xsl:attribute name="onClick">EditMemo('<xsl:value-of select="@name"/>')</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='code']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>')</xsl:attribute></input>
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='lookup']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupClick('<xsl:value-of select="@data"/>','<xsl:value-of select="@name"/>')</xsl:attribute></input>
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@lookupid"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='freecode']"><xsl:element name="textarea" xml:space="preserve"><xsl:attribute name="cols"><xsl:value-of select="@cols"/></xsl:attribute><xsl:attribute name="rows"><xsl:value-of select="@rows"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute><xsl:value-of select="text()"/></xsl:element><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>')</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='date']"><input type="text" size="30" onblur="dateLostFocus(this.name);" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectDate('<xsl:value-of select="@name"/>')</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='time']"><input type="text" size="30" onblur="timeLostFocus(this.name);" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='orgh']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('orgh','<xsl:value-of select="@name"/>')</xsl:attribute></input>
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='label']"><input type="text" size="30"  onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='memo']"><xsl:element name="textarea" xml:space="preserve"><xsl:attribute name="cols"><xsl:value-of select="@cols"/></xsl:attribute><xsl:attribute name="rows"><xsl:value-of select="@rows"/></xsl:attribute><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="onchange">setDataChanged(true);</xsl:attribute><xsl:value-of select="text()"/></xsl:element>
				<input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btnMemo</xsl:attribute><xsl:attribute name="onClick">EditMemo('<xsl:value-of select="@name"/>')</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='checkbox']"><input type="checkbox" value="1" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
					<xsl:if test="text()[.='1']"><xsl:attribute name="checked"></xsl:attribute></xsl:if>
					<xsl:if test="@checked"><xsl:attribute name="checked"></xsl:attribute></xsl:if></input>
				</xsl:when>
				<xsl:when test="@type[.='zip']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="zipLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='phone']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="phoneLostFocus(this);" onfocus="phoneGotFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='ssn']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="ssnLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='numeric']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="numLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='currency']"><input type="text" size="30" onchange="setDataChanged(true);" onblur="currencyLostFocus(this);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='entitylookup']"><input type="text" size="30" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@fieldmark"/>',1)</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='employeelookup']"><input type="text" size="30" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('employees',4,'<xsl:value-of select="@fieldmark"/>',4)</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='eidlookup']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@name"/>',2)</xsl:attribute></input>
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='vehiclelookup']"><input type="text" size="30" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('vehicle',5,'<xsl:value-of select="@fieldmark"/>',10)</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='claimnumberlookup']"><input type="text" size="30" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('claim',1,'<xsl:value-of select="@name"/>',6)</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='eventnumberlookup']"><input type="text" size="30" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('event',2,'<xsl:value-of select="@name"/>',7)</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='eventlookup']"><input type="text" size="30" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('event',2,'<xsl:value-of select="@fieldmark"/>',11)</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='vehiclenumberlookup']"><input type="text" size="30" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('vehicle',5,'<xsl:value-of select="@name"/>',8)</xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='policynumberlookup']"><input type="text" size="30" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('policy',6,'<xsl:value-of select="@name"/>',9)</xsl:attribute></input>
				</xsl:when>
				
				<xsl:when test="@type[.='policylookup']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupClaimPolicy('<xsl:value-of select="@name"/>')</xsl:attribute></input>
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
				</xsl:when>
				<xsl:when test="@type[.='codelist']"><select size="3"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
					<xsl:apply-templates select="option">
						<xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
	         	</xsl:apply-templates>
					</select> <input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectCode('<xsl:value-of select="@codetable"/>','<xsl:value-of select="@name"/>')</xsl:attribute></input>
					<input type="button" class="button" value=" - "><xsl:attribute name="name"><xsl:value-of select="@name"/>btndel</xsl:attribute><xsl:attribute name="onClick">deleteSelCode('<xsl:value-of select="@name"/>')</xsl:attribute></input>
					<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
	         		<xsl:attribute name="value"><xsl:for-each select="option"><xsl:value-of select="@value"/><xsl:if test="context()[not(end())]">,</xsl:if></xsl:for-each></xsl:attribute>
	         	</input>
				</xsl:when>
				<xsl:when test="@type[.='entitylist']"><select size="3"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
					<xsl:apply-templates select="option">
						<xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
	         	</xsl:apply-templates>
					</select> <input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">lookupData('<xsl:value-of select="@tableid"/>',4,'<xsl:value-of select="@name"/>',3)</xsl:attribute></input>
					<input type="button" class="button" value=" - "><xsl:attribute name="name"><xsl:value-of select="@name"/>btndel</xsl:attribute><xsl:attribute name="onClick">deleteSelCode('<xsl:value-of select="@name"/>')</xsl:attribute></input>
					<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
	         		<xsl:attribute name="value"><xsl:for-each select="option"><xsl:value-of select="@value"/><xsl:if test="context()[not(end())]">,</xsl:if></xsl:for-each></xsl:attribute>
	         	</input>
				</xsl:when>
				<xsl:when test="@type[.='policysearch']">
					<input type="text" size="30" onblur="eidLostFocus(this.name);" onchange="setDataChanged(true);">
						<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
					</input>
					<input type="button" class="button" value="...">
						<xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute>
						<xsl:attribute name="onClick">searchPolicies(6,'<xsl:value-of select="@name"/>')</xsl:attribute></input>
					<input type="hidden">
						<xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>
					</input>
				</xsl:when>
				<xsl:when test="@type[.='combobox']"><select size="1" onchange="setDataChanged(true);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="codeid"><xsl:value-of select="@codeid"/></xsl:attribute>
					<xsl:apply-templates select="option">
						<xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
	         	</xsl:apply-templates>
					</select>
				</xsl:when>
				<xsl:when test="@type[.='accountlist']"><input type="text" size="30" onblur="codeLostFocus(this.name);" onchange="setDataChanged(true);" onkeypress="return eatKeystrokes(event);"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input><input type="button" class="button" value="..."><xsl:attribute name="name"><xsl:value-of select="@name"/>btn</xsl:attribute><xsl:attribute name="onClick">selectAccountList('<xsl:value-of select="@name"/>')</xsl:attribute></input>
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
				</xsl:when>
				<xsl:otherwise>Unknown Element Type: <xsl:value-of select="@type"/>
				</xsl:otherwise>
			</xsl:choose>
			</td>
		</xsl:otherwise></xsl:choose>
	</tr>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="internal">
	<xsl:choose>
		<xsl:when test="@type[.='hidden']">
			<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="form">
	<xsl:value-of select="@title"/>
</xsl:template>

<xsl:template match="error">
<li class="errtext">
	<xsl:if test="@number[.='10000']"><img src="img/locked.gif" width="12" height="15" title="Security Message" border="0" />&#160;&#160;</xsl:if>
	<xsl:value-of select="text()"/>
</li>
</xsl:template>
	
</xsl:stylesheet>