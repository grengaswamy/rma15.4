function selCode(sCodeText,lCodeId)
{
	if(lCodeId!=0)
	{
		// Put the code on the calling form+close this one
		//alert('getcode.js selCode()');
		document.frmdata.bUnload.value = 'true';		
		//window.opener.document.codeSelected(sCodeText,lCodeId);
		window.opener.codeSelected(sCodeText,lCodeId);
	}
	return false;
}

function handleUnload()
{
	//alert('getcode.js handleUnload()');
	if(window.opener!=null)
	{
		//TR 4/11/02 do not unload if moving from codepage to codepage
		if (document.frmdata.bUnload.value == true)
		{
			window.opener.document.onCodeClose();
		}
	}
	return true;
}

function getPage(lPageNumber)
{
	document.frmdata.PageNumber.value=lPageNumber;
	document.frmdata.submit();
}