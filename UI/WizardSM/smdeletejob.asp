<!-- #include file ="session.inc" -->
<%Option Explicit
' Author: Denis Basaric, 03/28/2000
' Move documents passed in doc variable
' Id's are comma delimited
%>
<%Public objSession
initSession objSession
%> 
<script language="vbscript" runat="server">
Sub CancelRequest()
	Response.Redirect "smrepqueue.asp"
	Response.End
End Sub
</script>

<%
Dim sJobList
Dim vArrJob, f, bOk
Dim sMoveTo
dim objNewDoc
dim r, henv, db, rs
dim sCompleteFilename, sFilename, iPos, sJobName, sJobDesc
dim fso, objCSCFileStorage
Dim iDocPathType

sJobList=Request("jobs")
If sJobList="" Then
	CancelRequest
End If

iDocPathType=objSessionStr(SESSION_DOCPATHTYPE)
If LCase(Request.ServerVariables("REQUEST_METHOD"))="post" Then
	if iDocPathType = CSCStoreDatabase Then
		set objCSCFileStorage = CreateObject("CSCFileStorage.CCSCFileStorage")
		objCSCFileStorage.Init objSessionStr(SESSION_DSN), CSCStoreDatabase, objSessionStr(SESSION_DOCPATH), false
	end if

	set r= CreateObject("DTGRocket")
	henv = r.DB_InitEnvironment()
	db = r.DB_OpenDatabase(henv, Application("SM_DSN"), 0)

	'delete the files from disk or database
	Set fso = CreateObject("Scripting.FileSystemObject")
	rs = r.DB_CreateRecordset(db, "SELECT * FROM SM_JOBS WHERE COMPLETE <> 0 AND JOB_ID IN (" & sJobList & ") AND USER_ID = " & objSessionStr("UserID"), 3, 0)
	while Not r.DB_EOF(rs)
		if iDocPathType = CSCStoreFileSystem  Then
			r.DB_GetData rs, "OUTPUT_PATH", sCompleteFilename
			if sCompleteFilename & "" <> "none" then
				on error resume next
				fso.DeleteFile sCompleteFilename, True
				on error goto 0
			end if
		else
			'output_path is the connection string to document database, so need to grab file name off of the URL
			r.DB_GetData rs, "OUTPUT_PATH_URL", sCompleteFilename
			If Len(Trim(sCompleteFilename & "")) > 0 then
				sCompleteFileName = Mid(sCompleteFileName, Instr(1, sCompleteFileName, "file=") + 5)
				objCSCFileStorage.DeleteFile sCompleteFileName
			end if
		end if
			
		r.DB_MoveNext rs
	wend
	r.DB_CloseRecordset CInt(rs), 2
	set fso = Nothing
				
	'delete reference to deleted files from SM_JOBS
	r.DB_SQLExecute db, "DELETE FROM SM_JOBS WHERE JOB_ID IN (" & sJobList & ") AND USER_ID = " & objSessionStr("UserID")
		
	r.DB_CloseDatabase CInt(db)
	r.DB_FreeEnvironment CLng(henv)
		
	set r = Nothing
	set objCSCFileStorage = nothing
	Response.Redirect "smrepqueue.asp"
	Response.End
End If

Response.ExpiresAbsolute=DateAdd("h",-24,Now)
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>SORTMASTER Report Job Delete</title>
	<!-- [ Rajeev 11/12/2002 -- CSS Consolidation] link rel="stylesheet" href="forms.css" type="text/css"/-->
	<link rel="stylesheet" href="RMNet.css" type="text/css"/>
</head>
<body class="10pt">
<form name="frmData" method="post" action="smdeletejob.asp">
<input type="hidden" name="jobs" value="<%=sJobList%>" />
<table class="singleborder" align="center">
	<tr><td class="ctrlgroup">Confirm Report Job Delete</td></tr>
	<tr>
		<td nowrap="" class="datatd"><b>Are you sure you want to delete selected items?<br /><br />Data will be permanently deleted.</b><br>&nbsp;</td>
	</tr>
	<tr>
		<td align="center" nowrap="">
			<input type="submit" class="button" name="btnView" value="  Delete  ">
			<input type="button" class="button" name="btnView" value=" Cancel " onClick="document.location='smrepqueue.asp'">
		</td>
	</tr>
</table>
</form>

</body>
</html>