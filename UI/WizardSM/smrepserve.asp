<!-- #include file ="session.inc" -->
<!-- #include file ="dbutil.inc" -->
<!-- #include file ="CustomCriteria.inc" -->

<script language="VBScript" runat="server">
Response.Buffer = False

Dim objSendFile, objCSCFileStorage
Dim sFullName, sName, sExt, l
Dim sContentType
Dim iDocPathType, sDocPath, lJobID, sOutputPathURL, i

'Try to get required information
iDocPathType = Request("DPT") & ""
sName=Request("file")
lJobID = Request("JobID") & ""
If lJobID = "" Then
	sDocPath = ""
Else
	GetFileDocPath lJobID, sDocPath, sOutputPathURL
End If

If sDocPath = "" Then
	Response.Write "Can not get DocPath. Check record for JOB_ID=" & lJobID & " in SM_JOBS table.<br/>" & vbcrlf
	Response.End
End If 

'Get the DocPathType if it's not in the url
If iDocPathType = "" Then
	If sOutputPathURL <> "" Then
		i = InStr(1, sOutputPathURL, "DPT=")
		If i > 0 And Len(sOutputPathURL) > i + 5 Then
			iDocPathType = Mid(sOutputPathURL, i + 4, 1)
		End If
	End If
End If

'Get the file name if it's not in the url
If sName = "" Then
	If sOutputPathURL <> "" Then
		i = InStr(1, sOutputPathURL, "file=")
		If i > 0 And Len(sOutputPathURL) > i + 6 Then
			sName = Mid(sOutputPathURL, i + 5)
			i = InStr(1, sName, "&")
			If i > 0 Then
				sName = Left(sName, i-1)
			End If
		End If
	End If
End If

'Response.Write sName & ": " & iDocPathType & ": " & sDocPath
'Response.End 

If sName="" Then
	Response.Redirect "smfilemissing.asp"
	Response.End
End If
if sName = "none" then
	Response.Write "<b>Report Generated No Output</b>"
	Response.End
end if

If Left(sName,1)="\" Then s=Mid(sName,2)
sFullName=""

' Create object
Set objSendFile=CreateObject("DTGSendFile.1")

if iDocPathType = CSCStoreFileSystem  Then
	sFullName = sDocPath
	If Not USE_DCOM Then
		Dim objFSO
		Set objFSO=CreateObject("Scripting.FileSystemObject")
		If Not objFSO.FileExists(sFullName) Then
			Response.Write "File not found (" & sFullName & ")"
			Response.End
		End If
		Set objFSO=Nothing
	End If
else
	Set objCSCFileStorage = CreateObject("CSCFileStorage.CCSCFileStorage")
	
	'If DocPath contain file name remove it
	i = Instr(1, sDocPath, "\" & sName)
	If i > 0 Then
		sDocPath = Mid(sDocPath, 1, i - 1)
	End If
	
	objCSCFileStorage.Init "", CSCStoreDatabase, sDocPath, false
	sFullName = objCSCFileStorage.RetrieveFile(sName)
	set objCSCFileStorage = nothing
	if Len(Trim(sFullName)) = 0 then
		Response.Write "File not found (" & sName & ")"
		Response.End
	end if
end if

If Not USE_DCOM Then
	' Default Content-Type
	sContentType="application/x-octetstream"
	Response.AddHeader "Content-Disposition","inline; filename=" & sName
	' Get File extension if any
	l=InstrRev(sName,".")
	If l>0 Then
		sExt=LCase(Right(sName,Len(sName)-l))
		sContentType=objMimeTypes.LookupValue(sExt)
		If sContentType="" Then
			sContentType="application/x-octetstream"
		End If
	End If
	
	objSendFile.SendFileWin sFullName,sContentType
	
	'tkr 12/2002 delete file from temp folder
	if iDocPathType = CSCStoreDatabase Then
		On Error Resume Next
		objSendFile.KillFile sFullName
		On Error Goto 0
	end if
Else
	Dim size, vret
	'	'Have the remote objSendFile give open up the file from the DCOM 
	'box file system and hand it back to us in chunks so we can 
	' pass it out from the web-server box.
	On Error Resume Next
	size = objSendFile.OpenFile(sFullName)
	If Err.number <> 0 Then
		Response.Write Err.Description & " File " & sFullName & " was not found."
		Response.End
	End If
	On Error Goto 0
	
	' Default Content-Type
		l=InstrRev(sName,".")
	If l>0 Then 	' Get File extension if any
		sExt=LCase(Right(sName,Len(sName)-l))
		sContentType=objMimeTypes.LookupValue(sExt)
		If sContentType="" Then
			sContentType="application/x-octetstream"
		End If
	End If
	
Response.AddHeader "Content-Disposition","inline; filename=" & sName
Response.ContentType = sContentType	

	Do
   	vret = objSendFile.ReadFile(8192, bytesRead)
		If bytesRead <> 0 Then
			Response.BinaryWrite vret
		End If
	Loop While bytesRead > 0
	objSendFile.CloseFile
	
	If iDocPathType <> CSCStoreFileSystem Then'Clean Up DCOM Server Scratch File
		On Error Resume Next
		objSendFile.KillFile sFullName
		On Error Goto 0
	End If
End if
Set objSendFile=Nothing
Response.End

</script>