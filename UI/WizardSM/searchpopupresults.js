function supportCookies()
{
	document.cookie="cookietest=1;";
	if(getCookie("cookietest")=="1")
	{
		deleteCookie("cookietest");
		return true;
	}
	return false;	
}

function getCookie(sCookie)
{
  // cookies are separated by semicolons
  var aCookie = document.cookie.split(";");
  for (var i=0; i < aCookie.length; i++)
  {
    // a name/value pair (a crumb) is separated by an equal sign
    var aCrumb = aCookie[i].split("=");
	 var sName=replace(aCrumb[0]," ","");
    if (sCookie == sName) 
      return unescape(aCrumb[1]);
  }
  // a cookie with the requested name does not exist
  return null;
}

function setCookie(sName, sValue)
{
	document.cookie = sName + "=" + escape(sValue)+";";
	return true;
}

function replace(sSource, sSearchFor, sReplaceWith)
{
	var arr = new Array();
	arr=sSource.split(sSearchFor);
	return arr.join(sReplaceWith);
}

function deleteCookie(sCookie)
{
	var dExp=new Date();
	dExp.setFullYear(dExp.getFullYear()-1);
	document.cookie=sCookie+"=;expires=Mon, 01 Dec 1980 23:00:00 UTC;";
	return true;
}

function goEvent(lEventId)
{
	if(supportCookies())
	{
		setCookie("linkto","event.asp");
		setCookie("id",lEventId);
		setCookie("cmd","0");
		setTimeout('window.location.href = "event.asp"', 100);
	}
	else
	{
		window.location.href = "event.asp?id="+lEventId+"&cmd=0";
	}
	return true;
}

function goADM(sADMTable, lRecordId)
{
	if(supportCookies())
	{
		setCookie("linkto","adm.asp");
		setCookie("id",lRecordId);
		setCookie("cmd","0");
		setTimeout('window.location.href = "adm.asp?formname=' + sADMTable + '"', 100);
	}
	else
	{
		window.location.href = "adm.asp?formname=" + sADMTable + "&id="+lRecordId+"&cmd=0";
	}
	return true;
}

function selRecord(lRecordID)
{
	if(lRecordID!=0)
	{
		var searchcat;
		var admtable;
		
		searchcat=document.frmInfo.searchcat.value;
		admtable=document.frmInfo.admtable.value;
		
		switch(searchcat)
		{
			case "claim":
			{
				if(haveProperty(window.opener,"lookupCallback"))
				{
					if(window.opener.lookupCallback!=null)
					{
						eval("window.opener."+window.opener.lookupCallback+"("+lRecordID+")");
						return true;
					}
				}
				break;
			}
			case "event":
			{
				if(haveProperty(window.opener,"lookupCallback"))
				{
					if(window.opener.lookupCallback!=null)
					{
						eval("window.opener."+window.opener.lookupCallback+"("+lRecordID+")");
						return true;
					}
				}
				goEvent(lRecordID);
				break;
			}			
			case "adm":
			{
				goADM(admtable, lRecordID);
				break;
			}
			case "entity":
			{
				if(haveProperty(window.opener,"lookupCallback"))
				{
					if(window.opener.lookupCallback!=null)
					{
						eval("window.opener."+window.opener.lookupCallback+"("+lRecordID+")");
						return true;
					}
				}
				window.opener.document.entitySelected(lRecordID);
				break;
			}
			case "employee":
			{
				if(haveProperty(window.opener,"lookupCallback"))
				{
					if(window.opener.lookupCallback!=null)
					{
						eval("window.opener."+window.opener.lookupCallback+"("+lRecordID+")");
						return true;
					}
				}
				window.opener.document.employeeSelected(lRecordID);
				break;
			}
			case "policy":
			{
				window.opener.document.policySelected(lRecordID);
				break;
			}
			case "vehicle":
			{
				if(haveProperty(window.opener,"lookupCallback"))
				{
					if(window.opener.lookupCallback!=null)
					{
						eval("window.opener."+window.opener.lookupCallback+"("+lRecordID+")");
						return true;
					}
				}
				break;
			}
			case "patient":
			{
				if(haveProperty(window.opener,"lookupCallback"))
				{
					if(window.opener.lookupCallback!=null)
					{
						eval("window.opener."+window.opener.lookupCallback+"("+lRecordID+",'patient')");
						return true;
					}
				}
				window.opener.document.entitySelected(lRecordID,'patient');
				break;
			}
			default:
				if(haveProperty(window.opener,"lookupCallback"))
				{
					if(window.opener.lookupCallback!=null)
					{
						eval("window.opener."+window.opener.lookupCallback+"("+lRecordID+")");
						return true;
					}
				}
				alert("Default case." + document.frmInfo.searchcat.value);
				break;
		}
	}
	return true;
}

function handleUnload()
{
	if(window.opener!=null)
		window.opener.onCodeClose();
	return true;
}

function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p==sPropName)
			return true;
	}
	return false;
}