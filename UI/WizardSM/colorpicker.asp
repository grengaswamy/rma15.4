<!-- #include file ="session.inc" -->
<%Public objSession, objSecurityTable
Dim sStartPage
initSession objSession
'''Response.Write Request.QueryString("GLOBALINDEX")%>

<html>
	<head>
		<link rel="stylesheet" href="RMNet.css" type="text/css"/>
		<title>SMD - [Color Picker]</title>
		<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
		
		<style type=text/css>
			.rdbox {background-color: #ffbbbb}
			.grbox {background-color: #bbeebb}
			.blbox {background-color: #aaddee}
		</style>

		<script language=javascript>
		//<!--

			//initialize colors
			bgcolor_r = 255; 
			bgcolor_g = 255; 
			bgcolor_b = 255; 

			//preload slide images
			arrows = new Image(28,22);        arrows.src = "img/arrows.gif";
			arrows_dimmed = new Image(28,22); arrows_dimmed.src = "img/arrows_dimmed.gif";
			arrows_noup = new Image(28,22);   arrows_noup.src = "img/arrows_noup.gif";
			arrows_nodn = new Image(28,22);   arrows_nodn.src = "img/arrows_nodn.gif";

			function toDec(p_HexCode) 
			{ 
				return parseInt(p_HexCode,16); 
			}

			function toHex(decimal_number)
			//feed it a decimal number and it spits back a 2 digit hex number
			{
				var prefix = (decimal_number < 16) ? '0' : '';
				hex_number = decimal_number.toString(16);
				hex_number = hex_number.toUpperCase();
				return prefix + hex_number;
			}


			function toDecimalr(hexcode) { hexr = hexcode.substring(0,2); return parseInt(hexr,16); }
			function toDecimalg(hexcode) { hexg = hexcode.substring(2,4); return parseInt(hexg,16); }
			function toDecimalb(hexcode) { hexb = hexcode.substring(6,4); return parseInt(hexb,16); }


			function hitArrow(img_name, increment, flag)
			{
				if     (img_name == "img_bgcolor_r") { current_img_value = bgcolor_r; current_textbox = "bgcolor_r_value"}
				else if(img_name == "img_bgcolor_g") { current_img_value = bgcolor_g; current_textbox = "bgcolor_g_value"}
				else if(img_name == "img_bgcolor_b") { current_img_value = bgcolor_b; current_textbox = "bgcolor_b_value"}

				if(increment == "upa")
				{
				   current_img_value = current_img_value + 1;
				   if(current_img_value == 255)
				   {
				      window.document.images[img_name].src = arrows_noup.src;
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				   }
				   else if(current_img_value == 256)
				   {
				      current_img_value = 255;
				   }
				   else if((current_img_value < 255)&&(current_img_value != 1))
				   {
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				   }
				   else if(current_img_value == 1)
				   {
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				      window.document.images[img_name].src = arrows.src;
				   }
				}
				else if(increment == "upb")
				{
				   current_img_value = current_img_value + 8;
				   if(current_img_value >= 255)
				   {
				      current_img_value = 255;
				      window.document.images[img_name].src = arrows_noup.src;
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				   }
				   else if((current_img_value < 255)&&(current_img_value != 8))
				   {
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				   }
				   else if(current_img_value == 8)
				   {
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				      window.document.images[img_name].src = arrows.src;
				   }
				}
				else if(increment == "dna")
				{
				   current_img_value = current_img_value - 1;
				   if(current_img_value == 0)
				   {
				      window.document.images[img_name].src = arrows_nodn.src;
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				   }
				   else if(current_img_value == -1)
				   {
				      current_img_value = 0;
				   }
				   else if((current_img_value > 0)&&(current_img_value != 254))
				   {
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				   }
				   else if(current_img_value == 254)
				   {
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				      window.document.images[img_name].src = arrows.src;
				   }
				}
				else if(increment == "dnb")
				{
				   current_img_value = current_img_value - 8;
				   if(current_img_value <= 0)
				   {
				      current_img_value = 0;
				      window.document.images[img_name].src = arrows_nodn.src;
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				   }
				   else if((current_img_value > 0)&&(current_img_value != 247))
				   {
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				   }
				   else if(current_img_value == 247)
				   {
				      window.document.cpform[current_textbox].value = toHex(current_img_value);
				      window.document.images[img_name].src = arrows.src;
				   }
				}

				if     (img_name == "img_bgcolor_r") { bgcolor_r = current_img_value ;}
				else if(img_name == "img_bgcolor_g") { bgcolor_g = current_img_value ;}
				else if(img_name == "img_bgcolor_b") { bgcolor_b = current_img_value ;}
				      
				window.document.cpform.all.abc.bgColor= '#'+toHex(bgcolor_r)+toHex(bgcolor_g)+toHex(bgcolor_b);

				window.opener.window.document.frmData.hdRColor.value = toHex(bgcolor_r); 
				window.opener.window.document.frmData.hdGColor.value = toHex(bgcolor_g); 
				window.opener.window.document.frmData.hdBColor.value = toHex(bgcolor_b); 
			}

			function fillHexes()
			{
			    
					window.document.cpform.bgcolor_r_value.value = toHex(bgcolor_r);
					window.document.cpform.bgcolor_g_value.value = toHex(bgcolor_g);
					window.document.cpform.bgcolor_b_value.value = toHex(bgcolor_b);
					window.document.cpform.all.abc.bgColor= '#'+toHex(bgcolor_r)+toHex(bgcolor_g)+toHex(bgcolor_b);
			}

			function SendColor(sentcolor)
			{
			    //alert(sentcolor + '   sentcolor');
			    bgcolor_r = toDecimalr(sentcolor);
			    bgcolor_g = toDecimalg(sentcolor);
			    bgcolor_b = toDecimalb(sentcolor);
			    window.document.cpform.bgcolor_r_value.value = toHex(bgcolor_r);
			    window.document.cpform.bgcolor_g_value.value = toHex(bgcolor_g);
			    window.document.cpform.bgcolor_b_value.value = toHex(bgcolor_b);
			    
			    window.document.cpform.all.abc.bgColor= '#'+toHex(bgcolor_r)+toHex(bgcolor_g)+toHex(bgcolor_b);
			    if(bgcolor_r == 0)        { window.document.images["img_bgcolor_r"].src = arrows_nodn.src; }
			    else if(bgcolor_r == 255) { window.document.images["img_bgcolor_r"].src = arrows_noup.src; }
			    else                      { window.document.images["img_bgcolor_r"].src = arrows.src; }
			    if(bgcolor_g == 0)        { window.document.images["img_bgcolor_g"].src = arrows_nodn.src; }
			    else if(bgcolor_g == 255) { window.document.images["img_bgcolor_g"].src = arrows_noup.src; }
			    else                      { window.document.images["img_bgcolor_g"].src = arrows.src; }
			    if(bgcolor_b == 0)        { window.document.images["img_bgcolor_b"].src = arrows_nodn.src; }
			    else if(bgcolor_b == 255) { window.document.images["img_bgcolor_b"].src = arrows_noup.src; }
			    else                      { window.document.images["img_bgcolor_b"].src = arrows.src; }


				window.opener.window.document.frmData.hdRColor.value = toHex(bgcolor_r); 
				window.opener.window.document.frmData.hdGColor.value = toHex(bgcolor_g); 
				window.opener.window.document.frmData.hdBColor.value = toHex(bgcolor_b); 
				
			}
			
			function on_OK()
			{
				var sColor;
			    
			    //alert(window.opener.window.document.frmData.hdRColor.value);
			    if (window.opener.window.document.frmData.hdRColor.value != '' && window.document.cpform.hdCheckWindow.value == 'rpttitlefont')
				{
					bgcolor_r = window.opener.window.document.frmData.hdRColor.value;
					bgcolor_g = window.opener.window.document.frmData.hdGColor.value;
					bgcolor_b = window.opener.window.document.frmData.hdBColor.value;
					sColor = toDec(bgcolor_b + bgcolor_g + bgcolor_r);
				}
			   /* else if (window.opener.window.document.frmData.hdRColor.value != '' && window.document.cpform.hdCheckWindow.value == 'rptoptions')
				{
					bgcolor_r = window.opener.window.document.frmData.hdRColor.value;
					bgcolor_g = window.opener.window.document.frmData.hdGColor.value;
					bgcolor_b = window.opener.window.document.frmData.hdBColor.value;
					sColor = toDec(bgcolor_b + bgcolor_g + bgcolor_r);
				}*/
				else
				{
					if (window.document.cpform.hdCheckWindow.value == 'rptoptions')// && window.opener.window.document.frmData.hdRColor.value == '')//(window.document.cpform.hdColorCode.value != '' || window.document.cpform.hdColorCode.value != '-1')
						sColor = toDec(window.document.cpform.bgcolor_b_value.value + window.document.cpform.bgcolor_g_value.value + window.document.cpform.bgcolor_r_value.value);
					else
						sColor = toDec(toHex(bgcolor_r)+toHex(bgcolor_g)+toHex(bgcolor_b));
				}
				
				
				sampleColor = toDec(window.document.cpform.bgcolor_r_value.value + window.document.cpform.bgcolor_g_value.value + window.document.cpform.bgcolor_b_value.value);
				//alert(sampleColor + 'on_OK()');
				/*var arrTemp = window.document.cpform.hdColorCode.value.split("~~~~~");
				var sProperties = '';
				for(j=0;j<arrTemp.length;j++)
				{
					if (sProperties == '')
						sProperties =  arrTemp[j];
					else
					{
						if(j == 12)
							sProperties =  sProperties + '~~~~~' + sColor;
						else
							sProperties =  sProperties + '~~~~~' + arrTemp[j];
					}
				}*/

				window.opener.changeColor(sColor,sampleColor);
				window.opener.window.document.frmData.hdColor.value = sColor; 
				//window.opener.window.document.frmData.hdPOptions.value = sProperties;
				
				 
				
				//alert(toDecimalr(window.opener.window.document.frmData.hdRColor.value));
				//alert(window.opener.window.document.frmData.hdColor.value + '  on_OK()')
				//alert(window.opener.window.document.frmData.hdPOptions.value + 'GG');
				/*if (window.opener.window.document.frmData.hdRColor.value == '')
				{
				window.opener.window.document.frmData.hdRColor.value = toHex(bgcolor_r); 
				window.opener.window.document.frmData.hdGColor.value = toHex(bgcolor_g); 
				window.opener.window.document.frmData.hdBColor.value = toHex(bgcolor_b); 
				}*/
				//else
				//{
				//window.opener.window.document.frmData.hdRColor.value = toHex(bgcolor_r); 
				//window.opener.window.document.frmData.hdGColor.value = toHex(bgcolor_g); 
				//window.opener.window.document.frmData.hdBColor.value = toHex(bgcolor_b); 
				//}
				
				//alert('send:'+window.opener.window.document.frmData.hdRColor.value)
				//alert('send:'+window.opener.window.document.frmData.hdGColor.value)
				//alert('send:'+window.opener.window.document.frmData.hdBColor.value)
				
				window.close(); 
			}
			
			function window_onLoad()
			{
				//return false;
				//alert('Here'+toHex(parseInt(window.opener.window.document.frmData.hdColor.value)))
				//return false;

				

				//alert(window.opener.window.document.frmData.hdColor.value + '  window_onLoad()')
				
			    if (window.opener.window.document.frmData.hdRColor.value != '' && window.document.cpform.hdCheckWindow.value == 'rpttitlefont')
			    {
					window.document.cpform.bgcolor_r_value.value = window.opener.window.document.frmData.hdRColor.value;
					window.document.cpform.bgcolor_g_value.value = window.opener.window.document.frmData.hdGColor.value;
					window.document.cpform.bgcolor_b_value.value = window.opener.window.document.frmData.hdBColor.value;
					window.document.cpform.all.abc.bgColor= '#'+window.opener.window.document.frmData.hdRColor.value+window.opener.window.document.frmData.hdGColor.value+window.opener.window.document.frmData.hdBColor.value;
				}
				else
			    {
				var iColorCode = '';
				var arrColor;
					if (window.document.cpform.hdCheckWindow.value == 'rptoptions')// && window.document.cpform.hdColorCode.value != '' && window.document.cpform.hdColorCode.value != '-1')
					{
						//alert(window.document.cpform.hdColorCode.value);
						arrColor = window.document.cpform.hdColorCode.value.split("~~~~~");
						iColorCode = arrColor[12];
					//	alert(window.document.cpform.hdColorCode.value + 'If')
					}
					//else if (window.document.cpform.hdCheckWindow.value == 'rptoptions' && (window.document.cpform.hdColorCode.value == '' || window.document.cpform.hdColorCode.value == '-1'))
					//{
						//arrColor = window.document.cpform.hdColorCode.value.split("~~~~~");
					//	iColorCode = '';
					//}
					else
					{
						//alert('Else');
						iColorCode = window.opener.window.document.frmData.hdColor.value;
					}
					//alert(iColorCode + 'iColorCode')
			    if (iColorCode == '0' || iColorCode == '')
				{
					window.document.cpform.bgcolor_b_value.value = '00';
					window.document.cpform.bgcolor_g_value.value = '00';
					window.document.cpform.bgcolor_r_value.value = '00';
					window.document.cpform.all.abc.bgColor= '#'+'00'+'00'+'00';
					window.opener.window.document.frmData.hdRColor.value = '00';
					window.opener.window.document.frmData.hdGColor.value = '00';
					window.opener.window.document.frmData.hdBColor.value = '00';
					return;
				}
				
				//alert(iColorCode + 'OnLoad');
			   // alert(iColorCode + 'iColorCode')
			    //var k = toHex(window.opener.window.document.frmData.hdColor.value);
			    //alert(k + '  PP');
			   // return false;
			    bgcolor_r = toDecimalr(toHex(parseInt(iColorCode)));
			    bgcolor_g = toDecimalg(toHex(parseInt(iColorCode)));
			    bgcolor_b = toDecimalb(toHex(parseInt(iColorCode)));
				
				//alert(bgcolor_r + '  ' + bgcolor_g + '  ' + bgcolor_b + 'TT');
				if (isNaN(bgcolor_b) && isNaN(bgcolor_g))
				{
					window.document.cpform.bgcolor_b_value.value = '00';
					window.document.cpform.bgcolor_g_value.value = '00';
					window.document.cpform.bgcolor_r_value.value = toHex(bgcolor_r);
					window.document.cpform.all.abc.bgColor= '#' + toHex(bgcolor_r)+ '00'+ '00';
				}
				else if (isNaN(bgcolor_b))
				{
					window.document.cpform.bgcolor_b_value.value = '00';
					window.document.cpform.bgcolor_g_value.value = toHex(bgcolor_r);
					window.document.cpform.bgcolor_r_value.value = toHex(bgcolor_g);
					window.document.cpform.all.abc.bgColor= '#'+ toHex(bgcolor_g)+toHex(bgcolor_r)+'00';
				}
				else
				{
					window.document.cpform.bgcolor_b_value.value = toHex(bgcolor_r);
					window.document.cpform.bgcolor_g_value.value = toHex(bgcolor_g);
					window.document.cpform.bgcolor_r_value.value = toHex(bgcolor_b);
					window.document.cpform.all.abc.bgColor= '#'+toHex(bgcolor_b)+toHex(bgcolor_g)+toHex(bgcolor_r);
				}
				}
			}

		//-->
		</script>
	</head>

	<body onLoad="window_onLoad();">
		<table class="formsubtitle" border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="ctrlgroup">Select Color:</td>
			</tr>
		</table>

		<map name=webpal>
			<!--- Row 1 --->
			<area onclick="SendColor('330000');return false" shape=RECT coords=2,2,18,18 href="javascript:{SendColor('330000')}">
			<area onclick="SendColor('333300');return false" shape=RECT coords=18,2,34,18 href="javascript:{SendColor('333300')}">
			<area onclick="SendColor('336600');return false" shape=RECT coords=34,2,50,18 href="javascript:{SendColor('336600')}">
			<area onclick="SendColor('339900');return false" shape=RECT coords=50,2,66,18 href="javascript:{SendColor('339900')}">
			<area onclick="SendColor('33CC00');return false" shape=RECT coords=66,2,82,18 href="javascript:{SendColor('33CC00')}">
			<area onclick="SendColor('33FF00');return false" shape=RECT coords=82,2,98,18 href="javascript:{SendColor('33FF00')}">
			<area onclick="SendColor('66FF00');return false" shape=RECT coords=98,2,114,18 href="javascript:{SendColor('66FF00')}">
			<area onclick="SendColor('66CC00');return false" shape=RECT coords=114,2,130,18 href="javascript:{SendColor('66CC00')}">
			<area onclick="SendColor('669900');return false" shape=RECT coords=130,2,146,18 href="javascript:{SendColor('669900')}">
			<area onclick="SendColor('666600');return false" shape=RECT coords=146,2,162,18 href="javascript:{SendColor('666600')}">
			<area onclick="SendColor('663300');return false" shape=RECT coords=162,2,178,18 href="javascript:{SendColor('663300')}">
			<area onclick="SendColor('660000');return false" shape=RECT coords=178,2,194,18 href="javascript:{SendColor('660000')}">
			<area onclick="SendColor('FF0000');return false" shape=RECT coords=194,2,210,18 href="javascript:{SendColor('FF0000')}">
			<area onclick="SendColor('FF3300');return false" shape=RECT coords=210,2,226,18 href="javascript:{SendColor('FF3300')}">
			<area onclick="SendColor('FF6600');return false" shape=RECT coords=226,2,242,18 href="javascript:{SendColor('FF6600')}">
			<area onclick="SendColor('FF9900');return false" shape=RECT coords=242,2,258,18 href="javascript:{SendColor('FF9900')}">
			<area onclick="SendColor('FFCC00');return false" shape=RECT coords=258,2,274,18 href="javascript:{SendColor('FFCC00')}">
			<area onclick="SendColor('FFFF00');return false" shape=RECT coords=274,2,290,18 href="javascript:{SendColor('FFFF00')}">
			<!--- Row 2 --->
			<area onclick="SendColor('330033');return false" shape=RECT coords=2,18,18,34 href="javascript:{SendColor('330033')}">
			<area onclick="SendColor('333333');return false" shape=RECT coords=18,18,34,34 href="javascript:{SendColor('333333')}">
			<area onclick="SendColor('336633');return false" shape=RECT coords=34,18,50,34 href="javascript:{SendColor('336633')}">
			<area onclick="SendColor('339933');return false" shape=RECT coords=50,18,66,34 href="javascript:{SendColor('339933')}">
			<area onclick="SendColor('33CC33');return false" shape=RECT coords=66,18,82,34 href="javascript:{SendColor('33CC33')}">
			<area onclick="SendColor('33FF33');return false" shape=RECT coords=82,18,98,34 href="javascript:{SendColor('33FF33')}">
			<area onclick="SendColor('66FF33');return false" shape=RECT coords=98,18,114,34 href="javascript:{SendColor('66FF33')}">
			<area onclick="SendColor('66CC33');return false" shape=RECT coords=114,18,130,34 href="javascript:{SendColor('66CC33')}">
			<area onclick="SendColor('669933');return false" shape=RECT coords=130,18,146,34 href="javascript:{SendColor('669933')}">
			<area onclick="SendColor('666633');return false" shape=RECT coords=146,18,162,34 href="javascript:{SendColor('666633')}">
			<area onclick="SendColor('663333');return false" shape=RECT coords=162,18,178,34 href="javascript:{SendColor('663333')}">
			<area onclick="SendColor('660033');return false" shape=RECT coords=178,18,194,34 href="javascript:{SendColor('660033')}">
			<area onclick="SendColor('FF0033');return false" shape=RECT coords=194,18,210,34 href="javascript:{SendColor('FF0033')}">
			<area onclick="SendColor('FF3333');return false" shape=RECT coords=210,18,226,34 href="javascript:{SendColor('FF3333')}">
			<area onclick="SendColor('FF6633');return false" shape=RECT coords=226,18,242,34 href="javascript:{SendColor('FF6633')}">
			<area onclick="SendColor('FF9933');return false" shape=RECT coords=242,18,258,34 href="javascript:{SendColor('FF9933')}">
			<area onclick="SendColor('FFCC33');return false" shape=RECT coords=258,18,274,34 href="javascript:{SendColor('FFCC33')}">
			<area onclick="SendColor('FFFF33');return false" shape=RECT coords=274,18,290,34 href="javascript:{SendColor('FFFF33')}">
			<!--- Row 3 --->
			<area onclick="SendColor('330066');return false" shape=RECT coords=2,34,18,50 href="javascript:{SendColor('330066')}">
			<area onclick="SendColor('333366');return false" shape=RECT coords=18,34,34,50 href="javascript:{SendColor('333366')}">
			<area onclick="SendColor('336666');return false" shape=RECT coords=34,34,50,50 href="javascript:{SendColor('336666')}">
			<area onclick="SendColor('339966');return false" shape=RECT coords=50,34,66,50 href="javascript:{SendColor('339966')}">
			<area onclick="SendColor('33CC66');return false" shape=RECT coords=66,34,82,50 href="javascript:{SendColor('33CC66')}">
			<area onclick="SendColor('33FF66');return false" shape=RECT coords=82,34,98,50 href="javascript:{SendColor('33FF66')}">
			<area onclick="SendColor('66FF66');return false" shape=RECT coords=98,34,114,50 href="javascript:{SendColor('66FF66')}">
			<area onclick="SendColor('66CC66');return false" shape=RECT coords=114,34,130,50 href="javascript:{SendColor('66CC66')}">
			<area onclick="SendColor('669966');return false" shape=RECT coords=130,34,146,50 href="javascript:{SendColor('669966')}">
			<area onclick="SendColor('666666');return false" shape=RECT coords=146,34,162,50 href="javascript:{SendColor('666666')}">
			<area onclick="SendColor('663366');return false" shape=RECT coords=162,34,178,50 href="javascript:{SendColor('663366')}">
			<area onclick="SendColor('660066');return false" shape=RECT coords=178,34,194,50 href="javascript:{SendColor('660066')}">
			<area onclick="SendColor('FF0066');return false" shape=RECT coords=194,34,210,50 href="javascript:{SendColor('FF0066')}">
			<area onclick="SendColor('FF3366');return false" shape=RECT coords=210,34,226,50 href="javascript:{SendColor('FF3366')}">
			<area onclick="SendColor('FF6666');return false" shape=RECT coords=226,34,242,50 href="javascript:{SendColor('FF6666')}">
			<area onclick="SendColor('FF9966');return false" shape=RECT coords=242,34,258,50 href="javascript:{SendColor('FF9966')}">
			<area onclick="SendColor('FFCC66');return false" shape=RECT coords=258,34,274,50 href="javascript:{SendColor('FFCC66')}">
			<area onclick="SendColor('FFFF66');return false" shape=RECT coords=274,34,290,50 href="javascript:{SendColor('FFFF66')}">
			<!--- Row 4 --->
			<area onclick="SendColor('330099');return false" shape=RECT coords=2,50,18,66 href="javascript:{SendColor('330099')}">
			<area onclick="SendColor('333399');return false" shape=RECT coords=18,50,34,66 href="javascript:{SendColor('333399')}">
			<area onclick="SendColor('336699');return false" shape=RECT coords=34,50,50,66 href="javascript:{SendColor('336699')}">
			<area onclick="SendColor('339999');return false" shape=RECT coords=50,50,66,66 href="javascript:{SendColor('339999')}">
			<area onclick="SendColor('33CC99');return false" shape=RECT coords=66,50,82,66 href="javascript:{SendColor('33CC99')}">
			<area onclick="SendColor('33FF99');return false" shape=RECT coords=82,50,98,66 href="javascript:{SendColor('33FF99')}">
			<area onclick="SendColor('66FF99');return false" shape=RECT coords=98,50,114,66 href="javascript:{SendColor('66FF99')}">
			<area onclick="SendColor('66CC99');return false" shape=RECT coords=114,50,130,66 href="javascript:{SendColor('66CC99')}">
			<area onclick="SendColor('669999');return false" shape=RECT coords=130,50,146,66 href="javascript:{SendColor('669999')}">
			<area onclick="SendColor('666699');return false" shape=RECT coords=146,50,162,66 href="javascript:{SendColor('666699')}">
			<area onclick="SendColor('663399');return false" shape=RECT coords=162,50,178,66 href="javascript:{SendColor('663399')}">
			<area onclick="SendColor('660099');return false" shape=RECT coords=178,50,194,66 href="javascript:{SendColor('660099')}">
			<area onclick="SendColor('FF0099');return false" shape=RECT coords=194,50,210,66 href="javascript:{SendColor('FF0099')}">
			<area onclick="SendColor('FF3399');return false" shape=RECT coords=210,50,226,66 href="javascript:{SendColor('FF3399')}">
			<area onclick="SendColor('FF6699');return false" shape=RECT coords=226,50,242,66 href="javascript:{SendColor('FF6699')}">
			<area onclick="SendColor('FF9999');return false" shape=RECT coords=242,50,258,66 href="javascript:{SendColor('FF9999')}">
			<area onclick="SendColor('FFCC99');return false" shape=RECT coords=258,50,274,66 href="javascript:{SendColor('FFCC99')}">
			<area onclick="SendColor('FFFF99');return false" shape=RECT coords=274,50,290,66 href="javascript:{SendColor('FFFF99')}">
			<!--- Row 5 --->
			<area onclick="SendColor('3300CC');return false" shape=RECT coords=2,66,18,82 href="javascript:{SendColor('3300CC')}">
			<area onclick="SendColor('3333CC');return false" shape=RECT coords=18,66,34,82 href="javascript:{SendColor('3333CC')}">
			<area onclick="SendColor('3366CC');return false" shape=RECT coords=34,66,50,82 href="javascript:{SendColor('3366CC')}">
			<area onclick="SendColor('3399CC');return false" shape=RECT coords=50,66,66,82 href="javascript:{SendColor('3399CC')}">
			<area onclick="SendColor('33CCCC');return false" shape=RECT coords=66,66,82,82 href="javascript:{SendColor('33CCCC')}">
			<area onclick="SendColor('33FFCC');return false" shape=RECT coords=82,66,98,82 href="javascript:{SendColor('33FFCC')}">
			<area onclick="SendColor('66FFCC');return false" shape=RECT coords=98,66,114,82 href="javascript:{SendColor('66FFCC')}">
			<area onclick="SendColor('66CCCC');return false" shape=RECT coords=114,66,130,82 href="javascript:{SendColor('66CCCC')}">
			<area onclick="SendColor('6699CC');return false" shape=RECT coords=130,66,146,82 href="javascript:{SendColor('6699CC')}">
			<area onclick="SendColor('6666CC');return false" shape=RECT coords=146,66,162,82 href="javascript:{SendColor('6666CC')}">
			<area onclick="SendColor('6633CC');return false" shape=RECT coords=162,66,178,82 href="javascript:{SendColor('6633CC')}">
			<area onclick="SendColor('6600CC');return false" shape=RECT coords=178,66,194,82 href="javascript:{SendColor('6600CC')}">
			<area onclick="SendColor('FF00CC');return false" shape=RECT coords=194,66,210,82 href="javascript:{SendColor('FF00CC')}">
			<area onclick="SendColor('FF33CC');return false" shape=RECT coords=210,66,226,82 href="javascript:{SendColor('FF33CC')}">
			<area onclick="SendColor('FF66CC');return false" shape=RECT coords=226,66,242,82 href="javascript:{SendColor('FF66CC')}">
			<area onclick="SendColor('FF99CC');return false" shape=RECT coords=242,66,258,82 href="javascript:{SendColor('FF99CC')}">
			<area onclick="SendColor('FFCCCC');return false" shape=RECT coords=258,66,274,82 href="javascript:{SendColor('FFCCCC')}">
			<area onclick="SendColor('FFFFCC');return false" shape=RECT coords=274,66,290,82 href="javascript:{SendColor('FFFFCC')}">
			<!--- Row 6 --->
			<area onclick="SendColor('3300FF');return false" shape=RECT coords=2,82,18,98 href="javascript:{SendColor('3300FF')}">
			<area onclick="SendColor('3333FF');return false" shape=RECT coords=18,82,34,98 href="javascript:{SendColor('3333FF')}">
			<area onclick="SendColor('3366FF');return false" shape=RECT coords=34,82,50,98 href="javascript:{SendColor('3366FF')}">
			<area onclick="SendColor('3399FF');return false" shape=RECT coords=50,82,66,98 href="javascript:{SendColor('3399FF')}">
			<area onclick="SendColor('33CCFF');return false" shape=RECT coords=66,82,82,98 href="javascript:{SendColor('33CCFF')}">
			<area onclick="SendColor('33FFFF');return false" shape=RECT coords=82,82,98,98 href="javascript:{SendColor('33FFFF')}">
			<area onclick="SendColor('66FFFF');return false" shape=RECT coords=98,82,114,98 href="javascript:{SendColor('66FFFF')}">
			<area onclick="SendColor('66CCFF');return false" shape=RECT coords=114,82,130,98 href="javascript:{SendColor('66CCFF')}">
			<area onclick="SendColor('6699FF');return false" shape=RECT coords=130,82,146,98 href="javascript:{SendColor('6699FF')}">
			<area onclick="SendColor('6666FF');return false" shape=RECT coords=146,82,162,98 href="javascript:{SendColor('6666FF')}">
			<area onclick="SendColor('6633FF');return false" shape=RECT coords=162,82,178,98 href="javascript:{SendColor('6633FF')}">
			<area onclick="SendColor('6600FF');return false" shape=RECT coords=178,82,194,98 href="javascript:{SendColor('6600FF')}">
			<area onclick="SendColor('FF00FF');return false" shape=RECT coords=194,82,210,98 href="javascript:{SendColor('FF00FF')}">
			<area onclick="SendColor('FF33FF');return false" shape=RECT coords=210,82,226,98 href="javascript:{SendColor('FF33FF')}">
			<area onclick="SendColor('FF66FF');return false" shape=RECT coords=226,82,242,98 href="javascript:{SendColor('FF66FF')}">
			<area onclick="SendColor('FF99FF');return false" shape=RECT coords=242,82,258,98 href="javascript:{SendColor('FF99FF')}">
			<area onclick="SendColor('FFCCFF');return false" shape=RECT coords=258,82,274,98 href="javascript:{SendColor('FFCCFF')}">
			<area onclick="SendColor('FFFFFF');return false" shape=RECT coords=274,82,290,98 href="javascript:{SendColor('FFFFFF')}">
			<!--- Row 7 --->
			<area onclick="SendColor('0000FF');return false" shape=RECT coords=2,98,18,114 href="javascript:{SendColor('0000FF')}">
			<area onclick="SendColor('0033FF');return false" shape=RECT coords=18,98,34,114 href="javascript:{SendColor('0033FF')}">
			<area onclick="SendColor('0066FF');return false" shape=RECT coords=34,98,50,114 href="javascript:{SendColor('0066FF')}">
			<area onclick="SendColor('0099FF');return false" shape=RECT coords=50,98,66,114 href="javascript:{SendColor('0099FF')}">
			<area onclick="SendColor('00CCFF');return false" shape=RECT coords=66,98,82,114 href="javascript:{SendColor('00CCFF')}">
			<area onclick="SendColor('00FFFF');return false" shape=RECT coords=82,98,98,114 href="javascript:{SendColor('00FFFF')}">
			<area onclick="SendColor('99FFFF');return false" shape=RECT coords=98,98,114,114 href="javascript:{SendColor('99FFFF')}">
			<area onclick="SendColor('99CCFF');return false" shape=RECT coords=114,98,130,114 href="javascript:{SendColor('99CCFF')}">
			<area onclick="SendColor('9999FF');return false" shape=RECT coords=130,98,146,114 href="javascript:{SendColor('9999FF')}">
			<area onclick="SendColor('9966FF');return false" shape=RECT coords=146,98,162,114 href="javascript:{SendColor('9966FF')}">
			<area onclick="SendColor('9933FF');return false" shape=RECT coords=162,98,178,114 href="javascript:{SendColor('9933FF')}">
			<area onclick="SendColor('9900FF');return false" shape=RECT coords=178,98,194,114 href="javascript:{SendColor('9900FF')}">
			<area onclick="SendColor('CC00FF');return false" shape=RECT coords=194,98,210,114 href="javascript:{SendColor('CC00FF')}">
			<area onclick="SendColor('CC33FF');return false" shape=RECT coords=210,98,226,114 href="javascript:{SendColor('CC33FF')}">
			<area onclick="SendColor('CC66FF');return false" shape=RECT coords=226,98,242,114 href="javascript:{SendColor('CC66FF')}">
			<area onclick="SendColor('CC99FF');return false" shape=RECT coords=242,98,258,114 href="javascript:{SendColor('CC99FF')}">
			<area onclick="SendColor('CCCCFF');return false" shape=RECT coords=258,98,274,114 href="javascript:{SendColor('CCCCFF')}">
			<area onclick="SendColor('CCFFFF');return false" shape=RECT coords=274,98,290,114 href="javascript:{SendColor('CCFFFF')}">
			<!--- Row 8 --->
			<area onclick="SendColor('0000CC');return false" shape=RECT coords=2,114,18,130 href="javascript:{SendColor('0000CC')}">
			<area onclick="SendColor('0033CC');return false" shape=RECT coords=18,114,34,130 href="javascript:{SendColor('0033CC')}">
			<area onclick="SendColor('0066CC');return false" shape=RECT coords=34,114,50,130 href="javascript:{SendColor('0066CC')}">
			<area onclick="SendColor('0099CC');return false" shape=RECT coords=50,114,66,130 href="javascript:{SendColor('0099CC')}">
			<area onclick="SendColor('00CCCC');return false" shape=RECT coords=66,114,82,130 href="javascript:{SendColor('00CCCC')}">
			<area onclick="SendColor('00FFCC');return false" shape=RECT coords=82,114,98,130 href="javascript:{SendColor('00FFCC')}">
			<area onclick="SendColor('99FFCC');return false" shape=RECT coords=98,114,114,130 href="javascript:{SendColor('99FFCC')}">
			<area onclick="SendColor('99CCCC');return false" shape=RECT coords=114,114,130,130 href="javascript:{SendColor('99CCCC')}">
			<area onclick="SendColor('9999CC');return false" shape=RECT coords=130,114,146,130 href="javascript:{SendColor('9999CC')}">
			<area onclick="SendColor('9966CC');return false" shape=RECT coords=146,114,162,130 href="javascript:{SendColor('9966CC')}">
			<area onclick="SendColor('9933CC');return false" shape=RECT coords=162,114,178,130 href="javascript:{SendColor('9933CC')}">
			<area onclick="SendColor('9900CC');return false" shape=RECT coords=178,114,194,130 href="javascript:{SendColor('9900CC')}">
			<area onclick="SendColor('CC00CC');return false" shape=RECT coords=194,114,210,130 href="javascript:{SendColor('CC00CC')}">
			<area onclick="SendColor('CC33CC');return false" shape=RECT coords=210,114,226,130 href="javascript:{SendColor('CC33CC')}">
			<area onclick="SendColor('CC66CC');return false" shape=RECT coords=226,114,242,130 href="javascript:{SendColor('CC66CC')}">
			<area onclick="SendColor('CC99CC');return false" shape=RECT coords=242,114,258,130 href="javascript:{SendColor('CC99CC')}">
			<area onclick="SendColor('CCCCCC');return false" shape=RECT coords=258,114,274,130 href="javascript:{SendColor('CCCCCC')}">
			<area onclick="SendColor('CCFFCC');return false" shape=RECT coords=274,114,290,130 href="javascript:{SendColor('CCFFCC')}">
			<!--- Row 9 --->
			<area onclick="SendColor('000099');return false" shape=RECT coords=2,130,18,146 href="javascript:{SendColor('000099')}">
			<area onclick="SendColor('003399');return false" shape=RECT coords=18,130,34,146 href="javascript:{SendColor('003399')}">
			<area onclick="SendColor('006699');return false" shape=RECT coords=34,130,50,146 href="javascript:{SendColor('006699')}">
			<area onclick="SendColor('009999');return false" shape=RECT coords=50,130,66,146 href="javascript:{SendColor('009999')}">
			<area onclick="SendColor('00CC99');return false" shape=RECT coords=66,130,82,146 href="javascript:{SendColor('00CC99')}">
			<area onclick="SendColor('00FF99');return false" shape=RECT coords=82,130,98,146 href="javascript:{SendColor('00FF99')}">
			<area onclick="SendColor('99FF99');return false" shape=RECT coords=98,130,114,146 href="javascript:{SendColor('99FF99')}">
			<area onclick="SendColor('99CC99');return false" shape=RECT coords=114,130,130,146 href="javascript:{SendColor('99CC99')}">
			<area onclick="SendColor('999999');return false" shape=RECT coords=130,130,146,146 href="javascript:{SendColor('999999')}">
			<area onclick="SendColor('996699');return false" shape=RECT coords=146,130,162,146 href="javascript:{SendColor('996699')}">
			<area onclick="SendColor('993399');return false" shape=RECT coords=162,130,178,146 href="javascript:{SendColor('993399')}">
			<area onclick="SendColor('990099');return false" shape=RECT coords=178,130,194,146 href="javascript:{SendColor('990099')}">
			<area onclick="SendColor('CC0099');return false" shape=RECT coords=194,130,210,146 href="javascript:{SendColor('CC0099')}">
			<area onclick="SendColor('CC3399');return false" shape=RECT coords=210,130,226,146 href="javascript:{SendColor('CC3399')}">
			<area onclick="SendColor('CC6699');return false" shape=RECT coords=226,130,242,146 href="javascript:{SendColor('CC6699')}">
			<area onclick="SendColor('CC9999');return false" shape=RECT coords=242,130,258,146 href="javascript:{SendColor('CC9999')}">
			<area onclick="SendColor('CCCC99');return false" shape=RECT coords=258,130,274,146 href="javascript:{SendColor('CCCC99')}">
			<area onclick="SendColor('CCFF99');return false" shape=RECT coords=274,130,290,146 href="javascript:{SendColor('CCFF99')}">
			<!--- Row 10 --->
			<area onclick="SendColor('000066');return false" shape=RECT coords=2,146,18,162 href="javascript:{SendColor('000066')}">
			<area onclick="SendColor('003366');return false" shape=RECT coords=18,146,34,162 href="javascript:{SendColor('003366')}">
			<area onclick="SendColor('006666');return false" shape=RECT coords=34,146,50,162 href="javascript:{SendColor('006666')}">
			<area onclick="SendColor('009966');return false" shape=RECT coords=50,146,66,162 href="javascript:{SendColor('009966')}">
			<area onclick="SendColor('00CC66');return false" shape=RECT coords=66,146,82,162 href="javascript:{SendColor('00CC66')}">
			<area onclick="SendColor('00FF66');return false" shape=RECT coords=82,146,98,162 href="javascript:{SendColor('00FF66')}">
			<area onclick="SendColor('99FF66');return false" shape=RECT coords=98,146,114,162 href="javascript:{SendColor('99FF66')}">
			<area onclick="SendColor('99CC66');return false" shape=RECT coords=114,146,130,162 href="javascript:{SendColor('99CC66')}">
			<area onclick="SendColor('999966');return false" shape=RECT coords=130,146,146,162 href="javascript:{SendColor('999966')}">
			<area onclick="SendColor('996666');return false" shape=RECT coords=146,146,162,162 href="javascript:{SendColor('996666')}">
			<area onclick="SendColor('993366');return false" shape=RECT coords=162,146,178,162 href="javascript:{SendColor('993366')}">
			<area onclick="SendColor('990066');return false" shape=RECT coords=178,146,194,162 href="javascript:{SendColor('990066')}">
			<area onclick="SendColor('CC0066');return false" shape=RECT coords=194,146,210,162 href="javascript:{SendColor('CC0066')}">
			<area onclick="SendColor('CC3366');return false" shape=RECT coords=210,146,226,162 href="javascript:{SendColor('CC3366')}">
			<area onclick="SendColor('CC6666');return false" shape=RECT coords=226,146,242,162 href="javascript:{SendColor('CC6666')}">
			<area onclick="SendColor('CC9966');return false" shape=RECT coords=242,146,258,162 href="javascript:{SendColor('CC9966')}">
			<area onclick="SendColor('CCCC66');return false" shape=RECT coords=258,146,274,162 href="javascript:{SendColor('CCCC66')}">
			<area onclick="SendColor('CCFF66');return false" shape=RECT coords=274,146,290,162 href="javascript:{SendColor('CCFF66')}">
			<!--- Row 11 --->
			<area onclick="SendColor('000033');return false" shape=RECT coords=2,162,18,178 href="javascript:{SendColor('000033')}">
			<area onclick="SendColor('003333');return false" shape=RECT coords=18,162,34,178 href="javascript:{SendColor('003333')}">
			<area onclick="SendColor('006633');return false" shape=RECT coords=34,162,50,178 href="javascript:{SendColor('006633')}">
			<area onclick="SendColor('009933');return false" shape=RECT coords=50,162,66,178 href="javascript:{SendColor('009933')}">
			<area onclick="SendColor('00CC33');return false" shape=RECT coords=66,162,82,178 href="javascript:{SendColor('00CC33')}">
			<area onclick="SendColor('00FF33');return false" shape=RECT coords=82,162,98,178 href="javascript:{SendColor('00FF33')}">
			<area onclick="SendColor('99FF33');return false" shape=RECT coords=98,162,114,178 href="javascript:{SendColor('99FF33')}">
			<area onclick="SendColor('99CC33');return false" shape=RECT coords=114,162,130,178 href="javascript:{SendColor('99CC33')}">
			<area onclick="SendColor('999933');return false" shape=RECT coords=130,162,146,178 href="javascript:{SendColor('999933')}">
			<area onclick="SendColor('996633');return false" shape=RECT coords=146,162,162,178 href="javascript:{SendColor('996633')}">
			<area onclick="SendColor('993333');return false" shape=RECT coords=162,162,178,178 href="javascript:{SendColor('993333')}">
			<area onclick="SendColor('990033');return false" shape=RECT coords=178,162,194,178 href="javascript:{SendColor('990033')}">
			<area onclick="SendColor('CC0033');return false" shape=RECT coords=194,162,210,178 href="javascript:{SendColor('CC0033')}">
			<area onclick="SendColor('CC3333');return false" shape=RECT coords=210,162,226,178 href="javascript:{SendColor('CC3333')}">
			<area onclick="SendColor('CC6633');return false" shape=RECT coords=226,162,242,178 href="javascript:{SendColor('CC6633')}">
			<area onclick="SendColor('CC9933');return false" shape=RECT coords=242,162,258,178 href="javascript:{SendColor('CC9933')}">
			<area onclick="SendColor('CCCC33');return false" shape=RECT coords=258,162,274,178 href="javascript:{SendColor('CCCC33')}">
			<area onclick="SendColor('CCFF33');return false" shape=RECT coords=274,162,290,178 href="javascript:{SendColor('CCFF33')}">
			<!--- Row 12 --->
			<area onclick="SendColor('000000');return false" shape=RECT coords=2,178,18,194 href="javascript:{SendColor('000000')}">
			<area onclick="SendColor('003300');return false" shape=RECT coords=18,178,34,194 href="javascript:{SendColor('003300')}">
			<area onclick="SendColor('006600');return false" shape=RECT coords=34,178,50,194 href="javascript:{SendColor('006600')}">
			<area onclick="SendColor('009900');return false" shape=RECT coords=50,178,66,194 href="javascript:{SendColor('009900')}">
			<area onclick="SendColor('00CC00');return false" shape=RECT coords=66,178,82,194 href="javascript:{SendColor('00CC00')}">
			<area onclick="SendColor('00FF00');return false" shape=RECT coords=82,178,98,194 href="javascript:{SendColor('00FF00')}">
			<area onclick="SendColor('99FF00');return false" shape=RECT coords=98,178,114,194 href="javascript:{SendColor('99FF00')}">
			<area onclick="SendColor('99CC00');return false" shape=RECT coords=114,178,130,194 href="javascript:{SendColor('99CC00')}">
			<area onclick="SendColor('999900');return false" shape=RECT coords=130,178,146,194 href="javascript:{SendColor('999900')}">
			<area onclick="SendColor('996600');return false" shape=RECT coords=146,178,162,194 href="javascript:{SendColor('996600')}">
			<area onclick="SendColor('993300');return false" shape=RECT coords=162,178,178,194 href="javascript:{SendColor('993300')}">
			<area onclick="SendColor('990000');return false" shape=RECT coords=178,178,194,194 href="javascript:{SendColor('990000')}">
			<area onclick="SendColor('CC0000');return false" shape=RECT coords=194,178,210,194 href="javascript:{SendColor('CC0000')}">
			<area onclick="SendColor('CC3300');return false" shape=RECT coords=210,178,226,194 href="javascript:{SendColor('CC3300')}">
			<area onclick="SendColor('CC6600');return false" shape=RECT coords=226,178,242,194 href="javascript:{SendColor('CC6600')}">
			<area onclick="SendColor('CC9900');return false" shape=RECT coords=242,178,258,194 href="javascript:{SendColor('CC9900')}">
			<area onclick="SendColor('CCCC00');return false" shape=RECT coords=258,178,274,194 href="javascript:{SendColor('CCCC00')}">
			<area onclick="SendColor('CCFF00');return false" shape=RECT coords=274,178,290,194 href="javascript:{SendColor('CCFF00')}">
		</map>
		<!-- bgcolor -->
		<map name=bgcolor_r>
			<area onclick="hitArrow('img_bgcolor_r','upa','bgcolor');return false" shape=RECT coords=1,1,14,11 href="javascript:{;}">
			<area onclick="hitArrow('img_bgcolor_r','upb','bgcolor');return false" shape=RECT coords=15,1,28,11 href="javascript:{;}">
			<area onclick="hitArrow('img_bgcolor_r','dna','bgcolor');return false" shape=RECT coords=1,12,14,22 href="javascript:{;}">
			<area onclick="hitArrow('img_bgcolor_r','dnb','bgcolor');return false" shape=RECT coords=15,12,28,22 href="javascript:{;}">
		</map>
		<map name=bgcolor_g>
			<area onclick="hitArrow('img_bgcolor_g','upa','bgcolor');return false" shape=RECT coords=1,1,14,11 href="javascript:{;}">
			<area onclick="hitArrow('img_bgcolor_g','upb','bgcolor');return false" shape=RECT coords=15,1,28,11 href="javascript:{;}">
			<area onclick="hitArrow('img_bgcolor_g','dna','bgcolor');return false" shape=RECT coords=1,12,14,22 href="javascript:{;}">
			<area onclick="hitArrow('img_bgcolor_g','dnb','bgcolor');return false" shape=RECT coords=15,12,28,22 href="javascript:{;}">
		</map>
		<map name=bgcolor_b>
			<area onclick="hitArrow('img_bgcolor_b','upa','bgcolor');return false" shape=RECT coords=1,1,14,11 href="javascript:{;}">
			<area onclick="hitArrow('img_bgcolor_b','upb','bgcolor');return false" shape=RECT coords=15,1,28,11 href="javascript:{;}">
			<area onclick="hitArrow('img_bgcolor_b','dna','bgcolor');return false" shape=RECT coords=1,12,14,22 href="javascript:{;}">
			<area onclick="hitArrow('img_bgcolor_b','dnb','bgcolor');return false" shape=RECT coords=15,12,28,22 href="javascript:{;}">
		</map>
		
		<form name=cpform>
			<table cellSpacing=0 cellPadding=0 align=center border=0>
				<tbody>
					<tr>
						<td colSpan=6><img height=196 alt="" src="img/color216.gif" width=292 useMap=#webpal border=0>
						</td>
					</tr>
				</tbody>
			</table>

			<table cellSpacing=0 cellPadding=0 align=center border=0 id=TABLE1>
				<tbody>
					<tr>
						<td align=middle bgColor=#ffffff colSpan=11><img height=1 alt="" src="img/empty.gif" width=1 border=0>
						</td>
					</tr>
					<tr>
						<td><font face=arial,helvetica size=1>&nbsp;</font></td>
						<td align=middle><img height=1 src="img/empty.gif" width=4></td>
						<td class=rdbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=rdbox align=middle><font face=arial,helvetica size=1>RED</font></td>
						<td class=rdbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=grbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=grbox align=middle><font face=arial,helvetica size=1>GREEN</font></td>
						<td class=grbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=blbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=blbox align=middle><font face=arial,helvetica size=1>BLUE</font></td>
						<td class=blbox align=middle><img height=1 src="img/empty.gif" width=2></td>
					</tr>
					<tr>
						<td align=middle bgColor=#999999 colSpan=11><img height=1 alt="" src="img/empty.gif" width=1 border=0></td>
					</tr>
					<tr>
						<td>Color:</td>
						<td align=middle><img height=1 src="img/empty.gif" width=4></td>
						<td class=rdbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=rdbox align=middle><input readOnly maxLength=2 size=2 name=bgcolor_r_value><img height=22 src="img/arrows.gif" 
						  width=28 align=absMiddle useMap=#bgcolor_r vspace=2 border=0 name=img_bgcolor_r></td>
						<td class=rdbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=grbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=grbox align=middle><input readOnly maxLength=2 size=2 name=bgcolor_g_value><img height=22 src="img/arrows.gif" 
						  width=28 align=absMiddle useMap=#bgcolor_g vspace=2 border=0 name=img_bgcolor_g></td>
						<td class=grbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=blbox align=middle><img height=1 src="img/empty.gif" width=2></td>
						<td class=blbox align=middle><input readOnly maxLength=2 size=2 name=bgcolor_b_value><img height=22 src="img/arrows.gif" 
						  width=28 align=absMiddle useMap=#bgcolor_b vspace=2 border=0 name=img_bgcolor_b></td>
						<td class=blbox align=middle><img height=1 src="img/empty.gif" width=2></td></tr>
					<tr>
						<td align=middle bgColor=#999999 colSpan=11><img height=1 alt="" src="img/empty.gif" width=1 border=0></td></tr><!-- text -->
					<tr>
						<td>Effect:</td>
						<td align=middle><img height=1 src="img/empty.gif" width=4></td>
						<td id="abc" colSpan=9><table bordercolor="black" cellspacing="0" cellpadding="0" width="100%" border=1><tr><td>&nbsp;</td></tr></table></td>
					</tr>
					<tr>
						<td align=middle bgColor=#999999 colSpan=11><img height=1 alt="" src="img/empty.gif" width=1 border=0></td>
					</tr>
					<tr>
						<td align=center colSpan=11>
							<table width="100%" border="0" align=center>
								<tr>
									<td width="50%" align="right"><input class="button" type="button" value="OK" onClick="on_OK();" style="width:50%"></td>
									<td width="50%" align="left"><input class="button" type="button" value="Cancel" onClick="window.close()" style="width:50%"></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		<input type="hidden" name="hdColorCode" value="<%=Request.QueryString("a")%>">
		<input type="hidden" name="hdCheckWindow" value="<%=Request.QueryString("CHECKWINDOW")%>">
		<input type="hidden" name="hdIndex" value="<%=Request.QueryString("GLOBALINDEX")%>">
		
		</form>

		<script language=JavaScript>
		<!--
			fillHexes();
		//--></script>
	</body>
</html>
