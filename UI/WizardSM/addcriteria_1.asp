<!-- #include file ="session.inc" -->
<!-- #include file ="generic.asp" -->
<html> 
	<head> 
		<link rel="stylesheet" href="RMNet.css" type="text/css"/> 
		<title>SMD - [Add Criteria]</title> 

<%

Dim myArrayP, sDerivedFieldName 
Dim pageError
'-- Rajeev -- 03/09/2004
'''myArrayP = Split(objSessionStr("ArrayP"),",") 
myArrayP = Split(objSessionStr("ArrayP"),"*!^") 
i = Request.QueryString("a")
 
sDerivedFieldName = Right(myArrayP(i),Len(myArrayP(i))-InStr(myArrayP(i),":"))
Dim sTableInitial
sTableInitial = Left(myArrayP(i),InStr(myArrayP(i),":")-1)

Dim m_objSMList, lTableID, lDBTableID, arrFields()
 
Set m_objSMList = Nothing
On Error Resume Next
Set m_objSMList = Server.CreateObject("InitSMList.CInitSM")
On Error Goto 0

If m_objSMList Is Nothing Then
	Call ShowError(pageError, "Call to CInitSM class failed.", True)
End If

m_objSMList.m_RMConnectStr = objSessionStr(SESSION_DSN) 
m_objSMList.m_SMConnectStr = Application(APP_SMDDSN) 

Dim objXML 
Set objXML = Nothing
On Error Resume Next
Set objXML = Server.CreateObject("Msxml2.DOMDocument")
On Error Goto 0

If objXML Is Nothing Then
	Call ShowError(pageError, "XML Parser is not available.", True)
End If

objXML.async = false

objXML.loadXML Replace(Request.QueryString("c"), "&", "n")

lTableId = objXML.getElementsByTagName("column").item(0).attributes(1).text  
iFieldId = objXML.getElementsByTagName("column").item(0).attributes(2).text  

'-- If not derived field
If InStr(1, lTableId, "-") = 0 Then

	'-- Rajeev -- 06/25/2004 -- Force to show the fields
	m_objSMList.m_SuppTableName = "DUMMY"
	
	If objSessionStr(SESSION_MODULESECURITY)="1" Then 
		m_objSMList.FillSMFieldListForCriteria Application(APP_PERMCACHE & objSessionStr(SESSION_DSNID)), objSessionStr(SESSION_GROUPID), CLng(lTableID), CInt(iFieldId),  arrFields 
	Else 
		m_objSMList.FillSMFieldListForCriteria Nothing, "", CLng(lTableID), CInt(iFieldId), arrFields 
	End if 

	Dim arrFieldNames(), arrFieldType(), arrDBTableIDs(),arrFieldId()
	Call GetFieldNames(arrFields, arrFieldNames, arrFieldType, arrDBTableIDs,arrFieldId) 

	Dim a, m 
	
	Err.Clear()
	On Error Resume Next
	m = -1
	m = UBound(arrFieldType)
	
	If Err.number <> 9 And m <> "" And m >=0 Then
		For i = 0 To UBound(arrFieldType) 
			If i = 0 Then 
				a = arrFieldType(0) 
			Else 
				a = a & "," & arrFieldType(i) 
			End If 
		Next
	End If
	
	m = -1
	m = UBound(arrDBTableIDs)
	
	If Err.number <> 9 And m <> "" And m >=0 Then
		For i = 0 To UBound(arrDBTableIDs) 
			If i = 0 Then 
				c = arrDBTableIDs(0) 
			Else 
				c = c & "," & arrDBTableIDs(i) 
			End If 
		Next 
	End If

	m = -1
	m = UBound(arrFieldId)
	
	If Err.number <> 9 And m <> "" And m >=0 Then
		For i = 0 To UBound(arrFieldId) 
			If i = 0 Then 
				d = arrFieldId(0) 
			Else 
				d = d & "," & arrFieldId(i) 
			End If 
		Next 
	End If

	m = -1
	m = UBound(arrFieldNames)
	
	If Not (Err.number <> 9 And m <> "" And m >=0) Then
		Call ShowError(pageError, "No fields are available.", False)
	Else
		For i = 0 To UBound(arrFieldNames) 
			If i = 0 Then 
				b = arrFieldNames(0) 
			Else 
				b = b & "," & arrFieldNames(i) 
			End If 
		Next
	End If	
	
	On Error Goto 0
Else '-- Derived field

	If objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "text" _
		Or objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "time" _
		Or objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "datetime" Then
		a = 0
	ElseIf objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "formattednumeric" _
		Or objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "numeric" _
		Or objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "currency" Then
		a = 1
	ElseIf objXML.getElementsByTagName("column").item(0).childNodes.item(0).attributes(0).text = "date" Then
		a = 2
	End If

	b = objXML.getElementsByTagName("column").item(0).attributes(3).text
	c = lTableId
	d = iFieldId

End If

Set m_objSMList = Nothing

Function GetFieldNames(ByRef pArrFields, ByRef pArrFieldNames, ByRef pArrFieldType, ByRef pArrDBTableID, ByRef pArrFieldID) 
	'-- arrFields contains field name and id seperated by | 
	'-- Extract only field names from arrFields in sorted_arrFields 
	Err.Clear()
	On Error Resume Next 
	Dim m
	m = -1
	m = UBound(pArrFields) 

	If Err.number <> 9 And m <> "" And m >=0 Then 
		'-- Extract field names 
		For iCount = 0 To UBound(pArrFields) 
		ReDim Preserve pArrFieldNames(iCount) 
		ReDim Preserve pArrFieldType(iCount) 
		ReDim Preserve pArrDBTableID(iCount)
		ReDim Preserve pArrFieldID(iCount)

		temp = Split(pArrFields(iCount), "|") 
		pArrFieldNames(iCount) = temp(0) 
		pArrFieldType(iCount) = temp(1) 
		pArrDBTableID(iCount) = temp(2) 
		pArrFieldID(iCount) = temp(3) 
		Next 
	End IF 
	On Error Goto 0
End Function 
%> 

		<Script> 

			function pageLoaded() 
			{ 
				if(document.frmData.hdSelFields.value != '-1') 
//-- Rajeev -- 03/09/2004				
//				p = document.frmData.hdSelFields.value.split(',') 
				p = document.frmData.hdSelFields.value.split('*!^') 

				match = document.frmData.hdTableName.value.substring(0,6) + ": " + document.frmData.hdSelCriteriaField.value; 
				for(i=0;i<document.frmData.lstFields.length;i++) 
				{ 
					if(document.frmData.lstFields.options[i].text == match) 
					{ 
						var selIndex; 
						selIndex = i; 
						break; 
					} 
					
				} 
				document.frmData.lstFields.selectedIndex = selIndex; 

				CheckFieldType();
			} 

			function findIndex() 
			{ 
				var selIndex,selFieldType,selDBTableID, selTableID, selFieldID; 
				var arrFieldType = new Array; 
				var arrDBTableID = new Array; 
				var arrFieldID = new Array; 
				var selTableId;
				
				selIndex = window.document.frmData.lstFields.selectedIndex; 
				arrFieldType = window.document.frmData.hdFieldType.value.split(","); 
				arrDBTableID = window.document.frmData.hdDBTableId.value.split(","); 
				arrFieldID = window.document.frmData.hdFieldId.value.split(","); 
				
				selFieldType = arrFieldType[selIndex]; 
				selDBTableID = arrDBTableID[selIndex]; 
				selFieldID = arrFieldID[selIndex];
				selTableId = window.document.frmData.hdTableId.value; 
				if (selTableId.indexOf("-") > -1)
				{
					window.opener.window.document.frmData.hdSelFieldCriteria.value = window.document.frmData.lstFields.options[selIndex].text
				}
				else
				{
					//window.opener.window.document.frmData.hdSelFieldCriteria.value = window.document.frmData.lstFields.options[selIndex].text.substring(7,window.document.frmData.lstFields.options[selIndex].length) 
					window.opener.window.document.frmData.hdSelFieldCriteria.value = window.document.frmData.lstFields.options[selIndex].text.substring(window.document.frmData.lstFields.options[selIndex].text.indexOf(':') + 1,window.document.frmData.lstFields.options[selIndex].length)
				}
				var selCriteria 
				if(window.document.frmData.optSelCriteria[0].checked == true) 
				selCriteria = 0; 
				else 
				selCriteria = 1; 

				window.opener.openCriteria(selFieldType,selCriteria,selDBTableID,selTableId,selFieldID); 
				window.close(); 
			} 

			function CheckFieldType() 
			{ 
				var selIndex,selFieldType; 
				var arrFieldType = new Array; 
				selIndex = 
				window.document.frmData.lstFields.selectedIndex; 
				arrFieldType = window.document.frmData.hdFieldType.value.split(","); 
				selFieldType = arrFieldType[selIndex] 
				if ( selFieldType == 3 || selFieldType == 4 || selFieldType == 5) 
				{ 
					window.document.frmData.optSelCriteria[0].checked = 
					true; 
					window.document.frmData.optSelCriteria[0].disabled = true; 
					window.document.frmData.optSelCriteria[1].disabled = true; 
				} 
				else 
				{ 
					window.document.frmData.optSelCriteria[0].disabled = false; 
					window.document.frmData.optSelCriteria[1].disabled = false; 
				} 
			} 

		</Script> 
	</head> 

	<body onLoad="pageLoaded()"> 
	<form name="frmData" topmargin="0" leftmargin="0"> 
		<table class="formsubtitle" border="0" width="100%" align="center"> 
			<tr> 
				<td class="ctrlgroup">Select Criteria Field :</td> 
			</tr> 
		</table> 
		<table class1="singleborder" width="100%" height="80%" border="0" align="center"> 
			<tr height="33%"> 
				<td>&nbsp;</td> 
			</tr> 
			<tr height="44%"> 
				<td> 
					<table width="25%" border="0" align="center"> 
						<tr> 
							<td align="center">Field:&nbsp;</td>
							<td><select name="lstFields" onChange="CheckFieldType()"> 
								<%
								Dim i 
								'-- If derived field
								If InStr(1, lTableId, "-") > 0 Then
									Response.Write "<option>" & sDerivedFieldName & "</option>" 
								Else
									If Err.number <> 9 And m <> "" And m >=0 Then
										For i = 0 to UBound(arrFieldNames) 
											If i=0 then 
												Response.Write "<option selected>" & Left(sTableInitial,6) & ":" & arrFieldNames(i) & "</option>" 
											Else 
												Response.Write "<option>" & Left(sTableInitial,6) & ":" & arrFieldNames(i) & "</option>" 
											End If 
										Next 
									End If
								End If 
								%> 
								</select> 
							</td> 
						</tr> 
						<tr> 
							<td colspan="2">&nbsp;</td> 
						</tr> 
						<tr> 
							<td align="center" colspan="2"><input type="radio" value="1" name="optSelCriteria" checked>Value&nbsp;<input type="radio" value="2" name="optSelCriteria">Range</td> 
						</tr> 
					</table> 
					<hr> 
					<table align="center" cellspacing="0" cellpadding="0" width="100%"> 
						<tr> 
							<td align="center"> 
								<table width="100%"> 
									<tr> 
										<td width="33%"><input class="button" type="button" name="ok" value="OK" onClick="return findIndex();" style="width:100%"></td> 
										<td width="34%"><input class="button" type="button" name="cancel" value="Cancel" onClick="window.close()" style="width:100%"></td> 
										<td width="33%"><input class="button" type="button" name="Delete" value="Help" style="width:100%"></td> 
									</tr> 
								</table> 
							</td> 
						</tr> 
					</table> 
				</td> 
			</tr> 
			<tr height="23%"> 
				<td colspan="2">&nbsp;</td> 
			</tr> 
		</table> 

		<input type="hidden" name="hdSelFields"value="<%=objSessionStr("ArrayP")%>"> 
		<input type="hidden" name="hdFieldType" value="<%=a%>"> 
		<input type="hidden" name="hdFieldName" value="<%=b%>"> 
		<input type="hidden" name="hdDBTableId" value="<%=c%>"> 
		<input type="hidden" name="hdFieldId" value="<%=d%>"> 
		<input type="hidden" name="hdTableId" value="<%=lTableID%>"> 
		<input type="hidden" name="hdCurrentLevel" value="<%=objSessionStr("CurrentLevel")%>"> 
		<input type="hidden" name="hdSelCriteriaField" value="<%=Request.QueryString("b")%>"> 
		<input type="hidden" name="hdTableName" value="<%=sTableInitial%>"> 

	</form> 
	</body> 
</html>