<xsl:stylesheet version="2.0" xmlns:xhtml="http://www.w3.org/1999/xhtml" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	 xmlns:user="urn:SaxonImpl">
	<xsl:function name="app:sort-controls">
		<xsl:param name="in" />
		<xsl:perform-sort select="$in">
			<xsl:sort select="number(concat('0',@tabindex))" />
		</xsl:perform-sort>
	</xsl:function>
	<xsl:template match="/">
		<html>
			<head>
				<title>
					<xsl:apply-templates select="form" />
				</title>
				<link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
				<xsl:if test="form/@includefilename">
					<script language="JavaScript"><xsl:attribute name="SRC">
							<xsl:value-of select="form/@includefilename" />
						</xsl:attribute><![CDATA[{var i;}]]></script>
				</xsl:if>
			</head>
			<body class="10pt">
				<form name="frmData" method="post">
					<input type="hidden" name="sysCmd" />
					<div class="formtitle" id="formtitle">
						<xsl:value-of select="form/@title" />
					</div>
					<table border="0">
						<tr>
							<td>
								<a class="LightBold" href="#" onClick="window.print()">
									<img src="../../Images/recordsummary.gif" width="28" height="28" border="0"
										alt="" title="Print" />
								</a>
							</td>
						</tr>
						<tr>
							<td>
								<xsl:if test="form/errors">
									<br />
									<div class="errtextheader">Following errors were reported:</div>
									<ul>
										<xsl:apply-templates select="form/errors/error" />
									</ul>
								</xsl:if>
								<xsl:apply-templates select="form/control" />
								<table border="0">
									<xsl:for-each select="form/group">
										<tr>
											<td colspan="2">
												<table border="1" frame="below" width="100%">
													<tr>
														<td>
															<strong>
																<font color="#CCCC33" face="Arial, Helvetica, sans-serif">
																	<xsl:value-of select="@title" />
																</font>
															</strong>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<xsl:apply-templates select="displaycolumn/control | displaycolumn/control/@type['controlgroup']/control" />
									</xsl:for-each>
								</table>
								<xsl:apply-templates select="//internal" />
								<input type="hidden" name="formname">
									<xsl:attribute name="value">
										<xsl:for-each select="form">
											<xsl:value-of select="@name" />
										</xsl:for-each>
									</xsl:attribute>
								</input>
								<input type="hidden" name="sys_required">
									<xsl:attribute name="value">
										<xsl:for-each select=".//control">
											<xsl:if test="@required[.='yes']"><xsl:value-of select="@name" /><xsl:if test="@type[.='code']">_cid</xsl:if><xsl:if test="@type[.='orgh']">_cid</xsl:if>|</xsl:if>
										</xsl:for-each>
									</xsl:attribute>
								</input>
							</td>
							<td valign="top">
								<xsl:if test="/form/@image">
									<img border="0" align="right">
										<xsl:attribute name="src">img/<xsl:value-of select="/form/@image" /></xsl:attribute>
									</img>
								</xsl:if>
							</td>
						</tr>
					</table>
				</form>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="control">
		<xsl:choose>
			<xsl:when test="@type[.='id']">
				<input type="hidden">
					<xsl:attribute name="name">
						<xsl:value-of select="@name" />
					</xsl:attribute>
					<xsl:attribute name="value">
				
					</xsl:attribute>
				</input> 
			</xsl:when>
			<xsl:otherwise>
				<tr>
					<xsl:choose>
						<!-- Suppress the following Controls altogether -->
						<xsl:when test="@type[.='button']">
							<td colspan="2"></td>
						</xsl:when>
						<xsl:when test="@type[.='serverbutton']">
							<td colspan="2"></td>
						</xsl:when>
						<xsl:when test="@type[.='buttonscript']"> <!-- BSB (buttonscript was getting through on admin causing a problem. -->
							<td colspan="2"></td>
						</xsl:when>
						<xsl:when test="@type[.='href']">
							<td colspan="2"></td>
						</xsl:when>
						<!--Nitesh resolved Bug No.723 controls suppressed -->
						<xsl:when test="@type[.='linebreak']">
							<td colspan="2"></td>
						</xsl:when>
            <xsl:when test="@type[.='singlerow']">
							<td colspan="2"></td>
						</xsl:when>
            <!--npadhy MITS 15514 Added code to handle the Grid and GridandButtons Control for Policy Management
            This will be valid for other screens with grid as well-->
            <xsl:when test="@type[.='Grid'] or @type[.='GridAndButtons']">

              <xsl:variable name="ListTarget" select="@target" />
              <xsl:variable name="ListName" select="@name" />
              <xsl:variable name="HideNodes" select="@hidenodes" />
              <xsl:variable name="UniqueId" select="@uniqueid"/>
              <xsl:variable name="ref" select="@ref"/>
              <xsl:variable name="ShowHeader" select="@showheader"/>
              <xsl:variable name="ShowRadioButton" select="@showradiobutton"/>
              <xsl:variable name="LinkColumn" select="@linkcolumn"/>
              <xsl:variable name="onclick" select="@onclick"/>
              <xsl:variable name="editable" select="@editable"/>
              <!--xsl:value-of select="@gridtitle" /-->
              <!-- Ayush: MITS:18230: Start xsl:<td colspan="2">  colspan changed to 3 as grid was getting populated only in the 1st 2 column. /-->
              <td colspan="3">
              <!--Ayush: MITS:18230 End/-->
                <table>
                  <tr>
                    <td>
                      <xsl:attribute name="style">
                        width:<xsl:value-of select="@width"/>;height:<xsl:value-of select="@height"/>;
                        <!--As Auto scroll was not working since starting
                            By assuming auto scroll not included in most of the screens. To keep all previous screens intact 
                            introduced new attribute 'styleoverflowauto'to changes grid style overflow attribute.-->
                        <!--<xsl:if test="@styleoverflowauto[.='True']">
                                overflow:auto;
                              </xsl:if>-->
                      </xsl:attribute>
                      <table width="100%" cellspacing="2" cellpadding="2" border="1">
                        <tr>
                          <td>
                            <xsl:variable name="itemset"  select="user:evaluate($ListTarget)" />
                            <!--<xsl:value-of select="$itemset"/>-->

                            <tr>
                              <xsl:for-each select="$itemset/listhead/*">
                                <td>
                                  <b>
                                    <xsl:value-of select="text()" />
                                  </b>
                                </td>
                              </xsl:for-each>
                            </tr>

                            <xsl:for-each select="$itemset/option">
                              <tr>
                                
                                <xsl:for-each select="node()">
                                  
                                    <xsl:variable name="currentnode" select="concat('|',name(),'|')"/>
                                    <xsl:choose>
                                      <xsl:when test="$HideNodes!=''">
                                        <xsl:if test="not(contains($HideNodes,$currentnode)) and (name()!='')">
                                          <td>
                                            <xsl:choose>
                                              <xsl:when test=".=''">
                                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:value-of select="text()" />
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </td>
                                        </xsl:if>
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <td>
                                          <!--<xsl:choose>-->
                                          <!--<xsl:when test="@width">
                                                <xsl:attribute name="width">
                                                  <xsl:value-of select="@width" />%
                                                </xsl:attribute>-->
                                          <xsl:value-of select="text()" />
                                          <!--</xsl:when>-->
                                          <!--<xsl:otherwise>
                                                <xsl:attribute name="width">
                                                  <xsl:value-of select="$width" />%
                                                </xsl:attribute>
                                                <xsl:value-of select="text()" />
                                              </xsl:otherwise>-->
                                          <!--</xsl:choose>-->
                                        </td>
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  
                                </xsl:for-each>
                              </tr>
                            </xsl:for-each>

                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </xsl:when>
						<xsl:otherwise>
							<td>
								<xsl:choose>
									<xsl:when test="@required[.='yes']">
                    <td valign = "top">
                      <!--bkumar33:Cosmetic change-->
                      <b>
                        <u>
                          <xsl:value-of select="@title" />
                        </u>
                      </b>
                    </td>
									</xsl:when>
									<!--Nitesh resolved bug no. 000723-->
									<xsl:when test="@type[.='textlabel']">
										<xsl:value-of select="text()" separator="" />
									</xsl:when>
									<xsl:otherwise>
										
										<xsl:if test="@type[.!='radio'] and @type[.!='imagebuttonscript'] and @type[.!='imagebutton']">
                      <td valign = "top"> <!--bkumar33:Cosmetic change-->
                        <xsl:value-of select="@title" />
                      </td>
										</xsl:if>
										
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td>
								<xsl:choose>
									<xsl:when test="@type[.='text' or .='numericIntOnly']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
										<input type="hidden">
											<xsl:attribute name="name">
												<xsl:value-of select="@name" />
											</xsl:attribute>
											<xsl:attribute name="value"></xsl:attribute>
										</input>
									</xsl:when>
									<xsl:when test="@type[.='hidden']">
										<input type="hidden">
											<xsl:attribute name="name">
												<xsl:value-of select="@name" />
											</xsl:attribute>
											<xsl:attribute name="value">
												<xsl:value-of select="text()" />
											</xsl:attribute>
										</input>
									</xsl:when>
									<xsl:when test="@type[.='textml']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='code']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
                  <!-- code added by yatharth-->
                  <xsl:when test="@type[.='readonly'] ">
                    <xsl:choose>
                      <xsl:when test="@formatas[.='date']">
                        <xsl:variable name="itemsetref" select="@ref" />
                        <xsl:variable name="valnode">
                          <xsl:value-of  select="user:evaluate($itemsetref)" />
                        </xsl:variable>
                        <xsl:choose>
                          <xsl:when test="string($valnode)!=''">
                            <!--<xsl:call-template name="dt:format-date-time">
													                <xsl:with-param name="year" select="number(substring($valnode,1,4))" />
													                <xsl:with-param name="month" select="number(substring($valnode,5,2))" />
													                <xsl:with-param name="day" select="number(substring($valnode,7,2))" />
													                <xsl:with-param name="hour" select="12" />
													                <xsl:with-param name="minute" select="0" />
													                <xsl:with-param name="second" select="0" />
													                <xsl:with-param name="format" select="'%m/%d/%Y'" />
												                </xsl:call-template>-->
                            <xsl:value-of  select="user:formatdate(substring($valnode,1,4),substring($valnode,5,2),substring($valnode,7,2))" />
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of  select="user:evaluate($itemsetref)" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>
                      <xsl:when test="@formatas[.='time']">
                        <xsl:variable name="itemsetref" select="@ref" />
                        <xsl:variable name="valnode">
                          <xsl:value-of  select="user:evaluate($itemsetref)" />
                        </xsl:variable>
                        <xsl:choose>
                          <xsl:when test="string($valnode)!=''">
                            <!--<xsl:call-template name="dt:format-date-time">
													          <xsl:with-param name="hour" select="number(substring($valnode,1,2))" />
													          <xsl:with-param name="minute" select="number(substring($valnode,3,2))" />
													          <xsl:with-param name="second" select="number(substring($valnode,5,2))" />
													          <xsl:with-param name="format" select="'%i:%M %P'" />
												          </xsl:call-template>-->
                            <xsl:value-of  select="user:formattime($itemsetref)" />
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of  select="user:evaluate($itemsetref)" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>
                      <!-- PJS Added for datetime-->
                      <xsl:when test="@formatas[.='datetime']">
                        <xsl:variable name="itemsetref" select="@ref" />
                        <xsl:variable name="valnode">
                          <xsl:value-of  select="user:evaluate($itemsetref)" />
                        </xsl:variable>
                        <xsl:choose>
                          <xsl:when test="string($valnode)!=''">
                            <xsl:value-of  select="user:formatdatetime($valnode)" />
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of  select="user:evaluate($itemsetref)" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:variable name="itemsetref" select="@ref" />
                        <xsl:value-of  select="user:evaluate($itemsetref)" />
                      </xsl:otherwise>
                    </xsl:choose>
                    </xsl:when>
                  <!-- code ended by yatharth-->
									<xsl:when test="@type[.='readonly']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='lookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
                  <!--Added by Amitosh for new addresslookup control-->
                  <xsl:when test="@type[.='addresslookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
				  <!--Added by ajohari2 RMA-15779 for new claimantentitylookup control-->
				   <xsl:when test="@type[.='claimantentitylookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
				  
									<!--Added by averma62 for new catastrophe code-->
                  <xsl:when test="@type[.='sitelookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
									<xsl:when test="@type[.='catastrophelookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='freecode']">
										<xsl:variable name="itemsetref" select="@ref" />
                    <!--Bijender Has added For Mits : 14316-->
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                    <!--End For Mits : 14316-->
									</xsl:when>
									<xsl:when test="@type[.='date']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:variable name="valnode">
											<xsl:value-of  select="user:evaluate($itemsetref)" />
										</xsl:variable>
										<xsl:choose>
                      <!--MGaba2:MITS 16781:If value is already coming in a date format,no need to again format it-->
											<!--<xsl:when test="string($valnode)!=''">-->
                      <xsl:when test="string($valnode)!='' and not(contains(string($valnode),'/'))">
												<!--<xsl:call-template name="dt:format-date-time">
													<xsl:with-param name="year" select="number(substring($valnode,1,4))" />
													<xsl:with-param name="month" select="number(substring($valnode,5,2))" />
													<xsl:with-param name="day" select="number(substring($valnode,7,2))" />
													<xsl:with-param name="hour" select="12" />
													<xsl:with-param name="minute" select="0" />
													<xsl:with-param name="second" select="0" />
													<xsl:with-param name="format" select="'%m/%d/%Y'" />
												</xsl:call-template>-->
                        <xsl:value-of  select="user:formatdate(substring($valnode,1,4),substring($valnode,5,2),substring($valnode,7,2))" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of  select="user:evaluate($itemsetref)" />
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<!--Nitesh resolved bug no. 000723 Starts-->
									<xsl:when test="@type[.='datebuttonscript']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:variable name="valnode">
											<xsl:value-of  select="user:evaluate($itemsetref)" />
										</xsl:variable>
										<xsl:choose>
											<xsl:when test="string($valnode)!=''">
												<!--<xsl:call-template name="dt:format-date-time">
													<xsl:with-param name="year" select="number(substring($valnode,1,4))" />
													<xsl:with-param name="month" select="number(substring($valnode,5,2))" />
													<xsl:with-param name="day" select="number(substring($valnode,7,2))" />
													<xsl:with-param name="hour" select="12" />
													<xsl:with-param name="minute" select="0" />
													<xsl:with-param name="second" select="0" />
													<xsl:with-param name="format" select="'%m/%d/%Y'" />
												</xsl:call-template>-->
                        <xsl:value-of  select="user:formatdate(substring($valnode,1,4),substring($valnode,5,2),substring($valnode,7,2))" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of  select="user:evaluate($itemsetref)" />
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<!--Nitesh resolved bug no. 000723 Ends-->
									<xsl:when test="@type[.='time']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:variable name="valnode">
											<xsl:value-of  select="user:evaluate($itemsetref)" />
										</xsl:variable>
										<xsl:choose>
											<xsl:when test="string($valnode)!=''">
												<!--<xsl:call-template name="dt:format-date-time">
													<xsl:with-param name="hour" select="number(substring($valnode,1,2))" />
													<xsl:with-param name="minute" select="number(substring($valnode,3,2))" />
													<xsl:with-param name="second" select="number(substring($valnode,5,2))" />
													<xsl:with-param name="format" select="'%i:%M %P'" />
												</xsl:call-template>-->
                        <xsl:value-of  select="user:formattime($itemsetref)" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of  select="user:evaluate($itemsetref)" />
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:when test="@type[.='orgh']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='label']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='memo']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
										<!--<P /> bkumar33:cosmetic change no 48 -->
									</xsl:when>
                  <xsl:when test="@type[.='htmltext']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
									<!--<xsl:when test="@type[.='checkbox']"><xsl:choose><xsl:when test="text()[.='True']">Yess</xsl:when><xsl:otherwise><xsl:value-of select="."></xsl:value-of>Noo</xsl:otherwise></xsl:choose>
				</xsl:when>-->
									<xsl:when test="@type[.='checkbox']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:variable name="valnode">
											<xsl:value-of  select="user:evaluate($itemsetref)" />
										</xsl:variable>
										<xsl:choose>
										<!--akaushik5 Changed for RMA-6830 Starts-->
										<!--<xsl:when test="string($valnode)='True'">Yes</xsl:when>-->
                      <!--vgupta79 Changed for RMA-9498 Starts-->
										<xsl:when test="string($valnode)='True' or (string($valnode) ='-1') or (string($valnode) ='1')">Yes</xsl:when>
                      
                      <!--akaushik5 Changed for RMA-6830 Ends-->
											<xsl:otherwise>No</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:when test="@type[.='radio']">									
									</xsl:when>
                  <!--Changed by Milli MITS 18839 ComboBox data was not appearing on Record Summary Start-->
                  <!-- Commented by payal for RMA: 11842 Starts-->
                  <!--<xsl:when test="@type[.='combobox']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:variable name="itemset"  select="user:evaluate($itemsetref)" />
                    <xsl:for-each select="option">
                      <xsl:if test="@value=$itemset">
                        <xsl:value-of select="text()" />
                      </xsl:if>
                    </xsl:for-each>
                  </xsl:when>--><!-- Commented by payal for RMA: 11842 Ends--><!--Milli End-->
									<xsl:when test="@type[.='space']">
										
									</xsl:when>
									<xsl:when test="@type[.='controlgroup']">
										<xsl:apply-templates select="control" />
									</xsl:when>
									<xsl:when test="@type[.='zip']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='phone']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
                  <!--aahuja21 MITS 31616  Start-->
                  <xsl:when test="@type[.='phonetype']">
                    <xsl:variable name="itemsetref" select="@textref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <!--aahuja21 MITS 31616  End-->
                  <!--igupta3 JIRA RMA-301  Start-->
                  <xsl:when test="@type[.='taxid']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <!--igupta3 JIRA RMA-301 End-->
									<xsl:when test="@type[.='ssn']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
                  <!--MGaba2:Added a case for printing Record Id in Record Summary-->
									<xsl:when test="@type[.='numeric' or .='numericWithNoDataChange']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='currency']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:variable name="valnode">
											<xsl:value-of  select="user:evaluate($itemsetref)" />
										</xsl:variable>
										<xsl:choose>
											<xsl:when test="string($valnode)!=''">
												<!--<xsl:call-template name="currency:format">
													<xsl:with-param name="amount" select="$valnode" />
													<xsl:with-param name="format" select="'AMT'" />
												</xsl:call-template>-->
                        <xsl:value-of  select="user:formatcurrency($valnode)" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of  select="user:evaluate($itemsetref)" />
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
                  <!--patiententitylookup control added by Shivendu for MITS 22492-->
                  <xsl:when test="@type[.='patiententitylookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='entitylookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='employeelookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='eidlookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='claimnumberlookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='eventnumberlookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<!-- Nitesh,14Jan2006 :- bug 1330 resolved:  Starts-->
									<xsl:when test="@type[.='eventlookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<!-- Start:Nitin Goel,01/29/2010,MITS: 18230 Added code to handle the Property Lookup for Property Claim Management.-->
									<xsl:when test="@type[.='propertylookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<!-- End:Nitin Goel,01/29/2010,MITS: 18230-->
									<!--Start:Added by Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373-->
									<xsl:when test="@type[.='SuppTypePolicyManagementLookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<!--Rename the name of Policy Number Lookup to Policy Tracking Lookup,05/05/2010,MITS 20618-->
									<!--<xsl:when test="@type[.='SuppTypePolicyLookup']">-->
									    <xsl:when test="@type[.='SuppTypePolicyTrackingLookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>

									<!--End:Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373-->
                  <!--npadhy MITS 15514 Added code to handle the Pay Plan Lookup for Policy Management-->
                  <xsl:when test="@type[.='payplanlookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <!--npadhy MITS 15514 Added code to handle the Billing Rule Lookup for Policy Management-->
                  <xsl:when test="@type[.='billingrulelookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <!--npadhy MITS 15514 Added code to handle the Policy Lookup Control for Policy Management-->
                  <xsl:when test="@type[.='enhpolicylookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of select="user:evaluate($itemsetref)" />
                  </xsl:when>
									<xsl:when test="@type[.='codewithdetail']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='textlabel']"></xsl:when>
									<!-- Nitesh,14Jan2006 :- bug 1330 resolved:  Ends-->
									<xsl:when test="@type[.='vehiclenumberlookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
                  <!--Amitosh for Mits 24247 (03/08/2011)-->
                  <xsl:when test="@type[.='textpercent']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />%
                  </xsl:when>                        
                  <!--end Amitosh-->
									<xsl:when test="@type[.='policynumberlookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<!--rahul: start for Mits 25351 (07/06/2011)-->
                  <xsl:when test="@type[.='multipolicylookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										 <xsl:value-of select="user:toCsv($itemsetref)"/>
									  </xsl:when>
									    <xsl:when test="@type[.='multiunitlookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										 <xsl:value-of select="user:toCsv($itemsetref)"/>
									  </xsl:when>
                  <!--rahul: end -->
									<xsl:when test="@type[.='policylookup']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='codelist']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluateCodes($itemsetref)" />
                    <!-- Aman ML Change-->
									</xsl:when>
                  <!-- code modified yatharth  START-->
									<xsl:when test="@type[.='entitylist']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:variable name="itemset"  select="user:evaluate($itemsetref)" />                   
                    <xsl:for-each select="$itemset/Item">
                        <xsl:value-of select="user:getValue($itemsetref)" />
                      <!-- Aman ML Change-->
                      <br/> 
                    </xsl:for-each>
                    <!--	<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />-->
                    
                  </xsl:when>
                  <!-- yatharth END-->
									<xsl:when test="@type[.='policysearch']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:value-of  select="user:evaluate($itemsetref)" />
									</xsl:when>
									<xsl:when test="@type[.='labelonly']">
										<xsl:choose>
											<xsl:when test="@ref">
												<xsl:choose>
													<xsl:when test="@ref[.!='']">
                            <xsl:variable name="itemsetref" select="@ref" />
														<!--<xsl:variable name="valuenodepath" select="concat('document(''input:instance'')/',./@ref)" />-->
														<xsl:variable name="label-value"  select="user:evaluate($itemsetref)" />
														<xsl:value-of select="$label-value" />
													</xsl:when>
													<xsl:otherwise>
                            <xsl:value-of select="text()" separator="" />
													</xsl:otherwise>
												</xsl:choose>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="text()" separator="" />
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:when test="@type[.='labelwithref']">
										<xsl:choose>
											<xsl:when test="@ref">
												<xsl:choose>
													<xsl:when test="@ref[.!='']">
														<xsl:variable name="valuenodepath" select="@ref" />
														<xsl:variable name="label-value"  select="user:evaluate($valuenodepath)" />
														<xsl:if test="$label-value[.='True']">
															Yes
														</xsl:if>
														<xsl:if test="$label-value[.='False']">
															No
														</xsl:if>
														
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="text()" separator="" />
													</xsl:otherwise>
												</xsl:choose>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="text()" separator="" />
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
                  <xsl:when test="@type[.='ZapatecGrid']">
                    <!--Bijender Has added For Mits : 14316-->
                    <table border="1px" style="border:solid black 1px" >
                      <!--End Mits : 14316-->
                      <tr bgcolor="#6fb3e7" >
                        <xsl:call-template name='splitHeader'>
                          <xsl:with-param name="names" select="@Headers"/>
                        </xsl:call-template>
                      </tr>
                      <tr>
                        <xsl:call-template name='splitdata'>
                          <xsl:with-param name="names" select="@Data"/>
                        </xsl:call-template>
                      </tr>
                    </table>
                  </xsl:when>
                  <!--avipinsrivas start : Worked for JIRA - 13951 (Epic 7767 and Story 13196)-->
									<xsl:when test="@type[.='blank']">
                  </xsl:when>
									<xsl:when test="@type[.='combobox']">
										<xsl:variable name="itemsetref" select="@ref" />
										<xsl:variable name="itemset"  select="user:evaluate($itemsetref)" />
                    <!--<xsl:variable name="itemsetrefpath"  select="@itemsetref" />-->
                    <xsl:variable name="itemsetrefpath"  select="user:evaluate(@itemsetref)" />
                    
                    
                    <xsl:choose>
                      <xsl:when test="$itemsetrefpath/option">
                        <xsl:for-each select="$itemsetrefpath/option">
											    <xsl:if test="@value=$itemset">
												    <xsl:value-of select="text()" />
											    </xsl:if>
										    </xsl:for-each>
                      </xsl:when>
                      
                      <xsl:otherwise>
                        <xsl:variable name="itemrefpath"  select="user:evaluate(substring-before(@itemsetref, '/option'))" />
                        <xsl:for-each select="$itemrefpath/option">
											    <xsl:if test="@value=$itemset">
												    <xsl:value-of select="text()" />
											    </xsl:if>
										    </xsl:for-each>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <!--avipinsrivas end-->
                  <xsl:when test="@type[.='pientitylookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <xsl:when test="@type[.='customizedlistlookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <xsl:when test="@type[.='employeelastnamelookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <xsl:when test="@type[.='vehiclelookup' or .='SuppTypeVehicleLookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <xsl:when test="@type[.='banklookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <!--Mits 16658:Asif Start-->
                  <xsl:when test="@type[.='codewithdes']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <xsl:when test="@type[.='codedeslist']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <xsl:when test="@type[.='planlookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <!--Mits 16658:Asif End-->
                  <!--MITS 22128: Yatharth Start-->
                  <xsl:when test="@type[.='leaveplanlookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <xsl:when test="@type[.='javascript']">
                  </xsl:when>
                  <xsl:when test="@type[.='imagebuttonscript']">
                  </xsl:when>
                  <xsl:when test="@type[.='imagebutton']">
                  </xsl:when>
                  <xsl:when test="@type[.='claimadjusterlookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <xsl:when test="@type[.='UserLookup']">
                    <xsl:variable name="itemsetref" select="@ref" />
                    <xsl:value-of  select="user:evaluate($itemsetref)" />
                  </xsl:when>
                  <!-- MITS 22128: End-->
                  <xsl:otherwise>Unknown Control Type<!-- <xsl:variable name="itemsetref" select="@ref" /><xsl:value-of  select="user:evaluate($itemsetref)"/> -->
				          </xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</tr>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="//control/@type[.='controlgroup']/control">
		<xsl:choose>
			<xsl:when test="@type[.='labelwithref']">
				<xsl:choose>
					<xsl:when test="@ref">
						<xsl:choose>
							<xsl:when test="@ref[.!='']">
								<xsl:variable name="valuenodepath" select="concat('document(''input:instance'')/',./@ref)" />
								<xsl:variable name="label-value"  select="user:evaluate($valuenodepath)" />
								<xsl:if test="$label-value[.='True']">
									Yes
								</xsl:if>
								<xsl:if test="$label-value[.='False']">
									No
								</xsl:if>
								
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="text()" separator="" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="text()" separator="" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="internal">
		<xsl:choose>
			<xsl:when test="@type[.='hidden']">
				<input type="hidden">
					<xsl:attribute name="name">
						<xsl:value-of select="@name" />
					</xsl:attribute>
					<xsl:attribute name="value">
						<xsl:value-of select="@value" />
					</xsl:attribute>
				</input>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="form">
		<xsl:value-of select="@title" />
	</xsl:template>
	<xsl:template match="error">
		<li class="errtext">
			<xsl:if test="@number[.='10000']"><img src="img/locked.gif" width="12" height="15" title="Security Message" border="0" />&#160;&#160;</xsl:if>
			<xsl:variable name="itemsetref" select="@ref" />
			<xsl:value-of  select="user:evaluate($itemsetref)" />
		</li>
	</xsl:template>

  <xsl:template name="splitHeader">
    <xsl:param name="names"/>
    <xsl:variable name="first" select='substring-before($names,"|")'/>
    <xsl:variable name='rest' select='substring-after($names,"|")'/>

    <xsl:if test='$first'>
      <th>
        <FONT COLOR="white" >
          <xsl:value-of select='$first'/>
        </FONT>
      </th>
    </xsl:if>

    <xsl:if test='$rest'>

      <xsl:call-template name='splitHeader'>
        <xsl:with-param name='names' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>

      <th>
        <FONT COLOR="white" >
          <xsl:value-of select='$names'/>
        </FONT>
      </th>
    </xsl:if>
  </xsl:template>

  <xsl:template name="splitRow">
    <xsl:param name="names"/>
    <xsl:variable name="first" select='substring-before($names,"|")'/>
    <xsl:variable name='rest' select='substring-after($names,"|")'/>

    <!--Bijender Has added For Mits : 14316-->    
      <td >
        <xsl:value-of select='$first'/>        
      </td>          
    
    <xsl:if test="contains($rest,'|')">
      <xsl:call-template name='splitRow'>
        <xsl:with-param name='names' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    
    <xsl:if test="not(contains($rest,'|'))">
      <td >
        <xsl:value-of select='$rest'/>        
      </td>
      <!--End Mits : 14316-->
    </xsl:if>
  </xsl:template>

  <xsl:template name="splitdata">
    <xsl:param name="names"/>
    <xsl:variable name="first" select='substring-before($names,";")'/>
    <xsl:variable name='rest' select='substring-after($names,";")'/>
   <tr>
      <xsl:call-template name='splitRow'>
        <xsl:with-param name="names" select="$first"/>
      </xsl:call-template>
    </tr>
    <xsl:if test='$rest'>
      <xsl:call-template name='splitdata'>
        <xsl:with-param name='names' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>


</xsl:stylesheet>