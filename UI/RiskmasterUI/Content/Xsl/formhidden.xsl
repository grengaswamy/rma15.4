<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:template match="/">

	<form name="frmData">
			<xsl:for-each select="form/group">
				<xsl:apply-templates select="displaycolumn/control" />
			</xsl:for-each>
		<xsl:apply-templates select="//internal"/>
		<input type="hidden" name="sys_formname"><xsl:attribute name="value"><xsl:for-each select="form"><xsl:value-of select="@name"/></xsl:for-each></xsl:attribute></input>
	</form>
</xsl:template>


  
<xsl:template match="control">
  

<xsl:choose>
	<xsl:when test="@type[.='id']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value">
    <xsl:value-of select="text()"/>
  </xsl:attribute></input></xsl:when>
	<xsl:otherwise>
		<xsl:choose>
			<xsl:when test="@type[.='text']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='hidden']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='textml']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='code']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_codelookup</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			<input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_codelookup_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='codewithdetail']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			<input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='freecode']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='date']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='time']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='orgh']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			<input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='label']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='memo']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='checkbox']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='zip']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='numeric']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='currency']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='entitylookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			<input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='eidlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			<input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_cid</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute></input>
			</xsl:when>
      
      <!-- xsl:when test="@type[.='codelist']">
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
         		<xsl:attribute name="value">
						<xsl:apply-templates select="option">
							<xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
         			</xsl:apply-templates>
         		</xsl:attribute>
				</input>
			</xsl:when>
			<xsl:when test="@type[.='entitylist']">
				<input type="hidden"><xsl:attribute name="name"><xsl:value-of select="@name"/>_lst</xsl:attribute>
         		<xsl:attribute name="value">
						<xsl:apply-templates select="option">
							<xsl:template><xsl:copy><xsl:apply-templates select="@* | * | comment() | pi() | text()"/></xsl:copy></xsl:template>
         			</xsl:apply-templates>
         		</xsl:attribute>
				</input>
			</xsl:when -->
      
			<xsl:when test="@type[.='policylookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='claimnumberlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='eventnumberlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='vehiclenumberlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='employeelookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='eventlookup']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='buttonscript']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='phone']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='ssn']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
			<xsl:when test="@type[.='href']">
			</xsl:when>
			<xsl:when test="@type[.='readonly']"><input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute></input>
			</xsl:when>
      <xsl:when test="@type[.='customizedlistlookup']">
        <input type="hidden"><xsl:attribute name="id"><xsl:value-of select="@name"/>_rmsyslookup</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
        </input>
        <input type="hidden">
          <xsl:attribute name="id"><xsl:value-of select="@name"/>_rmsyslookup_cid</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="@codeid"/></xsl:attribute>
        </input>
      </xsl:when>
			<xsl:otherwise>Unknown Element Type: <xsl:value-of select="@type"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="internal">
	<xsl:choose>
		<xsl:when test="@type[.='hidden']">
			<input type="hidden"><xsl:attribute name="id">sys_<xsl:value-of select="@name"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></input>
		</xsl:when>
	</xsl:choose>
</xsl:template>
</xsl:stylesheet>