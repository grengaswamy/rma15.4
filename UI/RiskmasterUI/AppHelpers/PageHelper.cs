﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Collections.Specialized;


    public class PageHelper
    {
        /// <summary>
        /// Gets a handle to a Pages Client Script Manager
        /// </summary>
        /// <param name="objPage">reference handle to the current Page instance</param>
        /// <returns>ClientScriptManager for managing scripts in the page</returns>
        public static ClientScriptManager GetClientScriptManager(Page objPage)
        {
            return objPage.ClientScript;
        } // method: ClientScriptManager


        /// <summary>
        /// Gets the Submit Script required for registering on a Page
        /// </summary>
        public static string SubmitScript
        {
            get
            {
                string strSubmitScript = "<script>window.opener.document.forms[0].txtSubmit.value = \"SUBMIT\";window.opener.document.forms[0].submit();</script>";
                return strSubmitScript;
            }
        } // property SubmitScript

        #region Browser Information methods
        /// <summary>
        /// Gets a NameValue configuration section from the Web.config file
        /// </summary>
        /// <param name="strSectionName">string containing the name of ths specified
        /// custom configuration section</param>
        /// <returns>NameValueCollection containing all of the settings in the configuration section</returns>
        public static NameValueCollection GetNameValueSection(string strSectionName)
        {
            NameValueCollection nvCollSection = ConfigurationManager.GetSection(strSectionName) as NameValueCollection;

            return nvCollSection;

        }//method: GetNameValueSection

        /// <summary>
        /// Gets a NameValueCollection of all supported browser types
        /// exclusive of Internet Explorer
        /// </summary>
        /// <returns>NameValueCollection containing all supported browser types</returns>
        internal static NameValueCollection GetConfigurableBrowserTypes()
        {
            string IE_EXCLUSIONS = "IE";
            NameValueCollection nvCollSection = GetNameValueSection("SupportedBrowserTypes");
            NameValueCollection nvSupportedBrowsers = new NameValueCollection();

            foreach (string strBrowser in nvCollSection.Keys)
            {
                //Remove any instances of Internet Explorer
                //as supported additional browsers
                if (!strBrowser.Contains(IE_EXCLUSIONS))
                {
                    nvSupportedBrowsers.Add(strBrowser, strBrowser);
                }//if
            }//foreach

            return nvSupportedBrowsers;
        }//method: GetConfigurableBrowserTypes


        /// <summary>
        /// Gets a list of all currently supported browsers
        /// </summary>
        /// <returns>NameValueCollection containing all of the currently supported browser platforms</returns>
        internal static NameValueCollection GetSupportedBrowserTypes()
        {
            NameValueCollection nvSupportedBrowsers = new NameValueCollection();
            const string MOZILLA_FIREFOX_VERSION = "Firefox3.6";

            //Build the string for Firefox Supported Browser Information
            Version versionFirefox = GetFirefoxBrowserVersion(MOZILLA_FIREFOX_VERSION);
            string strFirefoxVersionInfo = string.Format("{0}.{1}", versionFirefox.Major, versionFirefox.Minor);


            nvSupportedBrowsers.Add("IE7", "Internet Explorer 7");
            nvSupportedBrowsers.Add("IE8", "Internet Explorer 8");
            nvSupportedBrowsers.Add("IE9", "Internet Explorer 9");
            nvSupportedBrowsers.Add(MOZILLA_FIREFOX_VERSION, string.Format("Mozilla Firefox {0}", strFirefoxVersionInfo));

            return nvSupportedBrowsers;
        }//method: GetSupportedBrowserTypes()

        /// <summary>
        /// Gets the Version information for Firefox browsers
        /// </summary>
        /// <param name="strBrowserType">string containing the Browser Type information</param>
        /// <returns>instance of a Version object which can be used to retrieve required Version elements</returns>
        internal static Version GetFirefoxBrowserVersion(string strBrowserType)
        {
            const string MOZILLA_FIREFOX_BROWSER_NAME = "Firefox";
            int intBrowserNameLen = MOZILLA_FIREFOX_BROWSER_NAME.Length;

            //Get the Firefox Browser Version
            string strBrowserInfo = strBrowserType.Substring(intBrowserNameLen, strBrowserType.Length - intBrowserNameLen);

            //Cast the Firefox Browser Version information into a Version object
            Version versionInfo = new Version(strBrowserInfo);

            return versionInfo;
        }//method: GetFirefoxBrowserVersion();


        /// <summary>
        /// Verifies if the browser that the client is using
        /// matches one of the browsers supported by RISKMASTER X
        /// </summary>
        /// <param name="strBrowserType">string containing the client browser version</param>
        /// <returns>boolean indicating whether or not the browser is supported</returns>
        public static bool IsSupportedBrowser(string strBrowserType)
        {
            bool blnIsSupportedBrowser = false;
            NameValueCollection nvCollSupportedBrowsers = GetSupportedBrowserTypes();

            foreach (string strBrowser in nvCollSupportedBrowsers.Keys)
            {
                //Verify that the current browser type is in the list of supported browsers
                if (strBrowserType.Contains(strBrowser))
                {
                    blnIsSupportedBrowser = true;
                    break;
                }
            }

            return blnIsSupportedBrowser;
        } // method: IsSupportedBrowser

        /// <summary>
        /// Gets the client's User Agent String
        /// to determine the browser name and version
        /// that the client is using
        /// </summary>
        /// <returns>browser user agent string containg
        /// abbreviation for browser name and browser version</returns>
        /// <remarks>IE = Internet Explorer
        /// Opera = Opera
        /// Firefox = Mozilla Firefox
        /// </remarks>
        /// <example>IE6 = Internet Explorer 6
        /// IE7 = Internet Explorer 7
        /// IE8 = Internet Explorer 8
        /// IE9 = Internet Explorer 9
        /// Firefox3.0.8 = Mozilla Firefox 3.08
        /// Firefox3.5.2 = Mozilla Firefox 3.5.2
        /// Desktop = Apple Safari or Google Chrome
        /// Opera9 = Opera 9.xxx</example>
        public static string GetClientBrowserType()
        {
            return HttpContext.Current.Request.Browser.Type;
        } // method: GetClientBrowserVersion

        /// <summary>
        /// Gets browser version information for the user's current browser
        /// </summary>
        /// <returns>string indicating the full version information for the user's current browser</returns>
        public static string GetClientBrowserVersion()
        {
            return HttpContext.Current.Request.Browser.Version;
        }//method: GetClientBrowserVersion 
        #endregion
    }
