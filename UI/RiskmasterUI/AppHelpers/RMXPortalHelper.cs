﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Xml.Linq;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Xml;
using System.Net;
using System.Runtime.Serialization;
using Riskmaster.Models;
using Riskmaster.Cache;

namespace Riskmaster.AppHelpers
{
    /// <summary>
    /// Handles all functions required for managing the RMXPortal
    /// </summary>
    public class RMXPortalHelper
    {

        #region Members
        private static Infragistics.WebUI.UltraWebTab.UltraWebTab mPortal;
        private static int iPortalNum = 0;

        #endregion // Members


        #region Portal

        /// <summary>
        /// Gets the current Portal Configuration
        /// either from the Cache or from a new instance
        /// </summary>
        private static XDocument PortalConfiguration
        {
            get
            {
                string cacheKey = "RMXPortalSettings";
                object cacheItem = CacheCommonFunctions.RetreiveValueFromCache<object>(cacheKey, AppHelper.ClientId);// HttpContext.Current.Cache[cacheKey] as XDocument;
                if (cacheItem == null)
                {
                    //string strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                    //string HomePage = PortalClient.ContentInfo(strSql);
                    PortalData oPortalData = new PortalData();
                    oPortalData.Sql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_PORTALINFO'";
                    oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    string HomePage = AppHelper.GetResponse<string>("RMService/Portal/ContentInfo", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 
                    if (string.IsNullOrEmpty(HomePage.Trim()))
                    {
                        //rsolanki2: init run; we will ened to upload the settings to sesssion db from Hompage.xml file in filesystem 
                        string sPortalConfigPagePath = "~/App_Data/Customization.xml";

                        // file presence check here 
                        sPortalConfigPagePath = HttpContext.Current.Server.MapPath(sPortalConfigPagePath);
                        XDocument xmlPortalConfigPage = XDocument.Load(sPortalConfigPagePath);

                        // upload to DB
                        oPortalData.Content = xmlPortalConfigPage.Root.ToString();
                        oPortalData.FileName = "CUSTOMIZE_PORTALINFO";
                        oPortalData.Token = AppHelper.Token;
                        AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 
                        //PortalClient.ContentInsert(xmlPortalConfigPage.Root.ToString(), "CUSTOMIZE_PORTALINFO");
                        cacheItem = xmlPortalConfigPage.Root.Value;
                        //rename the file to something else.
                        System.IO.File.Move(sPortalConfigPagePath, sPortalConfigPagePath + ".bak");
                        //HttpContext.Current.Cache.Insert(cacheKey, cacheItem);
                        CacheCommonFunctions.AddValue2Cache<object>(cacheKey, AppHelper.ClientId, cacheItem);
                        return xmlPortalConfigPage;

                    }
                    else
                    {
                        XDocument doc = XDocument.Parse(HomePage);
                        //PSARIN2 START:FIXED for R8 Perf Improvement:Caching was not working
                        //cacheItem = doc.Root.Value;
                        cacheItem = doc;
                        //PSARIN2 END:FIXED for R8 Perf Improvement:Caching was not working
                        //HttpContext.Current.Cache.Insert(cacheKey, cacheItem);
                        CacheCommonFunctions.AddValue2Cache<object>(cacheKey, AppHelper.ClientId, cacheItem);
                        return doc;
                    }

                }
                else
                {
                    return (XDocument)cacheItem;
                }


            }//get
        } //property: PortalConfiguration
        public static byte[] PortalHomeImage
        {
            get
            {
                string cacheimageKey = "RMXPortalImage";
                byte[] cacheItem = CacheCommonFunctions.RetreiveValueFromCache<byte[]>(cacheimageKey, AppHelper.ClientId);//HttpContext.Current.Cache[cacheKey] as string;
                if (cacheItem == null || cacheItem.Length == 0)
                {
                    PortalData oPortalData = new PortalData();
                    oPortalData.Sql = "SELECT FILE_CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_HOMEPAGE'";
                    oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    byte[] filecontent = AppHelper.GetResponse<byte[]>("RMService/Portal/filecontent", AppHelper.HttpVerb.POST, "application/json", oPortalData);
                    CacheCommonFunctions.AddValue2Cache<byte[]>(cacheimageKey, AppHelper.ClientId, filecontent);
                }
                return cacheItem;
            }
        }
        /// <summary>
        /// Gets the current Portal Home Page
        /// either from the Cache or from a new instance
        /// </summary>
        public static string PortalHomePage
        {
            get
            {
                string cacheKey = "RMXPortalHomePage";
                string cacheimageKey = "RMXPortalImage";
                string cacheItem = CacheCommonFunctions.RetreiveValueFromCache<string>(cacheKey, AppHelper.ClientId);//HttpContext.Current.Cache[cacheKey] as string;
                if (string.IsNullOrEmpty(cacheItem))
                {
                    //PortalServiceClient PortalClient = new PortalServiceClient();
                    //string strSql = "SELECT CONTENT FROM SETTINGS WHERE UPPER(NAME)='HOMEPAGE'";
                    //string strSql = "SELECT CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_HOMEPAGE'";
                    //string HomePage = PortalClient.ContentInfo(strSql);
                    PortalData oPortalData = new PortalData();
                    oPortalData.Sql = "SELECT CONTENT,FILE_CONTENT FROM CUSTOMIZE WHERE UPPER(FILENAME)='CUSTOMIZE_HOMEPAGE'";
                    oPortalData.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    PortalData oReturn  = AppHelper.GetResponse<PortalData>("RMService/Portal/homeinfo", AppHelper.HttpVerb.POST, "application/json", oPortalData);
                    string HomePage = oReturn.Content;
                    if (string.IsNullOrEmpty(HomePage))
                    {
                        //rsolanki2: init run; we will ened to upload the settings to sesssion db from Hompage.xml file in filesystem 
                        string sPortalHomePagePath = "~/App_Data/HomePage.xml";

                        // file presence check here 
                        sPortalHomePagePath = HttpContext.Current.Server.MapPath(sPortalHomePagePath);
                        XDocument xmlHomePage = XDocument.Load(sPortalHomePagePath);

                        // upload to DB
                        oPortalData.Content = xmlHomePage.Root.ToString();
                        oPortalData.FileName = "CUSTOMIZE_HOMEPAGE";
                        oPortalData.Token = AppHelper.Token;
                        AppHelper.GetResponse("RMService/Portal/Content", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPortalData); 
                        //PortalClient.ContentInsert(xmlHomePage.Root.ToString(), "CUSTOMIZE_HOMEPAGE");

                        //XDocument doc = XDocument.Parse(HomePage);
                        cacheItem = xmlHomePage.Root.Value;

                        //rename the file to something else.
                        System.IO.File.Move(sPortalHomePagePath, sPortalHomePagePath + ".bak");

                    }
                    else
                    {
                        XDocument doc = XDocument.Parse(HomePage);
                        cacheItem = doc.Root.Value;
                    }

                    //HttpContext.Current.Cache.Insert(cacheKey, cacheItem);

                    CacheCommonFunctions.AddValue2Cache<object>(cacheKey, AppHelper.ClientId, cacheItem);
                    CacheCommonFunctions.AddValue2Cache<byte[]>(cacheimageKey, AppHelper.ClientId, oReturn.FileContent);
                }

         
                return cacheItem;

                //string portalHomePagePath = "~/App_Data/HomePage.xml";
                //string cacheItem = HttpContext.Current.Cache[cacheKey] as string;
                //if (string.IsNullOrEmpty(cacheItem))
                //{
                //    XDocument xmlPortalConfigPage = XDocument.Load(HttpContext.Current.Server.MapPath(portalHomePagePath));
                //    cacheItem = xmlPortalConfigPage.Element("HomePage").Value.ToString();
                //    HttpContext.Current.Cache.Insert(cacheKey, cacheItem, new CacheDependency(HttpContext.Current.Server.MapPath(portalHomePagePath)));
                //}//if
                //return cacheItem.ToString();

            }//get
        } // property PortalHomePage

        /// <summary>
        /// Builds portal using the Customization.xml in App_Data
        /// </summary>
        /// <param name="uwtPortal"></param>
        public static void BuildPortal(ref Infragistics.WebUI.UltraWebTab.UltraWebTab uwtPortal, ref Dictionary<int,string> dictPortalTabs)
        {
            String sLoginName = String.Empty;
            const string RISKMASTER_PORTLET = "RISKMASTER";

            XDocument xmlPortalConfig = PortalConfiguration;

            // Generate tabs for "All Users"
            foreach (XElement Portlet in xmlPortalConfig.Element("PortalSettings").Element("AllUsersPortlets").Elements())
            {

                //Set the default Url based on Http Host Headers rather than what is present in Customization.xml
                if (Portlet.Attribute("name").Value.Equals(RISKMASTER_PORTLET))
                {
                    Portlet.Value = GetMDIPortalUrl();
                } // if

                // Generate new tab for each portlet
                uwtPortal.Tabs.Add(CreateTab(Portlet, ref dictPortalTabs));
            } // for

            // Generate specific user customized tabs

            sLoginName = HttpContext.Current.User.Identity.Name.ToString();
            XElement xmlIndividualPortlets = xmlPortalConfig.Element("PortalSettings").Element("IndividualPortletsInfo");

            // Build array of porlet id's for current user
            foreach (var vPortlets in xmlPortalConfig.Element("PortalSettings").Elements("Portlets"))
            {
                if (vPortlets.HasAttributes && vPortlets.Attribute("LoginName").Value.ToString().ToLower().Equals(sLoginName.ToLower()))
                {
                    foreach (var vPortletId in vPortlets.Elements())
                    {
                        uwtPortal.Tabs.Add(CreateCustomTab(vPortletId.Value.ToString(), xmlIndividualPortlets, ref dictPortalTabs));
                    } // foreach

                } // if

            } // for

            iPortalNum = 0;
        } // method: BuildPortal

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sPortletId"></param>
        /// <param name="xmlIndividualPortlets"></param>
        private static Infragistics.WebUI.UltraWebTab.Tab CreateCustomTab(string sPortletId, XElement xmlIndividualPortlets, ref Dictionary<int,String> dictPT)
        {
            return CreateTab(xmlIndividualPortlets.Elements().Where(e => e.Attribute("id").Value.ToString() == sPortletId).FirstOrDefault(),ref dictPT);

        } // method: AddCustomTab


        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlPortletInfo"></param>
        /// <param name="dictPT"></param>
        /// <returns></returns>
        private static Infragistics.WebUI.UltraWebTab.Tab CreateTab(XElement xmlPortletInfo, ref Dictionary<int,String> dictPT)
        {

            string sPortletName = xmlPortletInfo.Attribute("name").Value.ToString();
            string sDestinationUrl = xmlPortletInfo.Value.ToString().Trim();

            // MITS 16414 MAC - Adds each id and URL to a dictionary, later used to create hidden
            // variables in the main Portal page.  These hidden variables will then reference
            // each tabs destination when clicked
            dictPT.Add(iPortalNum,sDestinationUrl);
            iPortalNum++;

            // Create the tab and set it's attributes
            Infragistics.WebUI.UltraWebTab.Tab newPortlet = new Infragistics.WebUI.UltraWebTab.Tab();
            newPortlet.Text = sPortletName;

            if (xmlPortletInfo.Attribute("MainRMXPortlet") == null || xmlPortletInfo.Attribute("MainRMXPortlet").Value.ToString().ToLower() == "false")
            {
                newPortlet.AsyncOption = Infragistics.WebUI.UltraWebTab.AsyncTabOption.LoadOnDemand;

            }
            else
            {
                newPortlet.AsyncOption = Infragistics.WebUI.UltraWebTab.AsyncTabOption.Excluded;

                // MITS 16414 MAC - Only set TargetURL for RMX tab, all else will be set in javascript
                newPortlet.ContentPane.TargetUrl = sDestinationUrl;

            }

            newPortlet.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden;
            newPortlet.SelectedStyle.CssClass = "Portal_SelectedBody";

            newPortlet.ContentPane.BorderStyle = BorderStyle.None;
            newPortlet.ContentPane.BorderWidth = Unit.Pixel(0);

            return newPortlet;


        } // method: AddTab

        /// <summary>
        /// Gets the fully qualified MDI Portal Url
        /// </summary>
        /// <returns>string containing the fully qualified MDI Portal Url</returns>
        internal static string GetMDIPortalUrl()
        {
            string strPortalUrl = string.Empty;

            //Raman 12/02/2009 : We can do with relative url here. dont need to generate entire url
            //avipinsrivas start : Worked for JIRA - 16319
            strPortalUrl = string.Format("{0}/{1}", TranslatePortalUrl(), @"RiskmasterUI/MDI/Default.aspx");

            //strPortalUrl = string.Format(@"/RiskmasterUI/MDI/Default.aspx");
            //avipinsrivas end
            return strPortalUrl;
        } // method: GetMDIPortalUrl


        /// <summary>
        /// Translates the Base Portal Url
        /// </summary>
        /// <returns>string containing the base Portal Url</returns>
        internal static string TranslatePortalUrl()
        {
            const string SERVER_INSECURE = "0";  //Insecure = 0, Secure = 1
            const string DEFAULT_HTTP_PORT = "80";
            const string DEFAULT_HTTP_PREFIX = "http://";
            const string DEFAULT_HTTPS_PREFIX = "https://";
            const string DEFAULT_HTTPS_PORT = "443";

            string strServerName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            string strBaseUrl = string.Empty;
            string strServerPort = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            string strServerPortSecure = HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];


            if (strServerPortSecure.Equals(SERVER_INSECURE))
            {
                if (strServerPort.Equals(DEFAULT_HTTP_PORT))
                {
                    strBaseUrl = string.Format("{0}{1}{2}", DEFAULT_HTTP_PREFIX, strServerName, string.Empty);
                }//if
                else
                {
                    strBaseUrl = string.Format("{0}{1}:{2}", DEFAULT_HTTP_PREFIX, strServerName, strServerPort);
                }//else
            }//if
            else
            {
                if (strServerPort.Equals(DEFAULT_HTTPS_PORT))
                {
                    strBaseUrl = string.Format("{0}{1}", DEFAULT_HTTPS_PREFIX, strServerName);
                }//if
                else
                {
                    strBaseUrl = string.Format("{0}{1}:{2}", DEFAULT_HTTPS_PREFIX, strServerName, strServerPort);
                }//else
            }//else

            return strBaseUrl;
        } // method: TranslatePortalUrl


        #endregion // Portal

        #region Get Colors

        #region Get System.Drawing Colors
        public static Color GetGDIColorFromHtml(string strHtmlColor)
        {
            return ColorTranslator.FromHtml(strHtmlColor);
        }//method: GetGDIColorFromHtml

		//zmohammad MITs 34734
        public static Color GetForgotPasswordFontColorAsObj()
        {
            return GetGDIColorFromHtml(GetForgotPasswordColorAsHTML());
        } 


        public static Color GetLogoutFontColorAsObj()
        {
            return GetGDIColorFromHtml(GetLogoutFontColorAsHTML());
        } // method: GetLogoutFontColorAsColorObj

        public static Color GetLoginLabelFontColorAsObj()
        {
            return GetGDIColorFromHtml(GetLoginLabelFontColorAsHTML());
        } // method: GetLoginLabelFontColorAsColorObj

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Color GetLoginValueFontColorAsObj()
        {
            return GetGDIColorFromHtml(GetLoginValueFontColorAsHTML());
        } // method: GetLoginValueFontColorAsObj 
        #endregion


        #region GetHTMLColors
        public static string GetHtmlPortalColor(string strElementName)
        {
            string sReturnHTMLColor = string.Empty;
			//zmohammad MITs 34734
            XElement sPortalTheme = PortalConfiguration.Element("PortalSettings").Element("PortalTheme");
            if (sPortalTheme.Element(strElementName) != null)
                sReturnHTMLColor = sPortalTheme.Element(strElementName).Value.ToString();

            return sReturnHTMLColor;
        }//method: GetHtmlPortalColor()

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetBannerColorAsHTML()
        {
            return GetHtmlPortalColor("BackgroundColor");

        } // method: GetBannerColorAsHTML

        public static string GetLoginLabelFontColorAsHTML()
        {
            return GetHtmlPortalColor("LoginLabelFontColor");
        }

        public static string GetLoginValueFontColorAsHTML()
        {
            return GetHtmlPortalColor("LoginValueFontColor");
        }//method: GetLoginValueFontColorAsHtml()

        public static string GetLogoutFontColorAsHTML()
        {
            return GetHtmlPortalColor("LogoutFontColor");
        }//method: GetLogoutFontColorAsHTML()

		//zmohammad MITs 34734
        public static string GetForgotPasswordColorAsHTML()
        {
            return GetHtmlPortalColor("ForgotPasswordColor");
        }
        #endregion

        #endregion // Get Colors

        #region RMXLSSSettings
        //Debabrata Biswas Single 06/01/2010 Sign On MITS# MITS 21124 
        //Method implemented to Show/Hide RMX-LSS Single sign on link
        public static bool ShowRMXLSSSingleSignOnLink()
        {
            return IsRMXLSSSingleSignOnEnabled("ShowRMXLSSSingleSignOnLink");
        }//method: ShowRMXLSSSingleSignOnLink()

        //Debabrata Biswas Single 06/01/2010 Sign On MITS# MITS 21124 
        //Method implemented to Show/Hide RMX-LSS Single sign on link
        public static bool IsRMXLSSSingleSignOnEnabled(string strElementName)
        {
            bool bReturnRMXLSSSingleSignOnEnabled = false;
            string sRMXLSSSettingsValue = string.Empty;

            if (PortalConfiguration.Element("PortalSettings").Element("RMXLSSSettings").Element(strElementName) != null)
                sRMXLSSSettingsValue = PortalConfiguration.Element("PortalSettings").Element("RMXLSSSettings").Element(strElementName).Value.ToString();
            
            if(sRMXLSSSettingsValue=="")
                bReturnRMXLSSSingleSignOnEnabled = false;
            else if(sRMXLSSSettingsValue.ToUpper()=="TRUE")
                bReturnRMXLSSSingleSignOnEnabled = true;
            else
                bReturnRMXLSSSingleSignOnEnabled = false;

            return bReturnRMXLSSSingleSignOnEnabled;
        }//method: IsRMXLSSSingleSignOnEnabled()
        #endregion
       
    } // class RMXPortalUtility
}
