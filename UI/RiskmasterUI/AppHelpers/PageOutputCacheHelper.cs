﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Web;
using System.Timers;
using System.Text;
using Riskmaster.Models;
using System.Collections;
//using Riskmaster.UI.PageCacheService;

namespace Riskmaster.AppHelpers
{
    public class PageOutputCacheHelper
    {
        System.Timers.Timer m_Timer = new System.Timers.Timer();
        //Hashtable m_Clients = new Hashtable();
        Dictionary<string, DatabaseTableTimestamps> m_DSN2Timestamp = null;// new Dictionary<string, DatabaseTableTimestamps>();
        int m_PollingInterval = 300;
        public delegate void AsyncPageOutputCache(string iClientId);
        public PageOutputCacheHelper()
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["PageCachePollingInterval"]))
            {
                m_PollingInterval = int.Parse(ConfigurationManager.AppSettings["PageCachePollingInterval"]);
                if (m_PollingInterval < 60)
                    m_PollingInterval = 60;
            }

            //m_Timer.Interval = m_PollingInterval * 1000;
            //m_Timer.Elapsed += new ElapsedEventHandler(CheckDependencyHandler);
            ////m_Timer.Elapsed += new ElapsedEventHandler((sender, e) => CheckDependencyHandler(sender, e, AppHelper.ClientId));
            //m_Timer.Start();
        }

        public string GetCustomParamValue(HttpContext context, string custom)
        {
            StringBuilder sbParams = new StringBuilder();
            string sTimeStamp = string.Empty;
            DatabaseTableTimestamps oTimestamp = null;
            int iClientId = AppHelper.ClientId;
            //Get database id and page name
            string sDsnIDKey = AppHelper.ReadCookieValue("DsnId");
            //sDsnIDKey = sDsnIDKey + "_" + AppHelper.ClientId;
            m_DSN2Timestamp = Riskmaster.Cache.CacheCommonFunctions.RetreiveValueFromCache<Dictionary<string, DatabaseTableTimestamps>>("m_DSN2Timestamp", iClientId);
            if (m_DSN2Timestamp == null) m_DSN2Timestamp = new Dictionary<string, DatabaseTableTimestamps>();
            if (m_DSN2Timestamp.Keys.Contains(sDsnIDKey))
                oTimestamp = m_DSN2Timestamp[sDsnIDKey];

            if (oTimestamp == null)
            {
                oTimestamp = new DatabaseTableTimestamps(sDsnIDKey, iClientId);
                m_DSN2Timestamp.Add(sDsnIDKey, oTimestamp);
                Riskmaster.Cache.CacheCommonFunctions.AddValue2Cache<Dictionary<string, DatabaseTableTimestamps>>("m_DSN2Timestamp", iClientId, m_DSN2Timestamp);
            }

            string sPageName = AppHelper.GetPageName(context.Request.Url.ToString()).ToLower();
            foreach (string key in context.Request.QueryString.Keys)
            {
                sbParams.Append(string.Format("{0}={1};", key, context.Request.QueryString[key]));
            }

            sbParams.Append(string.Format("dsnid={0};timestamp={1};clientid={2}", sDsnIDKey, oTimestamp.GetPageTimestamp(sPageName), iClientId));
            string sKeyValue =Riskmaster.Cache.CacheCommonFunctions.RetreiveValueFromCache<string>("Clients", 0);
            if (string.IsNullOrEmpty(sKeyValue))
            {
                Riskmaster.Cache.CacheCommonFunctions.AddValue2Cache<string>("Clients", 0, iClientId.ToString());
                InvokePageCache(iClientId.ToString());
            }
            else if (!sKeyValue.Contains(iClientId.ToString()))
            {
                sKeyValue += "," + iClientId;
                Riskmaster.Cache.CacheCommonFunctions.UpdateValue2Cache<string>("Clients", 0, sKeyValue);
                InvokePageCache(sKeyValue);
            }
            return sbParams.ToString();
        }
        /// <summary>
        /// Invoke Page Cache
        /// </summary>
        /// <param name="sClientIds"></param>
        private void InvokePageCache(string sClientIds)
        {
            AsyncPageOutputCache dDelegate = new AsyncPageOutputCache(BeginPageCacheCheck);
            dDelegate.BeginInvoke(sClientIds, new AsyncCallback(EndPageCacheCheck), null);
        }
        /// <summary>
        /// Begin Check
        /// </summary>
        /// <param name="sClientId"></param>
        private void BeginPageCacheCheck(string sClientId)
        {
            if (m_Timer.Enabled == true)
            {
                m_Timer.Stop();
                m_Timer.Close(); 
                m_Timer.Dispose();
            }
            m_Timer = new System.Timers.Timer();
            m_Timer.Interval = (m_PollingInterval / sClientId.Split(',').Count<string>()) * 1000;
            //m_Timer.Elapsed += new ElapsedEventHandler(CheckDependencyHandler);
            m_Timer.Elapsed += new ElapsedEventHandler((sender, e) => CheckDependencyHandler(sender, e, sClientId));
            m_Timer.Start();
        }
        /// <summary>
        /// On end check
        /// </summary>
        /// <param name="ar"></param>
        private void EndPageCacheCheck(IAsyncResult ar)
        {
        }
        /// <summary>
        /// Timer handler to be called periodically to retrieve the latest time stamp from database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckDependencyHandler(object sender, ElapsedEventArgs e,string sClientIds)
        {
            string[] sCIds = sClientIds.Split(',');
            foreach (string sId in sCIds)
            {
                m_DSN2Timestamp = Riskmaster.Cache.CacheCommonFunctions.RetreiveValueFromCache<Dictionary<string, DatabaseTableTimestamps>>("m_DSN2Timestamp", int.Parse(sId));
                foreach (DatabaseTableTimestamps oTimeStamps in m_DSN2Timestamp.Values)
                {
                    oTimeStamps.GetTimeStamps(int.Parse(sId));
                }
            }
        }
    }

    /// <summary>
    /// This class will have method to retrieve time stamp for tables, such as CODES, SEARCH_VIEW. These time
    /// stamp values will be used to determine if the cached page is obsolete or not.
    /// </summary>
    internal class DatabaseTableTimestamps
    {
        int m_iDataSourceId;
        string m_sConnectionString = string.Empty;
        Dictionary<string, string> m_TableTimestamps = new Dictionary<string, string>();
        XDocument m_TableConfig = null;

        public DatabaseTableTimestamps(string sDataSourceId,int iClientId)
        {
            m_iDataSourceId = int.Parse(sDataSourceId);
            PageData oPageData = null;
            //Retrieve the connectionstring for this database which can be reused late
            //PageCacheServiceClient oClient = new PageCacheServiceClient();
            try
            {
                m_TableConfig = XDocument.Load(HttpContext.Current.Server.MapPath("~/App_Data") + "\\PageOutputCache.xml");
                oPageData = new PageData();
                oPageData.ClientId = iClientId;
                oPageData.DsnId = m_iDataSourceId;
                // m_sConnectionString = oClient.GetConnectionstringFromDSNId(iDataSourceId);
                m_sConnectionString = AppHelper.GetResponse<string>("RMService/Page/connkeybydsnid", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPageData);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if(oPageData!= null)
                  oPageData = null;
                //if (oClient != null)
                //    oClient.Close();
            }

            //Retrive the initial timestamps
            GetTimeStamps(iClientId);
        }

        /// <summary>
        /// Get time stamps for each page, which can be used to determine if a cache is still valid
        /// </summary>
        public void GetTimeStamps(int iClientId)
        {
            PageData oPageData = null;
            //Retrieve the connectionstring for this database which can be reused late
            //PageCacheServiceClient oClient = new PageCacheServiceClient();
            try
            {
                oPageData = new PageData();
                oPageData.ClientId = iClientId;
                oPageData.ConnKey = m_sConnectionString;
                oPageData.TableListConfig = m_TableConfig.ToString();
                //m_TableConfig = XDocument.Parse(oClient.GetTableTimestamps(m_sConnectionString, m_TableConfig.ToString()));
                if(HttpContext.Current == null)
                    m_TableConfig = XDocument.Parse(AppHelper.GetResponse<string>(Common.CommonFunctions.RetrieveBaseURL() + "/RMService/Page/tabletimestamps", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPageData));
                else
                    m_TableConfig = XDocument.Parse(AppHelper.GetResponse<string>("RMService/Page/tabletimestamps", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oPageData));

            }
            catch (Exception ex)
            {

            }
            finally
            {
                oPageData = null;
                //if (oClient != null)
                //    oClient.Close();
            }
        }


        /// <summary>
        /// Get the time stamp for a page
        /// </summary>
        /// <param name="sPageName">page name, such as, quicklookup.aspx</param>
        /// <returns></returns>
        public string GetPageTimestamp(string sPageName)
        {
            string sTimestamp = string.Empty;
            XElement oPage = m_TableConfig.XPathSelectElement(string.Format("//page[@name='{0}']", sPageName));
            if (oPage != null)
            {
                sTimestamp = oPage.Attribute("timestamp").Value;
            }

            return sTimestamp;
        }
    }
}