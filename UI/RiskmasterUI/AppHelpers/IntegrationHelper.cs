﻿// All Common UI helper functions would be written here
using System;
using System.Globalization;
using System.Resources;
using System.Collections;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Configuration;
internal class IntegrationHelper
{
    public static string GetTomcatRelativePage(int id)
    {
        string servername=ConfigurationManager.AppSettings["tomcatserver"].ToString();
        string page="";
        switch(id)
        {
            case 1:
                page="home?pg=riskmaster/DocumentManagement/file-listing";
                break;
            default:
                break;
        }
        string finalpage = servername + page;
        return finalpage;
    }
}
