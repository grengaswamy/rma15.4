﻿using System;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.RMXResourceManager;
using System.Configuration;
using System.Collections.Generic;
using Riskmaster.Models;

// This Call would help convert UI related errors to a format which our common ErrorControl UserControl wants 
public class ErrorHelper
{
    public const string LOG_CATEGORY_PRESENTATIONLAYER = "PresentationLayer";
    public const string LOG_CATEGORY_DEFAULT = "Default";

    public static void IsRMXSessionExists()
    {
        if (HttpContext.Current.Session["SessionId"] == null)
        {
            HttpContext.Current.Response.Redirect("~/UI/Shared/SessionExpired.aspx");
            //throw new Exception(Globalization.GetString("Riskmaster.Presentation.SessionExpired"));
        }
    }
   
    /// <summary>
    /// Handles the specified Exception using the Enterprise Library
    /// Exception Handling Block
    /// </summary>
    /// <param name="ex">instance of the Exception thrown by a method or event</param>
    public static void ExceptionHandler(Exception ex)
    {
        //Simply log the exception
        if (AppHelper.ClientId == 0)
        {
            bool rethrow = ExceptionPolicy.HandleException(ex, "Logging Policy");
        }
        else
        {
            Logging oLogging = new Logging();
            oLogging.EventId = 1001;
            oLogging.Message = ex.Message;
            oLogging.Category = LOG_CATEGORY_PRESENTATIONLAYER;
            oLogging.Priority = 0;
            oLogging.ClientId = AppHelper.ClientId;
            AppHelper.GetResponse("/RMService/LogMessage/write", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oLogging);
        }
        //TODO: Determine whether or not to rethrow the exception to allow it 
        //TODO: to propagate up the call stack
        //if (rethrow)
        //{
        //    throw;
        //} // if


    } // method: ExceptionHandler


    /// <summary>
    /// Wrapper method for the Enterprise Library Exception Handling
    /// Application Block
    /// </summary>
    /// <param name="ex">instance of the Exception thrown by a method or event</param>
    public static void logErrors(Exception exc)
    {
        //Call the method to handle Exceptions
        //using the Enterprise Library ExceptionHandling Block
        ExceptionHandler(exc);
    }
    public static void AddErrors(Exception exc)
    {

    }
    public static string FormatErrorsForUI(string Errors)
    {
        string errorhandle = "";
        errorhandle = @"<font class='warntext'>Following message(s) reported:</font><table cellpadding='0' cellspacing='0' border='0' class='errortext'>" +
                  "<tr>" +
                    "<td valign='top'>" +
                        "<img src='" + AppHelper.GetApplicationRootPath() + "/Images/error-large.gif'/>" +
                    "</td>" +
                    "<td valign='top' style='padding-left: 1em'>" +
                      "<ul>";
        errorhandle = errorhandle + "<li class='warntext'>" + Errors + "</li>";

        errorhandle += "</ul></td></tr></table>";
        return errorhandle;
    }
    public static string FormatErrorsForUI(string Errors , bool bIsError)
    {
        if (bIsError)
        {
            string errorhandle = "";
            errorhandle = @"<font class='errortext'>Following Errors have been reported</font><table cellpadding='0' cellspacing='0' border='0' class='errortext'>" +
                      "<tr>" +
                        "<td valign='top'>" +
                            "<img src='" + AppHelper.GetApplicationRootPath() + "/Images/error-large.gif'/>" +
                        "</td>" +
                        "<td valign='top' style='padding-left: 1em'>" +
                          "<ul>";
            errorhandle = errorhandle + "<li class='warntext'>" + Errors + "</li>";

            errorhandle += "</ul></td></tr></table>";
            return errorhandle;
        }
        else
        {
            return FormatErrorsForUI(Errors);
        }
        
    }

    public static string FormatServiceErrors(string Errors)
    {
        bool bIsSucess = false;
        return FormatServiceErrors(Errors, ref bIsSucess);
    }
    public static string FormatServiceErrors(string Errors, ref bool bIsSuccess)
    {
        string errorhandle = "";
        string ErrorMsg = string.Empty; //add ttumula2 RMA-7055
        bIsSuccess = false;
        if (Errors != null && Errors.Trim() != "")
        {
            XmlDocument objDoc = new XmlDocument();
            objDoc.LoadXml(Errors);

            if ((objDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
            {
                bIsSuccess = true;
                XmlNodeList oNodeList = objDoc.SelectNodes("//ExtendedStatus");
                if( oNodeList != null)
                {
                    if( oNodeList.Count > 0 )
                    {
                        errorhandle = @"<font class='warntext'>Following message(s) reported:</font><table cellpadding='0' cellspacing='0' border='0' class='errortext'>" +
                                  "<tr>" +
                                    "<td valign='top'>" +
                                        "<img src='" + AppHelper.GetApplicationRootPath() + "/Images/error-large.gif'/>" +
                                    "</td>" +
                                    "<td valign='top' style='padding-left: 1em'>" +
                                      "<ul>";
                        foreach (XmlNode node in objDoc.SelectNodes("//ExtendedStatus"))
                        {
                            errorhandle = errorhandle + "<li class='warntext'>" + UpdateErrorMessage(node.SelectSingleNode("ExtendedStatusDesc").InnerText) + "</li>";
                        }
                        errorhandle += "</ul></td></tr></table>";
                    }
                }
            }
            else
            {
                if (objDoc.SelectSingleNode("//ExtendedMsgType") != null)
                {
                    if ((objDoc.SelectSingleNode("//ExtendedMsgType").InnerText == "Warning") ||
                        (objDoc.SelectSingleNode("//ExtendedMsgType").InnerText == "Error") ||
                        (objDoc.SelectSingleNode("//ExtendedMsgType").InnerText == "SystemError"))
                    {
                        ErrorMsg = Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("ErrorHelper.ErrorList"); //add ttumula2 RMA-7055 
                        // errorhandle = @"<font class='errortext'>Following Errors have been reported:</font><table cellpadding='0' cellspacing='0' border='0' class='errortext'>" + //add ttumula2 RMA-7055
                        errorhandle = @"<font class='errortext'>" + ErrorMsg + "</font><table cellpadding='0' cellspacing='0' border='0' class='errortext'>" +
                                  "<tr>" +
                                    "<td valign='top'>" +
                                      "<img src='" + AppHelper.GetApplicationRootPath() + "/Images/error-large.gif'/>" +
                                    "</td>" +
                                    "<td valign='top' style='padding-left: 1em'>" +
                                      "<ul>";
                        foreach (XmlNode node in objDoc.SelectNodes("//ExtendedStatus"))
                        {//Condition added by Amitosh for Mits 24446 (03/29/2011)
                            if ((node.SelectSingleNode("ExtendedMsgType").InnerText == "Error") ||
                                (node.SelectSingleNode("ExtendedMsgType").InnerText == "Message") ||
                                (node.SelectSingleNode("ExtendedMsgType").InnerText == "Warning") ||
                                (node.SelectSingleNode("//ExtendedMsgType").InnerText == "SystemError"))
                            {
                                errorhandle = errorhandle + "<li class='warntext'>" + UpdateErrorMessage(node.SelectSingleNode("ExtendedStatusDesc").InnerText) + "</li>";
                            }
                        }
                        errorhandle += "</ul></td></tr></table>";

                       
                    }
                    else
                    {
                        if ((objDoc.SelectSingleNode("//ExtendedMsgType").InnerText == "PopupMessage"))
                        {
                            errorhandle = "<script language='javascript'>alert('";
                            foreach (XmlNode node in objDoc.SelectNodes("//ExtendedStatus/ExtendedMsgType[.='PopupMessage']/parent::node()"))
                            {
                                //Mona
                                //if ((node.SelectSingleNode("ExtendedMsgType").InnerText == "Warning"))
                                //{
                                errorhandle = errorhandle + UpdateErrorMessage(node.SelectSingleNode("ExtendedStatusDesc").InnerText) + @"\n";
                                //}
                            }
                            errorhandle += "')</script>";
                        }
                    }
                }
            }
        }
        return errorhandle;
    }
    /// <summary>
    /// Update the error message according to the language code
    /// </summary>
    /// <param name="p_sKeyValue"></param>
    /// <returns></returns>
    public static string UpdateErrorMessage(string p_sKeyValue)
    {
        string[] sDataSeparator = { "~*~" };
        string[] ArrDataSeparator = { "|^|" };
        string[] ArrErrMsg = p_sKeyValue.Split(sDataSeparator, StringSplitOptions.None);
        string sReturnMsg = string.Empty;
        string sKeyId = string.Empty;
        try
        {
            if (!p_sKeyValue.Contains("|^|"))
            {
                return p_sKeyValue;
            }
            string sShowErrCode = RMXResourceProvider.IsShowErrorCode();
            string sLangCode = AppHelper.GetLanguageCode();
            if (sLangCode.Length != 4 ) sLangCode = AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString();
            foreach (string s in ArrErrMsg)
            {
                if (s.Contains(sLangCode))
                {
                    sReturnMsg = s;
                    break;
                }
            }
            if (string.IsNullOrEmpty(sReturnMsg))
            {
                sLangCode = AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString();
                foreach (string s in ArrErrMsg)
                {
                    if (s.Contains(sLangCode))
                    {
                        sReturnMsg = s;
                        break;
                    }
                }
            }
            string[] sLangSeparator = { sLangCode };
            if (sShowErrCode.ToUpper() == "TRUE")
            {
                string[] sArr = sReturnMsg.Split(sLangSeparator, StringSplitOptions.None);
                if (sArr.Length > 1)
                {
                    for (int i = 0; i < sArr.Length - 1; i++)
                    {
                        sKeyId = sKeyId + AppendErrorCode(sArr[i].ToString());
                    }
                    sReturnMsg = sArr[sArr.Length - 1];
                }
                sReturnMsg = (sKeyId + ": " + sReturnMsg).Replace("|^|", "");
            }
            else
            {
                string[] sArr = sReturnMsg.Split(sLangSeparator, StringSplitOptions.None);
                if (sArr.Length > 1)
                {
                    sKeyId = sArr[0].Replace("|^|", "");
                    sReturnMsg = sArr[sArr.Length - 1].Replace("|^|", "");
                }
            }
        }
        catch (Exception ee)
        {

        }
        return (!string.IsNullOrEmpty(sKeyId) ? sReturnMsg : p_sKeyValue);
    }
    /// <summary>
    /// Append Error Code to the Resource Value
    /// </summary>
    /// <param name="sKeyID"></param>
    /// <returns></returns>
    private static string AppendErrorCode(string sKeyID)
    {
        if (string.IsNullOrEmpty(sKeyID))
        {
            return string.Empty;
        }
        else
        {
            sKeyID = sKeyID.Replace("|^|", "");
            sKeyID = "rmA-" + sKeyID + " ";
            return sKeyID;
        }
    }
    /// <summary>
    /// Checks whether permission violation exceptions have been thrown
    /// </summary>
    /// <param name="Errors"></param>
    /// <returns></returns>
    public static bool PermissionViolationErrors(string Errors , out bool p_bBackButton)
    {
        bool bPermissionViolation = false;
        p_bBackButton = false;
        if (Errors != null && Errors.Trim() != "")
        {
            XmlDocument objDoc = new XmlDocument();
            objDoc.LoadXml(Errors);

            XmlNode oNode = objDoc.SelectSingleNode("//ExtendedStatus/ExtendedStatusCd");
            if (oNode != null && (oNode.InnerText.Contains("PermissionFailure") || oNode.InnerText.Contains("LimitError")))
            {
                bPermissionViolation = true;
                XmlNode oErrorDesc = objDoc.SelectSingleNode("//ExtendedStatus/ExtendedStatusDesc");
                if (oErrorDesc != null)
                {
                    //TO DO: find a better solution than string compares..
                    //Raman Bhatia 05/05/2009
                    /*
                    switch (oErrorDesc.InnerText.ToLower())
                    {
                        case "no create permission.": p_bBackButton = false; break;
                        case "no update permission.": p_bBackButton = true; break;
                        case "no view permission.": p_bBackButton = false; break;
                        case "no delete permission.": p_bBackButton = true; break;
                        default: p_bBackButton = false; break;
                    }
                    */
                    string sErrortext = oErrorDesc.InnerText.ToLower();
                    if (sErrortext.Contains("no update permission") || sErrortext.Contains("no delete permission")) p_bBackButton = true;
                    else p_bBackButton = false;

                }
            }

        }
        return bPermissionViolation;
    }

    /// <summary>
    /// Provides a Facade for the display of BusinessAdapter Error
    /// messages in the User Interface
    /// </summary>
    /// <param name="ex">Exception which is thrown by the User Interface</param>
    /// <returns>string containing the formatted error output</returns>
    public static string DisplayBusinessAdapterErrors(Exception ex)
    {
        BusinessAdaptorErrors errors = new BusinessAdaptorErrors();
        string strErrorMsgs = string.Empty;


        errors.Add(ex, BusinessAdaptorErrorType.SystemError);

        //Get the formatted error output
        strErrorMsgs = formatUIErrorXML(errors);

        //Clean up
        errors = null;

        return strErrorMsgs;
    } // method: DisplayBusinessAdapterErrors


    public static string  formatUIErrorXML(BusinessAdaptorErrors errors)
    {
        XmlElement xmlElement = null;
        XmlElement xmlMsgsRoot = null;

        // Create error root element (MsgStatus)
        XmlDocument xmlDoc = new XmlDocument();
        XmlElement xmlErrRoot = xmlDoc.CreateElement("MsgStatus");
        xmlDoc.AppendChild(xmlErrRoot);

        xmlElement = xmlDoc.CreateElement("MsgStatusCd");
        xmlElement.InnerText = "Error";

        xmlErrRoot.AppendChild(xmlElement);

        if (errors != null)
        {
            foreach (BusinessAdaptorError err in errors)
            {
                // ... add individual error messages/warnings (if no errors, there will still be an empty ExtendedStatus element)
                xmlMsgsRoot = xmlDoc.CreateElement("ExtendedStatus");
                xmlErrRoot.AppendChild(xmlMsgsRoot);

                if (err.oException == null)
                {  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
                    // ... add error code/number
                    xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                    xmlElement.InnerText = err.ErrorCode;
                    xmlMsgsRoot.AppendChild(xmlElement);

                    // ... add error description
                    xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                    xmlElement.InnerText = err.ErrorDescription;
                    xmlMsgsRoot.AppendChild(xmlElement);
                }  // non-Exception case
                else
                {  // Exception case
                    // Determine error code - assembly + exception type
                    xmlElement = xmlDoc.CreateElement("ExtendedStatusCd");
                    xmlElement.InnerText = err.oException.Source + "." + err.oException.GetType().Name;
                    xmlMsgsRoot.AppendChild(xmlElement);

                    // ... add error description
                    xmlElement = xmlDoc.CreateElement("ExtendedStatusDesc");
                    if (err.ErrorDescription != "")
                        xmlElement.InnerText = err.ErrorDescription;
                    else
                        xmlElement.InnerText = err.oException.Message;

                    xmlMsgsRoot.AppendChild(xmlElement);
                }  // Exception case

                // ...add error type
                xmlElement = xmlDoc.CreateElement("ExtendedMsgType");
                switch (err.ErrorType)
                {
                    case BusinessAdaptorErrorType.SystemError:
                        xmlElement.InnerText = "SystemError";
                        break;
                    case BusinessAdaptorErrorType.Error:
                        xmlElement.InnerText = "Error";
                        break;
                    case BusinessAdaptorErrorType.Warning:
                        xmlElement.InnerText = "Warning";
                        break;
                    case BusinessAdaptorErrorType.Message:
                        xmlElement.InnerText = "Message";
                        break;
                    case BusinessAdaptorErrorType.PopupMessage:
                        xmlElement.InnerText = "PopupMessage";
                        break;
                    default:
                       break;
                };
                xmlMsgsRoot.AppendChild(xmlElement);
            }
        }
        return xmlDoc.OuterXml;
    }

    /// <summary>
    /// Check if the call to the cws is success or not
    /// </summary>
    /// <param name="sReturn"></param>
    /// <returns></returns>
    static public bool IsCWSCallSuccess(string sReturn)
    {
        if (string.IsNullOrEmpty(sReturn))
            return false;

        XElement oReturn = XElement.Parse(sReturn);
        XElement oMsgStatusCode = oReturn.XPathSelectElement("//MsgStatusCd");
        if (oMsgStatusCode != null)
        {
            if (string.Compare(oMsgStatusCode.Value, "error", true) == 0)
            {
                return false;
            }
        }

        return true;
    }


    /// <summary>
    /// Check if any error happens during the call to the cws
    /// </summary>
    /// <param name="sReturn"></param>
    /// <returns></returns>
    static public bool IsAnyErrorInCWSCall(string sReturn)
    {
        if (string.IsNullOrEmpty(sReturn))
            return false;

        XElement oReturn = XElement.Parse(sReturn);
        XElement oMsgStatusCode = oReturn.XPathSelectElement("//ExtendedMsgType[.='Error']");
        if (oMsgStatusCode != null)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Log just a message with a timestamp MITS 24745
    /// </summary>
    /// <param name="sMessage"></param>
    static public void Write(string sMessage)
    {

        if (!string.IsNullOrEmpty(sMessage))
        {
            if (AppHelper.ClientId == 0)
            {
                // Creates and fills the log entry with user information
                LogEntry log = new LogEntry();
                log.EventId = 1001;
                log.Message = sMessage;
                log.Categories.Add(LOG_CATEGORY_PRESENTATIONLAYER);
                log.Priority = 0;

                // Writes the log entry.
                Logger.Write(log);
            }
            else
            {
                Logging oLogData = new Logging();
                oLogData.EventId = 1001;
                oLogData.Message = sMessage;
                oLogData.Category = LOG_CATEGORY_PRESENTATIONLAYER;
                oLogData.Priority = 0;
                oLogData.ClientId = AppHelper.ClientId;
                AppHelper.GetResponse("/RMService/Logs", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oLogData);
            }
        }
    }
}