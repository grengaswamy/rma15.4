﻿/**********************************************************************************************
 *   Date     |  JIRA#   | Programmer | Description                                            *
 **********************************************************************************************
 * 11/25/2014 | RMA-345  | achouhan3   | New Search Result ML Changes 
 * 02/13/2015 | RMA-6405 | vchouhan6   | Added page for Async CLaim Activity log
 * 05/04/2015|RMA-5566   | achouhan3  | Added Reserve/Worksheet Approval page for ML Support
 **********************************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Resources;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Compilation;
using System.Xml.Linq;
//using Riskmaster.UI.RMXResourceService;
using Riskmaster.Models;
 

namespace Riskmaster.RMXResourceManager
{
    /// <summary>
    /// Created By: Deb Jena
    /// Date:04/24/2012
    /// </summary>
    public class RMXResourceProvider : DisposableBase, IResourceProvider
    {
        #region Static Variables
        private string classKey;
        private static Dictionary<string, Dictionary<string, string>> resourceCache = new Dictionary<string, Dictionary<string, string>>();
        private static Dictionary<string, Dictionary<string, string>> resourceGlobalCache = new Dictionary<string, Dictionary<string, string>>();
        //For future we can customize the base language replace 1033 with that
        private static string resBaseLang = "1033";
        private static Hashtable m_MLPagesCache = new Hashtable();
        private static object synObj= new object();
        private static string sShowErrorCode = "false";
        private static string[] sDataSeparator = { "|^|" };
        #endregion

        /// <summary>
        /// Get PageId for specific page
        /// </summary>
        /// <param name="sPageName"></param>
        /// <returns></returns>
        public static string PageId(string sPageName)
        {
            if (m_MLPagesCache.ContainsKey(sPageName.ToUpper()))
            {
                return m_MLPagesCache[sPageName.ToUpper()].ToString();
            }
            else
            {
                return "0"; 
            }
        }

        /// <summary>
        /// Loads the PageIds to Hastable
        /// </summary>
        static RMXResourceProvider()
        {
            //RMXResourceServiceClient oClient = null;
            RMResource objRes = null;
            try
            {
                //oClient = new RMXResourceServiceClient();
                //sShowErrorCode = oClient.ShowErrorCode();
                objRes = new RMResource();
                objRes.ClientId = AppHelper.ClientId;
                sShowErrorCode = AppHelper.GetResponse<string>("RMService/Resource/showerror", AppHelper.HttpVerb.GET, AppHelper.APPLICATION_JSON, objRes); 
                resBaseLang = AppHelper.GetBaseLanguageCodeWithCulture();
                XElement pageListElement = GetMLSupportedPageList();
                foreach (XElement oPageElem in pageListElement.Descendants("Page"))
                {
                    string sPageName = oPageElem.FirstAttribute.Value;
                    if (!m_MLPagesCache.Contains(sPageName.ToUpper()))
                    {
                        m_MLPagesCache.Add(sPageName.ToUpper(), oPageElem.LastAttribute.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException(ex.Message);
            }
            finally
            {
                if (objRes != null)
                {
                    objRes = null;
                }
            }
        }

        /// <summary>
        /// Sets the ClassKey Constructor
        /// </summary>
        /// <param name="classKey"></param>
        public RMXResourceProvider(string classKey)
        {
            this.classKey = classKey.ToUpper();
        }

        /// <summary>
        /// Get the resource value of specific key
        /// </summary>
        /// <param name="p_sResourceKey"></param>
        /// <param name="p_sPageId"></param>
        /// <param name="p_sResourceType"></param>
        /// <returns></returns>
        public static string GetSpecificObject(string p_sResourceKey, string p_sPageId, string p_sResourceType)
        {
            string resourceValue = string.Empty;
            string sLanguageCode = AppHelper.GetLanguageCode();
            Dictionary<string, string> resCacheByKey = null;
            p_sResourceKey = p_sPageId + "|^|" + p_sResourceType + "|^|" + p_sResourceKey;
            if (resourceCache.ContainsKey(sLanguageCode))
            {
                resCacheByKey = resourceCache[sLanguageCode];

                #region hlv added
                //*
                if (!resCacheByKey.ContainsKey(p_sResourceKey))
                {
                    //RMXResourceServiceClient oClient = null;
                    RMResource objRes = null;
                    try
                    {
                        objRes = new RMResource();
                        objRes.Token = AppHelper.Token;
                        objRes.ClientId = AppHelper.ClientId;
                        objRes.DataSourceId = "0";
                        objRes.PageId = p_sPageId;
                        objRes.LanguageCode = sLanguageCode;
                        objRes.ResourceKey = p_sResourceKey.Split(sDataSeparator, StringSplitOptions.None)[2];
                        objRes.ResourceType = p_sResourceType;
                        string sDefKeyValue = AppHelper.GetResponse<string>("RMService/Resource/resbylangcodeandkey", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes); 
                        //oClient = new RMXResourceServiceClient();
                        //string sDefKeyValue = oClient.GetResourceByLanguageAndKey(0, p_sPageId, sLanguageCode, p_sResourceKey.Split(sDataSeparator, StringSplitOptions.None)[2], p_sResourceType);
                        lock (synObj)
                        {
                            if (resCacheByKey == null)
                            {
                                resCacheByKey = new Dictionary<string, string>();
                                resourceCache.Add(sLanguageCode, resCacheByKey);
                            }
                            if (!resCacheByKey.ContainsKey(p_sResourceKey))
                                resCacheByKey.Add(p_sResourceKey, sDefKeyValue);
                        }
                    }
                    catch (Exception ee)
                    {
                        //resourceValue = p_sResourceKey;
                    }
                    finally
                    {
                        if (objRes != null)
                        {
                            objRes = null;
                        }
                    }
                }
                //*/
                #endregion

                if (resCacheByKey.ContainsKey(p_sResourceKey))
                {
                    resourceValue = resCacheByKey[p_sResourceKey];
                    return resourceValue;
                }
            }
            sLanguageCode = resBaseLang.Split('|')[0].ToString();
            if (resourceCache.ContainsKey(sLanguageCode))
            {
                resCacheByKey = resourceCache[sLanguageCode];
                if (resCacheByKey.ContainsKey(p_sResourceKey))
                    resourceValue = resCacheByKey[p_sResourceKey];
            }
            if (string.IsNullOrEmpty(resourceValue))
            {
                //RMXResourceServiceClient oClient = null;
                RMResource objRes = null;
                try
                {
                    objRes = new RMResource();
                    //oClient = new RMXResourceServiceClient();
                    objRes.ClientId = AppHelper.ClientId;
                    objRes.Token = AppHelper.Token;
                    objRes.DataSourceId = "0";
                    objRes.PageId = p_sPageId;
                    objRes.LanguageCode = sLanguageCode;
                    objRes.ResourceKey = p_sResourceKey.Split(sDataSeparator, StringSplitOptions.None)[2];
                    objRes.ResourceType = p_sResourceType;
                    //string sDefKeyValue = oClient.GetResourceByLanguageAndKey(0, p_sPageId, sLanguageCode, p_sResourceKey.Split(sDataSeparator,StringSplitOptions.None)[2], p_sResourceType);
                    string sDefKeyValue = AppHelper.GetResponse<string>("RMService/Resource/resbylangcodeandkey", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes); 
                    lock (synObj)
                    {
                        if (resCacheByKey == null)
                        {
                            resCacheByKey = new Dictionary<string, string>();
                            resourceCache.Add(sLanguageCode, resCacheByKey);
                        }
                        if (!resCacheByKey.ContainsKey(p_sResourceKey))
                            resCacheByKey.Add(p_sResourceKey, sDefKeyValue);
                    }
                }
                catch (Exception ee)
                {
                    resourceValue = p_sResourceKey;
                }
                finally
                {
                    if (objRes != null)
                    {
                        objRes = null;
                    }
                }
            }
            return resourceValue;
        }
        /// <summary>
        /// GetSpecificGlobalObject
        /// </summary>
        /// <param name="p_sResourceKey">ResourceKey</param>
        /// <returns></returns>
        public static string GetSpecificGlobalObject(string p_sResourceKey)
        {
            string resourceValue = string.Empty;
            string sLanguageCode = AppHelper.GetLanguageCode();
            Dictionary<string, string> resCacheByKey = null;
            try
            {
                if (resourceGlobalCache.ContainsKey(sLanguageCode))
                {
                    resCacheByKey = resourceGlobalCache[sLanguageCode];
                    if (resCacheByKey.ContainsKey(p_sResourceKey))
                    {
                        resourceValue = resCacheByKey[p_sResourceKey];
                        string IsShowErrorCode = RMXResourceProvider.IsShowErrorCode();
                        if (IsShowErrorCode.ToUpper() != "TRUE")
                        {
                            resourceValue = RemoveErrorCode(resourceValue);
                        }
                        return resourceValue;
                    }
                }
            }
            catch (Exception ee)
            {
            }
            return resourceValue;
        }
        /// <summary>
        /// Remove Error code from Resource Value
        /// </summary>
        /// <param name="sKeyValue"></param>
        /// <returns></returns>
        private static string RemoveErrorCode(string sKeyValue)
        {
            if (sKeyValue.StartsWith("rmA-"))
            {
                string sValue = sKeyValue.Substring(sKeyValue.IndexOf(":") + 1);
                return sValue;
            }
            else
            {
                return sKeyValue;
            }
        }
        /// <summary>
        /// GetAllGlobalResourceByLanguage
        /// </summary>
        /// <param name="p_DataSourceId">DataSourceId</param>
        /// <param name="p_LanguageCode">LanguageId</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetAllGlobalResourceByLanguage(int p_DataSourceId, string p_LanguageCode)
        {
            //string sLanguageCode = AppHelper.GetLanguageCode();
            Dictionary<string, string> resCacheByKey = null;
            if (resourceGlobalCache.ContainsKey(p_LanguageCode))
            {
                resCacheByKey = resourceGlobalCache[p_LanguageCode];
            }
            if (resCacheByKey == null)
            {
                //RMXResourceServiceClient oClient = null;
                RMResource objRes = null;
                try
                {
                    Dictionary<string, string> objDict = null;
                    objRes = new RMResource();
                    //oClient = new RMXResourceServiceClient();
                    //objDict = oClient.GetGlobalResourcesByLanguage(0, p_LanguageCode,AppHelper.ClientId);
                    objRes.ClientId = AppHelper.ClientId;
                    objRes.DataSourceId = p_DataSourceId.ToString(); 
                    objRes.LanguageCode = p_LanguageCode;
                    objDict = AppHelper.GetResponse<Dictionary<string, string>>("RMService/Resource/globalresbylangcode", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, objRes); 
                    lock (synObj)
                    {
                        if (resCacheByKey == null)
                        {
                            resCacheByKey = new Dictionary<string, string>();
                            resourceGlobalCache.Add(p_LanguageCode, resCacheByKey);
                        }
                        foreach (KeyValuePair<string, string> sKeyValue in objDict)
                        {
                            if (!string.IsNullOrEmpty(sKeyValue.Value))
                            {
                                if (!resCacheByKey.ContainsKey(sKeyValue.Key))
                                    resCacheByKey.Add(sKeyValue.Key, sKeyValue.Value);
                            }
                        }
                    }
                }
                finally
                {
                    if (objRes != null)
                    {
                        objRes = null;
                    }
                }
            }
            return resCacheByKey;
        }
        /// <summary>
        /// Get all resources for a specific resource type
        /// </summary>
        /// <param name="p_classKey"></param>
        /// <param name="p_resourceType"></param>
        /// <returns></returns>
        public static Dictionary<string,string> GetAllObjectByResourceType(string p_classKey,string p_resourceType)
        {
            string sLanguageCode = AppHelper.GetLanguageCode();
            Dictionary<string, string> resCacheByKeyType = null;
            if (resourceCache.ContainsKey(sLanguageCode))
            {
                resCacheByKeyType = resourceCache[sLanguageCode];
                resCacheByKeyType = resCacheByKeyType.Where(res => res.Key.StartsWith(p_classKey + "|^|" + p_resourceType) && res.Value is string)
                                    .ToDictionary(dict => dict.Key, dict => dict.Value);
            }
            if (resCacheByKeyType == null || resCacheByKeyType.Count == 0)  //mbahl3 MITS 30224
            {
                //RMXResourceServiceClient oClient = null;
                RMResource objRes = null;
                try
                {
                    //oClient = new RMXResourceServiceClient();
					//mbahl3 mits 30224
                    //Dictionary<string, string> objDict = oClient.GetAllResourceByLanguageAndPageIdAndResType(0, p_classKey, sLanguageCode, p_resourceType);
                    Riskmaster.Models.RMResource oRMResource = new Riskmaster.Models.RMResource();
                    oRMResource.LanguageCode = sLanguageCode;
                    oRMResource.ResourceType = p_resourceType;
                    oRMResource.PageId = p_classKey;
                    oRMResource.Token = AppHelper.ReadCookieValue("SessionId");
                    oRMResource.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    Dictionary<string, string> objDict = AppHelper.GetResponse<Dictionary<string, string>>("RMService/Resource/allresbylangcodepageandtype", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oRMResource); 
                    lock (synObj)
                    {
                        if (resCacheByKeyType == null)
                        {
                            resCacheByKeyType = new Dictionary<string, string>();
                            resourceCache.Add(sLanguageCode, resCacheByKeyType);
                        }
                        foreach (KeyValuePair<string, string> sKeyValue in objDict)
                        {
                            if (!string.IsNullOrEmpty(sKeyValue.Value))
                            {
                                if (!resCacheByKeyType.ContainsKey(sKeyValue.Key))
                                    resCacheByKeyType.Add(sKeyValue.Key, sKeyValue.Value);
                            }
                        }
                    }
						//mbahl3 mits 30224
                }
                finally
                {
                    if (objRes != null)
                    {
                        objRes = null;
                    }
                }
            }
            return resCacheByKeyType;
        }
        
        /// <summary>
        /// Clears the Resource for the page
        /// </summary>
        /// <param name="p_iPageId"></param>
        /// <param name="p_iLangCode"></param>
        public static void ClearResourceForPage(string p_iPageId, string p_iLangCode)
        {
            Dictionary<string, string> resCacheByKeyType = null;
            if(resourceCache.ContainsKey(p_iLangCode))
            {
                resCacheByKeyType = resourceCache[p_iLangCode];
                resCacheByKeyType = resCacheByKeyType.Where(res => res.Key.StartsWith(p_iPageId + "|^|") && res.Value is string)
                                    .ToDictionary(dict => dict.Key, dict => dict.Value);
                foreach (KeyValuePair<string, string> sKey in resCacheByKeyType)
                {
                   resourceCache[p_iLangCode].Remove(sKey.Key);
                }
            }
        }

        /// <summary>
        /// Universal Method to get resource for keys
        /// </summary>
        /// <param name="resourceKey"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object GetObject(string resourceKey, CultureInfo culture)
        {
            if (!this.classKey.EndsWith(".ASPX"))
                return null; 
            if (Disposed)
                throw new ObjectDisposedException("object is already disposed.");

            if (string.IsNullOrEmpty(resourceKey))
                throw new ArgumentNullException("resourceKey");
                     
            string resourceValue = string.Empty;
            string sLanguageCode = AppHelper.GetLanguageCode();
            Dictionary<string, string> resCacheByKey = null;
            if (resourceCache.ContainsKey(sLanguageCode))
            {
                resCacheByKey = resourceCache[sLanguageCode];
                resourceValue = GetResourceValueFromCache(resourceKey, resCacheByKey);
            }
            if (string.IsNullOrEmpty(resourceValue))
            {
                //RMXResourceServiceClient oClient = null;
                Dictionary<string, string> objDict = null;
                try
                {
                    //oClient = new RMXResourceServiceClient();
                    //objDict = oClient.GetAllResourceByLanguageAndPageId(0, m_MLPagesCache[classKey].ToString(), sLanguageCode, string.Empty);

                    RMResource oRMResource = new RMResource();
                    oRMResource.DataSourceId = "0";
                    oRMResource.PageId = m_MLPagesCache[classKey].ToString();
                    oRMResource.ResourceType = string.Empty;
                    oRMResource.LanguageCode = sLanguageCode;
                    oRMResource.Token = AppHelper.ReadCookieValue("SessionId");
                    oRMResource.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    objDict = AppHelper.GetResponse<Dictionary<string,string>>("RMService/Resource/allresbylangcodeandpageid", AppHelper.HttpVerb.POST, "application/json", oRMResource); 

                    resourceValue = GetResourceValueFromCache(resourceKey, objDict);
                    lock (synObj)
                    {
                        if (resCacheByKey == null)
                        {
                            resCacheByKey = new Dictionary<string, string>();
                            resourceCache.Add(sLanguageCode, resCacheByKey);
                        }
                        foreach (KeyValuePair<string, string> sKeyValue in objDict)
                        {
                            if (!string.IsNullOrEmpty(sKeyValue.Value))
                            {
                                if(!resCacheByKey.ContainsKey(sKeyValue.Key))
                                 resCacheByKey.Add(sKeyValue.Key, sKeyValue.Value);
                            }
                            else
                            {
                                oRMResource.LanguageCode = resBaseLang.Split('|')[0].ToString();
                                oRMResource.ResourceKey = resourceKey.Split(sDataSeparator, StringSplitOptions.None)[2];
                                string sDefKeyValue = AppHelper.GetResponse<string>("RMService/Resource/resbylangcodeandkey", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oRMResource);
                                //string sDefKeyValue = oClient.GetResourceByLanguageAndKey(0, m_MLPagesCache[classKey].ToString(), resBaseLang.Split('|')[0].ToString(), resourceKey.Split(sDataSeparator, StringSplitOptions.None)[2], string.Empty);
                                if (!resCacheByKey.ContainsKey(sKeyValue.Key))
                                 resCacheByKey.Add(sKeyValue.Key, sDefKeyValue);
                            }
                        }
                    }
                }
                catch (Exception ee)
                {
                    resourceValue = resourceKey;
                }
                finally
                {
                    //if (oClient != null)
                    //{
                    //    oClient = null;
                    //}
                }
                
            }
            return resourceValue;

        }
        //public class RMResource
        //{
        //    public string PageId
        //    {
        //        get;
        //        set;
        //    }
        //    public string ResourceId
        //    {
        //        get;
        //        set;
        //    }
        //    public string ResourceType
        //    {
        //        get;
        //        set;
        //    }
        //    public string ResourceKey
        //    {
        //        get;
        //        set;
        //    }
        //    public string ResourceValue
        //    {
        //        get;
        //        set;
        //    }
        //    public string LanguageCode
        //    {
        //        get;
        //        set;
        //    }
        //    public string DataSourceId
        //    {
        //        get;
        //        set;
        //    }
        //    public string User
        //    {
        //        get;
        //        set;
        //    }
        //    public string IsFDMPage
        //    {
        //        get;
        //        set;
        //    }
        //    public string LastUpdated
        //    {
        //        get;
        //        set;
        //    }
        //    public string Token { get; set; }
        //    public int ClientId { get; set; }
        //    public string ConnStringKey { get; set; }
        //    public string CountryId { get; set; }
        //}
        /// <summary>
        /// GetResourceValueFromCache
        /// </summary>
        /// <param name="resourceKey"></param>
        /// <param name="resCacheByKey"></param>
        /// <returns></returns>
        private string GetResourceValueFromCache(string resourceKey, Dictionary<string, string> resCacheByKey)
        {
            string resourceValue = string.Empty;
            if (resCacheByKey.ContainsKey(m_MLPagesCache[classKey] + "|^|" + ((int)RessoureType.LABEL).ToString() + "|^|" + resourceKey))
            {
                resourceValue = resCacheByKey[m_MLPagesCache[classKey] + "|^|" + ((int)RessoureType.LABEL).ToString() + "|^|" + resourceKey];
            }
            else if (resCacheByKey.ContainsKey(m_MLPagesCache[classKey] + "|^|" + ((int)RessoureType.BUTTON).ToString() + "|^|" + resourceKey))
            {
                resourceValue = resCacheByKey[m_MLPagesCache[classKey] + "|^|" + ((int)RessoureType.BUTTON).ToString() + "|^|" + resourceKey];
            }
            else if (resCacheByKey.ContainsKey(m_MLPagesCache[classKey] + "|^|" + ((int)RessoureType.TOOLTIP).ToString() + "|^|" + resourceKey))
            {
                resourceValue = resCacheByKey[m_MLPagesCache[classKey] + "|^|" + ((int)RessoureType.TOOLTIP).ToString() + "|^|" + resourceKey];
            }
            return resourceValue;
        }

        /// <summary>
        /// Get reader object
        /// </summary>
        public IResourceReader ResourceReader
        {
            get
            {
                if (Disposed)
                    throw new ObjectDisposedException("object is already disposed.");
                ListDictionary resourceDictionary = new ListDictionary();
                return new RMXResourceReader(resourceDictionary);
            }
        }

        /// <summary>
        /// CleanUp
        /// </summary>
        protected override void Cleanup()
        {
            try
            {
                resourceCache.Clear();
                //this.resourceCache.Clear();
            }
            finally
            {
                base.Cleanup();
            }
        }
        /// <summary>
        /// Is Show Error Code Enable
        /// </summary>
        /// <returns></returns>
        public static string IsShowErrorCode()
        {
            //RMXResourceServiceClient objClient = new RMXResourceServiceClient();
            string sShowErrCode = sShowErrorCode;
            RMResource objRes = null;
            try
            {
                objRes = new RMResource();
                //sShowErrCode = objClient.ShowErrorCode();
                sShowErrCode = AppHelper.GetResponse<string>("RMService/Resource/showerror", AppHelper.HttpVerb.GET, AppHelper.APPLICATION_JSON, objRes); 
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (objRes != null)
                {
                    objRes = null;
                }
            }
            return sShowErrCode;
        }
        /// <summary>
        /// Get the PageList with PageIds
        /// </summary>
        /// <returns></returns>
        static public XElement GetMLSupportedPageList()
        {
             XElement oTemplate = XElement.Parse(@"
                 <MultiLanguagePageList>
	               <Page name='DiaryList.aspx' PageId='1' />
                   <Page name='AttachDiary.aspx' PageId='2' />
                   <Page name='AttachDiarySuccessful.aspx' PageId='3' />
                   <Page name='CompleteDiary.aspx' PageId='4' />
                   <Page name='CreateDiary.aspx' PageId='5' />
                   <Page name='DeleteAllDiaries.aspx' PageId='6' />
                   <Page name='DiaryActivity.aspx' PageId='7' />
                   <Page name='DiaryDetails.aspx' PageId='8' />
                   <Page name='DiaryHistory.aspx' PageId='9' />
                   <Page name='DiaryListCalendar.aspx' PageId='10' />
                   <Page name='DiaryListing.aspx' PageId='11' />
                   <Page name='EditDiary.aspx' PageId='12' />
                   <Page name='PeekDiary.aspx' PageId='13' />
                   <Page name='PrintCalendar.aspx' PageId='14' />
                   <Page name='PrintListing.aspx' PageId='15' />
                   <Page name='RollDiary.aspx' PageId='16' />
                   <Page name='RouteDiary.aspx' PageId='17' />
                   <Page name='VoidDiary.aspx' PageId='18' />
                   <Page name='DiaryConfig.aspx' PageId='19' />
                   <Page name='AddDocument.aspx' PageId='20' />
                   <Page name='ConfirmDelete.aspx' PageId='22' />
                   <Page name='CopyDocument.aspx' PageId='23' />
                   <Page name='DisplayDocument.aspx' PageId='24' />
                   <Page name='DocList.aspx' PageId='25' />
                   <Page name='DocumentList.aspx' PageId='26' />
                   <Page name='DocumentListCommon.aspx' PageId='27' />
                   <Page name='EditDoc.aspx' PageId='28' />
                   <Page name='EmailDocuments.aspx' PageId='29' />
                   <Page name='MoveDocuments.aspx' PageId='30' />
                   <Page name='TestDocumentMgmt.aspx' PageId='31' />
                   <Page name='TransferDocument.aspx' PageId='32' />
                   <Page name='AdvancedSearch.aspx' PageId='33' />
                   <Page name='ProgressNotes.aspx' PageId='34' />
                   <Page name='ProgressNotesEditPage.aspx' PageId='35' />
                   <Page name='ProgressNotesEditTemplates.aspx' PageId='36' />
                   <Page name='ProgressNotesTemplates.aspx' PageId='37' />
                   <Page name='SelectClaim.aspx' PageId='38' />
                   <Page name='ViewNotesAsHtml.aspx' PageId='39' />
                   <Page name='FetchData.aspx' PageId='40' />
                   <Page name='MailMergeIssue.aspx' PageId='41' />
                   <Page name='MergeClaimLetter.aspx' PageId='42' />
                   <Page name='MergeEditResult.aspx' PageId='43' />
                   <Page name='MergeEditResultSaved.aspx' PageId='44' />
                   <Page name='MergeTemplate1.aspx' PageId='45' />
                   <Page name='MergeTemplate2.aspx' PageId='46' />
                   <Page name='MergeTemplateFilter.aspx' PageId='47' />
                   <Page name='OpenMailMergedLetter.aspx' PageId='48' />
                   <Page name='RecordSummary.aspx' PageId='49' />
                   <Page name='OpenMainMergeSaved.aspx' PageId='50' />
                   <Page name='TMSettingsPeriodically.aspx' PageId='51' />
                   <Page name='TMSettingsWeekly.aspx' PageId='52' />
                   <Page name='voidclearchecks.aspx' PageId='53' />
                   <Page name='ReserveListing.aspx' PageId='55' />
                   <Page name='ReserveListingBOB.aspx' PageId='56' />
                   <Page name='ModifyReserve.aspx' PageId='57' />
                   <Page name='AddPaymentsToCoverages.aspx' PageId='64' />
                   <Page name='AutoCheckList.aspx' PageId='65' />
                   <Page name='BulkCheckProcessPayments.aspx' PageId='66' />
                   <Page name='BulkCheckRelease.aspx' PageId='67' />
                   <Page name='ControlRequest.aspx' PageId='68' />
                   <Page name='CustomPaymentNotification.aspx' PageId='69' />
                   <Page name='InsufficientReserves.aspx' PageId='70' />
                   <Page name='PaymentHistory.aspx' PageId='71' />
                   <Page name='PaymentIntervalFrame.aspx' PageId='73' />
                   <Page name='ProcessClaims.aspx' PageId='74' />
                   <Page name='reissue.aspx' PageId='75' />
                   <Page name='StartupPaymentNotification.aspx' PageId='77' />
                   <Page name='MergeTemplates.aspx' PageId='79' />
                   <Page name='MergeEditTemplate1.aspx' PageId='80' />
                   <Page name='MergeCreateTemplate1.aspx' PageId='81' />
                   <Page name='MergeCreateTemplate2.aspx' PageId='82' />
                   <Page name='MergeCreateTemplate3.aspx' PageId='83' />
                   <Page name='MergeCreateTemplate4.aspx' PageId='84' />
                   <Page name='MergeEditAdminTrackingTemplate.aspx' PageId='85' />
                   <Page name='MergeEditTemplate2.aspx' PageId='86' />
                   <Page name='MergeEditTemplate3.aspx' PageId='87' />
                   <Page name='MergeEditTemplate4.aspx' PageId='88' />
                   <Page name='MergeErrorResponse.aspx' PageId='89' />                   
                   <Page name='MergeTemplateSaved.aspx' PageId='90' />
                   <Page name='CustomizeUserList.aspx' PageId='91' />
                   <Page name='MergeCreateAdminTrackingTemplate.aspx' PageId='92' />
                   <Page name='accountbalance.aspx' PageId='93' />
                   <Page name='balance.aspx' PageId='94' />
                   <Page name='CheckStocks.aspx' PageId='95' />
                   <Page name='CheckStocksClone.aspx' PageId='96' />
                   <Page name='clearmultiplechecks.aspx' PageId='97' />
                   <Page name='DisbursementAccountBalanceInfo.aspx' PageId='98' />
                   <Page name='MoneyMarketAccountBalanceInfo.aspx' PageId='99' />
                   <Page name='reports.aspx' PageId='100' />
                   <Page name='UploadFile.aspx' PageId='101' />
                   <Page name='DiaryCalendar.aspx' PageId='102' />
				   <Page name='HeaderConfig.aspx' PageId='103' />
                   <Page name='ResetChecks.aspx' PageId='104' />
                   <Page name='RecreateCheck.aspx' PageId='105' />   
                   <Page name='SearchResults.aspx' PageId='106' /> 
				   <Page name='TMScheduledView.aspx' PageId='107' />
				   <Page name='PSOSettings.aspx' PageId='108' />
				   <Page name='MMSEASettings.aspx' PageId='109' />
				   <Page name='RM1099Preferences.aspx' PageId='110' />
				   <Page name='ClaimActLog.aspx' PageId='111' />
				   <Page name='commentsummary.aspx' PageId='112' />
				   <Page name='ClaimStatusHistory.aspx' PageId='113'/>
				   <Page name='GetClaimHistory.aspx' PageId='114'/>
				    <Page name='CMMSEAXDATA.aspx' PageId='115' />
				   <Page name='BankingStatus.aspx' PageId='120'/>
				   <Page name='EntitySSNCheck.aspx' PageId='121'/>
				   <Page name='WPAAutoDiary.aspx' PageId='122'/>
                   <Page name='LoggedInUserList.aspx' PageId='123'/>
                   <Page name='ReportAccessMgmt.aspx' PageId='124'/>
                   <Page name='GrantReportAccess.aspx' PageId='125'/>
                   <Page name='SupplementalData.aspx' PageId='126'/>
                   <Page name='GridParameterSetup.aspx' PageId='127'/> 
                   <Page name='PVList.aspx' PageId='128'/>
                   <Page name='AutoAssignAdjusterCustom.aspx' PageId='129'/>
                   <Page name='CustomizeReports.aspx' PageId='130'/>
                   <Page name='CustomizeEditWindow.aspx' PageId='131'/>
                   <Page name='RSWCustomization.aspx' PageId='132'/>
                   <Page name='ExecSummary.aspx' PageId='155' />
                   <Page name='RecentEvents.aspx' PageId='156' />
                   <Page name='RecentClaims.aspx' PageId='157' />
                   <Page name='Default.aspx' PageId='158' />
				   <Page name='WordMergeEmailDetails.aspx' PageId='159' />
                   <Page name='FNOLAddReserve.aspx' PageId='160' />
                   <Page name='FNOLReserveSetUp.aspx' PageId='161' />
                   <Page name='WordMergeEmailDetailSetup.aspx' PageId='162' />
				   <Page name='FNOL.aspx' PageId='163' />    
				   <Page name='TMSettingsYearly.aspx' PageId='164' />
				   <Page name='TMSettingsOneTime.aspx' PageId='165' /> 
				   <Page name='TMSettings.aspx' PageId='166' />
				   <Page name='TMSettingsMonthly.aspx' PageId='167' /> 
				   <Page name='ISOMappingGrid.aspx' PageId='168' />
                   <Page name='ISOSetting.aspx' PageId='169' />
				   <Page name='MBRSettings.aspx' PageId='170' />
				   <Page name='TMView.aspx' PageId='171' />
				   <Page name='CheckBatchPrintSettings.aspx' PageId='172' />
				   <Page name='ClaimExportOrgGrid.aspx' PageId='173' />
                   <Page name='DAPositivePay.aspx' PageId='174' />
				   <Page name='DDSSettings.aspx' PageId='175' />
				   <Page name='TMStatusDetails.aspx' PageId='176' /> 
				   <Page name='UserVerification.aspx' PageId='177' />
                   <Page name='ClaimExportCSStarsSettings.aspx' PageId='178' />
				   <Page name='DISSettings.aspx' PageId='179' />
				   <Page name='MMSEAData.aspx' PageId='180' /> 
                   <Page name='MMSEAPartyData.aspx' PageId='181' />
                   <Page name='TPOCData.aspx' PageId='182' />
				   <Page name='AdjDatedTextList.aspx' PageId='183' />
				   <Page name='DupClaim.aspx' PageId='184' />
				   <Page name='PrintAdjusterDatedText.aspx' PageId='185' />
				   <Page name='VehicleAccidentsList.aspx' PageId='186' />
                   <Page name='BenCalc.aspx' PageId='187'/>
                   <Page name='EDIHistory.aspx' PageId='188'/>
                   <Page name='TableData.aspx' PageId='189'/>
                   <Page name='PVDisplayDocument.aspx' PageId='190'/>
                   <Page name='ConfRecPermissions.aspx' PageId='191'/>
                   <Page name='AdministrativeTracking.aspx' PageId='192'/>
                   <Page name='TableDataDetail.aspx' PageId='193'/>
                   <Page name='PVConfirmDelete.aspx' PageId='194'/>
                   <Page name='SelectBatch.aspx' PageId='195'/>
                   <Page name='MainPage.aspx' PageId='196'/>
                   <Page name='PVEditDoc.aspx' PageId='197'/>
                   <Page name='WPAUtil.aspx' PageId='198'/>
                   <Page name='MDAData.aspx' PageId='200'/>
                   <Page name='MedwatchReport.aspx' PageId='203'/>
                   <Page name='MMIHistory.aspx' PageId='204'/>
                   <Page name='DiaHistLookUp.aspx' PageId='205'/>
                   <Page name='DiagSearch.aspx' PageId='206'/>
                   <Page name='DayView.aspx' PageId='210'/>
                   <Page name='DiaryCalendarStyle.aspx' PageId='212'/>
                   <Page name='MonthView.aspx' PageId='213'/>
                   <Page name='WeekView.aspx' PageId='214'/>
                   <Page name='BRSCPTBasicCodeDetails.aspx' PageId='215'/>
                   <Page name='BRSCPTCodesListing.aspx' PageId='216'/>
                   <Page name='BRSCPTExtendedCodeDetails.aspx' PageId='217'/>
                   <Page name='BRSFactor.aspx' PageId='218'/>
                   <Page name='BRSFactors.aspx' PageId='219'/>
                   <Page name='BRSFeeTable.aspx' PageId='220'/>
                   <Page name='BRSFeeTableDetail.aspx' PageId='221'/>
                   <Page name='BRSHospitalPerDiem.aspx' PageId='222'/>
                   <Page name='BRSImportSchedule1.aspx' PageId='223'/>
                   <Page name='BRSModifierValue.aspx' PageId='224'/>
                   <Page name='BRSModifierValues.aspx' PageId='225'/>
                   <Page name='BRSSettings.aspx' PageId='226'/>
                   <Page name='PopUp.aspx' PageId='227'/>
                   <Page name='CustomizeRMXPortal.aspx' PageId='228'/>
                   <Page name='PortalUserTree.aspx' PageId='229'/>
                   <Page name='WebLinksSetup.aspx' PageId='230'/>
				   <Page name='LookUpClaimPlan.aspx' PageId='232'/>
                   <Page name='RecentClaimConfig.aspx' PageId='234'/>
                   <Page name='LookUpClaimPolicy.aspx' PageId='235'/>
                   <Page name='CatastropheLookUp.aspx' PageId='236'/>
				   <Page name='NavigationTreeSetup.aspx' PageId='250' />
				   <Page name='ClaimantUnit.aspx' PageId='252' />
                   <Page name='AddPolicyConfig.aspx' PageId='253' />
                   <Page name='SearchMain.aspx' PageId='254' />
                   <Page name='SearchLocalization.aspx' PageId='255' />
				   <Page name='SearchWizard.aspx' PageId='256' />
				   <Page name='QueryDesigner.aspx' PageId='257' />
				   <Page name='IntegralPolicySysDownloadResults.aspx' PageId='261' />
                   <Page name='SetUpPolicySystem.aspx' PageId='262' />
					<Page name='PersonInvMenuConfig.aspx' PageId='263' />
                    <Page name='IntegralPolicyData.aspx' PageId='264' />
                    <Page name='AsyncSearchResult.aspx' PageId='265' /> 
                    <Page name='AsyncClaimActLog.aspx' PageId='140' /> 

					<Page name='DeductibleGrid.aspx' PageId='266' />
                   <Page name='DeductibleDetails.aspx' PageId='267' />
                   <Page name='DeductibleHistory.aspx' PageId='268' />
                   <Page name='PrintChecksBatch.aspx' PageId='269' />
                   <Page name='DeductbleAggregateHistory.aspx' PageId='270' />
                   <Page name='DeductibleEventSummary.aspx' PageId='271' />
                   <Page name='DeductibleGroupDetails.aspx' PageId='272' />
                   <Page name='CoverageGroupMaintenance.aspx' PageId='273' />
                   <Page name='CoverageGroupsMaintenance.aspx' PageId='274' />
                    <Page name='CovGroupListAddLanguage.aspx' PageId='275' />
                   <Page name='CoveGroupDetailAddLanguage.aspx' PageId='276' />  
                   <Page name='DupAddress.aspx' PageId='277' />
						
					<Page name='event.aspx' PageId='10000' />
                   <Page name='eventdatedtext.aspx' PageId='10001' />
                   <Page name='fallinfo.aspx' PageId='10002' />
                   <Page name='medwatch.aspx' PageId='10003' />
                   <Page name='medwatchtest.aspx' PageId='10004' />
                   <Page name='concomitant.aspx' PageId='10005' />
                   <Page name='osha.aspx' PageId='10006' />
                   <Page name='qminitialreview.aspx' PageId='10007' />
                   <Page name='qmphysadvreview.aspx' PageId='10008' />
                   <Page name='qmcommdeptreview.aspx' PageId='10009' />
                   <Page name='qmqualmgrreview.aspx' PageId='10010' />
                   <Page name='sentinel.aspx' PageId='10011' />
                   <Page name='eventintervention.aspx' PageId='10012' />
                   <Page name='claimgc.aspx' PageId='10013' />
                   <Page name='claimpc.aspx' PageId='10014' />
                   <Page name='claimva.aspx' PageId='10015' />
                   <Page name='unit.aspx' PageId='10016' />
                   <Page name='claimwc.aspx' PageId='10017' />
                   <Page name='casemanagementlist.aspx' PageId='10018' />
                   <Page name='funds.aspx' PageId='10019' />
                   <Page name='split.aspx' PageId='10020' />
                   <Page name='cmxcmgrhist.aspx' PageId='10021' />
                   <Page name='cmxtreatmentpln.aspx' PageId='10022' />
                   <Page name='cmxmedmgtsavings.aspx' PageId='10023' />
                   <Page name='cmxaccommodation.aspx' PageId='10024' />
                   <Page name='cmxvocrehab.aspx' PageId='10025' />
                   <Page name='claimdi.aspx' PageId='10026' />
                   <Page name='bankaccount.aspx' PageId='10027' />
                   <Page name='deposit.aspx' PageId='10028' />
                   <Page name='employee.aspx' PageId='10029' />
                   <Page name='violation.aspx' PageId='10030' />
                   <Page name='dependent.aspx' PageId='10031' />
                   <Page name='entitymaint.aspx' PageId='10032' />
                   <Page name='propertyunit.aspx' PageId='10033' />
                   <Page name='orghierarchymaint.aspx' PageId='10034' />
                   <Page name='entityexposure.aspx' PageId='10035' />
                   <Page name='entityxcontactinfo.aspx' PageId='10036' />
                   <Page name='entityxoperatingas.aspx' PageId='10037' />
                   <Page name='patient.aspx' PageId='10038' />
                   <Page name='patientprocedure.aspx' PageId='10039' />
                   <Page name='driver.aspx' PageId='10040' />
                   <Page name='people.aspx' PageId='10041' />
                   <Page name='plan.aspx' PageId='10042' />
                   <Page name='physician.aspx' PageId='10043' />
                   <Page name='physicianprivilege.aspx' PageId='10044' />
                   <Page name='physiciancertification.aspx' PageId='10045' />
                   <Page name='physicianeducation.aspx' PageId='10046' />
                   <Page name='physicianprevhospital.aspx' PageId='10047' />
                   <Page name='policy.aspx' PageId='10048' />
                   <Page name='policycoverage.aspx' PageId='10049' />
                   <Page name='policymco.aspx' PageId='10050' />
                   <Page name='policyinsurer.aspx' PageId='10051' />
                   <Page name='policyreinsurer.aspx' PageId='10052' />
                   <Page name='staff.aspx' PageId='10053' />
                   <Page name='staffprivilege.aspx' PageId='10054' />
                   <Page name='vehicle.aspx' PageId='10055' />
                   <Page name='vehicleinspections.aspx' PageId='10056' />
                   <Page name='leaveplan.aspx' PageId='10057' />
                   <Page name='catastrophe.aspx' PageId='10058' />
                   <Page name='adjuster.aspx' PageId='10059' />
                   <Page name='adjusterdatedtext.aspx' PageId='10060' />
                   <Page name='claimant.aspx' PageId='10061' />
                   <Page name='defendant.aspx' PageId='10062' />
                   <Page name='demandoffer.aspx' PageId='10063' />
                   <Page name='expert.aspx' PageId='10064' />
                   <Page name='litigation.aspx' PageId='10065' />
                   <Page name='leave.aspx' PageId='10066' />
                   <Page name='subrogation.aspx' PageId='10067' />
                   <Page name='propertyloss.aspx' PageId='10068' />
                   <Page name='liabilityloss.aspx' PageId='10069' />
                   <Page name='piinjury.aspx' PageId='10070' />
                   <Page name='salvage.aspx' PageId='10071' />
                   <Page name='pidriver.aspx' PageId='10072' />
                   <Page name='piemployee.aspx' PageId='10073' />
                   <Page name='pidependent.aspx' PageId='10074' />
                   <Page name='pirestriction.aspx' PageId='10075' />
                   <Page name='piworkloss.aspx' PageId='10076' />
                   <Page name='pimedstaff.aspx' PageId='10077' />
                   <Page name='pimedstaffprivilege.aspx' PageId='10078' />
                   <Page name='pimedstaffcertification.aspx' PageId='10079' />
                   <Page name='piother.aspx' PageId='10080' />
                   <Page name='jurisdictionlicensecodes.aspx' PageId='10081' />
                   <Page name='pipatient.aspx' PageId='10082' />
                   <Page name='piprocedure.aspx' PageId='10083' />
                   <Page name='piphysician.aspx' PageId='10084' />
                   <Page name='providercontract.aspx' PageId='10085' />
                   <Page name='piprivilege.aspx' PageId='10086' />
                   <Page name='picertification.aspx' PageId='10087' />
                   <Page name='pieducation.aspx' PageId='10088' />
                   <Page name='piprevhospital.aspx' PageId='10089' />
                   <Page name='piwitness.aspx' PageId='10090' />
				   <Page name='Offset.aspx' PageId='10091' />
					<Page name='SelectClaimant.aspx' PageId='10092' />                                   
                    <Page name='casemanagement.aspx' PageId='10093' />
                    <Page name='casemgrnotes.aspx' PageId='10094' />
					<Page name='DCIMapping.aspx' PageId='301' />
					<Page name='TPACodeMapping.aspx' PageId='302' />
				    <Page name='AvailableTPA.aspx' PageId='303' />
				    <Page name='ProcessCodeMapping.aspx' PageId='304' /> 
					                   
                    <Page name='DocumentMapping.aspx' PageId='501' />
                    <Page name='DocMapForm.aspx' PageId='502' />
                    <Page name='UserDataMapping.aspx' PageId='503' />
	                <Page name='UserDataMapForm.aspx' PageId='504' />
	                <Page name='FUPFileLayout.aspx' PageId='505' />
	                <Page name='FileMarkMappings.aspx' PageId='506' />
	                <Page name='FileMarkMapForm.aspx' PageId='507' />

                    <Page name='MultiLangDateFormat.aspx' PageId='280' />
                    <Page name='MultiLanguage.aspx' PageId='281' />
                    <Page name='personinvolvedlist.aspx' PageId='508' />  
                    <Page name='cmxtreatmentplnlist.aspx' PageId='282' /> 
					<Page name='cmxmedmgtsavingslist.aspx' PageId='283' /> 
                    <Page name='piworklosslist.aspx' PageId='284' /> 
                    <Page name='pirestrictionlist.aspx' PageId='285' /> 
                    <Page name='AdditionalData.aspx' PageId='481' />
                    <Page name='PVEditField.aspx' PageId='20002' />
                    <Page name='CustomizeSettings.aspx' PageId='286' />
                    <Page name='TransAsync.aspx' PageId='551'/>
                    <Page name='trans.aspx' PageId='20003' />
                    <Page name='FinancialDetailHistory.aspx' PageId='290' />
                    <Page name='rmANgGrid.aspx' PageId='291' />
                    <Page name='PendingClaims.aspx' PageId='544' />
                    <Page name='ReserveCurrent.aspx' PageId='20005' />
                    <Page name='QuickLookup.aspx' PageId='552' />
 					<Page name='MoveFinancials.aspx' PageId='20004' />
                    <Page name='ApproveTrans.aspx' PageId='561' /> 
					<Page name='deductibledetail.aspx' PageId='10093' />
					<Page name='Address.aspx' PageId='20006' /> 
                    <Page name='ReserveTransactionDetail.aspx' PageId='562' />
                    <Page name='AsyncLookUpNavigation.aspx' PageId='563'/>				    
                    <Page name='AutoMailMergefilterWizard.aspx' PageId='564'/>		
                     <Page name='LimitTracking.aspx' PageId='292' />
                   <Page name='PolicyLimits.aspx' PageId='293' />

                    <Page name='ReserveSummary.aspx' PageId='565' />
                 </MultiLanguagePageList> 
             ");//10092 added by GBINDRA MITS#34104 02062014 WWIG GAP 15
            //10096 added by TTUMULA2 for Sprint2
            //282,283,284 added by CAGGARWAL4 for Sprint2
             //285,10093,10094 added by CAGGARWAL4 for Sprint3  
             //Bkuzhanthaim : MITS- 36027/RMA-342:250 added :auto expansion of all adjusters on load of claim.
             // RMA-345 AysncSearchResult.aspx page added for ML support
             //added PageID=20002 for JIra 6411 asharma326
            //RMA-5566  achouhan3   Added Reserve/Worksheet Approval page for ML Support
            //RMA-6404  achouhan3   Added to implement Angular grid on Supervisory Approval
			//RMA-8753 aahuja21,nshah28 (Added Address and DupAddress page for ML support)
             //RMA-11108 dvatsa Added AsyncLookUpNavigation.aspx page for ML support
             //RMA-16584 nshah28 (Added ReserveSummary page for ML support)

             return oTemplate;
        }
        /// <summary>
        /// Enum for ResourceType
        /// </summary>
        public enum RessoureType:int
        {
            LABEL=0,
            ALERTMESSAGE=1,
            TOOLTIP=2,
            ERROR_WARNING_MSG=3,
            BUTTON=4
        }
    }
}