﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Globalization;
using System.Web.SessionState;

namespace Riskmaster.RMXResourceManager
{
    public class JavaScriptResourceHandler
    {
        private static string[] sDataSeparator = { "|^|" };
        /// <summary>
        /// Get All the Alert messages in an array
        /// </summary>
        /// <param name="context"></param>
        /// <param name="p_PageId"></param>
        /// <param name="p_Varname"></param>
        /// <param name="p_ResType"></param>
        /// <returns></returns>
        public static string ProcessRequest(HttpContext context, string p_PageId, string p_Varname,string p_ResType)
        {
            HttpRequest Request = HttpContext.Current.Request;
            Dictionary<string, string> resDict = null;
            try
            {
                resDict = RMXResourceProvider.GetAllObjectByResourceType(p_PageId, p_ResType);
            }
            catch (Exception e)
            {
            }
            // filter the list to strip out controls (anything that contains a . in the ResourceId
            //resDict = resDict.Where(res => !res.Key.Contains('.') && res.Value is string)
            //          .ToDictionary(dict => dict.Key, dict => dict.Value);

            string javaScript = SerializeResourceDictionary(resDict, p_Varname);

            return javaScript;
        }

        /// <summary>
        /// Get All the Alert messages in an global array
        /// </summary>
        /// <param name="context"></param>
        /// <param name="p_PageId"></param>
        /// <param name="p_Varname"></param>
        /// <param name="p_ResType"></param>
        /// <returns></returns>
        public static string GlobalProcessRequest(HttpContext context, string p_Varname)
        {
            HttpRequest Request = HttpContext.Current.Request;
            Dictionary<string, string> resDict = null;
            string sLanguageCode = AppHelper.GetLanguageCode();
            try
            {
                resDict = RMXResourceProvider.GetAllGlobalResourceByLanguage(0, sLanguageCode);
				//Aman added the language code
                if (!resDict.ContainsKey("LanguageCode"))
                {
                    resDict.Add("LanguageCode", sLanguageCode);
                }
                else
                {
                    resDict["LanguageCode"] = sLanguageCode;
                }
				//Aman added the language code
            }
            catch (Exception e)
            {
            }
            string javaScript = SerializeResourceDictionary(resDict, p_Varname);
            return javaScript;
        }
        /// <summary>
        /// GlobalProcessSpecificKeyRequest
        /// </summary>
        /// <param name="context"></param>
        /// <param name="p_sKey"></param>
        /// <param name="p_Varname"></param>
        /// <returns></returns>
        public static string GlobalProcessSpecificKeyRequest(HttpContext context,string p_sKey, string p_Varname)
        {
            HttpRequest Request = HttpContext.Current.Request;
            Dictionary<string, string> resDict = null;
            try
            {
                resDict = new Dictionary<string, string>();
                resDict.Add(p_sKey, RMXResourceProvider.GetSpecificGlobalObject(p_sKey));
                return SerializeResourceDictionary(resDict, p_Varname);
            }
            catch (Exception e)
            {
                return "";
            }
        }
        /// <summary>
        /// Get Alert messages in an array
        /// </summary>
        /// <param name="context"></param>
        /// <param name="p_PageId"></param>
        /// <param name="p_ResType"></param>
        /// <param name="p_sKey"></param>
        /// <param name="p_Varname"></param>
        /// <returns></returns>
        public static string ProcessSpecificKeyRequest(HttpContext context, string p_PageId, string p_ResType, string p_sKey, string p_Varname)
        {
            HttpRequest Request = HttpContext.Current.Request;
            Dictionary<string, string> resDict = null;
            try
            {
                resDict = new Dictionary<string, string>();
                resDict.Add(p_PageId + "|^|" + p_ResType + "|^|" + p_sKey, RMXResourceProvider.GetSpecificObject(p_sKey, p_PageId, p_ResType));
                return SerializeResourceDictionary(resDict, p_Varname);
            }
            catch (Exception e)
            {
                return "";
            }
        }

        /// <summary>
        /// Generates the actual JavaScript object map string makes up the
        /// handler's result content.
        /// </summary>
        /// <param name="resxDict"></param>
        /// <param name="varname"></param>
        /// <returns></returns>
        public static string SerializeResourceDictionary(Dictionary<string, string> resxDict, string varname)
        {
            if (resxDict == null)
                return string.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append("var " + varname + " = {\r\n");
            string IsShowErrorCode = RMXResourceProvider.IsShowErrorCode();
            int anonymousIdCounter = 0;
            foreach (KeyValuePair<string, string> item in resxDict)
            {
                string value = item.Value as string;
                if (value == null)
                    continue; // only encode string values

                string key = item.Key;
                if (string.IsNullOrEmpty(item.Key))
                    key = "__id" + anonymousIdCounter++.ToString();

                key = key.Replace(".", "_");
                if (key.Contains(" "))
                    key = ToCamelCase(key);
                if (key.Split(sDataSeparator, StringSplitOptions.None).Length > 2)
                {
                    sb.Append("\t" + key.Split(sDataSeparator, StringSplitOptions.None)[2] + ": ");
                }
                else
                {
                    sb.Append("\t" + key + ": ");
                }
                if (IsShowErrorCode.ToUpper() == "TRUE")
                {
                    sb.Append(EncodeJsString(value));
                }
                else
                {
                    sb.Append(EncodeJsString(RemoveErrorCode(value)));
                }
                sb.Append(",\r\n");
            }
            sb.Append("}");

            // strip off ,/r/n at end of string (if any)
            sb.Replace(",\r\n}", "\r\n}");
            sb.Append(";\r\n");
            return sb.ToString();
        }
        /// <summary>
        /// Remove Error code from Resource Value
        /// </summary>
        /// <param name="sKeyValue"></param>
        /// <returns></returns>
        private static string RemoveErrorCode(string sKeyValue)
        {
            if (sKeyValue.StartsWith("rmA-"))
            {
                string sValue = sKeyValue.Substring(sKeyValue.IndexOf(":") + 1);
                return sValue;
            }
            else
            {
                return sKeyValue;
            }
        }
        /// <summary>
        /// Encodes a string to be represented as a string literal. The format
        /// is essentially a JSON string that is returned in double quotes.
        /// 
        /// The string returned includes outer quotes: 
        /// "Hello \"Rick\"!\r\nRock on"
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string EncodeJsString(string s)
        {
            if (s == null)
                return "null";

            StringBuilder sb = new StringBuilder();
            sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            sb.Append("\"");


            return sb.ToString();
        }

        /// <summary>
        /// Converts the phrase to specified convention.
        /// </summary>
        /// <param name="phrase"></param>
        /// <param name="cases">The cases.</param>
        /// <returns>string</returns>
        static string ToCamelCase(string phrase)
        {
            string[] splittedPhrase = phrase.Split(' ', '-', '.');
            var sb = new StringBuilder();
            sb.Append(splittedPhrase[0].ToLower());
            splittedPhrase[0] = string.Empty;
            foreach (String s in splittedPhrase)
            {
                char[] splittedPhraseChars = s.ToCharArray();
                if (splittedPhraseChars.Length > 0)
                {
                    splittedPhraseChars[0] = ((new String(splittedPhraseChars[0], 1)).ToUpper().ToCharArray())[0];
                }
                sb.Append(new String(splittedPhraseChars));
            }
            return sb.ToString();
        }


        /// <summary>
        /// Returns an error response to the client. Generates a 404 error
        /// </summary>
        /// <param name="Message">Error message to display</param>
        private void SendErrorResponse(string Message)
        {
            if (!string.IsNullOrEmpty(Message))
                Message = "Invalid Web Resource";

            HttpContext Context = HttpContext.Current;

            Context.Response.StatusCode = 404;
            Context.Response.StatusDescription = Message;
            Context.Response.End();
        }
    }
}
