﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Util;
namespace Riskmaster.UI.App_Code
{
    public class CustomRequestValidation : RequestValidator
    {
        private static string regex = ConfigurationManager.AppSettings["ValidationRegex"];
        public CustomRequestValidation() { }

        protected override bool IsValidRequestString(HttpContext context, string value,
            RequestValidationSource requestValidationSource, string collectionKey,
            out int validationFailureIndex)
        {
            try
            {
                validationFailureIndex = -1;  //Set a default value for the out parameter.        

                if (requestValidationSource == RequestValidationSource.Form)
                {

                    //If the form data contains less than and greater than characters then use logic to identify and send it to asp.net validation else validate. 

                    if (!(Regex.IsMatch(value, regex, RegexOptions.IgnoreCase)))
                    {
                        validationFailureIndex = -1;
                        return true;
                    }
                    else
                    {
                        if ((collectionKey.Contains("zapatec")) || (collectionKey.ToUpper().Contains("GRID")))
                        {
                            validationFailureIndex = -1;
                            return true;
                        }
                        //Leave any further checks to ASP.NET.
                        return base.IsValidRequestString(context, value, requestValidationSource,
                        collectionKey, out validationFailureIndex);
                    }
                }
                else
                {
                    return base.IsValidRequestString(context, value, requestValidationSource,
                                                     collectionKey, out validationFailureIndex);
                }
            }
            catch
            {
                return base.IsValidRequestString(context, value, requestValidationSource,
                                                    collectionKey, out validationFailureIndex);

            }
        }
    }
}