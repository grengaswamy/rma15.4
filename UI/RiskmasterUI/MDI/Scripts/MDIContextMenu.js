/************************************************************
* Subject: context menu functions
* Author: Nima Norouzi/FSG/CSC 
* Date: August 2008
************************************************************/

window.onscroll = function(event) {
    hidenodemenu();
}

function highitem(i) {
//hight light the item since the mouse pointer is over it
    i.className="nodemenuitemhigh";
}

function highidleitem(i) {
//hight light the idle item since the mouse pointer is over it
    i.className = "nodemenuidleitemhigh";
}
function lowitem(i) {
//change the item's style to normal since the mouse pointer is not over it any more
    i.className = "nodemenuitem";
}

function hidenodemenu() {
//hide the nodeMenu
    var menu = document.getElementById('nodeMenu');
    menu.style.display = 'none';
}

function keepnodemenu() {
//still keep the existing nodeMenu visible since the mouse pointer is over it
    var menu = document.getElementById('nodeMenu');
    menu.style.display = 'block';
}

function startnodemenu(e, p2) {

    window.scrollTo(0, 0);
    //start to create and show a new nodeMenu

    var ff = document.getElementById && !document.all
    var ie = document.all && document.getElementById
    var menu = document.getElementById('nodeMenu');
    if (document.getElementById('hdnIsNodeTreeShown').value == "false") {
        $("#nodeMenu").css("z-index", "99999");
        $("#nodeMenu").css("position", "absolute");
    }
    //delete items from the previous nodeMenu if existing.
    var i = 0;
    while (i < menu.childNodes.length) {
        child = menu.childNodes[i];
        if (child.nodeType === 1) {
            menu.removeChild(child);
        }
        else {
            i++;
        }
    }

    //turn all capturing and bubbling off to prevent any conflict with the browser right-click menu
    if (ie)
        window.event.cancelBubble = true;
    else if (ff)
        e.stopPropagation();

    if (p2 != null) {
        var list = p2.split(',');
        
        //make the nodeMenu visible
        menu.style.display = 'block';
        
        //call this function to start adding items to the nodeMenu
        additem(p2, 0, 0);

        //Find the distances between mouse pointer and right edge & between mouse pointer and bottomedge
        var rightedge = ie ? window.top.document.body.offsetWidth - event.clientX :
        window.innerWidth - e.clientX

        if (rightedge < menu.style.width)
        //if the rightedge distance is not enough for the width of the nodeMenu    
        //create the nodeMenu on the left of mouse pointer    
            menu.style.left=ie? 
                document.body.scrollLeft+event.clientX-menu.offsetWidth : 
                window.pageXOffset+e.clientX-menu.offsetWidth+"px"
        else
        //create the nodeMenu on the right of mnouse pointer
            menu.style.left=ie? document.body.scrollLeft+event.clientX : 
                window.pageXOffset+e.clientX+"px"


        var bottomedge = ie ? window.top.document.body.offsetHeight - event.clientY :
            window.innerHeight - e.clientY
        
        if (bottomedge < getMenuHeight(list.length) + 100)
        //create the nodeMenu above the mouse pointer
            menu.style.top = ie ?
                document.body.scrollTop + event.clientY - getMenuHeight(list.length) + bottomedge - 100 : 
                //window.pageYOffset+e.clientY-menu.offsetHeight+"px" //----sgupta320: Jira-17040
                window.pageYOffset + e.clientY - getMenuHeight(list.length) + "px"
        else
        //create the nodeMenu under the mouse pointer    
            menu.style.top=ie? document.body.scrollTop+event.clientY : 
                window.pageYOffset+e.clientY+"px"
    }
    return false;
}

function additem(p2, i, maxwidth) {
    //find the item starting at position i in the passing list, and add it to the nodeMenu
    
    var ratio;
    var div;
    var t;
    var list = p2.split(',');    
    //list[i] is the text on Menu, list[i+1] is its value, list[i+2] is its action
    if (maxwidth < list[i].length)
        maxwidth = list[i].length;

    var ff = document.getElementById && !document.all
    var ie = document.all && document.getElementById
    var menu = document.getElementById('nodeMenu');
    div = document.createElement("div");
    div.setAttribute('id', list[i]);
    div.className = "nodemenuitem";
    if (list[i + 2] == "NON") {
        div.innerHTML = "<span style='font-weight:bold'>" + list[i] + "</span>";
        div.onmouseover = new Function('highidleitem(this)');
    }
    else if (list[i + 2] == "PIE") {
        div.innerHTML = "<span onclick=\"MDIAddExistingPersonForPI('" + list[i + 1] + "');hidenodemenu();\">" + list[i].replace(' ', "&nbsp;") + "</span>";
        div.onmouseover = new Function('highitem(this)');
    }    
    else {
        div.innerHTML = "<span onclick=\"MDI('" + list[i + 1] + "', '" + list[i + 2] + "');hidenodemenu();\">" + list[i].replace(' ', "&nbsp;") + "</span>";
        div.onmouseover = new Function('highitem(this)');
    }
    div.onmouseout = new Function('lowitem(this)');

    menu.appendChild(div);
    //adjust the width and height of nodeMenu based on added the item
    if (ie) {
        menu.style.height = getMenuHeight(i);
        menu.style.width = 7 * maxwidth;
    }
    else if (ff) {
        menu.style.height = getMenuHeight(i) + "px";
        menu.style.width = (7 * maxwidth) + "px";
    }

    i += 3;
    if (i < list.length) {
    //put some delay and add the next item; so that the nodeMenu appears smoothly    
        t = setTimeout("additem('" + p2 + "'," + i + "," + maxwidth + ")", 50);
    }
}

function getMenuHeight(i) {
    var ff = document.getElementById && !document.all
    var ie = document.all && document.getElementById
    if (ie) {
        if (i == 0)
            ratio = 0.9;
        else
            ratio = Math.floor((i / 3) * (i + 4) / i);
            
        return Math.floor(18 * ratio);
    }
    else {
        //----sgupta320: Jira-17040
        return 5 * (i + 1) * Math.ceil(1 / (i + 1));
        //return 18 * (i + 1) * Math.ceil(1 / (i + 1));
    }
}
