﻿///********************************************************************
/// Amendment History
///********************************************************************
/// Date Amended   *            Amendment               *    Author
/// 02/10/2014         MITS 34260 - duplicate claim         Ngupta36
/// 20/11/2014         JIRA 4362 - claim  Policy expansion        Ngupta36
///********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections;
using Riskmaster.Models;
//using Riskmaster.UI.MDINavigationTreeNodeProfileService;

namespace Riskmaster.MDI.App_Code
{
    public class MDINavigationTreeProcess 
    {
        public string UserName { get; set; }
        public string DataSource { get; set; }

        public string PreviousNodeValuePath;
        public string NextSearchResultValue;
        public long SearchResultRecordId;
        public string ReachedMaxSearchScreens;
        public string AllowedSearchScreens;
        public long LastNumericId;
        public string Script;
        private string RelativePath;
        private string FDMLocalPath;
        public string Breadcrumb;
        public string CurrentClaimMDIId;
        private const string FileExt = ".aspx";

        public MDINavigationTreeProcess(string relativePath, string FDMLocalPath, string previousNodeValuePath, string previousScript, string searchResultRecordId, string allowedSearchScreens, string reachedMaxSearchScreens, string lastNewId, string breadcrumb, string currentClaimMDIId)
        {
            this.RelativePath = relativePath;
            this.FDMLocalPath = FDMLocalPath;
            this.PreviousNodeValuePath = previousNodeValuePath;
            this.Script = previousScript;
            this.SearchResultRecordId = Convert.ToInt64(searchResultRecordId);
            this.AllowedSearchScreens = allowedSearchScreens;
            this.LastNumericId = Convert.ToInt64(lastNewId);
            this.Breadcrumb = breadcrumb;
            this.CurrentClaimMDIId = currentClaimMDIId;
            /*this.AllowedScreens = allowedScreens;
            this.SearchScreens = searchScreens;
            this.Screens = screens;*/
        }

        private System.Web.UI.WebControls.TreeNode RefreshNode(System.Web.UI.WebControls.TreeNode tn, MDITreeNodeHint hint)
        {
            MDITreeNodeHint parentHint = new MDITreeNodeHint(hint.ParentPathValue, false);

            if (hint.NodeNewRecordId > 0) 
            {
                hint.NodeRecordId = hint.NodeNewRecordId;
            }
            hint.SetMDIId(parentHint.NodeMDIId);

            hint.NodePathValue = parentHint.NodePathValue + MDITreeNodeHint.PathValueSeparator + hint.GetNodeValue();
            hint.PopulateHierarchyProperties(); //this will take care of refreshing breadcrumb

            tn.Text = hint.GetNodeHTMLText();
            //tn.ToolTip = hint.NodePlainText;
            tn.Value = hint.GetNodeValue();
            return tn;
        }

        private System.Web.UI.WebControls.TreeNode NewNode(MDITreeNodeHint hint)
        {
            TreeNode tn = new TreeNode();
            tn.Text = hint.GetNodeHTMLText();
            //tn.ToolTip = hint.NodePlainText;
            tn.Value = hint.GetNodeValue();
            if (hint.NodeScreen == string.Empty)
                tn.SelectAction = TreeNodeSelectAction.None;
            tn.Select();
            return tn;
        }

        private System.Web.UI.WebControls.TreeNode NewRoot(string Root,string RootTitle)
        {
            MDITreeNodeHint hint2 = new MDITreeNodeHint(null
                , false);
            hint2.IsRoot = true;
            hint2.Root = Root;
            hint2.RootTitle = string.IsNullOrEmpty(RootTitle) ? "Document" : RootTitle;
            hint2.SetMDIId(Root);
            hint2.NodeRecordId = 0;
            hint2.NodeScreen = string.Empty;
            hint2.NodePlainText = Root;
            return NewNode(hint2);
        }

        private string SetLengthTo2(string textHelp)
        {
            if (textHelp.Length == 1)
            {
                textHelp = (char)48 + textHelp; //making it a two digit phrase
            }
            return textHelp;
        }

        private long GetNewSearchChildId()
        {
            string currentTime = SetLengthTo2(DateTime.Now.Day.ToString()) + SetLengthTo2(DateTime.Now.Hour.ToString()) + SetLengthTo2(DateTime.Now.Minute.ToString()) + SetLengthTo2(DateTime.Now.Second.ToString());
            return Convert.ToInt64(currentTime);
        }

        private long GetNewSearchResultId()
        {
            return this.SearchResultRecordId++;
        }

        //For now we get rid of "Claims" node; if the # of claims under one event happens to be a lot and customers really need the "Claims" node, we will add it again...
        private MDITreeNodeHint GetHintForClaimsNode(TreeNode parent)
        {
            MDITreeNodeHint hint = new MDITreeNodeHint(null, false);
            hint.Root = "Document";
            hint.NodeRecordId = 0;
            hint.NodeScreen = string.Empty;
            hint.NodePlainText = "Claims";

            MDITreeNodeHint parentHint = new MDITreeNodeHint(parent.Value, false);
            hint.SetMDIId(parentHint.NodeMDIId);
            hint.ParentPathValue = parent.ValuePath;
            hint.NodePathValue = hint.ParentPathValue + MDITreeNodeHint.PathValueSeparator + hint.GetNodeValue();
            return hint;
        }


        private System.Web.UI.WebControls.TreeNode InitFDMNode(MDITreeNodeHint hint, string parentPathValue)
        {
            MDITreeNodeHint parentHint = new MDITreeNodeHint(parentPathValue, false);
            hint.SetMDIId(parentHint.NodeMDIId);

            hint.NodePathValue = parentPathValue + MDITreeNodeHint.PathValueSeparator + hint.GetNodeValue();
            hint.PopulateHierarchyProperties();
            return NewNode(hint);
        }

        private System.Web.UI.WebControls.TreeNode InitResultNode(MDITreeNodeHint hint, string parentPathValue)
        {
            MDITreeNodeHint hint2 = new MDITreeNodeHint(parentPathValue, false);
            hint.NodeScreen = "Result";
            hint.NodeRecordId = GetNewSearchResultId();
            //this.ReachedMaxSearchScreens = ReachedMaximumSearchScreens();
            hint.SetMDIId(hint2.NodeMDIId);
            hint.NodePlainText = hint.NodeScreen + " " + SetLengthTo2(DateTime.Now.Hour.ToString()) + ":" + SetLengthTo2(DateTime.Now.Minute.ToString()) + ":" + SetLengthTo2(DateTime.Now.Second.ToString());

            hint.NodePathValue = parentPathValue + MDITreeNodeHint.PathValueSeparator + hint.GetNodeValue();
            hint.PopulateHierarchyProperties();

            return NewNode(hint);
        }

        private System.Web.UI.WebControls.TreeNode InitCriteriaNode(MDITreeNodeHint hint, string parentPathValue)
        {
            hint.NodeRecordId = GetNewSearchChildId();
            hint.SetMDIId(hint.Root);
            hint.NodePlainText = hint.NodeTitle + " Criteria";
            hint.ParentPathValue = parentPathValue;

            hint.NodePathValue = parentPathValue + MDITreeNodeHint.PathValueSeparator + hint.GetNodeValue();
            hint.PopulateHierarchyProperties();

            return NewNode(hint);
        }

        private System.Web.UI.WebControls.TreeNode InitOtherNode(MDITreeNodeHint hint, string parentPathValue)
        {
            hint.SetMDIId(hint.Root);

            hint.NodePlainText = hint.NodeTitle;

            hint.NodePathValue = parentPathValue + MDITreeNodeHint.PathValueSeparator + hint.GetNodeValue();
            hint.PopulateHierarchyProperties();

            return NewNode(hint);
        }

        private void SwitchToSearchScreen(MDITreeNodeHint hint)
        {
            if (this.ReachedMaxSearchScreens == "YES")
            {
                this.Script = "MDI('You have reached the maximum number of allowed Search screens! For more screens please remove some of the existing ones...', 'Msg');";
            }
            else if (hint.NodeScreen.ToLower() == "result")
            {
                this.Script = "MDI('" + hint.NodeMDIId + "', 'about:blank');";
            }
            else
            {
                this.Script = "MDI('" + hint.GetNodeValue() + "', '" + this.RelativePath + MDITreeNodeHint.DecodeSpecialChars(hint.NodeLocalPath) + MDITreeNodeHint.DecodeSpecialChars(hint.NodeParam) + "');";
                if (this.SearchResultRecordId == -1)
                    this.NextSearchResultValue = "MaxReached";
                else
                    this.NextSearchResultValue = hint.NodeMDIId + this.SearchResultRecordId;
            }
        }

        private void SwitchToFDMScreen(MDITreeNodeHint hint)
        {
            string path = this.RelativePath;
            string localPath = hint.NodeLocalPath;
            this.Script = string.Empty;
            if (localPath == string.Empty) // For FDM screens(the ones that are accessed step by step walking down to the hierarchy) the localPath is empty at this stage because they are not in MDIMenu.xml file
            {
                localPath = this.FDMLocalPath;
            }
            if (this.ReachedMaxSearchScreens == "YES")
            {
                this.Script = "MDI('You have reached the maximum number of allowed Search screens! For more screens please remove some of the existing ones...', 'Msg');";
            }
            else if (hint.IsRefreshed && !hint.IsReloaded)
            {
                this.Script = "MDI('" + hint.GetNodeValue() + "', '" + hint.NodeInitialValue + "');";
            }
            else if (hint.NodeScreen == "reservelisting")
            {
                MDITreeNodeHint hintParent = new MDITreeNodeHint(hint.NodePathValue, "MDIParent", false);
                string ScreenNavigate = "";
                switch (hintParent.NodeScreen)
                {
                    case "claimant":
                        if (hint.IsBOB == true)
                        {
                            MDIClaimantFinancialReq oMDIClaimantFinancialReq = new MDIClaimantFinancialReq();
                            MDIClaimantFinancialRes oMDIClaimantFinancialRes = new MDIClaimantFinancialRes();
                            oMDIClaimantFinancialReq.ClaimantRowId =  hintParent.NodeRecordId.ToString();
                            oMDIClaimantFinancialReq.Token = AppHelper.ReadCookieValue("SessionId");
                            oMDIClaimantFinancialReq.ClientId = AppHelper.ClientId;
                            oMDIClaimantFinancialRes = AppHelper.GetResponse<MDIClaimantFinancialRes>("RMService/MDI/GetClaimantDetails", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oMDIClaimantFinancialReq);
                            ScreenNavigate = "ReserveListingBOB.aspx";
                            
                                path += "UI/Reserves/" + ScreenNavigate + "?ClaimId=" + oMDIClaimantFinancialRes.ClaimID.ToString() + "&ClaimantListClaimantEID=" + oMDIClaimantFinancialRes.ClaimantEID.ToString() + "&parentsysformname=claimant";
                        }
                        else
                        {
                            path += "UI/Reserves/ReserveListing.aspx?ClaimId=" + hintParent.NodeParentId.ToString() + "&ClaimantId=" + hintParent.NodeRecordId.ToString();                         
                        }
                        break;
                    case "unit":
                        //pgupta93: RMA-4608 START
                        //path += "UI/Reserves/ReserveListing.aspx?ClaimId=" + hintParent.NodeParentId.ToString() + "&UnitRowId=" + hintParent.NodeRecordId.ToString();
                        path += "UI/Reserves/ReserveListing.aspx?ClaimId=" + hintParent.NodeParentId.ToString() + "&UnitRowId=" + hintParent.NodeRecordId.ToString() + "&FormName=" + hintParent.NodeScreen.ToString();
                        //pgupta93: RMA-4608 END
                        break;

                    default:
                        if (hint.IsBOB == true)
                        {
                            ScreenNavigate = "ReserveListingBOB.aspx";
                        }
                        else
                        {
                            ScreenNavigate = "ReserveListing.aspx";
                        }
                        path += "UI/Reserves/"+ScreenNavigate+"?ClaimId=" + hintParent.NodeRecordId.ToString();
                        break;
                }
            }
            else if (hint.NodeScreen.ToLower() == "reserveworksheet")
            {
                MDITreeNodeHint hintParent = new MDITreeNodeHint(hint.NodePathValue, "DataModelParent", false);
                if (hint.NodeRecordId == 0)
                {
                    if (hintParent.IsClaim)
                    {
                        path += "UI/LookupData/LookUpNavigation.aspx?sysformname=" + hint.NodeScreen + "&parentID=" + hint.NodeParentId + "&parentsysformname=claim";
                    }
                    else
                    {
                        path += "UI/LookupData/LookUpNavigation.aspx?sysformname=" + hint.NodeScreen + "&parentID=" + hint.NodeParentId + "&parentsysformname=" + hintParent.NodeScreen;
                    }
                }
                else
                {
                    switch (hintParent.NodeScreen)
                    {
                        case "claimant":
                            path += "UI/ReserveWorkSheet/ReserveWorkSheet.aspx?ClaimId=" + hintParent.NodeParentId.ToString() + "&ClaimantId=" + hintParent.NodeRecordId.ToString() + "&RSWId=" + hint.NodeRecordId.ToString();
                            break;
                        case "unit":
                            path += "UI/ReserveWorkSheet/ReserveWorkSheet.aspx?ClaimId=" + hintParent.NodeParentId.ToString() + "&UnitId=" + hintParent.NodeRecordId.ToString() + "&RSWId=" + hint.NodeRecordId.ToString();
                            break;
                        default:
                            path += "UI/ReserveWorkSheet/ReserveWorkSheet.aspx?ClaimId=" + hintParent.NodeRecordId.ToString() + "&RSWId=" + hint.NodeRecordId.ToString();
                            break;
                    }
                }
            }
            else if (hint.NodeRecordId == 0 && hint.NodeLocalPath != string.Empty) // like injected Diary List(Filtered) or Enhanced Notes
            {
                if (hint.NodeLocalPath.Contains((char)46)) // means localpath contains the filename and extension as well
                {
                    path += MDITreeNodeHint.DecodeSpecialChars(hint.NodeLocalPath) + MDITreeNodeHint.DecodeSpecialChars(hint.GetFinalizedNodeParam());
                }
                else
                {
                    path += MDITreeNodeHint.DecodeSpecialChars(hint.NodeLocalPath) + hint.NodeScreen + FileExt + MDITreeNodeHint.DecodeSpecialChars(hint.GetFinalizedNodeParam());
                }
            }
            else if (hint.NodeRecordId == 0 && hint.NodeScreen != "personinvolvedlist")
            {
             //dvatsa- JIRA 11108(start)
               List<string> lstNGScreens = new List<string>();
                lstNGScreens.Add("eventdatedtext");
                lstNGScreens.Add("adjuster");
                lstNGScreens.Add("adjusterdatedtext");
                lstNGScreens.Add("litigation");
                lstNGScreens.Add("subrogation");
                lstNGScreens.Add("arbitration");
                lstNGScreens.Add("propertyloss");
                lstNGScreens.Add("siteloss");
                lstNGScreens.Add("otherunitloss");
                lstNGScreens.Add("demandoffer");
                lstNGScreens.Add("expert");
                lstNGScreens.Add("claimant");
                lstNGScreens.Add("defendant");
                lstNGScreens.Add("vssassignment");
                lstNGScreens.Add("unit");
                lstNGScreens.Add("concomitant");
                lstNGScreens.Add("medwatchtest");
                lstNGScreens.Add("cmxcmgrhist");
                lstNGScreens.Add("cmxaccommodation");
                lstNGScreens.Add("casemgrnotes");
                lstNGScreens.Add("casemanagementlist");
                lstNGScreens.Add("liabilityloss");
                lstNGScreens.Add("claimxpolicy");
                if (lstNGScreens.Contains(hint.NodeScreen))
                {
                    MDITreeNodeHint hintParent = new MDITreeNodeHint(hint.NodePathValue, "DataModelParent", false);
                    path += "UI/LookupData/AsyncLookUpNavigation.aspx?sysformname=" + hint.NodeScreen + "&parentID=" + hint.NodeParentId + "&parentsysformname=" + hintParent.NodeScreen;
                   
                }
                 else
                {
                    //dvatsa- JIRA 11108 (end)
                    MDITreeNodeHint hintParent = new MDITreeNodeHint(hint.NodePathValue, "DataModelParent", false);
                    path += "UI/LookupData/LookUpNavigation.aspx?sysformname=" + hint.NodeScreen + "&parentID=" + hint.NodeParentId + "&parentsysformname=" + hintParent.NodeScreen;
                }
            }
            else if (hint.NodeRecordId != 0 && hint.NodeScreen == "casemanagementlist")
            {
                MDITreeNodeHint hintParent = new MDITreeNodeHint(hint.NodePathValue, "DataModelParent", false);
                path += localPath + "casemanagement" + FileExt + "?recordID=" + hint.NodeRecordId + "&parentID=" + hintParent.NodeRecordId + "&parentsysformname=" + hintParent.NodeScreen;
            }
            //MITS 34260 start
            else if (hint.NodeScreen == "claimxpolicy")
            {
                MDITreeNodeHint hintParent = new MDITreeNodeHint(hint.NodePathValue, "DataModelParent", false);
                path += localPath + "policy" + FileExt + "?recordID=" + hint.NodeRecordId + "&parentID=" + hint.NodeParentId + "&parentsysformname=" + hintParent.NodeScreen;
            }
            //MITS 34260 end
            else
            {
                MDITreeNodeHint hintParent = new MDITreeNodeHint(hint.NodePathValue, "DataModelParent", false);
                if (hint.NodeParam != string.Empty && !hint.NodeParamNeedsToBeFinalized())
                {
                    path += localPath + hint.NodeScreen.ToLower() + FileExt + hint.NodeParam + "&recordID=" + hint.NodeRecordId + "&parentID=" + hint.NodeParentId + "&parentsysformname=" + hintParent.NodeScreen;
                }
                else
                {
                    path += localPath + hint.NodeScreen.ToLower() + FileExt + "?recordID=" + hint.NodeRecordId + "&parentID=" + hint.NodeParentId + "&parentsysformname=" + hintParent.NodeScreen;
                }
            }

            if (this.Script == string.Empty)
            {
                this.Script = "MDI('" + hint.GetNodeValue() + "', '" + path + "');";
            }
        }

        private void SwitchToOtherScreen(MDITreeNodeHint hint)
        {
            if (this.ReachedMaxSearchScreens == "YES")
            {
                this.Script = "MDI('You have reached the maximum number of allowed Search screens! For more screens please remove some of the existing ones...', 'Msg');";
            }
            else if (hint.IsRefreshed)
            {
                this.Script = "MDI('" + hint.GetNodeValue() + "', '" + hint.NodeInitialValue + "');";
            }
            else
            {
                if (hint.NodeLocalPath.Contains((char)46)) // means localpath contains the filename and extension as well; usually we pre-provide filename and extension in MDIMenu.xml when we have multiple menu items with the same filename but different in parameters (e.g. Exec. Summaries); sysNames would be virtual for these.
                {
                    this.Script = "MDI('" + hint.GetNodeValue() + "', '" + this.RelativePath + MDITreeNodeHint.DecodeSpecialChars(hint.NodeLocalPath) + MDITreeNodeHint.DecodeSpecialChars(hint.GetFinalizedNodeParam()) + "');";
                }
                else
                {
                    this.Script = "MDI('" + hint.GetNodeValue() + "', '" + this.RelativePath + MDITreeNodeHint.DecodeSpecialChars(hint.NodeLocalPath) + hint.NodeScreen + FileExt + MDITreeNodeHint.DecodeSpecialChars(hint.GetFinalizedNodeParam()) + "');";
                }
            }

        }

        private TreeNode SetToBePopulatedOnDemand(TreeNode tn)
        {
            tn.ChildNodes.Clear();
            tn.ChildNodes.Add(new TreeNode("", "DUMMY"));
            tn.Collapse();
            return tn;
        }

        //Bkuzhanthaim : MITS- 36027/RMA-342: Optional Parameter CurrentProfile added
        //agupta298 :  RMA-5281 : Added Optional Parameterd PersonInvolved
        private void Populate(ref TreeNode tnSelected, MDINavigationTreeNodeProfileResponse CurrentProfile = null, Dictionary<string, bool> dPersonInvolved = null)
        {
            bool bAdjuster = false;
            bool bchildNodes = false;
            bool bAdjusterChild = false;
            int[] iADJ_ROW_ID = null;
            //Start : ngupta73 : MITS- 34260/RMA-4362
            bool bPolicy = false;
            //Start : sgupta320 : Jira-17400
            bool bPolicyChild = false; 
            int[] iPOL_ROW_ID = null;
            //End : ngupta73 : MITS- 34260/RMA-4362
            MDINavigationTreeNodeProfileResponse profile, profileADJ, profilePOL;
            MDITreeNodeHint hint = new MDITreeNodeHint(tnSelected.Value, false);
            MDITreeNodeHint hint2 = new MDITreeNodeHint(null, false);
            MDITreeNodeHint hint3 = new MDITreeNodeHint(null, false);
            //Start : ngupta73 : MITS- 34260/RMA-4362
            MDITreeNodeHint hint4 = new MDITreeNodeHint(null, false);
            int iChilds = tnSelected.ChildNodes.Count;
            //End : ngupta73 : MITS- 34260/RMA-4362
            if (CurrentProfile == null)
            {
                if (hint.NodeScreen == "reservelisting")
                {
                    MDITreeNodeHint hintParent = new MDITreeNodeHint(tnSelected.ValuePath, "DataModelParent", false);
                    profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeRecordId), hintParent.NodeScreen, Convert.ToInt32(hintParent.NodeRecordId));
                }
                else
                {
                    profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeRecordId));
                }
                CurrentProfile = profile;
            }

             //Start : ngupta73 : MITS- 34260/RMA-4362 : I had moved this code from UpdateNavTree Function and called Populate Function here
            if (CurrentProfile.NodeChildern != null)
            {
                if (tnSelected.ChildNodes.Count == CurrentProfile.NodeChildern.Count)
                {
                    bchildNodes = true;
                }
            }
            //End : ngupta73 : MITS- 34260/RMA-4362



            tnSelected.ChildNodes.Clear();
            if (CurrentProfile.NodeChildern != null)
            {
                TreeNode tnHelp = null;
                hint2.NodeParentId = (int)hint.NodeRecordId; // this line added only for Case Mgt. to show the pop-up message when the node is clicked but the parent claim has not been selected yet; eventually we replace the node with Case Managers as an immediate claim child, so we can remove this line also.
                //agupta298 :  RMA-5281 : Initialize PersonInvolved of MDITreeNodeHint object.
                if (dPersonInvolved != null)
                    hint2.PersonInvolved = dPersonInvolved;
                for (int i = 0; i < CurrentProfile.NodeChildern.Count; i++)
                {
                    //Bkuzhanthaim : MITS- 36027/RMA-342:auto expansion of all adjusters on load of claim.
                    if (CurrentProfile.NodeChildern[i][1].ToString() == "adjuster" && CurrentProfile.NodeChildern[i][2].Contains('/'))
                    {
                        iADJ_ROW_ID = Array.ConvertAll(CurrentProfile.NodeChildern[i][2].Split('/'), int.Parse);
                        CurrentProfile.NodeChildern[i][2] = iADJ_ROW_ID[0].ToString();
                        bAdjuster = true;
                    }
                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                    if (CurrentProfile.NodeChildern[i][1].ToString() == "claimxpolicy" && CurrentProfile.NodeChildern[i][2].Contains('/'))
                    {
                        iPOL_ROW_ID = Array.ConvertAll(CurrentProfile.NodeChildern[i][2].Split('/'), int.Parse);
                        CurrentProfile.NodeChildern[i][2] = iPOL_ROW_ID[0].ToString();
                        bPolicy = true;
                    }
                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.

                    hint2.ReadThisNarrowedProfile(CurrentProfile.NodeChildern[i]);

                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                    if (hint2.CouldPopulateOnDemand)
                    {
                        if (bchildNodes && tnHelp.ChildNodes.Count == 0)
                        {
                                //tnHelp = RefreshNode(tnHelp, hint2); //We might have created a new record for a one to one child (which is tab in parent screen). Now the one to one child has a recordId and we need to inject the id in the treenode's value so that its children can be identified on populating on demand.
                                tnHelp = SetToBePopulatedOnDemand(InitFDMNode(hint2, tnSelected.ValuePath));
                        }
                        else
                        {
                            tnHelp = SetToBePopulatedOnDemand(InitFDMNode(hint2, tnSelected.ValuePath));
                        }
                    }
                    else
                    {
                        tnHelp = InitFDMNode(hint2, tnSelected.ValuePath);
                        
                    }

                    //Start : ngupta73 : MITS- 34260/RMA-4362 : I had moved this code from UpdateNavTree Function and called Populate Function here
                    if(bchildNodes)
                    {
                        if (CurrentProfile.NodeChildern[i][1] == "unit" || CurrentProfile.NodeChildern[i][1] == "propertyloss" || CurrentProfile.NodeChildern[i][1] == "claimant" || CurrentProfile.NodeChildern[i][1] == "siteloss" || CurrentProfile.NodeChildern[i][1] == "otherunitloss" || CurrentProfile.NodeChildern[i][1] == "claimxpolicy" || CurrentProfile.NodeChildern[i][1] == "adjuster")
                        {
                            tnHelp = RefreshNode(tnHelp, hint2);
                        }
                    }
					//End : ngupta73 : MITS- 34260/RMA-4362

                    tnSelected.ChildNodes.Add(tnHelp);
                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                    
                    //Bkuzhanthaim : MITS- 36027/RMA-342:auto expansion of all adjusters on load of claim.
                    if (bAdjuster)
                    {
                        if (iADJ_ROW_ID[iADJ_ROW_ID.Count() - 1] == 1)
                        {

                        hint = new MDITreeNodeHint(null, false);
                        tnHelp = null;
                        for (int iRow = 1; iRow < iADJ_ROW_ID.Count() -1 ; iRow++) //iRow FirstIndex for Adjuster count and LastIndex for sysSettings
                        {
                            tnHelp = tnSelected.ChildNodes[tnSelected.ChildNodes.Count - 1];
                            profileADJ = GetProfile(CurrentProfile.NodeChildern[i][1].ToString(), iADJ_ROW_ID[iRow]);
                            hint3.ReadThisNarrowedProfile(profileADJ.NodeDetail[0]);
                            tnHelp.ChildNodes.Add(InitFDMNode(hint3, tnHelp.ValuePath));
                            //RMA-8405 : Adjuster dosn't having Childern(DatedText) for existing powerviews(adjuster.xml in VIEW_XML)
							if (profileADJ.NodeChildern.Count > 0)
                            {
 							    bAdjusterChild = true;
                                hint3 = new MDITreeNodeHint(null, false);
                                hint3.ReadThisNarrowedProfile(profileADJ.NodeChildern[0]);
                                hint3.NodeParentId = iADJ_ROW_ID[iRow];
                                tnHelp = tnHelp.ChildNodes[tnHelp.ChildNodes.Count - 1];
                                tnHelp.ChildNodes.Add(InitFDMNode(hint3, tnHelp.ValuePath));
                            }
                        }
                        bAdjuster = false;
                        if (bAdjusterChild)//while expanding claim the adjuster should be in expandtion mode.
                            tnHelp.Parent.Expand(); 
                        else
                            tnHelp.Expand(); 
                        }
                    }
                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                    if (bPolicy)
                    {
                        if (iPOL_ROW_ID[iPOL_ROW_ID.Count() - 1] == 1)
                        {

                            hint = new MDITreeNodeHint(null, false);
                            tnHelp = null;
                            for (int iRow = 1; iRow < iPOL_ROW_ID.Count() - 1; iRow++) //iRow FirstIndex for policy count and LastIndex for sysSettings
                            {
                                tnHelp = tnSelected.ChildNodes[tnSelected.ChildNodes.Count - 1];
                                profilePOL = GetProfile(CurrentProfile.NodeChildern[i][1].ToString(), iPOL_ROW_ID[iRow]);
                                hint4.ReadThisNarrowedProfile(profilePOL.NodeDetail[0]);
                                tnHelp.ChildNodes.Add(InitFDMNode(hint4, tnHelp.ValuePath));
                                //------sgupta320: Jira-17400 auto expansion of all PI on load of claim.
                                if (profilePOL.NodeChildern.Count > 0)
                                {
                                    bPolicyChild = true;
                                    hint4 = new MDITreeNodeHint(null, false);
                                    hint4.ReadThisNarrowedProfile(profilePOL.NodeChildern[0]);
                                    hint4.NodeParentId = iPOL_ROW_ID[iRow];
                                    if (dPersonInvolved != null)
                                        hint4.PersonInvolved = dPersonInvolved;
                                    tnHelp = tnHelp.ChildNodes[tnHelp.ChildNodes.Count - 1];
                                    tnHelp.ChildNodes.Add(InitFDMNode(hint4, tnHelp.ValuePath)); 
 
                                }
                            }
                            //----sgupta320: Jira-17400 while expanding claim the PI should be in expandtion mode.
                            bAdjuster = false;
                            if (bPolicyChild)
                                tnHelp.Parent.Expand();
                            else
                                tnHelp.Expand(); 
                            //-----sgupta320: commented for Jira-17400
                            //bPolicy = false;
                            //tnHelp.Expand();
                            ////----sgupta320
                            //tnHelp.Parent.Expand(); //To display the policy childrens in expandtion mode.
                            //------comment end--------------------------
                        }
                    }
                    BusinessRule(ref tnHelp, ref hint2);
                }
            }
            //tn.Expand();
        }

        private void MakeVisible(TreeNode tn)
        {
            while (tn.Parent != null)
            {
                tn.Parent.Expand();
                tn = tn.Parent;
            }
        }


        private TreeNode FindByMDIId(ref TreeNode tnRoot, string MDIId) //this 
        {
            int i = 0;
            int l = 0;
            int j = 0;
            string MDIIdHelp = string.Empty;
            string[] splitItems;
            TreeNode tnCurrent = null;
            if (!string.IsNullOrEmpty(MDIId) && tnRoot != null && tnRoot.Value != "Search")
            {
                tnCurrent = tnRoot;
                splitItems = MDIId.Split(MDITreeNodeHint.UnitSeparator);
                l = splitItems.Length;
                MDIIdHelp = splitItems[0];
                if (l == 1 && tnRoot.Value != MDIId)
                {
                    tnCurrent = null;
                }
                for (i = 1; i < l;)
                {
                    j = i;
                    MDIIdHelp += MDITreeNodeHint.UnitSeparator + splitItems[i];
                    foreach (TreeNode tnHelp in tnCurrent.ChildNodes)
                    {
                        if (tnHelp.Value.Substring(0, MDIIdHelp.Length) == MDIIdHelp)
                        {
                            tnCurrent = tnHelp;
                            i++;
                            break;
                        }
                    }
                    if (i == j) //MDIIdHelp was not found! In the last step it is MDIId; however in any step (MDIIdHelp can partially match MDIId) we realize it is not found we stop searching further...
                    {
                        tnCurrent = null;
                        break;
                    }
                }
            }

            return tnCurrent;
        }

        private TreeNode FindByValue(TreeNode parent, string childValue)
        {
            TreeNode tn2;
            foreach (TreeNode tn in parent.ChildNodes)
            {
                if (tn.Value == childValue)
                {
                    return tn;
                }
                else
                {
                    tn2 = FindByValue(tn, childValue);
                    if (tn2 != null)
                    {
                        return tn2;
                    }
                }
            }
            return null;
        }
        /*Added by gbindra WWIG Gap # 30 MITS # 35365 08/26/14*/
        //public System.Web.UI.WebControls.TreeView UpdateNavTree(System.Web.UI.WebControls.TreeView navTree, string eventArg,string sDocumentTitle,string sEventTitle)
        public System.Web.UI.WebControls.TreeView UpdateNavTree(System.Web.UI.WebControls.TreeView navTree, string eventArg, string sDocumentTitle, string sEventTitle, Dictionary<string,bool> dPersonInvolved)
        /*Added by gbindra WWIG Gap # 30 MITS # 35365 08/26/14*/
        {
            TreeNode tnSelected, tnToggled, tnHelp, tnRoot, tnParent;
            MDITreeNodeHint hint = new MDITreeNodeHint(eventArg, true);
            MDINavigationTreeNodeProfileResponse profile = null;
            MDINavigationTreeNodeProfileResponse parentProfile = null;
            bool duplicated = false;
            bool bIsClaimantOpened = false;
            Script = "hideAll();";

            tnSelected = navTree.SelectedNode;

            //if (hint.RunBusinessRule)
            //{
            //    BusinessRule(ref tnSelected, ref hint);
            //}

            //RMA-9462 - Start
            //This MDITreeNodeHint object is getting used in RefreshNode Function for  Person Involved screen.
            if (dPersonInvolved != null)
                hint.PersonInvolved = dPersonInvolved;
            //RMA-9462 - End

            if (hint.IsToggled)
            {
                if (Breadcrumb != string.Empty)
                {
                    Script = "MDI('','')"; // to do nothing; since we don't want to switch screens
                }
                tnToggled = navTree.FindNode(hint.NodePathValue);
                if (tnToggled != null && tnToggled.ChildNodes.Count == 1 && string.Compare(tnToggled.ChildNodes[0].Value, "DUMMY", true) == 0)
                {
                    hint.IsPopulated = true;
                }
            }
            else
            {
                tnToggled = null;
            }

            if (hint.IsRemoved)
            {
                bool alsoRemoveParent = true;
                tnHelp = navTree.FindNode(hint.NodePathValue);
                if (tnHelp != null)
                {
                    tnSelected = tnHelp;
                    alsoRemoveParent = false;
                }
                else // User clicked on "delete" button in toolbar.
                {
                    if (hint.NodeScreen == "RESERVEWORKSHEET") 
                    {
                        alsoRemoveParent = false;
                    }
                    if (!hint.IsOneToOne) //if it is not one to one like litigation then 
                    {
                        if (hint.Root != "Document" || hint.IsClaim || hint.IsEvent)
                        {
                            alsoRemoveParent = false;
                        }
                        else
                        {
                            tnParent = tnSelected.Parent;
                            //parent update (this will update # of records for list node)
                            MDITreeNodeHint hintParent = new MDITreeNodeHint(tnParent.ValuePath, false);
                            hintParent.PersonInvolved = dPersonInvolved;//gbindra - update the PersonInvolved count in case of remove.
                            MDITreeNodeHint hintDataModelParent = new MDITreeNodeHint(tnSelected.ValuePath, "DataModelParent", false);
                            profile = GetProfile(hintDataModelParent.NodeScreen, Convert.ToInt32(hintDataModelParent.NodeRecordId));

                            if (profile.NodeChildern != null)
                            {
                                for (int i = 0; i < profile.NodeChildern.Count; i++)
                                {
                                    if (profile.NodeChildern[i][1] == hintParent.NodeScreen)
                                    {
                                        if (hintParent.Presentable)
                                        {
                                            //Bkuzhanthaim : MITS- 36027/RMA-342:auto expansion of all adjusters on load of claim.
                                            if (hintParent.NodeScreen.ToString() == "adjuster" && profile.NodeChildern[i][2].Contains('/'))
                                            {
                                                profile.NodeChildern[i][2] = profile.NodeChildern[i][2].Split('/')[0].ToString();
                                            }

                                            //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                                            if (hintParent.NodeScreen.ToString() == "claimxpolicy" && profile.NodeChildern[i][2].Contains('/'))
                                            {
                                                profile.NodeChildern[i][2] = profile.NodeChildern[i][2].Split('/')[0].ToString();
                                            }
                                            //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.

                                            alsoRemoveParent = false;
                                            hintParent.ParentPathValue = tnParent.Parent.ValuePath;
                                            hintParent.ReadThisNarrowedProfile(profile.NodeChildern[i]);
                                            tnParent = RefreshNode(tnParent, hintParent);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (hint.IsRoot)
                {
                    navTree.Nodes.Remove(tnSelected);
                }
                else
                {
                    BusinessRule(ref tnSelected, ref hint);
                    tnParent = tnSelected.Parent;
                    tnParent.ChildNodes.Remove(tnSelected);
                    MDITreeNodeHint hint2 = new MDITreeNodeHint(tnParent.ValuePath, false);
                    if (hint2.IsRoot)
                    {
                        tnRoot = navTree.FindNode(hint2.NodePathValue);
                        if (tnRoot != null && tnRoot.ChildNodes.Count == 0)
                        {
                            navTree.Nodes.Remove(tnRoot);
                        }
                    }
                    else if (alsoRemoveParent)
                    {
                        tnParent.Parent.ChildNodes.Remove(tnParent);
                    }
                }
                if (navTree.SelectedNode == null)
                {
                    tnSelected = navTree.FindNode("DUMMY");
                    tnSelected.Select();
                }
                if (!hint.IsReplaced)
                {
                    this.PreviousNodeValuePath = "";
                    Breadcrumb = "";
                }
                else
                {
                    Breadcrumb = hint.Breadcrumb;
                }
            }
            else if (hint.AreChildrenCollapsed && hint.IsRoot)
            {
                tnRoot = navTree.FindNode(hint.NodePathValue);
                if (tnRoot != null)
                {
                    foreach (TreeNode tn in tnRoot.ChildNodes)
                    {
                        tn.CollapseAll();
                    }
                }
            }
            else if (hint.IsDirtied)
            {
                    BusinessRule(ref tnSelected, ref hint);
                    tnSelected.Text = hint.GetNodeHTMLText();
                    Script = "MDI('','')"; // to do nothing; since we don't want to switch screens
            }
            else
            {
                switch (hint.Root)
                {
                    case "DUMMY":
                        {   //if user click on the close button and close a dirty screen we need to refresh the node text and remove the unsaved image!
                            if (tnSelected != null)
                            {
                                MDITreeNodeHint hintSelected = new MDITreeNodeHint(tnSelected.ValuePath, false);
                                MDITreeNodeHint hintParent = new MDITreeNodeHint(tnSelected.ValuePath, "MDIParent", false);
                                tnParent = tnSelected.Parent;
                                // akaushik5 Added for RMA-17118 Starts
                                hintSelected.PersonInvolved = dPersonInvolved;
                                // akaushik5 Added for RMA-17118 Ends
                                //BusinessRule(ref tnParent, ref hintParent);
                                switch (hintSelected.Root)
                                {
                                    case "Document":
                                        {
                                            if (hintParent.IsRoot)
                                            {
                                                if (hintSelected.NodeRecordId < 0)
                                                {
                                                    hintSelected.IsRemoved = true;
                                                }
                                            }
                                            else if (hintParent.NodeRecordId <= 0 && !hintParent.IsOneToOne)
                                            {
                                                hintSelected.IsRemoved = true;
                                            }
                                            else if ((hintSelected.IsClaim || hintSelected.NodeScreen == "RESERVEWORKSHEET") && hintSelected.NodeRecordId < 0)
                                            {
                                                hintSelected.IsRemoved = true;
                                            }
                                        }
                                        break;
                                    case "Search":
                                        {
                                            if (tnSelected.ChildNodes.Count == 0)
                                            {
                                                hintSelected.IsRemoved = true;
                                            }
                                        }
                                        break;
                                    default:
                                        {
                                            hintSelected.IsRemoved = true;
                                        }
                                        break;
                                }

                                if (hintSelected.PlaceAt != string.Empty)
                                {
                                    hintSelected.IsRemoved = true;
                                }

                                BusinessRule(ref tnSelected, ref hintSelected);
                                if (hintSelected.IsRemoved == true)
                                {
                                    if (hintParent.IsRoot && tnSelected.Parent.ChildNodes.Count == 1)
                                    {
                                        navTree.Nodes.Remove(tnSelected.Parent);
                                    }
                                    else
                                    {
                                        tnSelected.Parent.ChildNodes.Remove(tnSelected);
                                        //BusinessRule(ref tnParent, ref hintParent);
                                    }
                                }
                                else
                                {
                                    tnSelected.Text = hintSelected.GetNodeHTMLText();
                                    tnSelected.CollapseAll();
                                }
                            }
                            break;
                        }
                    case "Search":
                        {
                            if (!hint.IsToggled)
                            {
                                //if (hint.NodeRecordId == 0 && this.ReachedMaxSearchScreens == "NO") //new criteria or result
                                if (hint.NotInNavTree)
                                {
                                    tnRoot = navTree.FindNode(hint.Root);
                                    if (hint.NodeScreen.ToLower() == "result")
                                    {
                                        navTree.SelectedNode.ChildNodes.Add(InitResultNode(hint, navTree.SelectedNode.ValuePath));
                                        navTree.SelectedNode.Expand();

                                    }
                                    else
                                    {
                                        if (tnRoot == null)
                                        {
                                            if (string.IsNullOrEmpty(hint.RootTitle) && hint.Root == "Document")
                                            {
                                                hint.RootTitle = sDocumentTitle;
                                            }
                                            tnRoot = NewRoot(hint.Root, hint.RootTitle);
                                            navTree.Nodes.Add(tnRoot);
                                        }
                                        tnSelected = InitCriteriaNode(hint, tnRoot.ValuePath);
                                        tnRoot.ChildNodes.Add(tnSelected);
                                        tnRoot.Expand();
                                    }
                                    //this.ReachedMaxSearchScreens = ReachedMaximumSearchScreens(tnHelp);
                                }
                                MakeVisible(tnSelected);
                                SwitchToSearchScreen(hint);
                            }
                            break;
                        }

                    case "Document":
                        {
                            MDITreeNodeHint hint2 = new MDITreeNodeHint(null, false);
                            hint2.PersonInvolved = dPersonInvolved; //Added by gbindra on 08/26/14 for WWIG GAP 30 MITS#35365
                            //For now we get rid of "Claims" node; if the # of claims under one event happens to be a lot and customers really need the "Claims" node, we will add it again...
                            //TreeNode tnClaims = null;
                            if (!hint.IsToggled)
                            {
                                tnRoot = navTree.FindNode(hint.Root);
                                if (hint.NotInNavTree)
                                {
                                    if (tnRoot == null)
                                    {
                                        if (string.IsNullOrEmpty(hint.RootTitle) && hint.Root == "Document")
                                        {
                                            //srajindersin MITS 34415 1/2/2014
                                            if (string.IsNullOrEmpty(sDocumentTitle))
                                            {
                                                throw new ApplicationException("parent.CommonValidations.NoDocumentPermission");
                                            }
                                            else
                                            {
                                                hint.RootTitle = sDocumentTitle;
                                            }
                                        }
                                        tnRoot = NewRoot(hint.Root, hint.RootTitle);
                                        navTree.Nodes.Add(tnRoot);
                                    }

                                    tnParent = null;
                                    if (hint.NodeRecordId < 0) //smaller than zero means this is a new record and needs to be assigned a positive recordId after it is saved; if it is zero means this is a node that does not need to have a record id (e.g. it might be a list node, filtered diary list, etc.)
                                    {
                                        profile = new MDINavigationTreeNodeProfileResponse();
                                        profile.NodeDetail = new List<string[]>();

                                        profile.NodeParent = null;
                                        profile.NodeChildern = null;
                                        profile.NodeSiblings = null;
                                        if (hint.IsClaim)
                                        {
                                            List<string[]> tmpNodeDetail = new List<string[]>();
                                            tmpNodeDetail.Add(new string[] { (--LastNumericId).ToString(), hint.NodeScreen, Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("MDInode.New"), hint.NodeTitle, string.Empty, Boolean.TrueString });
                                            profile.NodeDetail = tmpNodeDetail.ToList<string[]>();
                                            tmpNodeDetail=null;
                                            //profile.NodeDetail.Add(new string[] { (--LastNewId).ToString(), hint.NodeScreen, "NEW", hint.NodeScreen, string.Empty, Boolean.TrueString });
                                            if (hint.NodeParentId < 0)
                                            {
                                                List<string[]> tmpNodeParent = new List<string[]>();
                                                tmpNodeParent.Add(new string[] { (--LastNumericId).ToString(), "event", Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("MDInode.New"), (string.IsNullOrEmpty(sEventTitle) ? "Event" : sEventTitle), string.Empty, Boolean.TrueString });
                                                profile.NodeParent = tmpNodeParent.ToList<string[]>();
                                                tmpNodeParent = null;
                                               // profile.NodeParent = new List<string[]>();
                                                //profile.NodeParent.Add(new string[] { (--LastNewId).ToString(), "event", "NEW", "event", string.Empty, Boolean.TrueString });
                                            }
                                        }
                                        else
                                        {
                                            List<string[]> tmpNodeDetail = profile.NodeDetail.ToList();
                                            tmpNodeDetail.Add(new string[] { (--LastNumericId).ToString(), hint.NodeScreen, Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificGlobalObject("MDInode.New"), hint.NodeTitle, hint.NodeTitle, Boolean.TrueString });
                                            profile.NodeDetail = tmpNodeDetail.ToList<string[]>();
                                            tmpNodeDetail = null;
                                            //profile.NodeDetail.Add(new string[] { (--LastNewId).ToString(), hint.NodeScreen, "NEW", hint.NodeScreen, hint.NodeScreen, Boolean.TrueString });
                                        }
                                        tnParent = FindByMDIId(ref tnRoot, hint.GetParentMDIId());
                                           hint.NodePathValue = tnParent.ValuePath;
                                        }
                                    else
                                    {
                                        if (hint.NodeRecordId > 0)
                                        {
                                            //profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeRecordId));
                                            if (hint.NodeScreen == "demandoffer")//Neha Merging chages of nima from r7 patch set source
                                            {
                                                MDITreeNodeHint hintParent2 = new MDITreeNodeHint(tnSelected.ValuePath, "DataModelParent", false);
                                                profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeRecordId), hintParent2.NodeScreen, Convert.ToInt32(hintParent2.NodeRecordId));
                                            }
                                            else
                                            {
                                                profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeRecordId));
                                            }
                                        }
                                        else
                                        {
                                            profile = new MDINavigationTreeNodeProfileResponse();
                                            profile.NodeDetail = new List<string[]>();

                                            List<string[]> tmpNodeDetail = profile.NodeDetail.ToList();
                                            tmpNodeDetail.Add(new string[] { "0", hint.NodeScreen, string.Empty, string.Empty, hint.NodeTitle, Boolean.TrueString }); // This is an injected node.
                                            profile.NodeDetail = tmpNodeDetail.ToList<string[]>();
                                            tmpNodeDetail = null;
                                        }
                                    }

                                    hint.ReadThisNarrowedProfile(profile.NodeDetail[0]);

                                    //if (!hint.IsEvent && !hint.IsClaim)//user selected a record from one of the lookUpNavigation lists (i.e. Litigations, Adjusters, Claimants, etc.)
                                    if (hint.NodeParentId > 0 || !hint.IsEvent && !hint.IsClaim && !hint.IsFinancialFromClaimant)//either user requests a new node (claim or a node from a list node) under an existing parent OR user selected a record from one of the lookUpNavigation lists (i.e. Litigations, Adjusters, Claimants, etc.)
                                    {
                                        if (hint.PlaceAt == string.Empty || hint.PlaceAt.ToUpper() != "TOP")
                                        {
                                            if (tnParent == null)
                                            {
                                                tnParent = tnSelected;
                                            }
                                        }
                                        if (tnParent == null)
                                        {
                                            tnParent = tnRoot;
                                        }

                                        tnHelp = InitFDMNode(hint, tnParent.ValuePath);
                                        tnSelected = navTree.FindNode(hint.NodePathValue);
                                        if (tnSelected == null)
                                        {
                                            tnSelected = tnHelp;
                                            if (tnParent.ChildNodes.Count == 1 && string.Compare(tnParent.ChildNodes[0].Value, "DUMMY", true) == 0)
                                            {
                                                //agupta298 :  RMA-5281 : passing dPersonInvolved as a parameter.
                                                Populate(ref tnParent, null, dPersonInvolved);
                                            }
                                            tnParent.ChildNodes.Add(tnSelected);
                                            BusinessRule(ref tnSelected, ref hint);
                                        }
                                        else
                                        {
                                            duplicated = true;
                                        }
                                        tnParent.Expand();
                                    }
                                    else //it is an event or a claim; not in navTree; we need to create event/claim hierarchy on the tree.
                                    {
                                        if (profile.NodeParent != null) //requested node is a claim which is not in navTree; tnSelected is going to be the claim node
                                        {
                                            if (Convert.ToInt32(profile.NodeParent[0][0]) > 0)
                                            {
                                                parentProfile = GetProfile(profile.NodeParent[0][1], Convert.ToInt32(profile.NodeParent[0][0]));
                                                hint2.ReadThisNarrowedProfile(parentProfile.NodeDetail[0]);
                                            }
                                            else
                                            {
                                                hint2.ReadThisNarrowedProfile(profile.NodeParent[0]);
                                            }
                                            if (!hint.IsFinancialFromClaimant)
                                            {
                                                hint.NodeParentId = Convert.ToInt32(hint2.NodeRecordId);
                                                tnHelp = InitFDMNode(hint2, tnRoot.ValuePath);
                                                tnParent = navTree.FindNode(hint2.NodePathValue);
                                                if (tnParent == null)
                                                {
                                                    tnParent = tnHelp;
                                                    tnRoot.ChildNodes.Add(tnParent);
                                                }
                                            }
                                            else
                                            {
                                                hint.NodeParentId = Convert.ToInt32(hint2.NodeRecordId);
                                                tnHelp = InitFDMNode(hint2, tnSelected.ValuePath);
                                                tnParent = navTree.FindNode(hint2.NodePathValue);
                                                if (tnParent == null)
                                                {
                                                    tnParent = tnHelp;
                                                   tnSelected.ChildNodes.Add(tnParent);
                                                    bIsClaimantOpened = false;
                                                }
                                                else
                                                    bIsClaimantOpened = true;
                                               

                                                //tnParent = tnSelected;
                                            }
                                            //tnRoot.Expand();
                                            //MDITreeNodeHint hintParent = new MDITreeNodeHint(tnParent.ValuePath, false);

                                            //For now we get rid of "Claims" node; if the # of claims under one event happens to be a lot and customers really need the "Claims" node, we will add it again...
                                            /*hint2 = GetHintForClaimsNode(tnParent); //we don't know that Claims node exists or not; however since it is a static node we can easily have its hint(parsed information).
                                            tnClaims = navTree.FindNode(tnParent.ValuePath + '\\' + hint2.GetNodeValue());
                                            if (tnClaims == null)
                                            {
                                                tnClaims = NewNode(hint2);
                                                tnParent.ChildNodes.Add(tnClaims);
                                            }
                                            //tnParent.Expand();
                                            tnParent = tnClaims;
                                             */

                                            //tnSelected = InitFDMNode(hint, tnParent.ValuePath);
                                            //tnParent.ChildNodes.Add(tnSelected);

                                            //tnParent.Expand();

                                            tnHelp = InitFDMNode(hint, tnParent.ValuePath);
                                            tnSelected = navTree.FindNode(hint.NodePathValue);
                                            if (bIsClaimantOpened && hint.IsFinancialFromClaimant)
                                            {
                                                //if (hint.NodePathValue == string.Empty)
                                                //{
                                                    //it enters this block when User makes changes on a form but changes to that form are not allowed at that moment (e.g. two claims under the same event cannot be dirty at the same time)
                                                    //it also can come here when User clicks on Close button in order to close a dirty screen but when they get the alert they change their mind and then click on Cancel.
                                                    //it basically comes here for two things; 1- to make sure hint.NodePathValue is populated because path and Script are populated based on that in function "SwitchToFDMScreen". 2- In order to populate Breadcrumb
                                                    if (tnSelected == null)
                                                    {
                                                        tnSelected = FindByMDIId(ref tnRoot, hint.NodeMDIId);
                                                    }
                                                    if (tnSelected != null)
                                                    {
                                                        tnSelected.Select();
                                                        hint.NodePathValue = tnSelected.ValuePath;
                                                        hint.PopulateHierarchyProperties();
                                                    }
                                                //}
                                                UpdateCurrentClaim(ref tnRoot, ref hint);

                                                MakeVisible(tnSelected);
                                                SwitchToFDMScreen(hint);


                                            }
                                             if (tnSelected == null)
                                            {
                                                tnSelected = tnHelp;
                                                tnParent.ChildNodes.Add(tnSelected);

                                                if (profile.NodeSiblings != null)
                                                {
                                                    for (int i = 0; i < profile.NodeSiblings.Count; i++)
                                                    {
                                                        hint2.ReadThisNarrowedProfile(profile.NodeSiblings[i]);
                                                        if (hint2.CouldPopulateOnDemand)
                                                        {
                                                            tnParent.ChildNodes.Add(SetToBePopulatedOnDemand(InitFDMNode(hint2, tnParent.ValuePath)));
                                                        }
                                                        else
                                                        {
                                                            tnParent.ChildNodes.Add(InitFDMNode(hint2, tnParent.ValuePath));
                                                        }
                                                    }
                                                }

                                                //Add children for parent here if current node is not a new one
                                                if (parentProfile != null)
                                                {
                                                    if (parentProfile.NodeChildern != null)
                                                    {
                                                        for (int i = 0; i < parentProfile.NodeChildern.Count; i++)
                                                        {
                                                            if (hint.IsFinancialFromClaimant && parentProfile.NodeChildern[i][1] == "reservelisting")
                                                            {
                                                                continue;
                                                            }
                                                            hint2.ReadThisNarrowedProfile(parentProfile.NodeChildern[i]);
                                                            if (!hint2.IsClaim && hint2.Presentable)
                                                            {
                                                                if (hint2.CouldPopulateOnDemand)
                                                                {
                                                                    tnParent.ChildNodes.Add(SetToBePopulatedOnDemand(InitFDMNode(hint2, tnParent.ValuePath)));
                                                                }
                                                                else
                                                                {
                                                                    tnParent.ChildNodes.Add(InitFDMNode(hint2, tnParent.ValuePath));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                duplicated = true;
                                            }
                                        }
                                        else //requested node is an event which is not in navTree; tnSelected is going to be the event node
                                        {
                                            tnParent = tnRoot;
                                            hint.NodeParentId = 0;

                                            //tnSelected = InitFDMNode(hint, tnParent.ValuePath);
                                            //tnParent.ChildNodes.Add(tnSelected);
                                            //tnParent.Expand();

                                            tnHelp = InitFDMNode(hint, tnParent.ValuePath);
                                            tnSelected = navTree.FindNode(hint.NodePathValue);
                                            if (tnSelected == null)
                                            {
                                                tnSelected = tnHelp;
                                                tnParent.ChildNodes.Add(tnSelected);
                                            }
                                            else
                                            {
                                                duplicated = true;
                                            }

                                            //For now we get rid of "Claims" node; if the # of claims under one event happens to be a lot and customers really need the "Claims" node, we will add it again...
                                            /*hint2 = GetHintForClaimsNode(tnSelected);
                                            tnClaims = NewNode(hint2);
                                            tnSelected.ChildNodes.Add(tnClaims);
                                            tnParent = tnClaims;
                                             */
                                        }
                                    }
                                    UpdateCurrentClaim(ref tnRoot, ref hint);
                                    if (!duplicated && profile.NodeChildern != null)
                                    {
                                        //agupta298 :  RMA-5281 : passing dPersonInvolved as a parameter.
                                        Populate(ref tnSelected, profile, dPersonInvolved);
                                    }
                                    tnSelected.Select();
                                }
                                else if (hint.IsRefreshed)
                                {   //from root to the current child, we update the pathValue and MDIId (through Refresh method) based on parentPathValue; one at a time. Because only nodeValue is passed from client side and also we need to walkdown from an appropriate ancestor (the top most ancestor whom affected by this refresh) in order to get the right pathValue.
                                    bool bIsInjury = false;//Neha R8 14/09/2011 changes update parent node when new child is added.
                                    if (string.Compare(hint.NodeScreen, "casemanagementlist", true) == 0 || string.Compare(hint.NodeScreen, "piemployee") == 0 || string.Compare(hint.NodeScreen, "demandoffer", true) == 0)
                                    {
                                        bIsInjury = true;
                                        if (Convert.ToInt32(hint.NodeNewRecordId) <= 0)
                                        hint.NodeNewRecordId = Convert.ToInt32(hint.NodeRecordId);//Neha Setting so that we can get case mangement for injury.
                                    }
                                    if (Convert.ToInt32(hint.NodeNewRecordId) > 0) //sometimes we have injected nodes like Enhanced Notes that still have zero RecordId while refreshing Or current screens that don't match with corresponding screens like deposit under bankaccount or Dependants under WCClaim. if we later move all children into navTree the last two examples will be no longer the case.
                                    {
                                        if (hint.IsEvent)
                                        {
                                            profile = GetProfile(hint.NodeScreen, hint.NodeNewRecordId);
                                            tnParent = tnSelected.Parent;
                                            hint.ParentPathValue = tnParent.ValuePath;
                                            hint.ReadThisNarrowedProfile(profile.NodeDetail[0]);
                                            tnSelected = RefreshNode(tnSelected, hint);

                                            if (profile.NodeChildern != null) //This part mainly applies the business rule for one to one children under event(e.g. whether we must show Fall Info, MedWatch, etc. or not)
                                            {
                                                if (tnSelected.ChildNodes.Count == 0)
                                                {
                                                    for (int i = 0; i < profile.NodeChildern.Count; i++)
                                                    {
                                                        hint2.ReadThisNarrowedProfile(profile.NodeChildern[i]);
                                                        if (hint2.Presentable)
                                                        {
                                                            if (hint2.CouldPopulateOnDemand)
                                                            {
                                                                tnSelected.ChildNodes.Add(SetToBePopulatedOnDemand(InitFDMNode(hint2, tnSelected.ValuePath)));
                                                            }
                                                            else
                                                            {
                                                                tnSelected.ChildNodes.Add(InitFDMNode(hint2, tnSelected.ValuePath));
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    int j = 0;
                                                    for (int i = 0; i < profile.NodeChildern.Count; i++)
                                                    {
                                                        hint2.ReadThisNarrowedProfile(profile.NodeChildern[i]);
                                                        if (hint2.IsOneToOne)
                                                        {
                                                            tnHelp = null;
                                                            for (j = 0; j < tnSelected.ChildNodes.Count; j++)
                                                            {
                                                                hint2.SetMDIId(hint.NodeMDIId);
                                                                if (tnSelected.ChildNodes[j].Value == hint2.GetNodeValue())
                                                                {
                                                                    tnHelp = tnSelected.ChildNodes[j];
                                                                    break;
                                                                }
                                                            }
                                                            if (tnHelp == null)
                                                            {
                                                                if (hint2.Presentable)
                                                                {
                                                                    if (hint2.CouldPopulateOnDemand)
                                                                    {
                                                                        tnSelected.ChildNodes.Add(SetToBePopulatedOnDemand(InitFDMNode(hint2, tnSelected.ValuePath)));
                                                                    }
                                                                    else
                                                                    {
                                                                        tnSelected.ChildNodes.Add(InitFDMNode(hint2, tnSelected.ValuePath));
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (!hint2.Presentable)
                                                                {
                                                                    tnSelected.ChildNodes.RemoveAt(j);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if (hint.IsClaim)
                                        {
                                            profile = GetProfile(hint.NodeScreen, hint.NodeNewRecordId);
                                            //For now we get rid of "Claims" node; if the # of claims under one event happens to be a lot and customers really need the "Claims" node, we will add it again...
                                            /*tnClaims = tnSelected.Parent;
                                            tnParent = tnClaims.Parent;
                                             */
                                            tnParent = tnSelected.Parent;

                                            //event update
                                            MDITreeNodeHint hintParent = new MDITreeNodeHint(tnParent.ValuePath, false);
                                            hintParent.ParentPathValue = tnParent.Parent.ValuePath;
                                            hintParent.ReadThisNarrowedProfile(profile.NodeParent[0]);
                                            tnParent = RefreshNode(tnParent, hintParent);
                                            parentProfile = GetProfile(hintParent.NodeScreen, Convert.ToInt32(hintParent.NodeRecordId));

                                            if (parentProfile.NodeChildern != null && tnParent.ChildNodes.Count == 1)
                                            {
                                                for (int i = 0; i < parentProfile.NodeChildern.Count; i++)
                                                {
                                                    hint2.ReadThisNarrowedProfile(parentProfile.NodeChildern[i]);
                                                    if (!hint2.IsClaim && hint2.Presentable)
                                                        tnParent.ChildNodes.Add(InitFDMNode(hint2, tnParent.ValuePath));
                                                }
                                            }
                                            else if (parentProfile.NodeChildern != null && hint.NodeRecordId < 0) // updating person involved
                                            {
                                                hint2.ParentPathValue = tnParent.ValuePath;
                                                for (int i = 0; i < parentProfile.NodeChildern.Count; i++)
                                                {
                                                    hint2.ReadThisNarrowedProfile(parentProfile.NodeChildern[i]);
                                                    if (!hint2.IsClaim && !hint2.IsOneToOne)
                                                    {
                                                        for (int j = 0; j < tnParent.ChildNodes.Count; j++)
                                                        {
                                                            MDITreeNodeHint childHint = new MDITreeNodeHint(tnParent.ChildNodes[j].Value, false);
                                                            if (childHint.NodeScreen == hint2.NodeScreen)
                                                            {
                                                                tnHelp = RefreshNode(tnParent.ChildNodes[j], hint2);
                                                                tnParent.ChildNodes.RemoveAt(j);
                                                                tnParent.ChildNodes.AddAt(j, tnHelp);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            ////PSARIN2 Policy Interface Start Refresh Issue - This is a HACK as i don't
                                            ////want to disturb the code already written
                                            if (parentProfile.NodeChildern != null && tnParent.ChildNodes.Count > 1)
                                            {
                                                for (int i = 0; i < parentProfile.NodeChildern.Count; i++)
                                                {
                                                    tnHelp = tnParent.ChildNodes[i];
                                                    hint2.ParentPathValue = tnParent.ValuePath;
                                                    if (parentProfile.NodeChildern[i][1] == "personinvolvedlist")
                                                    {
                                                        hint2.ReadThisNarrowedProfile(parentProfile.NodeChildern[i]);
                                                        //tnHelp = RefreshNode(tnHelp, hint2);
                                                        //tnParent.ChildNodes.RemoveAt(i);
                                                        //tnParent.ChildNodes.AddAt(i, tnHelp);
                                                        //break;
                                                        for (int j = 0; j < tnParent.ChildNodes.Count; j++)
                                                        {
                                                            MDITreeNodeHint childHint = new MDITreeNodeHint(tnParent.ChildNodes[j].Value, false);
                                                            if (childHint.NodeScreen == hint2.NodeScreen)
                                                            {
                                                                tnHelp = RefreshNode(tnParent.ChildNodes[j], hint2);
                                                                tnParent.ChildNodes.RemoveAt(j);
                                                                tnParent.ChildNodes.AddAt(j, tnHelp);
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                            ////PSARIN2 Policy Interface End

                                            //"claims" node update
                                            //For now we get rid of "Claims" node; if the # of claims under one event happens to be a lot and customers really need the "Claims" node, we will add it again...
                                            /*MDITreeNodeHint hintClaims = new MDITreeNodeHint(tnClaims.ValuePath);
                                            hintClaims = GetHintForClaimsNode(tnParent);
                                            hintClaims.ParentPathValue = tnParent.ValuePath;
                                            tnClaims = RefreshNode(tnClaims, hintClaims);
                                            tnParent = tnClaims;
                                             */

                                            //claim update 

                                            hint.ParentPathValue = tnParent.ValuePath;
                                            hint.ReadThisNarrowedProfile(profile.NodeDetail[0]);
                                            tnSelected = RefreshNode(tnSelected, hint);

                                            //taking care of children which are tab in parents; we will check if any one to one child of this kind (tab in parent screen) has an existing record and the child itself can possibly have children under itself, then we make it as to be populated on demand. 
                                            if (profile.NodeChildern != null)
                                            {
                                                if (tnSelected.ChildNodes.Count == profile.NodeChildern.Count)
                                                {
                                                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                                                    Populate(ref tnSelected, profile,dPersonInvolved);
                                                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim. 
                                                }
                                                else if (tnSelected.ChildNodes.Count == 0)
                                                {
                                                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                                                    Populate(ref tnSelected, profile,dPersonInvolved);
                                                    //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                                                }
                                            }
                                        }
                                        else
                                        {   //neha  Changes done by nima merged from R7 PS source
                                            //profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeNewRecordId));
                                            //hint.ReadThisNarrowedProfile(profile.NodeDetail[0]);
                                            //BusinessRule(ref tnSelected, ref hint);
                                            tnParent = tnSelected.Parent;
                                            MDITreeNodeHint hintParent = new MDITreeNodeHint(tnParent.ValuePath, false);
                                            hintParent.ParentPathValue = tnParent.Parent.ValuePath;
                                            hintParent.PersonInvolved = dPersonInvolved;//gbindra - update the PersonInvolved count in case of add.
                                            if (!hint.IsOneToOne) //if it is not one to one like litigation then 
                                            {
                                               // tnParent = tnSelected.Parent;
                                                //parent update (this will update # of records for list node)
                                                //MDITreeNodeHint hintParent = new MDITreeNodeHint(tnParent.ValuePath, false);
                                                //hintParent.ParentPathValue = tnParent.Parent.ValuePath;
                                                if (hint.NodeScreen == "demandoffer")
                                                { //neha  Changes done by nima merged from R7 PS source
                                                    MDITreeNodeHint hintParent2 = new MDITreeNodeHint(tnSelected.ValuePath, "DataModelParent", false);
                                                    //MDINavigationTreeNodeProfileResponse profile2 = GetProfile(hint.NodeScreen, 0, hintParent2.NodeScreen, Convert.ToInt32(hintParent2.NodeRecordId));
                                                    //hintParent.ReadThisProfileForListNode(profile2.NodeSiblings.ToList(), profile.NodeDetail[0].ToArray(), true);
                                                    profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeNewRecordId), hintParent2.NodeScreen, Convert.ToInt32(hintParent2.NodeRecordId));
                                                    if(profile.NodeSiblings != null)
                                                    hintParent.ReadThisProfileForListNode(profile.NodeSiblings.ToList(), profile.NodeDetail[0].ToArray(), true);
                                                }
                                                else
                                                { //neha  Changes done by nima merged from R7 PS source
                                                    profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeNewRecordId));
                                                 //   hintParent.ReadThisNarrowedProfile(profile.NodeParent[0]);
                                                     if(profile.NodeSiblings != null)
                                                   hintParent.ReadThisProfileForListNode(profile.NodeSiblings.ToList(), profile.NodeDetail[0].ToArray(), false);

                                                }
                                                tnParent = RefreshNode(tnParent, hintParent);
                                                // Add by kuladeep for MITS 34685 Starts
                                                if (hintParent.NodeScreen.Equals("reservelisting"))
                                                {
                                                    this.BusinessRule(ref tnSelected, ref hint);
                                                }
                                                // Add by kuladeep for MITS 34685 Ends
                                            }
                                            else//neha  Changes done by nima merged from R7 PS source
                                            {//Added by Amitosh for mits 27232
                                                if (hint.NodeScreen == "salvage")
                                                {
                                                    MDITreeNodeHint hintParent2 = new MDITreeNodeHint(tnSelected.ValuePath, "DataModelParent", false);
                                                    profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeNewRecordId), hintParent2.NodeScreen, Convert.ToInt32(hintParent2.NodeRecordId));
                                                }
                                                else//End Amitosh
                                                profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeNewRecordId));
                                            }
                                            //current node update (this will update user prompt)
                                            hint.ParentPathValue = tnSelected.Parent.ValuePath;
                                            hint.ReadThisNarrowedProfile(profile.NodeDetail[0]);
                                            hint.NodeParam = string.Empty; // params are initially used to start screens, like pieid for adding existing persons under PI, are no longer useful, but also can cause problems in re-navigating. Hence we set them to empty string.
                                            tnSelected = RefreshNode(tnSelected, hint);
                                            //It is implemented only for injury screen.
                                            //taking care of children which are tab in parents ; we will check if any one to one child of this kind (tab in parent screen) has an existing record and the child itself can possibly have children under itself, then we make it as to be populated on demand. 
                                            if (profile.NodeChildern != null && bIsInjury )
                                            {
                                                if (tnSelected.ChildNodes.Count == profile.NodeChildern.Count)
                                                {
                                                    for (int i = 0; i < profile.NodeChildern.Count; i++)
                                                    {
                                                        tnHelp = tnSelected.ChildNodes[i];
                                                        hint2.ParentPathValue = tnSelected.ValuePath;
                                                        hint2.ReadThisNarrowedProfile(profile.NodeChildern[i]);
                                                        if (hint2.CouldPopulateOnDemand && tnHelp.ChildNodes.Count == 0)
                                                        {
                                                            tnHelp = RefreshNode(tnHelp, hint2); //We might have created a new record for a one to one child (which is tab in parent screen). Now the one to one child has a recordId and we need to inject the id in the treenode's value so that its children can be identified on populating on demand.
                                                            tnHelp = SetToBePopulatedOnDemand(tnHelp);
                                                        }
                                                    }
                                                }
                                            }
                                            if (profile.NodeChildern != null && tnSelected.ChildNodes.Count == 0) // (it has children in datamodel and tnSelected.ChildNodes.Count == 0 means it has just been created)which itself can have children, so we add children nodes under it.
                                            {
                                                for (int i = 0; i < profile.NodeChildern.Count; i++)
                                                {
                                                    hint2.ReadThisNarrowedProfile(profile.NodeChildern[i]);
                                                    if (hint2.CouldPopulateOnDemand)
                                                    {
                                                        tnSelected.ChildNodes.Add(SetToBePopulatedOnDemand(InitFDMNode(hint2, tnSelected.ValuePath)));
                                                    }
                                                    else
                                                    {
                                                        tnSelected.ChildNodes.Add(InitFDMNode(hint2, tnSelected.ValuePath));
                                                    }
                                                }
                                            }
                                            //Deb:Added to update the Person Involved when new claimant is addded
                                            if ((string.Compare(hint.NodeScreen, "adjuster", true) == 0) || (string.Compare(hint.NodeScreen, "claimant", true) == 0))
                                            {
                                                if (tnSelected.Parent.Parent.Parent != null)
                                                {
                                                    tnParent = tnSelected.Parent.Parent.Parent;
                                                    MDITreeNodeHint mhintParent = new MDITreeNodeHint(tnParent.ValuePath, false);
                                                    parentProfile = GetProfile("event", Convert.ToInt32(mhintParent.NodeRecordId));
                                                    hint2.ParentPathValue = tnParent.ValuePath;
                                                    for (int i = 0; i < parentProfile.NodeChildern.Count; i++)
                                                    {
                                                        hint2.ReadThisNarrowedProfile(parentProfile.NodeChildern[i]);
                                                        if (!hint2.IsClaim && !hint2.IsOneToOne)
                                                        {
                                                            for (int j = 0; j < tnParent.ChildNodes.Count; j++)
                                                            {
                                                                MDITreeNodeHint childHint = new MDITreeNodeHint(tnParent.ChildNodes[j].Value, false);
                                                                if (childHint.NodeScreen == hint2.NodeScreen)
                                                                {
                                                                    tnHelp = RefreshNode(tnParent.ChildNodes[j], hint2);
                                                                    tnParent.ChildNodes.RemoveAt(j);
                                                                    tnParent.ChildNodes.AddAt(j, tnHelp);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            //Deb:Added to update the Person Involved when new claimant is addded
                                        }
                                    }
                                    else
                                    {
                                        BusinessRule(ref tnSelected, ref hint);
                                        hint.ParentPathValue = tnSelected.Parent.ValuePath;
                                        tnSelected = RefreshNode(tnSelected, hint);
                                    }
                                    tnSelected.Select();
                                }
                                else if (hint.IsReloaded)
                                {

                                    hint.NodePathValue = tnSelected.ValuePath; 
                                    hint.ParentPathValue = tnSelected.Parent.ValuePath;
                                    //akaur9 Diary List (filtered) issue.
                                    if (hint.PlaceAt != "SELECTED")
                                    {
                                        if (!hint.IsOneToOne && hint.NodeRecordId == 0) // it is a list node
                                        {
                                            MDITreeNodeHint hintParent = new MDITreeNodeHint(hint.NodePathValue, "DataModelParent", false);
                                            profile = GetProfile(hint.NodeScreen, Convert.ToInt32(hint.NodeRecordId), hintParent.NodeScreen, Convert.ToInt32(hintParent.NodeRecordId));
                                            hint.ReadThisProfileForListNode(profile.NodeSiblings.ToList(), profile.NodeDetail[0].ToArray(), true);
                                        }
                                        else if (hint.NodeRecordId > 0)
                                        {
                                            profile = GetProfile(hint.NodeScreen, hint.NodeNewRecordId);
                                            hint.ReadThisNarrowedProfile(profile.NodeDetail[0]);
                                        }
                                    } //akaur9 Diary List (filtered) issue.
                                    tnSelected = RefreshNode(tnSelected, hint);
                                }
                                else
                                {
                                    if (hint.NodePathValue == string.Empty)
                                    {
                                        //it enters this block when User makes changes on a form but changes to that form are not allowed at that moment (e.g. two claims under the same event cannot be dirty at the same time)
                                        //it also can come here when User clicks on Close button in order to close a dirty screen but when they get the alert they change their mind and then click on Cancel.
                                        //it basically comes here for two things; 1- to make sure hint.NodePathValue is populated because path and Script are populated based on that in function "SwitchToFDMScreen". 2- In order to populate Breadcrumb
                                        if (tnSelected == null)
                                        {
                                            tnSelected = FindByMDIId(ref tnRoot, hint.NodeMDIId);
                                        }
                                        if (tnSelected != null)
                                        {
                                            tnSelected.Select();
                                            hint.NodePathValue = tnSelected.ValuePath;
                                            hint.PopulateHierarchyProperties();
                                        }
                                    }
                                    UpdateCurrentClaim(ref tnRoot, ref hint);
                                }
                                MakeVisible(tnSelected);
                                SwitchToFDMScreen(hint);
                            }
                            else //hint.IsToggled == true
                            {
                                if (hint.IsPopulated)
                                {
                                    //agupta298 :  RMA-5281 : passing dPersonInvolved as a parameter.
                                    Populate(ref tnToggled,null,dPersonInvolved);
                                    tnSelected.Select();
                                }
                            }
                            break;
                        }
                    case "Reports":
                    case "Diaries":
                    case "User Documents":
                    case "Funds":
                    case "Maintenance":
                    case "Help":
                    case "My Work":
                    case "Utilities":
                    case "Policy":
                        {
                            if (!hint.IsToggled)
                            {
                                if (hint.NotInNavTree)
                                {
                                    tnRoot = navTree.FindNode(hint.Root);
                                    if (tnRoot == null)
                                    {
                                        //ipuri DIS User Verification Mits: 33285 Code Merge from RMA 13.1 CHP3 branch start
                                        if (string.IsNullOrEmpty(hint.RootTitle) && hint.Root == "Utilities")
                                        {
                                            hint.RootTitle = "Utilities";
                                        }
                                        //ipuri DIS User Verification Mits: 33285 Code Merge from RMA 13.1 CHP3 branch end
                                        if(string.IsNullOrEmpty(hint.RootTitle) && hint.Root == "Document")
                                        {
                                            hint.RootTitle=sDocumentTitle;
                                        }
                                        tnRoot = NewRoot(hint.Root,hint.RootTitle);
                                        navTree.Nodes.Add(tnRoot);
                                    }
                                    tnParent = null;
                                    if (hint.PlaceAt.ToUpper() == "SELECTED") // By Default, any new node is added under selected node unless the selected node is null or is not under the same root; in these cases it is added right under the root.
                                    {
                                        if (tnSelected != null)
                                        {
                                            MDITreeNodeHint hintSelected = new MDITreeNodeHint(tnSelected.ValuePath, false);
                                            if (hintSelected.Root == hint.Root)
                                            {
                                                tnParent = tnSelected;
                                            }
                                        }
                                    }
                                    if (tnParent == null)
                                    {
                                        tnParent = tnRoot;
                                    }
                                    if (hint.NodeRecordId < 0)
                                    {
                                        hint.NodeRecordId = --LastNumericId;
                                    }
                                    // selecting the requested node as the selected node.
                                    tnHelp = InitOtherNode(hint, tnParent.ValuePath);
                                    tnSelected = navTree.FindNode(hint.NodePathValue);// see if it was already in navTree so that we don't duplicate it
                                    if (tnSelected == null)
                                    {
                                        tnSelected = tnHelp;
                                        tnParent.ChildNodes.Add(tnSelected);
                                    }
                                    else
                                    {
                                        tnSelected.Select();
                                    }
                                    tnParent.Expand();
                                }
                                else if (hint.IsRefreshed)
                                {
                                    //smishra54: MITS 26812
                                    if (hint.PlaceAt != "SELECTED")
                                    {
                                        hint.ParentPathValue = hint.Root;
                                        tnSelected = navTree.FindNode(hint.GetNodePathValue());
                                    }
                                    //smishra54: End
                                    tnSelected = RefreshNode(tnSelected, hint);
                                    tnSelected.Select();
                                }
                                else if (hint.IsReloaded)
                                {
                                    hint.NodePathValue = tnSelected.ValuePath;
                                    hint.ParentPathValue = tnSelected.Parent.ValuePath;
                                    tnSelected = RefreshNode(tnSelected, hint);
                                }
                                else
                                {
                                    if (hint.NodePathValue == string.Empty)
                                    {
                                        //it can come here when User clicks on Close button in order to close a dirty screen but when they get the alert they change their mind and then click on Cancel.
                                        //it basically comes here to make sure hint.NodePathValue is populated in order to populate Breadcrumb.
                                        if (tnSelected != null)
                                        {
                                            hint.NodePathValue = tnSelected.ValuePath;
                                            hint.PopulateHierarchyProperties();
                                        }
                                    }
                                }
                                MakeVisible(tnSelected);
                                SwitchToOtherScreen(hint);
                            }
                            break;
                        }
                    default:
                        {
                            hint.Root = "Other";
                            if (!hint.IsToggled)
                            {
                                if (hint.NotInNavTree)
                                {
                                    tnRoot = navTree.FindNode(hint.Root);
                                    if (tnRoot == null)
                                    {
                                        if (string.IsNullOrEmpty(hint.RootTitle) && hint.Root == "Document")
                                        {
                                            hint.RootTitle = sDocumentTitle;
                                        }
                                        tnRoot = NewRoot(hint.Root,hint.RootTitle);
                                        navTree.Nodes.Add(tnRoot);
                                    }
                                    tnParent = null;
                                    if (hint.PlaceAt.ToUpper() == "SELECTED") // By Default, any new node is added under selected node unless the selected node is null or is not under the same root; in these cases it is added right under the root.
                                    {
                                        if (tnSelected != null)
                                        {
                                            MDITreeNodeHint hintSelected = new MDITreeNodeHint(tnSelected.ValuePath, false);
                                            if (hintSelected.Root == hint.Root)
                                            {
                                                tnParent = tnSelected;
                                            }
                                        }
                                    }
                                    if (tnParent == null)
                                    {
                                        tnParent = tnRoot;
                                    }

                                    // selecting the requested node as the selected node.
                                    tnHelp = InitOtherNode(hint, tnParent.ValuePath);
                                    tnSelected = navTree.FindNode(hint.NodePathValue);// see if it was already in navTree so that we don't duplicate it
                                    if (tnSelected == null)
                                    {
                                        tnSelected = tnHelp;
                                        tnParent.ChildNodes.Add(tnSelected);
                                    }
                                    else
                                    {
                                        tnSelected.Select();
                                    }
                                    tnParent.Expand();
                                }
                                else if (hint.IsRefreshed)
                                {
                                    hint.ParentPathValue = hint.Root;
                                    tnSelected = navTree.FindNode(hint.GetNodePathValue());
                                    tnSelected = RefreshNode(tnSelected, hint);
                                    tnSelected.Select();
                                }
                                else if (hint.IsReloaded)
                                {
                                    hint.NodePathValue = tnSelected.ValuePath;
                                    hint.ParentPathValue = tnSelected.Parent.ValuePath;
                                    tnSelected = RefreshNode(tnSelected, hint);
                                }
                                MakeVisible(tnSelected);
                                SwitchToOtherScreen(hint);
                            }
                            break;
                        }
                }
                if (!hint.IsToggled)
                {
                    this.Breadcrumb = hint.Breadcrumb;
                }
                //if (hint.RunBusinessRule)
                //{
                //    BusinessRule(ref tnSelected, ref hint);
                //}
                if (tnSelected != null)
                {
                    PreviousNodeValuePath = tnSelected.ValuePath;
                }
            }

            return navTree;
        }

        private MDINavigationTreeNodeProfileResponse GetProfile(string screen, int id)
        {
            MDINavigationTreeNodeProfileRequest oMDINavigationTreeNodeProfileRequest = new MDINavigationTreeNodeProfileRequest();
            MDINavigationTreeNodeProfileResponse oProfileResponse = new MDINavigationTreeNodeProfileResponse();
            oMDINavigationTreeNodeProfileRequest.NodeId = id;
            oMDINavigationTreeNodeProfileRequest.ObjectName = screen;
            oMDINavigationTreeNodeProfileRequest.ViewId = Convert.ToInt32(AppHelper.ReadCookieValue("ViewId"));
            oMDINavigationTreeNodeProfileRequest.Token = AppHelper.ReadCookieValue("SessionId");
            oMDINavigationTreeNodeProfileRequest.ClientId = AppHelper.ClientId;
            //oMDINavigationTreeNodeProfileRequest oClient = new MDINavigationTreeNodeProfileServiceClient();
            //oProfileResponse = oClient.GetMDINavigationTreeNodeProfileData(oProfileRequest);
            oProfileResponse = AppHelper.GetResponse<MDINavigationTreeNodeProfileResponse>("RMService/MDI/profile", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oMDINavigationTreeNodeProfileRequest); 
            return oProfileResponse;
        }

        private MDINavigationTreeNodeProfileResponse GetProfile(string screen, int id, string parent, int parentId)
        {
            MDINavigationTreeNodeProfileRequestByParent oMDINavigationTreeNodeProfileRequestByParent = new MDINavigationTreeNodeProfileRequestByParent();
            MDINavigationTreeNodeProfileResponse oProfileResponse = new MDINavigationTreeNodeProfileResponse();
            oMDINavigationTreeNodeProfileRequestByParent.NodeId = id;
            oMDINavigationTreeNodeProfileRequestByParent.ObjectName = screen;
            oMDINavigationTreeNodeProfileRequestByParent.ParentId = parentId;
            oMDINavigationTreeNodeProfileRequestByParent.ParentObjectName = parent;
            oMDINavigationTreeNodeProfileRequestByParent.ViewId = Convert.ToInt32(AppHelper.ReadCookieValue("ViewId"));
            oMDINavigationTreeNodeProfileRequestByParent.Token = AppHelper.ReadCookieValue("SessionId");
            oMDINavigationTreeNodeProfileRequestByParent.ClientId = AppHelper.ClientId;
            //MDINavigationTreeNodeProfileServiceClient oClient = new MDINavigationTreeNodeProfileServiceClient();
            //oProfileResponse = oClient.GetMDINavigationTreeNodeProfileDataByParent(oProfileRequest);
            oProfileResponse = AppHelper.GetResponse<MDINavigationTreeNodeProfileResponse>("RMService/MDI/profilebyparent", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oMDINavigationTreeNodeProfileRequestByParent); 
            return oProfileResponse;
        }

        private void BusinessRule(ref TreeNode bn, ref MDITreeNodeHint bHint)
        {
            switch (bHint.NodeScreen)
            {
                case "reservelisting":
                    {//it comes here, a: when it is added as a claim child (in this case nothing is changed here, ShowContextMenu is already false(in ReadThisNarrowedProfile) because it is one to one)
                     // b: when close button is clicked and the text for reservelisting is refeshed to remove pencil. here we make sure whether context menu must be shown or not; while refreshing the text.
                     // c: a payment is created or edited so that reservelisting gets dirty. We set ShowContextMenu accordingly.
                        if (bn.ChildNodes.Count == 1 && string.Compare(bn.ChildNodes[0].Value, "DUMMY", true) != 0)
                        {
                            bHint.ShowContextMenu = true;
                        }
                        else
                        {
                            bHint.ShowContextMenu = false;
                        }
                        break;
                    }
                case "RESERVEWORKSHEET": //reserve current
                    { //it comes here, a: it is added as a reservelisting child while reservelisting is populated. in this case we set ShowContextMenu to false for reservelisting. for the node itself (RESERVEWORKSHEET) ShowContextMenu is already false because it is one to one.
                        //b: it is removed (the close button was clicked); context menu for reservelisting must be updated.
                      //c: it is removed because it was approved, and # of history and context menu for reservelisting must be updated.
                        //d: the reserve current is edited and gets dirty, but since there is no case in GetNodeHTMLText for reservecurrent then it soed not matter if ShowContextMenu is true or false for reserve current node; we don't need to do anything.
                        if (bn != null)
                        {
                            TreeNode bnParent = bn.Parent;
                            MDITreeNodeHint bHintParent = new MDITreeNodeHint(bn.ValuePath, "MDIParent", false);
                            if (bHint.IsRemoved == true || bHint.IsReplaced == true)
                            {
                                bHintParent.ShowContextMenu = true;
                                TreeNode bnHelp = null;
                                MDITreeNodeHint bHintHelp = null;
                                for (int i = 0; i < bnParent.ChildNodes.Count; i++)
                                {
                                    bnHelp = bnParent.ChildNodes[i];
                                    bHintHelp = new MDITreeNodeHint(bnHelp.ValuePath, false);
                                    if (bHintHelp.NodeScreen == "reserveworksheet")
                                    {
                                        TreeNode bnHelpHistory = null;
                                        MDITreeNodeHint bHintGrandParent = new MDITreeNodeHint(bnParent.ValuePath, "DataModelParent", false);
                                        MDINavigationTreeNodeProfileResponse bProfile = GetProfile(bHintParent.NodeScreen, Convert.ToInt32(bHintParent.NodeRecordId), bHintGrandParent.NodeScreen, Convert.ToInt32(bHintGrandParent.NodeRecordId));

                                        bHintHelp.ReadThisNarrowedProfile(bProfile.NodeChildern[0]);
                                        bHintHelp.ShowContextMenu = false;
                                        bHintHelp.ParentPathValue = bnParent.ValuePath;
                                        bnHelp = RefreshNode(bnHelp, bHintHelp);

                                        if (bHint.IsReplaced) {
                                            bProfile = GetProfile("reserveworksheet", Convert.ToInt32(bHint.NodeNewRecordId));
                                            bHint.ReadThisNarrowedProfile(bProfile.NodeDetail[0]);
                                            bnHelpHistory = InitFDMNode(bHint, bnHelp.ValuePath);
                                            bnHelp.ChildNodes.Add(bnHelpHistory);
                                            bnHelpHistory = RefreshNode(bnHelpHistory, bHint); //refreshing Breadcrumb
                                            bnHelpHistory.Select();
                                            bHint.IsRefreshed = true;

                                            MakeVisible(bnHelpHistory);
                                            SwitchToFDMScreen(bHint);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                bHintParent.ShowContextMenu = false;
                            }
                            if (bn.Value != "DUMMY")
                            {
                                bHintParent.ParentPathValue = bnParent.Parent.ValuePath;
                                bnParent = RefreshNode(bnParent, bHintParent);
                            }
                        }
                        break;
                    }
                case "reserveworksheet": //reserve history
                    {//it comes here, a: it (reserve history list node) is added as a child of reservelisting; so if parent is reservelisting and # of children for reservelisting is one (no pending) then we need context menu for reservelisting.
                     // b: it is a reserve history record to be added under reserve history list node. Later when we switch to reserveworksheet class we need two of them; 1- Reserveworksheet for record nodes 2- ReserveworksheetList for the list node.
                        TreeNode bnParent = bn.Parent;
                        MDITreeNodeHint bHintParent = new MDITreeNodeHint(bn.ValuePath, "MDIParent", false);

                        bHint.ShowContextMenu = false;
                        bHint.ParentPathValue = bnParent.ValuePath;
                        bn = RefreshNode(bn, bHint);

                        if (bHintParent.NodeScreen == "reservelisting" && bnParent.ChildNodes.Count == 1)
                        {
                            bHintParent.ShowContextMenu = true;
                            bHintParent.ParentPathValue = bnParent.Parent.ValuePath;
                            bnParent = RefreshNode(bnParent, bHintParent);
                        }
                        break;
                    }
            }
        }
        private void UpdateCurrentClaim(ref TreeNode tnRoot, ref MDITreeNodeHint hint)
        {
            TreeNode tn = null;
            TreeNode tnOld = null;
            MDITreeNodeHint hintParent = null;

            hintParent = hint;
            while (!hintParent.IsClaim && !hintParent.IsEvent)
            {
                //Added by Amitosh for MITS 24244 (03/07/2011)
                //hintParent = new MDITreeNodeHint(hintParent.NodePathValue, "DataModelParent", false);
                if (hintParent.NodePathValue != null)
                    hintParent = new MDITreeNodeHint(hintParent.NodePathValue, "DataModelParent", false);
                else
                    break;
            // end Amitosh
            }
            if (hintParent.IsClaim)// || hintParent.IsEvent)
            {
                tn = FindByMDIId(ref tnRoot, hintParent.NodeMDIId);
                if (tn != null)
                {
                    if (CurrentClaimMDIId != string.Empty)
                    {
                        tnOld = FindByMDIId(ref tnRoot, CurrentClaimMDIId);
                        if (tnOld != null)
                        {
                            tnOld.Text = tnOld.Text.Replace("<I>", string.Empty);
                            tnOld.Text = tnOld.Text.Replace("</I>", string.Empty);
                        }
                    }
                    tn.Text = "<I>" + tn.Text + "</I>";
                    CurrentClaimMDIId = hintParent.NodeMDIId;
                }
            }
        }
    }
}
