﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 14/01/2015 | RMA-344 | achouhan3  | Check for Browser version and refirect accordingly
 **********************************************************************************************/

using System;
using System.Linq;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
//using Riskmaster.UI.MDINavigationTreeNodeProfileService;
//using Riskmaster.UI.RMXResourceService;
//Ashish Ahuja Mits 33698
using Riskmaster.Security.Encryption;
using Riskmaster.Models;
using Riskmaster.Cache;

namespace Riskmaster.MDI.App_Code
{
    public class MDISetting
    {
        public string AllowedSearchScreens;
        public string AllowedTotalScreens;
        public string RelativePath;
        public string FDMLocalPath;
        public string FileExt;
        private string BaseDir;
        private List<string> landingArgs;
        private Infragistics.WebUI.UltraWebNavigator.UltraWebMenu MDIMenu;
        public string EventTile;
        public string DocumentTitle;
        public string sLang;
        public static Dictionary<string,Dictionary<string,string>> dictRootNodes = new Dictionary<string,Dictionary<string,string>>();
        public MDISetting()
        {
        }
        /// <summary>
        /// Provides a cached implementation of the MDI menu
        /// </summary>
        private string MDIMenuSiteMap
        {
            get
            {
                string sCacheItem = UpdateMDIMenu();
                return sCacheItem;
            }//get
        } // property MDIMenuSiteMap

        /// <summary>
        /// Get the MDI Menu
        /// </summary>
        /// <returns></returns>
        public string UpdateMDIMenu()
        {
            string cacheKey = "RMXMDIMenu";
            sLang = AppHelper.GetLanguageCode();
            string cacheItem = string.Empty;
            try
            {
                cacheKey = cacheKey + "_" + sLang;
                cacheItem = CacheCommonFunctions.RetreiveValueFromCache<string>(cacheKey, AppHelper.ClientId);// HttpRuntime.Cache[cacheKey] as string;
                if (string.IsNullOrEmpty(cacheItem))
                {
                    Riskmaster.Models.RMResource oRMResource = new Riskmaster.Models.RMResource();
                    oRMResource.LanguageCode = sLang;
                    oRMResource.Token = AppHelper.ReadCookieValue("SessionId");
                    oRMResource.ClientId = Convert.ToInt32(AppHelper.ClientId);
                    cacheItem = AppHelper.GetResponse<string>("RMService/Resource/mdi", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oRMResource); 
                    if (string.IsNullOrEmpty(cacheItem))
                    {
                        oRMResource.LanguageCode = AppHelper.GetBaseLanguageCodeWithCulture().Split('|')[0].ToString();
                        cacheItem = AppHelper.GetResponse<string>("RMService/Resource/mdi", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oRMResource); 
                    }
                    //HttpRuntime.Cache.Insert(cacheKey, cacheItem);
                    CacheCommonFunctions.AddValue2Cache<object>(cacheKey, AppHelper.ClientId, cacheItem);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
            return cacheItem;
        }
        /// <summary>
        /// Provides a cached implementation of the MDI navigation settings
        /// </summary>
        private XElement MDINavigationSettings
        {
            get
            {
                string cacheKey = "RMXMDINavigation";
                //string sMDISettingsXML = GetMDISettingsXML();
                object cacheItem = CacheCommonFunctions.RetreiveValueFromCache<object>(cacheKey, AppHelper.ClientId); //HttpContext.Current.Cache[cacheKey] as XElement;//todo : Deb
                if (cacheItem == null)
                {
                    cacheItem = XElement.Load(String.Format("{0}\\MDI\\App_Data\\MDISettings.xml", this.BaseDir));
                    //cacheItem = XElement.Parse(sMDISettingsXML);
                    CacheCommonFunctions.AddValue2Cache<object>(cacheKey, AppHelper.ClientId, cacheItem);
                    //HttpContext.Current.Cache.Insert(cacheKey, cacheItem, new CacheDependency(mdiSettingsPath)); 
                }//if

                return (XElement)cacheItem;
            }//get
        } // property MDINavigationSettings

        private string GetMDISettingsXML()
        {
            return @"<?xml version='1.0' encoding='utf-8' ?>
                     <MDI>
	                    <relativePath>../</relativePath> <!-- The default starting local path is 'MDI/Default.aspx'; so it needs to go one level back to reach RiskmasterUI virtual directory.-->
                      <FDMLocalPath>UI/FDM/</FDMLocalPath>
                      <fileExt>.aspx</fileExt>
	                    <clientMemory>
		                    <limited> <!-- 750 MB -->
			                    <selected>NO</selected>
			                    <allowedSearchScreens>8</allowedSearchScreens>
			                    <allowedTotalScreens>20</allowedTotalScreens>
	                        </limited>

		                    <normal> <!-- 1GB to 2GB -->
			                    <selected>YES</selected>
			                    <allowedSearchScreens>15</allowedSearchScreens>
			                    <allowedTotalScreens>50</allowedTotalScreens>
		                    </normal>

		                    <extended> <!-- 3GB and above -->
			                    <selected>NO</selected>
			                    <allowedSearchScreens>30</allowedSearchScreens>
			                    <allowedTotalScreens>100</allowedTotalScreens>
		                    </extended>
	                    </clientMemory>
                     </MDI>";
        }
        public MDISetting(ref Infragistics.WebUI.UltraWebNavigator.UltraWebMenu MDIMenu, ref List<string> landingArgs)
        {
            //Riskmaster.UI.MDINavigationTreeNodeProfileService.InitMDIRequest oRequest = new Riskmaster.UI.MDINavigationTreeNodeProfileService.InitMDIRequest();
            //Riskmaster.UI.MDINavigationTreeNodeProfileService.InitMDIResponse oResponse = new Riskmaster.UI.MDINavigationTreeNodeProfileService.InitMDIResponse();
            //Riskmaster.UI.MDINavigationTreeNodeProfileService.MDINavigationTreeNodeProfileServiceClient oClient = new Riskmaster.UI.MDINavigationTreeNodeProfileService.MDINavigationTreeNodeProfileServiceClient();

            this.MDIMenu = MDIMenu;
            this.landingArgs = landingArgs;
            BaseDir = System.AppDomain.CurrentDomain.BaseDirectory.ToString();

            //oRequest.ViewId = Convert.ToInt32(AppHelper.ReadCookieValue("ViewId"));
            //oRequest.Token = AppHelper.ReadCookieValue("SessionId");
            //oRequest.SerializedMDIMenu = this.MDIMenuSiteMap;
            //oRequest.ClientId = AppHelper.ClientId;
            //oResponse = oClient.InitMDI(oRequest);

            InitMDIRequest oInitMDIRequest = new InitMDIRequest();
            oInitMDIRequest.ViewId = Convert.ToInt32(AppHelper.ReadCookieValue("ViewId"));
            oInitMDIRequest.Token = AppHelper.ReadCookieValue("SessionId");
            oInitMDIRequest.SerializedMDIMenu = this.MDIMenuSiteMap;
            oInitMDIRequest.ClientId = AppHelper.ClientId;
            InitMDIResponse oResponse = AppHelper.GetResponse<InitMDIResponse>("RMService/MDI/init", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oInitMDIRequest); 

            SetMenu(oResponse.SerializedMDIMenu);
            SetProperties();
        }
        
            
        //private void SetMenu(string serializedMDIMenu)
        private void SetMenu(XElement xml)
        {
            Infragistics.WebUI.UltraWebNavigator.Items currentLevel = MDIMenu.Items;
            Infragistics.WebUI.UltraWebNavigator.Item menuItem;
            // XElement xml = XElement.Parse(serializedMDIMenu);
            string rootName = string.Empty;
            string navTreeArg = string.Empty;
            object obj = new object();
            Dictionary<string, string> dictNew = new Dictionary<string, string>();
            bool isBrowserSupport = false;// RMA-9874   Added for consideration of local path also in case of IE version >11 
            foreach (XElement el in xml.Descendants())
            {
                //if (el.Parent.Attribute("title") != null)
                if (el.Parent.Name == MDIMenu.ID)
                {
                    currentLevel = MDIMenu.Items;
                    rootName = string.Empty;
                }
                else
                {
                    while (el.Parent.Attribute("title").Value != currentLevel.OwnerItem.Text)
                    {
                        currentLevel = currentLevel.OwnerItem.Parent.Items;
                    }
                }

                if (el.Attribute("title") == null || el.Attribute("title").Value == string.Empty)
                {
                    el.Descendants().Remove();
                }
                else
                {
                    //nkaranam2 - 34408
                    Boolean blnFlag = false;
                    if (string.Compare(el.Attribute("title").Value, "Loss Code Mapping") == 0)
                    {
                        GetSysSettingsRequest oGetSysSettingsRequest = new GetSysSettingsRequest();
                        GetSysSettingsResponse oResponse = new GetSysSettingsResponse();
                        //MDINavigationTreeNodeProfileServiceClient oClient = new MDINavigationTreeNodeProfileServiceClient();
                        oGetSysSettingsRequest.Token = AppHelper.ReadCookieValue("SessionId");
                        List<String> oReqList = new List<String>();
                        oReqList.Add("MultiCovgPerClm");
                        oReqList.ToArray();
                        //String[] reqList = new String[5];
                        //reqList[0] = "MultiCovgPerClm";
                        oGetSysSettingsRequest.oSysSettingsReqList = oReqList.ToList<String>();
                        oGetSysSettingsRequest.ClientId = AppHelper.ClientId;
                        oGetSysSettingsRequest.Token = AppHelper.ReadCookieValue("SessionId");
                        //oResponse = oClient.GetSysSettings(oRequest);
                        oResponse = AppHelper.GetResponse<GetSysSettingsResponse>("RMService/MDI/settings", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oGetSysSettingsRequest);
                        Dictionary<String, Object> oDTResponse = new Dictionary<String, Object>();
                        oDTResponse = oResponse.oSysSettingsList;
                        int MulCovPol = (int)oDTResponse["MultiCovgPerClm"];
                        if (MulCovPol == 0)
                            blnFlag = true;
                    }
                    //nkaranam2 - 34408
                    if (el.Attribute("visible") != null && el.Attribute("visible").Value == Boolean.TrueString.ToString() && !blnFlag)
                    {
                        menuItem = new Infragistics.WebUI.UltraWebNavigator.Item();
                        menuItem.Text = el.Attribute("title").Value;
                        if (rootName == string.Empty)
                        {
                            //Deb ML Changes
                            //rootName = menuItem.Text;
                            switch (el.Attribute("sysName").Value)
                            {
                                case "DocumentRoot":
                                    rootName = menuItem.Text + "|Document";
                                    DocumentTitle = menuItem.Text;
                                    lock (obj)
                                    {
                                        dictNew.Add("Document", menuItem.Text);
                                    }
                                    break;
                                case "PolicyRoot":
                                    rootName = menuItem.Text + "|Policy";
                                    lock (obj)
                                    {
                                        dictNew.Add("Policy", menuItem.Text);
                                    }
                                    break;
                                case "DiariesRoot":
                                    rootName = menuItem.Text + "|Diaries";
                                    lock (obj)
                                    {
                                        dictNew.Add("Diaries", menuItem.Text);
                                    }
                                    break;
                                case "FundsRoot":
                                    rootName = menuItem.Text + "|Funds";
                                    lock (obj)
                                    {
                                        dictNew.Add("Funds", menuItem.Text);
                                    }
                                    break;
                                case "MaintenanceRoot":
                                    rootName = menuItem.Text + "|Maintenance";
                                    lock (obj)
                                    {
                                        dictNew.Add("Maintenance", menuItem.Text);
                                    }
                                    break;
                                case "MyWorkRoot":
                                    rootName = menuItem.Text + "|My Work";
                                    lock (obj)
                                    {
                                        dictNew.Add("My Work", menuItem.Text);
                                    }
                                    break;
                                case "ReportsRoot":
                                    rootName = menuItem.Text + "|Reports";
                                    lock (obj)
                                    {
                                        dictNew.Add("Reports", menuItem.Text);
                                    }
                                    break;
                                case "SearchRoot":
                                    rootName = menuItem.Text + "|Search";
                                    lock (obj)
                                    {
                                        dictNew.Add("Search", menuItem.Text);
                                    }
                                    break;
                                case "SecurityRoot":
                                    rootName = menuItem.Text + "|Security";
                                    lock (obj)
                                    {
                                        dictNew.Add("Security", menuItem.Text);
                                    }
                                    break;
                                case "UserDocumentsRoot":
                                    rootName = menuItem.Text + "|User Documents";
                                    lock (obj)
                                    {
                                        dictNew.Add("User Documents", menuItem.Text);
                                    }
                                    break;
                                case "utilitiesRoot":
                                    rootName = menuItem.Text + "|Utilities";
                                    lock (obj)
                                    {
                                        dictNew.Add("Utilities", menuItem.Text);
                                    }
                                    break;
                                case "HelpRoot":
                                    rootName = menuItem.Text + "|Help";
                                    lock (obj)
                                    {
                                        dictNew.Add("Help", menuItem.Text);
                                    }
                                    break;
                                case "OtherRoot":
                                    rootName = menuItem.Text + "|Other";
                                    lock (obj)
                                    {
                                        dictNew.Add("Other", menuItem.Text);
                                    }
                                    break;
                            }

                            //Deb ML Changes
                        }
                        if (el.Attribute("active") != null && el.Attribute("active").Value == Boolean.TrueString.ToString())
                        {
                            navTreeArg = string.Empty;
                            if (el.Attribute("sysName") != null && el.Attribute("sysName").Value != string.Empty && el.Attribute("localPath") != null && el.Parent.Name != MDIMenu.ID) // this is not a root and it is linked to a screen
                            {
                                //Deb ML Changes
                                navTreeArg = rootName + MDITreeNodeHint.PathValueSeparator + rootName.Split('|')[1].ToString() + MDITreeNodeHint.UnitSeparator;
                                //Deb ML Changes
                                if (el.Attribute("recordId") != null && el.Attribute("recordId").Value != string.Empty)
                                {
                                    navTreeArg += el.Attribute("recordId").Value;
                                }
                                else
                                {
                                    navTreeArg += "0";
                                }

                                //RMA-344 starts     Achouhan3        Check for IEbrowser and according decide to open search result page
                                if (HttpContext.Current.Request.Browser != null && String.Compare(HttpContext.Current.Request.Browser.Browser, "IE", true) == 0
                                    && HttpContext.Current.Request.Browser.MajorVersion < 10)
                                {
                                    navTreeArg += MDITreeNodeHint.GroupSeparator + el.Attribute("sysName").Value;
                                    isBrowserSupport = false;////RMA-9874  added for localpath consideration in case of IE version >11 starts
                                }
                                else if (el.Attribute("altSysName") != null)
                                {
                                    navTreeArg += MDITreeNodeHint.GroupSeparator + el.Attribute("altSysName").Value;
                                    isBrowserSupport = true;////RMA-9874  added for localpath consideration in case of IE version >11 starts
                                }
                                else
                                {
                                    isBrowserSupport = false;////RMA-9874  added for localpath consideration in case of IE version >11 starts
                                    navTreeArg += MDITreeNodeHint.GroupSeparator + el.Attribute("sysName").Value;
                                }
                                //navTreeArg += MDITreeNodeHint.GroupSeparator + el.Attribute("sysName").Value;
                                //RMA-344 ends     Achouhan3        Check for IEbrowser and according decide to open search result page

                                navTreeArg += MDITreeNodeHint.UnitSeparator + el.Attribute("title").Value;
                                //RMA-9874  added for localpath consideration in case of IE version >11 starts
                                //navTreeArg += MDITreeNodeHint.UnitSeparator + el.Attribute("localPath").Value;
                                if (isBrowserSupport && el.Attribute("localPath") != null && !String.IsNullOrEmpty(el.Attribute("localPath").Value)
                                    && el.Attribute("localPath").Value.Contains(".aspx"))
                                {
                                    navTreeArg += MDITreeNodeHint.UnitSeparator + el.Attribute("localPath").Value.Replace(System.IO.Path.GetFileName(el.Attribute("localPath").Value), "");
                                }
                                else
                                    navTreeArg += MDITreeNodeHint.UnitSeparator + el.Attribute("localPath").Value;
                                //RMA-9874  added for localpath consideration in case of IE version >11 ends 
                                navTreeArg += MDITreeNodeHint.UnitSeparator;
                                if (el.Attribute("params") != null)
                                {
                                    navTreeArg += el.Attribute("params").Value;
                                }
                                if (el.Attribute("sysName").Value == "event")
                                {
                                    EventTile = menuItem.Text;
                                }
                                navTreeArg = MDITreeNodeHint.EncodeSpecialChars(navTreeArg);

                                menuItem.TargetUrl = String.Format("javascript:__doPostBack('UpdatePanel1', '{0}');", navTreeArg);
                            }
                            if (el.Attribute("targetFrame") != null && el.Attribute("targetFrame").Value != string.Empty)
                            {
                                menuItem.TargetFrame = el.Attribute("targetFrame").Value;
                                menuItem.TargetUrl = el.Attribute("localPath").Value;
                            }
                            if (el.Attribute("landingItem") != null && navTreeArg != string.Empty)
                            {
                                if (el.Attribute("landingItem").Value.ToUpper() == "YES")
                                {
                                    this.landingArgs.Insert(0, (navTreeArg)); // we add non-primary one at the beginning
                                }
                                else if (el.Attribute("landingItem").Value.ToUpper() == "PRIMARY")
                                {
                                    this.landingArgs.Add(navTreeArg); // We add the Primary one at the end
                                }
                            }
                        }
                        else
                        {
                            menuItem.Styles.ForeColor = System.Drawing.Color.Gray;
                            foreach (XElement el2 in el.Descendants())
                            {
                                el2.SetAttributeValue("active", Boolean.FalseString.ToString());
                            }
                        }

                        currentLevel.Add(menuItem);

                        if (el.HasElements)
                        {
                            currentLevel = menuItem.Items;
                        }
                    }
                    else
                    {
                        el.Descendants().Remove();
                    }

                }
            }
            // akaushik5 Added for RMA-14280 Starts
            lock (obj)
            {
                // akaushik5 Added for RMA-14280 Ends
                if (!dictRootNodes.ContainsKey(sLang))
                {
                    dictRootNodes.Add(sLang, dictNew);
                }
                // akaushik5 Added for RMA-14280 Starts
            }
            // akaushik5 Added for RMA-14280 Ends
            dictNew = null;
        }

        private void SetProperties()
        {
            XElement xml = this.MDINavigationSettings;

            IEnumerable<XElement> query = from c in xml.Descendants("selected")
                                          where c.Value == "YES"
                                          select c.Parent.Element("allowedSearchScreens");
            XElement el = query.FirstOrDefault();
            if (el != null)
            {
                this.AllowedSearchScreens = el.Value;
            }
            else
            {
                throw new ApplicationException("The allowedSearchScreens element is missing in MDIsettings.xml!");
            }


            query = from c in xml.Descendants("selected")
                    where c.Value == "YES"
                    select c.Parent.Element("allowedTotalScreens");
            el = query.FirstOrDefault();
            if (el != null)
            {
                this.AllowedTotalScreens = el.Value;
            }
            else
            {
                throw new ApplicationException("The allowedTotalScreens element is missing in MDIsettings.xml!");
            }


            query = from c in xml.Descendants("relativePath")
                    select c;
            el = query.FirstOrDefault();
            if (el != null)
            {
                //Ashish Ahuja Mits 33698
               // this.RelativePath = el.Value;
                this.RelativePath = RMCryptography.EncryptString(el.Value);

            }
            else
            {
                throw new ApplicationException("The relativePath element is missing in MDIsettings.xml!");
            }


            query = from c in xml.Descendants("FDMLocalPath")
                    select c;
            el = query.FirstOrDefault();
            if (el != null)
            {
                this.FDMLocalPath = el.Value;
            }
            else
            {
                throw new ApplicationException("The FDMLocalPath element is missing in MDIsettings.xml!");
            }


            query = from c in xml.Descendants("fileExt")
                    select c;
            el = query.FirstOrDefault();
            if (el != null)
            {
                this.FileExt = el.Value;
            }
            else
            {
                throw new ApplicationException("The fileExt element is missing in MDIsettings.xml!");
            }
        }
    }
    //Deb: Multi Language Changes can be implementd in future
    /// <summary>
    /// MDIMenuResourceHelper helps to update the resource 
    /// </summary>
    //public class MDIMenuResourceHelper
    //{
    //    //Deb:Extra Things are commented and can be used in future
    //    private System.Timers.Timer m_Timer = new System.Timers.Timer();
    //    int m_PollingInterval = 300;
    //    public static bool IsTimerStarted { get; set; }
    //    private static volatile MDIMenuResourceHelper m_instance = null;
    //    private static object syncFactory = new object();
    //    private string m_LastUpdated = string.Empty;
    //    private Dictionary<string, string> MDICacheItems = new Dictionary<string, string>();
    //    /// <summary>
    //    /// Constructor
    //    /// </summary>
    //    private MDIMenuResourceHelper()
    //    {
    //        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RMXResourcePollingInterval"]))
    //        {
    //            m_PollingInterval = int.Parse(ConfigurationManager.AppSettings["RMXResourcePollingInterval"]);
    //            if (m_PollingInterval < 60)
    //                m_PollingInterval = 60;
    //        }
    //        m_Timer.Interval = m_PollingInterval * 1000;
    //        m_Timer.Elapsed += new ElapsedEventHandler(CheckDependencyHandler);
    //        m_Timer.Start();
    //        IsTimerStarted = true;
    //    }
    //    /// <summary>
    //    /// Get the singleton object
    //    /// </summary>
    //    public static MDIMenuResourceHelper Instance
    //    {
    //        get
    //        {
    //            if (m_instance == null)
    //            {
    //                lock (syncFactory)
    //                {
    //                    if (m_instance == null)
    //                        m_instance = new MDIMenuResourceHelper();
    //                }
    //            }
    //            return m_instance;
    //        }
    //    }
    //    /// <summary>
    //    /// Resets the cache if required
    //    /// </summary>
    //    /// <param name="sender"></param>
    //    /// <param name="e"></param>
    //    private void CheckDependencyHandler(object sender, ElapsedEventArgs e)
    //    {
    //        if (MDICacheItems.Count == 0)
    //            return;
    //        RMXResourceServiceClient oResClient = new RMXResourceServiceClient();
    //        List<string> list = null;
    //        string maxLastUpdated = string.Empty;
    //        try
    //        {
    //            List<string> oModifiedList = oResClient.GetChangedMDIMenuList(m_LastUpdated, out maxLastUpdated).ToList();
    //            if (oModifiedList == null)
    //                return;
    //            foreach (string sLang in oModifiedList)
    //            {
    //                list = new List<string>(MDICacheItems.Keys);
    //                foreach (string key in list)
    //                {
    //                    if (key.StartsWith("RMXMDIMenu_" + sLang))
    //                        MDICacheItems.Remove(key);
    //                }
    //            }
    //            m_LastUpdated = maxLastUpdated;
    //        }
    //        catch (Exception ee)
    //        {
    //        }
    //        finally
    //        {
    //            if (oResClient != null)
    //                oResClient.Close();
    //            list = null;
    //        }
    //    }
    //    //Deb:Extra Things are commented and can be used in future
    //    /// <summary>
    //    /// Gets the fresh new MDI Menu XML from database
    //    /// </summary>
    //    /// <returns></returns>
    //    public string UpdateMDIMenu()
    //    {
    //        string cacheKey = "RMXMDIMenu";
    //        string sLang = AppHelper.GetLanguageCode();
    //        RMXResourceServiceClient oResClient = new RMXResourceServiceClient();
    //        //string sDsnId = AppHelper.ReadCookieValue("DsnId");
    //        string cacheItem = string.Empty;
    //        try
    //        {
    //            //mdiMenuPath = String.Format("{0}\\MDI\\App_Data\\MDIMenu.xml", System.AppDomain.CurrentDomain.BaseDirectory.ToString());
    //            //cacheItem = XElement.Load(mdiMenuPath).ToString();
    //            //string sConnString = oResClient.GetConnectionstringFromDSNId(int.Parse(sDsnId));
    //            string sConnString = oResClient.GetConnectionstring("ViewDataSource");
    //            //Can be used in future
    //            //string sLastUpdated = oResClient.GetTimestampForMDIMenu(sConnString, Convert.ToInt32(sLang));
    //            //cacheKey = cacheKey + "_" + sLang + "_" + sLastUpdated;
    //            cacheKey = cacheKey + "_" + sLang;
    //            if(MDICacheItems.ContainsKey(cacheKey))
    //             cacheItem = MDICacheItems[cacheKey] as string;
    //            if (string.IsNullOrEmpty(cacheItem))
    //            {
    //                cacheItem = oResClient.GetMDIMenuXML(sConnString, Convert.ToInt32(sLang));
    //                if (string.IsNullOrEmpty(cacheItem))
    //                {
    //                  cacheItem = oResClient.GetMDIMenuXML(sConnString, 1033);
    //                }
    //                MDICacheItems.Add(cacheKey, cacheItem);
    //                //m_LastUpdated = sLastUpdated;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //        }
    //        finally
    //        {
    //            if (oResClient != null)
    //                oResClient.Close();
    //        }
    //        return cacheItem;
    //    }
    //}
}
