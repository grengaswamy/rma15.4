﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.ServiceModel;
using Riskmaster.Models;

namespace Riskmaster.ServiceHelpers
{
    public class PowerViewUpgradeServiceHelper
    {
        public string UpgradeXmlTagToAspxForm(bool breadonly, bool bTopDown, string viewXml, string divName)
        {
            PVUpgradeData request = null;
            PVUpgradeData objReturn;
            string returnVal = "";
            try
            {

                request = new PVUpgradeData();
                request.Token = AppHelper.GetSessionId();
                request.breadonly = breadonly;
                request.bTopDown = bTopDown;
                request.viewXml = viewXml;
                request.divName = divName;
                request.ClientId = AppHelper.ClientId;
                objReturn = AppHelper.GetResponse<PVUpgradeData>("RMService/PVUpgrade/upgrade", AppHelper.HttpVerb.POST, "application/json", request); 
                returnVal = objReturn.convertedAspx;
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return returnVal;
        }

        public string FetchJurisdictionalData(int stateID)
        {
            PVUpgradeData request = null;
            PVUpgradeData objReturn;
            string returnVal = "";
            try
            {
                request = new PVUpgradeData();
                request.Token = AppHelper.GetSessionId();
                request.ClientId = AppHelper.ClientId;
                request.stateID = stateID;
                objReturn = AppHelper.GetResponse<PVUpgradeData>("RMService/PVUpgrade/fetch", AppHelper.HttpVerb.POST, "application/json", request);
                returnVal = objReturn.jurisdictionalAspx;
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return returnVal;
        }
    }
}
