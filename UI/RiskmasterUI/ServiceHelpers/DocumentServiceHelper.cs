﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using Riskmaster.UI.DocumentService;
//using Riskmaster.UI.DataStreamingService;
using System.ServiceModel;
using System.Collections.Generic;
using Riskmaster.Models;

namespace Riskmaster.ServiceHelpers
{
    //All work related to manipuations / Add / Update / Delete would be done here
    public class DocumentServiceHelper
    {
        public DocumentType UpdateDocumentFile(DocumentType oDocumentType)
        {
            //DocumentServiceClient rmservice = null;
            DocumentType returnVal = null;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentType.ClientId = AppHelper.ClientId;
                //returnVal = rmservice.UpdateDocumentFile(document);
                returnVal = AppHelper.GetResponse<DocumentType>("RMService/Document/file", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oDocumentType);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }

        //rbhatia4:R8: Use Media View Setting : July 08 2011
        //Restructuring Document Management Code for choosing the selected vendor
        /*
		//Mona:PaperVision Merge: Checking whether PaperVision is ON or OFF
        public string GetPaperVisionSetting(DocumentType document)
        {
            DocumentServiceClient rmservice = null;
            string sIsPaperVisionOn = "0";
            try
            {
                rmservice = new DocumentServiceClient();
                sIsPaperVisionOn = rmservice.GetPaperVisionSetting(document);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return sIsPaperVisionOn;
        }
         */

        //rbhatia4:R8: Use Media View Setting : July 05 2011
        //Making  a general helper method to find out which document management  system has been used

        public DocumentVendorDetails GetDocumentManagementVendorDetails(DocumentVendorDetails oDocumentVendorDetails)
        {
            //DocumentServiceClient rmservice = null;
            string sVendorName = String.Empty;
            bool bObjResponse = false;
            DocumentVendorDetails objResponse = null;
            
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentVendorDetails.ClientId = AppHelper.ClientId;
                //objResponse = rmservice.GetDocumentManagementVendorDetails(document);
                objResponse = AppHelper.GetResponse<DocumentVendorDetails>("RMService/Document/vendordetails", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentVendorDetails);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return objResponse;
        }

        public RMServiceType AddDocument(DocumentType oDocumentType)
        {
            //DocumentServiceClient rmservice = null;
            RMServiceType returnVal = null;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentType.ClientId = AppHelper.ClientId;
                //returnVal = rmservice.CreateDocument(document);
                returnVal = AppHelper.GetResponse<RMServiceType>("RMService/Document/create", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentType);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }

        public DocumentType CreateDocumentDetail(DocumentType oDocumentType)
        {
            //DocumentServiceClient rmservice = null;
            DocumentType objResponse = null;
            long returnVal = 0;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentType.ClientId = AppHelper.ClientId;
                objResponse = AppHelper.GetResponse<DocumentType>("RMService/Document/createdetail", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentType);
                //objResponse = rmservice.CreateDocumentDetail(document);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return objResponse;
        }

        public RMServiceType AddLargeDocument(Riskmaster.UI.DataStreamingService.StreamedDocumentType document)
        {
            Riskmaster.UI.DataStreamingService.DataStreamingServiceClient rmservice = null;
            RMServiceType returnVal = null;
            try
            {
               // AppHelper.GetResponse(oStreamedDocumentType.BaseUrl + "RMService/DataStream/create", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oStreamedDocumentType);
                rmservice = new Riskmaster.UI.DataStreamingService.DataStreamingServiceClient();
                rmservice.CreateDocument(document.AttachRecordId,
                    document.AttachTable,
                    document.Category,
                    //npadhy Claim Number needs to be sent from Mobility
                    string.Empty,
                    document.Class,
                    AppHelper.ClientId,
                    document.Copy,
                    document.Create,
                    document.CreateDate,
                    document.Delete,
                    document.DocInternalType,
                    document.DocumentCategory,
                    document.DocumentClass,
                    document.DocumentId,
                    document.DocumentsType,
                    document.Download,
                    document.Edit,
                    document.Email,
                    document.Errors,
                    document.FileContents,
                    document.FileName,
                    document.FilePath,
                    document.FileSize,
                    document.FolderId,
                    document.FolderName,
                    document.FormName,
                    document.Keywords,
                    document.Move,
                    document.Notes,
                    document.Pid,
                    document.PsId,
                    document.Readonly,
                    document.ScreenFlag,
                    document.Subject,
                    document.Title,
                    document.Token,
                    document.Transfer,
                    document.Type,
                    document.UserId,
                    document.UserName,
                    document.View,
                    document.fileStream);
            }
            catch (FaultException<Riskmaster.UI.DataStreamingService.RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return returnVal;
        }

        public DocumentList DocumentsList(DocumentListRequest oDocumentListRequest)
        {
            //DocumentServiceClient rmservice = null;
            DocumentList returnVal = null;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentListRequest.ClientId = AppHelper.ClientId;
                //returnVal = rmservice.GetDocumentsList(document);
                returnVal = AppHelper.GetResponse<DocumentList>("RMService/Document/list", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentListRequest);
            }
            catch (FaultException<Riskmaster.Models.RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public DocumentList GetAttachments(DocumentListRequest oDocumentListRequest)
        {
            //DocumentServiceClient rmservice = null;
            DocumentList returnVal = null;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentListRequest.ClientId = AppHelper.ClientId;
                //returnVal = rmservice.GetAttachments(document);
                returnVal = AppHelper.GetResponse<DocumentList>("RMService/Document/attachments", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentListRequest);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public List<Folder> GetFoldersList(Folder oFolder)
        {
            //DocumentServiceClient rmservice = null;
            List<Folder> returnVal = null;
            try
            {
                oFolder.ClientId = AppHelper.ClientId;
                //rmservice = new DocumentServiceClient();
                //returnVal = rmservice.GetFolders(folder).ToList<Folder>();
                returnVal = AppHelper.GetResponse<List<Folder>>("RMService/Document/folderlist", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oFolder);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public UsersList GetUsers(DocumentUserRequest oDocumentUserRequest)
        {
            //DocumentServiceClient rmservice = null;
            UsersList returnVal = null;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentUserRequest.ClientId = AppHelper.ClientId;
                //returnVal = rmservice.GetUsers(request);
                returnVal = AppHelper.GetResponse<UsersList>("RMService/Document/users", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentUserRequest);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }

        public bool CreateFolder(FolderTypeRequest oFolderTypeRequest)
        {
            //DocumentServiceClient rmservice = null;
            bool returnVal = false;
            try
            {
                //rmservice = new DocumentServiceClient();
                //returnVal = rmservice.CreateFolder(folder);
                oFolderTypeRequest.ClientId = AppHelper.ClientId;
                returnVal = AppHelper.GetResponse<bool>("RMService/Document/folder", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oFolderTypeRequest);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public bool Delete(DocumentsDeleteRequest oDocumentsDeleteRequest)
        {
            //DocumentServiceClient rmservice = null;
            bool returnVal = false;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentsDeleteRequest.ClientId = AppHelper.ClientId;
                returnVal = AppHelper.GetResponse<bool>("RMService/Document/remove", AppHelper.HttpVerb.DELETE, AppHelper.APPLICATION_JSON, oDocumentsDeleteRequest);
                //returnVal = rmservice.Delete(request);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
               // rmservice.Close();
            }
            return returnVal;
        }
        public bool EmailDocuments(EmailDocumentsRequest oEmailDocumentsRequest)
        {
            //DocumentServiceClient rmservice = null;
            bool returnVal = false;
            try
            {
                //rmservice = new DocumentServiceClient();
                //returnVal = rmservice.EmailDocuments(request);
                oEmailDocumentsRequest.ClientId = AppHelper.ClientId;
                returnVal = AppHelper.GetResponse<bool>("RMService/Document/email", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oEmailDocumentsRequest);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public bool MoveDocuments(MoveDocumentsRequest oMoveDocumentsRequest)
        {
            //DocumentServiceClient rmservice = null;
            bool returnVal = false;
            try
            {
                //rmservice = new DocumentServiceClient();
                //returnVal = rmservice.MoveDocuments(request);
                oMoveDocumentsRequest.ClientId = AppHelper.ClientId;
                returnVal = AppHelper.GetResponse<bool>("RMService/Document/move", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oMoveDocumentsRequest);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public bool TransferDocuments(TransferDocumentsRequest oTransferDocumentsRequest)
        {
            //DocumentServiceClient rmservice = null;
            bool returnVal = false;
            try
            {
                //rmservice = new DocumentServiceClient();
                //returnVal = rmservice.TransferDocuments(request);
                oTransferDocumentsRequest.ClientId = AppHelper.ClientId;
                returnVal = AppHelper.GetResponse<bool>("RMService/Document/transfer", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oTransferDocumentsRequest);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public bool CopyDocuments(TransferDocumentsRequest oTransferDocumentsRequest)
        {
            //DocumentServiceClient rmservice = null;
            bool returnVal = false;
            try
            {
                //rmservice = new DocumentServiceClient();
                //returnVal = rmservice.CopyDocuments(request);
                oTransferDocumentsRequest.ClientId = AppHelper.ClientId;
                returnVal = AppHelper.GetResponse<bool>("RMService/Document/copy", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oTransferDocumentsRequest);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public bool UpdateDocument(DocumentType oDocumentType)
        {
            //DocumentServiceClient rmservice = null;
            bool returnVal = false;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentType.ClientId = AppHelper.ClientId;
                //returnVal = rmservice.UpdateDocument(request);
                returnVal = AppHelper.GetResponse<bool>("RMService/Document/edit", AppHelper.HttpVerb.PUT, AppHelper.APPLICATION_JSON, oDocumentType);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public DocumentType EditDocument(DocumentType oDocumentType)
        {
            //DocumentServiceClient rmservice = null;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentType.ClientId = AppHelper.ClientId;
                //oDocumentType = rmservice.GetDocument(document);
                oDocumentType = AppHelper.GetResponse<DocumentType>("RMService/Document/fetch", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentType);
                oDocumentType.CreateDate = AppHelper.GetDateTimeForUI(oDocumentType.CreateDate);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return oDocumentType;
        }
        public DocumentType Download(DocumentType oDocumentType)
        {
            //DocumentServiceClient rmservice = null;
            DocumentType returnVal = null; 
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentType.ClientId = AppHelper.ClientId;
                //returnVal = rmservice.RetrieveDocument(document);
                returnVal = AppHelper.GetResponse<DocumentType>("RMService/Document/retrieve", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentType);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }

        public Stream DownloadLargeDocument(StreamedDocumentType document)
        {
            Riskmaster.UI.DataStreamingService.DataStreamingServiceClient rmservice = null;
            Stream returnVal = null;
            try
            {
                rmservice = new Riskmaster.UI.DataStreamingService.DataStreamingServiceClient();
                returnVal = rmservice.RetrieveDocument(document.DocumentId.ToString(), document.PsId.ToString(), document.ScreenFlag, document.Token,document.ClientId,document.FormName); //rsushilaggar MITS 23234
                //byte[] obytes = AppHelper.GetResponse<byte[]>(oStreamedDocumentType.BaseUrl + "RMService/DataStream/retrieve", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oStreamedDocumentType);
                //returnVal = new MemoryStream(obytes);
            }
            catch (FaultException<Riskmaster.UI.DocumentService.RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return returnVal;
        }

        public DocumentType GetExecutiveSummaryFileDetails(DocumentType oDocumentType)
        {
            //DocumentServiceClient rmservice = null;
            DocumentType returnVal = null;
            try
            {
                //rmservice = new DocumentServiceClient();
                oDocumentType.ClientId = AppHelper.ClientId;
                //returnVal = rmservice.GetExecutiveSummaryFileDetails(document);
                returnVal = AppHelper.GetResponse<DocumentType>("RMService/Document/execsummary", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentType);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public bool CheckOutlookSetting(DocumentType oDocumentType)
        {
            //DocumentServiceClient rmservice = null;
            bool returnVal = false;
           
            try
            {
                //rmservice = new DocumentServiceClient();
                //returnVal = rmservice.CheckOutlookSetting(document);
                oDocumentType.ClientId = AppHelper.ClientId;
                returnVal = AppHelper.GetResponse<bool>("RMService/Document/outlooksetting", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oDocumentType);
            }
            catch (FaultException<RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                //rmservice.Close();
            }
            return returnVal;
        }
        public RMServiceType UploadFile(Riskmaster.UI.DataStreamingService.StreamedUploadDocumentType oRequest)
        {
            Riskmaster.UI.DataStreamingService.DataStreamingServiceClient rmservice = new UI.DataStreamingService.DataStreamingServiceClient();
            RMServiceType returnVal = null;
            try
            {
                //rmservice.UploadFile(oRequest.DocumentType, oRequest.FileName, oRequest.ModuleName, oRequest.RelatedId, oRequest.fileStream, oRequest.Token,oRequest.filePath, oRequest.Errors, oRequest.ClientId);
                 rmservice.UploadFile(oRequest.ClientId, oRequest.DocumentType,oRequest.Errors,  oRequest.FileName, oRequest.ModuleName, oRequest.RelatedId,  oRequest.Token, oRequest.filePath,oRequest.fileStream);
            }
            catch (FaultException<Riskmaster.UI.DataStreamingService.RMException> ee)
            {
                throw ee;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                rmservice.Close();
            }
            return returnVal;
            

        }
    }
}
