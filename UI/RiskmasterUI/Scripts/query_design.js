function openWizard()
{
//	window.open('home?pg=riskmaster/RMUtilities/SearchWizardStep1','SearchWizardStep1',
//		'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=no,scrollbars=yes');
	window.open('/RiskmasterUI/UI/Utilities/SearchWizard/SearchWizard.aspx','SearchWizardStep1',
		'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=no,scrollbars=yes');
	return false;
}

function editWizard(id)
{
//	window.open('home?pg=riskmaster/RMUtilities/SearchWizardStep1&amp;selectedid='+id,'SearchWizardStep1',
//		'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=no,scrollbars=yes');
	window.open('/RiskmasterUI/UI/Utilities/SearchWizard/SearchWizard.aspx?selectedid='+id,'SearchWizardStep1',
		'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=no,scrollbars=yes');
	return false;
}
function trim(svalue)
{
	svalue = svalue.replace(/^\s+|\s+$/g, ""); 
	return svalue;
}
function FixInputString(str)
{
	var s;
	s = String(str).replace(/'/g, " ");
	s = String(s).replace(/"/g, " ");
	return s;
}
//Shruti for MITS 8254 starts
var bAdd = "";
var sSearchName = "";
var sSearchType = "";//abansal23 on 05/19/2009 MITS 16621
function validate_step1() {   
    var s = trim(document.getElementById('Wizardlist_searchname').value);
	var strFixed;
	//Shruti for 8254
	
	//Shruti for 8254 ends
	if (s != "")   //Aman MITS 27212
    {
        if (CheckForDuplicacy()) 
        {
	        return false;
	    }
	    //Ashish Ahuja Mits 32710
	    //if(s.length > 25) 
	    if (s.length > 50) 
        {
            //Ashish Ahuja Mits 32710
            //alert("The supplied name exceeds the maximum (25 character) allowable length.");			
	        //alert("The supplied name exceeds the maximum (50 character) allowable length.");
	        alert(SearchWizardValidations.NameExceeds50Char);	        
            document.getElementById('Wizardlist_searchname').select();
            document.getElementById('Wizardlist_searchname').focus();
	     		  return false;
		}
//		Commented By Charanpreet for MITS 12370 : Start
//		strFixed = FixInputString(s);     : Commented by Charanpreet for MITS 12370        
//		//Start by Nitin for MITS 11733
//		document.getElementById('searchname').value = trim(strFixed);	
//		//End by Nitin for MITS 11733
//		if(s != strFixed)
//		{		
//		      //commented by Nitin for MITS 11733
//			  // document.getElementById('searchname').value = strFixed;	
//			  alert("Restricted characters have been removed from the name.");			
//		}	
//      Commented By Charanpreet for MITS 12370 : End
	}
	else
	{
	    //alert("Please supply a name for this form.");			    
	    alert(SearchWizardValidations.FormName);
        document.getElementById('Wizardlist_searchname').select();
        document.getElementById('Wizardlist_searchname').focus();
		return false;
	}
	

    s = new String(trim(document.getElementById('Wizardlist_searchdesc').value));

	if (s.length >250)
	{
	    //alert("The supplied description exceeds the maximum (250 character) allowable length.");		
	    alert(SearchWizardValidations.DescExceeds250Char);
        document.getElementById('Wizardlist_searchdesc').select();
        document.getElementById('Wizardlist_searchdesc').focus();
			return false;
	}			


	strFixed = FixInputString(s);
	if(s != strFixed)
	{
		
        document.getElementById('Wizardlist_searchdesc').value = strFixed;
	    //alert("Restricted characters have been removed from the description.");
        alert(SearchWizardValidations.RestrictedCharRemovedDesc);
	}	

//	s = document.getElementById('searchtype').options[document.getElementById('searchtype').selectedIndex].value;
	
//	if( s =="20") //Admin Tracking	
//	{			
//		document.getElementById('hdnaction').value = 'SearchWizardStep1a';
//	}	
//	else
//	{			
//		document.getElementById('hdnaction').value = 'SearchWizardStep2';
//	}
	//document.forms[0].submit();
	return true;
}
function GetWizardIndex()
{
    var index;
    if(document.getElementById('index0')!=null)
        index = 0;
        else if(document.getElementById('index1')!=null)
                index = 1;
                else if(document.getElementById('index2')!=null)
                        index = 2;
                        else if(document.getElementById('index3')!=null)
                            index = 3;
                            else if(document.getElementById('index4')!=null)
                                index =4;
                                else
                                     index = -1;
                            
    return index;

}

//Bijender has changed
function SelectAdminTracSearch() {
    var SelectedValue;
    var AdminTable;
    AdminTable = document.getElementById('Wizardlist$admtrcktable');
    SelectedValue = document.getElementById('hdnAdminSysName');
    if (AdminTable != null && SelectedValue != null) {
        for (var ival = 0; ival < AdminTable.length; ival++) {
            if (AdminTable[ival].selected) {
                SelectedValue.value = AdminTable[ival].innerText;
                break;
            }
        }
    }
}
//end
function validate(step)//abansal23 on 05/18/2009 for MITS 16614
{   
  var index;
  var bRetVal;
  //abansal23 on 05/18/2009 for MITS 16614 Starts
  var step_name = step;
  //Bijender has changed
  SelectAdminTracSearch();
  //end
  index = GetWizardIndex();
    if(index == '0') {
        //Bijender Mits 15065
        bRetVal = validate_step1();
        if (bRetVal == false) {
            return false;
            //Bijender End Mits 15065
        }
    }
    else if(index == '2')
    {
        if (step_name == 'Next')
        {
            bRetVal = validate_step2();
            if (bRetVal == false) 
            {
                return false;
                
                //Bijender End Mits 15065
            }
        }
    }
    else if(index == '3')
    {
        bRetVal = validate_step3(step_name);
        if (bRetVal == false)
        {
            return false;
            //Bijender End Mits 15065
        }
    }
    //abansal23 on 05/18/2009 for MITS 16614 Ends
    return true;
}
function validate_step2()
{

	var ListBox=document.getElementById('Wizardlist_selectedfields');
	
	if(ListBox.options.length==0)
	{
	    //alert("Please select some field(s) to be used in the search.");
	    alert(SearchWizardValidations.SelectSearchFields);
		return false;
	}
	//Parijat
//	document.getElementById('hdnaction').value = 'SearchWizardStep3';
	SaveList();
	//document.forms[0].submit();
	return true;
}

function SaveList()
{
	var allvalue="";
	var index = GetWizardIndex();
	var ctrl;
	var ctrl_selectedFields;
	var ctrl_hdnselectedFields;
	var ctrl_hdnList;
    if(index == '2')
    {
    ctrl_selectedFields = 'Wizardlist_selectedfields';
    ctrl_hdnselectedFields = 'Wizardlist_hdnSelectedFields';
	ctrl_hdnList = 'Wizardlist_hdnlist';
    }
    else if(index == '3')
    {
    ctrl_selectedFields = 'Wizardlist_selectedfields1';
    ctrl_hdnselectedFields = 'Wizardlist_hdnSelectedFields1';
	ctrl_hdnList = 'Wizardlist_hdnlist1';
    }
	ctrl=document.getElementById(ctrl_selectedFields);
	if (ctrl!=null)
	{
		try
		{
		//Parijat
		    ctrl1 = document.getElementById(ctrl_hdnselectedFields);
		    ctrl1.value = ctrl.value;
			for(var f=0;f<ctrl.options.length;f++)
			{
				allvalue=allvalue+ctrl.options[f].value+"|";
			}
			allvalue=allvalue.substring(0,allvalue.length-1);
			ctrl=document.getElementById(ctrl_hdnList);
			ctrl.value=allvalue;
		}
		catch(ex)
		{
			return true;
		}
	}
}

function validate_step3(step)//abansal23 on 05/18/2009 for MITS 16614
{

	var ListBox=document.getElementById('Wizardlist_selectedfields1');
	
	if(ListBox.options.length==0)
	{
	    //abansal23 on 05/18/2009 for MITS 16614 Starts
	    if (step == 'Next')
	    {
	        //alert("Please select some filed(s) to display");
	        alert(SearchWizardValidations.SelectDisplayFields);
		    return false;
		}
		//abansal23 on 05/18/2009 for MITS 16614 Ends
	}
//	document.getElementById('hdnaction').value = 'SearchWizardStep4';
	SaveList();
	//document.forms[0].submit();
	return true;
}

function CheckListboxExist(optValue)
{
	var del_value=0;
	
	for(var i=0;i<document.forms[0].elements.length;i++)
	{
		var objElem=document.forms[0].elements[i];
		var sId=new String(objElem.id);
			
		if(sId.substring(0,7)=="fields_")
		{
			if (sId.substring(7,sId.length) == optValue)
			{	
				return false;
			}
			else
			{				
				del_value=1;
			}
		}
	}
	return true;
}

function LoadFieldCategory()
{	
	var sCatDesc;
	var vCatDesc;
	var bDisplayList=true;
	
	sCatDesc = document.getElementById('searchcats').value;
	
	var checkNullCheckBox;	
	checkNullCheckBox = CheckListboxExist(sCatDesc);

	if (checkNullCheckBox )
	{
		sCatDesc="";
	}
	for(var i=0;i<document.forms[0].elements.length;i++)
	{
		var objElem=document.forms[0].elements[i];
		var sId=new String(objElem.id);

		if(sId.substring(0,7)=="fields_")
		{
			if (sId.substring(7,sId.length) == sCatDesc)
			{
				if (bDisplayList)
					bDisplayList=false;
				else
				{
					objElem.style.display ="inline";
					AdjustSelectedBoxWidth();
				}
			}
			else
			{
				objElem.style.display = "none";
			}
		}
	}
	
	if (sCatDesc=="" &&  document.getElementById('searchcats').options[document.getElementById('searchcats').selectedIndex].text != "Select a Category")
	{
		document.getElementById('fields_').length=0;	
	}

    if (document.getElementById('searchcats').options[document.getElementById('searchcats').selectedIndex].text == "Select a Category") if (document.getElementById('searchcats').options[document.getElementById('searchcats').selectedIndex].text == "Select a Category")
	{	
		document.getElementById('fields_').length=0;
		document.getElementById('fields_').options[document.getElementById('fields_').options.length] = new Option("No Category Selected", "", false, false);
		document.getElementById('fields_').style.display = "inline";
	}
	return true;
}

function AdjustSelectedBoxWidth()
{
	var CurWidth;
	CurWidth = FindVisibleListBox().offsetWidth;
	
	if(CurWidth >(Number(String(document.forms[0].Wizardlist_selectedfields.style.width).replace(/[^0-9]/g,""))))	
		document.forms[0].Wizardlist_selectedfields.style.width = CurWidth;
	return true;
}

function FindVisibleListBox()
{
	var str = new String();
	var ListBox;
	//Pick the Visible Field Listbox
	for(var i=0;i<document.forms[0].elements.length;i++)
	{
		str = document.forms[0].elements[i].id;
		if (str.substr(0,7) == "fields_")	
			if(document.forms[0].elements[i].style.display == "inline")
				ListBox = document.forms[0].elements[i]; 	
	}
	return ListBox;
}

function IsSelected(Elt)
{
	var i;
	var ctrl_selectedFields;
	var index = GetWizardIndex();
    if(index == '2')
        ctrl_selectedFields = 'Wizardlist_selectedfields';
    else if(index == '3')
        ctrl_selectedFields = 'Wizardlist_selectedfields1';
    else if(index == '4')
        ctrl_selectedFields = 'Wizardlist_selectedfields2';
	for (i=0; i<document.getElementById(ctrl_selectedFields).options.length ;i++)
	{
		if (String(Elt.value).indexOf(document.getElementById(ctrl_selectedFields).options[i].value)!= -1)
		{
			return true;
		} 
	}
	return false;
}

function AddSingle(objOptElt) {
    //abansal23 MITS 16525 On 06/22/2009 Starts
    var selectedIndex = objOptElt.selectedIndex;
    if (selectedIndex == -1)
        return false;
    var index = GetWizardIndex();
    //abansal23 MITS 16525 On 06/22/2009 Ends
    if (!IsSelected(objOptElt.options[objOptElt.selectedIndex])) //not already transferred?
	{
		if (objOptElt.value != "") 
		{
		    if(index == '2')
		        __doPostBack('tWizardlist_UpdatePanel1', 'DoubleClickedMode=From; index=' + index);
//			    document.getElementById('WizardList_selectedfields').options[document.getElementById('WizardList_selectedfields').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
			else if(index == '3')  
			    //document.getElementById('WizardList_selectedfields1').options[document.getElementById('WizardList_selectedfields1').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
			    __doPostBack('tWizardlist_UpdatePanel2', 'DoubleClickedMode=From; index=' + index);
			else if (index == '4')
			//document.getElementById('WizardList_selectedfields1').options[document.getElementById('WizardList_selectedfields1').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
			    __doPostBack('tWizardlist_UpdatePanel5', 'DoubleClickedMode=From; index=' + index); 
		}
		else
		{
		    //alert("This is not a valid field.");
		    alert(SearchWizardValidations.NotValidField);
		}
	}
	return true;
}

function AddSelected() {
   
	var i;
	var str = new String();
	var ListBox;
	var Opt;
	
//	ListBox = FindVisibleListBox();
    var index = GetWizardIndex();
    if(index == '2')
	    ListBox = document.getElementById('Wizardlist_fields');
	else if(index == '3')    
	    ListBox = document.getElementById('Wizardlist_fields1');
	if (ListBox == null) return false;
	
	//Run through the selected elts.
	for(i=0;i<ListBox.length;i++)
	{
		if(ListBox.options[i].selected == true) //selected for transfer?
			if(!IsSelected(ListBox.options[i])) //not already transferred?
			{
				Opt = ListBox.options[i];
				if (Opt.value != "") 
				{
				    if(index == '2')
					    document.getElementById('Wizardlist_selectedfields').options[document.getElementById('Wizardlist_selectedfields').options.length] = new Option(Opt.text, Opt.value, false, false);	
					else if(index == '3') 
					    document.getElementById('Wizardlist_selectedfields1').options[document.getElementById('Wizardlist_selectedfields1').options.length] = new Option(Opt.text, Opt.value, false, false);	
					       				
				}
			}
	}
	return false;
}

function AddSelectedUserGroup() {
   
	var i;
	var str = new String();
	var ListBox;
	var Opt;
	
	ListBox = document.getElementById('Wizardlist_usersgroups');
	
	//Run through the selected elts.
	for(i=0;i<ListBox.length;i++)
	{
		if(ListBox.options[i].selected == true) //selected for transfer?
			if(!IsSelected(ListBox.options[i])) //not already transferred?
			{
				Opt = ListBox.options[i];
				if (Opt.value != "") 
				{
					document.getElementById('Wizardlist_selectedfields2').options[document.getElementById('Wizardlist_selectedfields2').options.length] = new Option(Opt.text, Opt.value, false, false);					
				}
			}
	}
	ListBox.selectedIndex=-1;
	return false;
}

function RemoveSelected()
{
	var i;
	var ListBox;
	var index = GetWizardIndex();
	if(index == '2')
	    ListBox = document.getElementById('Wizardlist_selectedfields');
	else if(index =='3')    
	    ListBox = document.getElementById('Wizardlist_selectedfields1');
    else if(index =='4') 
	    ListBox = document.getElementById('Wizardlist_selectedfields2');
	    
	for(i=ListBox.options.length-1;i>=0;i--)
		if(ListBox.options[i].selected == true)
				ListBox.options[i] = null;

	ListBox.selectedIndex=-1;
	return false;
}

function RemoveSingle(objOptElt)
{
    //abansal23 MITS 16525 ON 06/22/2009
    var selectedIndex = objOptElt.selectedIndex;
    if (selectedIndex == -1)
        return false;
    var index = GetWizardIndex();
//    if(index == '2')
//	    document.getElementById('WizardList_selectedfields').options[objOptElt.index]=null;
//	else if(index == '3')
    //	    document.getElementById('WizardList_selectedfields1').options[objOptElt.index]=null;

    if (index == '2')
        __doPostBack('tWizardlist_UpdatePanel1', 'DoubleClickedMode=To; index=' + index);
    //			    document.getElementById('WizardList_selectedfields').options[document.getElementById('WizardList_selectedfields').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
    else if (index == '3')
    //document.getElementById('WizardList_selectedfields1').options[document.getElementById('WizardList_selectedfields1').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
        __doPostBack('tWizardlist_UpdatePanel2', 'DoubleClickedMode=To; index=' + index);
    else if (index == '4')
    //document.getElementById('WizardList_selectedfields1').options[document.getElementById('WizardList_selectedfields1').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
        __doPostBack('tWizardlist_UpdatePanel5', 'DoubleClickedMode=To; index=' + index); 
	return true;
}

function MoveUp()
{
	var iSelectedIndex;
	var ListBox;
	var index = GetWizardIndex();
    if(index == '2')
	    ListBox=document.getElementById('Wizardlist_selectedfields');
	else if(index == '3')    
	    ListBox=document.getElementById('Wizardlist_selectedfields1');
	var objOptElt;
	var sText;
	var sValue;
	
	if(ListBox.selectedIndex!=-1)
	{
		objOptElt = ListBox.options[ListBox.selectedIndex];
		sText=objOptElt.text;
		sValue=objOptElt.value;
		
		for(var i=0; i<ListBox.options.length; i++)
		{
			if(objOptElt.value == ListBox.options[i].value)
				iSelectedIndex=i;
		}
		if (iSelectedIndex!=0)
		{
			ListBox.options[iSelectedIndex].text=ListBox.options[iSelectedIndex-1].text;
			ListBox.options[iSelectedIndex].value=ListBox.options[iSelectedIndex-1].value;
			
			ListBox.options[iSelectedIndex-1].text=sText;
			ListBox.options[iSelectedIndex-1].value=sValue;
		}		
	}
	
	//Geeta 06-Oct-06 : Modified for fixing MITS no 7939 for Query Designer
	ListBox.selectedIndex=iSelectedIndex-1;
	return false;
}
	
function MoveDown()
{
	var iSelectedIndex;
	var ListBox;
	var index = GetWizardIndex();
    if(index == '2')
	    ListBox=document.getElementById('Wizardlist_selectedfields');
	else if(index == '3')
	    ListBox=document.getElementById('Wizardlist_selectedfields1');
	var objOptElt;
	var sText;
	var sValue;
	
	if(ListBox.selectedIndex!=-1)
	{
		objOptElt = ListBox.options[ListBox.selectedIndex];
		sText=objOptElt.text;
		sValue=objOptElt.value;
		
		for(var i=0; i<ListBox.options.length; i++)
		{
			if(objOptElt.value == ListBox.options[i].value)
				iSelectedIndex=i;
		}
		if (iSelectedIndex!=ListBox.options.length-1)
		{
			ListBox.options[iSelectedIndex].text=ListBox.options[iSelectedIndex+1].text;
			ListBox.options[iSelectedIndex].value=ListBox.options[iSelectedIndex+1].value;
			
			ListBox.options[iSelectedIndex+1].text=sText;
			ListBox.options[iSelectedIndex+1].value=sValue;
		}
	}
	
	//Geeta 06-Oct-06 : Modified for fixing MITS no 7939 for Query Designer
	ListBox.selectedIndex=iSelectedIndex+1;
	return false;
}

function goBack(sGotoPage)
{	
	if (document.getElementById('hdnAdmTrck'))
	{
		if (document.getElementById('hdnAdmTrck').value == '20')
		{
			document.getElementById('hdnaction').value = 'SearchWizardStep1a';
			SaveList();
			return true;
		}
	}
	document.getElementById('hdnaction').value = sGotoPage;
	SaveList();
	//document.forms[0].submit();
	return true;
}

function ToggleAllUsers()
{
var index = GetWizardIndex();
if (index == '4') {
    if (document.getElementById('Wizardlist_allowall').checked) {
        document.getElementById('Wizardlist_usersgroups').disabled = true;
        document.getElementById('Wizardlist_btnAdd1').disabled = true;
        document.getElementById('Wizardlist_btnRemove1').disabled = true;
        document.getElementById('Wizardlist_usersgroups').selectedIndex = -1;

        if (index == '2') {
            document.getElementById('Wizardlist_selectedfields').disabled = true;
            document.getElementById('Wizardlist_selectedfields').selectedIndex = -1;
        }
        else if (index == '3') {
            document.getElementById('Wizardlist_selectedfields1').disabled = true;
            document.getElementById('Wizardlist_selectedfields1').selectedIndex = -1;
        }
        else if (index == '4') {
            document.getElementById('Wizardlist_selectedfields2').disabled = true;
            document.getElementById('Wizardlist_selectedfields2').selectedIndex = -1;
        }
    }
    else {
        document.getElementById('Wizardlist_usersgroups').disabled = false;
        document.getElementById('Wizardlist_btnAdd1').disabled = false;
        document.getElementById('Wizardlist_btnRemove1').disabled = false;
        if (index == '2')
            document.getElementById('Wizardlist_selectedfields').disabled = false;
        else if (index == '3')
            document.getElementById('Wizardlist_selectedfields1').disabled = false;
        else if (index == '4')
            document.getElementById('Wizardlist_selectedfields2').disabled = false;
    }

} else if (index == '0') {
    AddOrEdit();
}
}

function SaveQuery()
{
	var allvalue="";
//	var index = GetWizardIndex();
//	if(index == '2')
//	    ctrl=document.getElementById('WizardList_selectedfields');
//	else if(index == '3')
//	    ctrl=document.getElementById('WizardList_selectedfields1');
//	else if(index == '4')
	    ctrl=document.getElementById('Wizardlist_selectedfields2');
	    
//     ctrl_hdnselectedFields = 'Wizardlist_hdnSelectedFields2';
//	 ctrl_hdnList = 'Wizardlist_hdnlist2';
	if (ctrl!=null)
	{
	    //shruti, 20th dec 2006, MITS 7943 starts
	    var ctrluser = document.getElementById('Wizardlist_selectusers');
	    if(ctrluser.checked)
	    {
	        
		    if(ctrl.options.length==0)
		    {
		        //alert("Please select some user.");
		        alert(SearchWizardValidations.SelectUser);
			    return false;
		    }
		}
		//shruti, 20th dec 2006, MITS 7943 ends
		for(var f=0;f<ctrl.options.length;f++)
		{
			allvalue=allvalue+ctrl.options[f].value+"|";
		}
		allvalue=allvalue.substring(0,allvalue.length-1);
		ctrl=document.getElementById('Wizardlist_hdnlist2');
		ctrl.value=allvalue;
	}
	
//	document.getElementById('hdnaction').value = 'SaveQuery';
//	document.forms[0].FormMode.value="close";
	return true;
}

function DeleteSelected()
{
    var allvalue="";
	var bDelSelect = false;
	for(var i=0;i<document.forms[0].elements.length;i++)
	{
		var objElem=document.forms[0].elements[i];
		if (objElem.type == "checkbox" && objElem.checked)
		{
			bDelSelect = true;
			if(allvalue=="")
			    allvalue = objElem.value;
			else
			    allvalue = allvalue + " " + objElem.value;
			continue;
		}
	}
	
	if (!bDelSelect)
	{
	    //alert("Please select at least one search view to delete.");
	    alert(SearchWizardValidations.SelectSearchToDelete);
		return false;
	}
	ctrl=document.getElementById('hdnDelete');
	ctrl.value = allvalue;
	if( confirm("Pressing 'OK' will completely remove the selected search views.\nAre you sure you wish to continue?")) 
    {//		document.getElementById('hdnaction').value="Delete";
        return true;
    }

	return false;	
}

function ShowProp() {
    
var ListBox;
var index = GetWizardIndex();
    if(index == '2')
	    ListBox=document.getElementById('Wizardlist_selectedfields');
	else if(index == '3')
	    ListBox=document.getElementById('Wizardlist_selectedfields1');
	var objOptElt = new String();
	var sSearchType;
	if (ListBox.selectedIndex!=-1)
	{
		objOptElt = ListBox.options[ListBox.selectedIndex].value;

		sSearchType = objOptElt.substring(objOptElt.lastIndexOf("_")+1,objOptElt.length)
		if (sSearchType == "3" || sSearchType == "14" || sSearchType == "6" || sSearchType == "15")
		{
			document.getElementById('hdnsenttoprop').value = sSearchType;
			//window.open('home?pg=riskmaster/RMUtilities/SearchFieldProps&amp;selectedtype='+sSearchType,'SearchFieldProps',
			//	'width=300,height=150,top='+(screen.availHeight-200)/2+',left='+(screen.availWidth-300)/2+',resizable=no,scrollbars=no,modal=yes');

			window.open('/RiskmasterUI/UI/Utilities/QueryDesigner/SearchFieldProps.aspx?selectedtype=' + sSearchType, 'SearchFieldProps',
			'width=300,height=150,top=' + (screen.availHeight - 200) / 2 + ',left=' + (screen.availWidth - 300) / 2 + ',resizable=no,scrollbars=no,modal=yes');
			
		}
		//Nitesh(10/09/2006) MITS 7938
		else 
		    //alert("This field does not contains property page.");
		    alert(SearchWizardValidations.FieldDoesNotContainPropertyPage);
	}
	else
	    //alert("Please select a field to view its properties.");
	    alert(SearchWizardValidations.SelectFieldToViewProp);
	
	return false;
}

function PropOK() {
    
	window.opener.ChangeValue(document.getElementById('fieldprops').value);
	window.close();
	return false;
}

function ChangeValue(newVal)
{
var ListBox;
var index = GetWizardIndex();
    if(index == '2')
	    ListBox=document.getElementById('Wizardlist_selectedfields');
	else if(index == '3')    
	    ListBox=document.getElementById('Wizardlist_selectedfields1');
	var objOptElt = new String();
	
	if (newVal != document.getElementById('hdnsenttoprop').value)
	{
		objOptElt = ListBox.options[ListBox.selectedIndex].value;
		objOptElt = objOptElt.substring(0,objOptElt.lastIndexOf("_")+1) + newVal;
		ListBox.options[ListBox.selectedIndex].value = objOptElt;
	}
	return false;
}


function CheckForDuplicacy() {
    
 //Bijender Mits 15065
    var ctrlSearchName = document.getElementById('Wizardlist_searchname');
    var ctrlSearchType = document.getElementById('Wizardlist_searchtype');
    //Bijender End Mits 14532
    if (bAdd == "true" || (bAdd == "false" && (ctrlSearchName.value != sSearchName || ctrlSearchType.value != sSearchType)))//abansal23 on 05/19/2009 MITS 16621     
    {    
        if(IsAlreadySelected(ctrlSearchName.value, ctrlSearchType.value))
		{
			ctrlSearchName.select();
			ctrlSearchName.focus();
			return true;
		}
    }
    return false;
}

function AddOrEdit() {
   
    if (document.getElementById('Wizardlist_searchname').value == "")
    {
        bAdd = "true";
    }
    else
    {
        bAdd = "false";
        sSearchName = document.getElementById('Wizardlist_searchname').value;
        sSearchType = document.getElementById('Wizardlist_searchtype').value; //abansal23 on 05/19/2009 MITS 16621
    }
}

function IsAlreadySelected(searchName, catId) {
   
    var itemArray;
    var iCatId;
    var sSearchNames="";

    if(window.opener.document.getElementById('hdnsearchname') != null && window.opener.document.getElementById('hdnsearchname').value != "")
    {
        var array = window.opener.document.getElementById('hdnsearchname').value.split('*~~(;)~~*');  // bpaskova JIRA 4350 changed separator
	    for(i = 0; i < array.length; i++)
        {
            var category = array[i];
            if(category != null && category.length > 0)
            {
                itemArray = category.split('*~~(^)~~*'); // bpaskova JIRA 4350 changed separator
                iCatId = parseInt(itemArray[0]);
                if(iCatId == catId)
                {
                    sSearchNames = sSearchNames + itemArray[1] + '*~~(!)~~*';//abansal23 on 05/20/2009 for duplicate search name issue.
                    continue; //abansal23 on 05/20/2009 for duplicate search name issue.
                }                
            }            
        }
        if(sSearchNames != null && sSearchNames != "")
        {   //the names generally can have comma in between, so using this pattern instead.
            
            array = sSearchNames.split('*~~(!)~~*');
            if (array.length > 1)
            {
	            for(var f=0;f<array.length;f++)
	            {
		            if(Trim(searchName) == Trim(array[f]))
		            {
		                //alert("The search with this name already exists.Please use another name.");
		                alert(SearchWizardValidations.SearchNameAlreadyExists);
		                return true;
		            }
	            }
	        }
	        else
	        {
	            if(Trim(searchName) == Trim(sSearchNames))
	            {
	                //alert("The search with this name already exists.Please use another name.");
	                alert(SearchWizardValidations.SearchNameAlreadyExists);
	                return true;
	            }
	        }
	    }
	}
	return false;
}

function Trim(sString) 
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}

//Shruti for MITS 8254 ends