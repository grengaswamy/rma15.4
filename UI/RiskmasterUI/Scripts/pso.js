var m_FormID="";
var m_FormParentID="";
var m_codeWindow=null;
var m_dateWindow=null;
var m_sFieldName="";
var m_IsSingle="";
var m_sType="";
var m_susername="";

//Added for ASP.NET Application
var m_ArrGroupName="";
var m_ArrGroupNameID="";
var m_ArrFieldControls="";
var m_ArrFieldControlsID="";
var m_ArrTitleControls="";
var m_ArrTitleControlsID="";
var m_ArrChangeTitle="";
var m_ArrLevel="";
var m_ArrOrderBy="";
var m_ArrMsgShow="";
var m_ArrMsgShowOn="";
var m_ArrShowControls="";
var m_ArrParentcode="";
var m_ArrParentLoB="";//added by shilpi
//Anu Tennyson Commented since this is not used and it was breaking in other browser. Since not used no need to change the implementation
//m_PageXml=new ActiveXObject("Microsoft.XMLDOM");
var m_PageXml = "";
//Anu Tennyson Ends
//End

var m_arrcontrol="";
var m_arrcodes="";
var m_eventId =0;
var m_sCheckFieldName="";
var m_LookupType=0;
var m_FormName="";
var m_Wnd=null;
var m_AllowRestore=false;
var m_sDupes="";
var EXISTREC_ALERT="You are working on a new record and this functionality is available for existing records only. Please save the data and try again.";

var CTRL_PASTE = 22; 
var CTRL_COPY = 3; 
var CTRL_CUT = 24; 
var TAB_KEY = 9; 
var DELETE_KEY = 46; 
var BACKSPACE_KEY = 8; 
var ENTER_KEY = 13; 
var RIGHT_ARROW_KEY = 39; 
var DOWN_ARROW_KEY = 40; 
var UP_ARROW_KEY = 38; 
var LEFT_ARROW_KEY = 37; 
var HOME_KEY = 36; 
var END_KEY = 35; 
var PAGEUP_KEY = 33; 
var PAGEDOWN_KEY = 34; 
var CAPS_LOCK_KEY = 20; 
var ESCAPE_KEY = 27; 

var NS4 = (document.layers);
var IE4 = (document.all);
var OPERA=(navigator.userAgent.toLowerCase().indexOf('opera') != -1);
var m_AutoLookup=false;
var m_autoSelection = false;
var m_DeviceDataDeleteChk = false;

function MotherandNeonateAllDelete(objCtrl) {
    var codesuperarr;
    var cntrlsuperarr;
    var cntrlarr;
    var obctrl = null;
    var obctrl1 = null;

        codesuperarr=m_ArrFieldControlsID.split("%#");
        cntrlsuperarr=m_ArrFieldControls.split("%#");
         for(var i=0;i<cntrlsuperarr.length;i++)    
         {
             cntrlarr = cntrlsuperarr[i].split(",");
             if (codesuperarr[i] == "*") {
                 for (j = 0; j < cntrlarr.length; j++) {

                     obctrlrow = eval("document.getElementById('" + cntrlarr[j] + "_ctlRow')");
                     if (obctrlrow != null) {
                         obctrlrow.style.display = "none";
                         obctrlrow.style.visibility = "hidden";
                     }

                     obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "')");
                     if (obctrlgroup == null) {
                         obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_codelookup')");
                     }
                     if (obctrlgroup == null) {
                         obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_txtMultiName')");
                     }
                     if (obctrlgroup == null) {
                         obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_txtDate')");
                     }
                     if (obctrlgroup == null) {
                         obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_TxtTime')");
                     }
                     if (obctrlgroup == null) {
                         obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_multicode')");
                     }
                     //neha goel--publix--05042011---finding control from page rather than page xml--end

                     if (obctrlgroup != null) {
                         EmptyControlRecursive(obctrlgroup);
                     }

                 } //enf for
             } //end inner if
             obctrlrow = null;
             obctrlgroup = null;
         } //end else

         return true;

}


function HideMultiCodeDelete(objCtrl)
{    
	var sCodeText="";
	var sTextDisplay = "";
	var sShortCode = "";
	var sCodeIdList="";	
	var sCodeId;
	var codesuperarr;
	var cntrlsuperarr;
	var cntrlarr;
	var obctrl=null;
	var obctrl1=null;
	var iCount = 0;	
	
	  iCount=objCtrl.length;    
      if(objCtrl.selectedIndex<0 && iCount>1)
	  {	    
	    //alert("Please select a record to delete."); 
	    return; 	    
      }
      if(iCount==1)
      {
            sCodeId = objCtrl.options[0].value;
            //objCtrl.options[0].selected=false;
            //objCtrl.selectedIndex=0
            //lcount = 0;
      }
      if(iCount==0)
      {
        //alert("No record present to delete."); 
	    return; 
      }  
	  
	  //Multiple deleting of the ListBox values
           for(var s=0;s<objCtrl.length;s++)
           {
               var objMatAdvOut = null;
               var objMatInftyp = null;
               var objMatBdyPart = null;
               var sMatBdyPart = "";
               var sMatInftypValue = "";
               var sMatAdvOutValue = "";
               var sValue = "";
               var objNeoMatAdvOut = null;
               var objNeoMatInftyp = null;
               var objNeoMatBdyPart = null;
               var sNeoMatBdyPart = "";
               var sNeoMatInftypValue = "";
               var sNeoMatAdvOutValue = "";              
            
            if(objCtrl.options[s].selected==true)
            {                                            
              sCodeIdList = sCodeIdList + "," + objCtrl.options[s].value;
	    //Hiding any controls which got displayed because of the field currently getting, deleted.
		//sCodeId = objCtrl.options[objCtrl.selectedIndex].value;
        sCodeId = objCtrl.options[s].value;
        sTextDisplay = objCtrl.options[s].text;
        sShortCode = sTextDisplay.split(" ")[0];

        //neha goel:PSO : do not hide in case of perinatal->mother->if mat adv out is "hemmo..." or inftype is "endo.."
        if (objCtrl != null && objCtrl.id == "psolstBodyPartMother_multicode" && sShortCode == "A1629") {
            objMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMother_multicode')");
            objMatInftyp = eval("document.getElementById('psocdMeternalInfectionMother_codelookup')");
            if (objMatInftyp != null && objMatInftyp.value != "") {
                sMatInftypValue = objMatInftyp.value;
                if(sMatInftypValue.indexOf("A1620")!=-1)
                      continue;
            }
            if (objMatAdvOut != null && objMatAdvOut.length>0) {
                sMatAdvOutValue = objMatAdvOut.innerText;
                if (sMatAdvOutValue.indexOf("A1599") != -1)
                    continue;
            }

        }

        //neha goel:PSO : do not hide in case of perinatal->mother->if mat body part is "" or inftype is "endo.."
        if (objCtrl != null && objCtrl.id == "psolstAdverseOutcomeMother_multicode" && sShortCode == "A1599") {
            objMatBdyPart = eval("document.getElementById('psolstBodyPartMother_multicode')");
            objMatInftyp = eval("document.getElementById('psocdMeternalInfectionMother_codelookup')");
            if (objMatInftyp != null && objMatInftyp.value != "") {
                sMatInftypValue = objMatInftyp.value;
                if (sMatInftypValue.indexOf("A1620") != -1)
                    continue;
            }
            if (objMatBdyPart != null && objMatBdyPart.length > 0) {
                sMatBdyPart = objMatBdyPart.innerText;
                if (sMatBdyPart.indexOf("A1629") != -1)
                    continue;
            }

        }

        //neha goel:PSO : do not hide in case of perinatal->mother->if mat adv out is "hemmo..." or inftype is "endo.."
        if (objCtrl != null && objCtrl.id == "psolstBodyPartMotherNeonate_multicode" && sShortCode == "A1629") {
            objMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMotherNeonate_multicode')");
            objMatInftyp = eval("document.getElementById('psocdMeternalInfectionMotherNeonate_codelookup')");
            if (objMatInftyp != null && objMatInftyp.value != "") {
                sMatInftypValue = objMatInftyp.value;
                if (sMatInftypValue.indexOf("A1620") != -1)
                    continue;
            }
            if (objMatAdvOut != null && objMatAdvOut.length > 0) {
                sMatAdvOutValue = objMatAdvOut.innerText;
                if (sMatAdvOutValue.indexOf("A1599") != -1)
                    continue;
            }

        }

        //neha goel:PSO : do not hide in case of perinatal->mother->if mat body part is "" or inftype is "endo.."
        if (objCtrl != null && objCtrl.id == "psolstAdverseOutcomeMotherNeonate_multicode" && sShortCode == "A1599") {
            objMatBdyPart = eval("document.getElementById('psolstBodyPartMotherNeonate_multicode')");
            objMatInftyp = eval("document.getElementById('psocdMeternalInfectionMotherNeonate_codelookup')");
            if (objMatInftyp != null && objMatInftyp.value != "") {
                sMatInftypValue = objMatInftyp.value;
                if (sMatInftypValue.indexOf("A1620") != -1)
                    continue;
            }
            if (objMatBdyPart != null && objMatBdyPart.length > 0) {
                sMatBdyPart = objMatBdyPart.innerText;
                if (sMatBdyPart.indexOf("A1629") != -1)
                    continue;
            }

        }

        //neha goel:PSO : do not hide in case of perinatal->mother->if mat body part is "" or inftype is "endo.."
        if (objCtrl != null && objCtrl.id == "psolstNeonateSustain_multicode" && (sShortCode == "A1665" || sShortCode == "A1668" || sShortCode == "A1671" || sShortCode == "A1644")) {
            objNeoMatBdyPart = eval("document.getElementById('psolstBodyPartMotherNeonate_multicode')");
            objNeoMatInftyp = eval("document.getElementById('psocdMeternalInfectionMotherNeonate_codelookup')");
            objNeoMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMotherNeonate_multicode')");
            if (objNeoMatInftyp != null && objNeoMatInftyp.value != "") {
                sMatInftypValue = objNeoMatInftyp.value;
                if (sMatInftypValue.indexOf("A1620") != -1)
                    continue;
            }
            if (objNeoMatBdyPart != null && objNeoMatBdyPart.length > 0) {
                sMatBdyPart = objNeoMatBdyPart.innerText;
                if (sMatBdyPart.indexOf("A1629") != -1)
                    continue;
            }
            if (objNeoMatAdvOut != null && objNeoMatAdvOut.length > 0) {
                sNeoMatAdvOutValue = objNeoMatAdvOut.innerText;
                if (sNeoMatAdvOutValue.indexOf("A1599") != -1)
                    continue;
            }
            if (objCtrl.length > 0) {
                sValue = objCtrl.innerText;
                if (sShortCode == "A1665") {
                    if (sValue.indexOf("A1668") != -1 || sValue.indexOf("A1671") != -1 || sValue.indexOf("A1644") != -1 || sValue.indexOf("A1659") != -1)
                        continue;
                }
                else if (sShortCode == "A1668") {
                    if (sValue.indexOf("A1665") != -1 || sValue.indexOf("A1671") != -1 || sValue.indexOf("A1644") != -1 || sValue.indexOf("A1659") != -1)
                        continue;
                }
                else if (sShortCode == "A1671") {
                    if (sValue.indexOf("A1665") != -1 || sValue.indexOf("A1668") != -1 || sValue.indexOf("A1644") != -1 || sValue.indexOf("A1659") != -1)
                        continue;
                }
                else if (sShortCode == "A1644") {
                    if (sValue.indexOf("A1665") != -1 || sValue.indexOf("A1668") != -1 || sValue.indexOf("A1671") != -1 || sValue.indexOf("A1659") != -1)
                        continue;
                }
            }

        }


	    codesuperarr=m_ArrFieldControlsID.split("%#");
        cntrlsuperarr=m_ArrFieldControls.split("%#");
         for(var i=0;i<cntrlsuperarr.length;i++)    
         {
             cntrlarr = cntrlsuperarr[i].split(",");

             if (objCtrl != null && objCtrl.id == "psolstNeonateSustain_multicode" && codesuperarr[i].indexOf(",") != -1) {
                 var codearr = codesuperarr[i].split(",");
                 for (var k = 0; k < codearr.length; k++) {
                     if (codearr[k] == sShortCode) {
                         for (j = 0; j < cntrlarr.length; j++) {
                             //neha goel:PSO : do not hide in case of perinatal->mother->if mat body part is "" or inftype is "endo.."
                             if (objCtrl != null && objCtrl.id == "psolstNeonateSustain_multicode" && sShortCode == "A1659" && cntrlarr[j] == "psocdVaginalDeliveryMotherNeonate") {
                                 if (objCtrl.length > 0) {
                                     sValue = objCtrl.innerText;
                                     if (sValue.indexOf("A1665") != -1 || sValue.indexOf("A1668") != -1 || sValue.indexOf("A1671") != -1 || sValue.indexOf("A1644") != -1)
                                         continue;
                                 }
                                 objNeoMatBdyPart = eval("document.getElementById('psolstBodyPartMotherNeonate_multicode')");
                                 objNeoMatInftyp = eval("document.getElementById('psocdMeternalInfectionMotherNeonate_codelookup')");
                                 objNeoMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMotherNeonate_multicode')");
                                 if (objNeoMatInftyp != null && objNeoMatInftyp.value != "") {
                                     sMatInftypValue = objNeoMatInftyp.value;
                                     if (sMatInftypValue.indexOf("A1620") != -1)
                                         continue;
                                 }
                                 if (objNeoMatBdyPart != null && objNeoMatBdyPart.length > 0) {
                                     sMatBdyPart = objNeoMatBdyPart.innerText;
                                     if (sMatBdyPart.indexOf("A1629") != -1)
                                         continue;
                                 }
                                 if (objNeoMatAdvOut != null && objNeoMatAdvOut.length > 0) {
                                     sNeoMatAdvOutValue = objNeoMatAdvOut.innerText;
                                     if (sNeoMatAdvOutValue.indexOf("A1599") != -1)
                                         continue;
                                 }
                                 if (objCtrl.length > 0) {
                                     sValue = objCtrl.innerText;
                                     if (sShortCode == "A1665") {
                                         if (sValue.indexOf("A1668") != -1 || sValue.indexOf("A1671") != -1 || sValue.indexOf("A1644") != -1 || sValue.indexOf("A1659") != -1)
                                             continue;
                                     }
                                     else if (sShortCode == "A1668") {
                                         if (sValue.indexOf("A1665") != -1 || sValue.indexOf("A1671") != -1 || sValue.indexOf("A1644") != -1 || sValue.indexOf("A1659") != -1)
                                             continue;
                                     }
                                     else if (sShortCode == "A1671") {
                                         if (sValue.indexOf("A1665") != -1 || sValue.indexOf("A1668") != -1 || sValue.indexOf("A1644") != -1 || sValue.indexOf("A1659") != -1)
                                             continue;
                                     }
                                     else if (sShortCode == "A1644") {
                                         if (sValue.indexOf("A1665") != -1 || sValue.indexOf("A1668") != -1 || sValue.indexOf("A1671") != -1 || sValue.indexOf("A1659") != -1)
                                             continue;
                                     }
                                 }

                             }


                             obctrlrow = eval("document.getElementById('" + cntrlarr[j] + "_ctlRow')");
                             if (obctrlrow != null) {
                                 obctrlrow.style.display = "none";
                                 obctrlrow.style.visibility = "hidden";
                             }

                             obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "')");
                             if (obctrlgroup == null) {
                                 obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_codelookup')");
                             }
                             if (obctrlgroup == null) {
                                 obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_txtMultiName')");
                             }
                             if (obctrlgroup == null) {
                                 obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_txtDate')");
                             }
                             if (obctrlgroup == null) {
                                 obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_TxtTime')");
                             }
                             if (obctrlgroup == null) {
                                 obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_multicode')");
                             }
                             //neha goel--publix--05042011---finding control from page rather than page xml--end

                             if (obctrlgroup != null) {
                                 EmptyControlRecursive(obctrlgroup);
                             }

                         } //end for
                     } //end inner if
                 }//end outer for
             }//enf if
             else {
                 //if(codesuperarr[i] == sCodeId)
                 if (codesuperarr[i] == sShortCode) {
                     for (j = 0; j < cntrlarr.length; j++) {

                         var sValue = "";
                         //neha goel:PSO : do not hide in case of perinatal->mother->if mat body part is "" or inftype is "endo.."
                         if (objCtrl != null && objCtrl.id == "psolstNeonateSustain_multicode" && sShortCode == "A1659" && cntrlarr[j] == "psocdVaginalDeliveryMotherNeonate") {
                             if (objCtrl.length > 0) {
                                 sValue = objCtrl.innerText;
                                 if (sValue.indexOf("A1665") != -1 || sValue.indexOf("A1668") != -1 || sValue.indexOf("A1671") != -1 || sValue.indexOf("A1644") != -1)
                                     continue;
                             }
                         }

                       
                         obctrlrow = eval("document.getElementById('" + cntrlarr[j] + "_ctlRow')");
                         if (obctrlrow != null) {
                             obctrlrow.style.display = "none";
                             obctrlrow.style.visibility = "hidden";
                         }

                         obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "')");
                         if (obctrlgroup == null) {
                             obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_codelookup')");
                         }
                         if (obctrlgroup == null) {
                             obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_txtMultiName')");
                         }
                         if (obctrlgroup == null) {
                             obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_txtDate')");
                         }
                         if (obctrlgroup == null) {
                             obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_TxtTime')");
                         }
                         if (obctrlgroup == null) {
                             obctrlgroup = eval("document.getElementById('" + cntrlarr[j] + "_multicode')");
                         }
                         //neha goel--publix--05042011---finding control from page rather than page xml--end

                         if (obctrlgroup != null) {
                             EmptyControlRecursive(obctrlgroup);
                         }

                     }//enf for
                 }//end inner if
             }//end else




	     }   
	    obctrlrow=null;
	    obctrlgroup=null;   
	    
	   }
     }
 
         
	return true;
}

function numLostFocus(objCtrl)
{
	if(objCtrl.value.length==0)
			return false;
			
objCtrl.value = objCtrl.value.replace(/,/gi, "");
var bCheckneg = "";
bCheckneg =  objCtrl.value;
 if(bCheckneg.indexOf("-")!=-1){
     objCtrl.value = "";
    alert("Please enter numeric value only.");
    objCtrl.focus();
    return false;

 }			
 //var IsFound = /^-?\d+\.{0,1}\d*$/.test(objCtrl.value); 
 var IsFound = /^-?\d+\d*$/.test(objCtrl.value);             
if(!IsFound)			
//if (isNaN(Math.round(objCtrl.value))) 
{
    objCtrl.value = "";
//START Nitin MITS#23511,01/18/2011
    alert("Please enter numeric value only.");
    objCtrl.focus();
    return false;
//END Nitin MITS#23511,01/18/2011
}
else
		//objCtrl.value=parseFloat(objCtrl.value);
		//objCtrl.value=parseInt(objCtrl.value);
		//objCtrl.value=Math.round(objCtrl.value);			
	return true;
}

function HideDisplayGroup(iID,sType,sText,lId,arrMultiCode,smulticode)
{
   	var obctrl;
	var obctrl1;
	var objGroup;
	var obctrlarr = new Array();	
	var obctrlelem;
	var codearr;
	var cntrlarr;
	var objName;
	var ArrCode;
	var objValue;
    var inpcode;
    var sFieldName;
    var codesuperarr;
    var cntrlsuperarr;
    var sCodeDesc = '';
    var sCodeId = 0;
    var sYear;
    var sMonth;
    var sDays;
    var sDate;
    var sGroupName;
    var childNodes;
    var i;
    var sControlName;
    var sControlHidden;
    var sControlVisibility;
    var sControlDis;
    var j;
    var k;
    var m;
    var obctrl2;
    var objCtrl;  
    var sCtrlName;
    var ifexists; // Shifted this to accomodate logic for n*n
    var sControlType; //Saurabh Walia: To handle memo fields.
    var objGroupHeader;
    var objPatientCount = null;
    //Setting the inpcode for single and multi code
    if(arrMultiCode =="" || arrMultiCode ==null)
    {
        inpcode= iID;
        arrMultiCode = ";" + iID + ";;"; // converting the single code value to the multiple code values
    } 
       

    if(sType=='G')
    {
        codesuperarr=m_ArrGroupNameID.split("%#");
        cntrlsuperarr=m_ArrGroupName.split("%#");
    }
    else
    {
        codesuperarr=m_ArrFieldControlsID.split("%#");
        cntrlsuperarr=m_ArrFieldControls.split("%#");
    }
    
   // To handle multicode and single code simultaneously.               

    for(var s=0;s<arrMultiCode.split("||").length;s++)
    {
        inpcode = arrMultiCode.split("||")[s].split(";")[1];
        
        //find if selected code has any display controlid 11/23/2009 Saurabh Walia: To handle * and ELSE
           for(var code=0;code<codesuperarr.length;code++)
           {
            for(var incode=0;incode<codesuperarr[code].split(",").length;incode++)
            {
              if(codesuperarr[code].split(",")[incode].toLowerCase()==inpcode.toLowerCase())
                ifexists = "yes";
            } 
         }
        //ravneet kaur: handling n*n hidedisplay
         sText = iID + " " + sText;
            var bshow=true;
    for(var i=0;i<cntrlsuperarr.length;i++)
    {
       bshow=false;
         cntrlarr=cntrlsuperarr[i].split(",");
         codearr = codesuperarr[i].split(",");

//         if (codearr == "A3" && iID == "A3") {
//	         objPatientCount = eval("document.getElementById('PatientCount')");
//	         if (objPatientCount != null && objPatientCount.value != "") {
//	             if (objPatientCount.value == "0") {
//	                 //alert("testing Patient Count");
//	                 //bPatientCountcheck = false;
//	                 continue;
//	             }
//	         }
//	     }

            
        //ravneet kaur: handling n*n hidedisplay                
              if(cntrlarr.length>1 && codearr.length>1 || cntrlarr.length>1 && codearr.length==1 || cntrlarr.length==1 && codearr.length>1)
              {
                    //If multiple controls and multiple code ids -COND 1
                    if(cntrlarr.length>1 && codearr.length>1)
                    {
                    for(var l=0;l<codearr.length;l++)
                    {
                  if((codearr[l].toLowerCase()==inpcode.toLowerCase()) || ((codearr[l].toLowerCase()=="*")&&(inpcode!="")) || ((codearr[l].toLowerCase()=="else")&& (ifexists != "yes") &&(inpcode!="")))   //If Selected value is in GroupNameIDs //If Selected value is in GroupNameIDs 11/23/2009 Saurabh Walia: To handle * and ELSE
                  {                    
                    
                            	        	            
	                for(var k=0;k<cntrlarr.length;k++) {
	                    if (codearr[l] == "A3" && iID == "A3") {
	                        objPatientCount = eval("document.getElementById('PatientCount')");
	                        if (objPatientCount != null && objPatientCount.value != "") {
	                            if (objPatientCount.value == "0") {
	                                //alert("testing Patient Count");
	                                //bPatientCountcheck = false;
	                                continue;
	                            }
	                        }
	                    }
                        //neha added to hide control when anonymous reporter is Y
	                    if (codearr.length == 3 && codearr[0] == "A3" && codearr[1] == "A6" && codearr[2] == "A9" && (iID == "A3" || iID == "A6" || iID == "A9")) {
	                        var objReporterTypeY = null;
	                        var sReporterTypeY = "";
	                        objReporterTypeY = eval("document.getElementById('psocdIdentifyReporter_codelookup')");
	                        if (objReporterTypeY != null && (objReporterTypeY.value != "" && objReporterTypeY.value != " ")) {
	                            sReporterTypeY = objReporterTypeY.value.split(" ")[0];
                            }
                            if (sReporterTypeY == "Y" && (cntrlarr[k]=="txtReporterName" || cntrlarr[k]=="txtReporterPhoneNumber" || cntrlarr[k]=="txtReporterEmail" || cntrlarr[k]=="txtReporterJobPosition")) {
                                continue;
                            }
                        }
                        //neha added to hide control when labor induced is  not Y
                        if (codearr.length == 2 && codearr[0] == "A1518" && codearr[1] == "A1524" && (iID == "A1518" || iID == "A1524")) {
                            var objLbInduced= null;
                            var sLbInduced = "";
                            objLbInduced = eval("document.getElementById('psocdLaborinduced_codelookup')");
                            if (objLbInduced != null && (objLbInduced.value != "" && objLbInduced.value != " ")) {
                                sLbInduced = objLbInduced.value.split(" ")[0];
                            }
                            if (sLbInduced != "3" && (cntrlarr[k] == "psocdLaborYes")) {
                                continue;
                            }
                        }       

	                if(sType =='G')
	                    {
	                        obctrl = eval("document.getElementById('" + cntrlarr[k] + "')");
	                        //neha for pso
	                        objGroupHeader = eval("document.getElementById('" + cntrlarr[k] + "_header')");
	                        bshow=true;
	                    }
	                else
                        {	  //neha goel--publix--05032011--removed  ctl00_Main_  ---start             
                            obctrl=eval("document.getElementById('" + cntrlarr[k]  +"_ctlRow')");                           
                            bshow=true;                            
                        }

                        //changed by rkaur7       
                        if(obctrl!=null)
                        {
	                    obctrl.style.display="";  //RMA-12071
                        obctrl.style.visibility="visible";  
                        }
                        //neha goel for pso group header
                        if (objGroupHeader != null) {
                            objGroupHeader.style.display = "";//RMA-12071
                            objGroupHeader.style.visibility = "visible";
                        }
                      	                      	                
	                if(sType =='G' && obctrl!=null)
	                 {
	                     //check for child controls and empty them and disable the validators assigned to them
                         objGroup = eval("document.getElementById('" + cntrlarr[j] + "_table')");
                         if (objGroup != null) {
                             childNodes = objGroup.childNodes[0].childNodes;

                             //for removing * on postback
                             var sHiddenControl = document.getElementById('hiddencontrols').value;
                             var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                             //end

                             for (k = 0; k < childNodes.length; k++) {
                                 if (childNodes[k].childNodes.length > 1) {
                                     sControlName = childNodes[k].childNodes[1].children[0].getAttribute("name");
                                     if (sControlName != null && sControlName != "" && sControlName.indexOf("$") != -1) {
                                         sControlName = sControlName.split("$")[0];
                                     }
                                     sControlHidden = childNodes[k].style.display;
                                     sControlVisibility = childNodes[k].style.visibility;                                     
                                     if (sControlHidden != "none" && sControlVisibility != "hidden") {
                                         obctrl = eval("document.getElementById('" + sControlName + "_Req')");
                                         if (obctrl != null) {
                                             obctrl.style.visibility = "hidden";
                                             //for removing * on postback--commented below two lines                                            
                                             //obctrl.enabled = true;                                             
                                            //ValidatorUpdateDisplay(obctrl);
                                             if (sHiddenControl.indexOf(sControlName + "_ctlRow") != -1) {
                                                 document.getElementById('hiddencontrols').value = sHiddenControl.replace(sControlName + "_ctlRow", " ");
                                             }
                                             if (sPnlHiddenControl.indexOf(sControlName) != -1) {
                                                 document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace(sControlName, " ");
                                             }

                                         }
                                     }
                                 }
                             } //end neha goel :pso logic
                         }
                     }
                     else
                     {
                        obctrl=eval("document.getElementById('"+cntrlarr[k]+"_Req')");                         
                         if(obctrl !=null)
                         {
                             obctrl.style.visibility = "hidden";
                             //for removing * on postback--commented below two lines                                            
                             //obctrl.enabled = true;                                             
                            //ValidatorUpdateDisplay(obctrl);
                             var sHiddenControl = document.getElementById('hiddencontrols').value;
                             var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                             if (sHiddenControl.indexOf(cntrlarr[k] + "_ctlRow") != -1) {
                                 document.getElementById('hiddencontrols').value = sHiddenControl.replace(cntrlarr[k] + "_ctlRow", " ");
                             }
                             if (sPnlHiddenControl.indexOf(cntrlarr[k]) != -1) {
                                 document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace(cntrlarr[k], " ");
                             }                     
                         }
                     }
	                obctrl=null;	       
	                         
                  }  
                   
                  } 
                             
                 }
                 if(bshow==false)
                 {
                 for(j=0;j<cntrlarr.length;j++)
	            {
	                if(sType =='G')
	                {
	                    obctrl = eval("document.getElementById('" + cntrlarr[j] + "')");
	                    objGroupHeader = eval("document.getElementById('" + cntrlarr[j] + "_header')");                
	                }
	                else
	                {	//neha goel--publix--05032011--removed  ctl00_Main_                   	                 
	                 obctrl=eval("document.getElementById('"+cntrlarr[j]  +"_ctlRow')");
	                                
                     }
	                 if(obctrl !=null)
	                 {
	                    obctrl.style.display="none";    
	                    obctrl.style.visibility="hidden";
	                }
	                //neha goel for pso group header
	                if (objGroupHeader != null) {
	                    objGroupHeader.style.display = "none";
	                    objGroupHeader.style.visibility = "hidden";
	                }
	                 if(sType =='G' && obctrl !=null)
	                 {
	                   //check for child controls and empty them and disable the validators assigned to them
	                     //neha goel: removed shell code and used table control to get asp controls instaed of page xml
	                     objGroup = eval("document.getElementById('" + cntrlarr[j] + "_table')");
	                     if (objGroup != null) {
	                         childNodes = objGroup.childNodes[0].childNodes;

	                         for (k = 0; k < childNodes.length; k++) {
	                             //EmptyControlRecursive(childNodes[k]);
	                             if (childNodes[k].childNodes.length > 1) {
	                                 EmptyControlRecursive(childNodes[k].childNodes[1].children[0]);
	                             }
	                         }
	                     }
	                    
	                 }
	                 else
	                 {
	                    //check for control and empty it and disable the validators assigned to them	                    
	                    //neha goel--publix--05042011---finding control from page rather than page xml--start	                                           
	                    objCtrl = eval("document.getElementById('" + cntrlarr[j] + "')");
	                    if (objCtrl == null) 
                        {
                            objCtrl = eval("document.getElementById('" + cntrlarr[j] + "_codelookup')");
	                    }                          

                        if (objCtrl == null) {
                          objCtrl = eval("document.getElementById('" + cntrlarr[j] + "_multicode')");
                        }                                               
                        //neha goel--publix--05042011---finding control from page rather than page xml--end
	                   
	                   if (objCtrl != null)
                         {  
                              EmptyControlRecursive(objCtrl);                           
                         } 
	                 }         
	            }  
                 }
                 
                 }
                 // COND1 END
                 
                 //COND 2 - multiple codeids and single control
                 else if(cntrlarr.length==1 && codearr.length>1)
                    {
                    for(var l=0;l<codearr.length;l++)
                    {
                  if((codearr[l].toLowerCase()==inpcode.toLowerCase()) || ((codearr[l].toLowerCase()=="*")&&(inpcode!="")) || ((codearr[l].toLowerCase()=="else")&& (ifexists != "yes") &&(inpcode!="")))   //If Selected value is in GroupNameIDs //If Selected value is in GroupNameIDs 11/23/2009 Saurabh Walia: To handle * and ELSE
	                {
	                    for (j = 0; j < cntrlarr.length; j++) {

	                        var objMedType = null;
	                        var objSubstanceType = null;
                            var sMedType = "";
                            var sSubstanceType = "";
                            var objMedEventDesc = null;
                            var sMedEventDesc = "";
                            if (iID == "A1200" || iID == "A1203" || iID == "A1209" || iID == "A1212" || iID == "A1215" || iID == "A1218") {
                                if (cntrlarr[j] == "USCMEDGrid") {
                                    objMedEventDesc = eval("document.getElementById('psocdBestEventUSC_codelookup')");
                                    if (objMedEventDesc != null && objMedEventDesc.value != "") {
                                        sMedEventDesc = objMedEventDesc.value;
                                        if (sMedEventDesc.indexOf("A1263") != -1) {
                                            continue;
                                        }
                                        else if (sMedEventDesc.indexOf("UNK") != -1) {
                                            continue;
                                        }
                                    }
                                }
                                else if (cntrlarr[j] == "NMMEDGrid") {
                                    objMedEventDesc = eval("document.getElementById('psocdBestEventNM_codelookup')");
                                    if (objMedEventDesc != null && objMedEventDesc.value != "") {
                                        sMedEventDesc = objMedEventDesc.value;
                                        if (sMedEventDesc.indexOf("A1263") != -1) {
                                            continue;
                                        }
                                        else if (sMedEventDesc.indexOf("UNK") != -1) {
                                            continue;
                                        }
                                    }
                                }
                                else if (cntrlarr[j] == "INCMEDGrid") {
                                    objMedEventDesc = eval("document.getElementById('psocdBestEventINC_codelookup')");
                                    if (objMedEventDesc != null && objMedEventDesc.value != "") {
                                        sMedEventDesc = objMedEventDesc.value;
                                        if (sMedEventDesc.indexOf("A1263") != -1) {
                                            continue;
                                        }
                                        else if (sMedEventDesc.indexOf("UNK") != -1) {
                                            continue;
                                        }
                                    }
                                }
                            }     

	                        if (iID == "A1260" || iID == "A1257"){
	                            if (cntrlarr[j] == "USCMEDGrid") {
	                                objSubstanceType = eval("document.getElementById('psocdSubstanceTypeUSC_codelookup')");
	                                if (objSubstanceType != null && objSubstanceType.value != "") {
	                                    sSubstanceType = objSubstanceType.value;
	                                    if (sSubstanceType.indexOf("A1206") != -1) {
	                                        continue;
	                                    }
	                                    else if (sSubstanceType.indexOf("A1197") != -1) {
	                                        objMedType = eval("document.getElementById('psocdMedicationTypeUSC_codelookup')");
	                                        if (objMedType != null && objMedType.value != "") {
	                                            sMedType = objMedType.value;
	                                            if (sMedType.indexOf("A1221") != -1) {
	                                                continue;
	                                            }
	                                        }
	                                    }
	                                }
	                            }
	                            else if (cntrlarr[j] == "NMMEDGrid") {
	                                objSubstanceType = eval("document.getElementById('psocdSubstanceTypeNM_codelookup')");
	                                if (objSubstanceType != null && objSubstanceType.value != "") {
	                                    sSubstanceType = objSubstanceType.value;
	                                    if (sSubstanceType.indexOf("A1206") != -1) {
	                                        continue;
	                                    }
	                                    else if (sSubstanceType.indexOf("A1197") != -1) {
	                                        objMedType = eval("document.getElementById('psocdMedicationTypeNM_codelookup')");
	                                        if (objMedType != null && objMedType.value != "") {
	                                            sMedType = objMedType.value;
	                                            if (sMedType.indexOf("A1221") != -1) {
	                                                continue;
	                                            }
	                                        }
	                                    }
	                                }
	                            }
	                            else if (cntrlarr[j] == "INCMEDGrid") {
	                                objSubstanceType = eval("document.getElementById('psocdSubstanceTypeINC_codelookup')");
	                                if (objSubstanceType != null && objSubstanceType.value != "") {
	                                    sSubstanceType = objSubstanceType.value;
	                                    if (sSubstanceType.indexOf("A1206") != -1) {
	                                        continue;
	                                    }
	                                    else if (sSubstanceType.indexOf("A1197") != -1) {
	                                        objMedType = eval("document.getElementById('psocdMedicationTypeINC_codelookup')");
	                                        if (objMedType != null && objMedType.value != "") {
	                                            sMedType = objMedType.value;
	                                            if (sMedType.indexOf("A1221") != -1) {
	                                                continue;
	                                            }
	                                        }
	                                    }
	                                }
	                            }
                            }


	                if(sType =='G')
	                    {
	                        obctrl = eval("document.getElementById('" + cntrlarr[0] + "')");
	                        objGroupHeader = eval("document.getElementById('" + cntrlarr[k] + "_header')");
	                        bshow=true;	                        
	                    }
	                else
                        {	  //neha goel--publix--05032011--added  ctl00_Main_            
                            obctrl=eval("document.getElementById('"+cntrlarr[j]  +"_ctlRow')");                                
                            bshow=true;                            
                        }
                         
                        if(obctrl!=null)
                        {
	                    obctrl.style.display="";//  RMA-12071
                        obctrl.style.visibility="visible";  
                        }
                        //neha goel for pso group header
                        if (objGroupHeader != null) {
                            objGroupHeader.style.display = ""; //RMA-12071
                            objGroupHeader.style.visibility = "visible";
                        }
                      	                      	                
	                if(sType =='G' && obctrl!=null)
	                 {
	                     //check for child controls and empty them and disable the validators assigned to them
                         objGroup = eval("document.getElementById('" + cntrlarr[j] + "_table')");
                         if (objGroup != null) {
                             childNodes = objGroup.childNodes[0].childNodes;

                             for (k = 0; k < childNodes.length; k++) {
                                 if (childNodes[k].childNodes.length > 1) {
                                     sControlName = childNodes[k].childNodes[1].children[0].getAttribute("name");
                                     if (sControlName != null && sControlName != "" && sControlName.indexOf("$") != -1) {
                                         sControlName = sControlName.split("$")[0];
                                     }
                                     sControlHidden = childNodes[k].style.display;
                                     sControlVisibility = childNodes[k].style.visibility;
                                     //sControlDis = childNodes[m].getAttribute("disabled");
                                     //if(sControlHidden!="yes" && sControlDis!="yes")
                                     if (sControlHidden != "none" && sControlVisibility != "hidden") {
                                         obctrl = eval("document.getElementById('" + sControlName + "_Req')");
                                         if (obctrl != null) {
                                             obctrl.style.visibility = "hidden";
                                             //for removing * on postback--commented below two lines                                            
                                             //obctrl.enabled = true;                                             
                                            //ValidatorUpdateDisplay(obctrl);
                                             var sHiddenControl = document.getElementById('hiddencontrols').value;
                                             var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                                             if (sHiddenControl.indexOf(sControlName + "_ctlRow") != -1) {
                                                 document.getElementById('hiddencontrols').value = sHiddenControl.replace(sControlName + "_ctlRow", " ");
                                             }
                                             if (sPnlHiddenControl.indexOf(sControlName) != -1) {
                                                 document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace(sControlName, " ");
                                             }
                                         }
                                     }
                                 }
                             } //end neha goel :pso logic
                         }
                     }
                     else
                     {
                        obctrl=eval("document.getElementById('"+cntrlarr[0]+"_Req')");                        
                         if(obctrl !=null)
                         {
                             obctrl.style.visibility = "hidden";
                             //for removing * on postback--commented below two lines                                            
                             //obctrl.enabled = true;                                             
                            //ValidatorUpdateDisplay(obctrl);
                             var sHiddenControl = document.getElementById('hiddencontrols').value;
                             var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                             if (sHiddenControl.indexOf(cntrlarr[0] + "_ctlRow") != -1) {
                                 document.getElementById('hiddencontrols').value = sHiddenControl.replace(cntrlarr[0] + "_ctlRow", " ");
                             }
                             if (sPnlHiddenControl.indexOf(cntrlarr[0]) != -1) {
                                 document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace(cntrlarr[0], " ");
                             }                             
                         }
                     }
	                obctrl=null;
	                }                  
                  }                
                 
                 }
                 
                  if(bshow==false)
                 {
                 for(j=0;j<cntrlarr.length;j++) {

                     var objNeoAdvOut = null;
                     sNeoAdvOutValue = "";
                     var objNeoMatAdvOut = null;
                     var objNeoMatInftyp = null;
                     var objNeoMatBdyPart = null;
                     var sNeoMatBdyPart = "";
                     var sNeoMatInftypValue = "";
                     var sNeoMatAdvOutValue = "";
	                //neha goel:PSO : do not hide in case of perinatal->mother->if mat adv out is "hemmo..." or inftype is "three or four"
	                if ((iID == "A1662" || iID == "A66") && cntrlarr[j] == "psocdVaginalDeliveryMotherNeonate") {
	                    objNeoAdvOut = eval("document.getElementById('psolstNeonateSustain_multicode')");
	                    objNeoMatBdyPart = eval("document.getElementById('psolstBodyPartMotherNeonate_multicode')");
	                    objNeoMatInftyp = eval("document.getElementById('psocdMeternalInfectionMotherNeonate_codelookup')");
	                    objNeoMatAdvOut = eval("document.getElementById('psolstBodyPartMotherNeonate_multicode')");
	                    if (objNeoMatInftyp != null && objNeoMatInftyp.value != "") {
	                        sMatInftypValue = objNeoMatInftyp.value;
	                        if (sMatInftypValue.indexOf("A1620") != -1)
	                            continue;
	                    }
	                    if (objNeoMatBdyPart != null && objNeoMatBdyPart.length > 0) {
	                        sMatBdyPart = objNeoMatBdyPart.innerText;
	                        if (sMatBdyPart.indexOf("A1629") != -1)
	                            continue;
	                    }
	                    if (objNeoMatAdvOut != null && objNeoMatAdvOut.length > 0) {
	                        sNeoMatAdvOutValue = objNeoMatAdvOut.innerText;
	                        if (sNeoMatAdvOutValue.indexOf("A1599") != -1)
	                            continue;
	                    }
	                    if (objNeoAdvOut != null && objNeoAdvOut.length > 0) {
	                        sNeoAdvOutValue = objNeoAdvOut.innerText;
	                        if (sNeoAdvOutValue.indexOf("A1659") != -1 || sNeoAdvOutValue.indexOf("A1665") != -1 || sNeoAdvOutValue.indexOf("A1668") != -1 || sNeoAdvOutValue.indexOf("A1671") != -1 || sNeoAdvOutValue.indexOf("A1644") != -1)
	                            continue;
	                    }
	                }         
                   
                    if(sType =='G')
	                {
	                    obctrl = eval("document.getElementById('" + cntrlarr[j] + "')");
	                    objGroupHeader = eval("document.getElementById('" + cntrlarr[j] + "_header')");                  
	                }
	                else
	                {//neha goel--publix---05032011---ctl00_Main_               	                 
	                 obctrl=eval("document.getElementById('"+cntrlarr[j]  +"_ctlRow')");
	                 	                 
                    }
	                 if(obctrl !=null)
	                 {
	                    obctrl.style.display="none";    
	                    obctrl.style.visibility="hidden";
	                    if (cntrlarr[j] == "pnlDevice") {
	                        m_DeviceDataDeleteChk = true;
	                        //document.getElementById("txtDeviceInfoSessionId").value = "";
	                        //document.getElementById("frmData").submit();
	                    }
	                }
	                //neha goel for pso group header
	                if (objGroupHeader != null) {
	                    objGroupHeader.style.display = "none";
	                    objGroupHeader.style.visibility = "hidden";
	                }
	                 if(sType =='G' && obctrl !=null)
	                 {
	                   //check for child controls and empty them and disable the validators assigned to them
	                     //neha goel: removed shell code and used table control to get asp controls instaed of page xml
	                     objGroup = eval("document.getElementById('" + cntrlarr[j] + "_table')");
	                     if (objGroup != null) {
	                         childNodes = objGroup.childNodes[0].childNodes;

	                         for (k = 0; k < childNodes.length; k++) {
	                             //EmptyControlRecursive(childNodes[k]);
	                             if (childNodes[k].childNodes.length > 1) {
	                                 EmptyControlRecursive(childNodes[k].childNodes[1].children[0]);
	                             }
	                         }
	                     }
	                    
	                 }
	                 else
	                 {
	                    //check for control and empty it and disable the validators assigned to them
	                    //neha goel--publix--05042011---finding control from page rather than page xml--start	                                                                 
	                     objCtrl = eval("document.getElementById('" + cntrlarr[j] + "')");
	                    if (objCtrl == null) 
                        {
                            objCtrl = eval("document.getElementById('" + cntrlarr[j] + "_codelookup')");
	                    }
                        if (objCtrl == null) {
                          objCtrl = eval("document.getElementById('" + cntrlarr[j] + "_multicode')");
                        }  
                        //neha goel--publix--05042011---finding control from page rather than page xml--end
	                   
	                   if (objCtrl != null && objCtrl.name != null)
                         {  
                              EmptyControlRecursive(objCtrl);                           
                         } 
	                 }         
	            }  
                 }
                 }
                 //COND 2 END
                 
                 //COND 3 - single code id and multiple controls
               else if(cntrlarr.length>1 && codearr.length==1)
                    {
                   
                  if((codearr[0].toLowerCase()==inpcode.toLowerCase()) || ((codearr[0].toLowerCase()=="*")&&(inpcode!="")) || ((codearr[0].toLowerCase()=="else")&& (ifexists != "yes") &&(inpcode!="")))   //If Selected value is in GroupNameIDs 11/23/2009 Saurabh Walia: To handle * and ELSE
	                {	        	        	            
	               
	                for(var k=0;k<cntrlarr.length;k++) {

	                    if (codearr[0] == "A3" && iID == "A3") {
	                        objPatientCount = eval("document.getElementById('PatientCount')");
	                        if (objPatientCount != null && objPatientCount.value != "") {
	                            if (objPatientCount.value == "0") {
	                                //alert("testing Patient Count");
	                                //bPatientCountcheck = false;
	                                continue;
	                            }
	                        }
	                    }   
	                
                    
                    if(sType =='G')
	                    {
	                        obctrl = eval("document.getElementById('" + cntrlarr[k] + "')");
	                        objGroupHeader = eval("document.getElementById('" + cntrlarr[k] + "_header')");
	                        bshow=true;	                       
	                    }
	                else
                        {	    //neha goel---pso-----added  ctl00_Main_            
                            obctrl=eval("document.getElementById('"+cntrlarr[k]  +"_ctlRow')");                            
                            bshow=true;                           
                        }                               
                        if(obctrl!=null)
                        {
	                    obctrl.style.display="";  //RMA-12071
                        obctrl.style.visibility="visible";  
                        }
                        //neha goel for pso group header
                        if (objGroupHeader != null) {
                            objGroupHeader.style.display = ""; //RMA-12071
                            objGroupHeader.style.visibility = "visible";
                        }
                      	                      	                
	                if(sType =='G' && obctrl!=null)
	                 {
	                     //check for child controls and empty them and disable the validators assigned to them
                         objGroup = eval("document.getElementById('" + cntrlarr[j] + "_table')");
                         if (objGroup != null) {
                             childNodes = objGroup.childNodes[0].childNodes;

                             for (k = 0; k < childNodes.length; k++) {
                                 if (childNodes[k].childNodes.length > 1) {
                                     sControlName = childNodes[k].childNodes[1].children[0].getAttribute("name");
                                     if (sControlName != null && sControlName != "" && sControlName.indexOf("$") != -1) {
                                         sControlName = sControlName.split("$")[0];
                                     }
                                     sControlHidden = childNodes[k].style.display;
                                     sControlVisibility = childNodes[k].style.visibility;
                                     //sControlDis = childNodes[m].getAttribute("disabled");
                                     //if(sControlHidden!="yes" && sControlDis!="yes")
                                     if (sControlHidden != "none" && sControlVisibility != "hidden") {
                                         obctrl = eval("document.getElementById('" + sControlName + "_Req')");
                                         if (obctrl != null) {
                                             obctrl.style.visibility = "hidden";
                                             //for removing * on postback--commented below two lines                                            
                                             //obctrl.enabled = true;                                             
                                            //ValidatorUpdateDisplay(obctrl);
                                             var sHiddenControl = document.getElementById('hiddencontrols').value;
                                             var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                                             if (sHiddenControl.indexOf(sControlName + "_ctlRow") != -1) {
                                                 document.getElementById('hiddencontrols').value = sHiddenControl.replace(sControlName + "_ctlRow", " ");
                                             }
                                             if (sPnlHiddenControl.indexOf(sControlName) != -1) {
                                                 document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace(sControlName, " ");
                                             }
                                         }
                                     }
                                 }
                             } //end neha goel :pso logic
                         }
                     }
                     else
                     { //neha goel---publix--05032011---removed Main1_
                        obctrl=eval("document.getElementById('"+cntrlarr[k]+"_Req')");                       
                        if(obctrl !=null)
                         {
                             obctrl.style.visibility = "hidden";
                             //for removing * on postback--commented below two lines                                            
                             //obctrl.enabled = true;                                             
                            //ValidatorUpdateDisplay(obctrl);
                             var sHiddenControl = document.getElementById('hiddencontrols').value;
                             var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                             if (sHiddenControl.indexOf(cntrlarr[k] + "_ctlRow") != -1) {
                                 document.getElementById('hiddencontrols').value = sHiddenControl.replace(cntrlarr[k] + "_ctlRow", " ");
                             }
                             if (sPnlHiddenControl.indexOf(cntrlarr[k]) != -1) {
                                 document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace(cntrlarr[k], " ");
                             } 
                         }
                     }
	                obctrl=null; 
                  }
                  }              
                 else
                 {
                 for(j=0;j<cntrlarr.length;j++)
	            {   //neha goel added for PSO: to skip hide in case of multicode when scode is 1 and cntrls to hide is>1
	                if (smulticode == "multicode") {
	                    continue;
	                }//end Neha Goel
                    
                    if(sType =='G')
	                {	                 
	                   obctrl=eval("document.getElementById('"+cntrlarr[j]  +"')");
	                   objGroupHeader = eval("document.getElementById('" + cntrlarr[j] + "_header')");
                    }
	                else
	                {	 //neha goel---publix--05032011---added  ctl00_Main_                 	                 
	                 obctrl=eval("document.getElementById('"+cntrlarr[j]  +"_ctlRow')");
	                                  
                    }
	                 if(obctrl !=null)
	                 {
	                    obctrl.style.display="none";    
	                    obctrl.style.visibility="hidden"; 
	                 }
	                    //neha goel for pso group header
	                    if (objGroupHeader != null) {
	                        objGroupHeader.style.display = "none";
	                        objGroupHeader.style.visibility = "hidden";
	                    }
                     if(sType =='G' && obctrl !=null)
	                 {
	                   //check for child controls and empty them and disable the validators assigned to them
	                     //neha goel: removed shell code and used table control to get asp controls instaed of page xml
	                     objGroup = eval("document.getElementById('" + cntrlarr[j] + "_table')");
	                     if (objGroup != null) {
	                         childNodes = objGroup.childNodes[0].childNodes;

	                         for (k = 0; k < childNodes.length; k++) {
	                             //EmptyControlRecursive(childNodes[k]);
	                             if (childNodes[k].childNodes.length > 1) {
	                                 EmptyControlRecursive(childNodes[k].childNodes[1].children[0]);
	                             }
	                         }
	                     }
	                    
	                 }
	                 else
	                 {	                                          
	                     objCtrl = eval("document.getElementById('" + cntrlarr[j] + "')");
	                    if (objCtrl == null) 
                        {
	                       objCtrl = eval("document.getElementById('" + cntrlarr[j] + "_codelookup')");
	                    }
                        if (objCtrl == null) {
                          objCtrl = eval("document.getElementById('" + cntrlarr[j] + "_multicode')");
                        }
	                   if (objCtrl != null && objCtrl.name != null)
                         {  
                              EmptyControlRecursive(objCtrl);                           
                         } 
	                 }         
	            }  
                 }
                 }
                 //COND 3 end                
                             
              }
            
                        
	    else if( (codesuperarr[i].toLowerCase()==inpcode.toLowerCase()) || ((codesuperarr[i].toLowerCase()=="*")&&(inpcode!="")) || ((codesuperarr[i].toLowerCase()=="else")&& (ifexists != "yes") &&(inpcode!="")))   //If Selected value is in GroupNameIDs
	        {	        	         	        
	            for(var j=0;j<cntrlarr.length;j++)
	            {

	                var objMedEventDesc = null;
	                var sMedEventDesc = "";
	                //var objPatientCount = null;                
	                if (iID == "A1218" || iID == "A1224" || iID == "UNK") {
	                    if (cntrlarr[j] == "USCMEDGrid") {
	                        objMedEventDesc = eval("document.getElementById('psocdBestEventUSC_codelookup')");
	                        if (objMedEventDesc != null && objMedEventDesc.value != "") {
	                            sMedEventDesc = objMedEventDesc.value;
	                            if (sMedEventDesc.indexOf("A1263") != -1) {
	                                continue;
	                            }
	                            else if (sMedEventDesc.indexOf("UNK") != -1) {
	                                continue;
	                            }	                           
	                        }
	                    }
	                    else if (cntrlarr[j] == "NMMEDGrid") {
	                        objMedEventDesc = eval("document.getElementById('psocdBestEventNM_codelookup')");
	                        if (objMedEventDesc != null && objMedEventDesc.value != "") {
	                            sMedEventDesc = objMedEventDesc.value;
	                            if (sMedEventDesc.indexOf("A1263") != -1) {
	                                continue;
	                            }
	                            else if (sMedEventDesc.indexOf("UNK") != -1) {
	                                continue;
	                            }	                            
	                        }
	                    }
	                    else if (cntrlarr[j] == "INCMEDGrid") {
	                        objMedEventDesc = eval("document.getElementById('psocdBestEventINC_codelookup')");
	                        if (objMedEventDesc != null && objMedEventDesc.value != "") {
	                            sMedEventDesc = objMedEventDesc.value;
	                            if (sMedEventDesc.indexOf("A1263") != -1) {
	                                continue;
	                            }
	                            else if (sMedEventDesc.indexOf("UNK") != -1) {
	                                continue;
	                            }	                            
	                        }
	                    }
	                }

	                //neha grid
	                var objParentDocument = window.opener.document;
	                if (iID == "A54" && (cntrlarr[j] == "psocdSubstanceTypeINC" || cntrlarr[j] == "psocdSubstanceTypeNM" || cntrlarr[j] == "psocdSubstanceTypeUSC")) {
	                    var objINCMEDGrid = null;
	                    var objNMMEDGrid = null;
	                    var objUSCMEDGrid = null;
	                    objINCMEDGrid = eval("document.getElementById('INCMEDGrid_ctlRow')");
	                    if (objINCMEDGrid != null) {
	                        //objINCMEDGrid.style.display = "none";
	                        //objINCMEDGrid.style.visibility = "hidden";
	                        if (document.getElementById("txtINCMedInfoSessionId") != null) {
	                            document.getElementById("txtINCMedInfoSessionId").value = "";
	                            var objCtl = document.getElementById("txtFunctionToCall");
	                            if (objCtl != null) {
	                                objCtl.value = "PSOFormAdaptor.INCMEDGridSubmit";
	                            }
	                            var objCtlGridName = document.getElementById("txtGridDataDeleted");
	                            if (objCtlGridName != null) {
	                                if (objCtlGridName.value == "") {
                                    objCtlGridName.value = "INCMEDGrid";
	                                }
	                                else {
	                                    objCtlGridName.value = objCtlGridName.value + "|" + "INCMEDGrid";
	                                }
	                            }
	                            //document.getElementById("frmData").submit();
	                            //pleaseWait.Show();

	                        }

	                    }
	                    objNMMEDGrid = eval("document.getElementById('NMMEDGrid_ctlRow')");
	                    if (objNMMEDGrid != null) {
	                        //objNMMEDGrid.style.display = "none";
	                        //objNMMEDGrid.style.visibility = "hidden";
	                        if (document.getElementById("txtNMMedInfoSessionId") != null) {
	                            document.getElementById("txtNMMedInfoSessionId").value = "";
	                            var objCtl = document.getElementById("txtFunctionToCall");
	                            if (objCtl != null) {
	                                objCtl.value = "PSOFormAdaptor.NMMEDGridSubmit";
	                            }
	                            var objCtlGridName = document.getElementById("txtGridDataDeleted");
	                            if (objCtlGridName != null) {
	                                if (objCtlGridName.value == "") {
	                                    objCtlGridName.value = "NMMEDGrid";
	                                }
	                                else {
	                                    objCtlGridName.value = objCtlGridName.value + "|" + "NMMEDGrid";
	                                }
	                            }
	                           // document.getElementById("frmData").submit();
	                            //pleaseWait.Show();
	                        }
	                    }
	                    objUSCMEDGrid = eval("document.getElementById('USCMEDGrid_ctlRow')");
	                    if (objUSCMEDGrid != null) {
	                        //objUSCMEDGrid.style.display = "none";
	                        //objUSCMEDGrid.style.visibility = "hidden";
	                        if (document.getElementById("txtUSCMedInfoSessionId") != null) {
	                            document.getElementById("txtUSCMedInfoSessionId").value = "";
	                            var objCtl = document.getElementById("txtFunctionToCall");
	                            if (objCtl != null) {
	                                objCtl.value = "PSOFormAdaptor.USCMEDGridSubmit";
	                            }
	                            var objCtlGridName = document.getElementById("txtGridDataDeleted");
	                            if (objCtlGridName != null) {
	                                if (objCtlGridName.value == "") {
	                                    objCtlGridName.value = "USCMEDGrid";
	                                }
	                                else {
	                                    objCtlGridName.value = objCtlGridName.value + "|" + "USCMEDGrid";
	                                }
	                            }
	                            //document.getElementById("frmData").submit();
	                           // pleaseWait.Show();
	                        }
	                    }
	                    document.getElementById("posted").value = "true";
	                    document.getElementById("frmData").submit();
	                    pleaseWait.Show();
	                }
	                
	                if (cntrlarr[j] == "pnlPIF" && iID == "A3") {
	                    objPatientCount = eval("document.getElementById('PatientCount')");
	                    var objSaveBtn = eval("document.getElementById('btnPSOSave')");
	                    if (objPatientCount != null && objPatientCount.value != "") {
	                        if (objPatientCount.value == "0") {
	                            alert("This incident does not involve any Patient(s). To submit a PSO Incident, please close this window and add a Patient Involved to the RMX Event.");
	                            if (objSaveBtn != null) {
	                                objSaveBtn.disabled = true;
	                            }
	                            return;
	                        }
	                        else {
	                            if (objSaveBtn != null) {
	                                objSaveBtn.disabled = false;
	                            }
	                        }
	                    }
	                }

	                if (cntrlarr[j] == "pnlNeonatal" && iID == "A1524") {
	                    objPatientCount = eval("document.getElementById('PatientCount')");
	                    var objSaveBtn = eval("document.getElementById('btnPSOSave')");	                   
	                    if (objPatientCount != null && objPatientCount.value != "") {
	                        if (objPatientCount.value < 2) {
	                            alert("This Incident involves only one Patient.You will not be able to submit the Neonatal Information with this PSO Incident." + "\nPlease close this window and add another Patient Involved to the RMX Event.");
	                            if (objSaveBtn != null) {
	                                objSaveBtn.disabled = true;
	                            }                            
	                        }
	                        else {
	                            if (objSaveBtn != null) {
	                                objSaveBtn.disabled = false;
	                            }
	                        }                        
	                    }
	                }	                         

                    if(sType =='G')
	                    {	         
	                    //In case of * empty all such controls except if the previous value is selected	                  
	                      if(codesuperarr[i].toLowerCase()=="*")
	                      {	                  
        	               //check for child controls and empty them and disable the validators assigned to them
	                          //neha goel: removed shell code and used table control to get asp controls instaed of page xml
	                          objGroup = eval("document.getElementById('" + cntrlarr[j] + "_table')");
	                          if (objGroup != null) {
	                              childNodes = objGroup.childNodes[0].childNodes;

	                              for (k = 0; k < childNodes.length; k++) {
	                                  //EmptyControlRecursive(childNodes[k]);
	                                  if (childNodes[k].childNodes.length > 1) {
	                                      EmptyControlRecursive(childNodes[k].childNodes[1].children[0]);
	                                  }
	                              }
	                          }
	                      }        	         
	                     //End                  
	                    obctrl=eval("document.getElementById('"+cntrlarr[j]  +"')");
	                    objGroupHeader = eval("document.getElementById('" + cntrlarr[j] + "_header')");
                        }
	                else
                        {	                                             
                         //In case of * empty all such controls except if the previous value is selected	                  
	                      if(codesuperarr[i].toLowerCase()=="*")
	                      {	                  
	                       objCtrl = eval("window.document.getElementById('"+m_sFieldName+"_cid')");
	                       if(objCtrl !=null)
	                       {
	                         if(objCtrl.value != lId)
	                         {
        	                   sCtrlName = "group/control[@name='" + cntrlarr[j] +"']";
	                           objCtrl = m_PageXml.getElementsByTagName(sCtrlName)[0];
	                           if (objCtrl != null && objCtrl.name != null)
                                 {  
                                      EmptyControlRecursive(objCtrl);                           
                                 } 
                              }
                           }
	                      }        	         
	                     //End   
	                     //neha goel---publix--05032011---added  ctl00_Main_   
	                     obctrl=eval("document.getElementById('"+cntrlarr[j]  +"_ctlRow')");
	                     
                        }
                   
	                if(obctrl !=null)
	                {
	                    obctrl.style.display="";  //RMA-12071
                        obctrl.style.visibility="visible";                                                
                        //For memo fields
                        //neha goel for pso group header
                        if (objGroupHeader != null) {
                            objGroupHeader.style.display = ""; //RMA-12071
                            objGroupHeader.style.visibility = "visible";
                        }


                    }
//                    //customized-neha goel
//                    if (iID == "A2250" && (cntrlarr[j] == "pnlMedication")) {
//                        var objINCMedSubTyp = null;
//                        var objNMMedSubTyp = null;
//                        var objUSCMedSubTyp = null;
//                        var objWhatReported = null;
//                        sWhatReported = "";
//                        objWhatReported = eval("document.getElementById('psocdWhatReported_codelookup')");
//                        if (objWhatReported != null && objWhatReported.value != "") {
//                            sWhatReported = objWhatReported.value;
//                            if (sWhatReported.indexOf("A3") != -1) {
//                                objINCMedSubTyp = eval("document.getElementById('psocdSubstanceTypeINC_ctlRow')");
//                                if (objINCMedSubTyp != null) {
//                                    objINCMedSubTyp.style.display = "inline";
//                                    objINCMedSubTyp.style.visibility = "visible";
//                                }
//                            }
//                            else if (sWhatReported.indexOf("A6") != -1) {
//                                objNMMedSubTyp = eval("document.getElementById('psocdSubstanceTypeNM_ctlRow')");
//                                if (objNMMedSubTyp != null) {
//                                    objNMMedSubTyp.style.display = "inline";
//                                    objNMMedSubTyp.style.visibility = "visible";
//                                }
//                            }
//                            else if (sWhatReported.indexOf("A9") != -1) {
//                                objUSCMedSubTyp = eval("document.getElementById('psocdSubstanceTypeUSC_ctlRow')");
//                                if (objUSCMedSubTyp != null) {
//                                    objUSCMedSubTyp.style.display = ""; //RMA-12071
//                                    objUSCMedSubTyp.style.visibility = "visible";
//                                }
//                            }
//                        }                      
//                       
//                    }     
                    
                    if(sType =='G' && obctrl!=null)
	                 {
	                   //check for child controls and empty them and disable the validators assigned to them
                       //neha goel: pso added the logic as per PSO architecture
	                   objGroup = eval("document.getElementById('" + cntrlarr[j] + "_table')");
	                   if (objGroup != null) {
	                       childNodes = objGroup.childNodes[0].childNodes;

	                       for (k = 0; k < childNodes.length; k++) {
	                           if (childNodes[k].childNodes.length > 1) {
	                               sControlName = childNodes[k].childNodes[1].children[0].getAttribute("name");
	                               if (sControlName != null && sControlName != "" && sControlName.indexOf("$") != -1) {
	                                   sControlName = sControlName.split("$")[0];
                                   }	                               
	                                //sControlDis = childNodes[m].getAttribute("disabled");
	                               //if(sControlHidden!="yes" && sControlDis!="yes")
	                               if (((iID == "A63" && cntrlarr[j] == "pnlSurgery") || (iID == "A51" && cntrlarr[j] == "pnlHAI")) && sControlName != "" && sControlName != null && childNodes[k].childNodes[1].children[0].value != "" && childNodes[k].childNodes[1].children[0].value != null) {
//	                                   var type = "";
//	                                   type = childNodes[k].childNodes[1].children[0].getAttribute("RMXType");
//	                                   EmptyControl(sControlName, type, '');                                  
	                                       if (childNodes[k].childNodes.length > 1) {
	                                           EmptyControlRecursive(childNodes[k].childNodes[1].children[0]);
	                                       }	                                   
	                               }

	                               sControlHidden = childNodes[k].style.display;
	                               sControlVisibility = childNodes[k].style.visibility;

	                                if (sControlHidden != "none" && sControlVisibility != "hidden")
                                       {
	                                      obctrl=eval("document.getElementById('"+sControlName+"_Req')");
	                                      if(obctrl !=null)
	                               	      {
	                               	          obctrl.style.visibility = "hidden";
	                               	          //for removing * on postback--commented below two lines                                            
	                               	          //obctrl.enabled = true;                                             
	                               	         //ValidatorUpdateDisplay(obctrl);
	                               	          var sHiddenControl = document.getElementById('hiddencontrols').value;
	                               	          var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
	                               	          if (sHiddenControl.indexOf(sControlName + "_ctlRow") != -1) {
	                               	              document.getElementById('hiddencontrols').value = sHiddenControl.replace(sControlName + "_ctlRow", " ");
	                               	          }
	                               	          if (sPnlHiddenControl.indexOf(sControlName) != -1) {
	                               	              document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace(sControlName, " ");
	                               	          }
	                               	       }
	                               	  }	                               	 
	                           }
	                       }//end neha goel :pso logic
	                   }

                     }
                     else
                     {//neha goel---publix--05032011---removed Main1_
                        obctrl=eval("document.getElementById('"+cntrlarr[j]+"_Req')");                        
                         if(obctrl !=null)
                         {                            
	                       obctrl.style.visibility="hidden";
	                       //for removing * on postback--commented below two lines                                            
	                       //obctrl.enabled = true;                                             
	                      //ValidatorUpdateDisplay(obctrl);
	                       var sHiddenControl = document.getElementById('hiddencontrols').value;
	                       var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
	                       if (sHiddenControl.indexOf(cntrlarr[j] + "_ctlRow") != -1) {
	                           document.getElementById('hiddencontrols').value = sHiddenControl.replace(cntrlarr[j] + "_ctlRow", " ");
	                       }
	                       if (sPnlHiddenControl.indexOf(cntrlarr[j]) != -1) {
	                           document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace(cntrlarr[j], " ");
	                       }                           
                         }
                     }
	                obctrl=null;	              
                  }   
           }     
	        else                                               //If selected value doesn't match with GroupNameIDs
	        {
	            //if (m_sType == "multicode")                    //neha goel for pso
	            if(smulticode=="multicode")                      //Skip hide function for Multicode.
	            continue;  
	            for(j=0;j<cntrlarr.length;j++)
	            {
	                //neha goel:PSO : do not hide in case of perinatal->mother->if mat adv out is "hemmo..." or inftype is "three or four"
	                if ((iID == "A1617" || iID=="A66") && cntrlarr[j] == "psocdVaginalDeliveryMother") {
	                    objMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMother_multicode')");
	                    objMatBdyPart = eval("document.getElementById('psolstBodyPartMother_multicode')");
	                    if (objMatBdyPart != null && objMatBdyPart.length > 0) {
	                        sMatBdyPartValue = objMatBdyPart.innerText;
	                        if (sMatBdyPartValue.indexOf("A1629") != -1)
	                            continue;
	                    }
	                    if (objMatAdvOut != null && objMatAdvOut.length > 0) {
	                        sMatAdvOutValue = objMatAdvOut.innerText;
	                        if (sMatAdvOutValue.indexOf("A1599") != -1)
	                            continue;
	                    }
	                }

	                //neha goel:PSO : do not hide in case of perinatal->mother->if mat adv out is "hemmo..." or inftype is "three or four"
	                if ((iID == "A1617" || iID == "A66") && cntrlarr[j] == "psocdVaginalDeliveryMotherNeonate") {
	                    objMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMotherNeonate_multicode')");
	                    objMatBdyPart = eval("document.getElementById('psolstBodyPartMotherNeonate_multicode')");
	                    if (objMatBdyPart != null && objMatBdyPart.length > 0) {
	                        sMatBdyPartValue = objMatBdyPart.innerText;
	                        if (sMatBdyPartValue.indexOf("A1629") != -1) {
	                            continue;
	                        }
	                        else if (sMatBdyPartValue.indexOf("A1626") != -1 || sMatBdyPartValue.indexOf("A1632") != -1 || sMatBdyPartValue.indexOf("A1635") != -1 || sMatBdyPartValue.indexOf("A1638") != -1 || sMatBdyPartValue.indexOf("A66") != -1) {
	                            ObjNeoSustain = eval("document.getElementById('psolstNeonateSustain_multicode')");
	                            if (ObjNeoSustain != null && ObjNeoSustain.length > 0) {
	                                sNeoSustainValue = ObjNeoSustain.innerText;
	                                if (sNeoSustainValue.indexOf("A1665") != -1 || sNeoSustainValue.indexOf("A1671") != -1 || sNeoSustainValue.indexOf("A1644") != -1 || sNeoSustainValue.indexOf("A1659") != -1 || sNeoSustainValue.indexOf("A1668") != -1)
	                                    continue;
	                            }
	                        }
	                    }
	                    if (objMatAdvOut != null && objMatAdvOut.length > 0) {
	                        sMatAdvOutValue = objMatAdvOut.innerText;
	                        if (sMatAdvOutValue.indexOf("A1599") != -1) {
	                            continue;
	                        }
	                        else if (sMatAdvOutValue.indexOf("A1602") != -1 || sMatAdvOutValue.indexOf("A1605") != -1 || sMatAdvOutValue.indexOf("A1596") != -1 || sMatAdvOutValue.indexOf("A1597") != -1 || sMatAdvOutValue.indexOf("A66") != -1) {
	                            ObjNeoSustain = eval("document.getElementById('psolstNeonateSustain_multicode')");
	                            if (ObjNeoSustain != null && ObjNeoSustain.length > 0) {
	                                sNeoSustainValue = ObjNeoSustain.innerText;
	                                if (sNeoSustainValue.indexOf("A1665") != -1 || sNeoSustainValue.indexOf("A1671") != -1 || sNeoSustainValue.indexOf("A1644") != -1 || sNeoSustainValue.indexOf("A1659") != -1 || sNeoSustainValue.indexOf("A1668") != -1)
	                                    continue;
	                            }
	                        }
	                    }
	                }
                    //neha grid
	                if ((iID == "A1263" || iID == "UNK") && cntrlarr[j] == "psolstIncorrectAction") {	                   
	                    var obctrlGridINC = null;
	                    var obctrlGridNM = null;
	                    var obctrlGridUSC = null;	                    
	                    obctrlGridINC = eval("document.getElementById('INCMEDGrid_ctlRow')");
	                    if (obctrlGridINC != null) {	                        
	                        if (document.getElementById("txtINCMedInfoSessionId") != null) {
	                            document.getElementById("txtINCMedInfoSessionId").value = "";	                            
	                        }
	                        var objCtl = document.getElementById("txtFunctionToCall");
	                        if (objCtl != null) {
	                            objCtl.value = "PSOFormAdaptor.INCMEDGridSubmit";
	                        }
	                        var objCtlGridName = document.getElementById("txtGridDataDeleted");
	                        if (objCtlGridName != null) {
	                            if (objCtlGridName.value == "") {
	                                objCtlGridName.value = "INCMEDGrid";
	                            }
	                            else {
	                                objCtlGridName.value = objCtlGridName.value + "|" + "INCMEDGrid";
	                            }
	                        }

	                    }
	                    obctrlGridNM = eval("document.getElementById('NMMEDGrid_ctlRow')");
	                    if (obctrlGridNM != null) {	                                          
	                        if (document.getElementById("txtNMMedInfoSessionId") != null) {
	                            document.getElementById("txtNMMedInfoSessionId").value = "";	                           
	                        }
	                        var objCtl = document.getElementById("txtFunctionToCall");
	                        if (objCtl != null) {
	                            objCtl.value = "PSOFormAdaptor.NMMEDGridSubmit";
	                        }
	                        var objCtlGridName = document.getElementById("txtGridDataDeleted");
	                        if (objCtlGridName != null) {
	                            if (objCtlGridName.value == "") {
	                                objCtlGridName.value = "NMMEDGrid";
	                            }
	                            else {
	                                objCtlGridName.value = objCtlGridName.value + "|" + "NMMEDGrid";
	                            }
	                        }
	                    }
	                    obctrlGridUSC = eval("document.getElementById('USCMEDGrid_ctlRow')");
	                    if (obctrlGridUSC != null) {	                        
	                        if (document.getElementById("txtUSCMedInfoSessionId") != null) {
	                            document.getElementById("txtUSCMedInfoSessionId").value = "";
	                        }
	                        var objCtl = document.getElementById("txtFunctionToCall");
	                        if (objCtl != null) {
	                            objCtl.value = "PSOFormAdaptor.USCMEDGridSubmit";
	                        }
	                        var objCtlGridName = document.getElementById("txtGridDataDeleted");
	                        if (objCtlGridName != null) {
	                            if (objCtlGridName.value == "") {
	                                objCtlGridName.value = "USCMEDGrid";
	                            }
	                            else {
	                                objCtlGridName.value = objCtlGridName.value + "|" + "USCMEDGrid";
	                            }
	                        }
	                    }
	                    document.getElementById("posted").value = "true";
	                    document.getElementById("frmData").submit();
	                    pleaseWait.Show();	                    	                    
	                }

	                if (cntrlarr[j] == "pnlPIF" && (iID == "A6" || iID == "A9")) {               
	                    var objSaveBtn = eval("document.getElementById('btnPSOSave')");
	                    if (objSaveBtn != null) {
	                        objSaveBtn.disabled = false;
	                    }
	                }

	                if (cntrlarr[j] == "pnlNeonatal" && (iID !="A1524")) {
	                    var objSaveBtn = eval("document.getElementById('btnPSOSave')");
	                    if (objSaveBtn != null) {
	                        objSaveBtn.disabled = false;
	                    }
	                }      



                    if(sType =='G')
	                {	                 
	                   obctrl=eval("document.getElementById('"+cntrlarr[j]  +"')");
	                   objGroupHeader = eval("document.getElementById('" + cntrlarr[j] + "_header')");
                    }
	                else
	                {	//neha goel---publix--05032011---added ctl00_Main_                	                 
	                 obctrl=eval("document.getElementById('"+cntrlarr[j]  +"_ctlRow')");	                  
                    }
	                 if(obctrl !=null)
	                 {	                    
	                    obctrlID=eval("document.getElementById('"+cntrlarr[j]  +"')");
	                     
	                    if (obctrlID == null) {
	                        obctrlID = eval("document.getElementById('" + cntrlarr[j] + "_codelookup')");
	                    }
	                    if (obctrlID == null) {
	                        obctrlID = eval("document.getElementById('" + cntrlarr[j] + "_multicode')");
	                    }               
                        if (obctrlID != null) {                            
                                obctrlID.value = "";                           
                        }	                    
	                    obctrl.style.display="none";    
	                    obctrl.style.visibility="hidden";

	                    if (cntrlarr[j] == "pnlDevice") {
	                        m_DeviceDataDeleteChk = true;	                        
	                    }
                     
                     }
	                    //neha goel for pso group header
	                    if (objGroupHeader != null) {
	                        objGroupHeader.style.display = "none";
	                        objGroupHeader.style.visibility = "hidden";
	                    }
                     if(sType =='G' && obctrl !=null)
	                 {
	                   //check for child controls and empty them and disable the validators assigned to them
	                     //neha goel: removed shell code and used table control to get asp controls instaed of page xml
	                     objGroup = eval("document.getElementById('" + cntrlarr[j] + "_table')");
	                     if (objGroup != null) {
	                         childNodes = objGroup.childNodes[0].childNodes;

	                         for (k = 0; k < childNodes.length; k++) {	                             
	                             if (childNodes[k].childNodes.length > 1) {
	                                 EmptyControlRecursive(childNodes[k].childNodes[1].children[0]);
	                             }
	                         }
	                     }
	                    
	                 }
	                 else
	                 {
	                    //check for control and empty it and disable the validators assigned to them
	                    //neha goel--publix--05042011---finding control from page rather than page xml--start	                                        
	                    objCtrl = eval("document.getElementById('" + cntrlarr[j] + "')");
	                    if (objCtrl == null) 
                        {
	                         objCtrl = eval("document.getElementById('" + cntrlarr[j] + "_codelookup')");
	                    } 
                        if (objCtrl == null) {
                          objCtrl = eval("document.getElementById('" + cntrlarr[j] + "_multicode')");
                        }   
                        //neha goel--pso---finding control from page rather than page xml--end                  
                     
                        obctrl=eval("document.getElementById('"+cntrlarr[j]+"_Req')");                        
                         if(obctrl !=null)
                         {                            
	                       obctrl.style.visibility="hidden";
	                       //obctrl.style.display = "none"; 
	                       //obctrl.enabled=false;
	                       //ValidatorUpdateDisplay(obctrl);                           
	                       var sHiddenControl = document.getElementById('hiddencontrols').value;
	                       var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
	                       if (sHiddenControl.indexOf(cntrlarr[j] + "_ctlRow") == -1) {
	                           document.getElementById('hiddencontrols').value = document.getElementById('hiddencontrols').value + "," + cntrlarr[j] + "_ctlRow";
	                       }
	                       if (sPnlHiddenControl.indexOf(cntrlarr[j]) == -1) {
	                           document.getElementById('pnlhiddencontrols').value = document.getElementById('pnlhiddencontrols').value + "," + cntrlarr[j];
	                       }

                         }                        
	                if (objCtrl != null && objCtrl.name != null)
                         {  
                              EmptyControlRecursive(objCtrl);                           
                         } 
	                 }         
	            }             
	        }              
    }
    }
    if(sType=='G')
    {
     	m_ArrGroupName ="";
    	m_ArrGroupNameID ="";
    }	
    else
    {
        m_ArrFieldControls ="";
	    m_ArrFieldControlsID=""; 
    }	
	smulticode = "";
	if (m_DeviceDataDeleteChk == true) {	  
	    document.getElementById("txtDeviceInfoSessionId").value = "";
	    m_DeviceDataDeleteChk = false;
	    var objCtl = document.getElementById("txtFunctionToCall");
	    if (objCtl != null) {
	        objCtl.value = "PSOFormAdaptor.DeviceGridSubmit";
	    }	    
	    var objCtlGridName = document.getElementById("txtGridDataDeleted");
	    if (objCtlGridName != null) {
	        if (objCtlGridName.value == "") {
	            objCtlGridName.value = "DevicesGrid";
	        }
	        else {
	            objCtlGridName.value = objCtlGridName.value + "|" + "DevicesGrid";
	        }
	    }
	    document.getElementById("posted").value = "true";
	    document.getElementById("frmData").submit();
	    pleaseWait.Show();
	}
}

function EmptyControl(sControlName, sControlType, sControlHidden) {
    var obctrl;
    var obctrl1;
    var obctrl2;    
    var obCtrlDisplay;
    var sCodeIdList="";
    var sCodeId="";    
     if (sControlName == null)
        {return;} 
    if(sControlName.split('$')[3]=="txtDate")
    {
       sControlType = "date";
    }
    else if(sControlName.split('$')[3]=="TxtTime")
    {
    sControlType = "time";
    }  
  
    if(sControlName.indexOf("$")!=-1){
    sControlName= sControlName.split('$')[0];//neha goel replaced index 2 with 0 for pso
    }
    switch(sControlType)
    {
        case "text":            
            obctrl = eval("document.getElementById('" + sControlName + "')"); //neha goel removed ctl00_Main for pso
            if (obctrl != null) {
                if (!obctrl.readOnly) {
                    obctrl.value = "";
                }                
            }
            break;    
     case "phone":
         obctrl = eval("document.getElementById('" + sControlName + "')"); //neha goel neha goel removed ctl00_Main for pso
         if(obctrl !=null)
         {
             if (!obctrl.readOnly) {
                 obctrl.value = "";
             }                   
         }
      break;
  case "dropdown":
      obctrl = eval("document.getElementById('" + sControlName + "')"); //neha goel neha goel removed ctl00_Main for pso
      if (obctrl != null) {
          if (!obctrl.readOnly) {
              obctrl.selectedIndex = 0;
          }                               
      }
      break;
  case "code":
      obctrl = eval("document.getElementById('" + sControlName + "_codelookup')");
      obctrl1 = eval("document.getElementById('" + sControlName + "_codelookup_cid')"); //neha goel neha goel removed ctl00_Main for pso
      if (sControlName == "psocdIdentifyReporter" || sControlName == "psocdLaborinduced") {
          break;
      }
      if (obctrl != null || obctrl1 != null) {          
          if (obctrl1.value == "0") {
              obctrl1.value = "0";
              obctrl1.defaultvalue = "0";
          }
          else if (!obctrl.readOnly) {
              obctrl1.value = "";
              obctrl.value = "";
              obctrl1.defaultvalue = "";
              obctrl.defaultvalue = "";
          }    
          
      }
      break;
  case "multicode":
      obctrl = eval("document.getElementById('" + sControlName + "_multicode')"); //neha goel neha goel removed ctl00_Main for pso
      obctrl1 = eval("document.getElementById('" + sControlName + "_multicode_lst')"); //neha goel neha goel removed ctl00_Main for pso      
      if (obctrl != null || obctrl1 != null) {
          if (!obctrl.disabled) {
              obctrl.options.length = 0;
              obctrl1.value = "";
          }
          //obctrl2.value="";
      }
      break;
  case "codelist":
      obctrl = eval("document.getElementById('" + sControlName + "_multicode')"); //neha goel neha goel removed ctl00_Main for pso
      obctrl1 = eval("document.getElementById('" + sControlName + "_multicode_lst')"); //neha goel neha goel removed ctl00_Main for pso     
      if (obctrl != null || obctrl1 != null) {
          if (!obctrl.disabled) {
              obctrl.options.length = 0;
              obctrl1.value = "";
          }
          //obctrl2.value="";
      }
      break;
  case "time":
      obctrl = eval("document.getElementById('" + sControlName + "_TxtTime')"); //neha goel neha goel removed ctl00_Main for pso
      if (obctrl != null) {
          if (!obctrl.readOnly) {
              obctrl.value = "";
          }          
      }
      break;
  case "date":
      obctrl = eval("document.getElementById('" + sControlName + "')");     //neha goel neha goel removed ctl00_Main for pso 
      if (obctrl != null) {
          if (!obctrl.readOnly) {
              obctrl.value = "";
              var objDateReq = eval("document.getElementById('" + sControlName + "_Req')");
              if (objDateReq != null) {
                  var objValDateCtrl = eval("document.getElementById('" + sControlName + "_ValidateDate')");
                  if (objValDateCtrl != null) {
                      objValDateCtrl.value = "";
                  }
              }

          }                                         
      }
      break;
  case "memo":
      obctrl = eval("document.getElementById('" + sControlName + "')"); //neha goel neha goel removed ctl00_Main for pso
      if (sControlName == "txtLocationDescription" || sControlName == "txtEventDescription") {
          break;
      }
      if (obctrl != null) {
          //if (!obctrl.readonly) {
              obctrl.value = "";
          //}                           
      }
      break;    
      //10/07/2009 Saurabh Walia: For handling All types of TextBoxes
  default:      
      obctrl = eval("document.getElementById('" + sControlName + "')"); //neha goel neha goel removed ctl00_Main for pso
      if (obctrl != null) {
          if (!obctrl.readOnly) {
              obctrl.value = "";
          }
      }
      break;    
      //End                          
}
    obctrl = eval("document.getElementById('" + sControlName + "_Req')"); //neha goel neha goel removed ctl00_Main for pso    
    if(obctrl !=null)
     {
         //ValidatorEnable(obctrl, false);
         var sHiddenControl = document.getElementById('hiddencontrols').value;
         var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
         if (sHiddenControl.indexOf(sControlName + "_ctlRow") == -1) {
             document.getElementById('hiddencontrols').value = document.getElementById('hiddencontrols').value + "," + sControlName + "_ctlRow";
         }
         if (sPnlHiddenControl.indexOf(sControlName) == -1) {
             document.getElementById('pnlhiddencontrols').value = document.getElementById('pnlhiddencontrols').value + "," + sControlName;
         }
     }     
        obctrl = null;
        obctrl = eval("document.getElementById('" + sControlName + "_ctlRow')"); //neha goel neha goel removed ctl00_Main for pso
        if(obctrl == null)
        {
            obctrl = eval("document.getElementById('" + sControlName + "_ctlCol')"); //neha goel neha goel removed ctl00_Main for pso
          if(obctrl!=null){
                              var obctrlTitle = eval("document.getElementById('"+sControlName+"_Title_ctlCol')")
                                  if(obctrlTitle!=null && obctrlTitle.style.display=="" && obctrlTitle.style.visibility=="visible"){ //RMA-12071
	                                    obctrlTitle.style.display="none";  
                                        obctrlTitle.style.visibility="hidden";  
                                        }
                              }
        }
        if(obctrl !=null)
        {
            if (obctrl.style.display == "" && obctrl.style.visibility == "visible") { //RMA-12071
            obctrl.style.display="none";    
            obctrl.style.visibility="hidden"; 
            }
        }     
     obctrl = null;
     obctrl1 = null;
     obctrl2 = null;
     obctrlTitle = null;
}

function EmptyControlRecursive(objCtrl)
{
    var sControlName;
    var sControlType;
    var sControlHidden;
    var sHideControlNameList;
    var sHideGroupNameList;
    //var sHideAddCntrlNameList;
    var cntrlsuperarr;
  	var cntrlarr;
  	//var addcntrlarr;
  	var grpsuperarr;
  	var grparr;
  	var sCntrlName;
  	var objCtrl1;
    
    sControlName = objCtrl.getAttribute("name");
    //neha goel--Pso--06032011
    if(sControlName==null)
    {
        sControlName = objCtrl.getAttribute("id");
    }
    
    //sControlType = objCtrl.getAttribute("type");
    sControlType = objCtrl.getAttribute("RMXType"); //neha goel commented to fetch by rmxtype for pso
    sControlHidden = objCtrl.getAttribute("hidden");
    sHideControlNameList = objCtrl.getAttribute("DisplayHideFieldName");   //neha goel corrected the group tag as per pso
    //sHideGroupNameList = objCtrl.getAttribute("displaygroupname");
    sHideGroupNameList = objCtrl.getAttribute("DisplayHideGroupName");    //neha goel corrected the group tag as per pso
    //sHideAddCntrlNameList = objCtrl.getAttribute("addcontrols");
    
    if(sHideControlNameList == null)
    {
        sHideControlNameList = "";
    }
    if(sControlHidden == null)
    {
        sControlHidden = "";
    }
    if(sHideGroupNameList == null)
    {
        sHideGroupNameList = "";
    }
//    if(sHideAddCntrlNameList == null)
//    {
//        sHideAddCntrlNameList = "";
//    }
    cntrlsuperarr=sHideControlNameList.split("%#");
    
    for(var i=0;i<cntrlsuperarr.length;i++)
    {
        cntrlarr=cntrlsuperarr[i].split(",");
        if(sHideControlNameList != "")
        {
            for(var j=0;j<cntrlarr.length;j++)
            {      
                //check for control and empty it and disable the validators assigned to them
                var objMatAdvOut = null;
                var objMatBdyPart = null;
                var objInfTyp = null;
                var sInfTypValue = "";
                var sMatBdyPartValue = "";
                var sMatAdvOutValue = "";
                var ObjNeoSustain = null;
                var sNeoSustainValue = "";

                //neha goel:PSO : do not hide in case of perinatal->mother->if mat adv out is "hemmo..." or inftype is "three or four"
                if (sControlName == "psocdMeternalInfectionMother$codelookup" && cntrlarr[j] == "psocdVaginalDeliveryMother") {
                    objMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMother_multicode')");
                    objMatBdyPart = eval("document.getElementById('psolstBodyPartMother_multicode')");
                    if (objMatBdyPart != null && objMatBdyPart.length > 0) {
                        sMatBdyPartValue = objMatBdyPart.innerText;
                        if (sMatBdyPartValue.indexOf("A1629") != -1)
                            continue;
                    }
                    if (objMatAdvOut != null && objMatAdvOut.length > 0) {
                        sMatAdvOutValue = objMatAdvOut.innerText;
                        if (sMatAdvOutValue.indexOf("A1599") != -1)
                            continue;
                    }
                }

                //neha goel:PSO : do not hide in case of perinatal->mother->if mat adv out is "hemmo..." or inftype is "endo.."
                if (sControlName == "psolstBodyPartMother$multicode" && cntrlarr[j] == "psocdVaginalDeliveryMother") {
                    objMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMother_multicode')");
                    objInfTyp = eval("document.getElementById('psocdMeternalInfectionMother_codelookup')");
                    if (objInfTyp != null && objInfTyp.value!= "") {
                        sInfTypValue = objInfTyp.value;
                        if (sInfTypValue.indexOf("A1620") != -1)
                            continue;
                    }
                    if (objMatAdvOut != null && objMatAdvOut.length > 0) {
                        sMatAdvOutValue = objMatAdvOut.innerText;
                        if (sMatAdvOutValue.indexOf("A1599") != -1)
                            continue;
                    }
                }

                //neha goel:PSO : do not hide in case of perinatal->mother->if mat adv out is "hemmo..." or inftype is "three or four"
                if (sControlName == "psocdMeternalInfectionMotherNeonate$codelookup" && cntrlarr[j] == "psocdVaginalDeliveryMotherNeonate") {
                    objMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMotherNeonate_multicode')");
                    objMatBdyPart = eval("document.getElementById('psolstBodyPartMotherNeonate_multicode')");
                    if (objMatBdyPart != null && objMatBdyPart.length > 0) {
                        sMatBdyPartValue = objMatBdyPart.innerText;
                        if (sMatBdyPartValue.indexOf("A1629") != -1)
                            continue;
                    }
                    if (objMatAdvOut != null && objMatAdvOut.length > 0) {
                        sMatAdvOutValue = objMatAdvOut.innerText;
                        if (sMatAdvOutValue.indexOf("A1599") != -1) {
                            continue;
                        }
                        else if (sMatAdvOutValue.indexOf("A1602") != -1 || sMatAdvOutValue.indexOf("A1605") != -1 || sMatAdvOutValue.indexOf("A1596") != -1 || sMatAdvOutValue.indexOf("A1597") != -1 || sMatAdvOutValue.indexOf("A66") != -1) {
                            ObjNeoSustain = eval("document.getElementById('psolstNeonateSustain_multicode')");
                            if (ObjNeoSustain != null && ObjNeoSustain.length > 0) {
                                sNeoSustainValue = ObjNeoSustain.innerText;
                                if (sNeoSustainValue.indexOf("A1665") != -1 || sNeoSustainValue.indexOf("A1671") != -1 || sNeoSustainValue.indexOf("A1644") != -1 || sNeoSustainValue.indexOf("A1659") != -1 || sNeoSustainValue.indexOf("A1668") != -1)
                                    continue;
                            }
                        }
                    }
                }

                //neha goel:PSO : do not hide in case of perinatal->mother->if mat adv out is "hemmo..." or inftype is "endo.."
                if (sControlName == "psolstBodyPartMotherNeonate$multicode" && cntrlarr[j] == "psocdVaginalDeliveryMotherNeonate") {
                    objMatAdvOut = eval("document.getElementById('psolstAdverseOutcomeMotherNeonate_multicode')");
                    objInfTyp = eval("document.getElementById('psocdMeternalInfectionMotherNeonate_codelookup')");
                    if (objInfTyp != null && objInfTyp.value != "") {
                        sInfTypValue = objInfTyp.value;
                        if (sInfTypValue.indexOf("A1620") != -1)
                            continue;
                    }
                    if (objMatAdvOut != null && objMatAdvOut.length > 0) {
                        sMatAdvOutValue = objMatAdvOut.innerText;
                        if (sMatAdvOutValue.indexOf("A1599") != -1) {
                            continue;
                        }
                        else if (sMatAdvOutValue.indexOf("A1602") != -1 || sMatAdvOutValue.indexOf("A1605") != -1 || sMatAdvOutValue.indexOf("A1596") != -1 || sMatAdvOutValue.indexOf("A1597") != -1 || sMatAdvOutValue.indexOf("A66") != -1) {
                            ObjNeoSustain = eval("document.getElementById('psolstNeonateSustain_multicode')");
                            if (ObjNeoSustain != null && ObjNeoSustain.length > 0) {
                                sNeoSustainValue = ObjNeoSustain.innerText;
                                if (sNeoSustainValue.indexOf("A1665") != -1 || sNeoSustainValue.indexOf("A1671") != -1 || sNeoSustainValue.indexOf("A1644") != -1 || sNeoSustainValue.indexOf("A1659") != -1 || sNeoSustainValue.indexOf("A1668") != -1)
                                    continue;
                            }
                        }
                    }
                }                

                objCtrl1 = eval("document.getElementById('" + cntrlarr[j] + "')");
                        if (objCtrl1 == null) 
                        {
                            objCtrl1 = eval("document.getElementById('" + cntrlarr[j] + "_codelookup')");
                        } 
                        if(objCtrl1==null)
                        {
                          objCtrl1=eval("document.getElementById('"+cntrlarr[j]+"_multicode')");
                        }  
                        if(objCtrl1==null)
                        {
                          objCtrl1=eval("document.getElementById('"+cntrlarr[j]+"_txtDate')");
                        } 
                        if(objCtrl1==null)
                        {
                          objCtrl1 =eval("document.getElementById('"+cntrlarr[j]+"_TxtTime')");
                        }         
	            if (objCtrl1 != null)
	            {
	                EmptyControlRecursive(objCtrl1);
	            }    
            }
        }
    }
//    addcntrlarr = sHideAddCntrlNameList.split(",");
//    if(sHideAddCntrlNameList != "")
//    {
//        for(var k=0;k<addcntrlarr.length;k++)
//        {
//            //check for control and empty it and disable the validators assigned to them
//            sCtrlName = "group/control[@name='" + addcntrlarr[k] +"']";
//            objCtrl1 = m_PageXml.getElementsByTagName(sCtrlName)[0];
//            if (objCtrl1 != null)
//            {
//                EmptyControlRecursive(objCtrl1);
//            }    
//        }
//    }
    EmptyControl(sControlName,sControlType,sControlHidden);
    
    grpsuperarr=sHideGroupNameList.split("%#");
    if(sHideGroupNameList != "")
    {
        for(var k=0;k<grpsuperarr.length;k++)
        {
            grparr=grpsuperarr[k].split(",");
            
            for(var l=0;l<grparr.length;l++)
            {
                if(grparr[l] != "")
                EmptyGroups(grparr[l]);
            }
        }   
    } 
}

function EmptyGroups(groupname)
{
    var obgroup;
    var objGroup;
    var obGroupHeader    
    var childNodes;
    var k;
   
    //obgroup=eval("document.getElementById('ctl00_Main_Main1_"+ groupname +"_Grouprow')");
    obgroup = eval("document.getElementById('" + groupname + "')");
    obGroupHeader = eval("document.getElementById('" + groupname + "_header')");
    if(obgroup !=null)
    {
       obgroup.style.display="none";
       obgroup.style.visibility = "hidden";

       if (obGroupHeader != null) {
           obGroupHeader.style.display = "none";
           obGroupHeader.style.visibility = "hidden";
       }

       //check for child controls for each group and empty them and disable the validators assigned to them
       //neha goel added logic as per pso
       objGroup = eval("document.getElementById('" + groupname + "_table')");
       if (objGroup != null) {
           childNodes = objGroup.childNodes[0].childNodes;

           for (k = 0; k < childNodes.length; k++) {
               if (childNodes[k].childNodes.length > 1) {
                   EmptyControlRecursive(childNodes[k].childNodes[1].children[0]);
               }
           }
       }
        
    }
} 

//spahariya MITS 26002 - 09/02/2011 implement help text functionality in shell
//Method to display the popup on the click of the help button
function displayPopupDiv(sMessagetoDisplay,sSelectedTextToBold, e) {    
    var posx = 0;
    var posy = 0;
    if (!e) { var e = window.event };
    var targ = e.target ? e.target : e.srcElement;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX;
        posy = e.clientY;
        if (document.documentElement.scrollLeft || document.documentElement.scrollTop) {
            posx += document.documentElement.scrollLeft;
            posy += document.documentElement.scrollTop;
        }
    }
    var div = document.getElementById('popup');
    if (!div) {
        var div = document.createElement('div');
        div.setAttribute('id', 'popup');
        div.className = 'popupdiv';
        div.appendChild(document.createTextNode(sMessagetoDisplay));
        document.getElementsByTagName('body')[0].appendChild(div);
        if (sSelectedTextToBold != "") {
            sMessagetoDisplay = sMessagetoDisplay.replace(/<b>|<B>/gi, ""); // remove previous formatting
            sMessagetoDisplay = sMessagetoDisplay.replace(/<\/b>|<\/B>/gi, "");
            if(sSelectedTextToBold.indexOf("~")!=-1){
                var wordarr = sSelectedTextToBold.split("~");
                for (var iwCount = 0; iwCount < wordarr.length; iwCount++) {
                    var word = "(" + wordarr[iwCount] + ")"; // must be in brackets 
                    var re = new RegExp(word, "gi"); // ignore case, global change 
                    sMessagetoDisplay = sMessagetoDisplay.replace(re, "<b>$1</b>");            
                }
                document.getElementById("popup").innerHTML = sMessagetoDisplay;
            }
            else{
                var word = sSelectedTextToBold;
                word = "(" + word + ")"; // must be in brackets            
                var re = new RegExp(word, "gi"); // ignore case, global change
                document.getElementById("popup").innerHTML = sMessagetoDisplay.replace(re, "<b>$1</b>");
            }
        } 
    }
    div.style.top = posy + 5 + 'px';
    div.style.left = posx + 5 + 'px';
}
//Method to hide the Div on mouseover
function hidePopupDiv() {
   
    var div = document.getElementById('popup');
    if (!div) { return };
    div.parentNode.removeChild(div);
}
//spahariya 09/02/2011 implement help text functionality in shell



function fnDeviceOk() {    
    var objCtl = document.getElementById("txtFunctionToCall");
    if (objCtl != null) {
        objCtl.value = "PSOFormAdaptor.SetDeviceInfo";
    }
    var objParentCtl = window.opener.document.getElementById("txtFunctionToCall");
    if (objParentCtl != null) {
        objParentCtl.value = "PSOFormAdaptor.SetDeviceInfo";
    }
    setMainPageDataChanged();
}

function fnRefreshParentDeviceOk() {    
    var sMode = "";
    if (document.getElementById("txtmode") != null) {
        sMode = document.getElementById("txtmode").value;
    }

    var objParentDocument = window.opener.document;
    if (sMode.toUpperCase() == "ADD") {
        if (objParentDocument.getElementById("txtDeviceInfoSessionId").value == "") {
            objParentDocument.getElementById("txtDeviceInfoSessionId").value = document.getElementById("SessionId").value;
        }
        else {
            objParentDocument.getElementById("txtDeviceInfoSessionId").value = objParentDocument.getElementById("txtDeviceInfoSessionId").value + "~" + document.getElementById("SessionId").value;
        }
    }
    else if (sMode.toUpperCase() == "EDIT") {
        var sTempSessionId = objParentDocument.getElementById("txtDeviceInfoSessionId").value;
        var sNewSessionId = document.getElementById("SessionId").value;
        var sDeviceSesionID = document.getElementById("txtDeviceInfoSessionId").value;
        if (sDeviceSesionID == "") {
            if (sTempSessionId == "") {
                sTempSessionId = sNewSessionId;
            }
            else {
                sTempSessionId = sTempSessionId + "~" + sNewSessionId;
            }
        }
        else {
            sTempSessionId = sTempSessionId.replace(sDeviceSesionID, sNewSessionId);
        }
        objParentDocument.getElementById("txtDeviceInfoSessionId").value = sTempSessionId;
    }
    objParentDocument.getElementById("posted").value = "true";
    objParentDocument.getElementById("gridsubmit").value = "true";    
    objParentDocument.getElementById("frmData").submit();    
    window.close();
}
function fnDeviceCancel() {
    window.close();
}
//***************************
function fnMedInfoOk() {   
    var objCtl = document.getElementById("txtFunctionToCall");
    if (objCtl != null) {
        objCtl.value = "PSOFormAdaptor.SetMedInfo";
    }
    var objParentCtl = window.opener.document.getElementById("txtFunctionToCall");
    if (objParentCtl != null) {
        objParentCtl.value = "PSOFormAdaptor.SetMedInfo";
    }
    setMainPageDataChanged();
}

function fnRefreshParentMedInfoOk() {   
    var sMode = "";
    if (document.getElementById("txtmode") != null) {
        sMode = document.getElementById("txtmode").value;
    }

    var sGridName = "";
    if (document.getElementById("txtgridname") != null) {
        sGridName = document.getElementById("txtgridname").value;
    }

    var objParentDocument = window.opener.document;
    if (sMode.toUpperCase() == "ADD") {
        switch (sGridName) {
            case "NMMEDGrid":
                   if (objParentDocument.getElementById("txtNMMedInfoSessionId").value == "") {
                        objParentDocument.getElementById("txtNMMedInfoSessionId").value = document.getElementById("SessionId").value;
                    }
                    else {
                        objParentDocument.getElementById("txtNMMedInfoSessionId").value = objParentDocument.getElementById("txtNMMedInfoSessionId").value + "~" + document.getElementById("SessionId").value;
                    }
                break;
            case "USCMEDGrid":
                    if (objParentDocument.getElementById("txtUSCMedInfoSessionId").value == "") {
                        objParentDocument.getElementById("txtUSCMedInfoSessionId").value = document.getElementById("SessionId").value;
                    }
                    else {
                        objParentDocument.getElementById("txtUSCMedInfoSessionId").value = objParentDocument.getElementById("txtUSCMedInfoSessionId").value + "~" + document.getElementById("SessionId").value;
                    }
                break;
                case "INCMEDGrid":
                    if (objParentDocument.getElementById("txtINCMedInfoSessionId").value == "") {
                        objParentDocument.getElementById("txtINCMedInfoSessionId").value = document.getElementById("SessionId").value;
                    }
                    else {
                        objParentDocument.getElementById("txtINCMedInfoSessionId").value = objParentDocument.getElementById("txtINCMedInfoSessionId").value + "~" + document.getElementById("SessionId").value;
                    }
                break;
            }
        
    }
    else if (sMode.toUpperCase() == "EDIT") {
        var sTempSessionId = "";
        var sNewSessionId = "";
        var sNMMedInfoSessionId = "";
        var sUSCMedInfoSessionId = "";
        var sINCMedInfoSessionId = "";
        switch (sGridName) {
            case "NMMEDGrid":
                sTempSessionId = objParentDocument.getElementById("txtNMMedInfoSessionId").value;
                sNewSessionId = document.getElementById("SessionId").value;
                sNMMedInfoSessionId = document.getElementById("txtMedInfoSessionId").value;
                if (sNMMedInfoSessionId == "") {
                    if (sTempSessionId == "") {
                        sTempSessionId = sNewSessionId;
                    }
                    else {
                        sTempSessionId = sTempSessionId + "~" + sNewSessionId;
                    }
                }
                else {
                    sTempSessionId = sTempSessionId.replace(sNMMedInfoSessionId, sNewSessionId);
                }
                objParentDocument.getElementById("txtNMMedInfoSessionId").value = sTempSessionId;        
                break;
            case "USCMEDGrid":
                sTempSessionId = objParentDocument.getElementById("txtUSCMedInfoSessionId").value;
                sNewSessionId = document.getElementById("SessionId").value;
                sUSCMedInfoSessionId = document.getElementById("txtMedInfoSessionId").value;
                if (sUSCMedInfoSessionId == "") {
                    if (sTempSessionId == "") {
                        sTempSessionId = sNewSessionId;
                    }
                    else {
                        sTempSessionId = sTempSessionId + "~" + sNewSessionId;
                    }
                }
                else {
                    sTempSessionId = sTempSessionId.replace(sUSCMedInfoSessionId, sNewSessionId);
                }
                objParentDocument.getElementById("txtUSCMedInfoSessionId").value = sTempSessionId;
                break;
            case "INCMEDGrid":
                sTempSessionId = objParentDocument.getElementById("txtINCMedInfoSessionId").value;
                sNewSessionId = document.getElementById("SessionId").value;
                sINCMedInfoSessionId = document.getElementById("txtMedInfoSessionId").value;
                if (sINCMedInfoSessionId == "") {
                    if (sTempSessionId == "") {
                        sTempSessionId = sNewSessionId;
                    }
                    else {
                        sTempSessionId = sTempSessionId + "~" + sNewSessionId;
                    }
                }
                else {
                    sTempSessionId = sTempSessionId.replace(sINCMedInfoSessionId, sNewSessionId);
                }
                objParentDocument.getElementById("txtINCMedInfoSessionId").value = sTempSessionId;
                break;
        }
    }
    objParentDocument.getElementById("posted").value = "true";
    objParentDocument.getElementById("gridsubmit").value = "true";
    objParentDocument.getElementById("frmData").submit();    
    window.close();
}
function fnMedInfoCancel() {
    window.close();
}
//******************************
//////*********************************
function fnLnkEvtOk() {   
    var objCtl = document.getElementById("txtFunctionToCall");
    if (objCtl != null) {
        objCtl.value = "PSOFormAdaptor.SetLnkEvtInfo";
    }
    var objParentCtl = window.opener.document.getElementById("txtFunctionToCall");
    if (objParentCtl != null) {
        objParentCtl.value = "PSOFormAdaptor.SetLnkEvtInfo";
    }
    setMainPageDataChanged();
}

function setMainPageDataChanged() {
    var oSysPageDataChanged = window.opener.document.getElementById("SysPageDataChanged");
    if (oSysPageDataChanged != null) {
        oSysPageDataChanged.value = "true";
    }
}

function fnRefreshParentLnkEvtInfoOk() {    
    var sMode = "";
    if (document.getElementById("txtmode") != null) {
        sMode = document.getElementById("txtmode").value;
    }

    var objParentDocument = window.opener.document;
    if (sMode.toUpperCase() == "ADD") {
        if (objParentDocument.getElementById("txtLnkEvtInfoSessionId").value == "") {
            objParentDocument.getElementById("txtLnkEvtInfoSessionId").value = document.getElementById("SessionId").value;
        }
        else {
            objParentDocument.getElementById("txtLnkEvtInfoSessionId").value = objParentDocument.getElementById("txtLnkEvtInfoSessionId").value + "~" + document.getElementById("SessionId").value;
        }
    }
    else if (sMode.toUpperCase() == "EDIT") {
        var sTempSessionId = objParentDocument.getElementById("txtLnkEvtInfoSessionId").value;
        var sLnkEvtSessionId = document.getElementById("txtlnkEvtInfoSessionId").value;
        var sNewSessionId = document.getElementById("SessionId").value;
        if (sLnkEvtSessionId == "") {
            if (sTempSessionId == "") {
                sTempSessionId = sNewSessionId;
            }
            else {
                sTempSessionId = sTempSessionId + "~" + sNewSessionId;
            }
        }
        else {
            sTempSessionId = sTempSessionId.replace(sLnkEvtSessionId, sNewSessionId);
        }
        objParentDocument.getElementById("txtLnkEvtInfoSessionId").value = sTempSessionId;
    }
    objParentDocument.getElementById("posted").value = "true";
    objParentDocument.getElementById("gridsubmit").value = "true";
    objParentDocument.getElementById("frmData").submit();    
    window.close();
}
function fnLnkEvtCancel() {
    window.close();
}
///////****************************
function HideControlsGrid() {
    
    var sGridName = "";
    if (document.getElementById("txtgridname") != null) {
        sGridName = document.getElementById("txtgridname").value;
    }
    var sReqControl = document.getElementById('requiredcontrols').value;
    switch (sGridName) {
        case "NMMEDGrid":
            document.getElementById("cdWasMedGiven_ctlRow").style.setAttribute('display', 'none');
            if (document.getElementById("cdWasMedGiven_Req") != null) {
                document.getElementById("cdWasMedGiven_Req").enabled = false;
                if (document.getElementById('requiredcontrols') != null) {
                    if (document.getElementById('requiredcontrols').value.indexOf("cdWasMedGiven_Req") != -1) {
                        document.getElementById('requiredcontrols').value = document.getElementById('requiredcontrols').value.replace("cdWasMedGiven_Req", " ");
                    }

                }
            }
            break;
        case "USCMEDGrid":
            document.getElementById("cdWasMedGiven_ctlRow").style.setAttribute('display', 'none');
            if (document.getElementById("cdWasMedGiven_Req") != null) {
                document.getElementById("cdWasMedGiven_Req").enabled = false;
                if (document.getElementById('requiredcontrols') != null) {
                    if (document.getElementById('requiredcontrols').value.indexOf("cdWasMedGiven_Req") != -1) {
                        document.getElementById('requiredcontrols').value = document.getElementById('requiredcontrols').value.replace("cdWasMedGiven_Req", " ");
                    }

                }
            }
            document.getElementById("cdWasMedPresc_ctlRow").style.setAttribute('display', 'none');
            if (document.getElementById("cdWasMedPresc_Req") != null) {
                document.getElementById("cdWasMedPresc_Req").enabled = false;
                if (document.getElementById('requiredcontrols') != null) {
                    if (document.getElementById('requiredcontrols').value.indexOf("cdWasMedPresc_Req") != -1) {
                        document.getElementById('requiredcontrols').value = document.getElementById('requiredcontrols').value.replace("cdWasMedPresc_Req", " ");
                    }

                }
            }
            break;
    }
}

function CheckHiddenContrls() { 
    var objMedGridHideINC = null;
    var objMedGridHideNM = null;
    var objMedGridHideUSC = null;
    var objDisplayMedOnSurg = null;
    var objMedGridHideINCInit = null;
    var objMedGridHideNMInit = null;
    var objMedGridHideUSCInit = null;
    var objReporterType = null;
    var sMedBestEventId = "";
    var sMedEventCatPSOId = "";
    var sReporterType = "";
    var objLaborInduced = null;
    var sLaborInduced = "";
    if (document.getElementById('posted') != null) {
        document.getElementById('posted').value = "";
        if (document.getElementById('gridsubmit') != null) {
            document.getElementById('gridsubmit').value = "";
        }
    }

    //for displaying pnlhai on surgery category control and vice versa--neha goel
    var objHAIOnSurgINC = null;
    var objHAIOnSurgNM = null;
    var objSurgChar = null;
    var objSurgCharbtn = null;
    var objHAITyp = null;
    var objHAITypbtn = null;  
    objHAIOnSurgINC = eval("document.getElementById('psocdEventcategory_codelookup')");
    objHAIOnSurgNM = eval("document.getElementById('psocdEventcategoryNM_codelookup')");
    if ((objHAIOnSurgINC != null && objHAIOnSurgINC.value != "" && objHAIOnSurgINC.value != " " && objHAIOnSurgINC.value.split(" ")[0] == "A63") || (objHAIOnSurgNM != null && objHAIOnSurgNM.value != "" && objHAIOnSurgNM.value != " " && objHAIOnSurgNM.value.split(" ")[0] == "A63")) {
        objSurgChar = eval("document.getElementById('psocdCharacterizedSurgery_codelookup')");
        objSurgCharbtn = eval("document.getElementById('psocdCharacterizedSurgery_codelookupbtn')");
        if (objSurgChar != null) {
            objSurgChar.DisplayHideGroupName = "pnlHAI";
            objSurgChar.DisplayHideGroupID = "A2172";
        }
        if (objSurgCharbtn != null) {
            objSurgCharbtn.DisplayHideGroupName = "pnlHAI";
            objSurgCharbtn.DisplayHideGroupID = "A2172";
        }
        objHAITyp = eval("document.getElementById('psocdHAITypeReported_codelookup')");
        objHAITypbtn = eval("document.getElementById('psocdHAITypeReported_codelookupbtn')");
        if (objHAITyp != null) {
            objHAITyp.DisplayHideGroupName = "";
            objHAITyp.DisplayHideGroupID = "";
        }
        if (objHAITypbtn != null) {
            objHAITypbtn.DisplayHideGroupName = "";
            objHAITypbtn.DisplayHideGroupID = "";
        }
    }
    if (objHAIOnSurgINC != null && objHAIOnSurgINC.value != "" && objHAIOnSurgINC.value != " " && objHAIOnSurgINC.value.split(" ")[0] == "A51") {
        objSurgChar = eval("document.getElementById('psocdCharacterizedSurgery_codelookup')");
        objSurgCharbtn = eval("document.getElementById('psocdCharacterizedSurgery_codelookupbtn')");
        if (objSurgChar != null) {
            objSurgChar.DisplayHideGroupName = "";
            objSurgChar.DisplayHideGroupID = "";
        }
        if (objSurgCharbtn != null) {
            objSurgCharbtn.DisplayHideGroupName = "";
            objSurgCharbtn.DisplayHideGroupID = "";
        }
        objHAITyp = eval("document.getElementById('psocdHAITypeReported_codelookup')");
        objHAITypbtn = eval("document.getElementById('psocdHAITypeReported_codelookupbtn')");
        if (objHAITyp != null) {
            objHAITyp.DisplayHideGroupName = "pnlSurgery";
            objHAITyp.DisplayHideGroupID = "433202001";
        }
        if (objHAITypbtn != null) {
            objHAITypbtn.DisplayHideGroupName = "pnlSurgery";
            objHAITypbtn.DisplayHideGroupID = "433202001";
        }
        if (objHAITyp.value.split(" ")[0] == "433202001") {
            var sHiddenSurgPnl = document.getElementById('hiddencontrols').value;
            if (sHiddenSurgPnl.indexOf("pnlSurgery") != -1) {
                sHiddenSurgPnl = sHiddenSurgPnl.replace("pnlSurgery", " ");
                document.getElementById('hiddencontrols').value = sHiddenSurgPnl.replace("pnlSurgery_header", " ");
            }
        }
    }
    //end
    
    var sHiddenControl = document.getElementById('hiddencontrols').value;
    if (sHiddenControl != "") {
        var arrHiddenControls = sHiddenControl.split(",");
        var objCtl = null;
        var objReqCtl = null;
        var sCtlName = "";
        for (var iCount = 0; iCount < arrHiddenControls.length; iCount++) {
            objCtl = eval("window.document.getElementById('" + arrHiddenControls[iCount] + "')");
            if (objCtl != null) {
                //JIRA: RMA 7170
                //objCtl.style.setAttribute('display', 'none');
                objCtl.style.display = "none";
            }
//            if (arrHiddenControls[iCount].indexOf("_ctlRow") != -1) {
//                sCtlName = arrHiddenControls[iCount].split("_")[0];
//                objReqCtl = eval("document.getElementById('" + sCtlName + "_Req')");
//                if (objReqCtl != null) {
//                    objReqCtl.enabled = false;
//                }
//            }
        }
    }

    //to hide reporter controls when Y is seleted for anonymous reporter
    objReporterType = eval("document.getElementById('psocdIdentifyReporter_codelookup')");
    if (objReporterType != null && (objReporterType.value != "" && objReporterType.value != " ")) {
        sReporterType = objReporterType.value.split(" ")[0];
    }
    if (sReporterType == "Y") {
        var objRepName = null;
        var objRepPhnNo = null;
        var objRepEmail = null;
        var objRepJobPosition = null;
        objRepName = eval("document.getElementById('txtReporterName_ctlRow')");
        if (objRepName != null) {
            objRepName.style.display = "none";
            objRepName.style.visibility = "hidden";
        }
        objRepPhnNo = eval("document.getElementById('txtReporterPhoneNumber_ctlRow')");
        if (objRepPhnNo != null) {
            objRepPhnNo.style.display = "none";
            objRepPhnNo.style.visibility = "hidden";
        }
        objRepEmail = eval("document.getElementById('txtReporterEmail_ctlRow')");
        if (objRepEmail != null) {
            objRepEmail.style.display = "none";
            objRepEmail.style.visibility = "hidden";
        }
        objRepJobPosition = eval("document.getElementById('txtReporterJobPosition_ctlRow')");
        if (objRepJobPosition != null) {
            objRepJobPosition.style.display = "none";
            objRepJobPosition.style.visibility = "hidden";
        }
    }
    //end
    //to hide psocdLaborYes when Y is not seleted for psocdlaborinduced
    objLaborInduced = eval("document.getElementById('psocdLaborinduced_codelookup')");
    if (objLaborInduced != null && (objLaborInduced.value != "" && objLaborInduced.value != " ")) {
        sLaborInduced = objLaborInduced.value.split(" ")[0];
    }
    if (sLaborInduced != "A15") {  //rkaur27 : RMA-19199
        var objLaborYes = null;
        var objLbValue = null;
        var objLbId = null;
        objLaborYes = eval("document.getElementById('psocdLaborYes_ctlRow')");
        if (objLaborYes != null) {
            objLaborYes.style.display = "none";
            objLaborYes.style.visibility = "hidden";
            objLbValue = eval("document.getElementById('psocdLaborYes_codelookup')");
            objLbId = eval("document.getElementById('psocdLaborYes_codelookup_cid')");
            if (objLbValue != null) {
                objLbValue.value = "";
                objLbValue.defaultvalue = "";
            }
            if (objLbId != null) {
                objLbId.value = "0";
                objLbId.defaultvalue = "0";
            }
             var objReqobjLaborYes = eval("document.getElementById('psocdLaborYes_Req')");
             if (objReqobjLaborYes != null) {
                 objReqobjLaborYes.style.visibility = "hidden";
                 var sHiddenControl = document.getElementById('hiddencontrols').value;
                 var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                 if (sHiddenControl.indexOf("psocdLaborYes_ctlRow") == -1) {
                     document.getElementById('hiddencontrols').value = document.getElementById('hiddencontrols').value + "," + "psocdLaborYes_ctlRow";
                 }
                 if (sPnlHiddenControl.indexOf("psocdLaborYes") == -1) {
                     document.getElementById('pnlhiddencontrols').value = document.getElementById('pnlhiddencontrols').value + "," + "psocdLaborYes";
                 }
             }
        }        
    }
     //end

    //to hide the grids when A1263 is seleted in best event desc of medication
    objMedGridHideINC = eval("document.getElementById('psocdBestEventINC_codelookup')");
    objMedGridHideNM = eval("document.getElementById('psocdBestEventNM_codelookup')");
    objMedGridHideUSC = eval("document.getElementById('psocdBestEventUSC_codelookup')");
    if (objMedGridHideINC != null && (objMedGridHideINC.value != "" && objMedGridHideINC.value != " ")) {
        sMedBestEventId = objMedGridHideINC.value.split(" ")[0];
    }
    else if (objMedGridHideNM != null && (objMedGridHideNM.value != "" && objMedGridHideNM.value != " ")) {
        sMedBestEventId = objMedGridHideNM.value.split(" ")[0];
    }
    else if (objMedGridHideUSC != null && (objMedGridHideUSC.value != "" && objMedGridHideUSC.value != " ")) {
        sMedBestEventId = objMedGridHideUSC.value.split(" ")[0];
    }
    if (sMedBestEventId == "A1263" || sMedBestEventId == "UNK") {        
        var obctrlGridINC = null;
        var obctrlGridNM = null;
        var obctrlGridUSC = null;
        obctrlGridINC = eval("document.getElementById('INCMEDGrid_ctlRow')");
        if (obctrlGridINC != null) {
            obctrlGridINC.style.display = "none";
            obctrlGridINC.style.visibility = "hidden";
        }
        obctrlGridNM = eval("document.getElementById('NMMEDGrid_ctlRow')");
        if (obctrlGridNM != null) {
            obctrlGridNM.style.display = "none";
            obctrlGridNM.style.visibility = "hidden";
        }
        obctrlGridUSC = eval("document.getElementById('USCMEDGrid_ctlRow')");
        if (obctrlGridUSC != null) {
            obctrlGridUSC.style.display = "none";
            obctrlGridUSC.style.visibility = "hidden";
        }
    }
    //end grid hide

    //neha grid
    //to hide the grids when medication is seleted intially
    objMedGridHideINCInit = eval("document.getElementById('psocdEventcategory_codelookup')");
    objMedGridHideNMInit = eval("document.getElementById('psocdEventcategoryNM_codelookup')");
    objMedGridHideUSCInit = eval("document.getElementById('psocdEventcategoryUSC_codelookup')");
    if (objMedGridHideINCInit != null && objMedGridHideINCInit.value != "" && objMedGridHideINCInit.value != " ") {
        sMedEventCatPSOId = objMedGridHideINCInit.value.split(" ")[0];
    }
    else if (objMedGridHideNMInit != null && objMedGridHideNMInit.value != "" && objMedGridHideNMInit.value != " ") {
        sMedEventCatPSOId = objMedGridHideNMInit.value.split(" ")[0];
    }
    else if (objMedGridHideUSCInit != null && objMedGridHideUSCInit.value != "" && objMedGridHideUSCInit.value != " ") {
        sMedEventCatPSOId = objMedGridHideUSCInit.value.split(" ")[0];
    }
    if (sMedEventCatPSOId == "A54") {
        var objINCMEDGrid = null;
        var objNMMEDGrid = null;
        var objUSCMEDGrid = null;
        var objINCMedSubTypInit = null;
        var objNMMedSubTypInit = null;
        var objUSCMedSubTypInit = null;
        var objINCMedType = null;
        var objNMMedType = null;
        var objUSCCMedType = null;
        var sINCMedType = null;
        var sNMMedType = null;
        var sUSCMedType = null;
        objINCMedSubTypInit = eval("document.getElementById('psocdSubstanceTypeINC_codelookup')");
        objNMMedSubTypInit = eval("document.getElementById('psocdSubstanceTypeNM_codelookup')");
        objUSCMedSubTypInit = eval("document.getElementById('psocdSubstanceTypeUSC_codelookup')"); 
        objINCMEDGrid = eval("document.getElementById('INCMEDGrid_ctlRow')");
        if (objINCMEDGrid != null) {
            if(objINCMedSubTypInit != null && (objINCMedSubTypInit.value == " " || objINCMedSubTypInit.value == "")){
                objINCMEDGrid.style.display = "none";
                objINCMEDGrid.style.visibility = "hidden"; 
            }
        }
        objINCMedType = eval("document.getElementById('psocdMedicationTypeINC_codelookup')");
        if (objINCMedType != null && objINCMedType.value != "") {
            sINCMedType = objINCMedType.value;
            if (sINCMedType.indexOf("A1221") != -1) {
                objINCMEDGrid.style.display = "none";
                objINCMEDGrid.style.visibility = "hidden";
            }
        }
        objNMMEDGrid = eval("document.getElementById('NMMEDGrid_ctlRow')");
        if (objNMMEDGrid != null) {
            if (objNMMedSubTypInit != null && (objNMMedSubTypInit.value == " " || objNMMedSubTypInit.value == "")) {
                objNMMEDGrid.style.display = "none";
                objNMMEDGrid.style.visibility = "hidden"; 
            }
        }
        objNMMedType = eval("document.getElementById('psocdMedicationTypeNM_codelookup')");
        if (objNMMedType != null && objNMMedType.value != "") {
            sNMMedType = objNMMedType.value;
            if (sNMMedType.indexOf("A1221") != -1) {
                objNMMEDGrid.style.display = "none";
                objNMMEDGrid.style.visibility = "hidden";
            }
        }
        objUSCMEDGrid = eval("document.getElementById('USCMEDGrid_ctlRow')");
        if (objUSCMEDGrid != null) {
            if (objUSCMedSubTypInit != null && (objUSCMedSubTypInit.value == " " || objUSCMedSubTypInit.value == "")) {
                objUSCMEDGrid.style.display = "none";
                objUSCMEDGrid.style.visibility = "hidden"; 
            }
        }
        objUSCCMedType = eval("document.getElementById('psocdMedicationTypeUSC_codelookup')");
        if (objUSCCMedType != null && objUSCCMedType.value != "") {
            sUSCMedType = objUSCCMedType.value;
            if (sUSCMedType.indexOf("A1221") != -1) {
                objUSCMEDGrid.style.display = "none";
                objUSCMEDGrid.style.visibility = "hidden";
            }
        }
	                      
    }

    //to  display the medication control on the selection of surgery category 
    var objDisplayMedOnSurg = null;
    var sDisplayMedOnSurg = "";
    objDisplayMedOnSurg = eval("document.getElementById('psocdEventAnesthesia_codelookup')");
    if (objDisplayMedOnSurg != null && objDisplayMedOnSurg.value != "") {
        sDisplayMedOnSurg = objDisplayMedOnSurg.value.split(" ")[0];
    }
    if (sDisplayMedOnSurg == "A2250") {
        var objINCMedSubTyp = null;
        var objNMMedSubTyp = null;
        var objUSCMedSubTyp = null;
        var objWhatReported = null;
        var objReqINCMedSubTyp = null;
        var objReqNMMedSubTyp = null;
        var objReqUSCMedSubTyp = null;
        sWhatReported = "";
        objWhatReported = eval("document.getElementById('psocdWhatReported_codelookup')");
        if (objWhatReported != null && objWhatReported.value != "") {
            sWhatReported = objWhatReported.value;
            if (sWhatReported.indexOf("A3") != -1) {
                objINCMedSubTyp = eval("document.getElementById('psocdSubstanceTypeINC_ctlRow')");
                if (objINCMedSubTyp != null) {
                    objINCMedSubTyp.style.display = ""; //RMA-12071
                    objINCMedSubTyp.style.visibility = "visible";
                }
                objReqINCMedSubTyp = eval("document.getElementById('psocdSubstanceTypeINC_Req')");
                if (objReqINCMedSubTyp != null) {
                    objReqINCMedSubTyp.style.visibility = "hidden";                    
                    var sHiddenControl = document.getElementById('hiddencontrols').value;
                    var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                    if (sHiddenControl.indexOf("psocdSubstanceTypeINC_ctlRow") != -1) {
                        document.getElementById('hiddencontrols').value = sHiddenControl.replace("psocdSubstanceTypeINC_ctlRow", " ");
                    }
                    if (sPnlHiddenControl.indexOf("psocdSubstanceTypeINC_ctlRow") != -1) {
                        document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace("psocdSubstanceTypeINC_ctlRow", " ");
                    }
                }
            }
            else if (sWhatReported.indexOf("A6") != -1) {
                objNMMedSubTyp = eval("document.getElementById('psocdSubstanceTypeNM_ctlRow')");
                if (objNMMedSubTyp != null) {
                    objNMMedSubTyp.style.display = ""; //RMA-12071
                    objNMMedSubTyp.style.visibility = "visible";
                }
                objReqNMMedSubTyp = eval("document.getElementById('psocdSubstanceTypeNM_Req')");
                if (objReqNMMedSubTyp != null) {
                    objReqNMMedSubTyp.style.visibility = "hidden";                    
                    var sHiddenControl = document.getElementById('hiddencontrols').value;
                    var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                    if (sHiddenControl.indexOf("psocdSubstanceTypeNM_ctlRow") != -1) {
                        document.getElementById('hiddencontrols').value = sHiddenControl.replace("psocdSubstanceTypeNM_ctlRow", " ");
                    }
                    if (sPnlHiddenControl.indexOf("psocdSubstanceTypeNM_ctlRow") != -1) {
                        document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace("psocdSubstanceTypeNM_ctlRow", " ");
                    }
                }
            }
            else if (sWhatReported.indexOf("A9") != -1) {
                objUSCMedSubTyp = eval("document.getElementById('psocdSubstanceTypeUSC_ctlRow')");
                if (objUSCMedSubTyp != null) {
                    objUSCMedSubTyp.style.display = ""; //RMA-12071
                    objUSCMedSubTyp.style.visibility = "visible";
                }
                objReqUSCMedSubTyp = eval("document.getElementById('psocdSubstanceTypeUSC_Req')");
                if (objReqUSCMedSubTyp != null) {
                    objReqUSCMedSubTyp.style.visibility = "hidden";                    
                    var sHiddenControl = document.getElementById('hiddencontrols').value;
                    var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
                    if (sHiddenControl.indexOf("psocdSubstanceTypeUSC_ctlRow") != -1) {
                        document.getElementById('hiddencontrols').value = sHiddenControl.replace("psocdSubstanceTypeUSC_ctlRow", " ");
                    }
                    if (sPnlHiddenControl.indexOf("psocdSubstanceTypeNM_ctlRow") != -1) {
                        document.getElementById('pnlhiddencontrols').value = sPnlHiddenControl.replace("psocdSubstanceTypeUSC_ctlRow", " ");
                    }
                }
            }
            var obctrlGridINC = null;
            var obctrlGridNM = null;
            var obctrlGridUSC = null;
            obctrlGridINC = eval("document.getElementById('INCMEDGrid_ctlRow')");
            if (obctrlGridINC != null) {
                obctrlGridINC.style.display = "none";
                obctrlGridINC.style.visibility = "hidden";
            }
            obctrlGridNM = eval("document.getElementById('NMMEDGrid_ctlRow')");
            if (obctrlGridNM != null) {
                obctrlGridNM.style.display = "none";
                obctrlGridNM.style.visibility = "hidden";
            }
            obctrlGridUSC = eval("document.getElementById('USCMEDGrid_ctlRow')");
            if (obctrlGridUSC != null) {
                obctrlGridUSC.style.display = "none";
                obctrlGridUSC.style.visibility = "hidden";
            }

        }
    }             
    //end
}
function fnPSOCancel() {
    window.close();
}

function fnPSODataOk() {    
    if (window.opener.document.getElementById("EvtPsoRowId") != null) {
        window.opener.document.getElementById("EvtPsoRowId").value = document.getElementById("EvtPsoRowId").value;
    }
    if (window.opener.document.getElementById("PatPsoRowId") != null) {
        window.opener.document.getElementById("PatPsoRowId").value = document.getElementById("PatPsoRowId").value;
    }
    if (document.getElementById('saveclicked') != null) {
        var sSaveClicked = document.getElementById('saveclicked').value;
        if (sSaveClicked == "true") {
            document.getElementById('saveclicked').value = "";
        }
    }
    window.close();
}
function fnPSOOkClicked() {
    if (Page_IsValid) {
        var objCtl = document.getElementById("txtFunctionToCall");
        if (objCtl != null) {
            objCtl.value = "PSOFormAdaptor.SavePSOData";
        }
        
        if (document.getElementById('saveclicked') != null) {
            var sSaveClicked = document.getElementById('saveclicked').value;
            if (sSaveClicked == "") {
                sSaveClicked = "true";
                document.getElementById('saveclicked').value = sSaveClicked;
            }
            else if (sSaveClicked == "true") {
                return false;
            }
        }
        if (document.getElementById('posted') != null) {
            document.getElementById('posted').value = "true";
        }
        pleaseWait.Show();
    }
    else {
        return false;
    }
    //end grid check
}

function confirmSubmit() {
    

    var sReqControl = document.getElementById('requiredcontrols').value;
    var sHiddenControl = document.getElementById('hiddencontrols').value;
    var sPnlHiddenControl = document.getElementById('pnlhiddencontrols').value;
    var sValidatorName = "";
    var sPnlValidator = "";
    if (Page_Validators != undefined && Page_Validators != null) {
        for (var iValCount = 0; iValCount < Page_Validators.length; iValCount++) {
            if (sReqControl.indexOf(Page_Validators[iValCount].id) != -1) {
                sValidatorName = Page_Validators[iValCount].id.split("_")[0] + "_ctlRow";
                sPnlValidator = Page_Validators[iValCount].id.split("_")[0];      
                sValidatorName = "\\b" + sValidatorName.replace(" ", "\\b \\b") + "\\b";
                sPnlValidator = "\\b" + sPnlValidator.replace(" ", "\\b \\b") + "\\b";
                if (sHiddenControl.toLowerCase().match(sValidatorName.toLowerCase()) == null && sPnlHiddenControl.toLowerCase().match(sPnlValidator.toLowerCase()) == null) {
                    ValidatorEnable(Page_Validators[iValCount], true);
                }
                else {
                    ValidatorEnable(Page_Validators[iValCount], false);
                }               
            }
        }
    }
    Page_ClientValidate();
}

function CopyDateToValidate(sCtrlName) {    
    var ctrlValidate = eval("document.getElementById('" + sCtrlName + "_ValidateDate')");
    var ctrlDateControl = eval("document.getElementById('" + sCtrlName + "')");
    if (ctrlValidate != null) {
        if (ctrlDateControl != null) {
            ctrlValidate.value = ctrlDateControl.value;
        }
    }
}

function fnDelPatRowId() {
    if (window.opener.document.getElementById("PatPsoRowId") != null) {
        window.opener.document.getElementById("PatPsoRowId").value = "";
    }
}
