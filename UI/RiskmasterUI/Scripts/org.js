var m_CurrentFuncId = '';
var m_CurrentEntityName = '';
var m_nodeDepth = 0;

function getOrg(xid, xname, level, parent,parentIds) {
    var tmpctname;
	var ctname;
	var ctid;
	var vName;
	var orgHierarchy;
	
	//tkr mits 10267  pass back pipe-delimited string of entire org hierarchy from selected level up to client
	//each array element is [level];[id]  
	//level 1 is client, level 8 is department
	if(level != null && parseInt(level) > 0)
	{
	    level = parseInt(level);
	    var parentId = parent;
	    orgHierarchy = new String(level.toString() + ';' + xid.toString() + '|');
	    var ctl = null;
	    for(var i = level - 1; i >=1; i--)
	    {
	    	ctl = null;
	    	if( parentId.length > 0 )
	        	ctl = document.getElementById(parentId.toString());
	        if(ctl != null)
	        {
	            orgHierarchy+= i.toString() + ';' + parentId.toString() + '|';
	            parentId = ctl.parent;
	        }
	        else
	            break;
	    }
	}
	vName = replaceSubstring(xname, "'", "''");
	//abansal23 on 05/13/2009 MITS 14847 starts
	var PIds = new Array();
	var sParentText;
	if (document.getElementById(parentIds) != null)
	{
	    sParentText = document.getElementById(parentIds).value;
	    if (!(sParentText.indexOf("//") == -1)) {
	        PIds = sParentText.split("//");
	        if (PIds.length == 8) {
	            for (i = 0; i < PIds.length - 1; i++) {
	                switch (i) {
	                    case 0:
	                        if (self.parent.opener.document.forms[0].OH_FACILITY_EID != null) {
	                            self.parent.opener.document.forms[0].OH_FACILITY_EID.value = PIds[i];
	                        }
	                        break;
	                    case 1:
	                        if (self.parent.opener.document.forms[0].OH_LOCATION_EID != null) {
	                            self.parent.opener.document.forms[0].OH_LOCATION_EID.value = PIds[i];
	                        }
	                        break;
	                    case 2:
	                        if (self.parent.opener.document.forms[0].OH_DIVISION_EID != null) {
	                            self.parent.opener.document.forms[0].OH_DIVISION_EID.value = PIds[i];
	                        }
	                        break;
	                    case 3:
	                        if (self.parent.opener.document.forms[0].OH_REGION_EID != null) {
	                            self.parent.opener.document.forms[0].OH_REGION_EID.value = PIds[i];
	                        }
	                        break;
	                    case 4:
	                        if (self.parent.opener.document.forms[0].OH_OPERATION_EID != null) {
	                            self.parent.opener.document.forms[0].OH_OPERATION_EID.value = PIds[i];
	                        }
	                        break;
	                    case 5:
	                        if (self.parent.opener.document.forms[0].OH_COMPANY_EID != null) {
	                            self.parent.opener.document.forms[0].OH_COMPANY_EID.value = PIds[i];
	                        }
	                        break;
	                    case 6:
	                        if (self.parent.opener.document.forms[0].OH_CLIENT_EID != null) {
	                            self.parent.opener.document.forms[0].OH_CLIENT_EID.value = PIds[i];
	                        }
	                        break;

	                }
	            }

	        }
	    }
	}
	//abansal23 on 05/13/2009 MITS 14847 ends

	if(xid!='0' && xid!='')
	{
		//Raman MITS 14898: This special if condition is not needed now
		/*
		if(self.parent.opener.name=='frm_Code_Detail')
		{
			tmpctname=self.parent.opener.m_ctname;
			ctname='self.parent.opener.forms[0].' + tmpctname;
			ctid='self.parent.opener.forms[0].' + tmpctname + '_cid';
			ctname=eval(ctname);
			ctid=eval(ctid);
			ctname.value=vName;
			ctid.value=xid;
		}
		else
		{
		*/
			if(self.parent.opener.document.codeSelected==null)
				self.parent.opener.codeSelected(vName,xid,null,orgHierarchy);
			else
				self.parent.opener.document.codeSelected(vName,xid,null,orgHierarchy);
		//}
		self.parent.close();
	}
}
// Goes through the inputString and replaces every occurrence of fromString with toString
function replaceSubstring(inputString, fromString, toString) {
  
   var temp = inputString;
   
   if (fromString == "")
      return inputString;

	// If the string being replaced is not a part of the replacement string (normal situation)
	if (toString.indexOf(fromString) == -1)
	{
      while (temp.indexOf(fromString) != -1) 
      {
         var toTheLeft = temp.substring(0, temp.indexOf(fromString));
         var toTheRight = temp.substring(temp.indexOf(fromString)+fromString.length, temp.length);
         temp = toTheLeft + toString + toTheRight;
      }
	} 
	// String being replaced is part of replacement string (like "+" being replaced with "++") - prevent an infinite loop
	else 
	{ 
      var midStrings = new Array("~", "`", "_", "^", "#");
      var midStringLen = 1;
      var midString = "";

      // Find a string that doesn't exist in the inputString to be used
      // as an "inbetween" string
      while (midString == "") 
      {
         for (var i=0; i < midStrings.length; i++) 
         {
            var tempMidString = "";
            for (var j=0; j < midStringLen; j++) 
            { 
				tempMidString += midStrings[i]; 
			}
            if (fromString.indexOf(tempMidString) == -1) 
            {
               midString = tempMidString;
               i = midStrings.length + 1;
            }
         }
      } 
      // Keep on going until we build an "inbetween" string that doesn't exist
      // Now go through and do two replaces - first, replace the "fromString" with the "inbetween" string
      while (temp.indexOf(fromString) != -1) 
      {
         var toTheLeft = temp.substring(0, temp.indexOf(fromString));
         var toTheRight = temp.substring(temp.indexOf(fromString)+fromString.length, temp.length);
         temp = toTheLeft + midString + toTheRight;
      }
      // Next, replace the "inbetween" string with the "toString"
      while (temp.indexOf(midString) != -1) 
      {
         var toTheLeft = temp.substring(0, temp.indexOf(midString));
         var toTheRight = temp.substring(temp.indexOf(midString)+midString.length, temp.length);
         temp = toTheLeft + toString + toTheRight;
      }
   } 
   // Ends the check to see if the string being replaced is part of the replacement string or not
   return temp; // Send the updated string back to the user

}
//MITS 13997 : Umesh
function getparentorg() {

    var entitytableid = parseInt(document.forms[0].entitytableid.value, 10)
    m_Wnd = window.open('../OrganisationHierarchy/FlatParentOrgSearch.aspx?entitytableid=' + entitytableid, 'flatparentorgsearch',
				'scrollbars=yes,resizable=yes,width=640,height=350' + ',top=' + (screen.availHeight - 350) / 2 + ',left=' + (screen.availWidth - 640) / 2);
    return false;
}
//MITS 13997 : End
function UpdateSearchCriteria() {
    document.getElementById("OT_hd_slevel").value = document.getElementById("OrgSearch_cmb_slevel").value;
    document.getElementById("OT_hd_txtsorg").value = document.getElementById("OrgSearch_txtsorg").value;
    //MITS 16771- Pankaj 5/29/09
    document.getElementById("OT_hd_txtcity").value = document.getElementById("OrgSearch_txtcity").value;
    document.getElementById("OT_hd_txtstate").value = document.getElementById("OrgSearch_txtstate_codelookup_cid").value;
    document.getElementById("OT_hd_txtzip").value = document.getElementById("OrgSearch_txtzip").value;
    pleaseWait.Show();
}
function printOrg() {

    //Added for MITS:16164-Wrong records coming in pdf of Org Hierarchy:Filter by Effective Date not working
    var sChkFilter = "0";
   
     if (document.getElementById('OT_chkFilter') != null) {
         if (document.getElementById('OT_chkFilter').checked) {
             sChkFilter = "1";
         }
     }
     //Added for MITS:16164-Wrong records coming in pdf of Org Hierarchy:Filter by Effective Date not working
     //mbahl3 Mit:25694 org hierarchy Print out .start
     window.open("../OrganisationHierarchy/PrintOrgHierarchy.aspx?chkFilter=" + sChkFilter + "&sLevel=" + document.getElementById("OrgSearch_cmb_slevel").value
                        , 'PrintOrgHierarchy',
                        'width=400,height=200' + ',top='
                        + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=yes,scrollbars=yes'); //mbahl3 Mit:25694 org hierarchy Print out end

    return false;
}
function LoadListPage() {
    var sLevel = GetEntityLevel(document.getElementById("OrgSearch_cmb_slevel").value);
    var sLOB = document.getElementById("OT_hd_lob").value;

    var sSearchText = document.getElementById("OrgSearch_txtsorg").value;
    //MITS 16771- Pankaj 5/29/09
    var sCityText = document.getElementById("OrgSearch_txtcity").value;
    var sStateId = document.getElementById("OrgSearch_txtstate_codelookup_cid").value;
    var sStateText = document.getElementById("OrgSearch_txtstate_codelookup").value;
    var sZipText = document.getElementById("OrgSearch_txtzip").value;
    sSearchText = sSearchText.replace("&", "%26");
    sCityText = sCityText.replace("&", "%26");
    sZipText = sZipText.replace("&", "%26");
    window.location.href = "/RiskmasterUI/UI/OrganisationHierarchy/OrgHierarchyLookupList.aspx?EntityId=" + sLevel + "&Lookup=" + escape(sSearchText) + "&City=" + sCityText + "&State=" + sStateId + "&StateText=" + sStateText + "&Zip=" + sZipText + "&Lob=" + sLOB + "&MaintainanceScreenFlag=true";
    // ijha MITS 28258: Add escape to the sSearchText
    pleaseWait.Show();
    return false;
}
function GetEntityLevel(sCode) {
    var entityTable = 1012;
    switch (sCode) {
        case 'C': entityTable = 1005;
            break;
        case 'CO': entityTable = 1006;
            break;
        case 'O': entityTable = 1007;
            break;
        case 'R': entityTable = 1008;
            break;
        case 'D': entityTable = 1009;
            break;
        case 'L': entityTable = 1010;
            break;
        case 'F': entityTable = 1011;
            break;
        case 'DT': entityTable = 1012;
            break;
        default: entityTable = 1012;
    }
    return entityTable;
}
function GetLevel(sCode) {
    var orgLevel = 8;
    switch (sCode) {
        case 'C': orgLevel = 1;
            break;
        case 'CO': orgLevel = 2;
            break;
        case 'O': orgLevel = 3;
            break;
        case 'R': orgLevel = 4;
            break;
        case 'D': orgLevel = 5;
            break;
        case 'L': orgLevel = 6;
            break;
        case 'F': orgLevel = 7;
            break;
        case 'DT': orgLevel = 8;
            break;
        default: orgLevel = 8;
    }
    return orgLevel;
}
function ValidateCloning() {
    
    if (m_CurrentFuncId == "") {
        alert("Please select a level for which cloning has to be performed.");
        return false;
    }
    if (document.getElementById('cloninglevelpreference1').checked == true) {
        if ((document.getElementById("OT_hd_selectednodevaluelevel").value || document.getElementById("OT_hd_selectednodevaluelevel").textContent) == 8) { //igupta3 Mits# 33301
            alert('Proper Level need to be selected in Org Hierarchy tree view for Cloning');
            return false;
        }
    }
    else {
        if (GetLevel(document.getElementById("cmb_desiredcloninglevel").value) <= (document.getElementById("OT_hd_selectednodevaluelevel").value|| document.getElementById("OT_hd_selectednodevaluelevel").textContent)) { //igupta3 Mits# 33301
            alert('Proper Level need to be selected in Org Hierarchy tree view for Cloning');
            return false;
        }
    }
    pleaseWait.Show();
    return true;
}
function UpdateParent(nodeText, nodeValue, currentnodeDepth) {
    //Function Not needed

}
function checkedit() {
    if (m_CurrentFuncId == "") {
        alert("Please select a node to edit.");
        return false;
    }

    var sValue = "";
    var sValue1 = "";
    var sValue3 = "";
    switch (m_nodeDepth) {
        case 1:
            sValue = 'Client';
            sValue1 = 'Client Info';
            sValue3 = '1005';
            break;
        case 2:
            sValue = 'Company';
            sValue1 = 'Company Info';
            sValue3 = '1006';
            break;
        case 3:
            sValue = 'Operation';
            sValue1 = 'Operation Info';
            sValue3 = '1007';
            break;
        case 4:
            sValue = 'Region';
            sValue1 = 'Region Info';
            sValue3 = '1008';
            break;
        case 5:
            sValue = 'Division';
            sValue1 = 'Division Info';
            sValue3 = '1009';
            break;
        case 6:
            sValue = 'Location';
            sValue1 = 'Location Info';
            sValue3 = '1010';
            break;
        case 7:
            sValue = 'Facility';
            sValue1 = 'Facility Info';
            sValue3 = '1011';
            break;
        case 8:
            sValue = 'Department';
            sValue1 = 'Department Info';
            sValue3 = '1012';
            break;
        default:
            sValue = 'Department';
            sValue1 = 'Department Info';
            sValue3 = '1012';
    }

    //MITS 24647 : Raman Bhatia : changed on 3/1/2012
    //We need to open Edit screen on a new MDI window

    var del = parent.unitSeparator;

		//mbahl3 mits 34199 reverted back 
    //    parent.MDIShowScreen(m_CurrentFuncId, "orghierarchymaint" + del + "" + del + "UI/FDM/orghierarchymaint" + ".aspx" + del + "?recordID=(NODERECORDID)&action=Edit&entitylevel=" + sValue3); //Amandeep MITS 28756
    parent.MDIShowScreen(m_CurrentFuncId, "OrgHierarchyRecord" + del + "" + del + "UI/FDM/orghierarchymaint" + ".aspx" + del + "?recordID=(NODERECORDID)&action=Edit&entitylevel=" + sValue3); //Amandeep MITS 28756

    //var sURL = "../FDM/orghierarchymaint.aspx?recordID=" + m_CurrentFuncId + "&action=Edit&entitylevel=" + sValue3;
    //window.location = sURL;
    //pleaseWait.Show();
    return false;
}
function checknew() {

    if (document.getElementById('OT_orgcaption').innerHTML == 'Department') {
        return false;
    }
    else {
        if (m_CurrentFuncId == "") {
            var sURL = "../FDM/orghierarchymaint.aspx?recordID=0&action=New";
            window.location = sURL;
            pleaseWait.Show();
            return false;
        }
        else {
            var sValue3 = "";

            switch (m_nodeDepth) {
                case 1:
                    sValue3 = '1006';
                    break;
                case 2:
                    sValue3 = '1007';
                    break;
                case 3:
                    sValue3 = '1008';
                    break;
                case 4:
                    sValue3 = '1009';
                    break;
                case 5:
                    sValue3 = '1010';
                    break;
                case 6:
                    sValue3 = '1011';
                    break;
                case 7:
                    sValue3 = '1012';
                    break;
                default:
                    sValue3 = '1005';
            }

            // npadhy MITS 17678 If there is & in the Querystring passed from the Javascript, then we repace the &
            // by the Character pair of "^@" in javascript and while retrieving the value from Querystring, we replace
            // the Character pair of "^@" by &
            var sProcessedEntityName = replaceSubstring(m_CurrentEntityName, "&", "^@");

            //MITS 24647 : Raman Bhatia : changed on 3/1/2012
            //We need to open Edit screen on a new MDI window

            var del = parent.unitSeparator;

          //mbahl3 mits 34199 reverted back
           parent.MDIShowScreen(m_CurrentFuncId, "OrgHierarchyRecord" + del + "" + del + "UI/FDM/orghierarchymaint" + ".aspx" + del + "?recordID=0&entitylevel=" + sValue3 + "&parentname=" + sProcessedEntityName + "&action=New&parenteid=" + m_CurrentFuncId);
           // parent.MDIShowScreen(m_CurrentFuncId, "orghierarchymaint" + del + "" + del + "UI/FDM/orghierarchymaint" + ".aspx" + del + "?recordID=0&entitylevel=" + sValue3 + "&parentname=" + sProcessedEntityName + "&action=New&parenteid=" + m_CurrentFuncId);
	    //mbahl3 mits 34199
           // var sURL = "../FDM/orghierarchymaint.aspx?recordID=0&parenteid=" + m_CurrentFuncId + "&action=New&entitylevel=" + sValue3 + "&parentname=" + sProcessedEntityName;
          //  window.location = sURL;
           // pleaseWait.Show();
            return false;

        }
    }

}
function scrollNodeIntoView() {

    try {
        var name = OT_OTV_Data.selectedNodeID.value;
        var newname = name;

        if (name != "") {
            var iPos = parseInt(name.substring(7));
            if (iPos > 100) {
                iPos = iPos + 5;
                newname = "OT_OTVt" + iPos;
            }
            else if (iPos > 50) {
                iPos = iPos + 5;
                newname = "OT_OTVt" + iPos;
            }
            else if (iPos > 10) {
                iPos = iPos + 4;
                newname = "OT_OTVt" + iPos;
            }
            var selectedNode = document.getElementById(newname);
            if (selectedNode != null) {
                selectedNode.scrollIntoView(false);
            }
            else {
                selectedNode = document.getElementById(name);
                if (selectedNode != null) {
                    selectedNode.scrollIntoView(false);
                }
            }

        }
        if (document.getElementById("OrgSearch_chkTreeNode") != null)
            document.getElementById("OrgSearch_chkTreeNode").checked = false;

        if (parent.MDISetUnDirty != null) {
            parent.MDISetUnDirty(0);
        }
    }
    catch (e) { }
}

function OnTreeClick(evt) {
    var src = window.event != window.undefined ? window.event.srcElement : evt.target;
    var nodeClick = src.tagName.toLowerCase() == 'a';
    if (nodeClick) {
        var nodeText = src.innerHTML || src.textContent; 
        m_CurrentEntityName = nodeText;
        var nodeValue = GetNodeValue(src);
        UpdateParent(nodeText, nodeValue, m_nodeDepth); return false;
    }
    return true;
}

function GetNodeValue(node) {
    var nodeValue = "";
    var nodePath = node.href.substring(node.href.indexOf(",") + 2, node.href.length - 2);
    if (window.opener) {
        var oGrpAssocOrgTree = window.opener.document.getElementById('GrpAssocOrgTree');
        if (oGrpAssocOrgTree != null) {
            oGrpAssocOrgTree.value = nodePath.substring(nodePath.indexOf("\\") + 2);
        }
    }
    //abansal23 on 05/13/2009 MITS 14847 Starts
    var nodeValues = nodePath.split("\\");
    if (nodeValues.length == 17 && document.getElementById('hdSelectedNodeHierarchyString') != null) {
        document.getElementById('hdSelectedNodeHierarchyString').value = nodeValues[14] + "//" + nodeValues[12] + "//" + nodeValues[10] + "//" + nodeValues[8] + "//" + nodeValues[6] + "//" + nodeValues[4] + "//" + nodeValues[2] + "//"; 
    }
    //abansal23 on 05/13/2009 MITS 14847 Ends
    if (nodeValues.length > 1)
    { nodeValue = nodeValues[nodeValues.length - 1]; m_nodeDepth = (nodeValues.length - 1) / 2; }
    else
    { nodeValue = nodeValues[0].substr(1); m_nodeDepth = 0; }
    var TreeViewID = node.href.substring(node.href.indexOf("'") + 1, node.href.indexOf("$"));
    var oOrgcaptionID = TreeViewID + '_orgcaption';
    var oOrgcaption = document.getElementById(oOrgcaptionID); if (oOrgcaption != null)
    {
        //Bharani - MITS : 36993 - Start
        //oOrgcaption.value = GetOrgLevel(m_nodeDepth);// igupta3 Mits:33301
        //oOrgcaption.innerText = GetOrgLevel(m_nodeDepth);
        if (navigator.appName == 'Microsoft Internet Explorer')
            oOrgcaption.innerText = GetOrgLevel(m_nodeDepth);
        else
            oOrgcaption.textContent = GetOrgLevel(m_nodeDepth);
        //Bharani - MITS : 36993 - End
    }
    var oOrgSelectedID = TreeViewID + '_hd_selectednodevalue';
    var oOrgSelected = document.getElementById(oOrgSelectedID);
    if (oOrgSelected != null)
    {
        oOrgSelected.value = nodeValue;// igupta3 Mits:33301
           // oOrgSelected.innerText = nodeValue;        
    }
    m_CurrentFuncId = nodeValue;   //mbahl3 Mits 34649 reverted back to old code 
    var oOrgSelectedIDlevel = TreeViewID + '_hd_selectednodevaluelevel'; 
    var oOrgSelectedlevel = document.getElementById(oOrgSelectedIDlevel);
    if (oOrgSelectedlevel != null)
    {
        oOrgSelectedlevel.value = m_nodeDepth;// igupta3 Mits:33301
        //oOrgSelectedlevel.innerText = m_nodeDepth;
    }  //mbahl3 Mits 34649 reverted back to old code
    return nodeValue;
}
function GetOrgLevel(iLevel) {
    var Title = ''; switch (iLevel) {
        case 1: Title = "Client";
            break;
        case 2: Title = "Company";
            break;
        case 3: Title = "Operation";
            break;
        case 4: Title = "Region";
            break;
        case 5: Title = "Division";
            break;
        case 6: Title = "Location";
            break;
        case 7: Title = "Facility";
            break;
        case 8: Title = "Department";
            break;


    }
    return Title;
}
function onChanged(ctname) {
    if (m_CurrentFuncId == '') {
        alert('select branch');
        if (document.getElementById('OrgSearch_chkTreeNode') != null && document.getElementById('OT_chkTreeNode') != null)
            document.getElementById('OT_chkTreeNode').checked = document.getElementById("OrgSearch_" + ctname).checked;
        return false;
    }
    if (document.getElementById('OrgSearch_chkTreeNode') != null && document.getElementById('OT_chkTreeNode') != null)
        document.getElementById('OT_chkTreeNode').checked = document.getElementById("OrgSearch_" + ctname).checked;
    return true;
}

function LoadAdvSearch() {//MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
    var sLevel= document.getElementById("OrgSearch_cmb_slevel").value;
    if (isNaN(parseInt(sLevel)))
        sLevel = GetEntityLevel(sLevel).toString();
    return lookupData('OrgAdvSrch',sLevel,4,'OrgAdvSrch',2);
}

//Yatharth: R7 Time Zone Enhancement

function EnableDisableTimeZoneControls() {

    if (document.getElementById('timezonetracking') != null) {

        if (document.getElementById('timezonetracking').checked == false) {
            document.getElementById('timezonecode_codelookup').disabled = true;
            document.getElementById('timezonecode_codelookup').value = "";
            document.getElementById('timezonecode_codelookupbtn').disabled = true;
            //document.getElementById('daylightsavings').checked = false;
            //document.getElementById('daylightsavings').disabled = true;
            //document.getElementById('timezonetracking').disabled = true;
            document.getElementById("lbl_timezonecode").className = 'label';
        }
        else {
            if (document.getElementById('timezonecode_codelookup').getAttribute('powerviewreadonly') != "true") {
                document.getElementById('timezonecode_codelookup').disabled = false;
                document.getElementById('timezonecode_codelookupbtn').disabled = false;
                //document.getElementById('daylightsavings').disabled = false;
                // document.getElementById('timezonetracking').disabled = false;
            }
            document.getElementById("lbl_timezonecode").className = 'required';
        }
    }
    return false;
}
