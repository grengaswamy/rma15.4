var DEFAULT_SELECT_WIDTH=162;
var objWord;
var	fso;
var RiskmasterSL; // RMA-8407 : MITS-36022 : Mudabbir 
var strForm,sPrefab,strFile,lDoc,lDocType;

var _mergeFailureErrorMessage = "<h3>Mail Merge Failure Information</h3><br/>If you cannot resolve the problem using this information please call technical support."
+ "<br/><br/><hr/>To complete the merge you must have these Internet Explorer security settings:"
+ "<ul><li>In Internet Explorer click 'Tools' - 'Internet Options' - 'Security' - 'Local Intranet' - 'Sites' - 'Advanced'</li>"
+ "<li>If you are using Windows Vista, and your RMX domain is reached via an Intranet, you must uncheck 'Automatically detect intranet network"
+ "<li>Also on the 'Security' panel, with 'Intranet' selected, click 'Advanced'</li>"
+ "<li>The domain of the Riskmaster application must NOT be included as an Intranet site (an example of a domain is 'https://www.YourRiskmasterSite.com'.  If your Riskmaster domain is listed as an Intranet site it must be removed from that list.</li>"
+ "<li>In Internet Explorer click 'Tools' - 'Internet Options' - 'Security' - 'Trusted Sites' - 'Sites'</li>"
+ "<li>The domain of the Riskmaster application MUST be included as a trusted site (an example of a domain is 'https://www.YourRiskmasterSite.com'</li>"
+ "<li>Also on the 'Security' panel, with 'Trusted Sites' selected, click 'custom level':"
    + "<ul>"
        + "<li>'Initialize and script ActiveX Objects not marked as safe...' must be set to 'Enabled'</li>"
        + "<li>'Access data sources across domains' must be set to either 'Prompt' or 'Enabled'</li>"
    + "</ul>"
+ "</li>"
+ "<li>Click 'OK' to accept the changes, then close and re-open your browser before trying again.</li></ul>"

function DisplayError(errMsg)
{
    document.write('<h3>Error Message</h3><p>' + errMsg + '</p><br />' + _mergeFailureErrorMessage); 
    return false;
}
function SelectTemplate(templateid,catname)
{
   location.href="MergeEditTemplate1.aspx?id="+templateid+"&catname="+catname;
}
function GetMergeHeaderFileName()
{
    //strForm could be a .doc or an .rtf
    var fileName = new String(strForm);
    fileName = fileName.toLowerCase();
    if(fileName.indexOf(".rtf") > 0)
        return String(strForm).replace(strFile,"hdr_" + String(strFile).replace(".rtf",".txt"));
    else if(fileName.indexOf(".doc") > 0)
        return String(strForm).replace(strFile,"hdr_" + String(strFile).replace(".doc",".txt"));
    else
        alert(parent.CommonValidations.ValidUnknownFileExtensionCheck + strForm + ","+ parent.CommonValidations.ValidFileExtensionsCheck);
}
function IsBrowserSupported()
{
	var i = String(navigator.appVersion).indexOf("MSIE");
	if (i==-1) 
		return false;
	if (5 > parseInt(String(navigator.appVersion).substr(i),10))
		return false;
	return true;
}
function pageLoaded()
{
	//Set focus to first field
	 var i; 
	 for (i=0;i<document.forms[0].length;i++)			
		{	 				
          if((document.forms[0].item(i).type=="text") || (document.forms[0].item(i).type=="select-one") || (document.forms[0].item(i).type=="textarea"))
			{
				document.forms[0].item(i).focus();
				break;
			}
		}
}		
function ValidateBrowser()
{
	if (!IsBrowserSupported())
		ReportClientErr("1","insufficient browser version","","client","MergeListTemplates.asp");
	return true;
}
function ReportClientErr(num,desc,msg,src,aborturl)
{
	window.location="MergeErrorResponse.asp?num=" + num + "&desc=" + desc + "&msg=" + msg + "&src=" + src + "&aborturl=" + aborturl;
	return true;
}
/*
if (!IsBrowserSupported())
	ReportClientErr("1","insufficient browser version","","client","MergeListTemplates.asp");

*/
function getSelDocuments()
{
	var sDoc=new String();
	for(var i=0;i<document.forms[0].elements.length;i++)
	{
		var objElem=document.forms[0].elements[i];
		var sId=new String(objElem.id);
		if(sId.substring(0,9)=="letterid_" && objElem.checked)
		{
			if(sDoc!="")
				sDoc=sDoc + " ";
			sDoc=sDoc + sId.substring(9,sId.length);
		}
	}
	return sDoc;
}

function DeleteSelected()
{
	var sDoc = new String();
	sDoc = getSelDocuments();
	document.forms[0].hdnDeletedItems.value=sDoc;
	if(sDoc=="")
	{
	    //alert(parent.CommonValidations.ValidPrintcheckParams); //"Please select at least one template to delete."

	    alert(MergeTemplatesValidations.ValidSelectTmpltDelete); // pmanoharan5 for JIRA 13727

		return false;
	}
	//Could use existing delconfirm page but I'm not clear on 
	// module level security....
	//window.open("delconfirm.asp");
	// SO ----
	//if( confirm("Pressing 'OK' will completely remove the selected merge templates.\nAre you sure you wish to continue?")) 
    //if (confirm(parent.CommonValidations.ValidConfirmDelete)) 

	if (confirm(MergeTemplatesValidations.ValidConfirmDelete)) // pmanoharan5 for JIRA 13727
	{
		//document.URL="MergeDeleteTemplate.asp?docs=" + escape(sDoc);
		//document.getElementById('hdnaction').value="MergeDeleteTemplate";

    //Shruti for MITS 9624 starts
		return true;
	}
	else
	{
	    return false;
	}
	//Shruti for MITS 9624 ends
}

function DeleteConfirmed()
{
	// Send request to the server
	document.URL="MergeDeleteTemplate.asp?docs=" + escape(sDoc);
	return true;
	
}


function AddSingle(objOptElt)
{

	if(!IsSelected(objOptElt)) //not already transferred?
	{

		if (objOptElt.value != "") 
		{
			document.getElementById('selectedfields').options[document.getElementById('selectedfields').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
		}
		else
		{
		    alert(parent.CommonValidations.ValidFieldCheck); //This is not a valid field.
		}
	}
	return true;
}



function RemoveSingle(objOptElt)
{
	var bDoIt;
	bDoIt = true;
	if(String(document.getElementById('selectedfields').options[objOptElt.index].value).charAt(0) =="_")
		bDoIt = window.confirm(parent.CommonValidations.ValidRemoveFieldCheck1 + parent.CommonValidations.ValidRemoveFieldCheck2); //"The field you wish to remove may already be in use within the document.  Are you sure you wish to remove it?"
	if(bDoIt)
		document.getElementById('selectedfields').options[objOptElt.index]=null;
	return true;
}


function AddSelected()
{
	var i;
	var str = new String();
	var ListBox;
	var Opt;
	
	ListBox = FindVisibleListBox();
	
	if (ListBox == null) return false;
	//Run through the selected elts.
	for(i=0;i<ListBox.length;i++)
	{
		if(ListBox.options[i].selected == true) //selected for transfer?
			if(!IsSelected(ListBox.options[i])) //not already transferred?
			{
				Opt = ListBox.options[i];
				if (Opt.value != "") 
				{
				    document.getElementById('selectedfields').options[document.getElementById('selectedfields').options.length] = new Option(Opt.text, Opt.value, false, false);					
				}
			}
	}
	//return true;
	return false;
}

function RemoveSelected()
{
	var i;
	var ListBox;
	var bDoIt;
	
	ListBox = document.getElementById('selectedfields');
	
	for(i=ListBox.options.length-1;i>=0;i--)
		if(ListBox.options[i].selected == true)
		{
			bDoIt = true;
			if(String(ListBox.options[i].value).charAt(0) =="_")
				bDoIt = window.confirm(parent.CommonValidations.ValidRemoveFieldCheck1 + ListBox.options[i].text + parent.CommonValidations.ValidRemoveFieldCheck2);
			if (bDoIt)
				ListBox.options[i] = null;
		}		
	//return true;
	return false;

}

/*function Cancel()
{

	location.href="MergeListTemplates.asp";
	// Always return values from function req. for Netscape compatiblity
	return true;
}
*/

function Cancel()
{
    //Shruti for 7337
    window.opener.close();
    //Shruti for 7337 ends
	window.close();
	//location.href="MergeListTemplates.asp";
	return false;
}

function IsSelected(Elt)
{
	

	var i;
	
	for (i=0; i<document.getElementById('selectedfields').options.length ;i++)
	{
		if (String(Elt.value).indexOf(document.getElementById('selectedfields').options[i].value)!= -1)
		{
			return true;
		} 
	}
	return false;
}


function FindVisibleListBox()
{
	var i;
	var str = new String();
	var ListBox;
	//Pick the Visible Field Listbox
	
	  for (i=0; i<document.forms[0].length ;i++)	
	{
	        str = document.forms[0].elements[i].id
		if (str.substr(0,7) == "fields_")		
			  if(document.forms[0].elements[i].style.display == "inline")			
				  ListBox = document.forms[0].elements[i]; 	
	}
	
	return ListBox;

}

//Verify that at least one record has been selected.
function ValidateRecords()
{
    var EmailCheckquerystring = "";
    if (document.getElementById('EmailCheck') != null) {
        querystring = document.getElementById('EmailCheck').value;
        if (EmailCheckquerystring == "on")
            EmailCheckquerystring = true;
    }
	var elt;
	var i;
	var bProceed;

	var none_checked="1";
	var prev_value_checkbox="";

    for (i=0; i<document.forms[0].length ;i++)
	{
		str = document.forms[0].elements[i].id;
		if (str.substr(0,6) == "rowid_")
		{	
	        if(document.forms[0].elements[i].checked == true)
			{
				if( prev_value_checkbox != "" )
					prev_value_checkbox += ",";
				prev_value_checkbox += document.forms[0].elements[i].value;
				//document.forms[0].target="_new";			
				none_checked="0";				
			}
		}
	}

	document.forms[0].hdnrowids.value = prev_value_checkbox;
			
	if (none_checked != "0")
	{
		alert(parent.CommonValidations.ValidRecordSelection)//At least one record must be selected to generate a letter.\nPlease choose (at least) one by placing a checkmark next to it and pressing \"Next\"."
		return false;
	}
    if ( document.getElementById('recordselect').value=='Checked' &&   document.getElementById('attach').value=='Checked')
	{	
	//which page to open(conditions)
		//document.getElementById('hdnAction').value="NextFetch_MergeEditResult";
	    //window.location.href="";
	    //window.open("")
	    //document.forms[0].action='MergeEditResult.aspx';
	    //document.forms[0].method="post";
        document.forms[0].action = 'FetchData.aspx?page=MergeEditResult&EmailCheck=' + EmailCheckquerystring;
	    document.forms[0].submit();
	}
	else
	{	
	//document.getElementById('hdnAction').value="NextFetch_OpenRTF";
        //window.location.href="OpenMailMergedLetter.aspx";
        //MITS ID: 33447 - nkaranam2
        //window.open("OpenMailMergedLetter.aspx")
	// RMA-8407 : MITS-36022 : Mudabbir
	//var useSilver = document.getElementById('hdnSilver');
	//var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
	//if (useSilver != null)
	//    useSilver = useSilver.value;//Chrome
	//else
	//    useSilverlight = !IEbrowser;
	//if (useSilverlight.toString() == "true") 
	//if(!IEbrowser)
	//window.open("OpenMailMergedLetter.aspx? UseSilverlight=" + useSilver);
	//else{
	//RMA-11511 : Index was outside the bounds of the array in chrome , no use of UseSilverlight (Querysting parameter) in window.open,hence commented.
	document.forms[0].action = "OpenMailMergedLetter.aspx?EmailCheck=" + EmailCheckquerystring;
	document.forms[0].submit();
	//}
	}
		
	return false;

}

function ValidateMergeLetter1()
{
    var EmailCheckquerystring = "";
    if (document.getElementById('EmailCheck') != null) {
        EmailCheckquerystring = document.getElementById('EmailCheck').checked;
        if (EmailCheckquerystring == "on")
            EmailCheckquerystring = true;
    }
	var Elt = document.getElementById('lettername');
	var chk1 = document.getElementById('recordselect');
	var chk2 = document.getElementById('attach');
    
    //JIRA RMA-1287 nshah28 Start
	displayemailoption();
    //JIRA RMA-1287 nshah28 End

	if (Elt.options[0].selected)
	{
	    //alert(parent.CommonValidations.SelectFormJSResrc); //'Please select a form.
	    alert(parent.CommonValidations.PleaseSelectTemplate);//RMA-15393 msampathkuma
		Elt.focus();
		return false;
	}
	//Set values for template name/Id/CatId, in the format of Name***TemplateId***CatId
	//svar iCount = Elt.options.length;
	var index = 0;
	var sNameID = "";
	var sValue = "";
	var sTemp = "";
	var iPosition = 0;
	var oHidden = null;
//	for(index=0; index<iCount; index++)
//	{
//		if( Elt.options[index].selected )
//		{
//			//Get CatId
//			sValue = Elt.options[index].value;
//			iPosition = sValue.lastIndexOf("***");
//			sTemp = sValue.substring(iPosition);
//			sTemp = sTemp.substring(3);
//			oHidden = document.getElementById('hdnCatId');
//			if( oHidden != null )
//				oHidden.value = sTemp;
//			
//			//Get TemplateId
//			sValue = sValue.substring(0, iPosition);
//			iPosition = sValue.lastIndexOf("***");
//			sTemp = sValue.substring(iPosition);
//			sTemp = sTemp.substring(3);
//			oHidden = document.getElementById('hdnTemplateId');
//			if( oHidden != null )
//				oHidden.value = sTemp;
//			
//			//Get Template Name
//			sValue = sValue.substring(0, iPosition);
//			oHidden = document.getElementById('hdnLetterName');
//			if( oHidden != null )
//				oHidden.value = sValue;
//			
//			break;
//		}
//	}
	
//	if (!document.getElementById("recordselect").checked) 	
//	{		
//		document.forms[0].target="_new";
//		
//	}
//	else
//	{
//		
//		document.forms[0].target="";
//	}
	if (  !(document.getElementById('attach').checked) &&  !(document.getElementById('recordselect').checked) )
	{
	    //document.getElementById('hdnDirectDisplay').value="1";		
	    //MITS ID: 33447 - nkaranam2
	    // RMA-8407 : MITS-36022 : Mudabbir 
	    //var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
	    //RMA - 10937 : Page should not open in the new window.
	    //if (!IEbrowser)
	    //window.open("OpenMailMergedLetter.aspx? UseSilverlight=" + useSilver);
	    //else{
	    document.forms[0].action = "OpenMailMergedLetter.aspx?EmailCheck=" + EmailCheckquerystring;
	    document.forms[0].submit();
	    // }
	}
	else if (  (document.getElementById('attach').checked) &&  (document.getElementById('recordselect').checked) )
	{
	    //document.forms[0].action="MergeTemplate2.aspx?opentype=samewindow";
	    //document.forms[0].method = "post";
	    document.forms[0].action = "MergeTemplate1.aspx?page=MergeTemplate2samewindow&EmailCheck=" + EmailCheckquerystring;
	    document.forms[0].submit();
	}
	else if (document.getElementById('recordselect').checked)
	{	
	    //document.getElementById('hdnAction').value="Next1Attach";
	    //document.forms[0].action="FetchData.aspx";
	    //document.forms[0].method = "post";
	    document.forms[0].action = "MergeTemplate1.aspx?page=FetchData&EmailCheck=" + EmailCheckquerystring;
	    document.forms[0].submit();
	}
	    //which page to open(conditions)/////////////////////////////
	else if (document.getElementById('attach').checked)
	{	
	    //document.getElementById('hdnAction').value="Next1Attach"
	    document.forms[0].target="";
		
	    if (window.opener.opener == null) {
	        // window.open("MergeTemplate2.aspx");
	        //MITS ID: 33447 - nkaranam2
		// RMA-8407 : MITS-36022 : Mudabbir 
	    var useSilver = document.getElementById('hdnSilver');
	        var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
	        if (useSilver != null)
	            useSilver = useSilver.value;
	        else
	            useSilverlight = !IEbrowser;
           // if (useSilverlight.toString() == "true") {//Chrome
	    //document.forms[0].action="MergeTemplate2.aspx?opentype=samewindow";
	        //document.forms[0].method="post";
	       // if (!IEbrowser) {
	            document.forms[0].action = "MergeTemplate1.aspx?page=MergeTemplate2samewindow&EmailCheck=" + EmailCheckquerystring;
	            document.forms[0].submit();
	        }

	        else {
		     document.forms[0].action = "MergeTemplate2.aspx?opentype=samewindow&EmailCheck=" + EmailCheckquerystring;
	            document.forms[0].submit();
	        }
	    //}
	}
	
//	else if( !(document.getElementById('recordselect').checked) )

//	{	
//		document.getElementById('hdnAction').value="Next1OpenRTFDirectly";
//		//var wnd=window.open('home?pg=riskmaster/MailMerge/OpenRTF','OpenRTF','width=350,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-350)/2);
//		//return false;

//	}
//	else if( document.getElementById('recordselect').checked )


//	{
//		//document.getElementById('hdnAction').value="Next1FetchData";
//	}
//		
	//document.getElementById('previousaction').value = document.getElementById('hdnAction').value;
	
	return false;
}



function goback()
{	
	history.go(-1);	

}

function ValidateMergeLetter2()
{
    var EmailCheckquerystring = "";
    if (document.getElementById('EmailCheck') != null) {
        EmailCheckquerystring = document.getElementById('EmailCheck').value;
        if (EmailCheckquerystring == "on")
            EmailCheckquerystring = true;
    }
    //alert ( document.getElementById('recordselect').value);
    //alert ( document.getElementById('attach').value);
    if ( document.getElementById('recordselect').value=='Checked' &&   document.getElementById('attach').value=='Checked')
	{		
		    //document.getElementById('hdnAction').value="Next2FetchData";
		    //document.forms[0].action="FetchData.aspx";
		    //document.forms[0].method = "post";
        document.forms[0].action = "MergeTemplate2.aspx?page=FetchData&EmailCheck=" + EmailCheckquerystring;
			document.forms[0].submit();
			
		
	}
	else if ( document.getElementById('recordselect').value=='Checked' )
	{		
		//document.getElementById('hdnAction').value="Next2FetchData";
        //document.forms[0].action="FetchData.aspx";
	    //document.forms[0].method="post";
	    document.forms[0].action = "MergeTemplate2.aspx?page=FetchData&EmailCheck=" + EmailCheckquerystring;
			document.forms[0].submit();
		
	}
	else
	{
        //document.forms[0].action="MergeEditResult.aspx";
	    //document.forms[0].method="post";
	    document.forms[0].action = "MergeTemplate2.aspx?page=MergeEditResult&EmailCheck=" + EmailCheckquerystring;
		document.forms[0].submit();
		//document.getElementById('hdnAction').value="Next2MergeEditResult";
	}					

	return false;
}


function FixInputString(str)
{


	var s;
	s = String(str).replace("'"," ");
	s = String(s).replace('"'," ");
	return s;
}

function Validate_MergeCreateLetter1()
{
	if(document.forms[0].allstatesselected.checked != true && document.forms[0].txtState.value=="")
	{
	    alert(parent.CommonValidations.ValidateStateInfoResrc);//Please enter the State information.
		return false;
	}
	//Shruti for 11412
	if(CheckForDuplicacy())
	{
	    return false;
	}
	//Shruti for 11412 ends
	var s = document.getElementById('lettername').value;

	var strFixed;
	if (Trim(s))
	{
		if(s.length > 25) 
		{
			  alert(parent.CommonValidations.ValidNameCheck);//The supplied name exceeds the maximum (25 character) allowable length.
			  document.getElementById('lettername').select();
			  document.getElementById('lettername').focus();
	     		  return false;
		}
		strFixed = FixInputString(s);
		if(s != strFixed)
		{			
			  document.getElementById('lettername').value = strFixed;
			  alert(parent.CommonValidations.ValidRestNameCharsCheck); //Restricted characters have been removed from the name.
		}	
	}
	else
	{
		alert(parent.CommonValidations.ValidFormNameCheck);//Please supply a name for this form.
  		document.getElementById('lettername').select();
  		document.getElementById('lettername').focus();
		return false;
	}
	

	s = new String(document.getElementById('letterdesc').value);

	if (s.length >250)
	{
			alert(parent.CommonValidations.ValidDescriptionCheck);//The supplied description exceeds the maximum (250 character) allowable length.
	   		document.getElementById('letterdesc').select();
	   		document.getElementById('letterdesc').focus();
			return false;
	}			


	strFixed = FixInputString(s);
	if(s != strFixed)
	{
		document.getElementById('letterdesc').value = strFixed;
		alert(parent.CommonValidations.ValidRestDescCharsCheck); //Restricted characters have been removed from the description.
}
//spahariya MITS 28867
if (document.forms[0].EmailCheck.checked == true) {
    //modified by swati for MITS # 36930
    //if (document.forms[0].lstMailRecipient.value == null || document.forms[0].lstMailRecipient.value == "") {
	    if (document.getElementById("selecteduserfields").value == null || document.getElementById("selecteduserfields").options.length == 0) {
        alert(parent.CommonValidations.SelDefReptReMail);   //Swati 30/01/2013
        //alert("Select default recipient to receive the mail.");
        return false;
    }
}
//spahariya - end
    //added by swati MITS # 36930
	ListBox = document.getElementById('selecteduserfields');
	for (i = 0; i < ListBox.options.length; i++) {
	    ListBox.options[i].selected = true;
	}
	document.getElementById('hdnSelectedUser').value = "";
	for (var i = 0; i < document.getElementById('selecteduserfields').options.length; i++) {
	    if (document.getElementById('hdnSelectedUser').value != "") {
	        document.getElementById('hdnSelectedUser').value = document.getElementById('hdnSelectedUser').value + ',' + document.getElementById('selecteduserfields').options[i].text;
	    }
	    else {
	        document.getElementById('hdnSelectedUser').value = document.getElementById('selecteduserfields').options[i].text;
	    }
	}
	document.getElementById('hdnSelectedUserID').value = "";
	for (var i = 0; i < document.getElementById('selecteduserfields').options.length; i++) {
	    if (document.getElementById('hdnSelectedUserID').value != "") {
	        document.getElementById('hdnSelectedUserID').value = document.getElementById('hdnSelectedUserID').value + ',' + document.getElementById('selecteduserfields').options[i].value;
	    }
	    else {
	        document.getElementById('hdnSelectedUserID').value = document.getElementById('selecteduserfields').options[i].value;
	    }
	}
	//var SelectedFields = document.getElementById('hdnSelectedUser').value;
	//arr_SelectedFieldsId = SelectedFields.split("_");
	//var count_no = 0;    
	//var regex = /^[\d\|]+(<ALL>){0,1}$/; //reg exp for matching all nos.
	//document.getElementById('hdnSelectedUserID').value = "";
	//for (i = 0; i < arr_SelectedFieldsId.length; i++) {
	//    if (regex.test(arr_SelectedFieldsId[i])) {
	//        document.getElementById('hdnSelectedUserID').value = document.getElementById('hdnSelectedUserID').value + ',' + arr_SelectedFieldsId[i];
	//    }
	//}
    //change end here by swati
	if (bAdd == "true") {
	    s = document.getElementById('lettertype').options[document.getElementById('lettertype').selectedIndex].value;
	    if (Trim(document.getElementById('hdnLetterType').value) != Trim(document.getElementById('lettertype').value)) {
	        document.getElementById('hdnSelectedFields').value = "";
	        document.getElementById('hdnSelectedFieldsID').value = "";
	    }
	    if (Trim(document.getElementById('lettertype').value) != "") {
	        document.getElementById('hdnLetterType').value = document.getElementById('lettertype').value;
	    }
	}
	//document.getElementById('hdnCatId').value = s;
		
	
	
	if(document.getElementById('prefab').value != 'n/a')
	{
		//document.getElementById('hdnPrefab').value = 'true';
		//document.getElementById('hdnTemplateFile').value =document.getElementById('prefab').value;
	}

	//document.getElementById('lettertype').style.display = "none";
	//document.getElementById('lettertype').disabled = "false";
	//document.forms[0].submit();
		
	return true;


}

function Cancel_MergeTemplate()
{
	document.getElementById('hdnaction').value="Cancel";
	if ( document.getElementById('hdnRTFContent')!= null )
	{
	 document.getElementById('hdnRTFContent').value='';
	}
	if ( document.getElementById('hdnformfilecontent')!= null )
	{
	 document.getElementById('hdnformfilecontent').value='';
	}
	if ( document.getElementById('hdnfilename')!= null )
	{
	 document.getElementById('hdnfilename').value='';
	}
}
function Cancel_Merge()
{
	document.getElementById('hdnaction').value="Cancel";
	return true;
}



/////function added by jasbinder

function CheckListboxExist(optValue)
{
	var del_value=0;
	
	for(var i=0;i<document.forms[0].elements.length;i++)
	{
		var objElem=document.forms[0].elements[i];
		var sId=new String(objElem.id);
			
		if(sId.substring(0,7)=="fields_")
		{
			if (sId.substring(7,sId.length) == optValue)
			{						
				return false
			}
			else
			{				
				del_value=1;
			}
		}
	}
	
	
		
	return true;

}

function LoadFieldCategory()
{
	var sCatDesc;
	var vCatDesc;
	
	//sCatDesc = document.getElementById('fieldcategory').text;
	//vCatDesc = document.getElementById('fieldcategory').text;
	sCatDesc=document.getElementById('fieldcategory').options[document.getElementById('fieldcategory').selectedIndex].text;
	vCatDesc=document.getElementById('fieldcategory').options[document.getElementById('fieldcategory').selectedIndex].text;

	///////////added by jasbinder/////////		
	var checkNullCheckBox;	
	checkNullCheckBox = CheckListboxExist(sCatDesc )

	if (checkNullCheckBox )
	{
		sCatDesc="";
	}
	////////////////////till here///////////////////
var stemp =false;
	for(var i=0;i<document.forms[0].elements.length;i++)
	{
		var objElem=document.forms[0].elements[i];
		var sId=new String(objElem.id);

		if(sId.substring(0,7)=="fields_")
		{

			if (sId.substring(7,sId.length) == sCatDesc && stemp == false)
			{
					stemp = true;
					objElem.style.display ="inline";
					AdjustSelectedBoxWidth();
			}
			else
			{
				objElem.style.display = "none";
			}
		}
	}

	if (sCatDesc=="" &&  document.getElementById('fieldcategory').options[document.getElementById('fieldcategory').selectedIndex].text != "Select a Category")
	{
	    
		document.getElementById('fields_').length=0;		
	
	}

	if (document.getElementById('fieldcategory').options[document.getElementById('fieldcategory').selectedIndex].text == "Select a Category")	
	{	
		document.getElementById('fields_').length=0;
		document.getElementById('fields_').options[document.getElementById('fields_').options.length] = new Option("No Category Selected", "", false, false);
	}
	// Always return values from function req. for Netscape compatiblity
	return true;
}


function AdjustSelectedBoxWidth()
{
	var CurWidth;
	CurWidth = FindVisibleListBox().offsetWidth;
	if(CurWidth >(Number(String(document.forms[0].selectedfields.style.width).replace(/[^0-9]/g,""))))	
		document.forms[0].selectedfields.style.width = CurWidth;
	return true;
}





/*function Validate_MergeEditTemplate2()
{
	
	//Do not store values until end of wizard.
	// values are tracked in hidden fields
	// maintained through posting and asp.

	var i;
	var ListBox;
	var AvailBox;
	
	//Turn off selections in Available List box so they ARE NOT 
	// recorded and sent back to Server ASP.
	AvailBox = FindVisibleListBox();

	if (AvailBox != null) 
		for(i=0;i<AvailBox.options.length;i++)
			AvailBox.options[i].selected = false;
	
	//Ensure that all fields ARE "Selected" in the 
	// SelectedFields listbox for correct submission 
	// to XForms via POST.	
	ListBox = document.getElementById('selectedfields');
	for(i=0;i<ListBox.options.length;i++)
		ListBox.options[i].selected = true;
	

	document.getElementById('hdnaction').value="MergeEditTemplate3";
	document.forms[0].submit();
	// Always return values from function req. for Netscape compatiblity 
	return true;
}

*/




function Validate_MergeEditTemplate2()
{
	//Do not store values until end of wizard.
	// values are tracked in hidden fields
	// maintained through posting and asp.

	var i;
	var ListBox;
	var AvailBox;
	
	//Turn off selections in Available List box so they ARE NOT 
	// recorded and sent back to Server ASP.
	AvailBox = FindVisibleListBox();

	if (AvailBox != null) 
		for(i=0;i<AvailBox.options.length;i++)
			AvailBox.options[i].selected = false;
	
	//Ensure that all fields ARE "Selected" in the 
	// SelectedFields listbox for correct submission 
	// to XForms via POST.	

	ListBox = document.getElementById('selectedfields');
	
	// Mihika 12/27/2005 Defect no. 1093
	// Asking user to select merge fields 
	if(ListBox.options.length==0)
	{
	    alert(parent.CommonValidations.ValidSelectFieldsCheck);//Please select Fields to Merge.
		ListBox.focus();
		return false;
	}
	
	for(i=0;i<ListBox.options.length;i++)
	{
		
		ListBox.options[i].selected = true;
	}
	

	document.getElementById('hdnSelectedFields').value = "";
	for (var i=0;i<document.getElementById('selectedfields').options.length;i++)
	{
		document.getElementById('hdnSelectedFields').value = document.getElementById('hdnSelectedFields').value + ',' + document.getElementById('selectedfields').options[i].value;	
	}
	var SelectedFields = document.getElementById('hdnSelectedFields').value;
	arr_SelectedFieldsId = SelectedFields.split("_");
	var count_no=0;	
	
	//Geeta 07/04/07 : Modified for MITS 7813	
	var regex=/^[\d\|]+(<ALL>){0,1}$/; //reg exp for matching all nos.
	document.getElementById('hdnSelectedFieldsID').value="";
	for (i=0;i<arr_SelectedFieldsId.length;i++)
	{
		if (regex.test(arr_SelectedFieldsId[i]))
		{			
			document.getElementById('hdnSelectedFieldsID').value = document.getElementById('hdnSelectedFieldsID').value + ',' + arr_SelectedFieldsId[i];				
		}
		
	}
	//document.getElementById('hdnaction').value="MergeEditTemplate3";
	//document.forms[0].submit();
	//document.forms[0].submit();
	// Always return values from function req. for Netscape compatiblity 
	return true;
}

function BackMergeTemplate1()
{


	
	//Do not store values until end of wizard.
	// values are tracked in hidden fields
	// maintained through posting and asp.

	var i;
	var ListBox;
	var AvailBox;
	
	//Turn off selections in Available List box so they ARE NOT 
	// recorded and sent back to Server ASP.
	AvailBox = FindVisibleListBox();

	if (AvailBox != null) 
		for(i=0;i<AvailBox.options.length;i++)
			AvailBox.options[i].selected = false;
	
	//Ensure that all fields ARE "Selected" in the 
	// SelectedFields listbox for correct submission 
	// to XForms via POST.	
	ListBox = document.getElementById('selectedfields');

	// Mihika 12/27/2005 Defect no. 1093
	// Asking user to select merge fields 
	
	for(i=0;i<ListBox.options.length;i++)
	{
		
		ListBox.options[i].selected = true;
	}
	


	document.getElementById('hdnSelectedFields').value = "";
	for (var i=0;i<document.getElementById('selectedfields').options.length;i++)
	{
		document.getElementById('hdnSelectedFields').value = document.getElementById('hdnSelectedFields').value + ',' + document.getElementById('selectedfields').options[i].value;	
	}

	var SelectedFields = document.getElementById('hdnSelectedFields').value;

	arr_SelectedFieldsId = SelectedFields.split("_");
	
	var count_no=0;
	
	//Geeta 07/04/07 : Modified for MITS 7813	
	var regex=/^[\d\|]+(<ALL>){0,1}$/; //reg exp for matching all nos.
	//document.getElementById('hdnSelectedFieldsID').value="";
	for (i=0;i<arr_SelectedFieldsId.length;i++)
	{
		if (regex.test(arr_SelectedFieldsId[i]))
		{			
			document.getElementById('hdnSelectedFieldsID').value = document.getElementById('hdnSelectedFieldsID').value + ',' + arr_SelectedFieldsId[i];				
		}
		
	}

	document.getElementById('hdnaction').value="MergeCreateTemplate3";
	
	// Always return values from function req. for Netscape compatiblity 
	return true;

	
}

function Validate_MergeCreateTemplate2()
{

	//Do not store values until end of wizard.
	// values are tracked in hidden fields
    // maintained through posting and asp.

	var i;
	var ListBox;
	var AvailBox;
	var oldPhoneFieldList=false;
   
	
	//Turn off selections in Available List box so they ARE NOT 
	// recorded and sent back to Server ASP.
	AvailBox = FindVisibleListBox();

	if (AvailBox != null) 
		for(i=0;i<AvailBox.options.length;i++)
			AvailBox.options[i].selected = false;
	
	//Ensure that all fields ARE "Selected" in the 
	// SelectedFields listbox for correct submission 
	// to XForms via POST.	
	ListBox = document.getElementById('selectedfields');

	// Mihika 12/27/2005 Defect no. 1093
	// Asking user to select merge fields 
	if(ListBox.options.length==0)
	{
		alert(parent.CommonValidations.ValidSelectFieldsCheck);//Please select Fields to Merge.
		ListBox.focus();
		return false;
	}

	for(i=0;i<ListBox.options.length;i++)
	{

	    ListBox.options[i].selected = true;
	    if ((ListBox.options[i].value.toLowerCase().indexOf('home phone') != -1) || (ListBox.options[i].value.toLowerCase().indexOf('office phone') != -1)) {
	        oldPhoneFieldList = true;  //Added to prevent validation being performed on click of back and next buttons for unsaved templates.
	    }
	}
	//Added Rakhi for R7:Add Emp Data Elements
	var oldPhoneField = document.getElementById('hdnOldPhoneFieldExists');
	if (oldPhoneField != null) {
	    var oldPhoneFieldExists = oldPhoneField.value;
	    if (oldPhoneFieldExists == "true" && oldPhoneFieldList) {
	        if (confirm(parent.CommonValidations.ValidOfficeHomePhnCheck)) {//Office and Home phone can now be viewed with a single phone field.\n Do you want to upgrade?
	            if (confirm(parent.CommonValidations.ValidFieldsUpgradeCheck)) {//The fields may already be in use within the document.  Are you sure you wish to upgrade them
	                for (i = ListBox.options.length - 1; i >= 0; i--) {
	                    if ((ListBox.options[i].value.toLowerCase().indexOf('home phone') != -1) || (ListBox.options[i].value.toLowerCase().indexOf('office phone') != -1))
	                        ListBox.options[i] = null;
	                }
	                alert(parent.CommonValidations.ValidAddPhoneFieldsCheck); // fields have been removed.Please add new phone fields from the desired categories and continue.
	                return false;
	            }
	        }
	    }

	}
	//Added Rakhi for R7:Add Emp Data Elements


	document.getElementById('hdnSelectedFields').value = "";
	for (var i=0;i<document.getElementById('selectedfields').options.length;i++)
	{
	    document.getElementById('hdnSelectedFields').value = document.getElementById('hdnSelectedFields').value + ',' + document.getElementById('selectedfields').options[i].value.replace(/\,/gi, "[comma]");	
	}

	var SelectedFields = document.getElementById('hdnSelectedFields').value;

	arr_SelectedFieldsId = SelectedFields.split("_");
	
	var count_no=0;
	
	//Geeta 07/04/07 : Modified for MITS 7813	
	var regex=/^[\d\|]+(<ALL>){0,1}$/; //reg exp for matching all nos.
	document.getElementById('hdnSelectedFieldsID').value="";
	for (i=0;i<arr_SelectedFieldsId.length;i++)
	{
		if (regex.test(arr_SelectedFieldsId[i]))
		{			
			document.getElementById('hdnSelectedFieldsID').value = document.getElementById('hdnSelectedFieldsID').value + ',' + arr_SelectedFieldsId[i];				
		}
		
	}

	document.getElementById('hdnaction').value="MergeCreateTemplate3";

	// Always return values from function req. for Netscape compatiblity 
	return true;

	
}





function Validate_MergeCreateTemplate2Back()
{


	
	//Do not store values until end of wizard.
	// values are tracked in hidden fields
	// maintained through posting and asp.
	var i;
	var ListBox;
	var AvailBox;
	//Turn off selections in Available List box so they ARE NOT 
	// recorded and sent back to Server ASP.
	AvailBox = FindVisibleListBox();

	if (AvailBox != null) 
		for(i=0;i<AvailBox.options.length;i++)
			AvailBox.options[i].selected = false;
	    
	//Ensure that all fields ARE "Selected" in the 
	// SelectedFields listbox for correct submission 
	// to XForms via POST.	
	ListBox = document.getElementById('selectedfields');

	// Mihika 12/27/2005 Defect no. 1093
	// Asking user to select merge fields 
	
	/*
	    if(ListBox.options.length==0)
	    {
		    alert('Please select Fields to Merge');
		    ListBox.focus();
		    return false;
	    }
	*/

	for(i=0;i<ListBox.options.length;i++)
	{
		
		ListBox.options[i].selected = true;
	}
	


	document.getElementById('hdnSelectedFields').value = "";
	for (var i=0;i<document.getElementById('selectedfields').options.length;i++) {
		document.getElementById('hdnSelectedFields').value = document.getElementById('hdnSelectedFields').value + ',' + document.getElementById('selectedfields').options[i].value;	
	}

	var SelectedFields = document.getElementById('hdnSelectedFields').value;
    
	arr_SelectedFieldsId = SelectedFields.split("_");
	
	var count_no=0;
	
	//Geeta 07/04/07 : Modified for MITS 7813	
	var regex=/^[\d\|]+(<ALL>){0,1}$/; //reg exp for matching all nos.
	document.getElementById('hdnSelectedFieldsID').value="";
	for (i=0;i<arr_SelectedFieldsId.length;i++)
	{
		if (regex.test(arr_SelectedFieldsId[i]))
		{			
			document.getElementById('hdnSelectedFieldsID').value = document.getElementById('hdnSelectedFieldsID').value + ',' + arr_SelectedFieldsId[i];				
		}
		
	}
	//document.getElementById('hdnaction').value="MergeCreateTemplate3";
	goBackMergeTemplate();
	// Always return values from function req. for Netscape compatiblity 
	return true;

	
}




function ToggleCustomPerms()
{
	var CurState;
	CurState = document.getElementById('allowall').checked;	
	document.getElementById('selectedperms').disabled = CurState;
	document.getElementById('availperms').disabled = CurState;
	document.getElementById('add').disabled = CurState;
	document.getElementById('remove').disabled = CurState;
	document.getElementById('hdnisallowallperms').value = CurState;
}

function Validate_MergeCreateTemplate3()
{
	//Do not store values until end of wizard.
	// values are tracked in hidden fields
	// maintained through posting and asp.
	
if ( document.getElementById('hdnisallowallperms').value=='false' && document.getElementById('selectedperms').options.length==0)
{
    alert(parent.CommonValidations.ValidAssignUserCheck); //Please assign at least one user or check allow all.
    return false;
}
	var i;
	var ListBox;
	var bAllowAll;

	bAllowAll = document.getElementById('allowall').value;

	//Turn off selections in Available List box so they ARE NOT
	// recorded and sent back to Server ASP.
	for(i=0;i<document.getElementById('availperms').options.length;i++)
			document.getElementById('availperms').options[i].selected = false;


	//Ensure that all fields are "Selected" in the
	// SelectedPerms listbox for correct submission
	// to ASP via POST.
	ListBox = document.getElementById('selectedperms');

	for(i=0;i<ListBox.options.length;i++)
		ListBox.options[i].selected = !document.getElementById('allowall').checked;

	//document.forms[0].action="MergeEditTemplate4.asp" ;

	//shakti : back support
	document.getElementById('hdnselectedperms').value = "";
	for (var i=0;i<document.getElementById('selectedperms').options.length;i++)
	{
		document.getElementById('hdnselectedperms').value = document.getElementById('hdnselectedperms').value + ';' + document.getElementById('selectedperms').options[i].text;	
	}
	
	
	
	document.getElementById('hdnselectedpermsids').value = "";
	
	for (var i=0;i<document.getElementById('selectedperms').options.length;i++)
	{
		document.getElementById('hdnselectedpermsids').value = document.getElementById('hdnselectedpermsids').value + ' ' + document.getElementById('selectedperms').options[i].value;	
	}//end
	
	//document.getElementById('hdnSelectedpermsusers').value = "";
	document.getElementById('hdnselectedpermsusers').value = ""; //Jira: 7486 -govind
	
	//document.getElementById('hdnSelectedpermsgroups').value = "";   
	document.getElementById('hdnselectedpermsgroups').value = ""; //Jira: 7486 -govind
	
	if(!document.getElementById('allowall').checked)
	{
		for (i=0;i<document.getElementById('selectedperms').options.length;i++)
		{
			if (document.getElementById('selectedperms').options[i].value.charAt(0) == 'g')
			{
			    //document.getElementById('hdnselectedpermsgroups').value = document.getElementById('hdnselectedpermsgroups').value + ',' + document.getElementById('Selectedperms').options[i].value.substring(6,document.getElementById('selectedperms').options[i].value.length);
			    document.getElementById('hdnselectedpermsgroups').value = document.getElementById('hdnselectedpermsgroups').value + ',' + document.getElementById('selectedperms').options[i].value.substring(6, document.getElementById('selectedperms').options[i].value.length); //Jira: 7486 -govind
				                                                                                                                                                   
			}
			else
			{
			    //document.getElementById('hdnselectedpermsusers').value = document.getElementById('hdnselectedpermsusers').value + ',' + document.getElementById('Selectedperms').options[i].value.substring(5, document.getElementById('selectedperms').options[i].value.length);
			    document.getElementById('hdnselectedpermsusers').value = document.getElementById('hdnselectedpermsusers').value + ',' + document.getElementById('selectedperms').options[i].value.substring(5, document.getElementById('selectedperms').options[i].value.length); //Jira: 7486 -govind
			}
		}
	}
	else
	{
		document.getElementById('hdnselectedpermsgroups').value=',0';
		document.getElementById('hdnselectedpermsusers').value=',0';
	}
	
	if (document.getElementById('hdnselectedpermsgroups').value=="")	
	{
		document.getElementById('hdnselectedpermsgroups').value=',0';
	}

	if (document.getElementById('hdnselectedpermsusers').value=="")	
	{
		document.getElementById('hdnselectedpermsusers').value=',0';
	}
	//document.getElementById('hdnaction').value="MergeCreateTemplate4";
	// Always return values from function req. for Netscape compatiblity
	return true;
}



function Cancel_MergeCreateTemplate3()
{
	document.getElementById('hdnaction').value="Cancel";
	// Always return values from function req. for Netscape compatiblity
	return true;
}

function AddUserGroup_MergeCreateTemplate3()
{
    alert(parent.CommonValidations.ValidAddUserGroupCode);//Add Code to add User/Group to the list box below.
	// Always return values from function req. for Netscape compatiblity
	return true;
}

function RemoveUserGroup_MergeCreateTemplate3()
{
    alert(parent.CommonValidations.ValidRemoveUserGroupCode);//Add Code to remove User/Group from the list box.
	// Always return values from function req. for Netscape compatiblity
	return true;
}



function AddSelected_MergeCreateTemplate3()
{
	var i;
	var str = new String();
	var ListBox;
	var Opt;

	ListBox = document.getElementById('availperms');
	if (ListBox == null) return false;

	//Run through the selected elts.
	for(i=0;i<ListBox.length;i++)
	{
		if(ListBox.options[i].selected == true) //selected for transfer?
			if(!IsSelected_MergeCreateTemplate3(ListBox.options[i])) //not already transferred?
			{
				Opt = ListBox.options[i];
				document.getElementById('selectedperms').options[document.getElementById('selectedperms').options.length] = new Option(Opt.text, Opt.value, false, false);
			}
	}
	//return true;
	return false
}

function RemoveSelected_MergeCreateTemplate3()
{
	var i;
	var ListBox;
	ListBox = document.getElementById('selectedperms');
	for(i=ListBox.options.length-1;i>=0;i--)
		if(ListBox.options[i].selected == true)
			ListBox.options[i] = null;
	// Always return values from function req. for Netscape compatiblity
	//return true;
	return false;
}

function AddSingle_MergeCreateTemplate3(objOptElt)
{
	if(!IsSelected_MergeCreateTemplate3(objOptElt)) //not already transferred?
	{		
		if (objOptElt.value != "")		
		{
			
			document.getElementById('selectedperms').options[document.getElementById('selectedperms').options.length] = new Option(objOptElt.text, objOptElt.value, false, false);
		}
		else
		{
		    alert(parent.CommonValidations.ValidUserGroupCheck); //This is not a valid user or group.
		}
	}
	return false;
}

function RemoveSingle_MergeCreateTemplate3(objOptElt)
{	
	document.getElementById('selectedperms').options[objOptElt.index]=null;
	return true;
}


function IsSelected_MergeCreateTemplate3(Elt)
{	
	var i;
	for (i=0; i<document.getElementById('selectedperms').options.length ;i++)
	{
		if (Elt.value == document.getElementById('selectedperms').options[i].value)
		{
			return true;
		}
	}
	return false;
}

/////////////////////////////////////////////////


function EditMergeTemplate()
{
	document.getElementById('hdnaction').value="MergeEditTemplate";
	return true;
}

function Validate_MergeEditTemplate1()
{
	if(document.forms[0].allstatesselected.checked != true && (document.forms[0].txtState.value=="" || document.forms[0].txtState.value==" "))
	{
	    alert(parent.CommonValidations.ValidateStateInfoResrc); //Please enter the State information.
		return false;
	}
	//Shruti for 11412
	if(CheckForDuplicacy())
	{
	    return false;
	}
	//Shruti for 11412 ends
	var s = document.getElementById('lettername').value;

	var strFixed;
	if (Trim(s))
	{
		if(s.length > 25) 
		{
		      alert(parent.CommonValidations.ValidNameCheck); //The supplied name exceeds the maximum (25 character) allowable length.
			  document.getElementById('lettername').select();
			  document.getElementById('lettername').focus();
	     		  return false;
		}
		strFixed = FixInputString(s);
		if(s != strFixed)
		{			
			  document.getElementById('lettername').value = strFixed;
			  alert(parent.CommonValidations.ValidRestNameCharsCheck);//Restricted characters have been removed from the name.
		}	
	}
	else
	{
		alert(parent.CommonValidations.FormName);//Please supply a name for this form.
  		document.getElementById('lettername').select();
		document.getElementById('lettername').focus();
		return false;
	}
	

	s = new String(document.getElementById('letterdesc').value);

	if (s.length >250)
	{
			alert(parent.CommonValidations.ValidDescriptionCheck);//The supplied description exceeds the maximum (250 character) allowable length.
	   		document.getElementById('letterdesc').select();
			document.getElementById('letterdesc').focus();
			return false;
	}			


	strFixed = FixInputString(s);
	if(s != strFixed)
	{
		
		document.getElementById('letterdesc').value = strFixed;	
		alert(parent.CommonValidations.RestrictedCharRemovedDesc);//Restricted characters have been removed from the description.
	}	

	s = document.getElementById('lettertype').options[document.getElementById('lettertype').selectedIndex].value;
	
	//document.getElementById('hdnCatId').value = s;
		
	if( s =="20") //Admin Tracking	
	{			
		document.getElementById('hdnaction').value = 'MergeEditTemplate2Admin';
	}	
	else
	{			
		document.getElementById('hdnaction').value = 'MergeEditTemplate2';
	}
	
	if(document.getElementById('txtmergedocumenttype').value == '')
	{
	    alert(parent.CommonValidations.ValidMergedDocType); //Please select the type of merge document.
		return false;
	}
	if(document.getElementById('txtmergedocumentformat').value == '')
	{
	    alert(parent.CommonValidations.ValidMergedDocFormat); //Please select the format of merge document.
		return false;
	}
	if(document.getElementById('txtState').value == '' && document.forms[0].allstatesselected.checked == false)
	{
	    alert(parent.CommonValidations.ValidSelectState); //Please select the State to properly categorize the template.
		return false;
	}
	
//spahariya MITS 28867
if (document.forms[0].EmailCheck.checked == true) {
        //modified by swati for MITS # 36930
    //if (document.forms[0].lstMailRecipient.value == null || document.forms[0].lstMailRecipient.value == "") {
	    if (document.getElementById("selecteduserfields").value == null || document.getElementById("selecteduserfields").options.length == 0) {
        alert(parent.CommonValidations.SelDefReptReMail);   //Swati 30/01/2013
        //alert("Select default recipient to receive the mail.");
        return false;
    }
}
//spahariya - end
	document.getElementById('lettertype').style.display = "none";
	document.getElementById('lettertype').disabled = "false";
//	document.forms[0].submit();
		
	return true;


}




function VerifyCatChange()
{	
	var lCatIdx = 0;

	//if((lCatIdx == 0) || (document.getElementById('selectedfields').length == 0))
	if(document.getElementById('selectedfields').length == 0)
	{	
		lCatIdx = document.getElementById('fieldcategory').selectedIndex;
		return true;
	}
	else
	{		
		document.getElementById('fieldcategory').selectedIndex =lCatIdx;
		alert(parent.CommonValidations.ValidAdminCatChangeCheck); //"Category cannot be changed while fields are selected.(All Admin Tracking fields must come from a single category)\n\nPlease remove the selected fields if you wish to use a different category.");
	}
	
	return false;
}




function RemoveSingle_MergeEditTemplate3(objOptElt)
{	
	document.getElementById('selectedperms').options[objOptElt.index]=null;
	return true;
}

function AddSingle_MergeEditTemplate3(objOptElt)
{

	if(!IsSelected_MergeCreateTemplate3(objOptElt)) //not already transferred?
	{		
		if (objOptElt.value != "")		{
			
				document.getElementById('selectedperms').options[document.getElementById('selectedperms').options.length] = new 	Option(objOptElt.text, objOptElt.value, false, false);
		}
		else
		{
		    alert(parent.CommonValidations.ValidUserGroupCheck); //"This is not a valid user or group.");
		}
	}
	return false;
}



//////////////////////////////////////////////////
function InitPageSettings_CreateTemplate4()
{
	//Interrogate the current state of affairs.
	
	try
	{	if(document.forms[0].prefab.value + "" != "")
			sPrefab = document.forms[0].prefab.value;
	}catch(e){;}
	try
	{	if(document.forms[0].filename.value + "" != "")
			strFile = document.forms[0].filename.value;
	}catch(e){;}
	try
	{	if(document.forms[0].doc.value + "" != "")
			lDoc = document.forms[0].doc.value;
	}catch(e){;}
	try
	{	if(document.forms[0].documenttype.value + "" != "")
			lDocType = document.forms[0].documenttype.value;
	}catch(e){lDocType = 0;}

	try
	{
		//Construct the local file path
	    strFile = document.getElementById('hdnfilename').value;
	    var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
        //RMA-8407 : MITS-36022 :mudabbir added if condition to check if silverlight is installed
        if (document.getElementById("hdnSilver") != null)
            useSilverlight = document.getElementById("hdnSilver").value;
        else
            useSilverlight = !IEbrowser;
	    //if (useSilverlight.toString() == "true") {
        if (useSilverlight == true || useSilverlight == "true") {
            if (Silverlight.isInstalled("5.0")) {
                //RMA-11630 : bkuzhanthaim : Silverlight installation Image should present only in chrome
                //document.getElementById("silverlightControlHost").style.display = "none";
            }
            else
                alert(parent.CommonValidations.ValidMergeMergeSilverlight);
        }
       
    else //if silverlight is absent then it will check for Active X
        {
        document.getElementById("silverlightControlHost").style.display = "none";//RMA-11630
		fso = new ActiveXObject("Scripting.FileSystemObject");
		strFile = document.getElementById('hdnfilename').value;
		//shakti
		strForm = fso.GetSpecialFolder(2).Path + "\\" + strFile;
    }
		// strForm = "C:\\Temp" + "\\" + strFile;
		//shakti	
	}
	catch (e)
	{
		//ReportClientErr("8","Insufficient Browser rights to create Scripting.FileSystemObject","","MergeEditTemplate4.asp","MergeListTemplates.asp");
		//document.getElementById('hdnaction').value = "ActiveXError";
		 document.forms[0].action="MergeErrorResponse.aspx";
	    document.forms[0].method="post";
		document.forms[0].submit();
		//return false;
	}
}
//shakti back support in create template
function InitPageSettings_MergeTemplate() {

	var arrSelectedFields= new Array();
	var arrSelectedFieldIds=new Array();
	arrSelectedFields = document.getElementById('hdnSelectedFields').value.split(',');
	arrSelectedFieldIds = document.getElementById('hdnSelectedFieldsID').value.split(',');

	for (i=1;i<arrSelectedFields.length;i++)
	{
		var sFieldSel  = new String(arrSelectedFields[i].valueOf().replace(/\[comma\]/gi, ","));
		sFieldSel=	sFieldSel.substring(sFieldSel.lastIndexOf("_",sFieldSel.length)+1,sFieldSel.length);

		if(!IsSelected(new Option(sFieldSel, arrSelectedFields[i].valueOf(), false, false)))
		{
			document.getElementById('selectedfields').options[document.getElementById('selectedfields').options.length] = new Option (sFieldSel, arrSelectedFields[i].valueOf(), false, false);					
		}
	} 
	if ( document.getElementById('fieldcategory').value != 'Select a Category')
	{
		LoadFieldCategory();
	}
	document.forms[0].method="post";
}

//added by swati for AIC Gap 8 MITS # 36930
function InitPageSettings_MergeTemplate1() {
    var arrSelectedFields = new Array();
    var arrSelectedFieldIds = new Array();
    if (document.getElementById('hdnSelectedUser') != null && document.getElementById('hdnSelectedUser').value != "") {
        arrSelectedFields = document.getElementById('hdnSelectedUser').value.split(',');
    }
    if (document.getElementById('hdnSelectedUserID') != null && document.getElementById('hdnSelectedUserID').value != "") {
        arrSelectedFieldIds = document.getElementById('hdnSelectedUserID').value.split(',');
    }
    for (i = 0; i < arrSelectedFields.length; i++) {        

        if (!IsUserSelected(new Option(arrSelectedFields[i].valueOf(), arrSelectedFieldIds[i].valueOf(), false, false))) {
            var opt = new Option(arrSelectedFields[i].valueOf(), arrSelectedFieldIds[i].valueOf(), false, false);
            if (opt.value != "") {
                document.getElementById('selecteduserfields').options[document.getElementById('selecteduserfields').options.length] = opt;
            }
        }
    }
    //document.forms[0].method = "post";
}

function IsUserSelected(Elt) {


    var i;

    for (i = 0; i < document.getElementById('selecteduserfields').options.length ; i++) {
        if (String(Elt.value).indexOf(document.getElementById('selecteduserfields').options[i].value) != -1) {
            return true;
        }
    }
    return false;
}
//change end here
function CleanAndCancel_CreateTemplate4()
{

	try
	{
		//Clean Up.
		if (fso.FileExists(strForm)) //Main doc temp file.
			fso.DeleteFile(strForm);
		//Header doc temp file.
		var headerFile = GetMergeHeaderFileName();
		if (fso.FileExists(headerFile))
			fso.DeleteFile(headerFile);
	
	}catch (e){;}
	Cancel_MergeTemplate();
	
	return true;
}

function decode(str)
{
	return unescape( str );
}

function encode( str ) {return escape( str );}

function Finish_CreateTemplate4()
{
    var step = '';
    //store word file as base64 text in hidden field

    // RMA-8407 : MITS-36022 : Mudabbir 
    var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
    if (document.getElementById("hdnSilver") != null)
        useSilverlight = document.getElementById("hdnSilver").value;
    else
        useSilverlight = !IEbrowser;
    //if (useSilverlight.toString() == "true") {
    if (useSilverlight == true || useSilverlight == "true" || !IEbrowser){
        if (Silverlight.isInstalled("5.0")) {
            document.getElementById("Silverlight").Value = "ClientBin/RiskmasterSL.xap";
            RiskmasterSL = document.getElementById("Silverlight");
            strForm = RiskmasterSL.Content.SL2JS.GetDocumentPath(strFile, "", "");
            var ret = RiskmasterSL.Content.SL2JS.Finish_CreateTemplate(strFile);
            if (ret == "true")
                return true;
            else
                return false;
        }
        else
            alert(parent.CommonValidations.ValidMergeMergeSilverlight);
    }
   

        //Mudabbir ends
    else {
        try {


            if (IsWordOpen_CreateTemplate4() && objWord.Documents.Count == 1) {
                //Aman ML Changes
                //step = 'activate Word';
                step = parent.CommonValidations.ValidActivateWordStepCheck;
                objWord.Activate();

            //step='get pointer to merged document';
            step = parent.CommonValidations.ValidPointerMergedDocCheck;
            var wordDoc = objWord.Documents.Item(1);

            //step='select, save and close the document';
            step = parent.CommonValidations.ValidSaveCloseCheck;
            wordDoc.Select();
            wordDoc.Save();   
            wordDoc.Close();           
        }
        
        //if editing, and the user never clicked "Launch Editor", then the
        //file was never written to disk
        if (fso.FileExists(strForm))
        {
	        var adTypeBinary = 1;
	        //step = 'instantiate Stream object';
	        step = parent.CommonValidations.ValidInstantiateStreamObjCheck;
            var stm = new ActiveXObject("ADODB.Stream");
            //step = 'set stm.Type';
            step = parent.CommonValidations.ValidSetStmTypeCheck;
            stm.Type = adTypeBinary;
            //step = 'open stream';
            step = parent.CommonValidations.ValidOpenStreamCheck;
            stm.Open();
            //step = 'read file ' + strForm + ' into stream';
            step = parent.CommonValidations.ValidReadFileCheck + " " + strForm + " " + parent.CommonValidations.ValidReadFileStreamCheck;
            stm.LoadFromFile(strForm);
            //step = 'read bytes from stream';
            step = parent.CommonValidations.ValidReadBytesStreamCheck;
            var bytes = stm.Read();
            //step = 'close stream';
            step = parent.CommonValidations.ValidCloseStreamCheck;
            stm.Close();
            stm = null;

            //step = 'instantiate xml document';
            step = parent.CommonValidations.ValidInstantiateXMLDocCheck;
            var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
            xmlDoc.async = false;
            //step = 'create root element';
            step = parent.CommonValidations.ValidRootElementCheck;
            var xmlElement = xmlDoc.createElement("root");
            xmlDoc.documentElement = xmlElement;
            xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
            //step = 'create element to hold word document';
            step = parent.CommonValidations.ValidHoldWordDocCheck;
            var xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
            xmlNode.dataType = "bin.base64"
            //step = 'read stream into element';
            step = parent.CommonValidations.ValidReadStreamEltCheck;
            xmlNode.nodeTypedValue = bytes;
            //step = 'read base64 text from element';
            step = parent.CommonValidations.ValidReadBase64TextCheck;
            var base64 = xmlNode.text; 
            
            step='cleanup';
            xmlDoc = null;
            xmlElement = null;
            xmlNode = null;
                
	        document.getElementById('hdnformfilecontent').value = base64;
            //nkaranam2
            document.getElementById('hdnfilename').value = strFile;
	    }
    }
    catch (e)
    {
        alert(parent.CommonValidations.ValidLocalCopyCheck + " " + strForm + " " + parent.CommonValidations.ValidNotSavedCheck + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
	    xmlDoc = null;
        xmlElement = null;
        xmlNode = null;
	    return false;
    }
	
	//Clean Up    
	try 
	{
	    //step = 'close word';
	    step = parent.CommonValidations.ValidCloseWordCheck;
		if(IsWordOpen_CreateTemplate4())
		{	
			try {objWord.Quit(0);} catch(e){;}
			objWord = null;
		}
        //step = 'delete local copy of document at ' + strForm;

        step = parent.CommonValidations.ValidDeleteLocalCopyCheck + " " + strForm;
		if (fso.FileExists(strForm)) //Main doc temp file.
			fso.DeleteFile(strForm);
		
		var headerFile = GetMergeHeaderFileName();
		//step = 'delete header file at ' + headerFile;
		step = parent.CommonValidations.ValidDeleteHeaderFileCheck + " " + headerFile;
		if (fso.FileExists(headerFile))
			fso.DeleteFile(headerFile);
	}
	catch (e)
	{
	    //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
	    alert(parent.CommonValidations.ValidLocalCopyCleanUpCheck + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
	}
	
	//post back
	try
	{
	    //step="post back to server";
	    step = parent.CommonValidations.ValidPostBackServerCheck;
	
	}
	catch (e)
	{
	    //alert("The word document could not be sent back to Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
	    alert(parent.CommonValidations.ValidErrorCheck + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
		return false;
	}
	
        return true;
    }
}

function AnyMergeFields() {// RMA-8407 : MITS-36022 : Mudabbir 
    alert(parent.CommonValidations.ValidCreateTemplateStep15);
}
function GenerateMergeFieldHeaderString()
{
	//alert('entered generatemergefieldsheaderstring');
	var i;
	var str = new String("");
	if(typeof(document.getElementById('selectedfields')) =="undefined")
		return ""
	var arrSelectedFields;

	arrSelectedFields = document.getElementById('selectedfields').value.split(',');

	for(i=1;i<arrSelectedFields.length;i++)
	{
		if (i!=1)
			str = str + '\t';
		str = str + '"' +  String(arrSelectedFields[i]).replace(/[^_]*_[^_]*_(.*)/, "$1").replace(/[, ]/g,"_") +'"';
	}

    return str.replace(/[^0-9a-zA-Z,\t_\[comma\]"]/g, "").replace(/\[comma\]/gi, "\,");

}


function SaveFileContent(obj)
{// RMA-8407 : MITS-36022 : Mudabbir 
	if(obj!=null)
    document.getElementById('hdnformfilecontent').value = obj;
}

function IsWordOpen_CreateTemplate4()
{

	//Handle Case where User has not yet closed Word. (Do not open duplicate windows.)
	try
	{
		if (objWord.visible != null)
			return true;
		else
			return false;
	}
	catch(e){return false;}

}

function StartWord_EditTemplate4()
{
    return StartWord_CreateTemplate4();
}

function StartWord_CreateTemplate4()
{
	var adTypeBinary = 1;
    var adSaveCreateOverWrite = 2;
    var adTypeText = 2;
	var step = '';
	var bSingleField;
	var bNoFields = false;
	var mergeHeaderFileName;
	var mergeFieldHeaders;
	var wordDoc;
	var createEmptyWordDoc = false;
	  //alert ("hi1");
	//save files to disk

    // RMA-8407 : MITS-36022 : Mudabbir 
    var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
    if (document.getElementById("hdnSilver") != null)
        useSilverlight = document.getElementById("hdnSilver").value;
    else
        useSilverlight = !IEbrowser;
    //if (useSilverlight.toString() == "true") {
        if (useSilverlight == true || useSilverlight == "true" || !IEbrowser){
        if (Silverlight.isInstalled("5.0")) {

            if (document.getElementById('hdnformfilecontent') != null) {
                document.getElementById("Silverlight").Value = "ClientBin/RiskmasterSL.xap";
                RiskmasterSL = document.getElementById("Silverlight");
                strForm = RiskmasterSL.Content.SL2JS.GetDocumentPath(strFile, "", "");
                var filecontentTemplate = (document.getElementById('hdnformfilecontent').value);
                var ret = RiskmasterSL.Content.SL2JS.StartWord(filecontentTemplate, lDocType,strFile);
                if (ret == "true")
                    return true;
                else
                    return false;
            }

            else
                return false;
        }
        else
            alert(parent.CommonValidations.ValidMergeMergeSilverlight);
    }
   
        //Mudabbir ends
    else {
        try {
            if (IsWordOpen_CreateTemplate4()) {
	        //step = 'Activate Word and return false';
	        step = parent.CommonValidations.ValidCreateTemplateStep1;
		    objWord.Activate();
		    return false;
	    }
		//step = 'get template base64 text';
		step = parent.CommonValidations.ValidCreateTemplateStep2;
	    var template = (document.getElementById('hdnformfilecontent').value);
	    if(template.length > 0)
	    {
	        //use xml object to convert base64 into array of bytes
	        //step = 'instantiate xml document';
	        step = parent.CommonValidations.ValidCreateTemplateStep3;	        
	        var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
	        //step = 'create b64 element';
	        step = parent.CommonValidations.ValidCreateTemplateStep4;	
	        var e = xmlDoc.createElement("b64");
	        //step = 'set element.dataType to bin.base64';
	        step = parent.CommonValidations.ValidCreateTemplateStep5;	
	        e.dataType = "bin.base64";
	        //step = 'set element.text to template base64 text';
	        step = parent.CommonValidations.ValidCreateTemplateStep6;	
            e.text = template;
            //step = 'get bytes from template element';
            step = parent.CommonValidations.ValidCreateTemplateStep7;	
            var templateBytes = e.nodeTypedValue;
            
            //use ADODB.Stream to save template bytes to disc
            //step = 'instantiate Stream object for template';
            step = parent.CommonValidations.ValidCreateTemplateStep8;	
            var stm = new ActiveXObject("ADODB.Stream");
            //step = 'set template stm.Type';
            step = parent.CommonValidations.ValidCreateTemplateStep9;	
            stm.Type = adTypeBinary;
            //step = 'open template stream';
            step = parent.CommonValidations.ValidCreateTemplateStep10;	
            stm.Open();
            //step = 'write bytes to template stream';
            step = parent.CommonValidations.ValidCreateTemplateStep11;	
            stm.Write(templateBytes);
            //step = 'save template stream to file ' + strForm;
            step = parent.CommonValidations.ValidCreateTemplateStep12 + " " + strForm;	
            stm.SaveToFile(strForm, adSaveCreateOverWrite);
            //step = 'close stream';
            step = parent.CommonValidations.ValidCreateTemplateStep13;	
            stm.Close();
            stm = null;
            
            e = null;
            xmlDoc = null;
        }
        else
        {
            createEmptyWordDoc = true;
        }
	   
	    //write merge field headers to disk if they exist
        //step = 'get merge field headers';
        step = parent.CommonValidations.ValidCreateTemplateStep14;	
	    //alert ("hi2");
	    mergeFieldHeaders = GenerateMergeFieldHeaderString();
	     //alert ("hi3" +mergeFieldHeaders);
	    if(mergeFieldHeaders == '')
	    {
	       bNoFields = true;
	       //alert('For your information, there are no Riskmaster merge fields in this document.');
	       alert(parent.CommonValidations.ValidCreateTemplateStep15);	
	    }
	    else
	    {   
	        mergeHeaderFileName = GetMergeHeaderFileName();
	        if (new String(mergeFieldHeaders).indexOf('\"\t\"') == -1)
	        {
	            //this is done to suppress MS Word prompt asking "what is the record delimiter?"
			    bSingleField = true;
			}
			else
			{
			    //step = 'write merge field headers to disk at ' + mergeHeaderFileName;
			    step = parent.CommonValidations.ValidCreateTemplateStep16 + " " + mergeHeaderFileName;	
	            var objFile = fso.CreateTextFile(mergeHeaderFileName,true);
	            objFile.WriteLine(mergeFieldHeaders);
		        objFile.Write(mergeFieldHeaders);
	            objFile.Close();
	            objFile = null;
	        }
	    }
    }
    catch (e)
    {
        //return DisplayError("The word template and headers could not be saved to this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);		
        return DisplayError(parent.CommonValidations.ValidCreateTemplateStep17 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);		
    }
    
    //open word and the template file
    try
    {
        //step="The local Microsft Word application required to complete the wizard could not be loaded.\nPlease verify that Microsoft Word is correctly installed.";
        step = (parent.CommonValidations.ValidCreateTemplateStep18).split('\\n').join('\n');
        objWord = new ActiveXObject("Word.Application");
        
        var version = parseInt(objWord.Version);
        
        /* tkr this hack is needed for Word 2003 and less (version 11.x and less).  
        not needed for 2007 (version 12.0 and greater).  it does not cause
        an error when called for Word 2007, but does not seem to do anything either */
        //this must occur before document loads!
        
        if(version < 12)
        {
            //step="The 'Insert Merge Field' toolbar control could not be added to Microsoft Word.\nPlease verify that Microsoft Word is properly installed and click the [Launch Word] button again.";
            step = (parent.CommonValidations.ValidCreateTemplateStep19).split('\\n').join('\n');
            var i, ctls, myCtl; 
            var bMergeFieldsVisible =false;
            ctls = objWord.Commandbars("Mail Merge").Controls;
            for (i=1; i<ctls.Count;i++)
            {
                if (ctls(i).Id == 30076)
                {
                    bMergeFieldsVisible = true;
                    break;
                }
            }
            if (bMergeFieldsVisible == false)
            {
                myCtl = ctls.Add(10,30076,"",2);
                myCtl.Reset();
            }
        }

        //step = 'Open Word Document ' + strForm;
        step = parent.CommonValidations.ValidCreateTemplateStep20 + " " +strForm;
        if(createEmptyWordDoc)
        {
            wordDoc = objWord.Documents.Add();
            //wordDoc.SaveAs(strForm, 0);
            //nkaranam2
            if (version < 12.0) {
                wordDoc.SaveAs(strForm, 0);
            }
            else if (version == 12.0 || version == 14.0 || version == 15.0) {
                if (strForm.indexOf(".docx") == -1)
                    strForm = strForm.replace(".doc", ".docx");
                if (strFile.indexOf(".docx") == -1)
                    strFile = strFile.replace(".doc", ".docx");
                objWord.ActiveDocument.Saveas(strForm, 12);
            }
        }
        else {
            //nkaranam2
            if (version < 12.0) {
                //wordDoc = objWord.Documents.Open(strForm, false, false, false);
                //User can not open ".docx" template with Office 2003 installed
                if (strForm.indexOf(".docx") > -1)
                {
                    alert(parent.CommonValidations.MSWordVersionCheck); //A newer version of Microsoft Office is necessary to open this template.
                    if (objWord != null) {
                        objWord.Quit(0);
                        objWord = null;
                    }
                    return false;
                }
                else
                wordDoc = objWord.Documents.Open(strForm, false, false, false);
            }
            else if (version == 12.0 || version == 14.0 || version == 15.0) {
                wordDoc = objWord.Documents.Open(strForm, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1);
                objWord.visible = true;
                objWord.Activate();

                if (strForm.indexOf(".docx")==-1)
                    strForm = strForm.replace(".doc", ".docx");
                if (strFile.indexOf(".docx") == -1)
                    strFile = strFile.replace(".doc", ".docx");

                objWord.ActiveDocument.Saveas(strForm, 12);
                wordDoc = objWord.Documents.Open(strForm);
            }
        }

        //step="The most recent version of the form is protected from changes.\nPlease unlock it and click the [Launch Word] button again.";
        step = (parent.CommonValidations.ValidCreateTemplateStep21).split('\\n').join('\n');
        if( wordDoc.ProtectionType != -1) //if not totally unlocked then
		    wordDoc.UnProtect("");

        //step="The merge fields at " + mergeHeaderFileName + ' could not be added to the document.';
		step = parent.CommonValidations.ValidCreateTemplateStep22 + " " + mergeHeaderFileName + " " + parent.CommonValidations.ValidCreateTemplateStep23;
		wordDoc.Select();
		wordDoc.MailMerge.MainDocumentType = lDocType;

	    if(!bNoFields)
		    if(!bSingleField)
			    wordDoc.MailMerge.OpenDataSource(mergeHeaderFileName,0,1);
		    else
			    wordDoc.MailMerge.CreateDataSource(mergeHeaderFileName,"","",mergeFieldHeaders);

		    //step='select and show document';
			step = parent.CommonValidations.ValidCreateTemplateStep24;
	    objWord.visible = true;
	    objWord.Activate();	
	    
    }
    catch(e)
    {
        //DisplayError("The word template and headers could not be opened on this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        DisplayError(parent.CommonValidations.ValidCreateTemplateStep25 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
        if(wordDoc != null)
        {
            try {wordDoc.Close();} catch(e){;}
            objWord = null;
        }
        if(objWord != null)
        {
            try {objWord.Quit(0);} catch(e){;}
            objWord = null;
        }
	    return false;
    } 
	
	return false;
    }
}

function Finish_EditTemplate4()
{
    return Finish_CreateTemplate4();
}

function Validate_MergeEditTemplate3()
{
	//Do not store values until end of wizard.
	// values are tracked in hidden fields
	// maintained through posting and asp.
if ( document.getElementById('hdnisallowallperms').value=='false' && document.getElementById('selectedperms').options.length==0)
{
    alert(parent.CommonValidations.ValidAssignUserCheck); //Please assign at least one user or check allow all.
    return false;
}
	var i;
	var ListBox;
	var bAllowAll;

	bAllowAll = document.getElementById('allowall').value;

	//Turn off selections in Available List box so they ARE NOT
	// recorded and sent back to Server ASP.
	for(i=0;i<document.getElementById('availperms').options.length;i++)
			document.getElementById('availperms').options[i].selected = false;


	//Ensure that all fields are "Selected" in the
	// SelectedPerms listbox for correct submission
	// to ASP via POST.
	ListBox = document.getElementById('selectedperms');

	for(i=0;i<ListBox.options.length;i++)
		ListBox.options[i].selected = !document.getElementById('allowall').checked;

	//document.forms[0].action="MergeEditTemplate4.asp" ;

	//shakti : back support 03-09-2006
	document.getElementById('hdnselectedperms').value = "";
	for (var i=0;i<document.getElementById('selectedperms').options.length;i++)
	{
		document.getElementById('hdnselectedperms').value = document.getElementById('hdnselectedperms').value + ';' + document.getElementById('selectedperms').options[i].text;	
	}//end
	document.getElementById('hdnselectedpermsids').value = "";
	
	for (var i=0;i<document.getElementById('selectedperms').options.length;i++)
	{
		document.getElementById('hdnselectedpermsids').value = document.getElementById('hdnselectedpermsids').value + ' ' + document.getElementById('selectedperms').options[i].value;	
	}//end

    //document.getElementById('hdnSelectedpermsusers').value="";
	document.getElementById('hdnselectedpermsusers').value = ""; //Jira: 7486 -govind

	//document.getElementById('hdnSelectedpermsgroups').value = "";
	document.getElementById('hdnselectedpermsgroups').value = ""; //Jira: 7486 -govind
	
	if(!document.getElementById('allowall').checked)
	{
		for (i=0;i<document.getElementById('selectedperms').options.length;i++)
		{
			if (document.getElementById('selectedperms').options[i].value.charAt(0) == 'g')
			{
			    //document.getElementById('hdnselectedpermsgroups').value = document.getElementById('hdnselectedpermsgroups').value + ',' + document.getElementById('Selectedperms').options[i].value.substring(6,document.getElementById('selectedperms').options[i].value.length);
			    document.getElementById('hdnselectedpermsgroups').value = document.getElementById('hdnselectedpermsgroups').value + ',' + document.getElementById('selectedperms').options[i].value.substring(6, document.getElementById('selectedperms').options[i].value.length); //Jira: 7486 -govind
			}
			else
			{
			    //document.getElementById('hdnselectedpermsusers').value = document.getElementById('hdnselectedpermsusers').value + ',' + document.getElementById('Selectedperms').options[i].value.substring(5, document.getElementById('selectedperms').options[i].value.length);
			    document.getElementById('hdnselectedpermsusers').value = document.getElementById('hdnselectedpermsusers').value + ',' + document.getElementById('selectedperms').options[i].value.substring(5, document.getElementById('selectedperms').options[i].value.length); //Jira: 7486 -govind
			}
		}
	}
	else
	{
		document.getElementById('hdnselectedpermsgroups').value=',0';
		document.getElementById('hdnselectedpermsusers').value=',0';
	}
	
	if (document.getElementById('hdnselectedpermsgroups').value=="")	
	{
		document.getElementById('hdnselectedpermsgroups').value=',0';
	}

	if (document.getElementById('hdnselectedpermsusers').value=="")	
	{
		document.getElementById('hdnselectedpermsusers').value=',0';
	}
	//document.getElementById('hdnaction').value="MergeEditTemplate4";
        document.getElementById('hdnisallowallperms').value = document.getElementById('allowall').checked;
	//document.forms[0].submit();
	// Always return values from function req. for Netscape compatiblity
	return true;
}


function back_temp4()
{
	//document.getElementById('hdnaction').value="back_temp4";
	
}


function AfterMergeSave()
{
	document.getElementById('hdnaction').value="MergeListTemplates";
}

function setdefaults()
{    //MITS 9159
	//if((document.forms[0].lettertype.value == document.forms[0].claimcategorycode.value) || (document.forms[0].lettertype.value == document.forms[0].eventcategorycode.value))
	
	/*if(document.forms[0].lettertype.value == document.forms[0].claimcategorycode.value)
		document.forms[0].lob.disabled = false;
	else
	{
	    document.forms[0].lob.value = -1;
		document.forms[0].lob.disabled = true;
	}*/
    if(document.forms[0].allstatesselected.checked == true)
	{
		document.forms[0].txtState.disabled = true;
		if (document.forms[0].txtStatebtn!=null)
		document.forms[0].txtStatebtn.disabled = true;
	}
	else
	{
		document.forms[0].txtState.disabled = false;
		if (document.forms[0].txtStatebtn!=null)
		document.forms[0].txtStatebtn.disabled = false;
	}
	//Mona:PaperVisionMerge : Animesh Inserted MITS 16697
	if (document.forms[0].PaperVisionActive !=null)
	{   
	    if(document.forms[0].PaperVisionActive.value == "True" && document.forms[0].TableName.value != "claim")
	    {
	        document.forms[0].attach.disabled=true;
	        alert(parent.CommonValidations.ValidPaperVisionForClaimsCheck); //PaperVision Document Management is active. Attachment functionality will only be available for claims.
    	    
	    }
	    else
	    {
	        if (document.forms[0].PaperVisionValidation != null)
	        {
                if (document.forms[0].PaperVisionActive.value == "True" && document.forms[0].TableName.value == "claim" && document.forms[0].PaperVisionValidation.value == "False")
                {
                    document.forms[0].attach.disabled=true;
                    alert(parent.CommonValidations.ValidPaperVisionDetailsMissing); //PaperVision Document Management is active. Attachment functionality will not be available since all the required PaperVision connectivity details are not available in the configuration file.
                }         	        
	        }
	    }
	}
	//Animesh Insertion Ends
	
	//Shruti for 11412
    	bAdd = "true";
    //Shruti for 11412 ends
   
    //Shruti 9629
    document.forms[0].lettername.focus();
}
function displayemailoption() {
    //spahariya MITS 28867
    if (document.forms[0].TableName.value == "claim" || document.forms[0].TableName.value == "event") {
        document.forms[0].lstMailRecipient.style.visibility = "visible";
        document.forms[0].lstMailRecipient.style.display = "inline";
        document.forms[0].EmailCheck.style.visibility = "visible";
        document.forms[0].EmailCheck.style.display = "inline";
        document.forms[0].lstMailRecipient.disabled = true;
        if (document.forms[0].lettername.value == null || document.forms[0].lettername.value == "0") {            
            document.forms[0].EmailCheck.disabled = true;
        }

        //JIRA RMA-1287 nshah28 start
        document.getElementById('lblMailRecipient').style.visibility = "visible";
        document.getElementById('lblMailRecipient').style.display = "inline";
        document.getElementById('lblEmailCheck').style.visibility = "visible";
        document.getElementById('lblEmailCheck').style.display = "inline";
        //JIRA RMA-1287 nshah28 end
    }
    else {
        document.getElementById('lblMailRecipient').style.visibility = "hidden";
        document.getElementById('lblMailRecipient').style.display = "none";
        document.forms[0].lstMailRecipient.style.visibility = "hidden";
        document.forms[0].lstMailRecipient.style.display = "none";
        document.getElementById('lblEmailCheck').style.visibility = "hidden";
        document.getElementById('lblEmailCheck').style.display = "none";
        document.forms[0].EmailCheck.style.visibility = "hidden";
        document.forms[0].EmailCheck.style.display = "none";
    }

}
function enablelobifclaimselected()
{
    //MITS 9159
	//if((document.forms[0].lettertype.value == document.forms[0].claimcategorycode.value) || (document.forms[0].lettertype.value == document.forms[0].eventcategorycode.value))
	if(document.forms[0].lettertype.value == document.forms[0].claimcategorycode.value)
		document.forms[0].lob.disabled = false;
	else
	{
	    document.forms[0].lob.value = -1;
		document.forms[0].lob.disabled = true;
	}
    enableSendEmail()
}
function alterstateselection()
{
	if(document.forms[0].allstatesselected.checked == true)
	{
		document.forms[0].txtState.disabled = true;
		document.forms[0].txtStatebtn.disabled = true;
		document.forms[0].txtState.value="";
		document.forms[0].txtState_cid.value="";
	}
	else
	{
		document.forms[0].txtState.disabled = false;
		document.forms[0].txtStatebtn.disabled = false;
	}
	
}
function onCodeClose()
{
	m_codeWindow=null;
	return true;
}
function setdefaultsforedit()
{
    //MITS 9159
	//if(document.forms[0].eventcategorycode.value == 'true' || document.forms[0].claimcategorycode.value == 'true')
//	if(document.forms[0].claimcategorycode.value == 'true')
//		document.forms[0].lob.disabled = false;
//	else
//	{
//	    document.forms[0].lob.value = -1;
//		document.forms[0].lob.disabled = true;
//	}
	if(document.forms[0].allstatesselected.checked == true)
	{
		document.forms[0].txtState.disabled = true;
		document.forms[0].txtStatebtn.disabled = true;
	}
	else
	{
		document.forms[0].txtState.disabled = false;
		document.forms[0].txtStatebtn.disabled = false;
	}
	document.forms[0].method="post";
	
	bAdd = "false";
	sTemplateName = document.forms[0].lettername.value;
    //added by swati for AIC Gap 8 MITS # 36930
	if (document.getElementById("EmailCheck").checked == false) {
	    document.getElementById("lstMailRecipient").disabled = true;
	}
    //change end here
	
	//Shruti 9629
    document.forms[0].lettername.focus();
}
function mailmergedefaults()
{   //Shruti, 9676
	if(document.forms[0].allstatesselected.checked == true)
	{
	    document.forms[0].txtState.value = "";
		document.forms[0].txtState.disabled = true;
		document.forms[0].txtStatebtn.disabled = true;
	}
	else
	{
		document.forms[0].txtState.disabled = false;
		document.forms[0].txtStatebtn.disabled = false;
	}
}
function validate_filter()
{

	if(document.forms[0].allstatesselected.checked != true && document.forms[0].txtState.value=="")
	{
		alert(parent.CommonValidations.ValidateStateInfoResrc);//Please enter the State information.
		return false;
	}
	//Raman Bhatia ..Removing Validation as suggested by Mike
	/*
	if(document.forms[0].txtmergedocumenttype.value=="")
	{
		alert("Please enter the type of merge document.");
		return false;
	}
	if(document.forms[0].txtmergedocumentformat.value=="")
	{
		alert("Please enter the format of merge document.");
		return false;
	}
	*/
	return true;
	
}
function goBack1()
{
	
	if( document.getElementById('hdnPreviousAction')!= null && document.getElementById('hdnPreviousAction').value == 'Next1FetchData')
		document.getElementById('hdnAction').value="previousaction";
	else
		document.getElementById('hdnAction').value="back";

	document.forms[0].target="";
}

function goBackMergeTemplate()
{
	try{
	document.getElementById('hdnexecutepipeline').value = "true";
	}catch(e){;}
}

function goBackMergeTemplate3()
{
	if(document.getElementById('hdnpreviousaction')!=null &&  document.getElementById('hdnpreviousaction').value != '')
		document.getElementById('hdnAction').value= document.getElementById('hdnpreviousaction').value;
	else
		document.getElementById('hdnAction').value="back";
	try{
	document.getElementById('hdnexecutepipeline').value = "true";
	document.getElementById('hdnRTFContent').value="";
	document.getElementById('hdnformfilecontent').value="";
	document.getElementById('hdnfilename').value="";
	}catch(e){;}
}

function InitPageSettings_MergeTemplate3()
{
	document.getElementById('selectedperms').style.width=document.getElementById('availperms').offsetWidth;
	
	var arrSelectedFields= new Array();
	var arrSelectedFieldIds=new Array();
	arrSelectedFields = document.getElementById('hdnselectedperms').value.split(';');
	arrSelectedFieldIds = document.getElementById('hdnselectedpermsids').value.split(' ');
	try{
	for (i=1;i<arrSelectedFields.length;i++)
	{
		var sFieldSel  = new String(arrSelectedFields[i].valueOf());
		var sFieldSelId = new String(arrSelectedFieldIds[i-1].valueOf());
		document.getElementById('selectedperms').options[document.getElementById('selectedperms').options.length] = new Option (sFieldSel, sFieldSelId, false, false);					
	} 
	}catch(e){;}
}

function backMergeTemplate2()
{
 	//shakti : back support 03-09-2006
	document.getElementById('hdnselectedperms').value = "";
	for (var i=0;i<document.getElementById('selectedperms').options.length;i++)
	{
		document.getElementById('hdnselectedperms').value = document.getElementById('hdnselectedperms').value + ';' + document.getElementById('selectedperms').options[i].text;	
	}//end
	document.getElementById('hdnselectedpermsids').value = "";
	for (var i=0;i<document.getElementById('selectedperms').options.length;i++)
	{
		document.getElementById('hdnselectedpermsids').value = document.getElementById('hdnselectedpermsids').value + ' ' + document.getElementById('selectedperms').options[i].value;	
	}//end
	goBackMergeTemplate();
	return true;
}
function backMergeTemplate1()
{
 	//shakti : back support 03-09-2006
	document.getElementById('hdnselectedperms').value = "";
	for (var i=0;i<document.getElementById('selectedperms').options.length;i++)
	{
		document.getElementById('hdnselectedperms').value = document.getElementById('hdnselectedperms').value + ';' + document.getElementById('selectedperms').options[i].text;	
	}//end
	document.getElementById('hdnselectedpermsids').value = "";
	for (var i=0;i<document.getElementById('selectedperms').options.length;i++)
	{
		document.getElementById('hdnselectedpermsids').value = document.getElementById('hdnselectedpermsids').value + ' ' + document.getElementById('selectedperms').options[i].value;	
	}//end
	goBackMergeTemplate();
	return true;
}
function changeGetFile()
{
document.getElementById('hdnTemplateFile').value = "";
 document.getElementById('hdnTemplateFile').value = document.getElementById('prefab').value ;
}

//Shruti for MITS 11412
var bAdd = "";
var sTemplateName = "";
function CheckForDuplicacy()
{
    var ctrlTemplateName = document.getElementById('lettername');
    var ctrlTemplateType = document.getElementById('lettertype');
    if(bAdd == "true" || (bAdd == "false" && ctrlTemplateName.value != sTemplateName))
    {
        if(IsAlreadySelected(ctrlTemplateName.value, ctrlTemplateType.value))
		{
			ctrlTemplateName.select();
			ctrlTemplateName.focus();
			return true;
		}
    }
    return false;
}

function IsAlreadySelected(templateName, catId)
{
    var itemArray;
    var iCatId;
    var sTemplateNames;
    var array;
    if(bAdd == "true")
    {
        if(document.getElementById('hdnTemplateNames') != null && document.getElementById('hdnTemplateNames').value != "")
        {
	        var array = document.getElementById('hdnTemplateNames').value.split(';');
	        for(i = 0; i < array.length; i++)
            {
                var category = array[i];
                if(category != null && category.length > 0)
                {
                    itemArray = category.split('^');
                    iCatId = parseInt(itemArray[0]);
                    if(iCatId == catId)
                    {
                        sTemplateNames = itemArray[1];
                        break;
                    }
                    
                }
                
            }
        }
    }
    else
    {
        sTemplateNames = document.getElementById('hdnTemplateNames').value;
    }
    if(sTemplateNames != null && sTemplateNames != "")
    {
    array = sTemplateNames.split(',');
	    for(var f=0;f<array.length;f++)
	    {
		    if(Trim(templateName) == Trim(array[f]))
		    {
		        alert(parent.CommonValidations.ValidMergeTemplateName); //"The merge template with this name already exists.Please use another name.");
		        return true;
		    }
	    }
	}
	return false;
}

function Trim(sString) 
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}

//Shruti for MITS 11412 ends
//spahariya MITS 28867 - start 06/26/2012
function alterCopyMailselection() {
    if (document.forms[0].EmailCheck.checked == true) {
        document.forms[0].lstMailRecipient.disabled = false;        
    }
    else {
        document.forms[0].lstMailRecipient.disabled = true;
        document.forms[0].lstMailRecipient.value = 0;
    }
}


function lettername_IndexChanged() {
    var sLetterValue;
    var sArrLetter;
    var sRecipientData = "";
    var sText = "";
    var sValue = "";
    var sSelectedData = "";
    var ddTemplateName = document.getElementById('lettername');
    var lstMailRecipient = document.getElementById('lstMailRecipient');
    var chkEmailCheck = document.getElementById('EmailCheck');
    sLetterValue = ddTemplateName.value;
    sArrLetter = sLetterValue.split("***");
    //added by swati MITS # 36930
    for (var count = lstMailRecipient.options.length - 1; count >= 0; count--) {
        try {
            lstMailRecipient.remove(count, null);
        } catch (error) {
            lstMailRecipient.remove(count);
        }
    }
    //end here by swati
    if (sArrLetter[3] != "" && sArrLetter[3] == "-1") {         
        chkEmailCheck.checked = true;
        chkEmailCheck.disabled = false;
        //commented by swati
        //lstMailRecipient.value = sArrLetter[4];
        //added by swati MITS # 36930
        var sRecipient = sArrLetter[4].split('|');
        for (var f = 0; f < sRecipient.length; f++) {
            sRecipientData = sRecipient[f].split(',');
            sText = "";
            if (sRecipientData[2] == "-1") {
                sText = sRecipientData[0] + "*";
                sValue = sRecipientData[1] + "*";                
                //if (sValue == "") {
                //    sValue = sRecipientData[1] + "*";
                //}
                //else {
                //    sValue = sValue + "," + sRecipientData[1] + "*";
                //}
            }
            else {
                sText = sRecipientData[0];
                sValue = sRecipientData[1];
                //if (sValue == "") {
                //    sValue = sRecipientData[1];
                //}
                //else {
                //    sValue = sValue + "," + sRecipientData[1];
                //}
            }
            if (sSelectedData == "") {
                sSelectedData = sText + "," + sValue;
            }
            else {
                sSelectedData = sSelectedData + '|' + sText + "," + sValue;
            }
            var opt = new Option(sText, sRecipientData[1], false, false);
            if (opt.value != "") {
                document.getElementById("lstMailRecipient").options[document.getElementById("lstMailRecipient").options.length] = opt;
            }
        }
        //document.getElementById('txtSelectedRecipient').value = lstMailRecipient.value;
        document.getElementById('txtSelectedRecipient').value = sSelectedData;
        //change end here by swati
       
    }
    else {         
        chkEmailCheck.checked = false;
        chkEmailCheck.disabled = true;
        lstMailRecipient.value = 0;
		document.getElementById('txtSelectedRecipient').value = "";
    }

}

function enableSendEmail() {
     
    if (document.forms[0].lettertype.value == document.forms[0].claimcategorycode.value || document.forms[0].lettertype.value == document.forms[0].eventcategorycode.value) {
        if (document.forms[0].EmailCheck.checked == true) {
            document.forms[0].EmailCheck.disabled = false;
            document.forms[0].lstMailRecipient.disabled = false;
        }
        else {
            document.forms[0].EmailCheck.checked = false;
            document.forms[0].EmailCheck.disabled = false;
            document.forms[0].lstMailRecipient.disabled = true;
            document.forms[0].lstMailRecipient.value = 0;
        }
        }        
    else {
        document.forms[0].EmailCheck.checked = false;
        document.forms[0].EmailCheck.disabled = true;
        document.forms[0].lstMailRecipient.disabled = true;
        document.forms[0].lstMailRecipient.value = 0;
    }
}
// Pradyumna 11/10/2014 MITS 36930 - Commented - Start
//function SubPreview() {    
//    var s = "";
//    if (document.forms[0].SubText1.value != "")
//        s = s + document.forms[0].SubText1.value;

//    if (document.forms[0].SubVar1.value != "") {
//        objElem = eval('document.forms[0].SubVar1');
//        for (var j = 1; j < objElem.length; j++) {
//            if (objElem[j].value == document.forms[0].SubVar1.value) {
//                s = s + " " + objElem[j].innerText;
//            }
//        }
//    }

//    if (document.forms[0].SubText1.value != "")
//        s = s + " " + document.forms[0].SubText2.value;

//    if (document.forms[0].SubVar2.value != "") {
//        objElem = eval('document.forms[0].SubVar2');
//        for (var j = 1; j < objElem.length; j++) {
//            if (objElem[j].value == document.forms[0].SubVar2.value) {
//                s = s + " " + objElem[j].innerText;
//            }
//        }
//    }
//    alert(s);
//}

//function BodyPreview() {    
//    var s = "";
//    if (document.forms[0].BodyText1.value != "")
//        s = s + document.forms[0].BodyText1.value;

//    if (document.forms[0].BodyVar1.value != "") {
//        objElem = eval('document.forms[0].BodyVar1');
//        for (var j = 1; j < objElem.length; j++) {
//            if (objElem[j].value == document.forms[0].BodyVar1.value) {
//                s = s + " " + objElem[j].innerText;
//            }
//        }
//    }

//    if (document.forms[0].BodyText2.value != "")
//        s = s + " " + document.forms[0].BodyText2.value;

//    if (document.forms[0].BodyVar2.value != "") {
//        objElem = eval('document.forms[0].BodyVar2');
//        for (var j = 1; j < objElem.length; j++) {
//            if (objElem[j].value == document.forms[0].BodyVar2.value) {
//                s = s + " " + objElem[j].innerText;
//            }
//        }
//    }

//    if (document.forms[0].BodyText3.value != "")
//        s = s + " " + document.forms[0].BodyText3.value;
//    alert(s);
//}
// Pradyumna 11/10/2014 MITS 36930 - Commented - End
function SetRetMessage() {
    var objFlag = document.getElementById('hdnFlagMessage');
    var objMessage = document.getElementById('RetMessage');
    if (objMessage != null && objFlag != null) {
        if (objMessage.value != "") {
            objFlag.value = "DisplayMessage";
            document.forms[0].submit();
        }
    }
}

function ExistInMergeGrid() {
    var objFormId = window.opener.document.getElementById("HdnFormIdInGrid");
    var sTemplate = document.forms[0].Template.value;
    var objRowID = document.getElementById("txtSelectedid");
    var objEditTempID = document.getElementById("HdnTempIdEdit");
    if (objFormId != null) {
        if (objFormId.value != "") {
            var arrFormID = objFormId.value.split(",");
            for (var i = 0; i < arrFormID.length; i++) {
                if (arrFormID[i] == sTemplate) {
                    if (objRowID.value != "" && arrFormID[i] == objEditTempID.value) {
                        return true;
                    }
                    else {
                        //alert("The selected template already has email details. Select a different template or edit the existing email detail entry.");
                        alert(parent.CommonValidations.ExistingEmailTempl);
                        document.forms[0].Template.value = 0;
                        return false;
                    }
                }
            }
        }
    }
}
// RMA-8407 : MITS-36022 : Mudabbir 
function getAndSetValues(obj, set, get, value) {

    if (obj == "1")
        Editor_Launched = 1;
    else {
        if (obj != null) {
            if (get == "yes") {

                var getvalue = eval(obj);
                if (getvalue.value == undefined)
                    return getvalue;
                else
                    return getvalue.value;
            }
            if (set == "yes") {
                var setvalue = eval(obj);
                setvalue.value = value;
            }
        }
    }
}

function DisplayErrorfromSilver(errMsg) {
    alert(errMsg);
    //document.write('<h3>Error Message</h3><p>' + errMsg + '</p><br />' + _mergeFailureErrorMessage); 
    document.write('<h3>' + parent.CommonValidations.ValidMergeMergeErrorMessage + '</h3><p>' + errMsg + '</p><br />');
    return false;
}
