 /**************************************************************
	 * $File		: EnhPolicy.cs
	 * $Revision	: 1.0
	 * $Date		: 28/Mar/2007
	 * $Author		: Vaibhav Kaushik
	 * $Comment		: 
	 * $Source		:  	
	**************************************************************/

var m_DataChanged=false;
var m_bUpdateType = "" ;
var TRANSACTION_GRID = "TransactionHistoryGrid" ;

function OnEnahncePolicyTypeChange() 
{
    var bUseBillingSystem = false;
    pleaseWait.Show();
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;

    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    if (bUseBillingSystem) 
    {
        var chkDoNotBill = eval('document.forms[0].donotbill');
        chkDoNotBill.disabled = false;
        var chkBillOverride = eval('document.forms[0].billtooverride');
        chkBillOverride.disabled = false;
    }
    window.document.forms[0].PostBackAction.value = "POLICY_STATE_CHANGE";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
}

function OnEnahncePolicyStateChange() 
{
    var bUseBillingSystem = false;
    pleaseWait.Show();
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;

    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    if (bUseBillingSystem) 
    {
        var chkDoNotBill = eval('document.forms[0].donotbill');
        chkDoNotBill.disabled = false;
        var chkBillOverride = eval('document.forms[0].billtooverride');
        chkBillOverride.disabled = false;
    }
    window.document.forms[0].PostBackAction.value = "POLICY_STATE_CHANGE";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
}

//Start (07/06/2010) MITS# 20820: Sumit
function OnEnhancePolicyDateChange() 
{
    pleaseWait.Show();
    m_bConfirmSave = false;
    window.document.forms[0].PostBackAction.value = "POLICY_DATE_CHANGE";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
}
//End:Sumit

/* Premium Calculation Change Begin */
function OnWaivePremiumChecked()
{
    var bUseBillingSystem = false;
    pleaseWait.Show();
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;

    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    if (bUseBillingSystem) 
    {
        var chkDoNotBill = eval('document.forms[0].donotbill');
        chkDoNotBill.disabled = false;
        var chkBillOverride = eval('document.forms[0].billtooverride');
        chkBillOverride.disabled = false;
    }
    window.document.forms[0].PostBackAction.value = "CALCULATION_PREMIUM_CHANGED";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
}

function OnExpModFactorChange()
{
    // npadhy Start MITS 18841 In RMWorld teh Exp. Mod. Factor can not be Negative.
    // Implemented the same with a Message
    if (document.getElementById('ExpModFactor') != null) 
    {
        var dblExpModfactor = document.getElementById('ExpModFactor').value;
        if (dblExpModfactor < 0) 
        {
            alert('The Exp. Mod. Factor can not be Negative');
            document.getElementById('ExpModFactor').value = 0;
            return false;
        }
    }
    // npadhy eND MITS 18841 In RMWorld teh Exp. Mod. Factor can not be Negative.
    // Implemented the same with a Message
    
    var bUseBillingSystem = false;
    pleaseWait.Show();
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;

    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    if (bUseBillingSystem) 
    {
        var chkDoNotBill = eval('document.forms[0].donotbill');
        chkDoNotBill.disabled = false;
        var chkBillOverride = eval('document.forms[0].billtooverride');
        chkBillOverride.disabled = false;
    }
    window.document.forms[0].PostBackAction.value = "CALCULATION_PREMIUM_CHANGED";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
}

function RangeSelectionOnLoad()
{
    loadTabList();
}
//Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
function ModifierRangeSelectionOnLoad()
{
    loadTabList();
}

//Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
function RangeSelectionOK()
{
    var iRowId = 1;
    var inputs = document.getElementsByTagName("input");
    
    for (i = 0; i < inputs.length; i++) 
    {
        if ((inputs[i].type == "radio") && (inputs[i].checked == true)) 
        {
            iRowId = GetPositionForSelectedGridRow("RangeSelectionGrid", inputs[i].value)
            break;
        }
    }    
        
    if( iRowId == 1 )
    {
        alert( "No Term selected." );
        return false ;
    }
    //if (iRowId <= 9)
    //{
    //    iRowId = "0" + iRowId;
    //}
    
    //var iTermNum = document.getElementById("RangeSelectionGrid_gvData_ctl" + iRowId + "_hfGrid").value;
    var iTermNum = document.getElementById("RangeSelectionGrid_gvData_hfGrid_"+(iRowId-2)).value;
    window.returnValue = iTermNum;
    window.close(); 
}

//Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
function ModifierRangeSelectionOK()
{
    var iRowId = 1;
    var inputs = document.getElementsByTagName("input");
    var iTermNum;
    
    for (i = 0; i < inputs.length; i++) 
    {
        if ((inputs[i].type == "radio") && (inputs[i].checked == true)) 
        {
            iRowId = GetPositionForSelectedGridRow("ModifierRangeSelectionGrid", inputs[i].value)
            break;
        }
    }    
        
    if( iRowId == 1 )
    {
        alert( "No Term selected." );
        return false ;
    }
    //if (iRowId <= 9)
    //{
    //    iRowId = "0" + iRowId;
    //}
    
    if (document.getElementById("ModifierRangeSelectionGrid_gvData_hfGrid_"+(iRowId-2)) != null)
    {
        iTermNum = document.getElementById("ModifierRangeSelectionGrid_gvData_hfGrid_" + (iRowId - 2)).value;
    }
    window.returnValue = iTermNum;
    window.close(); 
}

//Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
function OnDiscountFactorChange() 
{
    var bUseBillingSystem = false;
    pleaseWait.Show();
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;

    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    if (bUseBillingSystem) 
    {
        var chkDoNotBill = eval('document.forms[0].donotbill');
        chkDoNotBill.disabled = false;
        var chkBillOverride = eval('document.forms[0].billtooverride');
        chkBillOverride.disabled = false;
    }
    window.document.forms[0].PostBackAction.value = "CALCULATION_PREMIUM_CHANGED";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
}
//Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
function OnModifierChange()
{
    var bUseBillingSystem = false;
    pleaseWait.Show();
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;

    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    if (bUseBillingSystem) 
    {
        var chkDoNotBill = eval('document.forms[0].donotbill');
        chkDoNotBill.disabled = false;
        var chkBillOverride = eval('document.forms[0].billtooverride');
        chkBillOverride.disabled = false;
    }
     window.document.forms[0].PostBackAction.value = "CALCULATION_PREMIUM_CHANGED";
     window.document.forms[0].SysCmd.value = 7;
     window.document.forms[0].submit();
}
//Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
function RangeSelectionCancel()
{
    window.close()
}
//Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
function ModifierRangeSelectionCancel()
{
    SetDataChanged(false);
    window.close()
}
//Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
    
//pmahli 2/2/2007 EPA Discount Range Selection - Start
function OnRangeClicked(iDiscountId, iDiscountRowId) 
{
    var width = 550;
    var height = 275;
    var sQueryString ="";
    sQueryString = "DiscountRowId=" + iDiscountRowId + "&DiscountId=" + iDiscountId;
    var sReturnValue = showModalDialog("/RiskmasterUI/UI/EnhancedPolicy/RangeSelection.aspx?" + sQueryString, null, "dialogHeight:" + 250 + "px;dialogWidth:" + 580 + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");
    // If popup has been closed without "OK" .  
    if (sReturnValue == null || sReturnValue == "")
        return false;

    var sDiscountControl = document.getElementById("DiscountAmountFactor" + iDiscountId);

    if (sDiscountControl == null) {
        alert("An error has occurred!!!. Can not assign the value to the discount control.");
        return false;
    }

    sDiscountControl.value = sReturnValue;
    //rupal:start
    EnhPol_MultiCurrencyOnBlur(sDiscountControl); //  multicurrency enh 
    //rupal:end
    OnDiscountFactorChange();
    return false;            
}
/* Premium Calculation Change End*/

//Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
function OnModifierRangeClicked(iExpRateRowId, iModifierId)
{

    var width = 550;
    var height = 275;
    var sQueryString ="";
    var sUniqueId = iExpRateRowId + iModifierId;
    sQueryString = "ExpRateRowId=" + iExpRateRowId + "&ModifierId=" + iModifierId;
    var sReturnValue = showModalDialog("/RiskmasterUI/UI/EnhancedPolicy/ModifierRangeSelection.aspx?" + sQueryString, null, "dialogHeight:" + 250 + "px;dialogWidth:" + 300 + "px;Height:200px;resizable:no;status:no;scroll:no;help:no;center:yes;");
    // If popup has been closed without "OK" .  
    if (sReturnValue == null || sReturnValue == "")
        return false;

    var sModifierControl = document.getElementById("CoverageModiferAmountFactor" + sUniqueId);

    if (sModifierControl == null) {
        alert("An error has occurred!!!. Can not assign the value to the Modifier control.");
        return false;
    }

    sModifierControl.value = sReturnValue;
    EnhPol_MultiCurrencyOnBlur(sModifierControl); //rupal:mits 27502, this is actually part of multicurrency enh which was missing here
    OnModifierChange();
    return false;            
}
//Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
function ConvertToPolicy()
{
    if( window.document.forms[0].policyid.value == 0 )
    {
        alert( "Please save the Quote before converting it into a Policy" );
        return false ;
    }
    
    if( window.document.forms[0].insuredlist.options.length <= 1 )
    {
        // This case is specially for the "None Selected".
        if( window.document.forms[0].insuredlist.options.length == 1 )
        {
            if( window.document.forms[0].insuredlist.options[0].value == "0" )
            {
                alert( "At least one Insured must be attached to the Policy. Quote cannot be converted to Policy" );
                return false ;
            }
        }
        else
        {
            alert( "At least one Insured must be attached to the Policy. Quote cannot be converted to Policy" );
            return false ;             
        }
    }
        
    var bUseBillingSystem = false;
    var bDoNotBill = false;
    var chkDoNotBill;
    if(document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";            
        
    if(document.getElementById('donotbill') != null)
        chkDoNotBill = document.getElementById('donotbill');
    if(bUseBillingSystem)
    {
        if(chkDoNotBill.checked == false)
        {
            var txtBillingRuleId = eval('document.forms[0].billingrulerowid');
            var txtBillingRule = eval('document.forms[0].billingrulecode');
            var txtBillToTypeId=eval('document.forms[0].billtotype_codelookup_cid')
            var txtBillToType = eval('document.forms[0].billtotype_codelookup');
            var chkBillOverride = eval('document.forms[0].billtooverride');
            var txtIndLastNameId = eval('document.forms[0].indentityid');
            var txtIndLastName = eval('document.forms[0].indlastname');
            // Start Naresh (6/4/2007) Even if there is entry in Last Name in the New Bill Info, 
            // on clicking Convert to Policy the Message is displayed to fill the last name in New Bill Info 
            // Fixed with MITS 9511
            var txtNewlastNameId = eval('document.forms[0].newlastname_entityid');
            // End Naresh (6/4/2007) Fixed with MITS 9511
            var txtNewlastName = eval('document.forms[0].newlastname');
            if(txtBillingRuleId != null && txtBillingRule != null)
            {
                if(txtBillingRuleId.value==0)
                {
                    alert('A Billing Rule is required to convert a quote to a policy.');
                   // txtBillingRule.focus();
                    return false;
                }
            }
            if(txtBillToTypeId != null && txtBillToType != null)
            {
                if(txtBillToTypeId.value==0)
                {
                    alert('A Bill To Type is required to convert a quote to a policy.');
					for (i = 0; i < m_TabList.length; i++)
					{
						try {
							tabChange(m_TabList[i].id.substring(4));
							txtBillToType.focus();
							break;
							}
							// if required field is on invisible tab area
						catch (e) { continue; }
					}
                    return false;
                }
            }
            if(txtBillToType != null && txtIndLastNameId != null && txtIndLastName != null)
            {
                if(trim(txtBillToType.value).substring(0,1)=="F" && txtIndLastNameId.value==0)
                {
                    alert('Please select a Finance Company.');
					for (i = 0; i < m_TabList.length; i++)
					{
						try {
							tabChange(m_TabList[i].id.substring(4));
							txtIndLastName.focus();
							break;
							}
							// if required field is on invisible tab area
						catch (e) { continue; }
					}
                    return false;
                }
            }
            if(txtBillToType != null && txtNewlastNameId != null && txtNewlastName != null)
            {
                if(trim(txtBillToType.value).substring(0,1)=="I" && chkBillOverride.checked && (txtNewlastNameId.value==0 || txtNewlastNameId.value==null))
                {
                    alert('Please select a new billing entity/address.');
					for (i = 0; i < m_TabList.length; i++)
					{
						try {
							tabChange(m_TabList[i].id.substring(4));
							txtNewlastName.focus();
							break;
							}
							// if required field is on invisible tab area
						catch (e) { continue; }
					}
                    return false;
                }
            }
        }
    }

    // npadhy Start MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present 
    // then Policy should not be converted to Policy
    var bValidate = ValidateForBillingInsured('Quote', 'Policy');
    
    if(!bValidate)
        return bValidate;
    // npadhy End MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present
    // then Policy should not be converted to Policy
    
    if( !confirm( "Are you sure you wish to convert the Quote to a Policy?" ) )
        return false ;

    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;
    pleaseWait.Show();
    //Bharani - MITS : 20400 - Change 1/2
    document.getElementById('hdConvertPolicy').value = "yes";
    //Bharani - MITS : 20400 - Change 1/2
    window.document.forms[0].PostBackAction.value = "CONVERT_TO_POLICY";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();    
    return false ;
}
function RenewPolicy()
{
    // npadhy Start MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present 
    // then Policy should not be Renewed
    var bValidate = ValidateForBillingInsured('Policy', 'Renewed');

    if (!bValidate)
        return bValidate;
    // npadhy End MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present
    // then Policy should not be Renewed
    
   if( !confirm( "Are you sure you wish to renew the Policy?" ) )
    return false ;
    else 
   {
       // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
       m_bConfirmSave = false;

       pleaseWait.Show();
       
       EnableDisableControls();
       
	   window.document.forms[0].PostBackAction.value = "RENEW_POLICY";
	   window.document.forms[0].SysCmd.value = 7;
	   window.document.forms[0].submit();
	   return false;  
   }
}

function trim(svalue)
{
  svalue = svalue.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
  return svalue;
}

function CancelPolicy()
{
    // npadhy Start MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present
    // then Policy should not be Cancelled
    var bValidate = ValidateForBillingInsured('Policy', 'Cancelled');

    if (!bValidate)
        return bValidate;
    // npadhy End MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present
    // then Policy should not be Cancelled
    
    if( !confirm( "Are you sure you wish to cancel the Policy?" ) )
        return false ;
    var width = 400;
	var height = 275;
	var lPolicyID =document.forms[0].policyid.value;   // TODO
	var sEffDate = document.forms[0].EffectiveDate.value;
    var sReturnValue = showModalDialog('/RiskmasterUI/UI/EnhancedPolicy/CancelPolicy.aspx?PolicyID=' + lPolicyID + '&EffDate=' + sEffDate, 'CancelPolicy', 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');
    if (sReturnValue == null || sReturnValue == "")
        return false;

    var arrValue = sReturnValue.split('||');
    if (arrValue[0] != '')
        window.document.forms[0].DateForCancelPolicy.value = arrValue[0];
    else
        window.document.forms[0].DateForCancelPolicy.value = sEffDate;

    window.document.forms[0].TypeForCancelPolicy.value = arrValue[2];
    window.document.forms[0].ReasonForCancelPolicy.value = arrValue[1];
    
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;

    pleaseWait.Show();
    
    EnableDisableControls();
    
    
    window.document.forms[0].PostBackAction.value = "CANCEL_POLICY";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
    return false;
}

function AuditPolicyOnLoad()
{
    loadTabList();
}
function AuditPolicyChange()
{
    //TODO
}
function AuditPolicyOK()
{
    var inputs = document.getElementsByTagName("input");
    var iTermNumber="";
    for (i = 0; i < inputs.length; i++)
    {
	    if ((inputs[i].type=="radio") && (inputs[i].checked==true))
	    {
	        iTermNumber=inputs[i].value;
	        break;
	    }
    }	    
    
    if(iTermNumber == "")    
    {
        alert( "No Term selected." );
        return false ;
    }
    window.document.forms[0].Selected.value = "Selected";
    window.returnValue = iTermNumber;
    window.close();     
}

function AuditPolicy()
{    
    var width = 580;
	var height = 300;
    var sPolicyID="";

    // npadhy Start MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present 
    // then Policy should not be Audited
    var bValidate = ValidateForBillingInsured('Policy', 'Audited');

    if (!bValidate)
        return bValidate;
    // npadhy End MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present
    // then Policy should not be Audited
    
    if( !confirm( "Are you sure you wish to audit the Policy?" ) )
        return false ;
    sPolicyID     = window.document.forms[0].policyid.value;  
    var sQueryString = "PolicyID=" + sPolicyID;
    var iTermNumber = showModalDialog("/RiskmasterUI/UI/EnhancedPolicy/AuditPolicy.aspx?" + sQueryString, null, "dialogHeight:" + 300 + "px;dialogWidth:" + 580 + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");      
   
    // If popup has been closed without selecting TermNumber .  
    if( iTermNumber == null || iTermNumber == "" )
		return false ;
    
    // Assign the iTermNumber to the hidden variable and submit the page for postback.
    window.document.forms[0].TermNumberForAuditPolicy.value = iTermNumber ;

    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;

    pleaseWait.Show();
    
    EnableDisableControls();
    
    window.document.forms[0].PostBackAction.value = "AUDIT_POLICY";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
    return false;
} 

function AmendPolicyOk()
{
    if( window.document.forms[0].TransDate.value == "" )
    {
        alert( "The date entered is not valid. Please enter another date." );
        return false ;
    }

    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;
    
    window.document.forms[0].FunctionToCall.value = "EnhancePolicyAdaptor.AmendTransactionDate";
    window.document.forms[0].Validate.value = "false";
    window.document.forms[0].submit();
}

function AmendPolicyCancel()
{
    self.close();
}

function AmendPolicyOnLoad()
{
    var sTransactionDate = window.document.forms[0].TransDate.value; // aravi5 RMA-8170
    loadTabList();
    if( window.document.forms[0].Validate.value == "true" )
    {
        // window.returnValue = window.document.forms[0].TransDate.value;
        //aravi5 changes for RMA-8170 Chrome Issue - Getting an error while saving the issue date of Policy Management(ShowModalDialog) Starts
        if (get_browserName() == "IE")
            window.returnValue = sTransactionDate;
        else {
            if (sTransactionDate != null || sTransactionDate != "") {
                window.opener.document.forms[0].TransationDateForAmendPolicy.value = sTransactionDate;
                window.opener.document.forms[0].PostBackAction.value = "AMEND_POLICY";
                window.opener.document.forms[0].SysCmd.value = 7;
                window.opener.document.forms[0].submit();
            }
        }
        //aravi5 changes for RMA-8170 Chrome Issue - Getting an error while saving the issue date of Policy Management(ShowModalDialog) Ends
        window.close();
    }
    // showOverlayPopup(); // aravi5 RMA-8170
}

function AmendPolicy()
{
    var sPolicyID="";
    var width = 400 ;
	var height = 300 ;
    var bUseBillingSystem = false;

    // npadhy Start MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present
    // then Policy should not be Amended
    var bValidate = ValidateForBillingInsured('Policy', 'Amended');

    if (!bValidate)
        return bValidate;
    // npadhy End MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present
    // then Policy should not be Amended
    
    if( !confirm( "Are you sure you wish to amend the Policy?" ) )
        return false ;
    
     sPolicyID     = window.document.forms[0].policyid.value;
     var sQueryString = "PolicyID=" + sPolicyID;

     //aravi5 changes for RMA-8170 Chrome Issue - Getting an error while saving the issue date of Policy Management(ShowModalDialog) Starts
     //var sTransactionDate = showModalDialog("/RiskmasterUI/UI/EnhancedPolicy/AmendPolicy.aspx?" + sQueryString, null, "dialogHeight:" + 300 + "px;dialogWidth:" + 400 + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");
     if (get_browserName() == "IE") {
         var sTransactionDate = showModalDialog("/RiskmasterUI/UI/EnhancedPolicy/AmendPolicy.aspx?" + sQueryString, null, "dialogHeight:" + 300 + "px;dialogWidth:" + 400 + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");
         // If popup has been closed without entering Transaction Date.  
         if (sTransactionDate == null || sTransactionDate == "")
             return false;

         // Assign the Transaction Date to the hidden variable and submit the page for postback.
         window.document.forms[0].TransationDateForAmendPolicy.value = sTransactionDate;

         // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
         m_bConfirmSave = false;

         pleaseWait.Show();

         EnableDisableControls();

         window.document.forms[0].PostBackAction.value = "AMEND_POLICY";
         window.document.forms[0].SysCmd.value = 7;
         window.document.forms[0].submit();
         return false;
     }
     else {
         var sTransactionDate = window.open("/RiskmasterUI/UI/EnhancedPolicy/AmendPolicy.aspx?" + sQueryString, null, "height=" + 300 + ",width=" + 400 + ",resizable:no,status:no,scroll:no,help:no,center:yes;");
         showOverlay();
     }
     //aravi5 changes for RMA-8170 Chrome Issue - Getting an error while saving the issue date of Policy Management(ShowModalDialog) Ends
     return false;
 }

function AcceptTranaction()
{
    var iSelectedTransactionRowId = GetSelectedRowIdForTheGrid( TRANSACTION_GRID );
    var bUseBillingSystem = false;
    if (iSelectedTransactionRowId == 0)
    {
        alert( "No transaction selected." );
        return false ;
    }
    //Deb: MITS 35034
    //if (iSelectedTransactionRowId.toString().length == 1) 
    //{
    //    iSelectedTransactionRowId = "0" + iSelectedTransactionRowId;
    //}
     //Sumit-MITS#18251 -10/14/2009 -Fetching Transaction Type as well
    //var sStatusTransType = document.getElementById(TRANSACTION_GRID + "_gvData_ctl" + iSelectedTransactionRowId + "_hfGrid").value;
    var sStatusTransType = document.getElementById(TRANSACTION_GRID + "_gvData_hfGrid_" + (iSelectedTransactionRowId - 2)).value;
    //Deb: MITS 35034
    aStatusTransType = sStatusTransType.split("_");
    sStatus = aStatusTransType[0];
    
    if( sStatus != "PR" )
    {
        alert( "Transaction is not provisional and may not be accepted." );
        return false ;    
    }

    // npadhy Start MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present 
    // then Transaction should not be Accepted
    var bValidate = ValidateForBillingInsured('Transaction', 'Accepted');

    if (!bValidate)
        return bValidate;
    // npadhy End MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present
    // then Transaction should not be Accepted
    
    if( !confirm( "Are you sure you want to accept this transaction?" ) )
         return false ;

     // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;
    //pleaseWait.Show();    skumar

    EnableDisableControls();
    
    //Sumit-MITS#18251 -10/13/2009
        EditMemo('TransactionHistoryGrid','','Accept');
    //Sumit-MITS#18251 -10/13/2009 -Moved to Forms.js
//    window.document.forms[0].PostBackAction.value = "ACCEPT_TRANSACTION";
//    window.document.forms[0].SysCmd.value = 7;
//    window.document.forms[0].submit();

    // npadhy MITS 15218 and 15300 After a Transaction was Accepted or deleted, there was a double postback
    return false;
}

function DeleteTransaction()
{
    var iSelectedTransactionRowId = GetSelectedRowIdForTheGrid(TRANSACTION_GRID);
    var bUseBillingSystem = false;
    if (iSelectedTransactionRowId == 0)
    {
        alert( "No transaction selected." );
        return false ;
    }

//    if (iSelectedTransactionRowId.toString().length == 1) 
//    {
//        iSelectedTransactionRowId = "0" + iSelectedTransactionRowId;
//    }
    //Sumit-MITS#18251 -10/14/2009 -Fetching Transaction Type as well
    var sStatusTransType = document.getElementById(TRANSACTION_GRID + "_gvData_hfGrid_" + (iSelectedTransactionRowId-2)).value;
    aStatusTransType = sStatusTransType.split("_");
    sStatus = aStatusTransType[0];
    
    if( sStatus != "PR" )
    {
        alert( "Transaction is not provisional and may not be deleted." );
        return false ;    
    }
    
    if( ! confirm( "Are you sure you want to delete this transaction?" ) )
        return false;

    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;
    pleaseWait.Show();

    EnableDisableControls();
    
    window.document.forms[0].PostBackAction.value = "DELETE_TRANSACTION";
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
    
    // npadhy MITS 15218 and 15300 After a Transaction was Accepted or deleted, there was a double postback
    return false;  
}
// 29/10/2009 Ayush: Function added to find the Grid Count.
function GetGridRowsCount (listname)
{
    var iGridRowCount=0;
    inputs = document.getElementsByTagName("input");
	for (i = 0; i < inputs.length; i++)
	    if ((inputs[i].type=="radio"))
	        if (listname == inputs[i].name)	
			{
				iGridRowCount = iGridRowCount+1;
			}
	return( iGridRowCount);
}

function OnTransactionChange() 
{
    var bUseBillingSystem = false;
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;

    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    if (bUseBillingSystem) 
    {
        var chkDoNotBill = eval('document.forms[0].donotbill');
        chkDoNotBill.disabled = false;
        var chkBillOverride = eval('document.forms[0].billtooverride');
        chkBillOverride.disabled = false;
    }
    pleaseWait.Show();
    window.document.forms[0].PostBackAction.value = "TRANSACTION_CHANGE";
	window.document.forms[0].SysCmd.value = 7;
	window.document.forms[0].submit();	           
}

function EnhancePolicyEnabled()
{   
    document.forms[0].PrimaryPolicy.disabled = ! ( document.forms[0].PrimaryPolicyEnabled.value == "true" ) ;
	document.forms[0].rdoEventMadeCoverage.disabled = ! ( document.forms[0].CoverageEnabled.value == "true" ) ;;
	document.forms[0].rdoClaimMadeCoverage.disabled = ! ( document.forms[0].CoverageEnabled.value == "true" ) ;;
	document.forms[0].BankAccount.disabled = ! ( document.forms[0].BankAccountEnabled.value == "true" ) ;;

	if (document.getElementById('ExposureListGrid_New') != null) {  // If Condition Added by csingh7 : MITS 20427
	    var btnExposureAdd = document.getElementById('ExposureListGrid_New');
	    if (document.forms[0].ExposureListGrid_AddEnabled.value == "true")
	        btnExposureAdd.style.display = "inline";
	    else
	        btnExposureAdd.style.display = "none";
	}
	if (document.getElementById('ExposureListGrid_Edit') != null) { // If Condition Added by csingh7 : MITS 20427
	    var btnExposureEdit = document.getElementById('ExposureListGrid_Edit');
	    if (document.forms[0].ExposureListGrid_EditEnabled.value == "true")
	        btnExposureEdit.style.display = "inline";
	    else
	        btnExposureEdit.style.display = "none";
	}
	if (document.getElementById('ExposureListGrid_Delete') != null) {   // If Condition Added by csingh7 : MITS 20427
	    var btnExposureDelete = document.getElementById('ExposureListGrid_Delete');
	    if (document.forms[0].ExposureListGrid_DeleteEnabled.value == "true")
	        btnExposureDelete.style.display = "inline";
	    else
	        btnExposureDelete.style.display = "none";
	}
	if (document.getElementById('CoveragesGrid_New')) { // If Condition Added by csingh7 : MITS 20427
	    var btnCoverageAdd = document.getElementById('CoveragesGrid_New');
	    if (document.forms[0].CoveragesGrid_AddEnabled.value == "true")
	        btnCoverageAdd.style.display = "inline";
	    else
	        btnCoverageAdd.style.display = "none";
	}
	if (document.getElementById('CoveragesGrid_Edit')) {    // If Condition Added by csingh7 : MITS 20427
	    var btnCoverageEdit = document.getElementById('CoveragesGrid_Edit');
	    if (document.forms[0].CoveragesGrid_EditEnabled.value == "true")
	        btnCoverageEdit.style.display = "inline";
	    else
	        btnCoverageEdit.style.display = "none";
	}
	if (document.getElementById('CoveragesGrid_Delete')) {  // If Condition Added by csingh7 : MITS 20427
	    var btnCoverageDelete = document.getElementById('CoveragesGrid_Delete');
	    if (document.forms[0].CoveragesGrid_DeleteEnabled.value == "true")
	        btnCoverageDelete.style.display = "inline";
	    else
	        btnCoverageDelete.style.display = "none";
	}
	
	if( document.forms[0].WaiveTransactionPremium != null )
	    document.forms[0].WaiveTransactionPremium.disabled = ! ( document.forms[0].WaiveTransactionPremiumEnabled.value == "true" ) ;

	// npadhy MITS 18934 Disable the Select for Billing and Remove for Billing when Insured is disabled and not when Billing is disabled.
	// When the Quote is converted to Policy, the Billing Tab is disabled, but we can add/remove the Insured
	if( document.forms[0].insuredlist != null )
	{
	    document.forms[0].insuredlist.disabled = ! ( document.forms[0].InsurerInsuredEnabled.value == "true" ) ;
	    if (document.forms[0].InsurerInsuredEnabled.value == "true") 
	    {
	        document.forms[0].insuredlistbtn.style.display = "inline";
	        document.forms[0].insuredlistbtndel.style.display = "inline";
	        if (document.getElementById('btnSelectFB') != null)
	            document.getElementById('btnSelectFB').disabled = false;
	        if (document.getElementById('btnRemoveFB') != null)
	            document.getElementById('btnRemoveFB').disabled = false;
	    }
	    else 
	    {
	        document.forms[0].insuredlistbtn.style.display = "none";
	        document.forms[0].insuredlistbtndel.style.display = "none";
	        if (document.getElementById('btnSelectFB') != null)
	            document.getElementById('btnSelectFB').disabled = true;
	        if (document.getElementById('btnRemoveFB') != null)
	            document.getElementById('btnRemoveFB').disabled = true;
	    }
	    //document.forms[0].insuredlistbtn.disabled = ! ( document.forms[0].InsurerInsuredEnabled.value == "true" ) ;
	    //document.forms[0].insuredlistbtndel.disabled = ! ( document.forms[0].InsurerInsuredEnabled.value == "true" ) ;

	}
	
	document.forms[0].rdoQuote.disabled = true ;
	document.forms[0].rdoPolicy.disabled = true;
	// npadhy MITS 17516
	// If the Billing is not Activated then do not Show
	// 1. The Insured for Billing text box
	// 2. Select Insured for Billing
	// 3. Remove Insured from Billing

	var bUseBillingSystem = false;
	if (document.getElementById('UseBillingFlag') != null)
	    bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";

	if (bUseBillingSystem == false) 
	{
	    document.getElementById('lbl_billinginsured').style.display = "none";
	    document.forms[0].billinginsured.style.display = "none";
	    document.getElementById('btnSelectFB').style.display = "none";
	    document.getElementById('btnRemoveFB').style.display = "none";
	}
	else 
	{
	    document.getElementById('lbl_billinginsured').style.display = "inline";
	    document.forms[0].billinginsured.style.display = "inline";
	    document.getElementById('btnSelectFB').style.display = "inline";
	    document.getElementById('btnRemoveFB').style.display = "inline";
	}
		    
    EnableDisablePolicyBilling();		    
}

function SetDataChanged(p_bDataChanged)
{
	m_DataChanged = p_bDataChanged;
	return p_bDataChanged;
}

function IsDataChanged()
{
	return m_DataChanged;
}

function ConfirmSave()
{
	var bReturnValue = false;
	if (m_DataChanged)
	{
		bReturnValue = confirm('Data has changed. Do you want to save changes?');
	}
	return bReturnValue;
}

function ApplyBool(frmElt)
{	
	var eltMirror = eval('document.forms[0].' + frmElt.id + "_mirror");
	eltMirror.value="";

	if(!frmElt.checked)
	    eltMirror.value = "False";
			
	setDataChanged(true);
}

function GetSelectedRowIdForTheGrid( listname )
{
    var iPositionForSelectedGridRow=0;
    inputs = document.getElementsByTagName("input");
	for (i = 0; i < inputs.length; i++)
	    if ((inputs[i].type=="radio") && (inputs[i].checked==true))
	        if (listname == inputs[i].name)	
			{
				iPositionForSelectedGridRow = GetPositionForSelectedGridRow(listname,inputs[i].value);
				break;
			}
	return( iPositionForSelectedGridRow )		
}


//***********************************************************************************************************
//***** Functions for Policy Print*****
//       Added by Divya  
//		 Date: 12/26/2006
//************************************************************************************************************


//Purpose: To display Selected Forms Page.
function PolicyPrint()
{ 
    var lPolicyID =document.forms[0].policyid.value;
	var width = 560 ;
	var height = 330;
	var ret = false;
	//Start: Sumit-08/16/2010 - MITS# 21806
	if (window.document.forms[0].policyid.value == 0) {
	    alert("Please save the quote before attempting to add forms.");
	    return false;
	}
	//End:Sumit

	if (m_DataChanged) {        //csingh7 : Added check for unsaved data on Policy Screen.
	    alert("Data has changed with this record. Save it before printing.");
	    return false;
	}
	else
	    var wnd = window.open( "/RiskmasterUI/UI/EnhancedPolicy/PrintPolicy.aspx?PolicyId=" +lPolicyID  , 'SelectPolicy' , 'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',maximize=false,minimize=true,scrollbars=yes');
    //m_DataChanged=false ;
    return false;   
}

//Purpose: To display Add Forms Page.
function AddForms()
{
    // npadhy Start MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
    AddSelectedForms('GridDisplayForms', 'Add');
    // npadhy End MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
	var width = 560 ;
	var height = 330 ;
	var wnd2 = window.open( "/RiskmasterUI/UI/EnhancedPolicy/AddForm.aspx"  , 'AddForm' , 'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',maximize=false,minimize=true,scrollbars=yes');
}

function DisplayPrintForms(GridName)
{	
	if (IsDataChanged==true) 
	{
	    alert("Please save your changes before proceeding");	    
	}
    else
    {
		 var bVal=getSelectedFormIdAndNames(GridName);
    	 if (bVal==true)
		 {
            var width = 560 ;
			var height = 330 ;
			var wnd = window.open( "/RiskmasterUI/UI/EnhancedPolicy/PrintForm.aspx"  , 'PrintForm' , 'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=yes,scrollbars=yes');    
		 }	  
    }
    return false;
}

//Purpose: To Add the selected forms to the selected forms page.
function AddToSelectedForms(GridName)
{//Adding forms to the parent page from the application layer
   
    //var i = 2;
    var i = 0;
    var index = "";
    var chkBoxFormId="";
    var objForm = null;
    while(true)
    {
      //if(i<=9)
      // {
      //      index = "0" +i;
      // }
      // else
      // {
      //      index = i;
      // }
        
      //chkBoxFormId = GridName + "_ctl" + index + "_chk_forms";
      chkBoxFormId = GridName + "_chk_forms_" + i;  
      objForm = document.getElementById(chkBoxFormId);
      
      if(objForm != null)
      {      
          if(objForm.checked == true)  
            {      
                var objDisplayFormIds = window.opener.document.getElementById("DisplayFormIds");
                if(objDisplayFormIds != null)
                {
                    if(objDisplayFormIds.value == "")
                    {
                        objDisplayFormIds.value = objForm.value;
                    }
                    else
                    {
                        objDisplayFormIds.value += "," + objForm.value;                    
                    }
                }
            }     
         i=i+1;     
      }
      else
        break;        
    }    
    window.opener.document.getElementById("hdnAction").value="Add";
    window.opener.document.forms[0].submit();
    window.close();
    return false;
}

//Purpose: To check if form aleady exists.
function bCheckexists(formId)
{
   var temp =opener.document.getElementById(formId);
   if (temp != null)
	{ return true;}
   else
	{return false;}
}

//Purpose: stores selected form ids in a hidden variable
function getSelectedForms(GridName)
{//Adding forms to the parent page from the application layer
    
    //var i = 2;
    var i = 0;
    var index = "";
    var chkBoxFormId="";
    var objForm = null;
    while(true)
    {
      //if(i<=9)
      // {
      //      index = "0" +i;
      // }
      // else
      // {
      //      index = i;
      // }
        
      //chkBoxFormId = GridName + "_ctl" + index + "_chk_forms";
      chkBoxFormId = GridName + "_chk_forms_" + i;  
      objForm = document.getElementById(chkBoxFormId);
      var objselectedformids = document.getElementById("selectedformids");
      if(objForm != null)
      {      
          if(objForm.checked == true)  
            {    
                if(objselectedformids != null)
                {
                    if(objselectedformids.value == "")
                    {
                        objselectedformids.value = objForm.value;
                    }
                    else
                    {
                        objselectedformids.value += "," + objForm.value;                    
                    }
                }
            }     
         i=i+1;     
      }
      else
        break;        
    }    
    if(objselectedformids.value == "")
	{
		alert("No Form selected");
		return false;
    }
}

//Purpose:  Select All functionality 
function selectAll()
{
	var sName, sType;
	for(var i = 0; i<document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		sType = document.forms[0].elements[i].type;
		if( (sType == 'checkbox') && (sName != "") &&(sName.indexOf("chk_forms")>= 0))
		{
			document.forms[0].elements[i].checked = true;
		}
	}
}

//Purpose:  DeSelect All functionality 
function deselectAll()
{
	var sName, sType;
	for(var i = 0; i<document.forms[0].elements.length; i++)
	{
		sName = document.forms[0].elements[i].name;
		sType = document.forms[0].elements[i].type;
		if( (sType == 'checkbox') && (sName != "") &&(sName.indexOf("chk_forms")>= 0))
		{
			document.forms[0].elements[i].checked = false;
		}
	}
}

function FormsClick(gridName)
{
    //var chkSelectAllId = gridName + "_ctl01_chkForms";
    var chkSelectAllId = gridName + "_chkForms"; 
	if( document.getElementById(chkSelectAllId).checked )
	{
		selectAll();
		document.getElementById(chkSelectAllId).checked = true ;
	}
	else
	{	
	    deselectAll();
	    document.getElementById(chkSelectAllId).checked = false ;	
	}
}

//Purpose: stores selected form names in a hidden variable
function getSelectedFormIdAndNames(GridName)
{   
    //var i = 2;
    var i = 0;
    var index = "";
    var chkBoxFormId="";
    var lblFormTitle="";
    var hdnPolId="";
    var PolicyId = document.getElementById("policyid");    
    var objFormID = null;
    var objFormName = null;
    var objSelectedFormIds = document.getElementById("selectedformids");
    var objSelectedFormNames = document.getElementById("selectedformnames");
    objSelectedFormIds.value = "";
    objSelectedFormNames.value = "";
    while(true)
    {
      //if(i<=9)
      // {
      //      index = "0" +i;
      // }
      // else
      // {
      //      index = i;
      // }
        
      //chkBoxFormId = GridName + "_ctl" + index + "_chk_forms";
      chkBoxFormId = GridName + "_chk_forms_" + i;  
      objFormID = document.getElementById(chkBoxFormId);
      
      if(objFormID != null)
      {      
            if(objFormID.checked == true)  
            {    
                //hdnPolId = GridName + "_ctl" + index + "_hdnPolicyId";
                hdnPolId = GridName + "_hdnPolicyId_" + i; 
                if( document.getElementById(hdnPolId).value != PolicyId.value)
                {
                    alert("Please save the selected form(s) to continue.");
                    return false;
                }
                //lblFormTitle = GridName + "_ctl" + index + "_lblFrmTitle";
                lblFormTitle = GridName + "_lblFrmTitle_" + i; 
                 objFormName = document.getElementById(lblFormTitle);
                if(objSelectedFormIds != null)
                {
                    if(objSelectedFormIds.value == "")
                    {
                        objSelectedFormIds.value = objFormID.value;
                        objSelectedFormNames.value = objFormName.innerHTML;
                        
                    }
                    else
                    {
                        objSelectedFormIds.value += "," + objFormID.value;  
                        objSelectedFormNames.value += "||" + objFormName.innerHTML;                  
                    }
                }
            }     
            i=i+1;     
      }
      else
        break;        
    }
        
    if(objSelectedFormIds.value == "")
	{
		alert("No Form selected");
		return false;
    }     
    
     return true;  
}

//Purpose: Called On load of Print Page,to display selected forms.
function PrintPageLoaded()
{
    var lPolicyId= window.opener.document.getElementById('policyid').value;
	var formIds= window.opener.document.getElementById('selectedformids').value.split(",");
	var formnames= window.opener.document.getElementById('selectedformnames').value.split("||");
	var table =document.getElementById('selectedPrintForms');
    var InsuredList = window.opener.opener.document.getElementById('insuredlist');
   
   var bPrintForms = true;
    
    // Start Naresh 9/24/2007 MI Forms Changes
    for(i=0;i<formnames.length;i++)
    { 
        if(formnames[i] != "Insurer's Notice Of Issuance Of Policy(WC-400 MI)" &&  formnames[i] != "Notice Of Termination Of Liability(WC-401 MI)")
        {
            var Insured = "";
            //Raman Bhatia:02/12/2009: url modified for R5
            //sUrl = "home?pg=riskmaster/FROI/PolicyEnhFormPdf&PolicyId=" + lPolicyId + "&FormId=" + formIds[i] + "&InsuredId=" + Insured;
            sUrl = "/RiskmasterUI/UI/FROI/PolicyEnhFormPdf.aspx?PolicyId=" + lPolicyId + "&FormId=" + formIds[i] + "&InsuredId=" + Insured;
    	    var newrow = table.insertRow(-1);
	 
	        newCell = newrow.insertCell();
            newCell.innerHTML ="<a href=" +sUrl +">"+formnames[i]+"</a>"; 
    	}
    	else
    	{
    	
            // This case is specially for the "None Selected".
            if( InsuredList.options.length == 1 )
            {
                if( InsuredList.options[0].value == "0" )
                    bPrintForms = false;
            }
             
            if(bPrintForms)
            {
            
    	        for(j=0;j<InsuredList.length;j++)
    	        {
    	            //Raman Bhatia:02/12/2009: url modified for R5
    	            //sUrl ="home?pg=riskmaster/FROI/PolicyEnhFormPdf&PolicyId=" +lPolicyId + "&FormId="+formIds[i] + "&InsuredId="+InsuredList[j].value;
    	            sUrl = "/RiskmasterUI/UI/FROI/PolicyEnhFormPdf.aspx?PolicyId=" + lPolicyId + "&FormId=" + formIds[i] + "&InsuredId=" + InsuredList[j].value;
        	        
    	            var newrow = table.insertRow(-1);
    	            
    	            var k = j + 1;
    	 
	                newCell = newrow.insertCell();
                    newCell.innerHTML ="<a href=" +sUrl +">"+formnames[i]+ " - Insured " + k + " [" + InsuredList[j].text +"]</a>"; 
    	        }
    	    }
    	}
   }
   	return true;
}

//**********************************************************************************
//***** Functions for Cancel Policy *****
//       Added by Divya  
//		 Date: 01/30/2007
//************************************************************************************************************


function CancelPolicyOnOk()
{   if (document.forms[0].CancelType.options[document.forms[0].CancelType.selectedIndex].text=='')
    {   
        alert("Please Enter the cancellation type.");
        return false;
     }
     if (document.forms[0].CancelDate.value=='')
    {   
        alert("Please Enter the Date of cancellation.");
        return false;
     }
     /*Mukul(6/15/07) MITS 9783
     if (document.forms[0].CancelReason.options[document.forms[0].CancelReason.selectedIndex].text=='')
    {   
        alert("Please Enter the cancel reason.");
        return false;
     }
     */
	pleaseWait.Show(); 
}

function CancelPageLoad()
{

    loadTabList(); //Geeta 06/29/07 : Modified for MITS 9907
    if (window.document.forms[0].validate.value == "true") 
    {
        window.returnValue = window.document.forms[0].CancelDate.value + '||' + window.document.forms[0].CancelReason.options[window.document.forms[0].CancelReason.selectedIndex].value + '||' + window.document.forms[0].CancelType.options[window.document.forms[0].CancelType.selectedIndex].value;

        window.close();
    }
    OnCancelPolicyTypeFlat();
}

function OnCancelPolicyTypeFlat()
 
{ 
    setDataChanged(true);
    sCode=window.document.forms[0].CancelType.options[window.document.forms[0].CancelType.selectedIndex].text;

    if (sCode.substring(0,2)=="FL")
    { 
        window.document.forms[0].CancelDate.value = document.forms[0].EffDate.value;
        window.document.forms[0].CancelDate.disabled = true;
       // window.document.forms[0].CancelDatebtn.disabled =true; //vkumar258 ML changes
    }
    else
    {
         window.document.forms[0].CancelDate.value = "";
         window.document.forms[0].CancelDate.disabled = false;
        //window.document.forms[0].CancelDatebtn.disabled =false;//vkumar258 ML changes
    }
}

//pmahli 2/15/2007 EPA Earned Premium Calculation - Start
function OnEarnedPremium()
{
    var width = 400;
    var height = 275;
    sPolicyID = window.document.forms[0].policyid.value;
    // aravi5: RMA-9942 Enhanced Policy - Earned Premium page does not open on chrome. Starts
    // var sTransactionDate = showModalDialog("/RiskmasterUI/UI/EnhancedPolicy/EarnedPremium.aspx?PolicyID=" + sPolicyID, null, "dialogHeight:" + 300 + "px;dialogWidth:" + 400 + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");     
    if (get_browserName() == "Chrome") {
        var wnd = "/RiskmasterUI/UI/EnhancedPolicy/EarnedPremium.aspx?PolicyID=" + sPolicyID
        popUpWin = parent.MDICreatePopUpWin(wnd, parent.MDIPopUpWinType.Search, window, 400, 300);
        showOverlayPopup();
    }
    else {
        var sTransactionDate = showModalDialog("/RiskmasterUI/UI/EnhancedPolicy/EarnedPremium.aspx?PolicyID=" + sPolicyID, null, "dialogHeight:" + 300 + "px;dialogWidth:" + 400 + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");
    }
    // aravi5: RMA-9942 Enhanced Policy - Earned Premium page does not open on chrome. Ends
     return false;
 }

function EarnedPremiumCal()
{
     if( window.document.forms[0].ToDate.value == "" )
    {
        alert( "The date entered is not valid. Please enter another date." );
        return false ;
    }
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;
    
    window.document.forms[0].submit();
}

function EarnedPremiumCancel()
{
    self.close();
}

function EarnedPremiumOnLoad()
{
    loadTabList();
}

//pmahli 2/15/2007 EPA Earned Premium Calculation - End

//************************************************************************************
//Divya -functions for Reinstate Policy -start 

function ReinstateDateOnLoad()
{
    loadTabList();

    if (window.document.forms[0].validate.value == "true") 
    {
        window.returnValue = window.document.forms[0].ReinstateDate.value + '||' + window.document.getElementById("rdoWithoutLapse").value + '||' + window.document.getElementById("rdoWithLapse").value;
        window.close();
    }
}

function ReinstatePolicy()
{
    if( !confirm( "Are you sure you wish to reinstate the Policy?" ) )
        return false ;
    var width = 400;
	var height = 275;
	var lPolicyID =document.forms[0].policyid.value;
	var sReturnValue = showModalDialog("/RiskmasterUI/UI/EnhancedPolicy/ReinstatePolicy.aspx?PolicyID=" + lPolicyID, null, 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:no;scrollbars:no;status:no;center:yes;');	
	if (sReturnValue == null || sReturnValue == "")
	    return false;

	var arrValue = sReturnValue.split('||');

	window.document.forms[0].DateForReinstatePolicy.value = arrValue[0];
	if (arrValue[1] == "False")
	    window.document.forms[0].ReinstateWithLapseFlag.value = "true"
	else
	    window.document.forms[0].ReinstateWithLapseFlag.value = "false"

	// npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
	m_bConfirmSave = false;

	pleaseWait.Show();
	
	EnableDisableControls();
	
	window.document.forms[0].PostBackAction.value = "REINSTATE_POLICY";
	window.document.forms[0].SysCmd.value = 7;
	window.document.forms[0].submit(); 
    return false;    
}

function OnClickReinsRdoLapse(obj)
{ //show/hide date field on the basis of radio button selected
    if (obj.id=="rdoWithLapse")
    { 
        document.getElementById("DateRow").style.visibility="visible"       
    }
    else
    if(obj.id=="rdoWithoutLapse")
    {
        document.getElementById("DateRow").style.visibility="hidden";       
    }
}

function ReinstatePolicyOnOk()
{ 
  if (document.forms[0].rdoWithLapse.checked==true && document.forms[0].ReinstateDate.value=='')
   {   
        alert("Please Enter the Reinstate Date.");
        return false;
   }
   pleaseWait.Show(); 
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//                      METHODS FOR Coverages POPUPs
///////////////////////////////////////////////////////////////////////////////////////////////////
function EnhancePolicyCoveragesOnLoad()
{

    var txtCoverageType = document.getElementById("CoveragesType_codelookup");
    var btnCoverageType = document.getElementById("CoveragesType_codelookupbtn");
    var txtCoveragesStatus = document.getElementById("CoveragesStatus_codelookup");
    var btnCoveragesStatus = document.getElementById("CoveragesStatus_codelookupbtn");
    var txtEffectiveDate = document.getElementById("EffectiveDate");
    var btnEffectiveDate = document.getElementById("EffectiveDatebtn");
    var txtExpirationDate = document.getElementById("ExpirationDate");
    var btnExpirationDate = document.getElementById("ExpirationDatebtn");
    var txtPolicyLimit = document.getElementById("PolicyLimit");
    var txtOccurenceLimit = document.getElementById("OccurenceLimit");
    var txtClaimLimit = document.getElementById("ClaimLimit");
    var txtTotalPayments = document.getElementById("TotalPayments");
    var txtDeductible = document.getElementById("Deductible");
    var txtSelfInsuredRetention = document.getElementById("SelfInsuredRetention");
    var txtNextPolicy = document.getElementById("NextPolicy");
    var btnNextPolicy = document.getElementById("NextPolicybtn");
    var txtCancelNotice = document.getElementById("CancelNotice");
    var txtNotificationUser = document.getElementById("NotificationUser_rmsyslookup");
    var btnNotificationUser = document.getElementById("NotificationUser_rmsyslookupbtn");
    var txtBrokerLastName = document.getElementById("BrokerLastName");
    var btnExceptions = document.getElementById("ExceptionsbtnMemo");
    var btnRemarks = document.getElementById("RemarksbtnMemo");
    var btnOk = document.getElementById("btnCoveragesOk");
    var btnCancel = document.getElementById("btnCoveragesCancel");
    
    //Anu Tennyson for MITS 18229 STARTS 12/21/2009
    var btnDeductible = document.getElementById("Deductiblebtn");
    var txtDeductibleAmount = document.getElementById("DeductibleAmount");
    var lstUARAdded = document.getElementById("uaraddedlist");
    var btnUARAdded = document.getElementById("uaraddedlistbtn");
    var delBtnUARAdded = document.getElementById("uaraddedlistbtndel");
    var sPolicyStatus = window.opener.document.forms[0].PolicyQuoteStatus_codelookup.value;
    var sCoveragesStatus = document.forms[0].CoveragesStatus_codelookup.value;
    var sPolicyStatusId = sPolicyStatus.substring(0,1);
    var sCoveragesStatusId = sCoveragesStatus.substring(0,2);
    var sPrvFrmName = document.getElementById('prvfrmname').value;
    var sTransactionType = document.getElementById('TransactionType').value;
    if (btnDeductible != null)
    {
        if((sPolicyStatusId == "I") && 
        (sPrvFrmName == "policyenhpc" 
        || sPrvFrmName == "policyenhgl" 
        || sPrvFrmName == "policyenhal") &&
        ((sTransactionType == "EN" ||sTransactionType == "RN" ||sTransactionType == "NB") && 
         sCoveragesStatusId == "OK"))
        {
              btnDeductible.style.display="none";
        }
        if (sTransactionType == "CF" || sTransactionType == "CPR")
        {
              btnDeductible.style.display="none";
        }
        if (window.opener.document.forms[0].PolicyQuoteStatus_codelookup.value.substring(0,2) == "CV")
        {
              btnDeductible.style.display="none";
        }
      }
      //Manish multicurrency
      var amount = txtDeductible.getAttribute("Amount");
    if (amount == "0" && sPrvFrmName == "policyenhwc")
    {
        //currency
        //txtDeductible.value = "$0.00";
        txtDeductible.value = 0;
            
    }
    if ((amount == "0") && (sPrvFrmName == "policyenhpc" || sPrvFrmName == "policyenhgl" || sPrvFrmName == "policyenhal") && (document.forms[0].deductiblebtnset.value == "UnSet"))
    {
        //currency
        //txtDeductible.value = "$0.00";
        txtDeductible.value = 0;
        EnhPol_MultiCurrencyOnBlur(txtDeductible);  //manish multicurrency
    }
    //Anu Tennyson for MITS 18229 Ends
    // Coverage Type 
    if(document.forms[0].CovTypeCodeEnabled.value == "True")
    {
        if(txtCoverageType != null)
        {
            txtCoverageType.readOnly="";
            txtCoverageType.style.backgroundColor="white";
        }
        if(btnCoverageType != null)
        {
            btnCoverageType.style.display="inline";
        }
    }
    else if(document.forms[0].CovTypeCodeEnabled.value == "False")
    {
        if(txtCoverageType != null)
        {
            txtCoverageType.readOnly="readonly";
            txtCoverageType.style.backgroundColor="silver";
        }
        if(btnCoverageType != null)
        {
            btnCoverageType.style.display="none";
        }
    }
    // Coverage Status 
    if(document.forms[0].CoverageStatusEnabled.value == "True")
    {
        if(txtCoveragesStatus  != null)
        {
            txtCoveragesStatus.readOnly="";
            txtCoveragesStatus.style.backgroundColor="white";
        }
        if(btnCoveragesStatus != null)
        {
            btnCoveragesStatus.style.display="inline";
        }
    }
    else if(document.forms[0].CoverageStatusEnabled.value == "False")
    {
        if(txtCoveragesStatus  != null)
        {
            txtCoveragesStatus.readOnly="readonly";
            txtCoveragesStatus.style.backgroundColor="silver";
        }
        if(btnCoveragesStatus != null)
        {
            btnCoveragesStatus.style.display="none";
        }
    }
    // Effective Date 
    if(document.forms[0].EffDateEnabled.value == "True")
    {
        if(txtEffectiveDate != null)
        {
            txtEffectiveDate.readOnly="";
            txtEffectiveDate.style.backgroundColor="white";
        }
        if(btnEffectiveDate != null)
        {
            btnEffectiveDate.style.display="inline";
        }
    }
    else if(document.forms[0].EffDateEnabled.value == "False")
    {
        if(txtEffectiveDate  != null)
        {
            txtEffectiveDate.readOnly="readonly";
            txtEffectiveDate.style.backgroundColor="silver";
        }
        if(btnEffectiveDate != null)
        {
            btnEffectiveDate.style.display="none";
        }
    }
    // Expiration Date 
    if(document.forms[0].ExpDateEnabled.value == "True")
    {
        if(txtExpirationDate != null)
        {
            txtExpirationDate.readOnly="";
            txtExpirationDate.style.backgroundColor="white";
        }
        if(btnExpirationDate != null)
        {
            btnExpirationDate.style.display="inline";
        }
    }
    else if(document.forms[0].ExpDateEnabled.value == "False")
    {
        if(txtExpirationDate  != null)
        {
            txtExpirationDate.readOnly="readonly";
            txtExpirationDate.style.backgroundColor="silver";
        }
        if(btnExpirationDate != null)
        {
            btnExpirationDate.style.display="none";
        }
    }
    
    // Policy Limit
    if(document.forms[0].PolicyLimitEnabled.value == "True")
    {
        if(txtPolicyLimit != null)
        {
            txtPolicyLimit.readOnly="";
            txtPolicyLimit.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].PolicyLimitEnabled.value == "False")
    {
        if(txtPolicyLimit != null)
        {
            txtPolicyLimit.readOnly="readonly";
            txtPolicyLimit.style.backgroundColor="silver";
        }
    }
    
    // Occurence Limit
    if(document.forms[0].OccuranceLimitEnabled.value == "True")
    {
        if(txtOccurenceLimit != null)
        {
            txtOccurenceLimit.readOnly="";
            txtOccurenceLimit.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].OccuranceLimitEnabled.value == "False")
    {
        if(txtOccurenceLimit != null)
        {
            txtOccurenceLimit.readOnly="readonly";
            txtOccurenceLimit.style.backgroundColor="silver";
        }
    }
    
    // Claim Limit
    if(document.forms[0].ClaimLimitEnabled.value == "True")
    {
        if(txtClaimLimit != null)
        {
            txtClaimLimit.readOnly="";
            txtClaimLimit.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].ClaimLimitEnabled.value == "False")
    {
        if(txtClaimLimit != null)
        {
            txtClaimLimit.readOnly="readonly";
            txtClaimLimit.style.backgroundColor="silver";
        }
    }
    
    // Total Payments
    if(document.forms[0].TotalPaymentsEnabled.value == "True")
    {
        if(txtTotalPayments != null)
        {
            txtTotalPayments.readOnly="";
            txtTotalPayments.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].TotalPaymentsEnabled.value == "False")
    {
        if(txtTotalPayments != null)
        {
            txtTotalPayments.readOnly="readonly";
            txtTotalPayments.style.backgroundColor="silver";
        }
    }
    
    // Deductible
    if(document.forms[0].DeductibleEnabled.value == "True")
    {
        if(txtDeductible != null)
        {
        //Comemeted by Anu Tennyson for MITS 18229
//            txtDeductible.readOnly="";
            txtDeductible.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].DeductibleEnabled.value == "False")
    {
        if(txtDeductible != null)
        {
            txtDeductible.readOnly="readonly";
            txtDeductible.style.backgroundColor="silver";
        }
    }
    //Anu Tennyson
    //UAR List
    if (document.forms[0].UARaddedlistEnabled.value == "True") {
        if (lstUARAdded != null) {
            lstUARAdded.readOnly = "";
            lstUARAdded.style.backgroundColor = "white";
        }
        if (btnUARAdded != null) {
            btnUARAdded.style.display = "inline";
        }
        if (delBtnUARAdded != null) {
            delBtnUARAdded.style.display = "inline";
        }
    }
    else if (document.forms[0].UARaddedlistEnabled.value == "False") {
        if (lstUARAdded != null) {
            lstUARAdded.readOnly = "readonly";
            lstUARAdded.style.backgroundColor = "silver";
        }
        if (btnUARAdded != null) {
            btnUARAdded.style.display = "none";
        }
        if (delBtnUARAdded != null) {
            delBtnUARAdded.style.display = "none";
        }
    }
    //Anu Tennyson
    // Self Insured Retention
    if(document.forms[0].SelfInsuredRetentionEnabled.value == "True")
    {
        if(txtSelfInsuredRetention != null)
        {
            txtSelfInsuredRetention.readOnly="";
            txtSelfInsuredRetention.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].SelfInsuredRetentionEnabled.value == "False")
    {
        if(txtSelfInsuredRetention != null)
        {
            txtSelfInsuredRetention.readOnly="readonly";
            txtSelfInsuredRetention.style.backgroundColor="silver";
        }
    }
    
    // Next Policy 
    if(document.forms[0].NextPolicyIDEnabled.value == "True")
    {
        if(txtNextPolicy != null)
        {
            txtNextPolicy.readOnly="";
            txtNextPolicy.style.backgroundColor="white";
        }
        if(btnNextPolicy != null)
        {
            btnNextPolicy.style.display="inline";
        }
    }
    else if(document.forms[0].NextPolicyIDEnabled.value == "False")
    {
        if(txtNextPolicy  != null)
        {
            txtNextPolicy.readOnly="readonly";
            txtNextPolicy.style.backgroundColor="silver";
        }
        if(btnNextPolicy != null)
        {
            btnNextPolicy.style.display="none";
        }
    }
    
    // Cancel Notice
    if(document.forms[0].CancelNoticeDaysEnabled.value == "True")
    {
        if(txtCancelNotice != null)
        {
            txtCancelNotice.readOnly="";
            txtCancelNotice.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].CancelNoticeDaysEnabled.value == "False")
    {
        if(txtCancelNotice != null)
        {
            txtCancelNotice.readOnly="readonly";
            txtCancelNotice.style.backgroundColor="silver";
        }
    }
    
    // Notification User 
    if(document.forms[0].NotificationEIdEnabled.value == "True")
    {
        if(txtNotificationUser != null)
        {
            txtNotificationUser.readOnly="";
            txtNotificationUser.style.backgroundColor="white";
        }
        if(btnNotificationUser != null)
        {
            btnNotificationUser.style.display="inline";
        }
    }
    else if(document.forms[0].NotificationEIdEnabled.value == "False")
    {
        if(txtNotificationUser  != null)
        {
            txtNotificationUser.readOnly="readonly";
            txtNotificationUser.style.backgroundColor="silver";
        }
        if(btnNotificationUser != null)
        {
            btnNotificationUser.style.display="none";
        }
    }
    
    // Broker Last Name
    if(document.forms[0].BrokerLastNameEnabled.value == "True")
    {
        if(txtBrokerLastName != null)
        {
            txtBrokerLastName.readOnly="";
            txtBrokerLastName.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].BrokerLastNameEnabled.value == "False")
    {
        if(txtBrokerLastName != null)
        {
            txtBrokerLastName.readOnly="readonly";
            txtBrokerLastName.style.backgroundColor="silver";
        }
    }

    // Exceptions
    if(document.forms[0].ExceptionEnabled.value == "True")
    {
        if(btnExceptions != null)
        {
            btnExceptions.disabled = false;
        }
    }
    else if(document.forms[0].ExceptionEnabled.value == "False")
    {
        if(btnExceptions != null)
        {
            btnExceptions.disabled = true;
        }
    }
    
    // Remarks
    if(document.forms[0].RemarksEnabled.value == "True")
    {
        if(btnRemarks != null)
        {
            btnRemarks.disabled = false;
        }
    }
    else if(document.forms[0].ExceptionEnabled.value == "False")
    {
        if(btnRemarks != null)
        {
            btnRemarks.disabled = true;
        }
    }    
    // Ok Button
    if(document.forms[0].OkEnabled.value == "True")
    {
        if(btnOk != null)
        {
        //Deleted by Anu Tennyson for MITS 18229 12/21/2009
//           if( document.forms[0].FunctionToCall.value == "EnhancePolicyAdaptor.SetDateAndPolicy" || 
//                document.forms[0].ControlType.value=="NoControl" ) 
        //Anu Tennyson for MITS 18229 STARTS 12/21/2009
              if ((document.forms[0].FunctionToCall.value == "EnhancePolicyAdaptor.SetDateAndPolicy") || 
                (document.forms[0].ControlType.value=="NoControl") ||
                (document.forms[0].FunctionToCall.value == "EnhancePolicyAdaptor.CalculateDeductible"))
        //Anu Tennyson for MITS 18229 ENDS       
              {
               btnOk.disabled=false;
               }
               else
               {
                    btnOk.disabled=true;
               }
        }
    }
    else if(document.forms[0].OkEnabled.value == "False")
    {
        if(btnOk != null)
        {
            btnOk.disabled=true;
        }
    }
    // Cancel Button
    if(document.forms[0].CancelEnabled.value == "True")
    {
        if(btnCancel != null)
        {
            btnCancel.disabled=false;
        }
    }
    else if(document.forms[0].CancelEnabled.value == "False")
    {
        if(btnCancel != null)
        {
            btnCancel.disabled=true;
        }
    }
    //currency
    //Anu Tennyson for MITS 18229 STARTS 12/21/2009
    //Manish multicurrency
    //amount = txtDeductibleAmount.getAttribute("Amount"); rupal:mits 25702, txtDeductibleAmount is not multicurrency control
    amount = txtDeductibleAmount.value;
    if ((amount != "0" && document.forms[0].deductiblebtnset.value == "Set") && (sPrvFrmName == "policyenhpc" || sPrvFrmName == "policyenhgl" || sPrvFrmName == "policyenhal")) {
        if (document.forms[0].deductiblepresent.value != null)
        {
            if (!(document.forms[0].deductiblepresent.value == "NotPresent")) {
                txtDeductible.readOnly = "readonly";
            }
        }
    }
    //Anu Tennyson for MITS 18229 ENDS   
}

function fnCoveragesOk()
{
	   //pmahli MITS 9232 4/25/2007
	   //Changes done to make Coverages type Required Field   
		var bReturnValue = true;
		var txtCoveragesTypeId = document.getElementById("CoveragesType_codelookup_cid");
		var txtCoveragesType = document.getElementById("CoveragesType_codelookup");
		if(txtCoveragesTypeId != null)
		{
			if (txtCoveragesTypeId.value <= 0) {
				alert("Please select a Coverage Type Code");
				
				// npadhy MITS 16595 A javascript error used to come when user is at Supplemental page and without entering the Exposure Type,
				// Clicked Ok
				for (i = 0; i < m_TabList.length; i++)
					try {
					tabChange(m_TabList[i].id.substring(4));
					txtCoveragesType.focus();
					break;
				}
				// if required field is on invisible tab area
				catch (e) { continue; }
				return false;
			}
			else 
			{
				SetDataChanged(false);
			}
		}

		//Start: Sumit(10/07/2010)-MITS# 22506 - Included validation check for mandatory supplemental fields.
		if(!validateForm())
		{
			SetDataChanged(false);
			return false;
		}
		m_bConfirmSave = false;
		//End:Sumit
	   document.forms[0].FunctionToCall.value="EnhancePolicyAdaptor.SetCoverages";
}
 
function fnCoveragesCancel() {
    //Anu Tennyson - Chnaged for the pop up "DATA HAS CHANGED"
    m_bConfirmSave = false;
    //Anu Tennyson
    window.close();
}

function fnRefreshParentCoverageOk()
{
    var bUseBillingSystem = false;
    var objParentDocument = window.opener.document;
    if(objParentDocument==null)
    {
	    alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
	    return false;
    }
	var objParentDocumentForm = objParentDocument.forms[0];
    if(objParentDocumentForm==null)
    {
	    alert("Error. Unable to access parent window document form. Please contact your System Administrator. ");
	    return false;
    } 
    
	var sMode = document.getElementById("mode").value;
    var sGridName = document.getElementById("gridname").value;
    var iSelectedRowPosition = document.getElementById("selectedrowposition").value;
    var iCoverageId = document.getElementById("selectedid").value;
    if(sMode=="edit")
	{	
		objParentDocument.getElementById("CoveragesGrid_EditRowSessionId").value = iCoverageId.toString() + '|' + document.forms[0].SessionId.value ;
		
	}
	else if(sMode=="add")
	{
		objParentDocument.getElementById( sGridName + "_NewRowSessionId").value = document.forms[0].SessionId.value ;
	}
	objParentDocument.getElementById("CoveragesGridAction").value = sMode;

	if (objParentDocument.getElementById('UseBillingFlag') != null)
	    bUseBillingSystem = objParentDocument.getElementById('UseBillingFlag').value == "True";
	if (bUseBillingSystem) 
	{
	    var chkDoNotBill = eval('window.opener.document.forms[0].donotbill');
	    chkDoNotBill.disabled = false;
	    var chkBillOverride = eval('window.opener.document.forms[0].billtooverride');
	    chkBillOverride.disabled = false;
	}
    objParentDocumentForm.PostBackAction.value = "COVERAGES_EDIT";
    objParentDocumentForm.SysCmd.value = 7;
    objParentDocumentForm.submit();
    window.close();		
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//                      METHODS FOR Exposure POPUPs
///////////////////////////////////////////////////////////////////////////////////////////////////

function fnExpsoureOk() {
		//Anu Tennyson - Chnaged for the pop up "DATA HAS CHANGED"
		m_bConfirmSave = false;
		//Anu Tennyson
		if(ValidateExposure())
		{
			document.forms[0].FunctionToCall.value="EnhancePolicyAdaptor.SetExposure";    
		}
		else
		{
			return false;
		}
}        

function EnhancePolicyExposureOnLoad()
{
    // npadhy MITS 16595 We need to load the Tablist as after postback details of Tabs gets lost
    loadTabList();
    
    var txtExposureType = document.getElementById("ExposureType_codelookup");
    var btnExposureType = document.getElementById("ExposureType_codelookupbtn");
    var txtExposureStatus = document.getElementById("ExposureStatus_codelookup");
    var btnExposureStatus = document.getElementById("ExposureStatus_codelookupbtn");
    var txtEffectiveDate = document.getElementById("EffectiveDate");
    var btnEffectiveDate = document.getElementById("EffectiveDatebtn");
    var txtExpirationDate = document.getElementById("ExpirationDate");
    var btnExpirationDate = document.getElementById("ExpirationDatebtn");
    var txtExposureAmt = document.getElementById("AnnualExposureAmount");
    var txtRate = document.getElementById("Rate");
    var txtBaseRate = document.getElementById("BaseRate");
    var txtCoverageType = document.getElementById("CoverageType_codelookup");
    var btnCoverageType = document.getElementById("CoverageType_codelookupbtn");
    var txtHierachyLevel = document.getElementById("Department");
    var btnHierachyLevel = document.getElementById("Departmentbtn");
    var txtAmendDate = document.getElementById("DateAmended");
    var btnAmendDate = document.getElementById("DateAmendedbtn");
    var txtPremAdjustment = document.getElementById("PremiumAdjustment");
    var txtProRataAnnualPrem = document.getElementById("ProRataAnnualPremium");
    var txtFullAnnualPrem = document.getElementById("FullAnnualPremium");
    var btnExceptions = document.getElementById("ExceptionsbtnMemo");
    var btnRemarks = document.getElementById("RemarksbtnMemo");
    var btnOk = document.getElementById("btnExpsoureOk");
    var btnCancel = document.getElementById("btnExposureCancel");
    //Anu Tennyson For MITS 18229 Starts
    var sFormName = document.forms[0].SysFormName.value;
    var btnUARNewEnabled;
    var btnUARDeleteEnabled;
    if (sFormName == "exposureal") {
        if (document.getElementById("ALUARDetailsListGrid_New") != null) {
            btnUARNewEnabled = document.getElementById("ALUARDetailsListGrid_New");
        }
        if (document.getElementById("ALUARDetailsListGrid_Delete") != null) {
            btnUARDeleteEnabled = document.getElementById("ALUARDetailsListGrid_Delete");
        }
    }
    else if (sFormName == "exposurepc") {
        if (document.getElementById("PCUARDetailsListGrid_New") != null) {
            btnUARNewEnabled = document.getElementById("PCUARDetailsListGrid_New");
        }
        if (document.getElementById("PCUARDetailsListGrid_Delete") != null) {
            btnUARDeleteEnabled = document.getElementById("PCUARDetailsListGrid_Delete");
        }
    }
    //Anu Tennyson For MITS 18229 Ends
    // Exposure Type 
    if(document.forms[0].ExpTypeEnabled.value == "True")
    {
        if(txtExposureType != null)
        {
            txtExposureType.readOnly="";
            txtExposureType.style.backgroundColor="white";
        }
        if(btnExposureType != null)
        {
            btnExposureType.style.display="inline";
        }
    }
    else if(document.forms[0].ExpTypeEnabled.value == "False")
    {
        if(txtExposureType != null)
        {
            txtExposureType.readOnly="readonly";
            txtExposureType.style.backgroundColor="silver";
        }
        if(btnExposureType != null)
        {
            btnExposureType.style.display="none";
        }
    }
    // Exposure Status 
    if(document.forms[0].StatusEnabled.value == "True")
    {
        if(txtExposureStatus != null)
        {
            txtExposureStatus.readOnly="";
            txtExposureStatus.style.backgroundColor="white";
        }
        if(btnExposureStatus != null)
        {
            btnExposureStatus.style.display="inline";
        }
    }
    else if(document.forms[0].StatusEnabled.value == "False")
    {
        if(txtExposureStatus != null)
        {
            txtExposureStatus.readOnly="readonly";
            txtExposureStatus.style.backgroundColor="silver";
        }
        if(btnExposureStatus != null)
        {
            btnExposureStatus.style.display="none";
        }
    }
    // Effective Date 
    if(document.forms[0].EffDateEnabled.value == "True")
    {
        if(txtEffectiveDate != null)
        {
            txtEffectiveDate.readOnly="";
            txtEffectiveDate.style.backgroundColor="white";
        }
        if(btnEffectiveDate != null)
        {
            btnEffectiveDate.style.display="inline";
        }
    }
    else if(document.forms[0].EffDateEnabled.value == "False")
    {
        if(txtEffectiveDate != null)
        {
            txtEffectiveDate.readOnly="readonly";
            txtEffectiveDate.style.backgroundColor="silver";
        }
        if(btnEffectiveDate != null)
        {
            btnEffectiveDate.style.display="none";
        }
    }
    // Expiration Date 
    if(document.forms[0].ExpDateEnabled.value == "True")
    {
        if(txtExpirationDate != null)
        {
            txtExpirationDate.readOnly="";
            txtExpirationDate.style.backgroundColor="white";
        }
        if(btnExpirationDate != null)
        {
            btnExpirationDate.style.display="inline";
        }
    }
    else if(document.forms[0].ExpDateEnabled.value == "False")
    {
        if(txtExpirationDate != null)
        {
            txtExpirationDate.readOnly="readonly";
            txtExpirationDate.style.backgroundColor="silver";
        }
        if(btnExpirationDate != null)
        {
            btnExpirationDate.style.display="none";
        }
    }
    // Annual Exposure Amount
    if(document.forms[0].ExpAmountEnabled.value == "True")
    {
        if(txtExposureAmt != null)
        {
            txtExposureAmt.readOnly="";
            txtExposureAmt.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].ExpAmountEnabled.value == "False")
    {
        if(txtExposureAmt != null)
        {
            txtExposureAmt.readOnly="readonly";
            txtExposureAmt.style.backgroundColor="silver";
        }
    }
    // Rate
    if(document.forms[0].ExpRateEnabled.value == "True")
    {
        if(txtRate != null)
        {
            txtRate.readOnly="";
            txtRate.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].ExpRateEnabled.value == "False")
    {
        if(txtRate != null)
        {
            txtRate.readOnly="readonly";
            txtRate.style.backgroundColor="silver";
        }
    }
    // Base Rate
    if(document.forms[0].BaseRateEnabled.value == "True")
    {
        if(txtBaseRate != null)
        {
            txtBaseRate.readOnly="";
            txtBaseRate.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].BaseRateEnabled.value == "False")
    {
        if(txtBaseRate != null)
        {
            txtBaseRate.readOnly="readonly";
            txtBaseRate.style.backgroundColor="silver";
        }
    }
    // Coverage Type
    if(document.forms[0].CovTypeCodeEnabled.value == "True")
    {
        if(txtCoverageType != null)
        {
            txtCoverageType.readOnly="";
            txtCoverageType.style.backgroundColor="white";
        }
        if(btnCoverageType != null)
        {
            btnCoverageType.style.display="inline";
        }
    }
    else if(document.forms[0].CovTypeCodeEnabled.value == "False")
    {
        if(txtCoverageType != null)
        {
            txtCoverageType.readOnly="readonly";
            txtCoverageType.style.backgroundColor="silver";
        }
        if(btnCoverageType != null)
        {
            btnCoverageType.style.display="none";
        }
    }
    // Hierachy Level
    if(document.forms[0].HierachyLevelEnabled.value == "True")
    {
        if(txtHierachyLevel != null)
        {
            txtHierachyLevel.readOnly="";
            txtHierachyLevel.style.backgroundColor="white";
        }
        if(btnHierachyLevel != null)
        {
            btnHierachyLevel.style.display="inline";
        }
    }
    else if(document.forms[0].HierachyLevelEnabled.value == "False")
    {
        if(txtHierachyLevel != null)
        {
            txtHierachyLevel.readOnly="readonly";
            txtHierachyLevel.style.backgroundColor="silver";
        }
        if(btnHierachyLevel != null)
        {
            btnHierachyLevel.style.display="none";
        }
    }
    
    // Premium Adjustment
    if(document.forms[0].PremAmtEnabled.value == "True")
    {
        if(txtPremAdjustment != null)
        {
            txtPremAdjustment.readOnly="";
            txtPremAdjustment.style.backgroundColor="white";
        }
    }
    else if(document.forms[0].PremAmtEnabled.value == "False")
    {
        if(txtPremAdjustment != null)
        {
            txtPremAdjustment.readOnly="readonly";
            txtPremAdjustment.style.backgroundColor="silver";
        }
    }
    // Exceptions
    if(document.forms[0].ExceptionEnabled.value == "True")
    {
        if(btnExceptions != null)
        {
            btnExceptions.disabled = false;
        }
    }
    else if(document.forms[0].ExceptionEnabled.value == "False")
    {
        if(btnExceptions != null)
        {
            btnExceptions.disabled = true;
        }
    }
    
    // Remarks
    if(document.forms[0].RemarksEnabled.value == "True")
    {
        if(btnRemarks != null)
        {
            btnRemarks.disabled = false;
        }
    }
    else if(document.forms[0].RemarksEnabled.value == "False")
    {
        if(btnRemarks != null)
        {
            btnRemarks.disabled = true;
        }
   } 
   // Ok Button
//    if(document.forms[0].OkEnabled.value == "False" && 
//        (document.forms[0].PolicyStatus.value == "CV" || 
//        (document.forms[0].PolicyStatus.value == "I" && (document.forms[0].TransactionType.value == "EN" ||document.forms[0].TransactionType.value == "RN"))))
//    {
//        if(btnOk != null)
//        {
//            btnOk.disabled=true;
//        }
//    }
    // Ok Button
   //Sumit - 03/16/2010- MITS# 18229 - Added condition for UAR Grid Submit
    if(document.forms[0].OkEnabled.value == "True")
    {
        if(btnOk != null)
        {
            if( document.forms[0].FunctionToCall.value == "EnhancePolicyAdaptor.GetRate" &&
                document.forms[0].ControlType.value != "Date" || document.forms[0].FunctionToCall.value == "EnhancePolicyAdaptor.UpdateUARDetailList" || document.forms[0].FunctionToCall.value == "EnhancePolicyAdaptor.UpdatePropertyUARDetailList")
                btnOk.disabled=false;
            else
                btnOk.disabled=true;
        }
    }
    else if(document.forms[0].OkEnabled.value == "False")
    {
        if(btnOk != null)
        {
            btnOk.disabled=true;
        }
    }
    // Cancel Button
    if(document.forms[0].CancelEnabled.value == "True")
    {
    
        if(btnCancel != null)
        {
            btnCancel.disabled=false;
        }
    }
    else if(document.forms[0].CancelEnabled.value == "False")
    {
        if(btnCancel != null)
        {
            btnCancel.disabled=true;
        }
    }
    //Sumit - Start(04/19/2010) - MITS# 20369 - Display grid row count in label.
    //var sFormName = document.forms[0].SysFormName.value;
    var iGridRowCount = 0;
    if (sFormName == "exposureal")
     {
         var objALUarGrid = document.getElementById("ALUARDetailsListGrid_UarIds");
         if (objALUarGrid.value != "") {
             iGridRowCount = objALUarGrid.value.split("|").length;
             }
             
             var objALGridlbl = document.getElementById('ALUARDetailsListGrid_lblGridTitle');
             if (objALGridlbl != null) {
                 objALGridlbl.innerText = "UAR Details :  " + iGridRowCount + " Unit(s)";
             }
     }
    else if (sFormName == "exposurepc")
     {
         var objPCUarGrid = document.getElementById("PCUARDetailsListGrid_UarIds");
         if (objPCUarGrid.value != "") {
             iGridRowCount = objPCUarGrid.value.split("|").length;
         }
         
             var objPCGridlbl = document.getElementById('PCUARDetailsListGrid_lblGridTitle');
             if (objPCGridlbl != null) {
                 objPCGridlbl.innerText = "UAR Details :  " + iGridRowCount + " Unit(s)";
             }
     }
    //Sumit - End
     //Anu Tennyson for MITS 18229 STARTS
     if (sFormName == "exposureal") {
         if (document.forms[0].ALUARDetailsAddButtonEnabled.value == "False") {
             if (btnUARNewEnabled != null) {
                 btnUARNewEnabled.style.display = "none";
             }
         }
         if (document.forms[0].ALUARDetailsDeleteButtonEnabled.value == "False") {
             if (btnUARDeleteEnabled != null) {
                 btnUARDeleteEnabled.style.display = "none";
             }
         }
     }
     else if (sFormName == "exposurepc") {
         if (document.forms[0].PCUARDetailsAddButtonEnabled.value == "False") {
             if (btnUARNewEnabled != null) {
                 btnUARNewEnabled.style.display = "none";
             }
         }
         if (document.forms[0].PCUARDetailsDeleteButtonEnabled.value == "False") {
             if (btnUARDeleteEnabled != null) {
                 btnUARDeleteEnabled.style.display = "none";
             }
         }
     }
     //Anu Tennyson for MITS 18229 ENDS
}
 
function fnRefreshParentExposureOk()
{
    
    var bUseBillingSystem = false;
    var objParentDocument = window.opener.document;
    //Sumit - Start(03/16/2010) - MITS# 18229
    var sFormName = objParentDocument.getElementById("SysFormName").value;
    //Sumit - End
    if(objParentDocument==null)
    {
	    alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
	    return false;
    }
	
    var objParentDocumentForm = objParentDocument.forms[0];
    if(objParentDocumentForm==null)
    {
	    alert("Error. Unable to access parent window document form. Please contact your System Administrator. ");
	    return false;
    } 
	
    var sMode = document.getElementById("mode").value;
    var sGridName = document.getElementById("gridname").value;
    var iSelectedRowPosition = document.getElementById("selectedrowposition").value;
    var iExposureId = document.getElementById("selectedid").value;
    if(sMode=="edit")
	{	
        objParentDocument.getElementById("ExposureListGrid_EditRowSessionId").value = iExposureId.toString() + '|' + document.forms[0].SessionId.value ;
        //Sumit - Start(03/16/2010) - MITS# 18229
        if(document.forms[0].ALUARDetailsListGrid_UarSessionIds!=null)
        {
            objParentDocument.getElementById("ExposureListGrid_EditRowUarSessionIds").value = objParentDocument.getElementById("ExposureListGrid_EditRowUarSessionIds").value + "|" +document.forms[0].ALUARDetailsListGrid_UarSessionIdsPostDelete.value;
            objParentDocument.getElementById( sGridName + "_UarSessionIds").value=document.forms[0].ALUARDetailsListGrid_UarSessionIds.value;
        }
        //Sumit - End
        //Sumit - Start(03/16/2010) - MITS# 18229 - Property Policy
        if (document.forms[0].PCUARDetailsListGrid_UarSessionIds != null) {
            objParentDocument.getElementById("ExposureListGrid_EditRowUarSessionIds").value = objParentDocument.getElementById("ExposureListGrid_EditRowUarSessionIds").value + "|" + document.forms[0].PCUARDetailsListGrid_UarSessionIdsPostDelete.value;
            objParentDocument.getElementById(sGridName + "_UarSessionIds").value = document.forms[0].PCUARDetailsListGrid_UarSessionIds.value;
            //Sumit - Start(05/10/2010) - MITS# 20483
            if (objParentDocument.getElementById("ExposureListGrid_EditRowUarSessionId") != null) {
                objParentDocument.getElementById("ExposureListGrid_EditRowUarSessionId").value = document.forms[0].PCUARDetailsListGrid_EditRowSessionId.value;
            }
            //Sumit - End
        }
        //Sumit - End
	}
	else if(sMode=="add")
	{
		objParentDocument.getElementById( sGridName + "_NewRowSessionId").value = document.forms[0].SessionId.value ;
        //Sumit - Start(03/16/2010) - MITS# 18229
        if(document.forms[0].ALUARDetailsListGrid_UarSessionIds!=null)
        {
            objParentDocument.getElementById(sGridName + "_UarSessionIds").value = document.forms[0].ALUARDetailsListGrid_UarSessionIds.value;        
            objParentDocument.getElementById("ExposureListGrid_EditRowUarSessionIds").value = document.forms[0].ALUARDetailsListGrid_UarSessionIds.value;
        }
        //Sumit - End

        //Sumit - Start(03/16/2010) - MITS# 18229 - Property Policy
        if (document.forms[0].PCUARDetailsListGrid_UarSessionIds != null) {
            objParentDocument.getElementById(sGridName + "_UarSessionIds").value = document.forms[0].PCUARDetailsListGrid_UarSessionIds.value;
            objParentDocument.getElementById("ExposureListGrid_EditRowUarSessionIds").value = document.forms[0].PCUARDetailsListGrid_UarSessionIds.value;
        }
        //Sumit - End 
	}

    //Sumit - Start(03/16/2010) - MITS# 18229 - Refresh Deleted list on parent Page for Auto Liablity and Property Policy
	if (sFormName == "policyenhal" || sFormName == "policyenhpc") {
	    if (sFormName == "policyenhal") {
	        var arrUarRowIds = document.getElementById("ALUARDetailsListGrid_UarRowIds").value.split("|");
	    }
	    else {
	        var arrUarRowIds = document.getElementById("PCUARDetailsListGrid_UarRowIds").value.split("|");
	    }
	        
	    var objParentDeletedUarRowIds = objParentDocument.getElementById("ExposureListGrid_DeletedUarRowIds");
	    var arrParentDeletedUarRowIds = objParentDeletedUarRowIds.value.split("|");
	    var bMatch;
	    var sOut;
	    sOut = "";

	    for (i = 0; i < arrParentDeletedUarRowIds.length; i++) {
	        if (arrParentDeletedUarRowIds[i] != "") {
	            bMatch = false;
	            for (j = 0; j < arrUarRowIds.length; j++) {
	                if (arrUarRowIds[j] == arrParentDeletedUarRowIds[i]) {
	                    bMatch = true;
	                    break;
	                }
	            }
	            if (!bMatch) {
	                if (sOut == '')
	                    sOut = arrParentDeletedUarRowIds[i];
	                else
	                    sOut = sOut + '|' + arrParentDeletedUarRowIds[i];
	            }
	        }
	    }
	    objParentDeletedUarRowIds.value = sOut;
	}
	//Sumit - End

	//Sumit - Start(05/11/2010) - MITS# 20483 - Update Schedule Row IDs after deletion.
	if (sFormName == "policyenhpc") {
	    var objParentPostDeletedSchdRowIds = objParentDocument.getElementById("ExposureListGrid_ScheduleRowIdsPostDelete");
	    var objChildPostDeletedSchdRowIds = document.getElementById("PCUARDetailsListGrid_ScheduleRowIdsPostDelete");

	    if (objParentPostDeletedSchdRowIds != null && objChildPostDeletedSchdRowIds != null) {
	        if (objChildPostDeletedSchdRowIds.value != "") {
	            if (objParentPostDeletedSchdRowIds.value == "") {
	                objParentPostDeletedSchdRowIds.value = iExposureId + '?' + objChildPostDeletedSchdRowIds.value;
	            }
	            else {
	                objParentPostDeletedSchdRowIds.value = ReplaceAll(objParentPostDeletedSchdRowIds.value, '$', '?');
	                var arrScheduleIds = objParentPostDeletedSchdRowIds.value.split("?");
	                var boolAddNew = false;
	                var sFinal = "";

	                //Update - Values
	                var iCount = 0;
	                while (iCount < arrScheduleIds.length) {
	                    if (arrScheduleIds[iCount] == iExposureId.toString()) {
                            arrScheduleIds[iCount + 1] = objChildPostDeletedSchdRowIds.value;
	                        boolAddNew = false;
                            break;
	                    }
	                    else {
	                        boolAddNew = true;
	                    }
	                    iCount += 2;
	                }
	                
	                iCount = 0;
	                while (iCount < arrScheduleIds.length) {
	                    sFinal += arrScheduleIds[iCount] + '?' + arrScheduleIds[iCount + 1];
	                    if (iCount < arrScheduleIds.length - 2) {
	                        sFinal += '$';
	                    }
	                    iCount += 2;
	                }
	                objParentPostDeletedSchdRowIds.value = sFinal;
	                
	                if (boolAddNew == true) {
	                    objParentPostDeletedSchdRowIds.value += '$' + iExposureId + '?' + objChildPostDeletedSchdRowIds.value;
	                }
	            }
	        }
	    }
	}
	//Sumit - End 
	if (objParentDocument.getElementById('UseBillingFlag') != null)
	    bUseBillingSystem = objParentDocument.getElementById('UseBillingFlag').value == "True";
	if (bUseBillingSystem) 
	{
	    var chkDoNotBill = eval('window.opener.document.forms[0].donotbill');
	    chkDoNotBill.disabled = false;
	    var chkBillOverride = eval('window.opener.document.forms[0].billtooverride');
	    chkBillOverride.disabled = false;
	}

	//Sumit - Start(05/04/2010) - MITS# 20483 - Set Schedule session IDs for Uar
	if (sFormName == "policyenhpc") {
	    if (objParentDocument.getElementById("ExposureListGrid_ExposureUarSchedule") != null)
	        objParentDocument.getElementById("ExposureListGrid_ExposureUarSchedule").value = document.forms[0].PCUARDetailsListGrid_ExposureUarSchedule.value;
	}
	//Sumit - End
	
	objParentDocument.getElementById("ExposureListGridAction").value = sMode; 
    objParentDocumentForm.PostBackAction.value = "EXPOSURE_EDIT";
    objParentDocumentForm.SysCmd.value = 7;
    objParentDocumentForm.submit();
    window.close();		
}

//Anu Tennyson for MITS 18229 STARTS 12/21/2009
function CalculateDeductible(CallDeductible)
{
    if(CallDeductible)
    {
        document.forms[0].FunctionToCall.value = "EnhancePolicyAdaptor.CalculateDeductible";
        pleaseWait.Show();
        document.forms[0].submit();
    }
}      
//Anu Tennyson for MITS 18229 ENDS 12/21/2009
// Start Naresh MITS 9770 Amend when Rate was configured to be Fixed
// This also fixed 9700
function CalculateRate(EnableOk,CallRate,DateField,ChangeAmendDate,objCtrl)
{
    
  
    // Start MITS 9809 Exposure Amount takes Alphabets
    if(objCtrl != null)
    {
       
       if (isNaN(parseFloat(objCtrl.value)))
        
        {
            objCtrl.value = 0;
            EnhPol_MultiCurrencyOnBlur(objCtrl); //manish multicurrency
            // If the Amount is Zero than other fields should also be zero

            var txtPremAdjust = document.getElementById("PremiumAdjustment");
            txtPremAdjust.value = 0;
            EnhPol_MultiCurrencyOnBlur(txtPremAdjust);

            var txtPremAdjust = document.getElementById("ProRataAnnualPremium");
            txtPremAdjust.value = 0;
            EnhPol_MultiCurrencyOnBlur(txtPremAdjust);

            var txtPremAdjust = document.getElementById("FullAnnualPremium");
            txtPremAdjust.value = 0;
            EnhPol_MultiCurrencyOnBlur(txtPremAdjust);

//            document.getElementById('PremiumAdjustment').value="$0.00";
//            document.getElementById('ProRataAnnualPremium').value="$0.00";
//            document.getElementById('FullAnnualPremium').value="$0.00";
            return;
        }   
    }    
    // End MITS 9809 Exposure Amount takes Alphabets
    var btnExpsoureOk = document.getElementById('btnExpsoureOk');
    if(btnExpsoureOk != null )
        btnExpsoureOk.disabled = true;
    if(DateField)
        document.forms[0].ControlType.value="Date";  
    else
        document.forms[0].ControlType.value="";  
    if(DateField )
    {
        if(document.getElementById("TransactionType").value=="AU")
        {
            document.forms[0].FunctionToCall.value = "EnhancePolicyAdaptor.GetRate";
            pleaseWait.Show();
            document.forms[0].submit();
        }
    }
    else
    {
        if(ChangeAmendDate)
        {
            if(document.getElementById("TransactionType").value=="EN")
            {
                document.forms[0].DateAmended.value=document.getElementById("AmendDate").value;
            }
        }
        if(CallRate)
        {
            document.forms[0].FunctionToCall.value = "EnhancePolicyAdaptor.GetRate";
            pleaseWait.Show();
            document.forms[0].submit();
        }
        else
        {
            if(EnableOk)
                if(btnExpsoureOk != null )
                    btnExpsoureOk.disabled = false;
        }
    }
}
// End Naresh MITS 9770 Amend when Rate was configured to be Fixed

// Start Naresh MITS 8994 Function changed so that Negative values are not allowed.
function CoveragesEnableOk(param, objCtrl) {
  
    var btnOk = document.getElementById("btnCoveragesOk");  
    var dbl;
    if(objCtrl != null)
    {
        // Start Naresh MITS 9810 Cancel Notice Should not be more than 365
        if(objCtrl.id != "CancelNotice")
        {
            //dbl = new String(objCtrl.value);
            //dbl=dbl.replace(/[,\$]/g ,"");
            EnhPol_MultiCurrencyToDecimal(objCtrl);  //Aman Multi Currency
            dbl = objCtrl.getAttribute("Amount");
            //Anu Tennyson MITS 18229 Starts 12/21/2009
            if(isNaN(dbl))
            {
                if (objCtrl.id == "Deductible")
                {
                    //akaur9 Multi Currency    
                    objCtrl.value = 0;
                    EnhPol_MultiCurrencyOnBlur(objCtrl);  //manish multicurrency

                }
                else
                {
                    //akaur9 Multi Currency    
                    objCtrl.value = 0;
                    EnhPol_MultiCurrencyOnBlur(objCtrl); //manish multicurrency

                }
                return false;
            }
            //Anu Tennyson MITS 18229 ENDS
            if (Number(dbl) != "" && Number(dbl) >= 0)   //Aman Multi Currency
                btnOk.disabled=false;
       }
       else
       {
            if(objCtrl.value > 365 || objCtrl.value <= 0)
            {
                objCtrl.value = "0";
                 btnOk.disabled=true;
            }
            else
            {
                 btnOk.disabled=false;
            }
        }
        // End Naresh MITS 9810 Cancel Notice Should not be more than 365
        //Start: Neha Suresh Jain, 05/12/2010,MITS 20772 on change event of policy limit calculate deductible.
        if (objCtrl.id == "PolicyLimit") {            
            if (document.forms[0].FlatOrPerS.value == "P") {
                document.forms[0].FunctionToCall.value = "EnhancePolicyAdaptor.CalculateDeductible";
                document.forms[0].submit();
            }            
        }
        //End: Neha Suresh Jain
      }
    
    else
    {
        btnOk.disabled=false;
    }
    // Start Naresh MITS 9773 Amend Date was not coming in Coverages
    if(param =="EnableAmend" && document.getElementById("TransactionType").value == "EN")
    {
        document.forms[0].AmendedDate.value =document.getElementById("AmendDate").value;   
    }
    // End Naresh MITS 9773 Amend Date was not coming in Coverages
}
// End Naresh MITS 8994 Function changed so that Negative values are not allowed.

function fnExposureCancel() {
    //Anu Tennyson - Chnaged for the pop up "DATA HAS CHANGED"
    m_bConfirmSave = false;
    //Anu Tennyson
    window.close();
}

function ValidateDataExists(listname,mode)
{
    var sMessage;
    //Sumit - 04/13/2010 - Changed alerts as per Policy screen.
    var sFormName = document.getElementById("SysFormName").value;

    if (listname == "ExposureListGrid") {
        if (sFormName == "policyenhwc")
            sMessage = 'Exposure';
        else if (sFormName == "policyenhal" || sFormName == "policyenhpc")
            sMessage = 'UAR';
        else if (sFormName == "policyenhgl")
            sMessage = 'UAR/Exposure';
    }
    else
        sMessage = 'Coverage';
    if(mode=='add')
    {
        var txtEffectiveDate = document.getElementById("EffectiveDate");
		var txtExpirationDate = document.getElementById("ExpirationDate");
		var iStateId = 0;  
		if(document.getElementById("PolicyQuoteState_codelookup_cid") != null)
		    iStateId = document.getElementById("PolicyQuoteState_codelookup_cid").value;
		var txtState = document.getElementById("PolicyQuoteState_codelookup");
		var iType = 0;
		if (document.getElementById("PolicyQuoteType_codelookup_cid") != null)
		    iType = document.getElementById("PolicyQuoteType_codelookup_cid").value;
		var txtType = document.getElementById("PolicyQuoteType_codelookup");
		if(txtEffectiveDate.value == "")
		{
		    alert('Effective date must be entered before adding ' + sMessage);
		    return false;
		}
		if(txtExpirationDate.value == "")
		{
		    alert('Expiration date must be entered before adding ' + sMessage);
		    return false;
		}
		if (document.getElementById("PolicyQuoteState_codelookup_cid") != null && iStateId == 0)
		{
		    alert('State must be entered before adding ' + sMessage);
		    return false;
		}
		if (document.getElementById("PolicyQuoteType_codelookup_cid") != null && iType == 0)
		{
		    alert('Quote Type must be entered before adding ' + sMessage);
		    return false;
		}
    }
    else
    {
        if(listname=="ExposureListGrid")
        {
            var Count = document.getElementById('ExposureListGrid_Count').value;  
            if(Count == 1)
            {
                alert('The ' + sMessage + ' can not be deleted, atleast one ' + sMessage + ' needs to be attached to the policy');
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            var Count = document.getElementById('CoveragesGrid_Count').value;            
            if(Count == 1)
            {
                alert('The Coverage can not be deleted, atleast one coverage needs to be attached to the policy');
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    return true ;
}

function ValidateExposure()
{
    var bReturnValue = true;
    var txtExposureTypeId = document.getElementById("ExposureType_codelookup_cid");
    var txtExposureType = document.getElementById("ExposureType_codelookup");
    var txtAmount = document.getElementById("AnnualExposureAmount");

    //Sumit - 06/30/2010 - Changed alerts as per Exposure parent screen.
    var sMessage;
    var sType;
    var sExpType;
    var sFormName = window.opener.document.getElementById("SysFormName").value;

    if (sFormName == "policyenhwc") {
        sMessage = 'an Amount';
        sType = 'an Exposure';
        sExpType = 'Exposure';
    }
    else if (sFormName == "policyenhal" || sFormName == "policyenhpc") {
        sMessage = 'Number of Units';
        sExpType = 'UAR';
        if (sFormName == "policyenhal")
            sType = 'Vehicle';
        else if (sFormName == "policyenhpc")
            sType = 'Property';
    }
    else if (sFormName == "policyenhgl") {
        sMessage = 'an Amount/Number of Units';
        sType = 'an UAR/Exposure';
        sExpType = 'UAR/Exposure';
    }
    //End - Sumit
    if(txtExposureTypeId != null)
    {
        if(txtExposureTypeId.value <= 0) {
            //Sumit - 06/30/2010 - Changed alerts as per Exposure parent screen.
            alert("Please select " + sType + " Type Code");
//            End - Sumit
            // npadhy MITS 16595 A javascript error used to come when user is at Supplemental page and without entering the Exposure Type,
            // Clicked Ok
            for (i = 0; i < m_TabList.length; i++)
                try 
                {
                    tabChange(m_TabList[i].id.substring(4));
                    txtExposureType.focus();
                    break;
                }
            // if required field is on invisible tab area
            catch (e) { continue; }
            return false;
        }
    }
    // Naresh Changes done as the Amount has changed to 0
    if(txtAmount.value == "0") {
        //Sumit - 06/30/2010 - Changed alerts as per Exposure parent screen.
        var answer = confirm("The " + sExpType + " does not have " + sMessage + ". Do you wish to continue?");
        //End - Sumit
        if (!answer) 
        {
            // npadhy MITS 16595 A javascript error used to come when user is at Supplemental page and without entering the Coverage Type,
            // Clicked Ok
            for (i = 0; i < m_TabList.length; i++)
                try 
                {
                    tabChange(m_TabList[i].id.substring(4));
                    txtAmount.focus();
                    break;
                }
            // if required field is on invisible tab area
            catch (e) { continue; }
            return false;
        }
    }
	//Start: Sumit(10/07/2010)-MITS# 22506 - Included validation check for mandatory supplemental fields.
	if(!validateForm())
	{
		SetDataChanged(false);
		return false;
	}
	//End:Sumit
    return true;
}

/////////////////////// Naresh BILLING FUNCTION  START /////////////////////////////////////////////////

// Start Naresh Enable Disable Controls of Policy Billing Tab of Enhanced Policy on Form Load.
// Also assign the value of Specific Serialization Config.
function EnableDisablePolicyBilling()
{
    var bUseBillingSystem = false;
    if(document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    
    if(bUseBillingSystem)
    {
        var txtPolicyQuoteStatus = document.getElementById('PolicyQuoteStatus_codelookup');
        if(txtPolicyQuoteStatus != null)
        {
            var chkDoNotBill = eval('document.forms[0].donotbill');
            var txtPolicyQuoteName = eval('document.forms[0].PolicyQuoteName');
            // Start Naresh MITS 9808 The Disabled Billing Rule Textbox become enabled after Postback
            var txtPayPlanId = eval('document.forms[0].payplanrowid');
            // End Naresh MITS 9808 The Disabled Billing Rule Textbox become enabled after Postback
            var txtPayPlanCode = eval('document.forms[0].payplancode');
            var btnPayPlanCode = eval('document.forms[0].payplancodebtn');
            var txtBillingRule = eval('document.forms[0].billingrulecode');
            var btnBillingRule = eval('document.forms[0].billingrulecodebtn');
            var txtBillToType = eval('document.forms[0].billtotype_codelookup');
            var btnBillToType = eval('document.forms[0].billtotype_codelookupbtn');
            var chkBillOverride = eval('document.forms[0].billtooverride');
            var txtIndLastName = eval('document.forms[0].indlastname');
            var btnIndLastName = eval('document.forms[0].indlastnamebtn');
            var txtNewlastName = eval('document.forms[0].newlastname');
            var btnNewLastName = eval('document.forms[0].newlastnamebtn');
            var billtotypeval="";
            var FirstCharacter="";
            if(txtBillToType != null)
            {
                billtotypeval = txtBillToType.value;
                FirstCharacter = billtotypeval.substring(0,1);
            }
            var iPos = txtPolicyQuoteStatus.value.indexOf(" ");
            var StatusCode;
            if(iPos == -1)
                StatusCode = "";
            else
                StatusCode = txtPolicyQuoteStatus.value.substring(0,iPos);
            txtIndLastName.readOnly="readonly";
            txtIndLastName.style.backgroundColor="silver";
            btnIndLastName.style.display="none";
            switch(StatusCode)
            {
                
                 case "": // New
                 case "Q": // Quote
                 case "PR": //Provisonally Renewed
                    txtBillToType.readOnly = "";
                    txtBillToType.style.backgroundColor="white";
                    btnBillToType.style.display="inline";
                    if(FirstCharacter == "I")
                    {
                        chkBillOverride.disabled=false;
                    }
                    else if(FirstCharacter == "F")
                    {
                        chkBillOverride.disabled=true;
                        // To check for Add new ""
                        if((StatusCode == "Q" && trim(txtPolicyQuoteName.value) != "") || StatusCode == "PR")
                        {
                            txtIndLastName.readOnly="";
                            txtIndLastName.style.backgroundColor="white";
                            btnIndLastName.style.display="inline";
                        }
                    }
                    // Start Naresh MITS 9807 Disable Override Checkbox at Form Load
                    else
                    {
                        chkBillOverride.disabled=true;
                    }
                    // End Naresh MITS 9807 Disable Override Checkbox at Form Load
                    if(chkBillOverride.checked)
                    {
                        txtNewlastName.readOnly="";
                        txtNewlastName.style.backgroundColor="white";
                        btnNewLastName.style.display="inline";
                    }
                    else
                    {
                        txtNewlastName.readOnly="readonly";
                        txtNewlastName.style.backgroundColor="silver";
                        btnNewLastName.style.display="none";
                    }
                    txtPayPlanCode.readOnly="";
                    txtPayPlanCode.style.backgroundColor="white";
                    btnPayPlanCode.style.display="inline";
                    // Start Naresh MITS 9808 The Disabled Billing Rule Textbox become enabled after Postback
                    if(txtPayPlanId.value == 0)
                    {
						txtBillingRule.readOnly="";
						txtBillingRule.style.backgroundColor="white";
						btnBillingRule.style.display="inline";
                    }
                    else
                    {
						txtBillingRule.readOnly="readonly";
                        txtBillingRule.style.backgroundColor="silver";
                        btnBillingRule.style.display="none";
                    }
                    // End Naresh MITS 9808 The Disabled Billing Rule Textbox become enabled after Postback
                    chkDoNotBill.disabled = false;

                    // npadhy MITS 18934 Disable the Select for Billing and Remove for Billing when Insured is disabled and not when Billing is disabled.
                    // When the Quote is converted to Policy, the Billing Tab is disabled, but we can add/remove the Insured
                    
                    // npadhy MITS 17516 We need to enable/disable the Select Insured for Billing and Remove Insured from Billing Buttons
                    // Enable the Buttons if the Controls on Billing Tab is enabled
//                    if (document.forms[0].btnSelectFB != null)
//                        document.forms[0].btnSelectFB.disabled = false;
//                    if (document.forms[0].btnRemoveFB != null)
//	                    document.forms[0].btnRemoveFB.disabled = false;

                     break;
                 case "AU": //Audit
                 case "CV": // Converted Quote
                 case "R": // Revoked
                 case "I": // In effect
                 case "C": // Cancelled
                 case "CPR":
                 case "CF":
                 case "PCF": // Provisional Cancel
                 case "PCP":
                 case "PRN": //Provisional Reinstate
                 case "PRL":
                    txtBillToType.readOnly = "readonly";
                    txtBillToType.style.backgroundColor="silver";
                    btnBillToType.style.display="none";
                    chkBillOverride.disabled=true;
                    if(StatusCode == "I")
                    {
                        txtIndLastName.readOnly="readonly";
                        txtIndLastName.style.backgroundColor="silver";
                        btnIndLastName.style.display="none";
                    }
                    txtNewlastName.readOnly="readonly";
                    txtNewlastName.style.backgroundColor="silver";
                    btnNewLastName.style.display="none";
                    txtPayPlanCode.readOnly="readonly";
                    txtPayPlanCode.style.backgroundColor="silver";
                    btnPayPlanCode.style.display="none";
                    txtBillingRule.readOnly="readonly";
                    txtBillingRule.style.backgroundColor="silver";
                    btnBillingRule.style.display="none";
                    chkDoNotBill.disabled = true;

                    // npadhy MITS 18934 Disable the Select for Billing and Remove for Billing when Insured is disabled and not when Billing is disabled.
                    // When the Quote is converted to Policy, the Billing Tab is disabled, but we can add/remove the Insured
                    
                    // npadhy MITS 17516 We need to enable/disable the Select Insured for Billing and Remove Insured from Billing Buttons
                    // Disable the Buttons if the Controls on Billing Tab is disabled
//                    if (document.forms[0].btnSelectFB != null)
//                        document.forms[0].btnSelectFB.disabled = true;
//                    if (document.forms[0].btnRemoveFB != null)
//                        document.forms[0].btnRemoveFB.disabled = true;
                    break;
            }
        }
    }
}
// End Naresh Enable Disable Controls of Policy Billing Tab of Enhanced policy.

// Start Naresh Enable, Disbale, and Fill the Controls of Policy Billing Tab of Enhanced policy on the Click of Checkbox
function EnableDisableBillingRule()
{
    var chkBillOverride = eval('document.forms[0].billtooverride');
    if(chkBillOverride != null)
    {
        var value = chkBillOverride.checked;
        var txtIndLastNameId = eval('document.forms[0].indentityid');
        var txtIndLastName = eval('document.forms[0].indlastname');
        var txtNewLastNameId = eval('document.forms[0].newlastname_entityid');
        var txtNewLastName = eval('document.forms[0].newlastname');
        var btnNewLastName = eval('document.forms[0].newlastnamebtn');
        var txtIndFirstName = eval('document.forms[0].indfirstname');
        var txtNewFirstName = eval('document.forms[0].newfirstname');
        var txtIndAddr1 = eval('document.forms[0].indaddr1');
        var txtNewAddr1 = eval('document.forms[0].newaddr1');
        var txtIndAddr2 = eval('document.forms[0].indaddr2');
        var txtNewAddr2 = eval('document.forms[0].newaddr2');
        var txtIndAddr3 = eval('document.forms[0].indaddr3');
        var txtNewAddr3 = eval('document.forms[0].newaddr3');
        var txtIndAddr4 = eval('document.forms[0].indaddr4');
        var txtNewAddr4 = eval('document.forms[0].newaddr4');
        var txtIndCity = eval('document.forms[0].indcity');
        var txtNewCity = eval('document.forms[0].newcity');
        var txtIndState = eval('document.forms[0].indstateid');
        var txtNewState = eval('document.forms[0].newstateinfo');
        var txtIndZip = eval('document.forms[0].indzipcode');
        var txtNewZip = eval('document.forms[0].newzipcode');
        var txtIndCountry = eval('document.forms[0].indcountrycode');
        var txtNewCountry = eval('document.forms[0].newcountryinfo');
        if(value)
        {
            if(txtNewLastName != null)
            {
                txtNewLastName.readOnly="";
                txtNewLastName.style.backgroundColor="white";
            }
            if(btnNewLastName != null)
            {
                btnNewLastName.style.display="inline";
            }
        }
        else
        {
            // Start Naresh (6/4/2007) Empty all the Controls of Billing Info only if Nothing is 
            // Selected in Insured List Box
            // Fixed with MITS 9511
            if( window.document.forms[0].insuredlist.options.length < 1 )
            {
                if(txtIndLastNameId != null)
                {
                    txtIndLastNameId.value = 0;
                }
                if(txtIndLastName != null)
                {
                    txtIndLastName.value = "";
                }
                
                if(txtIndFirstName != null)
                {
                    txtIndFirstName.value = "";
                }
                
                if(txtIndAddr1 != null)
                {
                    txtIndAddr1.value = "";
                }
                
                if(txtIndAddr2 != null)
                {
                    txtIndAddr2.value = "";
                }
                
                if (txtIndAddr3 != null) {
                    txtIndAddr3.value = "";
                }

                if (txtIndAddr4 != null) {
                    txtIndAddr4.value = "";
                }
                if(txtIndCity != null)
                {
                    txtIndCity.value = "";
                }
                
                if(txtIndState != null)
                {
                    txtIndState.value = "";
                }
                
                if(txtIndZip != null)
                {
                    txtIndZip.value = "";
                }
                
                if(txtIndCountry != null)
                {
                    txtIndCountry.value = "";
                }
            }
            
            // Empty all the controls of New Billing Info if the Checkbox is Unchecked
            
            if(txtNewLastNameId != null)
            {
                txtNewLastNameId.value = 0;
            }
            if(txtNewLastName != null)
            {
                txtNewLastName.readOnly="readonly";
                txtNewLastName.style.backgroundColor="silver";
                txtNewLastName.value = "";
            }
            if(btnNewLastName != null)
            {
                btnNewLastName.style.display="none";
            }
            if(txtNewFirstName != null)
            {
                txtNewFirstName.value = "";
            }
            if(txtNewAddr1 != null)
            {
                txtNewAddr1.value = "";
            }
            if(txtNewAddr2 != null)
            {
                txtNewAddr2.value = "";
            }
            if (txtNewAddr3 != null)
            {
                txtNewAddr3.value = "";
            }
            if (txtNewAddr4 != null)
            {
                txtNewAddr4.value = "";
            }
            if(txtNewCity != null)
            {
                txtNewCity.value = "";
            }
            if(txtNewState != null)
            {
                txtNewState.value = "";
            }
            if(txtNewZip != null)
            {
                txtNewZip.value = "";
            }
            if(txtNewCountry != null)
            {
                txtNewCountry.value = "";
            }
        }
        // End Naresh (6/4/2007) Fixed with MITS 9511
    }
}
// End Naresh Enable, Disbale, and Fill the Controls of Policy Billing Tab of Enhanced policy on the Click of Checkbox

function OnEnhancePolicyCodeSelected()
{
    var bUseBillingSystem = false;
    // Start Naresh Enable Disable Controls, Set the value of controls when the Bill to Type is selected
	var txtBillToType = eval('document.forms[0].billtotype_codelookup');
	var chkBillOverride = eval('document.forms[0].billtooverride');
	var txtIndLastNameId = eval('document.forms[0].indentityid');
	var txtIndLastName = eval('document.forms[0].indlastname');
	var txtNewLastNameId = eval('document.forms[0].newlastname_entityid');
	var txtNewLastName = eval('document.forms[0].newlastname');
	var btnIndLastName = eval('document.forms[0].indlastnamebtn');
	var btnNewLastName = eval('document.forms[0].newlastnamebtn');
	var txtIndFirstName = eval('document.forms[0].indfirstname');
	var txtNewFirstName = eval('document.forms[0].newfirstname');
	var txtIndAddr1 = eval('document.forms[0].indaddr1');
	var txtNewAddr1 = eval('document.forms[0].newaddr1');
	var txtIndAddr2 = eval('document.forms[0].indaddr2');
	var txtNewAddr2 = eval('document.forms[0].newaddr2');
	var txtIndAddr3 = eval('document.forms[0].indaddr3');
	var txtNewAddr3 = eval('document.forms[0].newaddr3');
	var txtIndAddr4 = eval('document.forms[0].indaddr4');
	var txtNewAddr4 = eval('document.forms[0].newaddr4');
	var txtIndCity = eval('document.forms[0].indcity');
	var txtNewCity = eval('document.forms[0].newcity');
	var txtIndState = eval('document.forms[0].indstateid');
	var txtNewState = eval('document.forms[0].newstateinfo');
	var txtIndZip = eval('document.forms[0].indzipcode');
	var txtNewZip = eval('document.forms[0].newzipcode');
	var txtIndCountry = eval('document.forms[0].indcountrycode');
	var txtNewCountry = eval('document.forms[0].newcountryinfo');
	if(txtBillToType.value == "F Finance Company")
    {
	    if(txtIndLastName != null)
	    {
            txtIndLastName.readOnly="";
            txtIndLastName.style.backgroundColor="white";
        }
        if(btnIndLastName != null)
        {
            btnIndLastName.style.display="inline";
        }
        if(chkBillOverride != null)
        {
            chkBillOverride.disabled=true;
            chkBillOverride.checked=false;
        }
        if(txtNewLastName != null)
        {
            txtNewLastName.readOnly="readonly";
            txtNewLastName.style.backgroundColor="silver";
        }
        if(btnNewLastName != null)
        {
            btnNewLastName.style.display="none";
        }
        if(txtIndLastNameId != null)
        {
            txtIndLastNameId.value = 0;
        }
        if(txtIndLastName != null)
        {
            txtIndLastName.value = "";
        }
        if(txtNewLastNameId != null)
        {
            txtNewLastNameId.value = 0;
        }
        if(txtNewLastName != null)
        {
            txtNewLastName.value = "";
        }
        if(txtIndFirstName != null)
        {
            txtIndFirstName.value = "";
        }
        if(txtNewFirstName != null)
        {
            txtNewFirstName.value = "";
        }
        if(txtIndAddr1 != null)
        {
            txtIndAddr1.value = "";
        }
        if(txtNewAddr1 != null)
        {
            txtNewAddr1.value = "";
        }
        if(txtIndAddr2 != null)
        {
            txtIndAddr2.value = "";
        }
        if(txtNewAddr2 != null)
        {
            txtNewAddr2.value = "";
        }
        if (txtIndAddr3 != null)
        {
            txtIndAddr3.value = "";
        }
        if (txtNewAddr4 != null)
        {
            txtNewAddr4.value = "";
        }
        if(txtIndCity != null)
        {
            txtIndCity.value = "";
        }
        if(txtNewCity != null)
        {
            txtNewCity.value = "";
        }
        if(txtIndState != null)
        {
            txtIndState.value = "";
        }
        if(txtNewState != null)
        {
            txtNewState.value = "";
        }
        if(txtIndZip != null)
        {
            txtIndZip.value = "";
        }
        if(txtNewZip != null)
        {
            txtNewZip.value = "";
        }
        if(txtIndCountry != null)
        {
            txtIndCountry.value = "";
        }
        if(txtNewCountry != null)
        {
            txtNewCountry.value = "";
        }
	}
	else
	{
		if(txtIndLastName != null)
	    {
            txtIndLastName.readOnly="readonly";
            txtIndLastName.style.backgroundColor="silver";
        }
        if(chkBillOverride != null)
        {
            chkBillOverride.disabled=false;
        }
        if(txtNewLastName != null)
        {
            txtNewLastName.readOnly="readonly";
            txtNewLastName.style.backgroundColor="silver";
        }
        if(btnIndLastName != null)
        {
            btnIndLastName.style.display="none";
        }
        if(btnNewLastName != null)
        {
            btnNewLastName.style.display="none";
        }

        var billingInsuredId = document.getElementById('billinginsured_cid');
        if (billingInsuredId != null) 
        {
            setDataChanged(true);
            if(billingInsuredId.value != "0")
                FetchInsured(billingInsuredId.value, 'InsuredList');
        }
               
	}
	// End Naresh Enable Disable Controls, Set the value of controls when the Bill to Type is selected
}

/////////////////////// Naresh BILLING FUNCTION  END /////////////////////////////////////////////////
function PolicyValidateDate()
{
   var sEffectiveDate   = document.getElementById("EffectiveDate").value;
   var sExpirationDate = document.getElementById("ExpirationDate").value;
   //Start: Sumit(10/07/2010)-MITS# 21882
   var sEffdate = "";
   var sExpDate = "";
    //Aman MITS 29700
   var target = $("#" + "ExpirationDate");
   var inst = $.datepicker._getInst(target[0]);
   var format = target.datepicker('option', 'dateFormat');
   var format1 = format.substr(0, 2).toUpperCase();
   var format2 = format.substr(3, 2).toUpperCase();
   var format3 = format.substr(6, 4).toUpperCase();
   sDateSeparator = format.substr(2, 1).toUpperCase();
   if (sEffectiveDate != "")
   {

       //sEffdate = new Date(sEffectiveDate);
       var sDate = new String(sEffectiveDate);

       var sArr = sDate.split(sDateSeparator);
       var dArr = new Array();
       if (sArr.length == 3) {
           //Rakhel - Modified for ML - Start
           if (format1.substring(0, 1) === "D") {
               dArr[0] = new String(parseInt(sArr[1], 10));
               if (format3.substring(0, 1) === "Y") { // ddmmyy
                   dArr[1] = new String(parseInt(sArr[0], 10));
                   dArr[2] = new String(parseInt(sArr[2], 10));
               }
               else { //ddyymm
                   dArr[1] = new String(parseInt(sArr[2], 10));
                   dArr[2] = new String(parseInt(sArr[0], 10));
               }
           }

           if (format1.substring(0, 1) === "M") {
               dArr[0] = new String(parseInt(sArr[0], 10));
               if (format3.substring(0, 1) === "Y") { // mmddyy
                   dArr[1] = new String(parseInt(sArr[1], 10));
                   dArr[2] = new String(parseInt(sArr[2], 10));
               }
               else { //mmyydd
                   dArr[1] = new String(parseInt(sArr[2], 10));
                   dArr[2] = new String(parseInt(sArr[1], 10));
               }
           }

           if (format1.substring(0, 1) === "Y") {
               dArr[0] = new String(parseInt(sArr[2], 10));
               if (format3.substring(0, 1) === "M") { // yyddmm
                   dArr[1] = new String(parseInt(sArr[0], 10));
                   dArr[2] = new String(parseInt(sArr[1], 10));
               }
               else { //yymmdd
                   dArr[1] = new String(parseInt(sArr[1], 10));
                   dArr[2] = new String(parseInt(sArr[0], 10));
               }
           }
       }
       dArr[2] = Get4DigitYear(dArr[2]);
       //Rakhel - Modified for ML - Start

       sEffdate = parseInt(dArr[2]) + "/" + dArr[1].toString() + "/" + parseInt(dArr[0]);
       //Added to fix smoke issue
       //var dateEff = new Date(parseInt(dArr[2]), dArr[1].toString(), parseInt(dArr[2])); //Year, Month, Date
       
       var dateEff = new Date(parseInt(dArr[2]), parseInt(dArr[0]), parseInt(dArr[1])); //Year, Month, Date
   }
    if (sExpirationDate != "") {

        //sEffdate = new Date(sEffectiveDate);
        var sDate = new String(sExpirationDate);


        var sArr = sDate.split(sDateSeparator);
        var dArr = new Array();
        if (sArr.length == 3) {
            //Rakhel - Modified for ML - Start
            if (format1.substring(0, 1) === "D") {
                dArr[0] = new String(parseInt(sArr[1], 10));
                if (format3.substring(0, 1) === "Y") { // ddmmyy
                    dArr[1] = new String(parseInt(sArr[0], 10));
                    dArr[2] = new String(parseInt(sArr[2], 10));
                }
                else { //ddyymm
                    dArr[1] = new String(parseInt(sArr[2], 10));
                    dArr[2] = new String(parseInt(sArr[0], 10));
                }
            }

            if (format1.substring(0, 1) === "M") {
                dArr[0] = new String(parseInt(sArr[0], 10));
                if (format3.substring(0, 1) === "Y") { // mmddyy
                    dArr[1] = new String(parseInt(sArr[1], 10));
                    dArr[2] = new String(parseInt(sArr[2], 10));
                }
                else { //mmyydd
                    dArr[1] = new String(parseInt(sArr[2], 10));
                    dArr[2] = new String(parseInt(sArr[1], 10));
                }
            }

            if (format1.substring(0, 1) === "Y") {
                dArr[0] = new String(parseInt(sArr[2], 10));
                if (format3.substring(0, 1) === "M") { // yyddmm
                    dArr[1] = new String(parseInt(sArr[0], 10));
                    dArr[2] = new String(parseInt(sArr[1], 10));
                }
                else { //yymmdd
                    dArr[1] = new String(parseInt(sArr[1], 10));
                    dArr[2] = new String(parseInt(sArr[0], 10));
                }
            }
        }
        dArr[2] = Get4DigitYear(dArr[2]);
        //Rakhel - Modified for ML - Start

        sExpDate = parseInt(dArr[0]) + "/" + dArr[1].toString() + "/" + parseInt(dArr[2]);
        //Added to fix smoke issue
        //var dateExp = new Date(parseInt(dArr[2]), dArr[1].toString(), parseInt(dArr[0])); //Year, Month, Date
        var dateExp = new Date(parseInt(dArr[2]), parseInt(dArr[0]), parseInt(dArr[1])); //Year, Month, Date
        
    }
    //Aman MITS 29700
   //var sEffdate = sEffectiveDate.substring(6,10)+sEffectiveDate.substring(0,2)+sEffectiveDate.substring(3,5);
   //var sExpDate = sExpirationDate.substring(6,10)+sExpirationDate.substring(0,2)+sExpirationDate.substring(3,5);
    //End:Sumit
    
   //if(sExpirationDate != "" && sExpDate<sEffdate )
   //{
   //     alert("Effective date must be less than Expiration date");
   //     document.getElementById("EffectiveDate").value ="";
   //     document.getElementById("ExpirationDate").value = "";
   //     //Start (07/06/2010) MITS# 20820:Sumit - Update dates as user may change them at later stage.
   //     return false;
    //}
    //Commenting the above if condition for fixing smoke issue fix
    if (sExpirationDate != "" && dateExp < dateEff) {
        alert("Effective date must be less than Expiration date");
        document.getElementById("EffectiveDate").value = "";
        document.getElementById("ExpirationDate").value = "";
        return false;
    }

    OnEnhancePolicyDateChange();
    //End:Sumit
}

//Anjaneya MITS 11712 10/03/2008 
function ValidateBeginEndDate()
{
   var sEffectiveDate   = document.getElementById("mcobegindate").value;
   var sExpirationDate  = document.getElementById("mcoenddate").value;
   var sEffdate = sEffectiveDate.substring(6,10)+sEffectiveDate.substring(0,2)+sEffectiveDate.substring(3,5);
   var sExpDate = sExpirationDate.substring(6,10)+sExpirationDate.substring(0,2)+sExpirationDate.substring(3,5);
   if(sExpirationDate != "" && sExpDate<sEffdate )
   {
        alert("MCO Begin date must be less than MCO End date");
        document.getElementById("mcobegindate").value ="";
        document.getElementById("mcoenddate").value = "";
   }
}

//pmahli MITS 8770 4/12/2007 Disables Exposure Field when opened in Edit Mode
function DisableExposure()
{
    if(document.forms[0].EditAddMode.value == "edit")
    {
        //By abansal23 for codelookup control
    //Added bY Tushar MITS 18231
    
        if(document.forms[0].explob_codelookup !=null)   // Ayush: MITS18231
        {
            document.forms[0].explob_codelookup.value.readOnly = true;  
            document.forms[0].explob_codelookup.style.backgroundColor = "silver";
            document.forms[0].explob_codelookupbtn.style.display = "none";
        }
        else if(document.forms[0].covlob_codelookup !=null)   // Ayush:MITS 18231
        {
            document.forms[0].covlob_codelookup.value.readOnly = true;  
            document.forms[0].covlob_codelookup.style.backgroundColor = "silver";
            document.forms[0].covlob_codelookupbtn.style.display = "none";
        }
        else if (document.forms[0].uarlob_codelookup !=null)
        {
            document.forms[0].uarlob_codelookup.readOnly = true;
            document.forms[0].uarlob_codelookup.style.backgroundColor = "silver";
            document.forms[0].uarlob_codelookupbtn.style.display = "none";
        }
        
        
        if(document.forms[0].Exposure_codelookup !=null)
        {
            document.forms[0].Exposure_codelookup.readOnly = true;
            document.forms[0].Exposure_codelookup.style.backgroundColor = "silver";
            document.forms[0].Exposure_codelookupbtn.style.display = "none";
        }
        else if (document.forms[0].Coverage_codelookup !=null)
        {
            document.forms[0].Coverage_codelookup.readOnly = true;
            document.forms[0].Coverage_codelookup.style.backgroundColor = "silver";
            document.forms[0].Coverage_codelookupbtn.style.display = "none";
        }
        else if (document.forms[0].Unit_Type_codelookup !=null)
        {
            document.forms[0].Unit_Type_codelookup.readOnly = true;
            document.forms[0].Unit_Type_codelookup.style.backgroundColor = "silver";
            document.forms[0].Unit_Type_codelookupbtn.style.display = "none";
        }
       
        //End
    }
}
//pmahli

// Start Naresh Functions related to Primary Policy

// Set the value of the Checkbox as per the value of Hidden control on screen load
function EnhancePrimaryPolicyAssign()
{
    var chkPrimaryPolicy = eval('document.forms[0].PrimaryPolicy');
    if(document.forms[0].PrimaryPolicyHidden.value=="True" )
    {
        chkPrimaryPolicy.checked = true;
    }
    else
    {
        chkPrimaryPolicy.checked = false;
    }
}

// Set the Value of Hidden Variable as per the Click on the Checkbox
function SetPrimaryPolicyValue()
{
    var chkPrimaryPolicy = eval('document.forms[0].PrimaryPolicy');
    if(chkPrimaryPolicy.checked)
    {
        document.forms[0].PrimaryPolicyHidden.value = "True";
    }
    else
    {
        document.forms[0].PrimaryPolicyHidden.value = "False";
    }
}

// Start Naresh Functions related to Primary Policy

// npadhy MITS 17516
function OnInsuredListPopulate(mode, controlType) 
{
    var bUseBillingSystem = false;
    var billingInsuredId = document.getElementById('billinginsured_cid');
    var insuredId;
    if (billingInsuredId != null)
        insuredId = billingInsuredId.value;
    if (window.document.forms[0].UseBillingFlag.value == "True") 
    {

        if (mode == 'Add') 
        {
            if (trim(window.document.forms[0].billtotype_codelookup.value) == "I Insured") 
            {
                FetchInsured(insuredId, controlType);
            }
        }
        else 
        {
            if (mode == 'Delete') 
            {
                var insuredList = window.document.forms[0].insuredlist;
                var bFound = false;
                if (insuredList != null) {
                    for (var i = 0; i < insuredList.options.length; i++) {
                        if (insuredList.options[i].value == insuredId || insuredList.options[i].value == 0) 
                        {
                            bFound = true;
                            break;
                        }
                    }
                    if (bFound == false && insuredId != 0) 
                    {
                        RemoveInsuredFromBilling();
                    }
                }
            }
        }
    }
}
// End Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab

// Start Naresh (6/4/2007) MITS 9512 Populate the Insured details in the Policy Billing Tab in New Billing
function OnInsuredPopulate(insuredId,controlType)
{
    var bUseBillingSystem = false;
    FetchInsured(insuredId, controlType);
}

// Start Naresh (6/4/2007) MITS 9512 Populate the Insured details in the Policy Billing Tab in New Billing
// Start Naresh MITS 8994 Function was to be added in onblur for all the currency fields
// Check for any Negative amount entered by the user
function CheckNegative(objCtrl, IntegerValue) {
    
    //var dbl=new String(objCtrl.value);
    var amount = objCtrl.getAttribute("Amount");

    if (amount.indexOf('-') >= 0)
	{
	    if (IntegerValue == null) {

	        //akaur9 Multi Currency    
	        objCtrl.value = 0;
	        EnhPol_MultiCurrencyOnBlur(objCtrl); //manish multicurrency
	
		}
	    else {
	        //akaur9 Multi Currency    	    
	        objCtrl.value = 0;
	        EnhPol_MultiCurrencyOnBlur(objCtrl);  //manish multicurrency
          }
		return false;			
	} 
		
}
// End Naresh MITS 8994 Function was to be added in onblur for all the currency fields

//Manish Jain Multicurrency
function EnhPol_MultiCurrencyOnBlur(objCtrl) {
    //rupal:mits 27502
    var functionname=""; 
    functionname = objCtrl.getAttributeNode("onblur").value;      //akaur9 Multi Currency
    if (functionname != "null") {
        functionname = functionname.split('(')[0];
        window[functionname](objCtrl);
        //rupal
    }
}
//Aman Multi Currency Start
function EnhPol_MultiCurrencyToDecimal(objCtl) {
    var functionname = objCtl.getAttributeNode("onfocus").value;
    //remove 'format_' word
    functionname = functionname.replace("Format_", "");
    functionname = functionname.split('(')[0];
    window[functionname](objCtl);
}
//Aman Multicurrency --End

// Start Naresh MITS 9901 Mail Merge :- Claima Made and Event Made Policy Issue
function DeselectOther(obj)
{ 
    if (obj.id=="rdoEventMadeCoverage")
    { 
        obj.value =true;
        document.getElementById("rdoClaimMadeCoverage").value=false;
        document.getElementById("rdoClaimMadeCoverage").checked=false;
    }
    else
    if(obj.id=="rdoClaimMadeCoverage")
    {
        obj.value=true;
        document.getElementById("rdoEventMadeCoverage").value=false;
        document.getElementById("rdoEventMadeCoverage").checked=false;
    }
}

function EnhanceEventPolicyAssign()
{
    var rdoEvent = eval('document.forms[0].rdoEventMadeCoverage');
    if(document.forms[0].rdoEventMadeCoverageHidden.value=="True")
    {
        rdoEvent.checked = true;
    }
    else
    {
        rdoEvent.checked = false;
    }
    if(document.forms[0].PolicyQuoteStatus_codelookup.value=="Q Quoted" && document.forms[0].rdoEventMadeCoverageHidden.value == "False" && document.forms[0].rdoClaimMadeCoverageHidden.value == "False")
    {
        document.forms[0].rdoEventMadeCoverageHidden.value = "True";
        rdoEvent.checked = true;
    }

}

function SetEventFlagValue()
{
    var rdoEvent = eval('document.forms[0].rdoEventMadeCoverage');
    if(rdoEvent.checked)
    {
        document.forms[0].rdoEventMadeCoverageHidden.value = "True";
        document.forms[0].rdoClaimMadeCoverageHidden.value = "False";
    }
    else
    {
        document.forms[0].rdoEventMadeCoverageHidden.value = "False";
        document.forms[0].rdoClaimMadeCoverageHidden.value = "True";
    }
}

function EnhanceClaimPolicyAssign()
{
    var rdoClaim = eval('document.forms[0].rdoClaimMadeCoverage');
    if(document.forms[0].rdoClaimMadeCoverageHidden.value=="True" )
    {
        rdoClaim.checked = true;
    }
    else
    {
        rdoClaim.checked = false;
    }
}

function SetClaimFlagValue()
{
    var rdoClaim = eval('document.forms[0].rdoClaimMadeCoverage');
    if(rdoClaim.checked)
    {
        document.forms[0].rdoClaimMadeCoverageHidden.value = "True";
        document.forms[0].rdoEventMadeCoverageHidden.value = "False";
    }
    else
    {
        document.forms[0].rdoClaimMadeCoverageHidden.value = "False";
        document.forms[0].rdoEventMadeCoverageHidden.value = "True";
    }
}
// End Naresh MITS 9901 Mail Merge :- Claima Made and Event Made Policy Issue

function PolicyLoad()
{
    loadTabList();
    var tabObj = document.getElementById('LINKTABSGeneral');

    var objTabAction = document.getElementById("HdnTabPress");
    var objHiddenTab = document.getElementById("HdnTab");


    if (objTabAction != null && objTabAction.value != "") 
    {
        tabObj = document.getElementById('LINKTABS' + objTabAction.value);
        if (tabObj != null) 
        {
            tabObj.className = "Selected";
            if (objHiddenTab.value == 'undefined')
                tabChange(objTabAction.value);
            else
                tabChange(objTabAction.value, objHiddenTab.value);

        }
    }
    else 
    {
        if (tabObj != null) 
        {
            if (tabObj.className == "Selected")
                tabChange(m_TabList[0].id.substring(4));
        }
    }            

}

function PreventPageLoadBeforeSave()
{
    document.getElementById('HdnActionSave').value = "Save";
}

function SelectInsuredForBilling() 
{
    if (window.document.forms[0].insuredlist.options.length >= 1) 
    {
        // This case is specially for the "None Selected".
        if (window.document.forms[0].insuredlist.options.length == 1) 
        {
            if (window.document.forms[0].insuredlist.options[0].value != "0") 
            {
                TransferInsured();
                setDataChanged(true);
            }
            else
                alert('Please add an insured before clicking this button');
        }
        else
        {
            TransferInsured();
            setDataChanged(true);
        }
    }
    else if (window.document.forms[0].insuredlist.options.length == 0)
    {
        alert('Please add an insured before clicking this button');
    }
    return false;
}

function TransferInsured()
{
    var insuredList = window.document.forms[0].insuredlist;
    var bFound = false;
    if (insuredList != null)
    {
        for (var i = 0; i < insuredList.options.length; i++) 
        {
            if (insuredList.options[i].selected) 
            {
                var billingInsuredName = document.getElementById('billinginsured');
                var billingInsuredId = document.getElementById('billinginsured_cid');
                if(billingInsuredName != null && billingInsuredId != null)
                {
                    if (billingInsuredId.value != insuredList.options[i].value) 
                    {
                        billingInsuredName.value = insuredList.options[i].text;
                        billingInsuredId.value = insuredList.options[i].value;
                        OnInsuredListPopulate('Add', 'InsuredList');
                    }
                    else
                        alert('This insured is already selected for Billing');
                    bFound = true;
                }
                break;
            }
        }
        if (bFound == false)
            alert('Please select an insured before clicking this Button');
    }
    return false;
}

function RemoveInsuredFromBilling() 
{
    var billingInsuredName = document.getElementById('billinginsured');
    var billingInsuredId = document.getElementById('billinginsured_cid');
    
    if (billingInsuredName != null && billingInsuredId != null) 
    {
        if (billingInsuredId.value != "0") 
        {
            billingInsuredName.value = "";
            billingInsuredId.value = "0";

            setDataChanged(true);

            if (trim(window.document.forms[0].billtotype_codelookup.value) == "I Insured") 
            {
                FetchInsured(0, 'InsuredList');
            }
        }
        else
            alert('There is no insured to remove from Billing');
    }
    return false;
}

function FetchInsured(InsueredId, controlType) 
{
    document.forms[0].ControlType.value = controlType;
    document.forms[0].InsuredId.value = InsueredId;
    document.forms[0].PostBackAction.value = "INSURED_DETAILS";
    // npadhy MITS 15218 Pop up comes Data has changed .Do u want to save changes.
    m_bConfirmSave = false;
    pleaseWait.Show();

    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    if (bUseBillingSystem) {
        var chkDoNotBill = eval('document.forms[0].donotbill');
        chkDoNotBill.disabled = false;
        var chkBillOverride = eval('document.forms[0].billtooverride');
        chkBillOverride.disabled = false;
    }
    window.document.forms[0].SysCmd.value = 7;
    window.document.forms[0].submit();
}

// npadhy MITS 18859 Defined a function where any manipulation before the Polic/Quote is saved can be triggered.
function enhPolicySave() 
{
    // npadhy Start MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present 
    // then Policy should not be Saved
    var bValidate = ValidateForBillingInsured('Policy', 'saved');
    
    // If the validation is successful then EnableDisable Controls
    if (bValidate) 
    {
        EnableDisableControls();
    }
    return bValidate;
    // npadhy End MITS 18894 if Billing is enabled, Do not Bill is not Checked, Bill to Type is insured and Insured for Billing is not present 
    // then Policy should not be Saved
}

// npadhy MITS 18859 Do Not Bill Checkbox was getting Unselected after Postback.
function EnableDisableControls() 
{
    var bUseBillingSystem;
    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";
    if (bUseBillingSystem) 
    {
        var chkDoNotBill = eval('document.forms[0].donotbill');
        chkDoNotBill.disabled = false;
        var chkBillOverride = eval('document.forms[0].billtooverride');
        chkBillOverride.disabled = false;
    }
}

// npadhy Start MITS 18860 If the Forms are not saved already, then Clicking Add or delete button would refresh this page and non saved forms will disappear
// So running a loop which will persist these form ids
function AddSelectedForms(GridName,Mode) 
{
    //var i = 2;
    var i = 0;
    var index = "";
    var chkBoxFormId = "";
    var objDisplayFormIds = "";
    var ctlPolicyId = "";
    var iPolicyId = 0;
    var objForm = null;
    
    while (true) 
    {
        //if (i <= 9) 
        //{
        //    index = "0" + i;
        //}
        //else 
        //{
        //    index = i;
        //}

        //chkBoxFormId = GridName + "_ctl" + index + "_chk_forms";
        chkBoxFormId = GridName + "_chk_forms_" + i;  
        objForm = document.getElementById(chkBoxFormId);
        ctlPolicyId = document.getElementById(GridName + "_ctl" + index + "_hdnPolicyId");
        if (ctlPolicyId != null)
            iPolicyId = ctlPolicyId.value;
        if (objForm != null) 
        {
            if (iPolicyId == -1 &&(Mode == "Add" || (Mode!="Add" && objForm.checked == false))) 
            {
                objDisplayFormIds = window.self.document.getElementById("DisplayFormIds");
                if (objDisplayFormIds != null) 
                {
                    if (objDisplayFormIds.value == "") 
                    {
                        objDisplayFormIds.value = objForm.value;
                    }
                    else 
                    {
                        objDisplayFormIds.value += "," + objForm.value;
                    }
                }
            }
            i = i + 1;
        }
        else
            break;
    }      
}
// npadhy End MITS 18860 If the Forms are not saved already, then Clicking Add or delete button would refresh this page and non saved forms will disappear
// So running a loop which will persist these form ids

// npadhy Start MITS 18894 if Billing is enabled, Do not Bill is not Checked and Insured for Billing is not present 
// then Policy should not be Saved
function ValidateForBillingInsured(sObject, sTransaction) 
{
    
    var rdoIsQuoteChecked = eval('document.forms[0].rdoQuote');
    var chkDoNotBill = eval('document.forms[0].donotbill');
    var txtBillToType = eval('document.forms[0].billtotype_codelookup');

    var bQuote = false;
    var bUseBillingSystem;
    var bDoNotBill = false;
    var iInsuredId = 0;
    var sBillToType = "";

    // Check whether the Record is Quote or Policy. This condition is required obnly while Saving Quote or Policy.
    // If the Quote is getting saved, we do not want this validation
    if (rdoIsQuoteChecked.checked && sTransaction == "saved")
        bQuote = true;
    
    // Check whether Billing is enabled
    if (document.getElementById('UseBillingFlag') != null)
        bUseBillingSystem = document.getElementById('UseBillingFlag').value == "True";

    // Check whether Do not bill is checked
    if (bUseBillingSystem && chkDoNotBill.checked)
        bDoNotBill = true;
    
    // Check whether Billing Insured is Present
    if (document.getElementById('billinginsured_cid') != null)
        iInsuredId = document.getElementById('billinginsured_cid').value;
        
    // Check whether Insured is Selected in the Bill To Type
    if (txtBillToType != null)
        sBillToType = trim(txtBillToType.value).substring(0, 1);

    if (!bQuote && bUseBillingSystem && !bDoNotBill && iInsuredId == 0 && sBillToType == "I") 
    {
        switch(sObject)
        {
            case "Quote":
                alert('Please select an Insured for Billing. ' + sObject + ' can not be converted to ' + sTransaction + '.');
                break;
            case "Policy":
            case "Transaction":
                alert('Please select an Insured for Billing. ' + sObject + ' can not be ' + sTransaction + '.');
                break;
        }
        return false;
    }
    return true;
}
// npadhy End MITS 18894 if Billing is enabled, Do not Bill is not Checked and Insured for Billing is not present 
// then Policy should not be Saved
//Sumit - Start(03/16/2010) - MITS# 18229 - Set FunctionToCall to update UAR details list.
function fnUAROk() {
    var sFormName = window.opener.document.getElementById("SysFormName").value;
    var objCtl=window.opener.document.getElementById("FunctionToCall");
    if (objCtl != null) {
        if (sFormName == "exposureal")
            objCtl.value = "EnhancePolicyAdaptor.UpdateUARDetailList";
        else if (sFormName == "exposurepc")
            objCtl.value = "EnhancePolicyAdaptor.UpdatePropertyUARDetailList";
    }
}
//Sumit - End
 
//Sumit - Start(03/16/2010) - MITS# 18229
function fnUARCancel()
{
    window.close();
}
//Sumit - End

//Sumit - Start(03/16/2010) - MITS# 18229 - Called when clicked on OK button from UAR pop screen.
function fnRefreshParentUAROk() {   
    var objParentDocument = window.opener.document;
    if(objParentDocument==null)
    {
	    alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
	    return false;
    }
	var objParentDocumentForm = objParentDocument.forms[0];
    if(objParentDocumentForm==null)
    {
	    alert("Error. Unable to access parent window document form. Please contact your System Administrator. ");
	    return false;
    } 
    
	var sMode = document.getElementById("mode").value;
	var sGridName = document.getElementById("gridname").value;
	var sFormName = objParentDocument.getElementById("SysFormName").value;
    var iSelectedRowPosition = document.getElementById("selectedrowposition").value;
    var iUARId = document.getElementById("selectedid").value;

    if (sFormName == "exposureal") {
        //Logic to Check if Vehicle has already been added.
        var iSelectedVehicleID = document.getElementById("UnitID").value;
        var objVehicleIds = objParentDocument.getElementById("ALUARDetailsListGrid_UarIds");
        if (objVehicleIds != null) {
            var arrVehicleIds = objVehicleIds.value.split("|");
            for (i = 0; i < arrVehicleIds.length; i++) {
                if (arrVehicleIds[i] == iSelectedVehicleID) {
                    alert("Selected Vehicle has already been added.You cannot add duplicate Vehicle.");
                    return;
                }
            }
        }
    }

    if (sFormName == "exposurepc") {
        //Logic to Check if Property has already been added.
        var iSelectedPropertyID = document.getElementById("PropertyID").value;
        var objPropertyIds = objParentDocument.getElementById("PCUARDetailsListGrid_UarIds");
        if (objPropertyIds != null) {
            var arrPropertyIds = objPropertyIds.value.split("|");
            for (i = 0; i < arrPropertyIds.length; i++) {
                if (arrPropertyIds[i] == iSelectedPropertyID) {
                    alert("Selected property has already been added.You cannot add duplicate Property.");
                    return;
                }
            }
        }
    }

    if (sMode == "edit" && sFormName == "exposureal")
	{
	    if (objParentDocument.getElementById("ALUARDetailsListGrid_EditRowSessionId") != null)
	    {
	        objParentDocument.getElementById("ALUARDetailsListGrid_EditRowSessionId").value = iUARId.toString() + '|' + document.forms[0].SessionId.value;
	    }
	}
	else if (sMode == "edit" && sFormName == "exposurepc") {
	    if (objParentDocument.getElementById("PCUARDetailsListGrid_EditRowSessionId") != null) {
	        objParentDocument.getElementById("PCUARDetailsListGrid_EditRowSessionId").value = iUARId.toString() + '|' + document.forms[0].SessionId.value;
	        if (objParentDocument.getElementById(sGridName + "_UarSessionIds") != null) {
	            var sUARSchdSessionId;
	            var sSchSessionId;
	            var sNewUARSchdSessionID;
	            if (objParentDocument.getElementById(sGridName + "_UarSessionIds") != null) {
	                sUARSchdSessionId = objParentDocument.getElementById(sGridName + "_UarSessionIds").value;
	            }
	            if (document.getElementById("PCUARListGrid_SelectedSessionIds") != null) {
	                sSchSessionId = document.getElementById("PCUARListGrid_SelectedSessionIds").value;
	            }
	            if (document.getElementById("ScheduleListGrid_SessionIds") != null) {
	                sNewUARSchdSessionID = document.forms[0].SessionId.value + '^' + document.getElementById("ScheduleListGrid_SessionIds").value;
	            }
	            if (sSchSessionId == "^") {
	                var sNewUARSchdSessionId = sUARSchdSessionId + "$" + sNewUARSchdSessionID;
	                objParentDocument.getElementById(sGridName + "_UarSessionIds").value = sNewUARSchdSessionId;
	            }
	            else {
	                var sNewUARSchdSessionId = sUARSchdSessionId.replace(sSchSessionId, sNewUARSchdSessionID);
	                objParentDocument.getElementById(sGridName + "_UarSessionIds").value = sNewUARSchdSessionId;
	            }
	        }
	    }
	}
	else if (sMode == "add" && sFormName == "exposureal") {
	    if (objParentDocument.getElementById(sGridName + "_NewRowSessionId") != null)
	    {
		   objParentDocument.getElementById( sGridName + "_NewRowSessionId").value = document.forms[0].SessionId.value ;
		}
		if(objParentDocument.getElementById( sGridName + "_UarSessionIds").value=="")
    		objParentDocument.getElementById( sGridName + "_UarSessionIds").value = objParentDocument.getElementById( sGridName + "_NewRowSessionId").value;
    	else
    		objParentDocument.getElementById( sGridName + "_UarSessionIds").value = objParentDocument.getElementById( sGridName + "_UarSessionIds").value + "|"+ objParentDocument.getElementById( sGridName + "_NewRowSessionId").value;
    }
    else if (sMode == "add" && sFormName == "exposurepc") {
        objParentDocument.getElementById(sGridName + "_NewRowSessionId").value = document.forms[0].SessionId.value;
        if (objParentDocument.getElementById(sGridName + "_UarSessionIds").value == "")
            objParentDocument.getElementById(sGridName + "_UarSessionIds").value = document.forms[0].SessionId.value + "^" + document.forms[0].ScheduleListGrid_SessionIds.value;
        else {
            objParentDocument.getElementById(sGridName + "_UarSessionIds").value = objParentDocument.getElementById(sGridName + "_UarSessionIds").value + "$" + document.forms[0].SessionId.value + "^" + document.forms[0].ScheduleListGrid_SessionIds.value; ;
        }
    }
    if (sFormName == "exposureal") {
        if (objParentDocument.getElementById("ALUARDetailsListGridAction") != null)
        {
            objParentDocument.getElementById("ALUARDetailsListGridAction").value = sMode;
        }
    }
    else if (sFormName == "exposurepc") {
         if (objParentDocument.getElementById("PCUARDetailsListGridAction") != null)
         {
             objParentDocument.getElementById("PCUARDetailsListGridAction").value = sMode;
         }
    //Sumit - Start(05/04/2010) - MITS# 20483 - Set Schedule session IDs for Uar
    if (objParentDocument.getElementById("PCUARDetailsListGrid_ScheduleSessionIds") != null)
            objParentDocument.getElementById("PCUARDetailsListGrid_ScheduleSessionIds").value = document.forms[0].ScheduleListGrid_SessionIds.value;
    //Sumit - End
        
    }
    //Sumit - Start(05/04/2010) - MITS# 20483 - Refresh Deleted list on parent Page.
    if (sFormName == "exposurepc") {
        var arrScheduleRowIds = document.getElementById("ScheduleListGrid_ScheduleRowIds").value.split("|");
        var objParentDeletedSchdRowIds = objParentDocument.getElementById("PCUARDetailsListGrid_DeletedScheduleRowIds");
        var objParentPostDeletedSchdRowIds = objParentDocument.getElementById("PCUARDetailsListGrid_ScheduleRowIdsPostDelete");

        var arrParentDeletedSchdRowIds = objParentDeletedSchdRowIds.value.split("|");
        var bMatch;
        var sOut;
        sOut = "";

        for (i = 0; i < arrParentDeletedSchdRowIds.length; i++) {
            if (arrParentDeletedSchdRowIds[i] != "") {
                bMatch = false;
                for (j = 0; j < arrScheduleRowIds.length; j++) {
                    if (arrScheduleRowIds[j] == arrParentDeletedSchdRowIds[i]) {
                        bMatch = true;
                        break;
                    }
                }
                if (!bMatch) {
                    if (sOut == '')
                        sOut = arrParentDeletedSchdRowIds[i];
                    else
                        sOut = sOut + '|' + arrParentDeletedSchdRowIds[i];
                }
            }
        }
        objParentDeletedSchdRowIds.value = sOut;
        if (objParentPostDeletedSchdRowIds != null) {
            if (objParentDeletedSchdRowIds.value != "") {
                if (objParentPostDeletedSchdRowIds.value == "") {
                    objParentPostDeletedSchdRowIds.value = iUARId + '^' + objParentDeletedSchdRowIds.value;
                }
                else {
                    objParentPostDeletedSchdRowIds.value = ReplaceAll(objParentPostDeletedSchdRowIds.value, '~', '^');
                    var arrScheduleIds = objParentPostDeletedSchdRowIds.value.split("^");
                    var iCount = 0;
                    var boolAddNew = false;
                    var sFinal = "";
                    //Update - Values
                    iCount = 0;
                    while (iCount < arrScheduleIds.length) {
                        if (arrScheduleIds[iCount] == iUARId.toString()) {
                            if (objParentDeletedSchdRowIds.value != "")
                                arrScheduleIds[iCount + 1] += '|' + objParentDeletedSchdRowIds.value;
                            else
                                arrScheduleIds[iCount + 1] = objParentDeletedSchdRowIds.value;
                            boolAddNew = false;
                            break;
                        }
                        else {
                            boolAddNew = true;
                        }
                        iCount += 2;
                    }
                    //Populate value
                    iCount = 0;
                    while (iCount < arrScheduleIds.length) {
                         sFinal+= arrScheduleIds[iCount] + '^' + arrScheduleIds[iCount + 1];
                         if (iCount < arrScheduleIds.length - 2) {
                            sFinal += '~';
                        }
                        iCount += 2;
                    }
                    objParentPostDeletedSchdRowIds.value = sFinal;

                    if (boolAddNew == true) {
                        objParentPostDeletedSchdRowIds.value += '~' + iUARId + '^' + objParentDeletedSchdRowIds.value;
                    }
                }
            }
        }
    }

    //Sumit - End

    //Sumit - Start(05/04/2010) - MITS# 20483
    objParentDocumentForm.PostBackAction.value = "UAR_EDIT";
    //Sumit - End
	objParentDocumentForm.SysCmd.value = 7;
    objParentDocumentForm.submit();
    window.close();		
}
//Sumit - End

//Sumit - Start(03/16/2010) - MITS# 18229
function fnClearUARGrid(lCodeId, sFormName) {
    var sExposureTypeId = document.getElementById("ExposureType_codelookup_cid");
    var answer;
    if(sExposureTypeId!=null)
    {
        if(sExposureTypeId.value!="0" && lCodeId!=sExposureTypeId.value) {
            if (sFormName == "exposureal") {
                var tblUAR = document.getElementById('ALUARDetailsListGrid_gvData_ctl00').getElementsByTagName("tbody")[0];
            }
            else if (sFormName == "exposurepc") {
                var tblUAR = document.getElementById('PCUARDetailsListGrid_gvData_ctl00').getElementsByTagName("tbody")[0];
            }
               if(tblUAR!=null)
               {
                   var rowUAR = tblUAR.getElementsByTagName("TR").length;
                   if(rowUAR > 2) {
                       if (sFormName == "exposureal")
                           answer = confirm("Please delete all UAR details for previously selected vehicle type as it has changed. Do you wish to continue?");
                       else if (sFormName == "exposurepc")
                           answer = confirm("Please delete all UAR details for previously selected property type as it has changed. Do you wish to continue?");
                        if (answer) 
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                   }
                   else
                   {
                            return true;
                   }
               }
        }
        else
        {
            return true;
        }
    }
    return true;
}
//Sumit - End

//Sumit - Start(03/25/2010) - MITS# 18229
function UarDetailsOnLoad() {
    //Sumit - Start(05/14/2010) - MITS# 20483 - Maintain tabs on Add/Edit Uar details popup screen.
    loadTabList();
    var tabObj = document.getElementById("LINKTABSUarDetail");
    var objTabAction = document.getElementById("HdnTabPress");
    var objHiddenTab = document.getElementById("HdnTab");
    if (objTabAction != null && objTabAction.value != "") {
        tabObj = document.getElementById('LINKTABS' + objTabAction.value);
        if (tabObj != null) {
            tabObj.className = "Selected";
            if (objHiddenTab.value == 'undefined')
                tabChange(objTabAction.value);
            else
                tabChange(objTabAction.value, objHiddenTab.value);
        }
    }
    else {
        if (tabObj != null) {
            if (tabObj.className == "Selected")
                tabChange(m_TabList[0].id.substring(4));
        }
    }
    //Sumit - End

    var btnUarOk = document.getElementById("btnUarOk");
    var hdnOkEnabled = document.getElementById("hdnOkEnabled");
    if (hdnOkEnabled.value == "true")
        btnUarOk.disabled = false;
    else
        btnUarOk.disabled = true;

    //Sumit - Start(04/27/2010) - MITS# 20483 - Hide lookup button if edit is clicked.
    var mode = document.getElementById("mode");
    var btnPropIdlookup = document.getElementById("propertyidcodebtn");
    var txtPropID = document.getElementById("propertyidcode");
    if (mode != null) {
        if (mode.value == "edit" && btnPropIdlookup != null) {
            btnPropIdlookup.style.display = "none";
            if (txtPropID != null) {
                txtPropID.style.backgroundColor = "#d3d3d3";
            }
        }
    }
    if (document.getElementById("ScheduleListGrid_New") != null) {
        btnSchdNewEnabled = document.getElementById("ScheduleListGrid_New");
    }
    if (document.getElementById("ScheduleListGrid_Edit") != null) {
        btnSchdEditEnabled = document.getElementById("ScheduleListGrid_Edit");
    }
    if (document.getElementById("ScheduleListGrid_Delete") != null) {
        btnSchdDeleteEnabled = document.getElementById("ScheduleListGrid_Delete");
    }
    var objScheduleAddButtonEnabled=document.getElementById("ScheduleAddButtonEnabled");
    if (objScheduleAddButtonEnabled!= null) {
        if (objScheduleAddButtonEnabled.value == "False") {
            if (btnSchdNewEnabled != null) {
                btnSchdNewEnabled.style.display = "none";
            }
        }
    }
    
    var objScheduleEditButtonEnabled=document.getElementById("ScheduleEditButtonEnabled");
    if (objScheduleEditButtonEnabled!= null) {
        if (objScheduleEditButtonEnabled.value == "False") {
            if (btnSchdEditEnabled != null) {
                btnSchdEditEnabled.style.display = "none";
            }
        }
    }

    var objScheduleDeleteButtonEnabled = document.getElementById("ScheduleDeleteButtonEnabled");
    if (objScheduleDeleteButtonEnabled != null) {
        if (objScheduleDeleteButtonEnabled.value == "False") {
            if (btnSchdDeleteEnabled != null) {
                btnSchdDeleteEnabled.style.display = "none";
            }
        }
    }
    //Sumit - End
}
//Sumit - End

//Sumit - Start(04/28/2010) - MITS# 20483 - Set FunctionToCall to update Schedule details list.
function fnScheduleOk() {
    var objCtl = window.opener.document.getElementById("FunctionToCall");
    if (objCtl != null) {
        objCtl.value = "EnhancePolicyAdaptor.UpdateScheduleList";
    }
}
//Sumit - End

//Sumit - Start(04/28/2010) - MITS# 20483
function fnScheduleCancel() {
    window.close();
}
//Sumit - End

//Sumit - Start(03/16/2010) - MITS# 20483 - Called when clicked on OK button from UAR pop screen.
function fnRefreshParentScheduleOk() {
    var objParentDocument = window.opener.document;
    if (objParentDocument == null) {
        alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
        return false;
    }
    var objParentDocumentForm = objParentDocument.forms[0];
    if (objParentDocumentForm == null) {
        alert("Error. Unable to access parent window document form. Please contact your System Administrator. ");
        return false;
    }

    var sMode = document.getElementById("mode").value;
    var sGridName = document.getElementById("gridname").value;
    var iSelectedRowPosition = document.getElementById("selectedrowposition").value;
    var iScheduleId = document.getElementById("selectedid").value;
    var objName; 
    var objAmount; 
    if (document.getElementById("Name") != null)
    {
        objName = document.getElementById("Name");
    }
    if (document.getElementById("Amount") != null)
    {
        objAmount = document.getElementById("Amount");
    }

    if (objName.value == "" || objAmount.value == "") {
        alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
        if (objName.value == "")
            objName.focus();
        else if (objAmount.value == "")
            objAmount.focus();
        else
            objName.focus();
        return;
    }

    if (sMode == "edit") {
        objParentDocument.getElementById("ScheduleListGrid_EditRowSessionId").value = iScheduleId.toString() + '|' + document.forms[0].SessionId.value;
    }
    else if (sMode == "add") {
        objParentDocument.getElementById(sGridName + "_NewRowSessionId").value = document.forms[0].SessionId.value;
        if (objParentDocument.getElementById(sGridName + "_ScheduleSessionId").value == "")
            objParentDocument.getElementById(sGridName + "_ScheduleSessionId").value = objParentDocument.getElementById(sGridName + "_NewRowSessionId").value;
        else
            objParentDocument.getElementById(sGridName + "_ScheduleSessionId").value = objParentDocument.getElementById(sGridName + "_ScheduleSessionId").value + "|" + objParentDocument.getElementById(sGridName + "_NewRowSessionId").value;
    }

    objParentDocument.getElementById("ScheduleListGridAction").value = sMode;
    objParentDocumentForm.PostBackAction.value = "SCHEDULE_EDIT";
    objParentDocumentForm.SysCmd.value = 7;
    objParentDocumentForm.submit();
    window.close();
}
//Sumit - End
//Sumit - Start(05/28/2010) - MITS# 20483 - Replace all occurences of particular character/symbol.
function ReplaceAll(Source, stringToFind, stringToReplace) {
    var temp = Source;
    var index = temp.indexOf(stringToFind);
    while (index != -1) {
        temp = temp.replace(stringToFind, stringToReplace);
        index = temp.indexOf(stringToFind);
    }
    return temp;
}
//Sumit - End

//Sumit - Start(11/08/2010) - MITS# 21950 - Enable Ok button on supplementals page of Coverages and Exposure.
function EnableSupplementalsOkButton() {
    var objSysFormName = document.getElementById("SysFormName");
    if (objSysFormName != null) {
        var sFormName = objSysFormName.value;
        switch(sFormName)
        {
            case "exposure":
            case "exposureal":
            case "exposurepc":
                if (document.getElementById('btnExpsoureOk') != null)
                    document.getElementById('btnExpsoureOk').disabled = false;
                break;
            case "Coverages":
                if (document.getElementById('btnCoveragesOk') != null)
                    document.getElementById('btnCoveragesOk').disabled = false;
                break;
        }
    }
}
//Sumit - End
