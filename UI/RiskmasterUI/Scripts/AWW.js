var IfChange='False';


function AWWCalc_PageLoad() {   
    pageLoaded();
    //Start by Shivendu for AWW Calc
    var formOption=document.forms[0].FormOption;
    var alertFlag=false;

    switch(formOption.value) {
        case '0':
        case '1':
        case '13':
        case '26':
        case '52':
        case '61':
        case '149':
        case '529':
        case '523':
        case '524':
        case '521':
        case '527':
        case '129':
        case '12':
        case '8':
        case '14':
            break;
        default:
            alertFlag=true;
            break;

    }

    if(alertFlag) {
        alert("Please check the AWW option for this jurisdiction.");
        self.close();
    }

    //End by Shivendu for Aww Calc	
    if(document.getElementById("IfClose").value=='True') {
        if(document.getElementById("CompensationRate").value!="") {
            if(eval('window.opener.document.forms[0].comprate')!=null) {
                window.opener.document.forms[0].comprate.value=parseFloat(document.forms[0].CompensationRate.value.replace(/[,\$]/g,"")).toFixed(2);
            }
            else if(eval('window.opener.document.forms[0].comprate1')!=null) {
                window.opener.document.forms[0].comprate1.value=parseFloat(document.forms[0].CompensationRate.value.replace(/[,\$]/g,"")).toFixed(2);
            }
            else if(eval('window.opener.document.forms[0].UseCaseMgmt')!=null&&window.opener.document.forms[0].UseCaseMgmt.value=="true") {
                window.opener.document.forms[0].comprateHide.value=parseFloat(document.forms[0].CompensationRate.value.replace(/[,\$]/g,"")).toFixed(2);
            }
            else if(eval('window.opener.document.forms[0].UseCaseMgmt')!=null&&window.opener.document.forms[0].UseCaseMgmt.value=="false") {
                window.opener.document.forms[0].comprate1Hide.value=parseFloat(document.forms[0].CompensationRate.value.replace(/[,\$]/g,"")).toFixed(2);
            }
        }
        window.close();
    }
    //Start by Shivendu for AWW Calc
    var hideFlag=false;
    var statCode=document.forms[0].StateCode;
    switch(statCode.value) {
        //Arizona,Michigan,Nevada,Pennsylvania,Rhode Island,Washington,Wyoming,Colorado,Kentucky,Montana are special states 
        case 'AZ':
        case 'NV':
        case 'WY':
            hideFlag=true;
            document.forms[0].btnCalc.style.visibility="hidden";
            document.forms[0].btnClear.style.visibility="hidden";
            break;
        case 'WA':
        case 'VT':
            hideFlag=true;
            document.forms[0].btnClear.style.visibility="hidden";
            break;
        //Start by Shivendu for MITS 12095 
        case 'PA':
        case 'MI':
        case 'MT':
        case 'KY':
        case 'CO':
            document.forms[0].IncludeZero_Chk.disabled=true;
            break;
        //End by Shivendu for MITS 12095 
    }
    if(hideFlag) {
        document.forms[0].LastAWW.readOnly=true;
        return;

    }
    //End by Shivendu for AWW Calc
    /*Appears to be superfluous code--Ratheen
    if(document.forms[0].CloseFlag.value=="1")
    {
    window.close();
    return true;
    }*/
    if(document.forms[0].LastWorkWeekDate.value!='') {
        AWWCalc_OnDateChange();
    }

    if(document.forms[0].FormOption.value!="149") {

        var txtWksLstYr=document.getElementById("txtWeeksLastYear");
        txtWksLstYr.parentElement.style.display="none";
        txtWksLstYr=null;

        var ovrtime=document.getElementById("Overtime");
        ovrtime.parentElement.style.display="none";
        ovrtime=null;
    }

    document.forms[0].LastAWW.readOnly=true;

    if(document.forms[0].IncludeZero.value=='-1') {
        document.forms[0].IncludeZero_Chk.checked=true;
    }
    else
        document.forms[0].IncludeZero_Chk.checked=false;
}

function AWWCalc_OnDateChange() {   
    // Aman Multi Currency -- on entering the invalid Date(ss..) was making the week labels as NaN/NaN/NaN   
    dateLostFocus("LastWorkWeekDate");
    if (document.forms[0].LastWorkWeekDate.value != "") {
        var dt = new Date(document.forms[0].LastWorkWeekDate.value);
        if (document.forms[0].LastWorkWeekDate.value != '') {
            for (i = 51; i >= 0; i--) {
                var mon = dt.getMonth() + 1;
                var ctrlNam = 'lblWeek' + i;
                var ctrl = document.getElementById(ctrlNam);
                if (ctrl != null)
                    ctrl.innerText = mon + '/' + dt.getDate() + '/' + dt.getFullYear() + ':';
                dt = dateAdd('ww', -1, dt);
            }
        }
        else {
            for (i = 51; i >= 0; i--) {
                var ctrlNam = 'lblWeek' + i;
                var ctrl = document.getElementById(ctrlNam);
                if (ctrl != null)
                    ctrl.innerText = '';
            }
        }
    }
}

function dateAdd(p_Interval,p_Number,p_Date) {
    p_Number=new Number(p_Number);
    var dt=p_Date;
    switch(p_Interval.toLowerCase()) {
        case "yyyy": 
            {// year
                dt.setFullYear(dt.getFullYear()+p_Number);
                break;
            }
        case "q": 
            {		// quarter
                dt.setMonth(dt.getMonth()+(p_Number*3));
                break;
            }
        case "m": 
            {		// month
                dt.setMonth(dt.getMonth()+p_Number);
                break;
            }
        case "y": 	// day of year
        case "d": 	// day
        case "w": 
            {		// weekday
                dt.setDate(dt.getDate()+p_Number);
                break;
            }
        case "ww": 
            {	// week of year
                dt.setDate(dt.getDate()+(p_Number*7));
                break;
            }
        case "h": 
            {		// hour
                dt.setHours(dt.getHours()+p_Number);
                break;
            }
        case "n": 
            {		// minute
                dt.setMinutes(dt.getMinutes()+p_Number);
                break;
            }
        case "s": 
            {		// second
                dt.setSeconds(dt.getSeconds()+p_Number);
                break;
            }
        case "ms": 
            {		// second
                dt.setMilliseconds(dt.getMilliseconds()+p_Number);
                break;
            }
    }
    return dt;
}

function AWWCalc_SetAll() {
    var iWeek;
    switch(document.forms[0].FormOption.value) {
        case '0':
            iWeek='52';
            break;

        case '13':
            iWeek='13';
            break;

        case '26':
            iWeek='26';
            break;

        case '52':
            iWeek='52';
            break;

        case '61':
            iWeek='14';
            break;

        case '149':
            iWeek='13';
            break;

        case '529':
            iWeek='52';
            break;
        //Start by Shivendu for AWW Calculator 
        case '12':
            ibase=40;
            break;
        case '129':
            ibase=40;
            break;
        case '8':
            ibase=44;
            break;
        case '523':
            ibase=0;
            break;
        case '524':
            ibase=0;
            break;
        case '521':
            ibase=0;
            break;
        case '527':
            ibase=0;
            break;
        case '14':
            ibase=38;
            break;
        //End by Shivendu for AWW Calculator 
    }

    var msg=confirm("Are you sure want to over-write all "+iWeek+" Weekly Wages?");
    if(msg) {
        var objCtrl = document.getElementById("ConstantWage");  //Aman Multi Currency --Start
        var iConstantWage = 0;
        if (objCtrl != null) {
            AWW_MultiCurrencyToDecimal(objCtrl);
            iConstantWage = objCtrl.getAttribute("Amount");
        }
        for(i=0;i<=51;i++) {
            var ctrlNam='Week'+i;
            var ctrl=document.getElementById(ctrlNam);
            if (ctrl != null) {                
                ctrl.value = iConstantWage;
                AWW_MultiCurrencyOnBlur(ctrl);
            }  //Aman Multi Currency --End
        }
    }
}

function WrkDysLostFocus(objCtrl) {    
    ClculateConstWage();
    //return numLostFocus(objCtrl);
}

function DailyEarnLostFocus(objCtrl) {    
    ClculateConstWage();
    //return currencyLostFocus(objCtrl);
}

function ClculateConstWage() {    
    // npadhy MITS 15078 Problem of value getting converted to NaN is solved
    var dWorkDaysPerWeek = document.forms[0].WorkDaysPerWeek.value;
    var objCtrl = document.getElementById("DailyEarn");
    AWW_MultiCurrencyToDecimal(objCtrl);
    var Amount = objCtrl.getAttribute("Amount");
    //var dDailyEarn=document.forms[0].DailyEarn.value;
    //dWorkDaysPerWeek=dWorkDaysPerWeek.replace(/[,\$]/g,""); //Aman Multi Currency
    //dDailyEarn=dDailyEarn.replace(/[,\$]/g,"");
    // document.forms[0].ConstantWage.value=formatCurrency(parseFloat(dWorkDaysPerWeek)*parseFloat(dDailyEarn));
    objCtrl = document.getElementById("ConstantWage");
    objCtrl.value = parseFloat(dWorkDaysPerWeek) * parseFloat(Amount);
    AWW_MultiCurrencyOnBlur(objCtrl);
}

function currencyLostFocus(objCtrl) {
    var dbl=new String(objCtrl.value);

    // Strip and Validate Input
    // BSB Remove commas first so parseFloat doesn't incorrectly truncate value.
    dbl=dbl.replace(/[,\$]/g,"");

    if(dbl.length==0)
        return false;

    if(isNaN(parseFloat(dbl))) {
        objCtrl.value="";
        return false;
    }




    // Reassemble formatted Currency Value string.

    objCtrl.value=formatCurrency(dbl);
    return true;
}
function WeeksLostFocus(objCtrl) //edit by rahul-mits 9785
{
    if(objCtrl==null) return false;
    var dbl=new String(objCtrl.value);

    // Strip and Validate Input
    // BSB Remove commas first so parseFloat doesn't incorrectly truncate value.
    dbl=dbl.replace(/[,\$]/g,"");

    if(dbl.length==0)
        return false;
    dbl=parseInt(dbl,10);
    if(isNaN(parseFloat(dbl))) {
        objCtrl.value="";
        return false;
    }
    if(dbl<0||dbl>52) {
        objCtrl.value="";
        return false;
    }
    objCtrl.value=dbl;
    return true;
}
function formatCurrency(currencyVal) {
    //round to 2 places
    currencyVal=parseFloat(currencyVal);
    currencyVal=currencyVal.toFixed(2);

    //Handle Full Integer Section (Formats in groups of 3 with comma.)
    var base=new String(currencyVal);
    base=base.substring(0,base.lastIndexOf('.'));
    var newbase="";
    var j=0;
    var i=base.length-1;
    for(i;i>=0;i--) {
        if(j==3) {
            newbase=","+newbase;
            j=0;

        }
        newbase=base.charAt(i)+newbase;
        j++;
    }


    // Handle Fractional Part
    var str=new String(currencyVal);
    str=str.substring(str.lastIndexOf(".")+1);
    return newbase+"."+str;
}
function AWWCalc_Calculate() { 
    var ibase=0;
    var iTemp=0;
    var dTotal=0;
    var dAWW=0;
    var dWage=0;
    var iWeeksLstYr=0;
    var objCtrl = null;
    switch(document.forms[0].FormOption.value) {
        case '0':
            ibase=0;
            break;

        case '13':
            ibase=39;
            break;

        case '26':
            ibase=26;
            break;

        case '52':
            ibase=0;
            break;

        case '61':
            ibase=38;
            break;

        case '149':
            ibase=39;
            break;

        case '529':
            ibase=0;
            break;
        //Start by Shivendu for AWW Calculator 
        case '12':
            ibase=40;
            break;
        case '129':
            ibase=40;
            break;
        case '8':
            ibase=44;
            break;
        case '523':
            ibase=0;
            break;
        case '524':
            ibase=0;
            break;
        case '521':
            ibase=0;
            break;
        case '527':
            ibase=0;
            break;
        case '14':
            ibase=38;
            break;
        //End by Shivendu for AWW Calculator 
    }
    for(i=ibase;i<=51;i++) {
        var ctrlNam='Week'+i;
        var wage = document.getElementById(ctrlNam);
        AWW_MultiCurrencyToDecimal(wage);                                     //Aman Multi Currency-- Start
        //var valWithoutWildCardChars=wage.value.replace(/[,\$]/g,"");
        //dWage=valWithoutWildCardChars*1;
        dWage = Number(wage.getAttribute("Amount"));
        if(dWage==0) {
            //If statement added by Shivendu for null check
            if(document.forms[0].IncludeZero_Chk!=null)
                if(document.forms[0].IncludeZero_Chk.checked==true) {
                dTotal=Number(dTotal) + Number(dWage);
                iTemp++;
            }
        }
        else {
            dTotal = Number(dTotal) + Number(dWage);
            iTemp++;
        }
    }
    //If statement added by Shivendu for null check
    objCtrl = document.getElementById("txtBonuses");
    if (objCtrl != null)
    //Geeta 06/18/07 : Modified for Mits 9712	    
        if (document.forms[0].FormOption.value != "149") {
            AWW_MultiCurrencyToDecimal(objCtrl);
            dTotal = Number(dTotal) + Number(objCtrl.getAttribute("Amount")) * 1
    }
    if(document.forms[0].FormOption.value=='61'&&iTemp>13)//Florida
        iTemp=13;
    //Start by Shivendu for AWW Calc
    if(document.forms[0].StateCode.value=='VT') {
        iTemp=12;
    }
    if(document.forms[0].StateCode.value=='WA') {
        iTemp=12;
    }
    if(document.forms[0].StateCode.value=='MT') {
        iTemp=52;
    }
    if(document.forms[0].StateCode.value=='CO') {
        //Modified by Shivendu for MITS 12095
        iTemp=52;
    }
    if(document.forms[0].StateCode.value=='MI'||document.forms[0].StateCode.value=='PA'||document.forms[0].StateCode.value=='KY') {

        //Added by Shivendu for MITS 12095
        if(document.forms[0].StateCode.value=='MI')
            var weeksForMI=new Array(52);

        var dTotalFirstQuarter=0;
        var dTotalSecondQuarter=0;
        var dTotalThirdQuarter=0;
        var dTotalFourthQuarter=0;
        //Added by Shivendu for MITS 12095 
        var dTotalBest39Weeks=0
        for(i=ibase;i<=51;i++) {
            var ctrlNam='Week'+i;
            var wage=document.getElementById(ctrlNam);
            //var valWithoutWildCardChars=wage.value.replace(/[,\$]/g,"");
            // dWage=valWithoutWildCardChars*1;
            AWW_MultiCurrencyToDecimal(wage);
            dWage = wage.getAttribute("Amount") * 1;
            if(i<13)
                dTotalFirstQuarter = Number(dTotalFirstQuarter) + Number(dWage);
            else if(i>=13&&i<26)
                dTotalSecondQuarter = Number(dTotalSecondQuarter) + Number(dWage);
            else if(i>=26&&i<39)
                dTotalThirdQuarter = Number(dTotalThirdQuarter) + Number(dWage);
            else if(i>=39&&i<52)
                dTotalFourthQuarter = Number(dTotalFourthQuarter) + Number(dWage);

            //Added by Shivendu for MITS 12095
            if(document.forms[0].StateCode.value=='MI')
                weeksForMI[i]=dWage;

        }
        var quarters=new Array(4);
        quarters[0]=dTotalFirstQuarter;
        quarters[1]=dTotalSecondQuarter;
        quarters[2]=dTotalThirdQuarter;
        quarters[3]=dTotalFourthQuarter;
        quarters.sort(sortNumber);
        if(document.forms[0].StateCode.value=='MI') {
            //Modified by Shivendu for MITS 12095
            //dTotal = quarters[1] + quarters[2]  + quarters[3] + document.forms[0].txtBonuses.value * 1;
            //iTemp = 39;

            weeksForMI.sort(sortNumber);
            for(i=51;i>=13;i--) {
                dTotalBest39Weeks = Number(dTotalBest39Weeks) + Number(weeksForMI[i]);
            }
            dTotal = Number(dTotalBest39Weeks) + Number(objCtrl.getAttribute("Amount")) * 1;
            iTemp=39;
        }
        else if(document.forms[0].StateCode.value=='KY') {
            dTotal = Number(quarters[3]) + Number(objCtrl.getAttribute("Amount")) * 1;
            iTemp=13;
        }
        else {
            dTotal = Number(quarters[1]) + Number(quarters[2]) + Number(quarters[3]) + Number(objCtrl.getAttribute("Amount")) * 1;
            iTemp=39;
        }


    }
    IfChange='True';
    //End by Shivendu for AWW Calc

    if(iTemp!=0)
        dAWW = Number(dTotal) / Number(iTemp);
        //Aman MITS 27451
    if (Number(dTotal) <= 0) {
        IfChange = 'False';
    }

    //Geeta 06/18/07 : Modified for Mits 9712	
    if(document.forms[0].FormOption.value=="149") {
        //If statement added by Shivendu for null check
        if(document.forms[0].txtWeeksLastYear!=null)
            iWeeksLstYr=document.forms[0].txtWeeksLastYear.value;
        if(iWeeksLstYr>0) {
            //If statement added by Shivendu for null check
            //if(document.forms[0].txtBonuses!=null)
            //    dAWW=dAWW+document.forms[0].txtBonuses.value/iWeeksLstYr;
            objCtrl = document.getElementById("txtBonuses");
            if (objCtrl != null) {
                AWW_MultiCurrencyToDecimal(objCtrl);
                dAWW = Number(dAWW) + Number(objCtrl.getAttribute("Amount")) / iWeeksLstYr;
            }
            //If statement added by Shivendu for null check
            //if(document.forms[0].Overtime!=null)
            //    dAWW=dAWW+document.forms[0].Overtime.value/iWeeksLstYr;
            objCtrl = document.getElementById("Overtime");
            if (objCtrl != null) {
                AWW_MultiCurrencyToDecimal(objCtrl);
                dAWW = Number(dAWW) + Number(objCtrl.getAttribute("Amount")) / iWeeksLstYr;
            }
        }
    }

    //document.forms[0].AWW.value = formatCurrency(dAWW);

    objCtrl = document.getElementById("AWW");
    if (objCtrl != null) {
        objCtrl.value = Number(dAWW);
        AWW_MultiCurrencyOnBlur(objCtrl);
    }                                                               //Aman Multi Currency Changes made in between-- End
}

function OnCheckChange() {
    if(document.forms[0].IncludeZero_Chk.checked==true) {
        document.forms[0].IncludeZero.value='-1';
    }
    else
        document.forms[0].IncludeZero.value='0';
}

function AWWCalc_Save() {    
    //Added by Shivendu for MITS 12095	
    if(IfChange!='True') {
        alert("You must enter or calculate an AWW in order to save AWW to Weekly Wage");
        return false;
    }

    var arrWeekField=new String(document.forms[0].WeekName.value).split(",");

    if(arrWeekField[0].length>0) {
        var arrDayField="";
        var arrHrsField="";
        var ctrlWeek=null;
        var ctrlDays=null;
        var ctrlHrs=null;
        var ctrlDaysFlag=eval('document.forms[0].dys'+arrWeekField[0]);
        var ctrlHrsFlag=eval('document.forms[0].hrs'+arrWeekField[0]);

        for (iCount = 0; iCount <= (arrWeekField.length - 1); iCount++) {               //Aman Multi Currency  --Start
            //ctrlWeek = eval('document.forms[0].' + arrWeekField[iCount]);
            ctrlWeek = document.getElementById(arrWeekField[iCount]);
            AWW_MultiCurrencyToDecimal(ctrlWeek);   
            if(iCount==0) { 
                if (document.getElementById("FormOption").value == '61') {                    
                    //document.forms[0].WeekWage.value=ctrlWeek.value;
                    document.forms[0].WeekWage.value = ctrlWeek.getAttribute("Amount");
                }
                else {

                    // document.forms[0].WeekWage.value=ctrlWeek[0].value;
                    //smishra25 10/05/2010 MITS 22263, ctrlWeek cannot be an array
                    //document.forms[0].WeekWage.value = ctrlWeek.value;
                    document.forms[0].WeekWage.value = ctrlWeek.getAttribute("Amount");
                }
            }
            else {
                if(document.getElementById("FormOption").value=='61'){
                    //document.forms[0].WeekWage.value=document.forms[0].WeekWage.value+'|'+ctrlWeek.value;
                    document.forms[0].WeekWage.value = document.forms[0].WeekWage.value + '|' + ctrlWeek.getAttribute("Amount");
                }
                else{
                    //document.forms[0].WeekWage.value=document.forms[0].WeekWage.value+'|'+ctrlWeek[0].value;
                    //smishra25 10/05/2010 MITS 22263, ctrlWeek cannot be an array
                    //document.forms[0].WeekWage.value = document.forms[0].WeekWage.value + '|' + ctrlWeek.value;
                    document.forms[0].WeekWage.value = document.forms[0].WeekWage.value + '|' + ctrlWeek.getAttribute("Amount");
                }    
            }
        }

        if(ctrlDaysFlag!=null) {
            for(iCount=0;iCount<=(arrWeekField.length-1);iCount++) {
                //ctrlDays=eval('document.forms[0].dys'+arrWeekField[iCount]);
                ctrlDays = document.getElementById("dys" + arrWeekField[iCount]);
                AWW_MultiCurrencyToDecimal(ctrlDays);  
                if(iCount==0) {
                    // document.forms[0].WeekDays.value=ctrlDays.value;
                    document.forms[0].WeekDays.value = ctrlDays.getAttribute("Amount");
                }
                else {
                    //document.forms[0].WeekDays.value=document.forms[0].WeekDays.value+'|'+ctrlDays.value;
                    document.forms[0].WeekDays.value = document.forms[0].WeekDays.value + '|' + ctrlDays.getAttribute("Amount");
                }
            }
        }

        if(ctrlHrsFlag!=null) {
            for(iCount=0;iCount<=(arrWeekField.length-1);iCount++) {
                //ctrlHrs=eval('document.forms[0].hrs'+arrWeekField[iCount]);
                ctrlHrs = document.getElementById("hrs" + arrWeekField[iCount]);
                AWW_MultiCurrencyToDecimal(ctrlHrs);  
                if(iCount==0) {
                    //document.forms[0].WeekHours.value=ctrlHrs.value;
                    document.forms[0].WeekHours.value = ctrlHrs.getAttribute("Amount");
                }
                else {
                    //document.forms[0].WeekHours.value=document.forms[0].WeekHours.value+'|'+ctrlHrs.value;
                    document.forms[0].WeekHours.value = document.forms[0].WeekHours.value + '|' + ctrlHrs.getAttribute("Amount");
                }
            }
        }
    }

    var dblAWW=0;
    var bSuccess = false;
    var objCtrl = document.getElementById("AWW");
    if (objCtrl != null) {
        AWW_MultiCurrencyToDecimal(objCtrl);
        dblAWW = objCtrl.getAttribute("Amount");
    }
    //If condition added by Shivendu for AWW Calc 
    if(document.forms[0].StateCode.value!='AZ'&&document.forms[0].StateCode.value!='NV'&&document.forms[0].StateCode.value!='WY'&&document.forms[0].StateCode.value!='WA'&&document.forms[0].StateCode.value!='VT'&&document.forms[0].StateCode.value!='CO') {
        if(Number(dblAWW)>0&&document.forms[0].LastWorkWeekDate.value!='') {
            if(document.forms[0].StateCode.value=='CA') {
                alert('The update to the Average Weekly Wage will not make any automatic adjustments '+'\n'+
				   'to benefit payments made or pending (Scheduled Payments) of the following types: '+'\n'+
				   'TTD, TPD, PTD, PPD, Life Pension and/or VRMA.'+'\n'+'\n'+
				   'You will need to review all transactions made and pending to determine whether '+'\n'+
				   'you need to make adjustments to completed or pending benefit payments.');
            }
        }
        else if(Number(dblAWW)<=0) {
            alert('You must enter or calculate an AWW in order to save AWW to Weekly Wage');
            return false;
        }
        else if(document.forms[0].LastWorkWeekDate.value=='') {
            alert('You must enter a Last Week Worked date in order to save AWW to Weekly Wage');
            return false;
        }
    }
    m_DataChanged=false;
    document.getElementById("IfClose").value='True';
    //document.forms[0].Action.value='Save';

    //------- Added by Nikhil. for setting values on the parent page. ----//
    //TR# 1224
    /*var ctrl=eval('window.opener.document.forms[0].empweeklyrate');
    if(ctrl!=null) {
        //window.opener.document.forms[0].empweeklyrate.value=parseFloat(document.forms[0].AWW.value.replace(/[,\$]/g,"")).toFixed(2);
        window.opener.document.forms[0].empweeklyrate.value = objCtrl.getAttribute("Amount");
        AWW_MultiCurrencyOnBlur(window.opener.document.forms[0].empweeklyrate);
    }
    else {
        ctrl=eval('window.opener.document.forms[0].empweeklyrate1');
        if (ctrl != null) {
            //window.opener.document.forms[0].empweeklyrate1.value=parseFloat(document.forms[0].AWW.value.replace(/[,\$]/g,"")).toFixed(2);
            window.opener.document.forms[0].empweeklyrate1.value = objCtrl.getAttribute("Amount");
            AWW_MultiCurrencyOnBlur(window.opener.document.forms[0].empweeklyrate1);
        }
        else {
            //window.opener.document.forms[0].empweeklyrate2.value = parseFloat(document.forms[0].AWW.value.replace(/[,\$]/g, "")).toFixed(2);
            window.opener.document.forms[0].empweeklyrate2.value = objCtrl.getAttribute("Amount");
            AWW_MultiCurrencyOnBlur(window.opener.document.forms[0].empweeklyrate2);
        }
    }*/
    window.opener.setDataChanged(true);
    /*if(document.getElementById("LastAWW").value!=document.getElementById("AWW").value)
    {
    window.opener.setDataChanged(true);
    }*/
    //-------------------------------------------------------------------//
    //window.document.forms['frmData'].submit();

    //added by igupta3 MITS: 32200
    if (document.getElementById("hdnCalcbutton") != null) {
        document.getElementById("hdnCalcbutton").value = "Ok";
    }
}

function setDataChangedKD() {
    //checking length and format for negative number
    if(document.forms[0].WorkDaysPerWeek.value.charAt(0)=='-') {
        document.forms[0].WorkDaysPerWeek.value="";
    }
    //checking length and format for positive number
    else {
        if(document.forms[0].WorkDaysPerWeek.value.indexOf('.')>=0) {
            var pos1=document.forms[0].WorkDaysPerWeek.value.indexOf('.');
            var str1=document.forms[0].WorkDaysPerWeek.value.substring(0,pos1);
            var str2=document.forms[0].WorkDaysPerWeek.value.substring(pos1+1,document.forms[0].WorkDaysPerWeek.value.length);
            if(str1.length>1)
            { document.forms[0].WorkDaysPerWeek.value=document.forms[0].WorkDaysPerWeek.value.substring(0,1); }
            else if(str2.length>1)
            { document.forms[0].WorkDaysPerWeek.value=document.forms[0].WorkDaysPerWeek.value.substring(0,document.forms[0].WorkDaysPerWeek.value.length-1); }
        }
        else {
            if (document.forms[0].WorkDaysPerWeek.value.length > 1) {
               if(document.forms[0].WorkDaysPerWeek.value.substring(0, 1) == "0")   //Aman Multi Currency Start
                   document.forms[0].WorkDaysPerWeek.value = document.forms[0].WorkDaysPerWeek.value.substring(1, 2);
                else
                   document.forms[0].WorkDaysPerWeek.value = document.forms[0].WorkDaysPerWeek.value.substring(0, 1);
            }
            else
                document.forms[0].WorkDaysPerWeek.value = document.forms[0].WorkDaysPerWeek.value;
        }
    }

    //checking the validation for a number						
    if(document.forms[0].WorkDaysPerWeek.value.charAt(0)!=".") {
        if(isNaN(document.forms[0].WorkDaysPerWeek.value)==true) {
            document.forms[0].WorkDaysPerWeek.value=document.forms[0].WorkDaysPerWeek.value.substring(0,document.forms[0].WorkDaysPerWeek.value.length-1);
        }
    }

    if(isNaN(document.forms[0].WorkDaysPerWeek.value)==false) {
        if(parseFloat(document.forms[0].WorkDaysPerWeek.value)>=8)
            document.forms[0].WorkDaysPerWeek.value="";
        else if(parseFloat(document.forms[0].WorkDaysPerWeek.value)<8) {
            if(parseFloat(document.forms[0].WorkDaysPerWeek.value)>7) {
                if(document.forms[0].WorkDaysPerWeek.value.indexOf('.')>=0) {
                    var pos1=document.forms[0].WorkDaysPerWeek.value.indexOf('.');
                    var str1=document.forms[0].WorkDaysPerWeek.value.substring(0,pos1);
                    document.forms[0].WorkDaysPerWeek.value=str1;
                } 
            } 
        }
        else if(parseFloat(document.forms[0].WorkDaysPerWeek.value)<=7)
            document.forms[0].WorkDaysPerWeek.value=document.forms[0].WorkDaysPerWeek.value;
    }
}
function IfClear() {
                                            //Aman Multi Currency Start
    var objCtrl = document.getElementById("AWW");
    if (objCtrl != null) {
        objCtrl.value = 0;
        AWW_MultiCurrencyOnBlur(objCtrl);
    }
    //document.getElementById("AWW").value='0.0';
    //document.getElementById("txtBonuses").value='0.0';
    if((document.getElementById("FormOption").value=='13')||(document.getElementById("FormOption").value=='149')) {
        for(iCount=39;iCount<=51;iCount++) {
            var sTemp="";
            iCount=iCount+"";
            sTemp="Week"+iCount;
            objCtrl = document.getElementById(sTemp);
            if (objCtrl != null) {
                objCtrl.value = 0;
                AWW_MultiCurrencyOnBlur(objCtrl);
            }
            //document.getElementById(sTemp).value='0.0';
            if(document.getElementById("FormOption").value=='149') {
                sTemphrs="hrs"+sTemp;
                //document.getElementById(sTemphrs).value='0.0';
                objCtrl = document.getElementById(sTemphrs);
                if (objCtrl != null) {
                    objCtrl.value = 0;
                    AWW_MultiCurrencyOnBlur(objCtrl);
                }
            }
        }
    }
    if((document.getElementById("FormOption").value=='52')||(document.getElementById("FormOption").value=='529')) {
        for(iCount=0;iCount<=51;iCount++) {
            var sTemp="";
            iCount=iCount+"";
            sTemp="Week"+iCount;
            //document.getElementById(sTemp).value='0.0';
            objCtrl = document.getElementById(sTemp);
            if (objCtrl != null) {
                objCtrl.value = 0;
                AWW_MultiCurrencyOnBlur(objCtrl);
            }
            if(document.getElementById("FormOption").value=='529') {
                sTempdys="dys"+sTemp;
                //document.getElementById(sTempdys).value='0';
                objCtrl = document.getElementById(sTempdys);
                if (objCtrl != null) {
                    objCtrl.value = 0;
                    AWW_MultiCurrencyOnBlur(objCtrl);
                }
            }
        }
    }
    if(document.getElementById("FormOption").value=='26') {
        for(iCount=26;iCount<=51;iCount++) {
            var sTemp="";
            iCount=iCount+"";
            sTemp="Week"+iCount;
            //document.getElementById(sTemp).value='0.0';
            objCtrl = document.getElementById(sTemp);
            if (objCtrl != null) {
                objCtrl.value = 0;
                AWW_MultiCurrencyOnBlur(objCtrl);
            }
        }
    }
    if(document.getElementById("FormOption").value=='61') {
        for(iCount=38;iCount<=51;iCount++) {
            var sTemp="";
            iCount=iCount+"";
            sTemp="Week"+iCount;
            //document.getElementById(sTemp).value='0.0';
            objCtrl = document.getElementById(sTemp);
            if (objCtrl != null) {
                objCtrl.value = 0;
                AWW_MultiCurrencyOnBlur(objCtrl);
            }
            sTempdys="dys"+sTemp;
            //document.getElementById(sTempdys).value='0';
            objCtrl = document.getElementById(sTempdys);
            if (objCtrl != null) {
                objCtrl.value = 0;
                AWW_MultiCurrencyOnBlur(objCtrl);
            }

            sTemphrs="hrs"+sTemp;
            //document.getElementById(sTemphrs).value='0.0';
            objCtrl = document.getElementById(sTemphrs);
            if (objCtrl != null) {
                objCtrl.value = 0;
                AWW_MultiCurrencyOnBlur(objCtrl);
            }
        }
    }
    return false;

}
function AskSave() {
    if(IfChange=='True') {
        if(confirm("Data has changed.Do you want to save the changes?")) {
            document.getElementById("IfClose").value='True';            
            var ctrl = eval('window.opener.document.forms[0].empweeklyrate');
            var objCtrl = document.getElementById("AWW");   //Aman Multi Currency enh --start
            AWW_MultiCurrencyToDecimal(objCtrl);
            /*
            if(ctrl!=null) {
                //window.opener.document.forms[0].empweeklyrate.value=parseFloat(document.forms[0].AWW.value.replace(/[,\$]/g,"")).toFixed(2);
                window.opener.document.forms[0].empweeklyrate.value = objCtrl.getAttribute("Amount");
                AWW_MultiCurrencyOnBlur(window.opener.document.forms[0].empweeklyrate);
            }
            else {
                ctrl=eval('window.opener.document.forms[0].empweeklyrate1');
                if (ctrl != null) {
                    //window.opener.document.forms[0].empweeklyrate1.value = parseFloat(document.forms[0].AWW.value.replace(/[,\$]/g, "")).toFixed(2);
                    window.opener.document.forms[0].empweeklyrate1.value = objCtrl.getAttribute("Amount");
                    AWW_MultiCurrencyOnBlur(window.opener.document.forms[0].empweeklyrate1);
                }
                else {
                    //window.opener.document.forms[0].empweeklyrate2.value = parseFloat(document.forms[0].AWW.value.replace(/[,\$]/g, "")).toFixed(2);
                    window.opener.document.forms[0].empweeklyrate2.value = objCtrl.getAttribute("Amount");
                    AWW_MultiCurrencyOnBlur(window.opener.document.forms[0].empweeklyrate2);
                }
            }*/
            //window.opener.document.forms[0].empweeklyrate.value=parseFloat(document.forms[0].AWW.value.replace(/[,\$]/g ,"")).toFixed(2);
            var arrWeekField=new String(document.forms[0].WeekName.value).split(",");

            if(arrWeekField[0].length>0) {
                var arrDayField="";
                var arrHrsField="";
                var ctrlWeek=null;
                var ctrlDays=null;
                var ctrlHrs=null;
                var ctrlDaysFlag=eval('document.forms[0].dys'+arrWeekField[0]);
                var ctrlHrsFlag=eval('document.forms[0].hrs'+arrWeekField[0]);

                for(iCount=0;iCount<=(arrWeekField.length-1);iCount++) {
                    //ctrlWeek=eval('document.forms[0].'+arrWeekField[iCount]);
                    ctrlWeek = document.getElementById(arrWeekField[iCount]);
                    AWW_MultiCurrencyToDecimal(ctrlWeek);   
                    if(iCount==0) {
                        if(document.getElementById("FormOption").value=='61'){
                            //document.forms[0].WeekWage.value=ctrlWeek.value;
                            document.forms[0].WeekWage.value = ctrlWeek.getAttribute("Amount");
                        }
                        else{
                            //document.forms[0].WeekWage.value=ctrlWeek[0].value;
                            document.forms[0].WeekWage.value = ctrlWeek.getAttribute("Amount");
                        }
                    }
                    else {
                        if(document.getElementById("FormOption").value=='61'){
                            //document.forms[0].WeekWage.value=document.forms[0].WeekWage.value+'|'+ctrlWeek.value;
                            document.forms[0].WeekWage.value = document.forms[0].WeekWage.value + '|' + ctrlWeek.getAttribute("Amount");
                        }
                        else{
                            //document.forms[0].WeekWage.value=document.forms[0].WeekWage.value+'|'+ctrlWeek[0].value;
                            document.forms[0].WeekWage.value = document.forms[0].WeekWage.value + '|' + ctrlWeek.getAttribute("Amount"); ;
                        }    
                    }
                }

                if(ctrlDaysFlag!=null) {
                    for(iCount=0;iCount<=(arrWeekField.length-1);iCount++) {
                        //ctrlDays=eval('document.forms[0].dys'+arrWeekField[iCount]);
                        ctrlDays = document.getElementById("dys" + arrWeekField[iCount]);
                        AWW_MultiCurrencyToDecimal(ctrlDays);   
                        if(iCount==0) {
                            //document.forms[0].WeekDays.value=ctrlDays.value;
                            document.forms[0].WeekDays.value = ctrlDays.getAttribute("Amount");
                        }
                        else {
                            //document.forms[0].WeekDays.value=document.forms[0].WeekDays.value+'|'+ctrlDays.value;
                            document.forms[0].WeekDays.value = document.forms[0].WeekDays.value + '|' + ctrlDays.getAttribute("Amount");
                        }
                    }
                }

                if(ctrlHrsFlag!=null) {
                    for(iCount=0;iCount<=(arrWeekField.length-1);iCount++) {
                        //ctrlHrs=eval('document.forms[0].hrs'+arrWeekField[iCount]);
                        ctrlHrs=document.getElementById("hrs"+arrWeekField[iCount]);
                        AWW_MultiCurrencyToDecimal(ctrlHrs);   
                        if(iCount==0) {
                            //document.forms[0].WeekHours.value=ctrlHrs.value;
                            document.forms[0].WeekHours.value=ctrlHrs.getAttribute("Amount");
                        }
                        else {
                            //document.forms[0].WeekHours.value=document.forms[0].WeekHours.value+'|'+ctrlHrs.value;
                            document.forms[0].WeekHours.value = document.forms[0].WeekHours.value + '|' + ctrlHrs.getAttribute("Amount");   //Aman Multi Currency End changes made in between and resolved the MITS 27435
                        }
                    }
                }
            }
            document.forms[0].Action.value = 'Save';
            window.opener.setDataChanged(true);  //Aman MITS 27891
           // window.document.forms['frmData'].submit();
        }
        else {
            window.close();
        }
    }
    else {
        window.close();
    }

    //window.close();

}
function ValueChange(objCtrl) {   
    if(objCtrl.value<0) {
        objCtrl.value=0.0;
    }

    IfChange='True';
    return true;
}

//Aman Multi Currency --Start
function CurrencyValueChange(objCtrl) {    
    AWW_MultiCurrencyToDecimal(objCtrl);  //Aman Multi Currency --Start
    if (objCtrl.getAttribute("Amount") < 0) {
        objCtrl.value = 0;
    }
    AWW_MultiCurrencyOnBlur(objCtrl);  //Aman Multi Currency --End
    IfChange = 'True';
    return true;
}
//Aman Multi Currency --End

//Start by Shivendu for AWW Calc

function sortNumber(a,b) {
    return a-b
}
//End by Shivendu for AWW Calc

//Add by igupta3 for mits:32200 Start
function RefreshAWWData() {
    if (document.getElementById("hdnCalcbutton") != null) {
        if (document.getElementById("hdnCalcbutton").value == "Ok") {
            if (window.document.forms[0].hdnCompData != null && window.opener.document.forms[0].comprate1 != null) {
                window.opener.document.forms[0].comprate1.value = window.document.forms[0].hdnCompData.value;

            }
            else if (window.document.forms[0].hdnCompData != null && window.opener.document.forms[0].comprate != null) {
                window.opener.document.forms[0].comprate.value = window.document.forms[0].hdnCompData.value;
            }

            if (window.document.forms[0].hdnAWWData != null && window.opener.document.forms[0].averagewage1 != null) {
                window.opener.document.forms[0].averagewage1.value = window.document.forms[0].hdnAWWData.value;
            }
            else if (window.document.forms[0].hdnAWWData != null && window.opener.document.forms[0].averagewage != null) {
                window.opener.document.forms[0].averagewage.value = window.document.forms[0].hdnAWWData.value;

            }
        }
    }

}
//Add by igupta3 for mits:32200 End

//Aman Multicurrency --Start
function AWW_MultiCurrencyOnBlur(objCtrl) {
    functionname = objCtrl.getAttributeNode("onblur").value;
    functionname = functionname.split('(')[0];
    window[functionname](objCtrl);
}

function AWW_MultiCurrencyToDecimal(objCtl) {
    var functionname = objCtl.getAttributeNode("onfocus").value;
    //remove 'format_' word
    functionname = functionname.replace("Format_", "");
    functionname = functionname.split('(')[0];
    window[functionname](objCtl);
}
//Aman Multicurrency --End
