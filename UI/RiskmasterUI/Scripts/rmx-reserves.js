/**
* "RMX.Reserves" Namespace definition.
*  BSB 03.05.2007
*  Integrating IC Reserve Worksheets - take the opportunity to refactor Reserves javascript logic.
*  Add scoping to Javascript from now on.
*/

//BSB 03.05.2007 
// This approach keeps "global" variable usage more clear and explicit in Javascript and 
// prevents naming collisions.

if (typeof RMX == 'undefined')
  RMX = {};
if (typeof (RMX.Reserves) == 'undefined')
  RMX.Reserves = {};

//////////////////////////////////////////////////////////////
//Everything in this file MUST be prefixed with "RMX.Reserves"
//////////////////////////////////////////////////////////////

RMX.Reserves.BackToClaim = function() {
    switch (document.getElementById('hdlob').value) {
        case "241": sRedirectString = "/RiskmasterUI/UI/FDM/claimgc.aspx?recordID=" + document.getElementById('claimid').value + "&SysCmd=0";
            break;
        case "242": sRedirectString = "/RiskmasterUI/UI/FDM/claimva.aspx?recordID=" + document.getElementById('claimid').value + "&SysCmd=0";
            break;
        case "243": sRedirectString = "/RiskmasterUI/UI/FDM/claimwc.aspx?recordID=" + document.getElementById('claimid').value + "&SysCmd=0";
            break;
        case "844": sRedirectString = "./RiskmasterUI/UI/FDM/claimdi.aspx?recordID=" + document.getElementById('claimid').value + "&SysCmd=0";
            break;

    }
    pleaseWait.Show();
    window.location.href = sRedirectString;
    //urlNavigate('fdm?SysFormName=claim&amp;SysFormId=' + document.all("claimid").value + '&amp;SysCmd=0');
    return false;
}

RMX.Reserves.ScheduleChecks = function () {
    // Geeta  17-Oct-06: Modified for fixing MITS no. 8207						
    var sFrozenFlag = document.getElementById('frozenflag').value; //document.all("frozenflag").value; Mozilla fixdocument.all("frozenflag").value;
    var sClaimNumber = document.getElementById("claimnumber").value;//mozilla fix
    var sClaimStatus = document.getElementById("claimstatus").value;
    var sViewOnly = document.getElementById("ScheduleCheckViewPermissionOnly").value;
    var hdFDHButton = document.getElementById("hdFDHButton").value;// pgupta93: RMA-4608

    if (sClaimStatus == 'C') {
        alert("This is a closed claim.  Scheduled Checks can not be created for closed claims.");
        return false;
    }
    if (sFrozenFlag == 'True') {
        alert("Claim No " + sClaimNumber + " has its payments frozen. No further payments may be made until it is unfrozen");
        return false;
    }
    else {
        var sExternalParam = "<SysExternalParam><ClaimId>" + document.getElementById("claimid").value + "</ClaimId><ClaimNumber>" + document.getElementById("claimnumber").value + "</ClaimNumber><ClaimantEid>" + document.getElementById("claimanteid").value + "</ClaimantEid><UnitID>" + document.getElementById("unitid").value + "</UnitID><UnitRowID>" + document.getElementById("unitrowid").value + "</UnitRowID><FromRsvListing>" + "1" + "</FromRsvListing></SysExternalParam>";
        //Changed Rakhi for MITS 13804-START:Display Autocheck Records when Automatic Checks have View permission only.
        //        if (sViewOnly == '1') {
        //            sRedirectString = "/RiskmasterUI/UI/FDM/autoclaimchecks.aspx?SysExternalParam=" + sExternalParam + "&SysCmd=1";
        //        }
        //        else {
        //Added Rakhi for MITS 18939 on similar lines of MITS 17678:If there is & in the Querystring passed from the Javascript, then we repace the &
        // by the Character pair of "^@" in javascript and while retrieving the value from Querystring, we replace
        // the Character pair of "^@" by &
        sExternalParam = replaceSubstring(sExternalParam, "&", "^@");
        
        // pgupta93: RMA-4608 START
        var sQueryString = "&FormName=" + hdFDHButton  + "&FromFinancial=Yes";
        //sRedirectString = "/RiskmasterUI/UI/FDM/autoclaimchecks.aspx?SysExternalParam=" + escape(sExternalParam);
        sRedirectString = "/RiskmasterUI/UI/FDM/autoclaimchecks.aspx?SysExternalParam=" + escape(sExternalParam) + sQueryString;
        // pgupta93: RMA-4608 END

        //        }
        //Changed Rakhi for MITS 13804-END:Display Autocheck Records when Automatic Checks have View permission only.
        pleaseWait.Show();
        window.location.href = sRedirectString;
        return false;
    }
}

RMX.Reserves.ScheduleChecksBOB = function () {
    // Geeta  17-Oct-06: Modified for fixing MITS no. 8207						
    var sFrozenFlag = document.getElementById('frozenflag').value; //document.all("frozenflag").value; Mozilla fixdocument.all("frozenflag").value;
    var sClaimNumber = document.getElementById("claimnumber").value; //mozilla fix
    var sClaimStatus = document.getElementById("claimstatus").value;
    var sViewOnly = document.getElementById("ScheduleCheckViewPermissionOnly").value;
    if (sClaimStatus == 'C') {
        alert("This is a closed claim.  Scheduled Checks can not be created for closed claims.");
        return false;
    }
    if (sFrozenFlag == 'True') {
        alert("Claim No " + sClaimNumber + " has its payments frozen. No further payments may be made until it is unfrozen");
        return false;
    }
    else {
        //Ankit : Start Policy System Interface
        var CovgSeqNum = "";
        if (window.document.forms[0].CovgSeqNum)
            CovgSeqNum = window.document.forms[0].CovgSeqNum.value;
        //Ankit : End

        //mits 33996
        var TransSeqNum = "";
        if (window.document.forms[0].TransSeqNum)
            TransSeqNum = window.document.forms[0].TransSeqNum;
        //mits 33996
        //Ankit Start : Worked on MITS - 34297
        var CoverageKey = "";
        if (window.document.forms[0].CoverageKey)
            CoverageKey = window.document.forms[0].CoverageKey;
        //Ankit End

        var sExternalParam = "<SysExternalParam><ClaimId>" + document.getElementById("claimid").value + "</ClaimId><ClaimNumber>" +
        document.getElementById("claimnumber").value + "</ClaimNumber><ClaimantEid>" + document.getElementById("claimanteid").value
        + "</ClaimantEid><PolicyID>" + document.getElementById("policyid").value + "</PolicyID><RcRowId>" + document.getElementById("rc_row_id").value
        + "</RcRowId><PolCvgId>" + document.getElementById("polcvgid").value + "</PolCvgId><ResTypeCode>" + document.getElementById("Reserve").value
        + "</ResTypeCode><FromRsvListing>" + "1" + "</FromRsvListing><CvgLossId>" + document.getElementById("cvglossid").value + "</CvgLossId><RsvStatusParent>" + document.getElementById("ResStatusParent").value + "</RsvStatusParent></SysExternalParam>";

        //var sQueryString = "&ClaimId=" + document.getElementById("claimid").value + "&ClaimantEid=" + document.getElementById("claimanteid").value
        //+ "&PolicyID=" + document.getElementById("policyid").value + "&RcRowId=" + document.getElementById("rc_row_id").value
        //+ "&PolCvgId=" + document.getElementById("polcvgid").value + "&ResTypeCode=" + document.getElementById("Reserve").value + "&FromRsvListing="
        //+ "1" + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&CovgSeqNum=" + CovgSeqNum + "&CvgLossId=" + document.getElementById("cvglossid").value + "&RsvStatusParent=" + document.getElementById("ResStatusParent").value + "&TransSeqNum=" + document.getElementById("TransSeqNum").value + "&CoverageKey=" + document.getElementById("CoverageKey").value;       //Ankit Start : Worked on MITS - 34297
        var sQueryString = "&ClaimId=" + document.getElementById("claimid").value + "&ClaimantEid=" + document.getElementById("claimanteid").value
       + "&PolicyID=" + document.getElementById("policyid").value + "&RcRowId=" + document.getElementById("rc_row_id").value
       + "&PolCvgId=" + document.getElementById("polcvgid").value + "&ResTypeCode=" + document.getElementById("Reserve").value + "&FromRsvListing="
       + "1" + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&CovgSeqNum=" + CovgSeqNum + "&CvgLossId=" + document.getElementById("cvglossid").value + "&RsvStatusParent=" + document.getElementById("ResStatusParent").value + "&TransSeqNum=" + document.getElementById("TransSeqNum").value + "&CoverageKey=" + document.getElementById("CoverageKey").value + "&ClaimantListClaimantEID=" + document.getElementById('ClaimantListClaimantEID').value  + "&FromFinancial=Yes";
        //Changed Rakhi for MITS 13804-START:Display Autocheck Records when Automatic Checks have View permission only.
        //        if (sViewOnly == '1') {
        //            sRedirectString = "/RiskmasterUI/UI/FDM/autoclaimchecks.aspx?SysExternalParam=" + sExternalParam + "&SysCmd=1";
        //        }
        //        else {
        //Added Rakhi for MITS 18939 on similar lines of MITS 17678:If there is & in the Querystring passed from the Javascript, then we repace the &
        // by the Character pair of "^@" in javascript and while retrieving the value from Querystring, we replace
        // the Character pair of "^@" by &
        sExternalParam = replaceSubstring(sExternalParam, "&", "^@");
        //sQueryString = replaceSubstring(sQueryString, "&", "^@");
        sRedirectString = "/RiskmasterUI/UI/FDM/autoclaimchecks.aspx?SysExternalParam=" + escape(sExternalParam) + escape(sQueryString);
        //        }
        //Changed Rakhi for MITS 13804-END:Display Autocheck Records when Automatic Checks have View permission only.
        pleaseWait.Show();
        window.location.href = sRedirectString;
        return false;
    }
}

RMX.Reserves.NavigateToFunds = function (IsCollection) {

    var sFrozenFlag = document.getElementById("frozenflag").value;
    var sClaimNumber = document.getElementById("claimnumber").value;
    var hdFDHButton = document.getElementById("hdFDHButton").value;// pgupta93: RMA-4608

    if (sFrozenFlag == 'True') {
        alert("Claim No " + sClaimNumber + " has its payments frozen. No further payments may be made until it is unfrozen");
        return false;
    }
    else {
        //var sExternalParam = "<SysExternalParam><ClaimId>" + document.all("claimid").value + "</ClaimId><ClaimNumber>" + escape(document.all("claimnumber").value) + "</ClaimNumber><ClaimantEid>" + document.all("claimanteid").value + "</ClaimantEid><IsCollection>" + IsCollection + "</IsCollection><UnitID>" + document.all("UnitID").value + "</UnitID></SysExternalParam>";
        var sExternalParam = "<SysExternalParam><ClaimId>" + document.getElementById("claimid").value + "</ClaimId><ClaimantEid>" + document.getElementById("claimanteid").value + "</ClaimantEid><IsCollection>" + IsCollection + "</IsCollection><UnitID>" + document.getElementById("unitid").value + "</UnitID><UnitRowID>" + document.getElementById("unitrowid").value + "</UnitRowID><FromRsvListing>" + "1" + "</FromRsvListing></SysExternalParam>";
        //pgupta93: RMA-4608 START
        var sQueryString = "&FormName=" + hdFDHButton + "&FromFinancial=Yes";
        //sRedirectString = "/RiskmasterUI/UI/FDM/funds.aspx?SysExternalParam=" + sExternalParam;
        sRedirectString = "/RiskmasterUI/UI/FDM/funds.aspx?SysExternalParam=" + sExternalParam + sQueryString;
        //pgupta93: RMA-4608 END

        pleaseWait.Show();

        window.location.href = sRedirectString;
        return false;
    }

}

RMX.Reserves.NavigateToFundsBOB = function (IsCollection) {
    var sFrozenFlag = document.getElementById("frozenflag").value;
    var sClaimNumber = document.getElementById("claimnumber").value;
    var vIsMded;
    var ctrlIsMDeductible = document.getElementById("txtIsDeductible");
    if (ctrlIsMDeductible != null) {
        vIsMded = ctrlIsMDeductible.value;
    }
    if (sFrozenFlag == 'True') {
        alert("Claim No " + sClaimNumber + " has its payments frozen. No further payments may be made until it is unfrozen");
        return false;
    }
    else {
        //var sExternalParam = "<SysExternalParam><ClaimId>" + document.all("claimid").value + "</ClaimId><ClaimNumber>" + escape(document.all("claimnumber").value) + "</ClaimNumber><ClaimantEid>" + document.all("claimanteid").value + "</ClaimantEid><IsCollection>" + IsCollection + "</IsCollection><UnitID>" + document.all("UnitID").value + "</UnitID></SysExternalParam>";

        //rupal:start, For first & Final Payment
        //var sExternalParam = "<SysExternalParam><ClaimId>" + document.getElementById("claimid").value + "</ClaimId><ClaimantEid>" + document.getElementById("claimanteid").value + "</ClaimantEid><IsCollection>" + IsCollection + "</IsCollection><PolicyID>" + document.getElementById("policyid").value + "</PolicyID><RcRowId>" + document.getElementById("rc_row_id").value + "</RcRowId><PolCvgId>" + document.getElementById("polcvgid").value + "</PolCvgId><ResTypeCode>" + document.getElementById("Reserve").value + "</ResTypeCode><FromRsvListing>" + "1" + "</FromRsvListing></SysExternalParam>";
        //var sExternalParam = "<SysExternalParam><ClaimId>" + document.getElementById("claimid").value + "</ClaimId><ClaimantEid>" + document.getElementById("claimanteid").value + "</ClaimantEid><IsCollection>" + IsCollection + "</IsCollection><PolicyID>" + document.getElementById("policyid").value + "</PolicyID><RcRowId>" + document.getElementById("rc_row_id").value + "</RcRowId><PolCvgId>" + document.getElementById("polcvgid").value + "</PolCvgId><ResTypeCode>" + document.getElementById("Reserve").value + "</ResTypeCode><FromRsvListing>" + "1" + "</FromRsvListing><IsFirstFinalQueryString>" + document.getElementById("IsFirstFinal").value + "</IsFirstFinalQueryString><IsFirstFinalReadOnlyQueryString>" + document.getElementById("IsFirstFinalReadOnly").value + "</IsFirstFinalReadOnlyQueryString></SysExternalParam>";
        var sExternalParam = "<SysExternalParam><ClaimId>" + document.getElementById("claimid").value + "</ClaimId><ClaimantEid>" + document.getElementById("claimanteid").value + "</ClaimantEid><IsCollection>" + IsCollection + "</IsCollection><PolicyID>" + document.getElementById("policyid").value + "</PolicyID><RcRowId>" + document.getElementById("rc_row_id").value + "</RcRowId><PolCvgId>" + document.getElementById("polcvgid").value + "</PolCvgId><ResTypeCode>" + document.getElementById("Reserve").value + "</ResTypeCode><FromRsvListing>" + "1" + "</FromRsvListing><IsFirstFinalQueryString>" + document.getElementById("IsFirstFinal").value + "</IsFirstFinalQueryString><IsFirstFinalReadOnlyQueryString>" + document.getElementById("IsFirstFinalReadOnly").value + "</IsFirstFinalReadOnlyQueryString><CovgSeqNum>" + document.getElementById("CovgSeqNum").value + "</CovgSeqNum><CvgLossId>" + document.getElementById("cvglossid").value + "</CvgLossId><RsvStatusParent>" + document.getElementById("ResStatusParent").value + "</RsvStatusParent><TransSeqNum>" + document.getElementById("TransSeqNum").value + "</TransSeqNum><CoverageKey>" + document.getElementById("CoverageKey").value + "</CoverageKey><IsMDed>"+vIsMded+"</IsMDed></SysExternalParam>";

        //var sQueryString = "&ClaimId=" + document.getElementById("claimid").value + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&IsCollection=" + IsCollection + "&PolicyID=" + document.getElementById("policyid").value + "&RcRowId=" + document.getElementById("rc_row_id").value + "&PolCvgId=" + document.getElementById("polcvgid").value + "&ResTypeCode=" + document.getElementById("Reserve").value + "&FromRsvListing=" + "1" + "&ClaimantEid=" + document.getElementById("claimanteid").value;
        //var sQueryString = "&ClaimId=" + document.getElementById("claimid").value + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&IsCollection=" + IsCollection + "&PolicyID=" + document.getElementById("policyid").value + "&RcRowId=" + document.getElementById("rc_row_id").value + "&PolCvgId=" + document.getElementById("polcvgid").value + "&ResTypeCode=" + document.getElementById("Reserve").value + "&FromRsvListing=" + "1" + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&IsFirstFinalQueryString=" + document.getElementById("IsFirstFinal").value + "&IsFirstFinalControlReradOnlyQueryString=" + document.getElementById("IsFirstFinalReadOnly").value;
        //var sQueryString = "&ClaimId=" + document.getElementById("claimid").value + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&IsCollection=" + IsCollection + "&PolicyID=" + document.getElementById("policyid").value + "&RcRowId=" + document.getElementById("rc_row_id").value + "&PolCvgId=" + document.getElementById("polcvgid").value + "&ResTypeCode=" + document.getElementById("Reserve").value + "&FromRsvListing=" + "1" + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&IsFirstFinalQueryString=" + document.getElementById("IsFirstFinal").value + "&IsFirstFinalControlReradOnlyQueryString=" + document.getElementById("IsFirstFinalReadOnly").value + "&CovgSeqNum=" + document.getElementById("CovgSeqNum").value + "&CvgLossId=" + document.getElementById("cvglossid").value + "&RsvStatusParent=" + document.getElementById("ResStatusParent").value + "&TransSeqNum=" + document.getElementById("TransSeqNum").value + "&CoverageKey=" + document.getElementById("CoverageKey").value+"&IsMDed="+vIsMded;      //Ankit Start : Worked on MITS - 34297
        var sQueryString = "&ClaimId=" + document.getElementById("claimid").value + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&IsCollection=" + IsCollection + "&PolicyID=" + document.getElementById("policyid").value + "&RcRowId=" + document.getElementById("rc_row_id").value + "&PolCvgId=" + document.getElementById("polcvgid").value + "&ResTypeCode=" + document.getElementById("Reserve").value + "&FromRsvListing=" + "1" + "&ClaimantEid=" + document.getElementById("claimanteid").value + "&IsFirstFinalQueryString=" + document.getElementById("IsFirstFinal").value + "&IsFirstFinalControlReradOnlyQueryString=" + document.getElementById("IsFirstFinalReadOnly").value + "&CovgSeqNum=" + document.getElementById("CovgSeqNum").value + "&CvgLossId=" + document.getElementById("cvglossid").value + "&RsvStatusParent=" + document.getElementById("ResStatusParent").value + "&TransSeqNum=" + document.getElementById("TransSeqNum").value + "&CoverageKey=" + document.getElementById("CoverageKey").value + "&IsMDed=" + vIsMded + "&ClaimantListClaimantEID=" + document.getElementById('ClaimantListClaimantEID').value + "&FromFinancial=Yes";     
        //rupal:end, For First & Final Payment
        sRedirectString = "/RiskmasterUI/UI/FDM/funds.aspx?SysExternalParam=" + sExternalParam + sQueryString;
        //sRedirectString = "/RiskmasterUI/UI/FDM/funds.aspx?SysExternalParam=" + sQueryString;
        pleaseWait.Show();

        window.location.href = sRedirectString;
        return false;
    }

}

RMX.Reserves.DetailTrack = function()
{
	alert('Line of Business Parameters set to Claimant Level, not claim');
	return false;
}

// Geeta  17-Oct-06: Added for fixing MITS no. 8206 and 8207
RMX.Reserves.CheckForFrozenFlag = function () {
    var sFrozenFlag = document.getElementById('frozenflag').value; //document.all("frozenflag").value; Mozilla fix
    if (sFrozenFlag == 'True') {
        alert("This claim has its payments frozen. No further payments may be made until it is unfrozen.");
        return false;
    }
    else {
        return true;
    }
}

//bpaskova 13 Jan 2015: added for fixing JIRA RMA-6270
RMX.Reserves.CheckForOnHoldFlag = function (iIndex, sGridName, iPos) {
    var StatusObj = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + sGridName + "_GridView1_RsvStatusParent_" + (iPos)); // aaggarwal29: added null check since iPOs=-1 breaks the code
    if (StatusObj != null && StatusObj.innerHTML == 'H') {
        alert(parent.CommonValidations.ValidationPaymentagainstReserveofHoldStatus);
        return false;
    }
    else {
        return true;
    }
}
//rkotak: rma 11468 starts, this function no more needs index and grid name and its positions, when a reserve row is selected, the hidden values of selected grid row are populated in hidden controls on the page
RMX.Reserves.CheckForOnHoldFlagNew = function () {
    var Status = document.getElementById("ResStatusParent").value;//rkotak:rma 11468     
    if (Status != null && Status == 'H') {
        alert(parent.CommonValidations.ValidationPaymentagainstReserveofHoldStatus);
        return false;
    }
    else {
        return true;
    }
}
RMX.Reserves.CheckForDeductibleNew = function () {

    var Status = document.getElementById('DedTypeCode').value;//document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + sGridName + "_GridView1_DedTypeCode_" + (iPos));
    var ctrltxtNotDetDedVal = document.getElementById("txtNotDetDedVal");
    if (Status != null && Status == ctrltxtNotDetDedVal.value) {
        alert(parent.CommonValidations.NotDetDedValdTran);
        return false;
    }
    else {
        return true;
    }
}
//rkotak: rma 11468 ends

RMX.Reserves.CheckForDeductible = function (iIndex, sGridName, iPos) {
    
    var StatusObj = document.getElementById("TabContainer1_TabPanel" + iIndex + "_" + sGridName + "_GridView1_DedTypeCode_" + (iPos));
    var ctrltxtNotDetDedVal = document.getElementById("txtNotDetDedVal");
    if (StatusObj != null && StatusObj.innerHTML == ctrltxtNotDetDedVal.value) {

        alert(parent.CommonValidations.NotDetDedValdTran);
        return false;
    }
    else {
        return true;
    }
}

RMX.Reserves.GetDate = function()
{
	var dt = new Date();
	var sMonth = dt.getMonth()+1;
	document.getElementById("ProcDate").value = sMonth + "/" + dt.getDate() + "/" + dt.getYear();
}

RMX.Reserves.GetValueFromDropDown = function(value)
{
	document.getElementById("Reason").value = value;
}
					
//Function for client side validation
RMX.Reserves.ValidateForm = function()
{
	var sReserveAmount; //pmahli MITS 11038 12/25/2007 
	if(document.forms[0].ProcDate.value=="")
	{
		alert("Enter a valid Date.");
		document.forms[0].ProcDatebtn.focus();
		return false;
	}
	if(document.forms[0].txtAmount.value=="")
	{
		alert("Enter Reserve Amount.");
		document.forms[0].txtAmount.focus();
		return false;
	}
    //MITS:34082 START
	if (document.forms[0].ReservecurrencytypetextModify != null) { //JIRA RMA-1166 FDM Issues
	if (document.forms[0].ReservecurrencytypetextModify != undefined) {
	    if (document.forms[0].ReservecurrencytypetextModify.value == "") {
	        alert("Enter a Currency Type.");
	        document.forms[0].ReservecurrencytypetextModify.focus();
	        return false;
	        }
	    }
	}
    //MITS:34082 END
	//MITS 9515: Rmx forces reserves to be greater than zero..It should only give a warning
	if(document.forms[0].PrevResModifyzero != null)
	{
		//pmahli MITS 11038 12/25/2007 
            sReserveAmount = (document.forms[0].txtAmount.value).replace(/[,\$]/g ,"");
			sReserveAmount = parseFloat(sReserveAmount);
		if(sReserveAmount<=0 && document.forms[0].PrevResModifyzero.value=="True")
	    {
		    if(!confirm("The Reserve amount should be greater than $0.00. Do you really want to modify this reserve to $0.00?"))
		        return false;
	    }
	}
	return true;
}

RMX.Reserves.PrintWindowOpen = function () {
    if (document.forms[0].cvglossid == undefined) {
        window.open('PrintReserveHistory.aspx?ClaimId=' + document.forms[0].claimid.value + '&Policyid=' + document.forms[0].policyid.value + '&Polcvgid=' + document.forms[0].polcvgid.value + '&ClaimantId=' + document.forms[0].claimanteid.value + '&ClaimNumber=' + escape(document.forms[0].claimnumber.value) + '&UnitID=' + document.forms[0].unitid.value + '&Reserve=' + document.forms[0].Reserve.value + '&Lob=' + document.forms[0].lob.value + '&CvgLossId=', '', "width=600,height=400,top=" + (screen.availHeight - 400) / 2 + ",left=" + (screen.availWidth - 500) / 2 + ",resizable=yes");
    }
    else {
        // averma62 - MITS 28261 window.open('PrintReserveHistory.aspx?ClaimId=' + document.forms[0].claimid.value + '&ClaimantId=' + document.forms[0].claimanteid.value + '&ClaimNumber=' + escape(document.forms[0].claimnumber.value) + '&UnitID=' + document.forms[0].unitid.value + '&Reserve=' + document.forms[0].Reserve.value + '&Lob=' + document.forms[0].lob.value, '', "width=600,height=400,top=" + (screen.availHeight - 400) / 2 + ",left=" + (screen.availWidth - 500) / 2 + ",resizable=yes");
        window.open('PrintReserveHistory.aspx?ClaimId=' + document.forms[0].claimid.value + '&Policyid=' + document.forms[0].policyid.value + '&Polcvgid=' + document.forms[0].polcvgid.value + '&ClaimantId=' + document.forms[0].claimanteid.value + '&ClaimNumber=' + escape(document.forms[0].claimnumber.value) + '&UnitID=' + document.forms[0].unitid.value + '&Reserve=' + document.forms[0].Reserve.value + '&Lob=' + document.forms[0].lob.value + '&CvgLossId=' + document.forms[0].cvglossid.value, '', "width=600,height=400,top=" + (screen.availHeight - 400) / 2 + ",left=" + (screen.availWidth - 500) / 2 + ",resizable=yes"); // averma62 - MITS 28261
        //+ '&caption=' + escape(document.forms[0].Caption.value)
    }
    return false;
}

RMX.Reserves.Print = function()
{
    document.getElementById('lblClaim').innerText = window.opener.document.forms[0].Caption.value;
	self.print();

	return false;
}

RMX.Reserves.PageLoaded = function () {

    //BSB Add hook for custom reserve page loaded logic.
    if (typeof (RMX.Custom) != 'undefined')

        if (typeof (RMX.Custom.Reserves) != 'undefined')
            if (typeof (RMX.Custom.Reserves.PageLoaded) != 'undefined')
                RMX.Custom.Reserves.PageLoaded();

    if (RMX.Reserves.ApplyDefaultDateFlag == true)
        //RMX.Reserves.GetDate(); //Loads known date control id with today's date.	
    if (window.frameElement != null) {
        var parentType = parent.MDIGetScreenInfo(window.frameElement.id, "PARENTSCREENTYPE").toLowerCase();
        var btnLoadClaim = document.getElementById('btnBack');
        if ((parentType == "root") && ((document.forms[0].claimid.value != '') && (document.forms[0].claimid.value != '0'))) {
            if (btnLoadClaim != null) {
                btnLoadClaim.style.display = "";
            }
        }
        else {
            if (btnLoadClaim != null) {
                btnLoadClaim.style.display = "none";
            }
        }
    }
}

// Start Naresh 12/12/2006 MITS Id - 8532
RMX.Reserves.AssignValue = function() {
    //MITS 10367
    var objSubTitleValue = document.getElementById('subtitleValue');
    if (objSubTitleValue != null) {
        document.getElementById("subtitle").value = objSubTitleValue.value;
    }
    return true;

}
