﻿var script = document.createElement('script');
script.src = "../../Scripts/m-x.js";
document.getElementsByTagName('script')[0].parentNode.appendChild(script);

script = document.createElement('script');
script.src = "../../Scripts/DcomboBox.js";
document.getElementsByTagName('script')[0].parentNode.appendChild(script);

function ValForm() {

    if (document.forms[0].AssignedUser_lstUsers != null && document.forms[0].AssignedUser_lstUsers.options.length < 1) {
        //alert("Please select the user to assign diary to.");
        alert(CreateDiaryValidations.ValidAssignDiaryToCheck);
        return false;
    }

    if (document.forms[0].EntryName != null && (replace(document.forms[0].EntryName.value, " ", "") == "")) {
        //alert("Please enter the Task Name.");
        alert(CreateDiaryValidations.ValidTaskNameCheck);
        document.forms[0].EntryName.focus();
        return false;
    }

    if (document.forms[0].CompleteDate_date != null && (replace(document.forms[0].CompleteDate_date.value, " ", "") == "")) {
        //ravi commented to match the functionality with rmworld-dont know whether its as it should be --4/22/03
        //alert("Please enter the due date.");
        //document.forms[0].txtDueDate.focus();
        //return false;
    }

    if (document.forms[0].IssueEvery != null && document.forms[0].IssueEvery.checked) {
        if (document.forms[0].txt_IssueEvery_rd != null) {
            if (isNaN(parseInt(document.forms[0].txt_IssueEvery_rd.value))) {
                //MITS 22016: Raman Bhatia
                //alert("Please enter a number into the Issue every n days until the completion date field.");
                //alert("Please fill in a valid value in the days until completion date field to proceed !");
                alert(CreateDiaryValidations.ValidDaysToComplete);
                document.forms[0].txt_IssueEvery_rd.focus();
                return false;
            }
        }
    }
    //Umesh fix for MITS 7887 
    if (document.forms[0].IssueEvery != null && document.forms[0].IssueEvery.checked) {
        if (document.forms[0].txt_IssueEvery_rd != null) {
            if (parseInt(document.forms[0].txt_IssueEvery_rd.value) <= 0) {
                //alert("Please enter positive value.");
                alert(CreateDiaryValidations.ValidPositiveValue);
                document.forms[0].txt_IssueEvery_rd.focus();
                return false;
            }
        }
    }
    //end

    m_bConfirmSave = false;
}

function CancelDiarySave()
{
    if (m_DataChanged) {
        m_bConfirmSave = false;
    }
    return true;
}

function replace(sSource, sSearchFor, sReplaceWith) {
    var arr = new Array();
    arr = sSource.split(sSearchFor);
    return arr.join(sReplaceWith);
}

function CreateDiary_AddActivity()
{
    if (m_codeWindow != null)
        m_codeWindow.close();

    document.onCodeClose = onCodeClose;
    document.OnAddActivity = OnAddActivity;
    m_codeWindow = window.open('/RiskmasterUI/UI/Diaries/DiaryActivity.aspx', 'codeWnd',
						'width=400,height=200' + ',top=' + (screen.availHeight - 200) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=yes,scrollbars=yes');
    return false;
}

function OnAddActivity(sId, sText) {

    if (m_codeWindow != null)
        m_codeWindow.close();
    m_codeWindow = null;

    var objCtrl = document.forms[0].WrkAct;
    var bAdd = true;
    for (var i = 0; i < objCtrl.length; i++) {
        //MGaba2:MITS 20190:multiple free text were not getting added to work activity
        //In case of free text sId is ""  and objCtrl.options[i].value is also "".Hence previous condition was resulting true
        //in case multiple freetext field are added 
        //if(objCtrl.options[i].value == sId && sId != "0")	            
        if ((sId == "" && trim(objCtrl.options[i].innerText.toLowerCase() || objCtrl.options[i].textContent.toLowerCase()) == trim(sText).toLowerCase()) || (sId != "" && objCtrl.options[i].value == sId && sId != "0")) {
            bAdd = false;
        }
    }
    if (bAdd) {
        var objOption = new Option(sText, sId, false, false);
        objCtrl.options[objCtrl.length] = objOption;
        objCtrl = null;
        //Parijat: Mits 9390 an extension to it .
        //Actually implemeting Mits 8623 where it was not completely done, Now adding '|' in place of ','.
        if (document.forms[0].txtActivities.value != "")
            document.forms[0].txtActivities.value = document.forms[0].txtActivities.value + "|";
        document.forms[0].txtActivities.value = document.forms[0].txtActivities.value + sId + "|" + sText;
        document.forms[0].actstring.value = document.forms[0].txtActivities.value;
    }
}

function CreateDiary_DelActivity() {
    var objCtrl = document.forms[0].WrkAct;
    if (objCtrl.selectedIndex < 0)
        return false;

    var bRepeat = true;
    while (bRepeat) {
        bRepeat = false;
        for (var i = 0; i < objCtrl.length; i++) {
            // remove selected elements
            if (objCtrl.options[i].selected) {
                objCtrl.options[i] = null;
                bRepeat = true;
                break;
            }
        }
    }
    // Now create ids list
    var sId = "";
    for (var i = 0; i < objCtrl.length; i++) {
        //Parijat: MIts 9390 extension to this .
        //Actually implemeting Mits 8623 where it was not completely done, Now adding '|' in place of ','.
        if (sId != "")
            sId = sId + "|";
        sId = sId + objCtrl.options[i].value + "|" + replace(objCtrl.options[i].text, "|", "|");
    }
    objCtrl = null;
    document.forms[0].txtActivities.value = sId;
    document.forms[0].actstring.value = document.forms[0].txtActivities.value;
    return true;
}

var _isSelect = false;
function setDefaults() {
    if (document.forms[0].IssueOnComp != null)
        document.forms[0].IssueOnComp.checked = true;
    if (document.forms[0].EntryNameDcboBox != null) {
        // MGaba2: MITS 14739
        _offset = 0;
        //_offset = -5.5;
        ComboInit('EntryName');
        arrAllComboBoxes.push('EntryName');
    }
}

function check() {

    if (document.forms[0].IssueOnComp != null && document.forms[0].IssueEvery != null && document.forms[0].txt_IssueEvery_rd != null) {
        if (document.forms[0].IssueOnComp.checked) {
            document.forms[0].txt_IssueEvery_rd.disabled = true;
            document.forms[0].txt_IssueEvery_rd.value = "";
            document.forms[0].IssueEvery.value = "false";
            document.forms[0].IssueOnComp.value = "true";
        }
        else if (document.forms[0].IssueEvery.checked) {
            document.forms[0].txt_IssueEvery_rd.disabled = false;
            document.forms[0].IssueEvery.value = "true";
            document.forms[0].IssueOnComp.value = "false";
        }
    }
}

function Validate(btnName) {
    if (btnName == 'btnEdit' || btnName == 'btnRoll' || btnName == 'btnRoute' || btnName == 'btnComplete') {
        var otherDiariesEditPermission = document.getElementById("hdnOtherDiariesEditPerm");
        if (otherDiariesEditPermission && otherDiariesEditPermission.value == "1") {
            alert(DiaryDetailsValidations.OtherDiariesEditPermission);
            return false;
        }
    }
    if (document.getElementById('txtNotRouteableFlag') != null)
        var RouteableFlag = document.getElementById('txtNotRouteableFlag').value;
    if (document.getElementById('txtNotRollableFlag') != null)
        var RollableFlag = document.getElementById('txtNotRollableFlag').value;
    if (btnName == "btnRoute") {
        if (RouteableFlag == "-1") {
            alert(DiaryDetailsValidations.NotRoutablePerm);
            return false;
        }
    }
    else if (btnName == "btnRoll") {

        if (RollableFlag == "-1") {
            alert(DiaryDetailsValidations.NotRollablePerm);
            return false;
        }
    }
    return true;
}

function OpenDayViewCalendar()
{
    objCtrl = document.getElementById('AssignedUser_lstUsers');

    var varhdnUserStr = document.getElementById('hdnDiaryLstUsers');
    var varhdnGroupStr = document.getElementById('hdnDiaryLstGroups');
    var strGroup = varhdnGroupStr.value.replace(/\|/g, ",").split(",");
    var strUser = varhdnUserStr.value.replace(/\|/g, ",").split(",");
    var UID = "";
    if ($("#AssignedUser_lstUsers :selected").length > 1) {
        alert('Please select single user/group to view Due Dairies');
        return false;
    }

    if (objCtrl != null) {
        if ($("#AssignedUser_lstUsers :selected").length == 0 && objCtrl.length > 1) {
            alert('Please select atleast one user/group to view Due Dairies');
            return false;
        }

        if (objCtrl.length == 1) {
            if (varhdnGroupStr) {
                for (var j = 0; j < strGroup.length; j++) {
                    if ((objCtrl.options[0].value == strGroup[j])) {
                        UID = objCtrl.options[0].value;
                        break;
                    }
                }
            }
            if (varhdnUserStr && UID == '') {
                for (var k = 0; k < strUser.length; k++) {
                    if ((objCtrl.options[0].innerHTML == strUser[k])) {
                        UID = objCtrl.options[0].innerHTML;
                        break;
                    }
                }
            }
        }


        else if (objCtrl.length > 1) {
            //for (var i = 0; i < $("#AssignedUser_lstUsers :selected").length; i++) {
            if (varhdnGroupStr) {
                for (var j = 0; j < strGroup.length; j++) {
                    if ($("#AssignedUser_lstUsers :selected").val() == strGroup[j]) {
                        UID = $("#AssignedUser_lstUsers :selected").val();
                        break;
                    }
                }
            }
            if (varhdnUserStr && UID == '') {
                for (var k = 0; k < strUser.length; k++) {
                    if ($("#AssignedUser_lstUsers :selected").text() == strUser[k]) {
                        UID = $("#AssignedUser_lstUsers :selected").text();
                        break;
                    }
                }
            }
        }
        //}
    }
    else
    {
        if(strGroup[0]!="")
            UID = strGroup[0];
        if (strUser[0] != "" && UID == '')
            UID = strUser[0];
    }

    var selecteddate = $('#CompleteDate_date');
    window.open('/RiskmasterUI/UI/Diaries/DiaryCalendar/DiaryCalendar.aspx?id=' + UID + "&parentpage=creatediary&dt=" + selecteddate.val(), 'DiaryCalendar',
                        'width=800,height=400' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 700) / 2 + ',resizable=yes,scrollbars=yes');
    return false;
}