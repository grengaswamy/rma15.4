﻿
function Validatefields() {
    var test;
    var sfileName = document.forms[0].fulFileToProcess.value;

    if (document.forms[0].hTabName.value == "Export") {
        if (document.forms[0].txtExportOptionSetName.value == "") {

            alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
            document.forms[0].txtExportOptionSetName.focus();
            return false;
        }
        if (document.forms[0].ddlBankFormat.value == "") {
            alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
            document.forms[0].ddlBankFormat.focus();
            return false;
        }

        if (document.forms[0].ckbManually.checked == true) {
            if (document.forms[0].fromcheckdate.value == "" || document.forms[0].tocheckdate.value == "") {
                alert("If Manually Change Date Range is checked then a To Date and From Date must be selected.");
                document.forms[0].ddlBankAccount.focus();
                return false;
            }
        }
            //Vsoni5 : MITS 24953 : From Date field is made as mandatory field on DA PPY Criteria screen.
        else {
            if (document.forms[0].fromcheckdate.value == "") {
                document.forms[0].fromcheckdate.focus();
                alert("From Date must be selected.");
                return false;
            }
        }


        if (document.forms[0].txtTargetFileName.value == "") {
            alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
            document.forms[0].txtTargetFileName.focus();
            return false;
        }

        if (document.forms[0].ddlBankFormat.value == "" || document.forms[0].ddlBankFormat.value == "Corporate Claim" || document.forms[0].ddlBankFormat.value == "Wachovia" || document.forms[0].ddlBankFormat.value == "Northern Trust") {
            if (document.forms[0].ddlBankAccount.value == "0") {
                alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
                document.forms[0].ddlBankAccount.focus();
                return false;

            }
        }
        else {
            if (document.forms[0].ddlBankAccount.value == "0" && document.forms[0].lsbAccountName.length == 0) {
                alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
                document.forms[0].ddlBankAccount.focus();
                return false;
            }

        }
        if (document.forms[0].fromcheckdate.value != "" && document.forms[0].tocheckdate.value != "") {
            if (getdbDate(document.forms[0].fromcheckdate.value) > getdbDate(document.forms[0].tocheckdate.value)) {
                alert("The From Date must be less then the To Date");
                document.forms[0].fromcheckdate.focus();
                return false;
            }

        }
    }
    else {
        if (document.forms[0].txtImportOptionsetName.value == "") {

            alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
            document.forms[0].txtImportOptionsetName.focus();
            return false;
        }
        if (document.forms[0].ddlFileFormat.value == "") {
            alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
            document.forms[0].ddlFileFormat.focus();
            return false;
        }

        if (document.forms[0].fulFileToProcess.value == "") {
            alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
            document.forms[0].fulFileToProcess.focus();
            return false;
        }



    }
    document.forms[0].hdnaction.value = 'SaveOptionset';
    document.forms[0].submit();
    return true;
}




