// WPA Functions
// Author: Denis Basaric, 11,12/2000
var m_codeWindow=null;
var m_sFieldName="";
var ns, ie, ieversion, test;
var browserName = navigator.appName;                   // detect browser
var browserVersion = navigator.appVersion;
if (browserName == "Netscape")
{
	ie=0;
	ns=1;
}
else		//Assume IE
{
	ieversion=browserVersion.substr(browserVersion.search("MSIE ")+5,1);
	ie=1;
	ns=0;
}

//Switched param from obj to string to support NS.

function OnSort(sName)
{
	//Check if last sort was ascending
	var PrevOrder = parseInt(document.forms[0].orderby.value);
	var NewOrder = 0;
	//Assume all Ascending
	switch(sName)
	{
	 case 'hdrDue':
		document.forms[0].orderby.value = new String(1);
		break;
	 case 'hdrAttached':
		document.forms[0].orderby.value = new String(2);
		break;
	 case 'hdrText':
		document.forms[0].orderby.value = new String(3);
		break;
	case 'hdrPriority':
		document.forms[0].orderby.value = new String(4);
		break;
	//Geeta 11/17/2006 : Added to fix mits number 8139 for Work Activity
	case 'hdrWork':
		document.forms[0].orderby.value = new String(9);
		break;
	default:
		alert("Name [" + sName + "] type of not found.");
	}
	//Iff last sort was ascending, then switch to descending by adding offset
	NewOrder = parseInt(document.forms[0].orderby.value);
	if (PrevOrder == NewOrder)
		document.forms[0].orderby.value = new String(4 + parseInt(document.forms[0].orderby.value));
	//alert(document.forms[0].orderby.value);
	
	if(document.forms[0].chkdatedue!=null) OnAllActiveClick("selecteddate");
	else 
	{
		document.forms[0].ouraction.value = "Refresh";
		document.forms[0].submit();
	}
	return true;
}
function Complete()
{
	var lDiaryId=getDiaryId();
	
	if(lDiaryId==0)
	{
		alert("Please select diary to complete.");
		return false;
	}
	document.forms[0].selDiary.value = lDiaryId;
	//self.setTimeout("window.location.href=\"completediary.asp?diaryid="+lDiaryId+"&sid=19031\"",10);
	
	return true;
}

function Create()
{
	self.setTimeout("window.location.href=\"creatediary.asp?sid=19030\"",10);
	return true;
}

function Route()
{
	var lDiaryId=getDiaryId();
	
	if(lDiaryId==0)
	{
		alert("Please select diary to route.");
		return false;
	}
	document.forms[0].selDiary.value = lDiaryId;
	//self.setTimeout("window.location.href=\"wparoute.asp?diaryid="+lDiaryId+"&sid=19034\"",10);
	
	return true;
}
function Roll()
{
	var lDiaryId=getDiaryId();
	
	if(lDiaryId==0)
	{
		alert("Please select diary to roll.");
		return false;
	}
	document.forms[0].selDiary.value = lDiaryId;
	//self.setTimeout("window.location.href=\"wparoll.asp?diaryid="+lDiaryId+"&sid=19035\"",10);
	
	return true;
}

function Void()
{
	var lDiaryId=getDiaryId();
	document.forms[0].selDiary.value = lDiaryId;
	
	//self.setTimeout("window.location.href=\"wpavoid.asp?diaryid="+lDiaryId+"&sid=19033\"",10);
	
	return true;
}

/*
function Peek()
{
	window.location.href="wpapeek.asp?sid=19037";
	return true;
}
*/
function getDiaryId()
{
	var lDiaryId=0;
	if(eval("document.forms[0].selDiary")==null)
		return false;
	
	var len=eval("document.forms[0].selDiary.length");
	if(!isNaN(len))
	{
		for(var i=0;i<document.forms[0].selDiary.length;i++)
		{
			if(document.forms[0].selDiary[i].checked)
			{
				lDiaryId=document.forms[0].selDiary[i].value;
				document.forms[0].entryid.value = lDiaryId;
				break;
			}
		}
	}
	else
		lDiaryId=document.forms[0].selDiary.value;
	return lDiaryId;
}

/*
function OnAllActiveClick()
{
	var sParams='';
	
	if(eval(document.forms[0].chkdatedue)!=null){
		if(!document.forms[0].chkdatedue.checked)
		{
			var objCtrl=eval("document.forms[0].txtdatedue");
			if(objCtrl!=null)
			{
				objCtrl.value=getdbDate(objCtrl.value);
				alert(objCtrl.value);
				//sUrl=sUrl+"&datedue="+escape(getdbDate(document.forms[0].txtdatedue.value));
			}
			else
			{
				sParams = sParams + "&txtdatedue=" + getdbDate((new Date()).toUTCString());
				alert(sParams);
				//sUrl=sUrl+"&datedue="+getdbDate((new Date()).toUTCString());
			}
		}
	}
	
	//document.forms[0].action = document.forms[0].action + sParams;
	//self.setTimeout("document.forms[0].submit()",10);
	//self.setTimeout("window.location.href=\""+sUrl+"\"",10);
	return true;
}
*/

function OnShowNotesClick()
{
    document.forms[0].ouraction.value="Refresh";       
    document.forms[0].submit();
}

function OnAllActiveClick(requireddate)
{    
	//if(document.forms[0].chkdatedue.checked)
	if(document.forms[0].activediarychecked.checked)//rem umesh
	{
		document.forms[0].ouraction.value="Refresh";
		document.forms[0].txtdatedue.value="";
		document.forms[0].reqactdiary.value="1";
		document.forms[0].submit();
	}
	else
	{
		document.forms[0].ouraction.value="Refresh";
		if(requireddate != "selecteddate")
			document.forms[0].txtdatedue.value="today";
			document.forms[0].reqactdiary.value="1";
		document.forms[0].submit();
	}
}


function getdbDate(sDate)
{
	if(sDate=="")
		return "";
	var d=new Date(sDate);
	var sYear=new String(d.getFullYear());
	var sMonth=new String(d.getMonth()+1);
	var sDay=String(d.getDate());
	if(sMonth.length==1)
		sMonth="0" + sMonth;
	if(sDay.length==1)
		sDay="0" + sDay;
	
	return sYear+sMonth+sDay;
}

//function dateLostFocus(sCtrlName)
//{
//	var sDateSeparator;
//	var iDayPos=0, iMonthPos=0;
//	var d=new Date(1999,11,22);
//	var s=d.toLocaleString();
//	var sRet="";
//	//var objFormElem=eval('document.forms[0].'+sCtrlName);
//	var objFormElem = eval( "document.getElementById('" + sCtrlName + "')" );
//	var sDate=new String(objFormElem.value);
//	var iMonth=0, iDay=0, iYear=0;
//	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
//	if(sDate=="")
//		return "";
//	if(sDate.indexOf('/') == -1)
//	{
//		sDate = sDate.substr(0 , 2) + '/' + sDate.substr(2 , 2) + '/' + sDate.substr(4 , 4);
//	}
//	iDayPos=s.indexOf("22");
//	iMonthPos=s.indexOf("11");
//	sDateSeparator="/";
//	var sArr=sDate.split(sDateSeparator);
//	if(sArr.length==3)
//	{
//		sArr[0]=new String(parseInt(sArr[0],10));
//		sArr[1]=new String(parseInt(sArr[1],10));
//		sArr[2]=new String(parseInt(sArr[2],10));
		
//		//Convert potential 2-digit year to 4-digit year
//		sArr[2] = Get4DigitYear(sArr[2]);		
		
//		// Classic leap year calculation
//		if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
//			monthDays[1] = 29;
//		if(iDayPos<iMonthPos)
//		{
//			// Date should be as dd/mm/yyyy
//			if(parseInt(sArr[1],10)<1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
//				objFormElem.value="";
//		}
//		else
//		{
//			// Date is something like mm/dd/yyyy
//			if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
//				objFormElem.value="";
//		}
//		// Check the year
//		if((sArr[2].length!=4 && sArr[2].length<1))
//			objFormElem.value="";
//		// If date has been accepted
//		if(objFormElem.value!="")
//		{
//			// Format the date
//			if(sArr[0].length==1)
//				sArr[0]="0" + sArr[0];
//			if(sArr[1].length==1)
//				sArr[1]="0" + sArr[1];
//			if(sArr[2].length==2)
//				sArr[2]="20"+sArr[2];
//			else if(sArr[2].length==1)
//				sArr[2]="200"+sArr[2];
//			if(iDayPos<iMonthPos)
//				objFormElem.value=formatDate1(sArr[2] + sArr[1] + sArr[0]);
//			else
//				objFormElem.value=formatDate1(sArr[2] + sArr[0] + sArr[1]);
//		}
//	}
//	else
//		objFormElem.value="";
//	return true;
//}

//Form.js has a function with same name. Sometimes both form.js and wpamessage.js are
//linked in the same page. Create this function to avoid confusion.
//function dateLostFocus2(sCtrlName)
//{
//	var sDateSeparator;
//	var iDayPos=0, iMonthPos=0;
//	var d=new Date(1999,11,22);
//	var s=d.toLocaleString();
//	var sRet="";
//	//var objFormElem=eval('document.forms[0].'+sCtrlName);
//	var objFormElem = eval( "document.getElementById('" + sCtrlName + "')" );
//	var sDate=new String(objFormElem.value);
//	var iMonth=0, iDay=0, iYear=0;
//	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
//	if(sDate=="")
//	    return "";
//	// aaggarwal29 MITS 26611 start
//	// The code right now checks if "/" as a seperator is present or not.
//	//If the seperator is not present, it does not does any further processing 
//    // and clears the date value entered. This "if" clause will check and add 
//    //the seperator if not entered by user and does processing. 
//	if (sDate.indexOf('/') == -1) {
//	    sDate = sDate.substr(0, 2) + '/' + sDate.substr(2, 2) + '/' + sDate.substr(4, 4);
//	}
//    //aaggarwal29 MITS 26611 end
//	iDayPos=s.indexOf("22");
//	iMonthPos=s.indexOf("11");
//	sDateSeparator = "/";
//	var sArr=sDate.split(sDateSeparator);
//	if(sArr.length==3)
//	{
//		sArr[0]=new String(parseInt(sArr[0],10));
//		sArr[1]=new String(parseInt(sArr[1],10));
//		sArr[2]=new String(parseInt(sArr[2],10));
		
//		//Convert potential 2-digit year to 4-digit year
//		sArr[2] = Get4DigitYear(sArr[2]);
		
//		// Classic leap year calculation
//		if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
//			monthDays[1] = 29;
//		if(iDayPos<iMonthPos)
//		{
//			// Date should be as dd/mm/yyyy
//			if(parseInt(sArr[1],10)<1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
//				objFormElem.value="";
//		}
//		else
//		{
//			// Date is something like mm/dd/yyyy
//			if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
//				objFormElem.value="";
//		}
//		// Check the year
//		if((sArr[2].length!=4 && sArr[2].length<1))
//			objFormElem.value="";
//		// If date has been accepted
//		if(objFormElem.value!="")
//		{
//			// Format the date
//			if(sArr[0].length==1)
//				sArr[0]="0" + sArr[0];
//			if(sArr[1].length==1)
//				sArr[1]="0" + sArr[1];
//			if(sArr[2].length==2)
//				sArr[2]="20"+sArr[2];
//			else if(sArr[2].length==1)
//				sArr[2]="200"+sArr[2];
//			if(iDayPos<iMonthPos)
//				objFormElem.value=formatDate1(sArr[2] + sArr[1] + sArr[0]);
//			else
//				objFormElem.value=formatDate1(sArr[2] + sArr[0] + sArr[1]);
//		}
//	}
//	else
//		objFormElem.value="";
//	return true;
//}

//Convert 2-digit year to 4-digit year
function Get4DigitYear(sInputYear) {
    var sGuessedYear = sInputYear;
    var dCurrentDate = new Date();
    var iCurrentYear = dCurrentDate.getFullYear();
    var sCurrentYear = new String(iCurrentYear);

    //01/05/2010 Raman: Logic below failed for 2010 onwards
    //converting inputyear to 2 characters

    if (sInputYear.length == 1) {
        sInputYear = "0" + sInputYear;
    }

    //If the input year is not 4-digit year, get missing digit form current year
    //	if (sInputYear.length < 4)  Commented by csingh7 : MITS 14181
    if (sInputYear.length < 3)  // Added by csingh7 : MITS 14181
    {
        sGuessedYear = sCurrentYear.substr(0, 4 - sInputYear.length) + sInputYear;

        //If the guessed year is 20 years in the future, assume it's in the last centure
        var iGuessedYear = parseInt(sGuessedYear);
        if (iGuessedYear - iCurrentYear > 20) {
            iGuessedYear = iGuessedYear - 100;
            sGuessedYear = new String(iGuessedYear);
        }
    }

    return sGuessedYear;
}

function selectDate(sFieldName)
{
	//alert("selectDate");
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	
	m_codeWindow=window.open('csc-Theme/riskmaster/common/html/calendar.html','codeWnd',
		'width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
	//alert("calendaropen");
	return false;
}

function dateSelected(sDay, sMonth, sYear)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
//	objCtrl=eval('document.forms[0].'+m_sFieldName);
    objCtrl=document.getElementById(m_sFieldName);
	if(objCtrl!=null)
	{
		sDay=new String(sDay);
		if(sDay.length==1)
			sDay="0"+sDay;
		sMonth=new String(sMonth);
		if(sMonth.length==1)
			sMonth="0"+sMonth;
		sYear=new String(sYear);
			
		objCtrl.value=formatDate1(sYear+sMonth+sDay);
	}
	m_sFieldName="";
	m_codeWindow=null;
	return true;
}
function onCodeClose()
{
	m_codeWindow=null;
	return true;
}

function formatDate1(sParamDate)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}

// Hides code popup window
function onWindowFocus()
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	return true;
}

//tkr 6/22/07 determine if Jetspeed being used
//if using jetspeed, attempt to reference parent.parent.parent throws Permission Denied error
//if not using jetspeed can add any number of parents and never get error
isJetspeedUsed = function(win) {
    var b = false;
    try
    {
        var s = win.parent.parent.parent.location.href;
    }
    catch(ex)
    {
        b = true;
    }
    return b;
}

 // Edits the "show/hide" button on choice as appropriate (only used for peek).
 function OnLoad()
 {
    //tkr if using Jetspeed shrink diary window a little so buttons are not off screen.
    //Changed by Gagan for MITS 10650 : start
//    var heightMultiplier = 0.68;
//    if(isJetspeedUsed(window))
//        heightMultiplier = 0.50;
//    var heightMultiplier = 0.63;
//    if(isJetspeedUsed(window))
//        heightMultiplier = 0.45;
//    //Changed by Gagan for MITS 10650 : end
//        
//	if (ie)
//	{
//		if ((eval("document.all.divForms")!=null) && (ieversion>=6))
//			divForms.style.height=window.frames.frameElement.Height*heightMultiplier;			
//	}
//	else
//	{
//		var o_divforms;
//		o_divforms=document.getElementById("divForms");
//		if (o_divforms!=null)
//		{
//			o_divforms.style.height=window.frames.innerHeight*heightMultiplier;
//			o_divforms.style.width=window.frames.innerWidth*0.995;
//		}
//	}
//	
	//alert(document.forms[0].showhome.value);			
	var frm = document.forms[0];
	if (document.forms[0].showhome.value==1)
	{
		if(eval(frm.cmdPeek)!=null) {
			frm.cmdPeek.onclick=onHome;
			frm.cmdPeek.value="  Home  ";
		}
		if(eval(frm.cmdVoid)!=null){
			frm.cmdVoid.onclick=NotAvailViaPeek;
		}
		/*
		if(eval(frm.cmdDiaryList)!=null){
			frm.cmdDiaryList.onclick = NotAvailViaPeek;
		}
					
		if(eval(frm.cmdDiaryCalendar)!=null){
			frm.cmdDiaryCalendar.onclick = NotAvailViaPeek;
		}
		*/
	}	

 /*Set focus to the first field
	 var i;
	 for (i=0;i<document.forms[0].length;i++)			
		{	 				
 		   if((document.forms[0][i].type=="text") || (document.forms[0][i].type=="select-one")|| (document.forms[0][i].type=="textarea"))
			{
				if(document.forms[0][i].disabled==false){
				document.forms[0][i].focus();
				break;
				}
			}
		}	
	*/
	//Shruti, 07 Nov 2006, MITS 7886 starts
	if(document.forms[0].selDiary != null)
	{
		for(var i=0;i<document.forms[0].selDiary.length;i++)
		{
			if(document.forms[0].selDiary[i].checked)
			{
				lDiaryId=document.forms[0].selDiary[i].value;
				var tdname="_" + lDiaryId;
				showDetails(tdname);
				break;
			}
		}
	}
		
	return true;
			
 }
 function showDetails(tdname)
{
	document.getElementById("textRegarding").value=document.getElementById("tdRegarding"+tdname).value;
	document.getElementById("textNotes").value=document.getElementById("tdNotes"+tdname).value;
	document.getElementById("lblCreatedOn").innerHTML = "Created On: " + document.getElementById("tdCreatedOn" + tdname).value;
	document.getElementById("lblTo").innerHTML = "To: " + document.getElementById("tdTo" + tdname).value;
	document.getElementById("lblFrom").innerHTML = "From: " + document.getElementById("tdFrom" + tdname).value;
	document.getElementById("lblStatus").innerHTML = "Status: " + document.getElementById("tdStatus" + tdname).value;
}

//regarding if regarding = '' then ***
//notes  = inner text of current diaries node
//tdCreatedOn = create_date
//tdTo =  assigned_user , if assigned_user = '' then ***
//tdFrom = assigning_user , if assigning_user = '' then ***
//tdStatus = status , if status = ''then ***

// added by Nitin for R5 diaries
function showDiaryDetails(txtRegarding,textNotes,createdOn,assignedTo,assignedFrom,lblStatus,entry_id,attach_prompt,complete_date,taskName,notRoute,notRoll) {
//Commented by Amitosh because controls have been removed
//    if(textNotes.indexOf('`') != -1)
//	{
//	    textNotes = String(textNotes).replace(/`/g,"`'");
//	    textNotes = String(textNotes).replace(/`/g,"");
//	}
//	
//	if(txtRegarding.indexOf('`') != -1)
//	{
//	    txtRegarding = String(txtRegarding).replace(/`/g,"`'");
//	    txtRegarding = String(txtRegarding).replace(/`/g,"");
//	}
	
	//debugger;
//	document.getElementById("textRegarding").value = txtRegarding;
//	document.getElementById("textNotes").value= textNotes;
//	document.getElementById("lblCreatedOn").innerHTML="Created On: " + createdOn;
//	document.getElementById("lblTo").innerHTML = "To: " + assignedTo;
//	document.getElementById("lblFrom").innerHTML = "From: " + assignedFrom;
//	document.getElementById("lblStatus").innerHTML = "Status: " + lblStatus;
//	
	//added by Nitin 
	document.getElementById("hdnEntry_id").value = entry_id;
	document.getElementById("hdnAssigningUser").value = assignedFrom;
	document.getElementById("hdnAttachPrompt").value = attach_prompt;
	document.getElementById("hdnCreationDate").value = createdOn;
	document.getElementById("hdnCompletionDate").value = complete_date;
	document.getElementById("hdnUser").value = assignedTo;
	document.getElementById("hdnTaskName").value = taskName;
    //igupta3 JIRA 439
	document.getElementById("hdnNotRoutable").value = notRoute;
	document.getElementById("hdnNotRollable").value = notRoll;
}


//Shruti, 07 Nov 2006, MITS 7886 ends
 function onHome()
 {
	document.forms[0].cmd.value="home";
	document.forms[0].ouraction.value="";
	document.forms[0].assigneduser.value="";
	document.forms[0].submit();
	return false;
 }
 
 function onHomefromDiaryHistory()
 {
	document.forms[0].ouraction.value="gotomainpage";
	document.forms[0].assigneduser.value="";
	document.forms[0].submit();
	return false;
 }
 
 function NotAvailViaPeek()
 {
	alert("This function is not available while 'peeking'.\nPress the 'Home' button to return to your own messages and then try again.");
	return false;
 }
 function DiaryList()
{
	self.setTimeout("window.location.href=\"diarylist.asp\"",10);
	return true;
}

function DiaryCalendar()
{
	self.setTimeout("window.location.href=\"diarycalendar.asp\"",10);
	return true;
}

function Edit()
{
	var lDiaryId=getDiaryId();
	if(lDiaryId<=0)
	{
		alert("Please select diary to edit.");
		return false;
	}
	document.forms[0].selDiary.value = lDiaryId;	
	return true;
}

function History()
{
	self.setTimeout("window.location.href=\"wpahistory.asp\"",10);
	return true;
}

function Delete()
{
	var lDiaryId=getDiaryId();
	if(lDiaryId<=0)
	{
		alert("Please select diary to delete.");
		return false;
	}
	document.forms[0].selDiary.value = lDiaryId;
	return true;
}

function Cancel()
{
	self.setTimeout("window.location.href=\"wpamessages.asp?sid=19000\"",10);
	return true;
}

function diaryNavigate(sAction)
{
	var sParams='';
	if(eval(document.forms[0].chkdatedue)!=null){
		if(!document.forms[0].chkdatedue.checked)
		{
			var objCtrl=eval("document.forms[0].txtdatedue");
			if(objCtrl!=null)
				objCtrl.value=getdbDate(objCtrl.value);
				//sUrl=sUrl+"&datedue="+escape(getdbDate(document.forms[0].txtdatedue.value));
			else
				sParams = sParams + "&txtdatedue=" + getdbDate((new Date()).toUTCString());
				//sUrl=sUrl+"&datedue="+getdbDate((new Date()).toUTCString());
		}
		else
			sParams="&chkdatedue=1"
	}
		
	document.forms[0].action = sAction + sParams;
	self.setTimeout("document.forms[0].submit()",10);
	return true;
}

function formatDate1(sParamDate)
{
	//alert("nahin aayega");
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}

//function timeLostFocus(sCtrlName)
//	{
//	var objFormElem=eval('document.forms[0].'+sCtrlName);
//	var sTime=new String(objFormElem.value);
//	if(sTime=="")
//		return true;
//	sTime=sTime.toUpperCase();
//	var sArr=sTime.split(":");
//	if(sArr.length!=2)
//	{
//		objFormElem.value="";
//		return true;
//	}
//	if(sArr[0]=="" || sArr[1]=="" || isNaN(parseInt(sArr[0])) || isNaN(parseInt(sArr[1])))
//		{
//			objFormElem.value="";
//			return true;
//		}
//	sArr[0]=new String(parseInt(sArr[0],10));
//	sArr[1]=new String(parseInt(sArr[1],10));
//	if(sTime.indexOf("AM")<0 && sTime.indexOf("PM")<0)
//	{
//		if(parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>23 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
//		{
//			objFormElem.value="";
//			return true;
//		}
//		if(sArr[0].length==1)
//			sArr[0]="0" + sArr[0];
//		if(sArr[1].length==1)
//			sArr[1]="0" + sArr[1];
//		objFormElem.value=formatTime1(sArr[0]+sArr[1]);
//	}
//	else
//	{
//	if(parseInt(sArr[0],10)<=0 || parseInt(sArr[0],10)>12 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
//	{
//		objFormElem.value="";
//		return true;
//	}
//	if(sArr[0].length==1)
//		sArr[0]="0" + sArr[0];
//	if(sArr[1].length==1)
//		sArr[1]="0" + sArr[1];
//	objFormElem.value=sArr[0]+":"+sArr[1];
//	if(sTime.indexOf("AM")>=0)
//		objFormElem.value=objFormElem.value+" AM";
//	else
//		objFormElem.value=objFormElem.value+" PM";
//	}
//	return true;
//}
					
function formatTime1(sParamTime)
{
	if(sParamTime=="")
		return "";
	var sTime=new String(sParamTime);
		return sTime.substr(0,2) + ":" + sTime.substr(2,2);
}
	
function openattachment(attach_table , attach_recordid , attsecrecid ,pixmlformname, entryid)
{
	//alert(attach_table + attach_recordid + attsecrecid + pixmlformname);
	
	document.forms[0].attachtable.value = attach_table;
	document.forms[0].attachrecordid.value = attach_recordid;
	document.forms[0].attsecrecid.value = attsecrecid;
	document.forms[0].pixmlformname.value = pixmlformname;
	document.forms[0].entryid.value = entryid; //Shruti for MITS 10384. After opening attachment from diary history
	                                          //refresh link stops working.
	
	switch(attach_table)
	{
	
	 //recordID instead of sysformid
		case 'FUNDS' : 
		    ////Shruti for MITS 10384
			//document.forms[0].ouraction.value="movetofunds";
			//document.forms[0].functiontocall.value="FundManagementAdaptor.GetTransaction";
			
			MDIParent.CreateSingletonMDIChild("home?pg=riskmaster/Integrate/Integration&fdmscreen=funds&TransID=" + attach_recordid);
			//MDIParent.CreateSingletonMDIChild("home?pg=riskmaster/Funds/trans&TransID="+attach_recordid);
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
					   
		case "EVENT" :
			MDIParent.CreateSingletonMDIChild("home?pg=riskmaster/Integrate/Integration&fdmscreen=event&SysFormId=" + attach_recordid + "&SysCmd=0");
			//MDIParent.CreateSingletonMDIChild("fdm?SysFormName=event&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
					   
		case "CLAIM" :
			MDIParent.CreateSingletonMDIChild("fdm?SysFormName=claim&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
		
		// Start Naresh The Diaries for the Enhanced Policy were giving errors.
		case "POLICY_ENH" :
			MDIParent.CreateSingletonMDIChild("fdm?SysFormName=policyenh&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
			//Shruti for 10384
		case "CMXCMGRHIST":
		    var sAttachTable = 	"CmXCmgrHist";
		    MDIParent.CreateSingletonMDIChild("fdm?SysFormName=" + sAttachTable + "&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
		
		case "CMXVOCREHAB":
		    var sAttachTable = 	"CmXVocrehab";
		    MDIParent.CreateSingletonMDIChild("fdm?SysFormName=" + sAttachTable + "&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
		case "CMXCMGRHIST":
		    var sAttachTable = 	"CmXCmgrHist";
		    MDIParent.CreateSingletonMDIChild("fdm?SysFormName=" + sAttachTable + "&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
		case "CMXTREATMENTPLN":
		    var sAttachTable = 	"CmXTreatmentPln";
		    MDIParent.CreateSingletonMDIChild("fdm?SysFormName=" + sAttachTable + "&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
		case "CMXMEDMGTSAVINGS":
		    var sAttachTable = 	"CmXMedmgtsavings";
		    MDIParent.CreateSingletonMDIChild("fdm?SysFormName=" + sAttachTable + "&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
			
		case "CASEMGRNOTES":
		    var sAttachTable = 	"CaseMgrNotes";
		    MDIParent.CreateSingletonMDIChild("fdm?SysFormName=" + sAttachTable + "&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
			
		default:	
			var sAttachTable = 	attach_table.toLowerCase();
			MDIParent.CreateSingletonMDIChild("fdm?SysFormName=" + sAttachTable + "&SysFormId="+attach_recordid+"&SysCmd=0");
			//Shruti for MITS 10384
			document.forms[0].attachtable.value = "";
			document.forms[0].attachrecordid.value = "";
			document.forms[0].attsecrecid.value = "";
			document.forms[0].pixmlformname.value = "";
			document.forms[0].ouraction.value="Refresh";
			document.forms[0].submit();
			return false;
			break;
		 
	}
	return true;
}

// Mihika Defect# 2411 6-Mar-2006
// Providing a 'Back' button in case user is viewing Attached Diaries, to take back to the record.
// Apart from 'Funds' all are FDM screens and don't need a page submit
function Back()
{
	if (document.forms[0].attachtable.value != 'FUNDS')
	{
		document.location.href='fdm?SysFormName='+document.forms[0].attachtable.value+'&amp;SysFormId='+document.forms[0].attachrecordid.value+'&amp;SysCmd=0';
		return false;
	}
	else
	{
		document.forms[0].ouraction.value="movetofunds";
		document.forms[0].functiontocall.value="FundManagementAdaptor.GetTransaction";
		return true;
	}
}

function replace(sSource, sSearchFor, sReplaceWith)
{
	var arr = new Array();
	arr=sSource.split(sSearchFor);
	return arr.join(sReplaceWith);
}
function AddActivity()
{
	if(m_codeWindow!=null)
	m_codeWindow.close();

	document.onCodeClose=onCodeClose;
	document.OnAddActivity=OnAddActivity;
	m_codeWindow=window.open('home?pg=riskmaster/Diaries/DiaryActivity','codeWnd',
	'width=400,height=200'+',top='+(screen.availHeight-200)/2+',left='+(screen.availWidth-400)/2+',resizable=yes,scrollbars=yes');
	return false;
}

//Abhishek MITS 11998 - start
function ShowAllDiaries()
{
	document.forms[0].ouraction.value="Refresh";
	if(document.forms[0].chkShowDiariesForActiveRec.checked)
	{
		document.forms[0].hid_ShowAllDiariesForActiveRecord.value='yes';
	}
	else
		document.forms[0].hid_ShowAllDiariesForActiveRecord.value='no';
	document.forms[0].submit();
}
//Abhishek MITS 11998 - end
function ShowAllDiaries4Claim()
{
	document.forms[0].ouraction.value="Refresh";
	if(document.forms[0].chkShowAllClaimDiaries.checked)
	{
		document.forms[0].hid_ShowAllClaimDiaries.value='yes';
	}
	else
		document.forms[0].hid_ShowAllClaimDiaries.value='no';
	document.forms[0].submit();		
}

function OnAddActivity(sId,sText)
{
	if(m_codeWindow!=null)
	m_codeWindow.close();
	m_codeWindow=null;

	var objCtrl=document.forms[0].lstActivities;
	var bAdd=true;
	for(var i=0;i<objCtrl.length;i++)
	{
		if(objCtrl.options[i].value==sId && sId!="0")
		bAdd=false;
	}
	if(bAdd)
	{
		var objOption =	new Option(sText, sId, false, false);
		objCtrl.options[objCtrl.length] = objOption;
		objCtrl=null;
		if(document.forms[0].txtActivites.value!="")
			document.forms[0].txtActivites.value=document.forms[0].txtActivites.value+"|";
		document.forms[0].txtActivites.value=document.forms[0].txtActivites.value+sId+"|"+replace(sText, "|", "&#124;");
		document.forms[0].actstring.value = document.forms[0].txtActivites.value;
	}
}

function DelActivity()
{
	var objCtrl=document.forms[0].lstActivities;
	if(objCtrl.selectedIndex<0)
	return false;

	var bRepeat=true;
	while(bRepeat)
	{
		bRepeat=false;
		for(var i=0;i<objCtrl.length;i++)
		{
		// remove selected elements
		if(objCtrl.options[i].selected)
		{
			objCtrl.options[i]=null;
			bRepeat=true;
			break;
		}
		}
	}	
	// Now create ids list
	var sId="";
	for(var i=0;i<objCtrl.length;i++)
	{
		if(sId!="")
			sId=sId+"|";
		sId=sId+objCtrl.options[i].value+"|"+ replace(objCtrl.options[i].text, "|", "&#124;");
	}
	objCtrl=null;
	document.forms[0].txtActivites.value=sId;
	document.forms[0].actstring.value = document.forms[0].txtActivites.value;
	return true;
}
function numLostFocusNonNegative(objCtrl)
{
	if(objCtrl.value.length==0)
			return false;
	if (objCtrl.value.indexOf('-') == 0)
	{
		alert("Negative value not allowed for this field");
		objCtrl.focus();
		return false;
	}
	if(isNaN(parseFloat(objCtrl.value)))
		objCtrl.value="";
	else
		objCtrl.value=parseFloat(objCtrl.value);
	
	if(objCtrl.getAttribute('min')!=null)
	{
		if (!(isNaN(parseFloat(objCtrl.getAttribute('min')))))
		{
			if (parseFloat(objCtrl.value)<parseFloat(objCtrl.getAttribute('min')))
			{
				objCtrl.value=0;
			}
		}
	}
	
	return true;
}

// MITS 9391 : Anjaneya
function currencyLostFocus(objCtrl)
{
	var dbl=new String(objCtrl.value);
	
	// Strip and Validate Input
	// BSB Remove commas first so parseFloat doesn't incorrectly truncate value.	
	dbl=dbl.replace(/[,\$]/g ,"");
	
	if(dbl.length==0)
		return false;
	
	//Geeta 11/21/06 : Modified to fix Mits no. 8427 for Est. Collection 			
	if(dbl.indexOf('(') >= 0)
	{		
		if(dbl.indexOf('-') >= 0)
		{
			dbl=dbl.replace(/[\-\(\)]/g ,"");
		}
		else
		{
			dbl='-'+ dbl.replace(/[\(\)]/g ,"");	
		}
	}				
	else if(dbl.indexOf('-') >= 0)
	{
		dbl='-' + dbl.replace(/[\-]/g ,"");				
	} 		

	if(isNaN(parseFloat(dbl)))
	{
		objCtrl.value="";	
		return false;
	}	

	//round to 2 places
	dbl=parseFloat(dbl);
	//Parijat: Mits 7257-- hourly rate to have 3 places to decimal
	if(objCtrl.id =='emphourlyrate')
	{
	    dbl=dbl.toFixed(3);
	}
	else
	{
	dbl=dbl.toFixed(2);
	}

	//Handle Full Integer Section (Formats in groups of 3 with comma.)
	var base = new String(dbl);
	base = base.substring(0,base.lastIndexOf('.'));
	var newbase="";
	var j = 0;
	var i=base.length-1;
	for(i;i>=0;i--)
	{	if(j==3)
		{
			newbase = "," + newbase;	
			j=0;

		}
		newbase = base.charAt(i) + newbase;	
		j++;
	}

	
	// Handle Fractional Part
	var str = new String(dbl);
	str = str.substring(str.lastIndexOf(".")+1);

	// Reassemble formatted Currency Value string.

	objCtrl.value= "$" + newbase + "." + str;
	return true;
}
