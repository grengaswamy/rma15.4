try { 
    parent.CommonValidations = parent.parent.CommonValidations;
    if (parent.CommonValidations == undefined) {
        if (parent.opener != undefined) {
            if (parent.opener.parent != undefined) {
                if (parent.opener.parent.parent != undefined) {
                    parent.CommonValidations = parent.opener.parent.parent.CommonValidations;
                }
            }
        }
            //Deb: MITS 35042
        else if (window.dialogArguments != undefined) {
            if (window.dialogArguments.parent != undefined) {
                parent.CommonValidations = window.dialogArguments.parent.CommonValidations;
            }
        }
    }
} catch (e) {
}
function ListPaging(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
    var bDisableNavigation=false;
    var bDisablePrevNavigation=false;
    var bDisableNextNavigation=false;
    
    this.showRecords = function(from, to) {   
        var rows = document.getElementById(tableName).rows;
        // skip the table header row 
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)  
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
    
    this.showPage = function(pageNumber) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}
    	 //Rakhi-START:Disable Prev Navigation if it is the first page and Disable Next Navigation if it is the last page
        if(pageNumber=='1')
        {
            bDisablePrevNavigation=true;
        }
        else
        {
            bDisablePrevNavigation=false;
        }
        if(pageNumber==this.pages)
        {
            bDisableNextNavigation=true;
        }
        else
        {
            bDisableNextNavigation=false;
        }
        listpaging.showPageNavigation('listpaging', 'pageNavPosition'); 
        //Rakhi-END:Disable Prev Navigation if it is the first page and Disable Next Navigation if it is the last page
        

        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
       
       
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
       //Rakhi:To change active link color
        newPageAnchor.style.color='red';
        //Rakhi:To change active link color
        
       
        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
        
    }       
    
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);        
    }
    
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);   
        }
    }                        
    
    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1); 
        this.pages = Math.ceil(records / itemsPerPage);
         //Rakhi-START:Disable prev and next if  records are less than or equal to itemsPerPage
        if(records<=itemsPerPage)
        {
            bDisableNavigation=true;
        }
        else
        {
            bDisableNavigation=false;
        }
        //Rakhi-END:Disable prev and next if  records are less than or equal to itemsPerPage
        this.inited = true;
    }

    this.showPageNavigation = function(pagerName, positionId) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}
    	var element = document.getElementById(positionId);
        //Bijender Has add "return false" in <a> tag for Mits 14894
    	if (!bDisableNavigation) {
    	    
    	    var pagerHtml = '';
    	    if (!bDisablePrevNavigation)
	     //rkaur27 : RMA-19284 - Start(Removed quotes) 
    	        pagerHtml = '<a href="#" id="lnkprev" onclick="' + pagerName + '.prev();" class="pg-normal"><font face="Tahoma" size="2"> &#171 ' + parent.CommonValidations.PagingsortingPrev + '</font></a> | ';
    	    else
    	        pagerHtml = '<a href="#" disabled="true" id="lnkprev" onclick="' + pagerName + '.prev();return false;" class="pg-normal"><font face="Tahoma" size="2"> &#171 ' + parent.CommonValidations.PagingsortingPrev + '</font></a> | ';
    	    for (var page = 1; page <= this.pages; page++)
    	        pagerHtml += '<a href="#" id="pg' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + "<font face='Tahoma' size='2'>" + page + "</font>" + '</a> | ';
    	    if (!bDisableNextNavigation)
    	        pagerHtml += '<a href="#" id="lnknext" onclick="' + pagerName + '.next();" class="pg-normal"><font face="Tahoma" size="2">' + parent.CommonValidations.PagingsortingNext + ' &#187;</font></a>';
    	    else
    	        pagerHtml += '<a href="#" disabled="true" id="lnknext" onclick="' + pagerName + '.next();return false;" class="pg-normal"><font face="Tahoma" size="2">' + parent.CommonValidations.PagingsortingNext + ' &#187;</font></a>';
    	}
    	else {
    	    //var pagerHtml = '<a href="#" disabled="true" id="lnkprev" onclick="' + pagerName + '.prev();return false;" class="pg-normal"><font face="Tahoma" size="2"> &#171 prevtest2</font></a> | ';
    	    var pagerHtml = '<a href="#" disabled="true" id="lnkprev" onclick="' + pagerName + '.prev();return false;" class="pg-normal"><font face="Tahoma" size="2"> &#171 ' + parent.CommonValidations.PagingsortingPrev + '</font></a> | ';
    	    pagerHtml += '<a href="#" id="pg1" class="pg-normal" onclick="' + pagerName + '.showPage(1);">' + "<font face='Tahoma' size='2'>" + "1" + "</font>" + '</a> | ';
    	    //pagerHtml += '<a href="#" disabled="true" id="lnknext" onclick="' + pagerName + '.next();return false;" class="pg-normal"><font face="Tahoma" size="2">next &#187;</font></a>'
    	    pagerHtml += '<a href="#" disabled="true" id="lnknext" onclick="' + pagerName + '.next();return false;" class="pg-normal"><font face="Tahoma" size="2">' + parent.CommonValidations.PagingsortingNext + ' &#187;</font></a>'
            //rkaur27 : RMA-19284 - End(Removed quotes)
    	}
        //BIjender End Mits 14894
    	element.innerHTML = pagerHtml;
    }
}

/*----------------------------------------------------------------------------
    SortList : Performs a client side sort of the contents of a HTML Table.

    args:   ao_table, The object reference to the table to be sorted.
            ai_sortcol, The zero based column number to be sorted.
            ab_header, Bool to indicate if the table have a header
                row to be ignored.

    vars:   lastcol, used to store the last column sorted.
            lastseq, used to store the sequence the last column was sorted by.
----------------------------------------------------------------------------*/
var lastcol, lastseq;
function SortList( ao_table, ai_sortcol, ai_lnkcol,ab_header )
{

    var ir, ic, is, ii, id;   
    ir = ao_table.rows.length;
    if( ir < 1 ) return;

    if(ao_table.rows[1]!=null)
        ic = ao_table.rows[1].cells.length;
    // if we have a header row, ignore the first row
    if( ab_header == true ) is=1; else is=0;

    // take a copy of the data to shuffle in memory
    var row_data = new Array( ir );
    ii=0;
    
    var sRows =  document.all.tbllist.rows.length ;        
    
    //Geeta maintain the selected checkbox value in the table row
    if(document.getElementById("PageId1")!=null)
    { 
        for(var i=1; i <=sRows-1; i++)
        {     
            if(document.getElementById("PageId" + i).checked)
            {           
               ao_table.rows[i].cells[ic-3].innerText = "true" ;                  
            }
            else
            {
               ao_table.rows[i].cells[ic-3].innerText = "false" ;                          
            }        
        }
     }
    
    for( i=is; i < ir; i++ )
    {
        var col_data = new Array( ic );
        for( j=0; j < ic; j++ )
        {
            //col_data[j] = ao_table.rows[i].cells[j].innerHTML; //Modified ttumula2 for firefox support RMA-14675
            col_data[j] = getText(ao_table.rows[i].cells[j]);
        }
        row_data[ ii++ ] = col_data;
    }
    //debugger;
    // sort the data
    var bswap = false;
    var row1, row2;
    
    if( ai_sortcol != lastcol )
        lastseq = 'A';
    else
    {
        if( lastseq == 'A' ) lastseq = 'D'; else lastseq = 'A';
    }

    // if we have a header row we have one less row to sort
    if( ab_header == true ) id=ir-1; else id=ir;
    
    //Rakhi-START:Append Sort Image to Sort Column
    
     if( ab_header == true )
     {
        var cols=ao_table.rows[0].cells.length;
        for(var intCount=0;intCount<cols;intCount++)
        {
              if(ao_table.rows[0].cells[intCount].innerHTML!='' && ao_table.rows[0].cells[intCount].innerHTML.indexOf("SortList")!= -1 ) //To check it is a SortColumn or Not.
              { 
                    if(intCount==ai_sortcol)
                    {
                        var SortImage='';
                        if(lastseq=='A')
                            {
                            //SortImage=document.createElement('<img src="..\\..\\Images\\arrow_up_white.gif" border="0" />')   //Jira id: 9473    
                            
                            SortImage = document.createElement('img');
			    SortImage.setAttribute('src', '..\\..\\Images\\arrow_up_white.gif');
			    SortImage.setAttribute('border', '0');

                            }
                         else
                            {
                            //SortImage = document.createElement('<img src="..\\..\\Images\\arrow_down_white.gif" border="0" />') //Jira id: 9473

                            SortImage = document.createElement('img');
			    SortImage.setAttribute('src', '..\\..\\Images\\arrow_down_white.gif');
			    SortImage.setAttribute('border', '0');
                            }

                        //document.getElementById("HeaderColumn" + ai_sortcol).innerHTML = document.getElementById("HeaderColumn" + ai_sortcol).innerText; //To remove any <img> tag already appended
                        if (document.getElementById("HeaderColumn" + ai_sortcol)!=null)
                            document.getElementById("HeaderColumn" + ai_sortcol).innerHTML = getText(document.getElementById("HeaderColumn" + ai_sortcol)); //Modified ttumula2 for firefox support RMA-14675

                               for(var i=1; i <=sRows-1; i++)//append image only if data exists in rows of the Header Row.
                               {
                                    if(ao_table.rows[i].cells[ai_sortcol]!= null && trim(ao_table.rows[i].cells[ai_sortcol].innerHTML)!='' )
                                    {
                                        if (document.getElementById("HeaderColumn" + ai_sortcol)!=null)
                                            document.getElementById("HeaderColumn" + ai_sortcol).appendChild(SortImage);
                                       break; 
                                    }
                               }
                    } 
                    else
                     {
                       // document.getElementById("HeaderColumn" + intCount).innerHTML = document.getElementById("HeaderColumn" + intCount).innerText;
                        if (document.getElementById("HeaderColumn" + intCount)!=null)
                        	document.getElementById("HeaderColumn" + intCount).innerHTML = getText(document.getElementById("HeaderColumn" + intCount)); //Modified ttumula2 for firefox support RMA-14675
                     }        
           }    
       } 
     }       
    //Rakhi-End:Append Sort Image to Sort Column
    
    for( i=0; i < id; i++ )
    {
        bswap = false;
        for( j=0; j < id - 1; j++ )
        {
            // test the current value + the next and
            // swap if required.
            row1 = row_data[j];
            row2 = row_data[j+1];
            if( lastseq == "A" )
            {
                if( row1[ ai_sortcol ].toUpperCase() > row2[ ai_sortcol ].toUpperCase() )
                {
                    row_data[j+1] = row1;
                    row_data[j] = row2;
                    bswap=true;
                }
            }
            else
            {
                if( row1[ ai_sortcol ].toUpperCase() < row2[ ai_sortcol ].toUpperCase() )
                {
                    row_data[j+1] = row1;
                    row_data[j] = row2;
                    bswap=true;
                }
            }
        }
        if( bswap == false ) break;
    }

    // load the data back into the table
    //debugger;
    ii = is;
    var count = 1;
    var sLinkCol = ai_lnkcol;
    var sTempUrl = new String();
    var arrTempId = new String();
    
    for( i=0; i < id; i++ )
    {
        row1 = row_data[ i ];
        for( j=0; j < ic; j++ )
        {        
        if((j==0) && trim(row1[j])== '')
        {  
            //Do Nothing             
        }
        else if ((j == 0) && trim(row1[j]) != '' && document.getElementById("PageName") != null && document.getElementById("PageName").value.toLowerCase() != 'personinvolvedlist') //claimadjusterlist and adjusterdatedtextlist screen
        {
             if(document.getElementById(ai_lnkcol)!=null)
             {
                document.getElementById(ai_lnkcol + count ).innerHTML = row1[j]; 
                if(document.getElementById("PageName").value.toLowerCase()=='claimadjusterlist')                                
                    document.getElementById(ai_lnkcol + count).setAttribute("href","javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("+ '"' + ai_lnkcol + count + '","",false,"","'+ row1[ic-4]+'",false,true))');                                                                                  
                else 
                    document.getElementById(ai_lnkcol + count).setAttribute("href","javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("+ '"' + ai_lnkcol + count + '","",false,"","'+ row1[ic-2]+'",false,true))');                                                                                  
             }   
        } 
        else if(j==1 && document.getElementById("PageId" + count) !=null)
        {
            
            document.getElementById(ai_lnkcol + count).innerHTML = row1[j];
           //Changed for MITS:15713:Sorting of Person Involved.
	    //MITs 33812 : Undoing changes here and fixing below.
            if (document.getElementById("PageName") != null && document.getElementById("PageName").value.toLowerCase() == 'personinvolvedlist') //changes for onClientClick of PersonInvolved
                  {
                      
                      //document.getElementById(ai_lnkcol + count).setAttribute("href","javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("+ '"' + ai_lnkcol + count + '","",false,"","'+ row1[ic-2]+'",false,true))');
                      document.getElementById(ai_lnkcol + count).setAttribute("href", "#"); //to stop postback
                      var obj = row1[ic - 2];
                      var PIInfo = obj.split(",");
                      if (document.getElementById(ai_lnkcol + count + "_RowId")!=null)
                          document.getElementById(ai_lnkcol + count + "_RowId").value = trim(PIInfo[0]);

                      if (document.getElementById(ai_lnkcol + count + "_ScreenType")!=null)
                          document.getElementById(ai_lnkcol + count + "_ScreenType").value = trim(PIInfo[1]);

                      var obj = document.getElementById(ai_lnkcol + count);
                          $('#'+ai_lnkcol+count).bind("click", { id1: trim(PIInfo[0]), id2: trim(PIInfo[1]) }, function (event) { CallToMDI(event.data.id1,event.data.id2); });
                
            }

                  else { 
                  
                    document.getElementById(ai_lnkcol + count).setAttribute("href","javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("+ '"' + ai_lnkcol + count + '","",false,"","'+ row1[ic-2]+'",false,true))');
                }
                //Changed for MITS:15713:Sorting of Person Involved.
                    document.getElementById("PageId" + count).value = row1[ic - 1];
                    if (row1[ic - 3] == "true") {
                        document.getElementById("PageId" + count).checked = true;
                    }
                    else {
                        document.getElementById("PageId" + count).checked = false;
                    }
        } 
        else if(row1[j]=='...'&& document.getElementById("PageName") !=null)//claimadjusterlist screen
        {
             if(document.getElementById("PageName").value.toLowerCase()=='claimadjusterlist')   
             { 
                 var ai_lnkcol1=row1[ic-2];//second link column name
                 document.getElementById(ai_lnkcol1 + count ).innerHTML = row1[j];                                 
                 document.getElementById(ai_lnkcol1 + count).setAttribute("href","javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("+ '"' + ai_lnkcol1 + count + '","",false,"","'+ row1[ic-1]+'",false,true))');                                                                                     
             }
             else
             {
                 //ao_table.rows[ii].cells[j].innerText = row1[j];
                 ao_table.rows[ii].cells[j].innerHTML = row1[j]; //Modified ttumula2 for firefox support RMA-14675
             }    
        }       
        else
        {
            //Changed for MITS:15713:Sorting of Person Involved.
            //ao_table.rows[ii].cells[j].innerText = row1[j];
            //MITS 33812 : Error fixed due to change in number of columns.
            //if ((j == 3 || j == 4 || j == 5) && document.getElementById("PageName") != null && document.getElementById("PageName").value.toLowerCase() == "personinvolvedlist") //changes for onClientClick of PersonInvolved
            if ((j == 4 || j == 5) && document.getElementById("PageName") != null && document.getElementById("PageName").value.toLowerCase() == "personinvolvedlist") //Modified ttumula2 for Sorting Enabled for Personinvolve Level  RMA-14675
            {
                //do nothing 
            }
            else {
                //ao_table.rows[ii].cells[j].innerText = row1[j];
                ao_table.rows[ii].cells[j].innerHTML = trim(row1[j]); //Modified ttumula2 for firefox support RMA-14675
            }
                    //Changed for MITS:15713:Sorting of Person Involved.
        }
        }        
        ii++;
        count++;
    }
    
    lastcol = ai_sortcol;
}

  function DeleteList()
  {      
    var sRows =  document.all.tbllist.rows.length ;   
    var boolChecked=false;
    var ret;
    document.forms[0].PageIds.value ="";     
    for(var i=1; i <=sRows-1; i++)
    {     
        if(document.getElementById("PageId" + i).checked)
        {       
            if(trim(document.forms[0].PageIds.value)=='')
            {
                document.forms[0].PageIds.value = document.getElementById("PageId" + i).value ;
            }
            else
            {            
               document.forms[0].PageIds.value =  document.forms[0].PageIds.value  + " " + document.getElementById("PageId" + i).value;             
            }
            
             boolChecked=true;	
         }    
           	
        }
        
        if(boolChecked)
        {         
            ret = confirm(parent.CommonValidations.DeleteConfirm);
		    if(ret==true)
		    {
		        return true;                
            }
		    else
		    {
	            for(var j=1; j <=sRows-1; j++)		
		        {
		            document.getElementById("PageId" + j).checked = false;				 
			    }		  							    
		        return false;
		   }		   
		}		
		
		if (!boolChecked)
	    {
		    alert(parent.CommonValidations.SelectRecordDelete);
		    return false;
	    }
	    
 }  

function trim(str)
{
  while(str.charAt(0) == (" ") )
  {  str = str.substring(1);
  }
  while(str.charAt(str.length-1) == " " )
  {  str = str.substring(0,str.length-1);
  }
  return str;
}
function CallToMDI(obj) {
    var objPI = document.getElementById(obj.id).id;
    var param1 = document.getElementById(objPI + "_RowId").value;
    var param2 = document.getElementById(objPI + "_ScreenType").value;
    parent.MDIShowScreen(param1,param2);
    
    
    
    
    
    
}





function CallToMDI(rowid, screentype) {
    debugger;
    parent.MDIShowScreen(rowid, screentype);
}

function getText(el) //Add ttumula2 for firefox support RMA-14675
{
    //if (el.textContent) return el.textContent;
    if ((el!=null)&&(el.innerText))
        return el.innerText;
    return getText2(el);

    function getText2(el) {
        if((el!=null)&&(el.childNodes!=null))
            var x = el.childNodes;

        var txt = '';
        if (x != null) {
            for (var i = 0, len = x.length; i < len; ++i) {
                if (3 == x[i].nodeType) {
                    txt += x[i].data;
                } else if (1 == x[i].nodeType) {
                    txt += getText2(x[i]);
                }
            }
        }
        return txt.replace(/\s+/g,' ');
    }
}







