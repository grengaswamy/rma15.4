//Start Add ttumula2 for RMA-6959
try {
    parent.CommonValidations = parent.parent.CommonValidations;
    if (parent.CommonValidations == undefined) {
        if (parent.opener != undefined) {
            if (parent.opener.parent != undefined) {
                if (parent.opener.parent.parent != undefined) {
                    parent.CommonValidations = parent.opener.parent.parent.CommonValidations;
                }
            }
        }
            //Deb: MITS 35042
        else if (window.dialogArguments != undefined) {
            if (window.dialogArguments.parent != undefined) {
                parent.CommonValidations = window.dialogArguments.parent.CommonValidations;
            }
        }
    }
} catch (e) {
}

//End Add ttumula2 for RMA-6959
// Author: Tom Regan, 04/02/2002
// SRC statement in piworkloss.xml

/*if (document.forms[0].calculatedurations != null){
	if (document.forms[0].calculatedurations.value == "True"){
		setDataChanged(true);
		document.forms[0].calculatedurations.value="False";
	}
}*/

function calculateDurations()
{
	var dReturnToWork = new Date();
	var sLastWorkDay = document.getElementById('datelastworked').value; //new String($(datelastworked)[0].value);
	var i = sLastWorkDay.length;
	if (i == 0)
		{
	    //alert('Last Work Day is required');
	    alert(parent.CommonValidations.WorklossLastWorkDayrequired)
		return false;
		}
		
	var dLastWorkDay = new Date(sLastWorkDay);
	var sReturnToWork = document.getElementById('datereturned').value; //new String($(datereturned)[0].value);
	if (sReturnToWork.length > 0) {
	    dReturnToWork = new Date(sReturnToWork);

	    //MITS 29396 hlv 11/23/12 begin
	    var iDifference = dReturnToWork.getTime() - dLastWorkDay.getTime();
	    iDifference = Math.floor(iDifference / (1000 * 60 * 60 * 24));
	    if (iDifference <= 0) {
	        //alert('Error, Return to Work day must be after the Last Work Day');
	        alert(parent.CommonValidations.WorklossReturnWorkdayafterLastWorkDay)
	        return false;
	    }

	    var iMonth = dReturnToWork.getMonth();
	    iMonth = iMonth + 1;
	    //if (!window.ie)
	    document.getElementById('datereturned').value = iMonth + '/' + dReturnToWork.getDate() + '/' + dReturnToWork.getFullYear(); //igupta3
	    //else
	        //document.getElementById('datereturned').value = iMonth + '/' + dReturnToWork.getDate() + '/' + dReturnToWork.getYear();
	    //MITS 29396 hlv 11/23/12 end
	}
	else {  //MITS 29396 hlv 11/23/12 begin
	    // alert('Cannot calculate the duration. Please enter "Return to work" date');
	    alert(parent.CommonValidations.WorklossCannotcalculatedurationenterReturnworkdate)
	    document.getElementById('datereturned').focus();
	}
	//MITS 29396 hlv 11/23/12 end

	//MITS 29396 hlv 11/23/12 move top begin
//	var iDifference = dReturnToWork.getTime() - dLastWorkDay.getTime();
//	iDifference = Math.floor(iDifference / (1000 * 60 * 60 * 24));
//	if (iDifference <= 0)
//		{
//			alert('Error, Return to Work day must be after the Last Work Day');
//			return false;
//		}
	//MITS 29396 hlv 11/23/12 move top end
	
	//document.forms[0].calculatedurations.value = "True";
    document.getElementById('calculatedurations').value = "-1";
	//$(calculatedurations).value = "-1";

	//MITS 29396 hlv 11/23/12 move top begin
//	var iMonth = dReturnToWork.getMonth();
//	iMonth = iMonth+1;
//	document.forms[0].datereturned.value = iMonth + '/' + dReturnToWork.getDate() + '/' + dReturnToWork.getYear();
	//MITS 29396 hlv 11/23/12 move top end
	
    var s = document.getElementById('datereturned').name;//$(datereturned)[0].name;
	// TODO - Aditya - Check this functionality
	//var b = dateLostFocus(s);
	
	m_DataChanged = false;

	//Navigate(0);
	//MGaba2:05/21/2009: Form was getting submitted twice,due to which error was coming
	//Navigate(7);	
	document.forms[0].SysCmd.value = 7;
	document.forms[0].submit();
	
}
//Ritesh: Added to reset focus on control-MITS 27402
function CorrectFocus(ctrl) {    
    SetFocus(ctrl);
}