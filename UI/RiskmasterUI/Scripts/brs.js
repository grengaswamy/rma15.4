//'step constants also defined in brs.js, brsnet(CCalculate)
var step_Dates = 0;
var step_FeeSchedule = 1;
var step_LoadBrsXml = 2;
var step_Calculate = 3;
var step_Specialty = 4;
var step_ProcedureCase = 5;
var step_ICCPT = 6;
var step_Override = 7;
var step_EditSplit = 8;
var step_PreLoad = 9;
var m_OrigAmtToPay = 0;
// Navigation Constants
var SysCmdNavigationNone = 0
var SysCmdNavigationFirst = 1
var SysCmdNavigationPrev = 2
var SysCmdNavigationNext = 3
var SysCmdNavigationLast = 4
var SysCmdSave = 5
var SysCmdDelete = 6

//constants also defined in brsnet(CCalculate.BillOverrideType Enum); 66 & 77 unique to here
var DO_NOT_OVERRIDE = 0;         //Billed Amount
var APPLY_DISC_ON_SCHD = 1;      //Billed Amount * dPercentageBill
var APPLY_CONTRACT_AMOUNT = 2;   //Contract amount
var APPLY_DISC_ON_CONTRACT = 3;
var APPLY_PER_DIEM = 4;
var APPLY_PER_DIEM_STOP_LOSS = 5;
var APPLY_ENT_FEE_SCHD = 6; 	//scheduled amount from feeschedule1
var APPLY_ENT_FEE_SCHD_2 = 66; 	//scheduled amount from feeschedule2
var APPLY_DISC_ON_ENT_FEE = 7;   //scheduled amount * dPercentage from feeschedule1
var APPLY_DISC_ON_ENT_FEE_2 = 77;   //scheduled amount * dPercentage from feeschedule2

function haveProperty(obj, sPropName) 
{
    for (p in obj) 
    {
            if (p == sPropName)
            return true;
    }
    return false;
}

function brsInitialPageLoad() 
{
    pleaseWait.Show('start');

    var Gridname = document.getElementById('gridname').value + '_';
    var SelectedRowPosition = document.getElementById('selectedrowposition').value;
    var gridmode = document.getElementById('gridmode').value;

    var objParentDocument = window.opener.document;
    if (objParentDocument == null) 
    {
        alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
        return false;
    }

    var brsmode = gridmode;
    var oIsReadonly = objParentDocument.getElementById('isReadOnly');
    if (oIsReadonly != null)
     {
         if (oIsReadonly.value == 'true') 
        {
            brsmode = 'view';
        }
    }
    document.getElementById('brsmode').value = brsmode.toLowerCase();

    var ParentElement = null;

    //Get the last valid Row position which will be used to enable/disable navigation buttons
    var iLastRowPosition = 2;
    var sRowPosition = '';
    var ParentElementId = '';
    while (true)
     {
        if (parseInt(iLastRowPosition) < 10)
            sRowPosition = '0' + iLastRowPosition;
        else
            sRowPosition = iLastRowPosition;

        ParentElementId = Gridname + 'gvData_ctl' + sRowPosition + '_|' + sRowPosition + '|Data';
        ParentElement = objParentDocument.getElementById(ParentElementId);
        if (ParentElement == null) 
        {
            document.getElementById('lastrowposition').value = iLastRowPosition - 2;
            break;
        }
        iLastRowPosition++;
    }

    document.getElementById('txtData').value = "";

    ParentElement = objParentDocument.getElementById('claimid');
    if (ParentElement != null)
     {
        document.getElementById('claimid').value = ParentElement.value;
    }


    //    Mona: R8.2 : BRS in Carrier claim
    
    ParentElement = objParentDocument.getElementById('PolicyID');
    if (ParentElement != null) {
        document.getElementById('PolicyID').value = ParentElement.value;
    }
    ParentElement = objParentDocument.getElementById('PolCvgID');
    if (ParentElement != null) {
        document.getElementById('PolCvgID').value = ParentElement.value;
    }
    ParentElement = objParentDocument.getElementById('IsFirstFinalQueryString');
    if (ParentElement != null) {
        document.getElementById('IsFirstFinalQueryString').value = ParentElement.value;
    }

    ParentElement = objParentDocument.getElementById('RcRowID');
    if (ParentElement != null) {
        document.getElementById('RcRowID').value = ParentElement.value;
    }
    //Transaction codes were not coming
    ParentElement = objParentDocument.getElementById('clm_entityid');
    if (ParentElement != null) {
        document.getElementById('clm_entityid').value = ParentElement.value;
    }

    ParentElement = objParentDocument.getElementById('collectionflag');
    if (ParentElement != null)
    {
        if (ParentElement.checked == true)
        {
           document.getElementById('PaymentOrCollection').value = "collection";                
        }
        else
        {
            document.getElementById('PaymentOrCollection').value = "payment";  
        }        
    }
    //    Mona: R8.2 : BRS in Carrier claim


    ParentElement = objParentDocument.getElementById('pye_zipcode');
    document.getElementById('zip').value = ParentElement.value;

    ParentElement = objParentDocument.getElementById('transid');
    document.getElementById('transid').value = ParentElement.value;

    ParentElement = objParentDocument.getElementById('pye_entityid');
    document.getElementById('providereid').value = ParentElement.value;

    ParentElement = objParentDocument.getElementById('lob');
    document.getElementById('lineofbusinesscode').value = ParentElement.value;

    if (gridmode == 'edit')
     {
        var objResubmitflag = objParentDocument.getElementById('resubmitflag');
        if (objResubmitflag != null) 
        {
            if (objResubmitflag.checked)
                document.getElementById('resubmitflag').value = "true";
        }
    }

    var ParentElement = objParentDocument.getElementById("FundsBRSSplitsGrid_Data");
    if (ParentElement != null)
     {
        document.getElementById('txtData').value = ParentElement.value;
    }

    document.forms[0].submit();
}

function PopulateToDate(sCtrlName) 
{
    dateLostFocus(sCtrlName)
    var objFormElem = eval('document.forms[0].' + sCtrlName);
    if (objFormElem.value != "") 
    {
        //vkumar258 RMA-7898 Starts
      //  var sDate = new Date(objFormElem.value);
       // sDate.setDate(sDate.getDate());

        //var target = $("#changedate");
        var target = $("#" + sCtrlName);

        var inst = $.datepicker._getInst(target[0]);

            fdate = inst.selectedDay;
            fmonth = inst.selectedMonth;
            fyear = inst.selectedYear;
            var strDate = new Date();
            strDate.setFullYear(fyear, fmonth, fdate);


            inst.selectedDay = inst.currentDay = fdate;
            inst.selectedMonth = inst.currentMonth = fmonth;
            inst.selectedYear = inst.currentYear = fyear;
            var dateStr = $.datepicker._formatDate(inst, inst.currentDay, inst.currentMonth, inst.currentYear);
            dateStr = (dateStr != null ? dateStr : $.datepicker._formatDate(inst));

        //var dd = sDate.getDate();
        //var mm = sDate.getMonth() + 1; //January is 0!
        //var yyyy = sDate.getFullYear();
        //if (dd < 10) { dd = '0' + dd }
        //if (mm < 10) { mm = '0' + mm }
        //var strDate = new String(mm + '/' + dd + '/' + yyyy);

        //vkumar258 RMA-7898 End

        objFormElem = eval('document.forms[0].dtodate');
        if (objFormElem != null) 
        {
            //objFormElem.value = strDate;
            objFormElem.value = dateStr; //Date.parse(strDate.toDateString());

        }
    }

    //Try to populate the Fee Schedule list with the new from/to date
    document.forms[0].PostbackAction.value = "datechanged";
    document.forms[0].submit();
}


function ToDateChanged() 
{
    //Try to populate the Fee Schedule list with the new to date if the ToDate change is not
    //caused by FromDate change
    if (document.forms[0].PostbackAction.value != "datechanged")
     {
        document.forms[0].PostbackAction.value = "datechanged";
        document.forms[0].submit();
    }
}


function OnCancel() 
{
    var brsmode = document.forms[0].brsmode.value;
    var resubmitmode = document.forms[0].resubmitmode.value;
    if (brsmode != 'view' || resubmitmode == 'true')
     {
         if (!confirm("This will return you to the Funds screen without saving any bills you entered in this session.  Are you sure you want to close Bill Review System?")) 
        {
            return false;
        }
    }

    m_DataChanged = false;
    window.close();
    return false;
}


function brsSummary() 
{
    if (m_codeWindow != null)
        m_codeWindow.close();
    m_codeWindow = RMX.window.open("brssummary.asp", 'codeWnd', 'width=500,height=290' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    return true;
} 


function selectFee(s)
 {
    //when choose one, the other is unselected
    var l = 0;
    if (s == "feesched") {
        l = document.forms[0].feesched.value;
        if (l != 0 && l != null)
            document.forms[0].statefee.selectedIndex = -1;
    }
    else {
        l = document.forms[0].statefee.value;
        if (l != 0 && l != null)
            document.forms[0].feesched.selectedIndex = -1;
    }
    setDataChanged(true);

}


function onOverrideSelected(sOption, sAmount)
 {
    alert("Option: " + sOption + " amount:" + sAmount);
}
function showICCPT(sParam) { alert("called for iccpt"); }
function showSpecialtyShortCode(sParam) { alert("called for specialty short code"); }
function showProcedureCase(sParam) { alert("called for procedure case"); }
function showManualReduction(sParam) { alert("called for manual reduction"); }
//*************************************************************

function pageLoadedBrs() 
{
    // 05/01/2009 abisht Anyone who make changes to this function keep this in mind that this same function is used
    // as the page load function by provider contract screen.So any control that is not in contract screen will 
    // give error in this function so please make sure you have the null condition for that control.
    if (document.forms[0].ReturnButtonClicked != null){
         if (document.forms[0].ReturnButtonClicked.value == "true") 
        {
            return;
        }
    }
    //abahl3 starts, rma 111468,11121,11120
    if (document.getElementById("reservetypecode") != null) {
        if (window.opener.document.forms[0].ResTypeCode != null) {
            document.getElementById("reservetypecode").value = window.opener.document.forms[0].ResTypeCode.value;
        }
    }


    if (window.opener.document.forms[0].RcRowID != null) {

        if ((window.opener.document.forms[0].RcRowID.value != "") && (window.opener.document.forms[0].RcRowID.value != "0")) {
            if (document.forms[0].ReserveTypeCodeFt_codelookup != null) {
                document.forms[0].ReserveTypeCodeFt_codelookup.readOnly = "readonly";
                document.forms[0].ReserveTypeCodeFt_codelookup.style.backgroundColor = "#F2F2F2";

            }
            if (document.forms[0].ReserveTypeCodeFt_codelookupbtn != null)
                document.forms[0].ReserveTypeCodeFt_codelookupbtn.style.visibility = "hidden";

            if (document.forms[0].Coverage_codelookup_cid != null) {
                document.forms[0].Coverage_codelookup_cid.readonly = "readonly";
                document.forms[0].Coverage_codelookup_cid.color = "#ffffff";
            }
            if (document.forms[0].Coverage_codelookup != null) {
                document.forms[0].Coverage_codelookup.readOnly = "readonly";
                document.forms[0].Coverage_codelookup.style.backgroundColor = "#F2F2F2";
            }
            if (document.forms[0].Coverage_codelookupbtn != null)
                document.forms[0].Coverage_codelookupbtn.style.visibility = "hidden";
            if (document.forms[0].Policy != null)
                document.forms[0].Policy.disabled = true;
            if (document.forms[0].FFPayment != null)
                document.forms[0].FFPayment.disabled = true;
            if (document.forms[0].Unit != null)
                document.forms[0].Unit.disabled = true;

            if (document.forms[0].LossType_codelookup != null) {
                document.forms[0].LossType_codelookup.readOnly = "readonly";
                document.forms[0].LossType_codelookup.style.backgroundColor = "#F2F2F2";
                if (document.forms[0].LossType_codelookupbtn != null)
                    document.forms[0].LossType_codelookupbtn.style.visibility = "hidden";
            }
            if (document.forms[0].DisabilityCat_codelookup != null) {
                document.forms[0].DisabilityCat_codelookup.readOnly = "readonly";
                document.forms[0].DisabilityCat_codelookup.style.backgroundColor = "#F2F2F2";
                if (document.forms[0].DisabilityCat_codelookupbtn != null)
                    document.forms[0].LossType_codelookupbtn.style.visibility = "hidden";
            }
            if (document.forms[0].DisabilityLossType_codelookup != null) {
                document.forms[0].DisabilityLossType_codelookup.readOnly = "readonly";
                document.forms[0].DisabilityLossType_codelookup.style.backgroundColor = "#F2F2F2";
                if (document.forms[0].DisabilityLossType_codelookupbtn != null)
                    document.forms[0].DisabilityLossType_codelookupbtn.style.visibility = "hidden";
            }
        }
    }
    //abahl3 ends , rma 111468,11121,11120

    if (document.getElementById("reservetypecode") != null) {
        if (window.opener.document.forms[0].ResTypeCode != null) {
            document.getElementById("reservetypecode").value = window.opener.document.forms[0].ResTypeCode.value;
        }
    }

    
    if (document.forms[0].splitrowid != null) {

        if (((document.forms[0].splitrowid.value != "") && (Number( document.forms[0].splitrowid.value ) > "0"))) {
            if (document.forms[0].ReserveTypeCodeFt_codelookup != null) {
                document.forms[0].ReserveTypeCodeFt_codelookup.readOnly = "readonly";
                document.forms[0].ReserveTypeCodeFt_codelookup.style.backgroundColor = "#F2F2F2";
                
            }
            if (document.forms[0].ReserveTypeCodeFt_codelookupbtn != null)
                document.forms[0].ReserveTypeCodeFt_codelookupbtn.style.visibility = "hidden";

            if (document.forms[0].Coverage_codelookup_cid != null) {
                document.forms[0].Coverage_codelookup_cid.readonly = "readonly";
                document.forms[0].Coverage_codelookup_cid.color = "#ffffff";
            }
            if (document.forms[0].Coverage_codelookup != null) {
                document.forms[0].Coverage_codelookup.readOnly = "readonly";
                document.forms[0].Coverage_codelookup.style.backgroundColor = "#F2F2F2";
            }
            if (document.forms[0].Coverage_codelookupbtn != null)
                document.forms[0].Coverage_codelookupbtn.style.visibility = "hidden";
            if (document.forms[0].Policy != null)
                document.forms[0].Policy.disabled = true;
            if (document.forms[0].FFPayment != null)
                document.forms[0].FFPayment.disabled = true;
            if (document.forms[0].Unit != null)
                document.forms[0].Unit.disabled = true;

            if (document.forms[0].LossType_codelookup != null) {
                document.forms[0].LossType_codelookup.readOnly = "readonly";
                document.forms[0].LossType_codelookup.style.backgroundColor = "#F2F2F2";
                if (document.forms[0].LossType_codelookupbtn != null)
                    document.forms[0].LossType_codelookupbtn.style.visibility = "hidden";
            }
            if (document.forms[0].DisabilityCat_codelookup != null) {
                document.forms[0].DisabilityCat_codelookup.readOnly = "readonly";
                document.forms[0].DisabilityCat_codelookup.style.backgroundColor = "#F2F2F2";
                if (document.forms[0].DisabilityCat_codelookupbtn != null)
                    document.forms[0].LossType_codelookupbtn.style.visibility = "hidden";
            }
            if (document.forms[0].DisabilityLossType_codelookup != null) {
                document.forms[0].DisabilityLossType_codelookup.readOnly = "readonly";
                document.forms[0].DisabilityLossType_codelookup.style.backgroundColor = "#F2F2F2";
                if (document.forms[0].DisabilityLossType_codelookupbtn != null)
                    document.forms[0].DisabilityLossType_codelookupbtn.style.visibility = "hidden";
            }
        }
    }
    checkFee(document.forms[0].discschd);
    checkFee(document.forms[0].discschd2);
    checkFee(document.forms[0].discschdbill);
    setDataChanged(false);

    //This means it is time to return to the calling page after stashing or clearing 
    // xml data in session.
    if (document.forms[0].amounttopay != null) 
    {
        m_OrigAmtToPay = document.forms[0].amounttopay.value;
    }
    if (haveProperty(document.forms[0], "cmd"))
        if (document.forms[0].cmd.value == "exit")
            CancelForm();


    //Fix for Beta issue from MHA where screen must be repeatedly scrolled after calculation.
    var oAmountBilled = document.getElementById("amountbilled");
    if (oAmountBilled != null) 
    {
        //         if (ToCurrency(oAmountBilled.value) > 0)
        //         {
        //             if (!window.document.layers)
        //             {
        //                 tabChange("Calculate");
        //                 if (document.forms[0].btnReturn != null)
        //                 {
        //                     document.forms[0].btnReturn.focus();
        //                 }
        //             }
        //         }
        //         else
        //         {
        //             if (!window.document.layers)
        //             {
        //                 tabChange("BillItem");
        //             }
        //         }
    }

    //For override step, show override tab
    if (haveProperty(document.forms[0], "step")){
        if (document.forms[0].step.value == "7") 
        {
            tabChange("overridegroup");
        }
    }

    for (var i = 0; i < document.getElementsByTagName("A").length; i++)
     {
         if (document.getElementsByTagName("A")[i].name == "override")
         {
            pageLoadedBrsOverride();
            break;
        }
    }
    for (var i = 0; i < document.getElementsByTagName("DIV").length; i++)
     {
         if (document.getElementsByTagName("DIV")[i].className == "errtextheader")
         {
             if ((document.getElementsByTagName("DIV")[i].innerText || document.getElementsByTagName("DIV")[i].textContent) != "") 
            {
                if (!window.document.layers)
                    document.getElementsByTagName("DIV")[i].scrollIntoView();
                break;
            }
        }
    }
    var objctl = eval(document.forms[0].CmboSpecCds);
    if (objctl != null && objctl.value.length > 0) 
    {
        window.open('SpecialtyCode.aspx',
                        'SpecialtyCode', 'width=500,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    }
}

function pageLoadedBrsOverride() 
{

    tabChange("override");
    var frm = document.forms[0];
    // Rounding For Presentation
    frm.ovr_billedamount.value = ToCurrency(frm.ovr_billedamount.value);
    frm.ovr_discbilledamount.value = ToCurrency(frm.ovr_discbilledamount.value);
    frm.ovr_contractamount.value = ToCurrency(frm.ovr_contractamount.value);
    frm.ovr_disccontractamount.value = ToCurrency(frm.ovr_disccontractamount.value);
    frm.ovr_perdiemamount.value = ToCurrency(frm.ovr_perdiemamount.value);
    frm.ovr_perdiemstoplossamount.value = ToCurrency(frm.ovr_perdiemstoplossamount.value);
    frm.ovr_schedamount.value = ToCurrency(frm.ovr_schedamount.value);
    frm.ovr_discschedamount.value = ToCurrency(frm.ovr_discschedamount.value);
   
    //Apply Pre-Select(ed by dll) Lowest amount.
    selectOption(document.forms[0].override.value)
    //for(var i=0;i<document.forms[0].override.length;i++)
    //	if(document.forms[0].override[i].checked)
    //	{
    //		selectOption(document.forms[0].override[i].value);     //this is lowest amount to return
    //		break;
    //	}
    setDataChanged(false);
}

function checkFee(ctl) 
{
    if (ctl == null) return false;

    var b = (ctl.checked);
    switch (ctl.id)
     {
        case 'discschd':
            /*
			if(!b){
				document.forms[0].dpercentage.value=0;
				document.forms[0].dfactor.value=0;
				document.forms[0].feesched.selectedindex=-1;
				document.forms[0].statefee.selectedindex=-1;
			}
			*/
            document.forms[0].dpercentage.disabled = !b;
            document.forms[0].dfactor.disabled = !b;
            document.forms[0].feesched.disabled = !b;
            document.forms[0].statefee.disabled = !b;
            break;
        case 'discschd2':
            /*
			if(!b){
				document.forms[0].dpercentage2.value=0;
				document.forms[0].dfactor2.value=0;
				document.forms[0].feesched2.selectedindex=-1;
			}
			*/
            document.forms[0].dpercentage2.disabled = !b;
            document.forms[0].dfactor2.disabled = !b;
            document.forms[0].feesched2.disabled = !b;
            break;
        case 'discschdbill':
            /*
			if(!b){
				document.forms[0].dpercentagebill.value=0;
				document.forms[0].dfactor.value=0;
			}
			*/
            document.forms[0].dpercentagebill.disabled = !b;
            document.forms[0].dfactorbill.disabled = !b;
            break;
        default:
            alert('error, ' + ctl.name + ' unknown');
            break;
    }

    setDataChanged(true);
}


function changeDiscount(ctl, ctl2) 
{
    //each pair of controls must add up to 100.
    //note:  "factor" is a meaningless field copied from rmworld.  it is 100 minus the percentage and only exists on interface (not stored in database)
    var dbl = ctl.value;
    var dbl2 = 0;

    if (dbl > 100){
        alert('100% is maximum possible value');
        ctl.value = 100.0000;
        dbl = 100;
    }

    dbl2 = 100 - dbl;

    ctl2.value = dbl2.toFixed(2);
    numLostFocus(ctl2);
    setDataChanged(true);

}

function gotoStep(iStep)
 {
     switch (iStep) 
    {
        case 1:
            if (!ValidateDates(document.forms[0].all["dfromdate"].value, document.forms[0].all["dtodate"].value)) 
            {
                return false;
            }
            break;
        case 2:
            if (document.forms[0].zip.value == '') 
            {
                alert("Please enter zip code first.");
                return false;
            }
            break;
    }

    pleaseWait.Show('start');
    return true;
}


function ValidateDates(p_sFromDate, p_sToDate)
 {
     if (p_sFromDate == '' || p_sToDate == '')
     {
        alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
        if (p_sFromDate == '')
            document.forms[0].dfromdate.focus();
        else
            document.forms[0].dtodate.focus();
        return false;
    }
    var sFromDate = new Date(p_sFromDate)
    var sToDate = new Date(p_sToDate)

    var sFromDateValue = sFromDate.toDateString();
    var sToDateValue = sToDate.toDateString();

    if (sToDate < sFromDate) 
    {
        alert("To Date can not be less than From Date.")
        return false;
    }
    return true;
}

function CancelForm() 
{
    //alert("cancelling brs form");
    if (self.opener != null && self.opener != self)
        self.close();
    else
        self.setTimeout("urlNavigate('trans.asp?transid=" + document.forms[0].transid.value + "&claimid=" + document.forms[0].claimid.value + "')", 1);
    return true;
}

function urlNavigate(sUrl)
 {
    return RMX.urlNavigate(sUrl);
}

function BillingCode(ctl) 
{
    var ctlID = ctl.id;
    if (ctlID.substring(ctlID.length - 3) == "btn")
        ctlID = ctlID.substring(0, ctlID.length - 3);

    if (m_codeWindow != null)
        m_codeWindow.close();
    var find = "";
    var billingcodeCtrl = document.getElementById(ctlID);
    if (billingcodeCtrl != null)
        find = billingcodeCtrl.value + '';
    if (find != "")
        find = escape(find);

    var ddlFeeTable = document.forms[0].ddlFeeTable;
    var feesched = 0;
    if( ddlFeeTable.selectedIndex >= 0)
    {
    	feesched = ddlFeeTable.options[ddlFeeTable.selectedIndex].value + '';
    }
    var feesched2 = document.forms[0].feesched2.value + '';
    m_sFieldName = ctlID;
    m_codeWindow = window.open("BillingCodeLookup.aspx?feescheduleid=" + feesched + "&feescheduleid2=" + feesched2 + "&find=" + find, 'codeWnd',
              'width=500,height=290' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
   
    return false;
}


function ClearBillingCodeInput() 
{
    var obj = eval("document.forms[0]." + m_sFieldName);
    obj.value = '';
}


function billingcodeLostFocus(ctl) 
{
    var s = ctl.value;
    if (s == null || s == '')
        return false;

    var ddlFeeTable = document.forms[0].ddlFeeTable;
    if (ddlFeeTable.selectedIndex >= 0)
        BillingCode(ctl);
}

function billingCodeSelected(sCodeText, CodeId)
 {
    var ctl = document.getElementById(m_sFieldName);
    if (ctl != null)
        ctl.value = unescape(sCodeText);
    ctl = document.getElementById(m_sFieldName + "_cid");
    if (ctl != null)
        ctl.value = CodeId;
    if (m_codeWindow != null)
        m_codeWindow.close();
}

function selBillingCode(sCodeText, lCodeId)
 {

     if (lCodeId != 0) 
    {
        // Put the code on the calling form+close this one
        //alert('getcode.js selCode()');
        //document.forms[0].bUnload.value = 'true';		
        //window.opener.document.codeSelected(sCodeText,lCodeId);
        window.opener.billingCodeSelected(sCodeText, lCodeId);
    }
    return false;
}

function handleBillingcodeUnload() 
{
    //alert('getcode.js handleUnload()');
    if (window.opener != null) 
    {
        //TR 4/11/02 do not unload if moving from codepage to codepage
        if (document.forms[0].bUnload.value == true)
         {
            window.opener.document.onCodeClose();
        }
    }
    return true;
}

function Calculate()
 {
    document.forms[0].step.value = 3;
    document.forms[0].overrideoption.value = 0; //Cause override to re-display each time calculation is requested...
   
    var amountbilledTemp = ToCurrency(document.forms[0].amountbilled.value)
    if (amountbilledTemp == 0.00 || amountbilledTemp == 0) 
    {
        document.forms[0].amountbilled.value = ''
    }
    if (validateForm()) 
    {
        pleaseWait.Show('start');
        m_DataChanged = false;
        return true;
    }
    else 
    {
        return false;
    }

}

function ToCurrency(val) 
{

    var dbl = new String(val);
    // Strip and Validate Input
    // BSB Remove commas first so parseFloat doesn't incorrectly truncate value.
    dbl = dbl.replace(/[,\$]/g, "");

    if (dbl.length == 0)
        return 0;

    //MITS 23070 handle negative values
    var sNegativeFormat = /\(\d+.{0,1}\d{0,2}\)/;
    var re = new RegExp(sNegativeFormat);
    if (dbl.match(re))
     {
        dbl = dbl.replace("(", "").replace(")", "");
        dbl = "-" + dbl;
    }
    if (isNaN(parseFloat(dbl)))
        return 0;

    //round to 2 places
    dbl = parseFloat(dbl);
    dbl = dbl.toFixed(2);
    return dbl;
}
function Reduce(ForceByPercentage) 
{
    var dblReduce;
    //caggarwal4 Merged for issue RMA-9115 starts
    var iCalcType = document.frmData.calctype.value;
    if (!ForceByPercentage) 
    {
        dblReduce = ToCurrency(document.forms[0].amountreduced.value);
        document.forms[0].amountreducedpercent.value = "";
    } else 
    {
        dblReduce = document.forms[0].amountreducedpercent.value;
        document.forms[0].amountreduced.value = "";
    }

    if (dblReduce == 0) 
    {
        document.forms[0].amountreduced.value = "";
        document.forms[0].amountreducedpercent.value = "";
        document.forms[0].amountsaved.value = "";
        document.forms[0].amounttopay.value = m_OrigAmtToPay
        return false;
    }

    var dblAmtAllowed = ToCurrency(document.forms[0].amountallowed.value);
    var dblAmtBilled = ToCurrency(document.forms[0].amountbilled.value);
    var dblAmtToPay = ToCurrency(document.forms[0].amounttopay.value);

    // Calculation may not yeild a value but user should be allowed to continue.
    // This is indicated by BRS.Net returning "unlockcalc" in the cmd form value.
    var IsCalcUnlocked = (document.forms[0].cmd.value == "unlockcalc");
    if (dblAmtToPay <= 0 && !IsCalcUnlocked && (iCalcType != 18))  //caggarwal4 Merged for issue RMA-9115 
    {
        alert("A successfull calculation must be performed before a manual reduction can be applied.");
        document.forms[0].amountreduced.value = "";
        return false;
    }

    var dblAmtToPay;
    //caggarwal4 Merged for issue RMA-9115 starts
 
    if (!ForceByPercentage) {
        if (iCalcType == 18) {
            dblAmtToPay = ToCurrency(dblAmtBilled - dblReduce);
        }
        else {
            dblAmtToPay = ToCurrency(Math.min(dblAmtAllowed, dblAmtBilled) - dblReduce);
        }
    }
    else {
        if (iCalcType == 18) {
            dblAmtToPay = ToCurrency(dblAmtBilled * ((100 - dblReduce) / 100));
        }
        else {
            dblAmtToPay = ToCurrency(Math.min(dblAmtAllowed, dblAmtBilled) * ((100 - dblReduce) / 100));
        }
    }
    //caggarwal4 ends
    document.forms[0].amountsaved.value = ToCurrency(dblAmtBilled - dblAmtToPay);
    document.forms[0].amounttopay.value = dblAmtToPay;
    currencyLostFocus(document.forms[0].amounttopay);
    document.forms[0].amountreduced.value = ToCurrency(Math.min(dblAmtAllowed, dblAmtBilled) - dblAmtToPay);
    currencyLostFocus(document.forms[0].amountreduced);
    //document.forms[0].amountreducedpercent.value=""; //we don't store this percentage so we just dump it back to zero.
   
    return false;
}

function selectOption(iValue) 
{
    var i = 0;
    var frm = document.forms[0];
    //alert(iValue);
    frm.overrideoption.value = iValue;
    switch (parseInt(iValue)) 
    {
        case 0:
            frm.overrideamount.value = ToCurrency(frm.ovr_billedamount.value);
            break;
        case 1:
            frm.overrideamount.value = ToCurrency(frm.ovr_discbilledamount.value);
            break;
        case 2:
            frm.overrideamount.value = ToCurrency(frm.ovr_contractamount.value);
            break;
        case 3:
            frm.overrideamount.value = ToCurrency(frm.ovr_disccontractamount.value);
            break;
        case 4:
            frm.overrideamount.value = ToCurrency(frm.ovr_perdiemamount.value);
            break;
        case 5:
            frm.overrideamount.value = ToCurrency(frm.ovr_perdiemstoplossamount.value);
            break;
        case 6:
            frm.overrideamount.value = ToCurrency(frm.ovr_schedamount.value);
            break;
        case 7:
            frm.overrideamount.value = ToCurrency(frm.ovr_discschedamount.value);
            break;
    }
    return true;
}

function OnReturnClicked()
 {
    var amountbilledTemp = ToCurrency(document.forms[0].amountbilled.value)
    if (amountbilledTemp == 0.00 || amountbilledTemp == 0)
     {
        document.forms[0].amountbilled.value = ''
    }

    //Check if the current item is still in the process of being created and if all the
    //required fields have values	
    var sAddItemInProgress = document.forms[0].AddItemInProgress.value;
    if (sAddItemInProgress == 'true') 
    {
        //Check if all the required fiels have values. If not, ask user whether the current item 
        //should be cancelled
        if (!validateForm(true)) 
        {
            if (confirm('Not all required fields have values. Cancel current bill item and return previous bill items entered ?')) 
            {
                document.forms[0].AddCurrentBillItem.value = 'false';
                return true;
            }
            else
             {
                //Anwser no to Cancel current item, stay in the current item
                return false;
            }
        }
    }

    if (!validateForm())
     {
        return false;
    }
    

    if (!IsPaymentCalculated()) 
    {
        alert("You must complete at least one calculation to return an amount.  To close the window without calculation press 'Cancel'.");
        {
            return false;
        }
    }


    //Mona: preventing duplication of BRS splits    
        document.forms[0].PostbackAction.value = "Return";
    var oSysPageDataChanged = document.getElementById("SysPageDataChanged");
    if (oSysPageDataChanged != null)
     {
        oSysPageDataChanged.value = '';
    }

    pleaseWait.Show('start');
    return true;
}


//Navigation functionalitis:
//1. The navigation buttons will only be visible for Add/Clone mode
//2. If there is only one item in the list (the first added item, stay in this item without warning message)
//3. when click Move Previous/First button and not all required fields have values, popup meesage
//   and ask user if they want to cancel the current item (Creation of the current item is not done yet)
//   If anwser yes to cancel current item, remove the current item and go to the requested item; otherwise
//   stay in the current item
//4. When click Move Next/Last button and not all required fields have values, popup meesage
//   and ask user if they want to cancel the current item (Creation of the current item is not done yet)
//   If anwser yes to cancel current item, remove the current item and go to the last finished item; otherwise
//   stay in the current item
function OnNextBillItem() 
{
    var iCurrentRowPosition = document.forms[0].selectedrowposition.value;
    var iMoveToRowPosition = parseInt(iCurrentRowPosition) + 1;

    return MoveToBillItem(iMoveToRowPosition);
}

function OnLastBillItem() 
{
    //If already in the last row, don't try to do postback
    var iLastRowPosition = document.forms[0].lastrowposition.value;
    return MoveToBillItem(iLastRowPosition);
}

function OnPreviousBillItem()
 {
    var iCurrentRowPosition = document.forms[0].selectedrowposition.value;
    var iMoveToRowPosition = parseInt(iCurrentRowPosition) - 1;
    if (parseInt(iMoveToRowPosition) <= 1)
        return false;

    return MoveToBillItem(iMoveToRowPosition);
}


function OnFirstBillItem() 
{
    //If already in the first row, don't try to do postback
    var iCurrentRowPosition = document.forms[0].selectedrowposition.value;
    if (iCurrentRowPosition == 2)
        return false;

    return MoveToBillItem(2);
}

//will be called for previous/next/first/last bill item buttons
function MoveToBillItem(iMoveToRowPosition)
 {
     try
     {
         var iLastRowPosition = document.forms[0].lastrowposition.value;

        //If there is only one item in the list, do nothing
        if (iLastRowPosition == 2)
            return false;

        //Check if the current item is still in the process of being created and if all the
        //required fields have values	
        var sAddItemInProgress = document.forms[0].AddItemInProgress.value;
        if (sAddItemInProgress == 'true')
         {
            //Check if all the required fiels have values. If not, ask user whether the current item 
            //should be cancelled
             if (!validateForm(true)) 
            {
                if (confirm('Not all required fields have values. Cancel current bill item creation ?'))
                 {
                    document.forms[0].AddCurrentBillItem.value = 'false';
                    return true;
                }
                else 
                {
                    //Anwser no to Cancel current item, stay in the current item
                    return false;
                }
            }
        }

        //If user removes values for some required fields for an existing item, should not continue 
        //navigating to other item
        if (!validateForm()) 
        {
            return false;
        }

        if (parseInt(iMoveToRowPosition) > parseInt(iLastRowPosition))
            return false;

        return true;
    }
    catch (ex)
     {
        alert(ex.message);
        return false;
    }
}

function OkClose() 
{
    if (self.opener != null && self.opener != self) //Popped-up (used to avoid premature save prompt.)
    {
        if (haveProperty(self.opener.document, "callback"))
            eval("self.opener." + self.opener.document.callback + "()");
        self.close();
    }
}
//BRS FL Merge : Umesh
function ValidateNumericField(ctl) 
{    var title = "";
    switch (ctl.id)
     {
        case "RevenueCode":
            title = "Revenue Code"
            break;
        case "RefNo":
            title = "Ref. No."
            break;

    }

    if (ctl.value != "")
     {
         if (isNaN(parseInt(ctl.value)))
         {
            alert("Please enter numeric value in " + title);
            ctl.focus();
            return false;
        }
    }
}
//BRS FL Merge : End

function SpecCodePageLoaded() 
{
    var objctl = eval(document.forms[0].CmboSpecCds);
    var objctlparent = eval(window.opener.document.forms[0].CmboSpecCds);
    var sCmboSpecCds = objctlparent.value;
    var arSpecCodes = sCmboSpecCds.split("$%#");

    for (var i = 0; i < arSpecCodes.length; i++) 
    {
        var objOption = new Option(arSpecCodes[i], i);
        objctl.options[i] = objOption;
    }
    return true;
}

function ReturnSpecCode() 
{
    var sSpecCode = "";
    var sSpecCodeText = "";
    var iIndex = -1;
    var iIndex2 = -1;
    var iIndex3 = -1;

    var objctl = eval(document.forms[0].CmboSpecCds);
    objctl = objctl.options[objctl.selectedIndex];
    sSpecCodeText = objctl.text;
    iIndex = sSpecCodeText.indexOf(" ");
    // MITS 12139 Umesh
    iIndexNoSpec = sSpecCodeText.indexOf("No Specialty");

    if (iIndexNoSpec > 0) 
    {
        sSpecCode = "";
    }
    else
     {
        iIndex2 = sSpecCodeText.indexOf(" ", iIndex + 1);
        iIndex3 = sSpecCodeText.indexOf(" ", iIndex2 + 1);
        sSpecCode = sSpecCodeText.substring(iIndex2, iIndex3);
    }

    sSpecCodeText = sSpecCodeText.substring(0, iIndex);
    window.opener.document.getElementById('CmboSpecCdsAmount').value = sSpecCodeText;
    window.opener.document.getElementById('Specialty').value = sSpecCode;
    var objCalculate = window.opener.document.getElementById('btnCalculate');
    objCalculate.click();
    window.close(); 
}


// Updates the hidden column on the selected grid row/new row with the values of controls on the pop-up window
function SaveDataBack() 
{
    var Gridname = document.getElementById('gridname').value + '_';
    var SelectedRowPosition = document.getElementById('selectedrowposition').value;
    var mode = document.getElementById('mode').value;

    var objParentDocument = window.opener.document;
    var objParentDocumentForm = window.opener.document.forms[0];
    if (objParentDocument == null)
     {
        alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
        //return false;
    }

    //For new brs item, save data back to hidden field 
    var objData = objParentDocument.getElementById("FundsBRSSplitsGrid_Data");
    if (objData != null) 
    {
        objData.value = document.getElementById('txtData').value;
        var objSysBRSPostback = objParentDocument.getElementById("SysSplitPostback");
        if (objSysBRSPostback != null)
         {
            objSysBRSPostback.value = "true";
        }

        window.opener.pleaseWait.Show('Start');
        objParentDocumentForm.SysCmd.value = 7;
        objParentDocumentForm.submit();
        
        window.close();
    }

    return false;


    //    if (objParentDocumentForm == null)
    //    {
    //        alert("Error. Unable to access parent window document form. Please contact your System Administrator. ");
    //        //return false;
    //    }
    //    if (SelectedRowPosition.length == 1)
    //        SelectedRowPosition = '0' + SelectedRowPosition;
    //    var ParentElementId = Gridname + 'gvData_ctl' + SelectedRowPosition + '_|' + SelectedRowPosition + '|Data';
    //    var ParentElement = objParentDocument.getElementById(ParentElementId);
    //    if (ParentElement != null)
    //    {
    //        ParentElement.value = document.getElementById('txtData').value;
    //        var objGridRowAddedFlag = objParentDocument.getElementById(Gridname + "RowAddedFlag");
    //        var objGridRowAction = objParentDocument.getElementById(Gridname + "Action");
    //        if (objGridRowAddedFlag == null)
    //        {
    //            alert("Error. Unable to access RowAddedFlag on parent window document. Please contact your System Administrator. ");
    //            //return false;
    //        }
    //        if (objGridRowAction == null)
    //        {
    //            alert("Error. Unable to access Action on parent window document. Please contact your System Administrator. ");
    //            //return false;
    //        }
    //        if (objParentDocumentForm.hdPostBack != null)
    //            objParentDocumentForm.hdPostBack.value = "1";
    //        objGridRowAddedFlag.value = "true";
    //        objGridRowAction.value = mode;

    //        //Set hidden field main page DataChanged to true
    //        if (window.opener.setDataChanged != null)
    //        {
    //            window.opener.setDataChanged(true);
    //        }
    //        var oSysPagePostbackDone = window.opener.document.getElementById("SysPagePostbackDone");
    //        oSysPagePostbackDone.value = '';

    //        objParentDocumentForm.SysCmd.value = 7;
    //        objParentDocumentForm.submit();

    //        window.opener.pleaseWait.Show('Start');

    //        //start please wait for this page and stop it based on parent page's pastbackDone flag
    //        pleaseWait.Show('start');
    //        window.setTimeout("CheckParentPagePostbackStatus()", 1000);

    //        //hide the toolbar to prevent moving away from the current item before saving is done
    //        window.toolbardrift.style.display = 'none';
    //        
    //        //increase the last row position by 1 if a new item is saved
    //        var iLastRowPosition = document.forms[0].lastrowposition.value;
    //        document.forms[0].lastrowposition.value = parseInt(iLastRowPosition) + 1;

    //        //return true;
    //    }
    //    else
    //    {
    //        alert("Error. Unable to access parent window document. Please contact your System Administrator. ");
    //        //return false;
    //    }
}


//Check if the parent page's postback caused by the this page's saving action is done or not
function CheckParentPagePostbackStatus()
 {
    var oSysPagePostbackDone = window.opener.document.getElementById("SysPagePostbackDone");
    if (oSysPagePostbackDone != null)
     {
         if (oSysPagePostbackDone.value == 'true')
         {
            window.toolbardrift.style.display = '';
            pleaseWait.pleaseWait('stop');
            return;
        }
    }

    window.setTimeout("CheckParentPagePostbackStatus()", 1000);
}

//Check if the payment has been calculated
function IsPaymentCalculated() 
{
    //There would be no calculation Button if calctype = 20 i.e. FL Hospital
    var iCalcType = document.forms[0].calctype.value;
    if ((iCalcType != 20) && (iCalcType != 18))  //caggarwal4 Merged for Issue RMA-9115
     {
        var AmountAllowedTemp = ToCurrency(document.forms[0].amountallowed.value);
        // Calculation may not yeild a value but user should be allowed to continue.
        // This is indicated by BRS.Net returning "unlockcalc" in the cmd form value.
      
        //var IsCalcUnlocked = (document.forms[0].cmd.value=="unlockcalc");
        var sIsCalculated = document.forms[0].calculated.value;
        var sBrsMode = document.forms[0].brsmode.value
      
        /*Need to talk to Brian for the following if condition -Tanuj*/
        if (sIsCalculated != 'true' && sBrsMode == 'add') 
        {
            return false;
        }
    }

    return true;
}


function OnNewBillItem()
 {
    //Check if the amount has been calculated
     if (!IsPaymentCalculated()) 
    {
        alert("You must complete at least one calculation before adding another item.");
        {
            return false;
        }
    }

    if (validateForm()) {
        //Mona: preventing duplication of BRS splits
        document.forms[0].PostbackAction.value = "New";

        pleaseWait.Show('start');
        m_DataChanged = false;
        return true;
    }
    else
     {
        return false;
    }
}


function CheckPageDataChanged() 
{
    //Check hidden field SysPageDataChanged to see if the data have been saved
    var oSysPageDataChanged = document.getElementById("SysPageDataChanged");
    if (oSysPageDataChanged != null) 
    {
        if (oSysPageDataChanged.value == 'true') 
        {
            return true;
        }
    }

    return false;
}


function GetLastPosition() 
{
    var objParentDocument = opener.window.document;
    var ParentElement = null;
    var Gridname = 'FundsBRSSplitsGrid_';

    //Get the last valid Row position which will be used to enable/disable navigation buttons
    var iLastRowPosition = 2;
    var sRowPosition = '';
    var ParentElementId = '';
    while (true) 
    {
        if (iLastRowPosition < 10) 
        {
            sRowPosition = '0' + iLastRowPosition;
        }
        ParentElementId = Gridname + 'gvData_ctl' + sRowPosition + '_|' + sRowPosition + '|Data';
        ParentElement = objParentDocument.getElementById(ParentElementId);
        if (ParentElement == null)
         {
            //document.getElementById('lastrowposition').value = iLastRowPosition - 2;
            break;
        }
        iLastRowPosition++;
    }

    return iLastRowPosition - 2;
}

// Added by Amitosh for MITS 18528 : Start

// Function pageloadedPiContract() will be called in case of physician contract only.
// Funtion pageloadedPiContract() removes the unwanted functionality of pageLoadedBrs()
// which was displaying dirty pencil at the page load of providercontract.aspx.

function pageloadedDiContract() 
{
    checkFeeDiContract(document.forms[0].discschd);
    checkFeeDiContract(document.forms[0].discschd2);
    checkFeeDiContract(document.forms[0].discschdbill);

    setDataChanged(false);
}


function checkFeeDiContract(ctl) 
{
    if (ctl == null) return false;

    var b = (ctl.checked);
    switch (ctl.id)
     {
        case 'discschd':
            document.forms[0].dpercentage.disabled = !b;
            document.forms[0].dfactor.disabled = !b;
            document.forms[0].feesched.disabled = !b;
            document.forms[0].statefee.disabled = !b;
            break;
        case 'discschd2':
            document.forms[0].dpercentage2.disabled = !b;
            document.forms[0].dfactor2.disabled = !b;
            document.forms[0].feesched2.disabled = !b;
            break;
        case 'discschdbill':
            document.forms[0].dpercentagebill.disabled = !b;
            document.forms[0].dfactorbill.disabled = !b;
            break;
        default:
            alert('error, ' + ctl.name + ' unknown');
            break;
    }
}
// Added by Amitosh for MITS 18528 : End

function EnableCriteria(bEnable)
{
    var ctrlchkDupBRSSplits = document.getElementById("chkDupBRSSplits");
    var bEnable= false;
    if (ctrlchkDupBRSSplits != null) {
        bEnable = ctrlchkDupBRSSplits.checked;
    }
    var ctrlDupCriteria = document.getElementById("DupCriteria");
    if (ctrlDupCriteria != null)
    {
        ctrlDupCriteria.disabled  = !bEnable;
    }
}

// mkaran2 - Start - MITS #28419
function showBRSSummary() {    
    var dateofservice;
    var procedurecode;
    var billedamt;
    var paidamt;
    var savedamt;

    if (document.forms[0].dfromdate.value != null && document.forms[0].dfromdate.value != "" ) {
        dateofservice = document.forms[0].dfromdate.value;
    }

    if (document.getElementById("billingcode").value != null && document.getElementById("billingcode").value !="") {
        procedurecode = document.getElementById("billingcode").value;
    }

    if (document.getElementById("amountbilled").value != null && document.getElementById("amountbilled").value!= "") {
        billedamt = document.getElementById("amountbilled").value;
    }

    if (document.getElementById("amounttopay").value != null && document.getElementById("amounttopay").value != "") {
        paidamt = document.getElementById("amounttopay").value;
    }

    if (document.getElementById("amountsaved").value != null && document.getElementById("amountsaved").value != "") {
        savedamt = document.getElementById("amountsaved").value;
    }   
   
    window.open('BRSSummaryList.aspx?dateofservice=' + dateofservice + "&procedurecode=" + procedurecode + "&billedamt=" + billedamt + "&paidamt=" + paidamt + "&savedamt=" + savedamt,
    'BRSSummaryList', 'width=700,height=300' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');

    return false;
}
function SelectClose() {
    self.close();
    return false;
}
function LoadedBrsSummary() {
    if (window.opener.document.getElementById('hdnBRSSummary') && window.opener.document.getElementById('hdnBRSSummary').value == "")
        return;
    if (window.opener.document.getElementById('hdnBRSSummary') != null && document.getElementById("hdnBRSSummaryList") != null && document.getElementById("hdnBRSSummaryList").value == "") {
        document.getElementById("hdnBRSSummaryList").value = window.opener.document.getElementById('hdnBRSSummary').value;
        //document.forms[0].submit();
        window.opener.document.submit();
    }
    if (document.getElementById("hdnBRSSummaryList") && document.getElementById("hdnBRSSummaryList").value == "") {
        window.opener.document.submit();
    }
    return;
}     
// mkaran2 - End - MITS #28419