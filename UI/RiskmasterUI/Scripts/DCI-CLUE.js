﻿// <summary>
// Script for adding multiple search result to listbox.(Used on DCI/CLUE TM Optionset Screen)
// Added by agupta298.
// </summary>

function DeleteSelectedClaim(sListBoxFieldName, sTextBoxFieldName) {
    var objCtrl = null;
    if (m_codeWindow != null)
        m_codeWindow.close();
    objCtrl = eval("document.forms[0]." + sListBoxFieldName);
    if (objCtrl == null)
        return false;
    if (objCtrl.selectedIndex < 0)
        return false;

    var bRepeat = true;
    while (bRepeat) {
        bRepeat = false;
        for (var i = 0; i < objCtrl.length; i++) {
            // remove selected elements
            if (objCtrl.options[i].selected) {
                objCtrl.options[i] = null;
                bRepeat = true;
                break;
            }
        }
    }
    //Now create ids list
    var sId = "";
    for (var i = 0; i < objCtrl.length; i++) {
        if (sId != "")
            sId = sId + ",";
        sId = sId + objCtrl.options[i].value;
    }
    objCtrl = null;
    objCtrl = eval("document.forms[0]." + sTextBoxFieldName);
    if (objCtrl != null)
        objCtrl.value = sId;

    return true;
}

function AddClaimNumber(sClaimNumber, sListBoxFieldName, sTextBoxFieldName) {

    var objCtrl = null;
    objCtrl = eval("document.forms[0]." + sListBoxFieldName);
    if (objCtrl.type == "select-one" || objCtrl.type == "select-multiple") {
        var bAdd = true;
        for (var i = 0; i < objCtrl.length; i++) {
            if (objCtrl.options[i].value == sClaimNumber)
                bAdd = false;
        }

        if (bAdd) {
            var objOption = new Option(sClaimNumber, sClaimNumber, false, false);
            if (objCtrl.options[0] != null && objCtrl.options[0].value == 0) {
                objCtrl.options[0] = null;
            }
            objCtrl.options[objCtrl.length] = objOption;

            objCtrl = null;
            objCtrl = eval("document.forms[0]." + sTextBoxFieldName);
            if (objCtrl != null) {
                if (objCtrl.value != "" && objCtrl.value.substring(objCtrl.value.length, 1) != ",")
                    objCtrl.value = objCtrl.value + ",";
                objCtrl.value = objCtrl.value + sClaimNumber;
            }
        }
    }
}


//validation function for comma separated Email IDs 
function MultipleEmailLostFocus(objCtrl) {
    var IsValid = false;

    if (objCtrl.value.length == 0)
        return false;

    var arr = objCtrl.value.split(",");

    if (arr.length > 0) {
        for (var i = 0; i < arr.length; i++) {
            IsValid = emailLostFocus(arr[i]);
            if (IsValid == false) {
                objCtrl.value = '';
                return false;
            }
        }
    }
    else {
        objCtrl.value = '';
        return false;
    }
    return true;
}