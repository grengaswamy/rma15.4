parent.CommonValidations = parent.parent.CommonValidations;
if (parent.CommonValidations == undefined) {
    if (parent.opener != undefined) {
        if (parent.opener.parent != undefined) {
            parent.CommonValidations = parent.opener.parent.CommonValidations;
        }
    }
}
if (parent.CommonValidations == undefined) {
    if (parent.opener != undefined) {
        if (parent.opener.parent != undefined) {
            if (parent.opener.parent.parent != undefined) {
                parent.CommonValidations = parent.opener.parent.parent.CommonValidations;
            }
        }
    }
}
/*
//-- Author	:	Tanuj Narula
//-- Dated	:	10th,July 2005
//-- Purpose:	Contains client side functions.
*/
var m_codeWindow = null;
var m_DataChanged = false;
var bDefaultSelectionDB=true;
//rsolanki2 : domain authentication updates
var m_bIsDomainAuthenticationEnabled=false;
//var m_sRead= "<html>"+"<body>"+"<form name='frmData'"+"<br><br><br><br><br><br>"+"<h2 align='center'>Reading group module access permissions...</h2>"+"</form>"+"</body>"+"</html>"
if (parent.CommonValidations != null || parent.CommonValidations != undefined)
    m_sRead = parent.CommonValidations.SecurityJsHtml1
var objAJAXManager
 //This following variable replicates tree loading message page(ProgressWinLeftTreeLoad page); this is done to gain performance. Basically dont want to hit Apache for downloading this static page.
 //Trade-off between reusability and performance, performance has upper hand in here.            
//var m_sLeftTreeRead= "<html>"+"<body>"+"<form name='frmData'"+"<table align='center' width='100%' border='0'><tr><td width='100%' valign='middle' align='center'><font face='Verdana, Arial' size='4'><b>Please wait a moment...</b></font></td></tr></table>"+"</form>"+"</body>"+"</html>"
if (parent.CommonValidations != null || parent.CommonValidations != undefined)
    m_sLeftTreeRead = parent.CommonValidations.SecurityJsHtml2 + "," + parent.CommonValidations.SecurityJsHtml3
var m_ModuleGroupFlag = false;
var m_activestepindex = 0;
var m_CurrentFuncId = '';
var m_nodeDepth = 0;
var m_EntityType;
var m_IdDB = 0;
var m_nodeText = '';
var m_dsnid = 0; //abansal23: Clone for module security groups

function ChangeLoginInfo()
{
	window.open('ChangeLogin.aspx', 'ChangeLoginInfo', 'Width=440,Height=190,status=yes,top=' + (screen.availHeight-210)/2 + ',left=' + (screen.availWidth-460)/2 + '');
	return false;
}

function ClearLicenseHistory()
{
   
	window.open('ClearLicense.aspx', 'Clearlicensehistory', 'Width=420,Height=180,status=yes,top=' + (screen.availHeight-150)/2 + ',left=' + (screen.availWidth-390)/2 + '');
	return false;
	
}
function domainauthentication()
{
//    window.open('DomainAuthentication.aspx', 'DomainAuthentication', 'Width=450,Height=400,scrollbars=auto,status=yes,top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 450) / 2 + '');
    window.open('../Utilities/SSO/SSOConfig.aspx', 'DomainAuthentication', 'Width=450,Height=400,scrollbars=auto,status=yes,top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 450) / 2 + '');	
	return false;
}

function UpdateLicenses()
{
    if(!ConfirmSave())
        {
	      window.open('UpdateLicenses.aspx', 'UpdateLicenses', 'Width=420,Height=180,status=yes,top=' + (screen.availHeight-150)/2 + ',left=' + (screen.availWidth-390)/2 + '');
	    }
	    return false;
}


function SetEMailOptions()
{
  window.open('SetEmailOptions.aspx', 'SetEmailOptions', 'Width=390,Height=180,status=yes,top=' + (screen.availHeight-210)/2 + ',left=' + (screen.availWidth-460)/2 + '');
  return false;
}

function OpenDSNWizard()
{
  
   if(!ConfirmSave())
    {
	 window.open('ModifyDataSourceInformation.aspx', 'secDSN11', 'titlebar=0,Width=650,Height=400,status=yes,top=' + (screen.availHeight-400)/2 + ',left=' + (screen.availWidth-650)/2 + '');
	}
	return false;
}

function selectDate(sFieldName)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	
	m_codeWindow=window.open('csc-Theme\\riskmaster\\common\\html\\calendar.html','codeWnd',
		'width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
	return false;
}
function pageLoaded()
{
   document.onCodeClose=onCodeClose;
   document.dateSelected = dateSelected;
    //debugger;	
   //rsolanki2 : domain authentication updates
   var eleChkDomainOptionPage= document.getElementById('DomainAuthenticationOption'); 
   if (eleChkDomainOptionPage!=null) 
   {
    //alert(eleChkDomainOptionPage.value);
        var paramIsViaEdit=getQval("edit");
        if ((paramIsViaEdit+'').toLowerCase()=="true")
        {           
            var paramDomainName=getQval("selectedid");
            var eleDomainNameOption=document.getElementById('txtdomainName'); 
            if (eleDomainNameOption!=null)
                eleDomainNameOption.value=paramDomainName;
            var paramIsEnabled=getQval("enabled");            
            
            if ((paramIsEnabled+'').toLowerCase()=="enabled")
            {
                var eleIsDomainEnabled=document.getElementById('chkDomain'); 
                if (eleIsDomainEnabled!=null)
                {
                    eleIsDomainEnabled.checked=true;
                }
            }
            
        }
   }

    // start modified by ttumula2 for RMA-9316
    // changes for RMA - 9405 by sbhatnagar21
   if (window.opener != null){
       if (window.opener.parent.frames['MainFrame'].window.document.getElementById('optS3server') != null) {
           if (window.opener.parent.frames['MainFrame'].window.document.getElementById('optS3server').checked) {
               window.opener.parent.frames['MainFrame'].window.document.getElementById('ipdocpath').readOnly = true;
           }
       }
    }
// End modified by ttumula2 for RMA-9316
   var DomainElement= window.parent.frames['LeftTree'];  
   if (DomainElement!=null)
        DomainElement=DomainElement.document.getElementById('hdIsDomainAuthenticationEnabled'); ; 
   if (DomainElement!=null)
        m_bIsDomainAuthenticationEnabled = DomainElement.value;
}

//abansal23: Clone for module security groups
function EnableClone(chk)
{
    if(chk.checked == true) 
    {
        document.forms[0].lstGroups.disabled = false;
    }
    else
    {
        document.forms[0].lstGroups.disabled = true;
    }
}



function AddNewModuleGroup()
{
	try
	{
	    var bFlag = window.parent.frames['LeftTree'].GetModuleGroupFlag();
	    //abansal23: Clone for module security groups starts
	    if (m_dsnid == "")
	    {
	        if (window.parent.frames['LeftTree'].document.getElementById('txtDsnId').value != "")
	            m_dsnid = window.parent.frames['LeftTree'].document.getElementById('txtDsnId').value;
	        else if(window.parent.frames['LeftTree'].document.getElementById('DatasourceEntityDsnId').value!="")
	            m_dsnid = window.parent.frames['LeftTree'].document.getElementById('DatasourceEntityDsnId').value;
	        else
	           m_dsnid = window.parent.frames['LeftTree'].document.getElementById('ModuleGroupEntityDsnId').value;	   
	    }
	    //abansal23: Clone for module security groups ends
	 if(!ConfirmSave())
    {
		if (bFlag)
		{
		    //abansal23: Clone for module security groups
			window.open('AddNewModuleGroup.aspx?dsnid='+ m_dsnid, 'AddNewModuleGroup', 'Width=410,Height=170,status=yes,top=' + (screen.availHeight-210)/2 + ',left=' + (screen.availWidth-460)/2 + '');
		}	
			
		else
		{
			alert('Please select Module Security Groups node, any module security group or any datasource.');
			return false;
		}
		}
	}
	catch(ex)
	{}
	return false;
}
function RenameModuleGroup()
{
	try
	{
	    if(!ConfirmSave())
         {
			if (window.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value=="GrpModuleAccessPermission" )
			{
				window.open('RenameModuleGroup.aspx', 'RenameModuleGroup', 'Width=410,Height=170,status=yes,top=' + (screen.availHeight-210)/2 + ',left=' + (screen.availWidth-460)/2 + '');
			}
			else
			{
				alert('Please select a module group to rename');
				return false;
			}
		}
	}
	catch(ex)
	{}
	return false;
}
function ReLoadData()
{
	try
	{
	    setDataChanged(false)
	    if(window.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value=="GrpModuleAccessPermission")
	     {
	       //var wnd=window.open('home?pg=riskmaster/SecurityMgtSystem/ProgressWinMGrpRead','progressWnd',
			//	        'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
			  //          self.parent.wndProgress=wnd;
	       window.parent.frames['MainFrame'].location.reload()
	       return false;
	     }
	    ResetFormValues(window.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value)
		if(window.parent.frames['MainFrame'].document.getElementById('btnReset')!=null)
		window.parent.frames['MainFrame'].document.getElementById('btnReset').click()
	}
	catch(ex)
	{}
	return false;
}
function ResetFormValues(p_sSelectedEntityType)
{
	    switch(p_sSelectedEntityType)
	    {
	      case "User":
	       {
	         window.parent.frames['LeftTree'].document.getElementById('UserEntityDsnIdSelected').value=""
             window.parent.frames['MainFrame'].document.getElementById('hdSave').value = ""
             window.parent.frames['MainFrame'].document.getElementById('dsnid').value =""
	          break;
	       }
	       case "Datasource":
	       {
	          //Hack->force the reset here to get the original state of the radio buttons..
	          window.parent.frames['MainFrame'].document.getElementById('btnReset').click()
	         if(window.parent.frames['MainFrame'].document.getElementById('fsOpt').checked)
	          {
	            window.parent.frames['MainFrame'].document.getElementById("bt-docpath").disabled=true
	          }
	         else
	          {
	           window.parent.frames['MainFrame'].document.getElementById("bt-docpath").disabled=false
	          } 
	         break;
	       }
	       case "PermUser":
	       {
	       
	         window.parent.frames['LeftTree'].document.getElementById('UserPermModuleGrpSelectedId').value=""
             window.parent.frames['MainFrame'].document.getElementById('hdSave').value = ""
             window.parent.frames['MainFrame'].document.getElementById('hdSelectedModuleGrpId').value = ""
             window.parent.frames['MainFrame'].document.getElementById('btnReset').click()
             //hack-> On Clicking refresh, input boxes dont get enabled by default; so force it.
             var arrWeekDays = new Array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
             var sIp1=""
             var sip2=""
             for(iCount=0; iCount<7;iCount++)
              {
                sIp1="ip1-"+arrWeekDays[iCount]
                sIp2="ip2-"+arrWeekDays[iCount]
                if(window.parent.frames['MainFrame'].document.getElementById(sIp1).value!="")
                 {
                   window.parent.frames['MainFrame'].document.getElementById(sIp1).disabled=false
                 }
                 if(window.parent.frames['MainFrame'].document.getElementById(sIp2).value!="")
                 {
                   window.parent.frames['MainFrame'].document.getElementById(sIp2).disabled=false
                 }
              }
             //hack-> On Clicking refresh, input boxes dont get enabled by default; so force it.
             
	        break;
	       }
	       default:
	        {
	         break;
	        }
	    }
}
function dateSelected(sDay, sMonth, sYear)
{
	var objCtrl = null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	objCtrl = eval('document.forms[0].' + m_sFieldName);
	if (objCtrl != null)
	{
		sDay = new String(sDay);	
		if (sDay.length == 1)
			sDay = "0" + sDay;
			
		sMonth = new String(sMonth);
			
		if(sMonth.length == 1)
			sMonth = "0" + sMonth;
			
		sYear = new String(sYear);
		objCtrl.value = FormatDateLocal(sYear + sMonth + sDay);
	}
	m_sFieldName = "";
	return true;
}		

function FormatDateLocal(sParamDate)
{
	var sDateSeparator;
	var iDayPos = 0, iMonthPos = 0;
	var d = new Date(1999,11,22);
	var s = d.toLocaleString();
	var sRet = "";
	var sDate = new String(sParamDate);
		
	if (sDate == "")
		return "";
		
	iDayPos = s.indexOf("22");
	iMonthPos = s.indexOf("11");
	sDateSeparator = "/";
		
	if (iDayPos<iMonthPos)
		sRet = sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet = sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}
			
function TrimIt(aString) 
{
	// RETURNS INPUT ARGUMENT WITHOUT ANY LEADING OR TRAILING SPACES
	return (aString.replace(/^ +/, '')).replace(/ +$/, '');
}

function onCodeClose()
{
	m_codeWindow = null;
	return true;
}	

function DateLostFocus(sCtrlName)
{
	var sDateSeparator;
	var iDayPos = 0, iMonthPos = 0;
	var d=new Date(1999,11,22);
	var s = d.toLocaleString();
	var sRet="";
	var objFormElem = eval('document.forms[0].'+sCtrlName);
	var sDate = new String(objFormElem.value);
	var iMonth = 0, iDay = 0, iYear = 0;
	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if(sDate == "")
		return "";
	if(sDate.indexOf('/') == -1)
	{
		sDate = sDate.substr(0 , 2) + '/' + sDate.substr(2 , 2) + '/' + sDate.substr(4 , 4);
	}
	iDayPos = s.indexOf("22");
	iMonthPos = s.indexOf("11");
	sDateSeparator = "/";
	var sArr = sDate.split(sDateSeparator);
	if(sArr.length == 3)
	{
		if(isNaN(sArr[0]/2) || isNaN(sArr[1]/2) || isNaN(sArr[2]/2))
			{
				objFormElem.value = "";
				return true;
			}
			
		sArr[0] = new String(parseInt(sArr[0],10));
		sArr[1] = new String(parseInt(sArr[1],10));
		sArr[2] = new String(parseInt(sArr[2],10));

		//Convert potential 2-digit year to 4-digit year
		sArr[2] = Get4DigitYear(sArr[2]);

		// Classic leap year calculation
		if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
			monthDays[1] = 29;
		if(iDayPos<iMonthPos)
		{
			// Date should be as dd/mm/yyyy
			if(parseInt(sArr[1],10)<1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
				objFormElem.value="";
		}
		else
		{
			// Date is something like mm/dd/yyyy
			if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
				objFormElem.value="";
		}

		// Check the year
		if(parseInt(sArr[2],10)<10 || (sArr[2].length != 4 && sArr[2].length != 2))
			objFormElem.value = "";
		// If date has been accepted
		if(objFormElem.value != "")
		{
			// Format the date
			if(sArr[0].length == 1)
				sArr[0] = "0" + sArr[0];
			if(sArr[1].length == 1)
				sArr[1] = "0" + sArr[1];
			if(sArr[2].length == 2)
				sArr[2] = "19"+sArr[2];
			if(iDayPos<iMonthPos)
				objFormElem.value = FormatDateLocal(sArr[2] + sArr[1] + sArr[0]);
			else
				objFormElem.value = FormatDateLocal(sArr[2] + sArr[0] + sArr[1]);
		}
	}
	else
		objFormElem.value = "";
	return true;
}

//Convert 2-digit year to 4-digit year
function Get4DigitYear(sInputYear) {
    var sGuessedYear = sInputYear;
    var dCurrentDate = new Date();
    var iCurrentYear = dCurrentDate.getFullYear();
    var sCurrentYear = new String(iCurrentYear);

    //01/05/2010 Raman: Logic below failed for 2010 onwards
    //converting inputyear to 2 characters

    if (sInputYear.length == 1) {
        sInputYear = "0" + sInputYear;
    }

    //If the input year is not 4-digit year, get missing digit form current year
    //	if (sInputYear.length < 4)  Commented by csingh7 : MITS 14181
    if (sInputYear.length < 3)  // Added by csingh7 : MITS 14181
    {
        sGuessedYear = sCurrentYear.substr(0, 4 - sInputYear.length) + sInputYear;

        //If the guessed year is 20 years in the future, assume it's in the last centure
        var iGuessedYear = parseInt(sGuessedYear);
        if (iGuessedYear - iCurrentYear > 20) {
            iGuessedYear = iGuessedYear - 100;
            sGuessedYear = new String(iGuessedYear);
        }
    }

    return sGuessedYear;
}


function ConfirmSave()
{
	var ret = false;
	if(window.parent.m_DataChanged){
		ret = confirm('Data has changed. Do you want to save changes?');
		if ( ret)
		{
		 SaveSettings(null);
		}
	}
	window.parent.m_DataChanged=false;
	return ret;
}
function numLostFocus(objCtrl)
{
	if(objCtrl.value.length == 0)
			return false;
	if(isNaN(parseFloat(objCtrl.value)))
		objCtrl.value = "";
	else
		objCtrl.value = parseFloat(objCtrl.value);
	return true;
}

function phoneLostFocus(objCtrl)
{
	if(objCtrl.value.length == 0)
			return false;
	
	var sValue = new String(objCtrl.value);
	sValue = stripNonDigits(sValue);
	
	if(sValue.length<10)
	{
		objCtrl.value = "";
		alert("Invalid phone number. Please enter valid phone number. Example: (734) 462-5800 or (734) 462-580 Ext:9999");
		objCtrl.focus();
		return false;
	}
	
	var sExt = sValue.substr(10);
	if(sExt != "")
		sExt = " Ext:" + sExt;
	sValue = "("+sValue.substr(0,3)+")"+" "+sValue.substr(3,3)+"-"+sValue.substr(6,4)+sExt;
	
	objCtrl.value = sValue;
	return true;
}

function phoneGotFocus(objCtrl)
{
	if(objCtrl.value.length == 0)
			return false;
	
	var sValue = new String(objCtrl.value);
	sValue = stripNonDigits(sValue);
	var sExt = sValue.substr(10);
	if(sExt != "")
		sExt = " Ext:" + sExt;
	objCtrl.value = sValue.substr(0,10)+sExt;
	objCtrl.select();
}

function stripNonDigits(str) {
	return str.replace(/[^0-9]/g,"")
}


function zipLostFocus(objCtrl) {

                                                                                                                                                                                                                                                                                                                      
	if(objCtrl.value.length == 0)                                                                                                                                      
			return false;                                                                                                                                            
	if(canadaZip(objCtrl) == true)                                                                                                                                     
	{                                                                                                                                                                
		return true;                                                                                                                                                 
	}                                                                                                                                                                
	                                                                                                                                                                 
	var sValue = new String(objCtrl.value);                                                                                                                            
	var sLetter = "";                                                                                                                                                  
	if(sValue.substr(0,1)<'0' || sValue.substr(0,1)>'9')                                                                                                             
		sLetter = sValue.substr(0,1);                                                                                                                                  
		                                                                                                                                                             
		                                                                                                                                                             
	sValue = stripNonDigits(sValue);                                                                                                                                   
	if(((sValue.length == 5 || sValue.length == 9) && sLetter == ""))                                                                                
	{                                                                                                                                                                
		if(sValue.length>5)                                                                                                                                          
		{                                                                                                                                                            
			if(sLetter == "")                                                                                                                                          
				sValue = sValue.substr(0,5) + "-" + sValue.substr(5);                                                                                                      
		}                                                                                                                                                            
		else                                                                                                                                                         
		{                                                                                                                                                            
			if(sLetter != "")                                                                                                                                          
				sValue = sLetter + sValue;                                                                                                                               
		}                                                                                                                                                            
		objCtrl.value = sValue;                                                                                                                                        
		return true;                                                                                                                                                 
	}                                                                                                                                                                
	else                                                                                                                                                             
	{   //pmittal5 Mits 13580 01/29/09 - Canada Zip can have a Blank or No Space at fourth place	                                                                                                                                                             
		//alert("Invalid ZIP code entry. Please enter valid ZIP code. Ex. #####-#### or A#A-#A#(canada) Zip Format");                                                                                               
		alert("Invalid ZIP code entry. Please enter valid ZIP code. Ex. #####-#### or A#A#A#(canada) or A#A #A#(canada) Zip Format");                                                                                               
		objCtrl.value = "";                                                                                                                                            
		objCtrl.focus();                                                                                                                                             
		return false;                                                                                                                                                
	}                                                                                                                                                                
}  
                                                                        
function canadaZip(obj)                                                                                                                                              
{                                                                                                                                                                    
	if(obj.value.length == 0)                                                                                                                                          
	return false;                                                                                                                                                    
	var str = obj.value;                                                                                                                                               
	   
//pmittal5 Mits 13580 01/29/09 - It can be a Blank or No Space at fourth place		                                                                                                                                                                 
	//if(str.replace(/[a-z][0-9][a-z]-[0-9][a-z][0-9]/i,"") == "")         
	if((str.replace(/[a-z][0-9][a-z]\s[0-9][a-z][0-9]/i,"")=="") || (str.replace(/[a-z][0-9][a-z][0-9][a-z][0-9]/i,"")==""))                                                                                              
		return true;                                                                                                                                                 
	else                                                                                                                                                             
		return false;                                                                                                                                              
}                      

function emailLostFocus(objCtrl)
{
	if(objCtrl.value.length == 0)
		return false;
	
	var sValue = new String(objCtrl.value);

	if (sValue.indexOf('@') == -1 ||  sValue.indexOf('.') == -1)
	{
		alert('Invalid e-mail address.Please enter a valid e-mail address');
		objCtrl.focus();
		return false;
	}
	
	objCtrl.value = sValue;
	return true;
}



function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p == sPropName)
			return true;
	}
	return false;
}

function fixDoubleQuotes(pStr)
{
    return pStr.replace('"','""'); 
}

function timeLostFocus(sCtrlName)
{
	try
	{
		var objFormElem = eval('document.forms[0].'+sCtrlName);
		var sTime = new String(objFormElem.value);
		if(sTime == "")
			return true;
		sTime = sTime.toUpperCase();
		var sArr = sTime.split(":");
		if(sArr.length != 2)
		{
			objFormElem.value = "";
			return true;
		}
		if(sArr[0] == "" || sArr[1] == "" || isNaN(parseInt(sArr[0])) || isNaN(parseInt(sArr[1])))
		{
			objFormElem.value = "";
			return true;
		}
		sArr[0] = new String(parseInt(sArr[0],10));
		sArr[1] = new String(parseInt(sArr[1],10));
		if(sTime.indexOf("AM")<0 && sTime.indexOf("PM")<0)
		{
			if(parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>23 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
			{
				objFormElem.value = "";
				return false;
			}
			if(sArr[0].length == 1)
				sArr[0] = "0" + sArr[0];
			if(sArr[1].length == 1)
				sArr[1] = "0" + sArr[1];
			objFormElem.value = this.ConvertTime(sArr[0]+sArr[1]);
		}
		else
		{
			if(parseInt(sArr[0],10)<=0 || parseInt(sArr[0],10)>12 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
			{
				objFormElem.value = "";
				return false;
			}
			if(sArr[0].length == 1)
				sArr[0] = "0" + sArr[0];
			if(sArr[1].length == 1)
				sArr[1] = "0" + sArr[1];
			objFormElem.value = sArr[0] + ":" + sArr[1];
			if(sTime.indexOf("AM") >= 0)
				objFormElem.value = objFormElem.value + " AM";
			else
				objFormElem.value = objFormElem.value + " PM";
		}
		return false;
	}
	catch(ex)
	{}
}
function AddNewUser()
	{
		        if(!ConfirmSave())
		        {
				window.parent.frames['LeftTree'].document.getElementById('TreePostedBy').value="AddNewUser";
				window.parent.frames['LeftTree'].document.getElementById('NewUserId').value="";
				window.parent.frames['LeftTree'].document.getElementById('UserEntityUserId').value="";
				window.parent.frames['LeftTree'].document.getElementById('UserEntityDsnIdSelected').value="";
				parent.frames['MainFrame'].location.href='User.aspx?userid=0';
				return false;
			     }
			     return false;
		
}
//Start Add By ttumula2 on 22 Dec 2014 RMA-6033
function DateFormat() {
    window.open('MultiLangDateFormat.aspx', 'SetEmailOptions', 'Width=390,Height=580,status=yes,scrollbars=1,top=' + (screen.availHeight - 210) / 4 + ',left=' + (screen.availWidth - 460) / 2 + ''); // Add By ttumula2 on 06 Jan 2015 RMA-4281
    return false;
    //if (!ConfirmSave()) {
    //    window.parent.frames['LeftTree'].document.getElementById('TreePostedBy').value = "DateFormat";
    //    window.parent.frames['LeftTree'].document.getElementById('NewUserId').value = "";
    //    window.parent.frames['LeftTree'].document.getElementById('UserEntityUserId').value = "";
    //    window.parent.frames['LeftTree'].document.getElementById('UserEntityDsnIdSelected').value = "";
    //    parent.frames['MainFrame'].location.href = 'MultiLangDateFormat.aspx';
    //    return false;
    //}
    //return false;
//End Add By ttumula2 on 22 Dec 2014 RMA-6033
}
function state_Change()
{
// if xmlhttp shows "loaded"
if (objAJAXManager.ReadyState()==4)
  {
  // if "OK"
  if (objAJAXManager.Status()==200)
    {
      if(objAJAXManager.ResponseInDOM().getElementById("RetVal").value==0)
      {
      document.getElementById("Status").style.cssText="color:red;align=center"
      document.getElementById("Status").innerHTML =objAJAXManager.ResponseInDOM().getElementById("desc").value
      document.getElementById("btnOK").disabled = true
      }
      else
      {
        if(objAJAXManager.ResponseInDOM().getElementById("RetVal").value==1)
          {
            document.getElementById("Status").style.cssText="color:green;align=center"
            document.getElementById("Status").innerHTML = objAJAXManager.ResponseInDOM().getElementById("SuccessMsg").value
            document.getElementById("btnOK").disabled = false
          }
      }
    }
  else
    {
    alert("Problem retrieving XML data")
    }
  }
  else
  {
    document.getElementById("Status").style.cssText="color:#FF0000;"
    document.getElementById("Status").innerHTML ="Verifying the complexity...."
  }
}

function SaveSettings(b_pCalledFrmPopUp) {

    
  if(this.ShowPasswordScreen())
     {
       return false;
     }
  
  if((!IsDataChanged()) && (b_pCalledFrmPopUp==null))
	{
	return false;
	}
	//rsolanki2 : domain authentication updates
   var DomainElement= window.parent.frames['LeftTree'];  
   if (DomainElement!=null)
        DomainElement=DomainElement.document.getElementById('hdIsDomainAuthenticationEnabled'); ; 
   if (DomainElement!=null)
        m_bIsDomainAuthenticationEnabled = DomainElement.value;   
try
{
  var sAction = ""
  var bIsMainFramePosted=false;
  var bLookForFormPostings=true;
  var bPostLeftTree=true
  if(b_pCalledFrmPopUp!=null)
	{
		if(window.opener.parent.frames['LeftTree'].document.getElementById('TreePostedBy')!=null)
		    sAction = window.opener.parent.frames['LeftTree'].document.getElementById('TreePostedBy').value	
		else
		sAction="";
	}
  else
	{
		if(window.parent.frames['LeftTree'].document.getElementById('TreePostedBy')!=null)
		sAction=window.parent.frames['LeftTree'].document.getElementById('TreePostedBy').value
		else
		sAction="";
	}
 
 
  switch(sAction)
	      {
	            case "AddNewUser" :
	            {
	                
	                if(!ValidateUserEntity(1))
						{
						return false;
						}
	                //var wnd=window.open('home?pg=riskmaster/SecurityMgtSystem/ProgressWindow','progressWnd',
				    //    'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
			        //    self.parent.wndProgress=wnd;
					//    window.parent.frames['MainFrame'].document.all['hdAction'].value = "UsersAdaptor.Save"
					if(window.parent.frames['MainFrame'].document.getElementById('hdSave').value!="SaveWithDsn")
						{
						 window.parent.frames['MainFrame'].document.getElementById('hdSave').value="SaveWithNoDsn"
						}
//						else
//						{
//						   SetTreePostedByValue('UpdateUserPermEntity');
//						}
					if(window.parent.frames['MainFrame'].document.getElementById('hdSave').value=="")
						{
						window.parent.frames['MainFrame'].document.getElementById('hdSave').value="SaveWithNoDsn"
						}
						bPostLeftTree=false
					break;
	            }
	            case "UpdateUserEntity" :
	            {
	            
			// BSB/JAP Cannot assign user to group with this check in here...
	                // if(!ValidateUserEntity(1))
			//			{
			//			return false;
			//			}
                   
			           // var wnd=window.open('home?pg=riskmaster/SecurityMgtSystem/ProgressWindow','progressWnd',
						//	'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
						//	self.parent.wndProgress=wnd;
						if(window.parent.frames['MainFrame'].document.getElementById('hdSave').value!="SaveWithDsn")
							{
							window.parent.frames['MainFrame'].document.getElementById('hdSave').value="SaveWithNoDsn"
							}
//							else
//							{
//							    SetTreePostedByValue('UpdateUserPermEntity');
//							}		
						if(window.parent.frames['MainFrame'].document.getElementById('hdSave').value=="")
							{
							window.parent.frames['MainFrame'].document.getElementById('hdSave').value="SaveWithNoDsn"
							}
							bPostLeftTree=false
					
					break;
	            }
	            
	            case "AddNewDatasource" :
	            {

	                window.opener.parent.frames['LeftTree'].pleaseWait.Show();
                    window.opener.parent.frames['LeftTree'].window.document.forms['frmLeftTree'].submit()
                    window.close()
                    bIsMainFramePosted=true;
					break;
	            }
	            case "UpdateDatasourceEntity" :
	            {
					if(ValidateDatasourceSave())
					 {
					  window.parent.frames['MainFrame'].document.getElementById('hdSave').value="Save"
					  bPostLeftTree=false
					  break;
					 }
					else
					{
					  bLookForFormPostings=false
					} 
	            }
	        case "UpdateUserPermEntity":
	            {
	                if (self.parent.frames["MainFrame"].document.getElementById('permexpires') != null) {
	                    if (self.parent.frames["MainFrame"].document.getElementById('permexpires').checked) {
	                        if (self.parent.frames["MainFrame"].document.getElementById('expdate').value == "") {
	                            alert('Please Select The Expiration date');
	                            return false;
	                        }
	                    }
	                }
	                
	                if (ValidateUserPermSave()) {
	                    bPostLeftTree = false
	                    if (window.parent.frames['MainFrame'].document.getElementById('hdSave').value == "") {
	                        window.parent.frames['MainFrame'].document.getElementById('hdSave').value = "SaveNoModuleGrpSelected"
	                    }
	                    if (window.parent.frames['MainFrame'].document.getElementById('hdSave').value != "SaveWithModuleGrpSelected") {
	                        bPostLeftTree = false;
	                        window.parent.frames['MainFrame'].document.getElementById('hdSave').value = "SaveNoModuleGrpSelected"
	                    }
	                    if (window.parent.frames['LeftTree'].document.getElementById('DsnIdForCurrentlyLoadedMGrps') != null) {
	                        bPostLeftTree = false;
	                        window.parent.frames['LeftTree'].document.getElementById('DsnIdForLoadingMG').value = window.parent.frames['LeftTree'].document.getElementById('DsnIdForCurrentlyLoadedMGrps').value
	                        window.parent.frames['MainFrame'].document.getElementById('hdDsnIdForCurrentlyLoadedMGrps').value = window.parent.frames['LeftTree'].document.getElementById('DsnIdForCurrentlyLoadedMGrps').value

	                    }
	                    if (window.parent.frames['MainFrame'].document.getElementById('hdSave').value == "SaveWithModuleGrpSelected") {

	                        //window.parent.frames['LeftTree'].pleaseWait.Show();
	                        //var wnd=window.open('home?pg=riskmaster/SecurityMgtSystem/ProgressWindow','progressWnd',
	                        //	'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
	                        //	self.parent.wndProgress=wnd;
	                    }
	                }
	                else {
	                    bLookForFormPostings = false
	                }
	                break;
	            }
	            case "UpdateModuleGroupEntity" :
	            {
					
					break;
	            }
	            case "AddNewModuleGroupEntity" :
	            {
	                window.opener.parent.frames['LeftTree'].pleaseWait.Show();
                    window.opener.parent.frames['LeftTree'].window.document.forms['frmLeftTree'].submit()
                    window.close()
                    bLookForFormPostings=false;
				    break;
	            }
	            case "RenameModuleGroupEntity" :
	            {
	                window.opener.parent.frames['LeftTree'].pleaseWait.Show();
                    window.opener.parent.frames['LeftTree'].window.document.forms['frmLeftTree'].submit()
                    window.close()
                    bLookForFormPostings=false;
				    break;
	            }
	            case "ChangeLoginInfo" :
	            {
		           window.document.getElementById('hdAction').value = "OK"
		           window.document.forms['frmData'].submit()
                   bIsMainFramePosted=true;
                   //window.close()
				   break;
	            }
	            case "ClearLicenseHistory" :
	            {
		           window.document.getElementById('hdAction').value = "Clear"
		           window.document.forms['frmData'].submit()
                   bLookForFormPostings=true;
				   break;
	            }
	        case "UpdateModuleGroupPermissionEntity":
	            {
	                if (window.parent.frames['MainFrame'].document.forms['frmPermissionTree'] != null) {
	                    //var wnd=window.open('home?pg=riskmaster/SecurityMgtSystem/ProgressWindow','progressWnd',
	                    //		'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
	                    //		self.parent.wndProgress=wnd;

	                    window.parent.frames['MainFrame'].document.getElementById('hdFunctionToCall').value = "ModuleGroupsAdaptor.UpdateModulePermissions";
	                    window.parent.frames['MainFrame'].pleaseWait.Show();
	                    window.parent.frames['MainFrame'].document.forms['frmPermissionTree'].submit();
	                }
	                bLookForFormPostings = false;
	                break;
	            }
	            case "UpdateLicenses" :
	            {
					
		           window.document.getElementById('hdAction').value = "UpdateLicenses"
		           window.document.getElementById('hdDsnId').value =window.opener.parent.frames['MainFrame'].window.document.getElementById('dsnid').value
		           window.document.forms['frmData'].submit()
                   bLookForFormPostings=false;
				   break;
	            }
	             case "SetEmailOptions" :
	            {
	            
				   	
		           window.document.getElementById('hdAction').value = "OK"
		           window.document.forms['frmData'].submit()
                   bLookForFormPostings=false;
				   break;
	            }
	            case "DomainAuthentication" :
	            {
	               //rsolanki2 : Domain authentication updates.   	
		           window.document.getElementById('hdAction').value = "OK"
		           window.document.forms['frmData'].submit();
                   bLookForFormPostings=false;
                   window.opener.parent.location.reload();
                   window.close();
				   break;
	            }
	            default  :
	             {
                   bLookForFormPostings=false;
                   break;
                 }

	                
	      }

	      if(bLookForFormPostings)
	         {
				if(!bIsMainFramePosted)
					    {
					        window.parent.frames['MainFrame'].pleaseWait.Show();
						window.parent.frames['MainFrame'].document.forms['frmData'].submit()
						var SubmitTree=false
						}
				if(bPostLeftTree)
						{
						    window.parent.frames['LeftTree'].pleaseWait.Show();
						  window.parent.frames['LeftTree'].document.forms['frmLeftTree'].submit()
						}
		      }
		      setDataChanged(false)
		  
  return false;
  }
  catch(p_objException)
  {
  }
 return false;
}
function setDataChanged(b)
{
    window.parent.m_DataChanged=b
	return b;
}

function SetDsnId()
{
	if((window.parent.frames['MainFrame'].document.getElementById('lstDatasources').value==0)  || (window.parent.frames['MainFrame'].document.getElementById('lstDatasources').value==""))
	{
		window.parent.frames['MainFrame'].document.getElementById('hdSave').value=""
		window.parent.frames['LeftTree'].document.getElementById('UserEntityDsnIdSelected').value=""
		window.parent.frames['MainFrame'].document.getElementById('dsnid').value = ""
		return
	}
	window.parent.frames['LeftTree'].document.getElementById('UserEntityDsnIdSelected').value=window.parent.frames['MainFrame'].document.getElementById('lstDatasources').value
	window.parent.frames['MainFrame'].document.getElementById('hdSave').value = "SaveWithDsn"
	window.parent.frames['MainFrame'].document.getElementById('dsnid').value = window.parent.frames['MainFrame'].document.getElementById('lstDatasources').value
	window.parent.frames['LeftTree'].document.getElementById('hdShowPasswordScreen').value = "1"
	setDataChanged(true)
}
function SetModuleGroupId()
{
	if((window.parent.frames['MainFrame'].document.getElementById('lstModuleGroups').value==0)  || (window.parent.frames['MainFrame'].document.getElementById('lstModuleGroups').value==""))
	{
		window.parent.frames['LeftTree'].document.getElementById('UserPermModuleGrpSelectedId').value=""
		window.parent.frames['MainFrame'].document.getElementById('hdSave').value = ""
		window.parent.frames['MainFrame'].document.getElementById('hdSelectedModuleGrpId').value = ""
		return
	}
	window.parent.frames['LeftTree'].document.getElementById('UserPermModuleGrpSelectedId').value=window.parent.frames['MainFrame'].document.getElementById('lstModuleGroups').value
	window.parent.frames['MainFrame'].document.getElementById('hdSave').value = "SaveWithModuleGrpSelected"
	window.parent.frames['MainFrame'].document.getElementById('hdSelectedModuleGrpId').value = window.parent.frames['MainFrame'].document.getElementById('lstModuleGroups').value
	  
	setDataChanged(true)
}

function PermissionExpires()
{
  var sLoginName = document.getElementById("loginname").value
//parijat : Mits 11690
  if(document.getElementById("hdIsSMSAdminUser") != null)
  {
      if( document.getElementById("hdIsSMSAdminUser").value =="True" )
      {
       alert("Login Name \'" + sLoginName + "\' is a Security Admin User and this user login can never expire.") ;
            document.forms[0].expdate.value = '';
		    document.forms[0].expdate.disabled = true;
		    document.forms[0].expdatebtn.disabled = true;
		    document.forms[0].permexpires.checked = false;
    		
		    return ;
      } 
  }
	if (document.forms[0].permexpires.checked)
	{
		document.forms[0].expdate.disabled = false;
		document.forms[0].expdatebtn.disabled = false;
	}	
	else
	{
		document.forms[0].expdate.value = '';
		document.forms[0].expdate.disabled = true;
		document.forms[0].expdatebtn.disabled = true;
	}
}

function chkDSNWizard()
{
	if (window.document.forms[0].chkDSN.checked) 
	{
	    document.getElementById('hdAction').value="PredefinedDSN"
		window.document.forms[0].submit();
	}
	else
	{
	   
	    document.getElementById('hdAction').value="ODBCDrivers"
		window.document.forms[0].submit();
	}
}
function OnSelectDB(p_objRadioDbCtrl,p_sView)
{
    var bBypassDsnDupChk=false
    if(window.opener.parent.frames['MainFrame'].document.getElementById("hdBypassDupDsnNameChk")!=null)
	  {	
		    if(window.opener.parent.frames['MainFrame'].document.getElementById("hdBypassDupDsnNameChk").value=="true")
					{
					  bBypassDsnDupChk=true
					}
	  }			
   switch (p_sView)
   {
			case "file" :
			{
			document.getElementById('hdAction').value="FileLogin"
			document.getElementById('btnNext').disabled=false
			break;
			}
			case "server" :
			{
			document.getElementById('hdAction').value="ServerLogin"
			document.getElementById('btnNext').disabled=false
			break;
			}
			default :
			{
			   
			    if(bBypassDsnDupChk)
			     {
			      document.getElementById('hdAction').value="DSNLoginPage"
				  document.getElementById('btnNext').disabled=false
				  break;
			     }
				if(!IsPreDsnAlreadySelected(p_objRadioDbCtrl.nextSibling.data,""))
				{
				document.getElementById('hdAction').value="DSNLoginPage"
				document.getElementById('btnNext').disabled=false
				}
				else
				{
				 alert("Selected Data Source name is already in use.")
				 p_objRadioDbCtrl.checked=false
				 document.getElementById('btnNext').disabled=true
				}
			break;
			}
    
   }
   document.getElementById('hdView').value=p_sView
   bDefaultSelectionDB=false
 }
 

function SetWizardAction(p_objButtonCtrl, p_WizardType, event)
  {
	
	if(p_objButtonCtrl.name=="btnCancel" || event.clientY<0 || p_objButtonCtrl.name.indexOf("CancelButton")>0)
	{
		ResetSession();
		
		return false;
	}

	switch(p_WizardType)
	{
	  
	case "PreDSN" :
	{
	switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="ODBCDrivers"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnNext" :
					{
					if(bDefaultSelectionDB)
					{
					document.getElementById('hdAction').value="DSNLoginPage"
					}
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}	    
	case "ODBCDrivers" :
	{
	     switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="ODBCDrivers"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnNext" :
					{
					if(bDefaultSelectionDB)
					{
					if(document.getElementById('hdView').value=="file")
					{
						document.getElementById('hdAction').value="FileLogin"
					}
					else
					{
						document.getElementById('hdAction').value="ServerLogin"
					}
					}
					 
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
	case "ProcessDBLogin" :
	 {
			switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
						if(document.getElementById('hdProcessDbCalledFrm').value=="ServerLogin")
						{
						document.getElementById('hdAction').value="ServerLogin"
						}
						else
						{
							if(document.getElementById('hdProcessDbCalledFrm').value=="FileLogin")
							{
							document.getElementById('hdAction').value="FileLogin"
							}
							else
							{
							document.getElementById('hdAction').value="DSNLoginPage"
							}
						}
					window.document.forms[0].submit();
					
					break;
					}
					case "btnNext" :
					{
					document.getElementById('hdAction').value="ValidateDB"
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
  case "DSNLoginPage" :
	{
	switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="PredefinedDSN"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnNext" :
					{
					document.getElementById('hdAction').value="ProcessDBLogin"
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
	case "ValidateDB" :
	{
    	switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="ProcessDBLogin"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnNext" :
					{
					document.getElementById('hdAction').value="DbLicense"
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
 case "ServerLogin" :
	      {
	switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="ODBCDrivers"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnNext" :
					{
					document.getElementById('hdAction').value="ProcessDBLogin"
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
  case "FileLogin" :
	{
	switch (p_objButtonCtrl.name)
			{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="ODBCDrivers"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnNext" :
					{
					document.getElementById('proxyfileSelect').value=document.getElementById('fileSelect').value
					document.getElementById('hdAction').value="ProcessDBLogin"
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
case "DbLicense" :
	{
	   
	switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="ValidateDB"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnFinish" :
					{
					document.getElementById('hdAction').value="ValidateLicense"
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
 case "ExistingDsn" :
	{
	   
	switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="ProcessDBLogin"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnFinish" :
					{
					ClosePopUpOnSuccess(null,"ExistingDsn")
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
case "UpdateLicenses" :
	{
	 switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="UpdateLicenses"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnFinish" :
					{
					document.getElementById('hdAction').value="ValidateLicense"
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
case "AddNewModuleGroupEntity":
    {
        switch (p_objButtonCtrl.name) {
            case "btnBack":
                {
                    document.getElementById('hdAction').value = "UpdateLicenses"
                    window.document.forms[0].submit();

                    break;
                }
            case "btnOk":
                {
                    if (TrimIt(window.document.getElementById('groupname').value) == "") {
                        alert("Please enter a valid name for new Module Group")
                        return false;
                    }
                    window.document.getElementById('entitytype').value = window.opener.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value
                    window.document.getElementById('selectedentityid').value = window.opener.parent.frames['LeftTree'].window.document.getElementById('EntityIdForDeletion').value
                    window.document.getElementById('hdAction').value = "OK"
                    window.document.forms['frmData'].submit()
                    break;
                }
            case "btnCancel":
                {
                    document.getElementById('hdAction').value = "ClosePage"
                    break;
                }
            default:
                {
                    break;
                }
        }
        break;
    }
 case "RenameModuleGroupEntity" :
	{
	switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="UpdateLicenses"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnOk" :
					{
						if(TrimIt(window.document.getElementById('groupname').value)=="")
						{
							alert("Group name cannot be blank.");
							return false;
						}	
						else
						{
								
								window.document.getElementById('entitytype').value=window.opener.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value
								window.document.getElementById('selectedentityid').value= window.opener.parent.frames['LeftTree'].window.document.getElementById('EntityIdForDeletion').value	
								window.document.getElementById('hdAction').value = "OK"
								window.document.forms['frmData'].submit()
								
						}
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
 case "ClearLicenseHistory" :
	{
	switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="ClearLicense"
					window.document.forms[0].submit();
					break;
					}
					case "btnFinish" :
					{
					document.getElementById('hdAction').value="ValidateLicense"
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
case "ValidateLicense" :
	{
	switch (p_objButtonCtrl.name)
				{
					case "btnBack" :
					{
					document.getElementById('hdAction').value="DbLicense"
					window.document.forms[0].submit();
					
					break;
					}
					case "btnFinish" :
					{
					document.getElementById('hdAction').value="ValidateLicense"
					window.document.forms[0].submit();
					break;
					}
					case "btnCancel" :
					{
					document.getElementById('hdAction').value="ClosePage"
					break;
					}
					default:
					{
					break;
					}
				}
				break;
				}
	}
	return false;
}
function ResetSession()
{
  var ret;
   try
   {
	ret = confirm('Close Database Connection Wizard and lose any changes?');
	setDataChanged(false)
	if (ret)
	  { 
	    if(window.opener!= null)
	    {
	        if(window.opener.parent.frames['MainFrame'].document.getElementById("hdBypassDupDsnNameChk")!=null)
	         {
	           window.opener.parent.frames['MainFrame'].document.getElementById("hdBypassDupDsnNameChk").value="false"
	         }
	     }  
	    if(window.document.getElementById('hdAction')!= null)
	    {
	        window.document.getElementById('hdAction').value = "Cancel"
	    }
	    window.document.forms['frmData'].submit()
	    
		window.close();
	  }	
		
	else
		return false;
		
   }
   catch(p_objException)
   {
    throw p_objException
   }
  		
}
//Tanuj -july first week 2005
//p_sCompleteIdentityForEntity==Will help to open Windows in main frame
//Will be filled in by functions called through pop up windows
//Example-> ModuleGroupsAdaptor.SaveObject() method will post back complete identity(ids seperated through commas) for an entity;
//also this function is receiving entity type so can locate a entity completely..
function ClosePopUpOnSuccess(p_iId,p_sEntity,p_sCompleteIdentityForEntity)
{    
    var bCalledFrmPopUp = true;
   
	switch(p_sEntity)
	{
		case "Datasource":
		{
		   window.opener.parent.frames['LeftTree'].window.document.getElementById('TreePostedBy').value="AddNewDatasource"
		   window.opener.parent.frames['LeftTree'].window.document.getElementById('AddedDBId').value=p_iId
		   //window.opener.parent.frames['MainFrame'].location.href="Datasource.aspx?dsnid="+p_iId;
		   SaveSettings(bCalledFrmPopUp)
		   
		break
		}
		case "AddNewModuleGroupEntity":
		{
		   window.opener.parent.frames['LeftTree'].window.document.getElementById('TreePostedBy').value="AddNewModuleGroupEntity"
		   window.opener.parent.frames['LeftTree'].window.document.getElementById('AddedDBId').value=p_iId
		   
		   var arrIds=p_sCompleteIdentityForEntity.split(',')
		   window.opener.parent.frames['LeftTree'].document.getElementById('DsnIdForLoadingMG').value=arrIds[0]
		   SaveSettings(bCalledFrmPopUp)
		   //window.opener.parent.frames['MainFrame'].document.write(m_sRead)
		   //window.opener.parent.frames['MainFrame'].location.href="GrpModuleAccessPermission.aspx?dsnid="+arrIds[0]+"&groupid="+arrIds[1];
		   break;
		}
		case "RenameModuleGroupEntity":
		{
		   var sRead= m_sRead
		   window.opener.parent.frames['LeftTree'].window.document.getElementById('TreePostedBy').value="RenameModuleGroupEntity"
		   window.opener.parent.frames['LeftTree'].window.document.getElementById('AddedDBId').value=p_iId
		   var arrIds=p_sCompleteIdentityForEntity.split(',')
		   window.opener.parent.frames['LeftTree'].document.getElementById('DsnIdForLoadingMG').value=arrIds[0]
		   SaveSettings(bCalledFrmPopUp)
		   //window.opener.parent.frames['MainFrame'].document.write(sRead)
		   //window.opener.parent.frames['MainFrame'].location.href="GrpModuleAccessPermission.aspx?dsnid="+arrIds[0]+"&groupid="+arrIds[1];		   
		   break;
		}
		case "ExistingDsn":
		{
		  window.opener.parent.frames['MainFrame'].window.document.getElementById('ipdocpath').value=window.document.getElementById('connStr').value
		  if(window.opener.parent.frames['MainFrame'].document.getElementById("hdBypassDupDsnNameChk")!=null)
		   {
		     window.opener.parent.frames['MainFrame'].document.getElementById("hdBypassDupDsnNameChk").value="false"
		   }
		   //window.document.all['hdAction'].value = "Cancel"
	       //window.document.forms['frmData'].submit()
		   setDataChanged(true)
		   window.opener.setDataChanged(true)
		  window.close()
		  break;
		}
		
		case "ChangeLoginInfo":
		{
		  if(ValidateLoginInfo())
		    {
		     setDataChanged(false)
		     window.opener.parent.frames['LeftTree'].window.document.getElementById('TreePostedBy').value="ChangeLoginInfo"
		     SaveSettings(bCalledFrmPopUp)
		    }
		  break;
		}
		case "SetEmailOptions":
		    {		       
		        //mbahl3 JIRA[RMA-838]
		        var senderaltdomain = document.forms[0].senderalternatedomain;
		        var senderreplyaltdomain = document.forms[0].senderreplytodomain;
		        if (senderaltdomain.value != "") {
                    //sharishkumar Jira 6379 starts
		            if (/[@]/.test(senderaltdomain.value))
		                //if (/[@\d]/.test(senderaltdomain.value))
		                //sharishkumar Jira 6379 ends
		            {
	              alert(parent.CommonValidations.RestrictAltdomain);
		                return (false);
		            }
		        }
		        if (senderreplyaltdomain.value != "") {
		            //sharishkumar Jira 6379 starts
		            if (/[@]/.test(senderreplyaltdomain.value))
		                //if (/[@\d]/.test(senderreplyaltdomain.value))
		                //sharishkumar Jira 6379 ends
		            {
		                alert(parent.CommonValidations.RestrictReplyAltdomain);
		                return (false);
		            }
		        }
		        //mbahl3 JIRA[RMA-838]

		        if (ValidateEmailOpts())
		     {
		        setDataChanged(false)
				window.opener.parent.frames['LeftTree'].window.document.getElementById('TreePostedBy').value="SetEmailOptions"
				SaveSettings(bCalledFrmPopUp)
		     }
		    break;
		}
		case "DomainAuthentication":
		{   
		    if(ValidateDomainOpts())
		     {
		        setDataChanged(false)
				window.opener.parent.frames['LeftTree'].window.document.getElementById('TreePostedBy').value="DomainAuthentication"
				SaveSettings(bCalledFrmPopUp)
		     }
		    break;
		}
case "DomainAuthenticationModify":
    {
        //rsolanki2 : Domain authentication updates.   	
         if (ValidateDomainModifyOpts()) {
            setDataChanged(false);
            //oldName  status			 
            //window.document.all['hdAction'].value 
            window.document.getElementById('hdnAction').value = "OK";
            window.document.getElementById('sequenceid').value = window.document.getElementById('txtdomainName').value;
            //debugger;
            window.document.getElementById('oldName').value = getQval("selectedid");
            window.document.getElementById('status').value = window.document.getElementById('chkDomain').checked;
            window.document.getElementById('hdsavecomplete').value = "true";
            window.document.forms['frmData'].submit();
            bLookForFormPostings = false;
            //window.parent.location.navigate(window.parent.location.href); 
          //  window.opener.setTimeout("window.location.reload()", 1000);
//            window.opener.location.reload();
           // window.close();
        }
        break;
    }
		case "ClearLicenseHistory":
		{   
		        setDataChanged(false)
				window.opener.parent.frames['LeftTree'].window.document.getElementById('TreePostedBy').value="ClearLicenseHistory"
				SaveSettings(bCalledFrmPopUp)
		    
		    break;
		}
case "UpdateLicenses":
    {
        var objCode = document.getElementById('licensecode');
        if (objCode != null && objCode.value == "") {
            alert("License code cannot be blank.");
            return false;
        }
        setDataChanged(false)
        window.opener.parent.frames['LeftTree'].window.document.getElementById('TreePostedBy').value = "UpdateLicenses"
        SaveSettings(bCalledFrmPopUp)

        break;
    } 
		case "UpdateLicensesClose":
		{
		  window.opener.parent.frames['LeftTree'].window.document.getElementById('TreePostedBy').value="UpdateDatasourceEntity"
		  window.opener.parent.frames['MainFrame'].location.href="home?pg=riskmaster/SecurityMgtSystem/Datasource&amp;dsnid="+p_iId;
		  window.close()
		  break;
		}
		default:
		{
		   objUnknownEntityType = new Error ("Unknown entity type was passed to ClosePopUpOnSuccess() function");
		   throw objUnknownEntityType;
		 
		}
	}
	return false;

}
function DeleteSelectedItem() {
    
 if(!ConfirmSave())
 {
    if(window.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value=="")
    {
      return false
    }
    var sType1="Root Node Riskamster Security Tree"
    var sType2="Root Node Riskmaster Security Users"
    var sType3="Root Node Riskmaster Datasources"
    var sType4="Root Node Module Security Groups"
    var sType5="Root Node for PermToLogin"
    var sType6="GrpModuleAccessPermission"
    var sType7="Module Group User"
    var sType8="Root Node for Company" // Nitesh MITS 6184
    var sType9="NoData"
    if(window.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree')==null)
      {
        alert("Please select an entity/item for deletion.")
        return false;
      }
     
    switch(window.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value)
    {
      case sType1:
      case sType2:
      case sType3:
      case sType4:
      case sType5:
      case sType8:
      case sType9:
      {
        alert("Selected entity can not be deleted.")
        return false; 
      }
     
     
    }
    var bRet = confirm('Are you sure you want to delete selected item?');
    if(!bRet)
    {
    return false;
    }
    var bCalledFrmPopUp=true;
    window.parent.frames['LeftTree'].document.getElementById('TreePostedBy').value="Delete"
    switch(window.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value)
    {
      case sType6:
      case sType7:
       {
          var arrIds=window.parent.frames['LeftTree'].document.getElementById('EntityIdForDeletion').value.split(',')
          window.parent.frames['LeftTree'].document.getElementById('DsnIdForLoadingMG').value=arrIds[0]
          break;
       }
      default:
       {
        break;
       }
     
     
    }
//    window.parent.frames['MainFrame'].location.href="home?pg=riskmaster/SecurityMgtSystem/NoData"
    window.parent.frames['MainFrame'].location.href="NoData.html"
	window.parent.frames['LeftTree'].document.forms['frmLeftTree'].submit()
  }
  return false;
}
function AppendDelimiter(p_sVal)
{
  if(p_sVal.indexOf("|")==-1)
	{
	 return p_sVal ;
	}
	var sStart = p_sVal.substr(0,1)
       if(sStart != ",")
          {
            p_sVal = ","+p_sVal;
	       }
    var sEnd = p_sVal.substr(p_sVal.length-1,1)
         if(sEnd != ",")
             {
                 p_sVal =p_sVal + ","
		      }
		
  return p_sVal	
}

function RemoveDelimiter(p_sVal)
{
 if(p_sVal.indexOf("|")==-1)
	{
	 return p_sVal ;
	 
	}
	var sStart = p_sVal.substr(0,1)
       if(sStart == ",")
        {
          p_sVal = p_sVal.substr(1);
	}
	var sEnd = p_sVal.substr(p_sVal.length-1,1)
       
       if(sEnd == ",")
           {
         p_sVal = p_sVal.substr(0, p_sVal.length-1);
		}
		
  return p_sVal;	
} 
function AccessModules(p_iRevokeOrGrant)
{
     var oParentFuncId = window.parent.frames['MainFrame'].document.getElementById('hdCurrentFuncId');
     var sParentFuncId = "";
     if( oParentFuncId != null )
     	sParentFuncId = oParentFuncId.value;
     	
     if(sParentFuncId=="")
     {
        alert("Please select a Module Access Permission entity to Revoke or Grant permissions.")
        return false;
     }
     //var wnd=window.open('home?pg=riskmaster/SecurityMgtSystem/ProgressWindow','progressWnd',
		//	 	'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
     //self.parent.wndProgress=wnd;
	  		            
     if(p_iRevokeOrGrant==1)
     {
		window.parent.frames['MainFrame'].document.getElementById('hdGrantedFromFuncId').value = sParentFuncId;
     }
     else
     {
		window.parent.frames['MainFrame'].document.getElementById('hdRevokedFromFuncId').value = sParentFuncId;
     }
     
     window.parent.frames['MainFrame'].document.getElementById('hdFunctionToCall').value = "ModuleGroupsAdaptor.UpdateModulePermissions";

     window.parent.frames['MainFrame'].pleaseWait.Show();
     window.parent.frames['MainFrame'].document.forms['frmPermissionTree'].submit()
     setDataChanged(false) ;
     return false;
}

function AllowAccess(obj)
{
var ip1Id=""
var ip2Id=""
ip1Id="ip1"+obj.all[1].all[0].id
ip2Id="ip2"+obj.all[1].all[0].id
	try
	{
	     
		if (window.parent.m_DataChanged)
		{
			if (!obj.all(1).all(0).checked)
			{
			    document.getElementById(ip1Id).value=""
			    document.getElementById(ip2Id).value=""
			    document.getElementById(ip1Id).disabled=true
			    document.getElementById(ip2Id).disabled=true
			}
			else
			{
			    document.getElementById(ip1Id).disabled=false
			    document.getElementById(ip2Id).disabled=false
			    if(document.getElementById(ip1Id).value=="")
			    {
			     document.getElementById(ip1Id).value="12:00 AM"
			     }
			     if(document.getElementById(ip2Id).value=="")
			    {
			     document.getElementById(ip2Id).value="12:00 AM"
			     }
			}	
		}
	}
	catch(ex)
	{}
}
function TimeLostFocus(sCtrlName)
{
	
		var objFormElem = window.parent.frames['MainFrame'].document.getElementById(sCtrlName)
		var sTime = objFormElem.value
		if(sTime == "")
			return true;
		sTime = sTime.toUpperCase();
		var sArr = sTime.split(":");
		if(sArr.length != 2)
		{
			objFormElem.value = "";
			return true;
		}
		if(sArr[0] == "" || sArr[1] == "" || isNaN(parseInt(sArr[0])) || isNaN(parseInt(sArr[1])))
		{
			objFormElem.value = "";
			return true;
		}
		sArr[0] = new String(parseInt(sArr[0],10));
		sArr[1] = new String(parseInt(sArr[1],10));
		if(sTime.indexOf("AM")<0 && sTime.indexOf("PM")<0)
		{
			if(parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>23 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
			{
				objFormElem.value = "";
				return true;
			}
			if(sArr[0].length == 1)
				sArr[0] = "0" + sArr[0];
			if(sArr[1].length == 1)
				sArr[1] = "0" + sArr[1];
			objFormElem.value = this.ConvertTime(sArr[0]+sArr[1]);
		}
		else
		{
			if(parseInt(sArr[0],10)<=0 || parseInt(sArr[0],10)>12 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
			{
				objFormElem.value = "";
				return true;
			}
			if(sArr[0].length == 1)
				sArr[0] = "0" + sArr[0];
			if(sArr[1].length == 1)
				sArr[1] = "0" + sArr[1];
			objFormElem.value = sArr[0] + ":" + sArr[1];
			if(sTime.indexOf("AM") >= 0)
				objFormElem.value = objFormElem.value + " AM";
			else
				objFormElem.value = objFormElem.value + " PM";
		}
		return true;
	
	
}
function OnSelectServersOptions(p_objRadioDbCtrl)
{
    window.parent.frames['MainFrame'].document.getElementById("ipdocpath").readOnly = false;
    if (p_objRadioDbCtrl.value == 0 || p_objRadioDbCtrl.value == 2)
    {
        window.parent.frames['MainFrame'].document.getElementById("btdocpath").disabled = true;
    }
    else
    {
        window.parent.frames['MainFrame'].document.getElementById("btdocpath").disabled = false;
    }
    if (p_objRadioDbCtrl.value != 2) {
        window.parent.frames['MainFrame'].document.getElementById("ipdocpath").value = "";
    }
    else {
        window.parent.frames['MainFrame'].document.getElementById("ipdocpath").value = "rmas3bucket" + window.parent.frames['MainFrame'].document.getElementById("txts3bucketname").value;
        window.parent.frames['MainFrame'].document.getElementById("ipdocpath").readOnly = true;
    }
    setDataChanged(true);
}
function OpenDBWizardSetDocPath()
{

      window.parent.frames['MainFrame'].document.getElementById("hdBypassDupDsnNameChk").value="true"
      window.open('ModifyDataSourceInformation.aspx?InvokedFrom=SetDocPath', 'secDSN11', 'Width=650,Height=400,status=yes,top=' + (screen.availHeight-400)/2 + ',left=' + (screen.availWidth-650)/2 + '');
      return false;
}
function OpenDBWizard()
{

      window.parent.frames['MainFrame'].document.getElementById("hdBypassDupDsnNameChk").value="true"
      window.open('home?pg=riskmaster/SecurityMgtSystem/ODBCDrivers&amp;InvokedFrom=ExistingDsn', 'secDSN11', 'Width=650,Height=400,status=yes,top=' + (screen.availHeight-400)/2 + ',left=' + (screen.availWidth-650)/2 + '');
      return false;
}
function OpenDBWizardForEdit(p_iDsnid)
{
  p_iDsnid = document.getElementById('dsnid').value;
  if(!ConfirmSave())
	{
		window.open('ModifyDataSourceInformation.aspx?InvokedFrom=EditDsn&dsnid='+p_iDsnid, 'secDSN11', 'Width=650,Height=400,status=yes,top=' + (screen.availHeight-400)/2 + ',left=' + (screen.availWidth-650)/2 + '');
	}
	return false;
}
function ApplyBool(frmElt)
{
//	var eltMirror = window.document.all[frmElt.name]
//	eltMirror.value="";

//	if(!frmElt.checked)
//		eltMirror.value="False";
	
	setDataChanged(true);
}
function ConvertTime(sParamTime)
{
	if(sParamTime=="")
		return "";
	var sTime=new String(sParamTime);
	return sTime.substr(0,2) + ":" + sTime.substr(2,2);
}
function IsPreDsnAlreadySelected(p_sDesiredName,p_sByPassChkFor)
{
    var arrLinks=window.opener.parent.frames['LeftTree'].document.getElementsByTagName('body')[0].getElementsByTagName("a");
	for(i=0;i<arrLinks.length;i++)
	{
	  var sLinkVal=arrLinks[i].outerHTML
	  var iPos1=sLinkVal.indexOf("onclick")
	  var sOnClickVal = sLinkVal.substr(iPos1)
	  var arrOnClickParams=sOnClickVal.split(",")
	  var sVal =arrOnClickParams[1]
	  if( arrOnClickParams[1] == "'Datasource'")
	  {
	   if(p_sByPassChkFor!="")
	    {
	    if(TrimIt(p_sDesiredName.toLowerCase())==TrimIt(arrLinks[i].outerText.toLowerCase()) && TrimIt(p_sByPassChkFor.toLowerCase())!=TrimIt(arrLinks[i].outerText.toLowerCase()))
	     {
	     return true
	     }
	    }
	    else
	    {
	     if(TrimIt(p_sDesiredName.toLowerCase())==TrimIt(arrLinks[i].outerText.toLowerCase()))
	     {
	       return true
	     }
	    }
	  }
	}
	 return false
}
function CheckForUserSameName(p_sLastName, p_sFirstName, p_iUserDuplicateCount) {
    var sCompareString = '|' + p_sFirstName + ' ' + p_sLastName + '|';
    var usernamestring = window.parent.frames['LeftTree'].document.getElementById('usernamestring').value;
    var iPos = usernamestring.toLowerCase().indexOf(sCompareString.toLowerCase());
    //mbahl3 Mits 31088
    if (iPos >= 0) {
        //mbahl3 Mits  31088
        return false;
    }
    else
        return true;
	/*
	var arrLinks=window.parent.frames['LeftTree'].document.getElementsByTagName('body')[0].getElementsByTagName("a");
	var sDesiredName=TrimIt(p_sFirstName)+" "+TrimIt(p_sLastName)
	var iUserDuplicateCount=0;
    
	for(i=0;i<arrLinks.length;i++)
	{
	  var sLinkVal=arrLinks[i].outerHTML
	  var iPos1=sLinkVal.indexOf("onclick")
	  var sOnClickVal = sLinkVal.substr(iPos1)
	  var arrOnClickParams=sOnClickVal.split(",")
	  var sVal =arrOnClickParams[1]
	  if( arrOnClickParams[1] == "'User'")
	  {
	    if(sDesiredName.toLowerCase()==TrimIt(arrLinks[i].outerText.toLowerCase()))
	    {
	     iUserDuplicateCount++
	     if(p_iUserDuplicateCount==iUserDuplicateCount)
	     {
	       return false
	     }
	    }
	  }
	}
	 return true
	 */
}
function MGrpWithSameNameExists(p_sDesiredName)
{
	var arrLinks=window.opener.parent.frames['LeftTree'].document.getElementsByTagName('body')[0].getElementsByTagName("a");
	for(i=0;i<arrLinks.length;i++)
	{
	  var sLinkVal=arrLinks[i].outerHTML
	  var iPos1=sLinkVal.indexOf("onclick")
	  var sOnClickVal = sLinkVal.substr(iPos1)
	  var arrOnClickParams=sLinkVal.split(",")
	  var sVal =arrOnClickParams[1]
	  if( arrOnClickParams[1] == "'Module Security Group'")
	  {
	    if(TrimIt(p_sDesiredName.toLowerCase())==TrimIt(arrLinks[i].outerText.toLowerCase()))
	    {
	     return true
	    }
	  }
	}
	 return false
}
function ValidateUserEntity(p_iUserDuplicateCount)
{
            var sLastName=TrimIt(window.parent.frames['MainFrame'].document.getElementById("lastname").value)
            var sFirstName=TrimIt(window.parent.frames['MainFrame'].document.getElementById("firstname").value)
            var bRetVal=true
  	        if(sLastName=="" || sFirstName=="")
  	        {
  	            bRetVal=false
  	            alert("Last Name and First Name fields must have non empty value.")
  	            return bRetVal
  	        }
        
	    
	    if(!CheckForUserSameName(sLastName,sFirstName,p_iUserDuplicateCount))
	    {
	        alert('User ' + sFirstName + ' ' + sLastName + ' already exist. Please change last and/or First name and try again.');
	        bRetVal=false
			return bRetVal;
	    }
	    
	    return bRetVal;

}
function PostbackHandlerForUpdates(b_pCalledFrmPopUp,p_iDbId) {
   
  var sAction = ""
  var bIsMainFramePosted=false;
  var bLookForFormPostings=true;
  if(b_pCalledFrmPopUp!=null)
  {
	 sAction=window.opener.parent.frames['LeftTree'].document.getElementById('TreePostedBy').value	
  }
  else
  {
    sAction=window.parent.frames['LeftTree'].document.getElementById('TreePostedBy').value
  }
  var bPostLeftTree=true
  //debugger;
  switch(sAction)
	      {
	            
	            case "AddNewUser" :
	            {
                     window.parent.frames['LeftTree'].document.getElementById('NewUserId').value=p_iDbId
					break;
	            }
	            case "UpdateUserEntity" :
	            {
					break;
	            }
	            
	            case "AddNewDatasource" :
	            {
	                window.opener.parent.frames['LeftTree'].pleaseWait.Show();
                    window.opener.parent.frames['LeftTree'].window.document.forms['frmLeftTree'].submit()
                    window.close()
                    bIsMainFramePosted=true;
					break;
	            }
	            case "UpdateDatasourceEntity" :
	            {
					if(window.parent.frames['MainFrame'].document.getElementById('hdSave').value=="")
					{
					window.parent.frames['MainFrame'].document.getElementById('hdSave').value="Save"
					}
					window.parent.frames['MainFrame'].document.getElementById('hdSave').value="Save"
					bPostLeftTree=false
					break;
	            }
	            case "UpdateUserPermEntity" :
	            {
					break;
	            }
	            case "UpdateModuleGroupEntity" :
	            {
					
					break;
	            }
	            case "RenameModuleGroupEntity" :
	            {
	                window.opener.parent.frames['LeftTree'].pleaseWait.Show();
                    window.opener.parent.frames['LeftTree'].window.document.forms['frmLeftTree'].submit()
                    window.close()
                    bLookForFormPostings=false;
				   break;
	            }
	            case "ChangeLoginInfo" :
	            {
					
		           window.document.getElementById('hdAction').value = "OK"
		           window.document.forms['frmData'].submit()
                   bLookForFormPostings=false;
                   window.close()
				   break;
	            }
	            case "ClearLicenseHistory" :
	            {
					
		           window.document.getElementById('hdAction').value = "Clear"
		           window.document.forms['frmData'].submit()
                   bIsMainFramePosted=true;
				   break;
	            }
	        case "UpdateModuleGroupPermissionEntity":
	            {
	                window.parent.frames['MainFrame'].document.getElementById('PostedByRevokeAndGrantPermissions').value = "false"
	                window.parent.frames['MainFrame'].pleaseWait.Show();
	                window.parent.frames['MainFrame'].document.forms['frmPermissionTree'].submit()
	                bLookForFormPostings = false;
	                break;
	            }
	            
	             case "UpdateLicenses" :
					{
						
					window.document.getElementById('hdAction').value = "UpdateLicenses"
					window.document.getElementById('hdDsnId').value =window.opener.parent.frames['MainFrame'].window.document.getElementById('dsnid').value
					window.document.forms['frmData'].submit()
					bLookForFormPostings=false;
					break;
					}
	            
	            default  :
	            {
                   bLookForFormPostings=false;
                   break;
                 }

	                
	      }
	      if(bLookForFormPostings)
	         {
				
				if(bPostLeftTree)
						{
						    window.parent.frames['LeftTree'].pleaseWait.Show();
						 window.parent.frames['LeftTree'].document.forms['frmLeftTree'].submit()
						}
		      }
		     setDataChanged(false)
  return false;
}
function PopulateGrpName()
{
     var arrIds
     var iHyperlinkIdForSearch
     try
     {
     
     window.document.getElementById('groupname').value=window.opener.parent.frames['LeftTree'].m_nodeText;
	 window.document.getElementById('groupname').focus();
	 window.document.getElementById('groupname').select();
	 return false;
    }
    catch(ex)
    {
    }
  
}
function CloseProgressWindow()
{
     var iHyperlinkIdForSearch
     if(window.parent.frames['LeftTree'] != null)
     {
         if(window.parent.frames['LeftTree'].document.getElementById('SelectedItemTreeId')!=null)
           {
			    iHyperlinkIdForSearch="hyp-"+window.parent.frames['LeftTree'].document.getElementById('SelectedItemTreeId').value
			    if(iHyperlinkIdForSearch!="hyp-")
			    {
				    var arrLinks=window.parent.frames['LeftTree'].document.getElementsByTagName('body')[0].getElementsByTagName("a");
				    for(i=0;i<arrLinks.length;i++)
        			    {
					    if(arrLinks[i].getAttribute("id") == iHyperlinkIdForSearch)
					    {
						    window.parent.frames['LeftTree'].document.getElementById(iHyperlinkIdForSearch).focus()
						    break
					    } 
					    }
			    }
		    }	
	}
	if(self.parent!=null)
	{
		if(haveProperty(self.parent,"wndProgress") && self.parent.wndProgress!=null)
		{
			self.parent.wndProgress.close();
			self.parent.wndProgress=null;
		}
	}
}
function CheckDBName(p_objInputCtrl)
{    
    if(TrimIt(window.event.srcElement.value)=="")
		{
		document.getElementById("btnFinish").disabled=true
		return false;
		}
	if(IsPreDsnAlreadySelected(window.event.srcElement.value,document.getElementById("hdOrigEditDbName").value))
	     document.getElementById("btnFinish").disabled=true
	else
	    document.getElementById("btnFinish").disabled=false
}
function OnCancel()
	{
	  setDataChanged(false)
	  window.close()
	}
function ValidateLoginInfo()
{
  //rsolanki2 : domain authentication updates
//Parijat: Mits 11689/11691
  if ( (m_bIsDomainAuthenticationEnabled+'').toLowerCase()!="true")
  {
  if(TrimIt(document.getElementById("newlogin").value)=="" || TrimIt(document.getElementById("newpasswd").value)=="" || TrimIt(document.getElementById("confirmpasswd").value)=="")
    {
      alert("Login User Name or Password can't be empty")
      return false;
        }
        else
        {
          if(TrimIt(document.getElementById("newpasswd").value)!=document.getElementById("confirmpasswd").value)
           {
             alert("Password confirmation not same as password.")
             return false;
           }
           else
            {
              return true;
            }
         } 
    }
   else
    {
       if(TrimIt(document.getElementById("newpasswd").value)!=document.getElementById("confirmpasswd").value)
       {
         alert("Password confirmation not same as password.")
         return false;
       }
       else if(TrimIt(document.getElementById("newlogin").value)=="" )
       {
         alert("Login User Name can't be empty")
         return false;
       }
       else
       {
         return true;
       }
    } 
}
function ValidateDatasourceSave()
{
		if(TrimIt(window.parent.frames['MainFrame'].document.getElementById('ipdocpath').value)=="")
		{
			alert("Datasource Document Path is not set. You won\'t be able to use any feature related to user documents/attachments.")
		}

		   return true		
}
function ValidateUserPermSave()
{	
        //rsolanki2 : Domain authrntication updates.
//Parijat: Mits 11689/11691
        if ((m_bIsDomainAuthenticationEnabled+'').toLowerCase()!="true")
        {
        if(TrimIt(window.parent.frames['MainFrame'].document.getElementById('password').value)=="" || TrimIt(window.parent.frames['MainFrame'].document.getElementById('loginname').value)=="" )
		{
			alert("Login Name and Password must contain non-empty value.")
			return false;
		    }
		    var sLoginName = window.parent.frames['MainFrame'].document.getElementById('loginname').value
		    if(sLoginName.indexOf(' ')!=-1)
		    {
		    alert("Login Name may not contain space(s).")
		    return false ;
		    }
		}
		else
		{
		    if(TrimIt(window.parent.frames['MainFrame'].document.getElementById('loginname').value)=="" )
		    {
			    alert("Login Name must contain non-empty value.")
			    return false;
		    }
		    var sLoginName = window.parent.frames['MainFrame'].document.getElementById('loginname').value
		    if(sLoginName.indexOf(' ')!=-1)
		    {
		    alert("Login Name may not contain space(s).")
		    return false ;
		    }
		}
		return true;
}

function ValidateEmailOpts()
{
  var bRet=true


  if(window.parent.m_DataChanged)
    {
     
      bRet=confirm("You are about to change E-Mail options. Do you want to continue?")
      if(bRet)
        {
          return bRet;
        }
       else
        {
          return bRet;
        } 
        
    }
    else
    {
     setDataChanged(false)
     window.close()
    }
  
  
}
function ValidateDomainOpts()
{
  var bRet=true
  //debugger;
  if(window.parent.m_DataChanged)
    {
     
      bRet=confirm("You are about to change Domain authentication options. Do you want to continue?")
      if(bRet)
        {
          return bRet;
        }
       else
        {
          return bRet;
        } 
        
    }
    else
    {
     setDataChanged(false)
     window.close()
    }
  
  
}

function ValidateDomainModifyOpts()
{
  var bRet=true
  //debugger;
  if(window.parent.m_DataChanged)
    {
     
//      bRet=confirm("You are about to change Domain authentication options. Do you want to continue?")
//      if(bRet)
//        {
          return bRet;
//        }
//       else
//        {
//          return bRet;
//        } 
//        
    }
    else
    {
     setDataChanged(false)
     window.close()
    }
  
  
}

function GoBack(p_sEntityType)
{
   try
    {
    
      switch(p_sEntityType)
       {
         case "SecurityEntities":
          {
            window.parent.frames['LeftTree'].document.write(m_sLeftTreeRead)
            //window.parent.frames['LeftTree'].location.href="home?pg=riskmaster/SecurityMgtSystem/SecurityEntities"
            window.parent.frames['LeftTree'].location.href="SecurityEntities.aspx"
            break;
          }
          case "PermissionEntities":
          {
            window.parent.frames['MainFrame'].document.write(m_sRead)
            window.parent.frames['MainFrame'].window.history.back()
            break;
          }
          default:
          {
           break;
          }
       } 
    }
    catch(p_objExp)
    {
    }
   
}
function MakeAddParamsVisible(obj)
{
      if(obj.checked)
       {
        document.getElementById("DataSourceWizard_txtAreaAddParams").style.display="inline"
        document.getElementById("DataSourceWizard_txtAreaAddParams").focus()
       }
      else
      {
        document.getElementById("DataSourceWizard_txtAreaAddParams").style.display="none"
        
      }
           
     
}
function CheckForRequiredValues(p_sEntityType)
{
  switch(p_sEntityType)
  {
    case "ServerLogin": 
     {
        if(TrimIt(document.getElementById("inputServerName").value)=="")
          {
            document.getElementById("btnNext").disabled=true
          }
         break;
     }
     case "FileLogin": 
     {
       /*Need to have check for FileLogin entity.*/
       break;
     }
     case "DbLicense": 
     {
      
       if(IsPreDsnAlreadySelected(TrimIt(document.getElementById("inputDatasourcename").value),document.getElementById("hdOrigEditDbName").value))
          {
            document.getElementById("btnFinish").disabled=true
          }
        else
         {
           if(TrimIt(document.getElementById("inputDatasourcename").value)!="")
             document.getElementById("btnFinish").disabled=false
                 
         }  
         break;
     }
     default:
     {
      break;
     }
  }
}
function IsDataChanged()
{
 return window.parent.m_DataChanged
}
function OnKeyupDsnWizardFields(p_sEntityType)
{
  switch(p_sEntityType)
  {
    case "ServerLogin": 
     {
        if(TrimIt(document.getElementById("DataSourceWizard_inputServerName").value)!="")
          {
            document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton').disabled=false
          }
         else
         {
          document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton').disabled=true
         } 
         break;
     }
     case "FileLogin": 
     {
       if(TrimIt(document.getElementById("inputServerName").value)!="")
          {
            document.getElementById("btnNext").disabled=false
          }
         else
         {
          document.getElementById("btnNext").disabled=true
         } 
         break;
       
     }
     default:
     {
      break;
     }
  }

} 
function ChangePasswordInitial()
{                 
                  if(window.opener.parent.frames['LeftTree'].document.getElementById('SelectedEntityFromLeftTree').value=="PermUser")
                  {
                    window.opener.parent.frames['MainFrame'].document.getElementById('password').value =document.getElementById("newpasswd").value ;
                    setDataChanged( true )
                    window.close();
                    return ;
                  }
                  document.getElementById('hdAction').value="OK"
                  window.opener.parent.frames['LeftTree'].document.getElementById('hdShowPasswordScreen').value = "0"
                  if(window.opener.parent.frames['MainFrame'].document.getElementById('hdPwdFrmUserScreen')!=null)
                  {
                    window.opener.parent.frames['MainFrame'].document.getElementById('hdPwdFrmUserScreen').value =  document.getElementById('newpasswd').value
                    window.opener.parent.frames['MainFrame'].frames.SaveSettings(null) ;
                    return ;
                  }
                  window.document.forms['frmData'].submit()
                 
}
function UserOption(p_objCheckBoxCtrl, p_iUserId)
{
  if(p_objCheckBoxCtrl.checked)
  {
    if(document.getElementById('SelectedUserIds').value.indexOf(p_iUserId)==-1)
       {
          if(document.getElementById('SelectedUserIds').value=="")
            document.getElementById('SelectedUserIds').value = p_iUserId
           else
            {   
              document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value + "," + p_iUserId
            } 
       }
  }
  else
  {
    if(document.getElementById('SelectedUserIds').value.indexOf(",")==-1)
    {
       document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value.replace(p_iUserId,"")
    }   
    else
     {  
        if(document.getElementById('SelectedUserIds').value.indexOf(p_iUserId+",")==-1)
        {
          document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value.replace(p_iUserId,"")
        }
        else
        {
         document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value.replace(p_iUserId+",","")
        }
     }
  }
}
function UserOptionUnderModifications(p_objCheckBoxCtrl, p_iUserId)
{
  if(p_objCheckBoxCtrl.checked)
  {
    if(document.getElementById('SelectedUserIds').value.indexOf("," + p_iUserId + ",")==-1)
       {
          if(document.getElementById('SelectedUserIds').value=="")
            document.getElementById('SelectedUserIds').value = "," + p_iUserId + ","
           else
            {   
              document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value + "," + p_iUserId + ","
            } 
       }
  }
  else
  {
    if(document.getElementById('SelectedUserIds').value.indexOf(",")==-1)
    {
       document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value.replace(p_iUserId,"")
    }   
    else
     {  
        if(document.getElementById('SelectedUserIds').value.indexOf(p_iUserId+",")==-1)
        {
          document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value.replace(p_iUserId,"")
        }
        else
        {
         document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value.replace(p_iUserId+",","")
        }
     }
  }
}
function SelectAllUsers(p_objCtrl)
{
   var arrLinks=document.getElementsByTagName('body')[0].getElementsByTagName("input");
   var iCount = arrLinks.length
   for(iTmp=0; iTmp<iCount;iTmp++)
      {
        var objCtrl = arrLinks[iTmp]
        if(objCtrl.type=="checkbox")
        {
          if(objCtrl.id=="SingleCheckbox")
            {
              if(p_objCtrl.checked)
                 objCtrl.checked = true
              else
                 objCtrl.checked = false 
            }
        }
        else
        {
        if(objCtrl.type=="hidden")
        {
          if(objCtrl.id=="userid")
            {
                if(p_objCtrl.checked)
                 {
					if(document.getElementById('SelectedUserIds').value=="")
					{
						document.getElementById('SelectedUserIds').value = objCtrl.value
					}
					else
					{
						if(document.getElementById('SelectedUserIds').value.indexOf(objCtrl.value)==-1)
						{
						document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value + "," + objCtrl.value
						}
					}
				  }
				  else
				  {
				      document.getElementById('SelectedUserIds').value=""
				  }
            }
          }
        }
      }
}
function SelectAllUsersUnderModifications(p_objCtrl)
{
  var arrLinks=document.getElementsByTagName('body')[0].getElementsByTagName("input");
   var iCount = arrLinks.length
   for(iTmp=0; iTmp<iCount;iTmp++)
      {
        var objCtrl = arrLinks[iTmp]
        if(objCtrl.type=="checkbox")
        {
          if(objCtrl.id.toString().search("SingleCheckbox")!=-1)
            {
              if(p_objCtrl.checked)
                 objCtrl.checked = true
              else
                 objCtrl.checked = false 
            }
        }
        else
        {
//        if(objCtrl.type=="hidden")
//        {
         if(objCtrl.id.toString().search("userid")!=-1)
            {
                if(p_objCtrl.checked)
                 {
					if(document.getElementById('SelectedUserIds').value=="")
					{
						document.getElementById('SelectedUserIds').value = "," + objCtrl.value + ","
					}
					else
					{
						if(document.getElementById('SelectedUserIds').value.indexOf("," + objCtrl.value + ",")==-1)
						{ 
						document.getElementById('SelectedUserIds').value = document.getElementById('SelectedUserIds').value  + "," + objCtrl.value + ","
						}
					}
				  }
				  else
				  {
				      document.getElementById('SelectedUserIds').value=""
				  }
            }
//          }
        }
      }
}
function ExportSelectedUsers()
{
  var arrUserIds = document.getElementById('SelectedUserIds').value.split(",") 
  for(iCount=0; iCount<arrUserIds.length;iCount++)
  {
    if(arrUserIds[iCount]!="")
     {
       return true;
     }
  }
  alert("Please select User(s) to export.")
  return false;
  
}
function CheckStatus(p_iSubmitInterval)
{
  if(document.getElementById('Status').value=="1")
  {
   setTimeout("SubmitForm()",p_iSubmitInterval);

  }
}
function SubmitForm()
{
window.document.forms['frmData'].submit()
}
function ToUserListingPage()
{
  window.location.href='home?pg=riskmaster/SecurityMgtSystem/OpenLdapExporter';
  return true ;
}
/* //Arnab: MITS-10662 - Commented and Modified method as there will be no login name
function IsAdminUser()
{
   var sLoginName = document.all["loginname"].value
  if( document.all["hdIsSMSAdminUser"].value =="True" )
  {
   alert("Login Name \'" + sLoginName + "\' is a Security Admin User and can not be denied access to Security Mgt. System.") ;
   document.all["SMSAccess"].checked = true ;
  } 
  
}
*/
//Arnab: MITS-10662 - Modified method
function IsAdminUser(ctrl)
{            
      if( document.getElementById("hdIsSMSAdminUser").value =="True" )
      {
           alert("User is a Security Admin User and can not be denied access to Security Mgt. System.") ;
           document.getElementById("SMSAccess").checked = true ;
      }
      
      setDataChanged(true);
}

function SupressLength(p_objCtrl)
{
	var iMaxLength = 8;
	
	//get the maxlength from the control
	if( p_objCtrl != null )
	{
		if( !isNaN( p_objCtrl.maxLength) )
			iMaxLength = p_objCtrl.maxLength;
	}
	
	if(window.event.srcElement.value.length >= iMaxLength)
	{
		p_objCtrl.value = p_objCtrl.value.substr(0,iMaxLength) ;
		setDataChanged(true);
	} 
}
function ShowPasswordScreen(p_sRequestingPage)
{

 
 try
 {

   if(!IsPwdPolicyEnabled())
   {
     return false
   }
    if(p_sRequestingPage=="UserPerm")
    {
      window.parent.frames['LeftTree'].document.getElementById('hdShowPasswordScreen').value=0
    }
    else
    {
      if(p_sRequestingPage=="Login")
      {
        document.location.href="ChangePassword.aspx?showoldpwd=1&invokedfrom=pwdexpire";
	     return false ;
      }
    }
	if(window.parent.frames['LeftTree'].document.getElementById('hdShowPasswordScreen')!=null)
	{
		if(window.parent.frames['LeftTree'].document.getElementById('hdShowPasswordScreen').value==1)
			{
			  if(window.parent.frames['MainFrame'].document.getElementById('lstShowDatasources').options[0]!=null)
			  {
			    return false;
			  }
			  //var returnval = showModalDialog( "home?pg=riskmaster/SecurityMgtSystem/ChangePassword&amp;showoldpwd=0" , null , "dialogHeight:200px;dialogWidth:200px;resizable:no;status:no;scroll:no;help:no;center:yes;" );
			  //MITS 17812 : Raman Bhatia
			  
			  var sUrl = "ChangePassword.aspx?showoldpwd=0&lastname=" + window.parent.frames['MainFrame'].document.getElementById('lastname').value + "&firstname=" + window.parent.frames['MainFrame'].document.getElementById('firstname').value;
			  window.open(sUrl, 'ChangePassword', 'Width=650,Height=300,status=yes,top=' + (screen.availHeight - 150) / 2 + ',left=' + (screen.availWidth - 390) / 2 + '');
			return true ;
			}
			else
				{
					if(p_sRequestingPage=="UserPerm")
					{
					    //var returnval = showModalDialog( "home?pg=riskmaster/SecurityMgtSystem/ChangePassword&amp;showoldpwd=0" , null , "dialogHeight:200px;dialogWidth:200px;resizable:no;status:no;scroll:no;help:no;center:yes;" );
						window.open('ChangePassword.aspx?showoldpwd=0', 'ChangePassword', 'Width=650,Height=300,status=yes,top=' + (screen.availHeight-150)/2 + ',left=' + (screen.availWidth-390)/2 + '');
						return false ;

					}
				}
	}
 }
 catch(ex)
	{return false;}
 return false;
}
function NavigateToRM()
{
         document.location.href="../Home/Status.aspx";
	     return false ;
}
function ValidateComplexity(p_objCtrl)
{

 
  if(p_objCtrl!=null)
  {
	if(p_objCtrl.value == "")
	{
	return ;
    }
  }
  if(!IsPwdPolicyEnabled())
  {
    this.SupressLength(p_objCtrl)
    return ;
    
  }
 
  objAJAXManager = new AJAXManager()
  objAJAXManager.Init()
  objAJAXManager.CallbackFunction(this.state_Change)
  
  var sData = GetDataForPasswordComplexity(p_objCtrl)
  
  objAJAXManager.ProcessRequest("POST","home?pg=riskmaster/SecurityMgtSystem/ValidatePasswordComplexity",true,sData)
  
  return;
}
function GetDataForPasswordComplexity(p_objCtrl)
{
   if(window.opener!=null)
     {
       var sData = "Userid="
		if(window.opener.parent.frames['MainFrame'].document.getElementById('userid')!=null)
			{
				sData = sData + escape(window.opener.parent.frames['MainFrame'].document.getElementById('userid').value)
			}
			sData = sData + "&LastName="
			if(window.opener.parent.frames['MainFrame'].document.getElementById('lastname')!=null)
			{
				sData = sData + escape(window.opener.parent.frames['MainFrame'].document.getElementById('lastname').value)
			}
			sData = sData + "&FirstName="
			if(window.opener.parent.frames['MainFrame'].document.getElementById('firstname')!=null)
			{
				sData = sData + escape(window.opener.parent.frames['MainFrame'].document.getElementById('firstname').value)
			}
			sData = sData + "&Password=" + escape(p_objCtrl.value);
			return sData ;
	  }
	else
	  {
	     
	      var sData = "LoginName="
	      if(document.getElementById("hdLoginName")!=null)
			{
				sData = sData + escape(document.getElementById("hdLoginName").value)
			}
			
			sData = sData + "&Password=" + escape(p_objCtrl.value);
			return sData ;
	  }  		
		
}
function ChangePassword()
{
 return ChangePasswordMain(document.getElementById("oldpasswd"),document.getElementById("newpasswd"),document.getElementById("confirmpasswd") )
}
function ChangePasswordMain(p_objCtrlOldpasswd, p_objCtrlNewpasswd, p_objCtrlConfirmpasswd) {
    
  if(p_objCtrlOldpasswd==null)
   {
     if( TrimIt(p_objCtrlNewpasswd.value)==""||TrimIt(p_objCtrlConfirmpasswd.value)=="" )
		{
			alert("Password can not be empty.")
			return false ;
		}
      else
       {
         if(TrimIt(p_objCtrlNewpasswd.value)!=TrimIt(p_objCtrlConfirmpasswd.value))
          {
            alert("The New Password and Confirm Password entries must be identical.")
             return false ;
          }
          else		// Code Uncommented by csingh7 : MITS 17843
          {
            
              if (window.opener.parent.frames['MainFrame'].document.getElementById('filepath') != null)
             {
                    window.opener.parent.frames['MainFrame'].document.getElementById('password').value =document.getElementById("newpasswd").value ;
                    window.opener.parent.m_DataChanged = true;
                    if (document.getElementById("tbByPassService") != null)	// Null Check Added by csingh7 : MITS 17843
                        document.getElementById("tbByPassService").value = false;
                    window.opener.parent.frames['MainFrame'].SaveSettings(null);
                    pleaseWait.Show();
                    window.close();
                    return false;
                  }
                  window.opener.parent.frames['LeftTree'].document.getElementById('hdShowPasswordScreen').value = "0"
                  if(window.opener.parent.frames['MainFrame'].document.getElementById('hdPwdFrmUserScreen')!=null)
                  {
                    window.opener.parent.frames['MainFrame'].document.getElementById('hdPwdFrmUserScreen').value =  document.getElementById('newpasswd').value
                    window.opener.parent.frames['MainFrame'].SaveSettings(null) ;
                    window.close();
                    return false;
                  }     
          }		// Code Uncommented by csingh7 : MITS 17843 End
       } 
   }
  else
   {
       if(TrimIt(p_objCtrlOldpasswd.value)=="" ||TrimIt(p_objCtrlNewpasswd.value)==""||TrimIt(p_objCtrlConfirmpasswd.value)=="")
           {
             alert("Password can not be empty.")
             return false;
           }
       else
          {
			if(TrimIt(p_objCtrlNewpasswd.value)!=TrimIt(p_objCtrlConfirmpasswd.value))
			{
				alert("The New Password and Confirm Password entries must be identical.")
				return false ;
			}
           else
			{
				if(TrimIt(p_objCtrlOldpasswd.value)==TrimIt(p_objCtrlNewpasswd.value))
				{
					alert("Old Password and New Password cannot be the same.")
					return false ;
				}
				else
				{
				    document.getElementById('hdAction').value="OK"
				    if(document.getElementById("hdInvokedFrom")!=null)
                      {
						if(document.getElementById("hdInvokedFrom").value=="pwdexpire")  
							{
								// window.document.forms['frmData'].submit();
								//pleaseWait.Show();
								return true;
							}
						else
						  {
						      if(document.getElementById("hdInvokedFrom").value=="chgpwd")
						        {
						          // window.document.forms['frmData'].submit();
							    //	pleaseWait.Show();
							    	return true;
						        }
						  }	
                      }
					
					//window.parent.frames['LeftTree'].document.all['hdShowPasswordScreen'].value = "0"
					//if(window.parent.frames['MainFrame'].document.all['hdPwdFrmUserScreen']!=null)
					//{
					//	window.parent.frames['MainFrame'].document.all['hdPwdFrmUserScreen'].value =  document.all['newpasswd'].value
					//}
		                
					//window.document.forms['frmData'].submit()
					SaveSettings(null) ;
				}
			} 
       }    
   } 
 
  }
  function IsPwdPolicyEnabled()
  {
    //tkr 6/2007.  I could not determine why the bottom half of this function
    //exists, so I left it alone.  repaired top part so that it works on initial configuration,
    //when the 'LeftTree' frame does not exist.  Note that the PasswordPolicyEnabled boolean
    //is stored in both frames with different names.
    var frame = null;
    var hiddenField = null;
    
    frame = window.parent.frames['LeftTree'];
    if(frame != null)
    {
        hiddenField = frame.document.getElementById('hdIsPwdPolicyEnabled');
    }
    if(hiddenField == null)
    {
        frame = window.parent.frames['MainFrame']; 
        if(frame != null)
        {
            hiddenField=frame.document.getElementById('hdEnhSecurity');
        }
    }
    if(hiddenField != null)
    {
        if(hiddenField.value == "False")
            return false;
        else
            return true;
    }
    //tkr the rest of the function I'm leaving as-is.
      
     if(window.opener!=null)
		{
			if(window.opener.parent.parent.frames[0].document.getElementById("IsEnhSecurity")!=null)
			{
				if(window.opener.parent.parent.frames[0].document.getElementById("IsEnhSecurity").value == "False")
				{
				    if(document.getElementById("btnOK")!=null)
					document.getElementById("btnOK").disabled = false;
					return false;
				}
			}
		}
  else
    {
		if(parent.parent.frames[0].document.getElementById("IsEnhSecurity")!=null)
			{
			if( parent.parent.frames[0].document.getElementById("IsEnhSecurity").value == "False")
				{
		         
				 if(document.getElementById("btnOK")!=null)
					document.getElementById("btnOK").disabled = false;
				return false;
				}
			}
		  else
		    {
		       if(parent.parent.parent.frames[0].document.getElementById("IsEnhSecurity")!=null)
		         { 
		            if(parent.parent.parent.frames[0].document.getElementById("IsEnhSecurity").value == "False")//for MDI
						{
				         
						if(document.getElementById("btnOK")!=null)
							document.getElementById("btnOK").disabled = false;
						return false;
						}
		         }
		    }	
    }
    return true ;
  }
function LockUnlockSelUsers()
{
  var arrUserIds = document.getElementById('SelectedUserIds').value.split(",") 
  for(iCount=0; iCount<arrUserIds.length;iCount++)
  {
    if(arrUserIds[iCount]!="")
     {
         pleaseWait.Show();
       return true;
     }
  }
  alert("Please select atleast one User.")
  return false;
  
}


// rsolanki2 : Domain authentication updates
// added grid functions 
function validateGridForDeletion(listname)
{
	var selectedValue=0;
	var SelectedFound=false;
	var selectedId = "";
	var DomainName ="" ;
	
//	if (listname=='WPAAutoDiaryList')
//		return ValidateWPAAutoDiaryGridForDeletion();		
	inputs = document.all.tags("input");
	for (i = 0; i < inputs.length; i++)
	{
		if ((inputs[i].type=="radio") && (inputs[i].checked==true))
		{
//			inputname=inputs[i].id.substring(0,inputs[i].id.indexOf("|"));
            inputname=inputs[i].parentElement.nextSibling.firstChild.nodeValue;
			selectedValue=inputs[i].value;
				SelectedFound=true;
				if(inputs[i].id!=null)
				{
				    selectedId = inputs[i].id;
				    if (listname='DomainTableList')
				        DomainName = inputs[i].parentElement.nextSibling.firstChild.nodeValue;
				    break;
				}
		}
	}
	
	if (SelectedFound==false)
	{
		alert('Please select a record to delete.');
		return false;
	}
	else
	{
		switch(listname)
		{	
			
			case "DomainTableList":
				{
				    if(!self.confirm("Are you sure you want to delete the selected Domain?"))
				    {
					    return false;
				    }
				    //var sequenceid = selectedId + '|SequenceId|';
				    document.getElementById('sequenceid').value = DomainName;
//				    document.getElementById('hdAction').value = "delete";
//				    window.document.forms['frmData'].submit()
                   window.document.getElementById('hdAction').value = "OK"
		           window.document.forms['frmData'].submit();
		           //window.navigate('DomainAuthentication.aspx');//, 'DomainAuthentication', 'Width=450,Height=300,status=yes,top=' + (screen.availHeight-150)/2 + ',left=' + (screen.availWidth-390)/2 + '');	
                   //bLookForFormPostings=false;
				    return false;
				}
		}
	}
	
//	if (listname=="EntityXContactInfoGrid" || listname=="EntityXOperatingAsGrid" || listname == "FuturePaymentsGrid" || listname == "ThirdPartyPaymentsGrid" || listname =="LeaveDetailGrid" || listname == "ExposureListGrid")
//		setDataChanged(false);
	return true;
}

//             var maxNoOfPortlets=6;
//             var count=0, ObjTR=null;                 
//             while (true)
//             {    
//                 ObjTR=document.getElementById('PortalCustomizationGrid|'+( count + 4) + '|TR||');
//                 if (ObjTR !=null) 
//                    count++;
//                 else  
//                    break;      
//             }
//             if (count>=maxNoOfPortlets)
//             {
//                window.alert('Cannot add Portlet.\nOnly a maximum of ' + maxNoOfPortlets +' portlets are allowed');
//                return false;
//             } 


//rsolanki2 : Domain authentication updates
function openGridAddEditWindow(listname,mode,width,height,otherParams)
{
	var openpage;
	var pagepath;
	var iPositionForSelectedGridRow;
		
	if (mode=="add")
	{	 
	    if (listname=="DomainTableList")
        {
                openpage="DomainAuthenticationModify.aspx";
	            pagepath="/SecurityMgtSystem/DomainAuthenticationModify";
	            width=300;
	            height=170;
        }
		window.open(openpage,'DomainAuthenticationModify','Width=' + width + ',Height='+ height +',status=yes,top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+'');		
		//window.open('DomainAuthentication.aspx', 'DomainAuthentication', 'Width=450,Height=300,status=yes,top=' + (screen.availHeight-150)/2 + ',left=' + (screen.availWidth-390)/2 + '');
	}
	else
	{
	    // edit mode
		var CheckedFound=false;
		inputs = document.all.tags("input");
		var sDomainEnableValue="";
		for (i = 0; i < inputs.length; i++)
		{
			if ((inputs[i].type=="radio") && (inputs[i].checked==true))
			{
				//inputname=inputs[i].id.substring(0,inputs[i].id.indexOf("|"));				
				inputname=inputs[i].parentElement.nextSibling.firstChild.nodeValue;
				sDomainEnableValue=inputs[i].parentElement.nextSibling.nextSibling.nextSibling.innerText;				
				CheckedFound=true;
				break;
			}
		}
		if (CheckedFound==false)
		{
			alert('Please select a record to edit.');
			return false;
		} 
		else 
    		//if (listname==inputname)
		if (listname=='DomainTableList')	
		{	
		    openpage="DomainAuthenticationModify.aspx";
            pagepath="/SecurityMgtSystem/DomainAuthenticationModify";
            width=300;
            height=170;					
//			window.open(openpage+'&amp;edit=true&amp;selectedid='+inputname+'&amp;enabled='+sDomainEnableValue,openpage, 'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=no,scrollbars=yes');
			window.open(openpage+'?edit=true&amp;selectedid='+inputname+'&amp;enabled='+sDomainEnableValue,'DomainAuthenticationModify','Width=' + width + ',Height='+ height +',status=yes,top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+'');		
		}
	}
	return false;
}

//rsolanki2 : used for parsing Querystring
function getQval(name)
{
    var m, Q = window.location.search.substring(1);
    if ('' != Q)
    {
        var re = new RegExp(escape(name) + '=([^&$]+)');
        if (m = Q.match(re))
            return m[1];
        else 
            return '';
    }
    return '';
}
function SetModuleGroupFlag(value)
{
    m_ModuleGroupFlag = value;
}
function GetModuleGroupFlag()
{
    return m_ModuleGroupFlag;
}
function enableNext()
{
var obj = document.getElementById('DataSourceWizard_StartNavigationTemplateContainerID_StartNextButton');
if(obj!=null)
{
    obj.disabled = false;
    
}
}
function disableNext()
{
var obj = document.getElementById('DataSourceWizard_StartNavigationTemplateContainerID_StartNextButton');
if(obj!=null)
{
    obj.disabled = true;
}
}
function enableStepNext() {
    var obj = document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton')
    if (obj != null) {
        obj.disabled = false;

    }
}
function disableStepNext() {
    var obj = document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton')
    if (obj != null) {
        obj.disabled = true;
    }
}


function ClearRadioSelection()
{
    arrayRadio = document.getElementById("DataSourceGroup");
    for(i=0;i<arrayRadio.length;i++)
    {
        arrayRadio[i].checked = false;
    }
    disableNext()
}
function CustomizePage()
{
    pageNo = m_activestepindex;
    
    switch(pageNo)
    {
        case 0: arrayRadio = document.getElementById("DataSourceGroup");
                if(arrayRadio!=null && arrayRadio.length!=null)
                {
                    disableNext();
                    for(i=0;i<arrayRadio.length;i++)
                    {
                        if(arrayRadio[i].checked)
                        {
                            enableNext();
                        }
                    }
                }
                break;
        case 1: if(document.getElementById('DataSourceWizard_inputServerName') == null || document.getElementById('DataSourceWizard_inputServerName').value!="")
                {
                    document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton').disabled=false;
                }
                else
                {
                    document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton').disabled=true;
                }
                break;
        case 2: document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton').disabled=false;
                break;
        case 3: if(document.getElementById('DataSourceWizard_wizardstep4_iserror').value == "No")
                {
                    if(document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton')!=null)
                    {
                        document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton').disabled=false;
                    }
                }
                if(document.getElementById('DataSourceWizard_FinishNavigationTemplateContainerID_FinishButton')!=null)
                    {
                        if(document.getElementById('hdInvokedFrom').value == "SetDocPath")
                        {
                             if(document.getElementById('connStr').value == "")
                             {
                                    document.getElementById('DataSourceWizard_FinishNavigationTemplateContainerID_FinishButton').disabled=true;
                             }
                             else
                             {
                                document.getElementById('DataSourceWizard_FinishNavigationTemplateContainerID_FinishButton').disabled=false;
                             }
                             
                        }
                        
                    }
                break;
            case 4: if (document.getElementById('hdInvokedFrom').value == "SetDocPath") {
                    if (document.getElementById('connStr').value != "") {
                        ClosePopUpOnSuccess('', 'ExistingDsn');
                    }
                }
                if (document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton') != null) {
                    document.getElementById('DataSourceWizard_StepNavigationTemplateContainerID_StepNextButton').value = "Finish";
                    if (document.getElementById('DataSourceWizard_inputDatasourcename') != null) {
                        if (document.getElementById('DataSourceWizard_inputDatasourcename').value == "") {
                            disableStepNext();
                        }
                        if (document.getElementById('DataSourceWizard_olddatasourcename').value == "") {
                            document.getElementById('DataSourceWizard_olddatasourcename').value = document.getElementById('DataSourceWizard_inputDatasourcename').value
                        }
                        ValidateDataSource();
                    }
                }
                break;
        case 5: if(document.getElementById('DataSourceWizard_hdNewDBId').value != '')
                {
                    ClosePopUpOnSuccess(document.getElementById('DataSourceWizard_hdNewDBId').value , document.getElementById('DataSourceWizard_hdEntityType').value);
                    
                }
                break;
 }   
    
}
function pageLoad()
{
    if(document.getElementById("DataSourceWizard_step1") != null)
        m_activestepindex = 0;
    else if(document.getElementById("DataSourceWizard_step2") != null)
        m_activestepindex = 1;
    else if(document.getElementById("DataSourceWizard_step3") != null)
        m_activestepindex = 2;
    else if(document.getElementById("DataSourceWizard_step4") != null)
        m_activestepindex = 3;
    else if(document.getElementById("DataSourceWizard_step5") != null)
        m_activestepindex = 4;
    else if(document.getElementById("DataSourceWizard_step6") != null)
        m_activestepindex = 5;
    
    var oChkAddParams = document.getElementById('DataSourceWizard_chkAddParams');
    if(oChkAddParams!=null)
    {
        MakeAddParamsVisible(oChkAddParams);
    }
    
    CustomizePage();

}
function VerifyFinish()
{
    if(document.getElementById('hdInvokedFrom').value == "SetDocPath")
    {
         if(document.getElementById('connStr').value != "")
         {
                ClosePopUpOnSuccess('' , 'ExistingDsn');
         }

     }
     
     
}
function SelectAfterPostback(listname) {
    // Get Control Name
    var sControlName = listname.substring(0, listname.length - 4) + 'SelectedId';
    var radioSelect = document.getElementById(sControlName);

    if (radioSelect != null) {
        var gridElementsRadio = document.getElementsByName('MyRadioButton');

        if (gridElementsRadio != null) {
            for (var i = 0; i < gridElementsRadio.length; i++) {
                var gridName = gridElementsRadio[i].name;
                var selectedValue = gridElementsRadio[i].value;
                if (listname == gridName && selectedValue == radioSelect.value) {
                    // Get control
                    gridElementsRadio[i].checked = true;
                }
            }
        }
    }
}
function scrollNodeIntoView() {

    try {
        var name = LTV_Data.selectedNodeID.value;
        var newname = name;

        if (name != "") {
            var iPos = parseInt(name.substring(4));
            if (iPos > 100) {
                iPos = iPos - 25;
                newname = "LTVt" + iPos;
            }
            else if (iPos > 50) {
                iPos = iPos - 25;
                newname = "LTVt" + iPos;
            }
            else if (iPos > 10) {
                iPos = iPos - 4;
                newname = "LTVt" + iPos;
            }
            var selectedNode = document.getElementById(newname);
            if (selectedNode != null) {
                selectedNode.scrollIntoView(true);

                document.getElementById(name).click();
            }
        }
        window.parent.frames['LeftTree'].document.getElementById('UserEntityDsnIdSelected').value = "";
        var PageMainFrame = document.getElementById('PageToBeShownInMainFrame').value; // '<xsl:value-of select="//PostBackResults/PageToBeShownInMainFrame"/>';

        //enable toolbar buttons

        //MITS 19015 Raman Bhatia 12/08/2009
        //Commenting code for removed toolbar buttons.. Also added null check
        if (window.parent.frames['ToolBar'].document.getElementById('btnSave') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnSave').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnRefresh') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnRefresh').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnDelete') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnDelete').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnAddUser') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnAddUser').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnAddDB') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnAddDB').disabled = false;
        }
        //window.parent.frames['ToolBar'].document.getElementById('btnChangeLogin'].disabled = false;
        if (window.parent.frames['ToolBar'].document.getElementById('btnEmailOptions') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnEmailOptions').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnAddModule') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnAddModule').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnRenameModule') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnRenameModule').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnDomainAuthentication') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnDomainAuthentication').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnClearHistoy') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnClearHistoy').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnAssignPermissions') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnAssignPermissions').disabled = false;
        }
        if (window.parent.frames['ToolBar'].document.getElementById('btnRevokePermissions') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnRevokePermissions').disabled = false;
        }
        //Start Add By ttumula2 on 22 Dec 2014 RMA-6033
        if (window.parent.frames['ToolBar'].document.getElementById('btnDateFormat') != null) {
            window.parent.frames['ToolBar'].document.getElementById('btnDateFormat').disabled = false;
        }
        //Start Add By ttumula2 on 22 Dec 2014 RMA-6033
        //openInFrame(document.getElementById('PageToBeShownInMainFrame').value, document.getElementById('entityType').value, document.getElementById('NodeId').value, document.getElementById('idDb').value, "true");
        InitPage();

        

    }
    catch (e) { }
}


function OnTreeClick(evt) {
    var src = window.event != window.undefined ? window.event.srcElement : evt.target;
    
    //alert(window.event.srcElement);
    var nodeClick = src.tagName.toLowerCase() == 'a';
    if (nodeClick) {
        var nodeText = src.innerHTML;
        m_nodeText = nodeText;
        var nodeValue = GetNodeValue(src);
        if ((nodeValue.indexOf("dsnid") > 0 || nodeValue.indexOf("idDb") > 0) && (nodeValue.indexOf("UserPerm.aspx") < 0))
        {
            SetModuleGroupFlag(true);
            //abansal23: Clone for module security groups 
            if(nodeValue.indexOf("idDb") > 0)
                window.parent.frames['LeftTree'].document.getElementById('txtDsnId').value = nodeValue.substring(nodeValue.lastIndexOf('=') + 1);
        }
        else
            SetModuleGroupFlag(false);
        SetParams(nodeValue);
        //Lets Open the appropriate page in right frame
        if (window.parent.frames['MainFrame'] != null) {
            //window.parent.frames['MainFrame'].location.href = nodeValue;
            if (window.parent.frames['MainFrame'].pleaseWait != null) {
                window.parent.frames['MainFrame'].pleaseWait.Show();
            }

            openInFrame(nodeValue, m_EntityType, "", m_IdDB);
        }
        return false;
    }
    return true;
}
function getQueryVariable(query) {
    var sRet = "";
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (sRet == "")
            sRet = pair[1];
        else
            sRet = sRet + ',' + pair[1];
    }
    return sRet;
}

function SetParams(nodeValue) {
    var sType = nodeValue.substring(0, nodeValue.indexOf('.'));
   
    switch (sType) {
        case "User": m_EntityType = "User";
            //m_IdDB = nodeValue.substring(nodeValue.indexOf("userid=") + 7);
            m_IdDB = getQueryVariable(nodeValue);
            break;

        case "Datasource": m_EntityType = "Datasource";
            //m_IdDB = nodeValue.substring(nodeValue.indexOf("dsnid=") + 6);
            m_IdDB = getQueryVariable(nodeValue);
            break;
        case "GrpModuleAccessPermission": m_EntityType = "GrpModuleAccessPermission";
            m_IdDB = getQueryVariable(nodeValue);
            //alert("hi");
            //document.getElementById('SelectedEntityFromLeftTree').value = "Module Security Group";
            break;
        case "UserPerm": m_EntityType = "UserPerm";
            m_IdDB = getQueryVariable(nodeValue);
            break;


        default: m_EntityType = sType;
            
            m_IdDB = getQueryVariable(nodeValue);
            Answer = 0;
            if (m_IdDB != null) {
                RE = new RegExp("[^,]", "gi")
                Answer = m_IdDB.replace(RE, "").length
                switch (Answer) {
                    case 2: m_EntityType = "Module Group User";
                }
            }
            else {
                m_IdDB = "";
            }
            if (nodeValue.indexOf("idDb") > 0) {
                m_IdDB = nodeValue.substring(nodeValue.indexOf("idDb=") + 5);
            }
            else {
                if (m_IdDB == "")
                    m_IdDB = 0;
            }
    }
    document.getElementById('EntityIdForDeletion').value = m_IdDB;

}
function GetNodeValue(node) {
    var nodeValue = "";
    var nodePath = node.href.substring(node.href.indexOf(",") + 2, node.href.length - 2);
    var nodeValues = nodePath.split("\\");
    if (nodeValues.length > 1) {
        nodeValue = nodeValues[nodeValues.length - 1];
        m_nodeDepth = (nodeValues.length - 1) / 2;
    }
    else {
        nodeValue = nodeValues[0].substr(1);
        m_nodeDepth = 0;
    }
    var oOrgSelectedID = 'hd_selectednodevalue';
    var oOrgSelected = document.getElementById(oOrgSelectedID);
    if (oOrgSelected != null) {
        oOrgSelected.textContent = nodeValue; //igupta3 Mits: 33301
    }
    m_CurrentFuncId = nodeValue;
    return nodeValue;
}
function CloseIfSuccess() {
    PopulateGrpName();
    if (document.getElementById('hdAction').value == "OK") {
        ClosePopUpOnSuccess(document.getElementById('hdNewDBId').value, "RenameModuleGroupEntity", document.getElementById('selectedentityid').value);
    }

}
function ValidateDataSource() {
    var sCompareString = '|' + document.getElementById('DataSourceWizard_inputDatasourcename').value.trim() + '|';
    var datasourcestring = window.opener.parent.frames['LeftTree'].document.getElementById('datasourcestring').value;
    var sOlddatasourcename = document.getElementById('DataSourceWizard_olddatasourcename').value;
    if (sOlddatasourcename != "") {
        if (document.getElementById('DataSourceWizard_inputDatasourcename').value == sOlddatasourcename) { //Mits id: 20624
            enableStepNext();
            return true;
        }
    }
    var iPos = datasourcestring.toLowerCase().indexOf(sCompareString.toLowerCase());
    if (iPos >= 0) {
        //alert("Selected Data Source name is already in use.");
        //document.getElementById('DataSourceWizard_inputDatasourcename').focus();
        disableStepNext();
        return false;
    }
    else {
        enableStepNext();
        return true;
    }
}
function SelectAll(id) {
    document.getElementById(id).focus();
    document.getElementById(id).select();
}
function keyDown(e) {
    var n = (window.Event) ? e.which : e.keyCode;
    if (n == 38 || n == 40 || n == 39 || n == 37) return false;
}
     