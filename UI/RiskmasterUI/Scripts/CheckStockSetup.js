m_DataChanged = false;


function Save() 
{
	if( Validate() )
	{
		document.forms[0].test.value = "Save" ;
		document.forms[0].ouraction.value = "PerformAction" ;		
	
		CheckSave();	
		return true ;		
	}
	else
		return false ;
}

function Validate() {
    if (document.forms[0].StockName.value == "" || trimAll(document.forms[0].StockName.value)=="")
	{
	    alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
	    document.getElementById("FromBacktoBankAccounts").value = "";
	    document.forms[0].test.value = "";
		return false ;
	}
	return true ;	
}


function trimAll(sString) {
    while (sString.substring(0, 1) == ' ') {
        sString = sString.substring(1, sString.length);
    }
    while (sString.substring(sString.length - 1, sString.length) == ' ') {
        sString = sString.substring(0, sString.length - 1);
    }
    return sString;
}





function New()
{
	document.forms[0].test.value = "New" ;
	document.forms[0].ouraction.value = "New" ;
	
	if( ConfirmSave() )
		return Save();	
}

//MITS 14769 by Gagan : Start
function ConfirmSave() {
    var ret = false;
    if (m_DataChanged) {
        ret = confirm('Data has changed. Do you want to save changes?'); 
        if (ret == false)
            m_DataChanged = false;
    }
    return ret;
}





function Navigates() {
    
	//document.forms[0].test.value = sDirection ;
	//document.forms[0].ouraction.value = "PerformAction" ;
	
    /*Changed by Saurabh Arora for MITS 18908:Start

    if (ConfirmSave())
        return Save();
    else
        return true;
     */
    if (ConfirmSave())
    {
        var bReturn = Save();
	    if (bReturn == false)
	        return false;
	    m_DataChanged = false;
    }
    return true;
    //Changed by Saurabh Arora for MITS 18908:End
        
}

//MITS 14769 by Gagan : End

function CheckSave()
{
	document.getElementById('StockId1').value=document.getElementById('StockId').value;
	document.getElementById('StockId2').value=document.getElementById('StockId').value;
	document.getElementById('StockId3').value=document.getElementById('StockId').value;
	if((document.forms[0].sb1.value!='0')&&(document.forms[0].file1.value==""))
	{
		//document.forms[0].imageidx1.value='';
	}
	if((document.forms[0].sb2.value!='1')&&(document.forms[0].file2.value==""))
	{
		//document.forms[0].imageidx2.value='';
	}
	if((document.forms[0].sb3.value!='2')&&(document.forms[0].file3.value==""))
	{
		//document.forms[0].imageidx3.value='';
	}
	
	//Changed by Gagan for MITS 10963 : Start
	if(document.getElementById('hdnUnit').value=="Inches")
	{
	    document.forms[0].DateXa.value	= Math.round(document.forms[0].DateXt.value*1440);
	    document.forms[0].DateYa.value = Math.round(document.forms[0].DateYt.value * 1440);
	    //asingh263 mits 32712 starts
	    document.forms[0].EventDateXa.value = Math.round(document.forms[0].EventDateXt.value * 1440);
	    document.forms[0].EventDateYa.value = Math.round(document.forms[0].EventDateYt.value * 1440);
	    document.forms[0].ClaimDateXa.value = Math.round(document.forms[0].ClaimDateXt.value * 1440);
	    document.forms[0].ClaimDateYa.value = Math.round(document.forms[0].ClaimDateYt.value * 1440);
	    document.forms[0].PolicyNumberXa.value = Math.round(document.forms[0].PolicyNumberXt.value * 1440);
	    document.forms[0].PolicyNumberYa.value = Math.round(document.forms[0].PolicyNumberYt.value * 1440);
	    document.forms[0].InsuredNameXa.value = Math.round(document.forms[0].InsuredNameXt.value * 1440);
	    document.forms[0].InsuredNameYa.value = Math.round(document.forms[0].InsuredNameYt.value * 1440);
	    document.forms[0].PayeeAddressXa.value = Math.round(document.forms[0].PayeeAddressXt.value * 1440);
	    document.forms[0].PayeeAddressYa.value = Math.round(document.forms[0].PayeeAddressYt.value * 1440);

	    document.forms[0].AgentAddressXa.value = Math.round(document.forms[0].AgentAddressXt.value * 1440);
	    document.forms[0].AgentAddressYa.value = Math.round(document.forms[0].AgentAddressYt.value * 1440);

	    document.forms[0].PayeeAddOnCheckXa.value = Math.round(document.forms[0].PayeeAddOnCheckXt.value * 1440);
	    document.forms[0].PayeeAddOnCheckYa.value = Math.round(document.forms[0].PayeeAddOnCheckYt.value * 1440);
	    //asingh263 mits 32712 ends
	    document.forms[0].CheckNumberXa.value	= Math.round(document.forms[0].CheckNumberXt.value*1440);
	    document.forms[0].CheckNumberYa.value	= Math.round(document.forms[0].CheckNumberYt.value*1440);	
	    document.forms[0].AmountXa.value	= Math.round(document.forms[0].AmountXt.value*1440);
	    document.forms[0].AmountYa.value	= Math.round(document.forms[0].AmountYt.value*1440);	
	    document.forms[0].AmountTextXa.value	= Math.round(document.forms[0].AmountTextXt.value*1440);
	    document.forms[0].AmountTextYa.value	= Math.round(document.forms[0].AmountTextYt.value*1440);	
	    document.forms[0].ClaimNumberXa.value	= Math.round(document.forms[0].ClaimNumberXt.value*1440);
	    document.forms[0].ClaimNumberYa.value	= Math.round(document.forms[0].ClaimNumberYt.value*1440);	
	    document.forms[0].MemoTextXa.value	= Math.round(document.forms[0].MemoTextXt.value*1440);
	    document.forms[0].MemoTextYa.value	= Math.round(document.forms[0].MemoTextYt.value*1440);	
	    document.forms[0].TransDetailXa.value	= Math.round(document.forms[0].TransDetailXt.value*1440);
	    document.forms[0].TransDetailYa.value	= Math.round(document.forms[0].TransDetailYt.value*1440);	
	    document.forms[0].PayerXa.value	= Math.round(document.forms[0].PayerXt.value*1440);
	    document.forms[0].PayerYa.value	= Math.round(document.forms[0].PayerYt.value*1440);	
	    document.forms[0].PayeeXa.value	= Math.round(document.forms[0].PayeeXt.value*1440);
	    document.forms[0].PayeeYa.value	= Math.round(document.forms[0].PayeeYt.value*1440);		
	    document.forms[0].CheckOffsetXa.value	= Math.round(document.forms[0].CheckOffsetXt.value*1440);
	    document.forms[0].CheckOffsetYa.value	= Math.round(document.forms[0].CheckOffsetYt.value*1440);	
	    document.forms[0].MICROffsetXa.value	= Math.round(document.forms[0].MICROffsetXt.value*1440);
	    document.forms[0].MICROffsetYa.value	= Math.round(document.forms[0].MICROffsetYt.value*1440);
	    document.forms[0].tndOffsetXa.value	= Math.round(document.forms[0].tndOffsetXt.value*1440);
	    document.forms[0].tndOffsetYa.value	= Math.round(document.forms[0].tndOffsetYt.value*1440);   	
	}
	else
	{
	    document.forms[0].DateXa.value	= Math.round(document.forms[0].DateXt.value*1440/2.54);
	    document.forms[0].DateYa.value = Math.round(document.forms[0].DateYt.value * 1440 / 2.54);
	    //asingh263 mits 32712 starts
	    document.forms[0].EventDateXa.value = Math.round(document.forms[0].EventDateXt.value * 1440 / 2.54);
	    document.forms[0].EventDateYa.value = Math.round(document.forms[0].EventDateYt.value * 1440 / 2.54);
	    document.forms[0].ClaimDateXa.value = Math.round(document.forms[0].ClaimDateXt.value * 1440 / 2.54);
	    document.forms[0].ClaimDateYa.value = Math.round(document.forms[0].ClaimDateYt.value * 1440 / 2.54);
	    document.forms[0].PolicyNumberXa.value = Math.round(document.forms[0].PolicyNumberXt.value * 1440 / 2.54);
	    document.forms[0].PolicyNumberYa.value = Math.round(document.forms[0].PolicyNumberYt.value * 1440 / 2.54);
	    document.forms[0].InsuredNameXa.value = Math.round(document.forms[0].InsuredNameXt.value * 1440 / 2.54);
	    document.forms[0].InsuredNameYa.value = Math.round(document.forms[0].InsuredNameYt.value * 1440 / 2.54);
	    document.forms[0].PayeeAddressXa.value = Math.round(document.forms[0].PayeeAddressXt.value * 1440 / 2.54);
	    document.forms[0].PayeeAddressYa.value = Math.round(document.forms[0].PayeeAddressYt.value * 1440 / 2.54);

	    document.forms[0].AgentAddressXa.value = Math.round(document.forms[0].AgentAddressXt.value * 1440 / 2.54);
	    document.forms[0].AgentAddressYa.value = Math.round(document.forms[0].AgentAddressYt.value * 1440 / 2.54);

	    document.forms[0].PayeeAddOnCheckXa.value = Math.round(document.forms[0].PayeeAddOnCheckXt.value * 1440 / 2.54);
	    document.forms[0].PayeeAddOnCheckYa.value = Math.round(document.forms[0].PayeeAddOnCheckYt.value * 1440 / 2.54);
	    //asingh263 mits 32712 ends
	    document.forms[0].CheckNumberXa.value	= Math.round(document.forms[0].CheckNumberXt.value*1440/2.54);
	    document.forms[0].CheckNumberYa.value	= Math.round(document.forms[0].CheckNumberYt.value*1440/2.54);	
	    document.forms[0].AmountXa.value	= Math.round(document.forms[0].AmountXt.value*1440/2.54);
	    document.forms[0].AmountYa.value	= Math.round(document.forms[0].AmountYt.value*1440/2.54);	
	    document.forms[0].AmountTextXa.value	= Math.round(document.forms[0].AmountTextXt.value*1440/2.54);
	    document.forms[0].AmountTextYa.value	= Math.round(document.forms[0].AmountTextYt.value*1440/2.54);	
	    document.forms[0].ClaimNumberXa.value	= Math.round(document.forms[0].ClaimNumberXt.value*1440/2.54);
	    document.forms[0].ClaimNumberYa.value	= Math.round(document.forms[0].ClaimNumberYt.value*1440/2.54);	
	    document.forms[0].MemoTextXa.value	= Math.round(document.forms[0].MemoTextXt.value*1440/2.54);
	    document.forms[0].MemoTextYa.value	= Math.round(document.forms[0].MemoTextYt.value*1440/2.54);	
	    document.forms[0].TransDetailXa.value	= Math.round(document.forms[0].TransDetailXt.value*1440/2.54);
	    document.forms[0].TransDetailYa.value	= Math.round(document.forms[0].TransDetailYt.value*1440/2.54);	
	    document.forms[0].PayerXa.value	= Math.round(document.forms[0].PayerXt.value*1440/2.54);
	    document.forms[0].PayerYa.value	= Math.round(document.forms[0].PayerYt.value*1440/2.54);	
	    document.forms[0].PayeeXa.value	= Math.round(document.forms[0].PayeeXt.value*1440/2.54);
	    document.forms[0].PayeeYa.value	= Math.round(document.forms[0].PayeeYt.value*1440/2.54);		
	    document.forms[0].CheckOffsetXa.value	= Math.round(document.forms[0].CheckOffsetXt.value*1440/2.54);
	    document.forms[0].CheckOffsetYa.value	= Math.round(document.forms[0].CheckOffsetYt.value*1440/2.54);	
	    document.forms[0].MICROffsetXa.value	= Math.round(document.forms[0].MICROffsetXt.value*1440/2.54);
	    document.forms[0].MICROffsetYa.value	= Math.round(document.forms[0].MICROffsetYt.value*1440/2.54);
	    document.forms[0].tndOffsetXa.value	= Math.round(document.forms[0].tndOffsetXt.value*1440/2.54);
	    document.forms[0].tndOffsetYa.value	= Math.round(document.forms[0].tndOffsetYt.value*1440/2.54);   	
	}
	
	//Changed by Gagan for MITS 10963 : End
	
	
}
	
function FileTransfer()
{        
		document.forms[0].image.value=window.opener.document.forms[0].image.value;
		if((document.forms[0].FileSelected.value=='True')&&(document.forms[0].image.value=='0'))
		{
			var objControl=window.opener.document.getElementById('SetImageOne');
			//window.opener.document.forms[0].imageidx1.value='0';
			window.opener.document.forms[0].file1.value=document.forms[0].TempFileCreated.value;
			//alert( document.forms[0].TempFileCreated.value );			
			
			window.opener.document.forms[0].filename1.value=document.forms[0].filename.value;
			
			window.opener.document.forms[0].sb1.value='0';
			if(window.opener.document.forms[0].file1.value!="")
			{
			    var objlblNoImageSetID = window.opener.document.getElementById('lblNoImageSetID'); //Jira id: 7554
			    objlblNoImageSetID.innerHTML = '(Image Set)';
				//objControl.parentNode.previousSibling.previousSibling.previousSibling.childNodes[0].innerHTML='(Image Set)';
			}
			window.opener.m_DataChanged = true ;
			window.close();
					
		}
		else
		{
			if((document.forms[0].FileSelected.value=='True')&&(document.forms[0].image.value=='1'))
			{
				var objControl=window.opener.document.getElementById('SetImageTwo');
				window.opener.document.forms[0].imageidx2.value='1';
				window.opener.document.forms[0].file2.value=document.forms[0].TempFileCreated.value;
				//window.opener.document.forms[0].filename2.value=document.forms[0].filename.value;				
				window.opener.document.forms[0].sb2.value='1';
				if(window.opener.document.forms[0].file2.value!="")
				{
				    //objControl.parentNode.previousSibling.previousSibling.previousSibling.childNodes[0].innerHTML = '(Image Set)';
				    
				    var objlblNoImgSetID = window.opener.document.getElementById('lblNoImgSetID'); //Jira id: 7554
				    objlblNoImgSetID.innerHTML = '(Image Set)';
				}
				window.opener.m_DataChanged = true ;
				window.close();
			}
			else
				if((document.forms[0].FileSelected.value=='True')&&(document.forms[0].image.value=='2'))
				{
					var objControl=window.opener.document.getElementById('SetImageThree');
					window.opener.document.forms[0].imageidx3.value='2';
					window.opener.document.forms[0].file3.value=document.forms[0].TempFileCreated.value;
					//window.opener.document.forms[0].filename3.value=document.forms[0].filename.value;
					window.opener.document.forms[0].sb3.value='2';
					if(window.opener.document.forms[0].file3.value!="")
					{	   					
					    //objControl.parentNode.previousSibling.previousSibling.previousSibling.childNodes[0].innerHTML='(Image Set)';
					    var objlblNoImgeSetID = window.opener.document.getElementById('lblNoImgeSetID'); //Jira id: 7554
					    objlblNoImgeSetID.innerHTML = '(Image Set)';

					}
					window.opener.m_DataChanged = true ;
					window.close();
					
				}
		}	
				
}
	
function toggle_MICRType2()
{
	m_DataChanged = true ;
	document.forms[0].MICRFontName.disabled=false;
	document.forms[0].MICRFontSize.disabled=false;
			
}
function toggle_MICRType1()
{
	m_DataChanged = true ;
	document.forms[0].MICRFontName.disabled=true;
	document.forms[0].MICRFontSize.disabled=true;		
}
function AddressFlagzero()
{
	document.getElementById('hdnAddressFlag').value="0";
}
function AddressFlagone()
{
	document.getElementById('hdnAddressFlag').value="1";
}
function AddressFlagtwo()
{
	document.getElementById('hdnAddressFlag').value="2";
}		
function WindowOpen() {    
    if (document.forms[0].StockId.value == "") {

        alert("You are working on a new record and this functionality is available for existing records only. Please save the data and try again.");
        return false;

    }
        window.open('/RiskmasterUI/UI/BankAccount/CheckStocksClone.aspx?StockId=' + document.forms[0].StockId.value + '&AccountId=' + document.forms[0].AccountId.value, 'CheckStocksPrintSample',
	    'width=500,height=400' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    
	
	//window.open('home?pg=riskmaster/CheckStocks/CheckStocksClone&amp;StockId='+document.forms[0].StockId.value+'&AccountId=' + document.forms[0].AccountId.value,'',"width=500,height=400,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");	
	return false;									
}
function WindowOpenPrintSample() {


    if (document.forms[0].StockId.value == "") 
    {
        alert("You are working on a new record and this functionality is available for existing records only. Please save the data and try again.");
        return false;       
    }
    
	if( m_DataChanged)
	{
	    alert("You must save the current check stock first.");		
		return false ;
	}
	//window.open('home?pg=riskmaster/CheckStocks/CheckStocksPrintSample&amp;StockId='+document.forms[0].StockId.value,'',"width=500,height=400,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");	
//Ashish Ahuja - Pay To The Order Config
window.open('/RiskmasterUI/UI/BankAccount/CheckStocksPrintSample.aspx?StockId=' + document.forms[0].StockId.value + '&SampleText=' + document.forms[0].txtSampleText.value, 'CheckStocksPrintSample',
	    'width=500,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');

	
  // window.open('home?pg=riskmaster/CheckStocks/CheckStocksPrintSample&amp;StockId='+document.forms[0].StockId.value,'',"width=500,height=400,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");	
	
	
	return false;									
}

function PayToTheOrderValidate() {

    var sNumberOfRows = document.forms[0].NoofRowsForPayToOrder.value;
    var sNumberofChar = document.forms[0].NoofCharactersperPayToOrderRow.value;
    if (parseInt(sNumberOfRows, 10) < 0)
    {
        document.forms[0].NoofRowsForPayToOrder.value = "";
    }
    if (parseInt(sNumberofChar, 10) < 0) {
        document.forms[0].NoofCharactersperPayToOrderRow.value = "";
    }

}

function WindowOpenViewImage1()
{
	
	if(document.forms[0].file1.value!='')
	{	
		document.forms[0].image.value='0';
	}
	else
	{
		document.forms[0].image.value='';
	}
	
		document.getElementById('StockId1').value=document.getElementById('StockId').value;								
        //window.open('/RiskmasterUI/UI/BankAccount/CheckStocksClone.aspx?&amp;StockId='+document.forms[0].StockId.value + '&AccountId=' + document.forms[0].AccountId.value,'CheckStocksPrintSample',iew		               
		
		window.open('/RiskmasterUI/UI/BankAccount/ViewImage.aspx?imageid=0&stockid='+ document.getElementById('StockId').value+ '&tempfilename='+ window.document.forms[0].file1.value,'',
			"width=400,height=300,top="+(screen.availHeight-300)/2+",left="+(screen.availWidth-400)/2+",resizable=yes,scrollbars=yes");					
			
		document.forms[0].ViewImage.value='';	
		return false;									
	//return true;									
}
function WindowOpenViewImage2()
{	
	if(document.forms[0].file2.value!='')
	{	
		document.forms[0].image.value='1';
	}
	else
	{
		document.forms[0].image.value='';
	}
	document.getElementById('StockId2').value=document.getElementById('StockId').value;
	
    window.open('/RiskmasterUI/UI/BankAccount/ViewImage.aspx?imageid=1&stockid='+ document.getElementById('StockId').value+ '&tempfilename='+ window.document.forms[0].file2.value,'',
			"width=400,height=300,top="+(screen.availHeight-300)/2+",left="+(screen.availWidth-400)/2+",resizable=yes,scrollbars=yes");					

	//window.open('home?pg=riskmaster/CheckStocks/ViewImage&amp;imageid=1&amp;stockid='+ document.getElementById('StockId').value+ '&amp;tempfilename='+ window.document.forms[0].file2.value,'',
	//		"width=400,height=300,top="+(screen.availHeight-300)/2+",left="+(screen.availWidth-400)/2+",resizable=yes,scrollbars=yes");	
	
	document.forms[0].ViewImage.value='';	
	return false;									
	//return true;									
}
function WindowOpenViewImage3()
{	
	if(document.forms[0].file3.value!='')
	{	
		document.forms[0].image.value='2';
	}	
	else
	{
		document.forms[0].image.value='';
	}
	document.getElementById('StockId3').value=document.getElementById('StockId').value;
	
	window.open('/RiskmasterUI/UI/BankAccount/ViewImage.aspx?imageid=2&stockid='+ document.getElementById('StockId').value+ '&tempfilename='+ window.document.forms[0].file3.value,'',
			"width=400,height=300,top="+(screen.availHeight-300)/2+",left="+(screen.availWidth-400)/2+",resizable=yes,scrollbars=yes");					

//	window.open('home?pg=riskmaster/CheckStocks/ViewImage&amp;imageid=2&amp;stockid='+ document.getElementById('StockId').value+ '&amp;tempfilename='+ window.document.forms[0].file3.value,'',
//			"width=400,height=300,top="+(screen.availHeight-300)/2+",left="+(screen.availWidth-400)/2+",resizable=yes,scrollbars=yes");	
	document.forms[0].ViewImage.value='';
	return false;									
	//return true;										
}
function Forpopup()
{
	if(document.forms[0].clonesaved.value=='RecordSaved')
		{
			window.opener.document.forms[0].test.value="Last";
			window.opener.document.forms[0].ouraction.value="PerformAction";
			window.opener.document.forms[0].submit();
			window.close();			
		}
	document.getElementById('UseCurrentBankAccount').checked=true;
	document.forms[0].List.disabled=true;
}
	
function Close()
{
	window.close();
}
function Form()
{
	if(document.getElementById('SelectBankAccountFromList').checked)
	{
		document.getElementById('UseCurrentBankAccount').checked=false;
		document.forms[0].List.disabled=false;
	}
	else
	{
		if(document.getElementById('UseCurrentBankAccount').checked)
			document.forms[0].List.disabled=true;
	}

}

function SetSaveFlag() {    
    m_DataChanged = true;
    //ApplyBool(object);

}
				
function toggle_CreateImage1()
{
	m_DataChanged = true ;	
	document.getElementById('WindowsMetafilesoftwaregenerated').checked=false; //mkaran2 - MITS 30204
	document.getElementById('NoneorPrePrintedCheckStock').checked=false;
	document.getElementById('hdnRXLaserFlag').value="True";
	document.getElementById('hdnSoftForm').value="False";
	document.forms[0].RegularCheck.disabled=false;
	document.forms[0].NonnegotiableorVoid.disabled=false;
	document.forms[0].UseSpecialChecka.disabled=false;
	if (document.forms[0].UseSpecialChecka.checked)
	{
		document.forms[0].SpecialCheck.disabled=false;
		document.forms[0].ActivatewhenAmounta.disabled=false;
		document.forms[0].ActivatewhenAmountb.disabled=false;
	}
	document.getElementById('SetImageOne').disabled=true;
	document.getElementById('SetImageTwo').disabled=true;
	document.getElementById('SetImageThree').disabled=true;
	document.getElementById('View_Image1').disabled=true;
	document.getElementById('View_Image2').disabled=true;
	document.getElementById('View_Image3').disabled=true;
	document.forms[0].UseSpecialCheckb.disabled=true;
	//Added by Shivendu
	//document.forms[0].UseSpecialCheckb_mirror.value="";
	document.forms[0].ActivatewhenAmountb.disabled = true;
	document.getElementById('NewVisioVersion').disabled = true;	
}

function toggle_CreateImage2()
{
	m_DataChanged = true ;
	document.getElementById('RxLaserhardwarecartridgeorDIMM').checked=false;
	document.getElementById('NoneorPrePrintedCheckStock').checked=false;
	document.getElementById('hdnSoftForm').value="True";
	document.getElementById('hdnRXLaserFlag').value="False";
	document.getElementById('SetImageOne').disabled=false;
	document.getElementById('SetImageTwo').disabled=false;
	document.getElementById('SetImageThree').disabled=false;
	document.getElementById('View_Image1').disabled=false;
	document.getElementById('View_Image2').disabled=false;
	document.getElementById('View_Image3').disabled=false;
	document.forms[0].UseSpecialCheckb.disabled=false;
	if (document.forms[0].UseSpecialCheckb.checked)
	{
		document.forms[0].ActivatewhenAmountb.disabled=false;
		document.forms[0].SpecialCheck.disabled=false;
		document.forms[0].ActivatewhenAmounta.disabled=false;
	}
	document.forms[0].RegularCheck.disabled=true;
	document.forms[0].NonnegotiableorVoid.disabled=true;
	document.forms[0].UseSpecialChecka.disabled=true;
	//Added by Shivendu
	//document.forms[0].UseSpecialChecka_mirror.value="";
	document.forms[0].SpecialCheck.disabled=true;
	document.forms[0].ActivatewhenAmounta.disabled = true;
	document.getElementById('NewVisioVersion').disabled = false;			
}

function toggle_CreateImage3()
{
	m_DataChanged = true ;
	document.getElementById('hdnSoftForm').value="False";
	document.getElementById('hdnRXLaserFlag').value="False";
	document.getElementById('RxLaserhardwarecartridgeorDIMM').checked=false;
	document.getElementById('WindowsMetafilesoftwaregenerated').checked=false; //mkaran2 - MITS 30204
	document.forms[0].RegularCheck.disabled=true;
	document.forms[0].NonnegotiableorVoid.disabled=true;
	document.forms[0].UseSpecialChecka.disabled=true;
	document.forms[0].SpecialCheck.disabled=true;
	document.forms[0].ActivatewhenAmounta.disabled=true;
	document.getElementById('SetImageOne').disabled=true;
	document.getElementById('SetImageTwo').disabled=true;
	document.getElementById('SetImageThree').disabled=true;
	document.getElementById('View_Image1').disabled=true;
	document.getElementById('View_Image2').disabled=true;
	document.getElementById('View_Image3').disabled = true;
	document.getElementById('NewVisioVersion').disabled = true;
	document.forms[0].UseSpecialCheckb.disabled=true;
	document.forms[0].ActivatewhenAmountb.disabled=true;	
}
function toggle_al() {
        m_DataChanged = true;
		if(document.forms[0].DateXt.value > 23)
				document.forms[0].DateXt.value=0;
		if(document.forms[0].DateYt.value > 23)
		    document.forms[0].DateYt.value = 0;
    //asingh263 mits 32712 starts
		
		if (document.forms[0].PolicyNumberXt.value > 23)
		    document.forms[0].PolicyNumberXt.value = 0;
		if (document.forms[0].PolicyNumberYt.value > 23)
		    document.forms[0].PolicyNumberYt.value = 0;
		if (document.forms[0].InsuredNameXt.value > 23)
		    document.forms[0].InsuredNameXt.value = 0;
		if (document.forms[0].InsuredNameYt.value > 23)
		    document.forms[0].InsuredNameYt.value = 0;
		if (document.forms[0].PayeeAddressXt.value > 23)
		    document.forms[0].PayeeAddressXt.value = 0;
		if (document.forms[0].PayeeAddressYt.value > 23)
		    document.forms[0].PayeeAddressYt.value = 0;

		if (document.forms[0].AgentAddressXt.value > 23)
		    document.forms[0].AgentAddressXt.value = 0;
		if (document.forms[0].AgentAddressYt.value > 23)
		    document.forms[0].AgentAddressYt.value = 0;

		if (document.forms[0].PayeeAddOnCheckXt.value > 23)
		    document.forms[0].PayeeAddOnCheckXt.value = 0;
		if (document.forms[0].PayeeAddOnCheckYt.value > 23)
		    document.forms[0].PayeeAddOnCheckYt.value = 0;
    //asingh263 mits 32712 ends
		if(document.forms[0].CheckNumberXt.value > 23)
				document.forms[0].CheckNumberXt.value=0;
		if(document.forms[0].CheckNumberYt.value > 23)
				document.forms[0].CheckNumberYt.value=0;
		if(document.forms[0].AmountXt.value > 23)
				document.forms[0].AmountXt.value=0;
		if(document.forms[0].AmountYt.value > 23)
				document.forms[0].AmountYt.value=0;
		if(document.forms[0].ClaimNumberXt.value > 23)
				document.forms[0].ClaimNumberXt.value=0;
		if(document.forms[0].ClaimNumberYt.value > 23)
				document.forms[0].ClaimNumberYt.value=0;
		if(document.forms[0].AmountTextXt.value > 23)
				document.forms[0].AmountTextXt.value=0;
		if(document.forms[0].AmountTextYt.value > 23)
				document.forms[0].AmountTextYt.value=0;
		if(document.forms[0].MemoTextXt.value > 23)
				document.forms[0].MemoTextXt.value=0;
		if(document.forms[0].MemoTextYt.value > 23)
				document.forms[0].MemoTextYt.value=0;
		if(document.forms[0].TransDetailXt.value > 23)
				document.forms[0].TransDetailXt.value=0;
		if(document.forms[0].TransDetailYt.value > 23)
				document.forms[0].TransDetailYt.value=0;
		if(document.forms[0].PayerXt.value > 23)
				document.forms[0].PayerXt.value=0;
		if(document.forms[0].PayerYt.value > 23)
				document.forms[0].PayerYt.value=0;
		if(document.forms[0].PayeeXt.value > 23)
				document.forms[0].PayeeXt.value=0;
		if(document.forms[0].PayeeYt.value > 23)
				document.forms[0].PayeeYt.value=0;
		if(document.forms[0].CheckOffsetXt.value > 23)
				document.forms[0].CheckOffsetXt.value=0;
		if(document.forms[0].CheckOffsetYt.value > 23)
				document.forms[0].CheckOffsetYt.value=0;
		if(document.forms[0].MICROffsetXt.value > 23)
				document.forms[0].MICROffsetXt.value=0;
		if(document.forms[0].MICROffsetYt.value > 23)
				document.forms[0].MICROffsetYt.value=0;
		if(document.forms[0].tndOffsetXt.value > 23)
				document.forms[0].tndOffsetXt.value=0;
		if(document.forms[0].tndOffsetYt.value > 23)
				document.forms[0].tndOffsetYt.value=0;
		if(document.getElementById('hdnUnit').value=="Inches")
		{
		//Changed by Gagan for MITS 10963 : Start		
		//document.getElementById('Inches').checked=true;
		//document.getElementById('Centimeters').checked=false;
		//Changed by Gagan for MITS 10963 : End
		document.forms[0].DateXa.value	= document.forms[0].DateXt.value*1440;
		document.forms[0].DateYa.value = document.forms[0].DateYt.value * 1440;
		    //asingh263 mits 32712 starts
		document.forms[0].EventDateXa.value = document.forms[0].EventDateXt.value * 1440;
		document.forms[0].EventDateYa.value = document.forms[0].EventDateYt.value * 1440;
		document.forms[0].ClaimDateXa.value = document.forms[0].ClaimDateXt.value * 1440;
		document.forms[0].ClaimDateYa.value = document.forms[0].ClaimDateYt.value * 1440;
		document.forms[0].PolicyNumberXa.value = document.forms[0].PolicyNumberXt.value * 1440;
		document.forms[0].PolicyNumberYa.value = document.forms[0].PolicyNumberYt.value * 1440;
		document.forms[0].InsuredNameXa.value = document.forms[0].InsuredNameXt.value * 1440;
		document.forms[0].InsuredNameYa.value = document.forms[0].InsuredNameYt.value * 1440;
		document.forms[0].PayeeAddressXa.value = document.forms[0].PayeeAddressXt.value * 1440;
		document.forms[0].PayeeAddressYa.value = document.forms[0].PayeeAddressYt.value * 1440;

		document.forms[0].AgentAddressXa.value = document.forms[0].AgentAddressXt.value * 1440;
		document.forms[0].AgentAddressYa.value = document.forms[0].AgentAddressYt.value * 1440;

		document.forms[0].PayeeAddOnCheckXa.value = document.forms[0].PayeeAddOnCheckXt.value * 1440;
		document.forms[0].PayeeAddOnCheckYa.value = document.forms[0].PayeeAddOnCheckYt.value * 1440;
		    //asingh263 mits 32712 ends
		document.forms[0].CheckNumberXa.value	= document.forms[0].CheckNumberXt.value*1440;
		document.forms[0].CheckNumberYa.value	= document.forms[0].CheckNumberYt.value*1440;	
		document.forms[0].AmountXa.value	= document.forms[0].AmountXt.value*1440;
		document.forms[0].AmountYa.value	= document.forms[0].AmountYt.value*1440;	
		document.forms[0].AmountTextXa.value	= document.forms[0].AmountTextXt.value*1440;
		document.forms[0].AmountTextYa.value	= document.forms[0].AmountTextYt.value*1440;	
		document.forms[0].ClaimNumberXa.value	= document.forms[0].ClaimNumberXt.value*1440;
		document.forms[0].ClaimNumberYa.value	= document.forms[0].ClaimNumberYt.value*1440;	
		document.forms[0].MemoTextXa.value	= document.forms[0].MemoTextXt.value*1440;
		document.forms[0].MemoTextYa.value	= document.forms[0].MemoTextYt.value*1440;	
		document.forms[0].TransDetailXa.value	= document.forms[0].TransDetailXt.value*1440;
		document.forms[0].TransDetailYa.value	= document.forms[0].TransDetailYt.value*1440;	
		document.forms[0].PayerXa.value	= document.forms[0].PayerXt.value*1440;
		document.forms[0].PayerYa.value	= document.forms[0].PayerYt.value*1440;	
		document.forms[0].PayeeXa.value	= document.forms[0].PayeeXt.value*1440;
		document.forms[0].PayeeYa.value	= document.forms[0].PayeeYt.value*1440;		
		document.forms[0].CheckOffsetXa.value	= document.forms[0].CheckOffsetXt.value*1440;
		document.forms[0].CheckOffsetYa.value	= document.forms[0].CheckOffsetYt.value*1440;	
		document.forms[0].MICROffsetXa.value	= document.forms[0].MICROffsetXt.value*1440;
		document.forms[0].MICROffsetYa.value	= document.forms[0].MICROffsetYt.value*1440;
		document.forms[0].tndOffsetXa.value	= document.forms[0].tndOffsetXt.value*1440;
		document.forms[0].tndOffsetYa.value	= document.forms[0].tndOffsetYt.value*1440;
		}
		else
		{
		    //Changed by Gagan for MITS 10963 : Start		
			//document.getElementById('Inches').checked=false;
			//document.getElementById('Centimeters').checked=true;
			//Changed by Gagan for MITS 10963 : End
			document.forms[0].DateXa.value	= document.forms[0].DateXt.value*1440/2.54;
			document.forms[0].DateYa.value = document.forms[0].DateYt.value * 1440 / 2.54;
		    //asingh263 mits 32712 starts
			document.forms[0].EventDateXa.value = document.forms[0].EventDateXt.value * 1440/2.54;
			document.forms[0].EventDateYa.value = document.forms[0].EventDateYt.value * 1440/2.54;
			document.forms[0].ClaimDateXa.value = document.forms[0].ClaimDateXt.value * 1440/2.54;
			document.forms[0].ClaimDateYa.value = document.forms[0].ClaimDateYt.value * 1440/2.54;
			document.forms[0].PolicyNumberXa.value = document.forms[0].PolicyNumberXt.value * 1440/2.54;
			document.forms[0].PolicyNumberYa.value = document.forms[0].PolicyNumberYt.value * 1440/2.54;
			document.forms[0].InsuredNameXa.value = document.forms[0].InsuredNameXt.value * 1440/2.54;
			document.forms[0].InsuredNameYa.value = document.forms[0].InsuredNameYt.value * 1440/2.54;
			document.forms[0].PayeeAddressXa.value = document.forms[0].PayeeAddressXt.value * 1440/2.54;
			document.forms[0].PayeeAddressYa.value = document.forms[0].PayeeAddressYt.value * 1440 / 2.54;

			document.forms[0].AgentAddressXa.value = document.forms[0].AgentAddressXt.value * 1440 / 2.54;
			document.forms[0].AgentAddressYa.value = document.forms[0].AgentAddressYt.value * 1440 / 2.54;

			document.forms[0].PayeeAddOnCheckXa.value = document.forms[0].PayeeAddOnCheckXt.value * 1440 / 2.54;
			document.forms[0].PayeeAddOnCheckYa.value = document.forms[0].PayeeAddOnCheckYt.value * 1440 / 2.54;
		    //asingh263 mits 32712 ends
			document.forms[0].CheckNumberXa.value	= document.forms[0].CheckNumberXt.value*1440/2.54;
			document.forms[0].CheckNumberYa.value	= document.forms[0].CheckNumberYt.value*1440/2.54;	
			document.forms[0].AmountXa.value	= document.forms[0].AmountXt.value*1440/2.54;
			document.forms[0].AmountYa.value	= document.forms[0].AmountYt.value*1440/2.54;	
			document.forms[0].AmountTextXa.value	= document.forms[0].AmountTextXt.value*1440/2.54;
			document.forms[0].AmountTextYa.value	= document.forms[0].AmountTextYt.value*1440/2.54;	
			document.forms[0].ClaimNumberXa.value	= document.forms[0].ClaimNumberXt.value*1440/2.54;
			document.forms[0].ClaimNumberYa.value	= document.forms[0].ClaimNumberYt.value*1440/2.54;	
			document.forms[0].MemoTextXa.value	= document.forms[0].MemoTextXt.value*1440/2.54;
			document.forms[0].MemoTextYa.value	= document.forms[0].MemoTextYt.value*1440/2.54;	
			document.forms[0].TransDetailXa.value	= document.forms[0].TransDetailXt.value*1440/2.54;
			document.forms[0].TransDetailYa.value	= document.forms[0].TransDetailYt.value*1440/2.54;	
			document.forms[0].PayerXa.value	= document.forms[0].PayerXt.value*1440/2.54;
			document.forms[0].PayerYa.value	= document.forms[0].PayerYt.value*1440/2.54;	
			document.forms[0].PayeeXa.value	= document.forms[0].PayeeXt.value*1440/2.54;
			document.forms[0].PayeeYa.value	= document.forms[0].PayeeYt.value*1440/2.54;		
			document.forms[0].CheckOffsetXa.value	= document.forms[0].CheckOffsetXt.value*1440/2.54;
			document.forms[0].CheckOffsetYa.value	= document.forms[0].CheckOffsetYt.value*1440/2.54;	
			document.forms[0].MICROffsetXa.value	= document.forms[0].MICROffsetXt.value*1440/2.54;
			document.forms[0].MICROffsetYa.value	= document.forms[0].MICROffsetYt.value*1440/2.54;
			document.forms[0].tndOffsetXa.value	= document.forms[0].tndOffsetXt.value*1440/2.54;
			document.forms[0].tndOffsetYa.value	= document.forms[0].tndOffsetYt.value*1440/2.54;
		}
				
			
}
function OnPageLoading()
{
    loadTabList();
    //Changed by Saurabh P Arora for MITS 15158: Start  
	if(document.getElementById("StockId").value=="")
	{
	document.getElementById("delete").disabled=true;
	}
    //Changed by Saurabh P Arora for MITS 15158: End    
	if(document.forms[0].file1.value!="")
	{
		//var objControl1=document.getElementById('SetImageOne');
		//objControl1.parentNode.previousSibling.previousSibling.previousSibling.childNodes[0].innerHTML = ' (Image Set)   ';
		var objlblNoImageSetID = window.document.getElementById('lblNoImageSetID'); //Jira id: 7554
		objlblNoImageSetID.innerHTML = '(Image Set)';
	}
	if(document.forms[0].file2.value!="")
	{
		//var objControl2=document.getElementById('SetImageTwo');
	    //objControl2.parentNode.previousSibling.previousSibling.previousSibling.childNodes[0].innerHTML = ' (Image Set)   ';
	    var objlblNoImgSetID = window.document.getElementById('lblNoImgSetID'); //Jira id: 7554
	    objlblNoImgSetID.innerHTML = '(Image Set)';
	}
	if(document.forms[0].file3.value!="")
	{
		//var objControl3=document.getElementById('SetImageThree');
	    //objControl3.parentNode.previousSibling.previousSibling.previousSibling.childNodes[0].innerHTML = ' (Image Set)   ';
	    var objlblNoImgeSetID = window.document.getElementById('lblNoImgeSetID');  //Jira id: 7554
	    objlblNoImgeSetID.innerHTML = '(Image Set)';
	}
	if (document.getElementById('test').value=="New")	
	{	
		document.getElementById('NoneorPrePrintedCheckStock').checked=true;
		document.getElementById('Blank').checked=true;	
		//document.getElementById('RxLaser').checked=true;
		document.forms[0].ViewImage.value='';		
	}
	if (document.getElementById('test').value=="Delete")	
	{	
		document.getElementById('NoneorPrePrintedCheckStock').checked=true;
		document.getElementById('Blank').checked=true;	
		//document.getElementById('RxLaser').checked=true;
		document.forms[0].ViewImage.value='';		
	}
	/*if ((document.forms[0].image.value!="")&&(document.forms[0].ViewImage.value=='true'))
	{
		document.getElementById('StockId1').value=document.getElementById('StockId').value;
		document.getElementById('StockId2').value=document.getElementById('StockId').value;
		document.getElementById('StockId3').value=document.getElementById('StockId').value;
		window.open('home?pg=riskmaster/CheckStocks/ViewImage&amp;','',
			"width=400,height=300,top="+(screen.availHeight-300)/2+",left="+(screen.availWidth-400)/2+",resizable=yes,scrollbars=yes");
		document.forms[0].ViewImage.value='';	
	}*/
	if (document.getElementById('hdnAddressFlag').value=="0")	
	{	
		document.getElementById('Blank').checked=true;
	}
	else
	{
		if (document.getElementById('hdnAddressFlag').value=="1")
		{	
			document.getElementById('PayeeAddress').checked=true;
		}	
		else
			document.getElementById('PaymentDetail').checked=true;
	}
	if(document.getElementById('hdnRXLaserFlag').value=="" && document.getElementById('hdnSoftForm').value=="")
	{
		document.getElementById('NoneorPrePrintedCheckStock').checked=true;;
	}
	else
	{
		if (document.getElementById('hdnRXLaserFlag').value=="True")	
		{	
			document.getElementById('RxLaserhardwarecartridgeorDIMM').checked=true;
			//document.all["RxLaserhardwarecartridgeorDIMM"].checked=true;
			document.getElementById('NoneorPrePrintedCheckStock').checked=false;
			document.forms[0].RegularCheck.disabled=false;
			document.forms[0].NonnegotiableorVoid.disabled=false;
			document.forms[0].UseSpecialChecka.disabled=false;
			if (document.forms[0].UseSpecialChecka.checked)
			{
				document.forms[0].SpecialCheck.disabled=false;
				document.forms[0].ActivatewhenAmounta.disabled=false;
				document.forms[0].ActivatewhenAmountb.disabled=false;
			}
			else
			{
				document.forms[0].SpecialCheck.disabled=true;
				document.forms[0].ActivatewhenAmounta.disabled=true;
			}		
		}
		else
		{
				document.forms[0].RegularCheck.disabled=true;
				document.forms[0].NonnegotiableorVoid.disabled=true;
				document.forms[0].UseSpecialChecka.disabled=true;
				document.forms[0].SpecialCheck.disabled=true;
				document.forms[0].ActivatewhenAmounta.disabled=true;		
		}
		if (document.getElementById('hdnSoftForm').value=="True")	
		{		    
			document.getElementById('WindowsMetafilesoftwaregenerated').checked=true; //mkaran2 - MITS 30204
			//document.all["WindowsMetafilesoftwaregenerated"].checked=true;
			document.getElementById('NoneorPrePrintedCheckStock').checked=false;
			document.getElementById('SetImageOne').disabled=false;
			document.getElementById('SetImageTwo').disabled=false;
			document.getElementById('SetImageThree').disabled=false;
			document.getElementById('View_Image1').disabled=false;
			document.getElementById('View_Image2').disabled=false;
			document.getElementById('View_Image3').disabled=false;
			document.forms[0].UseSpecialCheckb.disabled=false;
			if (document.forms[0].UseSpecialCheckb.checked)
			{
				document.forms[0].ActivatewhenAmountb.disabled=false;
				document.forms[0].SpecialCheck.disabled=false;
				document.forms[0].ActivatewhenAmounta.disabled=false;
			}
			else
			{
				document.forms[0].ActivatewhenAmountb.disabled=true;
			}		
		}
		else
		{
				document.getElementById('SetImageOne').disabled=true;
				document.getElementById('SetImageTwo').disabled=true;
				document.getElementById('SetImageThree').disabled=true;
				document.getElementById('View_Image1').disabled=true;
				document.getElementById('View_Image2').disabled=true;
				document.getElementById('View_Image3').disabled=true;
				document.forms[0].UseSpecialCheckb.disabled=true;
				document.forms[0].ActivatewhenAmountb.disabled=true;		
		}	
		if ((document.getElementById('hdnSoftForm').value=="False")&&(document.getElementById('hdnRXLaserFlag').value=="False"))	
		{			
			document.getElementById('NoneorPrePrintedCheckStock').checked=true;
		}
		
	}
	
	if (document.getElementById('hdnUnit').value=="Inches")	
	{			
	    //Changed by Gagan for MITS 10963 : Start		
		//document.getElementById('Inches').checked=true;
		//Changed by Gagan for MITS 10963 : End
		document.forms[0].DateXt.value	= Math.round(document.forms[0].DateXa.value/1440*100)/100;
		document.forms[0].DateYt.value = Math.round(document.forms[0].DateYa.value / 1440 * 100) / 100;
	    //asingh263 mits 32712 starts
		document.forms[0].EventDateXt.value = Math.round(document.forms[0].EventDateXa.value / 1440 * 100) / 100;
		document.forms[0].EventDateYt.value = Math.round(document.forms[0].EventDateYa.value / 1440 * 100) / 100;
		document.forms[0].ClaimDateXt.value = Math.round(document.forms[0].ClaimDateXa.value / 1440 * 100) / 100;
		document.forms[0].ClaimDateYt.value = Math.round(document.forms[0].ClaimDateYa.value / 1440 * 100) / 100;
		document.forms[0].PolicyNumberXt.value = Math.round(document.forms[0].PolicyNumberXa.value / 1440 * 100) / 100;
		document.forms[0].PolicyNumberYt.value = Math.round(document.forms[0].PolicyNumberYa.value / 1440 * 100) / 100;
		document.forms[0].InsuredNameXt.value = Math.round(document.forms[0].InsuredNameXa.value / 1440 * 100) / 100;
		document.forms[0].InsuredNameYt.value = Math.round(document.forms[0].InsuredNameYa.value / 1440 * 100) / 100;
		document.forms[0].PayeeAddressXt.value = Math.round(document.forms[0].PayeeAddressXa.value / 1440 * 100) / 100;
		document.forms[0].PayeeAddressYt.value = Math.round(document.forms[0].PayeeAddressYa.value / 1440 * 100) / 100;

		document.forms[0].AgentAddressXt.value = Math.round(document.forms[0].AgentAddressXa.value / 1440 * 100) / 100;
		document.forms[0].AgentAddressYt.value = Math.round(document.forms[0].AgentAddressYa.value / 1440 * 100) / 100;

		document.forms[0].PayeeAddOnCheckXt.value = Math.round(document.forms[0].PayeeAddOnCheckXa.value / 1440 * 100) / 100;
		document.forms[0].PayeeAddOnCheckYt.value = Math.round(document.forms[0].PayeeAddOnCheckYa.value / 1440 * 100) / 100;
		
	    //asingh263 mits 32712 ends
		document.forms[0].CheckNumberXt.value	= Math.round(document.forms[0].CheckNumberXa.value/1440*100)/100;
		document.forms[0].CheckNumberYt.value	= Math.round(document.forms[0].CheckNumberYa.value/1440*100)/100;	
		document.forms[0].AmountXt.value	= Math.round(document.forms[0].AmountXa.value/1440*100)/100;
		document.forms[0].AmountYt.value	= Math.round(document.forms[0].AmountYa.value/1440*100)/100;	
		document.forms[0].AmountTextXt.value	= Math.round(document.forms[0].AmountTextXa.value/1440*100)/100;
		document.forms[0].AmountTextYt.value	= Math.round(document.forms[0].AmountTextYa.value/1440*100)/100;	
		document.forms[0].ClaimNumberXt.value	= Math.round(document.forms[0].ClaimNumberXa.value/1440*100)/100;
		document.forms[0].ClaimNumberYt.value	= Math.round(document.forms[0].ClaimNumberYa.value/1440*100)/100;	
		document.forms[0].MemoTextXt.value	= Math.round(document.forms[0].MemoTextXa.value/1440*100)/100;
		document.forms[0].MemoTextYt.value	= Math.round(document.forms[0].MemoTextYa.value/1440*100)/100;	
		document.forms[0].TransDetailXt.value	= Math.round(document.forms[0].TransDetailXa.value/1440*100)/100;
		document.forms[0].TransDetailYt.value	= Math.round(document.forms[0].TransDetailYa.value/1440*100)/100;	
		document.forms[0].PayerXt.value	= Math.round(document.forms[0].PayerXa.value/1440*100)/100;
		document.forms[0].PayerYt.value	= Math.round(document.forms[0].PayerYa.value/1440*100)/100;	
		document.forms[0].PayeeXt.value	= Math.round(document.forms[0].PayeeXa.value/1440*100)/100;
		document.forms[0].PayeeYt.value	= Math.round(document.forms[0].PayeeYa.value/1440*100)/100;		
		document.forms[0].CheckOffsetXt.value	= Math.round(document.forms[0].CheckOffsetXa.value/1440*100)/100;
		document.forms[0].CheckOffsetYt.value	= Math.round(document.forms[0].CheckOffsetYa.value/1440*100)/100;	
		document.forms[0].MICROffsetXt.value	= Math.round(document.forms[0].MICROffsetXa.value/1440*100)/100;
		document.forms[0].MICROffsetYt.value	= Math.round(document.forms[0].MICROffsetYa.value/1440*100)/100;
		document.forms[0].tndOffsetXt.value	= Math.round(document.forms[0].tndOffsetXa.value/1440*100)/100;
		document.forms[0].tndOffsetYt.value	= Math.round(document.forms[0].tndOffsetYa.value/1440*100)/100;		
	}
	else
	{
	    //Changed by Gagan for MITS 10963 : Start
		//document.getElementById('Centimeters').checked=true;
		//document.getElementById('Inches').checked=false;		
		
		document.forms[0].DateXt.value	= Math.round(document.forms[0].DateXa.value/1440*2.54*100)/100;
		document.forms[0].DateYt.value = Math.round(document.forms[0].DateYa.value / 1440 * 2.54 * 100) / 100;
	    //asingh263 mits 32712 starts
		document.forms[0].EventDateXt.value = Math.round(document.forms[0].EventDateXa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].EventDateYt.value = Math.round(document.forms[0].EventDateYa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].ClaimDateXt.value = Math.round(document.forms[0].ClaimDateXa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].ClaimDateYt.value = Math.round(document.forms[0].ClaimDateYa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].PolicyNumberXt.value = Math.round(document.forms[0].PolicyNumberXa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].PolicyNumberYt.value = Math.round(document.forms[0].PolicyNumberYa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].InsuredNameXt.value = Math.round(document.forms[0].InsuredNameXa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].InsuredNameYt.value = Math.round(document.forms[0].InsuredNameYa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].PayeeAddressXt.value = Math.round(document.forms[0].PayeeAddressXa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].PayeeAddressYt.value = Math.round(document.forms[0].PayeeAddressYa.value / 1440 * 2.54 * 100) / 100;

		document.forms[0].AgentAddressXt.value = Math.round(document.forms[0].AgentAddressXa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].AgentAddressYt.value = Math.round(document.forms[0].AgentAddressYa.value / 1440 * 2.54 * 100) / 100;


		document.forms[0].PayeeAddOnCheckXt.value = Math.round(document.forms[0].PayeeAddOnCheckXa.value / 1440 * 2.54 * 100) / 100;
		document.forms[0].PayeeAddOnCheckYt.value = Math.round(document.forms[0].PayeeAddOnCheckYa.value / 1440 * 2.54 * 100) / 100;

	    //asingh263 mits 32712 ends
		document.forms[0].CheckNumberXt.value	= Math.round(document.forms[0].CheckNumberXa.value/1440*2.54*100)/100;
		document.forms[0].CheckNumberYt.value	= Math.round(document.forms[0].CheckNumberYa.value/1440*2.54*100)/100;	
		document.forms[0].AmountXt.value	= Math.round(document.forms[0].AmountXa.value/1440*2.54*100)/100;
		document.forms[0].AmountYt.value	= Math.round(document.forms[0].AmountYa.value/1440*2.54*100)/100;	
		document.forms[0].AmountTextXt.value	= Math.round(document.forms[0].AmountTextXa.value/1440*2.54*100)/100;
		document.forms[0].AmountTextYt.value	= Math.round(document.forms[0].AmountTextYa.value/1440*2.54*100)/100;	
		document.forms[0].ClaimNumberXt.value	= Math.round(document.forms[0].ClaimNumberXa.value/1440*2.54*100)/100;
		document.forms[0].ClaimNumberYt.value	= Math.round(document.forms[0].ClaimNumberYa.value/1440*2.54*100)/100;	
		document.forms[0].MemoTextXt.value	= Math.round(document.forms[0].MemoTextXa.value/1440*2.54*100)/100;
		document.forms[0].MemoTextYt.value	= Math.round(document.forms[0].MemoTextYa.value/1440*2.54*100)/100;	
		document.forms[0].TransDetailXt.value	= Math.round(document.forms[0].TransDetailXa.value/1440*2.54*100)/100;
		document.forms[0].TransDetailYt.value	= Math.round(document.forms[0].TransDetailYa.value/1440*2.54*100)/100;	
		document.forms[0].PayerXt.value	= Math.round(document.forms[0].PayerXa.value/1440*2.54*100)/100;
		document.forms[0].PayerYt.value	= Math.round(document.forms[0].PayerYa.value/1440*2.54*100)/100;	
		document.forms[0].PayeeXt.value	= Math.round(document.forms[0].PayeeXa.value/1440*2.54*100)/100;
		document.forms[0].PayeeYt.value	= Math.round(document.forms[0].PayeeYa.value/1440*2.54*100)/100;		
		document.forms[0].CheckOffsetXt.value	= Math.round(document.forms[0].CheckOffsetXa.value/1440*2.54*100)/100;
		document.forms[0].CheckOffsetYt.value	= Math.round(document.forms[0].CheckOffsetYa.value/1440*2.54*100)/100;	
		document.forms[0].MICROffsetXt.value	= Math.round(document.forms[0].MICROffsetXa.value/1440*2.54*100)/100;
		document.forms[0].MICROffsetYt.value	= Math.round(document.forms[0].MICROffsetYa.value/1440*2.54*100)/100;
		document.forms[0].tndOffsetXt.value	= Math.round(document.forms[0].tndOffsetXa.value/1440*2.54*100)/100;
		document.forms[0].tndOffsetYt.value	= Math.round(document.forms[0].tndOffsetYa.value/1440*2.54*100)/100;		
	
	}
	
	//document.getElementById('Inches').checked=true;
	
	//Changed by Gagan for MITS 10963 : End
	
	MICREnabledsettings();

	if(document.forms[0].hdnSoftForm.value=="True")
		toggle_CreateImage2();
	if(document.forms[0].hdnRXLaserFlag.value=="True")
		toggle_CreateImage1();
	if((document.forms[0].hdnRXLaserFlag.value=="" || document.forms[0].hdnRXLaserFlag.value=="False") && (document.forms[0].hdnSoftForm.value=="" || document.forms[0].hdnSoftForm.value=="False"))
	{
		document.forms[0].RxLaserhardwarecartridgeorDIMM.selected=false;
		document.forms[0].WindowsMetafilesoftwaregenerated.checked = false; //mkaran2 - MITS 30204
		toggle_CreateImage3();
	}
	if (parent.MDISetUnDirty != null) {
	    parent.MDISetUnDirty(0);
	}
    //deb:MITS 24065
	if (m_DataChanged != false) {
	    parent.MDISetUnDirty(null, "0", true);
	}
    //deb
	m_DataChanged = false ;	
}
function toggle_BasicOptions()
{

    m_DataChanged = true;	
	if (document.forms[0].MICREnabled.checked)
	{
			document.forms[0].MICRNumber.disabled=false;
			document.forms[0].MICRFontName.disabled=false;
			document.forms[0].MICRFontSize.disabled=false;		
	}
	else
	{
			document.forms[0].MICRNumber.disabled=true;
			document.forms[0].MICRFontName.disabled=true;
			document.forms[0].MICRFontSize.disabled=true;
	}
	
	if (document.forms[0].PrintMemoonCheck.checked)
	{
			document.forms[0].memoFontName.disabled=false;
			document.forms[0].memoFontSize.disabled=false;
			document.forms[0].NoofRowsForMemo.disabled=false;
			document.forms[0].NoofCharactersperMemoRow.disabled=false;
	}
	else
	{
			document.forms[0].memoFontName.disabled=true;
			document.forms[0].memoFontSize.disabled=true;
			document.forms[0].NoofRowsForMemo.disabled=true;
			document.forms[0].NoofCharactersperMemoRow.disabled = true;
			//MITS 14661
			document.forms[0].memoFontName.selectedIndex= -1;
			document.forms[0].memoFontSize.selectedIndex= -1;
			document.forms[0].NoofRowsForMemo.value= "";
			document.forms[0].NoofCharactersperMemoRow.value = "";
	}
	
	if(document.forms[0].WindowsMetafilesoftwaregenerated.checked)
	{
	    if(document.forms[0].UseSpecialCheckb.checked)
	    {
		    document.forms[0].ActivatewhenAmountb.disabled=false;
	    }
	    else
	    {
		    document.forms[0].ActivatewhenAmountb.disabled=true;
	    }
	}
	
    if(document.forms[0].RxLaserhardwarecartridgeorDIMM.checked)
    {
        if(document.forms[0].UseSpecialChecka.checked)
        {
            document.forms[0].ActivatewhenAmounta.disabled=false;
            document.forms[0].SpecialCheck.disabled=false;
        }
        else
        {
            document.forms[0].ActivatewhenAmounta.disabled=true;
            document.forms[0].SpecialCheck.disabled=true;
        }
	}
}
//Changed by Gagan for MITS 10963 : Start		
//function Units()
//{
//	m_DataChanged = true ;
//	if(document.getElementById('Inches').checked)
//	{
//		document.getElementById('hdnUnit').value="Inches";
//		document.getElementById('Centimeters').checked=false;
//	}
//	if(document.getElementById('Centimeters').checked)
//	{
//		document.getElementById('hdnUnit').value="Centimeters";
//		document.getElementById('Inches').checked=false;
//	}
//}
//Changed by Gagan for MITS 10963 : End
function BrowserWindowone()
{
	document.forms[0].image.value='0';
	
	window.open('/RiskmasterUI/UI/BankAccount/UploadFile.aspx?&amp;','','UploadFile',
	    'width=100px,height=50px'+',top='+(screen.availHeight-50)/2+',left='+(screen.availWidth-100)/2+',resizable=yes,scrollbars=yes');
	
	//window.open('home?pg=riskmaster/CheckStocks/SetImage&amp;','',
	//		"width=500,height=400,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
	return false;
}
function BrowserWindowtwo()
{
	document.forms[0].image.value='1';
	
    window.open('/RiskmasterUI/UI/BankAccount/UploadFile.aspx?&amp;','','UploadFile',
	    'width=500,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	
	//window.open('home?pg=riskmaster/CheckStocks/SetImage&amp;','',
	//		"width=500,height=400,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
	return false;
}
function BrowserWindowthree()
{
	document.forms[0].image.value='2';
	
	window.open('/RiskmasterUI/UI/BankAccount/UploadFile.aspx?&amp;','','UploadFile',
	    'width=500,height=400'+',top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	
	//window.open('home?pg=riskmaster/CheckStocks/SetImage&amp;','',
	//		"width=500,height=400,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
	if(document.forms[0].file3.value!="")
	{
		document.getElementById('SetImageOne').value="Image set";
	}
	return false;
}
function WindowOpenThreePartChecks()
{
	window.open('home?pg=riskmaster/CheckStocks/ThreePartChecks&amp;','',
			"width=400,height=300,top="+(screen.availHeight-300)/2+",left="+(screen.availWidth-400)/2+",resizable=yes,scrollbars=yes");
	return false;
	
}
function RestoreDefault()
{
    //if( confirm("This will restore the default coordinate values for the Checklayout.Are you sure you want to proceed?")) //MITS 31003 - Rakhel ML changes
    if (confirm(CheckStocksValidations.ConfirmRestore))
	{
		m_DataChanged = true ;		
		//Changed by Gagan for MITS 10963 : Start		
		var iFactor = 1; //for Inches
		if(document.getElementById('hdnUnit').value=="Inches")
		{
		    iFactor = 1;		
		}
		else
		{
		   iFactor = 2.54;		
		}
		
		if( document.getElementById('WindowsMetafilesoftwaregenerated').checked==true )
		{
			document.forms[0].DateXt.value = Math.round(6.37 * iFactor * 100)/100;
			document.forms[0].DateYt.value = Math.round(2.59 * iFactor * 100)/100;	
			document.forms[0].EventDateXt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].EventDateYt.value = Math.round(3.60 * iFactor * 100) / 100;
			document.forms[0].ClaimDateXt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].ClaimDateYt.value = Math.round(3.80 * iFactor * 100) / 100;
			document.forms[0].PolicyNumberXt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].PolicyNumberYt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].InsuredNameXt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].InsuredNameYt.value = Math.round(4.20 * iFactor * 100) / 100;
			document.forms[0].PayeeAddressXt.value = Math.round(0.69 * iFactor * 100) / 100;
			document.forms[0].PayeeAddressYt.value = Math.round(3.20 * iFactor * 100) / 100;

			document.forms[0].AgentAddressXt.value = Math.round(3.00 * iFactor * 100) / 100;
			document.forms[0].AgentAddressYt.value = Math.round(2.00 * iFactor * 100) / 100;

			document.forms[0].PayeeAddOnCheckXt.value = Math.round(0.69 * iFactor * 100) / 100;
			document.forms[0].PayeeAddOnCheckYt.value = Math.round(3.4 * iFactor * 100) / 100;
		    //asingh263 mits 32712 ends
			document.forms[0].CheckNumberXt.value = Math.round(7.69 * iFactor * 100)/100;
			document.forms[0].CheckNumberYt.value = Math.round(2.59 * iFactor * 100)/100;	
			document.forms[0].AmountXt.value = Math.round(7.47 * iFactor * 100)/100;
			document.forms[0].AmountYt.value= Math.round(3.06 * iFactor * 100)/100;	
			document.forms[0].AmountTextXt.value = Math.round(0.88 * iFactor * 100)/100;
			document.forms[0].AmountTextYt.value = Math.round(3.01 * iFactor * 100)/100;				
			document.forms[0].ClaimNumberXt.value = Math.round(1.00 * iFactor * 100)/100;
			document.forms[0].ClaimNumberYt.value = Math.round(1.00 * iFactor * 100)/100;				
			document.forms[0].MemoTextXt.value = Math.round(0.78 * iFactor * 100)/100;
			document.forms[0].MemoTextYt.value = Math.round(4.35 * iFactor * 100)/100;	
			document.forms[0].TransDetailXt.value = Math.round(0.50 * iFactor * 100)/100;
			document.forms[0].TransDetailYt.value = Math.round(0.58 * iFactor * 100)/100;	
			document.forms[0].PayerXt.value = Math.round(12.00 * iFactor * 100)/100;
			document.forms[0].PayerYt.value = 0.00;	
			document.forms[0].PayeeXt.value = Math.round(0.69 * iFactor * 100)/100;
			document.forms[0].PayeeYt.value = Math.round(3.4 * iFactor * 100)/100;					
			document.forms[0].CheckOffsetXt.value = 0.00;
			document.forms[0].CheckOffsetYt.value = Math.round(6.00 * iFactor * 100)/100;	
			document.forms[0].MICROffsetXt.value = Math.round(1.12 * iFactor * 100)/100;
			document.forms[0].MICROffsetYt.value = Math.round(10.55 * iFactor * 100)/100;
			document.forms[0].tndOffsetXt.value = 0.00;
			document.forms[0].tndOffsetYt.value = 0.00;	
		}
		else
		{		
			document.forms[0].DateXt.value = Math.round(6.07 * iFactor * 100)/100;
			document.forms[0].DateYt.value = Math.round(2.50 * iFactor * 100) / 100;
		    //asingh263 mits 32712 starts
			document.forms[0].EventDateXt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].EventDateYt.value = Math.round(3.60 * iFactor * 100) / 100;
			document.forms[0].ClaimDateXt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].ClaimDateYt.value = Math.round(3.80 * iFactor * 100) / 100;
			document.forms[0].PolicyNumberXt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].PolicyNumberYt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].InsuredNameXt.value = Math.round(4.00 * iFactor * 100) / 100;
			document.forms[0].InsuredNameYt.value = Math.round(4.20 * iFactor * 100) / 100;
			document.forms[0].PayeeAddressXt.value = Math.round(0.69 * iFactor * 100) / 100;
			document.forms[0].PayeeAddressYt.value = Math.round(3.20 * iFactor * 100) / 100;

			document.forms[0].AgentAddressXt.value = Math.round(3.00 * iFactor * 100) / 100;
			document.forms[0].AgentAddressYt.value = Math.round(2.00 * iFactor * 100) / 100;


			document.forms[0].PayeeAddOnCheckXt.value = Math.round(0.69 * iFactor * 100) / 100;
			document.forms[0].PayeeAddOnCheckYt.value = Math.round(3.4 * iFactor * 100) / 100;
		    //asingh263 mits 32712 ends
			document.forms[0].CheckNumberXt.value = Math.round(7.47 * iFactor * 100)/100;
			document.forms[0].CheckNumberYt.value = Math.round(2.50 * iFactor * 100)/100;	
			document.forms[0].AmountXt.value = Math.round(6.77 * iFactor * 100)/100;
			document.forms[0].AmountYt.value= Math.round(3.00 * iFactor * 100)/100;	
			document.forms[0].AmountTextXt.value = Math.round(0.69 * iFactor * 100)/100;
			document.forms[0].AmountTextYt.value = Math.round(3.10 * iFactor * 100)/100;	
			document.forms[0].ClaimNumberXt.value = Math.round(1.00 * iFactor * 100)/100;
			document.forms[0].ClaimNumberYt.value = Math.round(1.00 * iFactor * 100)/100;	
			document.forms[0].MemoTextXt.value = Math.round(0.69 * iFactor * 100)/100;
			document.forms[0].MemoTextYt.value = Math.round(4.20 * iFactor * 100)/100;	
			document.forms[0].TransDetailXt.value = 0.00;
			document.forms[0].TransDetailYt.value = Math.round(0.40 * iFactor * 100)/100;	
			document.forms[0].PayerXt.value = Math.round(12.00 * iFactor * 100)/100;
			document.forms[0].PayerYt.value = 0.00;	
			document.forms[0].PayeeXt.value = Math.round(0.69 * iFactor * 100)/100;
			document.forms[0].PayeeYt.value = Math.round(3.40 * iFactor * 100)/100;		
			document.forms[0].CheckOffsetXt.value = 0.00;
			document.forms[0].CheckOffsetYt.value = Math.round(6.00 * iFactor * 100)/100;	
			document.forms[0].MICROffsetXt.value = Math.round(1.12 * iFactor * 100)/100;
			document.forms[0].MICROffsetYt.value = Math.round(10.55 * iFactor * 100)/100;
			document.forms[0].tndOffsetXt.value = 0.00; 
			document.forms[0].tndOffsetYt.value = 0.00; 
		}
		//Changed by Gagan for MITS 10963 : End
		return false;
	}
}
function CheckInTheMiddle()
{
	//if( confirm("This will restore the default coordinate values for the Three-PartCheck Layout with check printing in the middle portion.Are you sure you want to proceed?")) //MITS 31003 - Rakhel ML changes
    if (confirm(CheckStocksValidations.ConfirmCheckInTheMiddle))
	{
	    //Changed by Gagan for MITS 10963 : Start		
	    var iFactor = 1; //for Inches
		if(document.getElementById('hdnUnit').value=="Inches")
		{
		    iFactor = 1;		
		}
		else
		{
		   iFactor = 2.54;		
		}
	
		m_DataChanged = true ;
		document.forms[0].DateXt.value = Math.round(6.07 * iFactor * 100)/100;
		document.forms[0].DateYt.value = Math.round(0.60 * iFactor * 100) / 100;
        //asingh263 mits 32712 starts
		document.forms[0].EventDateXt.value = Math.round(5.00* iFactor * 100)/100;
		document.forms[0].EventDateYt.value = Math.round(1.50 * iFactor * 100) / 100;
		document.forms[0].ClaimDateXt.value = Math.round(5.00 * iFactor * 100) / 100;
		document.forms[0].ClaimDateYt.value = Math.round(1.80 * iFactor * 100) / 100;
		document.forms[0].PolicyNumberXt.value = Math.round(5.00 * iFactor * 100) / 100;
		document.forms[0].PolicyNumberYt.value = Math.round(2.10 * iFactor * 100) / 100;
		document.forms[0].InsuredNameXt.value = Math.round(5.00 * iFactor * 100) / 100;
		document.forms[0].InsuredNameYt.value = Math.round(2.40 * iFactor * 100) / 100;
		document.forms[0].PayeeAddressXt.value = Math.round(0.69 * iFactor * 100) / 100;
		document.forms[0].PayeeAddressYt.value = Math.round(3.20 * iFactor * 100) / 100;

		document.forms[0].AgentAddressXt.value = Math.round(3.00 * iFactor * 100) / 100;
		document.forms[0].AgentAddressYt.value = Math.round(2.00 * iFactor * 100) / 100;

		document.forms[0].PayeeAddOnCheckXt.value = Math.round(0.69 * iFactor * 100) / 100;
		document.forms[0].PayeeAddOnCheckYt.value = Math.round(1.52 * iFactor * 100) / 100;
        //asingh263 mits 32712 ends
		document.forms[0].CheckNumberXt.value = Math.round(7.47* iFactor * 100)/100;
		document.forms[0].CheckNumberYt.value = Math.round(0.60* iFactor * 100)/100;
		document.forms[0].AmountXt.value =Math.round(6.77* iFactor * 100)/100;
		document.forms[0].AmountYt.value= Math.round(1.10* iFactor * 100)/100;	
		document.forms[0].AmountTextXt.value = Math.round(0.69* iFactor * 100)/100;
		document.forms[0].AmountTextYt.value = Math.round(1.20* iFactor * 100)/100;	
		document.forms[0].ClaimNumberXt.value = Math.round(12.00* iFactor * 100)/100;
		document.forms[0].ClaimNumberYt.value = Math.round(1.00* iFactor * 100)/100;	
		document.forms[0].MemoTextXt.value = Math.round(0.69* iFactor * 100)/100;
		document.forms[0].MemoTextYt.value = Math.round(1.30* iFactor * 100)/100;	
		document.forms[0].TransDetailXt.value = 0.00;
		document.forms[0].TransDetailYt.value = Math.round(0.40* iFactor * 100)/100;	
		document.forms[0].PayerXt.value = Math.round(12.00* iFactor * 100)/100;
		document.forms[0].PayerYt.value = 0.00;	
		document.forms[0].PayeeXt.value = Math.round(0.69* iFactor * 100)/100;
		document.forms[0].PayeeYt.value = Math.round(1.50* iFactor * 100)/100;		
		document.forms[0].CheckOffsetXt.value = 0.00 ;
		document.forms[0].CheckOffsetYt.value = Math.round(3.60* iFactor * 100)/100 ;	
		document.forms[0].MICROffsetXt.value = Math.round(1.12* iFactor * 100)/100;
		document.forms[0].MICROffsetYt.value = Math.round(10.55* iFactor * 100)/100;
		document.forms[0].tndOffsetXt.value = Math.round(2.60* iFactor * 100)/100;
		document.forms[0].tndOffsetYt.value = Math.round(6.40* iFactor * 100)/100;
		window.close();
		return false;
	}
	else
	{
		window.close();
		return false;
	}
}
function CheckAtTheBottom()
{
	//if( confirm("This will restore the default coordinate values for the Three-PartCheck Layout with check printing in the bottom portion.Are you sure you want to proceed?")) //MITS 31003 - Rakhel ML changes
    if (confirm(CheckStocksValidations.ConfirmCheckAtTheBottom))
	{
		m_DataChanged = true ;
		var iFactor = 1; //for Inches
		if(document.getElementById('hdnUnit').value=="Inches")
		{
		    iFactor = 1;		
		}
		else
		{
		   iFactor = 2.54;		
		}
		document.forms[0].DateXt.value = Math.round(6.07* iFactor * 100)/100;
		document.forms[0].DateYt.value = Math.round(1.60 * iFactor * 100) / 100;
        //asingh263 mits 32712 starts
		document.forms[0].EventDateXt.value = Math.round(4.00 * iFactor * 100) / 100;
		document.forms[0].EventDateYt.value = Math.round(2.60 * iFactor * 100) / 100;
		document.forms[0].ClaimDateXt.value = Math.round(4.00 * iFactor * 100) / 100;
		document.forms[0].ClaimDateYt.value = Math.round(2.80 * iFactor * 100) / 100;
		document.forms[0].PolicyNumberXt.value = Math.round(4.00 * iFactor * 100) / 100;
		document.forms[0].PolicyNumberYt.value = Math.round(3.00 * iFactor * 100) / 100;
		document.forms[0].InsuredNameXt.value = Math.round(4.00 * iFactor * 100) / 100;
		document.forms[0].InsuredNameYt.value = Math.round(3.20 * iFactor * 100) / 100;
		document.forms[0].PayeeAddressXt.value = Math.round(0.69 * iFactor * 100) / 100;
		document.forms[0].PayeeAddressYt.value = Math.round(3.20 * iFactor * 100) / 100;

		document.forms[0].AgentAddressXt.value = Math.round(3.00 * iFactor * 100) / 100;
		document.forms[0].AgentAddressYt.value = Math.round(2.00 * iFactor * 100) / 100;

		document.forms[0].PayeeAddOnCheckXt.value = Math.round(0.69 * iFactor * 100) / 100;
		document.forms[0].PayeeAddOnCheckYt.value = Math.round(2.60 * iFactor * 100) / 100;
        //asingh263 mits 32712 ends
		document.forms[0].CheckNumberXt.value = Math.round(7.47* iFactor * 100)/100;
		document.forms[0].CheckNumberYt.value = Math.round(1.60* iFactor * 100)/100;	
		document.forms[0].AmountXt.value =Math.round(6.77* iFactor * 100)/100;
		document.forms[0].AmountYt.value= Math.round(2.10* iFactor * 100)/100;	
		document.forms[0].AmountTextXt.value = Math.round(0.69* iFactor * 100)/100;
		document.forms[0].AmountTextYt.value = Math.round(2.20* iFactor * 100)/100;	
		document.forms[0].ClaimNumberXt.value = Math.round(12.00 * iFactor * 100)/100;
		document.forms[0].ClaimNumberYt.value = Math.round(1.00* iFactor * 100)/100;	
		document.forms[0].MemoTextXt.value = Math.round(0.69* iFactor * 100)/100;
		document.forms[0].MemoTextYt.value = Math.round(3.30* iFactor * 100)/100;	
		document.forms[0].TransDetailXt.value = 0.00;
		document.forms[0].TransDetailYt.value = Math.round(0.40* iFactor * 100)/100;	
		document.forms[0].PayerXt.value =Math.round(12.00* iFactor * 100)/100;
		document.forms[0].PayerYt.value = 0.00;
		document.forms[0].PayerYt.value = 0.00;
		document.forms[0].PayeeXt.value = Math.round(0.69* iFactor * 100)/100;
		document.forms[0].PayeeYt.value = Math.round(2.50* iFactor * 100)/100;		
		document.forms[0].CheckOffsetXt.value = 0.00 ;
		document.forms[0].CheckOffsetYt.value = Math.round(6.40 * iFactor * 100)/100;	
		document.forms[0].MICROffsetXt.value = Math.round(1.12* iFactor * 100)/100;
		document.forms[0].MICROffsetYt.value = Math.round(10.55* iFactor * 100)/100;
		document.forms[0].tndOffsetXt.value = 0.00;
		document.forms[0].tndOffsetYt.value = Math.round(3.60* iFactor * 100)/100;
		//Changed by Gagan for MITS 10963 : End
		window.close();
		return false;
	}
	else
	{
		window.close();
		return false;
	}
	
}	
function MICREnabledsettings()
	{
		if(document.forms[0].MICREnabled.checked==true)
		{
			document.forms[0].MICRNumber.disabled=false;
//			document.forms[0].RxLaser.disabled=false;
//			document.forms[0].TrueTypeFont.disabled=false;
			settingsMICRType();				
		}
		else
		{
			m_DataChanged = true ;
			document.forms[0].MICRNumber.disabled=true;
//			document.forms[0].RxLaser.disabled=true;
//			document.forms[0].TrueTypeFont.disabled=true;
			document.forms[0].MICRFontName.disabled=true;
			document.forms[0].MICRFontSize.disabled=true;		
		}		
	}
	function settingsMICRType()
	{
		m_DataChanged = true ;
//		if(document.forms[0].RxLaser.checked==true)
//		{
//			document.forms[0].MICRFontName.disabled=true;
//			document.forms[0].MICRFontSize.disabled=true;	
//		}
//		if(document.forms[0].TrueTypeFont.checked==true)
//		{
			document.forms[0].MICRFontName.disabled=false;
			document.forms[0].MICRFontSize.disabled=false;	
		//}
//		if(document.forms[0].RxLaser.checked==false && document.forms[0].TrueTypeFont.checked==false)
//		{
//			document.forms[0].RxLaser.checked=true
//			document.forms[0].MICRFontName.disabled=true;
//			document.forms[0].MICRFontSize.disabled=true;
//		}		
	}		
	function copyimage()
	{
		if(document.forms[0].imageid.value!=10 || document.forms[0].imageid.value!='10')
		{
			if(document.forms[0].imageid.value==0 || document.forms[0].imageid.value=='0')
				document.forms[0].image.value=window.opener.document.forms[0].file1.value;
			if(document.forms[0].imageid.value==1 || document.forms[0].imageid.value=='1')
				document.forms[0].image.value=window.opener.document.forms[0].file2.value;
			if(document.forms[0].imageid.value==2 || document.forms[0].imageid.value=='2')
				document.forms[0].image.value=window.opener.document.forms[0].file3.value;
			document.forms[0].imageid.value='10';
			document.forms[0].submit();
		}
	}	