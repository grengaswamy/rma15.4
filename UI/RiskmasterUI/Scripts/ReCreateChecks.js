﻿function PrintBatchOk() {
    window.close();
    return false;
}

function PrintSelectedReport(type, name) {
    document.forms[0].hdnFileRequest.value = "1";
    document.getElementById('file').src = "RePrintCheckDownload.aspx?name=" + name + "&type=" + type;
    pleaseWait.Show();
    checkReadyState();
}
function PrintBatchOnClose() 
{
    window.returnValue = window.document.forms[0].StartNum.value + "||" + window.document.forms[0].EndNum.value + "||" + window.document.forms[0].hdnFileNameAndType.value + "||" + window.document.forms[0].hdnPrintCheckDetails.value;
    return false;
}
function CheckBatchChangedPre() {

	var iBatchId = window.document.forms[0].checkbatchpre.value;
	if (isNaN(iBatchId) || iBatchId.indexOf(".") != -1) {
		alert("Invalid Check Batch Number");
		window.document.forms[0].checkbatchpre.focus();
		return false;
	}

	window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.CheckBatchChangedPre";
	//window.document.forms[0].action.value = "PageRefresh";
	pleaseWait.Show();
	document.forms[0].submit();
	DisableBatchButtons()
	//OnFormSubmitPrintCheck();
}
function DisableBatchButtons() {
	window.document.forms[0].printbatch.disabled = true;
}

function BankAccountChange_ReCreateCheck(tabname) 
{
//    if (tabname == 'print') 
//    {
//        window.document.forms[0].bankaccountpre.value = window.document.forms[0].bankaccountprint.value;
//        window.document.forms[0].bankaccountpost.value = window.document.forms[0].bankaccountprint.value;
//        DisableBatchButtons()
//    }
   window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.AccountChanged";

}

function PrintCheckOnLoad() 
{
	//var stockCount = window.document.forms[0].checkstock.options.length > 0;

	//if (!stockCount)
	   // alert("There are no check stocks defined for this bank account. You will not be able to print any checks.");
	if (window.document.forms[0].printbatchflag.value == "True")// && stockCount)
		window.document.forms[0].printbatch.disabled = false;
	else
		window.document.forms[0].printbatch.disabled = true;
}


function PrintBatch() 
   {
			//var iFirstCheck = window.document.forms[0].firstcheck.value;

//            if (isNaN(iFirstCheck) || iFirstCheck.indexOf(".") != -1) 
//            {
//                alert("Invalid First Check Number ");
//                window.document.forms[0].firstcheck.focus();
//                window.document.forms[0].functiontocall.value = "";
//                return false;
//            }

			if (window.document.forms[0].fileandconsolidate.value == "True") 
			{
				if (!confirm("The rollup feature cannot be used in conjunction with the option to print the checks to the printer and a file.  This system will not use the rollup feature for this check batch.")) 
				{
					window.document.forms[0].functiontocall.value = "";
					return false;
				}
			}

	var iBatchId = window.document.forms[0].checkbatchpre.value;
	var iAccountId = window.document.forms[0].bankaccountprint.value;
	var sCheckDate = window.document.forms[0].checkdate.value;
    // npadhy RMA-11632 Starts - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks
	var iDistributionType = window.document.forms[0].ddlDistributionTypePrint.value;
    // npadhy RMA-11632 Ends - Introducing Distribution Type in Funds/Auto Checks and using it for reprinting checks

	//var bIncludeAutoPayments = window.document.forms[0].includeautopayments.checked;
	//var sSelectedAutoChecksIds = window.document.forms[0].selectedautochecksids.value;
   // var sUsingSelection = window.document.forms[0].selectchecks.checked;
  // var iCheckStockId = window.document.forms[0].checkstock.value;
	//Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010
	//var sorghierarchyprint = window.document.forms[0].orghierarchypre_lst.value;
	//var sorghierarchylevelprint = window.document.forms[0].orghierarchylevelpre.value;
	//End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010

	//var sOrderBy = window.document.forms[0].orderfieldbatch.value;

	var iNumAutoChecksToPrint = 0;

	//if (sUsingSelection && sSelectedAutoChecksIds != "")
	 //{
		//arrTemp = sSelectedAutoChecksIds.split(" ");
	   // iNumAutoChecksToPrint = arrTemp.length;
   // }
	//else
		iNumAutoChecksToPrint = 0;

	var width = 475;
	var height = 280;
	var sQueryString = "";

	sQueryString = "BatchId=" + iBatchId + "&AccountId=" + iAccountId + "&CheckDate=" + sCheckDate + "&DistributionType=" + iDistributionType;
	//	+ "&IncludeAutoPayments=" + bIncludeAutoPayments 
           //     + "&CheckStockId=" + iCheckStockId
				//	+ "&FirstCheck=" + iFirstCheck + "&OrderBy=" + sOrderBy
					//+ "&NumAutoChecksToPrint=" + iNumAutoChecksToPrint + "&UsingSelection=" + sUsingSelection
					//+ "&OrgHierarchyPrint=" + sorghierarchyprint + "&OrgHierarchyLevelPrint=" + sorghierarchylevelprint;
	//Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010

	//var returnval = showModalDialog("home?pg=riskmaster/PrintChecks/PrintChecksBatchFrame&" + sQueryString, null, "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:yes;help:no;center:yes;");
    // aravi5 RMA-10227 Re-Create Check File -Show modal issue starts
	//var returnval = showModalDialog("RePrintCheck.aspx?" + sQueryString, "PrintWnd", "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:yes;help:no;center:yes;");
	if (get_browserName() == "IE") {
	    var returnval = showModalDialog("RePrintCheck.aspx?" + sQueryString, "PrintWnd", "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:yes;help:no;center:yes;");
	}
	else {
	    var returnval = window.open("RePrintCheck.aspx?" + sQueryString, "PrintWnd", "height=" + height + ",width=" + width + ",resizable:no,status:no,scroll:yes,help:no,center:yes;");
	    return false;
	}
	// aravi5 RMA-10227 Re-Create Check File -Show modal issue ends
	//var returnval = showModalDialog("RePrintCheck.aspx");
	

	if (returnval == null || returnval == "||||||" || returnval == "")
		return false;

	var arrCheckDetail = returnval.split("||");
	var iStartNum = arrCheckDetail[0];
	var iEndNum = arrCheckDetail[1];
	var sFileNameAndType = arrCheckDetail[2];
	var sPrintCheckDetails = arrCheckDetail[3];

	window.document.forms[0].PrintCheckDetails.value = sPrintCheckDetails;
	window.document.forms[0].FileNameAndType.value = sFileNameAndType;

	if (confirm("You have printed checks in the range " + iStartNum + " through " + iEndNum + ". Press 'Ok' to record all of checks as having printed successfully. Press 'Cancel' if check printing did not print all checks correctly.")) {
		// Checks has been printed Successfully. Call the web service to update the Check Status. 	
		window.document.forms[0].FirstFailedCheckNumber.value = "0";
	}
	else {
		// All Checks does not print successfully. Ask the check number, up to where the checks has printed successfully.
		width = 475;
		height = 280;
		sQueryString = "";

		sQueryString = "StartNum=" + iStartNum + "&EndNum=" + iEndNum;

		var FirstFailedCheckNumber = showModalDialog("ChecksFailedToReprint.aspx?" + sQueryString, null, "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");
		if (FirstFailedCheckNumber == "") {
			// Checks has been printed Successfully. Call the web service to update the Check Status.
			window.document.forms[0].FirstFailedCheckNumber.value = "0";
		}
		else {
			window.document.forms[0].FirstFailedCheckNumber.value = FirstFailedCheckNumber;
		}
	}

	//window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.UpdateStatusForPrintedChecks";
	//window.document.forms[0].action.value = "PageRefresh" ;
	//window.document.forms[0].submit();
	//OnFormSubmitPrintCheck();	
	
}
