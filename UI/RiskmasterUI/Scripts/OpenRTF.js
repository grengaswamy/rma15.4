var Editor_Launched=0;
var objWord=null;
var	fso=null;
var strFile, strPath, strForm;
var useSilverlight;// RMA-8407 : MITS-36022 : Mudabbir 
var RiskmasterSL;
var bDSConvert = false;//nkaranam2
var strFormDoc = null;//nkaranam2
var phonyDataSourceDocName = 'x5t8y8p0j3h7v8x2m7b6t8.doc';
parent.CommonValidations = parent.parent.CommonValidations;
if (parent.CommonValidations == undefined) {
    if (parent.opener != undefined) {
        if (parent.opener.parent != undefined) {
            if (parent.opener.parent.parent != undefined) {
                parent.CommonValidations = parent.opener.parent.parent.CommonValidations;
            }
        }
    }
}
//var _mergeFailureErrorMessage = "<h3>Mail Merge Failure Information</h3><br/>If you cannot resolve the problem using this information please call technical support."
//+ "<br/><br/><hr/>To complete the merge you must have these Internet Explorer security settings:"
//+ "<ul><li>In Internet Explorer click 'Tools' - 'Internet Options' - 'Security' - 'Local Intranet' - 'Sites' - 'Advanced'</li>"
//+ "<li>If you are using Windows Vista, and your RMX domain is reached via an Intranet, you must uncheck 'Automatically detect intranet network"
//+ "<li>Also on the 'Security' panel, with 'Intranet' selected, click 'Advanced'</li>"
//+ "<li>The domain of the Riskmaster application must NOT be included as an Intranet site (an example of a domain is 'https://www.YourRiskmasterSite.com'.  If your Riskmaster domain is listed as an Intranet site it must be removed from that list.</li>"
//+ "<li>In Internet Explorer click 'Tools' - 'Internet Options' - 'Security' - 'Trusted Sites' - 'Sites'</li>"
//+ "<li>The domain of the Riskmaster application MUST be included as a trusted site (an example of a domain is 'https://www.YourRiskmasterSite.com'</li>"
//+ "<li>Also on the 'Security' panel, with 'Trusted Sites' selected, click 'custom level':"
//    + "<ul>"
//        + "<li>'Initialize and script ActiveX Objects not marked as safe...' must be set to either 'Prompt' or 'Enabled'</li>"
//        + "<li>'Access data sources across domains' must be set to either 'Prompt' or 'Enabled'</li>"
//    + "</ul>"
//+ "</li>"
//+ "<li>Click 'OK' to accept the changes, then close and re-open your browser before trying again.</li></ul>"

//Aman ML Enhancement
var _mergeFailureErrorMessage = "<h3>" + parent.CommonValidations.ValidMergeMergeError1 + "</h3><br/>" + parent.CommonValidations.ValidMergeMergeError2
+ "<br/><br/><hr/>" + parent.CommonValidations.ValidMergeMergeError3
+ "<ul><li>" + parent.CommonValidations.ValidMergeMergeError4 + "</li>"
+ "<li>" + parent.CommonValidations.ValidMergeMergeError5 + "</li>"
+ "<li>" + parent.CommonValidations.ValidMergeMergeError6 + "</li>"
+ "<li>" + parent.CommonValidations.ValidMergeMergeError7 + "</li>"
+ "<li>" + parent.CommonValidations.ValidMergeMergeError8 + "</li>"
+ "<li>" + parent.CommonValidations.ValidMergeMergeError9 + "</li>"
+ "<li>" + parent.CommonValidations.ValidMergeMergeError10
    + "<ul>"
        + "<li>" + parent.CommonValidations.ValidMergeMergeError14 + "</li>"
        + "<li>" + parent.CommonValidations.ValidMergeMergeError12 + "</li>"
    + "</ul>"
+ "</li>"
+ "<li>" + parent.CommonValidations.ValidMergeMergeError13 + "</li></ul>"

// rrachev JIRA RMA-499 
// Word Enumerated Constants
var wdSendToNewDocument = 0
var wdDoNotSaveChanges = 0
function DisplayError(errMsg)
{
    alert(errMsg);
    //document.write('<h3>Error Message</h3><p>' + errMsg + '</p><br />' + _mergeFailureErrorMessage); 
    document.write('<h3>' + parent.CommonValidations.ValidMergeMergeErrorMessage + '</h3><p>' + errMsg + '</p><br />' + _mergeFailureErrorMessage); 
    return false;
}

function GetDataSourceFileName()
{
    var dataSourceFileName = new String(strForm);
    dataSourceFileName = dataSourceFileName.replace(strFile,'DataSource' + strFile);
    //nkaranam2
    var sDSArray = dataSourceFileName.split(".");
    var sDSFormat = sDSArray[sDSArray.length - 1];
    if (sDSFormat == "doc") {
        bDSConvert = false;
    }
    else {
        dataSourceFileName = dataSourceFileName.replace(".docx", ".doc");
        bDSConvert = true;
    }
    return dataSourceFileName;
}
function InitPageSettingsOnBodyLoad()
{
	InitPageSettings();
	
	//enalbe Launch Merge button
	var btnLaunchMerge = document.getElementById("btnLaunchMerge");
	if( btnLaunchMerge != null )
	{
		btnLaunchMerge.disabled = false;
	}
}
function submitthisPage() {

	if (document.forms[0].firstTime.value == "") {
        document.forms[0].btnLaunchMerge.disabled = true;
        document.forms[0].firstTime.value = 1;
        document.forms[0].attach.value = window.opener.document.forms[0].attach.value;
        try {

            if (window.opener.document.forms[0].recordselect.checked) {
                document.forms[0].recordselect.value = window.opener.document.forms[0].recordselect.checked;
            }
        }
        catch (e) {
            document.forms[0].recordselect.value = window.opener.document.forms[0].recordselect.value;
        }
        document.forms[0].lettername.value = window.opener.document.forms[0].lettername.value;
        document.forms[0].DocTitle.value = window.opener.document.forms[0].DocTitle.value;
        document.forms[0].Subject.value = window.opener.document.forms[0].Subject.value;
        document.forms[0].Keywords.value = window.opener.document.forms[0].Keywords.value;
        document.forms[0].Notes.value = window.opener.document.forms[0].Notes.value;
        document.forms[0].Class.value = window.opener.document.forms[0].Class.value;
        document.forms[0].Category.value = window.opener.document.forms[0].Category.value;
        document.forms[0].Type.value = window.opener.document.forms[0].Type.value;
        document.forms[0].Comments.value = window.opener.document.forms[0].Comments.value;
        document.forms[0].DirectDisplay.value = window.opener.document.forms[0].DirectDisplay.value;
        document.forms[0].hdnrowids.value = window.opener.document.forms[0].hdnrowids.value;
        document.forms[0].RecordId.value = window.opener.document.forms[0].RecordId.value;
        document.forms[0].lettername.value = window.opener.document.forms[0].lettername.value;
        document.forms[0].EmailCheck.value = window.opener.document.forms[0].EmailCheck.value;
        document.forms[0].lstMailRecipient.value = window.opener.document.forms[0].lstMailRecipient.value;
        document.forms[0].TableName.value = window.opener.document.forms[0].TableName.value;
        document.forms[0].txtSelectedRecipient.value = window.opener.document.forms[0].txtSelectedRecipient.value;
        document.forms[0].submit();
        pleaseWait.Show();
    }
    else
        document.forms[0].btnLaunchMerge.disabled = false;
}

function InitPageSettings() {
    var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
    var step = '';
    //get name of template document temporary file
	try
	{
	    if (document.getElementById('hdnTempFileName') != null) {
	        if (document.getElementById('hdnTempFileName').value + "" != "") {
	            strFile = document.getElementById('hdnTempFileName').value;
                //nkaranam2
                if (IEbrowser) {
                    objWord = new ActiveXObject("Word.Application");
                    if (objWord.version >= 12.0) {
                        //strFile = strFile + "x";
                        document.getElementById("FileName").innerText = strFile + "x";
                    }
                    if (objWord != null) {
                        objWord.Quit(0);
                        objWord = null;
                    }
                }
	        }
	        else {
	            strFile = '';
	        }
	    }
	    else {
	        strFile = '';
	    }	        
	}
	catch(e){;}

    //get full path to where the template document will be written on client machine
   
    // RMA-8407 : MITS-36022 : Mudabbir 
    if (document.getElementById("hdnSilver") != null)
        useSilverlight = document.getElementById("hdnSilver").value;
    else
        useSilverlight = !IEbrowser;
    //if (useSilverlight.toString() == "true") {
    if (useSilverlight == true || useSilverlight == "true" || !IEbrowser) {
        if (Silverlight.isInstalled("5.0")) {
            //RMA-11630 : bkuzhanthaim : Silverlight installation Image should present only in chrome
            //document.getElementById("silverlightControlHost").style.display = "none";
        }
        else
            alert(parent.CommonValidations.ValidMergeMergeSilverlight);
    }

    else {
        try {
            document.getElementById("silverlightControlHost").style.display = "none";//RMA-11630
            //step = 'create Scripting.FileSystemObject';
            step = parent.CommonValidations.InitPageSettingStep1;
            fso = new ActiveXObject("Scripting.FileSystemObject");
            //step = 'retrieve GetSpecialFolder(2).Path';
            step = parent.CommonValidations.InitPageSettingStep2;
            var path = fso.GetSpecialFolder(2).Path;
            strForm = path + "\\" + strFile;
            return true;
        }
        catch (e) {
            //strForm = "Unable to discover user's special folder because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step;	
            strForm = parent.CommonValidations.InitPageSettingStep3 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step;
            return false;
        }

    }
}

function IsWordOpen()
{
	//Handle Case where User has not yet closed Word. (Do not open duplicate windows.)
	try
	{
		
		if (objWord.visible != null)
			return true;
		else
			return false;
	}
	catch(e){return false;}

}

function CleanAndClose()
{
	try
	{
		//Clean Up.
	    // RMA-8407 : MITS-36022 : Mudabbir 
	    var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
	    if (document.getElementById("hdnSilver") != null)
	        useSilverlight = document.getElementById("hdnSilver").value;
	    else
	        useSilverlight = !IEbrowser;
	    //if (useSilverlight.toString() == "true") {
	    if (useSilverlight == true || useSilverlight == "true" || !IEbrowser){
                    if (Silverlight.isInstalled("5.0")) {

                        document.getElementById("Silverlight").Value = "ClientBin/RiskmasterSL.xap";
                        RiskmasterSL = document.getElementById("Silverlight");
                        strForm = RiskmasterSL.Content.SL2JS.GetDocumentPath(strFile, "", "");
                        RiskmasterSL.Content.SL2JS.CleanAndClose();
                    }
                    else
                        alert(parent.CommonValidations.ValidMergeMergeSilverlight);
                }
                else {
		if (fso.FileExists(strForm)) //Main doc temp file.
			fso.DeleteFile(strForm);
                }
            }
	catch (e)
	{;}
	//Shruti for 7337
	window.opener.close();
	//Shruti for 7337 ends
	window.close();
	
	return false;
}

    function CleanAndCancel() {
        try {

            // RMA-8407 : MITS-36022 : Mudabbir 
            var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
            if (document.getElementById("hdnSilver") != null)
                useSilverlight = document.getElementById("hdnSilver").value;
            else
                useSilverlight = !IEbrowser;
            //if (useSilverlight.toString() == "true") {
            if (useSilverlight == true || useSilverlight == "true" || !IEbrowser){
                if (Silverlight.isInstalled("5.0")) {

                    document.getElementById("Silverlight").Value = "ClientBin/RiskmasterSL.xap";
                    RiskmasterSL = document.getElementById("Silverlight");
                    strForm = RiskmasterSL.Content.SL2JS.GetDocumentPath(strFile, "", "");
                    RiskmasterSL.Content.SL2JS.CleanAndCancel();
                }
                else
                    alert(parent.CommonValidations.ValidMergeMergeSilverlight);
            }

                //Mudabbir ends
            else {
                if (IsWordOpen()) {
                    objWord.Quit(0);
                    objWord = null;
                }

                //Clean Up.
                if (fso.FileExists(strForm)) //Main doc temp file.
                    fso.DeleteFile(strForm);
            }
        }
        catch (e)
        {; }
        //Shruti for 7337
        window.opener.close();
        window.close();

        return false;
    }

    function StartEditor(AX, saveToDisk) {
       // RMA-8407 : MITS-36022 : Mudabbir 
        var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
        if (document.getElementById("hdnSilver") != null)
            useSilverlight = document.getElementById("hdnSilver").value;
        else
            useSilverlight = !IEbrowser;
        //if (useSilverlight.toString() == "true") {
        if (useSilverlight == true || useSilverlight == "true" || !IEbrowser){
            if (Silverlight.isInstalled("5.0")) {

                document.getElementById("Silverlight").Value = "ClientBin/RiskmasterSL.xap";
                RiskmasterSL = document.getElementById("Silverlight");
                InitPageSettings();//setting strFile
                strForm = RiskmasterSL.Content.SL2JS.GetDocumentPath(strFile, "", "");
                var ret = RiskmasterSL.Content.SL2JS.StartEditor(AX, strFile, saveToDisk);
                if (ret == "true")
                    return true;
                else
                    return false;
            }
            else
                alert(parent.CommonValidations.ValidMergeMergeSilverlight);
        }
     
            //Mudabbir ends
        else {
            if (AX == '') {
                //tkr 8/2007 HACK.
                //could not figure out how to get the error page to show from here.
                //the xpl logic is incredibly complex and fragile, and throws
                //confusing error message when clicked twice
                if (!InitPageSettings()) {
                    //InitPageSettings() sets strForm to the error message
                    //alert('ddd');
                    return DisplayError(strForm);
                }
            }
            else {
                return false;
            }
            var adTypeBinary = 1;
            var adSaveCreateOverWrite = 2;
            var adTypeText = 2;
            //var step = 'check IsWordOpen';
            var step = parent.CommonValidations.CheckWordStatus;

            if (IsWordOpen()) {
                //step = 'activate Word';
                step = parent.CommonValidations.MergeStartEditorStep1;
                objWord.Activate();
                return false;
            }

            try {//save template and datasource local
                //use DomDocument to convert base64 string to bytes	
                //step = 'get template base64 text';
                step = parent.CommonValidations.ValidCreateTemplateStep2;
                var template = (document.getElementById('hdnRTFContent').value);
                if (template == null || template == "")
                    //throw new Error("The word merge template was not passed to the browser.  Please check the RMX web errors server error log for an error message.  The most likely cause of this error is that the word merge template file (*.doc or *.rtf) could not be found in document storage.");    
                    throw new Error(parent.CommonValidations.MergeTemplateError);

                //step = 'get data source base64';
                step = parent.CommonValidations.GetDataSourceBase64;
                var dataSource = (document.getElementById('hdnDataSource').value);
                if (dataSource == null || dataSource == "")
                    //throw new Error("The word merge data source was not passed to the browser, please check the RMX web errors server error log for an error message.  The most likely cause of this error is that the word merge header file (*.hdr file) could not be found in document storage, or the application could not interpret a merge field name.");    
                    throw new Error(parent.CommonValidations.MergeDataSourceError);

                //step = 'get header file base64';
                step = parent.CommonValidations.GetHeaderFileBase64;
                var headerContent = (document.getElementById('hdnHeaderContent').value);
                if (dataSource == null || headerContent == "")
                    //throw new Error("The word merge header was not passed to the browser, please check the RMX web errors server error log for an error message.  The most likely cause of this error is that the word merge header file (*.hdr file) could not be found in document storage, or the application could not interpret a merge field name.");    
                    throw new Error(parent.CommonValidations.MergeHeaderError);

                //step = 'instantiate xml document for template';
                step = parent.CommonValidations.InstantiateXMLDoc;
                var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
                //step = 'create b64 element for template';
                step = parent.CommonValidations.Createb64Element;
                var e = xmlDoc.createElement("b64");
                //step = 'set element.dataType to bin.base64 for template';
                step = parent.CommonValidations.SetElementDataType;
                e.dataType = "bin.base64";

                //step = 'set element.text to template base64 text';
                step = parent.CommonValidations.ValidCreateTemplateStep6;
                e.text = template;
                //step = 'get bytes from template element';
                step = parent.CommonValidations.ValidCreateTemplateStep7;
                var templateBytes = e.nodeTypedValue;

                //use ADODB.Stream to save template bytes to disc
                //step = 'instantiate Stream object for template';
                step = parent.CommonValidations.ValidCreateTemplateStep8;
                var stm = new ActiveXObject("ADODB.Stream");
                //step = 'set template stm.Type';
                step = parent.CommonValidations.ValidCreateTemplateStep9;
                stm.Type = adTypeBinary;
                //step = 'open template stream';
                step = parent.CommonValidations.ValidCreateTemplateStep10;
                stm.Open();
                //step = 'write bytes to template stream';
                step = parent.CommonValidations.ValidCreateTemplateStep11;
                stm.Write(templateBytes);
                //step = 'save template stream to file ' + strForm;
                step = parent.CommonValidations.ValidCreateTemplateStep12 + " " + strForm;
                stm.SaveToFile(strForm, adSaveCreateOverWrite);
                //step = 'close stream';
                step = parent.CommonValidations.ValidCreateTemplateStep13;
                stm.Close();
                stm = null;

                //step = 'instantiate xml document for datasource';
                step = parent.CommonValidations.InstantiateXMLDocDataSource;
                xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
                //step = 'create b64 element for datasource';
                step = parent.CommonValidations.Createb64ElementDataSource;
                e = xmlDoc.createElement("b64");
                //step = 'set element.dataType to bin.base64 for datasource';
                step = parent.CommonValidations.SetElementDataTypeDataSource;
                e.dataType = "bin.base64";

                //step = 'set element.text to datasource base64 text';
                step = parent.CommonValidations.SetElementText;
                e.text = dataSource;
                //step = 'get bytes from datasource element';
                step = parent.CommonValidations.GetBytesDataSource;
                var dataSourceBytes = e.nodeTypedValue;

                //use ADODB.Stream to save datasource to disk
                var dataSourceFileName = GetDataSourceFileName();
                //step = 'instantiate Stream object for data source';
                step = parent.CommonValidations.InstantiateStreamObj;
                stm = new ActiveXObject("ADODB.Stream");
                //step = 'set datasource stm.Type';
                step = parent.CommonValidations.SetDataSourceSTMType;
                stm.Type = adTypeBinary;
                //step = 'open datasource stream';
                step = parent.CommonValidations.OpenDataSourceStream;
                stm.Open();
                //step = 'write bytes to datasource stream';
                step = parent.CommonValidations.WriteDataSourceStream;
                stm.Write(dataSourceBytes);
                //step = 'save datasource stream to file ' + dataSourceFileName;
                step = parent.CommonValidations.SaveDataSourceStream + " " + dataSourceFileName;
                stm.SaveToFile(dataSourceFileName, adSaveCreateOverWrite);
                //step = 'close stream';
                step = parent.CommonValidations.ValidCloseStreamCheck;
                stm.Close();
                stm = null;

                //step = 'instantiate xml document for header';
                step = parent.CommonValidations.InstantiateXMLDocHeader;
                xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
                //step = 'create b64 element for header';
                step = parent.CommonValidations.Createb64ElementHeader;
                e = xmlDoc.createElement("b64");
                //step = 'set element.dataType to bin.base64 for header';
                step = parent.CommonValidations.SetElementDataTypeHeader;
                e.dataType = "bin.base64";

                //step = 'set element.text to header base64 text';
                step = parent.CommonValidations.SetElementTextHeader;
                e.text = headerContent;
                //step = 'get bytes from header element';
                step = parent.CommonValidations.GetBytesHeader;
                var headerBytes = e.nodeTypedValue;

		//use ADODB.Stream to save header to disk
		var headerFileName;
		if( strForm.length > 4 )
		{
			headerFileName = strForm.substring(0, strForm.length-4) + ".hdr";
			//step = 'instantiate Stream object for data source';
            step = parent.CommonValidations.InstantiateStreamObj;
			stm = new ActiveXObject("ADODB.Stream");
			//step = 'set datasource stm.Type';
            step = parent.CommonValidations.SetDataSourceSTMType;
			stm.Type = adTypeBinary;
			//step = 'open datasource stream';
            step = parent.CommonValidations.OpenDataSourceStream;
			stm.Open();
			//step = 'write bytes to datasource stream';
            step = parent.CommonValidations.WriteDataSourceStream;
			stm.Write(headerBytes );
			//step = 'save datasource stream to file ' + headerFileName;
            step = parent.CommonValidations.SaveDataSourceStream + " " + headerFileName;
			stm.SaveToFile(headerFileName, adSaveCreateOverWrite);
			//step = 'close stream';
            step = parent.CommonValidations.ValidCloseStreamCheck;
			stm.Close();
			stm = null;
        }
        e = null;
        xmlDoc = null;
    }
    catch (e)
    {		
	    //return DisplayError("The word template and data source could not be saved to this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        return DisplayError(parent.CommonValidations.WordMergeTempError + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
    }

    try
    {
    	//In the future if Office 2003 support is not required, should call WordMerge4Vesion11Lower12Higher
    	//and change data source content.
	//Function call Changed by pawan for mits 10983 reverted back to routine used by Tom since few properties worked well well with it.
    	WordMerge4Vesion12Higher(strForm, headerFileName, dataSourceFileName,saveToDisk);
    	//objWord.FileSaveAs(strForm);
    }
    catch (e)
    {
	    //return DisplayError("The local Microsft Word application required to complete the wizard could not be loaded because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        return DisplayError(parent.CommonValidations.WordApplicationError + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
    }

    Editor_Launched=1;
    return false;
        }
    
}

function WordMerge4Vesion12Higher(sFileName, sHeaderFileName, sDataFileName,saveToDisk)
{
	var wordDoc = null;
	var step = "";
	var isProtected = false;
	var activated = false; //rsharma220 for RMA-8384
    try
    {//open template
        //step = 'open word document: ' + strForm;
        step = parent.CommonValidations.OpenWordDocument + " " + strForm;
        objWord = new ActiveXObject("Word.Application");
        //Start
        // wordDoc = objWord.Documents.Open(strForm);
        //nkaranam2
        var sFileNameTemp = new String(strForm);
        var sArray = sFileNameTemp.split(".");
        var sFileFormat = sArray[sArray.length - 1];
        if (sFileFormat == "doc") {
            if (objWord.version < 12.0) {
                var TemplateFormat = document.getElementById('hdnTemplateFormat').value;
                if (TemplateFormat != "docx") {
                    wordDoc = objWord.Documents.Open(strForm);
                    objWord.ActiveDocument.Saveas(strForm);
                }
                else {
                    alert(parent.CommonValidations.MSWordVersionCheck); //A newer version of Microsoft Office is necessary to open this template.
                    if (objWord != null) {
                        objWord.Quit(0);
                        objWord = null;
                    }
                    return false;
                }
            }
            else if (objWord.version >= 12.0) {
                wordDoc = objWord.Documents.Open(strForm, 0, 0, 0, '', '', 0, '', '', 0, 0, 0, 1);
                //Try to save the document to make MSWord's SaveDate function work
                objWord.visible = true;
                objWord.Activate();
                //End
                strFormDoc = strForm;
                strForm = strForm.replace(".doc", ".docx");
                objWord.ActiveDocument.Saveas(strForm, 12);
                if (objWord != null) {
                    objWord.Quit(0);
                    objWord = null;
                }

                objWord = new ActiveXObject("Word.Application");
                wordDoc = objWord.Documents.Open(strForm);
            }
        }
        else if (sFileFormat == "docx") {
            if (objWord.version >= 12.0) {
                wordDoc = objWord.Documents.Open(strForm);
            }

        }
    }
    catch (e)
    {
	    //DisplayError("The word template at " + strForm + " could not be opened by Microsft Word because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        DisplayError(parent.CommonValidations.WordMergeTemplateErr1 + " " + strForm + " " + parent.CommonValidations.WordMergeTemplateErr2 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
	    if (objWord !=null )
	    {	
		    objWord.Quit(0);
		    objWord = null;
		    return false;
	    }
    }
												
    try
    {//Verify we have full edit rights.	Isprotected added by pawan for 10983								   
	    if( objWord.ActiveDocument.ProtectionType != -1) //if not totally unlocked then
	{	   
	 objWord.ActiveDocument.UnProtect("");
		isProtected = true;
	}
    }
    catch (e)
    {
	    //alert("The word template at " + strForm + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
        alert(parent.CommonValidations.WordMergeTemplateErr1 + " " + strForm + " " + (parent.CommonValidations.WordMergeTemplateErr3).split('\\n').join('\n'));
	    objWord.visible = true;
	    objWord.Activate();	
    }

    try
    {   
        //step='activate Word';
        step = parent.CommonValidations.ValidActivateWordStepCheck;
	    objWord.visible = true;
        //rsharma220 JIRA 8384 MITS 37850 : Activate used to get called before the document saved. Hence, put in sleep for delay.
	    for (var count = 0; count < 10; count++) {
	        try {
	            if (!activated) {
	                sleep(1000);
	                objWord.Activate();
	                activated = true;
	            }
	        }
	        catch (ex) {
	            if (count == 9) {
	                throw ex;
	            }
	        }
	    }
	    wordDoc.Select();  //this call instantiates MailMerge object
	    //step='open header';
        step = parent.CommonValidations.OpenHeader;
        if( sHeaderFileName.length > 0 )
        {
        	wordDoc.MailMerge.OpenHeaderSource(sHeaderFileName);
        }
	    //step='open data source';
        step = parent.CommonValidations.OpenDataSource;
        wordDoc.MailMerge.OpenDataSource(sDataFileName);
	wordDoc.MailMerge.DataSource.FirstRecord=1;
		
	//Get number of records to be merged
	wordDoc.MailMerge.DataSource.LastRecord=1;
	var objRecordNumber = document.getElementById("hdnNumRecords");
	if( objRecordNumber != null )
	{
		var sRecordNumber = objRecordNumber.value;
		if( !isNaN(sRecordNumber) )
		{
			wordDoc.MailMerge.DataSource.LastRecord = parseInt(sRecordNumber);
		}
	}
	wordDoc.MailMerge.Destination=0;

        //step='do mail merge';
        step = parent.CommonValidations.DoMailMerge;
        wordDoc.MailMerge.Execute(false);
        //step='select merged document';
        step = parent.CommonValidations.SelectMergedDoc;
        //var mergedDoc = objWord.Documents.Item(1);
	
        
        //step='close the original';
        step = parent.CommonValidations.CloseOriginal;
	//wordDoc.Saveas(strForm)
        //wordDoc.Select();   
        wordDoc.Close(0);//nkaranam2

        //step='select merged document';
	step = parent.CommonValidations.SelectMergedDoc;
	//Not need rt now doesn't suffice clients requirement pawan changed for mits 10983 enabling protection if already protected
	//if (isProtected == true)
	//	objWord.ActiveDocument.Protect(2,true,"");
		
	//change end
       //Deb : MITS 32759 
       objWord.visible = true;
       objWord.Activate();
       //Deb : MITS 32759 
       objWord.ActiveDocument.Select(); 

        if(saveToDisk)
        {
            //step='save the merged document over the original at ' + strForm;
            step = parent.CommonValidations.SaveMergedDoc + " " + strForm; 
            objWord.ActiveDocument.Saveas(strForm, 12);//nkaranam2
        }
	//nkaranam2
        if (strFormDoc != null && fso.FileExists(strFormDoc))
            fso.DeleteFile(strFormDoc);
    }
    catch (e)
    {
	   // DisplayError("Microsoft Word could not accomplish the merge because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        DisplayError(parent.CommonValidations.MSWordError + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
	    objWord.Quit(0);
	    objWord = null;
	    return false;
    }
    //Done
}

function Finish() 
{
    var step = '';
    // RMA-8407 : MITS-36022 : Mudabbir 
    var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
    if (document.getElementById("hdnSilver") != null)
        useSilverlight = document.getElementById("hdnSilver").value;
    else
        useSilverlight = !IEbrowser;
    //if (useSilverlight.toString() == "true") {
    if (useSilverlight == true || useSilverlight == "true" || !IEbrowser){
            if (Silverlight.isInstalled("5.0")) {

                document.getElementById("Silverlight").Value = "ClientBin/RiskmasterSL.xap";
                RiskmasterSL = document.getElementById("Silverlight");
                strForm = RiskmasterSL.Content.SL2JS.GetDocumentPath(strFile, "", "");
                var ret = RiskmasterSL.Content.SL2JS.Finish(Editor_Launched);
                if (ret == "true")
                    return true;
                else
                    return false;
            }
            else
                alert(parent.CommonValidations.ValidMergeMergeSilverlight);
        }
      
            //Mudabbir ends
        else {
	try
    {
	    if (Editor_Launched==0)
	    {
	        //alert("You must click the 'Launch Merge' button and complete the merge before finalizing.");
	        alert(MergeEditResultValidations.ValidCompleteMerge); //Aman ML Changes
		    return false;
	    }
		
        if (IsWordOpen() && objWord.Documents.Count == 1)
        {
            //step = 'activate Word';
            step = MergeEditResultValidations.ValidateFinishStep1; //Aman ML Change
            objWord.Activate();

            //step='get pointer to merged document';
            step = MergeEditResultValidations.ValidateFinishStep2;
            var wordDoc = objWord.Documents.Item(1);

            //step='select, save and close the document';
            step = MergeEditResultValidations.ValidateFinishStep3;
            wordDoc.Select();
            wordDoc.Save();   
            wordDoc.Close();           
        }
        
	    var adTypeBinary = 1;
	    //step = 'instantiate Stream object';
	    step = MergeEditResultValidations.ValidateFinishStep4;
        var stm = new ActiveXObject("ADODB.Stream");
        //step = 'set stm.Type';
        step = MergeEditResultValidations.ValidateFinishStep5;
        stm.Type = adTypeBinary;
        //step = 'open stream';
        step = MergeEditResultValidations.ValidateFinishStep6;
        stm.Open();
        //step = 'read file ' + strForm + ' into stream';
        step = MergeEditResultValidations.ValidateFinishStep7 + " " + strForm + " " + MergeEditResultValidations.ValidateFinishStep8;
       
        stm.LoadFromFile(strForm);
        //step = 'read bytes from stream';
        step = MergeEditResultValidations.ValidateFinishStep9;
        var bytes = stm.Read();
        //step = 'close stream';
        step = MergeEditResultValidations.ValidateFinishStep10;
        stm.Close();
        stm = null;

        //step = 'instantiate xml document';
        step = MergeEditResultValidations.ValidateFinishStep11;
        var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
        xmlDoc.async = false;
        //step = 'create root element';
        step = MergeEditResultValidations.ValidateFinishStep12;
        var xmlElement = xmlDoc.createElement("root");
        xmlDoc.documentElement = xmlElement;
        xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
        //step = 'create element to hold word document';
        step = MergeEditResultValidations.ValidateFinishStep13;
        var xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
        xmlNode.dataType = "bin.base64"
        //step = 'read stream into element';
        step = MergeEditResultValidations.ValidateFinishStep14;
        xmlNode.nodeTypedValue = bytes;
        //step = 'read base64 text from element';
        step = MergeEditResultValidations.ValidateFinishStep15;
        var base64 = xmlNode.text;

        //step='cleanup';
        step = MergeEditResultValidations.ValidateFinishStep16;
        xmlDoc = null;
        xmlElement = null;
        xmlNode = null;
            
	    document.all.hdnNewFileName.value = strForm;
	    document.all.hdnRTFContent.value = base64;
    }
    catch (e)
    {
        //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        alert(MergeEditResultValidations.ValidateFinishStep17 + " " + strForm + " " + MergeEditResultValidations.ValidateFinishStep18 + " " + e.name + ',' + e.message + '\r\n' + MergeEditResultValidations.ValidateFinishStep23 + " "+ step);
	    xmlDoc = null;
        xmlElement = null;
        xmlNode = null;
	    return false;
    }
	
	//Clean Up    
	try 
	{
	    //step = 'close word';
	    step = MergeEditResultValidations.ValidateFinishStep19;
		if(IsWordOpen())
		{	
			objWord.Quit(0);
			objWord = null;

        }
        //step = 'delete local copy of document at ' + strForm;
        step = MergeEditResultValidations.ValidateFinishStep20 + " " + strForm;
		if (fso.FileExists(strForm)) //Main doc temp file.
			fso.DeleteFile(strForm);
		
		var dataSourceFileName = GetDataSourceFileName();
		//step = 'delete data source file at ' + dataSourceFileName;
		step = MergeEditResultValidations.ValidateFinishStep21 + " " + dataSourceFileName;
		if (fso.FileExists(dataSourceFileName))
		    fso.DeleteFile(dataSourceFileName);
	}
	catch (e)
	{
	    //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
	    alert(MergeEditResultValidations.ValidateFinishStep21 + " " + e.name + ',' + e.message + '\r\n' + MergeEditResultValidations.ValidateFinishStep23 + " " + step);
	}
	
	//document.getElementById('hdnaction').value="AttachAndFinish";
	return true;
	}
    }

//spahariya 10/29/2012 MITS 28867- start ******************************
function sleep(milliSeconds){
	var startTime = new Date().getTime(); // get the current time
	while (new Date().getTime() < startTime + milliSeconds);
}

function FinishOpenMerge(saveToTemp) {
    var activated = false; //rsharma220 for RMA-8384
    // RMA-8407 : MITS-36022 : Mudabbir 
    var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
    if (document.getElementById("hdnSilver") != null)
        useSilverlight = document.getElementById("hdnSilver").value;
    else
        useSilverlight = !IEbrowser;
    //if (useSilverlight.toString() == "true") {
    if (useSilverlight == true || useSilverlight == "true" || !IEbrowser){
        if (Silverlight.isInstalled("5.0")) {
            document.getElementById("Silverlight").Value = "ClientBin/RiskmasterSL.xap";
            RiskmasterSL = document.getElementById("Silverlight");
            strForm = RiskmasterSL.Content.SL2JS.GetDocumentPath(strFile, "", "");
            var ret = RiskmasterSL.Content.SL2JS.FinishOpenMerge(saveToTemp, Editor_Launched, strFile);
            if (ret == "true")
                return true;
            else
                return false;
        }
        else
            alert(parent.CommonValidations.ValidMergeMergeSilverlight);
    }
        //Mudabbir ends
    else {
        var step = '';
        var wordTempDoc = null;
        var objWordTemp = null;
        if (Editor_Launched == 0) {
            alert(parent.CommonValidations.MustClickLaunchBtn); //Swati 30/01/2013
            //alert("You must click the 'Launch Merge' button and complete the merge before finalizing.");
            return false;
        }
        if (saveToTemp) {
            try {//open template
                //step = 'open word document: ' + strForm;
                step = parent.CommonValidations.OpenWordDocument + ' ' + strForm;   //Swati 31/01/2013
                objWordTemp = new ActiveXObject("Word.Application");
                wordTempDoc = objWordTemp.Documents.Open(strForm);

            //Try to save the document to make MSWord's SaveDate function work
                    objWordTemp.ActiveDocument.Saveas(strForm);
        }
        catch (e) {
            DisplayError(parent.CommonValidations.WordMergeTemplateErr1 + " " + sFileName + " " + parent.CommonValidations.WordMergeTemplateErr2 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);   //Swati 31/01/2013
            //DisplayError("The word template at " + strForm + " could not be opened by Microsft Word because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
            if (objWordTemp != null) {
                objWordTemp.Quit(0);
                objWordTemp = null;
                return false;
            }
        }

        try {//Verify we have full edit rights.	Isprotected added by pawan for 10983								   
            if (objWordTemp.ActiveDocument.ProtectionType != -1) //if not totally unlocked then
            {
                objWordTemp.ActiveDocument.UnProtect("");
                isProtected = true;
            }
        }
        catch (e) {
            //alert("The word template at " + strForm + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
            alert(parent.CommonValidations.WordMergeTemplateErr1 + " " + sFileName + " " + (parent.CommonValidations.WordMergeTemplateErr3).split('\\n').join('\n'));   //Swati 31/01/2013
            objWordTemp.visible = false;
            objWordTemp.Activate();
        }

        try {
            //step = 'activate Word';
            step = parent.CommonValidations.ValidActivateWordStepCheck;  //Swati 31/0/2013
            objWordTemp.visible = true;
            //rsharma220 JIRA 8384 MITS 37850 : Activate used to get called before the document saved. Hence, put in sleep for delay.
            for (var count = 0; count < 10; count++) {
                try {
                    if (!activated) {
                        sleep(1000);
                        objWordTemp.Activate();
                        activated = true;
                    }
                }
                catch (ex) {
                    if (count == 9) {
                        throw ex;
                    }
                }
            }
            wordTempDoc.Select();  //this call instantiates MailMerge object
            //step = 'open header';
            step = parent.CommonValidations.OpenHeader;  //Swati 31/0/2013
            //headerFileName = strForm.substring(0, strForm.length - 4) + ".hdr";
            var sFileNameTemp = new String(strForm);
            var sArray = sFileNameTemp.split(".");
            var sFileFormat = sArray[sArray.length - 1];
            if (sFileFormat == "doc") {
                headerFileName = strForm.substring(0, strForm.length - 4) + ".hdr";
            }
            else if (sFileFormat == "docx") {
                headerFileName = strForm.substring(0, strForm.length - 5) + ".hdr";
            }
            if (headerFileName.length > 0) {
                wordTempDoc.MailMerge.OpenHeaderSource(headerFileName);
            }
            //step = 'open data source';
            step = parent.CommonValidations.OpenDataSource;  //Swati 31/0/2013
            var dataSourceFileName = GetDataSourceFileName();
            wordTempDoc.MailMerge.OpenDataSource(dataSourceFileName);
            wordTempDoc.MailMerge.DataSource.FirstRecord = 1;

            //Get number of records to be merged
            wordTempDoc.MailMerge.DataSource.LastRecord = 1;
            var objRecordNumber = document.getElementById("hdnNumRecords");
            if (objRecordNumber != null) {
                var sRecordNumber = objRecordNumber.value;
                if (!isNaN(sRecordNumber)) {
                    wordTempDoc.MailMerge.DataSource.LastRecord = parseInt(sRecordNumber);
                }
            }
            wordTempDoc.MailMerge.Destination = 0;

            //step = 'do mail merge';
            step = parent.CommonValidations.DoMailMerge;  //Swati 31/0/2013
            wordTempDoc.MailMerge.Execute(false);
            //step = 'select merged document';
            step = parent.CommonValidations.SelectMergedDoc;  //Swati 31/0/2013
            //var mergedDoc = objWord.Documents.Item(1);


            //step = 'close the original';
            step = parent.CommonValidations.CloseOriginal;  //Swati 31/0/2013
            //wordTempDoc.Close();
            wordTempDoc.Close(0);//nkaranam2
            //step = 'select merged document';
            step = parent.CommonValidations.SelectMergedDoc;  //Swati 31/0/2013

            objWordTemp.ActiveDocument.Select();

            //step = 'save the merged document over the original at ' + strForm;
            step = parent.CommonValidations.SaveMergedDoc + ' ' + strForm;  //Swati 31/0/2013
            objWordTemp.ActiveDocument.Saveas(strForm);
            objWordTemp.Quit(0);
            objWordTemp = null;
            sleep(2000);

        }
        catch (e) {
            DisplayError(parent.CommonValidations.MSWordError + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step); // Swati 31/01/2013
            //DisplayError("Microsoft Word could not accomplish the merge because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
            objWordTemp.Quit(0);
            objWordTemp = null;
            return false;
        }
    }

    try {        

        if (IsWordOpen() && objWord.Documents.Count == 1) {
            //step = 'activate Word';
            step = MergeClaimLetterValidations.FinishOpenStep1; //Swati 31/01/2013
            objWord.Activate();

            //step = 'get pointer to merged document';
            step = MergeClaimLetterValidations.FinishOpenStep2; //Swati 31/01/2013
            var wordDoc = objWord.Documents.Item(1);

            //step = 'select, save and close the document';
            step = MergeClaimLetterValidations.FinishOpenStep3; //Swati 31/01/2013
            wordDoc.Select();
            wordDoc.Save();
            wordDoc.Close();
        }

        var adTypeBinary = 1;
        //step = 'instantiate Stream object';
        step = MergeClaimLetterValidations.FinishOpenStep4; //Swati 31/01/2013
        var stm = new ActiveXObject("ADODB.Stream");
        //step = 'set stm.Type';
        step = MergeClaimLetterValidations.FinishOpenStep5; //Swati 31/01/2013
        stm.Type = adTypeBinary;
        //step = 'open stream';
        step = MergeClaimLetterValidations.FinishOpenStep6; //Swati 31/01/2013
        stm.Open();
        //step = 'read file ' + strForm + ' into stream';
        step = MergeClaimLetterValidations.FinishOpenStep7 + ' ' + strForm + ' ' + MergeClaimLetterValidations.FinishOpenStep8;
        stm.LoadFromFile(strForm);
        //step = 'read bytes from stream';
        step = MergeClaimLetterValidations.FinishOpenStep9; //Swati 31/01/2013
        var bytes = stm.Read();
        //step = 'close stream';
        step = MergeClaimLetterValidations.FinishOpenStep10; //Swati 31/01/2013
        stm.Close();
        stm = null;

        //step = 'instantiate xml document';
        step = parent.CommonValidations.ValidCreateTemplateStep3;  //Swati 31/01/2013
        var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
        xmlDoc.async = false;
        //step = 'create root element';
        step = parent.CommonValidations.ValidRootElementCheck;  //Swati 31/01/2013
        var xmlElement = xmlDoc.createElement("root");
        xmlDoc.documentElement = xmlElement;
        xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
        //step = 'create element to hold word document';
        step = MergeClaimLetterValidations.FinishOpenStep11; //Swati 31/01/2013
        var xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
        xmlNode.dataType = "bin.base64"
        //step = 'read stream into element';
        step = MergeClaimLetterValidations.FinishOpenStep12; //Swati 31/01/2013
        xmlNode.nodeTypedValue = bytes;
        //step = 'read base64 text from element';
        step = MergeClaimLetterValidations.FinishOpenStep13; //Swati 31/01/2013
        var base64 = xmlNode.text;

        //step = 'cleanup';
        step = MergeClaimLetterValidations.FinishOpenStep14; //Swati 31/01/2013
        xmlDoc = null;
        xmlElement = null;
        xmlNode = null;

        document.all.hdnNewFileName.value = strForm;
        document.all.hdnRTFContent.value = base64;
		document.all.firstTime.value = "";

    }
    catch (e) {
        alert(MergeClaimLetterValidations.FinishOpenStepError1 + " " + strForm + " " + MergeClaimLetterValidations.FinishOpenStepError2 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step); // Swati 31/01/2013
        //alert("The local copy of the document at " + strForm + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        xmlDoc = null;
        xmlElement = null;
        xmlNode = null;
        return false;
    }

    //Clean Up    
    try {
        //step = 'close word';
        step = MergeClaimLetterValidations.FinishOpenStep15; //Swati 31/01/2013
        if (IsWordOpen()) {
            objWord.Quit(0);
            objWord = null;
        }

        //step = 'delete local copy of document at ' + strForm;
        step = parent.CommonValidations.ValidDeleteLocalCopyCheck + ' ' + strForm;   //Swati 31/01/2013
        if (fso.FileExists(strForm)) //Main doc temp file.
            fso.DeleteFile(strForm);

        dataSourceFileName = GetDataSourceFileName();
        step = MergeClaimLetterValidations.FinishOpenStep16 + ' ' + dataSourceFileName;  //Swati 31/01/2013
        //step = 'delete data source file at ' + dataSourceFileName;
        if (fso.FileExists(dataSourceFileName))
            fso.DeleteFile(dataSourceFileName);
    }
    catch (e) {
        alert(parent.CommonValidations.NoLocalCopyClean + " " + e.name + ',' + e.message + '\r\n' + MergeClaimLetterValidations.FinishOpenStep17 + " " + step); // Swati 30/01/2013
        //alert("The local copy of the document could not be cleaned up because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
    }

        //document.getElementById('hdnaction').value="AttachAndFinish";
        return true;
    }
}
//spahariya - end******************************
//Start :Added by Navdeep For Chubb ACKNowledgement Claim Letter
function StartClmEditor()
{
    // RMA-8407 : MITS-36022 : Mudabbir 
    var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
    if (document.getElementById("hdnSilver") != null)
        useSilverlight = document.getElementById("hdnSilver").value;
    else
        useSilverlight = !IEbrowser;
    //if (useSilverlight.toString() == "true") {
    if (useSilverlight == true || useSilverlight == "true" || !IEbrowser){
            if (Silverlight.isInstalled("5.0")) {

                document.getElementById("Silverlight").Value = "ClientBin/RiskmasterSL.xap";
                RiskmasterSL = document.getElementById("Silverlight");
                InitPageSettings();//setting strFile
                strForm = RiskmasterSL.Content.SL2JS.GetDocumentPath(strFile, "", "");
                var ret = RiskmasterSL.Content.SL2JS.StartClmEditor(strFile);
                if (ret == "true")
                    return true;
                else
                    return false;
            }
            else
                alert(parent.CommonValidations.ValidMergeMergeSilverlight);
        }
        else {
	if(!InitPageSettings())
	{		
		return DisplayError(strForm);
	}
	
    var adTypeBinary = 1;
    var adSaveCreateOverWrite = 2;
    var adTypeText = 2;
    //var step = 'check IsWordOpen';
    var step = MergeClaimLetterValidations.ValidateClaimLetterStep1;
    
    if (IsWordOpen())
    {
        //step = 'activate Word';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep34;
	    objWord.Activate();
	    return false;
    }
	
    try{//save template and datasource local
		//use DomDocument to convert base64 string to bytes	
        //step = 'get template base64 text';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep2;
		var template = (document.getElementById('hdnRTFContent').value);
		if(template == null || template == "")
		//throw new Error("The word merge template was not passed to the browser.  Please check the RMX web errors server error log for an error message.  The most likely cause of this error is that the word merge template file (*.doc or *.rtf) could not be found in document storage.");    
		    throw new Error(MergeClaimLetterValidations.ValidateClaimLetterStep3);

		//step = 'get data source base64';
		step = MergeClaimLetterValidations.ValidateClaimLetterStep4;
		var dataSource = (document.getElementById('hdnDataSource').value);
		if(dataSource == null || dataSource == "")
		//throw new Error("The word merge data source was not passed to the browser, please check the RMX web errors server error log for an error message.  The most likely cause of this error is that the word merge header file (*.hdr file) could not be found in document storage, or the application could not interpret a merge field name.");
		    throw new Error(MergeClaimLetterValidations.ValidateClaimLetterStep5);

		//step = 'get header file base64';
		step = MergeClaimLetterValidations.ValidateClaimLetterStep6;
		var headerContent = (document.getElementById('hdnHeaderContent').value);
		if(dataSource == null || headerContent == "")
		//throw new Error("The word merge header was not passed to the browser, please check the RMX web errors server error log for an error message.  The most likely cause of this error is that the word merge header file (*.hdr file) could not be found in document storage, or the application could not interpret a merge field name.");    
		    throw new Error(MergeClaimLetterValidations.ValidateClaimLetterStep7);
	
		document.all.hdnLetterGenerated.value = -1;
		
		var password = (document.getElementById('hdnPassword').value);
		var sSecFileName = (document.getElementById('hdnSecFileName').value);
		var numrecords = (document.getElementById('hdnNumRecords').value);
		var SplitData1 = (document.getElementById('hdnData4Lv1').value);
		var SplitData2 = (document.getElementById('hdnData4Lv2').value);
		var SplitData3 = (document.getElementById('hdnData4Lv3').value);
		var SplitData4 = (document.getElementById('hdnData4Lv4').value);
		var SplitData5 = (document.getElementById('hdnData4Lv5').value);
		var SplitData6 = (document.getElementById('hdnData4Lv6').value);
		var SplitData7 = (document.getElementById('hdnData4Lv7').value);
		var SplitData8 = (document.getElementById('hdnData4Lv8').value);
		
		if((SplitData1 == "") && (SplitData2 == "") && (SplitData3 == "") && (SplitData4 == "") && (SplitData5 == "") && (SplitData6 == "") && (SplitData7 == "") && (SplitData8 == ""))
		//throw new Error("The word merge data source was not passed to the browser, please check the RMX web errors server error log for an error message.  The most likely cause of this error is that the word merge header file (*.hdr file) could not be found in document storage, or the application could not interpret a merge field name.");
		    throw new Error(MergeClaimLetterValidations.ValidateClaimLetterStep8);
        
        if(password == "")
        //throw new Error("Email Claim Letter Password is not set at any level of Organization Hierarchy. No Claim Letters generated.")
            throw new Error(MergeClaimLetterValidations.ValidateClaimLetterStep9);

        //step = 'instantiate xml document for template';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep10;
		var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
		//step = 'create b64 element for template';
		step = MergeClaimLetterValidations.ValidateClaimLetterStep11;
		var e = xmlDoc.createElement("b64");
		//step = 'set element.dataType to bin.base64 for template';
		step = MergeClaimLetterValidations.ValidateClaimLetterStep12;
		e.dataType = "bin.base64";

		//step = 'set element.text to template base64 text';
		step = MergeClaimLetterValidations.ValidateClaimLetterStep13;
        e.text = template;
        //step = 'get bytes from template element';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep14;
        var templateBytes = e.nodeTypedValue;
        
        
        
        //use ADODB.Stream to save template bytes to disc
        //step = 'instantiate Stream object for template';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep15;
        var stm = new ActiveXObject("ADODB.Stream");
        //step = 'set template stm.Type';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep16;
        stm.Type = adTypeBinary;
        //step = 'open template stream';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep17;
        stm.Open();
        //step = 'write bytes to template stream';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep18;
        stm.Write(templateBytes);
        //step = 'save template stream to file ' + strForm;
        step = MergeClaimLetterValidations.ValidateClaimLetterStep19 + " " + strForm;
        stm.SaveToFile(strForm, adSaveCreateOverWrite);
        //step = 'close stream';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep20;
        stm.Close();
        stm = null;



        //step = 'instantiate xml document for header';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep21;
	   	xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
	   	//step = 'create b64 element for header';
	   	step = MergeClaimLetterValidations.ValidateClaimLetterStep22;
	   	e = xmlDoc.createElement("b64");
	   	//step = 'set element.dataType to bin.base64 for header';
	   	step = MergeClaimLetterValidations.ValidateClaimLetterStep23;
	   	e.dataType = "bin.base64";

	   	//step = 'set element.text to header base64 text';
	   	step = MergeClaimLetterValidations.ValidateClaimLetterStep24;
		e.text = headerContent;
		//step = 'get bytes from header element';
		step = MergeClaimLetterValidations.ValidateClaimLetterStep25;
		var headerBytes = e.nodeTypedValue;

		//use ADODB.Stream to save header to disk
		var headerFileName;
		if( strForm.length > 4 )
		{
			headerFileName = strForm.substring(0, strForm.length-4) + ".hdr";
			//step = 'instantiate Stream object for data source';
			step = MergeClaimLetterValidations.ValidateClaimLetterStep26;
			stm = new ActiveXObject("ADODB.Stream");
			//step = 'set datasource stm.Type';
			step = MergeClaimLetterValidations.ValidateClaimLetterStep27;
			stm.Type = adTypeBinary;
			//step = 'open datasource stream';
			step = MergeClaimLetterValidations.ValidateClaimLetterStep28;
			stm.Open();
			//step = 'write bytes to datasource stream';
			step = MergeClaimLetterValidations.ValidateClaimLetterStep29;
			stm.Write(headerBytes );
			//step = 'save datasource stream to file ' + headerFileName;
			step = MergeClaimLetterValidations.ValidateClaimLetterStep30 + " " + headerFileName;
			stm.SaveToFile(headerFileName, adSaveCreateOverWrite);
			//step = 'close stream';
			step = MergeClaimLetterValidations.ValidateClaimLetterStep20;
			stm.Close();
			stm = null;
        }
        e = null;
        xmlDoc = null;
    }
    catch (e)
    {		
	    document.all.hdnLetterGenerated.value = 0;
	    //return DisplayError("The word template and data source could not be saved to this machine for editing because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
	    return DisplayError(MergeClaimLetterValidations.ValidateClaimLetterStep31 + " " + e.name + ',' + e.message + '\r\n' + MergeClaimLetterValidations.ValidateClaimLetterStep32 + " " + step);
    }

    try
    {
    	//added by navdeep    
    	var dataFileName = "";   
    	var secFile = "";
    	var dataSourceFileName ="";
    	//function PrepareDataSource(dataSource, iNum)
        //if( numrecords > 1)
        //{
            for (var iCtr = 1; iCtr<=8; iCtr = iCtr + 1)
            {
                if((SplitData1 != "")  && iCtr == 1)
                {
                     dataFileName = GetDataFileName(sSecFileName, 1);
                     secFile = GetSecFileName (sSecFileName, 1);
                     dataSourceFileName = PrepareDataSource (SplitData1,1)
                     WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName,secFile , password );                 
                     //document.all.hdnTitle1.value = "Department"
                     //if (fso.FileExists(dataFileName))                     
                        document.all.hdnRTFContent1.value = ConvertLetterToStream(dataFileName);
                     //if (fso.FileExists(secFile))
                        document.all.hdnSecureRTFContent1.value = ConvertLetterToStream(secFile);                                          
                }
                if((SplitData2 != "")  && iCtr == 2)
                 {
                     dataFileName = GetDataFileName(sSecFileName, 2);
                     secFile = GetSecFileName (sSecFileName, 2);
                     dataSourceFileName = PrepareDataSource (SplitData2,2,Editor_Launched, xmlDoc )
                     WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName,secFile , password );     
                     //document.all.hdnTitle2.value = "Facility"
                     document.all.hdnRTFContent2.value = ConvertLetterToStream(dataFileName);
                     document.all.hdnSecureRTFContent2.value = ConvertLetterToStream(secFile);                                 
                 }
                if((SplitData3 != "")  && iCtr == 3)            
                {                
                    dataFileName = GetDataFileName(sSecFileName, 3);
                    secFile = GetSecFileName (sSecFileName, 3);
                    dataSourceFileName = PrepareDataSource (SplitData3,3)
                    WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName,secFile , password );
                    //document.all.hdnTitle3.value = "Location"
                    document.all.hdnRTFContent3.value = ConvertLetterToStream(dataFileName);
                    document.all.hdnSecureRTFContent3.value = ConvertLetterToStream(secFile);                     
                }
                if((SplitData4 != "")  && iCtr == 4)
                {
                    dataFileName = GetDataFileName(sSecFileName, 4);
                    secFile = GetSecFileName (sSecFileName, 4);
                    dataSourceFileName = PrepareDataSource (SplitData4,4)
                    WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName,secFile , password );
                    //document.all.hdnTitle4.value = "Division"
                    document.all.hdnRTFContent4.value = ConvertLetterToStream(dataFileName);
                    document.all.hdnSecureRTFContent4.value = ConvertLetterToStream(secFile);                     
                }
                if((SplitData5 != "")  && iCtr == 5)
                {
                    dataFileName = GetDataFileName(sSecFileName, 5);
                    secFile = GetSecFileName (sSecFileName, 5);
                    dataSourceFileName = PrepareDataSource (SplitData5,5)
                    WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName,secFile , password );
                    //document.all.hdnTitle5.value = "Region"
                    document.all.hdnRTFContent5.value = ConvertLetterToStream(dataFileName);
                    document.all.hdnSecureRTFContent5.value = ConvertLetterToStream(secFile);                     
                }
                if((SplitData6 != "")  && iCtr == 6)
                {
                    dataFileName = GetDataFileName(sSecFileName, 6);
                    secFile = GetSecFileName (sSecFileName, 6);
                    dataSourceFileName = PrepareDataSource (SplitData6,6)
                    WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName,secFile , password );
                  //  document.all.hdnTitle6.value = "Operation"
                    document.all.hdnRTFContent6.value = ConvertLetterToStream(dataFileName);
                    document.all.hdnSecureRTFContent6.value = ConvertLetterToStream(secFile);                     
                }
                if((SplitData7 != "")  && iCtr == 7)
                 {
                    dataFileName = GetDataFileName(sSecFileName, 7);
                    secFile = GetSecFileName (sSecFileName, 7);
                    dataSourceFileName = PrepareDataSource (SplitData7,7)
                    WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName,secFile , password );
                  //  document.all.hdnTitle7.value = "Company"
                    document.all.hdnRTFContent7.value = ConvertLetterToStream(dataFileName);
                    document.all.hdnSecureRTFContent7.value = ConvertLetterToStream(secFile);                     
                 }
                if((SplitData8 != "")  && iCtr == 8)
                {
                    dataFileName = GetDataFileName(sSecFileName, 8);                
                    secFile = GetSecFileName (sSecFileName, 8);
                    dataSourceFileName = PrepareDataSource (SplitData8,8)
                    WordMerge(strForm, headerFileName, dataSourceFileName, dataFileName,secFile , password );
                   // document.all.hdnTitle8.value = "Client"
                    document.all.hdnRTFContent8.value = ConvertLetterToStream(dataFileName);
                    document.all.hdnSecureRTFContent8.value = ConvertLetterToStream(secFile);                     
                }
                if (fso.FileExists(dataSourceFileName))
                    fso.DeleteFile(dataSourceFileName);
                if (fso.FileExists(dataFileName))
                    fso.DeleteFile(dataFileName);
                if (fso.FileExists(secFile))
                    fso.DeleteFile(secFile);                                
            }
            
            if (fso.FileExists(strForm))
               fso.DeleteFile(strForm);
            if (fso.FileExists(headerFileName))
               fso.DeleteFile(headerFileName);                     
        //}
             
    }
    catch (e)
    {
    	document.all.hdnLetterGenerated.value = 0;
    	//return DisplayError("The local Microsft Word application required to complete the wizard could not be loaded because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
    	return DisplayError(MergeClaimLetterValidations.ValidateClaimLetterStep33 + " " + e.name + ',' + e.message + '\r\n' + MergeClaimLetterValidations.ValidateClaimLetterStep32 + " " + step);
    }

    Editor_Launched=1;
    //document.all.hdnLetterGenerated.value = -1;
    return false;    
	}
    }

function WordMerge(sFileName, sHeaderFileName, sDataFileName, dataFileName, secFile, password) {
	var wordDoc = null;
	var step = "";
	var isProtected = false;
    
    try
    {//open template
        //step = 'open word document: ' + sFileName;
        step = parent.CommonValidations.OpenWordDocument + " " + sFileName;
        objWord = new ActiveXObject("Word.Application");
	    wordDoc = objWord.Documents.Open(sFileName);
	    
	    //Try to save the document to make MSWord's SaveDate function work
	    objWord.ActiveDocument.Saveas(sFileName)
    }
    catch (e)
    {
	    document.all.hdnLetterGenerated.value = 0;
	    //DisplayError("The word template at " + sFileName + " could not be opened by Microsft Word because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        DisplayError(parent.CommonValidations.WordMergeTemplateErr1 + " " + sFileName + " " + parent.CommonValidations.WordMergeTemplateErr2 + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
	    if (objWord !=null )
	    {	
		    objWord.Quit(0);
		    objWord = null;
		    return false;
	    }
    }
												
    try
    {//Verify we have full edit rights.	Isprotected added by pawan for 10983								   
	    if( objWord.ActiveDocument.ProtectionType != -1) //if not totally unlocked then
	{	   
	 objWord.ActiveDocument.UnProtect("");
		isProtected = true;
	}
    }
    catch (e)
    {
	    document.all.hdnLetterGenerated.value = 0;
	    //alert("The word template at " + sFileName + " is protected from changes.\nPlease unlock it and click the [Launch Word] button again. ");
        alert(parent.CommonValidations.WordMergeTemplateErr1 + " " + sFileName + " " + (parent.CommonValidations.WordMergeTemplateErr3).split('\\n').join('\n'));
	    objWord.visible = true;
	    objWord.Activate();	
    }

    try
    {   //step='activate Word';
        step = parent.CommonValidations.ValidActivateWordStepCheck;
//	    objWord.visible = false;
//	    objWord.Activate();	
	    wordDoc.Select();  //this call instantiates MailMerge object
	    //step='open header';
        step = parent.CommonValidations.OpenHeader;
        if( sHeaderFileName.length > 0 )
        {
        	wordDoc.MailMerge.OpenHeaderSource(sHeaderFileName);
        }
	    //step='open data source';
        step = parent.CommonValidations.OpenDataSource;
        wordDoc.MailMerge.OpenDataSource(sDataFileName);
	wordDoc.MailMerge.DataSource.FirstRecord=1;
		
	//Get number of records to be merged
	wordDoc.MailMerge.DataSource.LastRecord=1;
	var objRecordNumber = document.getElementById("hdnNumRecords");
	if( objRecordNumber != null )
	{
		var sRecordNumber = objRecordNumber.value;
		if( !isNaN(sRecordNumber) )
		{
			wordDoc.MailMerge.DataSource.LastRecord = parseInt(sRecordNumber);
		}
	}
        //JIRA 499
        //wordDoc.MailMerge.Destination=0;
	wordDoc.MailMerge.Destination = wdSendToNewDocument;

        //step='do mail merge';
        step = parent.CommonValidations.DoMailMerge;
        wordDoc.MailMerge.Execute(false);
        //step='select merged document';
        step = parent.CommonValidations.SelectMergedDoc;
        //var mergedDoc = objWord.Documents.Item(1);
	        
	    
	    //objWord.ActiveDocument.Saveas(secFile,0,0,"Navdeep"); 		
        
        //step='close the original';
        step = parent.CommonValidations.CloseOriginal;
	//wordDoc.Saveas(strForm)
        //wordDoc.Select();   
        // wordDoc.Close();
        wordDoc.Close(wdDoNotSaveChanges);

        //step='select merged document';
	 step = parent.CommonValidations.SelectMergedDoc;
	//Not need rt now doesn't suffice clients requirement pawan changed for mits 10983 enabling protection if already protected
	//if (isProtected == true)
	//	objWord.ActiveDocument.Protect(2,true,"");
		
	//change end

	objWord.ActiveDocument.Select();   
	//step='save the merged document over the original at ' + dataFileName;
    step = parent.CommonValidations.SaveMergedDoc + " " + dataFileName;
	objWord.ActiveDocument.Saveas(dataFileName); 		
	
	//step='save the Secured Document over the original at ' + secFile;    
    step = parent.CommonValidations.SaveSecuredDoc + " " + secFile;
	objWord.ActiveDocument.Saveas(secFile, 0, 0, password );
	
    objWord.Quit(0);
    objWord = null;
    
    for (var iCtr = 1; iCtr<=5; iCtr = iCtr + 1)
    {
        //setTimeout("FileMerge",1000);        
        wait(1000);
        if (fso.FileExists(secFile))
            break;
    }
        	
    } //
    catch (e)
    {
	    document.all.hdnLetterGenerated.value = 0;
	    //DisplayError("Microsoft Word could not accomplish the merge because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        DisplayError(parent.CommonValidations.MSWordError + " " + e.name + ',' + e.message + '\r\n' + parent.CommonValidations.ValidErrorStepCheck + " " + step);
	    objWord.Quit(0);
	    objWord = null;
	    return false;
    }
    //Done
}
//added by Navdeep
function GetDataFileName(sSecFileName,i)
{
    var dataFileName = new String(strForm);
    dataFileName = dataFileName.replace(strFile, sSecFileName + i + '.doc');
    return dataFileName;
}


function GetSecFileName(sSecFileName,i)
{
    var sSecFile = new String(strForm);
    sSecFile = sSecFile.replace(strFile, sSecFileName + i + 'Sec.doc');
    return sSecFile;
}

function PrepareDataSource(dataSource, iNum )
{
        var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
        var e = xmlDoc.createElement("b64");
        var adTypeBinary = 1;
        var adSaveCreateOverWrite = 2;
        //var adTypeText = 2;
        
        e.dataType = "bin.base64";
        //step = 'instantiate xml document for datasource';
        step = MergeClaimLetterValidations.ValidatePrepareDataSource1;
		xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
		//step = 'create b64 element for datasource';
		step = MergeClaimLetterValidations.ValidatePrepareDataSource2;
		e = xmlDoc.createElement("b64");
		//step = 'set element.dataType to bin.base64 for datasource';
		step = MergeClaimLetterValidations.ValidatePrepareDataSource3;
		e.dataType = "bin.base64";

		//step = 'set element.text to datasource base64 text';
		step = MergeClaimLetterValidations.ValidatePrepareDataSource4;
        e.text = dataSource;
        //step = 'get bytes from datasource element';
        step = MergeClaimLetterValidations.ValidatePrepareDataSource5;
        var dataSourceBytes = e.nodeTypedValue;
        
        //use ADODB.Stream to save datasource to disk
        var dataSourceFileName = GetDataFileContent(iNum);
        //step = 'instantiate Stream object for data source';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep26;
        stm = new ActiveXObject("ADODB.Stream");
        //step = 'set datasource stm.Type';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep27;
        stm.Type = adTypeBinary;
        //step = 'open datasource stream';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep28;
        stm.Open();
        //step = 'write bytes to datasource stream';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep29;
        stm.Write(dataSourceBytes );
        //step = 'save datasource stream to file ' + dataSourceFileName;
        step = MergeClaimLetterValidations.ValidateClaimLetterStep30 + " " + dataSourceFileName;
        stm.SaveToFile(dataSourceFileName, adSaveCreateOverWrite);
        //step = 'close stream';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep20;
        stm.Close();
        stm = null;
        return dataSourceFileName;
}

function GetDataFileContent(i)
{
    var dataSourceFileName = new String(strForm);
    dataSourceFileName = dataSourceFileName.replace(strFile,'DataSource' + i + strFile);
    return dataSourceFileName;
}


function wait(msecs)
{
    var start = new Date().getTime();
    var cur = start
    while(cur - start < msecs)
    {
        cur = new Date().getTime();
    } 
} 


//Convert Letter from .doc to Stream.
function ConvertLetterToStream(sFile, isSecured)
{
    var step = '';
    var base64 = "";     
    //Convert Back to Stream
	try
    {	    
	    var adTypeBinary = 1;
	    //step = 'instantiate Stream object';
	    step = MergeClaimLetterValidations.ValidateConvertLetterToStream13;
        var stm = new ActiveXObject("ADODB.Stream");
        //step = 'set stm.Type';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream1;
        stm.Type = adTypeBinary;
        //step = 'open stream';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream2;
        stm.Open();
        //step = 'read file ' + sFile + ' into stream';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream3 + " " + sFile + " " + MergeClaimLetterValidations.ValidateConvertLetterToStream4;
        stm.LoadFromFile(sFile);
        //step = 'read bytes from stream';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream5;
        var bytes = stm.Read();
        //step = 'close stream';
        step = MergeClaimLetterValidations.ValidateClaimLetterStep20;
        stm.Close();
        stm = null;
        
        //step = 'instantiate xml document';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream14;
        var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
        xmlDoc.async = false;
        //step = 'create root element';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream6;
        var xmlElement = xmlDoc.createElement("root");
        xmlDoc.documentElement = xmlElement;
        xmlElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com: datatypes ");
        //step = 'create element to hold word document';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream7;
        var xmlNode = xmlElement.appendChild(xmlDoc.createElement("wordDoc"));
        xmlNode.dataType = "bin.base64"
        //step = 'read stream into element';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream8;
        xmlNode.nodeTypedValue = bytes;
        //step = 'read base64 text from element';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream9;
        base64 = xmlNode.text; 
        
        //step='cleanup';
        step = MergeClaimLetterValidations.ValidateConvertLetterToStream10;
        xmlDoc = null;
        xmlElement = null;
        xmlNode = null;
    }
    catch (e)
    {
	    document.all.hdnLetterGenerated.value = 0;
	    //alert("The local copy of the document at " + sFile + " could not be saved back into Riskmaster because of the following error: " + e.name + ',' + e.message + '\r\nStep = ' + step);
        alert(MergeClaimLetterValidations.ValidateConvertLetterToStream11 + " " + sFile + MergeClaimLetterValidations.ValidateConvertLetterToStream12 + " " + e.name + ',' + e.message + '\r\n' + MergeClaimLetterValidations.ValidateClaimLetterStep32 + " " + step);
	    xmlDoc = null;
        xmlElement = null;
        xmlNode = null;
	    return base64;
    }
	return base64;
}//END :Added by Navdeep For Chubb ACKNowledgement Claim Letter
    // RMA-8407 : MITS-36022 : Mudabbir 
    function getAndSetValues(obj, set, get, value) {

        if (obj == "1")
            Editor_Launched = 1;
        else {
            if (obj != null) {
                if (get == "yes") {

                    var getvalue = eval(obj);
                    if (getvalue.value == undefined)
                        return getvalue;
                    else
                        return getvalue.value;
                }
                if (set == "yes") {
                    var setvalue = eval(obj);
                    setvalue.value = value;
                }
            }
        }
    }

    function DisplayErrorfromSilver(errMsg) {
        alert(errMsg);
        //document.write('<h3>Error Message</h3><p>' + errMsg + '</p><br />' + _mergeFailureErrorMessage); 
        document.write('<h3>' + parent.CommonValidations.ValidMergeMergeErrorMessage + '</h3><p>' + errMsg + '</p><br />');
        return false;
    }
