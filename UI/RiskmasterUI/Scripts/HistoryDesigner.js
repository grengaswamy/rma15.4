﻿var m_SelectedTableValue = "";


function fnDisableHistoryTracking() {
    var ret = confirm("Disabling History Tracking Feature will also delete metadata related with selection of Tables and Columns.Do you wish to Continue?");
    if (ret) {
        pleaseWait.Show();
        return true;
    }
    else {
        return false;
    }    
}


function pageLoaded() {
    setHiddenDataChanged(eval(document.forms[0].hdnDataChanged.value));
}

//This function is fired when Save button of History Designer is clicked
function fnSaveTree() {
    if (eval(document.forms[0].hdnDataChanged.value) == false) {
        alert("No change has been made in the selection of Tables or Columns.");
        return false;
    }
    setHiddenDataChanged(false);
    pleaseWait.Show();
}

//This function is called whenever there is a click on tree node
function OnTreeClick(evt)
{ 
    var src = window.event != window.undefined ? window.event.srcElement : evt.target;
    var nodeClick = src.tagName.toLowerCase() == 'a';
    if (nodeClick) {
        var sHref = src.href;
        if (sHref != null) {
            m_SelectedTableValue = sHref.substring(sHref.lastIndexOf("\\") + 1, sHref.lastIndexOf("'"));
        } 
        return false;
    }
    var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
    if (isChkBoxClick) {
        setHiddenDataChanged(true);
    }
}

//This function is called when Select/Deselect All button on top of History Designer is clicked
function SelectOrDeselectChildColumns()
{
    if (m_SelectedTableValue == "") {
        alert("Please select a Table to select or deselect its columns.")
        return false;
    }
    setHiddenDataChanged(true);
    pleaseWait.Show();
    return true;   
}

function setHiddenDataChanged(b) {    
    try {
        if (b == true) {
            if (parent.MDISetDirty != null) {
                parent.MDISetDirty();
            }
        }        
        document.forms[0].hdnDataChanged.value = b;
    }
    catch (e) { }
}

//This function is called when Save button of PurgeHistory pop up is clicked
function fnValidatePurgeRecord()
{
    var oSelectedIndex = document.getElementById('ddlTableName_HiddenField');
    if (oSelectedIndex != null && oSelectedIndex.value == "0")
    {
        alert("Please select Table Name.");
        return false;
    }

    var oToDate = document.getElementById('txtToDate');
    if (oToDate != null && trim(oToDate.value) == "")
    {
        alert("Please select To Date.");
        return false;
    }

    var oSystemDate = document.getElementById('hdnSystemDate');
    if (oSystemDate != null && trim(oSystemDate.value) != "" && oToDate != null)
    {
        var sSystemDate = oSystemDate.value;
        var sToDate = oToDate.value;
        var sTillDate = "";
        var sCurrentDate = "";
        
        if (sSystemDate.indexOf('/') == -1)
            sCurrentDate = sSystemDate.substring(4, 8) + sSystemDate.substring(0, 2) + sSystemDate.substring(2, 4);        
        else
            sCurrentDate = sSystemDate.substring(6, 10) + sSystemDate.substring(0, 2) + sSystemDate.substring(3, 5);

        if (sToDate.indexOf('/') == -1)
            sTillDate = sToDate.substring(4, 8) + sToDate.substring(0, 2) + sToDate.substring(2, 4);        
        else
            sTillDate = sToDate.substring(6, 10) + sToDate.substring(0, 2) + sToDate.substring(3, 5);


        if (sCurrentDate < sTillDate)
        {    
            alert("To Date cannot be set in future.");
            return false;
        }

        var oFromDate = document.getElementById('txtFromDate');
        if (oFromDate != null && trim(oFromDate.value) != "")
        {
            var sFromDate = oFromDate.value;
            var sFrmDate = "";

            if (sFromDate.indexOf('/') == -1)
                sFrmDate = sFromDate.substring(4, 8) + sFromDate.substring(0, 2) + sFromDate.substring(2, 4);
            else
                sFrmDate = sFromDate.substring(6, 10) + sFromDate.substring(0, 2) + sFromDate.substring(3, 5);

            if (sTillDate < sFrmDate)
            {
                alert("From Date must be less than To Date.");
                return false;
            }
        }
    } 
    return true;
}


