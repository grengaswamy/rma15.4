//**********************************************************************************************
//*   Date     |  MITS   | Programmer | Description                                            *
//**********************************************************************************************
//* 06/04/2014 | 33371   | ajohari2   | TPA changes changes
//* 03/14/2014 | 34082   | pgupta93   | Multicurrency changes
//* 05/27/2014 | 34276   | achouhan3  | SR Gap 18 - Entity ID & Entity ID Number
//* 02/27/2015 | RMA-6865       | nshah28      | Swiss Re - Ability to set Effective Date_or_Expiration Date on Address
//**********************************************************************************************//
parent.CommonValidations = parent.parent.CommonValidations;
if (parent.CommonValidations == undefined) {
    if (parent.opener != undefined) {
        if (parent.opener.parent != undefined) {
            if (parent.opener.parent.parent != undefined) {
                parent.CommonValidations = parent.opener.parent.parent.CommonValidations;
            }
        }
            //MITS:34082 START
        else if (window.dialogArguments != undefined) {
            if (window.dialogArguments.parent != undefined) {
                parent.CommonValidations = window.dialogArguments.parent.CommonValidations;
            }
        }
        //MITS:34082 END
    }
}
var m_DataChanged=false;

function SetDataChanged(p_bDataChanged)
{
	m_DataChanged = p_bDataChanged;
	return p_bDataChanged;
}

function IsDataChanged()
{
	return m_DataChanged;
}

function ConfirmSave()
{
	var bReturnValue = false;
	if(m_DataChanged){
	    //bReturnValue = confirm('Data has changed. Do you want to save changes?');
	    bReturnValue = confirm(parent.CommonValidations.ConfirmSave);
	}
	return bReturnValue;
}

function EntityXContactInfo_onOk()
{
	var bResult = false;
		
	//added by Navdeep - Added for Multi Line of Bus code in Contact Info Screen - Map the Grid from Busniness Layer( CHUBB ACK)
	//instead of from GridPopupbase Class
	var txtSysPostBack = window.opener.document.getElementById('SysPostBackAction');
   if(txtSysPostBack != null) // NULL check added by Shivendu for MITS 19145
    txtSysPostBack.value = "Last";	
	//end Navdeep
	if(IsDataChanged()==false)
	{
	    return false;
	    //PJS 02-12-2009 (MITS 14180):- No action taken if no change is ther on screen
		//window.close();
		//return true;
	}
	if(EntityXContactInfo_Validate())
	{
        return true;
	}
	else
	{
		return false;
	}
}

function EntityXContactInfo_onCancel()
{
    var retval = false;
    
	if(IsDataChanged()==true)
	{
		if(ConfirmSave()==true)
		{
		    if(EntityXContactInfo_Validate())
		    {
			    retval = true;
			}
			else
			{
			    retval = false;
			}
			
		}
		else
		{
		     window.close();
		    retval = false;
		}
		
	}
	else
	{
	    window.close();
		return false;
	}
	return retval;
}

function EntityXContactInfo_Validate()
{
    // PJS : MITS 14161 - trim spaces
    if (document.forms[0].ContactName.value.trim() == "")
    {
        document.forms[0].ContactName.value = "";
        //RMA-4627 - start
        //alert("Name and Initial and Contact Type are required."); //MITS:34276 - updated alert text
        alert("Name and Initial are required.");
        //RMA-4627 - end 
        if(m_TabList != null && m_TabList[0]!= null)// NULL check added by Shivendu for MITS 19145
        tabChange(m_TabList[0].id.substring(4));   //pmittal5 Mits 18623 11/19/09 - Shift the focus to "Contact Info" tab
        document.forms[0].ContactName.focus();
        return false; 
    }
    else if (document.forms[0].Initials.value.trim() == "") {
        document.forms[0].Initials.value = "";
        //RMA-4627 - start
        //alert("Name and Initial and Contact Type are required."); //MITS:34276 - updated alert text
        alert("Name and Initial are required.");
        //RMA-4627 - end 
        if (m_TabList != null && m_TabList[0] != null)// NULL check added by Shivendu for MITS 19145
            tabChange(m_TabList[0].id.substring(4));   //pmittal5 Mits 18623 11/19/09 - Shift the focus to "Contact Info" tab
        document.forms[0].Initials.focus();
        return false;
    }
        //RMA-4627 - Start: making contact type non required
        //MITS:34276 - Start : Address Type for multiple contact
    //else if (document.forms[0].ContactType_codelookup.value.trim() == "") {
    //    document.forms[0].ContactType_codelookup.value = "";
    //    alert("Name and Initial and Contact Type are required.");
    //    if (m_TabList != null && m_TabList[0] != null)// NULL check added by Shivendu for MITS 19145
    //        tabChange(m_TabList[0].id.substring(4));   //pmittal5 Mits 18623 11/19/09 - Shift the focus to "Contact Info" tab
    //    document.forms[0].Initials.focus();
    //    return false;
    //}
        //MITS:34276 - End : Address Type for multiple contact
        //RMA-4627 - End
    else {
        return true;
    }
    return false;
}

function EntityXOperatingAs_onOk()
{
	var bResult = false;
		
	if(IsDataChanged()==false)
	{
	    return false;
	    //PJS 02-12-2009 (MITS 14180):- No action taken if no change is ther on screen
		//window.close();
		//return true;
	}
	if(EntityXOperatingAs_Validate())
	{
	    return true;
	}
	else
	{
		return false;
	}
}

function EntityXOperatingAs_onCancel()
{
	var retval = false;
    
	if(IsDataChanged()==true)
	{
		if(ConfirmSave()==true)
		{
		    if(EntityXOperatingAs_Validate())
		    {
			    retval = true;
			}
			else
			{
			    retval = false;
			}
			
		}
		else
		{
		     window.close();
		    retval = false;
		}
		
	}
	else
	{
	    window.close();
		return false;
	}
	return retval;
}

function EntityXOperatingAs_Validate()
{

    // PJS : MITS 14161 - trim spaces
   if (document.forms[0].Initials.value.trim() == "") {
        document.forms[0].Initials.value = "";
        alert("Name and Initial are required.");
        document.forms[0].Initials.focus();
        return false;
    }
    else if (document.forms[0].OperatingAs.value.trim() == "") {
        document.forms[0].OperatingAs.value = "";
        alert("Name and Initial are required.");
        document.forms[0].OperatingAs.focus();
        return false;
    }
    else {
        return true;
    }
    return false;
// PJS : MITS 14161 - trim spaces 
	if(document.forms[0].Initials.value.trim()=="" || document.forms[0].OperatingAs.value.trim()=="")
	{
	    document.forms[0].Initials.value = "";
	    document.forms[0].OperatingAs.value = "";
		alert("Initial and Name are required.");
		return false;
	}
	else
	{
		return true;
	}
	return false;
}

function LeaveDetail_onOk()
{
	var bResult = false;    
	if(IsDataChanged()==false)
	{
		window.close();
		return true;
	}
	if(LeaveDetail_Validate())
	{
		return true;
	}
	else
	{
		return false;
	}
}

function LeaveDetail_onCancel()
{
	var retval = false;
    
	if(IsDataChanged()==true)
	{
		if(ConfirmSave()==true)
		{
		    if(LeaveDetail_Validate())
		    {
			    retval = true;
			}
			else
			{
			    retval = false;
			}
			
		}
		else
		{
		     window.close();
		    retval = false;
		}
		
	}
	else
	{
	    window.close();
		return false;
	}
	return retval;
}

function LeaveDetail_Validate()
{

    //if(document.forms[0].DateLeaveStart.value == "" || document.forms[0].DateLeaveEnd.value == "")
    if (document.forms[0].DateLeaveStart.value == "")//mbahl3 Mit 26492 date:11/11/2011
    {
        //alert("Please enter a Start Date and an End Date.");
        alert("Please enter a Start Date ")//mbahl3 Mit 26492 date:11/11/2011
        return false;
    }
		else if(( new Date(document.forms[0].DateLeaveStart.value) !="" &&  new Date(document.forms[0].DateLeaveEnd.value !="") && (new Date(document.forms[0].DateLeaveEnd.value) < new Date(document.forms[0].DateLeaveStart.value))))
		{
			alert("The End date must be greater than or equal to the start date.");
			return false;
		}	
 
		else 
		{		
			return true; 
		}	
}
function ClientLimits_onOk()
{
	var bResult = false;
	//Start MITS ID 31950 6/13/2013 psaiteja
	var windowMode = document.getElementById('mode').value;
	//End MITS ID 31950 6/13/2013 psaiteja
	if(IsDataChanged()==false &&  windowMode != "add")
	{
		window.close();
		return true;
	}
	if(ClientLimits_Validate())
	{
	    return true;
	}
	else
	{
		return false;
	}
}
function ClientLimits_onCancel()
{
	var retval = false;
    
	if(IsDataChanged()==true)
	{
		if(ConfirmSave()==true)
		{
		    if(ClientLimits_Validate())
		    {
			    retval = true;
			}
			else
			{
			    retval = false;
			}
			
		}
		else
		{
		     window.close();
		    retval = false;
		}
		
	}
	else
	{
	    window.close();
		return false;
	}
	return retval;
}

function ClientLimits_Validate() //From R4 checkClientLimits()
{	
    //Start MITS ID 31950 6/13/2013 psaiteja
    if ((document.forms[0].ReserveTypeCode_codelookup.value == "" && document.forms[0].LobCode_codelookup.value == "" && document.forms[0].MaxAmount.value == "$0.00")) {
        
        if (document.forms[0].PaymentFlag_Text.value == "0") {
            alert(parent.CommonValidations.LOBResTypeMaxAmt);
            return false;
        }
        else if (document.forms[0].PaymentFlag_Text.value == "-1") {
            alert(parent.CommonValidations.LOBMaxAmt);
            return false;
        }
    }
    else if (document.forms[0].LobCode_codelookup.value == "" && document.forms[0].MaxAmount.value == "$0.00")
    {
        alert(parent.CommonValidations.LOBMaxAmt);
        return false;
    }
    //else if (document.forms[0].ReserveTypeCode_codelookup.value != "" && (document.forms[0].ReserveTypeCode_codelookup.value == "" && document.forms[0].MaxAmount.value == "$0.00")) {
    //    alert(parent.CommonValidations.ResTypeMaxAmt);
    //    return false;
        //}
    else if (document.forms[0].ReserveTypeCode_codelookup.value == "" && document.forms[0].MaxAmount.value == "$0.00") {
        if (document.forms[0].PaymentFlag_Text.value == "0") {
            alert(parent.CommonValidations.ResTypeMaxAmt);
            return false;
        }
        else if (document.forms[0].PaymentFlag_Text.value == "-1") {
            alert(parent.CommonValidations.MaxAmt);
            return false;
        }
    }
    else if (document.forms[0].ReserveTypeCode_codelookup.value == "" && document.forms[0].LobCode_codelookup.value == "") {
        if (document.forms[0].PaymentFlag_Text.value == "0") {
            alert(parent.CommonValidations.LOBResType);
            return false;
        }
        else if (document.forms[0].PaymentFlag_Text.value == "-1") {
            alert(parent.CommonValidations.LOB);
            return false;
        }
    }
    else if (document.forms[0].LobCode_codelookup.value == "") {
        alert(parent.CommonValidations.LOB);
        return false;
    }
    else if (document.forms[0].MaxAmount.value == "$0.00") {
        alert(parent.CommonValidations.MaxAmt);
        return false;
    }
        //End MITS ID 31950 6/13/2013 psaiteja
    else {
        if (document.forms[0].PaymentFlag_Text.value == "0") {
            if (document.forms[0].ReserveTypeCode_codelookup.value == "") {
                alert(parent.CommonValidations.ResType);
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
	return false;
}
function CheckTypeofLimit(oCtrl)
{
	var ReserveTypeCodeDisplay="";
	ReserveTypeCodeDisplay=eval("document.getElementById('div_ReserveTypeCode')");
	if(document.getElementById(oCtrl.id).value=="-1")
	{
		ReserveTypeCodeDisplay.style.visibility="hidden";
		document.forms[0].ReserveTypeCode_codelookup.value="";
		document.forms[0].ReserveTypeCode_codelookup_cid.value="";
	}
	else
	{
		ReserveTypeCodeDisplay.style.visibility="visible";
	}
	setDataChanged(true);
}
function checkPopUpInfo()
{
	
	var ReserveTypeCodeDisplay="";
	ReserveTypeCodeDisplay=eval("document.getElementById('div_ReserveTypeCode')");
	
	if(document.forms[0].PaymentFlag_Text.value=="-1")
		ReserveTypeCodeDisplay.style.visibility="hidden";
	else
		ReserveTypeCodeDisplay.style.visibility="visible";
		
    return true;		
}

function Split_onOk() {
    var bResult = false;
    if (document.getElementById("mode").value != "add") {
        if (IsDataChanged() == false) {
            if (parseInt(GetPVControlValue('ExchangeRate')) == 1) {//MITS:34082 
            window.close();
            return true;
            }
        }
    }
    if (Split_Validate()) {
        //Try to prevent popup question in form.js, set m_DataChanged = false
        m_DataChanged = false;
        if(document.getElementById("Unit")!=null)
            $('#Unit').prop('disabled', false);
        if (document.getElementById("Policy") != null)
        $('#Policy').prop('disabled', false);
        return true;
    }
    else {
        return false;
    }
}

function Split_onLoad() {
    //stara Mits 16667

    var ReserveTypeCodeFt = "";
    var RsvStatusParent = "";
    ReserveTypeCodeFt= eval("document.getElementById('div_ReserveTypeCodeFt')");
    //ends here
    if (document.getElementById("txtPostBack").value == "DoneOK") {
        return;
    }
     
	if( window.opener.document == null)
		return;
	
	if (window.opener.document.forms[0] == null)
		return;	

     //stara Mits 16667
     if(window.opener.document.forms[0].voidflag !=null)
     {
      if(window.opener.document.forms[0].voidflag.checked == true)
      {
            if(ReserveTypeCodeFt !=null) 
            HideUnhideReserveType(ReserveTypeCodeFt);
           
      
      }
     }
      if(window.opener.document.forms[0].clearedflag !=null)
     {
       if(window.opener.document.forms[0].clearedflag.checked == true)
       {
           
             if(ReserveTypeCodeFt !=null)
               HideUnhideReserveType(ReserveTypeCodeFt); 
       }
    }
     //ends here 
    //retrieve lineofbusinesscode from the funds page. It will be used in transaction type lookup
    //05/01/2009 abisht As lineofbusinesscode was removed from the funds.xml we have to fetch data from lob control.
    //if (window.opener.document.forms[0].lineofbusinesscode != null)
    //   document.forms[0].lineofbusinesscode.value = window.opener.document.forms[0].lineofbusinesscode.value;
    if (window.opener.document.forms[0].lob != null)
        document.forms[0].lineofbusinesscode.value = window.opener.document.forms[0].lob.value;
    if (window.opener.document.forms[0].PolicyID != null)
        document.forms[0].PolicyID.value = window.opener.document.forms[0].PolicyID.value;
    if (window.opener.document.forms[0].RcRowID != null)
        document.forms[0].RcRowID.value = window.opener.document.forms[0].RcRowID.value;
    if (window.opener.document.forms[0].PolCvgID != null)
        document.forms[0].PolCvgID.value = window.opener.document.forms[0].PolCvgID.value;
    if (window.opener.document.forms[0].ResTypeCode != null)
        document.forms[0].ResTypeCode.value = window.opener.document.forms[0].ResTypeCode.value;
    if (window.opener.document.forms[0].clm_entityid != null)
        document.forms[0].clm_entityid.value = window.opener.document.forms[0].clm_entityid.value;
    //rupal:start, for first & final payment
    if (window.opener.document.forms[0].IsFirstFinalQueryString != null)
        document.forms[0].IsFirstFinalQueryString.value = window.opener.document.forms[0].IsFirstFinalQueryString.value;
    //rupal:end, for first & final payment
    //rupal:start, for policy system interface
    if (document.forms[0].CovgSeqNum != null) {
    if (document.forms[0].CovgSeqNum.value == "") {
        if (window.opener.document.forms[0].CovgSeqNum != null) {
            document.forms[0].CovgSeqNum.value = window.opener.document.forms[0].CovgSeqNum.value;
            }
        }
    }
//rupal:end, for policy system interface
//mits 33996
if (document.forms[0].TransSeqNum != null) {
    if (document.forms[0].TransSeqNum.value == "") {
        if (window.opener.document.forms[0].TransSeqNum != null) {
            document.forms[0].TransSeqNum.value = window.opener.document.forms[0].TransSeqNum.value;
        }
    }
}
    //mits 33996
    //Ankit Start : Worked on MITS - 34297
if (document.forms[0].CoverageKey != null) {
    if (document.forms[0].CoverageKey.value == "") {
        if (window.opener.document.forms[0].CoverageKey != null) {
            document.forms[0].CoverageKey.value = window.opener.document.forms[0].CoverageKey.value;
        }
    }
}
    //Ankit End
    if (window.opener.document.forms[0].CvgLossID != null) {
       if( document.forms[0].CvgLossID!=null)
        document.forms[0].CvgLossID.value = window.opener.document.forms[0].CvgLossID.value;
    }
    if (window.opener.document.forms[0].claimid != null)
    {
        document.forms[0].claimid.value = window.opener.document.forms[0].claimid.value;
            //stara Mits 16667
            if((document.forms[0].claimid.value == "") || (document.forms[0].claimid.value == "0"))
           {
           
              if(ReserveTypeCodeFt !=null)
             {
               HideUnhideReserveType(ReserveTypeCodeFt);
            
             }
           
          }
          //ends here
    }
    if (window.opener.document.forms[0].orgEid != null)
        document.forms[0].orgEid.value = window.opener.document.forms[0].orgEid.value;

    if (window.opener.document.forms[0].ResStatusParent != null)
        RsvStatusParent = window.opener.document.forms[0].ResStatusParent.value;
    if (document.forms[0].ResTypeStatus != null)
                    document.forms[0].ResTypeStatus.value = RsvStatusParent;

    //stara Mits 16667
    if ((RsvStatusParent.toUpperCase() == "C") || (document.forms[0].RcRowID.value != "")) {

        //if (ReserveTypeCodeFt != null) {
        //    HideUnhideReserveType(ReserveTypeCodeFt);

        //}
        //ends here

        //rupal:start, r8 first & final payment
        //when the funds form is in readonly mode, these newly aded control for first & final payment should also be readonly as other controsl
        if (document.forms[0].Coverage_codelookup_cid != null) {
            document.forms[0].Coverage_codelookup_cid.readonly = "readonly";
            document.forms[0].Coverage_codelookup_cid.color = "#ffffff";
        }
        //abahl3: start rma 11568,11121,11120
        if (document.forms[0].Coverage_codelookup != null) {
            document.forms[0].Coverage_codelookup.readOnly = "readonly";
            document.forms[0].Coverage_codelookup.style.backgroundColor = "#F2F2F2";
        }
        //abahl3: end rma 11568,11121,11120
        if (document.forms[0].Coverage_codelookupbtn != null)
            document.forms[0].Coverage_codelookupbtn.style.visibility = "hidden";//abahl3:  rma 11568,11121,11120
        if (document.forms[0].Policy != null)
            document.forms[0].Policy.disabled = true;
        if (document.forms[0].FFPayment != null)
            document.forms[0].FFPayment.disabled = true;
        if (document.forms[0].Unit != null)
            document.forms[0].Unit.disabled = true;
        //rupal:end

        if (document.forms[0].reservebalance != null)
            document.forms[0].reservebalance.Enabled = false;
        if (document.forms[0].ReserveTypeCode_codelookup_cid != null)
               document.forms[0].ReserveTypeCode_codelookup_cid.readOnly = "readonly";

        if (document.forms[0].ReserveTypeCode_codelookup != null) {
            document.forms[0].ReserveTypeCode_codelookup.readOnly = "readonly";
            document.forms[0].ReserveTypeCode_codelookup.style.backgroundColor = "#F2F2F2";
        }
        if (document.forms[0].ReserveTypeCode_codelookupbtn != null) {
            document.forms[0].ReserveTypeCode_codelookupbtn.style.visibility = "hidden";
        }


        if (document.forms[0].ReserveTypeCodeFt_codelookup_cid != null)
            document.forms[0].ReserveTypeCodeFt_codelookup_cid.readOnly = "readonly";

        //abahl3:start  rma 11568,11121,11120
        if (document.forms[0].ReserveTypeCode_codelookup != null) {
            document.forms[0].ReserveTypeCode_codelookup.readOnly = "readonly";
            document.forms[0].ReserveTypeCode_codelookup.style.backgroundColor = "#F2F2F2";
        }
        if (document.forms[0].ReserveTypeCode_codelookupbtn != null) {
            document.forms[0].ReserveTypeCode_codelookupbtn.style.visibility = "hidden";
        }

        if (document.forms[0].ReserveTypeCodeFt_codelookup_cid != null)
            document.forms[0].ReserveTypeCodeFt_codelookup_cid.readOnly = "readonly";

        if (document.forms[0].ReserveTypeCodeFt_codelookup != null) {
            document.forms[0].ReserveTypeCodeFt_codelookup.readOnly = "readonly";
            document.forms[0].ReserveTypeCodeFt_codelookup.style.backgroundColor = "#F2F2F2";
        }
        if (document.forms[0].ReserveTypeCodeFt_codelookupbtn != null) {
            document.forms[0].ReserveTypeCodeFt_codelookupbtn.style.visibility = "hidden";
        }
        //abahl3:ends  rma 11568,11121,11120


        //MITS 32397 start: LossType/ DisabilityCategory/ DisabilityLossType should be disabled for Void/Printed transactions   
        if (document.forms[0].LossType_codelookup != null) {
            document.forms[0].LossType_codelookup.readOnly = "readonly";
            document.forms[0].LossType_codelookup.style.backgroundColor = "#F2F2F2";//abahl3:  rma 11568,11121,11120
            if (document.forms[0].LossType_codelookupbtn != null)
                document.forms[0].LossType_codelookupbtn.style.visibility = "hidden";//abahl3:  rma 11568,11121,11120
        }
        if (document.forms[0].DisabilityCat_codelookup != null) {
            document.forms[0].DisabilityCat_codelookup.readOnly = "readonly";
            document.forms[0].DisabilityCat_codelookup.style.backgroundColor = "#F2F2F2";//abahl3:  rma 11568,11121,11120
            if (document.forms[0].DisabilityCat_codelookupbtn != null)
                document.forms[0].DisabilityCat_codelookupbtn.style.visibility = "hidden";//abahl3:  rma 11568,11121,11120
        }
        if (document.forms[0].DisabilityLossType_codelookup != null) {
            document.forms[0].DisabilityLossType_codelookup.readOnly = "readonly";
            document.forms[0].DisabilityLossType_codelookup.style.backgroundColor = "#F2F2F2";//abahl3:  rma 11568,11121,11120
            if (document.forms[0].DisabilityLossType_codelookupbtn != null)
                document.forms[0].DisabilityLossType_codelookupbtn.style.visibility = "hidden";//abahl3:  rma 11568,11121,11120
        }
        //MITS 32397 end

    }

    if (window.opener.document.forms[0].isReadOnly != null)
    {
        if (window.opener.document.forms[0].isReadOnly.value == "true")
        {
            //stara Mits 16667
            if (ReserveTypeCodeFt != null) {
                HideUnhideReserveType(ReserveTypeCodeFt);

            }
            //ends here

            //rupal:start, r8 first & final payment
            //when the funds form is in readonly mode, these newly aded control for first & final payment should also be readonly as other controsl
            if (document.forms[0].Coverage_codelookup_cid != null) {
                document.forms[0].Coverage_codelookup_cid.readonly = "readonly";
                document.forms[0].Coverage_codelookup_cid.color = "#ffffff";
            }
            if (document.forms[0].Coverage_codelookup != null)
                document.forms[0].Coverage_codelookup.readOnly = "readonly";
            if (document.forms[0].Coverage_codelookupbtn != null)
                document.forms[0].Coverage_codelookupbtn.disabled = "true";
            if (document.forms[0].Policy != null)
                document.forms[0].Policy.disabled = true;
            if (document.forms[0].FFPayment != null)
                document.forms[0].FFPayment.disabled = true;
            if (document.forms[0].Unit != null)
                document.forms[0].Unit.disabled = true;
            //rupal:end
            if (document.forms[0].TransTypeCode_codelookup_cid != null) {
                document.forms[0].TransTypeCode_codelookup_cid.readOnly = "readonly";
                document.forms[0].TransTypeCode_codelookup_cid.color = "#ffffff";
            }
            if (document.forms[0].TransTypeCode_codelookup != null)
                document.forms[0].TransTypeCode_codelookup.readOnly = "readonly";
            if (document.forms[0].TransTypeCode_codelookupbtn != null)
                document.forms[0].TransTypeCode_codelookupbtn.disabled = "true";
            if (document.forms[0].reservebalance != null)
                document.forms[0].reservebalance.Enabled = false;
            if (document.forms[0].ReserveTypeCode_codelookup_cid != null)
                document.forms[0].ReserveTypeCode_codelookup_cid.readOnly = "readonly";

            if (document.forms[0].ReserveTypeCode_codelookup != null)
                document.forms[0].ReserveTypeCode_codelookup.readOnly = "readonly";
            if (document.forms[0].ReserveTypeCode_codelookupbtn != null) {
                document.forms[0].ReserveTypeCode_codelookupbtn.disabled = "true";
            }

            //MITS 32397 start: LossType/ DisabilityCategory/ DisabilityLossType should be disabled for Void/Printed transactions   
            if (document.forms[0].LossType_codelookup != null) {
                document.forms[0].LossType_codelookup.readOnly = "readonly";
                if (document.forms[0].LossType_codelookupbtn != null)
                    document.forms[0].LossType_codelookupbtn.disabled = "true";
            }
            if (document.forms[0].DisabilityCat_codelookup != null) {
                document.forms[0].DisabilityCat_codelookup.readOnly = "readonly";
                if(document.forms[0].DisabilityCat_codelookupbtn !=null)
                    document.forms[0].DisabilityCat_codelookupbtn.disabled = "true";
            }
            if (document.forms[0].DisabilityLossType_codelookup != null) {
                document.forms[0].DisabilityLossType_codelookup.readOnly = "readonly";
                if (document.forms[0].DisabilityLossType_codelookupbtn != null)
                    document.forms[0].DisabilityLossType_codelookupbtn.disabled = "true";
            }
            //MITS 32397 end

            //igupta3 hiding datepicker images MITS: 32573 
            $(".ui-datepicker-trigger").hide();
            //

            if (document.forms[0].FromDate != null) {
                document.forms[0].FromDate.color = "#ffffff";
                document.forms[0].FromDate.readOnly = "readonly";
            }
            if (document.forms[0].FromDatebtn != null) {
                document.forms[0].FromDatebtn.disabled = "true";
            }
            if (document.forms[0].ToDate != null) {
                document.forms[0].ToDate.readOnly = "readonly";
                document.forms[0].ToDate.color = "#ffffff";
            }
            if (document.forms[0].ToDatebtn != null) {
                document.forms[0].ToDatebtn.disabled = "true";
            }
            if (document.forms[0].InvoiceNumber != null) {
                document.forms[0].InvoiceNumber.readOnly = "readonly";
                document.forms[0].InvoiceNumber.color = "#f5f5f5";
            }
            if (document.forms[0].InvoicedBy != null) {
                document.forms[0].InvoicedBy.readOnly = "readonly";
                document.forms[0].InvoicedBy.color = "#f5f5f5";
            }
            if (document.forms[0].InvoiceDatebtn != null) {
                document.forms[0].InvoiceDatebtn.disabled = "true";
            }
            if (document.forms[0].InvoiceDate != null) {
                document.forms[0].InvoiceDate.readOnly = "readonly";
                document.forms[0].InvoiceDate.color = "#f5f5f5";
            }

            if (document.forms[0].InvoiceAmount != null) {
                document.forms[0].InvoiceAmount.readOnly = "readonly";
                document.forms[0].InvoiceAmount.color = "#f5f5f5";
            }
            if (document.forms[0].PoNumber != null) {
                document.forms[0].PoNumber.readOnly = "readonly";
                document.forms[0].PoNumber.color = "#f5f5f5";
            }
            if (document.forms[0].Amount != null) {
                document.forms[0].Amount.readOnly = "readonly";
                document.forms[0].Amount.color = "#f5f5f5";
            }
            if (document.forms[0].GlAccountCode_codelookup_cid != null) {
                document.forms[0].GlAccountCode_codelookup_cid.readOnly = "readonly";
                document.forms[0].GlAccountCode_codelookup_cid.color = "#f5f5f5";
            }
            if (document.forms[0].GlAccountCode_codelookup != null) {
                document.forms[0].GlAccountCode_codelookup.readOnly = "readonly";
            }

            if (document.forms[0].GlAccountCode_codelookupbtn != null) {
                document.forms[0].GlAccountCode_codelookupbtn.disabled = "true";
            }
            //document.forms[0].GlAccountCode_codelookup_cid.color = "#f5f5f5";
            if (document.forms[0].btnOk != null) {
                document.forms[0].btnOk.Enabled = false;
            }
            // document.forms[0].isReadOnly.value = window.opener.document.forms[0].isReadOnly.value;
            //tanwar2 - mits 30910 - start
            var overrideDeductibleFlag = document.getElementById('overridedeductible');
            if (overrideDeductibleFlag != null) {
                overrideDeductibleFlag.disabled = true;
            }
            //tanwar2 - mits 30910 - end
        }
        //        var amount = document.getElementById("Amount");
        //        if (amount != null) {
        //            if (amount.value != null) {
        //                document.forms[0].Amount.value = "$0.00";
        //            }
        //            else {
        //                amount.innertext = "$0.00";
        //            }
        //        }
        //        SetPVControlValue("Amount", "$0.00");
        //            if(document.forms[0].reservebalance != null)//Null check added for MITS 17624
        //            {
        //                document.forms[0].reservebalance.value = "$0.00"; 
        //            }

        //End rsushilaggar
        //Deb MITS 26077
        if (document.forms[0].Amount != null) {
            if (document.forms[0].Amount.value == "") {
                SetPVControlValue("Amount", "$0.00");
                if (document.forms[0].reservebalance != null)//Null check added for MITS 17624
                {
                    document.forms[0].reservebalance.value = "$0.00";
                }
            }
        }
        //Deb MITS 26077
    }

    //rupal:start, first & final payment enh    
    //if (window.opener.document.forms[0].collectionflag.checked == true) { //mkaran2 - MITS 34434
    //start -  nbhatia6 .JIRA# RMA-11396 - Generate First and Final Recovery
    //if (window.opener.document.forms[0].collectionflag != null && window.opener.document.forms[0].collectionflag.checked == true) {
    //    if (document.getElementById("FirstFinalPayment") != null) {
    //        document.getElementById("FirstFinalPayment").checked = false;
    //        document.getElementById("FirstFinalPayment").disabled = true;
    //    }
    //}
   //end -  nbhatia6 .JIRA# RMA-11396 - Generate First and Final Recovery
    //rupal:end   

    if (document.getElementById("txtPostBack").value != "DoneOK")
    {
        SetReserves();
    }


}
//stara Mits 16667
function  HideUnhideReserveType(objReserveTypeCodeFt)
{
      var objReserveTypeCode=eval("document.getElementById('div_ReserveTypeCode')");
      
      objReserveTypeCodeFt.style.visibility="hidden";
      objReserveTypeCodeFt.style.display = "none";
      //Start rsushilaggar MITS 25353 Date 07/06/2011
      var objReserveTypeCodeft_Lookup = eval("document.getElementById('ReserveTypeCodeFt_codelookup')");
      if (objReserveTypeCodeft_Lookup != null) {
          objReserveTypeCodeft_Lookup.value = "";
      }

      var objReserveTypeCodeft_Lookup_Cid = eval("document.getElementById('ReserveTypeCodeFt_codelookup_cid')");
      if (objReserveTypeCodeft_Lookup_Cid != null) {
          objReserveTypeCodeft_Lookup_Cid.value = "";
      }
        //document.forms[0].ReserveTypeCodeFt_codelookup_cid.value="";
	    
      if (document.getElementById("mode").value == "add" && document.getElementById('TransTypeCode_codelookup') != null && document.getElementById('TransTypeCode_codelookup').getAttribute("powerviewreadonly") != "true") {
	        var TransTypeCode_codelookup = eval("document.getElementById('TransTypeCode_codelookup')");
	        if (TransTypeCode_codelookup != null) {
	            TransTypeCode_codelookup.disabled = false;
	            TransTypeCode_codelookup.style.backgroundColor = "";
	        }
	        var TransTypeCode_codelookupbtn = eval("document.getElementById('TransTypeCode_codelookupbtn')");
	        if (TransTypeCode_codelookupbtn != null) {
	            TransTypeCode_codelookupbtn.disabled = false;
	        }
	    
	     }
	  
      if(objReserveTypeCode !=null)
      {
      
        objReserveTypeCode.style.visibility="visible";
        objReserveTypeCode.style.display="inline";
      }
    //End rsushilaggar


}
//ends here

function Split_onCancel() 
{
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) 
        {
            var btnOK = document.getElementById("btnOk");
            btnOK.click();
        }
        else {
            window.close();
            retval = false;
        }
    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function Split_Validate() {
//rsushilaggar MITS 25353 date 07/06/2011
    if (GetPVControlValue('Amount') == "") {
        //mbahl3 mits 20872
         //Ankit Start - ML Changes
        //alert("The Amount Field is empty Please enter an amount.");
        alert(parent.CommonValidations.EmptyAmount);
        //End
       //mbahl3 mits 20872
        tabChange(m_TabList[0].id.substring(4));   //pmittal5 Mits 18623 11/19/09 - Shift the focus to "Transaction" tab
        if (document.forms[0].Amount != null) {
            document.forms[0].Amount.focus();
        }
        return false;
    }
    var sAmount = GetPVControlValue('Amount');
    if (sAmount.indexOf('(') == -1) {
        if (ToCurrency(GetPVControlValue('Amount')) == 0) {
	//rsharma220 MITS 31131
            var sRetVal = confirm(parent.CommonValidations.ZeroAmount); 
            if (!sRetVal) {
                if (m_TabList != null && m_TabList[0] != null)
                    tabChange(m_TabList[0].id.substring(4));   //pmittal5 Mits 18623 11/19/09 - Shift the focus to "Transaction" tab
                document.forms[0].Amount.focus();
                return false;
            }
            else {
                //MITS:34082 START
                //SetPVControlValue('Amount', "$0.00");
                SetPVControlValue('Amount', "0.00");
                //MITS:34082 END
            }
        }
    }

//pmittal5 Mits 18394 11/11/09 - Validation message should appear only when a claim is attached and Reserve Type lookup field is blank
    //stara Mits 16667
    if (document.forms[0].ReserveTypeCodeFt_codelookup != null && document.forms[0].claimid.value != "0" && document.forms[0].claimid.value != "") {
        if ((document.forms[0].ReserveTypeCodeFt_codelookup_cid.value == "") || (document.forms[0].ReserveTypeCodeFt_codelookup_cid.value == "0")) {
            if (document.forms[0].ReserveTypeCode_codelookup != null) {
                if ((document.forms[0].ReserveTypeCode_codelookup_cid.value == "") || (document.forms[0].ReserveTypeCode_codelookup_cid.value == "0")) {
                    if (parseInt(document.forms[0].ReserveTypeCode_codelookup_cid.value, 10) <= 0) {
                        //mbahl3 mits 20872 
                        //Ankit Start - ML Changes
                        //alert("Please select Reserve Type.");
                        alert(parent.CommonValidations.ValidReserveType);
                        //End
                       //mbahl3 mits 20872
                        return false;
                    }
                }
            }
        }
        //IC Retrofit for REM Transaction Code , MITS 20093		    
       //Changed by asingh264 on 12 March 2010 REM-MITS #19821
        if ((parseInt(document.forms[0].ReserveTypeCodeFt_codelookup_cid.value, 10) <= 0) && (document.forms[0].ReserveTypeCodeFt_codelookup_cid.value == "" || document.forms[0].ReserveTypeCode_codelookup_cid.value == "0")) {
            //alert("Please select Reserve Type.");
            document.forms[0].TransTypeCode_codelookup_cid.value = "";
            return false;
        }
		//End of REM-MITS #19821
    }
    //ends here

//    if ((document.forms[0].ReserveTypeCode_codelookup_cid.value == "") || (document.forms[0].ReserveTypeCode_codelookup_cid.value == "0")) {
//        alert("Please select Reserve Type.");
//        return false;
//    }

//    if (parseInt(document.forms[0].ReserveTypeCode_codelookup_cid.value, 10) <= 0 && document.forms[0].ReserveTypeCode_codelookup_cid.value == "") {
//        alert("Please select Reserve Type.");
//        return false;
//    }
    //End - pmittal5    
    if (document.forms[0].TransTypeCode_codelookup_cid != null && ((document.forms[0].TransTypeCode_codelookup_cid.value == "") || (document.forms[0].TransTypeCode_codelookup_cid.value == "0"))) {
      //rsharma220 MITS 31131
       alert(parent.CommonValidations.TransactionType);   
        return false;
    }

    if (document.forms[0].TransTypeCode_codelookup_cid!= null && parseInt(document.forms[0].TransTypeCode_codelookup_cid.value, 10) <= 0 && document.forms[0].TransTypeCode_codelookup_cid.value == "") {
       //rsharma220 MITS 31131
        alert(parent.CommonValidations.TransactionType); 
        return false;
    }


    //rupal:start, bob , coverage type wl be visible and mendatory when carrier claim is on
    if (document.forms[0].Coverage_codelookup_cid != null && ((document.forms[0].Coverage_codelookup_cid.value == "") || (document.forms[0].Coverage_codelookup_cid.value == "0"))) {
       //mbahl3 mits 20872
       //Ankit Start - ML Changes
        //alert("Please select Coverage.");
        alert(parent.CommonValidations.ValidCoverage);
        //End
	//mbahl3 mits 20872
        return false;
    }

    if (document.forms[0].Coverage_codelookup_cid != null && parseInt(document.forms[0].Coverage_codelookup_cid.value, 10) <= 0 && document.forms[0].Coverage_codelookup_cid.value == "") {
        //mbahl3 mits 20872
	//Ankit Start - ML Changes
        //alert("Please select Coverage.");
        alert(parent.CommonValidations.ValidCoverage);
        //End
	//mbahl3 mits 20872
        return false;
    }
    //rupal:end


    //pmittal5 Mits 18394 11/11/09 - Merged REM changes
    if (document.forms[0].ReserveTypeCode_codelookup != null) {
        if ((document.forms[0].ReserveTypeCode_codelookup_cid.value == 0) && ((document.forms[0].claimid.value != "") && (document.forms[0].claimid.value != "0"))) {
            //mbahl3 mits 20872
	    //Ankit Start - ML Changes
            //alert("Transaction Type must be linked to a Reserve Type,Cannot Save!");
            alert(parent.CommonValidations.ValidSave);
            //End
	    //mbahl3 mits 20872
            return false;
        }
    }
    //Raman 03/08/2009 : R6 Work Loss / Restriction
    var oAutoCrtWrkLossRest = window.opener.document.getElementById('AutoCrtWrkLossRest');
    var olob = window.opener.document.getElementById('lob');
    if (oAutoCrtWrkLossRest != null && oAutoCrtWrkLossRest.value.toLowerCase() == "true" && olob != null && (olob.value == "844" || olob.value == "243") && (document.getElementById("ToDate").value == "" || document.getElementById("FromDate").value == "")) {
        //Changed alert box to confirm box. MITS 18875:smishra start
        //mbahl3 mits 20872
	//Ankit Start - ML Changes
        //var bRet = confirm("Both From Date and To Date are mandatory for creation of Work Loss and Restriction records.Do you wish to save the payment without Work Loss and Restriction records being auto created ? ");
        var bRet = confirm(parent.CommonValidations.ValidDate);
        //End
	//mbahl3 mits 20872
        if (!bRet)
        {
            //Added for MITS 32087  -nnithiyanand
            if (document.forms[0].DupCriteria != null) {
                if (document.forms[0].DupCriteria.value == "11") {
                    if (document.forms[0].FromDate != null && document.forms[0].FromDate.value == "" && document.forms[0].ToDate != null && document.forms[0].ToDate.value == "") {
                        alert("The Duplicate Check for Payment History will not be done if From and To Dates are missing. Please ensure you have entered the dates in the From and To Date field.");
                    }
                }
            }
            return false;
        }
        //Changed alert box to confirm box. MITS 18875:smishra end
    }
    //Added for MITS 32087  -nnithiyanand
    if (document.forms[0].DupCriteria != null) {
        if (document.forms[0].DupCriteria.value == "11") {
            if (document.forms[0].FromDate != null && document.forms[0].FromDate.value == "" && document.forms[0].ToDate != null && document.forms[0].ToDate.value == "") {
                alert("The Duplicate Check for Payment History will not be done if From and To Dates are missing. Please ensure you have entered the dates in the From and To Date field.");
            }
        }
    }

    if (document.forms[0].FromDate != null && document.forms[0].FromDate.value != "" && document.forms[0].ToDate != null && document.forms[0].ToDate.value == "") {
        //mbahl3 mits 20872
	//Ankit Start - ML Changes
        //alert("Please enter 'To Date'");
        alert(parent.CommonValidations.ValidToDate);
        //End
	//mbahl3 mits 20872
        return false;
    }
    if (document.forms[0].FromDate != null && document.forms[0].FromDate.value == "" && document.forms[0].ToDate != null && document.forms[0].ToDate.value != "") {
        
	//mbahl3 mits 20872//Ankit Start - ML Changes
        //alert("Please enter 'From Date'");
        alert(parent.CommonValidations.ValidFromDate);
        //End
	//mbahl3 mits 20872
        return false;
    }
    var sToDate = new Date(document.getElementById("ToDate").value);
    var sFromDate = new Date(document.getElementById("FromDate").value);
    if (sFromDate > sToDate) {
        
	//mbahl3 mits 20872
	//Ankit Start - ML Changes
        //alert("From date must be less than To date");
        alert(parent.CommonValidations.ValidLessFromDate);
        //End
	//mbahl3 mits 20872
        document.getElementById("fromdate").value = "";
        document.getElementById("todate").value = "";
        return false;
    }
//Ankit Start : MITS - 20872
	//Mits id: 32090 - Govind - start
    var oEle = eval("document.forms[0].SysRequired");
    var arr = null;
    var objElem = null;
    if (oEle != null) {
        sReq = new String(document.forms[0].SysRequired.value);
        //Start : These controls are already handled at top of this function, so do not need to handle it again.
        sReq = sReq.replace('ReserveTypeCodeFt_codelookup_cid|', '');
        sReq = sReq.replace('Coverage_codelookup_cid|', '');
        sReq = sReq.replace('TransTypeCode_codelookup_cid|', '');
        sReq = sReq.replace('Amount|', '');
        //End
    }
    else {
        sReq = new String("");
    }

    arr = sReq.split("|");
    var arr = sReq.split("|");

    for (var i = 0; i < arr.length; i++) {
        objElem = null;
        if (Trim(arr[i]) != "") {
            objElem = eval('document.forms[0].' + arr[i]);
            arrValue = Trim(arr[i]);
        }

        if (objElem != null) {
            var s = new String(objElem.value);
            if (replace(s, " ", "") == "" || (arr[i].substring(arr[i].length - 4, arr[i].length) == "_cid" && s == "0")) {
                //alert("Not all required fields contain the value. Please enter the data into all underlined fields.");
                alert(parent.CommonValidations.MandatoryFieldValidation);
                if (arr[i].substring(arr[i].length - 4, arr[i].length) == "_cid")
                    objElem = eval("document.forms[0]." + arr[i].substring(0, arr[i].length - 4));
                objElem.focus();
                return false;
            }
        }
    }
	//Mits id: 32090 - Govind - End
    //Ankit End
    if (document.getElementById("overridedeductible") != null && document.getElementById("overridedeductible").checked != true) {
        if (document.getElementById("NotDetDedTypeCode") != null && document.getElementById("SelectedDedTypeCode") != null) {
            if (document.getElementById("NotDetDedTypeCode").value == document.getElementById("SelectedDedTypeCode").value) {
                //alert("Transaction cannot be proceeded on this reserve. The deductible type is Not Determined.");
		alert(parent.CommonValidations.NotDetDedValdTran);
                return false;
            }
        }
    }
    var CtrlDedRemainingAmt = document.getElementById("DedRemainingAmt");
    var iAmountOnSplit = parseFloatMC(document.getElementById('Amount').getAttribute("Amount"));
 
    if (CtrlDedRemainingAmt != null && document.getElementById("IsManualDed") != null && document.getElementById("IsManualDed").value =="1") {
        if (iAmountOnSplit > parseFloatMC(CtrlDedRemainingAmt.value)) {
            alert(parent.CommonValidations.DedAmtCheck);
	    //alert("Deductible cannot be saved. Deductible Collection Amount exceeds the Deductible Remaining Amount.");
            return false;
        }
        
    }
    //MITS:34082 MultiCurrency START
    if (ExchnageRateOverride()) {
        return true;
    }
    else { return false; }
    //MITS:34082 MultiCurrency END
}
//Deb Multi Currency
//MITS:34082 MultiCurrency START
function parseFloatMC(str) {

    if (typeof str === "number") {
        return str;
    }
    var array = str.split(/\.|,/);

    var value = '';
    for (var i in array) {
        if (i > 0 && i == array.length - 1) {
            value += ".";
        }
        value += array[i];
    }

    return Number(value);
}
function ExchnageRateOverride() {
    var sSplitCurrType = GetPVControlValue('CurrencyTypeForClaim_codelookup');
    if (sSplitCurrType != undefined) {
        var SplitExrate = GetPVControlValue('SplitToPaymentCurrRate');
        if (sSplitCurrType == '') {
            alert("Please select a currency type");
            document.forms[0].CurrencyTypeForClaim_codelookup.focus();
            return false;
        }
        var paymentCurrencyType = GetPVControlValue('PaymenmtCurrencyType');
        paymentCurrencyType = paymentCurrencyType.substring(0,5);
        var sAmount = GetPVControlValue('Amount');
        var iAmount = parseFloatMC(document.getElementById('Amount').getAttribute("Amount"));
        var sSplitCurrAmount = GetPVControlValue('SplitCurrAmount');
        sSplitCurrAmount = sAmount;
        SetPVControlValue('SplitCurrAmount', sSplitCurrAmount);
        var sInvoceAmount = GetPVControlValue('InvoiceAmount');
        var sSplitCurrInvoiceAmount = GetPVControlValue('SplitCurrencyInvAmount');
        var iInvoiceAmount = parseFloatMC(document.getElementById('InvoiceAmount').getAttribute("Amount"));

        if (sInvoceAmount.indexOf('(') == -1) {
            if (ToCurrency(GetPVControlValue('InvoiceAmount')) == 0) {
                SetPVControlValue('SplitCurrencyInvAmount', "0.00");
            }
        }

        sSplitCurrInvoiceAmount = sInvoceAmount;
        SetPVControlValue('SplitCurrencyInvAmount', sSplitCurrInvoiceAmount);
        var EffDate = GetPVControlValue('ExchangeRateDate')
        EffDate = EffDate.substring(0, 10);
        EffDate = EffDate.split(' ')[0];
        var dExcRateSplitToPayment = parseFloatMC(GetPVControlValue('ExchangeRate'));
        var sRetVal = '';
        var ratenew = (dExcRateSplitToPayment / 10);
        if (SplitExrate != undefined) {
            if (SplitExrate != "") {
                if (isNaN(parseFloat(SplitExrate))) {
                    alert(parent.CommonValidations.NumericValue);
                    document.forms[0].SplitToPaymentCurrRate.focus();
                    return false;
                }                
                if (SplitExrate <= 0) {
                    alert(parent.CommonValidations.NegativeValueNotAllowed);
                    document.forms[0].SplitToPaymentCurrRate.focus();
                    return false;
                }
                if (sSplitCurrType.substring(0,3) == paymentCurrencyType.substring(0,3)) {
                    if (SplitExrate != dExcRateSplitToPayment) {
                        alert(parent.CommonValidations.ExchangeRateOverridden);
                        document.forms[0].SplitToPaymentCurrRate.focus();
                        return false;
                    }
                }
                if (SplitExrate > (ratenew + dExcRateSplitToPayment) || SplitExrate < (dExcRateSplitToPayment - ratenew)) {
                    var newperc = (SplitExrate * 100) / dExcRateSplitToPayment;
                    if ((newperc > 110) || (newperc < 90)) {
                        if (newperc > 110) {
                            newperc = (newperc - 100).toFixed(2);
                            sRetVal = confirm("New rates entered are " + newperc + "% higher than the default rates of " + EffDate + ". Do you want to procced?");
                        }
                        else if (newperc < 90) {
                            newperc = (100 - newperc).toFixed(2);
                            sRetVal = confirm("New rates entered are " + newperc + "% lesser than the default rates of " + EffDate + ". Do you want to procced?");
                        }
                        if (!sRetVal) { return false; }
                        else {
                            SetPVControlValue('Amount', SplitExrate * iAmount);
                            SetPVControlValue('InvoiceAmount', SplitExrate * iInvoiceAmount);
                            return true;
                        }
                    }
                    else {
                        SetPVControlValue('Amount', SplitExrate * iAmount);
                        SetPVControlValue('InvoiceAmount', SplitExrate * iInvoiceAmount);
                        return true;
                    }
                }
                else {
                    SetPVControlValue('Amount', SplitExrate * iAmount);
                    SetPVControlValue('InvoiceAmount', SplitExrate * iInvoiceAmount);
                    return true;
                }

            }

            else { 
                var str0 = parent.CommonValidations.NoExchangeRateSet.split('||')[0];
                var str1 = parent.CommonValidations.NoExchangeRateSet.split('||')[1];
                alert(str0 + sSplitCurrType + str1 + paymentCurrencyType);
                return false;
            }
        }
        else {
            if (dExcRateSplitToPayment != "") {
                SetPVControlValue('Amount', dExcRateSplitToPayment * iAmount);
                SetPVControlValue('InvoiceAmount', dExcRateSplitToPayment * iInvoiceAmount);
            }
            else {   
                var str0 = parent.CommonValidations.NoExchangeRateSet.split('||')[0];
                var str1 = parent.CommonValidations.NoExchangeRateSet.split('||')[1];
                alert(str0 + sSplitCurrType + str1 + paymentCurrencyType);
                return false;
            }
            return true;
        }
        return true;
    }
    else {
        return true;
    }
}
//MITS:34082 MultiCurrency END
function ToCurrency(val) {

    var dbl = new String(val);
    // Strip and Validate Input
    // BSB Remove commas first so parseFloat doesn't incorrectly truncate value.
    if (isNaN(dbl.charAt(0)))
        dbl = dbl.substring(1);

    dbl = dbl.replace(/[,]/g, "");

    if (dbl.length == 0)
        return 0;

    if (isNaN(parseFloat(dbl)))
        return 0;

    //round to 2 places
    dbl = parseFloat(dbl);
    dbl = dbl.toFixed(2);
    return dbl;
}
//Deb Multi Currency
function EntityXSelfInsured_onOk() {
    var bResult = false;
    //JIRA 8901
    //if (IsDataChanged() == false) { 
    //    window.close(); 
    //    return true;
    //}
    if (EntityXSelfInsured_Validate()) {
        return true;
    }
    else {
        return false;
    }
}

function EntityXSelfInsured_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (EntityXSelfInsured_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}

function EntityXSelfInsured_Validate() {   
    if(document.forms[0].LobCode.value==0)
    {
        alert("Line Of Business is a required field");
        document.forms[0].LobCode.focus();
        return false;
    }   
        return true;
}

function FuturePayment_onOk() {
	var bResult = false;
		
	if(IsDataChanged()==false)
	{
		window.close();
		return true;
	}

	if(FuturePayment_Validate())
	{
		var sMode = document.getElementById("mode").value;
        window.opener.document.forms[0].SysPostBackAction.value = "futurepayments";
		if(sMode == "edit")
		{
		    if (document.getElementById("Tag").value > 0) {
//Deb : Commented Multi Currency
//                // MITS 15497: ybhaskar START
//		        var sAmount = document.forms[0].Amount.value;

//		        var iTempIndex = sAmount.indexOf("$");

//		        if (iTempIndex != -1) {
//		            sAmount = sAmount.replace('$', '');
//		        }
//		        
//		        sAmount = sAmount.replace(/,/g,'');

//		        document.getElementById("Amount").value = sAmount * -1;
//		        
//		        //MITS 15497: ybhaskar END
//Deb : Commented Multi Currency		        
		    }
            window.opener.document.forms[0].FuturePaymentsGrid_RowEditFlag.value = "true";
            window.opener.document.forms[0].FuturePaymentsGrid_RowAddedFlag.value = "false";
		}
		else if(sMode == "add")
		{
            window.opener.document.forms[0].FuturePaymentsGrid_RowAddedFlag.value = "true";
            window.opener.document.forms[0].FuturePaymentsGrid_RowEditFlag.value = "false";
		}

		if (document.forms[0].ReserveTypeCode_codelookup_cid != null) {
		    var sParentID = "";
		    sParentID = document.forms[0].ReserveTypeCode_codelookup_cid.value;
		    //added by rkaur
		    if(window.opener.document.forms[0].reserveID!=null)    
		    window.opener.document.forms[0].reserveID.value = sParentID;
		    //end here
}
//Changed by Gagan for MITS 20093
		//start:added by nitin goel,MITS 30910,05/15

		//if (document.frmData.overridedeductible != null) {
		   
		  //  if (window.opener.document.frmData.IsOverrideDedProcessing != null)
		   //     window.opener.document.frmData.IsOverrideDedProcessing.value = document.frmData.overridedeductible.checked;
	//	}

		if (document.getElementById("overridedeductible") != null) {

		    if (window.opener.document.getElementById("IsOverrideDedProcessing") != null)
		        window.opener.document.getElementById("IsOverrideDedProcessing").value = document.getElementById("overridedeductible").checked;
		}
		//end:added by nitin goel
if (document.forms[0].ReserveTypeCode$codelookup != null) {
    var sParentIDDesc = "";
    sParentIDDesc = document.forms[0].ReserveTypeCode$codelookup.value;

    if (window.opener.document.forms[0].reserveIDDesc != null)
        window.opener.document.forms[0].reserveIDDesc.value = sParentIDDesc;

}
if (document.forms[0].Coverage$codelookup != null) {
    var sParentIDDesc = "";
    sParentIDDesc = document.forms[0].Coverage$codelookup.value;

    if (window.opener.document.forms[0].CovIDDesc != null)
        window.opener.document.forms[0].CovIDDesc.value = sParentIDDesc;

}
if (document.forms[0].Coverage_codelookup_cid != null) {
    var sParentID = "";
    sParentID = document.forms[0].Coverage_codelookup_cid.value;
    //added by rkaur
    if (window.opener.document.forms[0].CovID != null)
        window.opener.document.forms[0].CovID.value = sParentID;
    //end here
}
//change for 32658 start
if (document.forms[0].DisabilityLossType_codelookup != null) {
    var sParentIDDesc = "";
    sParentIDDesc = document.forms[0].DisabilityLossType_codelookup.value;
    if (window.opener.document.forms[0].LossID != null)
        window.opener.document.forms[0].LossDesc.value = sParentIDDesc;
}
if (document.forms[0].DisabilityLossType_codelookup_cid != null) {
    var sParentIDDesc = "";
    sParentID = document.forms[0].DisabilityLossType_codelookup_cid.value;
    if(window.opener.document.forms[0].LossID != null)
        window.opener.document.forms[0].LossID.value = sParentID;
}

if (document.forms[0].LossType$codelookup != null) {
    var sParentIDDesc = "";
    sParentIDDesc = document.forms[0].LossType$codelookup.value;

    if (window.opener.document.forms[0].LossID != null)
        window.opener.document.forms[0].LossDesc.value = sParentIDDesc;

}
if (document.forms[0].LossType_codelookup_cid != null) {
    var sParentID = "";
    sParentID = document.forms[0].LossType_codelookup_cid.value;
    //added by rkaur
    if (window.opener.document.forms[0].LossID != null)
        window.opener.document.forms[0].LossID.value = sParentID;
    //end here
}
//end 32658; change here
if (document.forms[0].Policy != null) {
    var iSelectedIndex = window.document.forms[0].Policy.selectedIndex;
    if (iSelectedIndex > -1) {
        var policyid = window.document.forms[0].Policy.options[iSelectedIndex].value;
        var policydesc = window.document.forms[0].Policy.options[iSelectedIndex].text;
        if (window.opener.document.forms[0].PolID != null)
            window.opener.document.forms[0].PolID.value = policyid;
        if (window.opener.document.forms[0].PolIDDesc != null)
            window.opener.document.forms[0].PolIDDesc.value = policydesc;
    }
}
//rupal:start, r8 unit implementation
if (document.forms[0].Unit != null) {
    var iSelectedIndex = window.document.forms[0].Unit.selectedIndex;
    if (iSelectedIndex > -1) {
        var PolUnitID = window.document.forms[0].Unit.options[iSelectedIndex].value;
        var PolUnitDesc = window.document.forms[0].Unit.options[iSelectedIndex].text;
        if (window.opener.document.forms[0].PolUnitRowID != null)
            window.opener.document.forms[0].PolUnitRowID.value = PolUnitID;
        if (window.opener.document.forms[0].PolUnitDesc != null)
            window.opener.document.forms[0].PolUnitDesc.value = PolUnitDesc;
    }
}
//rupal:end

//Ankit : Start Policy System Interface
if (document.forms[0].CovgSeqNum != null) {
    if (window.opener.document.forms[0].CovgSeqNum != null)
        window.opener.document.forms[0].CovgSeqNum.value = document.forms[0].CovgSeqNum.value;
}
//Ankit : End
//mits 33996
if (document.forms[0].TransSeqNum != null) {
    if (window.opener.document.forms[0].TransSeqNum != null)
        window.opener.document.forms[0].TransSeqNum.value = document.forms[0].TransSeqNum.value;
}
	    //mits 33996
	    //Ankit Start : Worked on MITS - 34297
        if (document.forms[0].CoverageKey != null) {
            if (window.opener.document.forms[0].CoverageKey != null)
                window.opener.document.forms[0].CoverageKey.value = document.forms[0].CoverageKey.value;
        }
	    //Ankit End
		return true;
	}
	else
	{
		return false;
	}
}

function FuturePayment_onCancel()
{
	if(IsDataChanged()==true)
	{
		if(ConfirmSave()==true)
		{
			return FuturePayment_onOk();	
		}
	}
	window.close();
	return true;
}

function FuturePayment_onLoad() {
    
    //IC Retrofit for REM Transaction Code , MITS 20093
    //--added by rkaur7 on 02/18/2010 REM-MITS #19821
    var ReserveTypeCodeFt="";
    ReserveTypeCodeFt= eval("document.getElementById('div_ReserveTypeCodeFt')");
    //ends here
	var sMode = document.getElementById("mode").value;
    //IC Retrofit for REM Transaction Code , MITS 20093
   //added by asingh264 on 8 March 2010 MITS# 19821- To check if the opener is null or not and thus returns
	if(window.opener.document == null)
		return;
   if (window.opener.document.forms[0] == null)
		return;
   //ends here  
	
	if (window.opener.document.forms[0].lob != null)
        document.forms[0].lineofbusinesscode.value = window.opener.document.forms[0].lob.value;

    if (window.opener.document.forms[0].claimid != null)
    {
        document.forms[0].claimid.value = window.opener.document.forms[0].claimid.value;
             //IC Retrofit for REM Transaction Code , MITS 20093
             //--added by rkaur7 on 02/18/2010 REM-MITS #19821
            if((document.forms[0].claimid.value == "") || (document.forms[0].claimid.value == "0"))
           {
           
              if(ReserveTypeCodeFt !=null)
             {
               HideUnhideReserveType(ReserveTypeCodeFt);
            
             }
           
          }
          //ends rkaur7
        
    }

    if (window.opener.document.forms[0].orgEid != null)
        document.forms[0].orgEid.value = window.opener.document.forms[0].orgEid.value;

    //Deb:BOB    
    if (window.opener.document.forms[0].PolicyID != null)
        document.forms[0].PolicyID.value = window.opener.document.forms[0].PolicyID.value;
    if (window.opener.document.forms[0].PolCvgID != null)
        document.forms[0].PolCvgID.value = window.opener.document.forms[0].PolCvgID.value;
    if (window.opener.document.forms[0].ResTypeCode != null)
        document.forms[0].ResTypeCode.value = window.opener.document.forms[0].ResTypeCode.value;
    if (window.opener.document.forms[0].claimantEIdPassedIn != null)
        document.forms[0].clm_entityid.value = window.opener.document.forms[0].claimantEIdPassedIn.value;    
    //abahl3:statrts 11468,11121,11120
    if (window.opener.document.forms[0].RcRowID != null)
        document.forms[0].RcRowID.value = window.opener.document.forms[0].RcRowID.value;
    //abahl3:statrts 11468,11121,11120

    //Ankit : Start Policy System Interface
    if (document.forms[0].CovgSeqNum.value == "") {
        if (window.opener.document.forms[0].CovgSeqNum != null) {
            document.forms[0].CovgSeqNum.value = window.opener.document.forms[0].CovgSeqNum.value;
        }
    }
    //Ankit : End
    //mits 33996
    if (document.forms[0].TransSeqNum != null && document.forms[0].TransSeqNum.value == "") {
        if (window.opener.document.forms[0].TransSeqNum != null) {
            document.forms[0].TransSeqNum.value = window.opener.document.forms[0].TransSeqNum.value;
        }
    }
    //mits 33996
    //Ankit Start : Worked on MITS - 34297
    if (document.forms[0].CoverageKey != null) {
        if (document.forms[0].CoverageKey.value == "") {
            if (window.opener.document.forms[0].CoverageKey != null) {
                document.forms[0].CoverageKey.value = window.opener.document.forms[0].CoverageKey.value;
            }
        }
    }
    //Ankit End
    //Deb:BOB  
    if (window.opener.document.forms[0].CvgLossID != null) {
        if (document.forms[0].CvgLossID != null)
            document.forms[0].CvgLossID.value = window.opener.document.forms[0].CvgLossID.value;
    }
    var RsvStatusParent = "";
    if (window.opener.document.forms[0].ResStatusParent != null)
        RsvStatusParent = window.opener.document.forms[0].ResStatusParent.value;
    if (document.forms[0].ResTypeStatus != null)
        document.forms[0].ResTypeStatus.value = RsvStatusParent;


    if (window.opener.document.forms[0].autotransid != null) {
        if (document.forms[0].AutoTransId != null)
            document.forms[0].AutoTransId.value = window.opener.document.forms[0].autotransid.value;
    }

    //stara Mits 16667

    //abahl3 111468,11121,1120 starts
    if ((RsvStatusParent.toUpperCase() == "C") || (document.forms[0].RcRowID.value != "")) {
        //if (ReserveTypeCodeFt != null) {
        //    HideUnhideReserveType(ReserveTypeCodeFt);

        //}
        //ends here

        //rupal:start, r8 first & final payment
        //when the funds form is in readonly mode, these newly aded control for first & final payment should also be readonly as other controsl
        if (document.forms[0].Coverage_codelookup_cid != null) {
            document.forms[0].Coverage_codelookup_cid.readonly = "readonly";
            document.forms[0].Coverage_codelookup_cid.color = "#ffffff";
        }
        if (document.forms[0].Coverage_codelookup != null) {
            document.forms[0].Coverage_codelookup.readOnly = "readonly";
            document.forms[0].Coverage_codelookup.style.backgroundColor = "#F2F2F2";
        }
        if (document.forms[0].Coverage_codelookupbtn != null)
            document.forms[0].Coverage_codelookupbtn.style.visibility = "hidden";

        if (document.forms[0].Policy != null)
            document.forms[0].Policy.disabled = true;
        if (document.forms[0].FFPayment != null)
            document.forms[0].FFPayment.disabled = true;
        if (document.forms[0].Unit != null)
            document.forms[0].Unit.disabled = true;
        //rupal:end

        if (document.forms[0].reservebalance != null)
            document.forms[0].reservebalance.Enabled = false;
        if (document.forms[0].ReserveTypeCode_codelookup_cid != null)
            document.forms[0].ReserveTypeCode_codelookup_cid.readOnly = "readonly";

        if (document.forms[0].ReserveTypeCodeFt_codelookup_cid != null)
            document.forms[0].ReserveTypeCodeFt_codelookup_cid.readOnly = "readonly";

        if (document.forms[0].ReserveTypeCodeFt_codelookup != null) {
            document.forms[0].ReserveTypeCodeFt_codelookup.readOnly = "readonly";
            document.forms[0].ReserveTypeCodeFt_codelookup.style.backgroundColor = "#F2F2F2";
        }
        if (document.forms[0].ReserveTypeCodeFt_codelookupbtn != null) {
            document.forms[0].ReserveTypeCodeFt_codelookupbtn.style.visibility = "hidden";
        }


        if (document.forms[0].ReserveTypeCode_codelookup != null) {
            document.forms[0].ReserveTypeCode_codelookup.readOnly = "readonly";
            document.forms[0].ReserveTypeCode_codelookup.style.backgroundColor = "#F2F2F2";
        }
        if (document.forms[0].ReserveTypeCode_codelookupbtn != null) {
            document.forms[0].ReserveTypeCode_codelookupbtn.style.visibility = "hidden";
        }

        //MITS 32397 start: LossType/ DisabilityCategory/ DisabilityLossType should be disabled for Void/Printed transactions   
        if (document.forms[0].LossType_codelookup != null) {
            document.forms[0].LossType_codelookup.readOnly = "readonly";
            document.forms[0].LossType_codelookup.style.backgroundColor = "#F2F2F2";

            if (document.forms[0].LossType_codelookupbtn != null)
                document.forms[0].LossType_codelookupbtn.style.visibility = "hidden";
        }
        if (document.forms[0].DisabilityCat_codelookup != null) {
            document.forms[0].DisabilityCat_codelookup.readOnly = "readonly";
            document.forms[0].DisabilityCat_codelookup.style.backgroundColor = "#F2F2F2";

            if (document.forms[0].DisabilityCat_codelookupbtn != null)
                document.forms[0].DisabilityCat_codelookupbtn.style.visibility = "hidden";
        }
        if (document.forms[0].DisabilityLossType_codelookup != null) {
            document.forms[0].DisabilityLossType_codelookup.readOnly = "readonly";
            document.forms[0].DisabilityLossType_codelookup.style.backgroundColor = "#F2F2F2";

            if (document.forms[0].DisabilityLossType_codelookupbtn != null)
                document.forms[0].DisabilityLossType_codelookupbtn.style.visibility = "hidden";
        }
        //MITS 32397 end
    }

    //abahl3 111468,11121,1120 ends



	if(sMode=="edit") {
	    
		if( document.getElementById("Tag").value > 0 ) {
        //Deb Commented the Code for Multi Currency
        //	        var dAmount = new String(document.getElementById("Amount").value);
        //	        dAmount = dAmount.replace(/[,\$]/g, "");
        //	        // MITS: 15497: ybhaskar: START
        //	        
        //	        dAmount = dAmount.replace(/[,\(]/g, "");
        //	        dAmount = dAmount.replace(/[,\)]/g, "");
        //	        dAmount = dAmount * 1;

        //	        dAmount = parseFloat(dAmount);
        //	        
        //	        //Added for putting comma in 3 digit format
        //	        dAmount = dAmount.toFixed(2);
        //	        
        //	        //Handle Full Integer Section (Formats in groups of 3 with comma.)
        //	        var base = new String(dAmount);
        //	        base = base.substring(0, base.lastIndexOf('.'));
        //	        var newbase = "";
        //	        var j = 0;
        //	        var i = base.length - 1;
        //	        for (i; i >= 0; i--) {
        //	            if (j == 3) {
        //	                newbase = "," + newbase;
        //	                j = 0;
        //	            }
        //	            newbase = base.charAt(i) + newbase;
        //	            j++;
        //	        }
        //	        // Handle Fractional Part
        //	        var str = new String(dAmount);
        //	        str = str.substring(str.lastIndexOf(".") + 1);
        //	        // Reassemble formatted Currency Value string.
        //	        document.getElementById("Amount").value = "$" + newbase + "." + str;
		//	        // MITS: 15497: ybhaskar : END
        //Deb Commented the Code for Multi Currency
	    }
	    //rupal:bob change, in case of edit mode, policyid should be wiped out
	    document.forms[0].PolicyID.value = "";
	}
	SetReserves();
}

function FuturePayment_Validate() {
    //IC Retrofit for REM Transaction Code , MITS 20093
    //--added by rkaur7 on 02/18/2010 REM-MITS #19821
    if (document.forms[0].ReserveTypeCodeFt_codelookup != null && document.forms[0].claimid.value != "0" && document.forms[0].claimid.value != "") {
        if ((document.forms[0].ReserveTypeCodeFt_codelookup_cid.value == "") || (document.forms[0].ReserveTypeCodeFt_codelookup_cid.value == "0")) {
            if (document.forms[0].ReserveTypeCode_codelookup != null) {
                if ((document.forms[0].ReserveTypeCode_codelookup_cid.value == "") || (document.forms[0].ReserveTypeCode_codelookup_cid.value == "0")) {
                    //if (parseInt(document.forms[0].ReserveTypeCode_codelookup_cid.value, 10) <= 0) {
                        alert("Please select Reserve Type.");
                        return false;
                   // }
                }
            }
        }
        if (parseInt(document.forms[0].ReserveTypeCodeFt_codelookup_cid.value, 10) <= 0 && document.forms[0].ReserveTypeCodeFt_codelookup_cid.value == "") {
            alert("Please select Reserve Type.");
            return false;
        }
    }
    //ends rkaur7
    if (document.forms[0].TransTypeCode_codelookup.disabled == false) {
        if (document.forms[0].TransTypeCode_codelookup_cid.value == "" || document.forms[0].TransTypeCode_codelookup_cid.value == "0") {
            alert("Please select Transaction Type.");
            //IC Retrofit for REM Transaction Code , MITS 20093
            //Commented by asingh264 for MITS# 19821 on 4 March 2010--Reason:Focus can not be set on the objects which is disabled
            //document.forms[0].TransTypeCode_codelookupbtn.focus();
            //Comment block ends here
            return false;
        }

        if (parseInt(document.forms[0].TransTypeCode_codelookup_cid.value, 10) <= 0 && document.forms[0].TransTypeCode_codelookup.value == "") {
            alert("Please select Transaction Type.");
            document.forms[0].TransTypeCode_codelookupbtn.focus();
            return false;
        }
    }

    //rupal:start, bob
     var objCarrierclaim = window.opener.document.getElementById("MultipleCoverages");
     if (objCarrierclaim != null && objCarrierclaim.value == "True") {
         //coverage type wl be visible and mendatory when carrier claim is on
         if (document.forms[0].Coverage_codelookup_cid != null && ((document.forms[0].Coverage_codelookup_cid.value == "") || (document.forms[0].Coverage_codelookup_cid.value == "0"))) {
             alert("Please select Coverage.");
             return false;
         }
         if (document.forms[0].Coverage_codelookup_cid != null && parseInt(document.forms[0].Coverage_codelookup_cid.value, 10) <= 0 && document.forms[0].Coverage_codelookup_cid.value == "") {
             alert("Please select Coverage.");
             return false;
         }
     }
    //rupal:end



	var sAmount = document.forms[0].Amount.value;
	
	if(sAmount == "")
	{
		alert("Please enter Transaction Amount.");
		document.forms[0].Amount.focus();
		return false;
	}
	
//	var iTempIndex = sAmount.indexOf("$");
//	
//	if(iTempIndex != -1)
//	{
//	    sAmount = sAmount.replace('$','');
//	}
//	
//	if(parseFloat(sAmount)<= 0)
//	{
//		alert(" Negative or zero payments are not allowed.");
//		document.forms[0].Amount.focus();
//		return false;
//	}
	
	if(document.forms[0].ReserveTypeCode_codelookup.value==0 ) 
	{
		alert("Transaction Type must be linked to a Reserve Type,Cannot Save!");
		return false;
	}
	
	//Added Rakhi for MITS 12389:Start(Payment Detail Screen)
	if(document.forms[0].FromDate.value != "" && document.forms[0].ToDate.value == "" )
    {
        alert("Please enter To Date");
        return false;
	}
	if(document.forms[0].FromDate.value == "" && document.forms[0].ToDate.value != "" )
  	{
        alert("Please enter From Date");
        return false;
	}

	if (document.getElementById("IsOverrideDedProcessing") != null && document.getElementById("IsOverrideDedProcessing").checked != true) {
	    if (document.getElementById("NotDetDedTypeCode") != null && document.getElementById("SelectedDedTypeCode") != null) {
	        if (document.getElementById("NotDetDedTypeCode").value == document.getElementById("SelectedDedTypeCode").value) {
	            //alert("Transaction cannot be proceeded on this reserve. The deductible type is Not Determined.");
	            alert(parent.CommonValidations.NotDetDedValdTran);
	            return false;
	        }
	    }
	}
   //Added Rakhi for MITS 12389:End(Payment Detail Screen)

	return true;
}

function SetReserves() {
    var lCodeIdRes="";
    if (window.document.forms[0].TransTypeCode_codelookup_cid != null) {
        var lCodeIdTrans = window.document.forms[0].TransTypeCode_codelookup_cid.value;
        if (document.forms[0].ReserveTypeCodeFt_codelookup != null)
            lCodeIdRes = window.document.forms[0].ReserveTypeCodeFt_codelookup_cid.value;
        if (lCodeIdRes == "" || lCodeIdRes == 0)
            lCodeIdRes = window.document.forms[0].ResTypeCode.value;
        //if( lCodeId == "" || lCodeId == 0 )
        //    return;
    }
	var policyid = 0;
	if (document.forms[0].Policy) {
	    var iSelectedIndex = window.document.forms[0].Policy.selectedIndex;
	    if (iSelectedIndex > -1)
	        policyid = window.document.forms[0].Policy.options[iSelectedIndex].value;
	    else {
	        if (document.forms[0].PolicyID!=null) {
	            policyid = document.forms[0].PolicyID.value;
	        }
	    }
	}

	//rupal:start, r8 unit implementation
	var PolicyUnitRowId = 0;
	if (document.forms[0].Unit) {
	    var iSelectedIndex = window.document.forms[0].Unit.selectedIndex;
	    if (iSelectedIndex > -1) {
	        PolicyUnitRowId = window.document.forms[0].Unit.options[iSelectedIndex].value;
	    }
	}
    //rupal:end
	var lCoveCodeId = 0;
	if (document.forms[0].Coverage_codelookup_cid) {
	    lCoveCodeId = document.forms[0].Coverage_codelookup_cid.value;
	}

	if (document.forms[0].PolCvgID != null && lCoveCodeId=="") {
	    lCoveCodeId = document.forms[0].PolCvgID.value;
	 }
	

	var claimid = 0;
	if(window.opener.document.forms[0].claimid)
		claimid = window.opener.document.forms[0].claimid.value;
		
	var claimanteid = 0;
	if(window.opener.document.forms[0].clm_entityid)
		claimanteid = window.opener.document.forms[0].clm_entityid.value;
	
	var unitid = 0;
	if(window.opener.document.forms[0].unitid)
		unitid = window.opener.document.forms[0].unitid.value;
	//Deb Multi Currency
	var PmtCurrCode = 0;
	if (window.opener.document.forms[0].currencytypetext_codelookup_cid)
	    PmtCurrCode = window.opener.document.forms[0].currencytypetext_codelookup_cid.value;
    //MITS:34082 START
	if (document.forms[0].CurrencyTypeForClaim_codelookup_cid != null) {
	        if (document.forms[0].CurrencyTypeForClaim_codelookup_cid.value == "") {
	            document.forms[0].CurrencyTypeForClaim_codelookup_cid.value = PmtCurrCode;
	        
        }
    }
    //MITS:34082 END
	//Deb Multi Currency 
	//rupal:start,policy system interface
	var CovgSeqNum = "";
	if (document.forms[0].CovgSeqNum != null) {
	    if (document.forms[0].CovgSeqNum.value == "") {
	        if (window.opener.document.forms[0].CovgSeqNum) {
	            CovgSeqNum = window.opener.document.forms[0].CovgSeqNum.value;
	        }
	    }

	    else {
	        CovgSeqNum = document.forms[0].CovgSeqNum.value;
	    }
	}

	//mits 33996
	var TransSeqNum = "";
	if (document.forms[0].TransSeqNum != null) {
	    if (document.forms[0].TransSeqNum.value == "") {
	        if (window.opener.document.forms[0].TransSeqNum) {
	            TransSeqNum = window.opener.document.forms[0].TransSeqNum.value;
	        }
	    }

	    else {
	        TransSeqNum = document.forms[0].TransSeqNum.value;
	    }
	}
    //mits 33996
    //Ankit Start : Worked on MITS - 34297
	var CoverageKey = "";
	if (document.forms[0].CoverageKey != null) {
	    if (document.forms[0].CoverageKey.value == "") {
	        if (window.opener.document.forms[0].CoverageKey) {
	            CoverageKey = window.opener.document.forms[0].CoverageKey.value;
	        }
	    }
	    else {
	        CoverageKey = document.forms[0].CoverageKey.value;
	    }
	}
    //Ankit End

	var LossCodeType = "";
	if (document.forms[0].DisabilityLossType_codelookup_cid != null) {
	    if (document.forms[0].DisabilityLossType_codelookup_cid.value == "") {
	        if (window.opener.document.forms[0].DisabilityLossType_codelookup_cid) {
	            LossCodeType = window.opener.document.forms[0].DisabilityLossType_codelookup_cid.value;
	        }
	    }

	    else {
	        LossCodeType = document.forms[0].DisabilityLossType_codelookup_cid.value;
	    }
	}
	else if (document.forms[0].LossType_codelookup_cid != null) {
	    if (document.forms[0].LossType_codelookup_cid.value == "") {
	        if (window.opener.document.forms[0].LossType_codelookup_cid) {
	            LossCodeType = window.opener.document.forms[0].LossType_codelookup_cid.value;
	        }
	    }

	    else {
	        LossCodeType = document.forms[0].LossType_codelookup_cid.value;
	    }
	}
    //rupal:end
	var ctrlManualDed = document.getElementById("IsManualDed");
	var isManualDedVal;
	if (ctrlManualDed != null) {
	    isManualDedVal = ctrlManualDed.value;
	}
	if(claimid!=0) {
	    if (document.forms[0].ReserveTypeCodeFt_codelookup == null) {
	        if ((lCodeIdTrans == "" || lCodeIdTrans == 0) && (lCodeIdRes==0||lCodeIdRes==""))
	            return;
	        //Code changes by kuladeep for rmA14.1 Performance Start

	        //self.lookupCallback = "ReserveInfoSelected";
            ////rupal
	        ////m_Wnd = window.open('../Funds/ReservesInfoRender.aspx?ClaimId=' + claimid + '&TransTypeCode=' + lCodeIdTrans + '&ClaimantEid=' + claimanteid + "&UnitId=" + unitid + "&policyid=" + policyid + "&lCoveCodeId=" + lCoveCodeId + "&DataChanged=false&PolicyUnitRowId=" + PolicyUnitRowId + "&PmtCurrCode=" + PmtCurrCode
	        //m_Wnd = window.open('../Funds/ReservesInfoRender.aspx?ClaimId=' + claimid + '&TransTypeCode=' + lCodeIdTrans + '&ClaimantEid=' + claimanteid + "&UnitId=" + unitid + "&policyid=" + policyid + "&lCoveCodeId=" + lCoveCodeId + "&DataChanged=false&PolicyUnitRowId=" + PolicyUnitRowId + "&PmtCurrCode=" + PmtCurrCode + "&CovgSeqNum=" + CovgSeqNum + "&LossCode=" + LossCodeType +"&TransSeqNum="+TransSeqNum
            //    , 'progressWnd', 'width=400,height=150,top=' + ((screen.availHeight - 150) / 2) +
	        //    ',left="+((screen.availWidth-400)/2)+');
	        if (lCodeIdTrans > 0) {
	            if (parent.pleaseWait != null) {
	                parent.pleaseWait.pleaseWait('start');
	            }
	            $.ajax({
	                type: "POST",
	                url: "Split.aspx/GetReserveData",
	                contentType: "application/json; charset=utf-8",
	                data: '{ClaimId: "' + claimid + '",ReserveTransLookUpType:"TransTypeCode",ReserveTransTypeCode: "' + lCodeIdTrans + '",ClaimantEid:"' + claimanteid + '" , UnitId:"' + unitid + '",policyid:"' + policyid + '" , lCoveCodeId:"' + lCoveCodeId + '",DataChanged:"true",PolicyUnitRowId:"' + PolicyUnitRowId + '",PmtCurrCode:"' + PmtCurrCode + '", CovgSeqNum:"' + CovgSeqNum + '",LossCode:"' + LossCodeType + '",TransSeqNum:"' + TransSeqNum + '",CoverageKey:"' + CoverageKey + '",sIsManualDed:"'+isManualDedVal + '"}',
	                dataType: "json",
	                success: OnSuccess,
	                failure: function (response) {
	                    if (parent.pleaseWait != null) {
	                        parent.pleaseWait.pleaseWait('stop');
	                    }
	                    //TODO---For debug respond
	                    //alert(response.d);
	                }
	            });
	        } else if(lCodeIdRes>0){
	            if (parent.pleaseWait != null) {
	                parent.pleaseWait.pleaseWait('start');
	            }
	            $.ajax({
	                type: "POST",
	                url: "Split.aspx/GetReserveData",
	                contentType: "application/json; charset=utf-8",
	                data: '{ClaimId: "' + claimid + '",ReserveTransLookUpType:"ReserveTypeCode",ReserveTransTypeCode: "' + lCodeIdRes + '",ClaimantEid:"' + claimanteid + '" , UnitId:"' + unitid + '",policyid:"' + policyid + '" , lCoveCodeId:"' + lCoveCodeId + '",DataChanged:"true",PolicyUnitRowId:"' + PolicyUnitRowId + '",PmtCurrCode:"' + PmtCurrCode + '", CovgSeqNum:"' + CovgSeqNum + '",LossCode:"' + LossCodeType + '",TransSeqNum:"' + TransSeqNum + '",CoverageKey:"' + CoverageKey + '",sIsManualDed:"' + isManualDedVal + '"}',
	                dataType: "json",
	                success: OnSuccess,
	                failure: function (response) {
	                    if (parent.pleaseWait != null) {
	                        parent.pleaseWait.pleaseWait('stop');
	                    }
	                    //TODO---For debug respond
	                    //alert(response.d);
	                }
	            });
	        }
	        //Code changes by kuladeep for rmA14.1 Performance End
	    }
	    if (document.forms[0].ReserveTypeCodeFt_codelookup != null && (Trim(document.forms[0].TransTypeCode_codelookup.value) == "")) {
	        if (lCodeIdRes == "" || lCodeIdRes == 0) {
	            return;
	        }
	        if (document.getElementById('TransTypeCode_codelookup').getAttribute("powerviewreadonly") != "true") {
	            document.getElementById('TransTypeCode_codelookup').disabled = false;
	            if(document.getElementById('TransTypeCode_codelookupbtn') != null)
	                document.getElementById('TransTypeCode_codelookupbtn').disabled = false;
	            document.getElementById('TransTypeCode_codelookup').value = "";
	            document.getElementById('TransTypeCode_codelookup_cid').value = "";
	            document.getElementById('TransTypeCode_codelookup').style.backgroundColor = "";
	        }
	        //Code changes by kuladeep for rmA14.1 Performance Start
	        //self.lookupCallback = "ReserveInfoSelected";
            ////rupal
	        ////m_Wnd = window.open('../Funds/ReservesInfoRender.aspx?ClaimId=' + claimid + '&ReserveTypeCode=' + lCodeIdRes + '&ClaimantEid=' + claimanteid + "&UnitId=" + unitid + "&policyid=" + policyid + "&lCoveCodeId=" + lCoveCodeId + "&DataChanged=false&PolicyUnitRowId=" + PolicyUnitRowId + "&PmtCurrCode=" + PmtCurrCode
	        //m_Wnd = window.open('../Funds/ReservesInfoRender.aspx?ClaimId=' + claimid + '&ReserveTypeCode=' + lCodeIdRes + '&ClaimantEid=' + claimanteid + "&UnitId=" + unitid + "&policyid=" + policyid + "&lCoveCodeId=" + lCoveCodeId + "&DataChanged=false&PolicyUnitRowId=" + PolicyUnitRowId + "&PmtCurrCode=" + PmtCurrCode + "&CovgSeqNum=" + CovgSeqNum + "&LossCode=" + LossCodeType + "&TransSeqNum=" + TransSeqNum          
            //    , 'progressWnd', 'width=400,height=150,top=' + ((screen.availHeight - 150) / 2) +
            //    ',left="+((screen.availWidth-400)/2)+');


	        if (parent.pleaseWait != null) {
	            parent.pleaseWait.pleaseWait('start');
	        }
	        $.ajax({
	            type: "POST",
	            url: "Split.aspx/GetReserveData",
	            contentType: "application/json; charset=utf-8",
	            data: '{ClaimId: "' + claimid + '",ReserveTransLookUpType:"ReserveTypeCode",ReserveTransTypeCode: "' + lCodeIdRes + '",ClaimantEid:"' + claimanteid + '" , UnitId:"' + unitid + '",policyid:"' + policyid + '" , lCoveCodeId:"' + lCoveCodeId + '",DataChanged:"true",PolicyUnitRowId:"' + PolicyUnitRowId + '",PmtCurrCode:"' + PmtCurrCode + '", CovgSeqNum:"' + CovgSeqNum + '",LossCode:"' + LossCodeType + '",TransSeqNum:"' + TransSeqNum + '",CoverageKey:"' + CoverageKey + '",sIsManualDed:"' + isManualDedVal + '"}',
	            dataType: "json",
	            success: OnSuccess,
	            failure: function (response) {
	                if (parent.pleaseWait != null) {
	                    parent.pleaseWait.pleaseWait('stop');
	                }
	                //TODO---For debug respond
	                //alert(response.d);
	            }
	        });

	        //Code changes by kuladeep for rmA14.1 Performance End
	    }
	    else if (document.forms[0].ReserveTypeCodeFt_codelookup != null) {
	        if (lCodeIdRes == "" || lCodeIdRes == 0) {
	            return;
	        }
	        //Code changes by kuladeep for rmA14.1 Performance Start

	        //self.lookupCallback = "ReserveInfoSelected";
            ////rupal
	        ////m_Wnd = window.open('../Funds/ReservesInfoRender.aspx?ClaimId=' + claimid + '&ReserveTypeCode=' + lCodeIdRes + '&ClaimantEid=' + claimanteid + "&UnitId=" + unitid + "&policyid=" + policyid + "&lCoveCodeId=" + lCoveCodeId + "&DataChanged=false&PolicyUnitRowId=" + PolicyUnitRowId + "&PmtCurrCode=" + PmtCurrCode
	        //m_Wnd = window.open('../Funds/ReservesInfoRender.aspx?ClaimId=' + claimid + '&ReserveTypeCode=' + lCodeIdRes + '&ClaimantEid=' + claimanteid + "&UnitId=" + unitid + "&policyid=" + policyid + "&lCoveCodeId=" + lCoveCodeId + "&DataChanged=false&PolicyUnitRowId=" + PolicyUnitRowId + "&PmtCurrCode=" + PmtCurrCode + "&CovgSeqNum=" + CovgSeqNum + "&LossCode=" + LossCodeType + "&TransSeqNum=" + TransSeqNum
            //    , 'progressWnd', 'width=400,height=150,top=' + ((screen.availHeight - 150) / 2) +
	        //    ',left="+((screen.availWidth-400)/2)+');

	        if (parent.pleaseWait != null) {
	            parent.pleaseWait.pleaseWait('start');
	        }
	        $.ajax({
	            type: "POST",
	            url: "Split.aspx/GetReserveData",
	            contentType: "application/json; charset=utf-8",
	            data: '{ClaimId: "' + claimid + '",ReserveTransLookUpType:"ReserveTypeCode",ReserveTransTypeCode: "' + lCodeIdRes + '",ClaimantEid:"' + claimanteid + '" , UnitId:"' + unitid + '",policyid:"' + policyid + '" , lCoveCodeId:"' + lCoveCodeId + '",DataChanged:"true",PolicyUnitRowId:"' + PolicyUnitRowId + '",PmtCurrCode:"' + PmtCurrCode + '", CovgSeqNum:"' + CovgSeqNum + '",LossCode:"' + LossCodeType + '",TransSeqNum:"' + TransSeqNum + '",CoverageKey:"' + CoverageKey + '",sIsManualDed:"' + isManualDedVal + '"}',
	            dataType: "json",
	            success: OnSuccess,
	            failure: function (response) {
	                if (parent.pleaseWait != null) {
	                    parent.pleaseWait.pleaseWait('stop');
	                }
	                //TODO---For debug respond
	                //alert(response.d);
	            }
	        });

	        //Code changes by kuladeep for rmA14.1 Performance End
	    }         
	}
}

function Holidays_onOk() {
    var bResult = false;

    if (IsDataChanged() == false) {
        window.close();
        return true;
    }
    if (Holidays_Validate()) {
        return true;
    }
    else {
        return false;
    }
}
function Holidays_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (Holidays_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function Holidays_Validate()  //from R4 OnHolidaySave()
{   
   if (document.forms[0].Date.value=="")
	{
		alert("Please enter Date.");
		document.forms[0].Date.focus();
		return false;
	}
	if (document.forms[0].Description.value=="")
	{
		alert("Please enter Description.");
		document.forms[0].Description.focus();
		return false;
	}
    //jira isssue 1200
	if (document.forms[0].Description.value.length > 50) {
	    alert(parent.CommonValidations.DescLengthAlert);
	    document.forms[0].Description.focus();
	    return false;
	}


	if(document.forms[0].Org_orglist.value=="")
	{
		alert("Please select an Organization.");
		document.forms[0].Org.focus();
		return false;
	}
	//document.forms[0].FormMode.value="close"
	return true;

}

//MITS 33371 ajohari2: Start
function PolicySearchDataMapping_onOk() {
    var bResult = false;

    if (PolicySearchDataMapping_Validate()) {
        return true;
    }
    else {
        return false;
    }
}
function PolicySearchDataMapping_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (PolicySearchDataMapping_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }
        }
        else {
            window.close();
            retval = false;
        }
    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function PolicySearchDataMapping_Validate() {
    var flagVar = true;

    if (trim(document.forms[0].AgentCode.value) == "") {
        alert("Please enter Agent Code.");
        document.forms[0].AgentCode.focus();
        flagVar = false;
        return false;
    }
    if (trim(document.forms[0].MCO.value) == "") {
        alert("Please enter MCO.");
        document.forms[0].MCO.focus();
        flagVar = false;
        return false;
    }
    if (trim(document.forms[0].PCO.value) == "") {
        alert("Please enter PCO.");
        document.forms[0].PCO.focus();
        flagVar = false;
        return false;
    }
    if (trim(document.forms[0].BusUnit.value) == "") {
        alert("Please enter Business Unit.");
        document.forms[0].BusUnit.focus();
        flagVar = false;
        return false;
    }
    if (trim(document.forms[0].BusSeg.value) == "") {
        alert("Please enter Business Segment.");
        document.forms[0].BusSeg.focus();
        flagVar = false;
        return false;
    }

    if (flagVar == true) {
        return true;
    }

}
//MITS 33371 ajohari2: End

function ClaimTypeOptions_onOk() {
    //tkatsarski: 11/28/14 - RMA-4878: Error message was comming up, because there wasn't check if the data in the form is changed.
    if (ClaimTypeOptions_Validate()) {
        if (IsDataChanged() == false) {
            window.close();
        }
        return true;
    }
    else {
        return false;
    }
}

function ClaimTypeOptions_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (ClaimTypeOptions_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}

function ClaimTypeOptions_Validate()  
{   
 	var sCurClaimType=document.forms[0].CurClaimType_codelookup.value;
 	var sNewClaimType = document.forms[0].NewClaimType_codelookup.value;
 	var sIncSchChecks = document.forms[0].IncSchChecks_codelookup.value;
 	if (document.forms[0].Lob_codelookup.value == "") {
 	    alert("Please select a line of business.");
 	    document.forms[0].Lob_codelookup.focus();
 	    return false;
 	}
 	else if (sCurClaimType == "") {
 	    alert("Please select a valid Claim Type.");
 	    document.forms[0].CurClaimType_codelookup.focus();
 	    return false;
 	}
 	else if (sNewClaimType == "") {
 	    alert("Please select a valid Claim Type.");
 	    document.forms[0].NewClaimType_codelookup.focus();
 	    return false;
 	}
 	else if (document.forms[0].TrigType_codelookup.value == "") {
 	    alert("Please select a valid Transaction Type.");
 	    document.forms[0].TrigType_codelookup.focus();
 	    return false;
 	}
 	else if (sCurClaimType == sNewClaimType) {
 	    alert("Please select a claim type different from the current claim type.");
 	    document.forms[0].NewClaimType_codelookup.focus();
 	    return false;
 	}
	//document.forms[0].FormMode.value="close";
	return true;
}

function FLMaxRate_onOk() {
 var bResult = false;

    if (IsDataChanged() == false) {
        window.close();
        return true;
    }
    if (FLMaxRate_Validate()) {
        return true;
    }
    else {
        return false;
    }
}
function FLMaxRate_onCancel() {
    var retval = false;
    
    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (FLMaxRate_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function FLMaxRate_Validate()  //from R4 CheckRequiredFields()
{   
   var Year = document.getElementById('Year');
    var MaxRate = document.getElementById('MaxRate');
    
    var iYear;
    var dMaxRate;
    if(isNaN(iYear = parseInt(Year.value,10)))
    {
        alert('Please Enter the Year in format YYYY');
        Year.value = "";
        Year.focus();
        return false;
    }
    else if(iYear <= 999 )
    {
        alert('Please Enter the Year in format YYYY');
        Year.value = "";
        Year.focus();
        return false;
    }
    if( Year.value.length != 4 ) 
    {
        alert('Please Enter the Year in format YYYY');
        Year.value = "";
        Year.focus();
        return false;
    }
    if(isNaN(dMaxRate = parseFloat(MaxRate.value)))
    {
        alert('Please Enter a numeric value for Max Rate');
        MaxRate.value = "";
        MaxRate.focus();
        return false;
    }
    else if(dMaxRate <= 0)
    {
        alert('Max Rate should be a positive amount');
        MaxRate.value = "";
        MaxRate.focus();
        return false;
    }
    if(MaxRate.value == '')
    {
        alert('Please Enter a numeric value for Max Rate');
        MaxRate.value = "";
        MaxRate.focus();
        return false;
    }
    return true;
}	

function OnYearChange() //From R4 OnYearChange()
{
    var Year = document.getElementById('Year');
    var bError = "";
    var iYear;
    if(isNaN(iYear = parseInt(Year.value,10)))
    {
        bError = "true";
    }
    else if(iYear <= 999 )
    {
        bError = "true";
    }
    if( Year.value.length != 4 ) 
    {
        bError = "true";
    }
    if(bError == "true")
    {
	alert('Please Enter the Year in format YYYY');
        Year.value = "";
        Year.focus();
        return false;
    }
}

function OnMaxRateChange() //From R4 OnMaxRateChange()
{
    var MaxRate = document.getElementById('MaxRate');
    var dMaxRate = Number(MaxRate.value);

    if(isNaN(dMaxRate))
    {
        alert('Please Enter a numeric value for Max Rate');
        MaxRate.value = "";
        MaxRate.focus();
        return false;
    }
    else if(dMaxRate <= 0)
    {
        alert('Max Rate should be a positive amount');
        MaxRate.value = "";
        MaxRate.focus();
        return false;
    }
    if(MaxRate.value == '')
    {
        alert('Please Enter a numeric value for Max Rate');
        MaxRate.value = "";
        MaxRate.focus();
        return false;
    }
}


function ThirdPartyPayment_onOk()
{
	var sMode = document.getElementById("mode").value;

	if(IsDataChanged()==false)
	{
		window.close();
		return true;
	}
	
	if(ThirdPartyPayment_Validate())
	{
		if( document.getElementById("DeductAgainstPayeeFlag").checked == true )
			document.getElementById("DeductAgainstPayeeFlag").value = "True" ;
		else
			document.getElementById("DeductAgainstPayeeFlag").value = "False" ;
			
		if( document.getElementById("EnclosureFlag").checked == true )
			document.getElementById("EnclosureFlag").value = "True" ;
		else
			document.getElementById("EnclosureFlag").value = "False" ;
		
        window.opener.document.forms[0].SysPostBackAction.value = "thirdpartypayments";
        
        if(sMode == "add")
        {
            window.opener.document.forms[0].ThirdPartyPaymentsGrid_RowAddedFlag.value = "true";
            window.opener.document.forms[0].ThirdPartyPaymentsGrid_RowEditFlag.value = "false";
        }
        else if(sMode == "edit")
        {
            window.opener.document.forms[0].ThirdPartyPaymentsGrid_RowEditFlag.value = "true";
            window.opener.document.forms[0].ThirdPartyPaymentsGrid_RowAddedFlag.value = "false";
        }

        //Added by Amitosh for EFT
        var IsEFTBank = eval("document.forms[0].IsEFTBank");
          var obj = eval("document.forms[0].hasEntityEFtbankInfo");


          if (IsEFTBank != null && IsEFTBank.value == "true" && obj != null && obj.value != "true") {

              var sRetVal = confirm("The entity has no accepted banking information");
              if (!sRetVal)
                  return false;
              else {
                  var width = screen.availWidth - 60;
                  var height = screen.availHeight - 60;
                  if (wnd == null) {
                      var wnd = window.open("../FDM/bankinginfo.aspx?parentid=" + document.getElementById("PayeeEid").value + "&parentFormName=thirdpartypayments", null, 'width=1035,height=335,resizable=no,scrollbars=yes');
                      return false;
                  }

              }
              //end Amitosh
          }
		return true;
	}
	else
	{
		return false;
	}
}

function ThirdPartyPayment_onCancel()
{
	if(IsDataChanged()==true)
	{
		if(ConfirmSave()==true)
		{
			return ThirdPartyPayment_onOk();	
		}
	}
	window.close();
	return true;
}

function ThirdPartyPayment_Validate()
{	
	if(document.forms[0].LastName.value=="")
	{
		alert("Please enter Payee Last Name.");
		tabChange(m_TabList[0].id.substring(4));
		document.forms[0].LastName.focus();
		return false;	
	}
	if (document.forms[0].TransTypeCode_codelookup.disabled == false) {
	    if (document.forms[0].TransTypeCode_codelookup_cid.value == "" || document.forms[0].TransTypeCode_codelookup_cid.value == "0") {
	        alert("Please select Transaction Type.");

	        tabChange(m_TabList[1].id.substring(4));
	        //IC Retrofit for REM Transaction Code , MITS 20093
	        //Commented by asingh264 for MITS# 19821 on 4 March 2010--Reason:Focus can not be set on the objects which is disabled
	        //document.forms[0].TransTypeCode_codelookupbtn.focus();
	        //Comment block ends here
	        return false;
	    }

	    if (parseInt(document.forms[0].TransTypeCode_codelookup_cid.value, 10) <= 0 && document.forms[0].TransTypeCode_codelookup_cid.value == "") {
	        alert("Please select Transaction Type.");
	        tabChange(m_TabList[1].id.substring(4));
	        document.forms[0].TransTypeCode_codelookupbtn.focus();
	        return false;
	    }
	}

	//rupal:start, bob
	var objCarrierclaim = window.opener.document.getElementById("MultipleCoverages");
	if (objCarrierclaim != null && objCarrierclaim.value == "True") {
	    //coverage type wl be visible and mendatory when carrier claim is on
	    if (document.forms[0].Coverage_codelookup_cid != null && ((document.forms[0].Coverage_codelookup_cid.value == "") || (document.forms[0].Coverage_codelookup_cid.value == "0"))) {
	        alert("Please select Coverage.");
	        return false;
	    }
	    if (document.forms[0].Coverage_codelookup_cid != null && parseInt(document.forms[0].Coverage_codelookup_cid.value, 10) <= 0 && document.forms[0].Coverage_codelookup_cid.value == "") {
	        alert("Please select Coverage.");
	        return false;
	    }
	}
	//rupal:end
	
	var sAmount = document.forms[0].Amount.value;
	
	if(sAmount == "" )
	{
		alert("Please enter Transaction Amount.");
		tabChange(m_TabList[1].id.substring(4));
		if (document.getElementsByName("Enable")[0].value == "Enable Percent")
			document.forms[0].Amount.focus();
		else
			document.forms[0].PercentNumber.focus();
		return false;
	}
	
	var iTempIndex = sAmount.indexOf("$");
	
	if(iTempIndex != -1)
	{
	    sAmount = sAmount.replace('$','');
	}

	if(parseFloat(sAmount) <= 0 )
	{
		alert(" Negative or zero payments are not allowed.");
		tabChange(m_TabList[1].id.substring(4));
		if (document.getElementsByName("Enable")[0].value == "Enable Percent")
			document.forms[0].Amount.focus();
		else
			document.forms[0].PercentNumber.focus();
		return false;
	}
	
	if(document.forms[0].ReserveTypeCode_codelookup.value==0 && document.forms[0].ReserveTypeCodeFt_codelookup.value==0) 
	{
		alert("Transaction Type must be linked to a Reserve Type,Cannot Save!");
		return false;
	}
	
	return true;
}

function ThirdPartyPayment_onLoad()
{
    //IC Retrofit for REM Transaction Code , MITS 20093 
   //added by asingh264 on 4 March 2010 MITS#19821 to get the div control in a variable ReserveTypeCodeFt
    var ReserveTypeCodeFt="";
    ReserveTypeCodeFt= eval("document.getElementById('div_ReserveTypeCodeFt')");
    //MITS#19821 ends here
    
	var sMode = document.getElementById("mode").value;
	  //IC Retrofit for REM Transaction Code , MITS 20093
	  //added by asingh264 on 8 March 2010 MITS# 19821- To check if the opener is null or not and thus returns
	if(window.opener.document == null)
		return;
   if (window.opener.document.forms[0] == null)
		return;	
   //ends here
	if (window.opener.document.forms[0].lob != null)
        document.forms[0].lineofbusinesscode.value = window.opener.document.forms[0].lob.value;

    if (window.opener.document.forms[0].claimid != null)
        document.forms[0].claimid.value = window.opener.document.forms[0].claimid.value;
         //IC Retrofit for REM Transaction Code , MITS 20093
         //added by asingh264 MITS#19821 on 4 March 2010- Check for claimid 
            if((document.forms[0].claimid.value == "") || (document.forms[0].claimid.value == "0"))
           {
           
              if(ReserveTypeCodeFt !=null)
             {
               HideUnhideReserveType(ReserveTypeCodeFt);
             }
           
          }
          //MITS#19821 ends here
          //Changed by Gagan for MITS 20093
          
          if (window.opener.document.forms[0].reserveIDDesc != null) 
          {
              if (document.forms[0].ReserveTypeCodeFt_codelookup != null) {
                  document.forms[0].ReserveTypeCodeFt_codelookup.value = window.opener.document.forms[0].reserveIDDesc.value;
                  if(document.forms[0].ReserveTypeCode_codelookup != null)
                  document.forms[0].ReserveTypeCode_codelookup.value = window.opener.document.forms[0].reserveIDDesc.value;
                
            //  if (document.forms[0].ReserveTypeCodeFt$codelookup != null) {
              //    document.forms[0].ReserveTypeCodeFt$codelookup.value = window.opener.document.forms[0].reserveIDDesc.value;
                  if (document.forms[0].ReserveTypeCodeFt_codelookup_cid != null)
                      document.forms[0].ReserveTypeCodeFt_codelookup_cid.value = window.opener.document.forms[0].reserveID.value;
                  if (document.forms[0].ReserveTypeCode_codelookup_cid != null)
                      document.forms[0].ReserveTypeCode_codelookup_cid.value = window.opener.document.forms[0].reserveID.value;              
                  
              }


              else if(document.forms[0].ReserveTypeCode_codelookup!= null) {
              document.forms[0].ReserveTypeCode_codelookup.value = window.opener.document.forms[0].reserveIDDesc.value;

              if (document.forms[0].ReserveTypeCode_codelookup_cid != null)
                  document.forms[0].ReserveTypeCode_codelookup_cid.value =  window.opener.document.forms[0].reserveID.value;

//              else if(document.forms[0].ReserveTypeCode$codelookup != null) {
//              document.forms[0].ReserveTypeCode$codelookup.value = //window.opener.document.forms[0].reserveIDDesc.value;

//              if (document.forms[0].ReserveTypeCode_codelookup != null)
//                  document.forms[0].ReserveTypeCode_codelookup.value = //window.opener.document.forms[0].reserveID.value;
          }
          
          
        }

//      if (window.opener.document.forms[0].PolIDDesc != null) {
//          if (document.forms[0].Policy != null) {
//              var opt = document.createElement("option");
//              window.document.forms[0].Policy.options.add(opt);
//              opt.text = window.opener.document.forms[0].PolIDDesc.value;
//              opt.value = window.opener.document.forms[0].PolID.value;
//           }
//      }
      if (document.forms[0].Coverage_codelookup != null) {
          document.forms[0].Coverage_codelookup.value = window.opener.document.forms[0].CovIDDesc.value;

          if (document.forms[0].Coverage_codelookup_cid != null)
              document.forms[0].Coverage_codelookup_cid.value = window.opener.document.forms[0].CovID.value;
      }
      if (document.forms[0].LossType_codelookup != null) {
          document.forms[0].LossType_codelookup.value = window.opener.document.forms[0].LossDesc.value;

          if (document.forms[0].LossType_codelookup_cid != null)
              document.forms[0].LossType_codelookup_cid.value = window.opener.document.forms[0].LossID.value;
      }
    if (window.opener.document.forms[0].orgEid != null)
        document.forms[0].orgEid.value = window.opener.document.forms[0].orgEid.value;
    //abahl3 rma 111468,11121,11120 starts
    if (window.opener.document.forms[0].RcRowID != null)
        document.forms[0].RcRowID.value = window.opener.document.forms[0].RcRowID.value;
    //abahl3 rma 111468,11121,11120 ends

    //Ankit : Start Policy System Interface
    if (window.opener.document.forms[0].CovgSeqNum != null)
        document.forms[0].CovgSeqNum.value = window.opener.document.forms[0].CovgSeqNum.value;
    //Ankit : End
    //mits 33996
    if (window.opener.document.forms[0].TransSeqNum != null)
        document.forms[0].TransSeqNum.value = window.opener.document.forms[0].TransSeqNum.value;
    //mits 33996
    //Ankit Start : Worked on MITS - 34297
    if (window.opener.document.forms[0].CoverageKey != null && document.forms[0].CoverageKey != null)//JIRA RMA-1166 FDM Issues
        document.forms[0].CoverageKey.value = window.opener.document.forms[0].CoverageKey.value;
    //Ankit End
    if (window.opener.document.forms[0].claimantEIdPassedIn != null)
        document.forms[0].clm_entityid.value = window.opener.document.forms[0].claimantEIdPassedIn.value;    
    if(sMode == "add" )
    {
		document.getElementById("DeductAgainstPayeeFlag").checked = true ;
		document.getElementById("EnclosureFlag").checked = true ;	
	}
	
	//Start by Shivendu for MITS 7953
    if(document.getElementById("PercentNumber").value == "" && document.getElementById("Amount").value == "")
    {
        EnablePercent( true );
    }    
    else
    {
    //End by Shivendu for MITS 7953
	//gagnihotri MITS 15490 04/21/2009
        if( (parseFloat( document.getElementById("PercentNumber").value ) <= 0) || (document.getElementById("PercentNumber").value == "")) 
            EnableAmount( false );
        else
            EnablePercent( false );	
    	
        	
    }
    loadTabList();
    SetReserves();
    var RsvStatusParent = "";
    if (window.opener.document.forms[0].ResStatusParent != null)
        RsvStatusParent = window.opener.document.forms[0].ResStatusParent.value;
    if (document.forms[0].ResTypeStatus != null)
        document.forms[0].ResTypeStatus.value = RsvStatusParent;

    //abahl3 starts 11468,11121,11120 starts
    if ((RsvStatusParent.toUpperCase() == "C") || (document.forms[0].RcRowID.value != "")) {
        //if (ReserveTypeCodeFt != null) {
        //    HideUnhideReserveType(ReserveTypeCodeFt);

        //}
        //ends here

        //rupal:start, r8 first & final payment
        //when the funds form is in readonly mode, these newly aded control for first & final payment should also be readonly as other controsl
        if (document.forms[0].Coverage_codelookup_cid != null) {
            document.forms[0].Coverage_codelookup_cid.readonly = "readonly";
            document.forms[0].Coverage_codelookup_cid.color = "#ffffff";
        }
        if (document.forms[0].Coverage_codelookup != null) {
            document.forms[0].Coverage_codelookup.readOnly = "readonly";
            document.forms[0].Coverage_codelookup.style.backgroundColor = "#F2F2F2";
        }
        if (document.forms[0].Coverage_codelookupbtn != null) {
            //  document.forms[0].Coverage_codelookupbtn.disabled = "true";
            document.forms[0].Coverage_codelookupbtn.style.visibility = "hidden";
        }
        if (document.forms[0].Policy != null)
            document.forms[0].Policy.disabled = true;
        if (document.forms[0].FFPayment != null)
            document.forms[0].FFPayment.disabled = true;
        if (document.forms[0].Unit != null)
            document.forms[0].Unit.disabled = true;
        //rupal:end

        if (document.forms[0].ReserveTypeCodeFt_codelookup_cid != null)
            document.forms[0].ReserveTypeCodeFt_codelookup_cid.readOnly = "readonly";

        if (document.forms[0].ReserveTypeCodeFt_codelookup != null) {
            document.forms[0].ReserveTypeCodeFt_codelookup.readOnly = "readonly";
            document.forms[0].ReserveTypeCodeFt_codelookup.style.backgroundColor = "#F2F2F2";
        }
        if (document.forms[0].ReserveTypeCodeFt_codelookupbtn != null) {
            //document.forms[0].ReserveTypeCodeFt_codelookupbtn.disabled = "true";
            document.forms[0].ReserveTypeCodeFt_codelookupbtn.style.visibility = "hidden";
        }
        if (document.forms[0].reservebalance != null)
            document.forms[0].reservebalance.Enabled = false;
        if (document.forms[0].ReserveTypeCode_codelookup_cid != null)
            document.forms[0].ReserveTypeCode_codelookup_cid.readOnly = "readonly";

        if (document.forms[0].ReserveTypeCode_codelookup != null) {
            document.forms[0].ReserveTypeCode_codelookup.readOnly = "readonly";
            document.forms[0].ReserveTypeCode_codelookup.style.backgroundColor = "#F2F2F2";
        }
        if (document.forms[0].ReserveTypeCode_codelookupbtn != null) {
            //document.forms[0].ReserveTypeCode_codelookupbtn.disabled = "true";
            document.forms[0].ReserveTypeCode_codelookupbtn.style.visibility = "hidden";
        }

        //MITS 32397 start: LossType/ DisabilityCategory/ DisabilityLossType should be disabled for Void/Printed transactions   
        if (document.forms[0].LossType_codelookup != null) {
            document.forms[0].LossType_codelookup.readOnly = "readonly";
            document.forms[0].LossType_codelookup.style.backgroundColor = "#F2F2F2";

            if (document.forms[0].LossType_codelookupbtn != null) {
                //document.forms[0].LossType_codelookupbtn.disabled = "true";
                document.forms[0].LossType_codelookupbtn.style.visibility = "hidden";
            }
        }
        if (document.forms[0].DisabilityCat_codelookup != null) {
            document.forms[0].DisabilityCat_codelookup.readOnly = "readonly";
            document.forms[0].DisabilityCat_codelookup.style.backgroundColor = "#F2F2F2";
            if (document.forms[0].DisabilityCat_codelookupbtn != null)
                document.forms[0].DisabilityCat_codelookupbtn.style.visibility = "hidden";
        }
        if (document.forms[0].DisabilityLossType_codelookup != null) {
            document.forms[0].DisabilityLossType_codelookup.readOnly = "readonly";
            document.forms[0].DisabilityLossType_codelookup.style.backgroundColor = "#F2F2F2";

            if (document.forms[0].DisabilityLossType_codelookupbtn != null) {
                document.forms[0].DisabilityLossType_codelookupbtn.disabled = "true";
                document.forms[0].DisabilityLossType_codelookupbtn.style.visibility = "hidden";
            }
        }
        //MITS 32397 end
    }

}
//abahl3 starts 11468,11121,11120 ends


function EnablePercent( bResetValues )
{
     //MGaba2:MITS 16062:Value of percent/amount was not getting fetched from popup to parent screen
    //document.getElementById("PercentNumber").disabled = false ;
    document.getElementById("PercentNumber").readOnly = "";
	if( bResetValues )
	{
		document.getElementById("PercentNumber").value = "0" ;
		document.getElementById("Amount").value = "0" ;	
	}
    //MGaba2:MITS 16062:Value of percent/amount was not getting fetched from popup to parent screen
	//document.getElementById("Amount").disabled = true ;
	document.getElementById("Amount").readOnly = "readonly";
    //gagnihotri MITS 15472 04/21/2009
	document.getElementById("lbl_Amount").className = " label";
	document.getElementById("lbl_PercentNumber").className = " required";
	//document.getElementById("lbl_Amount").setAttribute("className", "label") ;
	//document.getElementById("lbl_PercentNumber").setAttribute("className", "required") ;
	document.getElementsByName("Enable")[0].value = "Enable Amount";
	
}

function EnableAmount( bResetValues )
{
    //MGaba2:MITS 16062:Value of percent/amount was not getting fetched from popup to parent screen
    //document.getElementById("PercentNumber").disabled = true ;
    document.getElementById("PercentNumber").readOnly = "readonly";
	if( bResetValues )
	{
		document.getElementById("PercentNumber").value = "0" ;
		document.getElementById("Amount").value = "0" ;	
	}
    //MGaba2:MITS 16062:Value of percent/amount was not getting fetched from popup to parent screen
	//document.getElementById("Amount").disabled = false ;
	document.getElementById("Amount").readOnly = "";
    //gagnihotri MITS 15472 04/21/2009
	document.getElementById("lbl_Amount").className = " required";
	document.getElementById("lbl_PercentNumber").className = " label";
	//document.getElementById("lbl_Amount").setAttribute("className", "required");
	//document.getElementById("lbl_PercentNumber").setAttribute("className", "label");
	document.getElementsByName("Enable")[0].value = "Enable Percent";
	
}

function fnEnable()
{
	if( document.getElementsByName("Enable")[0].value == "Enable Percent" )
	{
		EnablePercent( true );
		//gagnihotri MITS 15472 04/21/2009
		//Added by Shivendu for MITS 7953
		//MakePercentRequired();
	}
	else
	{
		EnableAmount( true );
		//gagnihotri MITS 15472 04/21/2009
		//Added by Shivendu for MITS 7953
		//MakeAmountRequired();
	}
}

//Start by Shivendu for MITS 7953
function MakePercentRequired()
{

    
    var objElts = document.getElementsByTagName("B"); 
	for(i=0;i<objElts.length;i++)
	  {
		
        
        if(objElts[i].innerHTML == '<u>Amount:</u>' || objElts[i].innerHTML == '<U>Amount:</U>')
        {
         objElts[i].parentNode.innerHTML = "Amount:";
        
        } 
        
	  }
	  var objElts = document.getElementsByTagName("TD");
	  for(i=0;i<objElts.length;i++)
	  {
		
        if(objElts[i].innerHTML == 'Percent:')
        {
         objElts[i].innerHTML = '<B><U>Percent:</U></B>';
        
        } 
        
	  }
}

function MakeAmountRequired()
{
    var objElts = document.getElementsByTagName("B"); 
	for(i=0;i<objElts.length;i++)
	  {
		
        if(objElts[i].innerHTML == '<u>Percent:</u>' || objElts[i].innerHTML == '<U>Percent:</U>')
        {
         objElts[i].parentNode.innerHTML = "Percent:";
        
        } 
        
	  }
	  var objElts = document.getElementsByTagName("TD");
	  for(i=0;i<objElts.length;i++)
	  {
		
        if(objElts[i].innerHTML == 'Amount:')
        {
         objElts[i].innerHTML = '<B><U>Amount:</U></B>';
        
        } 
        
	  }
}
//End by Shivendu for MITS 7953
function BillingRule_onOk() {
    var bResult = false;

//    if (IsDataChanged() == false) {   Commented by csingh7 : MITS 15326
//        window.close();
//        return true;
//    }
    if (BillingRule_Validate()) {
        return true;
    }
    else {
        return false;
    }
}
function BillingRule_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (BillingRule_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function BillingRule_Validate()  //from R4 ValidateBilling()
{   
   var noOfDays=0;
    //Changed by Gagan Bhatnagar for MITS 9268 : Start
    var CancelPendingDiaries;
    var CancelDiary;
    CancelDiary = document.forms[0].CancelDiary_codelookup.value.substring(0,1);    
    //Changed by Gagan Bhatnagar for MITS 9268 : End
    if (trim(document.forms[0].BillingRuleCode_codelookup_cid.value)=="" || trim(document.forms[0].BillingRuleCode_codelookup.value)=="")
	{
		alert("Please enter a billing rule code.");
		document.forms[0].BillingRuleCode_codelookup.focus();
		return false;
	}
	if (trim(document.forms[0].NoticeOffsetInd_codelookup_cid.value)=="" || trim(document.forms[0].NoticeOffsetInd_codelookup.value)=="")
	{
		alert("Please indicate what to offset the notice from.");
		document.forms[0].NoticeOffsetInd_codelookup.focus();
		return false;
	}	
	if (trim(document.forms[0].CancelPendDiary_codelookup_cid.value)=="" || trim(document.forms[0].CancelPendDiary_codelookup.value)=="")
	{
		alert("Please indicate when the cancel pending diary should be invoked.");
		document.forms[0].CancelPendDiary_codelookup.focus();
		return false;
	}
	if (trim(document.forms[0].CancelDiary_codelookup_cid.value)=="" || trim(document.forms[0].CancelDiary_codelookup.value)=="")
	{
		alert("Please indicate when the cancellation diary should be invoked.");
		document.forms[0].CancelDiary_codelookup.focus();
		return false;
	}	
	//Changed by Gagan Bhatnagar for MITS 9268 : Start
	if (trim(document.forms[0].FirstNoticeDays.value)=="" )
	{
		alert("Please enter the number of days for first notice generation.");
		document.forms[0].FirstNoticeDays.focus();
		return false;
	}
	if (trim(document.forms[0].FirstNoticeDays.value) < 0 )
	{
		alert("Please enter a positive value for first notice generation days.");
		document.forms[0].FirstNoticeDays.focus();
		return false;
	}	
	
	//if (trim(document.forms[0].CancelDiary_cid.value)=="2" || trim(document.forms[0].CancelDiary_cid.value)=="3" || trim(document.forms[0].CancelDiary_cid.value)=="4")
	if (CancelDiary == "2" || CancelDiary=="3" || CancelDiary=="4")
	{	
	//Changed by Gagan Bhatnagar for MITS 9268 : End
		if (trim(document.forms[0].SecondNoticeDays.value)=="")
		{
			alert("Please enter the number of days for second notice generation.");
			document.forms[0].SecondNoticeDays.focus();
			return false;
		}
	}
	//Changed by Gagan Bhatnagar for MITS 9268 : Start
	if (trim(document.forms[0].SecondNoticeDays.value) < 0 )
	{
		alert("Please enter a positive value for second notice generation days.");
		document.forms[0].SecondNoticeDays.focus();
		return false;
	}		
	//if (trim(document.forms[0].CancelDiary_cid.value)=="3" || trim(document.forms[0].CancelDiary_cid.value)=="4")
	if (CancelDiary=="3" || CancelDiary=="4")	
	//Changed by Gagan Bhatnagar for MITS 9268 : End
	{
		if (trim(document.forms[0].ThirdNoticeDays.value)=="")
		{
			alert("Please enter the number of days for third notice generation.");
			document.forms[0].ThirdNoticeDays.focus();
			return false;
		}
	}
	//Changed by Gagan Bhatnagar for MITS 9268 : Start
	if (trim(document.forms[0].ThirdNoticeDays.value) < 0 )
	{
		alert("Please enter a positive value for third notice generation days.");
		document.forms[0].ThirdNoticeDays.focus();
		return false;
	}	
	//if (trim(document.forms[0].CancelDiary_cid.value)=="4")
	if (CancelDiary == "4")	
	//Changed by Gagan Bhatnagar for MITS 9268 : End
	{
		if (trim(document.forms[0].FourthNoticeDays.value)=="")
		{
			alert("Please enter the number of days for fourth notice generation.");
			document.forms[0].FourthNoticeDays.focus();
			return false;
		}
	}
	//Changed by Gagan Bhatnagar for MITS 9268 : Start
	if (trim(document.forms[0].FourthNoticeDays.value) < 0 )
	{
		alert("Please enter a positive value for fourth notice generation days.");
		document.forms[0].FourthNoticeDays.focus();
		return false;
	}	
	//Changed by Gagan Bhatnagar for MITS 9268 : End
	arr=document.forms[0].CancelDiary_codelookup.value.split(' ');
	noOfDays=arr[0];
	noOfDays=noOfDays.substring(0,1);
	if (trim(noOfDays)!="1" && trim(noOfDays)!="2" && trim(noOfDays)!="3" && trim(noOfDays)!="4")
	{
	   alert("Please indicate when the cancellation diary should be invoked.");
	   document.forms[0].CancelDiary_codelookup.focus();
	   return false;
	}
	//Changed by Gagan Bhatnagar for MITS 9268 : Start
	if (trim(noOfDays)=="1" &&(!trim(document.forms[0].SecondNoticeDays.value)==""||!trim(document.forms[0].FourthNoticeDays.value)==""||!trim(document.forms[0].ThirdNoticeDays.value)==""))
	{
		alert("Cannot enter for second, third and fourth notice generation days.");
		document.forms[0].SecondNoticeDays.focus();
		return false;
	}	
	if (trim(noOfDays)=="2" &&(!trim(document.forms[0].FourthNoticeDays.value)==""||!trim(document.forms[0].ThirdNoticeDays.value)==""))
	{
		alert("Cannot enter for third and fourth notice generation days.");
		document.forms[0].FirstNoticeDays.focus();
		return false;
	}
	if (trim(noOfDays)=="3" &&(!trim(document.forms[0].FourthNoticeDays.value)==""))
	{
		alert("Cannot enter for fourth notice generation days.");
		document.forms[0].FirstNoticeDays.focus();
		return false;
    }

    //Changed by gagan for MITS 12466 : start        Added by csingh7 : As per MITS 12578
    if ((!trim(document.forms[0].FirstNoticeDays.value)) == "" && (!trim(document.forms[0].SecondNoticeDays.value)) == "") {
        if (parseInt(trim(document.forms[0].FirstNoticeDays.value)) > parseInt(trim(document.forms[0].SecondNoticeDays.value))) {
            alert("Second Notice days must be greater than equal to First Notice days.");
            document.forms[0].SecondNoticeDays.focus();
            return false;
        }
    }

    if ((!trim(document.forms[0].ThirdNoticeDays.value)) == "" && (!trim(document.forms[0].SecondNoticeDays.value)) == "") {
        if (parseInt(trim(document.forms[0].SecondNoticeDays.value)) > parseInt(trim(document.forms[0].ThirdNoticeDays.value))) {
            alert("Third Notice days must be greater than equal to Second Notice days.");
            document.forms[0].ThirdNoticeDays.focus();
            return false;
        }
    }

    if ((!trim(document.forms[0].ThirdNoticeDays.value)) == "" && (!trim(document.forms[0].FourthNoticeDays.value)) == "") {
        if (parseInt(trim(document.forms[0].ThirdNoticeDays.value)) > parseInt(trim(document.forms[0].FourthNoticeDays.value))) {
            alert("Fourth Notice days must be greater than equal to Third Notice days.");
            document.forms[0].FourthNoticeDays.focus();
            return false;
        }
    }
    //Changed by gagan for MITS 12466 : end
	
	/*
	if (trim(noOfDays)=="1" &&(!trim(document.forms[0].SecondNoticeDays.value)==""||!trim(document.forms[0].FourthNoticeDays.value)==""||!trim(document.forms[0].ThirdNoticeDays.value)==""))
	{
		alert("Cannot enter for second, third and fourth notice generation days.");
		document.forms[0].SecondNoticeDays.focus();
		return false;
	}
	
	if (trim(noOfDays)=="2" &&(!trim(document.forms[0].FirstNoticeDays.value)==""||!trim(document.forms[0].FourthNoticeDays.value)==""||!trim(document.forms[0].ThirdNoticeDays.value)==""))
	{
		alert("Cannot enter for first, third and fourth notice generation days.");
		document.forms[0].FirstNoticeDays.focus();
		return false;
	}
	if (trim(noOfDays)=="3" &&(!trim(document.forms[0].FirstNoticeDays.value)==""||!trim(document.forms[0].FourthNoticeDays.value)==""||!trim(document.forms[0].SecondNoticeDays.value)==""))
	{
		alert("Cannot enter for first, second and fourth notice generation days.");
		document.forms[0].FirstNoticeDays.focus();
		return false;
	}
	if (trim(noOfDays)=="4" &&(!trim(document.forms[0].FirstNoticeDays.value)==""||!trim(document.forms[0].ThirdNoticeDays.value)==""||!trim(document.forms[0].SecondNoticeDays.value)==""))
	{
		alert("Cannot enter for first, second, and third notice generation days.");
		document.forms[0].FirstNoticeDays.focus();
		return false;
	}
	*/
	//Changed by Gagan Bhatnagar for MITS 9268 : End
	if (trim(document.forms[0].OutstandBalIndic_codelookup_cid.value)=="" || trim(document.forms[0].OutstandBalIndic_codelookup.value)=="")
	{
		alert("Please enter the additional premium indicator.");
		document.forms[0].OutstandBalIndic_codelookup.focus();
		return false;
	}
	if (trim(document.forms[0].NonPayInd_codelookup_cid.value)=="" || trim(document.forms[0].NonPayInd_codelookup.value)=="")
	{
		alert("Please enter non payment indicator.");
		document.forms[0].NonPayInd_codelookup.focus();
		return false;
	}
	if (trim(document.forms[0].MaxNumRebills.value)=="")
	{
		alert("Please enter maximum number of rebills..");
		document.forms[0].MaxNumRebills.focus();
		return false;
	}
	if (trim(document.forms[0].DueDays.value)=="")
	{
		alert("Please enter due days.");
		document.forms[0].DueDays.focus();
		return false;
	}
	return true;
}

function trim(svalue)
{
  svalue = svalue.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
  return svalue;
}
function BRPayPlan_onOk() {
    var bResult = false;

    //    if (IsDataChanged() == false) {   Commented by csingh7 : MITS 15326
    //        window.close();
    //        return true;
    //    }
    if (BRPayPlan_Validate()) {
        return true;
    }
    else {
        return false;
    }
}
function BRPayPlan_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (BRPayPlan_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function BRPayPlan_Validate() //From R4 ValidateBRPayPlan() 
{
	if (trim(document.forms[0].PayPlanCode_codelookup_cid.value)=="" || trim(document.forms[0].PayPlanCode_codelookup_cid.value)=="0")
	{
		alert("Please enter a Pay plan code.");
		document.forms[0].PayPlanCode_codelookup.focus();
		return false;
	}
	
	if (trim(document.forms[0].billingrulecode_cid.value)=="" || trim(document.forms[0].billingrulecode_cid.value)=="0")
	{
		alert("Please enter a Billing Rule for the pay plan.");
		document.forms[0].billingrulecode.focus();
		return false;
	}
	
	if (trim(document.forms[0].LOB_codelookup_cid.value)=="" || trim(document.forms[0].LOB_codelookup_cid.value)=="0")
	{
		alert("Please enter lob.");
		document.forms[0].LOB_codelookup.focus();
		return false;
	}
	
	if (trim(document.forms[0].State_codelookup_cid.value)=="" || trim(document.forms[0].State_codelookup_cid.value)=="0")
	{
		alert("Please enter state for the pay plan.");
		document.forms[0].State_codelookup.focus();
		return false;
	}
	if (trim(document.forms[0].LateIndicator_codelookup_cid.value)=="" || trim(document.forms[0].LateIndicator_codelookup_cid.value)=="0")
	{
		alert("Please enter late start indicator.");
		document.forms[0].LateIndicator_codelookup.focus();
		return false;
	}
	if (trim(document.forms[0].ApplyAddPremiumto_codelookup_cid.value)=="" || trim(document.forms[0].ApplyAddPremiumto_codelookup_cid.value)=="0")
	{
		alert("Please enter premium adjustment indicator.");
		document.forms[0].ApplyAddPremiumto_codelookup.focus();
		return false;
	}
	if (trim(document.forms[0].InstGenType_codelookup_cid.value)=="" || trim(document.forms[0].InstGenType_codelookup_cid.value)=="0")
	{
		alert("Please enter installment generation type.");
		document.forms[0].InstGenType_codelookup.focus();
		return false;
	}
	
	if (trim(document.forms[0].OffsetFrom_codelookup_cid.value)=="" || trim(document.forms[0].OffsetFrom_codelookup_cid.value)=="0")
	{
		alert("Please enter offset indicator.");
		document.forms[0].OffsetFrom_codelookup.focus();
		return false;
	}
	str=document.forms[0].InstGenType_codelookup.value.substring(0,1);
	if (trim(str)=="C")
	{
		if (trim(document.forms[0].Install1.value)=="" || trim(document.forms[0].Install1.value)=="0")
		{
			alert("Please enter installment amount.");
			document.forms[0].Install1.focus();
			return false;
		}	
		if (trim(document.forms[0].InstallType1_codelookup.value)=="" || trim(document.forms[0].InstallType1_codelookup.value)=="0")
		{
	        alert("Please enter installment type.");
	        document.forms[0].InstallType1_codelookup.focus();
	        return false;
	    }
	    //npadhy RMSC retrofit Starts
	    if (trim(document.forms[0].GenLagDays1.value) == "" && document.forms[0].GenLagDays1.disabled == false) {
	        alert("Please enter generation lag days.");
	        document.forms[0].GenLagDays1.focus();
	        return false;
	    }
	    if (trim(document.forms[0].DueDays1.value) == "" && document.forms[0].DueDays1.disabled == false)
	    {
	        alert("Please enter due days.");
	        document.forms[0].DueDays1.focus();
	        return false;
	    }
	    if (trim(document.forms[0].GenDate1.value) == "" && document.forms[0].GenDate1.disabled == false) {
	        alert("Please enter generation date.");
	        document.forms[0].GenDate1.focus();
	        return false;
	    }
	    if (trim(document.forms[0].DueDate1.value) == "" && document.forms[0].DueDate1.disabled == false) {
	        alert("Please enter due date.");
	        document.forms[0].DueDate1.focus();
	        return false;
	    }
	    //npadhy RMSC retrofit Ends
	    for (i = 2; i <= 12; i++) {
	        ctrl1 = document.getElementById('Install' + i);
	        ctrl2 = document.getElementById('InstallType' + i + '_codelookup');
	        ctrl3 = document.getElementById('GenLagDays' + i);
	        ctrl4 = document.getElementById('DueDays' + i);
	        //npadhy RMSC retrofit Starts
	        ctrl5 = document.getElementById('GenDate' + i);
	        ctrl6 = document.getElementById('DueDate' + i);
	        //npadhy RMSC retrofit Ends
	        if (trim(ctrl1.value) != "" && trim(ctrl1.value) != "0") {
	            ctrlPrior = document.getElementById('Install' + (i - 1))
	            if (trim(ctrlPrior.value) == "" || trim(ctrlPrior.value) == "0") {
	                alert("Please enter prior installment amount.");
	                ctrlPrior.focus();
	                return false;
	            }

	            if (trim(ctrl2.value) == "" || trim(ctrl2.value) == "0") {
	                alert("Please enter installment type.");
	                ctrl2.focus();
	                return false;
	            }
	            //npadhy RMSC retrofit Starts
	            if (trim(ctrl3.value) == "" && ctrl3.disabled == false) {
	                alert("Please enter generation lag days.");
	                ctrl3.focus();
	                return false;
	            }
	            if (trim(ctrl4.value) == "" && ctrl4.disabled == false) {
	                alert("Please enter due days.");
	                ctrl4.focus();
	                return false;
	            }
	            if (trim(ctrl5.value) == "" && ctrl5.disabled == false) {
	                alert("Please enter generation date.");
	                ctrl5.focus();
	                return false;
	            }
	            if (trim(ctrl6.value) == "" && ctrl6.disabled == false) {
	                alert("Please enter due date.");
	                ctrl6.focus();
	                return false;
	            }
	            //npadhy RMSC retrofit Ends
	        }
	        //Shruti
	        if (trim(ctrl2.value) != "" && trim(ctrl2.value) != "0" && trim(ctrl2.value) != " ") {
	            ctrlPrior = document.getElementById('GenLagDays' + (i - 1))
	            //npadhy RMSC retrofit Starts
	            if (trim(ctrlPrior.value) == "" && ctrlPrior.disabled == false) {
	                alert("Please enter prior generation lag days");
	                ctrlPrior.focus();
	                return false;
	            }
	            if (trim(ctrl1.value) == "" || trim(ctrl1.value) == "0") {
	                alert("Please enter installment amount.");
	                ctrl1.focus();
	                return false;
	            }
	            if (trim(ctrl3.value) == "" && ctrl3.disabled == false) {
	                alert("Please enter generation lag days.");
	                ctrl3.focus();
	                return false;
	            }
	            if (trim(ctrl4.value) == "" && ctrl4.disabled == false) {
	                alert("Please enter due days.");
	                ctrl4.focus();
	                return false;
	            }
	            if (trim(ctrl5.value) == "" && ctrl5.disabled == false) {
	                alert("Please enter generation date.");
	                ctrl5.focus();
	                return false;
	            }
	            if (trim(ctrl6.value) == "" && ctrl6.disabled == false) {
	                alert("Please enter due date.");
	                ctrl5.focus();
	                return false;
	            }
	            //npadhy RMSC retrofit Ends
	        }
	        if (trim(ctrl3.value) != "" && trim(ctrl3.value) != "0") {
	            ctrlPrior = document.getElementById('InstallType' + (i - 1) + '_codelookup')
	            if (trim(ctrlPrior.value) == "" || trim(ctrlPrior.value) == "0") {
	                alert("Please enter prior installment type.");
	                ctrlPrior.focus();
	                return false;
	            }
	            if (trim(ctrl1.value) == "" || trim(ctrl1.value) == "0") {
	                alert("Please enter installment amount.");
	                ctrl1.focus();
	                return false;
	            }
	            if (trim(ctrl2.value) == "" || trim(ctrl2.value) == "0") {
	                alert("Please enter installment type.");
	                ctrl2.focus();
	                return false;
	            }
	            if (trim(ctrl4.value) == "") {
	                alert("Please enter due days.");
	                ctrl4.focus();
	                return false;
	            }
	        }
	        if (trim(ctrl4.value) != "" && trim(ctrl4.value) != "0") {
	            ctrlPrior = document.getElementById('DueDays' + (i - 1))
	            if (trim(ctrlPrior.value) == "") {
	                alert("Please enter prior due days");
	                ctrlPrior.focus();
	                return false;
	            }
	            if (trim(ctrl1.value) == "" || trim(ctrl1.value) == "0") {
	                alert("Please enter installment amount.");
	                ctrl1.focus();
	                return false;
	            }

	            if (trim(ctrl2.value) == "" || trim(ctrl2.value) == "0") {
	                alert("Please enter installment type.");
	                ctrl2.focus();
	                return false;
	            }
	            if (trim(ctrl3.value) == "") {
	                alert("Please enter generation lag days.");
	                ctrl3.focus();
	                return false;
	            }
	        }
	        //Animesh Inserted RMSC 
	        if (trim(ctrl5.value) != "" && trim(ctrl5.value) != "0") {
	            ctrlPrior = document.getElementById('GenDate' + (i - 1))
	            if (trim(ctrlPrior.value) == "") {
	                alert("Please enter prior generation date.");
	                ctrlPrior.focus();
	                return false;
	            }
	            if (trim(ctrl1.value) == "" || trim(ctrl1.value) == "0") {
	                alert("Please enter installment amount.");
	                ctrl1.focus();
	                return false;
	            }

	            if (trim(ctrl2.value) == "" || trim(ctrl2.value) == "0") {
	                alert("Please enter installment type.");
	                ctrl2.focus();
	                return false;
	            }
	            if (trim(ctrl6.value) == "") {
	                alert("Please enter due date.");
	                ctrl6.focus();
	                return false;
	            }
	        }
	        //npadhy RMSC retrofit Starts
	        if (trim(ctrl6.value) != "" && trim(ctrl5.value) != "0") {
	            ctrlPrior = document.getElementById('DueDate' + (i - 1))
	            if (trim(ctrlPrior.value) == "") {
	                alert("Please enter prior due date.");
	                ctrlPrior.focus();
	                return false;
	            }
	            if (trim(ctrl1.value) == "" || trim(ctrl1.value) == "0") {
	                alert("Please enter installment amount.");
	                ctrl1.focus();
	                return false;
	            }

	            if (trim(ctrl2.value) == "" || trim(ctrl2.value) == "0") {
	                alert("Please enter installment type.");
	                ctrl2.focus();
	                return false;
	            }
	            if (trim(ctrl5.value) == "") {
	                alert("Please enter generation date.");
	                ctrl6.focus();
	                return false;
	            }
	        }
	        //npadhy RMSC retrofit Ends
	    }
	}
	else 
	{
	    if (trim(document.forms[0].Frequency_codelookup_cid.value) == "" || trim(document.forms[0].Frequency_codelookup_cid.value) == "0") 
	    {
	        alert("Please enter frequency.");
	        document.forms[0].Frequency_codelookup.focus();
	        return false;
	    }
	    //npadhy RMSC retrofit Starts
	    if ((trim(document.forms[0].GenDay_codelookup_cid.value) == "" || trim(document.forms[0].GenDay_codelookup_cid.value) == "0") && document.forms[0].GenDay_codelookup.disabled == false) 
	    {
	        alert("Please enter generation day.");
	        document.forms[0].GenDay_codelookup.focus();
	        return false;
	    }
	    if (trim(document.forms[0].DueDays.value) == "" && document.forms[0].DueDays.disabled == false) 
	    {
	        alert("Please enter generation due days.");
	        document.forms[0].DueDays.focus();
	        return false;
	    }
	    if (trim(document.forms[0].GenDate.value) == "" && document.forms[0].GenDate.disabled == false) 
	    {
	        alert("Please enter generation date.");
	        document.forms[0].GenDate.focus();
	        return false;
	    }
	    if (trim(document.forms[0].DueDate.value) == "" && document.forms[0].DueDate.disabled == false) 
	    {
	        alert("Please enter due date.");
	        document.forms[0].DueDate.focus();
	        return false;
	    }
	    //npadhy RMSC retrofit Ends
	    if (trim(document.forms[0].Install1.value) == "" || trim(document.forms[0].Install1.value) == "0") 
	    {
	        alert("Please enter installment amount.");
	        document.forms[0].Install1.focus();
	        return false;
	    }
	    if (trim(document.forms[0].InstallType1_codelookup.value) == "" || trim(document.forms[0].InstallType1_codelookup.value) == "0") 
	    {
	        alert("Please enter installment type.");
	        document.forms[0].InstallType1_codelookup.focus();
	        return false;
	    }
	    for (i = 2; i <= 12; i++) 
	    {
	        ctrl1 = document.getElementById('Install' + i);
	        ctrl2 = document.getElementById('InstallType' + i + '_codelookup');
	        if (trim(ctrl1.value) != "" && trim(ctrl1.value) != "0") 
	        {
	            ctrlPrior = document.getElementById('Install' + (i - 1))
	            if (trim(ctrlPrior.value) == "" || trim(ctrlPrior.value) == "0") 
	            {
	                alert("Please enter prior install amount.");
	                ctrlPrior.focus();
	                return false;
	            }
	            if (trim(ctrl2.value) == "" || trim(ctrl2.value) == "0") 
	            {
	                alert("Please enter installment type.");
	                ctrl2.focus();
	                return false;
	            }
	        }
	        //Shruti
	        if (trim(ctrl2.value) != "" && ctrl2.value != " ") 
	        {
	            ctrlPrior = document.getElementById('InstallType' + (i - 1) + '_codelookup')
	            if (trim(ctrlPrior.value) == "" || trim(ctrlPrior.value) == "0") 
	            {
	                alert("Please enter prior install type.");
	                ctrlPrior.focus();
	                return false;
	            }
	            if (trim(ctrl1.value) == "" || trim(ctrl1.value) == "0") 
	            {
	                alert("Please enter installment amount.");
	                ctrl1.focus();
	                return false;
	            }
	        }

	    }
	}
	return true;	
}
function EarlyPayDiscounts_onOk() {
    var bResult = false;

    if (IsDataChanged() == false) {
        window.close();
        return true;
    }
    if (EarlyPayDiscounts_Validate()) {
        return true;
    }
    else {
        return false;
    }
}
function EarlyPayDiscounts_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (EarlyPayDiscounts_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function  EarlyPayDiscounts_Validate() //From R4 ValidateEarlyPayDiscounts()
{
    if (trim(document.forms[0].LOB_codelookup_cid.value)=="" || trim(document.forms[0].LOB_codelookup.value)=="")
	{
		alert("Please enter a line of business.");
		document.forms[0].LOB_codelookup.focus();
		return false;
	}
	if (trim(document.forms[0].Amount.value)=="")
	{
		alert("Please enter in discount amount.");
		document.forms[0].Amount.focus();
		return false;
	}
	if (trim(document.forms[0].DollarOrPercent_codelookup_cid.value)=="" || trim(document.forms[0].DollarOrPercent_codelookup.value)=="")
	{
		alert("Please indicate if discount amount is a dollar or percent.");
		document.forms[0].DollarOrPercent_codelookup.focus();
		return false;
	}
	if (trim(document.forms[0].DeadlineEligibilDate.value)=="")
	{
		alert("Please enter deadline eligibilty date for the discount.");
		document.forms[0].DeadlineEligibilDate.focus();
		return false;
	}
	return true;
}
function States_onOk() {
    var bResult = false;

    if (IsDataChanged() == false) {
        window.close();
        return true;
    }
    if (States_Validate()) {
        return true;
    }
    else {
        return false;
    }
}
function States_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (States_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function States_Validate() //From R4 OnStatesSave()
{

    if (trim(document.forms[0].StateAb.value) == "") {
        alert("Please enter State Abbrv.");
        document.forms[0].StateAb.focus();
        return false;
    }
    else {
        frm = window.opener.document.forms[0];
        ctrlStateAbLoad = document.getElementById("StateAbLoadvalue"); //ctrlStateAbLoad would be empty for Mode="add"
        for (i = 2; i <= frm.elements.length; i++) {
            if (i < 10)
                i = '0' + i;
            var ctrl = window.opener.document.getElementById("StatesGrid_gvData_ctl" + i + "_hfGrid");
            if (ctrl != null) {
                if (ctrlStateAbLoad.value.toUpperCase() != trim(document.forms[0].StateAb.value.toUpperCase()))
                 {
                     if (ctrl.value.toUpperCase() == trim(document.forms[0].StateAb.value.toUpperCase())) 
                    {
                        alert("This State abbreviation is already in use.Please select a unique state abbreviation.");
                        return false;
                    }
                }
            }

        }
    }

    if (trim(document.forms[0].StateName.value) == "") {
        alert("Please enter State Name.");
        document.forms[0].StateName.focus();
        return false;
    }
    //Umesh  MITS_7124 and MITS_7125
    /*if (trim(document.forms[0].Froi.value)=="")
    {
    alert("Please enter FROI.");
    document.forms[0].Froi.focus();
    return false;
    }*/

    if (document.forms[0].Froi.value != "") {
        if (isNaN(parseInt(document.forms[0].Froi.value))) {
            alert("Please enter numeric value in FROI");
            document.forms[0].Froi.focus();
            return false;
        }
        //start by Nitin for MITS 12175
        if (parseInt(document.forms[0].Froi.value) < 0) {
            alert("Please enter a non negetive numeric value in FROI");
            document.forms[0].Froi.focus();
            return false;
        }
        //end by Nitin for MITS 12175
    }
    //Umesh  MITS_7124 and MITS_7125
    //    document.forms[0].FormMode.value = "close";
    document.forms[0].StateAbLoadvalue.value = "";
    return true;
}
//rupal:start, 8 sep 2011, for r8 unclaimed and escheat enh
function FundsDormancy_onOk() {
    
    var bResult = false;

    if (IsDataChanged() == false) {
        window.close();
        return true;
    }
    if (FundsDormancy_Validate()) {
        return true;
    }
    else {
        return false;
    }
}

function FundsDormancy_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (FundsDormancy_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
  }

  function FundsDormancy_Validate() {
    //check 1: jurisdtion must be defined
    var JurisdictionId = document.forms[0].Jurisdiction_codelookup_cid.value;

    if (trim(JurisdictionId) == "" || trim(JurisdictionId) == "0") {
        alert("Please enter Jurisdiction.");
        document.forms[0].Jurisdiction_codelookup.focus();
        return false;
    }
    else 
    {
        frm = window.opener.document.forms[0];
        ctrlJurisdictionIdLoad = document.getElementById("JurisdictionIdLoadvalue"); //ctrlStateAbLoad would be empty for Mode="add"
        for (i = 0; i <= frm.elements.length; i++) {
            //if (i < 10)
              //  i = '0' + i;
            //var ctrl = window.opener.document.getElementById("FundsDormancygrid_gvData_ctl" + i + "_hfGrid");
            var ctrl = window.opener.document.getElementById("FundsDormancygrid_gvData_hfGrid_" + i); //JIRA RMA-1155
            if (ctrl != null) 
            {
                //tkatsarski: 11/28/14 - RMA-4878: Added check if ctrlJurisdictionIdLoad is null
                if (ctrlJurisdictionIdLoad != null)
                {
                    if (ctrlJurisdictionIdLoad.value != JurisdictionId) //in edit mode we have to make exception 
                    {
                        if (JurisdictionId == ctrl.value) {
                            alert("Dormancy period for this state is already defined. Please select other state.");
                            return false;
                        }
                    } //end if (ctrlJurisdictionIdLoad.value != JurisdictionId) 
                }
            } //end if (ctrl != null)
        } //end for loop
    }//end else
    
    //check 2: Either Unclaimed or Escheat Dormancy must be defined
    //zero days can not be defined as dormancy period. Zero days means no dormancy period is defined.
     var bUnclaimDormancyDefined = true;
     var bEscheatdormancyDefined = true;
     //Ankit Start : Worked on MITS - 31242
     var bReturn = true;
     //Ankit End

     if (trim(document.forms[0].UnclaimedDays.value) == "" || trim(document.forms[0].UnclaimedDays.value) == "0") {
         bUnclaimDormancyDefined = false; //no days defined for unclaimed dormancy
     }
     if (trim(document.forms[0].EscheatDays.value) == "" || trim(document.forms[0].EscheatDays.value) == "0") {
         document.forms[0].EscheatDays.value = "0"; //set the value to zero in case empty
         bEscheatdormancyDefined = false; //no days defined for escheat dormancy
     }
     //if none of the dormancy period is defined, then alert the user
     if (bUnclaimDormancyDefined == false) {
         alert("Unclaimed Days can not be left blank.");         
         bReturn = false;
     }

     if (bReturn) {
         if (parseInt(document.forms[0].UnclaimedDays.value) > parseInt(document.forms[0].EscheatDays.value)) {
             alert("Unclaimed Days should not be greater than Escheat Days.")
             bReturn = false;

         }
     }

     return bReturn;
}
//rupal:end

//Ishan: Multi Currency

function MultiCurrency_onOk() {
  
  var bResult = false;

  if (MultiCurrency_Validate()) {
    return true;
  }
  else {
    return false;
  }
}

function MultiCurrency_onCancel() {
  var retval = false;

  if (IsDataChanged() == true) {
    if (ConfirmSave() == true) {
      if (MultiCurrency_Validate()) {
        retval = true;
      }
      else {
        retval = false;
      }

    }
    else {
      window.close();
      retval = false;
    }

  }
  else {
    window.close();
    return false;
  }
  return retval;
}

function MultiCurrency_Validate() {
 
  //check 1: Source and Destination Currencies must be defined
  var SourceCurrencyId = document.forms[0].SourceCurrency_codelookup_cid.value;
  var DestinationCurrencyId = document.forms[0].DestinationCurrency_codelookup_cid.value;

  if (trim(SourceCurrencyId) == "" || trim(SourceCurrencyId) == "0") {
    alert("Please enter Source Currency.");
    document.forms[0].SourceCurrency_codelookup.focus();
    return false;
  }
  if (trim(DestinationCurrencyId) == "" || trim(DestinationCurrencyId) == "0") {
    alert("Please enter Destination Currency.");
    document.forms[0].DestinationCurrency_codelookup.focus();
    return false;
  }

  //check 2: Exchange Rate must be defined
  //var bExchangeRate = true;

  if (trim(document.forms[0].ExchangeRate.value) == "" || trim(document.forms[0].ExchangeRate.value) == "0") {
    alert("Exchange Rate can not be left blank.");
    return false;
    //document.forms[0].ExchangeRate.value = "0";//set the value to zero in case empty
    //bExchangeRate = false;
  }
  
  /*if (bExchangeRate == false) {
    alert("Exchange Rate can not be left blank.");
    return false;
  }*/
  
  //var numericExpression = /^[0-9]+$/;

  if (isNaN(parseFloat(document.forms[0].ExchangeRate.value))) {
      alert("Enter a numeric value");
      document.forms[0].ExchangeRate.focus();
      return false;
  }
//  else {
//      return true;
//  }

  // Check 3 : Source and destination are same then exchange rate should be 1
  // MITS 27120 : Ishan
  if(trim(SourceCurrencyId) == trim(DestinationCurrencyId)) {
      if (trim(document.forms[0].ExchangeRate.value) != "1") {
          alert("Exchange rate has to be 1 for Similar Source and Destination Currency")
          document.forms[0].ExchangeRate.focus();
          return false;
      }
  }

  return true;
}

// Ishan : end


//Changed by Gagan for MITS 9357 : Start
function stateabbrLostFocus(objCtrl) {

    var sValue = new String(objCtrl.value);

    if (sValue.length > 4) {
        alert("State abbreviation entry cannot take more than four characters. Please enter valid State abbreviation.");
        objCtrl.value = "";
        objCtrl.focus();
        return false;
    }
}
//Changed by Gagan for MITS 9357 : End

//gagnihotri MITS 15472 04/21/2009
function fnCalculateAmount()
{
	numLostFocus(document.getElementById("PercentNumber"));
	
	if( document.getElementById("PercentNumber").value == "" )
		document.getElementById("PercentNumber").value = 0 ;
		
	if( parseFloat( document.getElementById("PercentNumber").value ) > 100 )
		document.getElementById("PercentNumber").value = "100.00"
	document.getElementById("PercentNumber").value = parseInt(document.getElementById("PercentNumber").value * 100 )/100 ;
	document.getElementById("Amount").value = ( window.opener.document.getElementById( "NonThiredPartyAmount").value * document.getElementById("PercentNumber").value )/100;	
     //MGaba2:MITS 16062:Value of percent/amount was not getting fetched from popup to parent screen
	//Deb
	//currencyLostFocus(document.getElementById("Amount"));
	MultiCurrencyOnBlur_Form(document.getElementById("Amount"));
}
//MGaba2:MITS 15483:Needed for thirdpartypayments and futurepayments screen
//Moved it from trans.js to supportscreens.js
//Added Rakhi for MITS 12389(Payment Detail Screen):START 
function DateCompare(objCtrl) {

    var sEffectiveDate = new Date(document.getElementById("FromDate").value);//vkumar258 RMA-9153
    var sExpirationDate = new Date(document.getElementById("ToDate").value);
    if (sExpirationDate < sEffectiveDate) {
        alert("From date must be less than To date");
        //document.getElementById("fromdate").value = "";
        //document.getElementById("todate").value = "";
        objCtrl.value = "";
    }
}
//Added Rakhi for MITS 12389(Payment Detail Screen):END
function CMMSEAXData_onOk() {
    var bResult = false;

//    if (IsDataChanged() == false) {
//        window.close(); 
//        return true;
//    }
    if (CMMSEAXData_Validate()) {
        return true;
    }
    else {
        return false;
    }
}

function CMMSEAXData_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (CMMSEAXData_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function CMMSEAXData_Validate() {
    var SecondLobCode = document.getElementById('SecondLobCode'); //Added for Mits 20818
    if (document.forms[0].LobCode.value == 0) {
        //alert("Please select a Line Of Business.");
        alert(CMMSEAXDATAValidations.ValidationLOB);
        document.forms[0].LobCode.focus();
        return false;
    }
    else if (SecondLobCode.value != 0 && (document.getElementById('LobCode').value == SecondLobCode.value))  //Added else if for Mits 20818
    {
        //alert('Line of Business and Second Line of Business cannot be same');
        alert(CMMSEAXDATAValidations.ValidationLOBandSecLOBSame);
        return false;
    }
    if (document.forms[0].ReporterId.value == "") {
        //alert("Please enter MMSEA Reporter ID.");
        alert(CMMSEAXDATAValidations.ValidationEnterMMSEARepID);
        document.forms[0].ReporterId.focus();
        return false;
    }
    if (document.forms[0].SiteId.value == "") {
       // alert("Please enter MMSEA Office Code/Site ID.");
        alert(CMMSEAXDATAValidations.ValidationEnterMMSEASiteID);
        document.forms[0].SiteId.focus();
        return false;
    }
    return true;
}
function TPOCData_onOk() {
    var bResult = false;

    if (IsDataChanged() == false) {
        window.close();
        return true;
    }
    if (TPOCData_Validate()) {
        return true;
    }
    else {
        return false;
    }
}

function TPOCData_onCancel() {
    var retval = false;
   
    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (TPOCData_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function TPOCData_Validate() {
    document.getElementById('TpocEditFlag').value = -1;
    return true;
}
function MMSEAPartyData_onOk() {
    var bResult = false;

//    if (IsDataChanged() == false) {
//        window.close();
//        return true;
//    }
    if (MMSEAPartyData_Validate()) {
        return true;
    }
    else {
        return false;
    }
}

function MMSEAPartyData_onCancel() {
    var retval = false;

    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (MMSEAPartyData_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function MMSEAPartyData_Validate() {
    if (document.forms[0].att1lastfirstname.value == "") {
        //alert("Please select a Party Name.");
        alert(MMSEAPartyDataValidations.ValidSelectPartyName); //MITS 34109
        if (document.forms[0].att1lastfirstname.disabled == false) //Added for MITS 17762
			document.forms[0].att1lastfirstname.focus();
        return false;
    }
    else {
       
            var sNewPartyName = document.forms[0].att1lastfirstname.value;
            var sOldPartyName = document.forms[0].PartyName.value;
            if (sOldPartyName != sNewPartyName) {
                var objPartyName = document.forms[0].PartyNameData;
                if (objPartyName != null) {
                    var partyNames = objPartyName.value.split("|");
                    for (i = 0; i < partyNames.length; i++) {
                        if (partyNames[i] == sNewPartyName) {
                            //alert("Party Name already exists.");//MITS 34109
                            alert(MMSEAPartyDataValidations.ValidPartyNameExists);
                            document.forms[0].att1lastfirstname.focus();
                            return false;
                        }
                    }
                }
            }
    }
    return true;
}
//Animesh Inserted CPE MITS 18738
function AdjusterScreen_onCancel() {
    var retval = false;
    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (OnAdjusterScreenSave()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
function NonAvail_onCancel() {
    var retval = false;
    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (OnNonAvailDateTimeSave()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}
//Animesh Insertion ends
//Added Rakhi for R7:Emp Data Elements
//function AddressesXPhoneInfo_onOk() {
//    var bResult = false;
//    if (AddressesXPhoneInfo_Validate()) {
//        var txtDataChanged = window.opener.document.forms[0]('PopupDataChanged');
//        if (txtDataChanged != null)
//            txtDataChanged.value = "true";
//        return true;
//    }
//    else {
//        return false;
//    }
//}
//function AddressesXPhoneInfo_onCancel()
//{
//var retval = false;

//    if (IsDataChanged() == true) {
//        if (ConfirmSave() == true) {
//            if (AddressesXPhoneInfo_Validate()) {
//                retval = true;
//            }
//            else {
//                retval = false;
//            }

//        }
//        else {
//            window.close();
//            retval = false;
//        }

//    }
//    else {
//        window.close();
//        return false;
//    }
//    return retval;
//}

//function AddressesXPhoneInfo_Validate()
//{
//    
//    var oPhoneCode=document.getElementById('PhoneCode_codelookup');
//    var oPhoneNo=document.getElementById('PhoneNo');
//    if(oPhoneCode!=null && oPhoneNo!=null)
//    { 
//        if(oPhoneCode.value.trim()=="" && oPhoneNo.value.trim()=="")
//        {
//            alert('Both Phone Type and Phone No are required');
//            oPhoneCode.focus();
//            return false;
//        }
//        else if(oPhoneCode.value.trim()=="")
//        {
//            alert('Please enter a Phone Type');
//            oPhoneCode.focus();
//            return false;
//        }
//        else if(oPhoneNo.value.trim()=="")
//        {
//            alert('Please enter a valid Phone Number');
//            oPhoneNo.focus();
//             return false;
//        }
//        else
//        {
//            var sPhoneType = document.forms[0].PhoneCode_codelookup.value;
//            var sPhoneNo = document.forms[0].PhoneNo.value;
//            var oPhoneData = window.opener.document.forms[0].PhoneData;
//            var oSelectedId = window.opener.document.forms[0]('AddressXPhoneInfoSelectedId');
//                if (oPhoneData != null) {
//                    var oPhoneNumbers = oPhoneData.value.split(",");
//                    for (i = 0; i < oPhoneNumbers.length; i++) 
//                    {
//                        var oPhoneNumber = oPhoneNumbers[i].split("|");
//                        if (oPhoneNumber.length > 2) {
//                            var oPhoneType = oPhoneNumber[0];
//                            var oPhoneNo =  oPhoneNumber[1];
//                            var oRowId =    oPhoneNumber[2];
//                            if (oPhoneType == sPhoneType.toLowerCase() && oPhoneNo == sPhoneNo && oSelectedId.value != oRowId) {
//                                alert("Phone Type and  Phone Number already exists.");
//                                return false;
//                            }
//                        }
//                    }
//            }
//            
//           
//        }
//      return true;    
//    }
//   
//}
function EntityXAddressesInfo_onOk() {
    var bResult = false;
    if (EntityXAddressesInfo_Validate()) {
        return true;
    }
    else {
        return false;
    }
}
function EntityXAddressesInfo_onCancel()
{
    var retval = false;
//    var PopupGridChanged = document.getElementById("PopupDataChanged");
//    if (PopupGridChanged != null && PopupGridChanged.value.toLowerCase() == "true") {
//        SetDataChanged(true);
//    }
    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (EntityXAddressesInfo_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }

    }
    else {
        window.close();
        return false;
    }
    return retval;
}

function EntityXAddressesInfo_Validate(){
    var oPrimaryAddress = document.getElementById('PrimaryAddress');
    var oNewPrimaryRecord = document.getElementById('NewPrimaryRecord');
    var oPrimaryRow = window.opener.document.forms[0].PrimaryRow;
    var oPrimaryFlag = window.opener.document.forms[0].PrimaryFlag;
    var oPrimaryAddressChanged = window.opener.document.forms[0].PrimaryAddressChanged;

    //MITS:34276 starts - changed because it was causing error
    //    var oSelectedId = window.opener.document.forms[0]('EntityXAddressesInfoSelectedId');
    var oSelectedId = window.opener.document.forms[0].EntityXAddressesInfoSelectedId;
    //MITS:34276 ends
    var mode = document.getElementById('mode');
    //mbahl3 30221
    var sEffdate = "";  
    var sExpDate = "";
    var sExpirationDate = "";
    var sEffectiveDate = "";
    var oEffectiveDate = document.getElementById("EffectiveDate");
    var oExpirationDate = document.getElementById("ExpirationDate");
    //mbahl3 30221
    var sAddr1="";
    var sAddr2="";
    var sAddr3 = "";
    var sAddr4 = "";
    var sCity="";
    var sState = "";
    var sState_cid = "";
    var sCountry = "";
    var sCountry_cid = "";
    var sCounty="";
    var sZipCode="";
    var sEmail="";
    var sFax = "";
    //MITS 34276 - Start : Address Type for multiple addresses
    var sAddressType = "";
    var sAddressType_cid = "";
    //MITS 34276 - End : Address Type for multiple addresses
    //Mits 22242-Start:Address Data is not populated by default  on Patient  tab Address Fields
    var oPrimaryAddr1=null;
    var oPrimaryAddr2=null;
    var oPrimaryAddr3 = null;
    var oPrimaryAddr4 = null;
    var oPrimaryCity=null;
    var oPrimaryState=null;
    var oPrimaryCountry=null;
    var oPrimaryCounty=null;
    var oPrimaryZipCode=null;
    var oPrimaryEmail=null;
    var oPrimaryFax =null;

    var oFormName = window.opener.document.getElementById('SysFormName');
    if (oFormName != null) {
        if (oFormName.value.toLowerCase() == 'patient') {
            oPrimaryAddr1 = window.opener.document.getElementById('pataddr1');
            oPrimaryAddr2 = window.opener.document.getElementById('pataddr2');
            oPrimaryAddr3 = window.opener.document.getElementById('pataddr3');
            oPrimaryAddr4 = window.opener.document.getElementById('pataddr4');
            oPrimaryCity = window.opener.document.getElementById('patcity');
            oPrimaryState = window.opener.document.getElementById('patstateid_codelookup');
            oPrimaryCountry = window.opener.document.getElementById('patcountrycode_codelookup');
            oPrimaryCounty = window.opener.document.getElementById('patcounty');
            oPrimaryZipCode = window.opener.document.getElementById('patzipcode');
            oPrimaryEmail = window.opener.document.getElementById('patemailaddress');
            oPrimaryFax = window.opener.document.getElementById('patfaxnumber');
        }
        else {
            //Mits 22242-End:Address Data is not populated by default  on Patient  tab Address Fields
            oPrimaryAddr1 = window.opener.document.getElementById('addr1');
            oPrimaryAddr2 = window.opener.document.getElementById('addr2');
            oPrimaryAddr3 = window.opener.document.getElementById('addr3');
            oPrimaryAddr4 = window.opener.document.getElementById('addr4');
            oPrimaryCity = window.opener.document.getElementById('city');
            oPrimaryState = window.opener.document.getElementById('stateid_codelookup');
            oPrimaryCountry = window.opener.document.getElementById('countrycode_codelookup');
            oPrimaryCounty = window.opener.document.getElementById('county');
            oPrimaryZipCode = window.opener.document.getElementById('zipcode');
            oPrimaryEmail = window.opener.document.getElementById('emailaddress');
            oPrimaryFax = window.opener.document.getElementById('faxnumber');
        }//end else
    } //end oFormName
    //mbahl3 30221
    if ((oEffectiveDate != null) && (oExpirationDate != null)) {

        sEffectiveDate = oEffectiveDate.value;
        sExpirationDate = oExpirationDate.value;

        if (sEffectiveDate.indexOf('/') == -1)
                    sEffdate = sEffectiveDate.substring(4, 8) + sEffectiveDate.substring(0, 2) + sEffectiveDate.substring(2, 4);
                else
            sEffdate = sEffectiveDate.substring(6, 10) + sEffectiveDate.substring(0, 2) + sEffectiveDate.substring(3, 5);

        if (sExpirationDate.indexOf('/') == -1)
                    sExpDate = sExpirationDate.substring(4, 8) + sExpirationDate.substring(0, 2) + sExpirationDate.substring(2, 4);
                else
            sExpDate = sExpirationDate.substring(6, 10) + sExpirationDate.substring(0, 2) + sExpirationDate.substring(3, 5);

        
        if (sExpirationDate != "" && sExpDate < sEffdate) {
           // alert("Expiration Date must be greater than or equal to the Effective Date");
            alert(parent.CommonValidations.EffectiveDatelessthanExpirationDate);

           // oEffectiveDate.value = "";
            //oExpirationDate.value = "";
            return false;
        }
        
        //JIRA RMA-9040(Suggestion on RMA-6865) - ajohari2 Start
        //JIRA:6865 nshah28 START:
        //else
        //{

        //    if (oPrimaryAddress != null) {
        //        if (oPrimaryAddress.checked) {

        //            var date = new Date();
        //            var sTodaysDate = ("0" + (date.getMonth() + 1).toString()).substr(-2) + "/" + ("0" + date.getDate().toString()).substr(-2) + "/" + (date.getFullYear().toString());
        //            var sTodDate = "";
        //            if (sTodaysDate.indexOf('/') == -1)
        //                sTodDate = sTodaysDate.substring(4, 8) + sTodaysDate.substring(0, 2) + sTodaysDate.substring(2, 4);
        //            else
        //                sTodDate = sTodaysDate.substring(6, 10) + sTodaysDate.substring(0, 2) + sTodaysDate.substring(3, 5);

        //            if (sExpirationDate != "" && sExpDate < sTodDate) {
        //                //JIRA RMA-8583(Suggestion on RMA-6865) - ajohari2 Start
        //                //alert('You are not allowed to expire primary address.'); //JIRA RMA-8583(Suggestion on RMA-6865) nshah28
        //                alert(parent.CommonValidations.ExpirePrimaryAddress);
        //                //JIRA RMA-8583(Suggestion on RMA-6865) - ajohari2 End
        //                return false;
        //            }

        //        }

        //    }
        //}
        //JIRA:6865 END:
        //JIRA RMA-9040(Suggestion on RMA-6865) - ajohari2 End
    }
    //MITS 34276 - Start : Address Type for multiple addresses   
    var oAddressType = document.getElementById('AddressType_codelookup');
    if (oAddressType != null)
        sAddressType = oAddressType.value.trim();
    //MITS 34276 - End : Address Type for multiple addresses   
    //mbahl3 30221
    if (oPrimaryAddress != null) {
        if (oPrimaryAddress.checked) {
            if (oPrimaryFlag != null && oPrimaryRow != null && oSelectedId != null) {
                if (mode != null && mode.value.toLowerCase() == "edit") {
                    if (oPrimaryFlag.value.toLowerCase() == "true" && oPrimaryRow.value != oSelectedId.value) { //oSelectedId.value contains value of last edit Row which is retained even when we click new.
                        if (confirm('A Primary Address already exists.Do you want to mark this address as primary?')) {
                            if (oPrimaryAddressChanged != null) {
                                oPrimaryAddressChanged.value = "true";
                            }
                            if (oNewPrimaryRecord != null) {
                                oNewPrimaryRecord.value = "true";
                            }
                        }
                        else {
                            return false;
                        }
                    }
                }
                else if (mode != null && mode.value.toLowerCase() == "add") {
                    if (oPrimaryFlag.value.toLowerCase() == "true" && oPrimaryRow.value != "") {
                        if (confirm('A Primary Address already exists.Do you want to mark this address as primary?')) {
                            if (oPrimaryAddressChanged != null) {
                                oPrimaryAddressChanged.value = "true";
                            }
                            if (oNewPrimaryRecord != null) {
                                oNewPrimaryRecord.value = "true";
                            }
                        }
                        else {
                            return false;
                        }

                    }
                }

            }
            var oAddr1 = document.getElementById('Addr1');
            if (oAddr1 != null)
                sAddr1 = oAddr1.value.trim();
            var oAddr2 = document.getElementById('Addr2');
            if (oAddr2 != null)
                sAddr2 = oAddr2.value.trim();
            var oAddr3 = document.getElementById('Addr3');
            if (oAddr3 != null)
                sAddr3 = oAddr3.value.trim();
            var oAddr4 = document.getElementById('Addr4');
            if (oAddr4 != null)
                sAddr4 = oAddr4.value.trim();
            var oCity = document.getElementById('City');
            if (oCity != null)
                sCity = oCity.value.trim();
            var oState = document.getElementById('State_codelookup');
            if (oState != null) {
                sState = oState.value.trim();
                var oState_cid = document.getElementById('State_codelookup_cid');
                if (oState_cid != null)
                    sState_cid = oState_cid.value.trim();
            }
            var oCountry = document.getElementById('Country_codelookup');
            if (oCountry != null) {
                sCountry = oCountry.value.trim();
                var oCountry_cid = document.getElementById('Country_codelookup_cid');
                if (oCountry_cid != null)
                    sCountry_cid = oCountry_cid.value.trim();
            }
            var oCounty = document.getElementById('County');
            if (oCounty != null)
                sCounty = oCounty.value.trim();
            var oZipCode = document.getElementById('ZipCode');
            if (oZipCode != null)
                sZipCode = oZipCode.value.trim();
            var oEmail = document.getElementById('Email');
            if (oEmail != null) {
                sEmail = oEmail.value.trim();
            }
            var oFax = document.getElementById('Fax');
            if (oFax != null)
                sFax = oFax.value.trim();
            //RMA-4627 - Start making address type non required
            //MITS:34276 starts - add condition for address type
            if (sAddr1 == "" && sAddr2 == "" && sAddr3 == "" && sAddr4 == "" && sCity == "" && sCountry == "" && sCounty == "" && sState == "" && sEmail == "" && sFax == "" && sZipCode == "") {
            //if (sAddr1 == "" && sAddr2 == "" && sCity == "" && sCountry == "" && sCounty == "" && sState == "" && sEmail == "" && sFax == "" && sZipCode == "" && sAddressType == "") {
                //MITS:34276 ends
                //RMA-4627 - End
                alert('A primary address must contain data in at least one of fields');
                return false;
            }
            //Assigning values to fields on the Main Tab
            if (oPrimaryAddr1 != null)
                oPrimaryAddr1.value = sAddr1;

            if (oPrimaryAddr2 != null)
                oPrimaryAddr2.value = sAddr2;

            if (oPrimaryAddr3 != null)
                oPrimaryAddr3.value = sAddr3;

            if (oPrimaryAddr4 != null)
                oPrimaryAddr4.value = sAddr4;

            if (oPrimaryCity != null)
                oPrimaryCity.value = sCity;

            if (oPrimaryState != null) {
                oPrimaryState.value = sState;
                var oPrimaryState_cid = null;
                if (oFormName.value.toLowerCase() == 'patient') {
                    oPrimaryState_cid = window.opener.document.getElementById('patstateid_codelookup_cid');
                }
                else {
                    oPrimaryState_cid = window.opener.document.getElementById('stateid_codelookup_cid');
                }
                if (oPrimaryState_cid != null)
                    oPrimaryState_cid.value = sState_cid;
            }

            if (oPrimaryCountry != null) {
                oPrimaryCountry.value = sCountry;
                var oPrimaryCountry_cid = null;
                if (oFormName.value.toLowerCase() == 'patient') {
                    oPrimaryCountry_cid = window.opener.document.getElementById('patcountrycode_codelookup_cid');
                }
                else {
                    oPrimaryCountry_cid = window.opener.document.getElementById('countrycode_codelookup_cid');
                }
                if (oPrimaryCountry_cid != null)
                    oPrimaryCountry_cid.value = sCountry_cid;
            }

            if (oPrimaryCounty != null)
                oPrimaryCounty.value = sCounty;

            if (oPrimaryZipCode != null)
                oPrimaryZipCode.value = sZipCode;

            if (oPrimaryEmail != null)
                oPrimaryEmail.value = sEmail;

            if (oPrimaryFax != null)
                oPrimaryFax.value = sFax;
            //Assigning values to fields on the Main Tab
        }
        else {
            if (oPrimaryFlag != null && oPrimaryRow != null && oSelectedId != null) {
                if (mode != null && mode.value.toLowerCase() == "edit") {
                    if (oPrimaryFlag.value.toLowerCase() == "true" && oPrimaryRow.value == oSelectedId.value) { //oSelectedId.value contains value of last edit Row which is retained even when we click new.

                        //Removing values from fields on the Main Tab when the Primary Address is unchecked
                        if (oPrimaryAddr1 != null)
                            oPrimaryAddr1.value = "";

                        if (oPrimaryAddr2 != null)
                            oPrimaryAddr2.value = "";

                        if (oPrimaryAddr3 != null)
                            oPrimaryAddr3.value = "";

                        if (oPrimaryAddr4 != null)
                            oPrimaryAddr4.value = "";

                        if (oPrimaryCity != null)
                            oPrimaryCity.value = "";

                        if (oPrimaryState != null) {
                            oPrimaryState.value = "";
                            var oPrimaryState_cid = null;
                            if (oFormName.value.toLowerCase() == 'patient') {
                                oPrimaryState_cid = window.opener.document.getElementById('patstateid_codelookup_cid');
                            }
                            else {
                                oPrimaryState_cid = window.opener.document.getElementById('stateid_codelookup_cid');
                            }
                            if (oPrimaryState_cid != null)
                                oPrimaryState_cid.value = "-1";
                        }

                        if (oPrimaryCountry != null) {
                            oPrimaryCountry.value = "";
                            var oPrimaryCountry_cid = null;
                            if (oFormName.value.toLowerCase() == 'patient') {
                                oPrimaryCountry_cid = window.opener.document.getElementById('patcountrycode_codelookup_cid');
                            }
                            else {
                                oPrimaryCountry_cid = window.opener.document.getElementById('countrycode_codelookup_cid');
                            }
                            if (oPrimaryCountry_cid != null)
                                oPrimaryCountry_cid.value = "-1";
                        }

                        if (oPrimaryCounty != null)
                            oPrimaryCounty.value = "";

                        if (oPrimaryZipCode != null)
                            oPrimaryZipCode.value = "";

                        if (oPrimaryEmail != null)
                            oPrimaryEmail.value = "";

                        if (oPrimaryFax != null)
                            oPrimaryFax.value = "";
                        //Removing values from fields on the Main Tab when the Primary Address is unchecked
                    }
                }
            }
        }
    }
    //RMA-4627 - Start making address type non required
    //MITS:34276 - Start : Address Type for multiple addresses    
    //if (sAddressType == "") {
    //    alert("Address Type is required.");
    //    return false;
    //}
    //MITS:34276 - End
    //RMA-4627 - End
    return true;
    
}
//Added Rakhi for R7:Emp Data Elements

function PolicyChanged() {
    
    if (document.getElementById("PolicyID") != null && document.getElementById("Policy") != null)
    {
        document.getElementById("PolicyID").value = document.getElementById("Policy").value;
        document.getElementById("Unit").options.length = 0;
    }

    //rupal:start, r8 unit implementation
    //these hidden controls shouls also be wiped out when policy is changed
    if (document.getElementById("PolCvgID") != null) {
        document.getElementById("PolCvgID").value = "";
    }
    if (document.getElementById("UnitID") != null) {
        document.getElementById("UnitID").value = "";
    }
    if (document.getElementById("ResTypeCode") != null) {
        document.getElementById("ResTypeCode").value = "";
    }
    //rupal:end

    var txtCoverage = window.document.getElementById('Coverage_codelookup');
    if (txtCoverage != null) {
        txtCoverage.value = "";
    }
    var CoverageCode_cid = window.document.getElementById('Coverage_codelookup_cid');
    if (CoverageCode_cid != null) {
        CoverageCode_cid.value = "";
    }
    var txtReserveType = window.document.getElementById('ReserveTypeCodeFt_codelookup');
    if (txtReserveType != null) {
        txtReserveType.value = "";
    }
    var ReserveTypeCode_cid = window.document.getElementById('ReserveTypeCodeFt_codelookup_cid');
    if (ReserveTypeCode_cid != null) {
        ReserveTypeCode_cid.value = "";
    }
    var txtTransType = window.document.getElementById('TransTypeCode_codelookup');
    if (txtTransType != null) {
        txtTransType.value = "";
    }
    var TransTypeCode_cid = window.document.getElementById('TransTypeCode_codelookup_cid');
    if (TransTypeCode_cid != null) {
        TransTypeCode_cid.value = "";
    }
    var reservebalance = window.document.getElementById('reservebalance');
    if (reservebalance != null) {
        reservebalance.value = "";
    }
    

}

function UnitChanged() {

    var txtUnitID = window.document.getElementById('UnitID');
    var txtUnit = window.document.getElementById('Unit');
    if (txtUnit != null && txtUnitID != null) {
        txtUnitID.value = txtUnit.value;
    }

    //rupal:start, r8 unit implementation
    //these hidden controls shouls also be wiped out when unit is changed
    if (document.getElementById("PolCvgID") != null) {
        document.getElementById("PolCvgID").value = "";
    }
    if (document.getElementById("UnitID") != null) {
        document.getElementById("UnitID").value = "";
    }
    if (document.getElementById("ResTypeCode") != null) {
        document.getElementById("ResTypeCode").value = "";
    }
    //rupal:end

    var txtCoverage = window.document.getElementById('Coverage_codelookup');
    if (txtCoverage != null) {
        txtCoverage.value = "";
    }
    var CoverageCode_cid = window.document.getElementById('Coverage_codelookup_cid');
    if (CoverageCode_cid != null) {
        CoverageCode_cid.value = "";
    }
    var txtReserveType = window.document.getElementById('ReserveTypeCodeFt_codelookup');
    if (txtReserveType != null) {
        txtReserveType.value = "";
    }
    var ReserveTypeCode_cid = window.document.getElementById('ReserveTypeCodeFt_codelookup_cid');
    if (ReserveTypeCode_cid != null) {
        ReserveTypeCode_cid.value = "";
    }
    var txtTransType = window.document.getElementById('TransTypeCode_codelookup');
    if (txtTransType != null) {
        txtTransType.value = "";
    }
    var TransTypeCode_cid = window.document.getElementById('TransTypeCode_codelookup_cid');
    if (TransTypeCode_cid != null) {
        TransTypeCode_cid.value = "";
    }
    var reservebalance = window.document.getElementById('reservebalance');
    if (reservebalance != null) {
        reservebalance.value = "";
    }
}

/*
function UnitChanged() {
    var txtCoverage = window.document.getElementById('Coverage_codelookup');
    if (txtCoverage != null) {
        txtCoverage.value = "";
    }
    var CoverageCode_cid = window.document.getElementById('Coverage_codelookup_cid');
    if (CoverageCode_cid != null) {
        CoverageCode_cid.value = "";
    }
    var txtReserveType = window.document.getElementById('ReserveTypeCodeFt_codelookup');
    if (txtReserveType != null) {
        txtReserveType.value = "";
    }
    var ReserveTypeCode_cid = window.document.getElementById('ReserveTypeCodeFt_codelookup_cid');
    if (ReserveTypeCode_cid != null) {
        ReserveTypeCode_cid.value = "";
    }
    var txtTransType = window.document.getElementById('TransTypeCode_codelookup');
    if (txtTransType != null) {
        txtTransType.value = "";
    }
    var TransTypeCode_cid = window.document.getElementById('TransTypeCode_codelookup_cid');
    if (TransTypeCode_cid != null) {
        TransTypeCode_cid.value = "";
    }
    var reservebalance = window.document.getElementById('reservebalance');
    if (reservebalance != null) {
        reservebalance.value = "";
    }
}*/
function addToolTip() {
    //debugger;
    var ddl = window.document.getElementById("unit");
    if (ddl != null) {
        for (var i = 0; i < ddl.options.length; i++) {
            ddl.options[i].title = ddl.options[i].text;
        }
    }
}
function MergeMailDetailLoad() {
    loadTabList();
    var tabObj = document.getElementById('LINKTABSTemplate');

    var objTabAction = document.getElementById("HdnTabPress");
    var objHiddenTab = document.getElementById("HdnTab");

    if (objTabAction != null && objTabAction.value != "") {
        tabObj = document.getElementById('LINKTABS' + objTabAction.value);
        if (tabObj != null) {
            tabObj.className = "Selected";
            if (objHiddenTab.value == 'undefined')
                tabChange(objTabAction.value);
            else
                tabChange(objTabAction.value, objHiddenTab.value);
        }
    }
    else {
        if (tabObj != null) {
            if (tabObj.className == "Selected")
                tabChange(m_TabList[0].id.substring(4));
        }
    }

}
// spahariya MITS 30911 - FNOL Claim reserve screens
function FNOLClmRes_Save() {
    //debugger;
//    if (IsDataChanged() == false) {
//          window.close();
//        return true;
//    }
    if (FNOLClmRes_Validate()) {
        return true;
    }
    else {
        return false;
    }
}
function FNOLClmRes_onCancel() {
    var retval = false;
    var objAction = document.getElementById("txtSaveOnCancel");
                
    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (FNOLClmRes_Validate()) {
                retval = true;
                if (objAction != null) 
                    objAction.value = "save";
            }
            else {
                retval = false;
            }
        }
        else {
            //window.close();
            retval = true;
        }
    }
    else {
        //window.close();
        return true;
    }
    return retval;
}
function FNOLClmRes_Validate()  //from R4 OnHolidaySave()
{
    if (document.forms[0].cmbClaimant.value == "") {
        //changed by Swati Agarwal for ML changes
        //alert("Please select a Claimant.");
        alert(parent.CommonValidations.SelectClaimant);
        document.forms[0].cmbClaimant.focus();
        return false;
    }
    if (document.forms[0].cmbPolicy.value == "") {
        //changed by Swati Agarwal for ML changes
        //alert("Please select a Policy.");
        alert(parent.CommonValidations.SelectPolicy);
        document.forms[0].cmbPolicy.focus();
        return false;
    }
    if (document.forms[0].cmbUnit.value == "") {
        //changed by Swati Agarwal for ML changes
        //alert("Please select an Unit.");
        alert(parent.CommonValidations.SelectUnit);
        //document.forms[0].cmbUnit.focus();
        return false;
    }
    if (document.forms[0].cmbCoverageType.value == "") {
        //changed by Swati Agarwal for ML changes
        //alert("Please select a Coverage.");
        alert(parent.CommonValidations.SelectCoverage);
        //document.forms[0].cmbCoverageType.focus();
        return false;
    }
    if (document.forms[0].cmbLossType.value == "") {
        //changed by Swati Agarwal for ML changes
        //alert("Please select a Loss type.");
        alert(parent.CommonValidations.SelectLossType);
        //document.forms[0].cmbLossType.focus();
        return false;
    }
    //document.forms[0].FormMode.value="close"
    return true;

}

function FNOLClmRes_Next() {
    var bResult = false;  
    if (FNOLClmRes_Validate()) {
        return true;
    }
    else {
        return false;
    }
}


function FNOLResDelete() {
    //changed by Swati Agarwal for ML changes
    //alert("The selected row can not be deleted.");
    alert(parent.CommonValidations.DenyRowDel);
    return false;
}



// spahariya MITS 30911 - FNOL Claim reserve screens - End

//MITS:34276 -- Entity ID Type start
function EntityXEntityIDType_onOk() {
    var bResult = false;
    if (IsDataChanged() == false) {
        return false;
        //No Action taken if there is no change on screen
    }
    if (EntityXEntityIDType_Validate()) {
        return true;
    }
    else {
        return false;
    }
}

function EntityXEntityIDType_onCancel() {
    var retval = false;  
    if (IsDataChanged() == true) {
        if (ConfirmSave() == true) {
            if (EntityXEntityIDType_Validate()) {
                retval = true;
            }
            else {
                retval = false;
            }

        }
        else {
            window.close();
            retval = false;
        }
    }
    else {
        window.close();
        return false;
    }
    return retval;
}

function EntityXEntityIDType_Validate() {
    var sEffdate = "";
    var sExpDate = "";
    var sExpirationDate = "";
    var sEffectiveDate = "";
    var oEffectiveDate = document.getElementById("EffStartDate");
    var oExpirationDate = document.getElementById("EffEndDate");
    var sEntityType = "";
    var sEntityIDNum = "";

    if (document.forms[0].IdType_codelookup.value.trim() == "" || document.forms[0].IdNum.value.trim() == "") {
        document.forms[0].IdType_codelookup_cid.value = "";
        alert("Entity ID Type and Entity ID Number are required.");
        if (m_TabList != null && m_TabList[0] != null)
            tabChange(m_TabList[0].id.substring(4));
        document.forms[0].IdType_codelookup.focus();
        return false;
    }
    if ((oEffectiveDate != null) && (oExpirationDate != null)) {

        sEffectiveDate = oEffectiveDate.value;
        sExpirationDate = oExpirationDate.value;

        if (sEffectiveDate.indexOf('/') == -1)
            sEffdate = sEffectiveDate.substring(4, 8) + sEffectiveDate.substring(0, 2) + sEffectiveDate.substring(2, 4);
        else
            sEffdate = sEffectiveDate.substring(6, 10) + sEffectiveDate.substring(0, 2) + sEffectiveDate.substring(3, 5);

        if (sExpirationDate.indexOf('/') == -1)
            sExpDate = sExpirationDate.substring(4, 8) + sExpirationDate.substring(0, 2) + sExpirationDate.substring(2, 4);
        else
            sExpDate = sExpirationDate.substring(6, 10) + sExpirationDate.substring(0, 2) + sExpirationDate.substring(3, 5);


        if (sExpirationDate != "" && sExpDate < sEffdate) {
            alert(parent.CommonValidations.EffectiveDatelessthanExpirationDate);
            return false;
        }
    }

    //achouhan3    New/Update Vendor Permission
    var objParentDocument = window.opener.document;
    var cntrlGridName = document.getElementById("gridname").value;
    var arrVendor = null;
    var entSelectedRow = objParentDocument.getElementById(cntrlGridName + "_hfNewVendorPermission");
    if (document.forms[0].IdType_codelookup.value.trim() != '')
        arrVendor = document.forms[0].IdType_codelookup.value.trim().split(' ');
    if (entSelectedRow != null && arrVendor[0] != null && arrVendor[0].toLowerCase().trim() == 'vendor') {
        if (entSelectedRow.value.indexOf('ValidateNewVendor') >= 0 || entSelectedRow.value.indexOf('ValidateUpdateVendor') >= 0) {
            alert("You are not authorized to perform this action");
            return false;
        }
    }
    //achouhan3    End of New/Update Vendor Permission
    return true;
}
//MITS:34276-- Entity ID Type end
