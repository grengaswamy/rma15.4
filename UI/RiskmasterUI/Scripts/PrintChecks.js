﻿/**********************************************************************************************
*   Date     |  MITS/JIRA       | Programmer | Description                                    *
**********************************************************************************************
* 12/09/2014 | RMA-438          | ajohari2   | Underwriters - EFT Payments
**********************************************************************************************/
var wndPreCheckDetail;
var wndInsufficientFund;
var wndPrintChecksBatch;

function DisablePreButtons() {
    window.document.forms[0].printpre.disabled = true;
    window.document.forms[0].choose.disabled = true;
}

function DisableBatchButtons() {
    window.document.forms[0].printbatch.disabled = true;
}

function DisablePostButtons() {
    window.document.forms[0].printpost.disabled = true;
    window.document.forms[0].printeob.disabled = true;
}

// npadhy JIRA 6418 starts - The Navigation of Distribution Type should be as per bank account.
// When we change the distribution type in any of the tabs, we make the corresponding change in the rest of the tabs as well.
function BankAccountChange(tabname) {
    if (tabname == 'pre') {
        window.document.forms[0].bankaccountprint.value = window.document.forms[0].bankaccountpre.value;
        window.document.forms[0].bankaccountpost.value = window.document.forms[0].bankaccountpre.value;
        window.document.forms[0].ddlDistributionTypePrint.value = window.document.forms[0].ddlDistributionTypePre.value;
        window.document.forms[0].ddlDistributionTypePost.value = window.document.forms[0].ddlDistributionTypePre.value;
        DisablePreButtons()
    }
    if (tabname == 'print') {
        window.document.forms[0].bankaccountpre.value = window.document.forms[0].bankaccountprint.value;
        window.document.forms[0].bankaccountpost.value = window.document.forms[0].bankaccountprint.value;
        window.document.forms[0].ddlDistributionTypePre.value = window.document.forms[0].ddlDistributionTypePrint.value;
        window.document.forms[0].ddlDistributionTypePost.value = window.document.forms[0].ddlDistributionTypePrint.value;
        DisableBatchButtons()
    }
    if (tabname == 'post') {
        window.document.forms[0].bankaccountpre.value = window.document.forms[0].bankaccountpost.value;
        window.document.forms[0].bankaccountprint.value = window.document.forms[0].bankaccountpost.value;
        window.document.forms[0].ddlDistributionTypePre.value = window.document.forms[0].ddlDistributionTypePost.value;
        window.document.forms[0].ddlDistributionTypePrint.value = window.document.forms[0].ddlDistributionTypePost.value;
        DisablePostButtons()
    }

    window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.AccountChanged";
    //window.document.forms[0].action.value = "PageRefresh";
    //OnFormSubmitPrintCheck();
}
// npadhy JIRA 6418 Ends - The Navigation of Distribution Type should be as per bank account.
// When we change the distribution type in any of the tabs, we make the corresponding change in the rest of the tabs as well.

function CheckSetChanged(Id) {
    window.document.forms[0].todate.value = window.document.forms[0].todatetemp.value;
    window.document.forms[0].fromdate.value = window.document.forms[0].fromdatetemp.value;
    //Added by Debabrata Biswas Batch Printing Filter R6 Retrofit -03/07/2010-MITS 19715/20050 Date: 03/17/2010
    window.document.forms[0].orghierarchy.value = window.document.forms[0].orghierarchypre_lst.value; 
    if (Id != null) {
        if (Id.toString() == "allchecks") {
            window.document.forms[0].selectchecks.checked = false;
        }
    }
    window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.CheckSetChanged";
    //window.document.forms[0].action.value = "PageRefresh";

    DisablePreButtons()
    pleaseWait.Show();
    document.forms[0].submit();
    //OnFormSubmitPrintCheck();
}

function CheckBatchChangedPre() {
    var iBatchId = window.document.forms[0].checkbatchpre.value;
    if (isNaN(iBatchId) || iBatchId.indexOf(".") != -1) {
        alert("Invalid Check Batch Number");
        window.document.forms[0].checkbatchpre.focus();
        return false;
    }

    window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.CheckBatchChangedPre";
    //window.document.forms[0].action.value = "PageRefresh";
    pleaseWait.Show();
    document.forms[0].submit();
    DisableBatchButtons()
    //OnFormSubmitPrintCheck();
}

function CheckBatchChangedPost() {
    var iBatchId = window.document.forms[0].checkbatchpost.value;
    if (isNaN(iBatchId) || iBatchId.indexOf(".") != -1) {
        alert("Invalid Check Batch Number");
        window.document.forms[0].checkbatchpost.focus();
        return false;
    }

    window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.CheckBatchChangedPost";
    //window.document.forms[0].action.value = "PageRefresh";
    pleaseWait.Show();
    document.forms[0].submit();
    DisablePostButtons()
    //OnFormSubmitPrintCheck();
}

function OpenPreCheckDetailWindow() {
    var sfromdate = window.document.forms[0].fromdate.value;
    var stodate = window.document.forms[0].todate.value;
    var sfromdateflag = window.document.forms[0].fromdateflag.checked;
    var stodateflag = window.document.forms[0].todateflag.checked;
    var bincludeautopayments = window.document.forms[0].includeautopayments.checked;
    //skhare7 R8 enhancement
    var bincludecombinedpayments = window.document.forms[0].includecombinedpayments.checked;
    //skhare7 R8 end
    //JIRA:438 START: ajohari2
    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //var bchkEFTPayment = 'false';
    //if (window.document.forms[0].chkEFTPayment != null)
    //    bchkEFTPayment = window.document.forms[0].chkEFTPayment.checked;
    //JIRA:438 End: 
    // Distribution Tyoe
    var iDistributionType = window.document.forms[0].ddlDistributionTypePre.value;
    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    var iaccountid = window.document.forms[0].bankaccountpre.value;
    
	//Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010
    var sorghierarchypre = window.document.forms[0].orghierarchypre_lst.value ;
    var sorghierarchylevelpre = window.document.forms[0].orghierarchylevelpre.value ;
	//End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010
    var sselectedchecksids = window.document.forms[0].selectedchecksids.value;
    var sselectedautochecksids = window.document.forms[0].selectedautochecksids.value;
    var susingselection = window.document.forms[0].selectchecks.checked;
    var sorderby = window.document.forms[0].orderfieldpre.value;

    var width = screen.availWidth - 60;
    var height = screen.availHeight - 60;
    var sQueryString = "";

//    arrTemp = sselectedchecksids.split(",");
//    sselectedchecksids = arrTemp[0];
//    for (var i = 1; i < arrTemp.length; i++)
//        sselectedchecksids += " " + arrTemp[i];

//    arrTemp = sselectedautochecksids.split(",");
//    sselectedautochecksids = arrTemp[0];
//    for (var i = 1; i < arrTemp.length; i++)
//        sselectedautochecksids += " " + arrTemp[i];
	//orghierarchypre and orghierarchylevelpre parameters added by Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010
    //skhare7 R8 
    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //JIRA:438 START: ajohari2
    sQueryString = "fromdate=" + sfromdate + "&todate=" + stodate + "&fromdateflag=" + sfromdateflag
				+ "&todateflag=" + stodateflag
		        + "&includeautopayments=" + bincludeautopayments + "&includecombinedpays=" + bincludecombinedpayments //+ "&EFTPayment=" + bchkEFTPayment npadhy JIRA 6418
				+ "&accountid=" + iaccountid + "&orghierarchypre=" + sorghierarchypre + "&orghierarchylevelpre=" + sorghierarchylevelpre + "&selectedchecksids=" + sselectedchecksids 
				+ "&selectedautochecksids=" + sselectedautochecksids + "&usingselection=" + susingselection
				+ "&orderby=" + sorderby + "&DistributionType=" + iDistributionType;
    //JIRA:438 End: 
    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
	//MITS 24942 hlv 3/28/2012
	// wndPreCheckDetail = showModalDialog("ShowPreCheck.aspx", new String(sQueryString + ""), 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:yes;scrollbars:yes;status:no;center:yes;');
    // aravi5 RMA-10209 Print Checks - ShowModalDialog Issues starts
	if (get_browserName() == "IE") {
	    var wndPreCheckDetail = showModalDialog("ShowPreCheck.aspx", new String(sQueryString + ""), 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:yes;scrollbars:yes;status:no;center:yes;');
	    if (wndPreCheckDetail != "error" && confirm("Do you wish to go ahead and mark all checks in this report as being eligible to print?")) {
	        window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.SavePreCheckDetail";
	        document.forms[0].method = "post";
	        document.forms[0].submit();
	    }
	    else {
	        window.document.forms[0].functiontocall.value = "";
	    }
	}
	else {
	    var wndPreCheckDetail = window.open("ShowPreCheck.aspx", null, 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=yes,scrollbars=yes,status=no,center=yes;');
	    wndPreCheckDetail.dialogArguments = sQueryString;
	    var wnd = window.open("../../UI/PrintChecks/PreCheck.html", self, "width=500,height=100,help=no,scroll=yes,status=no");
	    return false;
	}
    // aravi5 RMA-10209 Print Checks - ShowModalDialog Issues ends
}

function OpenInsufficientFundWindow() {
    var sfromdate = window.document.forms[0].fromdate.value;
    var stodate = window.document.forms[0].todate.value;
    var sfromdateflag = window.document.forms[0].fromdateflag.checked;
    var stodateflag = window.document.forms[0].todateflag.checked;
    var bincludeautopayments = window.document.forms[0].includeautopayments.checked;
    var iaccountid = window.document.forms[0].bankaccountpre.value;
    var width = screen.availWidth - 60;
    var height = screen.availHeight - 60;
    var sQueryString = "";
    var sselectedchecksids = window.document.forms[0].selectedchecksids.value;
    var sselectedautochecksids = window.document.forms[0].selectedautochecksids.value;
    var susingselection = window.document.forms[0].selectchecks.checked;
    //skhare7 R8 enhancement
    var bincludecombinedpays = window.document.forms[0].includecombinedpayments.checked;
     // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //JIRA:438 START: ajohari2
    //var bchkEFTPayment = 'false';
    //if (window.document.forms[0].chkEFTPayment != null)
    //    bchkEFTPayment = window.document.forms[0].chkEFTPayment.checked;
    var idistributiontype = window.document.forms[0].ddlDistributionTypePre.value;
    sQueryString = "fromdate=" + sfromdate + "&todate=" + stodate + "&fromdateflag=" + sfromdateflag + "&todateflag=" + stodateflag + "&includeautopayments=" + bincludeautopayments + "&accountid=" + iaccountid + "&selectedchecksids=" + sselectedchecksids + "&selectedautochecksids=" + sselectedautochecksids + "&usingselection=" + susingselection + "&includecombinedpays=" + bincludecombinedpays + /*"&EFTPayment=" + bchkEFTPayment +*/ "&distributiontype=" + idistributiontype;
    //JIRA:438 End: 
    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //sQueryString = "fromdate=" + sfromdate + "&todate=" + stodate + "&fromdateflag=" + sfromdateflag + "&todateflag=" + stodateflag + "&includeautopayments=" + bincludeautopayments + "&accountid=" + iaccountid + "&selectedchecksids=" + sselectedchecksids + "&selectedautochecksids=" + sselectedautochecksids + "&usingselection=" + susingselection + "&includecombinedpays=" + bincludecombinedpays;
    //skhare7 R8 end
    //wndInsufficientFund = window.open( "home?pg=riskmaster/PrintChecks/InsufficientFund&amp;" + sQueryString , 'InsufficientFundsReport', 'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=yes,scrollbars=yes');	
//    wndPreCheckDetail = showModalDialog("home?pg=riskmaster/PrintChecks/InsufficientFundFrame&" + sQueryString, null, 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:yes;scrollbars:yes;status:no;center:yes;');
    //wndPreCheckDetail = showModalDialog("InsufficientFundFrame.aspx?" + sQueryString, null, 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:yes;scrollbars:yes;status:no;center:yes;');
    // aravi5 RMA-10209 Print Checks - ShowModalDialog Issues starts
    // wndPreCheckDetail = showModalDialog("ShowInsufficientFund.aspx?" + sQueryString, null, 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:yes;scrollbars:yes;status:no;center:yes;');
    if (get_browserName() == "IE") {
        // aravi5 RMA-11631 starts
        var wndPreCheckDetail = showModalDialog("ShowInsufficientFund.aspx?" + sQueryString, null, 'dialogWidth:' + width + 'px;dialogHeight:' + height + 'px;dialogtop:' + (screen.availHeight - height) / 2 + ';dialogleft:' + (screen.availWidth - width) / 2 + ';resizable:yes;scrollbars:yes;status:no;center:yes;');
        // var wndPreCheckDetail = window.open("ShowInsufficientFund.aspx?" + sQueryString, null, 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=yes,scrollbars=yes,status=no,center=yes;');
        if (confirm("There are payments to sub bank accounts with insufficient funds. Do you wish to print checks from the accounts that have sufficient funds?")) {
            OpenPreCheckDetailWindow();
            window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.PreCheckDetail";
        }
        else {
            window.document.forms[0].functiontocall.value = "";
        }
        // aravi5 RMA-11631 Ends
    }
    else {
        var wndPreCheckDetail = window.open("ShowInsufficientFund.aspx?" + sQueryString, null, 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=yes,scrollbars=yes,status=no,center=yes;');
        var wnd = window.open("../../UI/PrintChecks/InsufficientFunds.html", self, "width=500,height=100,help=no,scroll=yes,status=no"); // aravi5 RMA-11631
        return false;
    }
    // aravi5 RMA-10209 Print Checks - ShowModalDialog Issues ends
}

function OnFormSubmitPrintCheck() {
    document.forms[0].method = "post";
    self.setTimeout('document.forms[0].submit();', 200);

    var wnd = window.open('csc-Theme/riskmaster/common/html/progress.html', 'progressWnd',
			'width=400,height=150' + ',top=' + (screen.availHeight - 150) / 2 + ',left=' + (screen.availWidth - 400) / 2);
    self.parent.wndProgress = wnd;

    return true;
}

function PrintCheckOnLoad() {
//    CloseProgressWindow();
    parent.MDIScreenLoaded();
    loadTabList();
    
    tabChange(window.document.forms[0].hTabName.value);
    
    window.document.forms[0].choose.disabled = !window.document.forms[0].selectchecks.checked;

    window.document.forms[0].fromdatetemp.disabled = !window.document.forms[0].fromdateflag.checked;
   // window.document.forms[0].fromdatetempbtn.disabled = !window.document.forms[0].fromdateflag.checked;//vkumar258 - RMA-6037
    window.document.forms[0].todatetemp.disabled = !window.document.forms[0].todateflag.checked;
    // window.document.forms[0].todatetempbtn.disabled = !window.document.forms[0].todateflag.checked;//vkumar258 - RMA-6037

    window.document.forms[0].todatetemp.value = window.document.forms[0].todate.value;
    window.document.forms[0].fromdatetemp.value = window.document.forms[0].fromdate.value;
    
    var stockCount = window.document.forms[0].checkstock.options.length > 0;
    var isEftAcc = window.document.forms[0].tbIsEFTAccount.value;
    //Added by Amitosh For Eft Payment 
   if (isEftAcc == "true")
    {
        stockCount = true;
    }

    if (!stockCount)
        alert("There are no check stocks defined for this bank account. You will not be able to print any checks.");

    if (window.document.forms[0].printpreflag.value == "True" && stockCount)
        window.document.forms[0].printpre.disabled = false;
    else
        window.document.forms[0].printpre.disabled = true;

    if (window.document.forms[0].printbatchflag.value == "True" && stockCount)
        window.document.forms[0].printbatch.disabled = false;
    else
        window.document.forms[0].printbatch.disabled = true;

    if (window.document.forms[0].printpostflag.value == "True") {
        window.document.forms[0].printpost.disabled = false;
        window.document.forms[0].printeob.disabled = false;
    }
    else {
        window.document.forms[0].printpost.disabled = true;
        window.document.forms[0].printeob.disabled = true;
    }

    if (window.document.forms[0].usesubbankaccounts.value == "True")
        window.document.forms[0].subaccount.disabled = false;
    else
        window.document.forms[0].subaccount.disabled = true;

    if (window.document.forms[0].txtIsPrinterSelected.value == "false") {
        document.getElementById('formdemotitle').innerText = "You need to select a default printer in payment parameter setup or change the option 'Print Direct To Printer' off.";
        window.document.forms[0].printeob.disabled = true;
        window.document.forms[0].printbatch.disabled = true;
    }
        

    if (window.document.forms[0].functiontocall.value == "PrintChecksAdaptor.GetInsufficientFundsPreEditFlag") {
        if (window.document.forms[0].insufficientfund.value == "True") {
            OpenInsufficientFundWindow();
            window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.InsufficientFundsPreEdit";
        }
        else {
            //Start rsushilaggar MITS 37250 11/18/2014
            if (window.document.forms[0].TransDateNull.value == "True") 
            {
                return;
            } 
            else
            {
            OpenPreCheckDetailWindow();
            window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.PreCheckDetail";
            }
        }
    }
    //pmittal5 Mits 17988 10/07/09 - Enable/disable Check Date depending on the Allow Post Date flag in Check Options
    if (document.forms[0].CheckDateType != null) {
        if (document.forms[0].CheckDateType.value == "readonly") {
            if (document.forms[0].checkdate != null) {
                document.forms[0].checkdate.readOnly = true;
                document.forms[0].checkdate.style.backgroundColor = "silver";
            }
            if (document.forms[0].checkdatebtn != null) {
                document.forms[0].checkdatebtn.style.display = "none";
            }
        }
    }
    //end - pmittal5
    window.document.forms[0].functiontocall.value = ""; //RMA-10209 : Automatically  all checks gets moved to print check batch
    // RMA-10491 - Start : bkuzhanthaim :ShowModalDialog Issue in Chrome : PrintBatch method logic
    var IEbrowser = false || !!document.documentMode; //work upto IE6;
    var PrintBatchOk = window.document.forms[0].hdnPrintBatchOk.value;
    window.document.forms[0].hdnPrintBatchOk.value = "";
    //if (window.parent.document.forms[0].hdnPrintBatchOk.value == "Processed")
    //    window.parent.document.forms[0].submit();
    if (!IEbrowser && PrintBatchOk != "") {
        var arrCheckDetail = PrintBatchOk.split("||");
        var iStartNum = arrCheckDetail[0];
        var iEndNum = arrCheckDetail[1];
        var sFileNameAndType = arrCheckDetail[2];
        var sPrintCheckDetails = arrCheckDetail[3];
        var sPrintMode = arrCheckDetail[4];
        var hdnZeroChecksValue = 0;
        if (arrCheckDetail.length >= 6) 
            hdnZeroChecksValue = arrCheckDetail[5];
        window.document.forms[0].PrintCheckDetails.value = sPrintCheckDetails;
        window.document.forms[0].FileNameAndType.value = sFileNameAndType;
        if (sPrintMode != "PR" && hdnZeroChecksValue != "3") {
                var bdialogresult = confirm("You have printed checks in the range " + iStartNum + " through " + iEndNum + ". Press 'Ok' to record all of checks as having printed successfully. Press 'Cancel' if check printing did not print all checks correctly.");
                if (bdialogresult) {
                    // Checks has been printed Successfully. Call the web service to update the Check Status. 	
                    window.document.forms[0].FirstFailedCheckNumber.value = "0";
                    //window.document.forms[0].hdnRequest.value = "";
                    window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.UpdateStatusForPrintedChecks";
                    window.document.forms[0].method = "post";
                    window.document.forms[0].submit();
                }
                else {
                    // All Checks does not print successfully. Ask the check number, up to where the checks has printed successfully.
                    width = 475;
                    height = 280;
                    sQueryString = "";
                    sQueryString = "StartNum=" + iStartNum + "&EndNum=" + iEndNum;
                    window.open("ChecksFailedToPrint.aspx?" + sQueryString, self, 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no');
                    showOverlayPopup();
                }
        }
    }
    // RMA-10491 - End : bkuzhanthaim :ShowModalDialog Issue in Chrome
}

function PrintPre() {
    window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.GetInsufficientFundsPreEditFlag";
    //window.document.forms[0].action.value = "PageRefresh";
    window.document.forms[0].method = "post";
    window.document.forms[0].submit();
}

function PrintPost() {
    if (window.document.forms[0].detail.checked)
        window.document.getElementById("functiontocall").value = "PrintChecksAdaptor.PostCheckDetail";
    if (window.document.forms[0].summary.checked)
        window.document.getElementById("functiontocall").value = "PrintChecksAdaptor.PostCheckSummary";
    if (window.document.forms[0].subaccount.checked)
        window.document.getElementById("functiontocall").value = "PrintChecksAdaptor.PostCheckSubAccount";
    document.forms[0].hdnShowPleaseWait.value = "-1";
    //window.document.forms[0].action.value = "PageRefresh";
    window.document.forms[0].method = "post";
    window.document.forms[0].submit();
}

function PrintBatch() {
    var iFirstCheck = window.document.forms[0].firstcheck.value;

    if (isNaN(iFirstCheck) || iFirstCheck.indexOf(".") != -1) {
        alert("Invalid First Check Number ");
        window.document.forms[0].firstcheck.focus();
        window.document.forms[0].functiontocall.value = "";
        return false;
    }
    // MITS 33157 mcapps2 Start
    //if (window.document.forms[0].fileandconsolidate.value == "True") {
    //    if (!confirm("The rollup feature cannot be used in conjunction with the option to print the checks to the printer and a file.  This system will not use the rollup feature for this check batch.")) {
    //        window.document.forms[0].functiontocall.value = "";
    //        return false;
    //    }
    //}
    // MITS 33157 mcapps2 End
    var iBatchId = window.document.forms[0].checkbatchpre.value;
    var iAccountId = window.document.forms[0].bankaccountprint.value;
    var sCheckDate = window.document.forms[0].checkdate.value;
    var bIncludeAutoPayments = window.document.forms[0].includeautopayments.checked;
    var sSelectedAutoChecksIds = window.document.forms[0].selectedautochecksids.value;
    var sUsingSelection = window.document.forms[0].selectchecks.checked;
    var iCheckStockId = window.document.forms[0].checkstock.value;
    //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010
    var sorghierarchyprint = window.document.forms[0].orghierarchypre_lst.value;
    var sorghierarchylevelprint = window.document.forms[0].orghierarchylevelpre.value;
    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010

    var sOrderBy = window.document.forms[0].orderfieldbatch.value;

    var iNumAutoChecksToPrint = 0;

    if (sUsingSelection && sSelectedAutoChecksIds != "") {
        arrTemp = sSelectedAutoChecksIds.split(" ");
        iNumAutoChecksToPrint = arrTemp.length;
    }
    else
        iNumAutoChecksToPrint = 0;

    var width = 475;
    var height = 280;
    var sQueryString = "";
    //skhare7 R8 enhancement
    var bIncludeCombinedPayments = window.document.forms[0].includecombinedpayments.checked;

    //JIRA:438 START: ajohari2
    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //var bchkEFTPayment = 'false';
    //if (window.document.forms[0].chkEFTPayment != null)
    //    bchkEFTPayment = window.document.forms[0].chkEFTPayment.checked;

    var iDistributionType = window.document.forms[0].ddlDistributionTypePrint.value;

    sQueryString = "BatchId=" + iBatchId + "&AccountId=" + iAccountId + "&CheckDate=" + sCheckDate
					+ "&IncludeAutoPayments=" + bIncludeAutoPayments + "&CheckStockId=" + iCheckStockId
					+ "&FirstCheck=" + iFirstCheck + "&OrderBy=" + sOrderBy
					+ "&NumAutoChecksToPrint=" + iNumAutoChecksToPrint + "&UsingSelection=" + sUsingSelection
		            + "&OrgHierarchyPrint=" + sorghierarchyprint + "&OrgHierarchyLevelPrint=" + sorghierarchylevelprint + "&includecombinedpays=" + bIncludeCombinedPayments + /*"&EFTPayment=" + bchkEFTPayment +*/ "&DistributionType=" + iDistributionType;
    //JIRA:438 End: 
    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010
    //skhare7 R8 End
    //var returnval = showModalDialog("home?pg=riskmaster/PrintChecks/PrintChecksBatchFrame&" + sQueryString, null, "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:yes;help:no;center:yes;");
    // RMA-10491 - Start : bkuzhanthaim :ShowModalDialog Issue in Chrome
    var IEbrowser = false || !!document.documentMode; 
    if (!IEbrowser) {
        popUpWin = window.open("PrintChecksBatch.aspx?" + sQueryString, self, 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=no');    
        // showOverlayPopup();
        return false;
    }
    else
        var returnval = showModalDialog("PrintChecksBatch.aspx?" + sQueryString, self, "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:yes;help:no;center:yes;");

    if (returnval == null || returnval == "||||||" || returnval == "")
        return false;

    var arrCheckDetail = returnval.split("||");
    var iStartNum = arrCheckDetail[0];
    var iEndNum = arrCheckDetail[1];
    var sFileNameAndType = arrCheckDetail[2];
    var sPrintCheckDetails = arrCheckDetail[3];
    var sPrintMode = arrCheckDetail[4];

    //window.document.forms[0].PrintCheckDetails.value = sPrintCheckDetails.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;');//Reverting : 35827 bkuzhanthaim
    //tanwar2 - mits 30910 - start
    var hdnZeroChecksValue = 0;
    if (arrCheckDetail.length >= 6) {
        hdnZeroChecksValue = arrCheckDetail[5];
    }
    //tanwar2 - mits 30910 - end
    window.document.forms[0].PrintCheckDetails.value = sPrintCheckDetails;
    window.document.forms[0].FileNameAndType.value = sFileNameAndType;
    // skhare7 MITS 23664
    if (sPrintMode != "PR") { // skhare7 MITS 23664 End
        //tanwar2 - mits 30910 - added condtion - start
        if (hdnZeroChecksValue != "3") {
        if (confirm("You have printed checks in the range " + iStartNum + " through " + iEndNum + ". Press 'Ok' to record all of checks as having printed successfully. Press 'Cancel' if check printing did not print all checks correctly.")) {
            // Checks has been printed Successfully. Call the web service to update the Check Status. 	
            window.document.forms[0].FirstFailedCheckNumber.value = "0";
        }
        else {
            // All Checks does not print successfully. Ask the check number, up to where the checks has printed successfully.
            width = 475;
            height = 280;
            sQueryString = "";

            sQueryString = "StartNum=" + iStartNum + "&EndNum=" + iEndNum;

            var FirstFailedCheckNumber = showModalDialog("ChecksFailedToPrint.aspx?" + sQueryString, null, "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");

            if (FirstFailedCheckNumber == "") {
                // Checks has been printed Successfully. Call the web service to update the Check Status.
                window.document.forms[0].FirstFailedCheckNumber.value = "0";
            }
            else {
                window.document.forms[0].FirstFailedCheckNumber.value = FirstFailedCheckNumber;
            }
        }

        }
        window.document.forms[0].functiontocall.value = "PrintChecksAdaptor.UpdateStatusForPrintedChecks";
        //window.document.forms[0].action.value = "PageRefresh" ;
        window.document.forms[0].submit();
        //OnFormSubmitPrintCheck();	
    } 
}

function PrintEOB() {
    var iAccountId = window.document.forms[0].bankaccountpost.value;
    var iBatchNumber = window.document.forms[0].checkbatchpost.value;
    var sPostCheckDate = window.document.forms[0].postcheckdate.value;
    var sOrderBy = window.document.forms[0].orderfieldpost.value;
    // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
    // This change has caused the change in PrintEOB as well
    var iDistributionType = window.document.forms[0].ddlDistributionTypePost.value;
    // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
    // This change has caused the change in PrintEOB as well
    var width = 460;
    var height = 260;
    var sQueryString = "";

    if (sOrderBy == "Check Total")
        sOrderBy = "FUNDS.AMOUNT";
    else if (sOrderBy == "Claim Number")
        sOrderBy = "FUNDS.CLAIM_NUMBER";
    else if (sOrderBy == "Control Number")
        sOrderBy = "FUNDS.CTL_NUMBER";
    else if (sOrderBy == "Payee Name")
        sOrderBy = "FUNDS.LAST_NAME,FUNDS.FIRST_NAME";
    else if (sOrderBy == "Transaction Date")
        sOrderBy = "FUNDS.TRANS_DATE";
    else if (sOrderBy == "Check Number")
        sOrderBy = "FUNDS.TRANS_NUMBER";
    else if (sOrderBy == "Sub Bank Account")
        sOrderBy = "BANK_ACC_SUB";
    else if (sOrderBy == "Payer Level")
        sOrderBy = "PAYER_LEVEL";
    else if (sOrderBy == "Current Adjuster")
        sOrderBy = "ADJUSTER_EID";
    else
        sOrderBy = "";

    // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
    // This change has caused the change in PrintEOB as well
    sQueryString = "accountid=" + iAccountId + "&batchnumber=" + iBatchNumber + "&postcheckdate=" + sPostCheckDate + "&orderby=" + sOrderBy + "&distributiontype=" + iDistributionType;
    // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
    // This change has caused the change in PrintEOB as well
	//var wnd = window.open( "home?pg=riskmaster/PrintChecks/PostCheckEOB&amp;" + sQueryString , 'PostRegisterCheckBatchEOBReports' , 'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=no,scrollbars=yes');
    //var wnd = window.open("PostCheckEob.aspx?" + sQueryString, 'PostRegisterCheckBatchEOBReports', 'width=' + width + ',height=' + height + ',top=' + (screen.availHeight - height) / 2 + ',left=' + (screen.availWidth - width) / 2 + ',resizable=no,scrollbars=yes');
    // aravi5 RMA-10209 Print Checks - ShowModalDialog Issues starts
    // var returnval = showModalDialog("PostCheckEob.aspx?" + sQueryString, "PostRegisterCheckBatchEOBReports", "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:yes;help:no;center:yes;");
    if (get_browserName() == "IE") {
        var returnval = showModalDialog("PostCheckEob.aspx?" + sQueryString, "PostRegisterCheckBatchEOBReports", "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:yes;help:no;center:yes;");
    }
    else {
        var returnval = window.open("PostCheckEob.aspx?" + sQueryString, "PostRegisterCheckBatchEOBReports", "height=" + height + ",width=" + width + ",resizable=no,status=no,scroll=yes,help=no,center=yes;");
        showOverlay();
    }
    // aravi5 RMA-10209 Print Checks - ShowModalDialog Issues ends
    if (returnval != null && returnval != "") {
        window.document.forms[0].functiontocall.value = "PrintEOBAdaptor.DeleteEobFiles";
        window.document.forms[0].EOBFilesNames.value = returnval;
    }
    else {
        window.document.forms[0].functiontocall.value = "";
    }
}

//use this function to view source code on browser window created with showModalDialog()
function viewSource() {
    //<input type="button" value="View Source" onclick="viewSource()" />
    d = window.open();
    d.document.open('text/plain').write(document.documentElement.outerHTML);
}

function SortChecksList(orderBy) {

    document.body.style.cursor = 'wait';
    document.styleSheets[0].rules[0].style.cursor = 'wait';

    var orderByDirection = 'ascending';  //the default sort direction

    //toggle direction when orderby is repeated
    if (document.forms[0].orderby.value == orderBy) {
        orderByDirection = (document.forms[0].orderbydirection.value == 'ascending' ? 'descending' : 'ascending')
    }
    document.forms[0].orderbydirection.value = orderByDirection;

    document.forms[0].orderby.value = orderBy;

    document.forms[0].submit();

    return true;
}

function OpenChecksList() {
    var sfromdate = window.document.forms[0].fromdate.value;
    var stodate = window.document.forms[0].todate.value;
    var sfromdateflag = window.document.forms[0].fromdateflag.checked;
    var stodateflag = window.document.forms[0].todateflag.checked;
    var bincludeautopayments = window.document.forms[0].includeautopayments.checked;
    //skhare7 R8 enhancement
    var bincludecombinedpayments = window.document.forms[0].includecombinedpayments.checked;
    //skhare7 R8 end
    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //JIRA:438 START: ajohari2
    //var bchkEFTPayment = 'false';
    //if (window.document.forms[0].chkEFTPayment != null)
    //    bchkEFTPayment = window.document.forms[0].chkEFTPayment.checked;
    //JIRA:438 End: 
    var iDistrbutionType = window.document.forms[0].ddlDistributionTypePre.value;
    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    var iaccountid = window.document.forms[0].bankaccountpre.value;
	//Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010
    var sorghierarchypre = window.document.forms[0].orghierarchypre_lst.value ;
	var sorghierarchylevelpre = window.document.forms[0].orghierarchylevelpre.value ;
	//End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010
    var sselectedchecksids = window.document.forms[0].selectedchecksids.value;
    var sselectedautochecksids = window.document.forms[0].selectedautochecksids.value;

    var orderBy = window.document.forms[0].orderfieldpre.value;
    var width = 950;
    var height = 505;
    var sQueryString = "";
    var PrintPreFlag = window.document.forms[0].printpre.disabled;

    arrTemp = sselectedchecksids.split(",");
    sselectedchecksids = arrTemp[0];
    for (var i = 1; i < arrTemp.length; i++)
        sselectedchecksids += " " + arrTemp[i];

    arrTemp = sselectedautochecksids.split(",");
    sselectedautochecksids = arrTemp[0];
    for (var i = 1; i < arrTemp.length; i++)
        sselectedautochecksids += " " + arrTemp[i];
    //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date: 03/17/2010 orghierarchypre and orghierarchylevelpre parameters added
    //Change by kuladeep for mits:22878 (these extra field in query string are not used,so removed for hang out RMX screen)
    //sQueryString = "fromdate=" + sfromdate + "&todate=" + stodate + "&fromdateflag=" + sfromdateflag + "&todateflag=" + stodateflag + "&includeautopayments=" + bincludeautopayments + "&accountid=" + iaccountid + "&orghierarchypre=" + sorghierarchypre + "&orghierarchylevelpre=" + sorghierarchylevelpre + "&selectedchecksids=" + sselectedchecksids + "&selectedautochecksids=" + sselectedautochecksids + "&orderby=" + orderBy + "&includecombinedpays=" + bincludecombinedpayments;				
    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
    //JIRA:438 START: ajohari2
    sQueryString = "fromdate=" + sfromdate + "&todate=" + stodate + "&fromdateflag=" + sfromdateflag + "&todateflag=" + stodateflag + "&includeautopayments=" + bincludeautopayments + "&accountid=" + iaccountid + "&orghierarchypre=" + sorghierarchypre + "&orghierarchylevelpre=" + sorghierarchylevelpre + "&orderby=" + orderBy + "&includecombinedpays=" + bincludecombinedpayments + /*"&EFTPayment=" + bchkEFTPayment +*/ "&DistributionType=" + iDistrbutionType;
    //JIRA:438 End:
    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
    // So introducing Distribution Type and removing out EFTPayment
   
    window.document.forms[0].printpre.disabled = true;
    window.document.forms[0].choose.disabled = true;

    //MITS 27576 hlv 4/25/2012 begin
    var checkedids = window.document.forms[0].selectedchecksids.value + "||"
                   + window.document.forms[0].selectedautochecksids.value;
    window.document.forms[0].hdnCheckedids.value = checkedids;
    //var returnval = showModalDialog("SelectChecksFrame.aspx?" + sQueryString, null, "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");
    // aravi5 added for RMA-7066 Starts
    // var returnval = showModalDialog("SelectChecksFrame.aspx?" + sQueryString, checkedids, "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");
    if (get_browserName() == "IE") {
        var returnval = showModalDialog("SelectChecksFrame.aspx?" + sQueryString, checkedids, "dialogHeight:" + height + "px;dialogWidth:" + width + "px;resizable:no;status:no;scroll:no;help:no;center:yes;");
        //MITS 27576 hlv 4/25/2012 end
        if (returnval != null && returnval != "") {
            var arrSelectedChecks = returnval.split("||");
            window.document.forms[0].selectedchecksids.value = arrSelectedChecks[0];
            window.document.forms[0].selectedautochecksids.value = arrSelectedChecks[1];
            //Add by kuladeep for mits:22878 Start---If user select all checks through selection in that case we pass flag only,no check ids
            window.document.forms[0].hdnAllCheckSelected.value = arrSelectedChecks[2];
            window.document.getElementById("functiontocall").value = "PrintChecksAdaptor.CheckSetChanged";
            //window.document.all("action").value = "PageRefresh";
            document.forms[0].submit();
            //OnFormSubmitPrintCheck();
        }
        else {
            window.document.forms[0].printpre.disabled = PrintPreFlag;
            window.document.forms[0].choose.disabled = false;
            window.document.forms[0].functiontocall.value = "";
        }
    }
    else {
        var returnval = window.open("SelectChecksFrame.aspx?" + sQueryString, checkedids, "height=" + height + ",width=" + width + ",resizable=no;status=no,scroll=no,help=no,center=yes;");
        if (window.parent.parent.document.getElementById("overlaydiv") == null) {
            $('#cphHeaderBody', window.parent.parent.document).prepend("<div id=\"overlaydiv\" class=\"overlay\">&nbsp;</div>");
            $('.overlay', window.parent.parent.document).show();
        }
    }
    // aravi5 added for RMA-7066 Ends
}

function SelectChecksOnLoad() {

    //MITS 27576 hlv 4/25/2012 begin
    var tmp = "";
    if (false || !!document.documentMode) //Check is IE Browser 
        tmp = window.dialogArguments;
    else
        tmp = window.opener.document.forms[0].hdnCheckedids.value;
    tmp = tmp.split("||");
    var checksids = "," + tmp[0] + ",";
    var autocheckids = "," + tmp[1] + ",";

    var bCheckBoxes = self.document.getElementsByTagName("input");

    for (var i = 0; i < bCheckBoxes.length - 1; i++) {
        if (bCheckBoxes[i].id == "SelectCheck" && checksids.indexOf("," + bCheckBoxes[i].value + ",") >= 0) {
            bCheckBoxes[i].checked = true;
        }

        //
        if (bCheckBoxes[i].id == "SelectAutoCheck" && autocheckids.indexOf("," + bCheckBoxes[i].value + ",") >= 0) {
            bCheckBoxes[i].checked = true;
        }
    }
    //MITS 27576 hlv 4/25/2012 end

//    var bCheckBoxes = document.all.tags("input");
//    var bAllSelected = true;

//    for (var i = 0; i < bCheckBoxes.length - 1; i++) {
//        if (bCheckBoxes[i].type == "checkbox" && bCheckBoxes[i].id != "ChkAll") {
//            if (!bCheckBoxes[i].checked) {
//                bAllSelected = false;
//                break;
//            }
//        }
//    }
//    window.document.forms[0].ChkAll.checked = bAllSelected;
//    document.body.style.cursor = 'default';
}

function SelectAllChecks(Id) {
    var bCheckBoxes = document.getElementsByTagName("input");
    var isSelected = document.getElementById(Id).checked;
    //Add by kuladeep for mits:22878 Start---If user select all checks through selection in that case we pass flag only,no check ids
    if (document.getElementById("hdnSelectAllCheck") != null) {
        document.getElementById("hdnSelectAllCheck").value = isSelected;
    }

    for (var i = 0; i < bCheckBoxes.length - 1; i++) {
        if (bCheckBoxes[i].type == "checkbox") {
            //bCheckBoxes[i].checked = window.document.forms[0].ChkAll.checked;
            bCheckBoxes[i].checked = isSelected;
        }
    }
}

//hlv MITS 23153 begin here
function GetAllCheckState() {
    var chbCount = 0, chbCheckCount = 0;

    var bCheckBoxes = document.getElementsByTagName("input");
    for (var i = 0; i < bCheckBoxes.length - 1; i++) {
        if (bCheckBoxes[i].type == "checkbox") {
            if (bCheckBoxes[i].value == "on") { continue; }

            chbCount++;
            if (bCheckBoxes[i].checked) {
                chbCheckCount++;
            }
            else {
                break;
            }
        }
    }

    return (chbCount == chbCheckCount);
}

function GetAllCheckElement() {
    var res = null;
    var bCheckBoxes = document.getElementsByTagName("input");
    for (var i = 0; i < bCheckBoxes.length - 1; i++) {
        if (bCheckBoxes[i].id.indexOf("ChkAll") >=0 && bCheckBoxes[i].type == "checkbox") {
            res = bCheckBoxes[i];
            break;
        }
    }
    return res;
}

function CheckClick() {
    var SelectCheck = event.srcElement;
    var CheckBoxAll = GetAllCheckElement();

    if (SelectCheck.checked) {
        CheckBoxAll.checked = GetAllCheckState();
    }
    else {
        CheckBoxAll.checked = false;
    }
}
//hlv MITS 23153 end
function SelectChecksOk() {
    var SelectCheck = "";
    var SelectAutoCheck = "";


    //Add validation by kuladeep for mits:22878 Start
    var arrTempChecksCount = 0;
    var arrTempChecks = "";
    var arrTempAutoCheckCount = 0;
    var arrTempAutoCheck = "";
    var iPrintCheckLimit = 0;
    var isAllCheckSelected = "";
    var isDeSelectAnyCheck = "";
     var countAllRecords = 0; //Added by kkaur8 for MITS 32166
    //Add validation by kuladeep for mits:22878 End
    var IEbrowser = false || !!document.documentMode; //RMA-10209
    var sReturnval = "";
    var bCheckBoxes = self.document.getElementsByTagName("input");
     var bGVSelectChecks = self.document.getElementById("gvSelectChecksFrame");
    countAllRecords = bCheckBoxes.length;
     var nullDateChecks = "";
     var iRowCount = 0;

    for (var i = 0; i < bCheckBoxes.length - 1; i++) {
        if (bCheckBoxes[i].id == "SelectCheck") {
            if (bCheckBoxes[i].checked == true) {
                if (SelectCheck != "")
                    SelectCheck += "," + bCheckBoxes[i].value;
                else
                    SelectCheck += bCheckBoxes[i].value;
            }
        }
        if (bCheckBoxes[i].id == "SelectAutoCheck") {
            if (bCheckBoxes[i].checked) {
                if (SelectAutoCheck != "")
                    SelectAutoCheck += "," + bCheckBoxes[i].value;
                else
                    SelectAutoCheck += bCheckBoxes[i].value;
            }
         }
         //rsushilaggar MITS 37250 10/30/2013
         if (bCheckBoxes[i].type == "checkbox" && (bCheckBoxes[i].id == "SelectCheck" || bCheckBoxes[i].id == "SelectAutoCheck")) {
             iRowCount++;
             //JIRA RMA-15717 ajohari2: innerTEXT is not supported in Firefox
             if (bCheckBoxes[i].checked && bGVSelectChecks.rows[iRowCount].cells[1].innerHTML.replace(/^\s+|\s+$/gm, '') == "") {

                 if (nullDateChecks != "")
                     nullDateChecks += ", " + bGVSelectChecks.rows[iRowCount].cells[2].innerHTML;
                 else
                     nullDateChecks = bGVSelectChecks.rows[iRowCount].cells[2].innerHTML;
             }
         }
     }

        if (nullDateChecks != "")
        {
            alert(nullDateChecks + " control number(s) are not having transaction date. Please set the transaction date or exclude them from print check list.");
            return false;
        }
    //end rsushilaggar MITS 37250 


    //Add validation by kuladeep for mits:22878 Start
    if (document.getElementById("hdnSelectAllCheck") != null) {
        isAllCheckSelected = document.getElementById("hdnSelectAllCheck").value;
    }
    for (var i = 0; i < bCheckBoxes.length - 1; i++) {
        if (bCheckBoxes[i].id == "SelectCheck") {
            if (bCheckBoxes[i].checked == false) {
                isDeSelectAnyCheck = "true";
            }
        }
        if (bCheckBoxes[i].id == "SelectAutoCheck") {
            if (bCheckBoxes[i].checked == false) {
                isDeSelectAnyCheck = "true";
            }
        }
    }
    arrTempChecks = SelectCheck.split(",");
    if (arrTempChecks.length > 1) {
        arrTempChecksCount = arrTempChecks.length;
    }
    arrTempAutoCheck = SelectAutoCheck.split(",");
    if (arrTempAutoCheck.length > 1) {
        arrTempAutoCheckCount = arrTempAutoCheck.length;
    }

    if (document.getElementById('hdnPrtChkLmtId') != null) {
        iPrintCheckLimit = document.getElementById('hdnPrtChkLmtId').value;
    }

    if (isDeSelectAnyCheck == "true") {
        isAllCheckSelected = "false";
    }
    else if ((isDeSelectAnyCheck == "false" || isDeSelectAnyCheck == "") && countAllRecords == arrTempAutoCheckCount) {
        isAllCheckSelected = "true";
    }
    else if ((isDeSelectAnyCheck == "false" || isDeSelectAnyCheck == "") && countAllRecords != arrTempAutoCheckCount) {
        isAllCheckSelected = "false";
    }



    if ((isAllCheckSelected == "false" || isAllCheckSelected == "") || (isAllCheckSelected == "true" && isDeSelectAnyCheck == "true")) {
        if (iPrintCheckLimit > 0) {
            if (arrTempChecksCount + arrTempAutoCheckCount > iPrintCheckLimit) {
                alert("You can select either all items or not more than " + iPrintCheckLimit + " separate items!");
                return false;
            }
        }
    }
    //window.returnValue = SelectCheck + "||" + SelectAutoCheck;
    sReturnval = SelectCheck + "||" + SelectAutoCheck + "||" + isAllCheckSelected;
    //Add validation by kuladeep for mits:22878 End

    //RMA-10209 : Start : aravi5 All checks gets moved to print check batch in Chrome : OpenChecksList method logic
    if (IEbrowser) {
        window.returnValue = sReturnval;
    }
    else {
        if (sReturnval != null && sReturnval != "") {
            window.opener.document.forms[0].selectedchecksids.value = SelectCheck;
            window.opener.document.forms[0].selectedautochecksids.value = SelectAutoCheck;
            //Add by kuladeep for mits:22878 : If user select all checks through selection in that case we pass flag only,no check ids
            window.opener.document.forms[0].hdnAllCheckSelected.value = isAllCheckSelected;
            window.opener.document.getElementById("functiontocall").value = "PrintChecksAdaptor.CheckSetChanged";
            window.opener.document.forms[0].submit();
        }
        else {
            window.opener.document.forms[0].printpre.disabled = PrintPreFlag;
            window.opener.document.forms[0].choose.disabled = false;
            window.opener.document.forms[0].functiontocall.value = "";
        }
    }
    //RMA-10209 : End
    window.close();
    return false;
}

function SelectChecksCancel() {
    self.close();
    return false;
}

function PrintEOBOk() {
    self.close();
}

function PrintBatchOk() {
    //tanwar2 - mits 30910 - start
    var hdnZeroChecks = document.getElementById('hdnZeroChecks');
    if (hdnZeroChecks != null && hdnZeroChecks.value == "2") {
        hdnZeroChecks.value = "1";
    }
    //tanwar2 - mits 30910 - end
    window.close();
    return false;
}

function PrintBatchOnClose() {
    //tanwar2 - mits 30910 - start

    var ZeroChecks = 0;
    var hdnZeroChecks = document.getElementById('hdnZeroChecks');
    if (hdnZeroChecks != null) {
        ZeroChecks = hdnZeroChecks.value;
    }
    // skhare7 MITS 23664
    //window.returnValue = window.document.frmData.StartNum.value + "||" + window.document.frmData.EndNum.value + "||" + window.document.frmData.hdnFileNameAndType.value + "||" + window.document.frmData.hdnPrintCheckDetails.value + "||" + window.document.frmData.hdnPrintMode.value;
    var returnValue = window.document.forms[0].StartNum.value + "||" + window.document.forms[0].EndNum.value + "||" + window.document.forms[0].hdnFileNameAndType.value + "||" + window.document.forms[0].hdnPrintCheckDetails.value + "||" + window.document.forms[0].hdnPrintMode.value + "||" + ZeroChecks;

    //bkuzhanthaim : RMA-10491 : ShowModalDialog Issue in Chrome
    var IEbrowser = false || !!document.documentMode; //work upto IE6;
    if (IEbrowser) {
        window.returnValue = returnValue;
    }
    else {
        // RMA-10491 :- Maintenance : Policy Billing(PrintDisbursement) And Funds: Print Checks(PrintChecksBatch)
        window.opener.document.forms[0].hdnPrintBatchOk.value = returnValue;
        window.opener.document.forms[0].functiontocall.value = ""; //RMA-10209 : Automatically  all checks gets moved to print check batch
        //RMA-10491 : Page is submited  onload
        if (document.forms[0].hdnPostBack.value == "True") window.opener.document.forms[0].submit();
    }
    //tanwar2 - mits 30910 - End
    return false;
    // skhare7 MITS 23664 End
}

function ChecksFailedToPrintOk() {
    //tanwar2 - JIRA 5007 - start
    if (document.getElementById('hdnOkCancelClick') != null) {
        document.getElementById('hdnOkCancelClick').value = "1";
    }
    //tanwar2 - JIRA 5007 - end

    var iStartNum = window.document.forms[0].StartNum.value;
    var iEndNum = window.document.forms[0].EndNum.value;
    var iFirstFailedCheckNumber = window.document.forms[0].txtFirstFailedCheckNumber.value;
    var IEbrowser = false || !!document.documentMode; // RMA-10491

    if (isNaN(iFirstFailedCheckNumber) || (iStartNum > iFirstFailedCheckNumber) || (iFirstFailedCheckNumber > iEndNum)) {
        alert("Invalid Check Number ");
        return false;
    }
    // RMA-10491 : bkuzhanthaim :ShowModalDialog Issue in Chrome
    if (iFirstFailedCheckNumber == "") iFirstFailedCheckNumber = "0";// Checks has been printed Successfully. Call the web service to update the Check Status.
    if (IEbrowser)
        window.returnValue = iFirstFailedCheckNumber;
    else
        window.opener.document.forms[0].FirstFailedCheckNumber.value = iFirstFailedCheckNumber;
    window.close();
    return false;
}

function ChecksFailedToPrintCancel() {
    //tanwar2 - JIRA 5007 - start
    if (document.getElementById('hdnOkCancelClick') != null) {
        document.getElementById('hdnOkCancelClick').value = "1";
    }
    //tanwar2 - JIRA 5007 - end
    window.close();
    return false;
}

function PrintSelectedReport( type,name )
{
    document.forms[0].hdnFileRequest.value = "1" ;
    document.getElementById('file').src = "PrintCheckDownload.aspx?name=" + name + "&type=" + type;
    pleaseWait.Show();
    checkReadyState();		    
}

function UncheckOthers(iId) {
    if (iId.toString() == "detail") {
        window.document.forms[0].summary.checked = false;
        window.document.forms[0].subaccount.checked = false;
    }
    if (iId.toString() == "summary") {
        window.document.forms[0].detail.checked = false;
        window.document.forms[0].subaccount.checked = false;
    }
    if (iId.toString() == "subaccount") {
        window.document.forms[0].detail.checked = false;
        window.document.forms[0].summary.checked = false;
    }
}

function PrintEOBOnClose() {
    window.returnValue = window.document.forms[0].hdnFileNames.value; 
    return false;
}

//pmittal5 Mits 17988 10/07/09 - Check Date cannot be a Past Date
function validateCheckDate(sCtrlName) {
    var objFormElem = eval('document.forms[0].' + sCtrlName);
    if (objFormElem != null) {
        if (objFormElem.value != "") {
            var checkDate = new Date(objFormElem.value);
            var currentDate = new Date();
            currentDate.setHours(00, 00, 00, 00);
            if (checkDate < currentDate) {
                alert("Check Date cannot be prior to Current Date.");
                objFormElem.value = "";
                return false;
            }
        }
    }
}
