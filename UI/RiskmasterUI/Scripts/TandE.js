var child_window;
var inv_window;
var table_window;
var m_codeWindow;
var m_ctname;
var m_DataChanged = false; //Arnab: MITS-10682 - Added global variable for checking whether the data is changed or not
var m_InvoiceAmt = 0;
var m_isallocated;
var m_itemAmount = 0;
var m_LookupBusy = false;
var m_sFieldName;

try {
    var newwin = null;
}
catch (e) {
    alert(e);
}

function ValidateRateTableEntry() {
    var RateTable = window.document.forms[0].ratetable.value;
    var RateTableId = window.document.forms[0].ratetableid.value;

    if (RateTable == '') {
        alert("Select Rate Table");
        return false;
    } else {
        window.document.forms[0].h_ratetable.value = RateTable;
    }

    if (RateTableId == '') {
        alert("Select Rate Table");
        return false;
    } else {
        window.document.forms[0].h_ratetableid.value = RateTableId;
    }

    return true;
}

//sLinkTo is typically TimeAndExpenseInvoiceDetails.aspx (called from TimeAndExpense.aspx)
function formHandlerForTAndEDetails(sLinkTo, iRowId, iClaimId, iInvoiceId, iClaimNum) {
    //pleaseWait.Show('start');

    window.open(sLinkTo + "?RowId=" + iRowId + "&ClaimId=" + iClaimId + "&InvoiceId=" + iInvoiceId + "&ClaimNum=" + iClaimNum,
                'TandE', "width=750,height=500" + ",top=" +
                (screen.availHeight - 490) / 2 + ",left=" +
                (screen.availWidth - 740) / 2 + ",resizable=yes,scrollbars=yes");
}

//sLinkTo is typically TimeAndExpenseDetails.aspx (called from TimeAndExpenseInvoice.aspx)
function formHandlerForTAndEInvoice(sLinkTo, sAction, sRowId, sRateTableId, sIsAllocated) {
    ////debugger;
    //pleaseWait.Show('start');

    var ClaimNum = document.getElementById('clmNumber').value;
    var Vendor = document.getElementById('vendor').value;
    var VendorId = document.getElementById('vendor_cid').value;
    var InvoiceNumber = document.getElementById('invNumber').value;
    //rupal:start, multicurrency
    //m_InvoiceAmt = document.getElementById('invAmt').value;
    MultiCurrencyToDecimal_TandE(document.getElementById('invAmt'));
    m_InvoiceAmt = document.getElementById('invAmt').getAttribute("Amount");
    //MultiCurrencyOnBlur_TandE(document.getElementById('invAmt'));
    //rupal:end
    var sInvoiceDate = document.getElementById('invDate').value;
    sInvoiceDate = replace(sInvoiceDate, "/", "");
    var InvoiceDate = sInvoiceDate.substring(4, 8) + sInvoiceDate.substring(0, 2) + sInvoiceDate.substring(2, 4);
    //rupal:start, multicurrency
    //var AmtAvailable = (parseFloat(document.getElementById('invAmt').value) - parseFloat(document.getElementById('totavailable').value));
    MultiCurrencyToDecimal_TandE(document.getElementById('totavailable'));
    var AmtAvailable = (parseFloat(m_InvoiceAmt) - parseFloat(document.getElementById('totavailable').getAttribute("Amount")));
    //MultiCurrencyOnBlur_TandE(document.getElementById('totavailable'));

    MultiCurrencyToDecimal_TandE(document.getElementById('totavailable'));
    var TotalAvailable = parseFloat(document.getElementById('totavailable').getAttribute("Amount"));
    //MultiCurrencyOnBlur_TandE(document.getElementById('totavailable'));
    //rupal:end
    document.getElementById('hCurrentRowId').value = sRowId;

    if (table_window != null) {
        table_window.close();
    }

    table_window = window.open(sLinkTo + "?ctlnumber=" + sAction + "&rowid=" + sRowId + "&IsAllocated=" + sIsAllocated +
                               '&claimnum=' + ClaimNum + '&invnum=' + InvoiceNumber + '&invtot=' + m_InvoiceAmt,
                               'TandE', "width=820,height=550,resizable=yes,scrollbars=yes,top=" +
                               (screen.availHeight - 540) / 2 + ",left=" +
                               (screen.availWidth - 810) / 2);
}

function formHandler1(sLinkTo, sParms, sEnableForNew, sType, trackid) {
    //pleaseWait.Show('start');
    //debugger;
    var sLinkParams = new String();


    if (sType == "swindow") {
        var NextPageLink = sLinkTo;
        var ClaimNum = document.getElementById('clmNumber').value;
        var Vendor = document.getElementById('vendor').value;
        var VendorId = document.getElementById('vendor_cid').value;
        var InvoiceNumber = document.getElementById('invNumber').value;
        //rupal:start,multicurrency
        //m_InvoiceAmt = document.getElementById('invAmt').value;
        MultiCurrencyToDecimal_TandE(document.getElementById('invAmt'));
        m_InvoiceAmt = document.getElementById('invAmt').getAttribute("Amount");
        //MultiCurrencyOnBlur_TandE(document.getElementById('invAmt'));
        //rupal:end
        var sInvoiceDate = document.getElementById('txtfdate').value;
        sInvoiceDate = replace(sInvoiceDate, "/", "");
        var InvoiceDate = sInvoiceDate.substring(4, 8) + sInvoiceDate.substring(0, 2) + sInvoiceDate.substring(2, 4);
        //rupal:start, multicurrency
        //var AmtAvailable = (parseFloat(document.getElementById('invAmt').value) - parseFloat(document.getElementById('totavailable').value));
        MultiCurrencyToDecimal_TandE(document.getElementById('totavailable'));
        var AmtAvailable = (parseFloat(m_InvoiceAmt) - parseFloat(document.getElementById('totavailable').getAttribute("Amount")));
        var TotalAvailable = parseFloat(document.getElementById('totavailable').getAttribute("Amount"));
        //MultiCurrencyOnBlur_TandE(document.getElementById('totavailable'));
        //rupal:end
        // MAC : Some data does not seem to be passing correctly to the details form, adding this code to rectify it :
        var sTransType = document.getElementById('hCurrentTransType').value;
        var sRowId = document.getElementById('hCurrentRowId').value;
        var sRate = document.getElementById('hCurrentRate').value;
        var sUnit = document.getElementById('hCurrentUnit').value;
        var sIsBillable = document.getElementById('hCurrentIsBillable').value;
        var sItemAmount = document.getElementById('hCurrentItemAmount').value;

        if (table_window != null) {
            table_window.close();
        }

        //table_window = window.open(sLinkTo, 'TandE', 'width=650,height=450' + ',top=' +
        //                           (screen.availHeight - 290) / 2 + ',left=' + 
        //                           (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');

        //Starts by Nitin for Mits 12317 , pass isallocated value in query string when form is 'Time and Expense Invoice'

        if (m_isallocated != null) {
            PageQuery = NextPageLink + "?ctlnumber=" + sEnableForNew + "&rowid=" + sRowId + "&IsAllocated=" + m_isallocated ,
		                'TandE', 'width=750,height=500,resizable=yes,scrollbars=yes,' +
		                'toolbar=no,location=no,directories=no,status=no,menubar=no,left=' +
		                (screen.availWidth - 740) / 2 + ",top=" +
		                (screen.availHeight - 490) / 2;
            table_window = window.open(PageQuery);
            return false;
        }
        else {
            PageQuery = NextPageLink + "?ctlnumber=" + sEnableForNew + "&trackid=" + trackid,
		                'TandE', 'width=750,height=500,resizable=yes,scrollbars=yes,' +
		                'toolbar=no,location=no,directories=no,status=no,menubar=no,left=' +
		                (screen.availWidth - 740) / 2 + ",top=" +
		                (screen.availHeight - 490) / 2;
            table_window = window.open(PageQuery);
            return false;
        }
        //Ends by Nitin for Mits 12317
    }

    if (sType == "xxswindow") {
        if (table_window != null) {
            table_window.close();
        }

        table_window = window.open(sLinkTo, 'TandE', 'width=400,height=300' + ',top=' +
		                           (screen.availHeight - 290) / 2 + ',left=' +
		                           (screen.availWidth - 390) / 2 + ',resizable=yes,scrollbars=yes');
    }

    if (sType == "childwindow") {
        if (child_window != null) {
            child_window.close();
        }

        child_window = window.open(sLinkTo, 'RatesandUnits', 'width=500,height=200,top=' +
		                           (screen.availHeight - 190) / 2 + ',left=' +
		                           (screen.availWidth - 490) / 2 + ',resizable=yes,scrollbars=yes');
    }

    //Arnab: MITS-10682 - Modified existing condition for sending 'LookUpText' value to the server
    if (sType == "childwindow1") {
        //Taking the text from textbox(MITS-10682)
        var sLookUpText;
        var IsAllocated;
        var RateTableId;

        //added:Yukti,Dt:12/20/2013,MITS 34717
        //IsAllocated = window.opener.forms[0].h_allocated.value;
        //RateTableId = window.opener.forms[0].h_ratetableid.value;
        IsAllocated = window.opener.document.getElementById("h_allocated").value;
        RateTableId = window.opener.document.getElementById("h_ratetableid").value;

        if (m_DataChanged == true) {
            sLookUpText = document.forms[0].txtTransType.value;
        }
        else {
            sLookUpText = "";
        }

        if (child_window != null) {
            child_window.close();
        }
        //rupal:multicurrency
        var sPmtCurrCodeId = 0;

        if (window.opener.document.getElementById('currencytypetext_codelookup_cid') != null) {
            sPmtCurrCodeId = window.opener.document.getElementById('currencytypetext_codelookup_cid').value;
        }
        //rupal:end
        //child_window = window.open(sLinkTo + "&amp;rateid=" + window.opener.forms[0].h_ratetableid.value +
        //                           "&amp;txtLookUp=" + sLookUpText, 'RatesandUnits',
        //                           'width=500,height=200' + ',resizable=yes,scrollbars=yes');

        // akaushik5 Changed for RMA-19193 Starts
        //child_window = window.open(sLinkTo + "?rateid=" + RateTableId + "&allocated=" + IsAllocated + "&txtLookUp=" + sLookUpText + "&PmtCurrCodeId=" + sPmtCurrCodeId + "&ClaimNum=" + ClaimNum,
        var ClaimNum = "";
        if (document.getElementById('txtClaimnumber') != null) {
            ClaimNum = document.getElementById('txtClaimnumber').value;
        }

        child_window = window.open(sLinkTo + "?rateid=" + RateTableId + "&allocated=" + IsAllocated + "&txtLookUp=" + sLookUpText + "&PmtCurrCodeId=" + sPmtCurrCodeId + "&ClaimNum=" + ClaimNum,
        // akaushik5 Changed for RMA-19193 Ends
                                   'RatesandUnits', 'width=600,height=200,top=' +
		                           (screen.availHeight - 190) / 2 + ',left=' +
		                           (screen.availWidth - 590) / 2 + ',resizable=yes,scrollbars=yes');

        m_DataChanged = false; //Changed the status of flag so that it doesn't get clicked everytime(MITS-10682)
        //MITS-10682 - End
    }

    if (sType == "close") {
        window.close();
    }

    if (sType == "back") {
        window.history.back();
    }

    if (sType == "save") {
        if (sEnableForNew == 'invoice') {
            //MITS-9742 - Edited the alert message
            if (checkfields('req_fields', 'Not all required fields contain the value. Please enter the data into all underlined fields.', 'nullcheck') == true) {
                saveInvoice();
            }
        }

        if (sEnableForNew == 'ratetable') {
            saveRatetable();
        }
        //window.close();
    }

    if (sType == "normal") {
        if (sEnableForNew == 'invoice') {
            var claimid = document.getElementById('claimid').value;
            //var slink = 'invoices.asp?claimid=' + claimid;
            sLinkTo = sLinkTo + '&claimid=' + claimid;
        }

        if (sEnableForNew == 'saverateinfo') {
            var ratetable = document.getElementById('h_ratetable').value;
            var ratetableid = document.getElementById('h_ratetableid').value;
            sLinkTo = sLinkTo + '&RateTable=' + ratetable + '&RateTableId=' + ratetableid;
        }

        self.document.location.href = sLinkTo;
    }

    return false;
}

function setClaimRateTable(RateTableName, TableId) {
    //Ashish Ahuja : Mits 31897 Start
    RateTableName = String(RateTableName).replace("^#$", "'");
    //Ashish Ahuja : Mits 31897 End
    //Start by Nitin for Mits 12406
    RateTableName = String(RateTableName).replace(/&apos;/g, "'");
    RateTableName = String(RateTableName).replace(/`/g, "`'");
    RateTableName = String(RateTableName).replace(/`/g, "");
    RateTableName = String(RateTableName).replace(/&amp;/g, '&'); // Mits 35421
    //End by Nitin for Mits 12406
    //Added by Yukti,Dt:12/20/2013,MITS 34717
    //window.opener.forms[0].ratetable.value = RateTableName;
    //window.opener.forms[0].ratetableid.value = TableId;
    //window.opener.forms[0].hcustid.value = window.forms[0].hcustid.value;
    //window.opener.forms[0].hclmnum.value = window.forms[0].hclmnum.value;
    //window.opener.forms[0].hclmnumtag.value = window.forms[0].hclmnum.value;
    window.opener.document.getElementById("ratetable").value = RateTableName;
    window.opener.document.getElementById("ratetableid").value = TableId;
    window.opener.document.getElementById("hcustid").value = window.document.getElementById("hcustid").value;
    window.opener.document.getElementById("hclmnum").value = window.document.getElementById("hclmnum").value;
    window.opener.document.getElementById("hclmnumtag").value = window.document.getElementById("hclmnum").value;
    window.close();
}

function checkfields(fName, fmessage, valType) {
    var str = 'document.forms[0].';

    if (valType == 'nullcheck') {
        str = eval(str + fName);

        if (str.value != "") {
            var s = new String(str.value);
            var arr = s.split("|");
            var obj = null;
            var i;
            var c = arr.length;

            for (i = 0; i <= c - 1; i++) {
                obj = eval('document.forms[0].' + arr[i]);

                if (replace(obj.value, " ", "") == "") {
                    alert(obj.name + ' : ' + fmessage);
                    obj.focus();
                    return false;
                }
            }
        }
        return true;
    }
}

function select_option(id1, ctname) {
    m_ctname = ctname;

    if (table_window != null) {
        table_window.close();
    }

    table_window = window.open('home?pg=riskmaster/Search/MainPage&amp;formname=entity' + '&amp;TableId=' + id1, 'Codes',
                               'width=600,height=500,top=' +
	                           (screen.availHeight - 490) / 2 + ',left=' +
	                           (screen.availWidth - 590) / 2 + ',resizable=yes,scrollbars=yes');
}

function fillInvDetail() {
    //debugger;
    var varAvail = 0;

    document.forms[0].txtClaimnumber.value = window.opener.forms[0].clmNumber.value;

    if (eval(window.opener.forms[0].invNumber) != null) {
        document.forms[0].txtInvnum.value = window.opener.forms[0].invNumber.value;

        document.forms[0].invtotal.value = window.opener.forms[0].invAmt.value;
        MultiCurrencyOnBlur_TandE(document.forms[0].invtotal);//rupal:multicurrency

        //rupal:start, multicurrency
        //varAvail = (parseFloat(window.opener.document.forms[0].invAmt.value) - parseFloat(window.opener.document.forms[0].h_Invoiced.value));
        MultiCurrencyToDecimal_TandE(window.opener.document.forms[0].invAmt);
        varAvail = (parseFloat(window.opener.document.forms[0].invAmt.getAttribute("Amount")) - parseFloat(window.opener.document.forms[0].h_Invoiced.value));
        //MultiCurrencyOnBlur_TandE(document.getElementById('Amount'));
        //rupal:end
        varAvail = (Math.round(parseFloat(varAvail * 100)) / 100);

        if (document.forms[0].iteminv.value != '') {
            //rupal:multicurrency
            //m_itemAmount = document.forms[0].iteminv.value;
            MultiCurrencyToDecimal_TandE(document.forms[0].iteminv);
            m_itemAmount = document.forms[0].iteminv.getAttribute("Amount");
            //MultiCurrencyOnBlur_TandE(document.forms[0].iteminv);
        }
        //comeback
        document.forms[0].totalna.value = parseFloat(varAvail);
    }

    if (window.opener.forms[0].h_maxtrackid.value == '') {
        window.opener.forms[0].h_maxtrackid.value = eval(document.forms[0].maxtrackid.value);
        window.opener.forms[0].h_trackid.value = eval(document.forms[0].trackid.value);
    }

    if (document.forms[0].chkoverride.checked == false) {
        document.forms[0].txttotal.disabled = true;
        document.forms[0].txtrate.disabled = true;
        document.forms[0].txtunit.disabled = true;
    }

    document.dateSelected = dateSelected;
    document.onCodeClose = onCodeClose;
    document.codeSelected = codeSelected;
    document.SCLoaded = SCLoaded;
    document.SCCancel = SCCancel;
    document.SCClose = SCClose;
    document.SCUnload = SCUnload;
}

function saveInvDetail() {
    var allowSubmit = true;
    var sDate;
    var eDate;
    var sSaveMode = window.opener.document.getElementById('hSaveMode').value;
    if ((document.getElementById('hMode').value) == "edit")
    { var TrackId = parseInt(document.forms[0].hRowId.value); }
    else {
        var TrackId = parseInt(document.forms[0].hRowId.value - 2);
    }
    var obj = window.opener.document.getElementById("gvInvoiceDetails");
    var objAllocated = window.opener.document.getElementById("h_allocated");

    //code added by yatharth : MITS 17500 : : Removing the dollar signs for the controls. START
    //rupal:start, r8 multicurrency
    /*
    document.forms[0].iteminv.value = replace(document.forms[0].iteminv.value, "$", "");
    document.forms[0].iteminv.value = replace(document.forms[0].iteminv.value, ",", "");
    document.forms[0].invtotal.value = replace(document.forms[0].invtotal.value, "$", "");
    document.forms[0].invtotal.value = replace(document.forms[0].invtotal.value, ",", "");
    document.forms[0].txttotal.value = replace(document.forms[0].txttotal.value, "$", "");
    document.forms[0].txttotal.value = replace(document.forms[0].txttotal.value, ",", "");
    document.forms[0].totavailable.value = replace(document.forms[0].totavailable.value, "$", "");
    document.forms[0].totavailable.value = replace(document.forms[0].totavailable.value, ",", ""); 
    */
    //rupal:end
    //END
    //if (TrackId < 10) {
    //    TrackId = "0" + TrackId;
    //}

    //Start by Shivendu for MITS 17792: Moving all the validations up.

    //Checking for the Transaction Type
    if (Trim(document.getElementById('txtTransType').value) == '') {
        allowSubmit = false;
        alert('Enter Trans type');
        return allowSubmit;
    }

    //Arnab: MITS-9742 - End
    sDate = document.getElementById('txtfdate').value;
    eDate = document.getElementById('txttdate').value;

    //vkumar258 ML Changes Starts
    var target1 = $("#" + "txtfdate"); //$(document.getElementById('txtfdate').value); //
    var inst1 = $.datepicker._getInst(target1[0]);
    var fdate = ("0" + (inst1.selectedDay)).slice(-2);
    var fmonth = ("0" + (inst1.selectedMonth + 1)).slice(-2);
    var fyear = inst1.selectedYear;

    var nFromDate = fyear + fmonth + fdate;

    var target2 = $("#" + "txttdate");//$(document.getElementById('txttdate').value); //$
    var inst2 = $.datepicker._getInst(target2[0]);
    var tdate = ("0" + (inst2.selectedDay)).slice(-2);
    var tmonth = ("0" + (inst2.selectedMonth + 1)).slice(-2);
    var tyear = inst2.selectedYear;

    var nToDate = tyear + tmonth + tdate;
    //vkumar258 End


    if (eDate != '' && sDate == '') {
        allowSubmit = false;
        alert('Enter From Date');
        return allowSubmit; //Arnab: MITS-9742 - Added "return allowSubmit;" 	
    }

    if (eDate != '' && sDate != '') {
        if (nToDate < nFromDate) {
            allowSubmit = false;
            alert('To Date should be greater or equal to From Date');
            return allowSubmit; //Arnab: MITS-9742 - Added "return allowSubmit;"
        }
    }

    //Arnab: MITS-9742 - commented else if condition, appended that condition in the if condition and 
    //                   added "return allowSubmit;"
    //MGaba2:MITS 24175:On Editing a  detail,Able to save Non Billable invoice without Non billable Reason field
    if (document.forms[0].h_billable.value == "False" || document.forms[0].h_billable.value == "0") {
        if ((document.getElementById('nonbillreasoncode_cid').value == '') || (document.getElementById('nonbillreasoncode_cid').value == '0') || document.getElementById('nonbillreasoncode').value == '') {
            allowSubmit = false;
            //MGaba2: MITS 22277:Correcting casing of Alert Msg
            //alert('select non bill reason');
            alert('Please select Non Bill Reason.');
            return allowSubmit;
        }
        //Arnab: Commented extra lines
        //else if (document.all.nonbillreasoncode.value == '') {
        //alert('select non bill reason');
        //allowSubmit = false;
        //}
    }

    //Arnab: MITS-9742 
    //Added "return allowSubmit;" statement, modified the alert message, 
    //Appended condition for blank value check, modified the alert message
    if (document.getElementById('iteminv') != null) {
        //added by Nitin from Mits 12317
        //Added:Yukti,Dt:12/0/2013,MITS 34717
        //if (window.opener.forms[0].h_allocated.value == '1') {
        if (window.opener.document.getElementById('h_allocated').value == '1') {
            //zalam 05/08/2008 Mits:-12160
            //if(parseFloat(document.all.iteminv.value)==0 || document.all.iteminv.value=='')
            //abansal23 12/11/2009 MITS : 19080
            //rupal:multicurrency
            //if (parseFloat(document.getElementById('iteminv').value) == 0 || document.getElementById('iteminv').value == '') {
            //comeback

            MultiCurrencyToDecimal_TandE(document.getElementById('iteminv'));
            if (parseFloat(document.getElementById('iteminv').getAttribute("Amount")) == 0 || parseFloat(document.getElementById('iteminv').getAttribute("Amount")) == 0.00 || document.getElementById('iteminv').value == '') {
                //alert('Invalid Item Amount');
                allowSubmit = false;
                alert('Item Amount can not be Blank or equal to Zero');
                return allowSubmit; //Arnab: MITS-9742 - Added "return allowSubmit;" 	
            }
            //MultiCurrencyOnBlur_TandE(document.getElementById('iteminv'));
            //MITS-9742 - End

            //rupal:multicurrency
            //window.opener.forms[0].h_itemAmt.value = document.getElementById('iteminv').value;
            //Added:Yukti,Dt:12/20/2013,MITS 34717
            //window.opener.forms[0].h_itemAmt.value = document.getElementById('iteminv').getAttribute("Amount");
            window.opener.document.getElementById('h_itemAmt').value = document.getElementById('iteminv').getAttribute("Amount");
        }
    }

    //zalam 05/08/2008 Mits:-12160 Start
    //nadim for 15826
    //rupal:multicurrency
    //if (formatCurrency(document.getElementById('txttotal').value) == 0 || document.getElementById('txttotal').value == '' || document.getElementById('txttotal').value == '$0.00' || document.getElementById('txttotal').value == '0.00') {
    //comeback, check if parsefloat converts 0.00 or 0,00 to zero
    MultiCurrencyToDecimal_TandE(document.getElementById('txttotal'));
    if (parseFloat(document.getElementById('txttotal').getAttribute("Amount")) == 0 || parseFloat(document.getElementById('txttotal').getAttribute("Amount")) == 0.00 || document.getElementById('txttotal').getAttribute("Amount") == '') {
        allowSubmit = false;
        //alert('Total cannot be Zero');
        alert('Invalid without calculation of Total');
        return allowSubmit;
    }
    //MultiCurrencyOnBlur_TandE(document.getElementById('txttotal'));
    //zalam 05/08/2008 Mits:-12160 End

    //End by Shivendu for MITS 17792
    //rupal:start,multicurrency
    MultiCurrencyToDecimal_TandE(document.forms[0].iteminv);
    MultiCurrencyToDecimal_TandE(document.forms[0].invtotal);
    if (obj != null) {
        if (objAllocated.value == "1") {
            //rupal:start,multicurrency
            //window.opener.document.getElementById("gvInvoiceDetails_ctl" + TrackId + "_hInvamt").value = document.forms[0].iteminv.value;
            window.opener.document.getElementById("gvInvoiceDetails_hInvamt_" + TrackId).value = document.forms[0].iteminv.getAttribute("Amount");
            //window.opener.document.getElementById("gvInvoiceDetails_ctl" + TrackId + "_invtot").value = document.forms[0].invtotal.value;
            window.opener.document.getElementById("gvInvoiceDetails_invtot_" + TrackId).value = document.forms[0].invtotal.getAttribute("Amount");
            //rupal:end
        }
        //MultiCurrencyOnBlur_TandE(document.forms[0].iteminv);
        //MultiCurrencyOnBlur_TandE(document.forms[0].invtotal);
        //rupal:end

        window.opener.document.getElementById("gvInvoiceDetails_comment_" + TrackId).value = document.forms[0].txtcomment.value;
        window.opener.document.getElementById("gvInvoiceDetails_fromdate_" + TrackId).value = document.forms[0].h_fromdate.value;
        window.opener.document.getElementById("gvInvoiceDetails_fromdateFormatted_" + TrackId).value = document.forms[0].txtfdate.value;
        window.opener.document.getElementById("gvInvoiceDetails_hBillable_" + TrackId).value = document.forms[0].rbIsBillable.checked;
        window.opener.document.getElementById("gvInvoiceDetails_hOverRide_" + TrackId).value = document.forms[0].chkoverride.checked;
        window.opener.document.getElementById("gvInvoiceDetails_hTrackId_" + TrackId).value = TrackId;
        window.opener.document.getElementById("gvInvoiceDetails_id_" + TrackId).value = document.forms[0].h_id.value;
        window.opener.document.getElementById("gvInvoiceDetails_invnum_" + TrackId).value = document.forms[0].h_invnum.value;
        //rupal:start,multicurrency
        //window.opener.document.getElementById("gvInvoiceDetails_ctl" + TrackId + "_hBillAmt").value = document.forms[0].txttotal.value;
        MultiCurrencyToDecimal_TandE(document.forms[0].txttotal);
        window.opener.document.getElementById("gvInvoiceDetails_hBillAmt_" + TrackId).value = document.forms[0].txttotal.getAttribute("Amount");
        //MultiCurrencyOnBlur_TandE(document.forms[0].txttotal);
        //rupal:end

        // MITS 16059 MAC
        window.opener.document.getElementById("gvInvoiceDetails_nonbillreasoncode_" + TrackId).value = document.forms[0].nonbillreasoncode_cid.value;
        //window.opener.document.getElementById("gvInvoiceDetails_ctl" + TrackId + "_nonbillreasoncode").value = document.forms[0].nonbillreasoncode.value;
        window.opener.document.getElementById("gvInvoiceDetails_nonbillreason_" + TrackId).value = document.forms[0].nonbillreasoncode.value;

        window.opener.document.getElementById("gvInvoiceDetails_pid_" + TrackId).value = document.forms[0].h_pid.value;
        window.opener.document.getElementById("gvInvoiceDetails_postable_" + TrackId).value = document.forms[0].rbPostable.checked;
        window.opener.document.getElementById("gvInvoiceDetails_precomment_" + TrackId).value = document.forms[0].txtprecomment.value;
        window.opener.document.getElementById("gvInvoiceDetails_qty_" + TrackId).value = document.forms[0].txtQty.value;
        //rupal:start,multicurrency
        //window.opener.document.getElementById("gvInvoiceDetails_ctl" + TrackId + "_rate").value = document.forms[0].txtrate.value;
        MultiCurrencyToDecimal_TandE(document.forms[0].txtrate);
        window.opener.document.getElementById("gvInvoiceDetails_rate_" + TrackId).value = document.forms[0].txtrate.getAttribute("Amount");
        //MultiCurrencyOnBlur_TandE(document.forms[0].txtrate);
        //rupal:end
        window.opener.document.getElementById("gvInvoiceDetails_reason_" + TrackId).value = document.forms[0].txtreason.value;
        window.opener.document.getElementById("gvInvoiceDetails_reservetype_" + TrackId).value = document.forms[0].txtReservetype.value;
        window.opener.document.getElementById("gvInvoiceDetails_reservetypecode_" + TrackId).value = document.forms[0].h_reservetypecode.value;
        window.opener.document.getElementById("gvInvoiceDetails_status_" + TrackId).value = document.forms[0].h_status.value;
        window.opener.document.getElementById("gvInvoiceDetails_todate_" + TrackId).value = document.forms[0].h_todate.value;
        window.opener.document.getElementById("gvInvoiceDetails_todateFormatted_" + TrackId).value = document.forms[0].txttdate.value;
        window.opener.document.getElementById("gvInvoiceDetails_transtype_" + TrackId).value = document.forms[0].txtTransType.value;
        window.opener.document.getElementById("gvInvoiceDetails_transtypecode_" + TrackId).value = document.forms[0].h_transtypecode.value;
        window.opener.document.getElementById("gvInvoiceDetails_unit_" + TrackId).value = document.forms[0].txtunit.value;
    }

    //window.opener.forms[0].checkflag.value='false';
    //window.opener.forms[0].checkflag.innerText='false';

    //Arnab: MITS-9742  -   Added "return allowSubmit;"



    if (allowSubmit == true) {
        //Added:Yukti,Dt:12/20/2013,MITS 34717
        //window.opener.forms[0].h_save_rowid.value = document.getElementById('hRowId').value;
        window.opener.document.getElementById('h_save_rowid').value = document.getElementById('hRowId').value;
        //rupal:start,multicurrency
        //window.opener.forms[0].h_billamt.value = document.getElementById('txttotal').value;
        MultiCurrencyToDecimal_TandE(document.getElementById('txttotal'));
        //window.opener.forms[0].h_billamt.value = document.getElementById('txttotal').getAttribute("Amount");
        window.opener.document.getElementById('h_billamt').value = document.getElementById('txttotal').getAttribute("Amount");
        //MultiCurrencyOnBlur_TandE(document.getElementById('txttotal'));
        //rupal:end
        //added:Yukti,Dt:12/20/2013,MITS 34717
        //window.opener.forms[0].h_billable.value = document.getElementById('rbIsBillable').checked;
        //window.opener.forms[0].h_reservetype.value = document.getElementById('txtReservetype').value;
        //window.opener.forms[0].h_reservetypecode.value = document.getElementById('h_reservetypecode').value;
        //window.opener.forms[0].h_transtype.value = document.getElementById('txtTransType').value;
        //window.opener.forms[0].h_transtypecode.value = document.getElementById('h_transtypecode').value;
        //window.opener.forms[0].h_unit.value = document.getElementById('txtunit').value;
        window.opener.document.getElementById('h_billable').value = document.getElementById('rbIsBillable').checked;
        window.opener.document.getElementById('h_reservetype').value = document.getElementById('txtReservetype').value;
        window.opener.document.getElementById('h_reservetypecode').value = document.getElementById('h_reservetypecode').value;
        window.opener.document.getElementById('h_transtype').value = document.getElementById('txtTransType').value;
        window.opener.document.getElementById('h_transtypecode').value = document.getElementById('h_transtypecode').value;
        window.opener.document.getElementById('h_unit').value = document.getElementById('txtunit').value;

        //rupal:start, multicurrency
        //window.opener.forms[0].h_rate.value = document.getElementById('txtrate').value;
        MultiCurrencyToDecimal_TandE(document.getElementById('txtrate'));
        //window.opener.forms[0].h_rate.value = document.getElementById('txtrate').getAttribute("Amount");
        window.opener.document.getElementById('h_rate').value = document.getElementById('txtrate').getAttribute("Amount");
        //MultiCurrencyOnBlur_TandE(document.getElementById('txtrate'));
        //rupal:end
        //window.opener.forms[0].totavailable.value = document.getElementById('totavailable').value;

        //window.opener.forms[0].totavailable.value = document.getElementById('totavailable').value;
        window.opener.document.getElementById('totavailable').value = document.getElementById('totavailable').value;
        //MultiCurrencyOnBlur_TandE(window.opener.forms[0].totavailable);
        MultiCurrencyOnBlur_TandE(window.opener.document.getElementById('totavailable'));
        //code changed by yatharth : MITS 17500 : START
        var tempinvAmt;
        //tempinvAmt = window.opener.forms[0].invAmt.value;

        //St:Yukti, Dt:12/23/2013,MITS 34717
        //rupal:start, multicurrency
        //MultiCurrencyToDecimal_TandE(window.opener.forms[0].invAmt);
        //tempinvAmt = window.opener.forms[0].invAmt.getAttribute("Amount");
        MultiCurrencyToDecimal_TandE(window.opener.document.getElementById('invAmt'));
        tempinvAmt = window.opener.document.getElementById('invAmt').getAttribute("Amount");
        //ended:Yukti,Dt:12/23/2013,MITS 34717
        //MultiCurrencyOnBlur_TandE(document.getElementById('Amount'));
        //rupal:end
        //tempinvAmt = replace(tempinvAmt, "$", "");
        //tempinvAmt = replace(tempinvAmt, ",", "");

        //window.opener.forms[0].h_Invoiced.value = (parseFloat(tempinvAmt) - parseFloat(document.getElementById('totavailable').value)); //end yatharth : MITS 17500 :
        MultiCurrencyToDecimal_TandE(document.getElementById('totavailable'));
        //Added:Yukti,DT:12/23/2013,MITS 34717
        //window.opener.forms[0].h_Invoiced.value = (parseFloat(tempinvAmt) - parseFloat(document.getElementById('totavailable').getAttribute("Amount"))); //end yatharth : MITS 17500 :
        window.opener.document.getElementById('h_Invoiced').value = (parseFloat(tempinvAmt) - parseFloat(document.getElementById('totavailable').getAttribute("Amount"))); //end yatharth : MITS 17500 :
        //MultiCurrencyOnBlur_TandE(document.getElementById('totavailable'));        
        //window.opener.document.getElementById("totinvoice").innerText = 'Invoiced : $' + window.opener.forms[0].h_Invoiced.value;
        //rupal:end
        //added by Nitin for Mits 12317
        //Added:Yukti,Dt:12/23/2013,MITS 34717
        // window.opener.forms[0].allocatedFlag.value = document.getElementById("isAllocated").value;
        window.opener.document.getElementById('allocatedFlag').value = document.getElementById("isAllocated").value;

        var sToDate = document.getElementById('txttdate').value;
        sToDate = replace(sToDate, "/", "");
        //Added:Yukti,Dt:12/23/2013,MITS 34717
        //window.opener.forms[0].h_todate.value = sToDate.substring(4, 8) + sToDate.substring(0, 2) + sToDate.substring(2, 4);
        window.opener.document.getElementById('h_todate').value = sToDate.substring(4, 8) + sToDate.substring(0, 2) + sToDate.substring(2, 4);

        var sFromDate = document.getElementById('txtfdate').value;
        sToDate = replace(sFromDate, "/", "");
        //Added:yukti,Dt:12/23/2013,MITS 34717
        //window.opener.forms[0].h_fromdate.value = sFromDate.substring(4, 8) + sFromDate.substring(0, 2) + sFromDate.substring(2, 4);
        window.opener.document.getElementById('h_fromdate').value = sFromDate.substring(4, 8) + sFromDate.substring(0, 2) + sFromDate.substring(2, 4);

        //if (document.all.chkoverride.checked) {
        //  window.opener.forms[0].h_override.value = 'True';
        //} else {
        //  window.opener.forms[0].h_override.value = 'False';
        //}

        //document.all.txtrate.value = formatCurrency(document.all.txtrate.value);
        //rupal:startmulticurrency
        //var totalValue = getcalculatedValue(document.getElementById('txtQty').value, document.getElementById('txtrate').value);
        MultiCurrencyToDecimal_TandE(document.getElementById('txtrate'));
        var totalValue = getcalculatedValue(document.getElementById('txtQty').value, document.getElementById('txtrate').getAttribute("Amount"));
        //MultiCurrencyOnBlur_TandE(document.getElementById('txtrate'));        
        //rupal:end
        //Added:ygoyal3, Dt:12/23/2013,MITS 34717
        //if (document.forms[0].h_billable.value == "True") {
        //    document.forms[0].all.PostBillAmount.value = parseFloat(window.opener.forms[0].all.h_Billed.value - document.forms[0].all.PreBillAmount.value) + parseFloat(totalValue);
        //}
        //else {
        //    document.forms[0].all.PostBillAmount.value = parseFloat(window.opener.forms[0].all.h_Billed.value - document.forms[0].all.PreBillAmount.value);
        //}

        if (document.getElementById('h_billable').value == "True") {
            document.getElementById('PostBillAmount').value = parseFloat(document.getElementById('h_Billed').value - document.forms[0].PreBillAmount.value) + parseFloat(totalValue);
        }
        else {
            document.getElementById('PostBillAmount').value = parseFloat(document.getElementById('h_Billed').value - document.forms[0].PreBillAmount.value);
        }

        //Ended:ygoyal3,Dt:12/23/2013,MITS 34717
        //rupal:commented by rupal, need to verify through debugging, comeback
        //document.getElementById('PostBillAmount').value = formatCurrency(document.getElementById('PostBillAmount').value);

        //if (document.all.rbIsBillable.checked == true) {
        //  document.all.h_billable.value = "True";
        //} else {
        //  document.all.h_billable.value = "False";
        //}

        //Added:ygoyal3,Dt:12/23/2013,MITS 34717
        //window.opener.forms[0].h_nonbillreason.value = document.getElementById('nonbillreasoncode').value;
        //window.opener.forms[0].h_nonbillreasoncode.value = document.getElementById('nonbillreasoncode_cid').value;
        //window.opener.forms[0].h_comment.value = document.getElementById('txtcomment').value;
        //window.opener.forms[0].h_precomment.value = document.getElementById('txtprecomment').value;
        //window.opener.forms[0].h_qty.value = document.getElementById('txtQty').value;
        //window.opener.forms[0].h_reason.value = document.getElementById('txtreason').value;
        //window.opener.forms[0].h_postable.value = document.getElementById('optpostable').value;

        window.opener.document.getElementById('h_nonbillreason').value = document.getElementById('nonbillreasoncode').value;
        window.opener.document.getElementById('h_nonbillreasoncode').value = document.getElementById('nonbillreasoncode_cid').value;
        window.opener.document.getElementById('h_comment').value = document.getElementById('txtcomment').value;
        window.opener.document.getElementById('h_precomment').value = document.getElementById('txtprecomment').value;
        window.opener.document.getElementById('h_qty').value = document.getElementById('txtQty').value;
        window.opener.document.getElementById('h_reason').value = document.getElementById('txtreason').value;
        window.opener.document.getElementById('h_postable').value = document.getElementsByName('optpostable')[0].value;

        //if (window.opener.forms[0].h_trackmode.value == 'new') {
        //    window.opener.forms[0].h_trackid.value = window.opener.forms[0].h_maxtrackid.value;
        //    document.getElementById('maxtrackid').value = window.opener.forms[0].h_maxtrackid.value;
        //}
        //else {
        //    window.opener.forms[0].h_trackid.value = document.forms[0].hRowId.value;
        //    window.opener.forms[0].h_id.value = document.getElementById('id').value;
        //}

        if (window.opener.document.getElementById('h_trackmode').value == 'new') {
            window.opener.document.getElementById('h_trackid').value = window.opener.document.getElementById('h_maxtrackid').value;
            document.getElementById('maxtrackid').value = window.opener.document.getElementById('h_maxtrackid').value;
        }
        else {
            window.opener.document.getElementById('h_trackid').value = document.getElementById('hRowId').value;
            window.opener.document.getElementById('h_id').value = document.getElementById('id').value;
        }
        //Ended:ygoyal3, Dt:12/23/2013,MITS 34717
    }

    if (allowSubmit == true) {
        //Added:ygoyal3,Dt:12/23/2013,MITS 34717
        //document.forms[0].abc.value = "close";
        document.getElementById('abc').value = "close";

        SaveDetails(sSaveMode);

        return true;
    }
    else {
        return false;
    }
}

function SaveDetails(mode) {
    //var iNewMaxId = parseInt(window.opener.forms[0].h_maxtrackid.value);
    var iNewMaxId = parseInt(window.opener.document.getElementById('h_maxtrackid').value);
    var objAllocated = window.opener.document.getElementById("h_allocated");

    if (document.getElementById('hMode').value == "add") {
        iNewMaxId = parseInt(document.getElementById('hRowId').value);
    }

    //Added:Yukti,Dt:12/23/2013,MITS 34717
    //document.forms[0].h_vendor.value = window.opener.forms[0].vendor.value;
    //window.opener.forms[0].h_vendor.value = window.opener.forms[0].vendor.value;
    //window.opener.forms[0].h_maxtrackid.value = iNewMaxId;
    //window.opener.document.forms[0].h_postback.value = "savedetails";
    //window.opener.document.forms[0].h_claimanteid.value = document.getElementById('h_claimanteid').value; //MITS 16608
    //window.opener.document.forms[0].h_unitid.value = document.getElementById('h_unitid').value;//MITS 16608

    document.getElementById('h_vendor').value = window.opener.document.getElementById('vendor').value;
    window.opener.document.getElementById('h_vendor').value = window.opener.document.getElementById('vendor').value;
    window.opener.document.getElementById('h_maxtrackid').value = iNewMaxId;
    window.opener.document.getElementById('h_postback').value = "savedetails";
    window.opener.document.getElementById('h_claimanteid').value = document.getElementById('h_claimanteid').value; //MITS 16608
    window.opener.document.getElementById('h_unitid').value = document.getElementById('h_unitid').value;//MITS 16608

    if (objAllocated.value == "1") {
        //rupal:multicurrency
        //window.opener.forms[0].h_itemAmt.value = document.getElementById('iteminv').value;
        MultiCurrencyToDecimal_TandE(document.getElementById('iteminv'));
        //Added:Yukti,Dt:12/23/2013,MITS 34717
        //window.opener.forms[0].h_itemAmt.value = document.getElementById('iteminv').getAttribute("Amount");
        window.opener.document.getElementById('h_itemAmt').value = document.getElementById('iteminv').getAttribute("Amount");
        //MultiCurrencyOnBlur_TandE(document.getElementById('iteminv'));
        //rupal
    }

    window.opener.document.forms[0].submit();
}

function GetMaxValue() {
    document.forms[0].hRowId.value = window.opener.document.getElementById('h_maxtrackid').value;
}

function PageLoadDetailsLite() {
    if (document.forms[0].abc.value == "close") {
        //Added:ygoyal3, Dt:12/23/2013,MITS 34717
        //window.opener.forms[0].vendor.value = document.forms[0].h_vendor.value;
        //window.opener.forms[0].submit();
        window.opener.document.getElementById('vendor').value = document.getElementById('h_vendor').value;
        window.opener.document.forms[0].submit();
        window.close();
    } else {
        var varAvail = 0;
        //code modified by yatharth : MITS 17500 : START
        var invAmount = 0;
        var h_InvoicedAmount = 0;
        //rupal:start, r8 multicurrency
        var objinvAmt = window.opener.document.getElementById('invAmt');
        //invAmount = replace(window.opener.document.getElementById('invAmt').value, "$", "");
        //invAmount = replace(invAmount, ",", "");
        MultiCurrencyToDecimal_TandE(objinvAmt);
        invAmount = objinvAmt.getAttribute("Amount");
        //MultiCurrencyOnBlur_TandE(objinvAmt);

        //rupal:multicurrency, following code not required
        //h_InvoicedAmount = replace(window.opener.document.getElementById('h_Invoiced').value, "$", "");
        //h_InvoicedAmount = replace(h_InvoicedAmount, ",", "");
        h_InvoicedAmount = window.opener.document.getElementById('h_Invoiced').value;//rupal

        varAvail = (parseFloat(invAmount) - parseFloat(h_InvoicedAmount));
        //Code modified by yatharth : MITS 17500 : END
        varAvail = (Math.round(parseFloat(varAvail * 100)) / 100);
        document.forms[0].totavailable.value = varAvail;
        //rupal:start, r8 multicurrency
        MultiCurrencyOnBlur_TandE(document.forms[0].totavailable);
        //rupal:end

        var objInvTotal = document.getElementById("invtotal");
        if (objInvTotal != null) {
            //4-20-09 document.forms[0].invtotal.value = window.opener.document.all.totavailable.value;
            //yatharth : MITS 17500 : : modified the code for the dollar sign
            document.forms[0].invtotal.value = window.opener.document.getElementById('h_invoicetotal').value;

            //rupal:start, r8 multicurrency
            MultiCurrencyOnBlur_TandE(document.forms[0].invtotal);
            //currencyLostFocus(document.forms[0].invtotal); // to convert to $ sign//rupal:commented for multicurrency
            //rupal:end
        }
        //yatharth : 17500: START
        var objTxtRate = document.getElementById("txtrate");
        if (objTxtRate != null) {

            if (objTxtRate.value != 0 && objTxtRate != null) {
                //rupal:start, r8 multicurrency
                MultiCurrencyOnBlur_TandE(objTxtRate);
                //currencyLostFocus(objTxtRate);
                //rupal:end
            }
            //rupal:commented
            /*
            else {
                objTxtRate.value = "$0.00";
            }
            */
        }
        var objTxtTotal = document.getElementById("txttotal");
        if (objTxtTotal != null) {

            if (objTxtTotal.value != 0 && objTxtTotal != null) {
                //currencyLostFocus(objTxtTotal);
                //rupal:start, r8 multicurrency
                MultiCurrencyOnBlur_TandE(objTxtTotal);
                //rupal:end
            }
            //rupal:commented
            /*
            else {
                objTxtTotal.value = "$0.00";
            }
            */
        }
        var objTotAvailable = document.getElementById("totavailable");
        if (objTotAvailable != null) {

            if (objTotAvailable.value != 0 && objTotAvailable != null) {
                //currencyLostFocus(objTotAvailable);
                //rupal:start, r8 multicurrency
                MultiCurrencyOnBlur_TandE(objTotAvailable);
                document.getElementById('iteminv').value = ''; //RMA-16243-msampathkuma
                //rupal:end
            }
            //rupal:commented
            /*
            else {
                objTotAvailable.value = "$0.00";
            }
            */
        }
        //yatharth : 17500 END
        var objTxtInvnum = document.getElementById("txtInvnum");
        if (objTxtInvnum != null) {
            document.forms[0].txtInvnum.value = window.opener.document.getElementById('invNumber').value;
        }

        document.forms[0].h_invoicenum.value = window.opener.document.getElementById('invNumber').value;
        document.forms[0].h_invnum.value = window.opener.document.getElementById('invNumber').value;
        document.forms[0].h_vendor.value = window.opener.document.getElementById('h_vendor').value;
        document.forms[0].isAllocated.value = window.opener.document.getElementById('h_allocated').value;
        document.forms[0].txtClaimnumber.value = window.opener.document.getElementById('hclmnum').value;
        document.forms[0].h_claimanteid.value = window.opener.document.getElementById('h_claimanteid').value; //MITS 16608
        document.forms[0].h_unitid.value = window.opener.document.getElementById('h_unitid').value;//MITS 16608

        var objLblInvnum = document.getElementById("lblInvnum");
        var objLblinvtotal = document.getElementById("lblinvtotal");
        var objLbliteminv = document.getElementById("lbliteminv");
        var objTxtInvnum = document.getElementById("txtInvnum");
        var objInvtotal = document.getElementById("invtotal");
        var objIteminv = document.getElementById("iteminv");
        //objIteminv.value = "$0.00";//commented by rupal for multicurrency

        if (document.forms[0].isAllocated.value == "1") {
            objLblInvnum.parentNode.style.display = "";
            objLblinvtotal.parentNode.style.display = "";
            objLbliteminv.parentNode.style.display = "";
            objTxtInvnum.parentNode.style.display = "";
            objInvtotal.parentNode.style.display = "";
            objIteminv.parentNode.style.display = "";
        } else {
            objLblInvnum.parentNode.style.display = "none";
            objLblinvtotal.parentNode.style.display = "none";
            objLbliteminv.parentNode.style.display = "none";
            objTxtInvnum.parentNode.style.display = "none";
            objInvtotal.parentNode.style.display = "none";
            objIteminv.parentNode.style.display = "none";
        }
    }
}

function PageLoadDetails()
{
    //pleaseWait.Show('start');

    if (document.forms[0].abc.value == "close") {
        //window.opener.forms[0].checkflag.value='false';
        window.opener.document.forms[0].submit();
        //window.opener.forms[0].submit();
        window.close();
    }
        // MITS 7277
    else {
        var obj = window.opener.document.getElementById("gvInvoiceDetails");
        var objAllocated = window.opener.document.getElementById('h_allocated').value;
        document.forms[0].isAllocated.value = objAllocated;
        document.forms[0].txtClaimnumber.value = window.opener.document.getElementById('hclmnum').value;

        var objLblInvnum = document.getElementById("lblInvnum");
        var objLblinvtotal = document.getElementById("lblinvtotal");
        var objLbliteminv = document.getElementById("lbliteminv");
        var objTxtInvnum = document.getElementById("txtInvnum");
        var objInvtotal = document.getElementById("invtotal");
        var objIteminv = document.getElementById("iteminv");

        if (document.forms[0].isAllocated.value == "1") {
            objLblInvnum.parentNode.style.display = "";
            objLblinvtotal.parentNode.style.display = "";
            objLbliteminv.parentNode.style.display = "";
            objTxtInvnum.parentNode.style.display = "";
            objInvtotal.parentNode.style.display = "";
            objIteminv.parentNode.style.display = "";
        } else {
            objLblInvnum.parentNode.style.display = "none";
            objLblinvtotal.parentNode.style.display = "none";
            objLbliteminv.parentNode.style.display = "none";
            objTxtInvnum.parentNode.style.display = "none";
            objInvtotal.parentNode.style.display = "none";
            objIteminv.parentNode.style.display = "none";
        }



        if (obj != null) {
            //start at 2 so not to include the header row
            var tmode = window.opener.document.forms[0].h_trackmode.value;
            var cmode = document.forms[0].hMode.value;
            var TrackId = parseInt(document.forms[0].hRowId.value);

            //if (TrackId < 10) {
            //    TrackId = "0" + TrackId;
            //}
            var objTotAvailable = document.getElementById("totavailable");
            
            if (objAllocated == "1") {
                document.forms[0].txtInvnum.value = window.opener.document.getElementById('invNumber').value;
                //RMA-16243-start
                if (document.forms[0].htranstypeChange.value == "1" && objTotAvailable.value != 0 && objTotAvailable != null) {
                    document.forms[0].iteminv.value = '';
                    document.forms[0].htranstypeChange.value = '';

                    var hinv = parseFloat(window.opener.document.getElementById("gvInvoiceDetails_hInvamt_" + TrackId).value);
                    MultiCurrencyToDecimal_TandE(document.getElementById('totavailable'));
                    var totav = parseFloat(document.getElementById('totavailable').getAttribute("Amount"));
                    var calamt = hinv + totav;
                    document.forms[0].totavailable.value = calamt;
                    MultiCurrencyOnBlur_TandE(document.forms[0].totavailable);
                    window.opener.document.forms[0].h_Invoiced.value = '0.00';
                    document.forms[0].txtrate.value = '0.00';
                    document.forms[0].txttotal.value = '0.00';
                    MultiCurrencyOnBlur_TandE(document.forms[0].txtrate);
                    MultiCurrencyOnBlur_TandE(document.forms[0].txttotal);
                    window.opener.document.forms[0].totavailable.value = calamt;
                    MultiCurrencyOnBlur_TandE(window.opener.document.forms[0].totavailable);
                    return false;
                }
                else
                    document.forms[0].iteminv.value = window.opener.document.getElementById("gvInvoiceDetails_hInvamt_" + TrackId).value;
                //RMA-16243-end
                MultiCurrencyOnBlur_TandE(document.forms[0].iteminv); //rupal:multicurrency
                //currencyLostFocus(objIteminv); //code addded by yatharth : MITS 17500 ://commented by rupal for multicurrency
                document.forms[0].invtotal.value = window.opener.document.getElementById("gvInvoiceDetails_invtot_" + TrackId).value;
                //code added by yatharth : MITS 17500 :
                //currencyLostFocus(document.forms[0].invtotal);
                MultiCurrencyOnBlur_TandE(document.forms[0].invtotal); //rupal:multicurrency
            }

            document.forms[0].txtcomment.value = window.opener.document.getElementById("gvInvoiceDetails_comment_" + TrackId).value;
            document.forms[0].h_fromdate.value = window.opener.document.getElementById("gvInvoiceDetails_fromdate_" + TrackId).value;
            document.forms[0].txtfdate.value = window.opener.document.getElementById("gvInvoiceDetails_fromdateFormatted_" + TrackId).value;

            //start - MITS 15639 - mpalinski
            //billable handling
            var objBillable = window.opener.document.getElementById("gvInvoiceDetails_hBillable_" + TrackId).value;
            document.forms[0].h_billable.value = objBillable;
            if (objBillable != "0") {
                document.forms[0].rbIsBillable.checked = true;
                document.forms[0].rbIsNotBillable.checked = false;
                //MGaba2:MITS 22240:Details Screen was showing billable even if it is Non Billable
                document.getElementById('nonbillreasoncode').readOnly = true;
                document.getElementById('nonbillreasoncodebtn').disabled = true;
            } else {
                document.forms[0].rbIsBillable.checked = false;
                document.forms[0].rbIsNotBillable.checked = true;
                //MGaba2:MITS 22240:Details Screen was showing billable even if it is Non Billable
                document.getElementById('nonbillreasoncode').readOnly = false;
                document.getElementById('nonbillreasoncodebtn').disabled = false;
            }
            //document.forms[0].rbIsNotBillable.checked = (!document.forms[0].rbIsBillable.checked);
            //end - MITS 15639 - mpalinski

            //override handling
            var objOverRide = window.opener.document.getElementById("gvInvoiceDetails_hOverRide_" + TrackId).value;
            document.forms[0].h_checkoverride.value = objOverRide;
            document.forms[0].chkoverride.value = objOverRide;
            if (objOverRide != "0") {
                document.forms[0].chkoverride.checked = true;
            } else {
                document.forms[0].chkoverride.checked = false;
            }

            document.forms[0].h_id.value = window.opener.document.getElementById("gvInvoiceDetails_id_" + TrackId).value;
            document.forms[0].h_invnum.value = window.opener.document.getElementById("gvInvoiceDetails_invnum_" + TrackId).value;

            // MITS 16059 MAC
            //document.forms[0].nonbillreasoncode.value = window.opener.document.getElementById("gvInvoiceDetails_ctl" + TrackId + "_nonbillreasoncode").value;
            document.forms[0].nonbillreasoncode.value = window.opener.document.getElementById("gvInvoiceDetails_nonbillreason_" + TrackId).value;
            document.forms[0].nonbillreasoncode_cid.value = window.opener.document.getElementById("gvInvoiceDetails_nonbillreasoncode_" + TrackId).value;

            document.forms[0].h_pid.value = window.opener.document.getElementById("gvInvoiceDetails_pid_" + TrackId).value;

            //start - MITS 15639 - mpalinski
            //postable handling
            var objPostable = window.opener.document.getElementById("gvInvoiceDetails_postable_" + TrackId).value;
            document.forms[0].h_postable.value = objPostable;
            if (objPostable != "0") {
                document.forms[0].rbPostable.checked = true;
            } else {
                document.forms[0].rbPostable.checked = false;
            }
            document.forms[0].rbNonPostable.checked = (!document.forms[0].rbPostable.checked);
            //end - MITS 15639 - mpalinski

            document.forms[0].txtprecomment.value = window.opener.document.getElementById("gvInvoiceDetails_precomment_" + TrackId).value;


            document.forms[0].txtreason.value = window.opener.document.getElementById("gvInvoiceDetails_reason_" + TrackId).value;

            //transtype handling
            var objTransTypeCode = window.opener.document.getElementById("gvInvoiceDetails_transtypecode_" + TrackId).value;
            if (objTransTypeCode != '' && document.forms[0].h_ratemode.value != "saved") {
                document.forms[0].transtypecode.value = objTransTypeCode;
                document.forms[0].h_transtypecode.value = objTransTypeCode;
            } else {
                document.forms[0].transtypecode.value = document.forms[0].h_transtypecode.value;
            }

            // MITS 16184 MAC - Certain fields should not be updated if h_ratemode is equal to saved.
            if (document.forms[0].h_ratemode.value == "saved") {
                document.forms[0].h_ratemode.value = "";
            }
            else {
                document.forms[0].txtTransType.value = window.opener.document.getElementById("gvInvoiceDetails_transtype_" + TrackId).value;
                document.forms[0].txtrate.value = window.opener.document.getElementById("gvInvoiceDetails_rate_" + TrackId).value;
                MultiCurrencyOnBlur_TandE(document.forms[0].txtrate); //rupal:multicurrency
                document.forms[0].txtunit.value = window.opener.document.getElementById("gvInvoiceDetails_unit_" + TrackId).value;
                document.forms[0].txtQty.value = window.opener.document.getElementById("gvInvoiceDetails_qty_"+TrackId).value;
                document.forms[0].txttotal.value = window.opener.document.getElementById("gvInvoiceDetails_hBillAmt_" + TrackId).value;
                MultiCurrencyOnBlur_TandE(document.forms[0].txttotal); //rupal:multicurrency
                document.forms[0].txtReservetype.value = window.opener.document.getElementById("gvInvoiceDetails_reservetype_" + TrackId).value;
                //reservetype handling      
                var objReserveTypeCode = window.opener.document.getElementById("gvInvoiceDetails_reservetypecode_" + TrackId).value;
                if (objReserveTypeCode != '') {
                    document.forms[0].reservetypecode.value = objReserveTypeCode;
                    document.forms[0].h_reservetypecode.value = objReserveTypeCode;
                } else {
                    document.forms[0].reservetypecode.value = document.forms[0].h_reservetypecode.value;
                }
            }

            document.forms[0].h_status.value = window.opener.document.getElementById("gvInvoiceDetails_status_" + TrackId).value;
            document.forms[0].h_todate.value = window.opener.document.getElementById("gvInvoiceDetails_todate_" + TrackId).value;
            document.forms[0].txttdate.value = window.opener.document.getElementById("gvInvoiceDetails_todateFormatted_" + TrackId).value;
        }

        document.forms[0].h_claimnum.value = window.opener.document.getElementById('hclmnum').value;
        document.forms[0].h_invoicenum.value = window.opener.document.getElementById('invNumber').value;
        document.forms[0].h_invnum.value = window.opener.document.getElementById('invNumber').value;
        document.forms[0].h_claimanteid.value = window.opener.document.getElementById('h_claimanteid').value; //MITS 16608
        document.forms[0].h_unitid.value = window.opener.document.getElementById('h_unitid').value; //MITS 16608

        //------------------

        document.forms[0].h_ratetableid_child.value = window.opener.document.getElementById('h_ratetableid').value;
        document.forms[0].totavailable.value = window.opener.document.getElementById('totavailable').value;
        MultiCurrencyOnBlur_TandE(document.forms[0].totavailable); //rupal:multicurrency
        document.forms[0].invtotal.value = window.opener.document.getElementById('h_invoicetotal').value; //4-20-09
        MultiCurrencyOnBlur_TandE(document.forms[0].invtotal); //rupal:multicurrency
        //currencyLostFocus(document.forms[0].invtotal); // code added by yatharth : MITS 17500 ://commented by rupal for multicurrency

        if (document.forms[0].iteminv.value != '') {
            //m_itemAmount = document.forms[0].iteminv.value;
            //rupal:start
            MultiCurrencyToDecimal_TandE(document.forms[0].iteminv);
            m_itemAmount = document.forms[0].iteminv.getAttribute("Amount");
            //MultiCurrencyOnBlur_TandE(document.forms[0].iteminv);
            //rupal:multicurrency            
        }

        if (document.forms[0].txtQty.value != '' && document.forms[0].txtrate.value != '') {
            //rupal:start,multicurrency
            MultiCurrencyToDecimal_TandE(document.forms[0].txtrate);
            //if (isNaN(document.forms[0].txtQty.value) == false && isNaN(document.forms[0].txtrate.value) == false) {
            if (isNaN(document.forms[0].txtQty.value) == false && isNaN(document.forms[0].txtrate.getAttribute("Amount")) == false) {
                //var totalValue = getcalculatedValue(document.forms[0].txtQty.value, document.forms[0].txtrate.value);
                var totalValue = getcalculatedValue(document.forms[0].txtQty.value, document.forms[0].txtrate.getAttribute("Amount"));
                // akaushik5 Changed for MITS 38316 Starts
                //document.forms[0].all.txttotal.value = totalValue;
                document.getElementById("txttotal").value = totalValue;
                MultiCurrencyOnBlur_TandE(document.getElementById("txttotal"));
                // akaushik5 Changed for MITS 38316 Ends
                //MultiCurrencyOnBlur_TandE(document.forms[0].all.txttotal);

                // akaushik5 Changed for MITS 38316 Starts
                //if (document.forms[0].h_billable.value == "True") {
                //    document.forms[0].all.PostBillAmount.value = parseFloat(window.opener.document.getElementById('h_Billed').value - document.forms[0].all.PreBillAmount.value) + parseFloat(totalValue);
                //}
                //else {
                //    document.forms[0].all.PostBillAmount.value = parseFloat(window.opener.document.getElementById('h_Billed').value - document.forms[0].all.PreBillAmount.value);
                //}

                if (document.getElementById("h_billable").value == "True") {
                    document.getElementById("PostBillAmount").value = parseFloat(window.opener.document.getElementById('h_Billed').value - document.getElementById("PreBillAmount").value) + parseFloat(totalValue);
                }
                else {
                    document.getElementById("PostBillAmount").value = parseFloat(window.opener.document.getElementById('h_Billed').value - document.getElementById("PreBillAmount").value);
                }
                // akaushik5 Changed for MITS 38316 Ends
            }
            //MultiCurrencyOnBlur_TandE(document.forms[0].txtrate);
            //rupal:end
        }

        if (document.forms[0].chkoverride.value != "0") {
            document.forms[0].chkoverride.checked = true;
            document.forms[0].h_checkoverride.value = "True";
            document.forms[0].txtunit.readOnly = false;
            document.forms[0].txtrate.readOnly = false;
        }
        else {
            document.forms[0].chkoverride.checked = false;
            document.forms[0].h_checkoverride.value = "False";
            document.forms[0].txtunit.readOnly = true;
            document.forms[0].txtrate.readOnly = true;
        }
        //MGaba2:MITS 22240:Details Screen was showing billable even if it is Non Billable
        //Commenting the below code as condition was wrong
        //Also enabling/disabling the codelookup in start of this function where a condition already exists
        //nadim  MITS 12158
        //if (document.forms[0].h_billable.value == "True") {
        //            document.getElementById('optbillable').checked = true;
        //            document.getElementById('nonbillreasoncode').disabled = true;
        //            document.getElementById('nonbillreasoncodebtn').disabled = true;
        //        }
        //        // MITS 12158
        //        else {
        //            document.getElementById('optbillable').checked = true;
        //        }

        // MITS 15639 - MAC
        //        if (document.forms[0].h_postable.value == "0") {
        //            document.all.rbIsBillable.checked = false;
        //            document.all.rbIsNotBillable.checked = true;
        //        }
        //        else {
        //            document.all.rbIsBillable.checked = true;
        //            document.all.rbIsNotBillable.checked = false;
        //        }

        // Anjaneya : Added for MITS 7277
        if (document.getElementById('txtcomment').value != "") {
            if (document.getElementById('txtprecomment').value != "") {
                document.getElementById('txtprecomment').value = document.getElementById('txtprecomment').value + "; " + document.getElementById('txtcomment').value;
            }
            else {
                document.getElementById('txtprecomment').value = document.getElementById('txtcomment').value;
            }

            document.getElementById('txtcomment').value = "";
        }
        // Added by yatharth : MITS 17500 START
        var objTxtRate = document.getElementById("txtrate");
        //rupal:multicurrency (commented)
        /*
        if (objTxtRate != null) {
            
        if (objTxtRate.value != 0 && objTxtRate.value != null) {
        currencyLostFocus(objTxtRate);
        }
        else {
        objTxtRate.value = "$0.00";
        }            
        }
        */
        var objTxtTotal = document.getElementById("txttotal");
        //rupal:multicurrency (commented)
        /*
        if (objTxtTotal != null) {

            if (objTxtTotal.value != 0 && objTxtTotal != null) {
                currencyLostFocus(objTxtTotal);
            }
            else {
                objTxtTotal.value = "$0.00";
            }
        }
        //END: yatharth : MITS 17500
        */
    }
}

function saveRatetabledetail() {

    var allowSubmit = true;

    if (document.getElementById("transactiontypecode_codelookup").value == '') {
        allowSubmit = false;
        alert('Enter Trans type');
        return false;
    }
    else if (document.getElementById("unit_codelookup").value == '') {
        allowSubmit = false;
        alert('Enter Unit');
        return false;
    }
    else if (document.getElementById("rate").value == '') {
        allowSubmit = false;
        alert('Enter Rate');
        return false;
    }
    else if (isNaN(document.getElementById("rate").value)) {
        allowSubmit = false;
        alert('Enter Numeric in Rate field');
        document.getElementById("rate").focus();
        return false;
    }

    if (allowSubmit == true) {
        //window.opener.document.all -vs- window.opener.forms[0].all ?

        //MITS  17926 - start (mpalinski)
        var objRate = document.getElementById("rate").value;
        var dRoundedRate = (Math.round(parseFloat(objRate * 100)) / 100);
        //MITS  17926 - end



        //added:Yukti,Dt:12/20/2013,MITS 34717
        //window.opener.frmData.all("h_rate").value = dRoundedRate; //MITS  17926
        //window.opener.frmData.all("h_rowid").value = document.frmData.all("rowid").value;
        //window.opener.frmData.all("h_trackid").value = document.frmData.all("trackid").value;
        //window.opener.frmData.all("h_transtypecode").value = document.frmData.all("transactiontypecode_codelookup_cid").value;
        //window.opener.frmData.all("h_transtype").value = document.frmData.all("transactiontypecode_codelookup").value;
        //window.opener.frmData.all("h_unit").value = document.frmData.all("unit_codelookup").value;
        //window.opener.frmData.all("mode").value = document.frmData.all("mode").value;
        //window.opener.frmData.all("h_user").value = document.frmData.all("user").value;
        //window.opener.frmData.all("h_datetime").value = document.frmData.all("datetime").value;
        if (window.opener != null)
        {
            window.opener.document.getElementById("h_rate").value = dRoundedRate; //MITS  17926
            window.opener.document.getElementById("h_rowid").value = document.getElementById("rowid").value;
            window.opener.document.getElementById("h_trackid").value = document.getElementById("trackid").value;
            window.opener.document.getElementById("h_transtypecode").value = document.getElementById("transactiontypecode_codelookup_cid").value;
            window.opener.document.getElementById("h_transtype").value = document.getElementById("transactiontypecode_codelookup").value;
            window.opener.document.getElementById("h_unit").value = document.getElementById("unit_codelookup").value;
            window.opener.document.getElementById("mode").value = document.getElementById("mode").value;
            window.opener.document.getElementById("h_user").value = document.getElementById("user").value;
            window.opener.document.getElementById("h_datetime").value = document.getElementById("datetime").value;
        }

        //MGaba2:To Bring pencil sign on change
        if (m_DataChanged) {

            window.opener.setDataChanged(true);
            //added:Yukti,Dt:12/20/2013,MITS 34717
            //window.opener.forms[0].all("hidden_DataChanged").value = "true";
            //window.opener.forms[0].submit();
            window.opener.document.getElementById("hidden_DataChanged").value = "true";
            window.opener.document.forms[0].submit();
        }
        window.close();
        return false;
    }
}

function setOptVal(group, val) {
    for (var i = 0; i < group.length; i++) {
        if (group[i].value == val) {
            group[i].checked = true;
            return true;
        }
    }

    return ("");
}

function delitem(id) {
    //debugger;
    document.forms[0].h_postback.value = 'savedetails';
    document.forms[0].deleteflag.value = 'true';
    document.getElementById('trackid').value = id;

    var obj = document.getElementById("gvInvoiceDetails");

    if (obj != null) {
        //if (id < 10) {
        //    id = "0" + id;
        //}

        var objInvAmt = document.getElementById("gvInvoiceDetails_hInvamt_"+id);
        var dInvAmt = (Math.round(parseFloat(objInvAmt.value * 100)) / 100);

        //rupal:multicurrency
        //var objTotAvailable = document.forms[0].totavailable;        
        var objTotAvailable = document.forms[0].totavailable;
        MultiCurrencyToDecimal_TandE(objTotAvailable);
        var dTotAvailable = (Math.round(parseFloat(objTotAvailable.getAttribute("Amount") * 100)) / 100);
        var newAvail = (dTotAvailable + dInvAmt);
        document.forms[0].totavailable.value = newAvail;
        //rupal:multicurrency
        MultiCurrencyOnBlur_TandE(document.forms[0].totavailable);

        var objInvoiced = document.forms[0].h_Invoiced;
        var dInvoiced = (Math.round(parseFloat(objInvoiced.value * 100)) / 100);
        var newInvoiced = (dInvoiced - dInvAmt);
        document.forms[0].h_Invoiced.value = newInvoiced;
    }

    //MITS 12442 : Raman Bhatia
    //Screen crashes sometimes on deletion of transaction
    //Page getting submitted twice.. hence commenting following code
    var objMaxId = document.forms[0].h_maxtrackid;
    var iMaxId = parseInt(objMaxId.value) - 1;
    objMaxId.value = iMaxId;
    document.forms[0].submit();
}

function setInvoicePage() {
    
    if (eval(document.forms[0].invAmt) != null) {
        if (document.forms[0].invAmt.value == '') {
            document.forms[0].invAmt.value = '0';
        }

        document.forms[0].totavailable.value = setAvailAmt();
        //rupal:start, multicurrency
        MultiCurrencyOnBlur_TandE(document.forms[0].totavailable);
        //Code added by yatharth MITS 17500: TO get the $ sign in Available amount
        //document.forms[0].totavailable.value = "$" + document.forms[0].totavailable.value;
        //currencyLostFocus(document.forms[0].totavailable);
        //End yatharth
        //rupal:end
    }

    document.dateSelected = dateSelected;
    document.onCodeClose = onCodeClose;
    document.forms[0].vendor.value = document.forms[0].h_vendor.value;
}

function setAvailAmt() {
    //debugger;
    var varAvail = 0;

    if (eval(document.forms[0].h_Invoiced) != null) {
        if (document.forms[0].h_Invoiced.value == '') {
            document.forms[0].h_Invoiced.value = '0';
        }
    }

    //rupal:start, r8 multicurrency
    var objinvAmt = document.getElementById('invAmt');
    MultiCurrencyToDecimal_TandE(objinvAmt);
    invoiceamount = objinvAmt.getAttribute("Amount");
    //MultiCurrencyOnBlur_TandE(objinvAmt);

    //var invoiceamount = replace(document.forms[0].invAmt.value, "$", ""); //Code added by yatharth : MITS 17500 :
    //invoiceamount = replace(invoiceamount, ",", ""); // Code added by yatharth : MITS 17500 :
    //rupal:end
    if (invoiceamount.indexOf('(') >= 0 ) {

        invoiceamount = replace(invoiceamount, "(", "");
        invoiceamount = replace(invoiceamount, ")", "");
        invoiceamount = invoiceamount * -1;
    }
    varAvail = (parseFloat(invoiceamount) - parseFloat(document.forms[0].h_Invoiced.value));
    varAvail = (Math.round(parseFloat(varAvail * 100)) / 100);

    //code added by yatharth : MITS 17500 : - To change the display of total available amount to the form like $123.00

    return varAvail;
}

function selectDate(sFieldName) {
    if (m_codeWindow != null) {
        m_codeWindow.close();
    }

    m_sFieldName = sFieldName;
    m_codeWindow = window.open('calendar.html', 'codeWnd',
    'width=230,height=230' + ',top=' + (screen.availHeight - 230) / 2 +
    ',left=' + (screen.availWidth - 230) / 2 + ',resizable=yes,scrollbars=no');
    return false;
}

function dateSelected(sDay, sMonth, sYear) {
    //alert(m_sFieldName);
  
    var objCtrl = null;

    if (m_codeWindow != null) {
        m_codeWindow.close();
        //document.all.txtfdate.value = "pradeep";
    }

    objCtrl = eval('document.all.' + m_sFieldName);

    if (objCtrl != null) {
        sDay = new String(sDay);

        if (sDay.length == 1) {
            sDay = "0" + sDay;
        }

        sMonth = new String(sMonth);

        if (sMonth.length == 1) {
            sMonth = "0" + sMonth;
        }

        sYear = new String(sYear);
        //objCtrl.value = sYear + sMonth + sDay;
        //alert(formatDate(sYear + sMonth + sDay));
       
        objCtrl.value = sMonth + '/' + sDay + '/' + sYear;
    }

    m_sFieldName = "";
    m_codeWindow = null;
    //setDataChanged(true);

    return true;
}

function formatDate(sParamDate) {
    // alert(sParamDate);
    var sDateSeparator;
    var iDayPos = 0;
    var iMonthPos = 0;
    var d = new Date(1999, 11, 22);
    var s = d.toLocaleString();
    var sRet = "";
    var sDate = new String(sParamDate);

    if (sDate == "") {
        return "";
    }

    iDayPos = s.indexOf("22");
    iMonthPos = s.indexOf("11");

    //if (IE4)
    //sDateSeparator = s.charAt(iDayPos + 2);
    //else
    sDateSeparator = "/";

    if (iDayPos < iMonthPos) {
        sRet = sDate.substr(6, 2) + sDateSeparator + sDate.substr(4, 2) + sDateSeparator + sDate.substr(0, 4);
    }
    else {
        sRet = sDate.substr(4, 2) + sDateSeparator + sDate.substr(6, 2) + sDateSeparator + sDate.substr(0, 4);
        //alert(sRet);
    }

    return sRet;
}

function onCodeClose() {
    m_codeWindow = null;
    return true;
}

function replace(sSource, sSearchFor, sReplaceWith) {
    var arr = new Array();
    arr = sSource.split(sSearchFor);

    return arr.join(sReplaceWith);
}

//to what purpose does this function serve?
function selectCode(sCodeTable, sfield) {
}

function codeSelected(cText, cCode) {

    var obj = 'document.forms[0].' + m_sFieldName;
    obj = eval(obj);
    obj.value = cText;
    obj = 'document.forms[0].' + m_sFieldName + 'code';
    obj = eval(obj);

    if (obj == null) {
        obj = 'document.forms[0].' + m_sFieldName + '_id';
        obj = eval(obj);
    }

    obj.value = cCode;

    if (m_sFieldName == 'txtTransType') {
        settransChange();
    }

    if (table_window != null) {
        table_window.close();
    }

    if (m_codeWindow != null) {
        m_codeWindow.close();
    }
}

//function allocationSettings() {
//    //m_isallocated = getvalue(document.forms[0].optallocate);
//    
//    if (document.forms[0].optallocate.checked == true) {
//        m_isallocated = 1;
//    }
//    else {
//        m_isallocated = 0;
//    }

//    document.forms[0].h_trackmode.value = '';
//    document.forms[0].submit();
//}

//This Method is added by NItin for Mits 12317
function CheckIsAllocated() {
    var modeFlag = document.getElementById("mode");
    var checkFlag = document.getElementById("allocatedFlag");
    var allocatedFlag = document.getElementById("h_allocated");

    if (modeFlag.value == 'edit') {
        if (checkFlag.value == '') {
            (allocatedFlag.value == '1') ? document.forms[0].optallocate.checked = true : document.forms[0].optallocate.checked = false;
        }
        else {
            (checkFlag.value == '1') ? document.forms[0].optallocate.checked = true : document.forms[0].optallocate.checked = false;
        }
    }
    else {
        if (checkFlag.value == '1') {
            document.forms[0].optallocate.checked = true;
        }
        else if (checkFlag.value == '') {
            document.forms[0].optallocate.checked = true;
        }
        else {
            document.forms[0].optallocate.checked = false;
        }
    }
    allocationSettings();
}

//This Method is added by NItin for Mits 12317
function allocationSettings() {
    //debugger;
    var txtInvoiceNo = document.getElementById("invNumber");
    var txtinvAmt = document.getElementById("invAmt");
    var txtVender = document.getElementById("vendor");
    var txtAvailable = document.getElementById("totavailable");
    var checkFlag1 = document.getElementById("allocatedFlag");
    var invDate = document.getElementById("invDate");
    var hAlloc = document.getElementById("h_allocated");

    if (document.forms[0].optallocate.checked == true) {
        txtInvoiceNo.parentNode.parentNode.style.display = "";
        txtinvAmt.parentNode.parentNode.style.display = "";
        txtAvailable.parentNode.parentNode.style.display = "";
        txtAvailable.parentNode.parentNode.previousSibling.previousSibling.style.display = "";
        // akaushik5 Changed for MITS 38324 Starts
        //txtVender.parentNode.previousSibling.innerHTML = '<b><u>Vendor:</b></u>';
        //invDate.parentNode.previousSibling.innerHTML = '<b><u>Invoice Date</b></u>';
        $(txtVender).parent().prev('td').html('<b><u>Vendor:</b></u>');
        $(invDate).parent().prev('td').html('<b><u>Invoice Date</b></u>');
        // akaushik5 Changed for MITS 38324 Ends
        hAlloc.value = 1;
        m_isallocated = 1;
        checkFlag1.value = '1';
    }
    else {
        txtInvoiceNo.value = '';
        //rupal:start, multicurrency
        //txtAvailable.value = '$0.00'; //change yatharth : MITS 17500 :
        //txtinvAmt.value = '$0.00'; //change yatharth : MITS 17500 :
        txtAvailable.value=0;
        MultiCurrencyOnBlur_TandE(txtAvailable);
        txtinvAmt.value=0;
        MultiCurrencyOnBlur_TandE(txtinvAmt);
        //comeback here
        document.getElementById("totinvoice").innerHTML = 'Invoiced : $0.00';

        txtInvoiceNo.parentNode.parentNode.style.display = "none";
        txtinvAmt.parentNode.parentNode.style.display = "none";
        txtAvailable.parentNode.parentNode.style.display = "none";
        txtAvailable.parentNode.parentNode.previousSibling.previousSibling.style.display = "none";
        // akaushik5 Changed for MITS 38324 Starts
        //txtVender.parentNode.previousSibling.innerHTML = '<b><u>Work Done By:</b></u>';
        //invDate.parentNode.previousSibling.innerHTML = '<b><u>Date</b></u>';
        $(txtVender).parent().prev('td').html('<b><u>Work Done By:</b></u>');
        $(invDate).parent().prev('td').html('<b><u>Date</b></u>');
        // akaushik5 Changed for MITS 38324 Ends
        //pmittal5 Mits 14066 01/06/09 - Auto Populating Current Adjuster
        if (document.getElementById("h_adjId").value != "") {
            var lastname = document.getElementById("h_adjLastName").value;
            var firstname = document.getElementById("h_adjFirstName").value;

            if (lastname != '' && firstname != '') {
                txtVender.value = lastname + ", " + firstname;
            }
            else if (lastname != '') {
                txtVender.value = lastname;
            }
            else if (firstname != '') {
                txtVender.value = firstname;
            }
            else {
                txtVender.value = "";
            }

            document.getElementById("vendor_cid").value = document.getElementById("h_adjId").value;
            document.getElementById("h_adjLastName").value = "";
            document.getElementById("h_adjFirstName").value = "";
            document.getElementById("h_adjId").value = "";
        }
        //End - pmittal5

        //if Date is blank then setting it to current date
        if (invDate.value == '') {
            var today = new Date();
            var month = today.getMonth() + 1;
            var day = today.getDate();

            if (day < 10) {
                day = '0' + day;
            }

            if (month < 10) {
                month = '0' + month;
            }

            // akaushik5 Changed for MITS 38324 Starts
            //invDate.value = month + '/' + day + '/' + today.getYear();
            invDate.value = month + '/' + day + '/' + today.getFullYear();
            // akaushik5 Changed for MITS 38324 Ends
        }

        hAlloc.value = 0;
        m_isallocated = 0;
        checkFlag1.value = '0';
    }

    //document.forms[0].h_trackmode.value = '';

    return true;
}

function setCalValue(id1, id2, code1, codedesc, desc1, unit, rate) {
    //debugger;
    //Added:Yukti,DT:12/20/2013,MITS 34717
    //window.opener.forms[0].h_ratemode.value = 'saved';
    //window.opener.forms[0].h_transtype.value = code1 + ' ' + desc1;
    //window.opener.forms[0].h_transtypecode.value = id1;
    //window.opener.forms[0].h_reservetype.value = codedesc;
    //window.opener.forms[0].h_reservetypecode.value = id2;
    //window.opener.forms[0].h_unit.value = unit;
    window.opener.document.getElementById('h_ratemode').value = 'saved';
    window.opener.document.getElementById('h_transtype').value = code1 + ' ' + desc1;
    window.opener.document.getElementById('h_transtypecode').value = id1;
    window.opener.document.getElementById('h_reservetype').value = codedesc;
    window.opener.document.getElementById('h_reservetypecode').value = id2;
    window.opener.document.getElementById('h_unit').value = unit;
    //rupal:multicurrency
    //window.opener.forms[0].h_rate.value = formatCurrency(rate);

    //added:Yukti,DT:12/20/2013,MITS 34717
    //window.opener.forms[0].h_rate.value = rate;

    //window.opener.forms[0].h_nonbillreasoncode.value = window.opener.forms[0].nonbillreasoncode.value;//MITS 16272 MJP

    //window.opener.forms[0].h_qty.value = 1;
    window.opener.document.getElementById('h_rate').value = rate;

    window.opener.document.getElementById('h_nonbillreasoncode').value = window.opener.document.getElementById('nonbillreasoncode').value;//MITS 16272 MJP

    window.opener.document.getElementById('h_qty').value = 1;

    var totalValue = getcalculatedValue(1, rate);
    //Added:Yukti,Dt:12/20/2013,MITS 34717
    //window.opener.forms[0].h_total.value = totalValue;
    window.opener.document.getElementById('h_total').value = totalValue;

    //if (window.opener.forms[0].h_billable.value == "True") {
    if (window.opener.document.getElementById('h_billable').value == "True") {
        // window.opener.forms[0].PostBillAmount.value = parseFloat(window.opener.opener.document.forms[0].h_Billed.value - window.opener.forms[0].PreBillAmount.value) + parseFloat(totalValue);
        window.opener.document.getElementById('PostBillAmount').value = parseFloat(window.opener.opener.document.getElementById('h_Billed').value - window.opener.document.getElementById('PreBillAmount').value) + parseFloat(totalValue);
    }
    else {
        //window.opener.forms[0].PostBillAmount.value = parseFloat(window.opener.opener.document.forms[0].h_Billed.value - window.opener.forms[0].PreBillAmount.value);
        window.opener.document.getElementById('PostBillAmount').value = parseFloat(window.opener.opener.document.getElementById('h_Billed').value - window.opener.document.getElementById('PreBillAmount').value);
    }

    //window.opener.forms[0].submit();
    window.opener.document.forms[0].submit();
    window.close();
}

function getcalculatedValue(qty, rate) {
    //debugger;
    if (qty == '') {
        qty = 0;
    }

    if (rate == '') {
        rate = 0;
    }

    //rupal:start, ,ulticurrency
    return (qty * rate);
    //return formatCurrency(qty * rate);
    //rupal:end
    //return (Math.round(parseFloat(qty*rate*100)))/100;
}

function formatCurrency(strValue) {
    strValue = strValue.toString().replace(/\$|\,/g, '');
    dblValue = parseFloat(strValue);

    if (isNaN(dblValue) == true) {
        dblValue = 0;
    }

    blnSign = (dblValue == (dblValue = Math.abs(dblValue)));
    dblValue = Math.floor(dblValue * 100 + 0.50000000001);
    intCents = dblValue % 100;
    strCents = intCents.toString();
    dblValue = Math.floor(dblValue / 100).toString();

    if (intCents < 10) {
        strCents = "0" + strCents;
    }

    for (var i = 0; i < Math.floor((dblValue.length - (1 + i)) / 3); i++) {
        dblValue = dblValue.substring(0, dblValue.length - (4 * i + 3)) + dblValue.substring(dblValue.length - (4 * i + 3));
    }

    return (dblValue + '.' + strCents);
    //return (((blnSign)?'':'-') + '$' + dblValue + '.' + strCents);
}

function selectoption(pagename1, id1, ctname) {
    if (table_window != null) {
        table_window.close();
    }

    m_ctname = ctname;

    switch (ctname) {
        case 'vendor':
            if (document.forms[0].optallocate.checked == true) {
                m_isallocated = 1;
            }
            else {
                m_isallocated = 0;
            }

            m_isallocated = 1; //changed made on koh's instruction
            var param = "?idview=" + m_isallocated;

            table_window = window.open(pagename1 + param, 'TandE', 'width=600,height=500' + ',top=' +
            (screen.availHeight - 490) / 2 + ',left=' +
            (screen.availWidth - 590) / 2 + ',resizable=yes,scrollbars=yes');
            break;
        case 'customer':
            selectCodeLevel('orghlevel', ctname, id1);
            break;
        default:
    }
}

function selRecord(id, name) {
    var obj = 'parent.window.opener.forms[0].' + parent.window.opener.m_ctname;
    obj = eval(obj);
    obj.value = name;
    obj = 'parent.window.opener.forms[0].' + parent.window.opener.m_ctname + '_id';
    obj = eval(obj);
    obj.value = id;

    parent.window.opener.table_window.close();
}

function getvalueInv(group) {
    for (var i = 0; i < 2; i++) {
        if (group[i].checked == true) {
            return (group[i].value);
        }
    }

    return ("");
}

function setDataChanged(ctName) {
    //debugger;
    var obj;

    if ((ctName == 'txtQty') || (ctName == 'txtrate')) {
        //rupal:start,multicurrency
        //if (isNaN(document.getElementById('txtQty').value) == false && isNaN(document.getElementById('txtrate').value) == false) {
        MultiCurrencyToDecimal_TandE(document.getElementById('txtrate'));
        if (isNaN(document.getElementById('txtQty').value) == false && isNaN(document.getElementById('txtrate').getAttribute("Amount")) == false) {
            //document.getElementById('txtrate').value = formatCurrency(document.getElementById('txtrate').value);

            //document.getElementById('txttotal').value = getcalculatedValue(document.getElementById('txtQty').value, document.getElementById('txtrate').value);
            document.getElementById('txttotal').value = getcalculatedValue(document.getElementById('txtQty').value, document.getElementById('txtrate').getgetAttribute("Amount"));
            MultiCurrencyOnBlur_TandE(document.getElementById('txttotal'));
        }
        else {
            alert('Enter Numeric Value in Quantity and Rate');
            obj = eval('document.all.' + ctName);
            obj.focus();
        }
    }
    //MultiCurrencyOnBlur_TandE(document.getElementById('txtrate')); //rupal

    if (ctName == 'chkoverride') {
        var obj = document.getElementById('chkoverride');


        if (obj.checked == true) {
            document.getElementById('txttotal').disabled = false;
            document.getElementById('txtrate').disabled = false;
            document.getElementById('txtunit').disabled = false;
        }
        else {
            document.getElementById('txttotal').disabled = true;
            document.getElementById('txtrate').disabled = true;
            document.getElementById('txtunit').disabled = true;
        }
    }

    if (ctName == 'invAmt') {
        document.getElementById('totavailable').value = setAvailAmt();
        //rupal:multicurrency
        MultiCurrencyOnBlur_TandE(document.getElementById('totavailable'));
    }

    if (ctName == 'optbillable') {
        var optbill = false;
        var rtnFun;

        //******************************************Mohit Yadav
        //optbill=getvalueInv(document.all.optbillable);
        if (document.getElementById('rbIsBillable').checked) {
            optbill = true;
        }

        if (document.getElementById('rbIsNotBillable').checked) {
            optbill = false;
        }

        if (optbill == false) {
            document.getElementById('nonbillreasoncode').disabled = false;
            document.getElementById('nonbillreasoncodebtn').disabled = false;

            //***********************************************Mohit Yadav
            //rtnFun=setOptVal(document.all.optpostable,'0');
            if (document.getElementById('rbPostable').value == '0') {
                document.getElementById('rbPostable').checked = true;
            }

            if (document.getElementById('rbNonPostable').value == '0') {
                document.getElementById('rbNonPostable').checked = true;
            }
        }
        else {
            //***********************************************Mohit Yadav
            //document.all.txtbillreasoncode.value = 0;
            //document.all.txtbillreason.value     = "";
            //document.all.btnreason.disabled      = true;
            //document.all.txtbillreason.disabled  = true;
            document.getElementById('nonbillreasoncodebtn').disabled = true;
            document.getElementById('nonbillreasoncode').disabled = true;
        }
    }

    if (ctName == 'iteminv') {
        MultiCurrencyToDecimal_TandE(document.getElementById('iteminv')); //rupal
        //condition check the add or edit mode
        if (document.getElementById('totalna').value != '0') {
            //edit - edit case
            if ((document.getElementById('iteminv').value != '') && (document.getElementById('PreItemAmount').value != '')) {
                //rupal
                //document.getElementById('totalna').value = ((parseFloat(document.getElementById('totalna').value)) + (parseFloat(document.getElementById('PreItemAmount').value))) - (parseFloat(document.getElementById('iteminv').value));                
                document.getElementById('totalna').value = ((parseFloat(document.getElementById('totalna').value)) + (parseFloat(document.getElementById('PreItemAmount').value))) - (parseFloat(document.getElementById('iteminv').getAttribute("Amount")));
            }
                //edit - add case
            else {
                //rupal
                //document.getElementById('totalna').value = (parseFloat(document.getElementById('totalna').value)) - (parseFloat(document.getElementById('iteminv').value));
                document.getElementById('totalna').value = (parseFloat(document.getElementById('totalna').value)) - (parseFloat(document.getElementById('iteminv').getAttribute("Amount")));
            }
        }
        else {
            if (document.getElementById('iteminv').value != '') {
                MultiCurrencyToDecimal_TandE(document.getElementById('invtotal')); //rupal
                //document.getElementById('totalna').value = (parseFloat(document.getElementById('invtotal').value)) - (parseFloat(document.getElementById('iteminv').value));
                document.getElementById('totalna').value = (parseFloat(document.getElementById('invtotal').getAttribute("Amount"))) - (parseFloat(document.getElementById('iteminv').getAttribute("Amount")));
                //MultiCurrencyOnBlur_TandE(document.getElementById('invtotal'));
            }
        }
        //MultiCurrencyOnBlur_TandE(document.getElementById('iteminv'));
    } //end of the case
} //end of the function

function setDataChangedCheck(ctName) {
    //debugger;
    var obj;

    //Convert txtrate to numeric from currency
    //rupal:start,multicurrency
    //document.getElementById('txtrate').value = replace(document.getElementById('txtrate').value, "$", "");
    //document.getElementById('txtrate').value = replace(document.getElementById('txtrate').value, ",", "");
    //document.getElementById('txtrate').value = parseFloat(document.getElementById('txtrate').value);
    var objtxtrate = document.getElementById('txtrate');
    MultiCurrencyToDecimal_TandE(objtxtrate);
    var rateAmount = objtxtrate.getAttribute("Amount");
    //MultiCurrencyOnBlur_TandE(objtxtrate);    
    //rupal:end
    //end    
    if ((ctName == 'txtQty') || (ctName == 'txtrate')) {
        if (isNaN(document.getElementById('txtQty').value) == false && isNaN(rateAmount) == false) {
            //document.all.txtrate.value = formatCurrency(document.all.txtrate.value);
            var totalValue = getcalculatedValue(document.getElementById('txtQty').value, rateAmount);

            // akaushik5 Changed for MITS 38316 Starts
            //if (document.forms[0].h_billable.value == "True") {
            //    document.forms[0].all.PostBillAmount.value = parseFloat(window.opener.forms[0].all.h_Billed.value - document.forms[0].all.PreBillAmount.value) + parseFloat(totalValue);
            //}
            //else {
            //    document.forms[0].all.PostBillAmount.value = parseFloat(window.opener.forms[0].all.h_Billed.value - document.forms[0].all.PreBillAmount.value);
            //}

            if (document.getElementById("h_billable").value == "True") {
                document.getElementById("PostBillAmount").value = parseFloat(window.opener.document.getElementById("h_Billed").value - document.getElementById("PreBillAmount").value) + parseFloat(totalValue);
            }
            else {
                document.getElementById("PostBillAmount").value = parseFloat(window.opener.document.getElementById("h_Billed").value - document.getElementById("PreBillAmount").value);
            }
            // akaushik5 Changed for MITS 38316 Ends

            //rupal:start,multicurrency
            //document.getElementById('PostBillAmount').value = formatCurrency(document.getElementById('PostBillAmount').value);
            document.getElementById('PostBillAmount').value = document.getElementById('PostBillAmount').value;
            MultiCurrencyToDecimal_TandE(document.getElementById('txttotal'));
            document.getElementById('txttotal').value = totalValue;
            MultiCurrencyOnBlur_TandE(document.getElementById('txttotal'));
            //rupal:end:multicurrency

            //MITS 17501 - start
            var obj = document.getElementById('chkoverride');

            if (obj.checked == false) {
                obj.checked = true;
                document.getElementById('txtrate').readOnly = true;
                document.getElementById('txtunit').readOnly = true;
                //document.forms[0].chkoverride.value = "False";
                document.getElementById('h_checkoverride').value = "False";
            }
            //MITS 17501 - end
        }
        else {
            alert('Enter Numeric Value in Quantity and Rate');
            //Start by Shivendu for MITS 17790
            if (ctName == 'txtQty')
            {
                if(document.getElementById('txtQty') != null)
                {
                    document.getElementById('txtQty').value = 0;
                    if(document.getElementById('txttotal') != null)
                    {
                        document.getElementById('txttotal').value = getcalculatedValue(document.getElementById('txtQty').value, rateAmount);
                        MultiCurrencyOnBlur_TandE(document.getElementById('txttotal'));
                    }
                }
            }
            //End by Shivendu for MITS 17790

            obj = eval('document.all.' + ctName);
            obj.focus();
        }
    }
    //Converting rate to currency field again : yatharth 17500
    //currencyLostFocus(document.getElementById('txtrate')); //commented by rupal
} //end of the function

function setDataChangedInv(ctName) {
    //debugger;
    var obj;
    var invoiceamount;
    var hidden_invoice_amount;

    if (ctName == 'invAmt') {
        //rupal:start, r8 multicurrency
        var objtotavailable = document.getElementById('totavailable');
        objtotavailable.value = setAvailAmt();
        MultiCurrencyOnBlur_TandE(objtotavailable);
        //code added by yatharth : MITS 17500 :
        //document.all.totavailable.value = "$" + document.all.totavailable.value;

        // commented by rupal for multicurrency
        /*
        currencyLostFocus(document.getElementById('totavailable'));
        invoiceamount = replace(document.forms[0].invAmt.value, "$", "");
        invoiceamount = replace(invoiceamount, ",", "");        
        document.getElementById('h_invoicetotal').value =  parseFloat(invoiceamount); //end
        */
        var objinvAmt = document.getElementById('invAmt');
        MultiCurrencyToDecimal_TandE(objinvAmt);
        invoiceamount = objinvAmt.getAttribute("Amount");
        document.getElementById('h_invoicetotal').value = invoiceamount;
        //rupal:end
    }

    if (ctName == 'optbillable') {
        var optbill = false;
        var rtnFun;

        //******************************************Mohit Yadav
        if (document.getElementById('rbIsBillable').checked) {
            optbill = true;
        }

        if (document.getElementById('rbIsNotBillable').checked) {
            optbill = false;
        }

        if (optbill) {
            // MITS 15567 - MAC ***************************************
            document.getElementById('nonbillreasoncode').value = "";
            document.getElementById('nonbillreasoncode').readOnly = true;
            //document.all.nonbillreasoncode.disabled = true;
            // ********************************************************

            //***********************************************Mohit Yadav
            document.getElementById('nonbillreasoncodebtn').disabled = true;
            document.getElementById('h_billable').value = "True";
        } else {
            // MITS 15567 - MAC ***************************************
            document.getElementById('nonbillreasoncode').readOnly = false;
            //document.all.nonbillreasoncode.disabled = false;
            // ********************************************************

            document.getElementById('nonbillreasoncodebtn').disabled = false;
            document.getElementById('h_billable').value = "False";

            //***********************************************Mohit Yadav

            // MITS 15596 - MAC ***************************************
            // document.all.rbNonPostable.checked = true;
            // ********************************************************

            document.forms[0].h_postable.value = 0;
        }
    }

    if (ctName == 'optpostable') {
        if (document.getElementById('rbPostable').checked) {
            document.forms[0].h_postable.value = 1;
        }

        if (document.getElementById('rbNonPostable').checked) {
            document.forms[0].h_postable.value = 0;
        }
    }

    if (ctName == 'chkoverride') {
        var obj = eval('document.forms[0].chkoverride');

        if (obj.checked == true) {
            document.forms[0].txtrate.readOnly = false;
            document.forms[0].txtunit.readOnly = false;
            //document.forms[0].chkoverride.value = "True";
            document.forms[0].h_checkoverride.value = "True";
        }
        else {
            document.forms[0].txtrate.readOnly = true;
            document.forms[0].txtunit.readOnly = true;
            //document.forms[0].chkoverride.value = "False";
            document.forms[0].h_checkoverride.value = "False";
        }
    }

    if (ctName == 'iteminv') {
        var varAvail = 0;
        var iteminv = 0;
        var iteminvtemp;
        var temptotavailable;
        //code added by yatharth : MITS 17500 :
        //rupal:start, multicurrency
        //commented following code for multicurrency
        //invoiceamount = replace(window.opener.document.forms[0].invAmt.value, "$", "");
        //invoiceamount = replace(invoiceamount, ",", "");
        MultiCurrencyToDecimal_TandE(window.opener.document.forms[0].invAmt);
        invoiceamount = window.opener.document.forms[0].invAmt.getAttribute("Amount");
        //hidden_invoice_amount = replace(window.opener.document.forms[0].h_Invoiced.value, "$", "");
        //hidden_invoice_amount = replace(hidden_invoice_amount, ",", "");
        hidden_invoice_amount = window.opener.document.forms[0].h_Invoiced.value;
        //temptotavailable = window.opener.document.forms[0].totavailable.value;
        //temptotavailable = replace(temptotavailable, "$", "");
        //temptotavailable = replace(temptotavailable, ",", "");
        MultiCurrencyToDecimal_TandE(window.opener.document.forms[0].totavailable);
        temptotavailable = window.opener.document.forms[0].totavailable.getAttribute("Amount");
        //end
        //rupal:end
        varAvail = (parseFloat(invoiceamount) - parseFloat(hidden_invoice_amount));

        if (temptotavailable != invoiceamount) {
            varAvail = temptotavailable;
        }

        if (document.forms[0].iteminv.value != '') {
            //code added by yatharth : MITS 17500 : start
            //rupal:start, multicurrency, commented following code
            //iteminvtemp = document.forms[0].iteminv.value;
            //iteminvtemp = replace(iteminvtemp, "$", "");
            //iteminvtemp = replace(iteminvtemp, ",", "");
            MultiCurrencyToDecimal_TandE(document.forms[0].iteminv);
            iteminvtemp = document.forms[0].iteminv.getAttribute("Amount");
            //rupal:end
            //code added by yatharth : MITS 17500 : end
            iteminv = parseFloat(iteminvtemp);
        }

        //code added by yatharth : MITS 17500 :
        //rupal:start, multicurrency,following code not required
        /*
        if (m_itemAmount != "0" && m_itemAmount != "") {
            m_itemAmount = replace(m_itemAmount, "$", "");
            m_itemAmount = replace(m_itemAmount, ",", "");
        }
        */
        //rupal:end
        varAvail = ((parseFloat(varAvail) + parseFloat(m_itemAmount)) - iteminv);

        // MITS 16176 MAC - Added the condition checking txtTransTYpe to satisfy 
        // the RM World condition.  I think this means it was broken in R4 as well.
        //rupal:multicurrency
        //if ((document.forms[0].txtTransType.value != "" && document.forms[0].iteminv.value != '') && (document.forms[0].txtrate.value == '' || document.forms[0].txtrate.value == '0')) {
        MultiCurrencyToDecimal_TandE(document.forms[0].txtrate);
        if ((document.forms[0].txtTransType.value != "" && document.forms[0].iteminv.value != '') && (document.forms[0].txtrate.value == '' || document.forms[0].txtrate.getAttribute("Amount") == '0' || document.forms[0].txtrate.getAttribute("Amount") == "0.00")) {
            document.forms[0].txtrate.value = iteminv; //edited by yatharth : MITS 17500 :
            //rupal:start,multicurrency
            MultiCurrencyOnBlur_TandE(document.forms[0].txtrate);
            document.forms[0].txttotal.value = document.forms[0].iteminv.value;
            MultiCurrencyOnBlur_TandE(document.forms[0].txttotal);
            //rupal:end
        }
        //RMA-16243-start
        else {
            if (document.forms[0].txtTransType.value != "" && document.forms[0].iteminv.value != '') {
                document.forms[0].txtrate.value = iteminv;
                MultiCurrencyOnBlur_TandE(document.forms[0].txtrate);
                document.forms[0].txttotal.value = document.forms[0].iteminv.value;
                MultiCurrencyOnBlur_TandE(document.forms[0].txttotal);
            }
        }
        //RMA-16243-end
        // MITS 16176 MAC
        // -- START -- 
        var oTxtRate = document.forms[0].txtrate;
        var oTxtTransType = document.forms[0].txtTransType;
        var hRate = document.forms[0].h_rate;
        // MITS 16176 MAC - Checking the h_rate variable for 0.00 instead of the txtbox
        // that way it continuously updates the txtrate box with iteminv
        // when the rate of the trans type is zero

        //rupal:multicurrency
        //if (oTxtTransType.value != "" && oTxtRate != null && hRate != null && hRate.value == "0") {
        if (oTxtTransType.value != "" && oTxtRate != null && hRate != null && (hRate.value == "0" || hRate.value == "0.00")) {
            //if (oTxtTransType.value != "" && oTxtRate != null && hRate != null && hRate.value == "0.00") {
            oTxtRate.value = iteminv; //edited by yatharth : MITS 17500 :
            MultiCurrencyOnBlur_TandE(oTxtRate);//rupal
            setDataChangedCheck('txtrate');
        }
        // -- END --

        document.forms[0].totavailable.value = (Math.round(parseFloat(varAvail * 100)) / 100);
        MultiCurrencyOnBlur_TandE(document.forms[0].totavailable);
        //currencyLostFocus(document.forms[0].totavailable); //Yatharth 17500: COnvert to $
    }
} //end of the function

function isNumeric(obj, title) {
    if (isNaN(obj.value) == true || eval(obj.value) == null || eval(obj.value) < 0) {
        alert('Enter Numeric in Numeric Field - ' + title);
        obj.focus();
        return false;
    }
    else {
        return true;
    }
}

function getInvoice(id, cid, mode) {
    //if (inv_window != null) {
    //	inv_window.close();
    //}
    var ratecode = document.forms[0].ratetablecode.value;

    if (isNaN(ratecode) || eval(ratecode) == 0) {
        alert('Select Rate Table');
        return;
    }

    //inv_window = window.open('invoice.asp?invid=' + id + '&ratecode=' + ratecode + '&claimid=' + cid + '&mode=' + mode,
    //                         'Invoice', 'width=600,height=550' + ',top=' +
    //                         (screen.availHeight - 290) / 2 + ',left=' + 
    //                         (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');

    document.location.href = 'invoice.asp?invid=' + id + '&ratecode=' + ratecode + '&claimid=' + cid + '&mode=' + mode;
}

function saveInvoice() {
    //debugger;
    var allowSubmit = true;
    //if (getvalue(document.forms[0].optallocate) == '1')


    //Added by Nitin for Mits 12317
    if (document.forms[0].optallocate.checked == true) {
        //rupal
        //if ((isNumeric(document.forms[0].invAmt, 'Invoice Amount') == false) || (parseFloat(document.forms[0].h_Invoiced.value) != parseFloat(document.forms[0].invAmt.value)) || (parseFloat(document.forms[0].invAmt.value) <= 0)) {
        MultiCurrencyToDecimal_TandE(document.forms[0].invAmt);
        if ((parseFloat(document.forms[0].h_Invoiced.value) != parseFloat(document.forms[0].invAmt.getAttribute("Amount"))) || (parseFloat(document.forms[0].invAmt.getAttribute("Amount")) <= 0)) {
            allowSubmit = false;
            alert('Not valid Inv Amount or Inv Amounts Are not Equal to Total Invoice');
        }

        //if(document.forms[0].invNumber.value=='') {
        //  allowSubmit=false;
        //  alert('Enter Invoice number');
        //}
    }
    else {
        if ((isNumeric(document.forms[0].h_Billed, 'Billed') == false) || (parseFloat(document.forms[0].h_Billed.value) < 0)) {
            allowSubmit = false;
            alert('Enter Valid Bill Amount');
        }
    }

    if (document.forms[0].vendor.value == '') {
        allowSubmit = false;
        alert('Enter vendor/work done by');
    }

    if (document.forms[0].invDate.value == '') {
        allowSubmit = false;
        alert('Enter invoice date');
    }
    else {
        //if(document.forms[0].mode.value=="new") {

        var sdate = "01/01/1900";
        var edate = "01/01/2050";
        var invdate = document.forms[0].invDate.value;

        if (document.forms[0].effsdate.value != '') {
            sdate = document.forms[0].effsdate.value;
        }

        if (document.forms[0].effedate.value != '') {
            edate = document.forms[0].effedate.value;
        }

        if ((Date.parse(invdate) < Date.parse(sdate)) || (Date.parse(invdate) >= Date.parse(edate))) {
            alert('Invoice date must be greater than or equal to effective date and less than its expiration date to proceed. (' + sdate + '-' + edate + ')');
            allowSubmit = false;
        }
        //}
    }

    if (allowSubmit == true) {
        document.forms[0].h_save.value = "1";
        document.forms[0].submit();
    }

    return allowSubmit;
}

function billit() {

    var allowSubmit = true;
    var intclicked = 0;

    var obj = document.getElementById("gvExpenses");

    if (obj == null) {
        return false;
    }

    //subtract the total length by 1 so not to include the footer row
    var numOfRows = (document.getElementById("gvExpenses").rows.length - 2);

    if (numOfRows == 0) {
        allowSubmit = false;
    } else {
        //start at 2 so not to include the header row
        for (var i = 0; i < numOfRows; i++) {
            var iRow = i;

            //if (i < 10) {
            //    iRow = "0" + iRow;
            //}

            if (document.getElementById("gvExpenses_optBillit_"+iRow).checked == true) {

                // MITS 16190 MAC
                var InvoiceId = document.getElementById("gvExpenses_rowid_"+iRow).value;

                var msg = document.getElementById("gvExpenses_msg_"+iRow).value;
                var status = document.getElementById("gvExpenses_hstatus_"+iRow).value;
                var invoiceamt = document.getElementById("gvExpenses_hinvoiceamt_"+iRow).value;

                intclicked++;

                if (status == 'R' && invoiceamt != '$0.00') {
                    result = 'This invoice ' + msg + ' has not been printed. Invoice must be printed before billed.';
                    result = trim(result);
                    alert(result);
                    allowSubmit = false
                }
            }
        }
    }

    if (intclicked == 0) {
        alert('Document not selected');
        allowSubmit = false;
    }

    if (allowSubmit == true) {

        document.forms[0].billed_void.value = "1";
        document.forms[0].submit();

        // MITS 16190 MAC - Handle auto T&E report printing if checked
        if (document.getElementById('chkprintTE').checked) {
            window.open("../TAndE/TimeAndExpensePrintHistory.aspx?mode=report&invoiceid=" + InvoiceId, "Print",
                    "width=600,height=600,resizable=yes,scrollbars=yes,left=" +
                    (screen.availWidth - 590) / 2 + ",top=" +
                    (screen.availHeight - 590) / 2);
        }

    }

    return false;
}

function voidit() {

    var intclicked = 0;
    var allowSubmit = true;

    var obj = document.getElementById("gvExpenses");

    if (obj == null) {
        return false;
    }

    //subtract the total length by 1 so not to include the footer row
    var numOfRows = (document.getElementById("gvExpenses").rows.length - 2);
    if (numOfRows == 0) {
        allowSubmit = false;
    } else {
        //start at 2 so not to include the header row
        for (var i = 0; i < numOfRows; i++) {
            var iRow = i;

            //if (i < 10) {
            //    iRow = "0" + iRow;
            //}

            if (document.getElementById("gvExpenses_optBillit_"+iRow).checked == true) {
                intclicked++;
            }
        }
    }

    if (intclicked > 1) {
        alert('Select one bill at a time to void');
        allowSubmit = false;
    }
    else if (intclicked == 0) {
        alert('Document not selected');
        allowSubmit = false;
    }

    if (allowSubmit == true) {
        var confirmme = confirm('Do you want to void this bill?');

        if (confirmme == true) {
            document.forms[0].billed_void.value = "0";
            document.forms[0].submit();
        }
    }

    return false;
}

function trim(inputString) {
    // Removes leading and trailing spaces from the passed string. Also removes
    // consecutive spaces and replaces it with one space. If something besides
    // a string is passed in (null, custom object, etc.) then return the input.
    if (typeof inputString != "string") {
        return inputString;
    }

    var retValue = inputString;
    var ch = retValue.substring(0, 1);

    while (ch == " ") {
        // Check for spaces at the beginning of the string
        retValue = retValue.substring(1, retValue.length);
        ch = retValue.substring(0, 1);
    }

    ch = retValue.substring(retValue.length - 1, retValue.length);

    while (ch == " ") {
        // Check for spaces at the end of the string
        retValue = retValue.substring(0, retValue.length - 1);
        ch = retValue.substring(retValue.length - 1, retValue.length);
    }

    while (retValue.indexOf("  ") != -1) {
        // Note that there are two spaces in the string - look for multiple spaces within the string
        retValue = retValue.substring(0, retValue.indexOf("  ")) + retValue.substring(retValue.indexOf("  ") + 1, retValue.length);
        // Again, there are two spaces in each of the strings
    }

    return retValue; // Return the trimmed string back to the user
} // Ends

function getratetable(id, mode) {
    //if(inv_window!=null) {
    //  inv_window.close();
    //}

    //inv_window = window.open('ratetable.asp?tableid=' + id + '&mode=' + mode, 'RateTable',
    //                         'width=600,height=550' + ',top=' +
    //                         (screen.availHeight - 290) / 2 + ',left=' + 
    //                         (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');

    document.location.href = 'ratetable.asp?tableid=' + id + '&mode=' + mode;
}

function fillRatetabledetail() {
    document.dateSelected = dateSelected;
    document.codeSelected = codeSelected;

    if (window.opener.forms[0].h_maxtrackid.value == '') {
        window.opener.forms[0].h_maxtrackid.value = eval(document.forms[0].maxtrackid.value);
        window.opener.forms[0].h_trackid.value = eval(document.forms[0].trackid.value);
    }
}

function selectCodeLevel(sCodeTable, sFieldName, sOrgLevel) {
    var orglevel;

    if (m_codeWindow != null) {
        m_codeWindow.close();
    }

    m_sFieldName = sFieldName;
    //orglevel=getOrgLevel(sOrgLevel);
    m_codeWindow = window.open('org.asp?lob=' + sOrgLevel, 'Table',
                               'width=500,height=290' + ',top=' +
                               (screen.availHeight - 290) / 2 + ',left=' +
                               (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
    //m_codeWindow = window.open('getcode.asp?code=' + sCodeTable + '&orglevel=' + sOrgLevel, 'codeWnd',
    //                           'width=500,height=300' + ',top=' +
    //                           (screen.availHeight - 300) / 2 + ',left=' + 
    //                           (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');

    return false;
}

function getOrgLevel(fieldtitle) {
    var orglevel;
    newstr = new String(fieldtitle);

    switch (newstr.toUpperCase()) {
        case 'DEPARTMENT':
            orglevel = '8';
            break;
        case 'FACILITY':
            orglevel = '7';
            break;
        case 'LOCATION':
            orglevel = '6';
            break;
        case 'DIVISION':
            orglevel = '5';
            break;
        case 'REGION':
            orglevel = '4';
            break;
        case 'OPERATION':
            orglevel = '3';
            break;
        case 'COMPANY':
            orglevel = '2';
            break;
        case 'CLIENT':
            orglevel = '1';
            break;
        default:
            orglevel = '1';
    }

    return orglevel;
}

function setRatetable() {
    document.forms[0].h_trackmode.value = "";
    document.dateSelected = dateSelected;
    document.onCodeClose = onCodeClose;
    document.codeSelected = codeSelected;
}

function saveRatetable() {

    var k;
    var allowSubmit = true;

    //MITS-9742 - Edited the alert message
    if (!checkfields('req_fields', 'Not all required fields contain the value. Please enter the data into all underlined fields.', 'nullcheck')) {
        allowSubmit = false;
        return false;
    }
    if (allowSubmit) {
        if (Date.parse(document.forms[0].ExpirationDate.value) < Date.parse(document.forms[0].EffectiveDate.value)) {
            alert("Expiration Date should be greater then or equal to Effective Date.");
            allowSubmit = false;
            return false;
        }
    }

    //MITS 16561 - start - mpalinski
    var obj = document.getElementById("GridDisplayTable");

    if (obj == null) {
        allowSubmit = false;
        alert('Rate Table must have at least one Transaction Detail.');
        return false;
    }

    var numOfRows = (document.getElementById("GridDisplayTable").rows.length - 3);
    var vartransvalue=document.getElementById("GridDisplayTable_TransactionType_0").value;
    //if (numOfRows == 0) {
    if(vartransvalue==""){
        allowSubmit = false;
        alert('Rate Table must have at least one Transaction Detail.');
        return false;
    } else {
        for (i = 0; i <= (numOfRows + 1) ; i++) {
            for (j = i + 1; j <= (numOfRows + 1) ; j++) {
                k = i;
                //if (i <= 9) {
                //    k = "0" + i;
                //}
                var sTransType = "GridDisplayTable_TransactionType_"+k;

                k = j;
                //MITS 16561 - end - mpalinski                
                //if (j <= 9) {
                //    k = "0" + j;
                //}
                var sRestTransTypes = "GridDisplayTable_TransactionType_"+k;

                if (sTransType != null && sRestTransTypes != null) {
                    if (document.getElementById(sTransType).value == undefined || document.getElementById(sRestTransTypes).value == undefined) {
                        if (document.getElementById(sTransType).attributes[2].value == document.getElementById(sRestTransTypes).attributes[2].value) {
                            //Start - MITS 15575 - mpalinski
                            var sAlertTransType = String(document.getElementById(sTransType).innerHTML).replace(/&amp;/g, "&");
                            sAlertTransType = String(sAlertTransType).replace(/&apos;/g, "'");
                            alert("Duplicate entries of transaction type '" + sAlertTransType + "' are not allowed");
                            //End - MITS 15575 - mpalinski
                            return false;
                        }
                    }
                    else if (document.getElementById(sTransType).value == document.getElementById(sRestTransTypes).value) {
                        //pmittal5 MITS 12457 08/21/08
                        //alert("Duplicate entries of transaction type " + objElts[i].id + " are not allowed");

                        //Start - MITS 15575 - mpalinski
                        var sAlertTransType = String(document.getElementById(sTransType).innerHTML).replace(/&amp;/g, "&");
                        sAlertTransType = String(sAlertTransType).replace(/&apos;/g, "'");
                        alert("Duplicate entries of transaction type '" + sAlertTransType + "' are not allowed");
                        //End - MITS 15575 - mpalinski
                        return false;
                    }
                }
            }
        }
    }

    if (allowSubmit == true) {
        document.forms[0].mode.value = 'save';
    }
}

function settransChange() {
    window.opener.forms[0].h_transchange.value = "1";
}

function printdoc_new() {
    var intclicked = 0;
    var allowSubmit = true;
    var InvoiceId = 0;

    var obj = document.getElementById("gvExpenses");

    if (obj == null) {
        return false;
    }

    //subtract the total length by 1 so not to include the footer row
    var numOfRows = (document.getElementById("gvExpenses").rows.length - 2);

    //start at 2 so not to include the header row
    for (var i = 0; i < numOfRows; i++) {
        var iRow = i;

        //if (i < 10) {
        //    iRow = "0" + iRow;
        //}

        if (document.getElementById("gvExpenses_optBillit_"+iRow).checked == true) {
            InvoiceId = document.getElementById("gvExpenses_rowid_"+iRow).value;
            intclicked++;
        }
    }

    if (intclicked > 1) {
        alert('Select one document at a time to print');
        allowSubmit = false;
    }
    else if (intclicked == 0) {
        alert('Document not selected');
        allowSubmit = false;
    }

    if (allowSubmit == true) {
        //window.open("home?pg=riskmaster/TimeAndExpenses/timeandexpense-report" + "&amp;Invoiceid=" + invid, "Print",
        //            "width=600,height=600,resizable=yes,scrollbars=yes,left=" +
        //            (screen.availWidth - 500) / 2 + ",top=" + (screen.availHeight - 290) / 2);

        window.open("../TAndE/TimeAndExpensePrintHistory.aspx?mode=report&invoiceid=" + InvoiceId, "Print",
                    "width=600,height=600,resizable=yes,scrollbars=yes,left=" +
                    (screen.availWidth - 590) / 2 + ",top=" +
                    (screen.availHeight - 590) / 2);
    }

    return false;
}

function printdoc() {
    var allowSubmit = true;
    var result = 0;
    var intclicked = 0;
    var str = new String();
    var invid;

    if (eval(document.forms[0].optBillit) != null) {
        result = eval(document.forms[0].optBillit.length);

        if (result == null) {
            result = 1;
        }
    }
    else {
        return;
    }

    if (result == 0) {
        allowSubmit = false;
    }

    if (result > 1) {
        for (i = 0; i <= result - 1; i++) {
            if (document.forms[0].optBillit[i].checked == true) {
                intclicked = intclicked + 1;
                invid = eval(document.forms[0].optBillit[i]).value;
            }
        }
    }
    else if (result == 1) {
        if (document.forms[0].optBillit.checked == true) {
            intclicked = intclicked + 1;
            invid = eval(document.forms[0].optBillit).value;
        }
    }

    if (intclicked > 1) {
        alert('Select one doc at a time to print');
        allowSubmit = false;
        return;
    }
    else if (intclicked == 0) {
        alert('Document not selected');
        allowSubmit = false;
        return;
    }

    if (inv_window != null) {
        inv_window.close();
    }

    if (allowSubmit == true) {
        window.open("home?pg=riskmaster/TimeAndExpenses/timeandexpense-report" + "&amp;Invoiceid=" + invid, "Print",
		            "width=600,height=600,resizable=yes,scrollbars=yes,left=" +
		            (screen.availWidth - 590) / 2 + ",top=" +
		            (screen.availHeight - 590) / 2);
        return;
    }
}

function printhistory() {
    var allowSubmit = true;
    var claimid;

    if (eval(document.forms[0].optBillit) == null) {
        allowSubmit = false;
    }
    else {
        claimid = document.forms[0].claimid.value;
    }

    if (inv_window != null) {
        inv_window.close();
    }

    if (allowSubmit == true) {
        inv_window = window.open('TimeAndExpensesPrintHistory?mode=history&claimid=' + claimid, 'Print',
		                         'width=1000,height=600' + ',top=' +
		                         (screen.availHeight - 590) / 2 + ',left=' +
		                         (screen.availWidth - 990) / 2 + ',resizable=yes,scrollbars=yes');
    }
}

function printme() {
    window.print();
}

function printcheck1() {
    document.forms[0].action = "printchecks.asp";
    document.forms[0].submit();
}

function gotofunds() {
    var intclicked = 0;
    var allowSubmit = true;

    var obj = document.getElementById("gvExpenses");

    if (obj == null) {
        if (eval(document.forms[0].transid) == null) {
            allowSubmit = false;
        }
        else {
            window.location.href = "../FDM/Funds.aspx?recordID=" + document.forms[0].transid.value + "&FromTandE=True";
        }
        return false;
    }

    //subtract the total length by 1 so not to include the footer row
    var numOfRows = (document.getElementById("gvExpenses").rows.length - 2);

    //start at 2 so not to include the header row
    for (var i = 0; i < numOfRows; i++) {
        var iRow = i;

        //if (i < 10) {
        //    iRow = "0" + iRow;
        //}

        if (document.getElementById("gvExpenses_optBillit_"+iRow).checked == true) {
            var transid = document.getElementById("gvExpenses_transid_"+iRow).value;
            var payeeid = document.getElementById("gvExpenses_payeeid_"+iRow).value;
            var invoiceamt = document.getElementById("gvExpenses_hinvoiceamt_"+iRow).value;

            //MITS 16174
            //nsachdeva2 - MITS:27689 - 03/22/2012
            invoiceamt = invoiceamt.replace(/[^\d\x2D\x2E]/g, '');
            if (invoiceamt == '0.00') {
                //if (invoiceamt == '$0.00') {
                allowSubmit = false;
            }

            intclicked++;
        }
    }

    if (intclicked > 1) {
        alert('select only one invoice at a time');
        allowSubmit = false;
    }
    else if (intclicked == 0) {
        //MGaba2: MITS 22277:Correcting casing of Alert Msg
        //alert('select any invoice');
        alert('Please select an Invoice.');
        allowSubmit = false;
    }

    if (allowSubmit == true) {
        //window.location.href = "home?pg=riskmaster/Funds/trans&TransID=" +
        //                        document.forms[0].transid.value + '&ClaimID=' + document.forms[0].claimid.value;
        window.location.href = "../FDM/Funds.aspx?recordID=" + transid + "&FromTandE=True";
    }

    return false;
}

function SCLoaded(objWnd) {
    //alert(document.forms[0].txtcomment.value);
    objWnd.document.forms[0].fulltext.value = document.forms[0].txtcomment.value;
    objWnd.document.forms[0].callbackcancel.value = "SCCancel";
    objWnd.document.forms[0].callbackclose.value = "SCClose";
    objWnd.document.forms[0].submit();
}

function SCCancel(objWnd) {
    objWnd.close();
}

function SCClose(objWnd) {
    document.forms[0].txtcomment.value = objWnd.document.forms[0].fulltext.value;
    objWnd.close();
    alert("The spelling check is complete.");
}

//to what purpose does this function serve?
function SCUnload() { }

function AddRateDetail() {
    //this.document.callback="OnPaymentAdded";

    if (wnd == null) {
        document.getElementById("NewRateDetailAdded").value = "true";
        document.getElementById("ExistingRateDetailEdited").value = "false";

        //TODO: this is R4-style url, will need to change to R5 if this function is being called
        var wnd = window.open("home?pg=riskmaster/TimeAndExpense/rate-details", "TimeAndExpense",
                              "width=500,height=400,top=" +
                              (screen.availHeight - 390) / 2 + ",left=" +
		                      (screen.availWidth - 490) / 2 + ",resizable=yes,scrollbars=yes");
    }

    return false;
}

// Defect 001882 ; method added by Neelima Feb 2006
function ValidateInvoiceAmount() {
    var allowSubmit = true;
    var temptotavailable=null;

    //Arnab: MITS-9742 Added two check conditions for the required fields, Added "return allowSubmit;"
    if (document.forms[0].vendor.value == '')   //Checking for Vendor Name
    {
        allowSubmit = false;

        //Added by Nitin for Mits 12317
        if (document.forms[0].optallocate.checked == true) {
            alert('Enter Vendor Name');
        }
        else {
            alert('Enter Work Done By');
        }

        return allowSubmit;
    }
    //if () {
    //    return true;
    //}
    //RMA-15315:aaggarwal29 start
    if (document.forms[0].DistributionTypeTandE_codelookup_cid != null && (document.forms[0].DistributionTypeTandE_codelookup_cid.value==''|| document.forms[0].DistributionTypeTandE_codelookup_cid.value==0)) {
        allowSubmit = false;
        alert("Please select a Distribution Type.");
        return allowSubmit;
    }
    //add validation for EFT: TODO-Achla
    if (document.forms[0].EFTDistributionCodeId != null &&document.forms[0].IsEFTAccount!=null){
        if (document.forms[0].DistributionTypeTandE_codelookup_cid.value == document.forms[0].EFTDistributionCodeId.value && document.forms[0].IsEFTAccount.value != '1') {
            alert("EFT cannot be selected as a Distribution Type for Non EFT bank account.Please change the distribution type.");
            allowSubmit = false;
            return allowSubmit;
        }
    }
    if (document.forms[0].DistributionTypeTandE_codelookup_cid != null && document.forms[0].hdManualDistributionCodeId !=null) {
        if (document.forms[0].DistributionTypeTandE_codelookup_cid.value == document.forms[0].hdManualDistributionCodeId.value) {
            alert("Manual Distribution Type can only be selected with an available check number. Please change the distribution type.");
            allowSubmit = false;
            return allowSubmit;
        }
    }
//RMA-15315:aaggarwal29 end
    // MITS 15625 - MAC - Palinski changed txtfdate to invDate
    //if (document.forms[0].effsdate1 != null && document.forms[0].effedate != null && document.forms[0].txtfdate != null) {
    if (document.forms[0].effsdate1 != null && document.forms[0].effedate != null && document.forms[0].invDate != null) {
        var effectiveDate = document.forms[0].effsdate1.value;
        var expirydate = document.forms[0].effedate.value;
        // MITS 15625 - MAC - Palinski changed txtfdate to invDate
        //var invoicedateToCheck = getdbDate(document.forms[0].txtfdate.value);

        //vkumar258 - RMA-6037 - Starts

        var target = $("#" + "invDate");
        var inst = $.datepicker._getInst(target[0]);
        //var fdate = ("0" + (inst.selectedDay)).slice(-2);
        //var fmonth = ("0" + (inst.selectedMonth + 1)).slice(-2);
        //var fyear = inst.selectedYear;
        var sdate = $('#invDate').datepicker('getDate');//RMA-16243-msampathkuma
        if (sdate != null) {
            var fdate = ("0" + sdate.getDate()).slice(-2);
            var fmonth = ("0" + (sdate.getMonth() + 1)).slice(-2);
            var fyear = sdate.getFullYear();
            var nInvDate = fyear + fmonth + fdate;
        }
        
        if ((nInvDate < effectiveDate) || (nInvDate > expirydate))
        {
            //inst.selectedDay = inst.currentDay = effectiveDate.substr(6,2);
            //inst.selectedMonth = inst.currentMonth = effectiveDate.substr(4, 2)-1;
            //inst.selectedYear = inst.currentYear = effectiveDate.substr(0, 4);
            ////effectivedate1 = $.datepicker._formatDate(inst, inst.currentDay, inst.currentMonth, inst.currentYear);
            effectivedate1 = $.datepicker._formatDate(inst, effectiveDate.substr(6, 2), effectiveDate.substr(4, 2) - 1, effectiveDate.substr(0, 4));
            effectiveDate = (effectivedate1 != null ? effectivedate1 : $.datepicker._formatDate(sdate));

            //inst.selectedDay = inst.currentDay = expirydate.substr(6, 2);
            //inst.selectedMonth = inst.currentMonth = expirydate.substr(4, 2) - 1;
            //inst.selectedYear = inst.currentYear = expirydate.substr(0, 4);
            //expirydate1 = $.datepicker._formatDate(inst, inst.currentDay, inst.currentMonth, inst.currentYear);
             expirydate1 = $.datepicker._formatDate(inst, expirydate.substr(6, 2), expirydate.substr(4, 2) - 1, expirydate.substr(0, 4));
             expirydate = (expirydate1 != null ? expirydate1 : $.datepicker._formatDate(inst));
             alert("Invoice date must lie between Effective date (" + effectiveDate + ") and Expiration Date (" + expirydate + ") of the Rate Table");
            // akaushik5 Added for RMA-15315 Starts
            return false;
            // akaushik5 Added for RMA-15315 Ends
        }
        
        //vkumar258 - RMA-6037 - end



       // var invoicedateToCheck = getdbDate(document.forms[0].invDate.value);
        //nadim MITS 12159
       // var expirydate1 = expirydate.substr(4, 2) + "/" + expirydate.substr(6, 2) + "/" + expirydate.substr(0, 4);
        //var effectivedate1 = effectiveDate.substr(4, 2) + "/" + effectiveDate.substr(6, 2) + "/" + effectiveDate.substr(0, 4);

        if (effectiveDate == "" || expirydate == "") {
            if (document.forms[0].h_ratetablename != null) {
                var iEff = document.forms[0].h_ratetablename.value.indexOf("Date");
            }

            var iExp = document.forms[0].h_ratetablename.value.lastIndexOf("Date");
            effectiveDate = getdbDate(document.forms[0].h_ratetablename.value.substr(iEff + 5, 10));
            expirydate = getdbDate(document.forms[0].h_ratetablename.value.substr(iExp + 5, 10));
        }

        //if (effectiveDate != "" && expirydate != "") {
        //    if (invoicedateToCheck < effectiveDate || invoicedateToCheck > expirydate) {
        //        // MITS 15625 MAC - Changed "expiry" to "Expiration" and added dates to error message.
        //        alert("Invoice date must lie between Effective date (" + effectivedate1 + ") and Expiration Date (" + expirydate1 + ") of the Rate Table");
        //        return false;
        //    }
        //}
    }
    //End by Shivendu for MITS 11868

    //Checking whether checkbox is checked or not  by Nitin for Mits 12317
    if (document.forms[0].optallocate.checked == true) {
        //Checking for Invoice Amount, whether it is blank or negative or equal to zero
        //rupal:start, multicurrency
        //var invoiceamount = document.forms[0].invAmt.value;
        MultiCurrencyToDecimal_TandE(document.forms[0].invAmt);
        var invoiceamount = document.forms[0].invAmt.getAttribute("Amount");

        //invoiceamount = replace(invoiceamount, "$", "");
        //invoiceamount = replace(invoiceamount, ",", "");
        //if (document.forms[0].invAmt.value == '' || parseFloat(invoiceamount) == 0) {
        if (document.forms[0].invAmt.value == '' || parseFloat(invoiceamount) == 0 || parseFloat(invoiceamount) == 0.00) {
            allowSubmit = false;
            alert('Invoice Amount can not be Blank or equal to Zero');
            return allowSubmit;
        }
        //Added by yatharth MITS 17500
        MultiCurrencyToDecimal_TandE(document.forms[0].totavailable);
        temptotavailable = document.forms[0].totavailable.getAttribute("Amount");
        //temptotavailable = replace(temptotavailable, "$", "");
        //temptotavailable = replace(temptotavailable, ",", "");
        //Comparing Invoice amount with total available value
        if (parseFloat(temptotavailable) != 0 && parseFloat(temptotavailable) != 0.00) { //end //rupal
            allowSubmit = false;
            //alert('Not valid Inv Amount or Inv Amounts Are not Equal to Total Invoice'); //MITS:9742 - Commented alert to modify the message

            alert('Invoice Amounts Are not Equal to the sum of Invoice Amounts of Invoice Details'); //MITS:9742-Modified alert message
            return allowSubmit; //Arnab: MITS-9742 - Added "return allowSubmit;"
        }
    }
    else {
        //When no row is present in InvoiceDetails Div
        if (document.getElementById("div1").innerHTML == "Transaction Typeinvoice AmountbillableBill Amount") {
            alert("Please Enter Valid Invoice Details");
            allowSubmit = false;
            return allowSubmit;
        }
    }

    if (document.forms[0].invDate.value == '')    //Checking for the Invoice Date
    {
        allowSubmit = false;

        //Added by Nitin for Mits 12317
        if (document.forms[0].optallocate.checked == true) {
            alert('Enter Invoice Date');
        }
        else {
            alert('Enter Invoice Date');
        }
        return allowSubmit;
    }
    //MITS-9742 End

    document.forms[0].validated.value = allowSubmit.toString();
    //rupal:multicurrency, following code not required
    /*
    //Yatharth: MITS 17500: COverting the Avaliable amount to numeric from Currency
    if (document.forms[0].totavailable.value != null) {
        document.forms[0].totavailable.value = replace(document.forms[0].totavailable.value, "$", "");
        document.forms[0].totavailable.value = replace(document.forms[0].totavailable.value, ",", "");
        //document.forms[0].totavailable = parseFloat(document.forms[0].totavailable);
    }
    //End Yatharth
    */


    return allowSubmit;
}

//Defect 001886 fixed. Method added by Neelima Feb 2006
function InvoiceSelected() {
    var InvoiceId = 0;
    var intclicked = 0;
    var StatusCode = "";

    document.getElementById('hdnInvId').value = "";
    document.getElementById('h_status').value = "";

    var obj = document.getElementById("gvExpenses");

    if (obj == null) {
        return false;
    }

    //subtract the total length by 1 so not to include the footer row
    var numOfRows = (document.getElementById("gvExpenses").rows.length - 2);

    //start at 2 so not to include the header row
    for (var i = 0; i < numOfRows; i++) {
        var iRow = i;

        //if (i < 10) {
        //    iRow = "0" + iRow;
        //}

        if (document.getElementById("gvExpenses_optBillit_"+ iRow).checked == true) {
            intclicked++;

            InvoiceId = document.getElementById("gvExpenses_rowid_"+iRow).value;
            StatusCode = document.getElementById("gvExpenses_hstatus_"+iRow).value;
            document.getElementById('h_status').value = StatusCode;

            if (document.getElementById('hdnInvId').value != "") {
                //Modified by Shivendu for MITS 12320
                document.getElementById('hdnInvId').value = document.getElementById('hdnInvId').value + ',' + InvoiceId;
            }
            else {
                document.getElementById('hdnInvId').value = InvoiceId;
            }
        }
    }
}

//Arnab: MITS-10682 - Added method for OnBlur() event for the textbox
//It takes the control object. Makes out the button name. If the text is changed in the textbox it will fire
//the click event of that button which in turn fire the event written in button click event in view.xsl
function TransCodeLostFocus(sCtrl) {
    var sBtnCtrlName = sCtrl.id + "btn"; //Create button name
    var sValue = eval('document.forms[0].' + sCtrl.id);

    if (Trim(sValue.value) == "") {
        return false;
    }

    if (m_DataChanged) {
        var objFormElemButton = eval('document.forms[0].' + sBtnCtrlName);  //Create button element
        objFormElemButton.click();
        return true;
    }
}

//Desc: Looks for whether the data in the textbox has modified or not
//It is fired from the onChange() event of textbox in view.xsl
//function UpdateDataChanged(bFlag)
function UpdateDataChanged(sCtrl) {
    if (m_LookupBusy) {
        window.alert("The application is busy looking up data for a previous field.\nPlease wait for the results before modifying additional fields.");
        return false;
    }

    m_DataChanged = true;
    var sBtnCtrlName = sCtrl.id + "btn"; //Create button name
    var objFormElemButton = eval('document.forms[0].' + sBtnCtrlName); //Create button element
    objFormElemButton.focus();

    return m_DataChanged;
}
//MITS-10682 - End

//Start by Shivendu for MITS 12320
function SelectAllCheckBoxes() {
    var s;
    var value;

    if (document.getElementById("btnSelectAll") != null && document.getElementById("btnSelectAll").value == "Select All") {
        document.getElementById("btnSelectAll").value = "Undo Select";
        value = true;
    }
    else if (document.getElementById("btnSelectAll") != null) {
        document.getElementById("btnSelectAll").value = "Select All";
        value = false;
    }

    for (var i = 0; i < document.forms[0].length; i++) {
        if ((document.forms[0][i].type == "checkbox")) {
            s = document.forms[0][i];

            if (s.id.indexOf("optBillit") >= 0) {
                s.checked = value;
            }
        }
    }

    InvoiceSelected();
}
//End by Shivendu for MITS 12320

function checkVal() {
    if (document.getElementById('chkprintTE').checked) {
        document.getElementById('autorpt').value = 1;
    }
    else {
        document.getElementById('autorpt').value = 0;
    }
}

function checkBill(id) {
    var s = document.getElementById(id);

    if (s.checked) {
        document.getElementById('h_invoiceid').value = id;

        if (document.getElementById('hdnInvId').value != "") {
            document.getElementById('hdnInvId').value = document.getElementById('hdnInvId').value + ' ' + s.value;
        }
        else {
            document.getElementById('hdnInvId').value = s.value;
        }
    }
}

function checkRateTable() {
    var RateTable = document.getElementById('ratetable').value;
    var RateTableId = document.getElementById('ratetableid').value;
    var ClaimId = document.getElementById('claimid').value;
    var ClaimNum = document.getElementById('hclmnum').value;
    var ClaimantEid = document.getElementById('ClaimantEid').value; //MITS 16608
    var UnitId = document.getElementById('UnitId').value;//MITS 16608

    //Start - MITS 15559 - mpalinski
    if (RateTable == '') {
        RateTable = document.getElementById('h_ratetable').value;
        document.getElementById('ratetable').value = RateTable;
    }
    //End - MITS 15559

    //Start - MITS 15543 - mpalinski
    if (ClaimNum == '') {
        ClaimNum = document.getElementById('lblClaimNum').outerText;
        document.getElementById('hclmnum').value = ClaimNum;
    }
    //End - MITS 15543

    if (RateTable == '') {
        alert("Select Rate Table");
        return false;
    }

    if (RateTableId == '') {
        alert("Select Rate Table");
        return false;
    }

    //MITS 15559 & 16608 by MJP
    window.location.href = "../TAndE/TimeAndExpenseInvoice.aspx?mode=new" + "&ClaimId=" + ClaimId +
                           "&ClaimNum=" + ClaimNum + "&ClaimantEid=" + ClaimantEid + "&UnitId=" + UnitId +
                           "&RateTableId=" + RateTableId + "&RateTable=" + RateTable;

    return false;
}

function closeNewWin() {
    if (newwin != null) {
        newwin.close();
        newwin = null;
    }
}

function SaveRecord() {
    document.forms[0].actionid.value = "billit";
    document.forms[0].allowclsclm.value = "False";
    return true;
}

//Added By: Nikhil Garg		Bug No: 434		Dated: 02-Nov-2005
//R5 changes by: MJP                        Dated: 22-Jan-2009
function BackToClaim() {
    //pleaseWait.Show('start');

    window.location.href = "../FDM/claimgc.aspx?SysFormId=" + document.all("claimid").value + "&SysCmd=0";
    return false;
}

//MITS 15850 - MJP
function BackToFinancials() {
    //pleaseWait.Show('start');

    //MITS 16608 - added the ClaimaintEid and UnitId in case Reserve Tracking "Detail Level" is turned on via LOB setup...
    window.location.href = "../../UI/Reserves/ReserveListing.aspx?ClaimId=" + document.all("claimid").value + "&ClaimantEid=" + document.all("ClaimantEid").value + "&UnitId=" + document.all("UnitId").value;
    return false;
}

function BackToTandE() {
    //pleaseWait.Show('start');
    var ClaimId = document.all("claimid").value;
    var RateTable = document.all("h_ratetablename").value; //MITS 15559 by MJP
    var RateTableId = document.all("h_ratetableid").value; //MITS 15559 by MJP
    var ClaimantEid = document.all("h_claimanteid").value; //MITS 16608 by MJP
    var UnitId = document.all("h_unitid").value; //MITS 16608 by MJP

    //MITS 15559 & 16608 - added the ClaimaintEid and UnitId in case Reserve Tracking "Detail Level" is turned on via LOB setup...
    window.location.href = "../../UI/TAndE/TimeAndExpense.aspx?ClaimId=" + ClaimId + "&ClaimantEid=" + ClaimantEid + "&RateTableId=" + RateTableId + "&RateTable=" + RateTable + "&UnitId=" + UnitId;
    return false;
}

// Anjaneya MITS 12347 Changes Start
function setClaimTable(RateTableName, TableId, CustomerId, ClaimNumber) {
    //Start by Nitin for Mits 12406
    RateTableName = String(RateTableName).replace(/&apos;/g, "'");
    RateTableName = String(RateTableName).replace(/`/g, "`'");
    RateTableName = String(RateTableName).replace(/`/g, "");
    RateTableName = String(RateTableName).replace(/&amp;/g, '&'); // MITS 35421
    //End by Nitin for Mits 12406

    if (TableId != null && TableId != "") {
        document.forms[0].ratetable.value = RateTableName;
        document.forms[0].ratetableid.value = TableId;
        document.forms[0].h_ratetable.value = RateTableName; //MITS 15559 by MJP
        document.forms[0].hcustid.value = CustomerId;
        document.forms[0].hclmnum.value = ClaimNumber;
        document.forms[0].hclmnumtag.value = ClaimNumber;
    }
}
// Anjaneya MITS Changes End

function PageLoaded(errortext) {
    //debugger;
    //pleaseWait.Show('start');

    if (document.getElementById('autorpt').value == '') {
        document.getElementById('chkprintTE').checked = false;
    }

    if (document.getElementById('autorpt').value == '1') {
        document.getElementById('autorpt').value = '0';

        // window.open("home?pg=riskmaster/TimeAndExpenses/timeandexpense-billreport", "Print",
        //             "width=600,height=600,resizable=yes,scrollbars=yes,left=" +
        //             (screen.availWidth - 500) / 2 + ",top=" +
        //             (screen.availHeight - 290) / 2);

        window.open("TimeAndExpensePrintHistory.aspx?mode=report&invoiceid=" + document.getElementById('hdnInvId').value, "Print",
                    "width=600,height=600,resizable=yes,scrollbars=yes,left=" +
                    (screen.availWidth - 590) / 2 + ",top=" +
                    (screen.availHeight - 590) / 2);
    }

    //Insufficient Reserve Amount
    if (errortext != '') {
        if (errortext == 'Insufficient Reserves Available') {
            var sLink = 'home?pg=riskmaster/Funds/InsufficientReserves';

            if (newwin != null) {
                newwin.close();
                newwin = null;
            }

            this.document.callback = "OnInsufficientReservesDisplayed";

            window.open(sLink, 'SummaryTandE', 'width=500,height=300' + ',top=' +
                                (screen.availHeight - 290) / 2 + ',left=' +
                                (screen.availWidth - 490) / 2 +
                                ',resizable=yes,scrollbars=yes');

            return false;
        }
    }
}

function SetBack() {
    window.history.back();
    return true;
}

function PrintH(id) {
    // window.open("home?pg=riskmaster/TimeAndExpenses/print-history&claimid=" + id, "Print",
    //			   "width=1000,height=600,resizable=yes,scrollbars=yes,left=" +
    //			   (screen.availWidth - 500) / 2 + ",top=" + 
    //             (screen.availHeight - 290) / 2);

    window.open("TimeAndExpensePrintHistory.aspx?mode=history&claimid=" + id, "Print",
                "width=1000,height=600,resizable=yes,scrollbars=yes,left=" +
                (screen.availWidth - 990) / 2 + ",top=" +
                (screen.availHeight - 590) / 2);

    return false;
}

//function PrintR() {
//    if (document.all.h_invoiceid.value!="") {
//        window.open("home?pg=riskmaster/TimeAndExpenses/timeandexpense-report" +
//                    "&Invoiceid=" + document.all.h_invoiceid.value, "Print",
//                    "width=600,height=600,resizable=yes,scrollbars=yes,left=" +
//                    (screen.availWidth - 500) / 2 + ",top=" + 
//                    (screen.availHeight - 290) / 2);
//        return false;
//    }
//    
//    if (document.all.h_invoiceid.value="") {
//        alert("Document not selected");
//        return false; 
//    }
//}

function OpenDetails(id) {
    window.open("home?pg=riskmaster/TimeAndExpenses/summary-timeandexpense-details" + "&InvoiceId=" + id, "Invoice",
                "width=600,height=600,resizable=yes,scrollbars=yes,left=" +
                (screen.availWidth - 590) / 2 + ",top=" +
                (screen.availHeight - 590) / 2);
    return false;
}

//pmittal5 Mits 14018 12/12/08 - Search for Adjusters in case of "Work Done By" and for Entities in case of "Vendor"
// MITS 15624 - MAC
function CheckAllocatedForLookup() {
    if (document.forms[0].optallocate.checked == true)
        lookupData('vendor', '', 4, 'vendor', 2);
    else
        lookupData('vendor', '1047', 4, 'vendor', 2); // 1047 - Adjuster Table Id
}

//rupal:start
function MultiCurrencyOnBlur_TandE(objCtl) {
    var functionname = objCtl.getAttributeNode("onblur").value;
    functionname = functionname.split('(')[0];
    window[functionname](objCtl);
}

function MultiCurrencyToDecimal_TandE(objCtl) {
    var functionname = objCtl.getAttributeNode("onfocus").value;
    //remove 'format_' word
    functionname = functionname.replace("Format_", "");
    functionname = functionname.split('(')[0];
    window[functionname](objCtl);
}
//rupal:end
