function PageLoad(listname)
{
    var bChecked = false;
    var gridElementsRadio = document.getElementsByName('MyRadioButton');
    //igupta3 Mits:33301
    if (gridElementsRadio.length == 0 && !window.ie) {
        var num = 0;
        var gridElementsnew = new Array();
        try {
            var input = document.getElementsByTagName('input');
            for (i = 0; i < input.length; i++) {
                if (input[i].id == "MyRadioButton") {
                    //gridElements[num] = document.getElementById('MyRadioButton');
                    gridElementsnew[num] = input[i];
                    num++;
                }
            }
            gridElementsRadio = gridElementsnew;
        }
        catch (e) {
        }
    }
    //Mgaba2:MITS 19613:Made the changes for FROI Options.
    //Correct Radio button was not getting selected in below mentioned scenario
    //For example:client filters the records without filling any criteria
    //clicks radio button of one of the record ,again filters the records by enterring say Jurisdiction as California
    //Then radio button selection is not changed
    
    var sHdnValue = document.getElementById('hdnKey').value;

    if (sHdnValue == "") {
        document.getElementById('hdnKey').value = "k-1;-1";
        sHdnValue = "k-1;-1";
    }
        
    if (gridElementsRadio != null) {

        for (var i = 0; i < gridElementsRadio.length; i++) {
            var gridName = gridElementsRadio[i].name;
           // if (listname == gridName && gridElementsRadio[i].checked) {
            if (listname == gridName && gridElementsRadio[i].value == sHdnValue) {
                gridElementsRadio[i].checked = true;
                //bChecked = true;
                break;
            }
        }
    }
    
//    if (!bChecked && gridElementsRadio[0] != null)
//        gridElementsRadio[0].checked = true;

//    if (document.getElementById('hdnKey').value == "")
    //        document.getElementById('hdnKey').value = "k-1;-1";

    Enable();
    return false;
}

function Enable()
{
	// Enabling/Disabling for Claim Administrator Org Hierarchy
	if (document.getElementById('ClmAdm2').checked)
	{
		document.getElementById('ClmAdminOrgHier').disabled = false;
	}
	else
		document.getElementById('ClmAdminOrgHier').disabled = true;
	
	// Claim Administrator TPA
	if (document.getElementById('ClmAdm5').checked)
	{
		document.getElementById('ClmAdmTPA').disabled = false;
		document.getElementById('ClmAdmTPAbtn').disabled = false;
	}
	else
	{
		document.getElementById('ClmAdmTPA').disabled = true;
		document.getElementById('ClmAdmTPAbtn').disabled = true;
	}
		
	// Claim Administrator Default Company
	if (document.getElementById('ClmAdm4').checked)
	{
		document.getElementById('ClmAdmDef').disabled = false;
		document.getElementById('ClmAdmDefbtn').disabled = false;
	}
	else
	{
		document.getElementById('ClmAdmDef').disabled = true;
		document.getElementById('ClmAdmDefbtn').disabled = true;
	}
	
	// Carrier - Org Hierarchy
	if (document.getElementById('Carrier1').checked)
		document.getElementById('CarrierOrgHier').disabled = false;
	else
		document.getElementById('CarrierOrgHier').disabled = true;
	
	// Carrier - Default Company
	if (document.getElementById('Carrier2').checked)
	{
		document.getElementById('CarrierDef').disabled = false;
		document.getElementById('CarrierDefbtn').disabled = false;
	}
	else
	{
		document.getElementById('CarrierDef').disabled = true;
		document.getElementById('CarrierDefbtn').disabled = true;
	}
	//Start by Shivendu for MITS 11367
	
	if (document.getElementById('BlankTPA')!=null && document.getElementById('BlankTPA').checked)
	{
		document.getElementById('TPALookup').style.display = 'none';
		document.getElementById('TPALookupbtn').style.display = 'none';
	}
	else if(document.getElementById('DefaultTPA')!=null && document.getElementById('DefaultTPA').checked)
	{
		document.getElementById('TPALookup').style.display = '';
		document.getElementById('TPALookupbtn').style.display = '';
	}
	//End by Shivendu for MITS 11367
	
	// Check whether FROI Options or Juris Options
	if (document.getElementById('Neb1') != null)
	{
		// Nebraska - Org Hierarchy
		if (document.getElementById('Neb1').checked)
			document.getElementById('NebraskaOrgHier').disabled = false;
		else
			document.getElementById('NebraskaOrgHier').disabled = true;
		
		// Nebraska - Parent Company
		    if (document.getElementById('Neb2').checked)
		    {
			    document.getElementById('NebraskaDef').disabled = false;
			    document.getElementById('NebraskaDefbtn').disabled = false;
		    }
		    else
		    {
			    document.getElementById('NebraskaDef').disabled = true;
			    document.getElementById('NebraskaDefbtn').disabled = true;
		    }
		
		// Tennessesse - Org Hierarchy
		if (document.getElementById('Ten1').checked)
			document.getElementById('TennessesseOrgHier').disabled = false;
		else
			document.getElementById('TennessesseOrgHier').disabled = true;
		
		// Tennessesse - Parent Company
		if (document.getElementById('Ten2').checked)
		{
			document.getElementById('TennessesseDef').disabled = false;
			document.getElementById('TennessesseDefbtn').disabled = false;
		}
		else
		{
			document.getElementById('TennessesseDef').disabled = true;
			document.getElementById('TennessesseDefbtn').disabled = true;
		}
		
		// Virginia - Org Hierarchy
		if (document.getElementById('Vir1').checked)
			document.getElementById('VirginiaOrgHier').disabled = false;
		else
			document.getElementById('VirginiaOrgHier').disabled = true;
		
		// Virginia - Parent Company
		if (document.getElementById('Vir2').checked)
		{
			document.getElementById('VirginiaDef').disabled = false;
			document.getElementById('VirginiaDefbtn').disabled = false;
		}
		else
		{
			document.getElementById('VirginiaDef').disabled = true;
			document.getElementById('VirginiaDefbtn').disabled = true;
		}
	}
	
	return false;
}

function LoadValues(listname) {    
        var gridElementsRadio = document.getElementsByName('MyRadioButton');
    //igupta3 Mits:33301
        if (gridElementsRadio.length == 0 && !window.ie) {
            var num = 0;
            var gridElementsnew = new Array();
            try {
                var input = document.getElementsByTagName('input');
                for (i = 0; i < input.length; i++) {
                    if (input[i].id == "MyRadioButton") {
                        //gridElements[num] = document.getElementById('MyRadioButton');
                        gridElementsnew[num] = input[i];
                        num++;
                    }
                }
                gridElementsRadio = gridElementsnew;
            }
            catch (e) {
            }
        }

	    if(gridElementsRadio != null)
	    {
	        for(var i=0;i<gridElementsRadio.length;i++)
	        {
	            var gridName = gridElementsRadio[i].name;
	            if(listname == gridName && gridElementsRadio[i].checked)
	            {
	                document.getElementById('hdnKey').value=gridElementsRadio[i].value;
	                break;
	            }
            }
        }
	document.getElementById('hdnAction').value = 'LoadValue';
	document.forms[0].submit();
	return true;
}

function ValidateAndSave()
{
	for(var i=0;i<document.forms[0].elements.length;i++)
	{
		var objElem=document.forms[0].elements[i];
		var sId=new String(objElem.id);
		
		//Start by SHivendu for MITS 11367
		if(document.getElementById('DefaultTPA')!=null && document.getElementById('DefaultTPA').checked)
		{
		    if (document.getElementById('TPALookup')!=null && document.getElementById('TPALookup').value == "")
				    {
					    alert("Default TPA can not be selected without an entity.");
					    document.getElementById('TPALookup').focus();
					    return false;
				    }
	    }
		//End by SHivendu for MITS 11367
		// Claim Administrator validation
		if(sId.substring(0,6)=="ClmAdm" && objElem.type == "radio" && objElem.checked)
		{
			if (sId.substring(6) == "4") // case of WC Claim Admin
			{
				if (document.getElementById('ClmAdmDef').value == "")
				{
					alert("Claim Administrator Default Entity can not be selected without an entity.");
					document.getElementById('ClmAdmDef').focus();
					return false;
				}
			}
			else if (sId.substring(6) == "5") // case of TPA
			{
				if (document.getElementById('ClmAdmTPA').value == "")
				{
					alert("Claims Administrator - TPA Entity can not be selected without an entity.");
					document.getElementById('ClmAdmTPA').focus();
					return false;
				}
			}
		}
		
		// Carrier validation
		if(sId.substring(0,7)=="Carrier" && objElem.type == "radio" && objElem.checked)
		{
			if (sId.substring(7) == "2") // case of Default Carrier
			{
				if (document.getElementById('CarrierDef').value == "")
				{
					alert("Insurance Carrier Default Entity can not be selected without an entity.");
					document.getElementById('CarrierDef').focus();
					return false;
				}
			}
		}
		
		// Jurisdiction of Nebraska - validation
		if(sId.substring(0,3)=="Neb" && objElem.type == "radio" && objElem.checked)
		{
			if (sId.substring(3) == "2") // case of Parent Company
			{
				if (document.getElementById('NebraskaDef').value == "")
				{
					alert("Jurisdiction of Nebraska default parent can not be selected without an entity.");
					document.getElementById('NebraskaDef').focus();
					return false;
				}
			}
		}
		
		// Jurisdiction of Tennessesse - validation
		if(sId.substring(0,3)=="Ten" && objElem.type == "radio" && objElem.checked)
		{
			if (sId.substring(3) == "2") // case of Parent Company
			{
				if (document.getElementById('TennessesseDef').value == "")
				{
					alert("Jurisdiction of Tennessesse default parent can not be selected without an entity.");
					document.getElementById('TennessesseDef').focus();
					return false;
				}
			}
		}
		
		// Jurisdiction of Virginia - validation
		if(sId.substring(0,3)=="Vir" && objElem.type == "radio" && objElem.checked)
		{
			if (sId.substring(3) == "2") // case of Parent Company
			{
				if (document.getElementById('VirginiaDef').value == "")
				{
					alert("Jurisdiction of Virginia default parent can not be selected without an entity.");
					document.getElementById('VirginiaDef').focus();
					return false;
				}
			}
		}
	} // end of for loop
	
	document.getElementById('hdnAction').value = 'Save';
	return true;
}

function DeleteRecord(listname)
{
	var bDel = false;
	bDel = confirm('Are you sure you want to delete the record?');
	if (bDel)
	{
	    var gridElementsRadio = document.getElementsByName('MyRadioButton');
	    //igupta3 Mits:33301
	    if (gridElementsRadio.length == 0 && !window.ie) {
	        var num = 0;
	        var gridElementsnew = new Array();
	        try {
	            var input = document.getElementsByTagName('input');
	            for (i = 0; i < input.length; i++) {
	                if (input[i].id == "MyRadioButton") {
	                    //gridElements[num] = document.getElementById('MyRadioButton');
	                    gridElementsnew[num] = input[i];
	                    num++;
	                }
	            }
	            gridElementsRadio = gridElementsnew;
	        }
	        catch (e) {
	        }
	    }

	    if(gridElementsRadio != null)
	    {
	        for(var i=0;i<gridElementsRadio.length;i++)
	        {
	            var gridName = gridElementsRadio[i].name;
	            if(listname == gridName && gridElementsRadio[i].checked)
	            {
	                if(gridElementsRadio[i].value == "k-1;-1")
	                {
	                    alert("Can not delete the double default record.");
	                    return false;
	                }
	            }
            }
        }
		document.getElementById('hdnAction').value = 'Delete';
		document.forms[0].submit();
		return true;
	}
	else
		return false;
}

function NewRecord(listname)
{
	var sToSend = "";
	var gridElementsRadio = document.getElementsByName('MyRadioButton');
    //igupta3 Mits:33301
	if (gridElementsRadio.length == 0 && !window.ie) {
	    var num = 0;
	    var gridElementsnew = new Array();
	    try {
	        var input = document.getElementsByTagName('input');
	        for (i = 0; i < input.length; i++) {
	            if (input[i].id == "MyRadioButton") {
	                //gridElements[num] = document.getElementById('MyRadioButton');
	                gridElementsnew[num] = input[i];
	                num++;
	            }
	        }
	        gridElementsRadio = gridElementsnew;
	    }
	    catch (e) {
	    }
	}
	    if(gridElementsRadio != null)
	    {
	        for(var i=0;i<gridElementsRadio.length;i++)
	        {
	            var gridName = gridElementsRadio[i].name;
	            if(listname == gridName)
	            {
	               sToSend = sToSend + gridElementsRadio[i].value.substring(1) + "|";
	            }
            }
        }

	sToSend = sToSend.substring(0, sToSend.length - 1);
	sToSend = sToSend + "&form=" + document.getElementById('hdnForm').value;
	window.open('FROIOptionsNew.aspx?list=' + sToSend, 'FROIOptionsNew', 'width=400,height=300,top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 400) / 2 + ',resizable=no,scrollbars=yes');
	document.getElementById('hdnAction').value = 'SaveNew';
	return false;
}

function ToggleEntity()
{

	if (document.getElementById('allorg').checked)
	{
		document.getElementById('EntityOrgHier').disabled = true;
		document.getElementById('EntityOrgHierbtn').disabled = true;
	}
	else if (document.getElementById('selectorg').checked)
	{
		document.getElementById('EntityOrgHier').disabled = false;
		document.getElementById('EntityOrgHierbtn').disabled = false;
	}
	
	if (document.getElementById('alljuris').checked)
	{
		document.getElementById('JurisState').disabled = true;
		document.getElementById('JurisStatebtn').disabled = true;
	}
	else if (document.getElementById('selectjuris').checked)
	{
		document.getElementById('JurisState').disabled = false;
		document.getElementById('JurisStatebtn').disabled = false;
	}
	return false;
}

function SaveNew()
{
	var sList = new String(document.getElementById('ExistingList').value);
	var sJuris = "";
	var sSel = "";

	if (document.getElementById('selectorg').checked)
	{
		if (document.getElementById('EntityOrgHier').value == "")
		{
			alert('Please select an Entity.');
			document.getElementById('EntityOrgHier').focus();
			return false;
		}
		sSel = document.getElementById('EntityOrgHier_cid').value;
	}
	else
	{
		sSel = "-1";
	}
	if (document.getElementById('selectjuris').checked)
	{
		if (document.getElementById('JurisState').value == "")
		{
			alert('Please select a Jurisdictional State.');
			document.getElementById('JurisState').focus();
			return false;
		}
		sJuris = document.getElementById('JurisState_cid').value;
	}
	else
	{
		sJuris = "-1";
	}
	
	if (sList.indexOf(sJuris+";"+sSel) == -1)
	{
		document.getElementById('hdnAction').value = 'SaveNew';
		document.forms[0].FormMode.value="close";	
		window.opener.document.forms[0].hdnKey.value = ""; //MGaba2:MITS 19613
		return true;
	}
	else
	{
		alert('That combination exists in the database and cannot be added.');
		return false;
	}
}

function checkWindow()
{
//Parijat : Mits 11825
    if(document.forms[0].FormMode != null)
    {
	    if (document.forms[0].FormMode.value=="close")
	    {
		    window.opener.document.forms[0].submit();	
		    window.close();
	    }
	}
}
//Bijender has changed for Mits 15669
function PrintHistory() 
{
	var bChecked = false;
	var sFormId = "";
	var gridElementsRadio = document.getElementsByName('MyRadioButton');
    //igupta3 Mits:33301
	if (gridElementsRadio.length == 0 && !window.ie) {
	    var num = 0;
	    var gridElementsnew = new Array();
	    try {
	        var input = document.getElementsByTagName('input');
	        for (i = 0; i < input.length; i++) {
	            if (input[i].id == "MyRadioButton") {
	                //gridElements[num] = document.getElementById('MyRadioButton');
	                gridElementsnew[num] = input[i];
	                num++;
	            }
	        }
	        gridElementsRadio = gridElementsnew;
	    }
	    catch (e) {
	    }
	}
	    if(gridElementsRadio != null)
	    {
	        for(var i=0;i<gridElementsRadio.length;i++)
	        {
	            var gridName = gridElementsRadio[i].name;
	            if (gridName == 'FROIFormsGrid' && gridElementsRadio[i].checked)
	            {
	                bChecked = true;
	                sFormId = gridElementsRadio[i].value;
	                break;
	            }
            }
        }
	if (!bChecked)
	{
		alert('Please select a form first.');
		return false;
	}
	window.open('FROIHistory.aspx?formid='+sFormId,'FROIHistory',
		'width=500,height=300,top='+(screen.availHeight-400)/2+',left='+(screen.availWidth-400)/2+',resizable=no,scrollbars=yes');

	return false;
}
//Yatharth: MITS 20033: Parameter added as the function call is common for 2 screens: FROIPrep.aspx, WCFormPrep.aspx
function CheckForSkip(screenname) {
    var orghierarchy;
    var claimid;
    var folder;
    
    if (screenname == 'FROI') {
        if (document.getElementById('SkipFroiPreparerPrompt').value == '1') {
            //document.forms[0].action = "FroiForms.aspx";
            document.forms[0].action = "WCFormPrep.aspx?Submit=FroiForms";
            document.forms[0].submit();
            return false;
        }
        document.getElementById('AttachForm_chk').checked = false;
    }
    else if (screenname == "WCForm") {
        //Start:commented by psaiteja for MITS 23840 
        // path changed to rmxref = "/Instance/Document/JurisPreparer/rememberInfo"
     //if(document.getElementById('SkipJurisPreparerPrompt') != null)
     //{
     // if (document.getElementById('SkipJurisPreparerPrompt').value == '1') {
     //       orghierarchy = document.getElementById("OrgHierarchy").value;
     //       claimid = document.getElementById("ClaimId").value;
     //       folder = document.getElementById("Folder").value;
     //       document.forms[0].action = "WCForms.aspx?claimid="+ claimid + "&folder=" + folder + "&OrgHierarchy=" + orghierarchy;
     
     //   document.forms[0].submit();
     //   return false;
     //   }
        // }
        if (document.getElementById('skipprompt') != null) {
            if (document.getElementById('skipprompt').value == 'True' || document.getElementById('skipprompt').value == 'true') {
                orghierarchy = document.getElementById("OrgHierarchy").value;
                claimid = document.getElementById("ClaimId").value;
                folder = document.getElementById("Folder").value;
//            document.forms[0].action = "WCForms.aspx?claimid="+ claimid + "&folder=" + folder + "&OrgHierarchy=" + orghierarchy;
            document.forms[0].action = "WCFormPrep.aspx?Submit=WCForms";
        document.forms[0].submit();
                return false;
            }
        }
        //End:commented by psaiteja for MITS 23840 
    }
}
function OpenFroiForms() {
//    document.forms[0].action = "FroiForms.aspx";
    document.forms[0].action = "WCFormPrep.aspx?Submit=FroiForms";
    document.forms[0].submit();
    return false;
}

//MITS 20033:Yatharth: Function added to open WC-PDF Forms
function OpenWCForms() {
    var sClaimId = document.getElementById("ClaimId").value;
    var sState = document.getElementById("Folder").value;
    var sOrgHierarchyId = document.getElementById("OrgHierarchy").value;
    
//    document.forms[0].action = "WCForms.aspx?claimid=" + sClaimId + '&folder=' + sState + '&OrgHierarchy=' + sOrgHierarchyId;
    document.forms[0].action = "WCFormPrep.aspx?Submit=WCForms";
    document.forms[0].submit();
    return false;
}
	
					
function OnCheckChange()
	{
		if(document.forms[0].AttachForm_chk.checked==true)
		{
			document.forms[0].AttachForm.value='true';
		}
		else
			document.forms[0].AttachForm.value='false';
	}
function Back(p_iID, p_sFormName ) {
      try
	  {
		if(p_sFormName=="")
		{
			var objFormNameError = new Error ("Target form name is blank, please provide a valid form name for navigation.");
			throw objFormNameError;
		}
		if(wnd == null)
		{
		    if (p_sFormName == "241")
		        p_sFormName = "claimgc"
		    else if (p_sFormName == "242")
		        p_sFormName = "claimva"
            //MITS 22555 psaiteja Start
		    //var wnd = window.location(p_sFormName + ".aspx?recordID=" + p_iID + "&SysCmd=0");
		    var wnd = window.location.replace("/RiskmasterUI/UI/FDM/" + p_sFormName + ".aspx?recordID=" + p_iID + "&SysCmd=0");
		    //MITS 22555 psaiteja END
		}
	  } 
     catch(p_objErr)
     {
       throw p_objErr;
     }
     return false;
   }
function TrimIt(aString) 
{
	// RETURNS INPUT ARGUMENT WITHOUT ANY LEADING OR TRAILING SPACES
	return (aString.replace(/^ +/, '')).replace(/ +$/, '');
}
					
//This function is used to Popup a window on which fdf will be streamed back.
// This will handle the default behaviour in IE Browsers Ver > 5.01
// However if user has updated his settings e.i. through Tools->Internet Options -> Advanced Tab-> Under
// Browsing options 'Reuse Window for Launching Shortcuts' is unchecked
// then a problem of a blank window coming would occur.
//There is no suitable way avl to detect this setting through browser
// It can be done through JScript.
function ShowFroiPdf(sFormId)   
{
	
	var sClaimId = document.getElementById('ClaimId').value;
        var sAttachForm = document.getElementById('AttachForm').value;
	var sName = document.getElementById('Name').value;
	var sTitle = document.getElementById('Title').value;
	var sPhone = document.getElementById('Phone').value;
	var sQueryString = 'FormId=' + sFormId + '&ClaimId=' + sClaimId + '&AttachForm=' + sAttachForm + '&Name=' + sName + '&Title=' + sTitle + '&Phone=' + sPhone;
	
	//This window will provide ready context for pdf document/xml coming back
	//If JScript is allowed to run we can make opening of blank window
	// as conditional by calling ReadRegistry function.
	//var myWindow = window.open('', sFormId,'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');
	var m_windowCallToXPL = window.open('FROIPdf.aspx?' + sQueryString, sFormId,'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');
	return false;
}


//This is a sample function to show how we can use JScript to detect
//function ReadRegistry{
//
//		var WshShell = new ActiveXObject("WScript.Shell");
//               var c = WshShell.RegRead("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Internet Explorer\\AdvancedOptions\\BROWSE\\REUSEWINDOWS\\Checkedvalue");
//		
//}

function ShowWCPdf(sFormId)
{
	var sClaimId = document.getElementById('ClaimId').value;
    var sState = document.getElementById('State').value;
    //MITS - 10753 - Added OrgHierarchyId into the query string
	var sOrgHierarchyId = document.getElementById('OrgHierarchy').value;	
	//Arnab	
	var sName = document.getElementById('Name').value;
	var sTitle = document.getElementById('Title').value;
	var sPhone = document.getElementById('Phone').value;
	//MITS - 10753 - Modified the query string
	var sQueryString = 'FormId=' + sFormId + '&ClaimId=' + sClaimId + '&State='+sState+'&amp;Name=' + sName + '&Title=' + sTitle + '&Phone=' + sPhone + '&OrgHierarchy=' + sOrgHierarchyId;
	
	//This window will provide ready context for pdf document/xml coming back
	//If JScript is allowed to run we can make opening of blank window 
	// as conditional by calling ReadRegistry function. 		
	//var myWindow = window.open('', sFormId,'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');						

	var m_windowCallToXPL = window.open('WCFormPdf.aspx?' + sQueryString, sFormId,'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');																	
	return false;						
}

function ShowAcordPdf(sFormId,sFormText)
{
    var sClaimId = document.getElementById('ClaimId').value;
    //Geeta : Mits 13482
	var sClaimNumber = document.getElementById('ClaimNumber').value;
	var sQueryString = 'FormId=' + sFormId + '&ClaimId=' + sClaimId + '&Name=' + sFormText + '&ClaimNumber=' + sClaimNumber;

	//This window will provide ready context for pdf document/xml coming back
	//If JScript is allowed to run we can make opening of blank window 
	// as conditional by calling ReadRegistry function. 		
	//var myWindow = window.open('', sFormId,'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');	
	var m_windowCallToXPL = window.open('AcordFormPdf.aspx?' + sQueryString,sFormId ,'width=650,height=550,top=' + (screen.availHeight - 550) / 2 + ',left=' + (screen.availWidth - 650) / 2 + ',resizable=yes,scrollbars=yes');
	return false;						
}

function ShowClaimFormPdf(sFormId)
{
	var sClaimId = document.getElementById('ClaimId').value;
        var sState = document.getElementById('State').value;
	
	var sQueryString = 'FormId='+sFormId+'&ClaimId='+sClaimId+'&State='+sState;
	
	//This window will provide ready context for pdf document/xml coming back
	//If JScript is allowed to run we can make opening of blank window 
	// as conditional by calling ReadRegistry function. 		
	//var myWindow = window.open('', sFormId,'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');						

	///
	///
	var m_windowCallToXPL = window.open('home?pg=riskmaster/FROI/ClaimFormPdf&amp;' + sQueryString, sFormId,'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');
	//return false;						
}
// Validates/Formats the phone number field after it loses focus
function phoneLostFocus(objCtrl)
{
	if(objCtrl.value.length==0)
			return false;
	
	var sValue=new String(objCtrl.value);
	sValue=stripNonDigits(sValue);
	
	if(sValue.length<10)
	{
		// Invalid phone number
		objCtrl.value="";
		alert("Invalid phone number. Please enter valid phone number. Example: (734) 462-5800 or (734) 462-580 Ext:9999");
		objCtrl.focus();
		return false;
	}
	
	// Reformat (###) ###-####  Ext:#####
	var sExt=sValue.substr(10);
	if(sExt!="")
		sExt=" Ext:" + sExt;
	sValue="("+sValue.substr(0,3)+")"+" "+sValue.substr(3,3)+"-"+sValue.substr(6,4)+sExt;
	
	objCtrl.value=sValue;
	return true;
}

function phoneGotFocus(objCtrl)
{
	if(objCtrl.value.length==0)
			return false;
	
	var sValue=new String(objCtrl.value);
	sValue=stripNonDigits(sValue);
	var sExt=sValue.substr(10);
	if(sExt!="")
		sExt=" Ext:" + sExt;
	objCtrl.value=sValue.substr(0,10)+sExt;
	objCtrl.select();

}

function stripNonDigits(str) {
	return str.replace(/[^0-9]/g,"")
}

//MGaba2:MITS 19613
function fnMakeKeyValueBlank() {
    document.getElementById('hdnKey').value = "";
}