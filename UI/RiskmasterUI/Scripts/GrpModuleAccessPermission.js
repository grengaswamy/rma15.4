
var objAJAXManager;
var sNameDelimiter = "@%";
var sInsideIdDelimiter = ",";
var sIdDelimiter = "|";
 var m_nodeText = '';
         var m_nodeValue = '';
         var m_currentCheckedNodeValue = '';

//event handler when + sign is called to expand child node
function expandNode(oParentNode)
{
	var sParentId = oParentNode.data.attributes.id;
	var oCtrl = document.getElementById("hdCurrentFuncId");
	if( oCtrl != null )
		oCtrl.value = sParentId;
	
	objAJAXManager = new AJAXManager()
	objAJAXManager.Init()
	objAJAXManager.CallbackFunction(this.state_Change)

	var sData = getPostbackData(sParentId);
	
	objAJAXManager.ProcessRequest("POST","home?pg=riskmaster/SecurityMgtSystem/GetChildPermissions",true,sData)

	return;
}

//create the postback data when expanding child nodes.
function getPostbackData(parentFuncId)
{
	var oDsnId = document.getElementById("hdDsnId");
	var sData = "dsnid=" + oDsnId.value;
	
	var oGroupId = document.getElementById("hdGroupId");
	sData += "&groupid=" + oGroupId.value;
	
	var oCurrentFuncId = document.getElementById("hdCurrentFuncId");
	sData += "&parentFuncId=" + oCurrentFuncId.value;
	
	return sData;
}

//call back function when expanding child nodes
function state_Change()
{
	// if xmlhttp shows "loaded"
	if (objAJAXManager.ReadyState()==4)
	{
		// if "OK"
		if (objAJAXManager.Status()==200)
		{
			var sRetVal = objAJAXManager.ResponseInDOM().all["RetVal"].value;
			var oRetVal = document.getElementById("hdRetVal");
			oRetVal.value = sRetVal;
			var oCurrentFuncID = document.getElementById("hdCurrentFuncID");
			var sParentID = oCurrentFuncID.value;
			populateChildNodes(sParentID);
		}
		else
		{
			alert("Problem retrieving XML data")
		}
	}
}

//populate all the child nodes for the current node
function populateChildNodes(parentFuncID)
{
	var oParentNode = null;
	oParentNode = tree.getNode(parentFuncID);
		
	var objRetVal = document.getElementById("hdRetVal");	
	var sReturnValue = objRetVal.value;
	
	var objDoc = new ActiveXObject("Microsoft.XMLDOM");
	objDoc.loadXML(sReturnValue);
	var objFunctionList = objDoc.selectNodes("/functions/function");
	var objFunction = null;
	var iNodeIndex = 0;
	var sID = "";
	var id = 1;
	var sName = "";
	var sChildCount = "";
	var bAllowed = false;
	var bHasChild = false;
	var sAllowed = "";
	for(iNodeIndex=0; iNodeIndex<objFunctionList.length; iNodeIndex++)
	{
		objFunction = objFunctionList[iNodeIndex];
		sID = objFunction.attributes.getNamedItem("id").nodeValue;
        sChildCount = objFunction.attributes.getNamedItem("childCount").nodeValue;
        sName = objFunction.attributes.getNamedItem("name").nodeValue;
        sAllowed = objFunction.attributes.getNamedItem("allowed").nodeValue;
        
        bHasChild = false;
		if( parseInt(sChildCount) > 0 )
			bHasChild = true;
			
		bAllowed = false;
		if( parseInt(sAllowed) > 0 )
			bAllowed = true;
		
		id = parseInt(sID);	
		addNode(oParentNode, id, sName, bAllowed, bHasChild, true);	
	}
}

//Add a child node to the tree under current node
function addNode(oParentNode, childID, sName, bAllowed, bHasChild, bCheckBox)
{
	var oNode = null;
	var s;

	if( oParentNode == null )
		s = 'tree';
	else
		s = 'oParentNode';
		
	s = s + '.appendChild({"label": ';
	
	if( bCheckBox )
	{
		s = s + '"<input type=checkbox id=chk_' + childID +
			' onclick=checkboxClick(' + childID + ') ';
		if( bAllowed )
			s = s + 'checked ';
		s = s + '/>';
	}
	else
	{
		s = s + '"';
	}
			
	s = s + sName + '", attributes:{"id": "' + childID + '", "populated":"false"}';
		
	if( bHasChild )
		s = s + ',"children": []';
		
	s = s + '},';
		
	if( oParentNode == null )
		s = s + 'null,';
	
	s = s + 'false)';
	
	oNode = eval(s);

}


//When the checkbox is clicked to grant/revoke a permission
function checkboxClick(id , bIsChecked)
{
	var oGrantedFuncIDs = document.getElementById("hdGrantedFuncIDs");
	var oRevokedFuncIDs = document.getElementById("hdRevokedFuncIDs");
	var sDelimiter = "|";
	if( bIsChecked )
	{
		var sRevokedFuncIDs = oRevokedFuncIDs.value;
		oRevokedFuncIDs.value = sRevokedFuncIDs.replace(sDelimiter+id+sDelimiter, '');
		if( oGrantedFuncIDs.value == null )
			oGrantedFuncIDs.value = "";
		oGrantedFuncIDs.value += sDelimiter+id+sDelimiter;	
	}
	else
	{
		var sGrantedFuncIDs = oGrantedFuncIDs.value;
		oGrantedFuncIDs.value = sGrantedFuncIDs.replace(sDelimiter+id+sDelimiter, '');
		if( oRevokedFuncIDs.value == null )
			oRevokedFuncIDs.value = "";
		oRevokedFuncIDs.value += sDelimiter+id+sDelimiter;
	}
	window.parent.m_DataChanged=true;
}


//Set the current function Id value when a node is clicked
function selectNode( oNode )
{
	var sId = oNode.data.attributes.id;
	var oCtrl = document.getElementById("hdCurrentFuncId");
	if( oCtrl != null )
		oCtrl.value = sId;
}

function onLoad()
{
	//Add the root node for the whole tree.
	//var objRetVal = document.getElementById("hdRetVal");
	//var sFuncIDNames = objRetVal.value;
	var bAllowed = true;
	//if( sFuncIDNames.length < 2 * sNameDelimiter.length)
	//{
	//	bAllowed = false;
	//}
	addNode(null, 0, "Module Access Permissions", bAllowed, true, false);
	var oParentNode = tree.getNode(0);
	oParentNode.data.attributes.populated = 'true';
	
	//Populate the top-level child ndes
	populateChildNodes( 0 );
	CloseProgressWindow();
}

function CloseProgressWindow()
{
	if(self.parent!=null)
	{
		if(haveProperty(self.parent,"wndProgress") && self.parent.wndProgress!=null)
		{
			self.parent.wndProgress.close();
			self.parent.wndProgress=null;
		}
	}
}

function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p == sPropName)
			return true;
	}
	return false;
}
 function OnTreeClick(evt)
            {
                var src = window.event != window.undefined ? window.event.srcElement : evt.target;
                //alert(window.event.srcElement);
                var nodeClick = src.tagName.toLowerCase() == 'a';
                if(nodeClick)
                {
                    var nodeText = src.innerText;
                    m_nodeText = nodeText;
                    var nodeValue = GetNodeValue(src);
                    m_nodeValue = nodeValue;
                    document.getElementById('hdCurrentFuncId').value = nodeValue;
                    return false;
                }
                 var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
                 if(isChkBoxClick)
                 {
                    m_nodeValue = src.nextSibling.href.substring(src.nextSibling.href.lastIndexOf("\\")+1,src.nextSibling.href.lastIndexOf("'"));
                    document.getElementById('hdCurrentFuncId').value = m_nodeValue;
                    checkboxClick(m_nodeValue , src.checked);
                 }
                return true;
            }
        function GetNodeValue(node)
            { 
                var nodeValue = "";
                var nodePath = node.href.substring(node.href.indexOf(",")+2,node.href.length-2);
                var nodeValues = nodePath.split("\\");
                if(nodeValues.length > 1)
                {
                    nodeValue = nodeValues[nodeValues.length - 1];
                    m_nodeDepth=(nodeValues.length - 1)/2;
                }
                else
                {
                    nodeValue = nodeValues[0].substr(1);
                    m_nodeDepth=0;
                } 
                return nodeValue;
            }