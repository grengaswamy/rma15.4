parent.CommonValidations = parent.parent.CommonValidations;
if (parent.CommonValidations == undefined) {
    if (parent.opener != undefined) {
        if (parent.opener.parent != undefined) {
            if (parent.opener.parent.parent != undefined) {
                parent.CommonValidations = parent.opener.parent.parent.CommonValidations;
            }
        }
    }
}

function AddToUsers()
{
	var wnd=window.open('about:blank','progressWnd',
			'width=350,height=250'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-350)/2+',scrollbars');
	wnd.document.writeln(parent.CommonValidations.EmailDocAddToUsersHtml1);
	var c=document.forms[0].totalusers.value;
	for(var i=1;i<=c;i++)
	{
	    wnd.document.writeln(parent.CommonValidations.EmailDocAddToUsersHtml2 + i + parent.CommonValidations.EmailDocAddToUsersHtml3 + eval(parent.CommonValidations.EmailDocAddToUsersHtml4 + i).value + parent.CommonValidations.EmailDocAddToUsersHtml5 + eval(parent.CommonValidations.EmailDocAddToUsersHtml6 + i).value + parent.CommonValidations.EmailDocAddToUsersHtml7);
		
	}
	wnd.document.writeln(parent.CommonValidations.EmailDocAddToUsersHtml8);
	
	return false;
}

function UserChoosen(l)
{
	if(eval("document.forms[0].eid_"+l).value!="")
	{
		if(document.forms[0].emailidto.value=="")
			document.forms[0].emailidto.value=eval("document.forms[0].eid_"+l).value;
		else if(document.forms[0].emailidto.value.substr((document.forms[0].emailidto.value.length - 1) , 1)==',')
			document.forms[0].emailidto.value = document.forms[0].emailidto.value + ' ' + eval("document.forms[0].eid_"+l).value;
		else if(document.forms[0].emailidto.value.substr((document.forms[0].emailidto.value.length - 2) , 2)==', ')
			document.forms[0].emailidto.value = document.forms[0].emailidto.value + eval("document.forms[0].eid_"+l).value;
		else
		{
			document.forms[0].emailidto.value = document.forms[0].emailidto.value + ' , ' + eval("document.forms[0].eid_"+l).value;
		}
	}
	return true;
}

function AddCcUsers()
{
	var wnd=window.open('about:blank','progressWnd',
			'width=350,height=250'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-350)/2+',scrollbars');
	wnd.document.writeln(parent.CommonValidations.EmailDocAddToUsersHtml1);
	var c=document.forms[0].totalusers.value;
	for(var i=1;i<=c;i++)
	{
	    wnd.document.writeln(parent.CommonValidations.EmailDocAddCcUsersHtml1 + i + parent.CommonValidations.EmailDocAddToUsersHtml3 + eval(parent.CommonValidations.EmailDocAddToUsersHtml4 + i).value + parent.CommonValidations.EmailDocAddToUsersHtml5 + eval(parent.CommonValidations.EmailDocAddToUsersHtml6 + i).value + parent.CommonValidations.EmailDocAddToUsersHtml7);
		
	}
	wnd.document.writeln(parent.CommonValidations.EmailDocAddToUsersHtml8);
	//wnd.document.close();
	//m_codeWindow=wnd;
	
	return false;
}

function CcUserChoosen(l)
{
	if(eval("document.forms[0].eid_"+l).value!="")
	{
		if(document.forms[0].emailidcc.value=="")
			document.forms[0].emailidcc.value=eval("document.forms[0].eid_"+l).value;
		else if(document.forms[0].emailidcc.value.substr((document.forms[0].emailidcc.value.length - 1) , 1)==',')
			document.forms[0].emailidcc.value = document.forms[0].emailidcc.value + ' ' + eval("document.forms[0].eid_"+l).value;
		else if(document.forms[0].emailidcc.value.substr((document.forms[0].emailidcc.value.length - 2) , 2)==', ')
			document.forms[0].emailidcc.value = document.forms[0].emailidcc.value + eval("document.forms[0].eid_"+l).value;
		else
		{
			document.forms[0].emailidcc.value = document.forms[0].emailidcc.value + ' , ' + eval("document.forms[0].eid_"+l).value;
		}
	}
	return true;
}

function Validate() //PJS 12-18-2007 MITS 10896 Modified Validate function
{
	if(document.forms[0].emailidto.value=="") 
	{
	    //alert("You must enter a Recipient to send mail.");
	    alert(parent.CommonValidations.ValidRecipient);
		return false;
	}
	else
	{
		 var all = new Array();
         var single = new Array();
         var i=0;
		 var strEmailTo,strEmailCc;
		 validRegExp = /^[^@]+@[^@]+.[a-z]{2,}$/i;
		 strEmailTo = trim(document.forms[0].emailidto.value);
		 strEmailCc = trim(document.forms[0].emailidcc.value);
		 all = strEmailTo.split(",");
		 while(i < all.length)
		{
			 single = all[i].toString();
			 single =  trim(single);
			 if(i < (all.length-1))
			{
				 if (single.search(validRegExp) == -1)
					         {
					             //alert(" A valid To address is required.");
					             alert(parent.CommonValidations.ValidEmailToAddress);
								 document.forms[0].emailidto.focus();
						         return false;
					         }
			}
			else
			{
				if (single!="")
				{
				
				 if (single.search(validRegExp) == -1)
					         {
					             //alert(" A valid To address is required.");
					             alert(parent.CommonValidations.ValidEmailToAddress);
								 document.forms[0].emailidto.focus();
						         return false;
					         }
				}
			}
			 i=i+1;
		}
		 i=0;
		 all = strEmailCc.split(",");
		 while(i < all.length)
		{
			 single = all[i].toString();
			 single =  trim(single);
			 if(i < (all.length-1))
			{
				 if (single.search(validRegExp) == -1)
					         {
					             //alert(" A valid Cc address is required.");
					             alert(parent.CommonValidations.ValidCcAddress);
								 document.forms[0].emailidcc.focus();
						         return false;
					         }
			}
			else
			{
				if (single!="")
				{
				 if (single.search(validRegExp) == -1)
					         {
					             //alert(" A valid Cc address is required.");
					             alert(parent.CommonValidations.ValidCcAddress);
								 document.forms[0].emailidcc.focus();
						         return false;
					         }
				}
			}
			 i=i+1;
		}
		 document.forms[0].regarding.disabled = "false";
		 return true;
	}
}
//PJS 12-18-2007 MITS 10896 Added Trim function
function trim(svalue)
{
  svalue = svalue.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
  return svalue;
}

//Abanals23  4/22/2009 MITS 15688
function SuccessClose()
{
    //alert('Email sent successfully');
    alert(parent.CommonValidations.EmailSentSuccessfully);
    window.close();
}
//smishra25:Start: Adding a function which streams a file and then saves it on disc
function StreamAndSave(template, strForm) {
    // RMA-8407 : MITS-36022 : Mudabbir 
    var IEbrowser = false || !!document.documentMode; //Detect IE browser by duck-typing method
    if (document.getElementById("hdnSilver") != null)
        useSilverlight = document.getElementById("hdnSilver").value;
    if (useSilverlight == true || useSilverlight == "true" || !IEbrowser) {
        if (Silverlight.isInstalled("5.0")) {

            document.getElementById("Silverlight").Value = "ClientBin/RiskmasterSL.xap";
            RiskmasterSL = document.getElementById("Silverlight");
            strForm = RiskmasterSL.Content.SL2JS.GetDocumentPath(strFile, "", "");
            RiskmasterSL.Content.SL2JS.StreamAndSave(template, strForm);
        }
        else
            alert("Please install Silverlight Developer Runtime 5.0 in your machine. Also check whether Registry scripts are run in your machine");
    }

        //Mudabbir ends

    else {
        var step = '';
        var adTypeBinary = 1;
        var adSaveCreateOverWrite = 2;
        if (template.length > 0) {
            //use xml object to convert base64 into array of bytes
            //alert('StreamAndSave');
            step = 'instantiate xml document';
            var xmlDoc = new ActiveXObject("MSXML2.DOMDocument");
            step = 'create b64 element';
            var e = xmlDoc.createElement("b64");
            step = 'set element.dataType to bin.base64';
            e.dataType = "bin.base64";
            step = 'set element.text to template base64 text';
            e.text = template;
            step = 'get bytes from template element';
            var templateBytes = e.nodeTypedValue;

        //use ADODB.Stream to save template bytes to disc
        step = 'instantiate Stream object for template';
        var stm = new ActiveXObject("ADODB.Stream");
        step = 'set template stm.Type';
        stm.Type = adTypeBinary;
        step = 'open template stream';
        stm.Open();
        step = 'write bytes to template stream';
        stm.Write(templateBytes);
        step = 'save template stream to file ' + strForm;
        stm.SaveToFile(strForm, adSaveCreateOverWrite);
        step = 'close stream';
        stm.Close();
        stm = null;

            SendAttach(strForm);
        }
    }
}

//smishra25: End:Adding a function which streams a file and then saves it on disc


//MITS 27964 hlv 6/27/2012 begin
if (window.opener != null) {
    window.opener.window.parent.parent.iwintype = document.title;
}

window.onunload = function () {
    if (window.opener != null) {
        window.opener.window.parent.parent.iwintype = "";
    }
}
//MITS 27964 hlv 6/27/2012 end
// RMA-8407 : MITS-36022 : Mudabbir 
function getAndSetValues(obj, set, get, value) {

    if (obj == "1")
        Editor_Launched = 1;
    else {
        if (obj != null) {
            if (get == "yes") {

                var getvalue = eval(obj);
                if (getvalue.value == undefined)
                    return getvalue;
                else
                    return getvalue.value;
            }
            if (set == "yes") {
                var setvalue = eval(obj);
                setvalue.value = value;
            }
        }
    }
}

function DisplayErrorfromSilver(errMsg) {
    alert(errMsg);
    //document.write('<h3>Error Message</h3><p>' + errMsg + '</p><br />' + _mergeFailureErrorMessage); 
    document.write('<h3>' + parent.CommonValidations.ValidMergeMergeErrorMessage + '</h3><p>' + errMsg + '</p><br />');
    return false;
}
