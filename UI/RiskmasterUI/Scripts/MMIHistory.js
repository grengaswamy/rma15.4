var table_window;
var m_codeWindow;
var m_sFieldName;
var child_window;
var m_ctname;
var m_itemAmount=0;
var inv_window;

function checkReason(sText)
{
    document.getElementById('hdnReason').value = sText;
}

function setDataChangedKD()
{
	//checking length and format for negative number
	if (document.forms[0].disability.value.charAt(0)=='-')
	{
		if (document.forms[0].disability.value.indexOf('.') >= 0)
		{
			var pos1 = document.forms[0].disability.value.indexOf('.');
			var str1 = document.forms[0].disability.value.substring(0,pos1);
			var str2 = document.forms[0].disability.value.substring(pos1+1,document.forms[0].disability.value.length);
			if (str1.length > 6)
				{document.forms[0].disability.value = document.forms[0].disability.value.substring(0,6);}
			else if (str2.length > 2)
				{document.forms[0].disability.value = document.forms[0].disability.value.substring(0,document.forms[0].disability.value.length-1);}
		}
		else
		{
			if(document.forms[0].disability.value.length < 7)
				document.forms[0].disability.value = document.forms[0].disability.value;
			else
				document.forms[0].disability.value = document.forms[0].disability.value.substring(0,6);
		}
	}
	//checking length and format for positive number
	else
	{
		if (document.forms[0].disability.value.indexOf('.') >= 0)
		{
			var pos1 = document.forms[0].disability.value.indexOf('.');
			var str1 = document.forms[0].disability.value.substring(0,pos1);
			var str2 = document.forms[0].disability.value.substring(pos1+1,document.forms[0].disability.value.length);
			if (str1.length > 2)
				{document.forms[0].disability.value = document.forms[0].disability.value.substring(0,6);}
			else if (str2.length > 2)
				{document.forms[0].disability.value = document.forms[0].disability.value.substring(0,document.forms[0].disability.value.length-1);}
		}
		else
		{
			if(document.forms[0].disability.value.length < 3)
				document.forms[0].disability.value = document.forms[0].disability.value;
			else
				document.forms[0].disability.value = document.forms[0].disability.value.substring(0,2);
		}
	}
	
	//checking the validation for a number						
	if (document.forms[0].disability.value.charAt(0)=='-')
	{
		document.forms[0].disability.value = document.forms[0].disability.value;
		if (document.forms[0].disability.value.charAt(1)=='.')
		{
			docval = document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
			docval = "0" + docval;
			if (isNaN(docval))
			{
				document.forms[0].disability.value = document.forms[0].disability.value.substring(0,document.forms[0].disability.value.length-1);
			}
		}
		else
		{
			docval = document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
			if (isNaN(docval))
			{
				document.forms[0].disability.value = "";
			}
		}
	}
	else if (document.forms[0].disability.value.charAt(0) != ".")
	{
		if (isNaN(document.forms[0].disability.value)==true)
		{
			document.forms[0].disability.value = "";
		}
	}
	else
	{
		docval = "0" + document.forms[0].disability.value;
		if (isNaN(docval))
		{
			document.forms[0].disability.value = "";
		}
	}				
}

function setDataChanged()
{
	if (document.forms[0].disability.value.charAt(0)=='-')
	{
		//var RExp = new RegExp();
		//RExp = /^(\d{7}|-\d{5}.\d{2})$/;
		
		if (isNaN(document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length))==true)
		{
			document.forms[0].disability.value="";
		}
		else
		{
			var docval	= document.forms[0].disability.value;
			
			var decplace = document.forms[0].disability.value.indexOf('.');

			if (decplace==-1)
			{												
				var len = document.forms[0].disability.value.length;
				if (len==1){
					document.forms[0].disability.value = "";										
				}
				else if (len==2){
					document.forms[0].disability.value = document.forms[0].disability.value + ".00";
					docval = "-" + "0000" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
				}
				else if (len==3){
					document.forms[0].disability.value = document.forms[0].disability.value + ".00";
					docval = "-" + "000" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
				}
				else if (len==4){
					document.forms[0].disability.value = document.forms[0].disability.value + ".00";
					docval = "-" + "00" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
				}
				else if (len==5){
					document.forms[0].disability.value = document.forms[0].disability.value + ".00";
					docval = "-" + "0" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
				}
				else{
					document.forms[0].disability.value = document.forms[0].disability.value + ".00";
					docval = document.forms[0].disability.value;
				}
			}
	
			if (decplace==1)
			{
				var len1 = document.forms[0].disability.value.length;
				var str1 = document.forms[0].disability.value.substring(decplace+1,len1+1);
				var len2 = str1.length;
				if (len2==0)
				{
					document.forms[0].disability.value = "-" + "0" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length) + "00";
				}
				else if (len2==1)
				{
					document.forms[0].disability.value = "-" + "0" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length) + "0";
				}
				else
				{
					document.forms[0].disability.value = "-" + "0" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
					document.forms[0].disability.value = "-" + Math.round(document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length) * 100)/100;
				}
				docval = "-" + "0000" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
			}
	
			if (decplace>1)
			{
				var len3 = document.forms[0].disability.value.length;
				
				var str2 = document.forms[0].disability.value.substring(decplace+1,len3);
				var len4 = str2.length;
				if (len4==0)
				{
					document.forms[0].disability.value = document.forms[0].disability.value + "00";
				}
				if (len4==1)
				{
					document.forms[0].disability.value = document.forms[0].disability.value + "0";
				}
				if (len4==2)
				{
					document.forms[0].disability.value = document.forms[0].disability.value;
				}
					
				document.forms[0].disability.value = "-" + Math.round(document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length) * 100)/100;
				
				if (decplace==2){
					docval = "-" + "0000" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
				}								
				if (decplace==3){
					docval = "-" + "000" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
				}
				if (decplace==4){
					docval = "-" + "00" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
				}
				if (decplace==5){
					docval = "-" + "0" + document.forms[0].disability.value.substring(1,document.forms[0].disability.value.length);
				}
				if (decplace==6){
					docval = document.forms[0].disability.value;
				}
				
				if (decplace>6){
					document.forms[0].disability.value = "";
					//docval = "-00000.00";
				}
			}
		
			//if (RExp.exec(docval))
			//{
			//	return;
			//}
			//else
			//{
			//	document.forms[0].disability.value = "";
			//}
		}
	}
	else
	{
		var RExp = new RegExp();
		RExp = /^(\d{4}|\d{2}.\d{2})$/;
		
		if (isNaN(document.forms[0].disability.value)==true)
		{
			document.forms[0].disability.value="";
		}
		else
		{
			var docval	= document.forms[0].disability.value;
			
			var decplace = document.forms[0].disability.value.indexOf('.');

			if (decplace==-1)
			{												
				var len = document.forms[0].disability.value.length;
				if (len==1){
					document.forms[0].disability.value = document.forms[0].disability.value + ".00";
					docval = "0" + document.forms[0].disability.value;
				}
				else{
					document.forms[0].disability.value = document.forms[0].disability.value + ".00";
					docval = document.forms[0].disability.value
				}
			}
	
			if (decplace==0)
			{
				var len1 = document.forms[0].disability.value.length;
				var str1 = document.forms[0].disability.value.substring(decplace+1,len1+1);
				var len2 = str1.length;
				if (len2==0)
				{
					document.forms[0].disability.value = "0" + document.forms[0].disability.value + "00";
					docval = "0" + document.forms[0].disability.value;
				}
				if (len2==1)
				{
					document.forms[0].disability.value = "0" + document.forms[0].disability.value + "0";
					docval = "0" + document.forms[0].disability.value;
				}
				if (len2>1)
				{
					document.forms[0].disability.value = "0" + document.forms[0].disability.value;
					docval = "0" + document.forms[0].disability.value;
				}
				document.forms[0].disability.value = Math.round(document.forms[0].disability.value * 100)/100;
			}
	
			if (decplace>0)
			{
				var len3 = document.forms[0].disability.value.length;
				if (decplace==1)
				{
					var str2 = document.forms[0].disability.value.substring(decplace+1,len3);
					var len4 = str2.length;
					if (len4==0)
					{
						document.forms[0].disability.value = document.forms[0].disability.value + "00";
						docval = "0" + document.forms[0].disability.value;
					}
					if (len4==1)
					{
						document.forms[0].disability.value = document.forms[0].disability.value + "0";
						docval = "0" + document.forms[0].disability.value;
					}
					if (len4==2)
					{
						document.forms[0].disability.value = document.forms[0].disability.value;
						docval = "0" + document.forms[0].disability.value;
					}
					document.forms[0].disability.value = Math.round(document.forms[0].disability.value * 100)/100;
				}
			
				if (decplace==2)
				{
					var str3 = document.forms[0].disability.value.substring(decplace+1,len3);
					var len5 = str3.length;
					if (len5==0)
					{
						document.forms[0].disability.value = document.forms[0].disability.value + "00";
					}
					if (len5==1)
					{
						document.forms[0].disability.value = document.forms[0].disability.value + "0";
					}
					if (len5==2)
					{
						document.forms[0].disability.value = document.forms[0].disability.value;
					}
					document.forms[0].disability.value = Math.round(document.forms[0].disability.value * 100)/100;
					docval = document.forms[0].disability.value;
				}
				
				if (decplace>2)
				{
					document.forms[0].disability.value = "";
					docval = "00.00";
				}
			}
		
			//if (RExp.exec(docval))
			//{
			//	return;
			//}
			//else
			//{
			//	document.forms[0].disability.value = "";
			//}
		}
	}
}

function checkMMIHistory(usrname,pirowid, maxdate, curdate)
{
	var datearray = document.forms[0].mmidate.value.split("/");
	var datecur = datearray[2]+datearray[0]+datearray[1];
			
	if (document.forms[0].mmidate.value=="")
	{
		alert("MMI Date is needed for adding an MMI history entry.");
		return false;
	}
	else
	{
		//the parent window has different control name depends on whether Case Management activated.
		var oClaimMMIDate = null;
		 //Parijat:Mits 9582
		oClaimMMIDate = eval("window.opener.document.forms[0].mmidate");
		//oClaimMMIDate = eval("document.forms[0].mmidate");//asharma326 MITS 35076
		//sgoel6 MITS 14887 04/29/2009
		if (oClaimMMIDate == null || oClaimMMIDate==undefined)
		    oClaimMMIDate = eval("window.opener.document.forms[0].mmidate1");//asharma326 MITS 35076
		if (oClaimMMIDate == null || oClaimMMIDate == undefined)
		    oClaimMMIDate = eval("window.opener.document.forms[0].mmidate2");	//asharma326 MITS 35076
        //Parijat:Mits 9582
        if(oClaimMMIDate!= null)
        {
		    if(maxdate!='')
		    {			
			    if (maxdate > datecur)
			    { 
				    oClaimMMIDate.value = maxdate.substring(4,6)+'/'+maxdate.substring(6,8)+'/'+maxdate.substring(0,4);
			    }
			    else 
			    {
				    oClaimMMIDate.value = datecur.substring(4,6)+'/'+datecur.substring(6,8)+'/'+datecur.substring(0,4);
			    }
		    }
		    else
			    oClaimMMIDate.value = datecur.substring(4,6)+'/'+datecur.substring(6,8)+'/'+datecur.substring(0,4);
		}
		return true;
	}
}

function checkLink()
{
	window.open("home?pg=riskmaster/Search/MainPage&amp;formname=physician","Physician",
				"width=600,height=500,resizable=yes,scrollbars=yes,left=250,top=150,screenX=250,screenY=150");
		return false;
}
function TrimIt(aString) 
{
	// RETURNS INPUT ARGUMENT WITHOUT ANY LEADING OR TRAILING SPACES
	return (aString.replace(/^ +/, '')).replace(/ +$/, '');
}
function PageLoadedMMI()
{
	if(document.forms[0].hdntrack.value=='False')
		window.close();
}
//sgoel6 MITS 04/28/2009
function fillLatestMMIDate(latestDate)
{
	//the parent window has different control name depends on whether Case Management activated.
	var oClaimMMIDate = null;
	oClaimMMIDate = eval("window.opener.document.forms[0].mmidate");//asharma326 MITS 35076
	if (oClaimMMIDate == null || oClaimMMIDate == undefined)
	    oClaimMMIDate = eval("window.opener.document.forms[0].mmidate1");//asharma326 MITS 35076
	if (oClaimMMIDate == null || oClaimMMIDate == undefined)
	    oClaimMMIDate = eval("window.opener.document.forms[0].mmidate2");//asharma326 MITS 35076
    
    if(oClaimMMIDate!= null)
        oClaimMMIDate.value = latestDate;
	
	return true;
}
