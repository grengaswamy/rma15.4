// Author: Denis Basaric, 11/01/1999
// Last Modified by: Denis Basaric, 01/16/2002
var m_codeWindow=null;
var m_sFieldName="";
var m_Wnd=null;
var m_datachanged='false';

var NS4 = (document.layers);
var IE4 = (document.all);

var m_FormSubmitted=false;
function OnSubmitForm()
{
	if(m_FormSubmitted)
	{
		alert("You already submitted this form. Please wait for server to respond.");
		return false;
	}
	
	m_FormSubmitted=true;
	return true;
}

function OnPageLoad()
{
    //MITS 34959 srajindersin 1/16/2014
    if (document.getElementById('hdnInProcess') != null) {
        document.forms[0].hdnInProcess.value = "0";
        var obj = eval("document.forms[0].setasdefault");
        if (obj != null) {
            obj.checked = (document.forms[0].defaultviewid.value == document.forms[0].viewid.value && document.forms[0].defaultviewid.value != 0 && document.forms[0].defaultviewid.value != "")
        }

        var i;
        //Set focus to first field 
        for (i = 0; i < document.forms[0].length; i++) {
            if (document.forms[0][i].type == "text") {
                document.forms[0][i].focus();
                break;
            }
        }
        document.dateSelected = dateSelected;
        document.codeSelected = codeSelected;
        document.onCodeClose = onCodeClose;
        document.entitySelected = entitySelected;
        self.onfocus = onWindowFocus;
        return true;
    }
}
// added by mdhamija : MITS 23468
function OnFormClear() {
    //Set focus to first field 
    for (var i = 0; i < document.forms[0].length; i++) {
        if (document.forms[0][i].type == "text") {
            try
            {
            document.forms[0][i].focus();
            break;
        }
            catch (e) { }
    }
    }

    //rkulavil : RMA-9392 start
    var sDropDownList = document.getElementsByTagName("select");
    if (sDropDownList != null)
    {
        for (i = 0; i < sDropDownList.length ; i++) {
            BetweenOption('date', sDropDownList[i].id.replace("op",""));
        }
    }
    //rkulavil : RMA-9392 end
    return false;
}
// Hides code popup window
function onWindowFocus()
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=null;
	
	return true;
}

function onCodeClose()
{
	m_codeWindow=null;
	
	return true;
}
function getOrgLevel(enttable)
{
	var orglevel=8;
	switch (enttable)
	{
	case 1005:
		orglevel=1;
		break;
	case 1006:
		orglevel=2;
		break;
	case 1007:
		orglevel=3;
		break;
	case 1008:
		orglevel=4;
		break;
	case 1009:
		orglevel=5;
		break;
	case 1010:
		orglevel=6;
		break;
	case 1011:
		orglevel=7;
		break;
	case 1012:
		orglevel=8;
		break;
    //PJS(05-12-09), MITS 15761
    //Search should allow selection of all levels by default in department fields. 
    case 0:
        orglevel = "all";
        break;

	}
	return orglevel;
}

function select_option(id1,ctname)
{
	m_ctname=ctname;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	m_codeWindow=window.open("home?pg=riskmaster/OrgHierarchy/Org&amp;tablename=nothing&amp;rowid=0&amp;lob=0&amp;searchOrgId=&amp;section="+id1,'Codes','width=500,height=350'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	
	return false;
}

function selectOrg(sCodeTable,sFieldName,ent_table,tableid)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	if(sCodeTable=='orgh')
	{
		if(parseInt(ent_table)>=1005 && parseInt(ent_table)<=1012 || parseInt(ent_table)==0)
		{
			var orglevel;
			var objFormElem=eval('document.forms[0].'+sFieldName+'_cid');
			var sFind;
			if (objFormElem != null)
			   sFind=objFormElem.value;
			else
			   sFind = '';

			orglevel=getOrgLevel(parseInt(ent_table));

			if(sFind=='0')
				sFind='';
//			m_codeWindow=window.open('home?pg=riskmaster/OrgHierarchy/Org&tablename=nothing&rowid=0&lob='+orglevel+'&searchOrgId='+sFind,'Table','width=740,height=620'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
			m_codeWindow=window.open('../OrganisationHierarchy/OrgHierarchyLookup.aspx?amp;tablename=nothing&rowid=0&lob='+orglevel+'&searchOrgId='+sFind,'Table','width=1000,height=620'+',top='+(screen.availHeight-500)/2+',left='+(screen.availWidth-1000)/2+',resizable=yes,scrollbars=yes');			
		}
	    else {
            // akaushik5 Changed for MITS 38299 Starts
	        var viewId = 4;
	        if (ent_table === "EMPLOYEES") {
	            viewId = -1
	        }
	        lookupData(sFieldName, ent_table, viewId, sFieldName, 3);
	        // akaushik5 Changed for MITS 38299 Ends
	    }
	}	
	else
	{
		m_codeWindow=window.open('getcode.asp?code='+sCodeTable,'codeWnd',
			'width=500,height=300'+',top='+(screen.availHeight-300)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
	}
	return false;
}

function codeSelected(sCodeText,lCodeId)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	
	objCtrl=eval('document.forms[0].'+m_sFieldName);
	if(objCtrl!=null)
	{
		if(objCtrl.type=="textarea")
		{
			objCtrl.value=objCtrl.value + sCodeText + "\n";
		}
		else if(objCtrl.type=="select-one" || objCtrl.type=="select-multiple")
		{
			var bAdd=true;
			for(var i=0;i<objCtrl.length;i++)
			{
				if(objCtrl.options[i].value==lCodeId)
					bAdd=false;
			}
			if(bAdd)
			{
				var objOption = new Option(sCodeText, lCodeId, false, false);
				objCtrl.options[objCtrl.length] = objOption;
				objCtrl=null;
				objCtrl=eval("document.forms[0]." + m_sFieldName+"_lst");
				if(objCtrl!=null)
				{
					if(objCtrl.value!="" && objCtrl.value.substring(objCtrl.value.length-1,1)!=",")
						objCtrl.value=objCtrl.value+",";
					objCtrl.value=objCtrl.value+lCodeId;
				}
			}
		}
		else
		{
			objCtrl.value=sCodeText;
			objCtrl.codeText=sCodeText;
			objCtrl=eval('document.forms[0].'+m_sFieldName+"_cid");
			objCtrl.value=lCodeId;			
		}

	}
	
	m_sFieldName="";
	m_codeWindow=null;
	return true;
}

function lookupData(sFieldName, sTableId, sViewId, sFieldMark, lookupType)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	
	m_sFieldName=sFieldMark;
	m_LookupType=lookupType;
	
	self.lookupCallback="entitySelected";

	// m_codeWindow=window.open('home?pg=riskmaster/Search/MainPage&viewid='+sViewId+'&TableId='+sTableId,'searchWnd',
    //zmohammad MITs 33828 : Changing window name slightly so it does not clash with Parent window name. Can be made generic for several layers but not needed as of now.
	m_codeWindow = window.open('/RiskmasterUI/UI/Search/searchmain.aspx?viewid=' + sViewId + '&formname=' + sTableId + '&type=' + lookupType + '&screenflag=2', 'searchWind',
	'width=550,height=450'+',top='+(screen.availHeight-450)/2+',left='+(screen.availWidth-550)/2+',resizable=yes,scrollbars=yes');
	
		
	return false;
}

function entitySelected(sEntityId)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
	if(sEntityId!="")
	{
		if(sEntityId!="-1")	
		{		
			//self.setTimeout("m_Wnd=window.open('getentitydata.asp?entityid="+sEntityId+
			//	"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
			//	",left="+((screen.availWidth-400)/2)+"');",100);

			// self.setTimeout("m_Wnd=RMX.window.open('home?pg=riskmaster/Search/SearchRender&entityid="+sEntityId+"&type="+sType+"&lookuptype="+m_LookupType+
		    self.setTimeout("m_Wnd=window.open('/RiskmasterUI/UI/Search/SearchRender.aspx?entityid=" + sEntityId + "&lookuptype=2" +
					"','progressWnd','width=400,height=150,top="+((screen.availHeight-150)/2)+
					",left="+((screen.availWidth-400)/2)+"');",100);
			document.entityLoaded=entitySelected;
			return true;
		}	
		else
		{
			var objCtrl;
			objCtrl=eval("document.forms[0]." + m_sFieldName);
			
			if(objCtrl.type=="select-one" || objCtrl.type=="select-multiple")
			{
				var sEntityName, sEntityId;
				var sEntityName, sEntityId;
				obj=eval("m_Wnd.document.forms[0].lastfirstname");
				if(obj!=null)
					sEntityName=obj.value;
				obj=null;				
				obj=eval("m_Wnd.document.forms[0].entityid");
				if(obj!=null)
					sEntityId=obj.value;
						
				obj2=eval("document.forms[0]." + m_sFieldName);
				if(obj2!=null)
				{
					var bAdd=true;
					
					for(var i=0;i<obj2.length;i++)
					{
						if(obj2.options[i].value==sEntityId)
						bAdd=false;
					}
					
					if(bAdd)
					{
						var objOption = new Option(sEntityName, sEntityId, false, false);
						obj2.options[obj2.length] = objOption;
						obj2=null;
						obj2=eval("document.forms[0]." + m_sFieldName+"_lst");
						
						if(obj2!=null)
						{
							if(obj2.value!="" && obj2.value.substring(obj2.value.length-1,1)!=" ")
								obj2.value=obj2.value+" ";
							obj2.value=obj2.value+sEntityId;
						}
					}
				}
			}
			else
			{
				//alert(eval("m_Wnd.document.forms[0].entityid").value);
				//alert(objCtrl.name);
				objCtrl.value=eval("m_Wnd.document.forms[0].lastfirstname").value;
				objCtrl=eval("document.forms[0]." + m_sFieldName + "_cid");
				objCtrl.value=eval("m_Wnd.document.forms[0].entityid").value;
			}		
			if(m_Wnd!=null)
			m_Wnd.close();
			m_Wnd=null;
			m_sFieldName="";
			m_codeWindow=null;
			return true;
		}	
	}
}

function onEnter(sCodeTable,xname)
{
	e = window.event;
	if (e.keyCode==13)
	{	
		m_datachanged='true';
		onLostFocus(sCodeTable,xname);
		return false;
	}
}

//sgoel6 04/07/2009 MITS 14192
function onOrgLostFocus(sCodeTable,xname,orgLevel)
{
    var obj;
	if(m_datachanged=='true')
	{
		obj='document.forms[0].'+xname;
		obj=eval(obj);
        if(eval('document.forms[0].title')!=null) 
            sTitle = document.forms[0].title.value;
        else
            sTitle = '';
        var sFind=obj.value;
		if(m_codeWindow!=null)
			m_codeWindow.close();
		if(sFind!='')
		{
			m_sFieldName=xname;			
			if(parseInt(orgLevel)>=1005 && parseInt(orgLevel)<=1012 || parseInt(orgLevel)==0)
			{
			    orgLevel = getOrgLevel(parseInt(orgLevel));
			    //abansal23 MITS 16751 on 7/1/2009 starts
			    if(orgLevel == 'all')
			    {
			        orgLevel = 0;
			    }
			    //abansal23 MITS 16751 on 7/1/2009 ends
			    orgLevel=getOrgLevelEx(parseInt(orgLevel));
			}
			else if(parseInt(orgLevel)>=1 && parseInt(orgLevel)<=8 || parseInt(orgLevel)==0)
			{
			    orgLevel=getOrgLevelEx(parseInt(orgLevel));
			}			
		    // apeykov JIRA RMA-4507
			//m_codeWindow = window.open("/RiskmasterUI/UI/Codes/QuickLookup.aspx?codetype=code." + sCodeTable + "&amp;lookupstring=" + sFind + "&Title=" + sTitle + "&Orglevel=" + orgLevel, 'codeWnd', 'width=500,height=290' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
			m_codeWindow = window.open("/RiskmasterUI/UI/Codes/QuickLookup.aspx?codetype=code." + sCodeTable + "&lookupstring=" + sFind + "&Title=" + sTitle + "&Orglevel=" + orgLevel, 'codeWnd', 'width=500,height=290' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
		}
		else
		{
			obj=eval('document.forms[0].'+xname+'_cid');
			if (obj!=null)
				obj.value=0;
		}
		m_datachanged='false'
	}		
}
//sgoel6 MITS 14467 04/14/2009 For Search screens Title passed will be "Search"
//function onLostFocus(sCodeTable,xname)
function onLostFocus(sCodeTable,xname,pTitle)
{
	var obj;
	if(m_datachanged=='true')
	{
	    obj = 'document.forms[0].' + xname;
		obj=eval(obj);
		
		//sgoel6 MITS 14467 04/14/2009 For Search screens Title passed will be "Search"
        if (pTitle==undefined)
        {
            //pmittal5  MITS:12318  06/13/08	
            if(eval('document.forms[0].title')!=null) 
                sTitle = document.forms[0].title.value;
            else
                sTitle = '';
            //End - pmittal5            
        }
        else//"Search"
        {
            sTitle=pTitle;
        }
		
		
        var sFind=obj.value;
		if(m_codeWindow!=null)
			m_codeWindow.close();
		if(sFind!='')
		{
			m_sFieldName=xname;
			//pmittal5  MITS:12318  06/13/08
			//m_codeWindow=window.open("home?pg=riskmaster/CodesList/quicklookup&type=code." + sCodeTable + "&find=" + sFind,'codeWnd','width=500,height=290'+',top='+(screen.availHeight-290)/2+',left='+(screen.availWidth-500)/2+',resizable=yes,scrollbars=yes');
		    //RMA-6369  achouhan3   Fix for Browser compatabibilty issue Starts,also apeykov made same fix for RMA-4507
		    //m_codeWindow = window.open("/RiskmasterUI/UI/Codes/QuickLookup.aspx?codetype=code." + sCodeTable + "&amp;lookupstring=" + sFind + "&Title=" + sTitle, 'codeWnd', 'width=500,height=290' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
			m_codeWindow = window.open("/RiskmasterUI/UI/Codes/QuickLookup.aspx?codetype=code." + sCodeTable + "&lookupstring=" + sFind + "&Title=" + sTitle, 'codeWnd', 'width=500,height=290' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
		    //RMA-6369  achouhan3       Fix for Browser compatabibilty issue Ends
		}
		else
		{
			obj=eval('document.forms[0].'+xname+'_cid');
			if (obj!=null)
				obj.value=0;
		}
		m_datachanged='false'
	}		
}

function datachanged(ischanged)
{
	m_datachanged=ischanged;
}

//TBD


function deleteSelCode(sFieldName)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.forms[0].'+sFieldName);
	if(objCtrl==null)
		return false;
	if(objCtrl.selectedIndex<0)
		return false;
	
	var bRepeat=true;
	while(bRepeat)
	{
		bRepeat=false;
		for(var i=0;i<objCtrl.length;i++)
		{
			// remove selected elements
			if(objCtrl.options[i].selected)
			{
				objCtrl.options[i]=null;
				bRepeat=true;
				break;
			}
		}
	}
	// Now create ids list
	var sId="";
	for(var i=0;i<objCtrl.length;i++)
	{
			if(sId!="")
				sId=sId+" ";
			sId=sId+objCtrl.options[i].value;
	}
	objCtrl=null;
	objCtrl=eval("document.forms[0]." + sFieldName+"_lst");
	if(objCtrl!=null)
		objCtrl.value=sId;

	return false;
}

function codeLostFocus(sCtrlName)
{
	var objFormElem=eval('document.forms[0].'+sCtrlName);
	if(objFormElem.value=="")
	{
		objFormElem.codeText="";
		objFormElem.codeId=0;
	}
	else
	{
		if(haveProperty(objFormElem,"codeText"))
			objFormElem.value=objFormElem.codeText;
		else
			objFormElem.value="";
	}
	
	return true;
}

function haveProperty(obj, sPropName)
{
	for(p in obj)
	{
		if(p==sPropName)
			return true;
	}
	return false;
}

/*function dateLostFocus(sCtrlName)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var objFormElem=eval('document.forms[0].'+sCtrlName);
	var sDate=new String(objFormElem.value);
	var iMonth=0, iDay=0, iYear=0;
	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	var sArr=sDate.split(sDateSeparator);
	if(sArr.length==3)
	{
		sArr[0]=new String(parseInt(sArr[0],10));
		sArr[1]=new String(parseInt(sArr[1],10));
		sArr[2]=new String(parseInt(sArr[2],10));
		// Classic leap year calculation
		if (((parseInt(sArr[2],10) % 4 == 0) && (parseInt(sArr[2],10) % 100 != 0)) || (parseInt(sArr[2],10) % 400 == 0))
			monthDays[1] = 29;
		if(iDayPos<iMonthPos)
		{
			// Date should be as dd/mm/yyyy
			if(parseInt(sArr[1],10)<1 || parseInt(sArr[1],10)>12 ||  parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>monthDays[parseInt(sArr[1],10)-1])
				objFormElem.value="";
		}
		else
		{
			// Date is something like mm/dd/yyyy
			if(parseInt(sArr[0],10)<1 || parseInt(sArr[0],10)>12 ||  parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>monthDays[parseInt(sArr[0],10)-1])
				objFormElem.value="";
		}
		// Check the year
		if(parseInt(sArr[2],10)<10 || (sArr[2].length!=4 && sArr[2].length!=2))
			objFormElem.value="";
		// If date has been accepted
		if(objFormElem.value!="")
		{
			// Format the date
			if(sArr[0].length==1)
				sArr[0]="0" + sArr[0];
			if(sArr[1].length==1)
				sArr[1]="0" + sArr[1];
			if(sArr[2].length==2)
				sArr[2]="19"+sArr[2];
			if(iDayPos<iMonthPos)
				objFormElem.value=formatDate(sArr[2] + sArr[1] + sArr[0]);
			else
				objFormElem.value=formatDate(sArr[2] + sArr[0] + sArr[1]);
		}
	}
	else
		objFormElem.value="";
	return true;
}
*/
function timeLostFocus(sCtrlName)
{
	var objFormElem=eval('document.forms[0].'+sCtrlName);
	var sTime=new String(objFormElem.value);
	if(sTime=="")
		return true;
	var sArr=sTime.split(":");
	if(sArr.length!=2)
	{
		objFormElem.value="";
		return true;
	}
	sArr[0]=new String(parseInt(sArr[0],10));
	sArr[1]=new String(parseInt(sArr[1],10));
	if(parseInt(sArr[0],10)<0 || parseInt(sArr[0],10)>23 || parseInt(sArr[1],10)<0 || parseInt(sArr[1],10)>59)
	{
		objFormElem.value="";
		return true;
	}
	if(sArr[0].length==1)
		sArr[0]="0" + sArr[0];
	if(sArr[1].length==1)
		sArr[1]="0" + sArr[1];
	objFormElem.value=formatTime(sArr[0]+sArr[1]);		
	
	return true;
}

function formatDate(sParamDate)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}

function formatTime(sParamTime)
{
	if(sParamTime=="")
		return "";
	var sTime=new String(sParamTime);
	return sTime.substr(0,2) + ":" + sTime.substr(2,2);
}

function handleUnload()
{
	if(window.opener!=null)
		window.opener.document.onCodeClose();
	
	return true;
}

function btnSetAsDefaultClick() {
   
		var sTableId='';
		var sType='';
		var formname = '';
		var sysformname = '';
        var screenflag = '';
        var tablename='';//MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
        var rowid='';
        var eventdate='';
        var claimdate='';
        var policydate='';
        var filter = '';
        var hideglobalsearch = '';
		if(document.forms[0].catid)
			sType="&type="+document.forms[0].catid.value
		if(document.forms[0].hdFormName)
		    formname = "&formname=" + document.forms[0].hdFormName.value
		if (document.forms[0].hdSysFormName)                                     //Added by csingh7 : MITS 12545
		    sysformname = "&sysformname=" + document.forms[0].hdSysFormName.value
        
        if(document.forms[0].hdScreenFlag)
            screenflag = "&screenflag="+document.forms[0].hdScreenFlag.value;
        //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
        if(document.forms[0].hdtablename)
            tablename  = "&tablename="+document.forms[0].hdtablename.value;            
        if(document.forms[0].hdrowid)
            rowid = "&rowid="+document.forms[0].hdrowid.value;
        if(document.forms[0].hdeventdate)
            eventdate = "&eventdate="+document.forms[0].hdeventdate.value;
        if(document.forms[0].hdclaimdate)
            claimdate = "&claimdate="+document.forms[0].hdclaimdate.value;       
        if(document.forms[0].hdpolicydate)
            policydate = "&policydate="+document.forms[0].hdpolicydate.value;                   
        if(document.forms[0].hdfilter)
            filter = "&filter="+document.forms[0].hdfilter.value;                                             
        //avipinsrivas Start : Worked for 13999 (Issue of 13196 - Epic 7767)
        if (document.forms[0].hdhideglobalsearch)
            hideglobalsearch = "&hideglobalsearch=" + document.forms[0].hdhideglobalsearch.value;
        //avipinsrivas end
        var sUrl = "searchmain.aspx" + "?viewid=" + document.forms[0].viewid.value + sType + formname + sysformname + screenflag + tablename+ rowid + eventdate + claimdate + policydate + filter + hideglobalsearch ;
        sUrl = sUrl + '&SetAsDefault=' + true;
		  
	
		window.location.href=sUrl;
}

function CheckInProcess()
{
	if (document.forms[0].hdnInProcess.value=='1')
	{
	  window.alert("The application is busy looking up data for a previous field.\nPlease wait for the results before modifying additional fields.");
	  return false;
	}
}
function OnViewChange(sPage)
{
	document.forms[0].hdnInProcess.value="1";
	if(document.forms[0].cboViews.selectedIndex > 0)
	{
		var sTableId='';
		var sType='';
		var formname = '';
		var screenflag = '';
        var tablename='';//MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
        var rowid='';
        var eventdate='';
        var claimdate='';		
        var policydate='';	
        var filter = '';
        var hideglobalsearch = '';      //avipinsrivas Start : Worked for 13999 (Issue of 13196 - Epic 7767)
		if (sPage=='searchmain.aspx')  //tkr 1/2003 hack to allow choice of custom searches in popup
			sTableId='&tableid='+document.forms[0].tablerestrict.value;
		if(document.forms[0].catid)
			sType="&type="+document.forms[0].catid.value
		if(document.forms[0].hdFormName)
		    formname = "&formname=" + document.forms[0].hdFormName.value
		if (document.forms[0].hdSysFormName)                                     //Added by csingh7 : MITS 12545
		    sysformname = "&sysformname=" + document.forms[0].hdSysFormName.value
		
		if(document.forms[0].hdScreenFlag)
		    screenflag = "&screenflag="+document.forms[0].hdScreenFlag.value;
//		var sUrl=sPage+"&viewid="+document.forms[0].cboViews.options[document.forms[0].cboViews.selectedIndex].value+sTableId+sType+formname;
        //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
        if(document.forms[0].hdtablename)
            tablename  = "&tablename="+document.forms[0].hdtablename.value;            
        if(document.forms[0].hdrowid)
            rowid = "&rowid="+document.forms[0].hdrowid.value;
        if(document.forms[0].hdeventdate)
            eventdate = "&eventdate="+document.forms[0].hdeventdate.value;
        if(document.forms[0].hdclaimdate)
            claimdate = "&claimdate="+document.forms[0].hdclaimdate.value;   
        if(document.forms[0].hdpolicydate)
            policydate = "&policydate="+document.forms[0].hdpolicydate.value;               
        if(document.forms[0].hdfilter)
            filter = "&filter="+document.forms[0].hdfilter.value;               
	    //avipinsrivas Start : Worked for 13999 (Issue of 13196 - Epic 7767)
        if (document.forms[0].hdhideglobalsearch)
            hideglobalsearch = "&hideglobalsearch=" + document.forms[0].hdhideglobalsearch.value;
        //avipinsrivas end
		var sUrl=sPage+"?viewid="+document.forms[0].cboViews.options[document.forms[0].cboViews.selectedIndex].value+sTableId+sType+formname+sysformname+screenflag
		+tablename + rowid + eventdate + claimdate + policydate + filter + hideglobalsearch;

		//		alert(sUrl);
		
		window.location.href=sUrl;
	}
}

function replace(sSource, sSearchFor, sReplaceWith)
{
	var arr = new Array();
	arr=sSource.split(sSearchFor);
	return arr.join(sReplaceWith);
}

function SetDefaultView()
{
    var sysformname = '';
	var iViewId=document.forms[0].viewid.value;
	var sScreenFlag = document.forms[0].hdScreenFlag.value;
	if (iViewId=="")
		return false;
	var iType=document.forms[0].catid.value;
	var bChecked = (document.forms[0].setasdefault.checked==true);
	//Start MITS 11128 by parag
	var formname = document.forms[0].hdFormName.value;
	if (document.forms[0].hdSysFormName)             //Added by csingh7 : MITS 12545
	    sysformname = document.forms[0].hdSysFormName.value  
	    //MITS 17157 Pankaj 7/15/09 Org Hierarchy Advanced Search
	    var tablename='';
        var rowid='';
        var eventdate='';
        var claimdate='';
        var policydate='';        
        var filter = '';
        var hideglobalsearch = '';              //avipinsrivas Start : Worked for JIRAs - 9610 (Issue-4634 / Epic-340)
        if(document.forms[0].hdtablename)
            tablename  = "&tablename="+document.forms[0].hdtablename.value;            
        if(document.forms[0].hdrowid)
            rowid = "&rowid="+document.forms[0].hdrowid.value;
        if(document.forms[0].hdeventdate)
            eventdate = "&eventdate="+document.forms[0].hdeventdate.value;
        if(document.forms[0].hdclaimdate)
            claimdate = "&claimdate="+document.forms[0].hdclaimdate.value;    
        if(document.forms[0].hdpolicydate)
            policydate = "&policydate="+document.forms[0].hdpolicydate.value;                
        if(document.forms[0].hdfilter)
            filter = "&filter=" + document.forms[0].hdfilter.value;
        //avipinsrivas Start : Worked for JIRAs - 9610 (Issue-4634 / Epic-340)
        if (document.forms[0].hdhideglobalsearch)
            hideglobalsearch = "&hideglobalsearch=" + document.forms[0].hdhideglobalsearch.value;
        //avipinsrivas End
    document.location="../Search/searchmain.aspx?formname="+formname+"&sysformname="+sysformname+"&setdefault="+bChecked+"&viewid="+iViewId+"&type="+iType+"&screenflag="+sScreenFlag
    + tablename + rowid+ eventdate+ claimdate + policydate + filter + hideglobalsearch ;
	//End MITS 11128 by parag
	return false;
}

/*function SetDefaultViewSearchGen()
{
	var sys_ex='';
	var iViewId=document.forms[0].viewid.value;
	if (iViewId=="")
		return false;
	var iType=document.forms[0].catid.value;
	var bChecked = (document.forms[0].setasdefault.checked==true);
	if(eval("document.forms[0].sys_ex"))
		sys_ex="&formname="+document.forms[0].sys_ex.value;
		
	document.location="searchgen.asp?setdefault="+bChecked+"&viewid="+iViewId+"&type="+iType+sys_ex;
	return false;
}*/

/*function selectDate(sFieldName)
{
	if(m_codeWindow!=null)
		m_codeWindow.close();
		
	m_sFieldName=sFieldName;
	
	m_codeWindow=window.open('calendar.html','codeWnd',
		'width=230,height=230'+',top='+(screen.availHeight-230)/2+',left='+(screen.availWidth-230)/2+',resizable=yes,scrollbars=no');
	return false;
}*/

function dateSelected(sDay, sMonth, sYear)
{
	var objCtrl=null;
	if(m_codeWindow!=null)
		m_codeWindow.close();
	objCtrl=eval('document.forms[0].'+m_sFieldName);
	if(objCtrl!=null)
	{
		sDay=new String(sDay);
		if(sDay.length==1)
			sDay="0"+sDay;
		sMonth=new String(sMonth);
		if(sMonth.length==1)
			sMonth="0"+sMonth;
		sYear=new String(sYear);
			
		objCtrl.value=formatDate(sYear+sMonth+sDay);
	}
	m_sFieldName="";
	m_codeWindow=null;
	return true;
}

function ssnLostFocus(objCtrl)
{
	// ###-##-####
	/* 
	if(objCtrl.value.length==0)
			return false;
	var sValue=new String(objCtrl.value);
	sValue=stripNonDigits(sValue);
	if(sValue.length!=9)
	{
		alert("Please enter valid SSN number.");
		objCtrl.value="";
		objCtrl.focus();
		return false;
	}
	objCtrl.value=sValue.substr(0,3)+"-"+sValue.substr(3,2)+"-"+sValue.substr(5,4);
	return true;
	*/
	if(objCtrl.value.length==0)
			return false;
	var sValue=new String(objCtrl.value);
	//tkr 4/2003 do not enforce format if user using wildcard
	if(sValue.indexOf('*')!=-1)
		return true;
	var sCheck=new String();
	//check is 9 digits
	sCheck=stripNonDigits(sValue);
	if(sCheck.length!=9)
	{
		alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
		objCtrl.value="";
		objCtrl.focus();
		return false;
	}
	//user entered SSN with dashes at wrong places
	if(sValue.length==11)
	{
		if(sValue.charAt(3)!="-" || sValue.charAt(6)!="-")
		{
			alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
			objCtrl.value="";
			objCtrl.focus();
			return false;
		}
		return true;	
	}		
	
	//user entered a tax id with dashes
	if(sValue.length==10)
	{
		if(sValue.charAt(2)!="-")
		{
			alert("Please enter valid SSN (###-##-####) or TaxID (##-#######).");
			objCtrl.value="";
			objCtrl.focus();
			return false;
		}
		return true;
	}
    //fall through:  if user did not add dashes, default to SSN ###-##-####
    //MITS 28481 hlv 6/8/12 comment
	//objCtrl.value=sCheck.substr(0,3)+"-"+sCheck.substr(3,2)+"-"+sCheck.substr(5,4); 
	return true;
	
}

function stripNonDigits(str) {
	return str.replace(/[^0-9]/g,"")
}
//Function Added by Shivendu for MITS 11735
  function checkEnterCurrency(e,objCtrl)
          { 
          
          //e is event object passed from function invocation
          var characterCode; //literal character code will be stored in this variable

          if(e && e.which)
          { //if which property of event object is supported (NN4)
          e = e;
          characterCode = e.which; //character code is contained in NN4's which property
          }
          else{
          e = event;
          characterCode = e.keyCode; //character code is contained in IE's keyCode property
          }

          if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
           currencyLostFocus(objCtrl);
          }

          }
 //Function Added by Shivendu for MITS 11735
  function checkEnterNum(e,objCtrl)
          { 
          
          //e is event object passed from function invocation
          var characterCode; //literal character code will be stored in this variable

          if(e && e.which)
          { //if which property of event object is supported (NN4)
          e = e;
          characterCode = e.which; //character code is contained in NN4's which property
          }
          else{
          e = event;
          characterCode = e.keyCode; //character code is contained in IE's keyCode property
          }

		  //Start rsushilaggar Added the code to allow only numeric number to get entered in the field
		  if (!(characterCode >= 48 && characterCode <= 57)) {
			  e.returnValue = false;
		  }
		  //End rsushilaggar

          if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
           numLostFocus(objCtrl);
          }

          }
          
//Abhay-02/27/09-Function to set value for combobox              
function AssignValue(val,objCtrlId)
{   
    var objCtrl = document.getElementById(objCtrlId + '_lst');
    if(objCtrl != null)
    {
        objCtrl.value = val;
    }
}
//sgoel6 04/09/2009 MITS 14192 Function from form.js
function getOrgLevelEx(iLevel)
{
       switch(iLevel)
    {
        case 8:
            return 'DEPARTMENT';
            break;
        case 7:
            return 'FACILITY';
            break;
        case 6:
            return 'LOCATION';
            break;
        case 5:
            return 'DIVISION';
            break;
        case 4:
            return 'REGION';
            break;
        case 3:
            return 'OPERATION';
            break;
        case 2:
            return 'COMPANY';
            break;
        case 1:
            return 'CLIENT';
        case 0: //abansal23 MITS 16751 on 7/1/2009
            return 'ALL';
            break;
    }    
}

function SubmitQuery()
{
    if(event.keyCode)
    {
        if (event.keyCode == 13) 
        {   // csingh7 MITS 18830 : changed id for Submit Query button to btnSubmit1 as per searchcriteria.xsl
            document.getElementById('btnSubmit1').focus();       //Added by csingh7 : MITS 15262
            document.getElementById('btnSubmit1').click();
            return false;
        }
    } 
    else 
    {
        return true;
    }
}

//skhare7 Diary search start
function SelectAttachedOption(type, controlid) {
    var tempid;
    var controlDrpDwn;
    var controlEnd;
    var control;
    
    tempid = controlid + "op";
    controlDrpDwn = document.getElementById(tempid);
    tempid = controlid + "Label";
    controlEnd = document.getElementById(tempid);

    if (controlDrpDwn != null) 
    {

        if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value.toLowerCase() == "claim") {
            if (controlEnd != null) {
                controlEnd.value = "Claim Number :";
            }
        }
        else if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value.toLowerCase() == "event")
         {

             if (controlEnd != null) {
                 controlEnd.value = "Event Number :";
             }


         }
         else if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value.toLowerCase() == "policy") {

             if (controlEnd != null) {
                 controlEnd.value = "Policy Number :";
             }


         }
         else if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value.toLowerCase() == "claimant") {

             if (controlEnd != null) {
                 controlEnd.value = "Claimant Last Name :";
             }


         }
         else if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value.toLowerCase() == "entity") {

             if (controlEnd != null) {
                 controlEnd.value = "Entity Last Name :";
             }


         }
         else if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value.toLowerCase() == "pi") {

             if (controlEnd != null) {
                 controlEnd.value = "PI Last Name :";
             }


         }
         else if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value.toLowerCase() == " ") {

             if (controlEnd != null) {
                 controlEnd.value = "";
             }


         }
         else if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value.toLowerCase() == "funds") {

             if (controlEnd != null) {
                 controlEnd.value = "Control Number :";
             }


         }
         else if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value.toLowerCase() == "policyenh") {

             if (controlEnd != null) {
                 controlEnd.value = "Policy Name :";
             }


         }
    }

    return false;
}
//skhare7 Diary search End
//ybhaskar: MITS 14684: START

function BetweenOption(type,controlid) 
{
    var tempid;
    var controlDrpDwn;
    var controlEnd;
    var control;

    tempid = controlid + "op";
    controlDrpDwn = document.getElementById(tempid);
    tempid = controlid + "end";
    controlEnd = document.getElementById(tempid);
    var calendarEnd = $("#"+tempid); //RMA-JIRA 1535

    if (controlDrpDwn != null) {

        switch (type) {
            case "date":
                tempid = "btntwo" + controlid;
                control = document.getElementById(tempid);

                if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value == "between") {
                    if (controlEnd != null)
                    { controlEnd.style.display = ""; }
                    if (control != null)
                    { control.style.display = ""; }
                    //RMA-JIRA 1535 start
                    if (calendarEnd != null)
                    { calendarEnd.datepicker("option", "showOn", "button"); }
                    //RMA-JIRA 1535 end
                }
                else {
                    if (controlEnd != null)
                    { controlEnd.style.display = "none"; }
                    if (control != null)
                    { control.style.display = "none"; }
                    //RMA-JIRA 1535 start
                    if (calendarEnd != null)
                    { calendarEnd.datepicker("option", "showOn", "focus"); }
                    //RMA-JIRA 1535 end 
                }
                break;
            case "time":
                if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value == "between") {
                    if (controlEnd != null)
                    { controlEnd.style.display = ""; }
                }
                else {
                    if (controlEnd != null)
                    { controlEnd.style.display = "none"; }
                }
                break;
            case "currency":
                if (controlDrpDwn.options[controlDrpDwn.selectedIndex].value == "between") {
                    if (controlEnd != null)
                    { controlEnd.style.display = ""; }
                }
                else {
                    if (controlEnd != null)
                    { controlEnd.style.display = "none"; }
                }
                break;
        }
   }
    
    return false;
}

//ybhaskar: MITS 14684: END

//skhare7 JIRA 340 Entity Roles start
function SetUsefullEntitySearch()
{
    var formname = '';
    var bEntitychecked = '';
    var sysformname = '';
    var iViewId = "-1";
    //avipinsrivas Start : Worked for JIRA - 5294 (Entity Role)
    if(document.forms[0].viewid != null)
        iViewId = document.forms[0].viewid.value;
    //avipinsrivas End
    var sScreenFlag = document.forms[0].hdScreenFlag.value;
    if (iViewId == "")
        return false;
    var iType = document.forms[0].catid.value;
    var bChecked = (document.forms[0].setasdefault.checked == true);
    //Start MITS 11128 by parag
    // if (bChecked)
    if (document.forms[0].fullentitysearch != null)
    {

        bEntitychecked = (document.forms[0].fullentitysearch.checked == true);

     
    }
    //if (bEntitychecked)
    //    document.forms[0].hdFormName.value = 'entity';
    
      formname = document.forms[0].hdFormName.value;


    if (document.forms[0].hdSysFormName)  
        sysformname = document.forms[0].hdSysFormName.value
    
    var tablename = '';
    var rowid = '';
    var eventdate = '';
    var claimdate = '';
    var policydate = '';
    var filter = '';
    if (document.forms[0].hdtablename)
        tablename = "&tablename=" + document.forms[0].hdtablename.value;
    if (document.forms[0].hdrowid)
        rowid = "&rowid=" + document.forms[0].hdrowid.value;
    if (document.forms[0].hdeventdate)
        eventdate = "&eventdate=" + document.forms[0].hdeventdate.value;
    if (document.forms[0].hdclaimdate)
        claimdate = "&claimdate=" + document.forms[0].hdclaimdate.value;
    if (document.forms[0].hdpolicydate)
        policydate = "&policydate=" + document.forms[0].hdpolicydate.value;
    if (document.forms[0].hdfilter)
        filter = "&filter=" + document.forms[0].hdfilter.value;
   

    document.location = "../Search/searchmain.aspx?formname=" + formname + "&sysformname=" + sysformname + "&fullentitysearch=" + bEntitychecked + "&setdefault=" + bChecked + "&viewid=" + iViewId + "&type=" + iType + "&screenflag=" + sScreenFlag
    + tablename + rowid + eventdate + claimdate + policydate + filter;
    
    return false;
    
}
//skhare7 JIRA 340 Entity Roles End
