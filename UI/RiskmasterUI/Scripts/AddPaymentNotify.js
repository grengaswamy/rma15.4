
function trim(value)
{
	value = value.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2');
	return value;
}
function DisableDiaryManager()
{
	if (document.forms[0].DiaryToManager.checked)
	{
	document.forms[0].QueuePayment.checked=false;
	  
	document.forms[0].UseSupChain.checked=false;
	  
	document.forms[0].DaysApproval.value="";
	  
	document.forms[0].DaysApproval.disabled=true;
	}
}
function DisableTrans()
{
  if (document.forms[0].SpecTranTypeCodes.checked==false)
  {
  document.forms[0].TransCodes.disabled=true;
  document.forms[0].TransCodesbtndel.disabled=true;
  document.forms[0].TransCodesbtn.disabled=true;
  }
  else
  {
   
  document.forms[0].TransCodes.disabled=false;
  document.forms[0].TransCodesbtndel.disabled=false;
  document.forms[0].TransCodesbtn.disabled=false;
  }
  
}
function Check()
{
    if (CheckExists())
     {
       
        window.opener.document.forms[0].hdnNewLOB.value = document.forms[0].LBusiness.value;
        window.opener.document.forms[0].hdnNewUserToNotify.value = document.forms[0].UNotify.value;
        window.opener.document.forms[0].hdnNewNotifyPeriod.value = document.forms[0].NPeriod.value;
        window.opener.document.forms[0].hdnNewDateCriteria.value = document.forms[0].DCriteria.value;
        var RecordId = window.opener.document.forms[0].hdnRecordId.value;
        if (RecordId == "")
         {
          
            window.opener.document.forms[0].hdnRecordId.value = -1;
        }
        else {
           
            window.opener.document.forms[0].hdnRecordId.value = parseInt(window.opener.document.forms[0].hdnRecordId.value) - parseInt(1);
        }
        var PaymentValues = document.forms[0].UNotify.value + "|" + document.forms[0].LBusiness.value + "|" + document.forms[0].NPeriod.value + "|" + document.forms[0].DCriteria.value + "|" + window.opener.document.forms[0].hdnRecordId.value + "**";
        window.opener.document.forms[0].hdnPaymentNotifyValue.value = window.opener.document.forms[0].hdnPaymentNotifyValue.value + PaymentValues; //Asif
        window.opener.document.forms[0].hdnPayment.value = window.opener.document.forms[0].hdnPayment.value + PaymentValues; //Asif

        window.opener.document.forms[0].hdnAction.value = "Add";
        window.opener.document.forms[0].hidden_DataChanged.value = "true";
        window.opener.document.forms[0].hdnCriteria.value = "Y";
        //window.opener.document.forms[0].submit();
        //self.close();

    }
    //Added By Asif
    else
        return false;
		
}
function CheckExists()
{
	arr=window.opener.document.forms[0].hdnPaymentNotifyValue.value.split("**");
	//str=document.forms[0].UserNotify.value+"|"+document.forms[0].LOB.value+"|"+document.forms[0].NotifyPeriod.value+"|"+document.forms[0].DateCriteria.value;
	str=document.forms[0].UNotify.value+"|"+document.forms[0].LBusiness.value+"|"+document.forms[0].NPeriod.value+"|"+document.forms[0].DCriteria.value;
	for(i=0;i<arr.length;i++)
	{
	   arrTemp=arr[i].split("|");
	   sValue=arrTemp[0]+"|"+arrTemp[1]+"|"+arrTemp[2]+"|"+arrTemp[3];
	   if (sValue==str)
	  { 
	    alert("The chosen settings have already been entered.");
	    return false;
	  }
	}
	return true;
}

function DisableDuplicatePayment()
{
  if (document.forms[0].DupPayCheck.checked)
  {
  	document.forms[0].DupCriteria.disabled=false;
  }
  else
  {
   	document.forms[0].DupCriteria.disabled=true;
  }
  
}

function DisableQueuePayment()
{
	if (document.forms[0].QueuePayment.checked)
	{
	document.forms[0].DiaryToManager.checked=false;
	  
	document.forms[0].UseSupChain.checked=true;
	  
	   
	document.forms[0].DaysApproval.disabled=false;
	  
	document.forms[0].DaysApproval.value="1";
	  
	}
 
}
function DisableUseSupChain()
{
	if (document.forms[0].UseSupChain.checked)
	{
	document.forms[0].DiaryToManager.checked=false;
	document.forms[0].DaysApproval.disabled=false;  
	document.forms[0].DaysApproval.value="1"; 
	}
	else
	{
	document.forms[0].QueuePayment.checked=false;
	document.forms[0].DaysApproval.value=""; 
	document.forms[0].DaysApproval.disabled=true;  
	   
	}
}
function setSize()
{
	document.forms[0].DaysApproval.style.size="5"
}
function SelectAll()
{
	var allvalue="";
	ctrl=document.getElementById('TransCodes');
	try
	{
	for(var f=0;f<ctrl.options.length;f++)
	{
		allvalue=allvalue+ctrl.options[f].value+",";
	}
	ctrl=document.getElementById('hdnTransValues');
	ctrl.value=allvalue;
	}catch(ex)
	{
	 return true;
	}
	
	return true;
}
function SelectAll1()
{

	ctrl=document.getElementById('TransCodes');

	for(var f=0;f<ctrl.options.length;f++)
	{
		ctrl.options[f].selected=true;
			
	}
	value1=ctrl.value;

	//value=trim(value);

	ctrl=document.getElementById('hdnTransValues');
	ctrl.value=value1;

	return true;
}					
function SetCriteria()
{
    if (CheckExists())
    {
	document.forms[0].hdnCriteria.value="Y";
	return true;
	}
	else
	{
	 return false;
	}
}
function DeleteValue()
{
	document.forms[0].hdnCriteria.value="Y";
}
function OpenCriteria()
{
 /* if (document.forms[0].UseSupChain.checked)
  {
	document.forms[0].DaysApproval.disabled=true;
  }
  else
  {
    document.forms[0].DaysApproval.disabled=false;
  }
 */
 if (document.forms[0].DupCriteria.value=="1" ||document.forms[0].DupCriteria.value=="5"
  ||document.forms[0].DupCriteria.value=="6" || document.forms[0].DupCriteria.value=="8")
	{
	m_Wnd=window.open('csc-Pages/riskmaster/RMUtilities/CheckOptions/common/html/test.html','edit','width=350,height=150'+',top='+(screen.availHeight-600)/2+',left='+(screen.availWidth-600)/2+',resizable=yes,scrollbars=yes');
		
	}
}