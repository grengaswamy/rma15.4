﻿
/*exported app*/
//var app = angular.module('rmaApp', ['ngGrid']);
//app.directive('onlyNum', function () {
//    return {

//        link: function (scope, element, attrs) {
//            var keyCode = [8, 9, 36, 37, 39, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];
//            element.bind("keydown", function (event) {
//                console.log($.inArray(event.which, keyCode));
//                if ($.inArray(event.which, keyCode) == -1) {
//                    scope.$apply(function () {
//                        scope.$eval(attrs.onlyNum);
//                        event.preventDefault();
//                    });
//                    event.preventDefault();
//                }

//            });
//        }
//    }
//});

app.directive('onlyNum', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^0-9]/g, '');                
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});