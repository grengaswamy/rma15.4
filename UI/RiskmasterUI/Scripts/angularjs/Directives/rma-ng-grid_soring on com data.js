/*exported app*/
var app = angular.module('rmaApp', ['ngGrid']);
app.directive('rmaGrid', function ($timeout,$http) {
    return {
      restrict:'E',
	  transclude:false,
	  scope:false,
        template: 
       
        '<img ng-src="../../Images/tb_save_active.png" style="cursor: pointer;cursor: hand;" ng-click="savePreference()" title="' + NgGridToolTips.ttSavePreference + '"/> '      
       + '<img ng-src="../../Images/redo.gif" style="cursor: pointer;cursor: hand;" ng-click="restoreDefault()" title="' + NgGridToolTips.ttRestoreDefaults + '"/> '
       + '<img ng-src="../../Images/tb_exportexcel_active.png" style="cursor: pointer;cursor: hand;" onclick="redirectE2E()" title="' + NgGridToolTips.ttExport + '"/> '
       + '<div class="gridStyle" ng-grid="gridOptions">'       
       + '<div class="no-rows" ng-show="!show">'
       +        '<div class="msg"> <span>' + NgGridLabels.lblNoRows + '</span> </div>'
       +     '</div>'
       + '</div>',
	  
        link: function (scope, attrs) {
            //can declare variable inside directive and they will be available on page's scope
	    scope.moreFilters={
		   a : 'true',	
		   b : 'true'
	    };
	  },
	  controller: function ($timeout, $scope, $attrs) {
	       
	      $scope.pagingOptions = {
	          pageSizes: [],
	          pageSize: 10,
	          currentPage: 1
	      };
	      $scope.sortDetails = { fields: '', direction: ''};
	      //$scope.SortCol = "Status";	 
	      $scope.setSortDetails = function (sortCols,sortDirections) {
	          //$scope.SortDirectionsArray = [];
	          //$scope.sortColArray = [];
	          //$scope.sortColArray = sortColsJsonString.split(',');
	          //$scope.SortDirectionsArray = sortDirectionJsonString.split(',');       
	          //$scope.sortDetails = {	             
	          //    fields: $scope.sortColArray,
	          //    directions: $scope.SortDirectionsArray
	          //};
	          //$scope.SortDirectionsArray = JSON.parse(sortCols);
	          //$scope.sortColArray = JSON.parse(sortDirection);
	          
	          $scope.sortDetails = {
	              fields: sortCols,
	              directions: sortDirections
	          };
	      };
	      //debugger;
	      $scope.gridData = [];
	      $scope.completeData = [];
	      $scope.gridColumns = [];
	      $scope.additionalData = [];
	      $scope.totalItems = 0;
	      $scope.totalServerItems = 0;
	      $scope.AdditionalUserPref = [];
	      //var layoutPlugin = new ngGridLayoutPlugin();
	      //$scope.updateLayout = function () {
	      //    layoutPlugin.updateGridLayout();
	      //};

	     
	      //read attribute values :start
          //all attributes must be in small capps
	      //enable client side paging/ if enable paging is true and clientsidepaging attribute is not suplllied, we will consider it as true for default value
	      if ($attrs["clientsidepaging"] != undefined && $attrs["clientsidepaging"] != "")
	          $scope.clientSidePaging = $scope.$eval($attrs.clientsidepaging); //string values for the attribute can be directly picked wth out eval, but we want thevalue as boolean so using eval
	      else
	          $scope.clientSidePaging = true;
	      //Enable Cell Edit in Focus
	      if ($attrs["enablecelleditonfocus"] != undefined && $attrs["enablecelleditonfocus"] != "")
	          $scope.enableCellEditOnFocus = $scope.$eval($attrs.enablecelleditonfocus);
	      else
	          $scope.enableCellEditOnFocus = false;
	      if ($attrs["enablerowselection"] != undefined && $attrs["enablerowselection"] != "")
	          $scope.enableRowSelection = $scope.$eval($attrs.enablerowselection);
	      else
	          $scope.enableRowSelection = true;
	      //Enable multiple row selection or not, defaul;t will be true if attribute not provided
	      if ($attrs["multiselect"] != undefined && $attrs["multiselect"] != "")
	          $scope.multiSelect = $scope.$eval($attrs.multiSelect);
	      else
	          $scope.multiSelect = false;
	      //page size
	      if ($attrs["pagesize"] != undefined && $attrs["pagesize"] != "")
	          $scope.pagingOptions.pageSize = $attrs.pagesize;
	      //else
	      //    $scope.pagingOptions.pageSize = "10";
	      //grid id
	      if ($attrs["id"] != undefined && $attrs["id"] != "") //mendatory attribute to have on grid
	          $scope.gridid = $attrs.id;
	      //enable client side sorting/ if clientsidesorting attribute is not suplllied, we will consider it as true for default value
	      if ($attrs["clientsidesorting"] != undefined && $attrs["clientsidesorting"] != "")
	          $scope.clientsidesorting = $scope.$eval($attrs.clientsidesorting); //string values for the attribute can be directly picked wth out eval, but we want thevalue as boolean so using eval
	      else
	          $scope.clientsidesorting = true;

	      if ($scope.clientsidesorting == false)
	          $scope.useExternalSorting = true; //enable custom sorting if clientsidesorting is set to false
	      else
	          $scope.useExternalSorting = false;
	     	      
	      if ($attrs["showselectioncheckbox"] != undefined && $attrs["showselectioncheckbox"] != "")
	          $scope.showSelectionCheckbox = $scope.$eval($attrs.showselectioncheckbox); //string values for the attribute can be directly picked wth out eval, but we want thevalue as boolean so using eval
	      else
	          $scope.showSelectionCheckbox = true;
	    
	      //read attributes: end

	     
	      $scope.updateGridOptions = function () {
	          $scope.pagingOptions.currentPage = 1;// this will be modified for server side paging

	          //$scope.gridColumns = gridColDef;
	          $scope.gridOptions = {
	              data: 'gridData',
	              columnDefs: 'gridColumns',
	              enableSorting: true,
	              enablePaging: true,
	              showFilter: false,
	              showFooter: true,
	              //footerTemplate: '<div ng-show="showFooter" class="ngFooterPanel" ng-class="{\'ui-widget-content\': jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}" ng-style="footerStyle()"><div class="ngTotalSelectContainer"><div class="ngFooterTotalItems" ng-class="{\'ngNoMultiSelect\': !multiSelect}" ><span class="ngLabel">' + NgGridLabels.lblTotalItems + ' {{maxRows()}}</span><span ng-show="filterText.length > 0" class="ngLabel">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span></div><div class="ngFooterSelectedItems" ng-show="multiSelect"><span class="ngLabel">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}</span></div></div>',
	              footerTemplate: '<div ng-show="showFooter" class="ngFooterPanel" ng-class="{\'ui-widget-content\': jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}" ng-style="footerStyle()"><div class="ngTotalSelectContainer"><div class="ngFooterTotalItems" ng-class="{\'ngNoMultiSelect\': !multiSelect}" ><span class="ngLabel">' + NgGridLabels.lblTotalItems + ' {{maxRows()}}</span><span ng-show="filterText.length > 0" class="ngLabel">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span></div><div class="ngFooterSelectedItems" ng-show="multiSelect"><span class="ngLabel">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}</span></div></div><div class="ngPagerContainer" style="float: right; margin-top: 10px;" ng-show="enablePaging" ng-class="{\'ngNoMultiSelect\': !multiSelect}"><div style="float:left; margin-right: 10px;" class="ngRowCountPicker"><span style="float: left; margin-top: 4px; margin-right: 5px;" class="ngLabel">' + NgGridLabels.lblPageSize + '</span><input type="text"  ng-click="stopClickProp($event)" ng-model="pagingOptions.pageSize" style="float: left;height: 20px; width: 100px" only-num maxlength="5" /></div><div style="float:left; margin-right: 10px; line-height:25px;" class="ngPagerControl" style="float: left; min-width: 135px;"><button type="button" class="ngPagerButton" ng-click="pageToFirst()" ng-disabled="cantPageBackward()" title="' + NgGridToolTips.ttFirstPage + '"><div class="ngPagerFirstTriangle"><div class="ngPagerFirstBar"></div></div></button><button type="button" class="ngPagerButton" ng-click="pageBackward()" ng-disabled="cantPageBackward()" title="' + NgGridToolTips.ttPreviousPage + '"><div class="ngPagerFirstTriangle ngPagerPrevTriangle"></div></button><input class="ngPagerCurrent" min="1" max="{{maxPages()}}" type="text" style="width:50px; height: 24px; margin-top: 1px; padding: 0 4px;"  ng-model="pagingOptions.currentPage" only-num maxlength="5" /><span class="ngLabel">' + NgGridLabels.lblof + '</span> <span class="ngLabel">{{maxPages()}}</span> <button type="button" class="ngPagerButton" ng-click="pageForward()" ng-disabled="cantPageForward()" title="' + NgGridToolTips.ttNextPage + '"><div class="ngPagerLastTriangle ngPagerNextTriangle"></div></button><button type="button" class="ngPagerButton" ng-click="pageToLast()" ng-disabled="cantPageToLast()" title="' + NgGridToolTips.ttLastPage + '"><div class="ngPagerLastTriangle"><div class="ngPagerLastBar"></div></div></button></div> </div></div>',	              
	              pagingOptions: $scope.pagingOptions,
	              enableColumnResize: true,
	              enableCellSelection: false,
	              enableColumnReordering: true,
	              showColumnMenu: true,
	              enableCellEditOnFocus: $scope.enableCellEditOnFocus,
	              plugins: [new FilterPlugin(), new ngGridExportPlugin()],
	              jqueryUIDraggable: true,
	              i18n: 'en',
	              headerRowHeight: 60,
	              multiSelect: $scope.multiSelect,
	              useExternalSorting: $scope.useExternalSorting,
	              showSelectionCheckbox: $scope.showSelectionCheckbox,
	              totalServerItems: 'totalServerItems',
	              completeData: 'completeData',
	              sortInfo: $scope.sortDetails,
                  completeData:$scope.completeData
	          };
	      };

	      $scope.bindGrid = function (JsonData, JsonUserPref, additionalInfo) {
	          try {	             
	              $scope.gridData = JsonData;
	              $scope.completeData = JsonData;
	              $scope.gridColumns = JsonUserPref.colDef;
	              $scope.additionalData = additionalInfo;
	              //var iTotItems = Number.parseInt(JsonAdditionalData[0].TotalCount);
	              $scope.totalItems = parseInt(additionalInfo[0].TotalCount);
	              $scope.totalServerItems = $scope.totalItems; //must be available on controller from server

	              //page size is available to be defined as grid attribute for default page size and user can change it as per its preference
                  //if page size is not defined as grid attribute, the we will get page size from user pref. if page size is not available in user pref also, then we will set it as 10 by default
	              if (JsonUserPref.PageSize == undefined || JsonUserPref.PageSize == "") {
	                  if ($attrs["pagesize"] != undefined && $attrs["pagesize"] != "")
	                      $scope.pagingOptions.pageSize = $attrs.pagesize;
	                  else
	                      $scope.pagingOptions.pageSize = "10";
	              }
	              else
	                  $scope.pagingOptions.pageSize = JsonUserPref.PageSize;
	              if ($scope.clientsidesorting)
	                  $scope.setSortDetails(JsonUserPref.SortColumn, JsonUserPref.SortDirection);	             
	               $scope.updateGridOptions();
	             
	          }
	          catch (exception) { }
	      };	      

	      $scope.updateEmptyGridOptions = function () {
	          $scope.pagingOptions.currentPage = 1;// this will be modified for server side paging
	          $scope.gridOptions = {
	              data: 'gridData',
	              columnDefs: 'gridColumns'	              
	          };
	      };	      
	        
	      $scope.getGridData = function () {
	          try
	          {
	              var JsonData = JSON.parse(document.getElementById("hdnJsonData").value);
	              var JsonUserPref = JSON.parse(document.getElementById("hdnJsonUserPref").value);
	              var JsonAdditionalData = JSON.parse(document.getElementById("hdnJsonAdditionalData").value);
	              $scope.JsonAdditionalUserPref = JsonUserPref.AdditionalUserPref;
	              $scope.bindGrid(JsonData, JsonUserPref, JsonAdditionalData);	             
	          }
	          catch(error)
	          {
	              $scope.ShowError(error);
	          }
	      };
	      $scope.getGridData();
	      
          //paging code:start
	      $scope.setPagingData = function (data, page, pageSize, totalLength) {	         
	          $scope.totalServerItems = totalLength;
	          if (totalLength == null || totalLength <= 0) {	             
	              //$('#divReserveGrid').hide();
	              //$('#divImage').hide();
	          }
	          else {
	              //$('#divReserveGrid').show();
	              //$('#divImage').show();	             
	              var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
	              //alert(pagedData.length);
	              //		          $scope.myData = pagedData;
	              $scope.gridData = pagedData;
	              if (!$scope.$$phase) {
	                  $scope.$apply();
	              }
	          }
	      };
	      
	      $scope.setPagingData($scope.completeData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.totalServerItems);

	      $scope.$watch('pagingOptions', function (newVal, oldVal) {
	          if ($scope.clientSidePaging == true) {
	              
	              $scope.pagingBaseOptions(newVal, oldVal, $scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.completeData);
	          }
	          else {
	              //write code here to get paging data from server.
	          }
	      }, true);	     	      

	      $scope.pagingBaseOptions = function (newVal, oldVal, basePageSize, baseCurrentPage, data) {
	          if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize != oldVal.pageSize)) {
	              var maxPageNum = Math.ceil($scope.totalServerItems / $scope.pagingOptions.pageSize);
	              if (baseCurrentPage > maxPageNum) {
	                  baseCurrentPage = maxPageNum;	                 
	              }
	              $scope.setPagingData(data, baseCurrentPage, basePageSize, $scope.totalServerItems);
	          }
	      };

	         
          //paging code:end

	      //for extrenalsorting :start, this function needs complete testing..not tested as of now. uncomment for enabling external sorting and write code
	      
	      //$scope.$watch('sortDetails', function (newVal, oldVal) {
	      //    if (newVal !== oldVal) {
	      //        //write code here to do server side coding
	      //    }
	      //}, true);
	      
	      //for external sorting :end
	     
	      $scope.$on('ngGridEventSorted', function (args, sortInfo) {
	          $scope.sortDetails.fields = sortInfo.fields;
	          $scope.sortDetails.direction = sortInfo.directions;
	      });

          //filter code starts
	      function FilterPlugin() {

	          var self = this;
	          self.grid = null;
	          self.scope = null;
	          self.init = function (scope, grid) {
	              self.scope = scope;
	              self.grid = grid;

	              scope.SearchText = function () {
	                  var searchQuery = "";
	                  angular.forEach(self.scope.columns, function (col) {
	                      if (col.visible && col.filterText) {
	                          var filterText = col.filterText + '; ';
	                          //searchQuery += col.displayName + ": " + filterText;
	                          searchQuery += col.field + ": " + filterText;
	                          //var filterText = (col.filterText.indexOf('*') === 0 ? col.filterText.replace('*', '') : "^" + col.filterText) + ";";
	                          //searchQuery += col.displayName + ": " + filterText;
	                      }
	                  });
	                  return searchQuery;
	              };

	              function SetFilterValues(searchQuery) {
	                  self.scope.$parent.filterText = searchQuery;
	                  self.grid.searchProvider.evalFilter();
	              };

	              scope.$watch(scope.SearchText, SetFilterValues);
	          },
              self.scope = undefined,
              self.grid = undefined
	      };
	      var pluginInstance = new FilterPlugin();

	      $scope.setFilter = function (searchQuery) {
	          angular.forEach(FilterPlugin.scope.columns, function (col) {
	              if (col.visible && col.filterText) {
	                  col.filterText = searchQuery;
	              }
	          });
	          FilterPlugin.scope.$parent.filterText = searchQuery;
	          FilterPlugin.grid.searchProvider.evalFilter();
	      };
          
	      //filter code ends

          //save preference code starts
	      $scope.$on('ngGridEventColumns', function (event, newColumns) {	          
	          $scope.colCollection = newColumns;
	      });

	      $scope.savePreference = function () {
	         
	          $scope.colDef = [];
	          for (var i = 0; i < $scope.colCollection.length; i++) {
	              //if degault Name is undefined 
	              //which means that this column was never added by rma and it is column added by NG grid, for example, selection check box. this kind of columns are added by NG automaticaly so 
                  //we don't need to store them as user preference in dtabae. these columns will also not available in column menu option
	              if ($scope.colCollection[i].colDef.defaultName != undefined) { 
	                  $scope.colDef.push({
	                      field: $scope.colCollection[i].colDef.field,
	                      displayName: $scope.colCollection[i].displayName,
	                      defaultName: $scope.colCollection[i].colDef.defaultName,
	                      visible: $scope.colCollection[i].visible,
	                      cellTemplate: $scope.colCollection[i].colDef.cellTemplate,
	                      width: $scope.colCollection[i].width,
	                      headerCellTemplate: $scope.colCollection[i].headerCellTemplate,
	                      alwaysInvisibleOnColumnMenu: $scope.colCollection[i].colDef.alwaysInvisibleOnColumnMenu,
	                      sortingAlgorithm: $scope.colCollection[i].sortingAlgorithm,
	                      sortFn: $scope.colCollection[i].colDef.sortingAlgorithm
	                  });
	              }
	          }
	          $scope.gridPreference =
                 {
                     colDef: $scope.colDef,
                     PageSize: $scope.pagingOptions.pageSize,
                     SortColumn: $scope.sortDetails.fields,
                     SortDirection: $scope.sortDetails.directions,
                     AdditionalUserPref: $scope.JsonAdditionalUserPref
                 }
	          
	          var imgLoading = $("#pleaseWaitFrame", parent.document.body);
	          imgLoading.css({ display: "block" });
	          	          
	          $http({
	              //url: "FinancialDetailHistory.aspx/SavePreferences",
	              url: $scope.pageName + "/SavePreferences",
	              method: "POST",
	              data: { gridPreference: JSON.stringify($scope.gridPreference), gridId: $scope.gridid },
	              headers: {
	                  'Content-Type': 'application/json'
	              }
	          })
              .success(function (data, status, config, headers) {                 
                  imgLoading.css({ display: "none" });
                  var responseJsonObj = JSON.parse(data.d);
                  if (JSON.parse(data.d).response == "Success") {
                  }
                  else {
                      $scope.ShowError(JSON.parse(data.d).response);
                  }
                  
              })
              .error(function (data, status, config, headers) {                  
                  imgLoading.css({ display: "none" });
                  $scope.ShowError(JSON.parse(data.d).response);
                 
              });
	         
	          //return false;
	      };
          //save preference code ends
          
          //restore default code starts
	      $scope.restoreDefault = function () {
	          if (confirm(NgGridAlertMessages.RestoreDefaults)) {
	              var imgLoading = $("#pleaseWaitFrame", parent.document.body);
	              imgLoading.css({ display: "block" });
	              $scope.inputData=
	              {
	                  GridId: $scope.gridid
	              }
	              $http({
	                  url: $scope.pageName + "/RestoreDefault",
	                  // url: "FinancialDetailHistory.aspx/RestoreDefault",
	                  method: "POST",
	                  data: {inputData : JSON.stringify($scope.inputData)},
	                  headers: {
	                      'Content-Type': 'application/json'
	                  }
	              })
                  .success(function (data, status, config, headers) {
                      imgLoading.css({ display: "none" });
                      if (JSON.parse(data.d).response == "Success") {
                          $scope.bindGrid($scope.completeData, JSON.parse(JSON.parse(data.d).userPref), $scope.additionalData);
                      }
                      else {
                          $scope.ShowError(JSON.parse(data.d).response);
                      }

                  })
                  .error(function (data, status, config, headers) {                   
                      imgLoading.css({ display: "none" });
                      $scope.ShowError(JSON.parse(data.d).response);
                  });
	          }
	      };

          //restore default code ends

	      $scope.ShowError = function (erorData) {
	          var errorCtrl = $("#ErrorControl1_lblError");
	          errorCtrl.html('<font class="errortext">' + NgGridLabels.lblError +'</font><table cellspacing="0" cellpadding="0" border="0" class="errortext"><tbody><tr><td valign="top"><img src="/RiskmasterUI/Images/error-large.gif"></td><td valign="top" style="padding-left: 1em"><ul><li class="warntext">' + erorData + '</li></ul></td></tr></tbody></table>');
	      };
		  
	      $scope.show = parseInt($scope.totalItems);

	      }
    }
  });