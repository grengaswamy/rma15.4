﻿var dataExcel = {};
var headerExcel = {};
//new export to excel plugin
function ngGridExportPlugin(opts) {
    var self = this;
    self.grid = null;
    self.scope = null;
    self.services = null;
    self.init = function (scope, grid, services) {
        self.grid = grid;
        self.scope = scope;
        self.services = services;
        function showDs() {
            var keys = [];
            var displayNames = [];
            var HeaderTypes_Local = {};
            var JsonData_Local = [];
            var ExportToExcelfield = "";//rma 16828
            angular.forEach(self.scope.columns, function (col) {
               
                //rma 16828 starts, we want a generic fix to get a value that we want in excel
                if (col.colDef.allowExportToExcel != null && col.colDef.allowExportToExcel && col.visible && col.displayName != "✔")  // RMA-4307
                {                    
                    displayNames.push(col.displayName);                    
                    ExportToExcelfield = col.field + ".ExportToExcelValue";
                    if (col.colDef.isCustomExportToExcelValueDefined!=null && col.colDef.isCustomExportToExcelValueDefined)
                        keys.push(ExportToExcelfield);
                    else
                        keys.push(col.field);                   
                }
                else {//supoort old grids that does not have "allowExportToExcel" attribute
                    if (col.visible && col.displayName != "✔")  // RMA-4307
                    {
                        displayNames.push(col.displayName);
                        keys.push(col.field);
                    }
                }
                //rma 16828 ends
            });


            for (var k in displayNames) {                
                HeaderTypes_Local[csvStringify(displayNames[k])] = "String";
            }
            var gridName = scope.gridName;//rkotak:display proper file name for exported data//rkotak:rma-10915
            var ExcelData = {};
            var i = 0;
            var gridData = self.grid.filteredRows;//As compared to before code this is 1st change
            for (var gridRow in gridData) {
                for (k in keys) {
                    var curCellRaw = '';

                    if (opts != null && opts.columnOverrides != null && opts.columnOverrides[keys[k]] != null) {
                        curCellRaw = opts.columnOverrides[keys[k]](gridData[gridRow].entity[keys[k]]); //opts.columnOverrides[keys[k]](self.services.UtilityService.evalProperty(gridData[gridRow].entity, keys[k])); //As compared to before code this is 2nd change
                    } else {
                        curCellRaw = gridData[gridRow].entity[keys[k]];//self.services.UtilityService.evalProperty(gridData[gridRow].entity, keys[k]); //As compared to before code this is 3rd change
                    }
                    ExcelData[displayNames[k]] = validateXMLSpecialCharacters(csvStringify(curCellRaw));//rkotak:rma13014

                }
                if (gridRow == i) {
                    JsonData_Local[i] = ExcelData;
                    i++;
                    ExcelData = {};
                }

            }

            dataExcel[gridName] = JsonData_Local;
            headerExcel[gridName] = HeaderTypes_Local;

            var fp = grid.$root.find(".ngFooterPanel");
            var csvDataLinkPrevious = grid.$root.find(".ngFooterPanel  .csv-data-link-span");
            if (csvDataLinkPrevious != null) { csvDataLinkPrevious.remove(); }
            var csvDataLinkHtml = "<span class=\"csv-data-link-span pull-right\" style=\"display:none\">";
            csvDataLinkHtml += "<a id=\"test\" style=\"visibility: hidden\" >";           
            csvDataLinkHtml += "Excel Export</a> &nbsp;&nbsp;";
            csvDataLinkHtml += "</br></span>"; //End csv-data-link-span            
            fp.append(csvDataLinkHtml);

        }

        //rkotak start : rma 13014
        function validateXMLSpecialCharacters(str) {
            if (str == null || str == undefined)
                return '';
            if (typeof (str) === 'string') {

                str = str.replace(/&/g, "&amp;");
                str = str.replace(/</g, "&lt;");
                str = str.replace(/>/g, "&gt;");
                str = str.replace(/"/g, "&quot;");
                str = str.replace(/'/g, "&apos;");
                return str;
            }
        }
        //rkotak:rma 13014 ends
        //rkotak:rma 13014 ends
        function csvStringify(str) {
            if (str == null) { // we want to catch anything null-ish, hence just == not ===
                return '';
            }
            if (typeof (str) === 'number') {
                return '' + str;
            }
            if (typeof (str) === 'boolean') {
                return (str ? 'TRUE' : 'FALSE');
            }           

            //return JSON.stringify(str).replace(/"/g, '""');
            return (str);
        }

        setTimeout(showDs, 0);

        scope.catHashKeys = function () {
            var hash = '';
            for (var idx in grid.columns) {
                if (grid.columns[idx].visible) {
                    hash += grid.columns[idx].$$hashKey;
                }
            }
            return hash;
        };

        if (opts && opts.customDataWatcher) {
            scope.$watch(opts.customDataWatcher, showDs);
        } else {
            scope.$watch(scope.catHashKeys, showDs);
        }
        //ENDCUSTOMIZATION

        // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: only export filtered rows and visible columns.
        // to do this, we need to react when the filter changes or the visible columns change
        // NEW:
        //scope.$parent.$on('ngGridEventFilter', showDs);
        scope.$parent.$on('ngGridEventRows', showDs);//rma-10477
        scope.$parent.$on('ngGridEventColumns', showDs);
        //ISSUE RESOLUTION : 20-Jan-2015 || RMA-6887 Change page size & click random page will always export page #1
        scope.$parent.$on('ngGridEventData', showDs);
    };

};
//e2e plugin

function redirectE2E(gridName) {
    downloadReport(dataExcel[gridName],headerExcel[gridName],gridName);
}

//********************Export to Excel Code Function*****************************************
//Coded By: vchouhan6/ RMA-4307
//Creating Excel XML
function emitXmlHeader(HeaderTypes_Global) {
    if (HeaderTypes_Global != {}) {
        //$('#divActLogGrid').show();
        var headerRow = '<ss:Row>\n';

        for (var colName in HeaderTypes_Global) {
            headerRow += '  <ss:Cell ss:StyleID="s1">\n';
            headerRow += '    <ss:Data ss:Type="String">';
            headerRow += colName + '</ss:Data>\n';
            headerRow += '  </ss:Cell>\n';
        }

        headerRow += '</ss:Row>\n';
        var h1 = '<?xml version="1.0"?>\n' +
                '<ss:Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"\n' +
                'xmlns:o="urn:schemas-microsoft-com:office:office"\n' +
                'xmlns:x="urn:schemas-microsoft-com:office:excel"\n' +
                'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"\n' +
                'xmlns:html="http://www.w3.org/TR/REC-html40">\n' +
                //--------------------------Adding Styles------------------
                //style default
                '<ss:Styles>\n' +
                    '<ss:Style ss:ID="Default" ss:Name="Normal">\n' +
                        '<ss:Alignment ss:Vertical="Bottom"/>\n' +
                        '<ss:Interior ss:Color="#FFFFFF"  ss:Pattern="Solid"/>\n' +
                    '</ss:Style>\n' +
                    //style s1
                    '<ss:Style ss:ID="s1">\n' +
                        '<ss:Alignment ss:Vertical="Center" ss:WrapText="1"/>\n' +
                        '<ss:Borders>\n' +
                        '<ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>\n' +
                        '<ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>\n' +
                        '<ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>\n' +
                        '<ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>\n' +
                        '</ss:Borders>\n' +
                        '<ss:Font ss:Bold="1"/>\n' +
                        '<ss:Interior ss:Color="#D4D4D4"  ss:Pattern="Solid"/>\n' +
                    '</ss:Style>\n' +
                    //style s2
                        '<ss:Style ss:ID="s2">\n' +
                        '<ss:Alignment ss:Vertical="Center" ss:WrapText="1"/>\n' +
                        '<ss:Borders>\n' +
                        '<ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>\n' +
                        '<ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>\n' +
                        '<ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>\n' +
                        '<ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>\n' +
                        '</ss:Borders>\n' +
                        '<ss:Font ss:Bold="0"/>\n' +
                        '<ss:Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>\n' +
                    '</ss:Style>\n' +
                '</ss:Styles>\n' +
                //----------------------------
                '<ss:Worksheet ss:Name="Sheet1" >\n' +
              '<ss:Table>\n';

        for (var colName1 in HeaderTypes_Global) {
            // h1 += '<ss:Column ss:AutoFitWidth="1" ss:Width="135"/>\n';
            h1 += '<ss:Column ss:Width="120"/>\n';
        }
        return h1 + headerRow;
    }

}

function emitXmlFooter() {
    return '\n</ss:Table>\n' +
           '</ss:Worksheet>\n' +
           '</ss:Workbook>\n';
}

function jsonToSsXml(jsonObject, HeaderTypes_Global) {
    var row;
    var col;
    var xml;
    var data = typeof jsonObject != "object" ? JSON.parse(jsonObject) : jsonObject;

    xml = emitXmlHeader(HeaderTypes_Global);

    for (row = 0; row < data.length; row++) {
        //Adding Rows 
        xml += '<ss:Row>\n';
        for (col in data[row]) {
            xml += '<ss:Cell ss:StyleID="s2">\n';
            xml += '<ss:Data ss:Type="' + HeaderTypes_Global[col] + '">';
            xml += data[row][col] + '</ss:Data>\n';
            xml += '</ss:Cell>\n';
        }
        xml += '</ss:Row>\n';
    }

    xml += emitXmlFooter();
    return xml;
}

function download(content, filename, contentType) {    
    if (!contentType) contentType = 'application/octet-stream';
    var a = document.getElementById('test');
    var blobObject = new Blob([content], {
        'type': contentType
    });

    //if (!supportsDataUri()) {
    if (window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blobObject, filename);
    }
    //   } else {
    //RMA-11009     achouhan3   Added IF statement to handle access Denied exception in IE
    var IsIEBrowser = detectIE();
    if (!IsIEBrowser) {
        a.href = window.URL.createObjectURL(blobObject);
        a.download = filename;
        a.click();
    }
}

function supportsDataUri() {
    var isOldIE = navigator.appName === "Microsoft Internet Explorer";
    var isIE11 = !!navigator.userAgent.match(/Trident\/7\./);
    return !(isOldIE || isIE11); //Return true if not any IE
}

function downloadReport(JsonData_Global, HeaderTypes_Global, gridName) {
    if (JsonData_Global != [] || HeaderTypes_Global != {}) {
        if (gridName == "")
            //download(jsonToSsXml(JsonData_Global, HeaderTypes_Global), "ExportedData.xls", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//rkotak:rma-10915
            download(jsonToSsXml(JsonData_Global, HeaderTypes_Global), "ExportedData.xls", "application/vnd.ms-excel");//rkotak:rma-10915,achouhan3 14746
        else
            //download(jsonToSsXml(JsonData_Global, HeaderTypes_Global), gridName + "-ExportedData.xls", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//rkotak:rma-10915
            download(jsonToSsXml(JsonData_Global, HeaderTypes_Global), gridName + "-ExportedData.xls", "application/vnd.ms-excel");//rkotak:rma-10915,achouhan3 14746
    }
    else {
        console.log("cannot able to export what is not initalized");
    }
}
/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return true;
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return true;
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // IE 12 => return version number
        return true;
    }

    // other browser
    return false;
}

//****************************************************************************