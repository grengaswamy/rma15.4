﻿var app = angular.module('rmaApp', ['ngGrid']);
app.controller('reserveController', function ($scope, $http, $timeout, $filter) {
    var layoutPlugin = new ngGridLayoutPlugin();
    $scope.completeData = [];
    $scope.reserveData = [];
    $scope.selectedReserve = [];
    $scope.selectedRow = [];
    $scope.ErrorRows = [];
    $scope.reserveModel = {
        isShowAllItems: "",
        isShowMyTrans: ""      // Added By Nitika For JIRA 8253 
    };
    $scope.sortDetails = { fields: '', direction: '', name: '' };
    $scope.isAllItem = false;
    $scope.approveComment = '';
    $scope.approveRejectRsv = '';
    $scope.resvHistRowID = '-1';
    $scope.PrefColumnDef = [];
    $scope.ColPosition = [];
    $scope.ColDefs = [];
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: 0,
        currentPage: 1
    };
   // RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality starts
    $scope.pageName = "TransAsync.aspx"; 
    $scope.GridId = "divReserveGrid";
    $scope.gridName = "ReserveApproval";
    var strQuery = $('#hdQuery').val();
    if (strQuery == "ReserveWorkSheet") {
        $scope.gridName = "ReserveWorksheetApproval";
    }
    else {
        $scope.gridName = "ReserveApproval";
    }
    //// RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality End

    //achouhan3    added to handle Multiselect Checkbox in case of My pending Reserve Starts
    $scope.ShowAllCheckBoxes;
    $scope.IsMultiSelect;
    //$scope.gridName = "ReserveApproval";
    //achouhan3    added to handle Multiselect Checkbox in case of My pending Reserve Ends
    $scope.updateLayout = function () {
        layoutPlugin.updateGridLayout();
    };

    Object.toparams = function ObjecttoParams(obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + obj[key]);
        }
        return p.join('&');
    };
    //achouhan3    added to handle Multiselect Checkbox in case of My pending Reserve Starts
    if ($('#hdnParent').val() == "MyWork" && $('#hdQuery').val() != "ReserveWorkSheet") {
        $scope.ShowAllCheckBoxes = false;
        $scope.IsMultiSelect = false;
    }
    else {
        $scope.ShowAllCheckBoxes = true;
        $scope.IsMultiSelect = true;
    }
    if ($('#hdnParent').val() == "MyWork")
    {
        $scope.reserveModel.isShowMyTrans = true;
    }
    else
        $scope.reserveModel.isShowMyTrans = false;
    //achouhan3    added to handle Multiselect Checkbox in case of My pending Reserve ends 

    var setFilter = function SetGridFilter(searchQuery) {
        angular.forEach(filterBarPlugin.scope.columns, function (col) {
            if (col.visible && col.filterText) {
                col.filterText = searchQuery;
            }
        });
        filterBarPlugin.scope.$parent.filterText = searchQuery;
        filterBarPlugin.grid.searchProvider.evalFilter();
    };

    var filterBarPlugin = {
        init: function (scope, grid) {
            filterBarPlugin.scope = scope;
            filterBarPlugin.grid = grid;
            $scope.$watch(function () {
                var searchQuery = "";
                angular.forEach(filterBarPlugin.scope.columns, function (col) {
                    if (col.visible && col.filterText) {
                        var filterText = col.filterText + '; ';
                        searchQuery += col.displayName + ": " + filterText;
                        //var filterText = (col.filterText.indexOf('*') === 0 ? col.filterText.replace('*', '') : "^" + col.filterText) + ";";
                        //searchQuery += col.displayName + ": " + filterText;
                    }
                });
                return searchQuery;
            }, function (searchQuery) {
                filterBarPlugin.scope.$parent.filterText = searchQuery;
                filterBarPlugin.grid.searchProvider.evalFilter();
            });
        },
        scope: undefined,
        grid: undefined
    };

    $scope.setPagingData = function (data, page, pageSize, totalLength) {
        //RMA-7571  achuhan3 for rebinding of grid starts
        $scope.totalServerItems = totalLength;
        //RMA-7571  achuhan3 for rebinding of grid ends
        //$scope.selectedReserve.length = 0;
        if (totalLength == null || totalLength <= 0) {
            $('#divReserveGrid').hide();
            $('#divImage').hide();
        }
        else {
            $('#divReserveGrid').show();
            $('#divImage').show();
            //var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
            var pagedData = data;
            $scope.reserveData = pagedData;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
            //RMA-10715 achouhan3   Added to select header checkbox when Mutliselect is on while scrolling starts
            $('.ngViewport').on('scroll', function (e) {
                var horizontal = e.currentTarget.scrollLeft;
                if (horizontal) {
                    setTimeout(function () {
                        $scope.toggleSelection();
                    }, 300);
                }
            });
            //RMA-10715 achouhan3   Added to select header checkbox when Mutliselect is on while scrolling Ends
        }
    };

    $scope.gridOptions = {
        data: 'reserveData',
        columnDefs: 'ColDefs',
        enablePaging: false,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        useExternalSorting: true,
        selectedItems: $scope.selectedReserve,
        totalServerItems: 'totalServerItems',
        //plugins: [layoutPlugin],
        footerTemplate: '<div ng-show="showFooter" class="ngFooterPanel" ng-class="{\'ui-widget-content\': jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}" ng-style="footerStyle()"><div class="ngTotalSelectContainer"><div class="ngFooterTotalItems" ng-class="{\'ngNoMultiSelect\': !multiSelect}" ><span class="ngLabel"> ' + CreateReserveLabels.lblTotalItems + ' {{maxRows()}}</span><span ng-show="filterText.length > 0" class="ngLabel">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span></div><div class="ngFooterSelectedItems" ng-show="multiSelect"><span class="ngLabel">' + CreateReserveLabels.lblSelectedItems + ' {{selectedItems.length}}</span></div></div><div class="ngPagerContainer" style="float: right; margin-top: 10px;" ng-show="enablePaging" ng-class="{\'ngNoMultiSelect\': !multiSelect}"><div style="float:left; margin-right: 10px;" class="ngRowCountPicker"><span style="float: left; margin-top: 4px; margin-right: 5px;" class="ngLabel">' + CreateReserveLabels.lblPageSize + ' </span><input type="text" id="txtPageSize" ng-click="stopClickProp($event)" ng-model="pagingOptions.pageSize" style="float: left;height: 20px; width: 100px"/></div><div style="float:left; margin-right: 10px; line-height:25px;" class="ngPagerControl" style="float: left; min-width: 135px;"><button class="ngPagerButton" ng-click="pageToFirst()" ng-disabled="cantPageBackward()" title="' + CreateReserveToolTip.ttFirstPage + '"><div class="ngPagerFirstTriangle"><div class="ngPagerFirstBar"></div></div></button><button class="ngPagerButton" ng-click="pageBackward()" ng-disabled="cantPageBackward()" title="' + CreateReserveToolTip.ttPreviousPage + '"><div class="ngPagerFirstTriangle ngPagerPrevTriangle"></div></button><input class="ngPagerCurrent" min="1" max="{{maxPages()}}" type="number" style="width:50px; height: 24px; margin-top: 1px; padding: 0 4px;" ng-model="pagingOptions.currentPage"/><button class="ngPagerButton" ng-click="pageForward()" ng-disabled="cantPageForward()" title="' + CreateReserveToolTip.ttNextPage + '"><div class="ngPagerLastTriangle ngPagerNextTriangle"></div></button><button class="ngPagerButton" ng-click="pageToLast()" ng-disabled="cantPageToLast()" title="' + CreateReserveToolTip.ttLastPage + '"><div class="ngPagerLastTriangle"><div class="ngPagerLastBar"></div></div></button></div> </div></div>',
        showFilter: false,
        enableColumnResize: true,
        jqueryUIDraggable: true,
        i18n: 'en',
        //multiSelect: true,
        //plugins: [filterBarPlugin],
        headerRowHeight: 60,
        multiSelect: $scope.IsMultiSelect,
        // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: Adding Export to excel plugin
        //plugins: [filterBarPlugin, new ngGridCsvExportPlugin 
        // RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality starts
        plugins: [filterBarPlugin, new ngGridExportPlugin()],
        // RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality ends
        multiRowSelection: false,
        enableRowSelection: true,
        showSelectionCheckbox: $scope.ShowAllCheckBoxes // RMA-10211 Added for Multiselection
        //END CUSTOMIZATION    
    };

    $scope.ShowError = function (erorData) {
        var errorCtrl = $("#ErrorControl1_lblError");
        errorCtrl.html('<font class="errortext">Following Errors have been reported:</font><table cellspacing="0" cellpadding="0" border="0" class="errortext"><tbody><tr><td valign="top"><img src="/RiskmasterUI/Images/error-large.gif"></td><td valign="top" style="padding-left: 1em"><ul><li class="warntext">' + erorData.Description + '</li></ul></td></tr></tbody></table>');
    };

    $scope.ClearError = function () {
        var errorCtrl = $("#ErrorControl1_lblError");
        errorCtrl.html('');
    }

    $scope.clearControl = function () {
        $('#txtAppRejReqCom').val('');
        //pkumari3 JIRA-18255 start
        if (angular.element('#rejectreasoncode')[0] != null && typeof (angular.element('#rejectreasoncode')[0]) !== 'undefined')
            angular.element('#rejectreasoncode')[0].value = '0';
        //pkumari3 JIRA-18255 end
        $scope.selectedReserve.length = 0;
        $scope.ErrorRows.length = 0;
        $scope.selectedRow.length = 0;
    }

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        var regNumeric = /^[\d ]*$/;
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize != oldVal.pageSize)) {
            if (regNumeric.test(newVal.pageSize)) {
                $scope.setPagingData($scope.completeData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.totalServerItems);
                $timeout(function () { $scope.toggleSelection();});
            }
            else {
                $('#txtPageSize').val('0');
                $scope.pagingOptions.pageSize = 0;
                alert(CreateReserveValidations.ValidNumericVal);

            }
        }

    }, true);


    $scope.$watch('sortDetails', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.pagingOptions.currentPage = 1;
            $scope.getReserveDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);

    $scope.$on('ngGridEventColumns', function (event, newColumns) {
        $scope.PrefColumnDef = newColumns;
    });


    $scope.$on('ngGridEventSorted', function (args, sortInfo) {
        var iCount = 1;
        $scope.sortDetails.name = sortInfo.fields[0];
        $scope.sortDetails.direction = sortInfo.directions[0];
        //for (var i = 0; i < $scope.ColPosition.length; i++) {
        //    if ($scope.ColPosition[i].visible) {
        //        if ($scope.ColPosition[i] != null && $scope.ColPosition[i].field == $scope.sortDetails.name) {
        //            $scope.sortDetails.fields = iCount;
        //            break;
        //        }
        //        iCount++;
        //    }
        //}
    });

    $scope.$watch('selectedReserve', function (newVal, oldVal) {
        //Commented for RMA-10194 and added this columns in grid itself starts
        //var dateSubmitted = '';
        // var userSubmitted = '';
        //Commented for RMA-10194 and added this columns in grid itself ends
        var rowID = '-1';
        var claimID = '';
        var strRowID = '';
        // RMA-5566      achouhan3       Updated for Multiple selection for approval/Rejection Starts
        for (var i = 0; i < $scope.selectedReserve.length; i++) {
            if ($scope.selectedReserve[i] != null) {
                //Commented for RMA-10194 and added this columns in grid itself starts
                //if ($scope.selectedReserve[i].DateSubmitted != null)
                //    dateSubmitted = dateSubmitted + ($scope.selectedReserve[i].DateSubmitted != "" ? $scope.selectedReserve[i].DateSubmitted + "," : "");
                //else
                //    dateSubmitted = dateSubmitted + ($scope.selectedReserve[i].DATESUBMITTED != "" ? $scope.selectedReserve[i].DATESUBMITTED + "," : "");
                //if ($scope.selectedReserve[i].SubmittedBy != null)
                //    userSubmitted = userSubmitted + ($scope.selectedReserve[i].SubmittedBy != "" ? $scope.selectedReserve[i].SubmittedBy + "," : "");
                //else
                //    userSubmitted = userSubmitted + ($scope.selectedReserve[i].SUBMITTEDBY != "" ? $scope.selectedReserve[i].SUBMITTEDBY + "," : "");
                //Commented for RMA-10194 and added this columns in grid itself ends
                //if ($scope.selectedReserve[i].ClaimID != null) //Commented for Print Worksheet 
                //    claimID = claimID + ($scope.selectedReserve[i].ClaimID != "" ? $scope.selectedReserve[i].ClaimID + "," : "");
                if ($scope.selectedReserve[i].RSWROWID != null)
                    strRowID = strRowID + ($scope.selectedReserve[i].RSWROWID != "" ? $scope.selectedReserve[i].RSWROWID + "," : "");
                else if ($scope.selectedReserve[i].RswRowId != null)
                    strRowID = strRowID + ($scope.selectedReserve[i].RswRowId != "" ? $scope.selectedReserve[i].RswRowId + "," : "");
            }
        }
        rowID = strRowID;
        if ($scope.selectedReserve.length == 0) {
            //Commented for RMA-10194 and added this columns in grid itself starts
            // dateSubmitted = '';
            //userSubmitted = '';
            //Commented for RMA-10194 and added this columns in grid itself ends
            claimID = '';
            rowID = '-1';
            //RMA-10284     achouhan3       Added for uncheck header check box when grid rebind starts
            if ($('input:checkbox.ngSelectionHeader') != null) {
                //$('input:checkbox.ngSelectionHeader').prop('checked', false);
                $scope.gridOptions.$gridScope.toggleSelectAll(null, false);
                var selectAllHeader = angular.element(".ngSelectionHeader").scope();
                if (selectAllHeader != null && selectAllHeader) selectAllHeader.allSelected = false;
            }
            //RMA-10284     achouhan3       Added for uncheck header check box when grid rebind ends
        }

        // RMA-5566      achouhan3       Updated for Multiple selection for approval/Rejection Ends
        //if ($scope.selectedReserve[0] != null && $scope.selectedReserve[0].RSWROWID != null)
        //    rowID = $scope.selectedReserve[0].RSWROWID;
        //else if ($scope.selectedReserve[0] != null && $scope.selectedReserve[0].RswRowId != null)
        //    rowID = $scope.selectedReserve[0].RswRowId;
        //else
        //    rowID = '-1';
        // RMA-5566      achouhan3       Updated for Multiple selection for approval/Rejection Ends
        //Commented for RMA-10196 and added this columns in grid itself starts
        //$('#lblDateSubmitted').html(CreateReserveLabels.lblDateSubmitted + ": " + dateSubmitted.slice(0, -1));
        //$('#lblSubmittedBy').html(CreateReserveLabels.lblSubmittedBy + ": " + userSubmitted.slice(0, -1));
        //Commented for RMA-10196 and added this columns in grid itself ends
        // RMA-5566      achouhan3       Updated for Multiple selection for approval/Rejection Ends
        if ($scope.selectedReserve[0] != null && $scope.selectedReserve[0].ClaimID != null)
            $('#claimid').val($scope.selectedReserve[0].ClaimID);
        else if ($scope.selectedReserve[0] != null && $scope.selectedReserve[0].CLAIMID != null)
            $('#claimid').val($scope.selectedReserve[0].CLAIMID);
        else
            $('#claimid').val('0');
        // $('#claimid').val(claimID.slice(0, -1));
        //RMA-10171 achouhan3   issue fiz for alert message on multi select while print
        //$('#hdnRswId').val(rowID.slice(0, -1));
        if (rowID != "-1")
            $('#hdnRswId').val(rowID.slice(0, -1));
        else
            $('#hdnRswId').val(rowID);
        $scope.selectedRow = $scope.selectedReserve;
        //RMA-10587 achouhan3  Selection issue fixed
        $scope.toggleSelection();
    }, true);

    $scope.getReserveDataAsync = function (pageSize, page) {
        $scope.clearControl();
        $scope.ClearError();
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        var strQuery = $('#hdQuery').val();
        var strParent = $('#hdnParent').val(); //RMA-8253 pgupta93
        setTimeout(function () {
            imgLoading.css({ display: "block" });
            var p = {
                isShowAllItem: $scope.reserveModel.isShowAllItems,
                isShowMyTrans: $scope.reserveModel.isShowMyTrans,//RMA-8253 pgupta93
                approveComment: '',
                approveRejectRsv: '',
                resvHistRowID: '',
                sortDirection: $scope.sortDetails.direction,
                sortField: $scope.sortDetails.name,
            };
            $http({
                //RMA-8253 pgupta93 START
                //url: "TransAsync.aspx?call=getReserve&Title=" + strQuery,
                url: "TransAsync.aspx?call=getReserve&Title=" + strQuery + "&Parent=" + strParent,
                //RMA-8253 pgupta93 END
                method: "POST",
                data: Object.toparams(p),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                async: true
            }).success(function (data) {
                $timeout(function () {
                    //RMA-8253 pgupta93 START
                    var IsShowTrans = $scope.reserveModel.isShowMyTrans;
                    if (data.ResponseData.Data == null || !$.trim(data) || data == undefined || data.ResponseData.Data.TotalCount > 0 || data.length == 0) {
                        $('#lblEmptyData').show();
                        $('#lblEmptyData').html("<br /><br />" + parent.CommonValidations.lblEmptyData);
                        if (strParent != '' || IsShowTrans == true)
                            $('#btnPrint').hide();
                        else
                            $('#btnPrint').show();

                    }
                    else {
                        $('#lblEmptyData').hide();
                        $('#btnPrint').show();
                    }
                    //RMA-8253 pgupta93 END
                    if (data.Success) {
                        $scope.ColDefs = data.ResponseData.ColumnDef;
                        $scope.ColPosition = data.ResponseData.ColumnPosition;
                        $scope.completeData = data.ResponseData.Data;
                        $scope.pagingOptions.pageSize = data.ResponseData.PageSize;
                        $scope.setPagingData($scope.completeData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, data.ResponseData.TotalCount);
                        //$('.ngViewport').on('scroll', function () { if ($(document).scrollLeft()) { alert('test'); } });
                    }
                    else {
                        $scope.ShowError(data);
                    }
                    imgLoading.css({ display: "none" });
                });
            }).error(function (error) {
                imgLoading.css({ display: "none" });
            });
        }, 100);
    };

    $scope.ApproveClick = function () {
        $scope.ApproveRejectReserve('1');
        
    }
    $scope.RejectClick = function () {
        //JIRA-13983 pkumari3
        //if ($scope.ValidateRejectReasonDropdownClick())
        $scope.ApproveRejectReserve('2');
      
        //else alert("Please select a Reject Reason Code.");
        //JIRA-13983 pkumari3
    }
    //JIRA-13983 pkumari3
    $scope.ValidateRejectReasonDropdownClick = function () {

        if (document.getElementById("rejectreasoncode").value == "0") {
            return false;
        }
        return true;
    }
    //JIRA-13983 pkumari3

    $scope.ApproveRejectReserve = function (optType) {
        var strQuery = $('#hdQuery').val();
        $scope.ClearError();
        if ($scope.selectedReserve[0] != null && ($scope.selectedReserve[0].RSWROWID != null || $scope.selectedReserve[0].RswRowId != null)) {
            var imgLoading = $("#pleaseWaitFrame", parent.document.body);
            var strAppRejComment = $('#txtAppRejReqCom').val();
            var rowID = '0';
            var claimID = '';
            var rowIDs = '';   // RMA-5566      achouhan3       Added for Multiple selection for approval/Rejection
            if ($scope.selectedReserve[0].RSWROWID != null)
                rowID = $scope.selectedReserve[0].RSWROWID;
            else if ($scope.selectedReserve[0].RswRowId != null)
                rowID = $scope.selectedReserve[0].RswRowId;

            if ($scope.selectedReserve[0] != null && $scope.selectedReserve[0].ClaimID != null)
                claimID = $scope.selectedReserve[0].ClaimID;
            else if ($scope.selectedReserve[0] != null && $scope.selectedReserve[0].CLAIMID != null)
                claimID = $scope.selectedReserve[0].CLAIMID;
            else
                claimID = '0';

            //RMA-5566     achouhan3       Changed for multiple selection Starts
            for (var i = 0; i < $scope.selectedReserve.length; i++) {
                if ($scope.selectedReserve[i].RSWROWID != null)
                    rowIDs = rowIDs + $scope.selectedReserve[i].RSWROWID + ",";
                else if ($scope.selectedReserve[0].RswRowId != null)
                    rowIDs = rowIDs + $scope.selectedReserve[i].RswRowId + ",";
            }
            //RMA-5566     achouhan3       Changed for multiple selection Ends

            setTimeout(function () {
                imgLoading.css({ display: "block" });
                var p = {
                    isShowAllItem: $scope.reserveModel.isShowAllItems,
                    approveComment: strAppRejComment,
                    operationType: optType,
                    resvHistRowID: rowIDs, // RMA-5566      achouhan3       Updated for mutliple selection
                    ClaimID: claimID,
                    sortDirection: $scope.sortDetails.direction,
                    sortField: $scope.sortDetails.name,
                };
                $http({
                    url: "TransAsync.aspx?call=approveRejectReserve&Title=" + strQuery,
                    method: "POST",
                    data: Object.toparams(p),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    async: true
                }).success(function (data) {
                    $timeout(function () {
                        //RMA-5566  achouhan3   Handled Muliple Selection
                        if (data != null && data.ResponseData != null) {
                            //if(data.Success){
                            //$scope.ColDefs = data.ResponseData.ColumnDef;
                            //$scope.ColPosition = data.ResponseData.ColumnPosition;
                            //RMA-5566  achouhan3   Handled Muliple Selection

                            $scope.completeData = data.ResponseData.Data;
                            setFilter('');
                            if (data.Description != null && data.Description != "") {
                                data.Description = $scope.ShowMessage(data.Description);
                                $scope.ShowError(data);
                            }
                            //$scope.pagingOptions.pageSize = data.ResponseData.PageSize;
                            //if (data.ResponseData.SortField != null && data.ResponseData.SortField != '') {
                            //    $scope.sortOptions.fields[0] = data.ResponseData.SortField;
                            //    $scope.sortOptions.directions[0] = data.ResponseData.SortDirection;
                            //}
                            $scope.setPagingData($scope.completeData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, data.ResponseData.TotalCount);
                            //if ($('CallFor').val() == "No")
                            //{
                            //    alert("Reserves exceed your authority limits. The worksheet is being sent to your supervisor for approval.");
                            //    $('CallFor').val('');
                            //}

                        }
                        if (!data.Success) {
                            data.Description = $scope.ShowMessage(data.Description);
                            $scope.ShowError(data);
                        }
                        $scope.clearControl();
                        imgLoading.css({ display: "none" });
                        if (parent.MDISetUnDirty != null)//jira RMA-19518
                            parent.MDISetUnDirty(null, "0", false);
                    });
                }).error(function (error) {
                    imgLoading.css({ display: "none" });
                });
            }, 100);
        }
        else {
            var strOperation = '';
            var strMessage = '';
            if (optType == "1" && strQuery == "ReserveWorkSheet")
                strMessage = CreateReserveValidations.ValidApproveReserveWorksheet;
            else if (optType == "1")
                strMessage = CreateReserveValidations.ValidApproveReserve;

            else if (strQuery == "ReserveWorkSheet")
                strMessage = CreateReserveValidations.ValidRejectReserveWorksheet;
            else
                strMessage = CreateReserveValidations.ValidRejectReserve;

            alert(strMessage);
        }
    };

    $scope.SavePreferences = function () {
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        imgLoading.css({ display: "block" });
        var strQuery = $('#hdQuery').val();
        //  $scope.criteriaXML = document.getElementById("hdCriteriaXml").value;
        var p = {
            grdPageSize: $scope.pagingOptions.pageSize,
            columnObject: JSON.stringify($scope.PrefColumnDef),
            sortDirection: $scope.sortDetails.direction,
            sortField: $scope.sortDetails.name,
            sortFieldIndex: $scope.sortDetails.fields
        };
        $http({
            url: "TransAsync.aspx?call=SavePreferences&Title=" + strQuery,
            method: "POST",
            data: Object.toparams(p),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (data) {
            imgLoading.css({ display: "none" });
            if (data.Success) {
                //document.getElementById("hdUserPref").value = data.ResponseData.userPrefXML;
                $scope.ClearError();
            }
            else {
                $scope.ShowError(data);
            }
        }).error(function (error) {
            imgLoading.css({ display: "none" });
        });

    };

    $scope.ShowAllItems = function () {
        $scope.getReserveDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    // Added By Nitika For JIRA 8253 START
    //achouhan3    Modified to handle Multiselect Checkbox in case of My pending Reserve Starts
    $scope.ShowMyTrans = function () {
        var isShowMyTrans = $scope.reserveModel.isShowMyTrans;
        var strQuery = $('#hdQuery').val();
        var parent = $("#hdnParent").val();
        if (isShowMyTrans == true && parent != "MyWork") {
            parent = "MyWork";
            //RMA-11501     achouhan3   Modified to show Reserve Worksheet when clicked from Reserve Worksheet Screen
            window.location.href = "../ReserveApproval/TransAsync.aspx?Title=" + strQuery + "&Parent=MyWork&IsRedirect=true";
        }
        else {
            parent = "";
            //RMA-11501     achouhan3   Modified to show Reserve Worksheet when clicked from Reserve Worksheet Screen
            window.location.href = "../ReserveApproval/TransAsync.aspx?Title=" + strQuery + "&Parent=";
        }
        //$scope.getReserveDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        //if (isShowMyTrans == true) {
        //    $('#chkShowAllItem').hide();
        //    $('#divbottom').hide();
        //    $('#spnShowAllItem').hide();
        //    $('#btnReject').hide();
        //    $('#btnApprove').hide();

        //}
        //else {

        //    $('#chkShowAllItem').show();
        //    $('#divbottom').show();
        //    $('#spnShowAllItem').show();
        //    $('#btnReject').show();
        //    $('#btnApprove').show();

        //}
    }
    //achouhan3    Modified to handle Multiselect Checkbox in case of My pending Reserve Ends
    // Added By Nitika For JIRA 8253  END
    $scope.getReserveDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);


    $scope.ShowMessage = function (ErrorResults) {
        var result = null;
        var errorMessage = '';
        var strQuery = $('#hdQuery').val();
        //$scope.aErrorReserve = ErrorResults.split(',');

        var rowMsg = ErrorResults.split(',');
        //sachin 6385
        var rows = [];
        var msgs = [];
        for (var i = 0; i < rowMsg.length ; i++)
        {
            var o = rowMsg[i].split('~');
            if (o.length > 1) {
                rows.push(o[0]);
                msgs.push(o[1]);
            }
            else {
                rows.push(o[0]);
            }
        }
        //sachin 6385
        $scope.aErrorReserve = rows;

        if ($scope.aErrorReserve.length > 0) {

            for (var i = 0; i < $scope.aErrorReserve.length; i++) {
                if (isNaN($scope.aErrorReserve[i])) {
                    return errorMessage = ErrorResults;
                }
                if ($scope.aErrorReserve.length > 0 && $scope.selectedRow[0].RSWROWID != null) {
                    result = $filter('filter')($scope.selectedRow, { RSWROWID: $scope.aErrorReserve[i] })[0];
                }
                else if ($scope.aErrorReserve.length > 0 && $scope.selectedReserve[0].RswRowId != null) {
                    result = $filter('filter')($scope.selectedRow, { RswRowId: $scope.aErrorReserve[i] })[0];
                }
                if (result != null)
                    $scope.ErrorRows.push(result);
            }
        }
        else if (!isNaN(ErrorResults)) {
            if ($scope.selectedRow[0].RSWROWID != null)
                result = $filter('filter')($scope.selectedRow, { RSWROWID: ErrorResults })[0];
            else if ($scope.selectedReserve[0].RswRowId != null)
                result = $filter('filter')($scope.selectedRow, { RswRowId: ErrorResults })[0];
            if (result != null)
                $scope.ErrorRows.push(result);
        }
        else {
            errorMessage = ErrorResults;
        }
        if ($scope.ErrorRows.length > 0)
            errorMessage = errorMessage + CreateReserveLabels.lblReserveProcessed.format([$scope.aErrorReserve.length, $scope.selectedRow.length]) + "<br/>";
        for (var i = 0; i < $scope.ErrorRows.length; i++) {
            if (strQuery == "ReserveWorkSheet") {
                if($scope.ErrorRows[i].ClaimNumber!=null)
                    errorMessage = errorMessage + CreateReserveLabels.lblReserveWorkSheetError.format([$scope.ErrorRows[i].ClaimNumber, CreateReserveLabels.lblExpenseReserve, $scope.ErrorRows[i].ExpenseReserve, CreateReserveLabels.lblIndemnityReserve, $scope.ErrorRows[i].IndemnityReserve, CreateReserveLabels.lblRecoveryReserve, $scope.ErrorRows[i].RecoveryReserve, $scope.ErrorRows[i].SubmittedBy, $scope.ErrorRows[i].DateSubmitted, msgs[i]]) + "<br/>";//sachin 6385
                else
                    errorMessage = errorMessage + CreateReserveLabels.lblReserveWorkSheetError.format([$scope.ErrorRows[i].CLAIMNUMBER, CreateReserveLabels.lblExpenseReserve, $scope.ErrorRows[i].EXPENSERESERVE, CreateReserveLabels.lblIndemnityReserve, $scope.ErrorRows[i].INDEMNITYRESERVE, CreateReserveLabels.lblRecoveryReserve, $scope.ErrorRows[i].RECOVERYRESERVE, $scope.ErrorRows[i].SUBMITTEDBY, $scope.ErrorRows[i].DATESUBMITTED, msgs[i]]) + "<br/>";//sachin 6385
            }
            else
                //errorMessage = errorMessage  + CreateReserveLabels.lblReserveError.format([$scope.ErrorRows[i].CLAIM_NUMBER, $scope.ErrorRows[i].RESERVE_TYPE_CODE, $scope.ErrorRows[i].RESERVE_AMOUNT, $scope.ErrorRows[i].SUBMITTEDBY, $scope.ErrorRows[i].DATESUBMITTED], $scope.ErrorRows[i].HOLDREASON) + "<br/>";
                errorMessage = errorMessage + CreateReserveLabels.lblReserveError.format([$scope.ErrorRows[i].CLAIM_NUMBER, $scope.ErrorRows[i].RESERVE_TYPE_CODE, $scope.ErrorRows[i].RESERVE_AMOUNT, $scope.ErrorRows[i].SUBMITTEDBY, $scope.ErrorRows[i].DATESUBMITTED,msgs[i]])  +  "<br/>";//sachin 6385
        }
        return errorMessage;
    }

    //var stringformat = function () {
    //    // The string containing the format items (e.g. "{0}")
    //    // will and always has to be the first argument.
    //    var theString = arguments[0];

    //    // start with the second argument (i = 1)
    //    for (var i = 1; i < arguments.length; i++) {
    //        // "gm" = RegEx options for Global search (more than one instance)
    //        // and for Multiline search
    //        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
    //        theString = theString.replace(regEx, arguments[i]);
    //    }

    //    return theString;
    //};
    String.prototype.format = function (args) {
        var str = this;
        return str.replace(String.prototype.format.regex, function (item) {
            var intVal = parseInt(item.substring(1, item.length - 1));
            var replace;
            if (intVal >= 0) {
                replace = args[intVal];
            } else if (intVal === -1) {
                replace = "{";
            } else if (intVal === -2) {
                replace = "}";
            } else {
                replace = "";
            }
            return replace;
        });
    };
    String.prototype.format.regex = new RegExp("{-?[0-9]+}", "g");

    $scope.toggleSelection = function () {

        //RMA-10284 Added to retain Multi selection Starts
        var result = null;
        var selectAllHeader = angular.element(".ngSelectionHeader").scope();

        var rows = $scope.gridOptions.$gridScope.renderedRows, allChecked = true, IsAllSelected = false;
        for (var r = 0; r < rows.length; r++) {
            if (rows[r].entity.RSWROWID != null) {
                result = $filter('filter')($scope.selectedRow, { RSWROWID: rows[r].entity.RSWROWID })[0];
            }
            else if (rows[r].entity.RswRowId != null) {
                result = $filter('filter')($scope.selectedRow, { RswRowId: rows[r].entity.RswRowId })[0];
            }
            if (result == null) {
                allChecked = false;
                break;
            }
        }

        if (selectAllHeader != null && selectAllHeader) {
            selectAllHeader.allSelected = allChecked;
            IsAllSelected = allChecked;
            //RMA-10715 achouhan3   Added to select header checkbox when Mutliselect is on while scrolling starts
            if ($('.ngSelectionHeader')[0] != null && $('.ngSelectionHeader')[0].checked != null) {
                if (IsAllSelected) {
                    $('.ngSelectionHeader')[0].checked = true;
                }
                else {
                    $('.ngSelectionHeader')[0].checked = false;
                }
            }
            //RMA-10715 achouhan3   Added to select header checkbox when Mutliselect is on while scrolling Ends

        }
        //RMA-10284 Added to retain Multi selection Ends
    }

    $scope.ExportToExcel = function () {
        redirectE2E($scope.gridName);
    };
});