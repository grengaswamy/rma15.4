﻿//******************************************************************************************************************************************
//*   Date     | JIRA/Issue | Programmer | Description                                                                                        *
//******************************************************************************************************************************************
//* 01/06/2015 | RMA4307    | vchouhan6   | Generic Export to Excel for visible columns, independent of size, order of columns and UI filters.
//* 01/28/2015 | RMA6405    | vchouhan6   | Convert Activity Log screen to ng Grid and provide Export to Excel feature
//* 02/20/2015 | RMA-8068   | vchouhan6   | showing error message when No records found in Claim Activity Log
//* 02/20/2015 | RMA-8115   | vchouhan6   | restrict user to enter page size to max 9 digits on page size
//******************************************************************************************************************************************//
var app = angular.module('rmaAppCAL', ['ngGrid']);
app.controller('ClaimActLogController', function ($scope, $http, $timeout) {
    var layoutPlugin = new ngGridLayoutPlugin();
    $scope.completeData = [];
    $scope.AllData = [];
    $scope.selectedRow = [];
    $scope.sortDetails = { fields: '', direction: '', name: '' };
    $scope.PrefColumnDef = [];
    $scope.ColDefs = [];
    $scope.ColPosition = [];
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: 0,
        currentPage: 1
    };
    //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality starts 
    $scope.pageName = "AsyncClaimActLog.aspx"; 
    $scope.GridId = "divActLogGrid";
    $scope.gridName = "ClaimActivityLog"; 
    //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality ends
    $scope.updateLayout = function () {
        layoutPlugin.updateGridLayout();
    };

    Object.toparams = function ObjecttoParams(obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + obj[key]);
        }
        return p.join('&');
    };


    var filterBarPlugin = {
        init: function (scope, grid) {
            filterBarPlugin.scope = scope;
            filterBarPlugin.grid = grid;
            $scope.$watch(function () {
                var searchQuery = "";
                angular.forEach(filterBarPlugin.scope.columns, function (col) {
                    if (col.visible && col.filterText) {
                        var filterText = col.filterText + '; ';
                        searchQuery += col.displayName + ": " + filterText;
                        //var filterText = (col.filterText.indexOf('*') === 0 ? col.filterText.replace('*', '') : "^" + col.filterText) + ";";
                        //searchQuery += col.displayName + ": " + filterText;
                    }
                });
                return searchQuery;
            }, function (searchQuery) {
                filterBarPlugin.scope.$parent.filterText = searchQuery;
                filterBarPlugin.grid.searchProvider.evalFilter();
            });
        },
        scope: undefined,
        grid: undefined
    };

    $scope.setPagingData = function (data, page, pageSize, totalLength) {
        //RMA-7571  achuhan3 for rebinding of grid starts
        $scope.totalServerItems = totalLength;
        //RMA-7571  achuhan3 for rebinding of grid ends
        if (totalLength == null || totalLength <= 0) {
            $('#divActLogGrid').hide();
            $('#divImage').hide();
        }
        else {
            $('#divActLogGrid').show();
            $('#divImage').show();
            var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
            $scope.AllData = pagedData;
                if (!$scope.$$phase)
                {
                        $scope.$apply();
                }
            }
        
    };

    $scope.gridOptions = {
        data: 'AllData',

        columnDefs: 'ColDefs',
        enablePaging: true,
        showFooter: true,

        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        // useExternalSorting: true,
        // CUSTOMIZATION by vchouhan6 for JIRA-RMA8115: restrict user to enter page size to max 9 digits on both page size 
        footerTemplate: '<div ng-show="showFooter" class="ngFooterPanel" ng-class="{\'ui-widget-content\': jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}" ng-style="footerStyle()"><div class="ngTotalSelectContainer"><div class="ngFooterTotalItems" ng-class="{\'ngNoMultiSelect\': !multiSelect}" ><span class="ngLabel"> Total Items {{maxRows()}}</span><span ng-show="filterText.length > 0" class="ngLabel">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span></div><div class="ngFooterSelectedItems" ng-show="multiSelect"><span class="ngLabel">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}</span></div></div><div class="ngPagerContainer" style="float: right; margin-top: 10px;" ng-show="enablePaging" ng-class="{\'ngNoMultiSelect\': !multiSelect}"><div style="float:left; margin-right: 10px;" class="ngRowCountPicker"><span style="float: left; margin-top: 4px; margin-right: 5px;" class="ngLabel">PageSize </span><input type="text" maxlength = "9" min="1" max="{{maxPageSize()}}" id="txtPageSize" ng-click="stopClickProp($event)" ng-model="pagingOptions.pageSize" style="float: left;height: 20px; width: 100px"/></div><div style="float:left; margin-right: 10px; line-height:25px;" class="ngPagerControl" style="float: left; min-width: 135px;"><button class="ngPagerButton" ng-click="pageToFirst()" ng-disabled="cantPageBackward()" title="First Page"><div class="ngPagerFirstTriangle"><div class="ngPagerFirstBar"></div></div></button><button class="ngPagerButton" ng-click="pageBackward()" ng-disabled="cantPageBackward()" title="Previous Page"><div class="ngPagerFirstTriangle ngPagerPrevTriangle"></div></button><input id="txtPageNumber" class="ngPagerCurrent" min="1" max="{{maxPages()}}" type="number"  style="width:50px; height: 24px; margin-top: 1px; padding: 0 4px;" ng-model="pagingOptions.currentPage"/><button class="ngPagerButton" ng-click="pageForward()" ng-disabled="cantPageForward()" title="Next Page"><div class="ngPagerLastTriangle ngPagerNextTriangle"></div></button><button class="ngPagerButton" ng-click="pageToLast()" ng-disabled="cantPageToLast()" title="Last Page"><div class="ngPagerLastTriangle"><div class="ngPagerLastBar"></div></div></button></div> </div></div>',
        //END CUSTOMIZATION 
        showFilter: false,
        enableColumnResize: true,
        jqueryUIDraggable: true,
        multiSelect: false,
        //plugins: [filterBarPlugin],
        headerRowHeight: 60,
        // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: Adding Export to excel plugin
        //plugins: [filterBarPlugin, new ngGridCsvExportPlugin()],
        //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality starts 
        plugins: [filterBarPlugin, new ngGridExportPlugin()],
        //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality ends
        multiRowSelection: false,
        enableRowSelection: true
        //END CUSTOMIZATION    
    };

    $scope.ShowError = function (erorData) {
        var errorCtrl = $("#ErrorControl1_lblError");
        errorCtrl.html('<font class="errortext">Following Errors have been reported:</font><table cellspacing="0" cellpadding="0" border="0" class="errortext"><tbody><tr><td valign="top"><img src="/RiskmasterUI/Images/error-large.gif"></td><td valign="top" style="padding-left: 1em"><ul><li class="warntext">' + erorData.Description + '</li></ul></td></tr></tbody></table>');
    };
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
       // var regNumeric = /^[\d ]*$/;
        //   /^\d+$/
        $("#txtPageNumber").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                return false;
            }
        });
        $("#txtPageSize").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                return false;
            }
        });
        if (newVal !== oldVal && (newVal.currentPage !== oldVal.currentPage || newVal.pageSize != oldVal.pageSize)) {
                    $scope.setPagingData($scope.completeData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, $scope.totalServerItems);
                    }
    }, true);

    $scope.$watch('sortDetails', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.pagingOptions.currentPage = 1;
            $scope.getClaimActLogPageDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);

    $scope.$on('ngGridEventColumns', function (event, newColumns) {
        $scope.PrefColumnDef = newColumns;
    });


    $scope.$on('ngGridEventSorted', function (args, sortInfo) {
        var iCount = 1;
        $scope.sortDetails.name = sortInfo.fields[0];
        $scope.sortDetails.direction = sortInfo.directions[0];
        //for (var i = 0; i < $scope.ColPosition.length; i++) {
        //    if ($scope.ColPosition[i].visible) {
        //        if ($scope.ColPosition[i] != null && $scope.ColPosition[i].field == $scope.sortDetails.name) {
        //            $scope.sortDetails.fields = iCount;
        //            break;
        //        }
        //        iCount++;
        //    }
        //}
    });

    $scope.getClaimActLogPageDataAsync = function (pageSize, page) {
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        var strFormName = $('#hdnFormName').val();
        var strClaimId = $('#hdnClaimId').val();
        var errorCtrl = $("#ErrorControl1_lblError");
        setTimeout(function () {
            imgLoading.css({ display: "block" });

            var p = {
                pageNumber: page,
                pageSize: pageSize,
                totalRecords: $scope.totalServerItems,
                IsPageindex: $scope.isPageIndex,
                sortDirection: $scope.sortDetails.direction,
                sortField: $scope.sortDetails.name,
            };
            $http({
                url: "AsyncClaimActLog.aspx?call=getdata&ClaimId=" + strClaimId + "&SysFormName=" + strFormName,
                method: "POST",
                data: Object.toparams(p),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                async: true
            }).success(function (data) {
                $timeout(function () {
                    // CUSTOMIZATION by vchouhan6 for JIRA-RMA8068 Start: error message when No records found in Claim Activity Log
                    if (data.ResponseData.Data == null || !$.trim(data) || data == undefined || data.ResponseData.Data.TotalCount > 0 || data.length == 0)
                    {
                        $('#lblEmptyData').show();
                        $('#lblEmptyData').html("<br /><br />"+ parent.CommonValidations.lblEmptyData);
                        
                    }
                    else {
                        $('#lblEmptyData').hide();
                    }
                    // END CUSTOMIZATION
                    if (data.Success) {
                        $scope.ColDefs = data.ResponseData.ColumnDef;
                        $scope.ColPosition = data.ResponseData.ColumnPosition;
                        $scope.completeData = data.ResponseData.Data;
                         
                        $scope.pagingOptions.pageSize = data.ResponseData.PageSize;
                        $scope.setPagingData($scope.completeData, $scope.pagingOptions.currentPage, $scope.pagingOptions.pageSize, data.ResponseData.TotalCount);
                    }
                    else {
                        $scope.ShowError(data);
                    }
                    imgLoading.css({ display: "none" });
                });
            }).error(function (error) {
                imgLoading.css({ display: "none" });
            });

        }, 100);
    };

    //$scope.ChangePageSize = function () {
    //    if ($scope.pagingOptions.pageSize != '' && $scope.pagingOptions.pageSize != 0) {
    //        $scope.IsCall = true;
    //        $scope.isPageIndex = false;
    //        $scope.pagingOptions.currentPage = 1;
    //        $scope.getSearchPageDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    //    }
    //};

    $scope.SavePreferences = function () {
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        imgLoading.css({ display: "block" });
        var strFormName = $('#hdnFormName').val();
        var strClaimId = $('#hdnClaimId').val();
        var p = {
            grdPageSize: $scope.pagingOptions.pageSize,
            columnObject: JSON.stringify($scope.PrefColumnDef),
            sortDirection: $scope.sortDetails.direction,
            sortField: $scope.sortDetails.name,
            sortFieldIndex: $scope.sortDetails.fields
        };
        $http({
            url: "AsyncClaimActLog.aspx?call=SavePreferences&ClaimId=" + strClaimId + "&SysFormName=" + strFormName,
            method: "POST",
            data: Object.toparams(p),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (data) {
            imgLoading.css({ display: "none" });
            if (data.Success) {
                //document.getElementById("hdUserPref").value = data.ResponseData.userPrefXML;
            }
            else {
                $scope.ShowError(data);
            }
        }).error(function (error) {
            imgLoading.css({ display: "none" });
        });

    };

    $scope.getClaimActLogPageDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    $scope.ExportToExcel = function () {
        redirectE2E($scope.gridName);
    };
});