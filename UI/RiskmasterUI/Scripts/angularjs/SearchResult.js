﻿//******************************************************************************************************************************************
//*   Date     |  JIRA   | Programmer | Description                                                                                        *
//******************************************************************************************************************************************
//* 01/06/2015 | RMA4307  | vchouhan6   | Generic Export to Excel for visible columns, independent of size, order of columns and UI filters.
//* 02/10/2015 | RMA4307  | achouhan3   | RMA-7158 Look Up Issue fix
//******************************************************************************************************************************************//
var app = angular.module('rmaApp', ['ngGrid']);
app.controller('searchController', function ($scope, $http, $timeout) {
    var layoutPlugin = new ngGridLayoutPlugin();
    $scope.criteriaXML = '';
    $scope.viewName = '';
    $scope.filterOptions = {
        filterText: "",
        filterColIndex : "",
        useExternalFilter: false
    };
    $scope.AllData = [];
    $scope.sortDetails = { fields : '',direction:'',name : ''};
    $scope.myData = [];
    $scope.ColDefs = [];
    $scope.ColPosition = [];
    $scope.totalServerItems = 0;
    $scope.IsCall = false;
    $scope.PrefColumnDef = [];
    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: '',
        currentPage: 1
    };
    $scope.filteredList = [];
    $scope.mySelections = [];
    $scope.sortColIndex = 0;
    $scope.isPageIndex = false;
    
    $scope.updateLayout = function () {
        layoutPlugin.updateGridLayout();
    };
    //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality starts
    $scope.pageName = "AsyncSearchResult.aspx";
    $scope.GridId = "SearchResultGrid";   
    var hdnEEformname = $('#hdnEEFormName').val();
    $scope.gridName = hdnEEformname + "_Search";
    //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality ends
    $scope.setPagingData = function (data, page, pageSize, totalLength, searchCategory) {
        if (totalLength == null || totalLength <= 0) {
            $('#divImage').hide();
            $('#ng-app').hide();
            if ($('#hdScreenFlag').val() == '2' || $('#hdScreenFlag').val() == '3') {
                $('#divReturnSearch').show();
                $('#tblNoRecord').hide();
            }
           else {
                $('#tblNoRecord').show();
                $('#divReturnSearch').hide();
            }
        }
        else {
            $('#divImage').show();
            $('#ng-app').show();
            $('#ng-app').css({ display: "" });
            $('#tblNoRecord').hide();
            $('#divReturnSearch').hide();
            // var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
            if (page == 1)
                $scope.totalServerItems = totalLength;
            $scope.myData = angular.fromJson(data);
            $('#searchcat').val(searchCategory);
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }
    };

    Object.toparams = function ObjecttoParams(obj) {
        var p = [];
        for (var key in obj) {
            p.push(key + '=' + obj[key]);
        }
        return p.join('&');
    };

    var filterBarPlugin = {
        init: function (scope, grid) {            
            filterBarPlugin.scope = scope;
            filterBarPlugin.grid = grid;
            $scope.$watch(function () {
                var searchQuery = "";
                angular.forEach(filterBarPlugin.scope.columns, function (col) {
                    if (col.visible && col.filterText) {
                        var filterText = col.filterText + '; ';
                        searchQuery += col.displayName + ": " + filterText;
                        //var filterText = (col.filterText.indexOf('*') === 0 ? col.filterText.replace('*', '') : "^" + col.filterText) + ";";
                        //searchQuery += col.displayName + ": " + filterText;
                    }
                });
                return searchQuery;
            }, function (searchQuery) {
                filterBarPlugin.scope.$parent.filterText = searchQuery;
                filterBarPlugin.grid.searchProvider.evalFilter();
            });
        },
        scope: undefined,
        grid: undefined
    };


    $scope.gridOptions = {
        data: 'myData',
        //plugins: [layoutPlugin],
        columnDefs: 'ColDefs',
        enablePaging: true,     
        //showFooter: true,
        footerTemplate: '<div ng-show="showFooter" class="ngFooterPanel" ng-class="{\'ui-widget-content\': jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}" ng-style="footerStyle()"><div class="ngTotalSelectContainer"><div class="ngFooterTotalItems" ng-class="{\'ngNoMultiSelect\': !multiSelect}" ><span class="ngLabel">' + CreateAsyncSearchLabel.lblTotalItems + ' {{maxRows()}}</span><span ng-show="filterText.length > 0" class="ngLabel">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})</span></div><div class="ngFooterSelectedItems" ng-show="multiSelect"><span class="ngLabel">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}</span></div></div><div class="ngPagerContainer" style="float: right; margin-top: 10px;" ng-show="enablePaging" ng-class="{\'ngNoMultiSelect\': !multiSelect}"><div style="float:left; margin-right: 10px;" class="ngRowCountPicker"><span style="float: left; margin-top: 6px; margin-right: 5px;" class="ngLabel">' + CreateAsyncSearchLabel.lblPageSize + '</span><input type="text" maxlength = "9" ng-click="stopClickProp($event)" ng-model="pagingOptions.pageSize" style="float: left;height: 20px; width: 40px"/> <a class=\"k-grid-filter\" href=\"#\" tabindex=\"-1\" ng-click=\"ChangePageSize()\" title="' + CreateAsyncSearchTT.ttChangePageSize + '"><span class=\"k-icon k-filter\"></span></a></div><div style="float:left; margin-right: 10px; line-height:25px;" class="ngPagerControl" style="float: left; min-width: 135px;"><button class="ngPagerButton" ng-click="pageToFirst()" ng-disabled="cantPageBackward()" title="' + CreateAsyncSearchTT.ttFirstPage + '"><div class="ngPagerFirstTriangle"><div class="ngPagerFirstBar"></div></div></button><button class="ngPagerButton" ng-click="pageBackward()" ng-disabled="cantPageBackward()" title="' + CreateAsyncSearchTT.ttPreviousPage + '"><div class="ngPagerFirstTriangle ngPagerPrevTriangle"></div></button><input class="ngPagerCurrent" min="1" max="{{maxPages()}}" type="number" style="width:70px; height: 24px; margin-top: 1px; padding: 0 4px;" ng-model="pagingOptions.currentPage"/><button class="ngPagerButton" ng-click="pageForward()" ng-disabled="cantPageForward()" title="' + CreateAsyncSearchTT.ttNextPage + '"><div class="ngPagerLastTriangle ngPagerNextTriangle"></div></button><button class="ngPagerButton" ng-click="pageToLast()" ng-disabled="cantPageToLast()" title="' + CreateAsyncSearchTT.ttLastPage + '"><div class="ngPagerLastTriangle"><div class="ngPagerLastBar"></div></div></button></div> </div></div>',
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        showFilter: false,
        //enableColumnReordering: true,
        enableColumnResize: true,
        jqueryUIDraggable: true,
        multiSelect: false,
        useExternalSorting: true,
        headerRowHeight: 60,
        // CUSTOMIZATION by vchouhan6 for JIRA-RMA4307: Adding Export to excel plugin
        //plugins: [filterBarPlugin, new ngGridCsvExportPlugin()], //dnehe commented
        //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality starts 
        plugins: [filterBarPlugin, new ngGridExportPlugin()], 
        //RMA-4307	achouhan3	Added for incorporation of Export to Excel functionality ends
        showFooter: true,
        multiRowSelection: false,
        enableRowSelection: true
        //END CUSTOMIZATION    
	};

    $scope.getSearchPageDataAsync = function (pageSize, page) {
       
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        var hdScreenFlag = $('#hdScreenFlag').val();    //RMA-7158      achouhan3       Entity Look Up blank screen issue
        setTimeout(function () {
            imgLoading.css({ display: "block" });
            $scope.criteriaXML = document.getElementById("hdCriteriaXml").value;
            $scope.UserPrefXML = document.getElementById("hdUserPref").value;
            var p = {
                pageNumber: page,
                pageSize: pageSize,
                screenFlag: hdScreenFlag,           ////RMA-7158      achouhan3       Entity Look Up blank screen issue starts
                totalRecords: $scope.totalServerItems,
                sortDirection: $scope.sortDetails.direction,
                sortField: $scope.sortDetails.fields,
                IsPageindex: $scope.isPageIndex,
                UserPref: $scope.UserPrefXML,
                criteriaXML: $scope.criteriaXML
            };
            $http({
                url: "AsyncSearchResult.aspx?call=getdata",
                method: "POST",
                data: Object.toparams(p),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                async:true
            }).success(function (data) {
                $timeout(function () {
                    if (data.Success) {                        
                        $scope.ColDefs = data.ResponseData.ColumnDef;
                        $scope.ColPosition = data.ResponseData.ColumnPosition;
                        $scope.AllData = data.ResponseData.Data;
                        $scope.pagingOptions.pageSize = data.ResponseData.PageSize;
                        $('#admtable').val(data.ResponseData.admtable); //RMA-11641- msampathkuma
                        $scope.setPagingData($scope.AllData, page, data.ResponseData.PageSize, data.ResponseData.TotalCount, data.ResponseData.SearchCat);
                    }
                    else {
                        ShowError(data);
                    }
                    imgLoading.css({ display: "none" });
                });
            }).error(function (error) {
                imgLoading.css({ display: "none" });
            });
            $scope.IsCall = false;
        }, 100);
    };


    $scope.getSearchPageDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    //$scope.$watch('filterOptions', function (newVal, oldVal) {
    //    if (newVal !== oldVal) {
    //        $scope.getSearchPageDataAsync($scope.pagingOptions.pageSize, 1);
    //       }
    //    }, true);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage && !$scope.IsCall) {
            $scope.getSearchPageDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
        //else if (newVal !== oldVal && parseInt(newVal.pageSize) !== parseInt(oldVal.pageSize) && !$scope.firstCall) {
        //    $scope.isPageIndex = false;
        //    $scope.firstCall = false;
        //    $scope.getSearchPageDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        //}
        
    }, true);

    $scope.$watch('sortDetails', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.IsCall = true;
            $scope.pagingOptions.currentPage = 1;
            $scope.getSearchPageDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);

    $scope.$on('ngGridEventColumns', function (event, newColumns) {
        //angular.forEach(newColumns, function (column, index) {
        //    //Need to removal the empty column / fix the group bug.
        //    if (column.field !== '' && typeof column.field !== 'undefined' && $scope.sortDetails.name != '' && $scope.sortDetails.name == column.field) {
        //        column.sortDirection = $scope.sortDetails.direction;                
        //    }
        //});
        $scope.PrefColumnDef = newColumns;
    });

    $scope.$on('ngGridEventSorted', function (args, sortInfo) {
        var iCount = 1;
        $scope.sortDetails.name = sortInfo.fields[0];
        $scope.sortDetails.direction = sortInfo.directions[0];
        for (var i = 0; i < $scope.ColPosition.length; i++) {
            if ($scope.ColPosition[i].visible) {
                if ($scope.ColPosition[i] != null && $scope.ColPosition[i].field == $scope.sortDetails.name) {
                    $scope.sortDetails.fields = iCount;
                    break;
                }
                iCount++;
            }
        }
    });

    
    $scope.ChangePageSize = function () {
        if ($scope.pagingOptions.pageSize != '' && $scope.pagingOptions.pageSize != 0) {
            $scope.IsCall = true;
            $scope.isPageIndex = false;
            $scope.pagingOptions.currentPage = 1;
            $scope.getSearchPageDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    };

    $scope.ShowError = function (erorData) {
        var errorCtrl = $("#ErrorControl1_lblError");
        errorCtrl.html('<font class="errortext">Following Errors have been reported:</font><table cellspacing="0" cellpadding="0" border="0" class="errortext"><tbody><tr><td valign="top"><img src="/RiskmasterUI/Images/error-large.gif"></td><td valign="top" style="padding-left: 1em"><ul><li class="warntext">' + data.Description + '</li></ul></td></tr></tbody></table>');
    };

    $scope.SavePreferences = function () {
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        var strSearch = $('#searchcat').val();
        imgLoading.css({ display: "block" });
        $scope.criteriaXML = document.getElementById("hdCriteriaXml").value;
        
        var p = {
            grdPageSize: $scope.pagingOptions.pageSize,
            searchCat: strSearch,            
            columnObject: JSON.stringify($scope.PrefColumnDef),
            sortDirection: $scope.sortDetails.direction,
            sortField: $scope.sortDetails.name,
            sortFieldIndex: $scope.sortDetails.fields,
            criteriaXML: $scope.criteriaXML
        };
        $http({
            url: "AsyncSearchResult.aspx?call=SavePreferences",
            method: "POST",
            data: Object.toparams(p),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (data) {
            imgLoading.css({ display: "none" });
            if (data.Success) {
                document.getElementById("hdUserPref").value = data.ResponseData.userPrefXML;
            }
            else {
                ShowError(data);
            }
        }).error(function (error) {
            imgLoading.css({ display: "none" });
        });

    };
    $scope.ExportToExcel = function () {
        redirectE2E($scope.gridName);
    };
});

