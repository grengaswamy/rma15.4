﻿app.controller('ReserveListingController', function ($scope, $http, $attrs) {
    //var layoutPlugin = new ngGridLayoutPlugin();

    //$scope.completeData = [];
    $scope.sortOptions = { fields: [], directions: [] };
    //$scope.ColDefs = [];
    //$scope.isAllItem = false;
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: '',
        currentPage: 1
    };
    $scope.pageName = "ReserveListingBOB.aspx"; //this is mendatory to be defined
    //PostInputDataForGrid - this variable is must in each controller. grid directive will use this array to pass input parameters used to bind the grid data
    var imgLoading = $("#pleaseWaitFrame", parent.document.body);
    //rma 16494 starts
    if (imgLoading != null)
        imgLoading.css({ display: "none" });
    //rma 16494 ends
    $scope.InputDataForGrid =
    {
       
        ClaimID: document.getElementById("claimid").value,
        CurrencyType: document.getElementById("Selected").value,
        ClaimantEID: document.getElementById("ClaimantListClaimantEID").value,//RMA-8490
        GridId: "gridReserveListing", //this is mendatory for each grid
      
    };
    
    //start: get settings 
    $scope.GetAdditionalData = function () {
        try {
            var additionalData = document.getElementById("hdnJsonAdditionalData");
            if (additionalData != null) {
                var additionalDataJsonString = additionalData.value;
                var jsonAdditionalData = JSON.parse(additionalDataJsonString);
                document.getElementById('txtUseMulticurrency').value = jsonAdditionalData[0].Multicurrency;                
            }
        }
        catch (e) {
        }
    };
    $scope.lssCheckBoxchanged = function (rowIndex,lssCheckBoxID) {
        if ($scope.gridOptions != null && $scope.gridOptions !== 'undefined')
            gridRows = $scope.gridOptions.$gridScope.renderedRows;
        var lsscheckBoxId = "lssCheckBox" + rowIndex;
        var lssCheckBox = document.getElementById(lsscheckBoxId);
        var lssCheckBoxValue = "0"; //-1 for checked, 0 for unchecked, 11 for undefined checkbox //-1 for undefined checkbox, 0 for unchecked, 1 for checked
        var lssExportToExcelValue = "";//rma 16828
        if (lssCheckBox == null || lssCheckBox == undefined) {
            lssCheckBoxValue = "1";
            lssExportToExcelValue = "";//rma 16828
        }
        else {
            if (lssCheckBox.checked) {
                lssCheckBoxValue = "-1";
                lssExportToExcelValue = "True";//rma 16828
            }
            else {
                lssCheckBoxValue = "0";
                lssExportToExcelValue = "False";//rma 16828
            }
        }
        if (gridRows != null) {
            gridRows[rowIndex].entity["LssExport.DbValue"] = lssCheckBoxValue;
            gridRows[rowIndex].entity["LssExport.ExportToExcelValue"] = lssExportToExcelValue;//rma 16828           
        }
    }
    $scope.GetAdditionalData();
    //end:get settings 
    //rma 16780 starts
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        var lsscheckBoxId = "";
        var lssExportValue = "";
        var lssCheckBox;
        if ($scope.gridOptions != null && $scope.gridOptions !== 'undefined')
            gridRows = $scope.gridOptions.$gridScope.renderedRows;
        if (gridRows != null) {
            for (var rowIndex = 0; rowIndex < gridRows.length; rowIndex++) {
                lsscheckBoxId = "lssCheckBox" + rowIndex;
                lssCheckBox = document.getElementById(lsscheckBoxId);
                lssExportValue = gridRows[rowIndex].entity["LssExport.DbValue"];
                if (lssCheckBox != null) {
                    if (lssExportValue == "0")
                        lssCheckBox.checked = false;
                    else if (lssExportValue == "-1")
                        lssCheckBox.checked = true;
                    //-1 for checked, 0 for unchecked, 11 for undefined checkbox //-1 for undefined checkbox, 0 for unchecked, 1 for checked
                }
            }
        }
    });
    //rma 16780 starts
    
      
    $("#drdCurrencytype").change(function () {
        //rma 16494 starts
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        if ($scope.additionalData[0].ClaimCurrCode == $scope.additionalData[0].BaseCurrCode) {
            imgLoading.css({ display: "none" });

        }
        else
            imgLoading.css({ display: "block" });
        //rma 16494 ends
        $scope.InputDataForGrid.CurrencyType = $("#drdCurrencytype option:selected").val();
         
        if (document.getElementById("Selected") != null)
            document.getElementById("Selected").value = $("#drdCurrencytype option:selected").val();
        return true;
    });
   
    $scope.$watch('mySelections', function (newVal, oldVal) {
        if ($scope.mySelections != undefined) {
            if ($scope.mySelections.length > 0 && $scope.mySelections[0].reserveid != null) {
                if (document.getElementById("hdnreserveid") != null)
                    document.getElementById("hdnreserveid").value = $scope.mySelections[0].reserveid;
            }
            else if (document.getElementById("hdnreserveid") != null)
                document.getElementById("hdnreserveid").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].reserveid != null) {
                if (document.getElementById("rc_row_id") != null)
                    document.getElementById("rc_row_id").value = $scope.mySelections[0].reserveid;
            }
            else if (document.getElementById("rc_row_id") != null)
                document.getElementById("rc_row_id").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].ClaimantEid != null) {
                if (document.getElementById("claimanteid") != null)
                    document.getElementById("claimanteid").value = $scope.mySelections[0].ClaimantEid;
            }
            else if (document.getElementById("claimanteid") != null)
                document.getElementById("claimanteid").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].policyid != null) {
                if (document.getElementById("policyid") != null)
                    document.getElementById("policyid").value = $scope.mySelections[0].policyid;
            }
            else if (document.getElementById("policyid") != null)
                document.getElementById("policyid").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].polcvgrowid != null) {
                if (document.getElementById("polcvgid") != null)
                    document.getElementById("polcvgid").value = $scope.mySelections[0].polcvgrowid;
            }
            else if (document.getElementById("polcvgid") != null)
                document.getElementById("polcvgid").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].policyunitrowid != null) {
                if (document.getElementById("txtUnitID") != null)
                    document.getElementById("txtUnitID").value = $scope.mySelections[0].policyunitrowid;
            }
            else if (document.getElementById("txtUnitID") != null)
                document.getElementById("txtUnitID").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].polcvglossrowid != null) {
                if (document.getElementById("cvglossid") != null)
                    document.getElementById("cvglossid").value = $scope.mySelections[0].polcvglossrowid;
            }
            else if (document.getElementById("cvglossid") != null)
                document.getElementById("cvglossid").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].reservetypecode != null) {
                if (document.getElementById("Reserve") != null)
                    document.getElementById("Reserve").value = $scope.mySelections[0].reservetypecode;
            }
            else if (document.getElementById("Reserve") != null)
                document.getElementById("Reserve").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].claimantname != null) {
                if (document.getElementById("txtClaimants") != null)
                    document.getElementById("txtClaimants").value = $scope.mySelections[0].claimantname;
            }
            else if (document.getElementById("txtClaimants") != null)
                document.getElementById("txtClaimants").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].reservetype != null) {
                if (document.getElementById("MasResType") != null)
                    document.getElementById("MasResType").value = $scope.mySelections[0].reservetype;
            }
            else if (document.getElementById("MasResType") != null)
                document.getElementById("MasResType").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].reservename != null) {
                if (document.getElementById("ResType") != null)
                    document.getElementById("ResType").value = $scope.mySelections[0].reservename;
            }
            else if (document.getElementById("ResType") != null)
                document.getElementById("ResType").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].preventcollonres != null) {
                if (document.getElementById("PreventCollOnRes") != null)
                    document.getElementById("PreventCollOnRes").value = $scope.mySelections[0].preventcollonres;
            }
            else if (document.getElementById("PreventCollOnRes") != null)
                document.getElementById("PreventCollOnRes").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].RsvStatusParent != null) {
                if (document.getElementById("ResStatusParent") != null)
                    document.getElementById("ResStatusParent").value = $scope.mySelections[0].RsvStatusParent;
            }
            else if (document.getElementById("ResStatusParent") != null)
                document.getElementById("ResStatusParent").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].transseqnum != null) {
                if (document.getElementById("TransSeqNum") != null)
                    document.getElementById("TransSeqNum").value = $scope.mySelections[0].transseqnum;
            }
            else if (document.getElementById("TransSeqNum") != null)
                document.getElementById("TransSeqNum").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].covgseqnum != null) {
                if (document.getElementById("CovgSeqNum") != null)
                    document.getElementById("CovgSeqNum").value = $scope.mySelections[0].covgseqnum;
            }
            else if (document.getElementById("CovgSeqNum") != null)
                document.getElementById("CovgSeqNum").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].CoverageKey != null) {
                if (document.getElementById("CoverageKey") != null)
                    document.getElementById("CoverageKey").value = $scope.mySelections[0].CoverageKey;
            }
            else if (document.getElementById("CoverageKey") != null)
                document.getElementById("CoverageKey").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].reservestatusdesc != null) {
                if (document.getElementById("Stauts") != null)
                    document.getElementById("Stauts").value = $scope.mySelections[0].reservestatusdesc;
            }
            else if (document.getElementById("Stauts") != null)
                document.getElementById("Stauts").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].unit != null) {
                if (document.getElementById("txtUnitText") != null)
                    document.getElementById("txtUnitText").value = $scope.mySelections[0].unit;
                if (document.getElementById("Unit") != null)
                    document.getElementById("Unit").value = $scope.mySelections[0].unit;
                if (document.getElementById("lblUnit") != null)
                    document.getElementById("lblUnit").value = $scope.mySelections[0].unit;
            }
            else {
                if (document.getElementById("lblUnit") != null)
                    document.getElementById("lblUnit").value = "";
                if (document.getElementById("Unit") != null)
                    document.getElementById("Unit").value = "";
                if (document.getElementById("txtUnitText") != null)
                    document.getElementById("txtUnitText").value = "";
            }


            if ($scope.mySelections.length > 0 && $scope.mySelections[0].losstypename != null) {
                if (document.getElementById("Losstype") != null)
                    document.getElementById("Losstype").value = $scope.mySelections[0].losstypename;
            }
            else if (document.getElementById("Losstype") != null)
                document.getElementById("Losstype").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].reservestatus != null) {
                if (document.getElementById("txtReserveTypeCode") != null)
                    document.getElementById("txtReserveTypeCode").value = $scope.mySelections[0].reservestatus;
            }
            else if (document.getElementById("txtReserveTypeCode") != null)
                document.getElementById("txtReserveTypeCode").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].losstypecode != null) {
                if (document.getElementById("losstypecode") != null)
                    document.getElementById("losstypecode").value = $scope.mySelections[0].losstypecode;
            }
            else if (document.getElementById("losstypecode") != null)
                document.getElementById("losstypecode").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].balance != null) {
                if (document.getElementById("balanceAmount") != null)
                    document.getElementById("balanceAmount").value = $scope.mySelections[0].balance;
            }
            else if (document.getElementById("balanceAmount") != null)
                document.getElementById("balanceAmount").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].incurred != null) {
                if (document.getElementById("incurredAmount") != null)
                    document.getElementById("incurredAmount").value = $scope.mySelections[0].incurred;
            }
            else if (document.getElementById("incurredAmount") != null)
                document.getElementById("incurredAmount").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].reservecatdesc != null) {
                if (document.getElementById("reservecatdesc") != null)
                    document.getElementById("reservecatdesc").value = $scope.mySelections[0].reservecatdesc;
            }
            else if (document.getElementById("reservecatdesc") != null)
                document.getElementById("reservecatdesc").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].DedTypeCode != null) {
                if (document.getElementById("DedTypeCode") != null)
                    document.getElementById("DedTypeCode").value = $scope.mySelections[0].DedTypeCode;
            }
            else if (document.getElementById("DedTypeCode") != null)
                document.getElementById("DedTypeCode").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections.length > 0 && $scope.mySelections[0].DedRcRowId != null) {
                if (document.getElementById("DedRcRowId") != null)
                    document.getElementById("DedRcRowId").value = $scope.mySelections[0].DedRcRowId;
            }
            else if(document.getElementById("DedRcRowId") != null)
                document.getElementById("DedRcRowId").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].policyname != null) {
                if (document.getElementById("PolicyName") != null)
                    document.getElementById("PolicyName").value = $scope.mySelections[0].policyname;
            }
            else if (document.getElementById("PolicyName") != null)
                document.getElementById("PolicyName").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].unit != null) {
                if (document.getElementById("Unit") != null)
                    document.getElementById("Unit").value = $scope.mySelections[0].unit;
            }
            else if (document.getElementById("Unit") != null)
                document.getElementById("Unit").value = "";

            if ($scope.mySelections.length > 0 && $scope.mySelections[0].coveragename != null) {
                if (document.getElementById("CoverageType") != null)
                    document.getElementById("CoverageType").value = $scope.mySelections[0].coveragename;
            }
            else if (document.getElementById("CoverageType") != null)
                document.getElementById("CoverageType").value = "";

        }
        
    }, true);

    $("#LSSExport").click(function () {
        //string format
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        imgLoading.css({ display: "block" });
        var hdLSSReserve = document.getElementById('hdLSSReserve');
        if (hdLSSReserve != null)
            hdLSSReserve.value = "";
        var gridRows;
        var lssCheckBoxId = "";
        if ($scope.gridOptions != null && $scope.gridOptions !== 'undefined')
            gridRows = $scope.gridOptions.$gridScope.renderedRows;
        if (gridRows != null) {
            for (var r = 0; r < gridRows.length; r++) {
                lssCheckBoxId = "lssCheckBox" + gridRows[r].rowIndex;
                var lssCheckBox = document.getElementById(lssCheckBoxId);
                if (lssCheckBox != null && lssCheckBox.checked == true && lssCheckBox.disabled == false) {
                    if (hdLSSReserve != null && hdLSSReserve.value == "")
                        hdLSSReserve.value = gridRows[r].entity["ClaimantEid"] + "_" + gridRows[r].entity["reserveid"];
                    else
                        hdLSSReserve.value = hdLSSReserve.value + ',' + gridRows[r].entity["ClaimantEid"] + "_" + gridRows[r].entity["reserveid"];
                }
            }//for loop ends
        }

        imgLoading.css({ display: "none" });
        if (hdLSSReserve.value == "") {
            alert(ReserveListingBOBValidations.SelectRowLSSPush);
            return false;
        }
        else
            return true;
    });
    $("#btnBack").click(function () {

        var sRedirectString;
    
        var sClaimId = $('#claimid').val();
        sRedirectString = "/RiskmasterUI/UI/LookupData/AsyncLookUpNavigation.aspx?sysformname=claimant" + "&parentID=" + sClaimId;
      
        window.location.href = sRedirectString;

        return false;
    });
});