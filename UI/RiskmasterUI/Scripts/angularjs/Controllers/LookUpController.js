﻿app.controller('LookUpNavigationController', function ($timeout, $scope, $attrs) {

    $scope.sortOptions = { fields: [], directions: [] };

    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: '',
        currentPage: 1
    };

    $scope.pageName = "AsyncLookUpNavigation.aspx"; //this is mendatory to be defined

    $scope.InputDataForGrid =
    {
        GridId: $scope.gridid
    };
    //RMA-8490 START
    $scope.$watch('mySelections', function (newVal, oldVal) {
        
        if ($scope.mySelections != undefined && $scope.mySelections.length > 0 && $scope.mySelections[0].pid2 != null) {
            if (document.getElementById("hdnClaimanteid") != null)
                document.getElementById("hdnClaimanteid").value = $scope.mySelections[0].pid2;
        }
        else if (document.getElementById("hdnClaimanteid") != null)
            document.getElementById("hdnClaimanteid").value = "";

        if ($scope.mySelections != undefined && $scope.mySelections.length > 0 && $scope.mySelections[0].pid != null) {
            if (document.getElementById("hdnpid") != null)
                document.getElementById("hdnpid").value = $scope.mySelections[0].pid;
        }
        else if (document.getElementById("hdnpid") != null)
            document.getElementById("hdnpid").value = "";

    }, true);
    try
    {
        $scope.addData = [];
        var formName = document.getElementById("form").value;
        if (document.getElementById("hdnJsonAdditionalData").value != "") {
            addData = document.getElementById("hdnCarrierClaims").value = JSON.parse(document.getElementById("hdnJsonAdditionalData").value);
            //document.getElementById("hdnCarrierClaims").value = addData[0].carrierclaims;
            if (addData[0].carrierclaims == "-1" && formName=="claimant")
                document.getElementById("divSummary").setAttribute("style", "display:normal;");
            else
                document.getElementById("divSummary").setAttribute("style", "display:none;");

        }
    }
    catch(ex)
    {
    }
    //RMA-8490 END
    $timeout(function () {
        $scope.$apply(function () {
            $scope.gridid = document.getElementById("hdnFormName").value; //dvatsa
        });
    });

});
