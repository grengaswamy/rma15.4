﻿app.factory('erosionDetailsFactory', function () {
    var erosionDetails = {};
    erosionDetails.erosionDetailsModel = {
        data: [],
        UserPref: [],
        AdditionalData: []
    };
    return erosionDetails;
});

app.controller('PolicyLimitController', function ($scope, $rootScope, $http, $attrs, erosionDetailsFactory) {
    // var imgLoading = angular.element("#pleaseWaitFrame", parent.document.body);
    $scope.pageName = "PolicyLimits.aspx"; //this is mendatory to be defined
    $scope.InputDataForGrid =
   {
       GridId: "gridPolicyLimits" //this is mendatory for each grid
   };
    $scope.getErosionData = function () {
  
        var imgLoading = $("#pleaseWaitFrame", parent.document.body);
        imgLoading.css({ display: "block" });
        
        document.getElementById("divErosions").setAttribute("style", "display:normal;");
        var limitId = "";
        if (document.getElementById("txtSelectedId") != null)
            limitId = document.getElementById("txtSelectedId").value;
        $http({
            //url: "FinancialDetailHistory.aspx/SavePreferences",
            url: $scope.pageName + "/GetErosion",
            method: "POST",
            data: { LimitId: limitId, PolicyId: document.getElementById("txtPolicyId").value },
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .success(function (data, status, config, headers) {
            var responseJsonObj = JSON.parse(data.d);
            if (typeof (JSON.parse(data.d).response) !== 'undefined' && JSON.parse(data.d).response.trim() == "Success") {
                erosionDetailsFactory.erosionDetailsModel.data = JSON.parse(JSON.parse(data.d).Data);
                erosionDetailsFactory.erosionDetailsModel.UserPref = JSON.parse(JSON.parse(data.d).UserPref);
                erosionDetailsFactory.erosionDetailsModel.AdditionalData = JSON.parse(JSON.parse(data.d).AdditionalData);
                $rootScope.$emit("UpdateErosionDetails", {});
                // divPaymentScope.UpdateScopeVariablesForGrid(JSON.parse(JSON.parse(data.d).PaymentData), JSON.parse(JSON.parse(data.d).PaymentUserPref), JSON.parse(JSON.parse(data.d).PaymentAdditionalData));
            }
            else {
                $scope.ShowError(JSON.parse(data.d).errorMessage);
            }
            imgLoading.css({ display: "none" });
        })
        .error(function (data, status, config, headers) {
            imgLoading.css({ display: "none" });
            $scope.ShowError(JSON.parse(data.d).errorMessage);

        });
    };

    $scope.$watch('mySelections', function (newVal, oldVal) {
        if ($scope.mySelections != null && $scope.mySelections.length > 0) {
            if ($scope.mySelections[0].limit_id != null) {
                if (document.getElementById("txtSelectedId") != null)
                    document.getElementById("txtSelectedId").value = $scope.mySelections[0].limit_id;
            }
        }
        else if (document.getElementById("txtSelectedId") != null)
            document.getElementById("txtSelectedId").value = "";

        if ($scope.additionalData != undefined && $scope.additionalData[0].ApplyLimits == "True")
            $scope.getErosionData();
    }, true);
});

app.controller('ErosionsController', function ($scope, $rootScope, $http, $attrs, erosionDetailsFactory) {
    var imgLoading = angular.element("#pleaseWaitFrame", parent.document.body);
    $scope.pageName = "PolicyLimits.aspx"; //this is mendatory to be defined
    $scope.InputDataForGrid =
   {
       GridId: "gridErosions" //this is mendatory for each grid
   };
    $rootScope.$on("UpdateErosionDetails", function () {
        $scope.UpdateScopeVariablesForGrid(erosionDetailsFactory.erosionDetailsModel.data, erosionDetailsFactory.erosionDetailsModel.UserPref, erosionDetailsFactory.erosionDetailsModel.AdditionalData);
    });
});


