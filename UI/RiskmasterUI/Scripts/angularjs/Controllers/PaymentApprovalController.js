﻿

app.factory('searchFactory', function () {
    var facSearch = {};
    facSearch.searchModel = {
        IsSearchEnabled: false,
        PayeeLastNameComparison: "=",
        PayeeLastName: "",
        PayeeFirstNameComparison: "=",
        PayeeFirstName: "",
        PrimaryClaimantLastNameComparison: "=",
        PrimaryClaimantLastName: "",
        PrimaryClaimantFirstNameComparison: "=",
        PrimaryClaimantFirstName: "",
        TransDateComparison: "=",
        TransFromDate: "",
        TransToDate: "",
        PayeeStatusComparison: "=",
        PayeeStatus: ""
    };
    return facSearch;
});

app.controller('supervisoryApprovalController', function ($scope, $http, $attrs, $filter, $timeout, searchFactory,$element) {
    var imgLoading = angular.element("#pleaseWaitFrame", parent.document.body);
    $scope.sortOptions = { fields: [], directions: [] };
    //$scope.ColDefs = [];
    //$scope.FinanicalHistoryDetailData = [];
    //$scope.isAllItem = false;
    $scope.totalServerItems = 0;
    $scope.SelectedPaymentItems = [];
    $scope.selectionModel = {
        enableMultiSelect: true,
        enableSelectionCheckBox: true
    };
    $scope.isAllowOverride = false;
    $scope.isAllowOffset = false;
    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: '',
        currentPage: 1
    };
    $scope.pageName = "ApproveTrans.aspx"; //this is mendatory to be defined
    //PostInputDataForGrid - this variable is must in each controller. grid directive will use this array to pass input parameters used to bind the grid data
    $scope.InputDataForGrid =
    {
        GridId: "gridPaymentApproval" //this is mendatory for each grid
    };
    $scope.paymentModel = {
        isShowAllItems: "",
        isShowMyTrans: "",      // Added By Nitika For JIRA 8253 
        isPayeeStatus: "",      // 7810  achouhan3   
        isCurrencyType: ""    //JIRA RMA-5572 (RMA-14832) ajohari2
    };
    //rkotak:  rma 14873, 14900 starts
    $scope.customCellTemplateForSelectionCheckBox = '<div class=\"ngSelectionCell\"><input tabindex=\"-1\" class=\"ngSelectionCheckbox\" type=\"checkbox\" ng-checked=\"checkSelectioncheckBox(row)\" ng-disabled=\"disableSelectioncheckBox(row)\" /></div>';
    //rkotak:  rma 14873, 14900 ends
    var lastScrollLeft = 0;
    //RMA-9875  achouhan3    added to handle Multiselect Checkbox in case of My pending Reserve Starts
    if (angular.element('#hdnParent')[0].value == "true") {
        $scope.selectionModel.enableMultiSelect = false;
        $scope.selectionModel.enableSelectionCheckBox = false;
        angular.element('#lnkSearchView').css({ display: "none" });
        angular.element('#trReason').css({ display: "none" });
        //RMA-13730 achouhan3 Added for mYpending transaction
        angular.element('#TABSCollectionsfromLSS').css({ display: "none" });
        angular.element('#divrmaPaymentGrid').css({ display: "none" });
        angular.element('#divrmaCollectionGrid').css({ display: "none" });
        angular.element('#spnFundsTransPayment').css({ display: "none" });
        angular.element('#btnDenialDeny').css({ display: "none" });
        angular.element('#btnPrint').css({ display: "none" });
        angular.element('#divDisplayPayeeStatus').css({ display: "none" });
        
    }
    else {
        $scope.selectionModel.enableMultiSelect = true;
        $scope.selectionModel.enableSelectionCheckBox = true;
        angular.element('#lnkSearchView').css({ display: "block" });
        //RMA-13865 achouhan3   Modified to hide Colon
        if (angular.element('#reason')[0] != undefined && angular.element('#reason')[0] != null)
            angular.element('#trReason').css({ display: "block" });
        else
            angular.element('#trReason').css({ display: "none" });
    }
    if (angular.element('#hdnParent')[0].value == "true") {
        $scope.paymentModel.isShowMyTrans = true;
    }
    else
        $scope.paymentModel.isShowMyTrans = false;

    //RMA-9875 achouhan3    Modified to handle Multiselect Checkbox in case of My pending Reserve Starts
    $scope.ShowMyTrans = function () {
        imgLoading.css({ display: "block" });
        var isShowMyTrans = $scope.paymentModel.isShowMyTrans;
        var parent = angular.element("#hdnParent")[0].value;
        if (isShowMyTrans == true && parent != "true") {
            parent = "true";
            //RMA-11501     achouhan3   Modified to show Reserve Worksheet when clicked from Reserve Worksheet Screen
            window.location.href = "../SupervisoryApproval/ApproveTrans.aspx?MyTrans=true";
        }
        else {
            parent = "";
            //RMA-11501     achouhan3   Modified to show Reserve Worksheet when clicked from Reserve Worksheet Screen
            window.location.href = "../SupervisoryApproval/ApproveTrans.aspx";
        }
    }
    //RMA-9875  achouhan3    added to handle Multiselect Checkbox in case of My pending Reserve ends 

    $scope.paymentModel.isCurrencyType = "cc";//JIRA RMA-5572 (RMA-14832) ajohari2

    $scope.postData =
    {
        TransId: '',
        CallingAction: '',
        VoidReason: '',
        Comments: '',
        OverrideAmount: '',
        IsOverride: false,
        IsApplyOffset: false,
        isShowAll: false, //RMA-13630     achouhan3   Modified to remove extra added records
        isHoldPayeeStatus: false,
        CurrencyType: ''   //JIRA RMA-5572 (RMA-14832) ajohari2
    };

    $scope.GetAllItems = function () {
        var divCollectionScope;
        var divDenialScope;
        var sUseMultiCurrency;//JIRA RMA-5572 (RMA-14832) ajohari2
        divCollectionScope = angular.element('#divCollection').scope();
        divDenialScope = angular.element('#divDenyPayment').scope();
        imgLoading.css({ display: "block" });
        //JIRA RMA-5572 (RMA-14832) ajohari2 START
        if (angular.element('#hdnCurrencyType')[0] != null && typeof (angular.element('#hdnCurrencyType')[0]) !== 'undefined')
            angular.element('#hdnCurrencyType')[0].value = $scope.paymentModel.isCurrencyType;

        if (angular.element('#hdnUseMultiCurrency')[0] != null && typeof (angular.element('#hdnUseMultiCurrency')[0]) !== 'undefined')
            sUseMultiCurrency = angular.element('#hdnUseMultiCurrency')[0].value;
        //JIRA RMA-5572 (RMA-14832) ajohari2 END

        $http({
            url: $scope.pageName + "/BindDataToGrid",
            method: "POST",
            data: { showAll: $scope.paymentModel.isShowAllItems, showMyTrans: $scope.paymentModel.isShowMyTrans, payeeStatus: $scope.paymentModel.isPayeeStatus, currencyType: $scope.paymentModel.isCurrencyType, sUseMultiCurrency: sUseMultiCurrency },//JIRA RMA-5572 (RMA-14832) ajohari2
            headers: {
                'Content-Type': 'application/json'
            }
        })
          .success(function (data, status, config, headers) {
              //imgLoading.css({ display: "none" });
              if (typeof (JSON.parse(data.d).response) !== 'undefined' && JSON.parse(data.d).response == "Success") {
                  if ($scope.paymentModel.isShowMyTrans) {
                      $scope.gridOptions.multiSelect = false;
                      $scope.gridOptions.showSelectionCheckbox = false;
                      $scope.updateGridOptions();
                  }
                  $scope.UpdateScopeVariablesForGrid(JSON.parse(JSON.parse(data.d).PaymentData), JSON.parse(JSON.parse(data.d).PaymentUserPref), JSON.parse(JSON.parse(data.d).PaymentAdditionalData));
                  if (divCollectionScope != null) {
                      $timeout(function () {
                          divCollectionScope.$apply(function () {
                              divCollectionScope.UpdateScopeVariablesForGrid(JSON.parse(JSON.parse(data.d).CollectionData), JSON.parse(JSON.parse(data.d).CollectionUserPref), JSON.parse(JSON.parse(data.d).CollectionAdditionalData));
                              divCollectionScope.clearControl(); //RMA-15403    achouhan3   Added nullify grid after load
                          });
                      });
                  }
                  if (divDenialScope != null) {
                      $timeout(function () {
                          divDenialScope.$apply(function () {
                              divDenialScope.UpdateScopeVariablesForGrid(JSON.parse(JSON.parse(data.d).DenialData), JSON.parse(JSON.parse(data.d).DenialUserPref), JSON.parse(JSON.parse(data.d).DenialAdditionalData));
                              divDenialScope.clearControl(); //RMA-15403    achouhan3   Added nullify grid after load
                          });
                      });
                  }
                  imgLoading.css({ display: "" });
              }
              else {
                  $scope.ShowError(JSON.parse(data.d).response);
              }
              $scope.clearControl();              
              if (parent.MDISetUnDirty !=null)
                parent.MDISetUnDirty(null, "0", false);
          })
          .error(function (data, status, config, headers) {
              imgLoading.css({ display: "none" });
              $scope.ShowError(JSON.parse(data.d).response);
          });
    }

    $scope.$watch('mySelections', function (newVal, oldVal) {
        if ($scope.mySelections != null && $scope.mySelections.length == 0) {
            //RMA-10284     achouhan3       Added for uncheck header check box when grid rebind starts
            if (angular.element('#divPayment .ngSelectionHeader')[0] != null) {
                //angular.element('input:checkbox.ngSelectionHeader').prop('checked', false);
                $scope.gridOptions.$gridScope.toggleSelectAll(null, false);
                var selectAllHeader = angular.element("#divPayment .ngSelectionHeader").scope();
                if (selectAllHeader != null && selectAllHeader) selectAllHeader.allSelected = false;
            }
            //RMA-10284     achouhan3       Added for uncheck header check box when grid rebind ends
        }
        if ($scope.mySelections != null)
            $scope.SelectedPaymentItems = $scope.mySelections;
        //RMA-10587 achouhan3  Selection issue fixed
        $scope.toggleSelection();
    }, true);

    $scope.toggleSelection = function () {
        var result = null;
        var rows = null;
        var selectAllHeader = null;
        var ngSelectionHeader = angular.element("#divPayment .ngSelectionHeader");
        if (ngSelectionHeader != null && ngSelectionHeader != undefined)
            selectAllHeader = angular.element("#divPayment .ngSelectionHeader").scope();
        if ($scope.gridOptions != null && $scope.gridOptions !== 'undefined')
            rows = $scope.gridOptions.$gridScope.renderedRows, allChecked = true, IsAllSelected = false;
        if (rows != null) {
            for (var r = 0; r < rows.length; r++) {
                //RMA-14900 achouhan3   Header Checkbox Issue
                if (typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {
                    if (rows[r].entity.HOLD_REASON_CODE.trim() !== 'PAYEE_NOT_APPRV' && rows[r].entity.HOLD_REASON_CODE.trim() !== 'PAYEE_REJECT')
                        result = $filter('filter')($scope.SelectedPaymentItems, { "TransId": rows[r].entity["TransId"] })[0];
                        //RMA-15710     achouhan3       modified condition
                    else if (rows[r].entity.PayeeStatus !== 'undefined' && rows[r].entity.PayeeStatus.trim() === 'Approved')
                        result = $filter('filter')($scope.SelectedPaymentItems, { "TransId": rows[r].entity["TransId"] })[0];
                    else
                        result = rows[r].entity;
                }
                else
                    result = $filter('filter')($scope.SelectedPaymentItems, { "TransId": rows[r].entity["TransId"] })[0];
                if (result == null) {
                    allChecked = false;
                    break;
                }
            }
        }

        if (rows.length == 0)
            allChecked = false;
        if ($scope.SelectedPaymentItems.length == 0)
            allChecked = false;
        if (selectAllHeader != null && selectAllHeader) {
            selectAllHeader.allSelected = allChecked;
            IsAllSelected = allChecked;
            //RMA-10715 achouhan3   Added to select header checkbox when Mutliselect is on while scrolling starts
            if (angular.element('#divPayment .ngSelectionHeader')[0] != null && angular.element('#divPayment .ngSelectionHeader')[0].checked != null) {
                if (IsAllSelected) {
                    angular.element('#divPayment .ngSelectionHeader')[0].checked = true;
                }
                else {
                    angular.element('#divPayment .ngSelectionHeader')[0].checked = false;
                }
            }

            //RMA-10715 achouhan3   Added to select header checkbox when Mutliselect is on while scrolling Ends

        }
    };

    $scope.PaymentAction = function (sAction) {
        divPaymentScope = angular.element('#divPayment').scope();
        var TransID = '';
        var sVoidReason = '';//RMA-13493 achouhan3   Added to intialize string
        var sComments = '';//RMA-13493 achouhan3   Added to intialize string
        var overrideAmount = '';
        var sVoidcodereason = ''//RMA-18261 pkumari3

        var sUseMultiCurrency;//JIRA RMA-5572 (RMA-14832) ajohari2
        //JIRA RMA-5572 (RMA-14832) ajohari2 START
        if (angular.element('#hdnUseMultiCurrency')[0] != null && typeof (angular.element('#hdnUseMultiCurrency')[0]) !== 'undefined')
            sUseMultiCurrency = angular.element('#hdnUseMultiCurrency')[0].value;
        //JIRA RMA-5572 (RMA-14832) ajohari2 END
        //RMA-18261 pkumari3 start
        if (angular.element('#voidcodereason')[0] != null && typeof (angular.element('#voidcodereason')[0]) !== 'undefined') {
            sVoidcodereason = angular.element('#voidcodereason')[0].value;
        }
        //RMA-18261 pkumari3 end
        if (angular.element('#voidreason')[0] != null && typeof (angular.element('#voidreason')[0]) !== 'undefined')
            sVoidReason = angular.element('#voidreason')[0].value;
        if (angular.element('#tboverrideamount')[0] != null && typeof (angular.element('#tboverrideamount')[0]) !== 'undefined')
            overrideAmount = angular.element('#tboverrideamount')[0].value;
        if (angular.element('#reason')[0] != null && typeof (angular.element('#reason')[0]) !== 'undefined')
            sComments = angular.element('#reason')[0].value;
        if ($scope.SelectedPaymentItems != null && $scope.SelectedPaymentItems.length > 0) {
            for (var i = 0; i < $scope.SelectedPaymentItems.length; i++) {
                TransID = TransID + $scope.SelectedPaymentItems[i].TransId + ",";
            }
        }
        else {
            alert(PaymentValidations.ValidSelectRecord);
            return false;
        }
        if (sAction === "Approve" && $scope.isAllowOverride && overrideAmount == "") // Check for override and overrde Amount
        {
            alert(PaymentValidations.ValidOverrideAmt);
            return false;
        }
        else if (sAction === "Void") {
            if (angular.element('#hdnVoidReasonReqd')[0] != null && typeof (angular.element('#hdnVoidReasonReqd')[0]) !== 'undefined' && angular.element('#hdnVoidReasonReqd')[0].value === 'true') { //rkaur27: RMA-18653
            //RMA-18261 pkumari3 start
                //if (sVoidcodereason === "0" || sVoidcodereason === "") {
                //    alert("Please select a Void Reason Code.");
                //    return false;
                //}
                    //RMA-18261 pkumari3 end
            //else 
                if (sVoidReason === "") {
                    alert(PaymentValidations.ValidCheckReason);
                    return false;
                }
            }
        }

        if ($scope.SelectedPaymentItems != null && $scope.SelectedPaymentItems.length > 1 && sAction === "Void") {
            var bResult = confirm(PaymentValidations.ValidSameCheckReason);
            if (!bResult)
                return false;
        }
        imgLoading.css({ display: "block" });
        $scope.postData.TransId = TransID;
        $scope.postData.VoidReason = sVoidReason;
        $scope.postData.CallingAction = sAction;
        $scope.postData.OverrideAmount = overrideAmount;
        //RMA-13492     achouhan3   Added to corrrect overrude validation starts
        $scope.postData.IsOverride = $scope.isAllowOverride;
        $scope.postData.IsApplyOffset = $scope.isAllowOffset;
        //RMA-13492     achouhan3   Added to corrrect overrude validation starts
        $scope.postData.CurrencyType = $scope.paymentModel.isCurrencyType;   //JIRA RMA-5572 (RMA-14832) ajohari2
        $scope.postData.Comments = sComments;
        $scope.postData.isShowAll = $scope.paymentModel.isShowAllItems;  //RMA-13630     achouhan3   Modified to remove extra added records
        $scope.postData.isHoldPayeeStatus = $scope.paymentModel.isPayeeStatus;
        searchFactory.searchModel.IsSearchEnabled = true;
        if (typeof (angular.element('#lstPayeeLastName')[0]) !== 'undefined' && angular.element('#lstPayeeLastName')[0] != null)
            searchFactory.searchModel.PayeeLastNameComparison = angular.element('#lstPayeeLastName')[0].value;
        if (typeof (angular.element('#txtPayeeLastName')[0]) !== 'undefined' && angular.element('#txtPayeeLastName')[0] != null)
            searchFactory.searchModel.PayeeLastName = angular.element('#txtPayeeLastName')[0].value;
        if (typeof (angular.element('#lstPayeeFirstName')[0]) !== 'undefined' && angular.element('#lstPayeeFirstName')[0] != null)
            searchFactory.searchModel.PayeeFirstNameComparison = angular.element('#lstPayeeFirstName')[0].value;
        if (typeof (angular.element('#txtPayeeFirstName')[0]) !== 'undefined' && angular.element('#txtPayeeFirstName')[0] != null)
            searchFactory.searchModel.PayeeFirstName = angular.element('#txtPayeeFirstName')[0].value;
        if (typeof (angular.element('#lstPrimaryClaimantLastName')[0]) !== 'undefined' && angular.element('#lstPrimaryClaimantLastName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantLastNameComparison = angular.element('#lstPrimaryClaimantLastName')[0].value;
        if (typeof (angular.element('#txtPrimaryClaimantLastName')[0]) !== 'undefined' && angular.element('#txtPrimaryClaimantLastName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantLastName = angular.element('#txtPrimaryClaimantLastName')[0].value;
        if (typeof (angular.element('#lstPrimaryClaimantFirstName')[0]) !== 'undefined' && angular.element('#lstPrimaryClaimantFirstName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantFirstNameComparison = angular.element('#lstPrimaryClaimantFirstName')[0].value;
        if (typeof (angular.element('#txtPrimaryClaimantFirstName')[0]) !== 'undefined' && angular.element('#txtPrimaryClaimantFirstName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantFirstName = angular.element('#txtPrimaryClaimantFirstName')[0].value;
        if (typeof (angular.element('#TransDateop')[0]) !== 'undefined' && angular.element('#TransDateop')[0] != null)
            searchFactory.searchModel.TransDateComparison = angular.element('#TransDateop')[0].value;
        if (typeof (angular.element('#TransDatestart')[0]) !== 'undefined' && angular.element('#TransDatestart')[0] != null)
            searchFactory.searchModel.TransFromDate = angular.element('#TransDatestart')[0].value;
        if (typeof (angular.element('#TransDateend')[0]) !== 'undefined' && angular.element('#TransDateend')[0] != null)
            searchFactory.searchModel.TransToDate = angular.element('#TransDateend')[0].value;
        if (typeof (angular.element('#lstPayeeStatus')[0]) !== 'undefined' && angular.element('#lstPayeeStatus')[0] != null)
            searchFactory.searchModel.PayeeStatusComparison = angular.element('#lstPayeeStatus')[0].value;
        if (typeof (angular.element('#PayeeStatus_codelookup')[0]) !== 'undefined' && angular.element('#PayeeStatus_codelookup')[0] != null)
            searchFactory.searchModel.PayeeStatus = angular.element('#PayeeStatus_codelookup')[0].value;
        $http({
            url: $scope.pageName + "/ActionMethod",
            method: "POST",
            data: { postData: JSON.stringify($scope.postData), searchInput: JSON.stringify(searchFactory.searchModel), sUseMultiCurrency: sUseMultiCurrency },//JIRA RMA-5572 (RMA-14832) ajohari2
            headers: {
                'Content-Type': 'application/json'
            }
        })
          .success(function (data, status, config, headers) {
              //imgLoading.css({ display: "none" });
              if (typeof (JSON.parse(data.d).response) !== 'undefined' && JSON.parse(data.d).response.trim() == "Success") {
                  if (sAction !== "DenialDeny")
                      divPaymentScope.UpdateScopeVariablesForGrid(JSON.parse(JSON.parse(data.d).PaymentData), JSON.parse(JSON.parse(data.d).PaymentUserPref), JSON.parse(JSON.parse(data.d).PaymentAdditionalData));
              }
              else {
                  divPaymentScope.ShowError(JSON.parse(data.d).error);
              }
              //RMA-13778   achouuan3   Added for error Handling
              if (JSON.parse(data.d).error.trim() !== "") {
                  divPaymentScope.ShowError(JSON.parse(data.d).error);
              }
              //RMA-13778   achouuan3   Added for error Handling
              $scope.clearControl();
              imgLoading.css({ display: "none" });
              if (parent.MDISetUnDirty != null)
                  parent.MDISetUnDirty(null, "0", false);
          })
          .error(function (data, status, config, headers) {
              imgLoading.css({ display: "none" });
              divPaymentScope.ShowError(JSON.parse(data.d).error); //RMA-13778   achouuan3   Added for error Handling
          });
    };

    $scope.clearControl = function () {
        //RMA - 13631   achouhan3   Modified to clear all control starts
        if (typeof (angular.element('#tboverrideamount')[0]) != 'undefined' && angular.element('#tboverrideamount')[0] != null)
            angular.element('#tboverrideamount')[0].value = '';
        if (typeof (angular.element('#reason')[0]) != 'undefined' && angular.element('#reason')[0] != null)
            angular.element('#reason')[0].value = '';
        if (angular.element('#voidreason')[0] != null && typeof (angular.element('#voidreason')[0]) != 'undefined') {
            angular.element('#voidreason')[0].value = '';
            angular.element('#voidreason_HTML')[0].value = ''; //RMA-13861  achouhan3   Added  to clean HTML text too

        }
        //RMA-18261 pkumari3 start
        if (angular.element('#voidcodereason')[0] != null && typeof (angular.element('#voidcodereason')[0]) !== 'undefined')
             angular.element('#voidcodereason')[0].value = '0';
        //RMA-18261 pkumari3 end
        $scope.isAllowOverride = false;
        $scope.isAllowOffset = false;
        $scope.SelectedPaymentItems.length = 0;  // RMA-13630   achouhan3  Nullify selected items
        //RMA - 13631   achouhan3   Modified to clear all control Ends
    };
       
    //rkotak:  rma 14873, 14900 starts
    $scope.checkSelectioncheckBox = function (row) {
        
        if (row != null && typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {

            if (typeof row.entity.PayeeStatus !== 'undefined' && row.entity.PayeeStatus.trim() !== 'Approved' && (typeof row.entity !== 'undefined' && typeof row.entity.HOLD_REASON_CODE !== 'undefined' && (row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_NOT_APPRV' || row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_REJECT'))) {                
                return false;
            }
            else
                return row.selected;
        }
        else
            return row.selected;
    };

    $scope.disableSelectioncheckBox = function (row) {
        
        if (row != null && typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {

            if (typeof row.entity.PayeeStatus !== 'undefined' && row.entity.PayeeStatus.trim() !== 'Approved' && (typeof row.entity !== 'undefined' && typeof row.entity.HOLD_REASON_CODE !== 'undefined' && (row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_NOT_APPRV' || row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_REJECT'))) {                
                return true;
            }
        }        
    };    
   
    $scope.ev_beforeSelectionChangeEvent = function (dataRow) {
        //in this event, we want to control "enableRowSelection" for a pirticular row. if a row is not available for selection, then return false, true other wise
        if (dataRow.length) {
            return true;
        }
        else {
            if (typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {
                if (typeof dataRow.entity !== 'undefined' && typeof dataRow.entity.PayeeStatus !== 'undefined' && dataRow.entity.PayeeStatus.trim() !== 'Approved' && (typeof dataRow.entity.HOLD_REASON_CODE !== 'undefined' && (dataRow.entity.HOLD_REASON_CODE.trim() == 'PAYEE_NOT_APPRV' || dataRow.entity.HOLD_REASON_CODE.trim() == 'PAYEE_REJECT')))//ajohari2 RMA 6404 and RMA 9875               
                    return false;
                else
                    return true;
            }
            else
                return true;
        }
    };
    //rkotak:  rma 14873, 14900 ends

    $scope.RestoreDefaultCallBak = function () {
        $scope.clearControl();
    };

    //RMA-14900     achouhan3       Added to handle scroll event for header Checkbox
    setTimeout(function () {
        $element.find('.ngViewport').on('scroll', function (e) {
            var horizontal = e.currentTarget.scrollLeft;
            if (lastScrollLeft != horizontal) {
                setTimeout(function () {
                    $scope.toggleSelection();
                }, 300);
                lastScrollLeft = horizontal;
            }
        });
    }, 300);
    //RMA-14900     achouhan3       Added to handle scroll event for header Checkbox
});

app.controller('collectionController', function ($scope, $http, $attrs, $filter, searchFactory, $element) {
    var imgLoading = angular.element("#pleaseWaitFrame", parent.document.body);
    $scope.sortOptions = { fields: [], directions: [] };
    $scope.totalServerItems = 0;

    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: '',
        currentPage: 1
    };
    $scope.SelectedCollectionItems = [];
    $scope.selectionModel = {
        enableMultiSelect: true,
        enableSelectionCheckBox: true     // Added By Nitika For JIRA 8253            
    };
    $scope.pageName = "ApproveTrans.aspx"; //this is mendatory to be defined
    //PostInputDataForGrid - this variable is must in each controller. grid directive will use this array to pass input parameters used to bind the grid data
    $scope.InputDataForGrid =
    {
        GridId: "gridCollectionApproval", //this is mendatory for each grid
    };
    //rkotak:  rma 14873, 14900 starts
    $scope.customCellTemplateForSelectionCheckBox = '<div class=\"ngSelectionCell\"><input tabindex=\"-1\" class=\"ngSelectionCheckbox\" type=\"checkbox\" ng-checked=\"checkSelectioncheckBox(row)\" ng-disabled=\"disableSelectioncheckBox(row)\" /></div>';
    //rkotak:  rma 14873, 14900 ends
    var lastScrollLeft = 0;
    //RMA-9875  achouhan3    added to handle Multiselect Checkbox in case of My pending Reserve Starts
    if (angular.element('#hdnParent')[0].value == "true") {
        $scope.selectionModel.enableMultiSelect = false;
        $scope.selectionModel.enableSelectionCheckBox = false;
    }
    else {
        $scope.selectionModel.enableMultiSelect = true;
        $scope.selectionModel.enableSelectionCheckBox = true;
    }

    //achouhan3    Modified to handle Multiselect Checkbox in case of My pending Reserve Starts
    $scope.$watch('mySelections', function (newVal, oldVal) {
        if ($scope.mySelections != null && $scope.mySelections.length == 0) {
            //RMA-10284     achouhan3       Added for uncheck header check box when grid rebind starts
            if (angular.element('#divCollection .ngSelectionHeader') != null) {
                //angular.element('input:checkbox.ngSelectionHeader').prop('checked', false);
                $scope.gridOptions.$gridScope.toggleSelectAll(null, false);
                var selectAllHeader = angular.element("#divCollection .ngSelectionHeader").scope();
                if (selectAllHeader != null && selectAllHeader) selectAllHeader.allSelected = false;
            }
            //RMA-10284     achouhan3       Added for uncheck header check box when grid rebind ends
        }
        if ($scope.mySelections != null)
            $scope.SelectedCollectionItems = $scope.mySelections;
        //RMA-10587 achouhan3  Selection issue fixed
        $scope.toggleSelection();
    }, true);

    $scope.postData = {
        TransId: '',
        CallingAction: '',
        Comments: '',
        isShowAll: false, //RMA-13630     achouhan3   Modified to remove extra added records
        CurrencyType: ''   //JIRA RMA-5572 (RMA-14832) ajohari2
    };

    $scope.CollectionAction = function (sAction) {
        divCollection = angular.element('#divCollection').scope();
        var divPaymentScope = angular.element('#divPayment').scope();  //RMA-13630     achouhan3   Modified to remove extra added records
        var TransID = '';
        var sComments = ''; //RMA-13493 achouhan3   Added to intialize string
        var sUseMultiCurrency;//JIRA RMA-5572 (RMA-14832) ajohari2
        //JIRA RMA-5572 (RMA-14832) ajohari2 START
        if (angular.element('#hdnUseMultiCurrency')[0] != null && typeof (angular.element('#hdnUseMultiCurrency')[0]) !== 'undefined')
            sUseMultiCurrency = angular.element('#hdnUseMultiCurrency')[0].value;
        //JIRA RMA-5572 (RMA-14832) ajohari2 END
        if (angular.element('#reason') != null && typeof (angular.element('#reason')[0]) !== 'undefined')
            sComments = angular.element('#reason')[0].value.trim();
        if ($scope.SelectedCollectionItems != null && $scope.SelectedCollectionItems.length > 0) {
            for (var i = 0; i < $scope.SelectedCollectionItems.length; i++) {
                TransID = TransID + $scope.SelectedCollectionItems[i].TransId + ",";
            }
        }
        else {
            alert(PaymentValidations.ValidSelectRecord);
            return false;
        }
        imgLoading.css({ display: "block" });
        $scope.postData.TransId = TransID;
        $scope.postData.CallingAction = sAction;
        $scope.postData.Comments = sComments;
        $scope.postData.isShowAll = divPaymentScope.paymentModel.isShowAllItems;  //RMA-13630     achouhan3   Modified to remove extra added records
        $scope.postData.CurrencyType = divPaymentScope.paymentModel.isCurrencyType;   //JIRA RMA-5572 (RMA-14832) ajohari2
        searchFactory.searchModel.IsSearchEnabled = true;
        if (typeof (angular.element('#lstPayeeLastName')[0]) !== 'undefined' && angular.element('#lstPayeeLastName')[0] != null)
            searchFactory.searchModel.PayeeLastNameComparison = angular.element('#lstPayeeLastName')[0].value;
        if (typeof (angular.element('#txtPayeeLastName')[0]) !== 'undefined' && angular.element('#txtPayeeLastName')[0] != null)
            searchFactory.searchModel.PayeeLastName = angular.element('#txtPayeeLastName')[0].value;
        if (typeof (angular.element('#lstPayeeFirstName')[0]) !== 'undefined' && angular.element('#lstPayeeFirstName')[0] != null)
            searchFactory.searchModel.PayeeFirstNameComparison = angular.element('#lstPayeeFirstName')[0].value;
        if (typeof (angular.element('#txtPayeeFirstName')[0]) !== 'undefined' && angular.element('#txtPayeeFirstName')[0] != null)
            searchFactory.searchModel.PayeeFirstName = angular.element('#txtPayeeFirstName')[0].value;
        if (typeof (angular.element('#lstPrimaryClaimantLastName')[0]) !== 'undefined' && angular.element('#lstPrimaryClaimantLastName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantLastNameComparison = angular.element('#lstPrimaryClaimantLastName')[0].value;
        if (typeof (angular.element('#txtPrimaryClaimantLastName')[0]) !== 'undefined' && angular.element('#txtPrimaryClaimantLastName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantLastName = angular.element('#txtPrimaryClaimantLastName')[0].value;
        if (typeof (angular.element('#lstPrimaryClaimantFirstName')[0]) !== 'undefined' && angular.element('#lstPrimaryClaimantFirstName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantFirstNameComparison = angular.element('#lstPrimaryClaimantFirstName')[0].value;
        if (typeof (angular.element('#txtPrimaryClaimantFirstName')[0]) !== 'undefined' && angular.element('#txtPrimaryClaimantFirstName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantFirstName = angular.element('#txtPrimaryClaimantFirstName')[0].value;
        if (typeof (angular.element('#TransDateop')[0]) !== 'undefined' && angular.element('#TransDateop')[0] != null)
            searchFactory.searchModel.TransDateComparison = angular.element('#TransDateop')[0].value;
        if (typeof (angular.element('#TransDatestart')[0]) !== 'undefined' && angular.element('#TransDatestart')[0] != null)
            searchFactory.searchModel.TransFromDate = angular.element('#TransDatestart')[0].value;
        if (typeof (angular.element('#TransDateend')[0]) !== 'undefined' && angular.element('#TransDateend')[0] != null)
            searchFactory.searchModel.TransToDate = angular.element('#TransDateend')[0].value;
        if (typeof (angular.element('#lstPayeeStatus')[0]) !== 'undefined' && angular.element('#lstPayeeStatus')[0] != null)
            searchFactory.searchModel.PayeeStatusComparison = angular.element('#lstPayeeStatus')[0].value;
        if (typeof (angular.element('#PayeeStatus_codelookup')[0]) !== 'undefined' && angular.element('#PayeeStatus_codelookup')[0] != null)
            searchFactory.searchModel.PayeeStatus = angular.element('#PayeeStatus_codelookup')[0].value;
        $http({
            url: $scope.pageName + "/CollectionMethod",
            method: "POST",
            data: { postData: JSON.stringify($scope.postData), searchInput: JSON.stringify(searchFactory.searchModel), sUseMultiCurrency: sUseMultiCurrency },//JIRA RMA-5572 (RMA-14832) ajohari2
            headers: {
                'Content-Type': 'application/json'
            }
        })
          .success(function (data, status, config, headers) {
              //imgLoading.css({ display: "none" });
              if (typeof (JSON.parse(data.d).response) !== 'undefined' && JSON.parse(data.d).response.trim() == "Success") {
                  divCollection.UpdateScopeVariablesForGrid(JSON.parse(JSON.parse(data.d).CollectionData), JSON.parse(JSON.parse(data.d).CollectionUserPref), JSON.parse(JSON.parse(data.d).CollectionAdditionalData));
              }
              else {
                  divCollection.ShowError(JSON.parse(data.d).error);
              }
              //RMA-13778   achouuan3   Added for error Handling
              if (JSON.parse(data.d).error.trim() !== "") {
                  divCollection.ShowError(JSON.parse(data.d).error);
              }
              $scope.clearControl();              
              imgLoading.css({ display: "none" });
              if (parent.MDISetUnDirty != null)
                  parent.MDISetUnDirty(null, "0", false);
          })
          .error(function (data, status, config, headers) {
              imgLoading.css({ display: "none" });
              divCollection.ShowError(JSON.parse(data.d).error);
          });
    }

    $scope.toggleSelection = function () {
        var result = null;
        var rows = null;
        var selectAllHeader = angular.element("#divCollection .ngSelectionHeader").scope();
        if ($scope.gridOptions != null && $scope.gridOptions !== 'undefined')
            rows = $scope.gridOptions.$gridScope.renderedRows, allChecked = true, IsAllSelected = false;
        if (rows != null) {
            for (var r = 0; r < rows.length; r++) {
                //RMA-14900 achouhan3   Header Checkbox Issue
                if (typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {
                    //RMA-15710     achouhan3       modified condition
                    if (rows[r].entity.PayeeStatus !== 'undefined' && rows[r].entity["PayeeStatus"] === "Approved") {
                        result = $filter('filter')($scope.SelectedCollectionItems, { "TransId": rows[r].entity["TransId"] })[0];
                    }
                    else
                        result = rows[r].entity;
                }
                else
                    result = $filter('filter')($scope.SelectedCollectionItems, { "TransId": rows[r].entity["TransId"] })[0];
                if (result == null) {
                    allChecked = false;
                    break;
                }
            }
        }

        if (rows.length == 0)
            allChecked = false;
        if ($scope.SelectedCollectionItems.length == 0)
            allChecked = false;
        if (selectAllHeader != null && selectAllHeader) {
            selectAllHeader.allSelected = allChecked;
            IsAllSelected = allChecked;
            //RMA-10715 achouhan3   Added to select header checkbox when Mutliselect is on while scrolling starts
            if (angular.element('#divCollection .ngSelectionHeader')[0] != null && angular.element('#divCollection .ngSelectionHeader')[0].checked != null) {
                if (IsAllSelected) {
                    angular.element('#divCollection .ngSelectionHeader')[0].checked = true;
                }
                else {
                    angular.element('#divCollection .ngSelectionHeader')[0].checked = false;
                }
            }
        }
    }

    $scope.clearControl = function () {
        if (typeof (angular.element('#reason')[0]) != 'undefined' && angular.element('#reason')[0] != null)
            angular.element('#reason')[0].value = '';
        $scope.SelectedCollectionItems.length = 0;  // RMA-13630   achouhan3  Nullify selected items
    };

    $scope.RestoreDefaultCallBak = function () {
        $scope.clearControl();
    };
    //rkotak:  rma 14873, 14900 starts
    $scope.ev_beforeSelectionChangeEvent = function (dataRow) {
        //in this event, we want to control "enableRowSelection" for a pirticular row. if a row is not available for selection, then return false, true other wise
        if (dataRow.length) {
            return true;
        }
        else {
            if (typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {
                if (typeof dataRow.entity !== 'undefined' && typeof dataRow.entity.PayeeStatus !== 'undefined' && dataRow.entity.PayeeStatus.trim() !== 'Approved')//ajohari2 RMA 6404 and RMA 9875
                    return false;
                else
                    return true;
            }
            else
                return true;
        }
    };

    $scope.checkSelectioncheckBox = function (row) {

        if (row != null && typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {
            if (typeof row.entity.PayeeStatus !== 'undefined' && row.entity.PayeeStatus.trim() !== 'Approved')
                return false;            
            else
                return row.selected;
        }
        else//rma 17077 starts
            return row.selected;//rma 17077 ends
    };

    $scope.disableSelectioncheckBox = function (row) {

        if (row != null && typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {

            if (typeof row.entity.PayeeStatus !== 'undefined' && row.entity.PayeeStatus.trim() !== 'Approved' && (typeof row.entity !== 'undefined' && typeof row.entity.HOLD_REASON_CODE !== 'undefined' && (row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_NOT_APPRV' || row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_REJECT'))) {
                return true;
            }
        }
    };
    //rkotak:  rma 14873, 14900 starts

    //RMA-14900     achouhan3       Added to handle scroll event for header Checkbox
    setTimeout(function () {
        $element.find('.ngViewport').on('scroll', function (e) {
            var horizontal = e.currentTarget.scrollLeft;
            if (lastScrollLeft != horizontal) {
                setTimeout(function () {
                    $scope.toggleSelection();
                }, 300);
                lastScrollLeft = horizontal;
            }
        });
    }, 300);
    //RMA-14900     achouhan3       Added to handle scroll event for header Checkbox
});

app.controller('denyPaymentController', function ($scope, $http, $attrs, $filter, searchFactory, $timeout, $element) {
    var imgLoading = angular.element("#pleaseWaitFrame", parent.document.body);
    $scope.sortOptions = { fields: [], directions: [] };
    $scope.SelectedDenyItems = [];
    //$scope.ColDefs = [];
    //$scope.FinanicalHistoryDetailData = [];
    //$scope.isAllItem = false;
    $scope.totalServerItems = 0;
    $scope.selectionModel = {
        enableMultiSelect: true,
        enableMultiSelect: true      // Added By Nitika For JIRA 8253 
    };
    $scope.bAllSelected = false;
    $scope.pagingOptions = {
        pageSizes: [],
        pageSize: '',
        currentPage: 1
    };
    $scope.pageName = "ApproveTrans.aspx"; //this is mendatory to be defined
    //PostInputDataForGrid - this variable is must in each controller. grid directive will use this array to pass input parameters used to bind the grid data
    $scope.InputDataForGrid =
    {
        GridId: "gridDenialPayment", //this is mendatory for each grid
    };
    ///rkotak:  rma 14873, 14900 starts
    $scope.customCellTemplateForSelectionCheckBox = '<div class=\"ngSelectionCell\"><input tabindex=\"-1\" class=\"ngSelectionCheckbox\" type=\"checkbox\" ng-checked=\"checkSelectioncheckBox(row)\" ng-disabled=\"disableSelectioncheckBox(row)\" /></div>';
    //rkotak:  rma 14873, 14900 ends
    var lastScrollLeft = 0;
    //RMA-9875  achouhan3    added to handle Multiselect Checkbox in case of My pending Reserve Starts
    if (angular.element('#hdnParent')[0].value == "true") {
        $scope.selectionModel.enableMultiSelect = false;
        $scope.selectionModel.enableSelectionCheckBox = false;
        $timeout(function () {
            $scope.$apply(function () {
                $scope.gridid = "gridMyPendingTransaction";
                $scope.gridName = "MyPendingTransaction";
            });
        });
    }
    else {
        $scope.selectionModel.enableMultiSelect = true;
        $scope.selectionModel.enableSelectionCheckBox = true;
        $timeout(function () {
            $scope.$apply(function () {
                $scope.gridid = "gridDenialPayment";
                $scope.gridName = "DeniedTransaction";
            });
        });
    }
    
    //achouhan3    Modified to handle Multiselect Checkbox in case of My pending Reserve Starts
    $scope.$watch('mySelections', function (newVal, oldVal) {
        if ($scope.mySelections != null && $scope.mySelections.length == 0) {
            //RMA-10284     achouhan3       Added for uncheck header check box when grid rebind starts
            if (angular.element('#divDenyPayment .ngSelectionHeader') != null) {
                //angular.element('input:checkbox.ngSelectionHeader').prop('checked', false);
                $scope.gridOptions.$gridScope.toggleSelectAll(null, false);
                var selectAllHeader = angular.element("#divDenyPayment .ngSelectionHeader").scope();
                if (selectAllHeader != null && selectAllHeader) selectAllHeader.allSelected = false;
            }
            //RMA-10284     achouhan3       Added for uncheck header check box when grid rebind ends
        }
        if ($scope.mySelections != null)
            $scope.SelectedDenyItems = $scope.mySelections;
        //RMA-10587 achouhan3  Selection issue fixed
        $scope.toggleSelection();
    }, true);

    $scope.postData = {
        TransId: '',
        CallingAction: '',
        Comments: '',
        isShowAll: false,  //RMA-13630     achouhan3   Modified to remove extra added records
        CurrencyType: ''   //JIRA RMA-5572 (RMA-14832) ajohari2
    };

    $scope.PaymentAction = function (sAction) {
        var divPaymentScope = angular.element('#divPayment').scope();  //RMA-13630     achouhan3   Modified to remove extra added records
        var TransID = '';
        var sComments = ''; //RMA-13493 achouhan3   Added to intialize string
        var sUseMultiCurrency;//JIRA RMA-5572 (RMA-14832) ajohari2
        //JIRA RMA-5572 (RMA-14832) ajohari2 START
        if (angular.element('#hdnUseMultiCurrency')[0] != null && typeof (angular.element('#hdnUseMultiCurrency')[0]) !== 'undefined')
            sUseMultiCurrency = angular.element('#hdnUseMultiCurrency')[0].value;
        //JIRA RMA-5572 (RMA-14832) ajohari2 END
        if (angular.element('#reason') != null && typeof (angular.element('#reason')[0]) !== 'undefined')
            sComments = angular.element('#reason')[0].value.trim();
        if ($scope.SelectedDenyItems != null && $scope.SelectedDenyItems.length > 0) {
            for (var i = 0; i < $scope.SelectedDenyItems.length; i++) {
                TransID = TransID + $scope.SelectedDenyItems[i].TransId + ",";
            }
        }
        else {
            alert(PaymentValidations.ValidSelectRecord);
            return false;
        }
        imgLoading.css({ display: "block" });
        $scope.postData.TransId = TransID;
        $scope.postData.CallingAction = sAction;
        $scope.postData.Comments = sComments;
        $scope.postData.isShowAll = divPaymentScope.paymentModel.isShowAllItems;  //RMA-13630     achouhan3   Modified to remove extra added records
        $scope.postData.CurrencyType = divPaymentScope.paymentModel.isCurrencyType;   //JIRA RMA-5572 (RMA-14832) ajohari2
        searchFactory.searchModel.IsSearchEnabled = true;
        if (typeof (angular.element('#lstPayeeLastName')[0]) !== 'undefined' && angular.element('#lstPayeeLastName')[0] != null)
            searchFactory.searchModel.PayeeLastNameComparison = angular.element('#lstPayeeLastName')[0].value;
        if (typeof (angular.element('#txtPayeeLastName')[0]) !== 'undefined' && angular.element('#txtPayeeLastName')[0] != null)
            searchFactory.searchModel.PayeeLastName = angular.element('#txtPayeeLastName')[0].value;
        if (typeof (angular.element('#lstPayeeFirstName')[0]) !== 'undefined' && angular.element('#lstPayeeFirstName')[0] != null)
            searchFactory.searchModel.PayeeFirstNameComparison = angular.element('#lstPayeeFirstName')[0].value;
        if (typeof (angular.element('#txtPayeeFirstName')[0]) !== 'undefined' && angular.element('#txtPayeeFirstName')[0] != null)
            searchFactory.searchModel.PayeeFirstName = angular.element('#txtPayeeFirstName')[0].value;
        if (typeof (angular.element('#lstPrimaryClaimantLastName')[0]) !== 'undefined' && angular.element('#lstPrimaryClaimantLastName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantLastNameComparison = angular.element('#lstPrimaryClaimantLastName')[0].value;
        if (typeof (angular.element('#txtPrimaryClaimantLastName')[0]) !== 'undefined' && angular.element('#txtPrimaryClaimantLastName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantLastName = angular.element('#txtPrimaryClaimantLastName')[0].value;
        if (typeof (angular.element('#lstPrimaryClaimantFirstName')[0]) !== 'undefined' && angular.element('#lstPrimaryClaimantFirstName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantFirstNameComparison = angular.element('#lstPrimaryClaimantFirstName')[0].value;
        if (typeof (angular.element('#txtPrimaryClaimantFirstName')[0]) !== 'undefined' && angular.element('#txtPrimaryClaimantFirstName')[0] != null)
            searchFactory.searchModel.PrimaryClaimantFirstName = angular.element('#txtPrimaryClaimantFirstName')[0].value;
        if (typeof (angular.element('#TransDateop')[0]) !== 'undefined' && angular.element('#TransDateop')[0] != null)
            searchFactory.searchModel.TransDateComparison = angular.element('#TransDateop')[0].value;
        if (typeof (angular.element('#TransDatestart')[0]) !== 'undefined' && angular.element('#TransDatestart')[0] != null)
            searchFactory.searchModel.TransFromDate = angular.element('#TransDatestart')[0].value;
        if (typeof (angular.element('#TransDateend')[0]) !== 'undefined' && angular.element('#TransDateend')[0] != null)
            searchFactory.searchModel.TransToDate = angular.element('#TransDateend')[0].value;
        if (typeof (angular.element('#lstPayeeStatus')[0]) !== 'undefined' && angular.element('#lstPayeeStatus')[0] != null)
            searchFactory.searchModel.PayeeStatusComparison = angular.element('#lstPayeeStatus')[0].value;
        if (typeof (angular.element('#PayeeStatus_codelookup')[0]) !== 'undefined' && angular.element('#PayeeStatus_codelookup')[0] != null)
            searchFactory.searchModel.PayeeStatus = angular.element('#PayeeStatus_codelookup')[0].value;
        $http({
            url: $scope.pageName + "/ActionMethod",
            method: "POST",
            data: { postData: JSON.stringify($scope.postData), searchInput: JSON.stringify(searchFactory.searchModel), sUseMultiCurrency: sUseMultiCurrency },//JIRA RMA-5572 (RMA-14832) ajohari2
            headers: {
                'Content-Type': 'application/json'
            }
        })
          .success(function (data, status, config, headers) {
              if (typeof (JSON.parse(data.d).response) !== 'undefined' && JSON.parse(data.d).response.trim() == "Success") {
                  if (sAction === "DenialDeny")
                      $scope.UpdateScopeVariablesForGrid(JSON.parse(JSON.parse(data.d).DenialData), JSON.parse(JSON.parse(data.d).DenialUserPref), JSON.parse(JSON.parse(data.d).DenialAdditionalData));
              }
              else {
                  $scope.ShowError(JSON.parse(data.d).error);
              }
              //RMA-13778   achouuan3   Added for error Handling
              if (JSON.parse(data.d).error.trim() !== "") {
                  $scope.ShowError(JSON.parse(data.d).error);
              }
              $scope.clearControl();              
              imgLoading.css({ display: "none" });
              if (parent.MDISetUnDirty != null)
                  parent.MDISetUnDirty(null, "0", false);
          })
          .error(function (data, status, config, headers) {
              imgLoading.css({ display: "none" });
              $scope.ShowError(JSON.parse(data.d).error);
          });
    }

    $scope.toggleSelection = function () {
        var result = null;
        var selectAllHeader = angular.element("#divDenyPayment .ngSelectionHeader").scope();

        var rows = $scope.gridOptions.$gridScope.renderedRows, allChecked = true, IsAllSelected = false;
        for (var r = 0; r < rows.length; r++) {
            //RMA-14900 achouhan3   Header Checkbox Issue
            if (typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {
                if (rows[r].entity.HOLD_REASON_CODE.trim() !== 'PAYEE_NOT_APPRV' && rows[r].entity.HOLD_REASON_CODE.trim() !== 'PAYEE_REJECT') //14903
                 {
                     result = $filter('filter')($scope.SelectedDenyItems, { "TransId": rows[r].entity["TransId"] })[0];
                }
                //RMA-15710     achouhan3       modified condition
                else if (rows[r].entity.PayeeStatus !== 'undefined' && rows[r].entity.PayeeStatus.trim() === 'Approved')
                    result = $filter('filter')($scope.SelectedDenyItems, { "TransId": rows[r].entity["TransId"] })[0];
                else
                   result = rows[r].entity;
            }
            else
                result = $filter('filter')($scope.SelectedDenyItems, { "TransId": rows[r].entity["TransId"] })[0];
            if (result == null) {
                allChecked = false;
                break;
            }
        }

        if (rows.length == 0)
            allChecked = false;
        if ($scope.SelectedDenyItems.length == 0)
            allChecked = false;
        if (selectAllHeader != null && selectAllHeader) {
            selectAllHeader.allSelected = allChecked;
            IsAllSelected = allChecked;
            //RMA-10715 achouhan3   Added to select header checkbox when Mutliselect is on while scrolling starts
            if (angular.element('#divDenyPayment .ngSelectionHeader')[0] != null && angular.element('#divDenyPayment .ngSelectionHeader')[0].checked != null) {
                if (IsAllSelected) {
                    angular.element('#divDenyPayment .ngSelectionHeader')[0].checked = true;
                }
                else {
                    angular.element('#divDenyPayment .ngSelectionHeader')[0].checked = false;
                }
            }
        }
    }
    $scope.clearControl = function () {
        if (typeof (angular.element('#reason')[0]) != 'undefined' && angular.element('#reason')[0] != null)
            angular.element('#reason')[0].value = '';
        $scope.SelectedDenyItems.length = 0; // RMA-13630   achouhan3  Nullify selected items
    };

    $scope.RestoreDefaultCallBak = function () {
        $scope.clearControl();
    };

    //rkotak:  rma 14873, 14900 starts
    $scope.checkSelectioncheckBox = function (row) {

        if (row != null && typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {

            if (typeof row.entity.PayeeStatus !== 'undefined' && row.entity.PayeeStatus.trim() !== 'Approved' && (typeof row.entity !== 'undefined' && typeof row.entity.HOLD_REASON_CODE !== 'undefined' && (row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_NOT_APPRV' || row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_REJECT'))) {                
                return false;
            }
            else
                return row.selected;
        }
        else
            return row.selected;
    };

    $scope.disableSelectioncheckBox = function (row) {
        if (row != null && typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {

            if (typeof row.entity.PayeeStatus !== 'undefined' && row.entity.PayeeStatus.trim() !== 'Approved' && (typeof row.entity !== 'undefined' && typeof row.entity.HOLD_REASON_CODE !== 'undefined' && (row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_NOT_APPRV' || row.entity.HOLD_REASON_CODE.trim() == 'PAYEE_REJECT'))) {                
                return true;
            }
        }
    };

    $scope.ev_beforeSelectionChangeEvent = function (dataRow) {        
        //in this event, we want to control "enableRowSelection" for a pirticular row. if a row is not available for selection, then return false, true other wise
        if (dataRow.length) {
            return true;
        }
        else {
            if (typeof angular.element('#hdnEntityApprovalFlag')[0] != 'undefined' && angular.element('#hdnEntityApprovalFlag')[0].value == "true") {
                if (typeof dataRow.entity !== 'undefined' && typeof dataRow.entity.PayeeStatus !== 'undefined' && dataRow.entity.PayeeStatus.trim() !== 'Approved' && (typeof dataRow.entity.HOLD_REASON_CODE !== 'undefined' && (dataRow.entity.HOLD_REASON_CODE.trim() == 'PAYEE_NOT_APPRV' || dataRow.entity.HOLD_REASON_CODE.trim() == 'PAYEE_REJECT'))) //14903//ajohari2 RMA 6404 and RMA 9875
                    return false;
                else
                    return true;
            }
            else
                return true;
        }
    };

    //rkotak:  rma 14873, 14900 ends
    //RMA-14900     achouhan3       Added to handle scroll event for header Checkbox
    setTimeout(function () {
        $element.find('.ngViewport').on('scroll', function (e) {
            var horizontal = e.currentTarget.scrollLeft;
            if (lastScrollLeft != horizontal) {
                setTimeout(function () {
                    $scope.toggleSelection();
                }, 300);
                lastScrollLeft = horizontal;
            }
        });
    }, 300);
    //RMA-14900     achouhan3       Added to handle scroll event for header Checkbox

});
