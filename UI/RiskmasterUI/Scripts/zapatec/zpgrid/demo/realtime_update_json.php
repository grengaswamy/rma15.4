<?php
/**
 * Part of Zapatec Grid realtime updates example. Generates random changes for
 * the Grid.
 *
 * Copyright (c) 2004-2006 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 */

/* $Id: realtime_update_json.php 7323 2007-06-01 21:05:51Z alex $ */

// Array with company names used in this example
$aCompanies = array(
 "Alcoa Inc",
 "AT&T Inc.",
 "General Electric Company",
 "General Motors Corporation",
 "Intel Corporation",
 "Microsoft Corporation",
 "Pfizer Inc",
 "The Home Depot, Inc.",
 "Verizon Communications",
 "Walt Disney Company"
);

// Forms an object passed to splice method of the Grid
function formChange() {
	global $aCompanies;

	// Get random company name
	$sCompany = $aCompanies[mt_rand(0, 9)];

	// Get random price
	$fPrice = mt_rand(2000, 4000) / 100;

	// Get random price change
	$fChange = mt_rand(1, 30) / 100;

	// Get randomly minus or plus
	$iMinus = mt_rand(0, 1);
	if ($iMinus) {
		$fChange = -$fChange;
	}

	// Calculate % change
	$fPercent = round($fChange * 100 / ($fPrice - $fChange), 2);

	// Get current time
	$iTime = time();

	// Form object for splice
	return array(

		// Find changed row by primary key value
		"atKey" => $sCompany,

		// Change one row
		"howMany" => 1,

		// New values for the row
		"rows" => array(
			array(
				"cells" => array(
					array("v" => $sCompany),
					array("v" => sprintf("%01.2f", $fPrice)),
					array("v" => sprintf("%+01.2f", $fChange)),
					array("v" => sprintf("%+01.2f", $fPercent) . "%"),
					array("v" => $iTime)
				)
			)
		)

	);

}

// Change random number of rows
$aChanges = array();
$iChanges = mt_rand(1, 5);
for ($iChange = 0; $iChange < $iChanges; $iChange++) {
	array_push($aChanges, formChange());
}

// Output result object converted into JSON string
require_once 'JSON.php';
$objJsonConverter = new Services_JSON();
echo $objJsonConverter->encode($aChanges);

?>