/**
 * @fileoverview Common functions used in Zapatec Two Grids Sharing Controls
 * Example.
 *
 * <pre>
 * Copyright (c) 2004-2006 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 * </pre>
 */

/* $Id: double.js 7323 2007-06-01 21:05:51Z alex $ */

/**
 * Converts number of milliseconds since January 1, 1970, 00:00:00.000 into
 * a date string.
 * @private
 */
function fromTimestamp(iTimestamp) {
	var oDate = new Date(Math.round(iTimestamp));
	var sMonth = oDate.getMonth() + 1;
	if (sMonth < 10) {
		sMonth = '0' + sMonth;
	}
	var sDay = oDate.getDate();
	if (sDay < 10) {
		sDay = '0' + sDay;
	}
	var sYear = oDate.getYear();
	if (sYear < 1900) {
		sYear += 1900;
	}
	sYear += '';
	sYear = sYear.substr(2);
	return sMonth + '/' + sDay + '/' + sYear;
}

/**
 * Range of items slider "onChange" event listener. Called when slider position
 * is changed programmatically.
 * @private
 */
function onRangeOfDatesChange(iMin, iMax) {
	// Display scale
	document.getElementById('rangeOfDates').innerHTML =
	 fromTimestamp(iMin) + ' - ' + fromTimestamp(iMax);
}

/**
 * Range of items slider "newPosition" event listener. Called when slider is
 * drag-n-dropped.
 * @private
 */
function onRangeOfDatesDrag(iMin, iMax) {
	onRangeOfDatesChange(iMin, iMax);
	// Limit range of items
	oGrid1.limitRange({
		column: 1,
		min: iMin,
		max: iMax
	});
	oGrid2.limitRange({
		column: 1,
		min: iMin,
		max: iMax
	});
}

/**
 * Range of minutes slider "onChange" event listener. Called when slider
 * position is changed programmatically.
 * @private
 */
function onRangeOfMinutesChange(iMin, iMax) {
	iMin = Math.round(iMin);
	iMax = Math.round(iMax);
	// Display scale
	document.getElementById('rangeOfMinutes').innerHTML =
	 iMin + ' - ' + iMax;
}

/**
 * Range of minutes slider "newPosition" event listener. Called when slider is
 * drag-n-dropped.
 * @private
 */
function onRangeOfMinutesDrag(iMin, iMax) {
	onRangeOfMinutesChange(iMin, iMax);
	// Limit range of items
	oGrid1.limitRange({
		column: 5,
		minValue: iMin,
		maxValue: iMax
	});
	oGrid2.limitRange({
		column: 5,
		minValue: iMin,
		maxValue: iMax
	});
}

/**
 * Holds items slider object.
 * @private
 */
var oDateSlider;

/**
 * Holds minutes slider object.
 * @private
 */
var oMinuteSlider;

/**
 * Passed to grid through eventListeners config option as 'gridInitialized'
 * event listener and called when grid is initialized. Grid object can be
 * accessed through "this" because function is called in scope of grid object.
 * @private
 */
function onGridInit() {
	// Skip initialization event of the first grid because we need both grids to
	// be initialized
	if (!oGrid1) {
		return;
	}
	var oRange1, oRange2, iMin, iMax;
	// Range of Dates
	// oGrid1 is already assigned at this point
	// oGrid2 is not assigned yet at this point, but we are able to access the
	// second grid because this function is called in scope of it
	oRange1 = oGrid1.getColumnRange({column: 1});
	oRange2 = this.getColumnRange({column: 1});
	if (oRange1 && oRange2) {
		// Combine ranges of both grids
		iMin = Math.min(oRange1.min, oRange2.min);
		iMax = Math.max(oRange1.max, oRange2.max);
		// Show combined range
		document.getElementById('rangeOfDates').innerHTML = 
			fromTimestamp(iMin) + ' - ' + fromTimestamp(iMax);
		if (oDateSlider) {
			// Update slider
			oDateSlider.reset(iMin, iMax);
		} else {
			// Draw items slider
			oDateSlider = new Zapatec.Slider({
				div: 'rangeOfDatesScale', 
				length: 99,
				dual: true, 
				orientation: 'H',
				step: 1,
				range : [iMin, iMax],
				eventListeners: {
					'onChange': onRangeOfDatesChange,
					'newPosition': onRangeOfDatesDrag
				}
			});
		}
	}
	// Range of Minutes
	oRange1 = oGrid1.getColumnRange({column: 5});
	oRange2 = this.getColumnRange({column: 5});
	if (oRange1 && oRange2) {
		// Combine ranges of both grids
		iMin = Math.min(oRange1.min, oRange2.min);
		iMax = Math.max(oRange1.max, oRange2.max);
		// Show combined range
		document.getElementById('rangeOfMinutes').innerHTML = iMin + ' - ' + iMax;
		if (oMinuteSlider) {
			// Update slider
			oMinuteSlider.reset(iMin, iMax);
		} else {
			// Draw minutes slider
			oMinuteSlider = new Zapatec.Slider({
				div: 'rangeOfMinutesScale', 
				length: 99,
				dual: true, 
				orientation: 'H',
				step: 1,
				range : [iMin, iMax],
				eventListeners: {
					'onChange': onRangeOfMinutesChange,
					'newPosition': onRangeOfMinutesDrag
				}
			});
		}
	}
	// Display filter out checkboxes
	onGridFiltered.call(this);
}

/**
 * Passed to grid through eventListeners config option as 'gridFiltered'
 * event listener and called when grid is filtered. Grid object can be
 * accessed through "this" because function is called in scope of grid object.
 * @private
 */
function onGridFiltered() {
	var sId1 = oGrid1.getId().toString();
	var sId2 = this.getId().toString();
	// Template used to display "Select all" and "Clear" links
	var aTplL = [
	 '<div><a href="javascript:void(0)" onclick="Zapatec.Grid.checkboxSelectAllOnClick(\'',
	 sId1,
	 "',[",
	 '', // Column id
	 ']);Zapatec.Grid.checkboxSelectAllOnClick(\'',
	 sId2,
	 "',[",
	 '', // Column id
	 '])">Select all</a> | <a href="javascript:void(0)" onclick="Zapatec.Grid.checkboxClearAllOnClick(\'',
	 sId1,
	 "',[",
	 '', // Column id
	 ']);Zapatec.Grid.checkboxClearAllOnClick(\'',
	 sId2,
	 "',[",
	 '', // Column id
	 '])">Clear</a></div>'
	];
	// Template used to display checkbox
	var aTplC = [
	 '<div><input type="checkbox" ',
	 '', // Checked or not
	 'onclick="filter(this.form);Zapatec.Grid.checkboxOnClick(\'',
	 sId1,
	 "',[",
	 '', // Column id
	 "],unescape('",
	 '', // Escaped value
	 '\'),this.checked);Zapatec.Grid.checkboxOnClick(\'',
	 sId2,
	 "',[",
	 '', // Column id
	 "],unescape('",
	 '', // Escaped value
	 '\'),this.checked)"/><a href="javascript:void(0)" onclick="Zapatec.Grid.checkboxLinkOnClick(\'',
	 sId1,
	 "',[",
	 '', // Column id
	 "],unescape('",
	 '', // Escaped value
	 '\'));Zapatec.Grid.checkboxLinkOnClick(\'',
	 sId2,
	 "',[",
	 '', // Column id
	 "],unescape('",
	 '', // Escaped value
	 '\'))">',
	 '', // Value
	 '</a></div>'
	];
	// Filter Rate Period
	var aVals, aVals2, iVals, iVals2, iVal, iVal2, oVal, sVal;
	var aHiddenVals, aHiddenV;
	oRange1 = oGrid1.getColumnRange({column: 3});
	oRange2 = this.getColumnRange({column: 3});
	if (oRange1 && oRange2) {
		// Combine unique values of both grids
		aVals = oRange1.values;
		aVals2 = oRange2.values;
		iVals2 = aVals2.length;
		for (iVal2 = 0; iVals2--; iVal2++) {
			oVal = aVals2[iVal2];
			sVal = oVal.c;
			iVals = aVals.length;
			for (iVal = 0; iVals--; iVal++) {
				if (aVals[iVal].c == sVal) {
					break;
				}
			}
			if (iVals < 0) {
				aVals.push(oVal);
			}
		}
		// Sort values
		aVals.sort(function(sL, sR) {
			if (sL.c < sR.c) {
				return -1;
			}
			if (sL.c > sR.c) {
				return 1;
			}
			return 0;
		});
		// Get hidden values
		aHiddenVals = oGrid1.getFieldById(3).hiddenValues;
		if (aHiddenVals instanceof Array) {
			aHiddenV = this.getFieldById(3).hiddenValues;
			if (aHiddenV instanceof Array) {
				aHiddenVals = aHiddenVals.concat(aHiddenV);
			}
		} else {
			aHiddenVals = this.getFieldById(3).hiddenValues;
		}

		// Display checkboxes
		aTplL[3] = aTplL[7] = aTplL[11] = aTplL[15] = '3';
		aTplC[5] = aTplC[11] = aTplC[17] = aTplC[23] = '3';
		displayCheckboxes('filterOutRate', aTplL, aTplC, aVals, aHiddenVals);
	}
	// Filter Minutes
	oRange1 = oGrid1.getColumnRange({column: 5});
	oRange2 = this.getColumnRange({column: 5});
	if (oRange1 && oRange2) {
		// Combine unique values of both grids
		aVals = oRange1.values;
		aVals2 = oRange2.values;
		iVals2 = aVals2.length;
		for (iVal2 = 0; iVals2--; iVal2++) {
			oVal = aVals2[iVal2];
			sVal = oVal.c;
			iVals = aVals.length;
			for (iVal = 0; iVals--; iVal++) {
				if (aVals[iVal].c == sVal) {
					break;
				}
			}
			if (iVals < 0) {
				aVals.push(oVal);
			}
		}
		// Sort values descending
		aVals.sort(function(sL, sR) {
			if (sL.c < sR.c) {
				return 1;
			}
			if (sL.c > sR.c) {
				return -1;
			}
			return 0;
		});
		// Get hidden values
		aHiddenVals = oGrid1.getFieldById(5).hiddenValues;
		if (aHiddenVals instanceof Array) {
			aHiddenV = this.getFieldById(5).hiddenValues;
			if (aHiddenV instanceof Array) {
				aHiddenVals = aHiddenVals.concat(aHiddenV);
			}
		} else {
			aHiddenVals = this.getFieldById(5).hiddenValues;
		}
		// Display checkboxes
		aTplL[3] = aTplL[7] = aTplL[11] = aTplL[15] = '5';
		aTplC[5] = aTplC[11] = aTplC[17] = aTplC[23] = '5';
		displayCheckboxes('filterOutMinutes', aTplL, aTplC, aVals, aHiddenVals);
	}
}

/**
 * Displays filter out checkboxes.
 * @private
 */
function displayCheckboxes(sContainerId, aTplL, aTplC, aVals, aHiddenVals) {
	// Get container
	var oContr = Zapatec.Widget.getElementById(sContainerId);
	if (!oContr) {
		return;
	}
	// Form output
	var aHtml = [];
	// "Select all" and "Clear" links
	aHtml.push(aTplL.join(''));
	// Checkboxes
	var fIndexOf = Zapatec.Utils.arrIndexOf;
	var sVal;
	for (var iVal = 0; iVal < aVals.length; iVal++) {
		sVal = aVals[iVal].v + ''; // Transform into string
		// Check if this value is hidden
		if (!(aHiddenVals instanceof Array) || fIndexOf(aHiddenVals, sVal) < 0) {
			aTplC[1] = 'checked ';
		} else {
			aTplC[1] = '';
		}
		aTplC[27] = sVal;
		aTplC[7] = aTplC[13] = aTplC[19] = aTplC[25] = escape(sVal);
		aHtml.push(aTplC.join(''));
	}
	oContr.innerHTML = aHtml.join('');
}

/**
 * Sets filter to the grid.
 * @private
 */
function filter(oForm) {
	if (oGrid1) {
		if (oForm.isRegExp.checked) {
			oGrid1.setFilter({
				regexp: oForm.textFilter.value
			});
		} else {
			oGrid1.setFilter({
				text: oForm.textFilter.value
			});
		}
	}
	if (oGrid2) {
		if (oForm.isRegExp.checked) {
			oGrid2.setFilter({
				regexp: oForm.textFilter.value
			});
		} else {
			oGrid2.setFilter({
				text: oForm.textFilter.value
			});
		}
	}
	return false;
}

/**
 * Removes all filters from the grid.
 * @private
 */
function resetControls(oForm) {
	// Reset form
	oForm.textFilter.value = '';
	// Reset sliders
	for (var iWidget = 0; iWidget < Zapatec.Widget.all.length; iWidget++) {
		var oWidget = Zapatec.Widget.all[iWidget];
		if (oWidget.constructor == Zapatec.Slider) {
			oWidget.setPos(oWidget.config.range[0], oWidget.config.range[1]);
		}
	}
	// Reset filters
	if (oGrid1) {
		oGrid1.resetFilters();
	}
	if (oGrid2) {
		oGrid2.resetFilters();
	}
}
