{
	"style": "border: solid black 1px",
	"headerStyle": "",
	"fields": [
		{"title": "Date", "dataType": "date"},
		{"title": "Account", "dataType": "string", sortByColumn: 4},
		{"title": "Full Name", "dataType": "istring", "style": "font-weight: bold"},
		{"title": "Ref#", "dataType": "integer"},
		{"title": "Amount", "dataType": "float", "hidden": true}
	],
	"rows": [
		{"cells": [{"v": "10/03/2005"}, {"v": "SMITH-001"}, {"v": "Smith, John"}, {"v": 123, "style": "text-align:right;"}, {"v": -100.01, "style": "color: #f00; text-align:right;"}]},
		{"cells": [{"v": "10/03/2005"}, {"v": "SMITH-001"}, {"v": "Smith, John"}, {"v": 124, "style": "text-align:right;"}, {"v": -40.04, "style": "color: #f00; text-align:right;"}]},
		{"cells": [{"v": "10/04/2005"}, {"v": "SMITH-002"}, {"v": "Smith, Bob"}, {"v": 125, "style": "text-align:right;"}, {"v": 50.05, "style": "text-align:right;"}]},
		{"cells": [{"v": "10/05/2005"}, {"v": "SMITH-003"}, {"v": "SMITH, Dan"}, {"v": 999, "style": "text-align:right;"}, {"v": 25.52, "style": "text-align:right;"}]},
		{"cells": [{"v": "9/23/2005"}, {"v": "JONES-001"}, {"v": "Jones, Mary"}, {"v": 22, "style": "text-align:right;"}, {"v": 12.21, "style": "text-align:right;"}]},
		{"cells": [{"v": "1/01/2005"}, {"v": "BROWN-001"}, {"v": "Brown, Jane"}, {"v": 1, "style": "text-align:right;"}, {"v": -2.34, "style": "color: #f00; text-align:right;"}]}
	]
}
