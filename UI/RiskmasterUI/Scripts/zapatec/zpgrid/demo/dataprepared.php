<?php
/**
 * Example of JSON source preparation for Grid with dataPrepared config option
 * turned on. Can be used instead of dataprepared.txt file in Zapatec Grid
 * dataprepared demo.
 *
 * Copyright (c) 2004-2007 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 */

/* $Id: dataprepared.php 7323 2007-06-01 21:05:51Z alex $ */

// Fields array
$fields = array(
	array("i" => 0, "title" => "ID", "dataType" => "int"),
	array("i" => 1, "title" => "Artist", "columnWidth" => "80px"),
	array("i" => 2, "title" => "Song", "columnWidth" => "100px"),
	array("i" => 3, "title" => "Price", "dataType" => "float"),
	array("i" => 4, "title" => "Keywords", "columnWidth" => "155px"),
	array("i" => 5, "title" => "Image", "sortByColumn" => 0, "columnWidth" => "60px"),
);

// Forms data cell
function addCell(&$cells, $rowNumber, $value) {
	array_push($cells, array(
		"i" => count($cells),
		"r" => $rowNumber,
		"v" => $value
	));
}

// Forms data cell having compare value
function addCellCompare(&$cells, $rowNumber, $value, $compareValue) {
	array_push($cells, array(
		"i" => count($cells),
		"r" => $rowNumber,
		"v" => $value,
		"c" => $compareValue
	));
}

// Forms data row
function addRow(&$rows, $values) {
	$rowNumber = count($rows);
	$row = array(
		"i" => $rowNumber,
		"cells" => array()
	);
	for ($i = 0; $i < count($values); $i++) {
		$value = $values[$i];
		if (is_array($value)) {
			addCellCompare($row["cells"], $rowNumber, $value[0], $value[1]);
		} else {
			addCell($row["cells"], $rowNumber, $value);
		}
	}
	array_push($rows, $row);
}

// Form 3 rows
$rows = array();

addRow($rows, array(469787265, "Sheetal", "Devi Stuti", 0.99,
 array("79348354.79348457.79348367 Devi Stuti", "Sheetal Devi Stuti"),
 '<img src="http://a1.phobos.apple.com/r10/Music/y2005/m08/d24/h13/mzi.vpggbsny.170x170-99.jpg"/>'));

addRow($rows, array(469787266, "Hemanta Mukherjee", "Aamar Natun Gaaner", 0.99,
 array("17689784.20051430.20051428 Aamar Natun Gaaner", "Hemanta Mukherjee Aamar Natun Gaaner"),
 '<img src="http://a1.phobos.apple.com/r10/Music/y2004/m08/d04/h10/s05.jgvydfff.170x170-99.jpg"/>'));

addRow($rows, array(469787267, "Jon Miller", "Cheers", 0.99,
 array("2523817.4638666.4638638 Cheers", "Jon Miller Cheers"),
 '<img src="http://a1.phobos.apple.com/r10/Music/y2003/m12/d27/h00/s05.ktdwvzph.170x170-99.jpg"/>'));

// Output result object converted into JSON string
require_once 'JSON.php';
$objJsonConverter = new Services_JSON();
echo $objJsonConverter->encode(array(
	"fields" => $fields,
	"rows" => $rows
));

?>