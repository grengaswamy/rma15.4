/**
 * @fileoverview Part of Zapatec Grid Example Of Multiple Row Selection With
 * Radios.
 *
 * <pre>
 * Copyright (c) 2004-2007 by Zapatec, Inc.
 * http://www.zapatec.com
 * 1700 MLK Way, Berkeley, California,
 * 94709, U.S.A.
 * All rights reserved.
 * </pre>
 */

/* $Id: select_rows_with_radios.js 7323 2007-06-01 21:05:51Z alex $ */

/*
 * Check if demo was loaded from server
 */
if (document.location.toString().indexOf('http') != 0) {
	alert('Since this example demonstrates interaction between server and\
 javascript application, it must be loaded from server. This example does not\
 work if opened from local hard drive.');
}

/**
 * Will hold checked rows.
 * @ignore
 */
var oCheckedRows = {};

/**
 * Sends the rows picked as a POST using the Transport.
 * @ignore
 */
function submitChecked() {
	// Form value to send
	var aValues = [];
	for (var iRow in oCheckedRows) {
		if (oCheckedRows[iRow]) {
			aValues.push(oCheckedRows[iRow]);
		}
	}
	var sValue = aValues.join(';');
	// Show value
	showMsg('Following value will be sent to the server:', sValue);
	// Send value to the server
	Zapatec.Transport.fetchJsonObj({
		url: 'select_rows_with.php',
		method: 'POST',
		content: 'checked=' + sValue,
		onLoad: function(oResponse) {
			showMsg('Following value was received on server:', oResponse.received);
		}
	});
}

/**
 * Shows alert message.
 * @ignore
 */
function showMsg(sMsg, sData) {
	alert(sMsg + '\n' + sData.split(';').join(';\n'));
}

/**
 * Saves radio state in the corresponding row object.
 * @ignore
 */
function updateRow(oRow, bChecked) {
	// Pack row string to send to the server
	var aValues = [];
	if (bChecked) {
		aValues.push('Yes');
	} else {
		aValues.push('No');
	}
	// Get cells
	var aCells = oGrid.getRowCells(oRow);
	for (var iCell = 1; iCell < aCells.length; iCell++) {
		aValues.push(oGrid.getCellValueString(aCells[iCell]));
	}
	// Put this row into checked array
	oCheckedRows[oGrid.getRowId(oRow)] = aValues.join(',');
	// Save radio state
	if (bChecked) {
		// To show this radio as checked after grid redrawing
		oGrid.setCellValue(oGrid.getCellByRow(oRow, 0), '<form\
		 style="margin:0px"><input type="radio" name="radio"\
		 checked="checked"/>Yes<input type="radio" name="radio"\
		 />No</form>');
	} else {
		// To show this radio as unchecked after grid redrawing
		oGrid.setCellValue(oGrid.getCellByRow(oRow, 0), '<form\
		 style="margin:0px"><input type="radio" name="radio"\
		 />Yes<input type="radio" name="radio" checked="checked"/>No</form>');
	}
}

/**
 * Returns radio for the specified row.
 * @ignore
 */
function getRadio(oRow) {
	// Get first cell element
	var sCellId = 'zpGrid' + oGrid.getId() + 'Row' + oGrid.getRowId(oRow) +
	 'Cell0';
	var oCell = document.getElementById(sCellId);
	if (!oCell) {
		return null;
	}
	// Get form element
	var aForms = oCell.getElementsByTagName('form');
	if (aForms && aForms.length) {
		// Return radio
		return aForms[0].radio;
	}
	return null;
}

/**
 * Selects/unselects a row.
 * @ignore
 */
function selectRow(oRow, bChecked) {
	// Save checkbox state
	updateRow(oRow, bChecked);
	// Set radio value
	var aRadios = getRadio(oRow);
	if (aRadios && aRadios.length) {
		if (bChecked) {
			// Yes
			aRadios[0].checked = true;
		} else {
			// No
			aRadios[1].checked = true;
		}
	}
}

/**
 * Selects/unselects all the rows on the current page.
 * @ignore
 */
function selectPage(bSelect) {
	// For each row on current page
	var aRows = oGrid.applyPaging();
	for (var iRow = 0; iRow < aRows.length; iRow++) {
		// Set checkbox
		selectRow(aRows[iRow], bSelect);
	}
}

/**
 * Gets called when row is clicked.
 * @ignore
 */
function rowOnClick(oGrid, oRow) {
	// Get radio value
	var aRadios = getRadio(oRow);
	if (aRadios && aRadios.length) {
		if (aRadios[0].checked) {
			// Yes
			updateRow(oRow, true);
		} else if (aRadios[1].checked) {
			// No
			updateRow(oRow, false);
		}
	}
}

/**
 * Gets called when grid is refreshed.
 * @ignore
 */
function gridOnRefresh(oGrid) {
	// All rows are checked flag
	var bAllChecked = true;
	// All rows are unchecked flag
	var bAllUnchecked = true;
	// For each row on current page
	var aRows = oGrid.applyPaging();
	for (var iRow = 0; iRow < aRows.length; iRow++) {
		// Test if row is checked
		var aRadios = getRadio(aRows[iRow]);
		if (aRadios && aRadios.length) {
			if (!aRadios[0].checked) {
				bAllChecked = false;
			}
			if (!aRadios[1].checked) {
				bAllUnchecked = false;
			}
		}
	}
	if (bAllChecked) {
		// Set radio to "Yes" if all rows on current page are selected
		document.getElementById('selectPageYes').checked = true;
	} else if (bAllUnchecked) {
		// Set radio to "No" if all rows on current page are unselected
		document.getElementById('selectPageNo').checked = true;
	}
}
