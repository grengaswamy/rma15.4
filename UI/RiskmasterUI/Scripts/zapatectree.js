﻿var allloginnames;
var result;
var m_bIsTreeStillLoading = false;
var cacounter = 0, ca = new Array(); //rsolanki2: optimization for the treeUpdate function 
// rsolanki2 : a javscript setTimeout-0 hack to fool browser that js has finsihed processing 
// & it may continue with rendering 
function UpdateZapatecTree(p_selectedloginnames) {
    if (m_bIsTreeStillLoading == true || m_bIsTreeStillLoading == 'true') {
        alert('tree still updating... please wait.');
        return false;
    }
    //MITS 12275 Raman Bhatia -- 05/13/2008
    //Remove # from selectedloginnames string

    p_selectedloginnames = p_selectedloginnames.replace('#', '');
    p_selectedloginnames = p_selectedloginnames.replace('$', '');

    m_bIsTreeStillLoading = true;
    allloginnames = ',' + p_selectedloginnames;
    result = tree.findAll(function(node) { return node; });
    (function() {
        var i, length, data, el, start;
        length = result.length;
        if (length > 70) {
            //showing the progress bar only when there are more than 100 nodes 
            document.getElementById('progressbarDiv').style.display = '';
        }
        el = document.getElementById("progressbarZap").firstChild;

        function sort(progressFn) {
            i = 1;

            (function() {
                try {
                    if (!result[i].children[0]) {
                        if (allloginnames.indexOf(',' + result[i].data.label.toString().replace(/^\s+|\s+$/g, '') + ',') == -1) {
                            result[i].checkboxChanged(false);
                        }
                        else {
                            result[i].checkboxChanged(true);
                        }
                    }
                }
                catch (e)
            { }

                i++;
                progressFn(i, length);
                if (i < length) {
                    setTimeout(arguments.callee, 0);
                }
            })();
        }

        sort(function(value, total) {
            try {
                el.style.width = (100 * value / total) + "%";
                if (value >= total) {
                    document.getElementById('progressbarDiv').style.display = 'none';
                    result = "";
                    m_bIsTreeStillLoading = false;
                }
            }
            catch (e)
        { }
        });

    })();

}

function UpdateZapatecTree_old(p_selectedloginnames) {
    var allloginnames = p_selectedloginnames;
    var loginnamearray = allloginnames.split(",");
    var result = tree.findAll(function(node) {
        return node;
    });
    var IsChecked = false;
    for (var ii = 0; ii < result.length; ii++) {
        IsChecked = false;
        for (i = 0; i < loginnamearray.length; i++) {
            var leftcmp = new String(result[ii].data.label);
            var trimmedleftcmp = leftcmp.replace(/^\s+|\s+$/g, '');
            var rgtcmp = loginnamearray[i];

            if (trimmedleftcmp == rgtcmp) {
                result[ii].checkboxChanged(true);
                IsChecked = true;
            }
        }
        if (IsChecked == false) {
            result[ii].checkboxChanged(false);
        }

    }

    return false;
}

function removeNodes() {
    var result = tree.findAll(function(node) {
        return node.data.isChecked;
    });

    if (result == null || result.length == 0) {
        alert("No nodes checked");
    } else {
        if (confirm("Areyou sure you want to delete " + result.length + " nodes?")) {
            var content = ["Following nodes were removed:"];

            for (var ii = 0; ii < result.length; ii++) {
                var str = result[ii].data.label.replace(/\n/g, "").replace(/^\s*/, "").replace(/\s*$/, "");
                result[ii].destroy();
                content.push("'" + str + "'");
            }

            alert(content.join("\n"));
        }
    }
}

//rsolanki2: this function is called  after the tree is updated by the SelectUsers / settimeout  function to update 
//the parent nodes/root node with the partially selected background color.  (the function is only being used during intial page load
//  One the page loads and all the boxes are updated as per the portlet selected. this function is called to update the node parents accordingly  ) .
// this function is NOT being used each checkbox click. at the checkboxes, there is another "heavy" fucntion which comes into play
function client_OnTreeNodeChecked() 
{
    cacounter = 0    
    ca[0] = 0;
    var obj, treeNode, tables, numTables, i, node, value, intvalue;
    var currentnodelevel, oldnodelevel, space, j;
    //obj = document.getElementById('LTVn0CheckBox');
    obj = $("#LTVn0CheckBox")[0];
    if (obj.tagName == "INPUT" && obj.type == "checkbox") {
        treeNode = obj;
        do {
            obj = obj.parentElement;
        } while (obj.tagName != "TABLE")
        tables = obj.parentElement.getElementsByTagName("TABLE");
        numTables = tables.length;
        for (i = numTables - 1; i >= 0; i--) {
            currentnodelevel = tables[i].rows[0].cells.length;
            node = tables[i].rows[0].cells[currentnodelevel - 1].getElementsByTagName("INPUT");
            value = node[0].checked;
            if (value == true)
                intvalue = 1;
            else
                intvalue = 0;
            if (i == (numTables - 1))
                oldnodelevel = currentnodelevel;
            if (currentnodelevel > oldnodelevel) {
                space = currentnodelevel - oldnodelevel;
                for (j = 0; j < space; j++)
                    ca[cacounter++] = '*';
                ca[cacounter++] = intvalue;
            }
            else if (currentnodelevel < oldnodelevel) {
                ca[cacounter++] = "??";
                arraycalculator(ca);
                if (ca[cacounter - 1] == 0) {
                    node[0].checked = false;
                    node[0].style.backgroundColor = "white";
                }
                if (ca[cacounter - 1] == 1) {
                    node[0].checked = true;
                    node[0].style.backgroundColor = "white";
                }
                if (ca[cacounter - 1] == 2) {
                    node[0].checked = true;
                    node[0].style.backgroundColor = "#999999";//  "red";
                }

            }
            else if (currentnodelevel == oldnodelevel) {
                ca[cacounter++] = intvalue;
            }
            if (oldnodelevel != currentnodelevel)
                oldnodelevel = currentnodelevel;
        }
    }    
}
function arraycalculator() {
    var y = 0, sum = 0, pa = false, i, p;
    for (i = cacounter - 1; i > 0; i--) {
        if (ca[i] == '*') {
            y = i + 1;
            break;
        }
    }
    for (i = cacounter - 2; i >= y; i--) {
        if (ca[i] == 2)
            pa = true;
        sum = sum + ca[i];
    }
    if (y == 0)
        p = y;
    else
        p = y - 1;
    if (sum == ((cacounter - 1) - y) && pa == false) {
        ca[p] = 1;
    }
    else if (sum == 0) {
        ca[p] = 0;
    }
    else if (pa == true) {
        ca[p] = 2;
    }
    else
        ca[p] = 2;
    for (i = p + 1; i < cacounter; i++) {
        ca[i] = 0;
    }
    cacounter = p + 1;
    return ca;
}
    



//function client_OnTreeNodeChecked() { 
//    
//    
//    var obj = document.getElementById('LTVn0CheckBox'); 
//    var iSum = 0, iProd = 1, bIsChilds = false, objParentNode, bIsRootPartiallySelected = false; 
//    var bIsRootFullySelected=1, bIsIteratingChild=false; 
//    // window.event.srcElement; 
//    //style.backgroundColor = "red"; 
//    var treeNodeFound = false; 
//    var checkedState; 
//    if (obj.tagName == "INPUT" && obj.type == "checkbox") 
//    { 
//        var treeNode = obj; 
//        checkedState = treeNode.checked; 
//        do 
//        { 
//            obj = obj.parentElement; 
//        } while (obj.tagName != "TABLE") 
//        var parentTreeLevel = obj.rows[0].cells.length; 
//        var parentTreeNode = obj.rows[0].cells[0]; 
//        var tables = obj.parentElement.getElementsByTagName("TABLE"); 
//        var numTables = tables.length 
//        if (numTables >= 1) 
//        { 
//            for (i = 0; i < numTables; i++) 
//            { 
//                
//                if (tables[i] == obj) 
//                { 
//                    treeNodeFound = true; 
//                    i++; 
//                    if (i == numTables-1) 
//                    { 
//                        // maybe update root here 
//                        //alert('upper return'); 
//                        if (bIsRootFullySelected) { 
//                            treeNode.checked = true; 
//                            treeNode.style.backgroundColor = "white"; 
//                        } else if (bIsRootPartiallySelected) { 
//                            treeNode.checked = true; 
//                            treeNode.style.backgroundColor = "#999999"; 
//                        } 
//                        else { 
//                            treeNode.checked = false; 
//                            treeNode.style.backgroundColor = "white"; 
//                        } 
//                        return; 
//                    } 
//                } 
//                if (treeNodeFound == true) 
//                { 
//                    var childTreeLevel = tables[i].rows[0].cells.length; 
//                    if (bIsChilds == false && childTreeLevel == 3) 
//                    {     
//                        // we alo need to do testing with the subnode at the last level 
//                        if(i<numTables) 
//                        { 
//                            var objNextNodeLevel=3; 
//                            if (tables[i + 1] != undefined) 
//                            { objNextNodeLevel = tables[i + 1].rows[0].cells.length; } 
//                            
//                            
//                            if (objNextNodeLevel == 4) { bIsChilds = true; } 
//                            else 
//                            { 
//                                var inputs = tables[i].rows[0].cells[childTreeLevel - 1].getElementsByTagName("INPUT"); 
//                                var value = (inputs[0].checked == true) ? 1 : 0; 
//                                if (value == 0) 
//                                { 
//                                    bIsRootFullySelected = 0; 
//                                }else 
//                                { 
//                                    bIsRootPartiallySelected = true; 
//                                } 
//                            } 
//                        } 
//                        //bIsIteratingChild = true; 
//                        iSum = 0; 
//                        iProd = 1; 
//                        objParentNode = tables[i].rows[0].cells[childTreeLevel - 1].getElementsByTagName("INPUT"); 
//                    } else if (bIsChilds == true && childTreeLevel == 4) 
//                    { 
//                        var inputs = tables[i].rows[0].cells[childTreeLevel - 1].getElementsByTagName("INPUT"); 
//                        var value = (inputs[0].checked == true) ? 1 : 0; 
//                        iSum += value; 
//                        iProd *= value; 
//                    } else if (bIsChilds == true && childTreeLevel == 3) 
//                    { 
//                        //1 = selected:  0 = unselected 
//                        //objParentNode[0].checked == true; 
//                        bIsChilds = false; 
//                        var inputs = tables[i].rows[0].cells[childTreeLevel - 1].getElementsByTagName("INPUT"); 
//                        var value = (inputs[0].checked == true) ? 1 : 0; 
//                        if (value == 0) 
//                        { 
//                            bIsRootFullySelected = 0; 
//                        } else 
//                        { 
//                            bIsRootPartiallySelected = true; 
//                        } 
//                        
//                        
//                        if (iSum > 0 && iProd == 0) 
//                        { 
//                            //partially selected 
//                            objParentNode[0].style.backgroundColor = "#999999"; 
//                            objParentNode[0].checked = true; 
//                            bIsRootPartiallySelected = true; 
//                            bIsRootFullySelected = 0 ; 
//                            
//                        } else if (iSum == 0 && iProd == 0) 
//                        { 
//                            //Not selected 
//                            objParentNode[0].style.backgroundColor = "white"; 
//                            objParentNode[0].checked = false; 
//                            //bIsRootPartiallySelected = false; 
//                            bIsRootFullySelected = 0; 
//                        } else 
//                        { 
//                            //fully selected 
//                            objParentNode[0].style.backgroundColor = "white"; 
//                            objParentNode[0].checked = true; 
//                            //bIsRootFullySelected = 0; 
//                            bIsRootPartiallySelected = true; 
//                        } 
//                    }                   
//                                  
//                    
//                    if (childTreeLevel > parentTreeLevel) 
//                    { 
//                        var cell = tables[i].rows[0].cells[childTreeLevel - 1]; 
//                        var inputs = cell.getElementsByTagName("INPUT"); 
//                        //inputs[0].checked = checkedState; 
//                    } 
//                    else 
//                    { 
//                        // maybe update root here 
//                        if (bIsRootFullySelected) 
//                        { 
//                            treeNode.checked = true; 
//                            treeNode.style.backgroundColor = "white"; 
//                        } else if (bIsRootPartiallySelected) 
//                        { 
//                            treeNode.checked = true; 
//                            treeNode.style.backgroundColor = "#999999"; 
//                        } 
//                        else 
//                        { 
//                            treeNode.checked = false; 
//                            treeNode.style.backgroundColor = "white"; 
//                        } 
//                        return; 
//                    } 
//                } 
//            } 

//            if (iSum > 0 && iProd == 0) { 
//                //partially selected 
//                objParentNode[0].style.backgroundColor = "#999999"; 
//                objParentNode[0].checked = true; 
//                bIsRootPartiallySelected = true; 
//                bIsRootFullySelected = 0; 

//            } else if (iSum == 0 && iProd == 0) { 
//                //Not selected 
//                objParentNode[0].style.backgroundColor = "white"; 
//                objParentNode[0].checked = false; 
//                //bIsRootPartiallySelected = false; 
//                bIsRootFullySelected = 0; 
//            } else if (i != numTables ) { 
//                //fully selected 
//                //objParentNode[0].style.backgroundColor = "white"; 
//                objParentNode[0].checked = true; 
//                //bIsRootFullySelected = 0; 
//                bIsRootPartiallySelected = true; 
//            } 
//            
//            // for updating the rootnode 
//            if (bIsRootFullySelected) { 
//                treeNode.checked = true; 
//                treeNode.style.backgroundColor = "white"; 
//            } else if (bIsRootPartiallySelected) { 
//                treeNode.checked = true; 
//                treeNode.style.backgroundColor = "#999999"; 
//            } 
//            else { 
//                treeNode.checked = false; 
//                treeNode.style.backgroundColor = "white"; 
//            } 
//        } 
//    } 
//} 
//function Save() {
//    var objDataChanged = window.parent.document.getElementById('DataChanged');
//    if (objDataChanged != null) {
//        if (objDataChanged.value == "true") {
//            alert("Please save the portlets data to proceed.");
//            return false;
//        }
//    }
//    if (document.forms[0].hdngriddatachangedflag.value == "") {
//        alert("Please select a radio button in User Specific Customization Grid to proceed.");
//        return false;
//    }

////    var result = tree.findAll(function(node) {
////        return node.data.isChecked;
////    });

////    var allNodes = document.getElementById("selectednodes");
////    allNodes.value = "";
////    for (var ii = 0; ii < result.length; ii++) {
////        var str = result[ii].data.label.replace(/\n/g, "").replace(/^\s*/, "").replace(/\s*$/, "");
////        if (!result[ii].children[0]) {
////            if (ii == (result.length - 1))
////                allNodes.value = allNodes.value + str
////            else
////                allNodes.value = allNodes.value + str + ",";
////        }
//    //    }
//    var inputs = document.all.tags("input");
//    var SelectedFound, selectedValue, selectedId = "0";
//    for (i = 0; i < inputs.length; i++) {
//        if ((inputs[i].type == "checkbox") && (inputs[i].checked == true)) {
//            SelectedFound = true;
//            if (inputs[i].id != null) {
//                selectedId = inputs[i].id;
//            }
//        }
//    }

//    document.forms[0].SelectedID.value = selectedId;
//    window.parent.document.forms[0].hdnselecteduserid.value = "clearsession";
//    //alert(window.parent.document.forms[0].hdnaction.value);
//    //return false;
//    document.forms[0].hdnrefreshparentflag.value = "true";
//    //MITS 12376 Raman Bhatia : Adding null check
//    if (window.top.document.getElementById('RMXisZapatecTreeWindowSaving') != null) {
//        window.top.document.getElementById('RMXisZapatecTreeWindowSaving').value = "true";
//    }

//    //window.parent.document.forms[0].submit();
//    //window.parent.window.location = 'home?pg=riskmaster/Funds/choice';
//    return true;
//}

function selectNode(oNode) {
    if (!oNode.children[0]) {
        document.forms[0].hdnselecteduser.value = oNode.data.label;
        document.getElementById('highlighteduser').innerHTML = "Highlighted User: " + "<b style='text-align:center;'>" + document.forms[0].hdnselecteduser.value + "</b><br/><br/>";
    }
    else {
        document.forms[0].hdnselecteduser.value = "";
        document.getElementById('highlighteduser').innerHTML = "";
    }
}

function onLoad() {
    var oParentNode = tree.getNode(0);
    oParentNode.data.attributes.populated = 'true';
    tree.expandToLevel(1);
    if (document.forms[0].hdnrefreshparentflag.value == "true") {
        document.forms[0].hdnrefreshparentflag.value = "false";
        window.parent.document.forms[0].hdnselecteduserid.value = ""; // changed 
        window.parent.document.getElementById('hdnselecteduserid').value = "";
        //alert("Please remember to press the toolbar Save button after you are done with all your changes");
        var wnd = RMX.window.open('csc-Theme\\riskmaster\\common\\html\\progress.html', 'progressWnd', 'width=400,height=150' + ',top=' + (screen.availHeight - 150) / 2 + ',left=' + (screen.availWidth - 400) / 2);
        self.parent.wndProgress = wnd;
        window.parent.document.getElementById('Save').click();
        //alert(window.parent.document.getElementById('hdnselecteduserid').value);
    }
}