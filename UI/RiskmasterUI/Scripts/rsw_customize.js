// JScript source code

var imgFolderOpen = new Image(16,16); 
var imgFolderClosed = new Image(16,16);
var oldNodId="";
var m_DataChanged=false;
var m_DisableSubmit=false;//smahajan6m
//shobhana
var m_arrReasonsToBeChecked = new Array();
var m_arrCommentsToBeChecked = new Array();
var m_ReserveSummaryCreated = false;

imgFolderOpen.src = "csc-Theme/riskmaster/img/handle.collapse.more.gif"; 
imgFolderClosed.src = "csc-Theme/riskmaster/img/handle.expand.end.gif";

function ToggleFolder(id) {
var folderopen; 

 document.getElementById('icon-' + id).src = imgFolderClosed.src;

    if (document.getElementById(id).style.display == "inline") {
        folderopen = "false";
    } else {
        folderopen = "true";
    }

    if (folderopen == "false") {
    document.getElementById(id).style.display = "none";
    document.getElementById('icon-' + id).src = imgFolderClosed.src;
    } else {
    document.getElementById(id).style.display = "inline";
    document.getElementById('icon-' + id).src = imgFolderOpen.src;
    }
    return false;
}
// rswTypeToOpen may start with evt, clm, stl
function SetWipedoutAfterApprove() 
{
  // debugger
    var ctrlRSWStatus = document.getElementById('hdnRSWStatus');
    var hdnApprovedBtnClicked = document.getElementById('hdnApprovedBtnClicked');

    if (hdnApprovedBtnClicked.value == 'true') 
    {
        if (ctrlRSWStatus.value == 'Approved') 
        {
            parent.MDISetWipedOut();
        }
    }
}


//Changed by Nitin for ReserveWorkSheet on 08-Sept
function RefreshRSWScreen() 
{//debugger;
    var Status = document.getElementById('hdnReqStatus');
	var ctlUnderLimit = document.getElementById('hdnUnderLimit');
	var ctlSMSCheckToUpdate = document.getElementById('hdnSMSCheckToUpdate');
	var ctrlReserveTypeTabs = document.getElementById('hdnReserveTypeTabs'); //hdnReserveTypeTabs
	//debugger;

	if (ctlUnderLimit != null && ctlUnderLimit.value != "")
	{
	    //checking if there are security permissions to update

	   if (ctlUnderLimit.value == "Yes") 
      {
        parent.MDIRefreshCurrentNode();
        if (ctlSMSCheckToUpdate.value != "No") 
        {
            var ret = false;
            //ret = confirm("Reserves are within your approval authority. Worksheet does not need to be submitted for further approval. \r\nClick 'OK' to Approve the worksheet and apply the new reserves to the Claim or \r\nClick 'Cancel' to close this message and then click 'Approve' to Approve the worksheet and apply the new reserves to the Claim.");
            if (ret) 
            {
                document.getElementById('hdnAproveFromClientSide').value = "true";
                pleaseWait.Show();
                document.forms[0].submit();
                return false;
            }
        }
        else 
        {
            alert("Reserves are within your approval authority. You do not have sufficient security permission to Approve this Worksheet.The worksheet is being sent to your supervisor for approval.");
        }
      }
	  else if (ctlUnderLimit.value == "No")
	  {
	      alert("Reserves exceed your authority limits. The worksheet is being sent to your supervisor for approval.");
	      parent.MDIRefreshCurrentNode();	    
	  }
	  else if (ctlUnderLimit.value == "NoSupervisor")
	  {
	    alert("Reserves exceed your authority limits and you don't have any supervisor. The worksheet will only be approved/rejected by your supervisor.");
	  }
	  else if (ctlUnderLimit.value == "Reject") {
	    parent.MDIRefreshCurrentNode();
	    //alert("Reserves are within your approval authority. Worksheet has been Rejected.  After closing this message, You may select this Rejected worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
	  }
	  else if (ctlUnderLimit.value == "NoAuthSubmit")
	  {
	    alert("You are not authorized to submit this worksheet.");
	  }
      ctlUnderLimit.value = "";
	}
    //By Nitin on 06-Nov-2009
    //if there is no any ReserveType for a current claim then show ReserveType Tab
	if (ctrlReserveTypeTabs != null) 
	{
	    if (ctrlReserveTypeTabs.value == '')
	        CreateReserveSummary();
	}
	
	return true;
}
//added by shobhana//
function CalculateReserveAmount_OnBlur(reserve_Type,trans_Type)
{
    var txtReserveTypePaid = document.getElementById('txt_' + reserve_Type + '_Paid');
    var txtReserveTypeBal = document.getElementById('txt_' + reserve_Type + '_Outstanding');
    var txtReserveTypeIncurred = document.getElementById('txt_' + reserve_Type + '_TotalIncurred');
    var txtReserveTypeNewBal = document.getElementById('txt_' + reserve_Type + '_NEWOutstanding');
    var txtReserveTypeNewIncurred = document.getElementById('txt_' + reserve_Type + '_NEWTOTALINCURRED');
    var hdnReserveTypeCollection = document.getElementById('hdn_' + reserve_Type + '_Collection');
    var hdnReserveTypeReserveChange = document.getElementById('hdn_' + reserve_Type + '_ReserveChange');
    var hdnReserveTypeNewResAmt = document.getElementById('hdn_' + reserve_Type + '_ReserveAmt');
    var hdnReserveTypeNewBal = document.getElementById('hdn_' + reserve_Type + '_NEWOutstanding');
    var hdnReserveTypeNewIncurred = document.getElementById('hdn_' + reserve_Type + '_NEWTOTALINCURRED');
    var txtReserveTypeComments = document.getElementById('txt_' + reserve_Type + '_Comments');
    var txtReserveTypeReason = document.getElementById('txt_' + reserve_Type + '_ReserveWorksheetReason'); 

    var txtCurrentTransTypeIncurredAmt = document.getElementById('txt_' + reserve_Type + '_TransTypeIncurredAmount_' + trans_Type);
    var txtCurrentTransTypeNewReserveBal = document.getElementById('txt_' + reserve_Type + '_TransTypeNewReserveBal_' + trans_Type);
    var txtCurrentTransTypePaidToDate = document.getElementById('txt_' + reserve_Type + '_Paid_' + trans_Type);

    var txtCurrentTransTypeOther = document.getElementById('txt_' + reserve_Type + '_TransTypeIncurredAmount_OtherAdjustment');
    var hdnCurrentTransTypeOther = document.getElementById('hdn_' + reserve_Type + '_TransTypeIncurredAmount_OtherAdjustment');
    var hdnCurrentTransTypeIncurredAmt = document.getElementById('hdn_' + reserve_Type + '_TransTypeIncurredAmount_' + trans_Type);
    var hdnCurrentTransTypeNewReserveBal = document.getElementById('hdn_' + reserve_Type + '_TransTypeNewReserveBal_' + trans_Type);
    
    var arrCtrls;
    var transTypeControl = null;
    
    var totalNewAmount = 0.0;
    var dTemp = 0.0;
    var dPaid = 0.0;
    var dCollect = 0.0;
    var dNewResAmt = 0.0;
    var dNewIncurred = 0.0;
    var dNewBalance = 0.0;
    var dCurrentTransTypeReserveBal = 0.0;
    
    
    var hdnCtrlToClear = document.getElementById('hdnCtrlToEdit');

    if (hdnCtrlToClear != null) 
    {
       arrCtrls = hdnCtrlToClear.value.split(",");

       for (var count = 0; count < arrCtrls.length; count++) 
       {
           if (arrCtrls[count].indexOf("txt_" + reserve_Type + "_TransTypeIncurredAmount") != -1) 
           {
               transTypeControl = document.getElementById(arrCtrls[count]);
               if (transTypeControl != null) 
               {
                   totalNewAmount += ConvertStrToDbl(transTypeControl.value);
               }
           } 
       }
   }
                                     //commented by Nitin for Mits 18877
    if (txtReserveTypePaid != null)  //&& totalNewAmount >= 0.0)
    {   
        dPaid = ConvertStrToDbl(txtReserveTypePaid.value);
        dCollect = ConvertStrToDbl(hdnReserveTypeCollection.value);
        hdnReserveTypeNewIncurred.value = ConvertDblToCurr(totalNewAmount);
        hdnReserveTypeNewBal.value = ConvertDblToCurr(totalNewAmount - dPaid);   
        //setting reserve amount would used at the time of saving approved ws
        hdnReserveTypeNewResAmt.value = hdnReserveTypeNewIncurred.value;
        
        //calc Reserve change amount
        if ((ConvertStrToDbl(txtReserveTypePaid.value) + ConvertStrToDbl(txtReserveTypeBal.value)) == ConvertStrToDbl(hdnReserveTypeNewIncurred.value))
        {
            hdnReserveTypeReserveChange.value = "No Change";
            m_bReserveAmntChanges = false;
        }
        else
        {
            hdnReserveTypeReserveChange.value = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(hdnReserveTypeNewIncurred.value) - (ConvertStrToDbl(txtReserveTypePaid.value) + ConvertStrToDbl(txtReserveTypeBal.value))));
            m_bReserveAmntChanges = true;
        }
        
        //setting Reserchange flag true ,that would be used at the time of Saving Validation
        m_bReserveAmntChanges = true;
    }
    else if (txtReserveTypePaid != null && totalNewAmount == 0.0) 
    {
        hdnReserveTypeNewBal.value = txtReserveTypeBal.value;
        hdnReserveTypeNewIncurred.value = txtReserveTypeIncurred.value;
        hdnReserveTypeReserveChange.value = "No Change";
        m_bReserveAmntChanges = false;
    }

    txtReserveTypeNewBal.value = hdnReserveTypeNewBal.value;
    txtReserveTypeNewIncurred.value = hdnReserveTypeNewIncurred.value;
    
    if (txtCurrentTransTypeIncurredAmt != null) 
    {
        currencyLostFocus(txtCurrentTransTypeIncurredAmt);

        dCurrentTransTypeReserveBal = ConvertStrToDbl(txtCurrentTransTypeIncurredAmt.value) - ConvertStrToDbl(txtCurrentTransTypePaidToDate.value)
        txtCurrentTransTypeNewReserveBal.value = ConvertDblToCurr(dCurrentTransTypeReserveBal);

        //updating corresponding hidden fields to retain changed values at serverside
        hdnCurrentTransTypeNewReserveBal.value = txtCurrentTransTypeNewReserveBal.value;

        //nnorouzi; MITS 18989 and cleaning up the code start
        if (hdnCurrentTransTypeIncurredAmt.value != txtCurrentTransTypeIncurredAmt.value) 
        {
            m_arrReasonsToBeChecked[reserve_Type] = reserve_Type;
            m_arrCommentsToBeChecked[reserve_Type] = reserve_Type;             
            hdnCurrentTransTypeIncurredAmt.value = txtCurrentTransTypeIncurredAmt.value;
        }
        //nnorouzi; MITS 18989 and cleaning up the code end
    }
    else 
    {
        //means it is other adjustment
        if (txtCurrentTransTypeOther != null) 
        {
                currencyLostFocus(txtCurrentTransTypeOther);

                //nnorouzi; MITS 18989 and cleaning up the code start
                if (hdnCurrentTransTypeOther.value != txtCurrentTransTypeOther.value) {
                    m_arrReasonsToBeChecked[reserve_Type] = reserve_Type;
                    m_arrCommentsToBeChecked[reserve_Type] = reserve_Type;                        
                    hdnCurrentTransTypeOther.value = txtCurrentTransTypeOther.value;
                }
                //nnorouzi; MITS 18989 and cleaning up the code end
            }
        }
}

//end shobhana
function OpenWorkSheet(id, rswTypeToOpen)
{
	var ctrlStatus = document.getElementById('hdnRSWStatus');
	var ctlRSWType = self.parent.frames["BottomFrame"].document.getElementById('RSWType').value;
	
	var clmstsRelID = self.parent.frames["LeftTree"].document.getElementById('ClmStsRelID').value;
    var clmstsRel = self.parent.frames["LeftTree"].document.getElementById('ClmStsRel').value;
    
    if (clmstsRelID != null)
    {
     if (clmstsRelID == '8')
     {
       alert("Reserve Worksheet can not be opened/created when the Claim is Closed or Open-Pending.");
       self.parent.close();
       return false;
     }
    }
    	
	if (ctrlStatus != null)
	{
		if (ctrlStatus.value == "New" || ctrlStatus.value == "Pending")
		{
			var ret = false;
			ret = confirm("A worksheet is already open. You will lose any changes made if another worksheet is opened. Click 'OK' to continue and 'Cancel' to continue working with the current worksheet.");
			if (ret)
			{
				if(oldNodId!="")
					document.all["hyp-"+oldNodId].style.cssText="FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;COLOR: black;BACKGROUND-COLOR: none" 			
				document.all["hyp-"+id].style.cssText="FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;COLOR: white; BACKGROUND-COLOR: darkblue"
				oldNodId=id;				
				
				location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&rswid='+id;
				ShowPlsWait();
				return false;
			}
			else
			{
				return false;
			}
		}
	}

	if(oldNodId!="")
		document.all["hyp-"+oldNodId].style.cssText="FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;COLOR: black;BACKGROUND-COLOR: none"    
	document.all["hyp-"+id].style.cssText="FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;COLOR: white; BACKGROUND-COLOR: darkblue"    		
	oldNodId=id;
	
	var ctrl = document.getElementById("hyp-"+id);	
	if (ctrl != null)
	{		
		if (ctrl.title.indexOf('Rejected') != -1) // If rejected sheet is being opened
		{
			var ret = false;
			ret = confirm("Do you want to open the Rejected worksheet in 'Edit' Mode? Click 'OK' for Edit Mode and 'Cancel' for Read-Only Mode.");
			if (ret)
			{
				location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&editRej=true&rswid='+id;
				ShowPlsWait();
				return false;
			}
		}
	}
	location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&rswid='+id;
	
	ShowPlsWait();
	return false;
}

function LoadNew()
{
	m_DisableSubmit=false;//smahajan6m
	var ret = false;
	var ctrl = document.getElementById('hdnReqStatus');
	
	var clmstsRelID = self.parent.frames["LeftTree"].document.getElementById('ClmStsRelID').value;
    var clmstsRel = self.parent.frames["LeftTree"].document.getElementById('ClmStsRel').value;

    if (clmstsRelID != null)
    {
     if (clmstsRelID == '8')
     {
       alert("Reserve Worksheet can not be opened/created when the Claim is Closed or Open-Pending.");
       self.parent.close();
       return false;
     }
    }
	
	if (ctrl != null) // A worksheet is already open
	{
		if (ctrl.value == "New")//New worksheet
		{
			ret = false;
			ret = confirm("A worksheet is already open. You will lose any changes made if a new worksheet is opened. Click 'OK' to continue and 'Cancel' to continue working with the current worksheet.");
			if (!ret)
				return false;
		}
	}
		
	var iClaimId = document.all("claimid").value;
	if (self.parent.frames["LeftTree"].document.getElementById('penapproval').value == 'True')
	{
		alert("A worksheet with status 'Pending Approval' exists on this claim. A new worksheet may not be created until that worksheet has been either approved or rejected.");
		return false;
	}
	else if (self.parent.frames["LeftTree"].document.getElementById('pending').value == 'True')
	{
		alert("A worksheet with the status 'Pending' has already been created. You may select that worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
		return false;
	}
	else if (self.parent.frames["LeftTree"].document.getElementById('rejected').value == 'True')
	{
		alert("A worksheet with the status of 'Rejected' is present.  You may select that worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
		return false;
	}
	else if (self.parent.frames["LeftTree"].document.getElementById('approved').value != '0')
	{
		ret = false;
		ret = confirm("A current 'Approved' worksheet is present. Click 'OK' to create a new worksheet with values from the approved worksheet. Click on 'Cancel' to create a new worksheet from scratch.");
		if (ret)
			location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&RSWType='+document.getElementById('RSWType').value+'&FromApproved='+self.parent.frames["LeftTree"].document.getElementById('approved').value+'&claimid='+iClaimId;
		else
			location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&RSWType='+document.getElementById('RSWType').value+'&claimid='+iClaimId;
		if(oldNodId!="")
			document.all["hyp-"+oldNodId].style.cssText="FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;COLOR: black;BACKGROUND-COLOR: none"    
		oldNodId="";		
		ShowPlsWait();
		return false;
	}
	location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&RSWType='+document.getElementById('RSWType').value+'&claimid='+iClaimId;
	if(oldNodId!="")
		document.all["hyp-"+oldNodId].style.cssText="FONT-SIZE: 10pt;FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif;HEIGHT: 10px;text-decoration:none;COLOR: black;BACKGROUND-COLOR: none" 
	oldNodId="";		
	ShowPlsWait();
	return false;
}

function RSW_Radio_Change(ctl)
{	
	var s=ctl.value;

	//Update Date Submitted
	var ctrl=document.getElementById('dtSubmit_'+s);
	if(ctrl!=null)
		document.getElementById('DateSubmitted').innerText='Date Submitted: '+ctrl.value;
	
	//Update Submitted by
	var ctrl=document.getElementById('submitBy_'+s);
	if(ctrl!=null)
		document.getElementById('SubmittedBy').innerText='Submitted By: '+ctrl.value;	
}

function ToggleCheck(obj)
{
	if (obj.id == "statuteLim")
	{
		if (obj.checked == true)
			document.getElementById('openRep').checked = false;
	}
	else if (obj.id == "openRep")
	{
		if (obj.checked == true)
			document.getElementById('statuteLim').checked = false;
	}
	return false;
}	

function RefreshLeftTree()
{
	var Status = document.getElementById('hdnReqStatus');

	var ctlUnderLimit = document.getElementById('hdnUnderLimit');
	if (ctlUnderLimit != null && ctlUnderLimit.value != "")
	{
	  if (ctlUnderLimit.value == "Yes")
	  {
	    var ret = false;
		ret = confirm("Reserves are within your approval authority. Worksheet does not need to be submitted for further approval. \r\nClick 'OK' to Approve the worksheet and apply the new reserves to the Claim or \r\nClick 'Cancel' to close this message and then click 'Approve' to Approve the worksheet and apply the new reserves to the Claim.");
		if (ret)
		{
			self.parent.frames["BottomFrame"].document.getElementById('RefreshLeftTree').value = "Y";
			document.getElementById('hdnAction').value = "Approve";
			document.forms[0].submit();
			ShowPlsWait();
			return false;
		}
	  }
	  else if (ctlUnderLimit.value == "No")
	  {
	    alert("Reserves exceed your authority limits. The worksheet is being sent to your supervisor for approval.");
	  }
	  else if (ctlUnderLimit.value == "NoSupervisor")
	  {
	    alert("Reserves exceed your authority limits and you don't have any supervisor. The worksheet will only be approved/rejected by your supervisor.");
	  }
	  else if (ctlUnderLimit.value == "Reject")
	  {
	    alert("Reserves are within your approval authority. Worksheet has been Rejected.  After closing this message, You may select this Rejected worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
	    location.href = "home?pg=riskmaster/Reserves/ReserveWorksheet/DefaultBlankMain";
	  }
	  else if (ctlUnderLimit.value == "NoAuthEdit")
	  {
	    alert("You are not authorized to edit this worksheet.");
	  }
	  else if (ctlUnderLimit.value == "NoAuthSubmit")
	  {
	    alert("You are not authorized to submit this worksheet.");
	  }
	  else if (ctlUnderLimit.value == "Approve") // When a worksheet has been approved.
	    self.parent.window.opener.location.reload(true); // refreshing parent window.
	  else if (ctlUnderLimit.value == "Approve") // When a worksheet has been approved.
	    self.parent.window.opener.location.reload(true); // refreshing parent window.
	    
	  ctlUnderLimit.value = "";
	}
	
	if (self.parent.frames["BottomFrame"].document.getElementById('RefreshLeftTree') != null)
	{
		if (self.parent.frames["BottomFrame"].document.getElementById('RefreshLeftTree').value == "Y")
		{
			self.parent.frames["BottomFrame"].document.getElementById('RefreshLeftTree').value = "N";
			if (document.getElementById('hdnRSWid') != null && document.getElementById('hdnClaimId') != null) // worksheet loaded
			{
				self.parent.frames["LeftTree"].location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/LeftFrame&RSWType='
					+ self.parent.frames["BottomFrame"].document.getElementById('RSWType').value
					+ '&id='
					+ document.getElementById('hdnRSWid').value
					+ '&claimid='
					+ document.getElementById('hdnClaimId').value;
			}
			else if (self.parent.frames["BottomFrame"].document.getElementById('claimid') != null)
			{
				self.parent.frames["LeftTree"].location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/LeftFrame&RSWType='
					+ self.parent.frames["BottomFrame"].document.getElementById('RSWType').value
					+ '&claimid='
					+ self.parent.frames["BottomFrame"].document.getElementById('claimid').value;
			}
			return false;
		}
	}
	return false;
}

function SaveData() {  
    var ret = false;
	var bCheckRatesBSave;
	var bCheckMedicalWCBSave;
	var bCheckTempDisWCBSave;
	var bCheckPermDisWCBSave;
	var bCheckVocRehabWCBSave;
	var bCheckLegalWCBSave;
	var bCheckAllExpWCBSave;

//	debugger;

	var ctrlstatus = document.getElementById('hdnRSWStatus'); // Get the status of the worksheet open.
	//shobhana
	if (document.getElementById('RswTypeName').value != null)
	 {
	     var sRSWType = document.getElementById('RswTypeName').value;
	     if (sRSWType == "Generic")
	      {
	          alert("A Reserve Worksheet with the status '" + ctrlstatus.value + "' can not be changed and saved.");
	         return false;
	     }

  	}
	if (ctrlstatus != null)
	{
	    bCheckRatesBSave = CheckRatesBSave();
	    bCheckMedicalWCBSave = CheckMedicalWCBSave();
	    bCheckTempDisWCBSave = CheckTempDisWCBSave();
	    bCheckPermDisWCBSave = CheckPermDisWCBSave();
	    bCheckVocRehabWCBSave = CheckVocRehabWCBSave();
	    bCheckLegalWCBSave = CheckLegalWCBSave();
	    bCheckAllExpWCBSave = CheckAllExpWCBSave();
	    //debugger;
	  

	     if(document.getElementById('CommPerWC')!=null)
	  {  if(document.getElementById('CommPerWC').value!='')
	          document.getElementById('hdn_CommPerWC').value = document.getElementById('CommPerWC').value;
	    }
	    	     if(document.getElementById('CommTemWC')!=null)
	    	     {
	       if(document.getElementById('CommTemWC').value!='')
	    document.getElementById('hdn_CommTemWC').value=document.getElementById('CommTemWC').value;
	    }
	     if(document.getElementById('CommLegWC')!=null)
	     {
	       if(document.getElementById('CommLegWC').value!='')
	           document.getElementById('hdn_CommLegWC').value = document.getElementById('CommLegWC').value;
	    }
	     if(document.getElementById('CommAllWC')!=null)
	     {
	       if(document.getElementById('CommAllWC').value!='')
	           document.getElementById('hdn_CommAllWC').value = document.getElementById('CommAllWC').value;
	    }
	    if(document.getElementById('CommVocWC')!=null)
	    {
	       if(document.getElementById('CommVocWC').value!='')
	           document.getElementById('hdn_CommVocWC').value = document.getElementById('CommVocWC').value;
	    }
	     if(document.getElementById('CommMedWC')!=null)
	     {
	       if(document.getElementById('CommMedWC').value!='')
	           document.getElementById('hdn_CommMedWC').value = document.getElementById('CommMedWC').value;
	    }
	     if(document.getElementById('txt_RESERVESUMMARY_Comments')!=null)
	     {
	       if(document.getElementById('txt_RESERVESUMMARY_Comments').value!='')
	           document.getElementById('hdn_RESERVESUMMARY_Comments').value = document.getElementById('txt_RESERVESUMMARY_Comments').value;
	    
	    }
	    
    	    if ((!bCheckRatesBSave && !bCheckMedicalWCBSave && !bCheckTempDisWCBSave && !bCheckPermDisWCBSave && !bCheckVocRehabWCBSave && !bCheckLegalWCBSave && !bCheckAllExpWCBSave))
	        {
                alert("No reserve amounts have been entered or reserves have not changed as compared to existing reserves for all reserve categories. This reserve worksheet cannot be saved.");
		        return false;
	        }
	    	    else 
	    {
            if (!ValidateReasonsAndComments()) 
	        {
	            return false;
	        }
	    }
	    //skhare7 Mits 19027
	    if (!ValidatePreModResTOZeroSetting()) {
	        return false;

	    }
	    if (ctrlstatus.value == "New" || ctrlstatus.value == "Pending" || ctrlstatus.value == "Rejected")
		{
		    pleaseWait.Show();
		    m_bConfirmSave = false;
		    return true;
		}
		else
		{
			alert("A Reserve Worksheet with the status '" + ctrlstatus.value + "' can not be changed and saved.");
			return false;
		}
}


	return false;
}
//skhare7 Mits 19027

function ValidatePreModResTOZeroSetting() {

    var hdnCtrlToClear = document.getElementById('hdnCtrlPreResModZero');
    var key = null;
    var arrReserve = null;
    var arrFlag = null;
    var arrCtrl = null;
    var txtCommentsControl = null;
    var hdnCommentsControl = null;
    var sReserveAmount;
    var bValidated = false;


 //   debugger;


    if (hdnCtrlToClear != null) {
        arrCtrl = hdnCtrlToClear.value.split(",");
        for (var i = 0; i < m_TabList.length; i++) {
            key = m_TabList[i].id.substring(4);
            resvalue = key.substring(0, 3);
           // NewIncAmtMedWC
            transTypeControl = document.getElementById('NewIncAmt' + resvalue + 'WC');
            if (transTypeControl != null) {
                RSW_MultiCurrencyToDecimal(transTypeControl);
                for (var count = 0; count < arrCtrl.length; count++) {
                    if (transTypeControl.id == arrCtrl[count]) {
                        //sReserveAmount = (transTypeControl.value).replace(/[,\$]/g, "");
                        sReserveAmount = transTypeControl.getAttribute("Amount");
                        if (transTypeControl.getAttribute("Amount") == "0" || transTypeControl.getAttribute("Amount") == "0.00") 
                        {
                            sReserveAmount = 0;
                        }
                        else 
                        {

                            sReserveAmount = parseFloat(sReserveAmount);
                        }
                        if (document.getElementById('hdnPrevModValtoZero') != null) {


                            if (sReserveAmount <= 0 && document.getElementById('hdnPrevModValtoZero').value == "True") {
                                tabChange(key);
                                if (!confirm("The Reserve amount should be greater than 0. Do you really want to modify this reserve to 0?")) {  //  arrFlag
                                    bValidated = false;
                                    return bValidated;
                                }
                            }



                        }


                    }
                }
            }
        }

    }
    bValidated = true;
    return bValidated;
}
//end

function GetSelectedRSWid()
{
	inputs = document.all.tags("input");
	for (i = 0; i < inputs.length; i++)
	{
		if ((inputs[i].type=="radio") && (inputs[i].checked==true))
		{
			inputid=inputs[i].value;
			return inputid;
		}
	}
}

function DeleteWorksheet()
{
//debugger;
	 var ret = false;
	var ctrl = document.getElementById('hdnRSWStatus'); // Get the status of the worksheet open.

	if (ctrl != null)
	 {

	     if (document.getElementById('RswTypeName') != null)
	     {
	        var sRSWType = document.getElementById('RswTypeName').value;
	        if (sRSWType == "Generic") 
	        {
	            alert("A Reserve Worksheet with the status approved can not be changed and saved.");
	            return false;
	        }

	    }

		if (ctrl.value == "New")
		{
			ret = confirm("This worksheet has not been saved yet. Do you want to clear the data?");
			if (ret)
				ClearData();
			return false;
		}
		else if (ctrl.value == "Pending" || ctrl.value == "Rejected" || ctrl.value == "Rejected (Edit Mode)") // Alert the user and then delete the worksheet
		{		   
			return DeleteRecord();
		}
		else
		{
			alert("A Reserve Worksheet with the status '" + ctrl.value + "' can not be deleted.");
			return false;
		}
	}

	return false;
}

function ClearData()
{
    var ctrl;
	//shobhana add new hidden field
	var ClearControls = document.getElementById('hdnClearRecord');
		
	var ctrlStatus = document.getElementById('hdnRSWStatus');
	if (ctrlStatus != null)
	{
		if (ctrlStatus.value == "New" || ctrlStatus.value == "Pending" || ctrlStatus.value == "Rejected")
		{
			var ret = confirm("Are you sure, you want to clear the worksheet?");
			if (!ret)
				return false;
			
			if (ClearControls != null)
			{
				var arrCtrls = ClearControls.value.split(",");
				for (var count = 0; count < arrCtrls.length; count ++)
				{
					ctrl = document.getElementById(arrCtrls[count]);
					if (ctrl != null)
					{
						switch (ctrl.type)
						{
							case 'textarea':
							case 'text':
							case 'select-one':
								ctrl.value = "";
								break;
							case 'checkbox':
								ctrl.checked = false;
								break;
							default:
								ctrl.value = "";
								break;
						} // end of switch case
					}
				} // end of for loop
			}
			
			if (document.getElementById("CommMedWC") != null)
			    document.getElementById("CommMedWC").value = "";//Medical Comment
			if (document.getElementById("CommTemWC") != null)
			    document.getElementById("CommTemWC").value = "";//Temporary Disability
            if (document.getElementById("CommPerWC") != null)
			    document.getElementById("CommPerWC").value = "";//Permanent Disability
			if (document.getElementById("CommVocWC") != null)
			    document.getElementById("CommVocWC").value = "";//Vocational Rehabilitation
			if (document.getElementById("CommLegWC") != null)
			    document.getElementById("CommLegWC").value = "";
			if (document.getElementById("CommAllWC") != null)
			    document.getElementById("CommAllWC").value = "";
			if (document.getElementById("CommResSummWC") != null)
    			document.getElementById("CommResSummWC").value = "";
			
			if (document.getElementById("NewIncAmtMedWC") != null)
			    document.getElementById("NewIncAmtMedWC").value = (document.getElementById("hdnIncurredMedWC").value);
			if (document.getElementById("NewIncAmtTemWC") != null)
			    document.getElementById("NewIncAmtTemWC").value = (document.getElementById("hdnIncurredTemWC").value);
            if (document.getElementById("NewIncAmtPerWC") != null)
			    document.getElementById("NewIncAmtPerWC").value = (document.getElementById("hdnIncurredPerWC").value);
			if (document.getElementById("NewIncAmtVocWC") != null)
			    document.getElementById("NewIncAmtVocWC").value = (document.getElementById("hdnIncurredVocWC").value);
			if (document.getElementById("NewIncAmtLegWC") != null)
			    document.getElementById("NewIncAmtLegWC").value = (document.getElementById("hdnIncurredLegWC").value);
			if (document.getElementById("NewIncAmtAllWC") != null)
			    document.getElementById("NewIncAmtAllWC").value = (document.getElementById("hdnIncurredAllWC").value);
			
			if (document.getElementById("NewBalAmtMedWC") != null)
			    document.getElementById("NewBalAmtMedWC").value = (document.getElementById("hdnBalAmtMedWC").value);
			if (document.getElementById("NewBalAmtTemWC") != null)
			    document.getElementById("NewBalAmtTemWC").value = (document.getElementById("hdnBalAmtTemWC").value);			    
            if (document.getElementById("NewBalAmtPerWC") != null)
			    document.getElementById("NewBalAmtPerWC").value = (document.getElementById("hdnBalAmtPerWC").value);			    
			if (document.getElementById("NewBalAmtVocWC") != null)
			    document.getElementById("NewBalAmtVocWC").value = (document.getElementById("hdnBalAmtVocWC").value);			    
			if (document.getElementById("NewBalAmtLegWC") != null)
			    document.getElementById("NewBalAmtLegWC").value = (document.getElementById("hdnBalAmtLegWC").value);			    
			if (document.getElementById("NewBalAmtAllWC") != null)
			    document.getElementById("NewBalAmtAllWC").value = (document.getElementById("hdnBalAmtAllWC").value);
						
            CreateReserveSummary();    
						
		}
		else
		{
			alert("Cannot Clear Data when the Worksheet status is '" + ctrlStatus.value + "'.");
		}
	}	
	return false;
}

function SubmitSheet()
{

//debugger;
    var ctrlStatus = document.getElementById('hdnRSWStatus');
    //shobhana
    
    
    //smahajan6m
	if(m_DisableSubmit)
	{
		alert("A Reserve Worksheet has already been submitted.");
		return false;
	}
	//smahajan6m
    
    var bCheckRatesBSave;
	var bCheckMedicalWCBSave;
	var bCheckTempDisWCBSave;
	var bCheckPermDisWCBSave;
	var bCheckVocRehabWCBSave;
	var bCheckLegalWCBSave;
	var bCheckAllExpWCBSave;

	if (ctrlStatus != null) {
	    if (document.getElementById('RswTypeName')!= null) {
	        var sRSWType = document.getElementById('RswTypeName').value;
	        if (sRSWType == "Generic") {
	            alert("A Reserve Worksheet with the status approved can not be changed and saved.");
	            return false;
	        }

	    }
   
		//A pending approval worksheet can be resubmitted by the manager.
		//In this case no validation is required since its already validated and furthermore the fields are readonly.
		//The need is only to re-submit the same to that it goes to his manager further
		bCheckRatesBSave = CheckRatesBSave();
	    bCheckMedicalWCBSave = CheckMedicalWCBSave();
	    bCheckTempDisWCBSave = CheckTempDisWCBSave();
	    bCheckPermDisWCBSave = CheckPermDisWCBSave();
	    bCheckVocRehabWCBSave = CheckVocRehabWCBSave();
	    bCheckLegalWCBSave = CheckLegalWCBSave();
	    bCheckAllExpWCBSave = CheckAllExpWCBSave();
	    // debugger;
	  

	    if(document.getElementById('CommPerWC')!=null)
	  {  if(document.getElementById('CommPerWC').value!='')
	          document.getElementById('hdn_CommPerWC').value = document.getElementById('CommPerWC').value;
	    }
	    	     if(document.getElementById('CommTemWC')!=null)
	    	     {
	       if(document.getElementById('CommTemWC').value!='')
	           document.getElementById('hdn_CommTemWC').value = document.getElementById('CommTemWC').value;
	    }
	     if(document.getElementById('CommLegWC')!=null)
	     {
	       if(document.getElementById('CommLegWC').value!='')
	           document.getElementById('hdn_CommLegWC').value = document.getElementById('CommLegWC').value;
	    }
	     if(document.getElementById('CommAllWC')!=null)
	     {
	       if(document.getElementById('CommAllWC').value!='')
	           document.getElementById('hdn_CommAllWC').value = document.getElementById('CommAllWC').value;
	    }
	    if(document.getElementById('CommVocWC')!=null)
	    {
	       if(document.getElementById('CommVocWC').value!='')
	           document.getElementById('hdn_CommVocWC').value = document.getElementById('CommVocWC').value;
	    }
	     if(document.getElementById('CommMedWC')!=null)
	     {
	       if(document.getElementById('CommMedWC').value!='')
	           document.getElementById('hdn_CommMedWC').value = document.getElementById('CommMedWC').value;
	    }
	     if(document.getElementById('txt_RESERVESUMMARY_Comments')!=null)
	     {
	       if(document.getElementById('txt_RESERVESUMMARY_Comments').value!='')
	           document.getElementById('hdn_RESERVESUMMARY_Comments').value = document.getElementById('txt_RESERVESUMMARY_Comments').value;
	    
	    }
	    
    	
	        if ((!bCheckRatesBSave && !bCheckMedicalWCBSave && !bCheckTempDisWCBSave && !bCheckPermDisWCBSave && !bCheckVocRehabWCBSave && !bCheckLegalWCBSave && !bCheckAllExpWCBSave))
	        {
            	alert("No reserve amounts have been entered or reserves have not changed in this worksheet as compared to existing reserves for all reserve categories. This worksheet cannot be submitted for approval.");
		       	return false;
	        }
	        else 
	       {
            if (!ValidateReasonsAndComments()) 
	        {
	            return false;
	        }
	    }
	    //skhare7 MITS 19027
	    if (!ValidatePreModResTOZeroSetting()) {
	        return false;

	    }
        
			
		
 if (document.getElementById('hdnCurrentUser').value == document.getElementById('hdnSubmittedTo').value)
		{
		        m_DisableSubmit = true; //smahajan6m
		        pleaseWait.Show();
			    return true;  //submit
        }
                   			
    if (ctrlStatus.value == "Pending")   //|| ctrlStatus.value == "Pending Approval") // submit only when the status is pending/ || ctrlStatus.value == "Rejected (Edit Mode)"
		{
		    m_DisableSubmit = true; //smahajan6m
		    m_bConfirmSave = false;
		    pleaseWait.Show();
		    return true;
        }		else
			alert("A Reserve Worksheet with the status '" + ctrlStatus.value + "' can not be submitted.");
	}
    return false;	
}
//changed by shobhana 
function ValidateReasonsAndComments() 
{
    var arrCtrls = null;
    var reserveTypeCommentControl = null;
    var rserverSummaryComment = null;
    var bValidated = null;
  //  debugger;
    
    //nnorouzi; MITS 18989 and cleaning up the code start    
    var key = null;
    var controlToBeChecked = null;
    var reasonCommentsAlert = "Please fill in the Reason and/or Comments!"
    var reasonCommentsRequired;
var resvalue=null;

    reasonCommentsRequired = document.getElementById('hdn_ReasonCommentsRequired');
    if (String(true) == reasonCommentsRequired.value.toLowerCase()) {
        for (var i = 0; i < m_TabList.length; i++) {
            key = m_TabList[i].id.substring(4);
            resvalue=key.substring(0,3);
          //  ResReasonTemWC_codelookup
            if (m_arrReasonsToBeChecked[key]) {
                controlToBeChecked = document.getElementById('ResReason' + resvalue + 'WC_codelookup');
                if (controlToBeChecked && controlToBeChecked.value == '') {
                    tabChange(key);
                    alert(reasonCommentsAlert);
                    return false;
                }//CommPerWC
            }
            if (m_arrCommentsToBeChecked[key]) {
                controlToBeChecked = document.getElementById('Comm' + resvalue + 'WC');
                if (controlToBeChecked && controlToBeChecked.value == '') {
                    tabChange(key);
                    alert(reasonCommentsAlert);
                    return false;
                }
            }
            if (i == m_TabList.length - 1) {
                controlToBeChecked = document.getElementById('txt_RESERVESUMMARY_Comments');
                if (controlToBeChecked && controlToBeChecked.value == '') {
                    if (!m_ReserveSummaryCreated) {
                        CreateReserveSummary();
                    }
                    tabChange(key);
                    alert(reasonCommentsAlert);
                    return false;
                }
            }
        }
    }
    //nnorouzi; MITS 18989 and cleaning up the code end    
    
    return true;
}


// Called from the main Reserve Worksheet, rather than the Approval Screen
function Approve() {    
    var ctrlStatus = document.getElementById('hdnRSWStatus');

 	
	var bCheckRatesBSave;
	var bCheckMedicalWCBSave;
	var bCheckTempDisWCBSave;
	var bCheckPermDisWCBSave;
	var bCheckVocRehabWCBSave;
	var bCheckLegalWCBSave;
	var bCheckAllExpWCBSave;

	if (ctrlStatus != null) 
	{//shobhana
	    if (document.getElementById('RswTypeName')!= null) 
	    {
	        var sRSWType = document.getElementById('RswTypeName').value;
	        if (sRSWType == "Generic") {
	            alert("A Reserve Worksheet with the status approved can not be changed and saved.");
	            return false;
	        }

	    }

		//Submit only when the status is Pending Approval
		bCheckRatesBSave = CheckRatesBSave();
	    bCheckMedicalWCBSave = CheckMedicalWCBSave();
	    bCheckTempDisWCBSave = CheckTempDisWCBSave();
	    bCheckPermDisWCBSave = CheckPermDisWCBSave();
	    bCheckVocRehabWCBSave = CheckVocRehabWCBSave();
	    bCheckLegalWCBSave = CheckLegalWCBSave();
	    bCheckAllExpWCBSave = CheckAllExpWCBSave();
	    

   	     if(document.getElementById('CommPerWC')!=null)
	  {  if(document.getElementById('CommPerWC').value!='')
	          document.getElementById('hdn_CommPerWC').value = document.getElementById('CommPerWC').value;
	    }
	    	     if(document.getElementById('CommTemWC')!=null)
	    	     {
	       if(document.getElementById('CommTemWC').value!='')
	           document.getElementById('hdn_CommTemWC').value = document.getElementById('CommTemWC').value;
	    }
	     if(document.getElementById('CommLegWC')!=null)
	     {
	       if(document.getElementById('CommLegWC').value!='')
	           document.getElementById('hdn_CommLegWC').value = document.getElementById('CommLegWC').value;
	    }
	     if(document.getElementById('CommAllWC')!=null)
	     {
	       if(document.getElementById('CommAllWC').value!='')
	           document.getElementById('hdn_CommAllWC').value = document.getElementById('CommAllWC').value;
	    }
	    if(document.getElementById('CommVocWC')!=null)
	    {
	       if(document.getElementById('CommVocWC').value!='')
	           document.getElementById('hdn_CommVocWC').value = document.getElementById('CommVocWC').value;
	    }
	     if(document.getElementById('CommMedWC')!=null)
	     {
	       if(document.getElementById('CommMedWC').value!='')
	           document.getElementById('hdn_CommMedWC').value = document.getElementById('CommMedWC').value;
	    }
	     if(document.getElementById('txt_RESERVESUMMARY_Comments')!=null)
	     {
	       if(document.getElementById('txt_RESERVESUMMARY_Comments').value!='')
	           document.getElementById('hdn_RESERVESUMMARY_Comments').value = document.getElementById('txt_RESERVESUMMARY_Comments').value;
	    
	    }
	    
    	    if ((!bCheckRatesBSave && !bCheckMedicalWCBSave && !bCheckTempDisWCBSave && !bCheckPermDisWCBSave && !bCheckVocRehabWCBSave && !bCheckLegalWCBSave && !bCheckAllExpWCBSave))
	        
	        {
            	alert("No reserve amounts have been entered or reserves have not changed in this worksheet as compared to existing reserves for all reserve categories. This worksheet cannot be approved. Please 'Reject' this worksheet and select this rejected worksheet from the left pane and either (a) continue editing it and then submit it for approval; or (b) delete it, and then create a new worksheet.");
		       	return false;
	        }
			    else 
	    {
            if (!ValidateReasonsAndComments()) 
	        {
	            return false;
	        }
	    }
	    //skhare7 MITS 19027
//	    if (!ValidatePreModResTOZeroSetting()) {
//	        return false;

//	    }
        
		
		if (ctrlStatus.value == "Pending Approval" || ctrlStatus.value == "Pending") 
		{
		    m_bConfirmSave = false;
		    pleaseWait.Show();
		    //commented by Nitin for Mits 18882
		    //parent.MDISetWipedOut();
		    return true;
		}
		else
			alert("A Reserve Worksheet with the status '" + ctrlStatus.value + "' can not be approved.");
	}
	
	return false;
}

function trimAll(sString) 
{
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}

function Reject()
{
	var ctrlStatus = document.getElementById('hdnRSWStatus');
	if (ctrlStatus != null) 
	{//shobhana
	    if (document.getElementById('RswTypeName') != null)
	     {
	        var sRSWType = document.getElementById('RswTypeName').value;
	        if (sRSWType == "Generic") {
	            alert("A Reserve Worksheet with the status approved can not be changed and saved.");
	            return false;
	        }

	    }

		if (ctrlStatus.value == "Pending Approval") // reject only when the status is pending approval
		{
			m_bConfirmSave = false;
		    pleaseWait.Show();
			return true;
		}
		else
			alert("A Reserve Worksheet with the status '" + ctrlStatus.value + "' can not be rejected.");
	}
	
	return false;
}

//By Vaibhav for Safeway reserve worksheet
function ApproveFromApprovalScreen()
{
    var bNoRecord= false;
    var bRadio = document.all.tags("input");
        
        for( var i = 0 ; i < bRadio.length - 1 ; i++ )
          {
              if( bRadio[i].type == "radio" )
              {
	              bNoRecord = true;;
	              break;
              }
         }
         
         
    if(!bNoRecord)
    {
        alert('Only an existing Reserve Worksheet can be Approved.');
        return false;
    }
    //By Vaibhav for Safeway MITS # 13938
    return checkCharacterLength();
}

function RejectFromApprovalScreen()
{
    var bNoRecord= false;
    var bRadio = document.all.tags("input");
        
        for( var i = 0 ; i < bRadio.length - 1 ; i++ )
          {
              if( bRadio[i].type == "radio" )
              {
	              bNoRecord = true;;
	              break;
              }
         }
         
         
    if(!bNoRecord)
    {
        alert('Only an existing Reserve Worksheet can be Rejected.');
        return false;
    }
    //By Vaibhav for Safeway MITS # 13938
    return checkCharacterLength();
}

//By Vaibhav for Safeway MITS # 13938 : start
function checkCharacterLength()
{
    var maxLength = 255;
    if(document.getElementById("txtAppRejReqCom").value.length > maxLength)
    {
        alert('You cannot enter more than 255 characters in Reason field.');
        return false;
    }
}

function EditWorkSheet()
{

    var iClaimId = "-1";
    var bNoRecord= false;
    var bRadio = document.all.tags("input");
        
        for( var i = 0 ; i < bRadio.length - 1 ; i++ )
          {
              if( bRadio[i].type == "radio" )
              {
	              bNoRecord = true;;
	              break;
              }
         }
         
         
    if(!bNoRecord)
    {
        alert('Only an existing Reserve Worksheet can be Opened.');
        return false;
    } 
      
    iClaimId = document.getElementById('claimid').value;
    iRSWId = document.getElementById('hdnRswId').value;
	    
    if (iClaimId == "-1")
	    alert("Only an existing Reserve Worksheet can be Opened.");
    else
    {
        if (iRSWId == "-1")
        {
		    m_EditMode = true;
            window.open('home?pg=riskmaster/Reserves/ReserveWorksheet/ReserveWorksheet&amp;RSWType=CRW&amp;claimid='+iClaimId,'ReserveWorksheet',
		    'width=950,height=700'+',top='+(screen.availHeight-700)/2+',left='+(screen.availWidth-950)/2+',resizable=no,scrollbars=yes');
		}
	    else
	    {
			m_EditMode = true;
            window.open('home?pg=riskmaster/Reserves/ReserveWorksheet/ReserveWorksheet&amp;RSWType=CRW&amp;claimid='+iClaimId+'&amp;rswid='+iRSWId,'ReserveWorksheet',
		    'width=950,height=700'+',top='+(screen.availHeight-700)/2+',left='+(screen.availWidth-950)/2+',resizable=no,scrollbars=yes');
	    }    
    }
    
   return false;
}

function CheckParentScreen()
{
    //get the current URL 
    var url = window.location.toString();
    //get the parameters 
    url.match(/\?(.+)$/); 
    var params = RegExp.$1; 
    //split up the query string and store in an associative array 
    var params = params.split("&"); 
    var queryStringList = {};  
    for(var i=0;i<params.length;i++) 
    {     
        var tmp = params[i].split("=");     
        queryStringList[tmp[0]] = unescape(tmp[1]);
        if (unescape(tmp[0]) == "rswid")
        {
            if (unescape(tmp[1]) != "")
                location.href = 'home?pg=riskmaster/Reserves/ReserveWorksheet/MainFrame&rswid='+unescape(tmp[1]);
                
        }
    }  
}

function PrintWorksheet()
{

    var iRSWId = "-1";
    var rswtypename = "";
    var sRSWCustomXML = "";
    
    sRSWCustomXML = document.getElementById('hdnCustomTransXML').value;
    //By Vaibhav for Reserve worksheet : Added if condition
////    if(document.title== "Reserve Approval")
////    {
////        iRSWId = document.getElementById('hdnRswId').value;
////		    
////	    if (iRSWId == "-1")
////		    alert("Only an existing Reserve Worksheet can be Printed.");
////	    else
////	    {
////		    var sQueryString = "rswid="+ iRSWId;
////		    window.open('home?pg=riskmaster/Reserves/ReserveWorksheet/PrintWorksheet&amp;' + sQueryString, 'PrintRSW',
////			    'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');
////	    }
////    }
////    //Vaibhav code end
////    else
////    {
////	    var ctrlStatus = document.getElementById('hdnRSWStatus');	

////	    if (ctrlStatus != null)
////	    {
////		    iRSWId = document.getElementById('hdnRSWid').value;
////		    if (iRSWId == "-1")
////			    alert("Only an existing Reserve Worksheet can be printed. Please save the Worksheet in order to print it.");
////		    else
////		    {
////			    var sQueryString = "rswid="+ iRSWId;
////			    window.open('home?pg=riskmaster/Reserves/ReserveWorksheet/PrintWorksheet&amp;' + sQueryString, 'PrintRSW',
////				    'width=650,height=550,top='+(screen.availHeight-550)/2+',left='+(screen.availWidth-650)/2+',resizable=yes,scrollbars=yes');
////		    }
////	    }
////	}
////						
////	return false;
    if (document.getElementById('hdnRSWid') != null && document.getElementById('RswTypeName') != null) {//debugger;
//        if (document.getElementById('RswTypeName').value != null) 
//        {
//            var sRSWType = document.getElementById('RswTypeName').value;
//            if (sRSWType == "Generic")
//             {
//              
//                return false;
//            }

//        }

		  iRSWId = document.getElementById('hdnRSWid').value;
		  rswtypename = document.getElementById('RswTypeName').value;
        if (iRSWId == "-1")
            alert("Only an existing Reserve Worksheet can be printed. Please save the Worksheet in order to print it.");
        else 
        {
            var sQueryString = "rswid=" + iRSWId;
            window.open('/RiskmasterUI/UI/ReserveWorksheet/PrintWorksheet.aspx?' + sQueryString + '&RSWCustomXML=' + sRSWCustomXML + '&RswTypeName=' + rswtypename, 'PrintRSW',
		    'width=650,height=550,top=' + (screen.availHeight - 550) / 2 + ',left=' + (screen.availWidth - 650) / 2 + ',resizable=yes,scrollbars=yes');
        }
	}
    return false;

}

function Tree_Load()
{
var scrType="";
var oCtrl = document.getElementById('selectedID');
if(oCtrl!=null)
	oldNodId= oCtrl.value;
else
	oldNodId="";
	
oCtrl = document.getElementById('scr');	
if(oCtrl.value=='ra')
	{
		oCtrl = document.getElementById('RSWType');
		switch(oCtrl.value)
		{
			case "CRW":
				scrType="clm";
				break;
		}
		OpenWorkSheet(oldNodId,scrType);
	}
}

/*function onSaveMedReserve()//Mohit
{
	var NewMedRes = document.getElementById('NewMedResAmt').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnMedList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var MedInc = window.opener.document.getElementById('hdnMedIncurred').value;
	var BalMedRes = window.opener.document.getElementById('MedBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewMedRes));
	window.opener.document.getElementById('hdnMedList').value = before+list+after;
	ival = parseInt(ival) + 1;
	window.opener.document.getElementById('MedList|'+ ival +'|NewMedResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewMedRes));

	for (var i = 2; i < 13; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('MedList|'+ i +'|NewMedResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewMedInc').innerText = ConvertDblToCurr(ConvertStrToDbl(MedInc) + TotNewAmt);
	window.opener.document.getElementById('NewMedBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalMedRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

/*function onSaveIndReserve()//Mohit
{
	var NewIndRes = document.getElementById('NewIndResAmt').value;
	var IndWeeks = document.getElementById('IndWeeks').value;
	var IndRates = document.getElementById('IndRate').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnIndList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var IndInc = window.opener.document.getElementById('hdnIndIncurred').value;
	var BalIndRes = window.opener.document.getElementById('IndBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewIndRes));
	window.opener.document.getElementById('hdnIndList').value = before+list+after;
	ival = parseInt(ival) + 1;
	
	if (parseInt(ival) != 9 && parseInt(ival) != 10){
	window.opener.document.getElementById('IndList|'+ ival +'|IndWeeks|TD|').innerText = ConvertStrToDbl(IndWeeks);
	window.opener.document.getElementById('IndList|'+ ival +'|IndRate|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(IndRates));
	}	
	window.opener.document.getElementById('IndList|'+ ival +'|NewIndResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewIndRes));

	for (var i = 2; i < 11; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('IndList|'+ i +'|NewIndResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewIndInc').innerText = ConvertDblToCurr(ConvertStrToDbl(IndInc) + TotNewAmt);
	window.opener.document.getElementById('NewIndBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalIndRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

/*function onSaveRehabReserve()//Mohit
{
	var NewRehabRes = document.getElementById('NewRehabResAmt').value;
	var RehabWeeks = document.getElementById('RehabWeeks').value;
	var RehabRates = document.getElementById('RehabRate').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnRehabList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var RehabInc = window.opener.document.getElementById('hdnRehabIncurred').value;
	var BalRehabRes = window.opener.document.getElementById('RehabBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewRehabRes));
	window.opener.document.getElementById('hdnRehabList').value = before+list+after;
	ival = parseInt(ival) + 1;
	if (parseInt(ival) == 2){
	window.opener.document.getElementById('RehabList|'+ ival +'|RehabWeeks|TD|').innerText = ConvertStrToDbl(RehabWeeks);
	window.opener.document.getElementById('RehabList|'+ ival +'|RehabRate|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(RehabRates));
	}
	window.opener.document.getElementById('RehabList|'+ ival +'|NewRehabResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewRehabRes));

	for (var i = 2; i < 6; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('RehabList|'+ i +'|NewRehabResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewRehabInc').innerText = ConvertDblToCurr(ConvertStrToDbl(RehabInc) + TotNewAmt);
	window.opener.document.getElementById('NewRehabBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalRehabRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

/*function onSaveLegalReserve()//Mohit
{
	var NewLegalRes = document.getElementById('NewLegalResAmt').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnLegalList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var LegalInc = window.opener.document.getElementById('hdnLegalIncurred').value;
	var BalLegalRes = window.opener.document.getElementById('LegalBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewLegalRes));
	window.opener.document.getElementById('hdnLegalList').value = before+list+after;
	ival = parseInt(ival) + 1;
	window.opener.document.getElementById('LegalList|'+ ival +'|NewLegalResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewLegalRes));

	for (var i = 2; i < 5; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('LegalList|'+ i +'|NewLegalResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewLegalInc').innerText = ConvertDblToCurr(ConvertStrToDbl(LegalInc) + TotNewAmt);
	window.opener.document.getElementById('NewLegalBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalLegalRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

//Safeway: Mohit Yadav: Save the Reserve Worksheet for Other reserves
/*function onSaveOthReserve()
{
	var NewOthRes = document.getElementById('NewOthResAmt').value;
	var ival = document.getElementById('selectedrowid').value;
	var hdnList = window.opener.document.getElementById('hdnOthList').value;
	var before = hdnList.substring(0, hdnList.indexOf(";" + ival + "|") + 1);
	var after = "";
	var list = "";
	
	var OthInc = window.opener.document.getElementById('hdnOthIncurred').value;
	var BalOthRes = window.opener.document.getElementById('OthBalAmt').value;
	
	var TotNewAmt = 0.0;
	
	if (hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1) != -1)
		after = hdnList.substring(hdnList.indexOf(';', hdnList.indexOf(";" + ival + "|")+1));
		
	list = ival + "|" + ConvertDblToCurr(ConvertStrToDbl(NewOthRes));
	window.opener.document.getElementById('hdnOthList').value = before+list+after;
	ival = parseInt(ival) + 1;
	window.opener.document.getElementById('OthList|'+ ival +'|NewOthResAmt|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(NewOthRes));

	for (var i = 2; i < 4; i ++)	
	{
		TotNewAmt += ConvertStrToDbl(window.opener.document.getElementById('OthList|'+ i +'|NewOthResAmt|TD|').innerText);
	}
	window.opener.document.getElementById('NewOthInc').innerText = ConvertDblToCurr(ConvertStrToDbl(OthInc) + TotNewAmt);
	window.opener.document.getElementById('NewOthBalAmt').innerText = ConvertDblToCurr(ConvertStrToDbl(BalOthRes) + TotNewAmt);
	// Populate values on 'Reserve Summary' tab
	window.opener.CalcResAna();
	window.close();	
	
	return false;
}*/

/*function LoadValuesMed()//Mohit
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('NewMedResAmt').readOnly = true;
		document.getElementById('NewMedResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	var Desc1 = "MedList|" + iRowID + "|MedResCat|TD|";
	document.getElementById('Desc1').value = window.opener.document.getElementById(Desc1).innerText;
	Desc1 = "MedList|" + iRowID + "|NewMedResAmt|TD|";
	document.getElementById('NewMedResAmt').value = window.opener.document.getElementById(Desc1).innerText;
	return false;
}*/

/*function LoadValuesInd()//Mohit
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('IndWeeks').readOnly = true;
		document.getElementById('IndWeeks').style.backgroundColor = 'silver';
		
		document.getElementById('IndRate').readOnly = true;
		document.getElementById('IndRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewIndResAmt').readOnly = true;
		document.getElementById('NewIndResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	if (iRowID == "2")
	   {
	    document.getElementById('IndRate').readOnly = true;
		document.getElementById('IndRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewIndResAmt').readOnly = true;
		document.getElementById('NewIndResAmt').style.backgroundColor = 'silver';
	   }
	if (iRowID == "4")
	   {
	    document.getElementById('IndRate').readOnly = true;
		document.getElementById('IndRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewIndResAmt').readOnly = true;
		document.getElementById('NewIndResAmt').style.backgroundColor = 'silver';
	   }
	if (iRowID == "9" || iRowID == "10")
	   {
	    document.getElementById('IndWeeks').readOnly = true;
		document.getElementById('IndWeeks').style.backgroundColor = 'silver';
		
		document.getElementById('IndRate').readOnly = true;
		document.getElementById('IndRate').style.backgroundColor = 'silver';
	   }
	var Desc2 = "IndList|" + iRowID + "|IndResCat|TD|";
	document.getElementById('Desc2').value = window.opener.document.getElementById(Desc2).innerText;	
	Desc2 = "IndList|" + iRowID + "|IndWeeks|TD|";
	document.getElementById('IndWeeks').value = window.opener.document.getElementById(Desc2).innerText;
	Desc2 = "IndList|" + iRowID + "|IndRate|TD|";
	document.getElementById('IndRate').value = window.opener.document.getElementById(Desc2).innerText;
	Desc2 = "IndList|" + iRowID + "|NewIndResAmt|TD|";
	document.getElementById('NewIndResAmt').value = window.opener.document.getElementById(Desc2).innerText;
	
	el = document.getElementById('IndWeeks');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", IndWeeks_Blur);
	}
	el = document.getElementById('IndRate');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", IndRate_Blur);
	}
	return false;
}*/

/*function LoadValuesRehab()//Mohit
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('RehabWeeks').readOnly = true;
		document.getElementById('RehabWeeks').style.backgroundColor = 'silver';
		
		document.getElementById('RehabRate').readOnly = true;
		document.getElementById('RehabRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewRehabResAmt').readOnly = true;
		document.getElementById('NewRehabResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	if (iRowID == "2")
	   {
	    document.getElementById('RehabRate').readOnly = true;
		document.getElementById('RehabRate').style.backgroundColor = 'silver';
		
		document.getElementById('NewRehabResAmt').readOnly = true;
		document.getElementById('NewRehabResAmt').style.backgroundColor = 'silver';
	   }
	if (iRowID == "3" || iRowID == "4" || iRowID == "5")
	   {
	    document.getElementById('RehabWeeks').readOnly = true;
		document.getElementById('RehabWeeks').style.backgroundColor = 'silver';
		
		document.getElementById('RehabRate').readOnly = true;
		document.getElementById('RehabRate').style.backgroundColor = 'silver';
	   }
	var Desc3 = "RehabList|" + iRowID + "|RehabResCat|TD|";
	document.getElementById('Desc3').value = window.opener.document.getElementById(Desc3).innerText;	
	Desc3 = "RehabList|" + iRowID + "|RehabWeeks|TD|";
	document.getElementById('RehabWeeks').value = window.opener.document.getElementById(Desc3).innerText;
	Desc3 = "RehabList|" + iRowID + "|RehabRate|TD|";
	document.getElementById('RehabRate').value = window.opener.document.getElementById(Desc3).innerText;
	Desc3 = "RehabList|" + iRowID + "|NewRehabResAmt|TD|";
	document.getElementById('NewRehabResAmt').value = window.opener.document.getElementById(Desc3).innerText;
	
	el = document.getElementById('RehabWeeks');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", RehabWeeks_Blur);
	}
	el = document.getElementById('RehabRate');	
	if(el!=null)
	{
        el.setAttribute("onblur", "");
	    RMX.addEvent(el, "blur", RehabRate_Blur);
	}
	return false;
}*/

/*function LoadValuesLegal()//Mohit
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('NewLegalResAmt').readOnly = true;
		document.getElementById('NewLegalResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	var Desc4 = "LegalList|" + iRowID + "|LegalResCat|TD|";
	document.getElementById('Desc4').value = window.opener.document.getElementById(Desc4).innerText;
	Desc4 = "LegalList|" + iRowID + "|NewLegalResAmt|TD|";
	document.getElementById('NewLegalResAmt').value = window.opener.document.getElementById(Desc4).innerText;
	return false;
}*/

//Safeway: Mohit Yadav: Reserve Worksheet for Other reserves
/*function LoadValuesOth()
{	
	var iRowID = parseInt(document.getElementById('selectedrowid').value);
	var ctrl = window.opener.document.getElementById('Edit'+iRowID);
	
	if (ctrl != null && ctrl.value == "false")
	{
		document.getElementById('NewOthResAmt').readOnly = true;
		document.getElementById('NewOthResAmt').style.backgroundColor = 'silver';
		
		document.getElementById('Save').disabled = true;
	}
	iRowID += 1;
	var Desc5 = "OthList|" + iRowID + "|OthResCat|TD|";
	document.getElementById('Desc5').value = window.opener.document.getElementById(Desc5).innerText;
	Desc5 = "OthList|" + iRowID + "|NewOthResAmt|TD|";
	document.getElementById('NewOthResAmt').value = window.opener.document.getElementById(Desc5).innerText;
	return false;
}*/
//sk
function ConvertStrToDbl(str)
{
	//str = str.replace("$", "");  //Aman Multi Currency --Commented the functionality of this function
	
	//while (str.indexOf(",") != -1)
	//	str = str.replace(",", "");
		
    //if(str.indexOf('(') >= 0)
    //{
     //   str=str.replace(/[\(\)]/g ,"");
	
    //if(str.indexOf('-') >= 0)
   // {
        // str=str.replace(/[\(\)]/g ,"");
//str = str.replace(/[\-\(\)]/g, "");
	 //   str= new String(-1 * ConvertStrToDbl(str));
    //}
		
	if (str == "")
		return 0.0;
	else
		return parseFloat(str);
}

function ConvertDblToCurr(dbl)
{
	//round to 2 places
	dbl=parseFloat(dbl);
	dbl=dbl.toFixed(2);

	//Handle Full Integer Section (Formats in groups of 3 with comma.)
	var base = new String(dbl);
	base = base.substring(0,base.lastIndexOf('.'));
	var newbase="";
	var j = 0;
	var i=base.length-1;
	for(i;i>=0;i--)
	{	if(j==3)
		{
			newbase = "," + newbase;	
			j=0;

		}
		newbase = base.charAt(i) + newbase;	
		j++;
	}
	
	// Handle Fractional Part
	var str = new String(dbl);
	str = str.substring(str.lastIndexOf(".")+1);

	// Reassemble formatted Currency Value string.
	return "$" + newbase + "." + str;
}

function ShowPlsWait()
{
//	var wnd=window.open('csc-Theme\\riskmaster\\common\\html\\progress.html','progressWnd',
//				'width=400,height=150'+',top='+(screen.availHeight-150)/2+',left='+(screen.availWidth-400)/2);
//	self.parent.wndProgress=wnd;
//	return false;
}
function TransTypeNewReserveBalance_OnBlur(reserve_Type, trans_Type) 
{
    var txtCurrentTransTypeIncurredAmt = document.getElementById('txt_' + reserve_Type + '_TransTypeIncurredAmount_' + trans_Type);
    var txtCurrentTransTypeNewReserveBal = document.getElementById('txt_' + reserve_Type + '_TransTypeNewReserveBal_' + trans_Type);
    var txtCurrentTransTypePaidToDate = document.getElementById('txt_' + reserve_Type + '_Paid_' + trans_Type);
    
    var dCurrentTransTypeIncuredAmt = 0.0;

    dCurrentTransTypeIncuredAmt = ConvertStrToDbl(txtCurrentTransTypeNewReserveBal.value) + ConvertStrToDbl(txtCurrentTransTypePaidToDate.value)
    txtCurrentTransTypeIncurredAmt.value = ConvertDblToCurr(dCurrentTransTypeIncuredAmt);

    CalculateReserveAmount_OnBlur(reserve_Type, trans_Type); 
}

function formatRMDate2(sParamDate)
{
	var sDateSeparator;
	var iDayPos=0, iMonthPos=0;
	var d=new Date(1999,11,22);
	var s=d.toLocaleString();
	var sRet="";
	var sDate=new String(sParamDate);
	if(sDate=="")
		return "";
	iDayPos=s.indexOf("22");
	iMonthPos=s.indexOf("11");
	//if(IE4)
	//	sDateSeparator=s.charAt(iDayPos+2);
	//else
		sDateSeparator="/";
	if(iDayPos<iMonthPos)
		sRet=sDate.substr(6,2) + sDateSeparator + sDate.substr(4,2) + sDateSeparator + sDate.substr(0,4);
	else
		sRet=sDate.substr(4,2) + sDateSeparator + sDate.substr(6,2) + sDateSeparator + sDate.substr(0,4);
	return sRet;
}


function SetFormNameRSW(frmName)
{
	m_frmName = frmName;
}  

function Rsw_Load()//instance.xml
{
    var ret = false;
    if(document.getElementById('hdnRSWid')!=null)
  {  var rswid = document.getElementById('hdnRSWid').value;}
	var ctrl = document.getElementById('hdnSubmittedTo');
//	alert('hi');
	if (rswid == "-1")
	{  
	    if (ctrl != null)
	    {
		    if (ctrl.value == "None")
		    {
			    ret = false;
			    ret = confirm("No valid Supervisor / Manager exists for current user. Click 'OK' to continue and 'Cancel' to exit from Reserve Worksheet.");
			    if (!ret)
			    {
			   // location.href = "home?pg=riskmaster/Reserves/ReserveWorksheet/DefaultBlankMain";
			    return false;
			    }
		    }
	    }
	}
	
	//By Vaibhav for Safeway RSW on 11/18/2008: for setting an automatic $500 reserve if the Claim Type is �Medical Only� 
	//and the Claim Status is �Open�
	
	if(document.getElementById('hdnRSWStatus')!=null && document.getElementById('hdnRSWStatus').value=="New")
	{
	    if(document.getElementById('hdnClaimStatusCode')!=null && document.getElementById('hdnClaimTypeCode') !=null)
	    {
            if(document.getElementById('hdnClaimStatusCode').value=="02" && document.getElementById('hdnClaimTypeCode').value=="6")
            {
                //document.getElementById('Amt12MedWC').value = ConvertDblToCurr(500);//Other (Future Medical, etc.)
                document.getElementById('Amt12MedWC').value = 500; //Other (Future Medical, etc.)
                RSW_MultiCurrencyOnBlur(document.getElementById('Amt12MedWC'));
            }
        }
	}
	
	
	//Vaibhav code end
	
    //onblur
//   	el = document.getElementById('Amt1MedWC');//Medical
//	if(el!=null)
//	{
//        AddMedicalWC();
//       // CalcResAna();
//	}
//	
//	el = document.getElementById('Amt1WeeksTemWC');//Temporary Disability
//	if(el!=null)
//	{
//        AddTempDisWC();
//	}
//	
//	el = document.getElementById('txtAmt1PerWC');//Permanent Disability
//	if(el!=null)
//	{
//        AddPermDisWC();
//	}
//	
//	el = document.getElementById('Amt1WeeksVocWC');//Vocational Rehabilitation
//	if(el!=null)
//	{
//        AddVocRehabWC();
//	}
//	
//	el = document.getElementById('Amt1LegWC');//Legal
//	if(el!=null)
//	{
//        AddLegalWC();
//	}
//	
//	el = document.getElementById('Amt1AllWC');//Allocated Expense
//	if(el!=null)
//	{
//        AddAllExpWC();
//	}
//	
	//onchange
	
	
	
//////	el = document.getElementById('ClaimInfo');
//////	if(el!=null)
//////	{
//////        el.setAttribute("onclick", "");
//////	    RMX.addEvent(el, "click", ClaimInfo_Click);
//////	}
//////	
//////	el = document.getElementById('MedicalWC');
//////	if(el!=null)
//////	{
//////        el.setAttribute("onclick", "");
//////	    RMX.addEvent(el, "click", MedicalWC_Click);
//////	}
//////	
//////	el = document.getElementById('TemporaryDisabilityWC');
//////	if(el!=null)
//////	{
//////        el.setAttribute("onclick", "");
//////	    RMX.addEvent(el, "click", TemporaryDisabilityWC_Click);
//////	}
//////	
//////	el = document.getElementById('PermanentDisabilityWC');
//////	if(el!=null)
//////	{
//////        el.setAttribute("onclick", "");
//////	    RMX.addEvent(el, "click", PermanentDisabilityWC_Click);
//////	}
//////	
//////	el = document.getElementById('VocationalRehabilitationWC');
//////	if(el!=null)
//////	{
//////        el.setAttribute("onclick", "");
//////	    RMX.addEvent(el, "click", VocationalRehabilitationWC_Click);
//////	}
//////	
//////	el = document.getElementById('LegalWC');
//////	if(el!=null)
//////	{
//////        el.setAttribute("onclick", "");
//////	    RMX.addEvent(el, "click", LegalWC_Click);
//////	}
//////	
//////	el = document.getElementById('AllocatedExpenseWC');
//////	if(el!=null)
//////	{
//////        el.setAttribute("onclick", "");
//////	    RMX.addEvent(el, "click", AllocatedExpenseWC_Click);
//////	}
//////	
//////	el = document.getElementById('ResSummaryWC');
//////	if(el!=null)
//////	{
//////        el.setAttribute("onclick", "");
//////	    RMX.addEvent(el, "click", ResSummaryWC_Click);
//////	}
//////	
	//Correct the numeric control
	el = document.getElementById('Amt1WeeksTemWC');//Temproary Total Weeks
	if(el!=null)
	{
        var dbl=new String(el.value);    	
	    //if(dbl.indexOf('$') >= 0)  //Aman Multi Currency
		//    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt2WeeksTemWC');//Temproary Partial Weeks
	if(el!=null)
	{
	    var dbl = new String(el.value);    	//Aman Multi Currency
	    //if(dbl.indexOf('$') >= 0)
		//    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt1WeeksPerWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);     //Aman Multi Currency	
//	    if(dbl.indexOf('$') >= 0)
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt2WeeksPerWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);     //Aman Multi Currency	
//	    if(dbl.indexOf('$') >= 0)
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt3WeeksPerWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);     //Aman Multi Currency	
//	    if(dbl.indexOf('$') >= 0)
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt4WeeksPerWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);     //Aman Multi Currency	
//	    if(dbl.indexOf('$') >= 0)
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt5WeeksPerWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);    //Aman Multi Currency 	
//	    if(dbl.indexOf('$') >= 0)
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt6WeeksPerWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);    //Aman Multi Currency	
//	    if(dbl.indexOf('$') >= 0)
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('AmtWeeks9PerWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);   //Aman Multi Currency 	
//	    if(dbl.indexOf('$') >= 0)
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('AmtWeeks10PerWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);    	 //Aman Multi Currency
//	    if(dbl.indexOf('$') >= 0)
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('AmtWeeks11PerWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);    	 //Aman Multi Currency
//	    if(dbl.indexOf('$') >= 0)
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('Amt1WeeksVocWC');
	if(el!=null)
	{
	    var dbl = new String(el.value);     //Aman Multi Currency	
//	    if(dbl.indexOf('$') >= 0)  
//		    el.value=parseInt(dbl.replace(/[\$]/g ,""));   
		
		if (parseInt(el.value) == 0 || el.value == "")
		    el.value = "";
		else
		    RSWnumLostFocus(el);
	}
	
	el = document.getElementById('IncurredAmtMedWC');
	if (el != null)   //Aman Multi Currency--Start
	{
	    el.value = document.getElementById("hdnIncurredMedWC").value;
	    RSW_MultiCurrencyOnBlur(el);  
	}

   el = document.getElementById('IncurredAmtTemWC');
 //  debugger;
   if (el != null) {
       el.value = document.getElementById("hdnIncurredTemWC").value;
       RSW_MultiCurrencyOnBlur(el);  
   }
       
    el = document.getElementById('IncurredAmtPerWC');
    if (el != null) {
        el.value = document.getElementById("hdnIncurredPerWC").value;
        RSW_MultiCurrencyOnBlur(el);  
    }  
    el = document.getElementById('IncurredAmtVocWC');
    if (el != null) {
        el.value = document.getElementById("hdnIncurredVocWC").value;
        RSW_MultiCurrencyOnBlur(el);  
    }
    el = document.getElementById('IncurredAmtLegWC');
    if (el != null) {
        el.value = document.getElementById("hdnIncurredLegWC").value;
        RSW_MultiCurrencyOnBlur(el);  
    }
    el = document.getElementById('IncurredAmtAllWC');
    if (el != null) {
        el.value = document.getElementById("hdnIncurredAllWC").value;
        RSW_MultiCurrencyOnBlur(el);  
    }
   //Aman Multi Currency--End
}

//onblur //commented by shobhana 
////function TempTotalRate_Blur(){RSWcurrencyLostFocus(document.getElementById('TempTotalRate'));}
////function TempPartialRate_Blur(){RSWcurrencyLostFocus(document.getElementById('TempPartialRate'));}
////function PPDRate_Blur(){RSWcurrencyLostFocus(document.getElementById('PPDRate'));}
////function MaxDurationPTDRate_Blur(){RSWcurrencyLostFocus(document.getElementById('MaxDurationPTDRate'));}
////function SurvivorshipRate_Blur(){RSWcurrencyLostFocus(document.getElementById('SurvivorshipRate'));}
////function DependencyRate_Blur(){RSWcurrencyLostFocus(document.getElementById('DependencyRate'));}

//////endd done till here
////function DisfigurementRate_Blur(){RSWcurrencyLostFocus(document.getElementById('DisfigurementRate'));}
////function LifePensionRate_Blur(){RSWcurrencyLostFocus(document.getElementById('LifePensionRate'));}
////function VocRehabTDRate_Blur(){RSWcurrencyLostFocus(document.getElementById('VocRehabTDRate'));}

////function Amt1MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1MedWC'));}//Hospital Expense
////function Amt2MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2MedWC'));}//Physician Expense
////function Amt3MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3MedWC'));}//Physical Therapy /Chiro/Work Hardening
////function Amt4MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4MedWC'));}//Medical Case Management
////function Amt5MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt5MedWC'));}//Utilization Review
////function Amt6MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt6MedWC'));}//Diagnostic Testing
////function Amt7MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt7MedWC'));}//Drugs, Prosthetics, etc.
////function Amt8MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt8MedWC'));}//2nd Opinion/Consult (not legal)
////function Amt9MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt9MedWC'));}//Mileage
////function Amt10MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt10MedWC'));}//Equipment
////function Amt11MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt11MedWC'));}//Legal Exams/Procedures
////function Amt12MedWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt12MedWC'));}//Other (Future Medical, etc.)

////function Amt1WeeksTemWC_Blur(){RSWnumLostFocus(document.getElementById('Amt1WeeksTemWC'));}//Temproary Total Weeks
////function Amt2WeeksTemWC_Blur(){RSWnumLostFocus(document.getElementById('Amt2WeeksTemWC'));}//Temproary Partial Weeks

////function Amt1WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt1WeeksPerWC'));}//PPD Weeks
////function Amt2WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt2WeeksPerWC'));}//Life Pension Weeks
////function Amt3WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt3WeeksPerWC'));}//Max Duration/PTD Weeks
////function Amt4WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt4WeeksPerWC'));}//Survivorship Weeks
////function Amt5WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt5WeeksPerWC'));}//Dependency Weeks
////function Amt6WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('Amt6WeeksPerWC'));}//Disfigurement Weeks
////function Amt9WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('AmtWeeks9PerWC'));}//PPD Weeks
////function Amt10WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('AmtWeeks10PerWC'));}//PPD Weeks
////function Amt11WeeksPerWC_Blur(){RSWnumLostFocus(document.getElementById('AmtWeeks11PerWC'));}//PPD Weeks

////function Amt1RateTemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1RateTemWC'));}//Temproary Total Rates
////function Amt2RateTemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2RateTemWC'));}//Temproary Partial Rates

////function Amt1RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1RatePerWC'));}//PPD Rates
////function Amt2RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2RatePerWC'));}//Life Pension Rates
////function Amt3RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3RatePerWC'));}//Max Duration/PTD Rates
////function Amt4RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4RatePerWC'));}//Survivorship Rates
////function Amt5RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt5RatePerWC'));}//Dependency Rates
////function Amt6RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt6RatePerWC'));}//Disfigurement Rates
////function Amt9RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmtRate9PerWC'));}//PPD Rates
////function Amt10RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('AmtRate10PerWC'));}//PPD Rates
////function Amt11RatePerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('AmtRate11PerWC'));}//PPD Rates

////function Amt1TemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt1TemWC'));}//Temproary Total
////function Amt2TemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt2TemWC'));}//Temproary Partial
////function Amt3TemWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3TemWC'));}//Other (Describe)

////function Amt1PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt1PerWC'));}//PPD
////function Amt2PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt2PerWC'));}//Life Pension
////function Amt3PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt3PerWC'));}//Max Duration/PTD
////function Amt4PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt4PerWC'));}//Survivorship
////function Amt5PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt5PerWC'));}//Dependency
////function Amt6PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt6PerWC'));}//Disfigurement
////function Amt7PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt7PerWC'));}//LEC
////function Amt8PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt8PerWC'));}//Scheduled PD
////function Amt9PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('AmtPerTWC9'));}//PPD
////function Amt10PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt10PerWC'));}//PPD
////function Amt11PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt11PerWC'));}//PPD
////function Amt12PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('AmtPerWC12'));}//Attorney Fee
////function Amt13PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('AmtPerWC13'));}//Employer Liability
////function Amt14PerWC_Blur(){RSWcurrencyLostFocus(document.getElementById('AmtPerWC14'));}//Other (Child Support, MSA, Burial, etc.)

////function Amt1WeeksVocWC_Blur(){RSWnumLostFocus(document.getElementById('Amt1WeeksVocWC'));}//Voc Rehab TD Weeks

////function Amt1RateVocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1RateVocWC'));}//Voc Rehab TD Rates

////function Amt1VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('txtAmt1VocWC'));}
////function Amt2VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2VocWC'));}
////function Amt3VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3VocWC'));}
////function Amt4VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4VocWC'));}
////function Amt5VocWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt5VocWC'));}

////function Amt1LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1LegWC'));}
////function Amt2LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2LegWC'));}
////function Amt3LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3LegWC'));}
////function Amt4LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4LegWC'));}
////function Amt5LegWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt5LegWC'));}

////function Amt1AllWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt1AllWC'));}
////function Amt2AllWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt2AllWC'));}
////function Amt3AllWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt3AllWC'));}
////function Amt4AllWC_Blur(){RSWcurrencyLostFocus(document.getElementById('Amt4AllWC'));}

////function Amt1PercentPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt1PercentPerWC'));}
////function Amt1ApportionPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt1ApportionPerWC'));}
////function Amt1PercentAfterApportionPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt1PercentAfterApportionPerWC'));}

////function Amt2PercentApportionPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt2PercentApportionPerWC'));}

////function Amt9PercentPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt9PercentPerWC'));}
////function Amt10PercentPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt10PercentPerWC'));}
////function Amt11PercentPerWC_Blur(){RSWpercentLostFocus(document.getElementById('Amt11PercentPerWC'));}

//////donetill on 7/04 by shobhana

//////onchange
////function Amt1MedWC_Chg(){RSWsetDataChanged('true');}//Hospital Expense
////function Amt2MedWC_Chg(){RSWsetDataChanged('true');}//Physician Expense
////function Amt3MedWC_Chg(){RSWsetDataChanged('true');}//Physical Therapy /Chiro/Work Hardening
////function Amt4MedWC_Chg(){RSWsetDataChanged('true');}//Medical Case Management
////function Amt5MedWC_Chg(){RSWsetDataChanged('true');}//Utilization Review
////function Amt6MedWC_Chg(){RSWsetDataChanged('true');}//Diagnostic Testing
////function Amt7MedWC_Chg(){RSWsetDataChanged('true');}//Drugs, Prosthetics, etc.
////function Amt8MedWC_Chg(){RSWsetDataChanged('true');}//2nd Opinion/Consult (not legal)
////function Amt9MedWC_Chg(){RSWsetDataChanged('true');}//Mileage
////function Amt10MedWC_Chg(){RSWsetDataChanged('true');}//Equipment
////function Amt11MedWC_Chg(){RSWsetDataChanged('true');}//Legal Exams/Procedures
////function Amt12MedWC_Chg(){RSWsetDataChanged('true');}//Other (Future Medical, etc.)

////function Amt1WeeksTemWC_Chg(){RSWsetDataChanged('true');}//Temproary Total Weeks
////function Amt2WeeksTemWC_Chg(){RSWsetDataChanged('true');}//Temproary Partial Weeks

////function Amt1WeeksPerWC_Chg(){RSWsetDataChanged('true');}//PPD Weeks
////function Amt2WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Life Pension Weeks
////function Amt3WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Max Duration/PTD Weeks
////function Amt4WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Survivorship Weeks
////function Amt5WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Dependency Weeks
////function Amt6WeeksPerWC_Chg(){RSWsetDataChanged('true');}//Disfigurement Weeks
////function Amt9WeeksPerWC_Chg(){RSWsetDataChanged('true');}//PPD Weeks
////function Amt10WeeksPerWC_Chg(){RSWsetDataChanged('true');}//PPD Weeks
////function Amt11WeeksPerWC_Chg(){RSWsetDataChanged('true');}//PPD Weeks

////function Amt1RateTemWC_Chg(){RSWsetDataChanged('true');}//Temproary Total Rates
////function Amt2RateTemWC_Chg(){RSWsetDataChanged('true');}//Temproary Partial Rates

////function Amt1RatePerWC_Chg(){RSWsetDataChanged('true');}//PPD Rates
////function Amt2RatePerWC_Chg(){RSWsetDataChanged('true');}//Life Pension Rates
////function Amt3RatePerWC_Chg(){RSWsetDataChanged('true');}//Max Duration/PTD Rates
////function Amt4RatePerWC_Chg(){RSWsetDataChanged('true');}//Survivorship Rates
////function Amt5RatePerWC_Chg(){RSWsetDataChanged('true');}//Dependency Rates
////function Amt6RatePerWC_Chg(){RSWsetDataChanged('true');}//Disfigurement Rates
////function Amt9RatePerWC_Chg(){RSWsetDataChanged('true');}//PPD Rates
////function Amt10RatePerWC_Chg(){RSWsetDataChanged('true');}//PPD Rates
////function Amt11RatePerWC_Chg(){RSWsetDataChanged('true');}//PPD Rates

////function Amt1TemWC_Chg(){RSWsetDataChanged('true');}//Temproary Total
////function Amt2TemWC_Chg(){RSWsetDataChanged('true');}//Temproary Partial
////function Amt3TemWC_Chg(){RSWsetDataChanged('true');}//Other (Describe)

////function Amt1PerWC_Chg(){RSWsetDataChanged('true');}//PPD
////function Amt2PerWC_Chg(){RSWsetDataChanged('true');}//Life Pension
////function Amt3PerWC_Chg(){RSWsetDataChanged('true');}//Max Duration/PTD
////function Amt4PerWC_Chg(){RSWsetDataChanged('true');}//Survivorship
////function Amt5PerWC_Chg(){RSWsetDataChanged('true');}//Dependency
////function Amt6PerWC_Chg(){RSWsetDataChanged('true');}//Disfigurement
////function Amt7PerWC_Chg(){RSWsetDataChanged('true');}//LEC
////function Amt8PerWC_Chg(){RSWsetDataChanged('true');}//Scheduled PD
////function Amt9PerWC_Chg(){RSWsetDataChanged('true');}//PPD
////function Amt10PerWC_Chg(){RSWsetDataChanged('true');}//PPD
////function Amt11PerWC_Chg(){RSWsetDataChanged('true');}//PPD
////function Amt12PerWC_Chg(){RSWsetDataChanged('true');}//Attorney Fee
////function Amt13PerWC_Chg(){RSWsetDataChanged('true');}//Employer Liability
////function Amt14PerWC_Chg(){RSWsetDataChanged('true');}//Other (Child Support, MSA, Burial, etc.)

////function Amt1WeeksVocWC_Chg(){RSWsetDataChanged('true');}//Voc Rehab TD weeks

////function Amt2VocWC_Chg(){RSWsetDataChanged('true');}
////function Amt3VocWC_Chg(){RSWsetDataChanged('true');}
////function Amt4VocWC_Chg(){RSWsetDataChanged('true');}
////function Amt5VocWC_Chg(){RSWsetDataChanged('true');}

////function Amt1LegWC_Chg(){RSWsetDataChanged('true');}
////function Amt2LegWC_Chg(){RSWsetDataChanged('true');}
////function Amt3LegWC_Chg(){RSWsetDataChanged('true');}
////function Amt4LegWC_Chg(){RSWsetDataChanged('true');}
////function Amt5LegWC_Chg(){RSWsetDataChanged('true');}

////function Amt1AllWC_Chg(){RSWsetDataChanged('true');}
////function Amt2AllWC_Chg(){RSWsetDataChanged('true');}
////function Amt3AllWC_Chg(){RSWsetDataChanged('true');}
////function Amt4AllWC_Chg(){RSWsetDataChanged('true');}

//Comments blur

//function ClaimInfo_Click(){TabChange('ClaimInfo');}
//function MedicalWC_Click(){TabChange('MedicalWC');}
//function TemporaryDisabilityWC_Click(){TabChange('TemporaryDisabilityWC');}
//function PermanentDisabilityWC_Click(){TabChange('PermanentDisabilityWC');}
//function VocationalRehabilitationWC_Click(){TabChange('VocationalRehabilitationWC');}
//function LegalWC_Click(){TabChange('LegalWC');}
//function AllocatedExpenseWC_Click(){TabChange('AllocatedExpenseWC');}
//function ResSummaryWC_Click(){TabChange('ResSummaryWC');}

function openRSWGridAddEditWindow(listname,mode,width,height)
{
	var openpage;
	var pagepath;
	var iPositionForSelectedGridRow;
	switch(listname)
	{			
		//Commented by Mohit Yadav as per new requirement: Start
		/*case "MedList":
			openpage="MedicalReserves";
			pagepath="/Reserves/ReserveWorksheet/MedList";		
			break;
		case "IndList":
			openpage="IndemnityReserves";
			pagepath="/Reserves/ReserveWorksheet/IndList";		
			break;
		case "RehabList":
			openpage="RehabReserves";
			pagepath="/Reserves/ReserveWorksheet/RehabList";		
			break;
		case "LegalList":
			openpage="LegalReserves";
			pagepath="/Reserves/ReserveWorksheet/LegalList";		
			break;
		case "OthList":
			openpage="OtherReserves";
			pagepath="/Reserves/ReserveWorksheet/OthList";		
			break;*/
		//Commented by Mohit Yadav as per new requirement: End
	}	
	if (mode=="add")
	{	
		RMX.window.open('home?pg=riskmaster'+pagepath,openpage,
			'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=no,scrollbars=yes');
	}
	else
	{
		var CheckedFound=false;
			inputs = document.all.tags("input");
			for (i = 0; i < inputs.length; i++)
			{
				if ((inputs[i].type=="radio") && (inputs[i].checked==true))
				{
					inputname=inputs[i].id.substring(0,inputs[i].id.indexOf("|"));
					if (listname==inputname)	
					{
						CheckedFound=true;
						RMX.window.open('home?pg=riskmaster'+pagepath+'&amp;selectedid='+inputs[i].value,openpage,
							'width=' + width + ',height='+ height +',top='+(screen.availHeight-height)/2+',left='+(screen.availWidth-width)/2+',resizable=no,scrollbars=yes');
					}
				}
			}

		if (CheckedFound==false)
		{
			alert('Please select a record to edit.');
			return false;
		}
	}
	return false;
}

function ConvertDblToPer(dbl)
{
	dbl=parseFloat(dbl);
	dbl=dbl.toFixed(2);

	var base = new String(dbl);
	base = base.substring(0,base.lastIndexOf('.'));
	var newbase="";
	var j = 0;
	var i=base.length-1;
	for(i;i>=0;i--)
	{	if(j==3)
		{
			newbase = "," + newbase;	
			j=0;

		}
		newbase = base.charAt(i) + newbase;	
		j++;
	}
	var str = new String(dbl);
	str = str.substring(str.lastIndexOf(".")+1);
	return newbase + "." + str + "%" ;
}

//Safeway: Mohit Yadav: For Reserve Worksheet currency controls
function RSWcurrencyLostFocus(objCtrl) {   
    //Aman Multi Currency --Start
    //var dbl=new String(objCtrl.value);    
    RSW_MultiCurrencyToDecimal(objCtrl);
    var dbl = objCtrl.getAttribute("Amount");
    if (!objCtrl.getAttribute('readonly'))
    {
	    // Strip and Validate Input
	    // BSB Remove commas first so parseFloat doesn't incorrectly truncate value.	
	    //dbl=dbl.replace(/[,\$]/g ,"");
	    
	    if(dbl.length==0)
	    {
	        //This is done for cases where the currency field blank
	        //objCtrl.value= "$" + "0" + "." + "00";
	        objCtrl.value = 0;
	        RSW_MultiCurrencyOnBlur(objCtrl);
	        AutoPopulateRate(objCtrl);
		    return false;
	    }

	    //for Est. Collection 	
	    //shobhana		
//	    if(dbl.indexOf('(') >= 0)
//	    {		
//		    if(dbl.indexOf('-') >= 0)
//		    {
//			    dbl=dbl.replace(/[\-\(\)]/g ,"");
//		    }
//		    else
//		    {
//			    dbl=dbl.replace(/[\(\)]/g ,"");	
//		    }
//	    }				
//	    else if(dbl.indexOf('-') >= 0)
//	    {
//		    dbl=dbl.replace(/[\-]/g ,"");
//	    } 		
//    
//	    if(isNaN(parseFloat(dbl)))
//	    {
//		    objCtrl.value="$" + "0" + "." + "00";;	
//		    return false;
//	    }	
	    
	    //round to 2 places
	    dbl=parseFloat(dbl);
	    //hourly rate to have 3 places to decimal
	    if(objCtrl.id =='emphourlyrate')
	    {
	        dbl=dbl.toFixed(3);
	    }
	    else if (objCtrl.id =='txtClaimTempTotalRate' || objCtrl.id =='TempPartialRate' || objCtrl.id =='txtClaimPPDRate' ||objCtrl.id =='MaxDurationPTDRate' ||objCtrl.id =='txtClaimSurvivorshipRate' ||objCtrl.id =='DependencyRate' ||objCtrl.id =='txtClaimDisfigurementRate' ||objCtrl.id =='LifePensionRate' ||objCtrl.id =='txtClaimVocRehabTDRate' || objCtrl.id =='Amt7PerWC')
	    {
	        dbl=dbl.toFixed(2);
	    }
	    else
	    {
	    dbl=Math.round(dbl);//As per Safeway they want the amounts to be rounded-off
	    dbl=dbl.toFixed(2);
	    }

	    //Handle Full Integer Section (Formats in groups of 3 with comma.)
//	    var base = new String(dbl);
//	    base = base.substring(0,base.lastIndexOf('.'));
//	    var newbase="";
//	    var j = 0;
//	    var i=base.length-1;
//	    for(i;i>=0;i--)
//	    {	if(j==3)
//		    {
//			    newbase = "," + newbase;	
//			    j=0;

//		    }
//		    newbase = base.charAt(i) + newbase;	
//		    j++;
//	    }

	
	    // Handle Fractional Part
//	    var str = new String(dbl);
//	    str = str.substring(str.lastIndexOf(".")+1);

//	    // Reassemble formatted Currency Value string.

//	    objCtrl.value= "$" + newbase + "." + str;
	    AutoPopulateRate(objCtrl);
	    return true;
	}
	else
	    return false;
}

//Safeway: Mohit Yadav: For Reserve Worksheet numeric controls
function AutoPopulateRate(objctrl) {

    RSW_MultiCurrencyOnBlur(objctrl);

    var ival = objctrl.getAttribute("Amount");
    if (objctrl.id == "txtClaimTempTotalRate")
       {
        if (document.getElementById('Amt1RateTemWC') != null)
        {
            document.getElementById('Amt1RateTemWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('Amt1RateTemWC'));
               document.getElementById('hdn_Amt1RateTemWC').value = ival;
            //document.getElementById('txtAmt1TemWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt1WeeksTemWC').value) * ConvertStrToDbl(ival)));
               //  document.getElementById('hdn_Amt1TemWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(document.getElementById('Amt1WeeksTemWC').value) * ConvertStrToDbl(ival)));              
               document.getElementById('txtAmt1TemWC').value = Math.round(Number(document.getElementById('Amt1WeeksTemWC').value) * Number(ival));
               RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt1TemWC'));
               document.getElementById('hdn_Amt1TemWC').value = Math.round(Number(document.getElementById('Amt1WeeksTemWC').value) * Number(ival));
            AddTempDisWC();
        }
       }
       
    if (objctrl.id == "TempPartialRate")
       {
        if (document.getElementById('Amt2RateTemWC') != null)
        {
            document.getElementById('Amt2RateTemWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('Amt2RateTemWC'));
              document.getElementById('hdn_Amt2RateTemWC').value = ival;
              document.getElementById('txtAmt2TemWC').value = Math.round(Number(document.getElementById('Amt2WeeksTemWC').value) * Number(ival));
              RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt2TemWC'));
               document.getElementById('hdn_Amt2TemWC').value = Math.round(Number(document.getElementById('Amt2WeeksTemWC').value) * Number(ival));
            AddTempDisWC();
        }
       }
    
    if (objctrl.id == "txtClaimPPDRate")
    {
        if (document.getElementById('Amt1RatePerWC') != null)
        {
            document.getElementById('Amt1RatePerWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('Amt1RatePerWC'));
             document.getElementById('hdn_Amt1RatePerWC').value = ival;
             document.getElementById('txtAmt1PerWC').value = Math.round(Number(document.getElementById('Amt1WeeksPerWC').value) * Number(ival));
             RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt1PerWC'));
            document.getElementById('hdn_Amt1PerWC').value = Math.round(Number(document.getElementById('Amt1WeeksPerWC').value) * Number(ival));
            AddPermDisWC();
        }
        
        if (document.getElementById('txtAmtRate9PerWC') != null)
        {
            document.getElementById('txtAmtRate9PerWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('txtAmtRate9PerWC'));
            document.getElementById('hdn_txtAmtRate9PerWC').value = ival;
            //RSW_MultiCurrencyToDecimal(document.getElementById('AmtPercent9PerWCPerGP'));
            document.getElementById('AmtPerTWC9').value = Math.round((Number(ival) * Number(document.getElementById('AmtWeeks9PerWC').value) * ConvertStrToDbl(document.getElementById('AmtPercent9PerWCPerGP').value)) / 100);
              RSW_MultiCurrencyOnBlur(document.getElementById('AmtPerTWC9'));
              document.getElementById('hdn_AmtPerTWC9').value = Math.round((Number(ival) * Number(document.getElementById('AmtWeeks9PerWC').value) * ConvertStrToDbl(document.getElementById('AmtPercent9PerWCPerGP').value)) / 100);
            AddPermDisWC();
        }
        
        if (document.getElementById('AmtRate10PerWC') != null)
        {
            document.getElementById('AmtRate10PerWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('AmtRate10PerWC'));
            document.getElementById('hdn_AmtRate10PerWC').value = ival;
            //RSW_MultiCurrencyToDecimal(document.getElementById('AmtPercent10PerWCPerGP'));
            document.getElementById('txtAmt10PerWC').value = Math.round((Number(ival) * Number(document.getElementById('AmtWeeks10PerWC').value) * ConvertStrToDbl(document.getElementById('AmtPercent10PerWCPerGP').value)) / 100);
            RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt10PerWC'));
            document.getElementById('hdn_txtAmt10PerWC').value = Math.round((Number(ival) * Number(document.getElementById('AmtWeeks10PerWC').value) * ConvertStrToDbl(document.getElementById('AmtPercent10PerWCPerGP').value)) / 100);
            AddPermDisWC();
        }
        
        if (document.getElementById('AmtRate11PerWC') != null)
        {
            document.getElementById('AmtRate11PerWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('AmtRate11PerWC'));
            document.getElementById('hdn_AmtRate11PerWC').value = ival;
            //RSW_MultiCurrencyToDecimal(document.getElementById('AmtPercent11PerWCPerGP'));
            document.getElementById('txtAmt11PerWC').value = Math.round((Number(ival) * Number(document.getElementById('AmtWeeks11PerWC').value) * ConvertStrToDbl(document.getElementById('AmtPercent11PerWCPerGP').value)) / 100);
            RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt11PerWC'));
            document.getElementById('hdn_txtAmt11PerWC').value = Math.round((Number(ival) * Number(document.getElementById('AmtWeeks11PerWC').value) * ConvertStrToDbl(document.getElementById('AmtPercent11PerWCPerGP').value)) / 100);
            AddPermDisWC();
        }
        
    }
    
    if (objctrl.id == "MaxDurationPTDRate")
       {
        if (document.getElementById('Amt3RatePerWC') != null)
        {
            document.getElementById('Amt3RatePerWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('Amt3RatePerWC'));
            document.getElementById('hdn_Amt3RatePerWC').value = ival;
            document.getElementById('txtAmt3PerWC').value = Math.round(Number(document.getElementById('Amt3WeeksPerWC').value) * Number(ival));
            RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt3PerWC'));
               document.getElementById('hdn_Amt3PerWC').value = Math.round(Number(document.getElementById('Amt3WeeksPerWC').value) * Number(ival));
            AddPermDisWC();
        }
       }
    
    if (objctrl.id == "txtClaimSurvivorshipRate")
       {
        if (document.getElementById('Amt4RatePerWC') != null)
        {
            document.getElementById('Amt4RatePerWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('Amt4RatePerWC'));
              document.getElementById('hdn_Amt4RatePerWC').value = ival;
              document.getElementById('txtAmt4PerWC').value = Math.round(Number(document.getElementById('Amt4WeeksPerWC').value) * Number(ival));
              RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt4PerWC'));
              document.getElementById('hdn_Amt4PerWC').value = Math.round(Number(document.getElementById('Amt4WeeksPerWC').value) * Number(ival));
            AddPermDisWC();
        }
       }
    
    if (objctrl.id == "DependencyRate")
       {
        if (document.getElementById('Amt5RatePerWC') != null)
        {
            document.getElementById('Amt5RatePerWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('Amt5RatePerWC'));
             document.getElementById('hdn_Amt5RatePerWC').value = ival;
             document.getElementById('txtAmt5PerWC').value = Math.round(Number(document.getElementById('Amt5WeeksPerWC').value) * Number(ival));
             RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt5PerWC'));
             document.getElementById('hdn_Amt5PerWC').value = Math.round(Number(document.getElementById('Amt5WeeksPerWC').value) * Number(ival));
            AddPermDisWC();
        }
       }
    
    if (objctrl.id == "txtClaimDisfigurementRate")
       {
        if (document.getElementById('Amt6RatePerWC') != null)
        {
            document.getElementById('Amt6RatePerWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('Amt6RatePerWC'));
            document.getElementById('hdn_Amt6RatePerWC').value = ival;
            document.getElementById('txtAmt6PerWC').value = Math.round(Number(document.getElementById('Amt6WeeksPerWC').value) * Number(ival));
            RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt6PerWC'));
            document.getElementById('hdn_Amt6PerWC').value = Math.round(Number(document.getElementById('Amt6WeeksPerWC').value) * Number(ival));
            AddPermDisWC();
        }
       }
    
    if (objctrl.id == "LifePensionRate")
       {
        if (document.getElementById('Amt2RatePerWC') != null)
        {
            document.getElementById('Amt2RatePerWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('Amt2RatePerWC'));
              document.getElementById('hdn_Amt2RatePerWC').value = ival;
              document.getElementById('txtAmt2PerWC').value = Math.round(Number(document.getElementById('Amt2WeeksPerWC').value) * Number(ival));
              RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt2PerWC'));
              document.getElementById('hdn_Amt2PerWC').value = Math.round(Number(document.getElementById('Amt2WeeksPerWC').value) * Number(ival));
            AddPermDisWC();
        }
       }
       
    if (objctrl.id == "txtClaimVocRehabTDRate")
       {
        if (document.getElementById('Amt1RateVocWC') != null)
        {
            document.getElementById('Amt1RateVocWC').value = ival;
            RSW_MultiCurrencyOnBlur(document.getElementById('Amt1RateVocWC'));
             document.getElementById('hdn_Amt1RateVocWC').value = ival;
             document.getElementById('txtAmt1VocWC').value = Math.round(Number(document.getElementById('Amt1WeeksVocWC').value) * Number(ival));
             RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt1VocWC'));
             document.getElementById('hdn_Amt1VocWC').value = Math.round(Number(document.getElementById('Amt1WeeksVocWC').value) * Number(ival));
            AddVocRehabWC();
        }
       }
       
    if (objctrl.id == "Amt1MedWC" || objctrl.id == "Amt2MedWC" || objctrl.id == "Amt3MedWC" || objctrl.id == "Amt4MedWC" || objctrl.id == "Amt5MedWC" || objctrl.id == "Amt6MedWC" || objctrl.id == "Amt7MedWC" || objctrl.id == "Amt8MedWC" || objctrl.id == "Amt9MedWC" || objctrl.id == "Amt10MedWC" || objctrl.id == "Amt11MedWC" || objctrl.id == "Amt12MedWC")
       {
          // debugger;
        AddMedicalWC();
       }
    if (objctrl.id == "txtAmt1TemWC" || objctrl.id == "txtAmt2TemWC" || objctrl.id == "Amt3TemWC")
       {
        if (objctrl.id == "txtAmt1TemWC") {
            RSW_MultiCurrencyToDecimal(document.getElementById('Amt1RateTemWC'));
            document.getElementById('txtAmt1TemWC').value = Math.round(Number(document.getElementById('Amt1WeeksTemWC').value) * Number(document.getElementById('Amt1RateTemWC').getAttribute("Amount")));
            RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt1TemWC'));
            document.getElementById('hdn_Amt1TemWC').value = Math.round(Number(document.getElementById('Amt1WeeksTemWC').value) * Number(document.getElementById('Amt1RateTemWC').getAttribute("Amount")));
            }
        if (objctrl.id == "txtAmt2TemWC") {
            RSW_MultiCurrencyToDecimal(document.getElementById('Amt2RateTemWC'));
            document.getElementById('txtAmt2TemWC').value = Math.round(Number(document.getElementById('Amt2WeeksTemWC').value) * Number(document.getElementById('Amt2RateTemWC').getAttribute("Amount")));
            RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt2TemWC'));
            document.getElementById('hdn_Amt2TemWC').value = Math.round(Number(document.getElementById('Amt2WeeksTemWC').value) * Number(document.getElementById('Amt2RateTemWC').getAttribute("Amount")));
}
        AddTempDisWC();
       }
    if (objctrl.id == "Amt7PerWC" || objctrl.id == "Amt8PerWC" || objctrl.id == "AmtPerWC12" || objctrl.id == "AmtPerWC13" || objctrl.id == "AmtPerWC14")
       {
        AddPermDisWC();
       }
    if (objctrl.id == "Amt2VocWC" || objctrl.id == "Amt3VocWC" || objctrl.id == "Amt4VocWC" || objctrl.id == "Amt5VocWC")
       {
        AddVocRehabWC();
       }
    if (objctrl.id == "Amt1LegWC" || objctrl.id == "Amt2LegWC" || objctrl.id == "Amt3LegWC" || objctrl.id == "Amt4LegWC" || objctrl.id == "Amt5LegWC")
       {
        AddLegalWC();
       }
    if (objctrl.id == "Amt1AllWC" || objctrl.id == "Amt2AllWC" || objctrl.id == "Amt3AllWC" || objctrl.id == "Amt4AllWC")
       {
        AddAllExpWC();
       }
       
       CreateReserveSummary()
     //  CalcResAna();
}

//Safeway: Mohit Yadav: For Reserve Worksheet numeric controls
function RSWnumLostFocus(objCtrl) {   
	if (!objCtrl.getAttribute('readonly'))
    {
	    if(objCtrl.value.length==0)
	    {
	        objCtrl.value=0;
	        AutoCalResAmt(objCtrl);
		    return false;
	    }
			
	    if(isNaN(parseFloat(objCtrl.value)))
	        objCtrl.value = 0;
        else if(isNaN(objCtrl.value))  //Aman Multi Currency
            objCtrl.value = 0;
	    else
		    objCtrl.value=Math.round(objCtrl.value);  //Weeks to be non-decimal field //parseFloat(objCtrl.value);
	
	    if(objCtrl.getAttribute('min')!=null)
	    {
		    if (!(isNaN(parseFloat(objCtrl.getAttribute('min')))))
		    {
			    if (parseFloat(objCtrl.value)<parseFloat(objCtrl.getAttribute('min')))
			    {
				    objCtrl.value=0;
			    }
		    }
	    }
    	
	    if(objCtrl.getAttribute('max')!=null)
	    {
		    if (!(isNaN(parseFloat(objCtrl.getAttribute('max')))))
		    {
			    if (parseFloat(objCtrl.value)> parseFloat(objCtrl.getAttribute('max')))
			    {
				    objCtrl.value=0;
			    }
		    }
	    }
	
	    var dbl=new String(objCtrl.value);	
    	
	    if(dbl.indexOf('-') >= 0)
		    objCtrl.value=dbl.replace(/[\-]/g ,"");
    				
	    AutoCalResAmt(objCtrl);			
	    return true;
	}
	else
	    return false;
}

//Safeway: Mohit Yadav: For Calculating Reserve Amounts in Reserve Worksheet on filling weeks/Rate
function AutoCalResAmt(objctrl) { 
    var ival = objctrl.value;
        
    if (objctrl.id == "Amt1WeeksTemWC")
       {
        //document.getElementById('txtAmt1TemWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt1RateTemWC').value)));
        //   document.getElementById('hdn_Amt1TemWC').innerText = ConvertDblToCurr(Math.round(ival * ConvertStrToDbl(document.getElementById('Amt1RateTemWC').value)));
           document.getElementById('txtAmt1TemWC').value = Math.round(ival * Number(document.getElementById('Amt1RateTemWC').getAttribute("Amount")));
           document.getElementById('hdn_Amt1TemWC').value = Math.round(ival * Number(document.getElementById('Amt1RateTemWC').getAttribute("Amount")));
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt1TemWC'));
        AddTempDisWC();
        
       }
    if (objctrl.id == "Amt2WeeksTemWC")
       {
           document.getElementById('txtAmt2TemWC').value = Math.round(ival * Number(document.getElementById('Amt2RateTemWC').getAttribute("Amount")));
           document.getElementById('hdn_Amt2TemWC').value = Math.round(ival * Number(document.getElementById('Amt2RateTemWC').getAttribute("Amount")));
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt2TemWC'));
        AddTempDisWC();
       }
    if (objctrl.id == "Amt1WeeksPerWC")
       {
           document.getElementById('txtAmt1PerWC').value = Math.round(ival * Number(document.getElementById('Amt1RatePerWC').getAttribute("Amount")));
           document.getElementById('hdn_Amt1PerWC').value = Math.round(ival * Number(document.getElementById('Amt1RatePerWC').getAttribute("Amount")));
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt1PerWC'));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt2WeeksPerWC")
       {
           document.getElementById('txtAmt2PerWC').value = Math.round(ival * Number(document.getElementById('Amt2RatePerWC').getAttribute("Amount")));
           document.getElementById('hdn_Amt2PerWC').value = Math.round(ival * Number(document.getElementById('Amt2RatePerWC').getAttribute("Amount")));
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt2PerWC'));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt3WeeksPerWC")
       {
           document.getElementById('txtAmt3PerWC').value = Math.round(ival * Number(document.getElementById('Amt3RatePerWC').getAttribute("Amount")));
           document.getElementById('hdn_Amt3PerWC').value = Math.round(ival * Number(document.getElementById('Amt3RatePerWC').getAttribute("Amount")));
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt3PerWC'));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt4WeeksPerWC")
       {
           document.getElementById('txtAmt4PerWC').value = Math.round(ival * Number(document.getElementById('Amt4RatePerWC').getAttribute("Amount")));
           document.getElementById('hdn_Amt4PerWC').value = Math.round(ival * Number(document.getElementById('Amt4RatePerWC').getAttribute("Amount")));
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt4PerWC'));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt5WeeksPerWC")
       {
           document.getElementById('txtAmt5PerWC').value = Math.round(ival * Number(document.getElementById('Amt5RatePerWC').getAttribute("Amount")));
           document.getElementById('hdn_Amt5PerWC').value = Math.round(ival * Number(document.getElementById('Amt5RatePerWC').getAttribute("Amount")));
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt5PerWC'));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt6WeeksPerWC")
       {
           document.getElementById('txtAmt6PerWC').value = Math.round(ival * Number(document.getElementById('Amt6RatePerWC').getAttribute("Amount")));
           document.getElementById('hdn_Amt6PerWC').value = Math.round(ival * Number(document.getElementById('Amt6RatePerWC').getAttribute("Amount")));
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt6PerWC'));
        AddPermDisWC();
       }
    if (objctrl.id == "AmtWeeks9PerWC")
       {
      // debugger;
           document.getElementById('AmtPerTWC9').value = Math.round((Number(ival) * ConvertStrToDbl(document.getElementById('AmtPercent9PerWCPerGP').value) * Number(document.getElementById('txtAmtRate9PerWC').getAttribute("Amount"))) / 100);
           document.getElementById('hdn_AmtPerTWC9').value = Math.round((Number(ival) * ConvertStrToDbl(document.getElementById('AmtPercent9PerWCPerGP').value) * Number(document.getElementById('txtAmtRate9PerWC').getAttribute("Amount"))) / 100);
           RSW_MultiCurrencyOnBlur(document.getElementById('AmtPerTWC9'));
        AddPermDisWC();
       }
    if (objctrl.id == "AmtWeeks10PerWC")
       {
           document.getElementById('txtAmt10PerWC').value = Math.round((Number(ival) * ConvertStrToDbl(document.getElementById('AmtPercent10PerWCPerGP').value) * ConvertStrToDbl(document.getElementById('AmtRate10PerWC').getAttribute("Amount"))) / 100);
           document.getElementById('hdn_txtAmt10PerWC').value = Math.round((Number(ival) * ConvertStrToDbl(document.getElementById('AmtPercent10PerWCPerGP').value) * ConvertStrToDbl(document.getElementById('AmtRate10PerWC').getAttribute("Amount"))) / 100);
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt10PerWC'));
        AddPermDisWC();
       }
    if (objctrl.id == "AmtWeeks11PerWC")
       {
           document.getElementById('txtAmt11PerWC').value = Math.round((Number(ival) * ConvertStrToDbl(document.getElementById('AmtPercent11PerWCPerGP').value) * ConvertStrToDbl(document.getElementById('AmtRate11PerWC').getAttribute("Amount"))) / 100);
           document.getElementById('hdn_txtAmt11PerWC').value = Math.round((Number(ival) * ConvertStrToDbl(document.getElementById('AmtPercent11PerWCPerGP').value) * ConvertStrToDbl(document.getElementById('AmtRate11PerWC').getAttribute("Amount"))) / 100);
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt11PerWC'));
        AddPermDisWC();
       }
    if (objctrl.id == "Amt1WeeksVocWC")
       {
//        document.getElementById('txtAmt1VocWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt1RateVocWC').value)));
           //            document.getElementById('hdn_Amt1VocWC').innerText = ConvertDblToCurr(Math.round(ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('Amt1RateVocWC').value)));
           document.getElementById('txtAmt1VocWC').value = Math.round(Number(ival) * Number(document.getElementById('Amt1RateVocWC').getAttribute("Amount")));
           document.getElementById('hdn_Amt1VocWC').value = Math.round(Number(ival) * Number(document.getElementById('Amt1RateVocWC').getAttribute("Amount")));
           RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt1VocWC'));
        AddVocRehabWC();
       }
    CreateReserveSummary();
}
function CreateReserveSummary() 
{
    var tab = null;
    var root = null;
    var tbo = null;
    var row = null;
    var cell = null;
    var arrCtrls = null;
    var hdnReserveTypeTabs = null;
    var txtReserveTypePaid = null;
    var sReserveCategoryDesc;
    var txtReserveTypePrevOutStanding = null;
    var hdnReserveTypeNewOutStanding = null;
    var hdnReserveTypeNewIncurred = null;
    var hdnReserveTypeReserveChange = null;
    var txtReserveTypeComments = null;
    var sReserveTypeComments = "";
    var txtReserveSummaryComments = null;
    var dTotalPaidAmt = 0.0;
    var dTotalPrevOutStandingAmt = 0.0;
    var dTotalNewOutStandingAmt = 0.0;
    var dTotalNewIncurredAmt = 0.0;
    var dTotalReserveChangeAmt = 0.0;
    var amount = 0;
    //changed by shobhana for safeway R7
    //for medical
  root = document.getElementById('divReserveSummary');
    root.innerHTML = "";
    tab = document.createElement('table');
    tab.border = "0";
    tab.cellPadding = "0";
    tab.cellSpacing = "0";
    tab.className = "divScroll";
    tbo = document.createElement('tbody');
    

    //creating header and its cells for Reserve summary table
    row = document.createElement('tr');
    row.className = "colheader6";

    //cell 0
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode(' Reserve Category '));
    row.appendChild(cell);

    //cell1
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode(' Paid '));
    row.appendChild(cell);

    //cell2
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode('Previous Outstanding  '));
    row.appendChild(cell);

    //cell3
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode(' New  Outstanding '));
    row.appendChild(cell);

    //cell4
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode(' New Incurred '));
    row.appendChild(cell);

    //cell5
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode('Reserve Change'));
    row.appendChild(cell);

    //adding this header row to table body
    tbo.appendChild(row);
    
    hdnReserveTypeTabs = document.getElementById('hdnReserveTypeTabs');
    if (hdnReserveTypeTabs != null) 
    {
        arrCtrls = hdnReserveTypeTabs.value.split(",");
    }
    else 
    {
        return false;
    }
    
   	//For Medical WC Reserves
 for (var count = 1; count < arrCtrls.length-2; count++) 
    {
//debugger;
        txtReserveTypePaid = document.getElementById('PaidAmt' + arrCtrls[count].substring(0,3) + 'WC');
        txtReserveTypePrevOutStanding = document.getElementById('BalAmt' + arrCtrls[count].substring(0,3) + 'WC');
        hdnReserveTypeNewOutStanding = document.getElementById('hdn_NewBalAmt' + arrCtrls[count].substring(0,3) + 'WC');
        hdnReserveTypeNewIncurred = document.getElementById('hdn_NewIncAmt' + arrCtrls[count].substring(0,3) +  'WC');
        hdnReserveTypeReserveChange = document.getElementById('hdn_REserveChange' + arrCtrls[count].substring(0, 3) + 'WC');
        sReserveCategoryDesc = document.getElementById('TABS' + arrCtrls[count]);

        txtReserveTypeComments = document.getElementById('Comm' + arrCtrls[count].substring(0,3) + 'WC');
        
document.getElementById('hdn_Comm' + arrCtrls[count].substring(0,3) + 'WC').innerText=txtReserveTypeComments.value;

//Aman Multi Currency
if (txtReserveTypePaid != null) {
    RSW_MultiCurrencyToDecimal(txtReserveTypePaid);
}
if (txtReserveTypePrevOutStanding != null) {
    RSW_MultiCurrencyToDecimal(txtReserveTypePrevOutStanding);
}
       
amount = txtReserveTypePaid.getAttribute("Amount");
//dTotalPaidAmt += ConvertStrToDbl(txtReserveTypePaid.value);
dTotalPaidAmt = Number(dTotalPaidAmt) + Number(amount);
//dTotalPrevOutStandingAmt += ConvertStrToDbl(txtReserveTypePrevOutStanding.value);
dTotalPrevOutStandingAmt = Number(dTotalPrevOutStandingAmt) + Number(txtReserveTypePrevOutStanding.getAttribute("Amount"));
//dTotalNewOutStandingAmt += ConvertStrToDbl(hdnReserveTypeNewOutStanding.value);
dTotalNewOutStandingAmt = Number(dTotalNewOutStandingAmt) + Number(hdnReserveTypeNewOutStanding.value);
//dTotalNewIncurredAmt += ConvertStrToDbl(hdnReserveTypeNewIncurred.value);
dTotalNewIncurredAmt = Number(dTotalNewIncurredAmt) + Number(hdnReserveTypeNewIncurred.value);
        if (txtReserveTypeComments.value != '')
            sReserveTypeComments += txtReserveTypeComments.value + '\r\n';
        else
            sReserveTypeComments += txtReserveTypeComments.value;
  
     // if((ConvertStrToDbl(txtReserveTypePaid.value) + ConvertStrToDbl(txtReserveTypePrevOutStanding.value)) == ConvertStrToDbl(hdnReserveTypeNewIncurred.value))
        if (Number(txtReserveTypePaid.getAttribute("Amount")) + Number(txtReserveTypePrevOutStanding.getAttribute("Amount")) == Number(hdnReserveTypeNewIncurred.value))
        {
            hdnReserveTypeReserveChange.value = "No Change";
            document.getElementById('hdn_REserveChange' + arrCtrls[count].substring(0,3) +  'WC').value= hdnReserveTypeReserveChange.value ;
           // m_bReserveAmntChanges = false;
        }
        else
        {
            //hdnReserveTypeReserveChange.value = (ConvertDblToCurr(ConvertStrToDbl(hdnReserveTypeNewIncurred.value) - (ConvertStrToDbl(txtReserveTypePaid.value) + ConvertStrToDbl(txtReserveTypePrevOutStanding.value))));
            hdnReserveTypeReserveChange.value = hdnReserveTypeNewIncurred.value - Number(txtReserveTypePaid.getAttribute("Amount")) + Number(txtReserveTypePrevOutStanding.getAttribute("Amount"));
            document.getElementById('hdn_REserveChange' + arrCtrls[count].substring(0,3) +  'WC').value=hdnReserveTypeReserveChange.value ;
          //  m_bReserveAmntChanges = true;
        }
        if (hdnReserveTypeReserveChange.value != 'No Change') 
        {
           dTotalReserveChangeAmt = dTotalReserveChangeAmt + Number(hdnReserveTypeReserveChange.value);
        }
        row = document.createElement('tr');
       
        //cell 0
        cell = document.createElement('td');
        cell.className = "data";
        cell.appendChild(document.createTextNode(sReserveCategoryDesc.innerText));
        row.appendChild(cell);
        
        //cell 1
        cell = document.createElement('td');
        cell.className = "data";
        cell.appendChild(document.createTextNode(txtReserveTypePaid.value));
        row.appendChild(cell);

        //cell 2
        cell = document.createElement('td');
        cell.className = "data";
        cell.appendChild(document.createTextNode(txtReserveTypePrevOutStanding.value));
        row.appendChild(cell);

        //cell 3
        cell = document.createElement('td');
        cell.className = "data";
        //Aman -- Multi Currency Control is being passed to get the type of function to call for the hidden controls
        // and then replacing its value with the decimal value again
        amount = hdnReserveTypeNewOutStanding.value;
        RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeNewOutStanding);
        cell.appendChild(document.createTextNode(hdnReserveTypeNewOutStanding.value));
        row.appendChild(cell);
        hdnReserveTypeNewOutStanding.value = amount;

        //cell 4
        cell = document.createElement('td');
        cell.className = "data";
        //Aman -- Multi Currency Control is being passed to get the type of function to call for the hidden controls
        // and then replacing its value with the decimal value again
        amount = hdnReserveTypeNewIncurred.value;
        RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeNewIncurred);
        cell.appendChild(document.createTextNode(hdnReserveTypeNewIncurred.value));
        row.appendChild(cell);
        hdnReserveTypeNewIncurred.value = amount;

        //cell 5
        cell = document.createElement('td');
        cell.className = "data";
        //Aman -- Multi Currency Control is being passed to get the type of function to call for the hidden controls
        // and then replacing its value with the decimal value again
        amount = hdnReserveTypeReserveChange.value;
        RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
        cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
        row.appendChild(cell);
        hdnReserveTypeReserveChange.value = amount;

        tbo.appendChild(row);
    }
    
    tab.appendChild(tbo);
    root.appendChild(tab);

    //Creating Row showing totals
    
    row = document.createElement('tr');
    row.className = "colheader6";

    var temp = hdnReserveTypeReserveChange.value;

    //cell 0
    cell = document.createElement('td');
    cell.style.color = "White";
    cell.appendChild(document.createTextNode('SUMMARY TOTALS'));
    row.appendChild(cell);

    //cell1
    cell = document.createElement('td');
    cell.style.color = "White";
    hdnReserveTypeReserveChange.value = dTotalPaidAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    //cell.appendChild(document.createTextNode(ConvertDblToCurr(dTotalPaidAmt)));
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
    row.appendChild(cell);

    //cell2
    cell = document.createElement('td');
    cell.style.color = "White";
    hdnReserveTypeReserveChange.value = dTotalPrevOutStandingAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    //cell.appendChild(document.createTextNode(ConvertDblToCurr(dTotalPrevOutStandingAmt)));
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
    row.appendChild(cell);

    //cell3
    cell = document.createElement('td');
    cell.style.color = "White";
    hdnReserveTypeReserveChange.value = dTotalNewOutStandingAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    //cell.appendChild(document.createTextNode(ConvertDblToCurr(dTotalNewOutStandingAmt)));
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
    row.appendChild(cell);

    //cell4
    cell = document.createElement('td');
    cell.style.color = "White";
    hdnReserveTypeReserveChange.value = dTotalNewIncurredAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    //cell.appendChild(document.createTextNode(ConvertDblToCurr(dTotalNewIncurredAmt)));
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
    row.appendChild(cell);

    //cell5
    cell = document.createElement('td');
    cell.style.color = "White";
    hdnReserveTypeReserveChange.value = dTotalReserveChangeAmt;
    RSW_HiddentoMultiCurrencyType(txtReserveTypePaid, hdnReserveTypeReserveChange);
    //cell.appendChild(document.createTextNode(ConvertDblToCurr(dTotalReserveChangeAmt)));
    cell.appendChild(document.createTextNode(hdnReserveTypeReserveChange.value));
    row.appendChild(cell);

    //adding this header row to table body
    tbo.appendChild(row);
    root.appendChild(tab);
    m_ReserveSummaryCreated = true;
    document.getElementById('txt_RESERVESUMMARY_Comments').innerText = sReserveTypeComments;

    hdnReserveTypeReserveChange.value = temp; //Aman ---End
    return false;
    
}


//Safeway: Mohit Yadav: For Reserve Worksheet Comments
function Comm_Blur()
{    
    //Commented as per MITS 12892
    /*var ival = objctrl.value;        
    document.getElementById('CommMedWC').innerText = ival;
    document.getElementById('CommIndWC').innerText = ival;
    document.getElementById('CommRehWC').innerText = ival;
    document.getElementById('CommLegWC').innerText = ival;
    document.getElementById('CommOthWC').innerText = ival;
    document.getElementById('AllCommWC').innerText = ival;*/

    //Changes as per MITS 12892        
    //var sval = objctrl.value;
    var sCommAll = '';
    
    if (document.getElementById('CommMedWC') != null)
        sCommAll = document.getElementById('CommMedWC').innerText;//Medical Comment
    if (document.getElementById('CommTemWC') != null)
        sCommAll = sCommAll + '\r\n' + document.getElementById('CommTemWC').innerText;//Temporary Disability Comment
    if (document.getElementById('CommPerWC') != null)
        sCommAll = sCommAll + '\r\n' + document.getElementById('CommPerWC').innerText;//Permanent Disability Comment
    if (document.getElementById('CommVocWC') != null)
        sCommAll = sCommAll + '\r\n' + document.getElementById('CommVocWC').innerText;//Vocational Rehabilitation Comment
    if (document.getElementById('CommLegWC') != null)
        sCommAll = sCommAll + '\r\n' + document.getElementById('CommLegWC').innerText;//Legal Comment
    if (document.getElementById('CommAllWC') != null)
        sCommAll = sCommAll + '\r\n' + document.getElementById('CommAllWC').innerText;
    if (document.getElementById('CommResSummWC') != null)
        document.getElementById('CommResSummWC').innerText = sCommAll;
}

//Safeway: Mohit Yadav: For Reserve Worksheet Reserve Summary
function CalcResAna()
{
	var sCurAmt;
	var sNewList = "ResSummWCList;RowId|Paid|PrevBal|NewBal|NewInc|ResChg";
	//For Medical WC Reserves
	if (document.getElementById('ResSummWC|2|Paid|TD|') != null &&
	document.getElementById('PaidAmtMedWC') != null)
	{
	    document.getElementById('ResSummWC|2|Paid|TD|').innerText = document.getElementById('PaidAmtMedWC').value;
	    document.getElementById('ResSummWC|2|PrevBal|TD|').innerText = document.getElementById('BalAmtMedWC').value;
	    //document.getElementById('ResSummWC|2|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(document.getElementById('NewBalAmtMedWC').value));
	    document.getElementById('ResSummWC|2|NewBal|TD|').innerText = document.getElementById('NewBalAmtMedWC').value;
	    document.getElementById('ResSummWC|2|NewInc|TD|').innerText = document.getElementById('NewIncAmtMedWC').value;
	}
	//For Temporary Disability Reserves
	if (document.getElementById('ResSummWC|3|Paid|TD|') != null &&
	document.getElementById('PaidAmtTemWC') != null)
	{
	    document.getElementById('ResSummWC|3|Paid|TD|').innerText = document.getElementById('PaidAmtTemWC').value;
	    document.getElementById('ResSummWC|3|PrevBal|TD|').innerText = document.getElementById('BalAmtTemWC').value;
	    //document.getElementById('ResSummWC|3|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(document.getElementById('NewBalAmtTemWC').value));
	    document.getElementById('ResSummWC|3|NewBal|TD|').innerText = document.getElementById('NewBalAmtTemWC').value;
	    document.getElementById('ResSummWC|3|NewInc|TD|').innerText = document.getElementById('NewIncAmtTemWC').value;
	}
	//For Permanent Disability Reserves
	if (document.getElementById('ResSummWC|4|Paid|TD|') != null &&
	document.getElementById('PaidAmtPerWC') != null)
	{
	    document.getElementById('ResSummWC|4|Paid|TD|').innerText = document.getElementById('PaidAmtPerWC').value;
	    document.getElementById('ResSummWC|4|PrevBal|TD|').innerText = document.getElementById('BalAmtPerWC').value;
	    //document.getElementById('ResSummWC|4|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(document.getElementById('NewBalAmtPerWC').value));
	    document.getElementById('ResSummWC|4|NewBal|TD|').innerText = document.getElementById('NewBalAmtPerWC').value;
	    document.getElementById('ResSummWC|4|NewInc|TD|').innerText = document.getElementById('NewIncAmtPerWC').value;
	}
	//For Voc Rehab WC Reserves
	if (document.getElementById('ResSummWC|5|Paid|TD|') != null &&
	document.getElementById('PaidAmtVocWC') != null)
	{
	    document.getElementById('ResSummWC|5|Paid|TD|').innerText = document.getElementById('PaidAmtVocWC').value;
	    document.getElementById('ResSummWC|5|PrevBal|TD|').innerText = document.getElementById('BalAmtVocWC').value;
	    //document.getElementById('ResSummWC|5|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(document.getElementById('NewBalAmtVocWC').value));
	    document.getElementById('ResSummWC|5|NewBal|TD|').innerText = document.getElementById('NewBalAmtVocWC').value;
	    document.getElementById('ResSummWC|5|NewInc|TD|').innerText = document.getElementById('NewIncAmtVocWC').value;
	}
	//For Legal WC Reserves
	if (document.getElementById('ResSummWC|6|Paid|TD|') != null &&
	document.getElementById('PaidAmtLegWC') != null)
	{
	    document.getElementById('ResSummWC|6|Paid|TD|').innerText = document.getElementById('PaidAmtLegWC').value;
	    document.getElementById('ResSummWC|6|PrevBal|TD|').innerText = document.getElementById('BalAmtLegWC').value;
	    //document.getElementById('ResSummWC|6|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(document.getElementById('NewBalAmtLegWC').value));
	    document.getElementById('ResSummWC|6|NewBal|TD|').innerText = document.getElementById('NewBalAmtLegWC').value;
	    document.getElementById('ResSummWC|6|NewInc|TD|').innerText = document.getElementById('NewIncAmtLegWC').value;
	}
	//For Allocated Expense WC Reserves
	if (document.getElementById('ResSummWC|7|Paid|TD|') != null &&
	document.getElementById('PaidAmtAllWC') != null)
	{
	    document.getElementById('ResSummWC|7|Paid|TD|').innerText = document.getElementById('PaidAmtAllWC').value;
	    document.getElementById('ResSummWC|7|PrevBal|TD|').innerText = document.getElementById('BalAmtAllWC').value;
	    //document.getElementById('ResSummWC|7|NewBal|TD|').innerText = ConvertDblToCurr(ConvertStrToDbl(document.getElementById('NewBalAmtAllWC').value));
	    document.getElementById('ResSummWC|7|NewBal|TD|').innerText = document.getElementById('NewBalAmtAllWC').value;
	    document.getElementById('ResSummWC|7|NewInc|TD|').innerText = document.getElementById('NewIncAmtAllWC').value;
	}
	//For Reserve Change
	if (document.getElementById('ResSummWC|2|Paid|TD|') != null)
	{
	    if ((ConvertStrToDbl(document.getElementById('ResSummWC|2|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|2|PrevBal|TD|').innerText)) == ConvertStrToDbl(document.getElementById('ResSummWC|2|NewInc|TD|').innerText))
	        document.getElementById('ResSummWC|2|ResChg|TD|').innerText = "No Change";
	    else
	        document.getElementById('ResSummWC|2|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(document.getElementById('ResSummWC|2|NewInc|TD|').innerText) - (ConvertStrToDbl(document.getElementById('ResSummWC|2|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|2|PrevBal|TD|').innerText))));
    }
    if (document.getElementById('ResSummWC|3|Paid|TD|') != null)
	{
        if ((ConvertStrToDbl(document.getElementById('ResSummWC|3|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|3|PrevBal|TD|').innerText)) == ConvertStrToDbl(document.getElementById('ResSummWC|3|NewInc|TD|').innerText))
	        document.getElementById('ResSummWC|3|ResChg|TD|').innerText = "No Change";
	    else
	        document.getElementById('ResSummWC|3|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(document.getElementById('ResSummWC|3|NewInc|TD|').innerText) - (ConvertStrToDbl(document.getElementById('ResSummWC|3|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|3|PrevBal|TD|').innerText))));
	}
	if (document.getElementById('ResSummWC|4|Paid|TD|') != null)
	{    
        if ((ConvertStrToDbl(document.getElementById('ResSummWC|4|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|4|PrevBal|TD|').innerText)) == ConvertStrToDbl(document.getElementById('ResSummWC|4|NewInc|TD|').innerText))
	        document.getElementById('ResSummWC|4|ResChg|TD|').innerText = "No Change";
	    else
	        document.getElementById('ResSummWC|4|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(document.getElementById('ResSummWC|4|NewInc|TD|').innerText) - (ConvertStrToDbl(document.getElementById('ResSummWC|4|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|4|PrevBal|TD|').innerText))));
	}
	if (document.getElementById('ResSummWC|5|Paid|TD|') != null)
	{     
	    if ((ConvertStrToDbl(document.getElementById('ResSummWC|5|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|5|PrevBal|TD|').innerText)) == ConvertStrToDbl(document.getElementById('ResSummWC|5|NewInc|TD|').innerText))
	        document.getElementById('ResSummWC|5|ResChg|TD|').innerText = "No Change";
	    else
	        document.getElementById('ResSummWC|5|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(document.getElementById('ResSummWC|5|NewInc|TD|').innerText) - (ConvertStrToDbl(document.getElementById('ResSummWC|5|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|5|PrevBal|TD|').innerText))));
	}    
    if (document.getElementById('ResSummWC|6|Paid|TD|') != null)
	{
        if ((ConvertStrToDbl(document.getElementById('ResSummWC|6|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|6|PrevBal|TD|').innerText)) == ConvertStrToDbl(document.getElementById('ResSummWC|6|NewInc|TD|').innerText))
	        document.getElementById('ResSummWC|6|ResChg|TD|').innerText = "No Change";
	    else
	        document.getElementById('ResSummWC|6|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(document.getElementById('ResSummWC|6|NewInc|TD|').innerText) - (ConvertStrToDbl(document.getElementById('ResSummWC|6|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|6|PrevBal|TD|').innerText))));
	}
	if (document.getElementById('ResSummWC|7|Paid|TD|') != null)
	{
        if ((ConvertStrToDbl(document.getElementById('ResSummWC|7|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|7|PrevBal|TD|').innerText)) == ConvertStrToDbl(document.getElementById('ResSummWC|7|NewInc|TD|').innerText))
	        document.getElementById('ResSummWC|7|ResChg|TD|').innerText = "No Change";
	    else
	        document.getElementById('ResSummWC|7|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(document.getElementById('ResSummWC|7|NewInc|TD|').innerText) - (ConvertStrToDbl(document.getElementById('ResSummWC|7|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|7|PrevBal|TD|').innerText))));
	}
	//For Summary Totals
    var TPaid = 0.0;
    var TPBal = 0.0;
    var TNBal = 0.0;
    var TNInc = 0.0;
    
    for (var i = 2; i < 8; i ++)	
	{
	    if (document.getElementById('ResSummWC|'+ i +'|Paid|TD|') != null)
	    {
	        TPaid += ConvertStrToDbl(document.getElementById('ResSummWC|'+ i +'|Paid|TD|').innerText);
	        TPBal += ConvertStrToDbl(document.getElementById('ResSummWC|'+ i +'|PrevBal|TD|').innerText);
	        TNBal += ConvertStrToDbl(document.getElementById('ResSummWC|'+ i +'|NewBal|TD|').innerText);
	        TNInc += ConvertStrToDbl(document.getElementById('ResSummWC|'+ i +'|NewInc|TD|').innerText);
	    }
	}
    
	if (document.getElementById('ResSummWC|8|Paid|TD|') != null)
	{
	    document.getElementById('ResSummWC|8|Paid|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(TPaid));
	    document.getElementById('ResSummWC|8|PrevBal|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(TPBal));
	    document.getElementById('ResSummWC|8|NewBal|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(TNBal));
	    document.getElementById('ResSummWC|8|NewInc|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(TNInc));
    	
	    if ((ConvertStrToDbl(document.getElementById('ResSummWC|8|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|8|PrevBal|TD|').innerText)) == ConvertStrToDbl(document.getElementById('ResSummWC|8|NewInc|TD|').innerText))
        {
            document.getElementById('ResSummWC|8|ResChg|TD|').innerText = "No Change";
	        document.getElementById('hdnclaimType').value = "-1";
	    }
	    else
	    {
	        document.getElementById('ResSummWC|8|ResChg|TD|').innerText = CorrectNegAmt(ConvertDblToCurr(ConvertStrToDbl(document.getElementById('ResSummWC|8|NewInc|TD|').innerText) - (ConvertStrToDbl(document.getElementById('ResSummWC|8|Paid|TD|').innerText) + ConvertStrToDbl(document.getElementById('ResSummWC|8|PrevBal|TD|').innerText))));
	        document.getElementById('hdnclaimType').value = "0";
	    }
	}
	
	sNewList = sNewList + 
	    ";1|" + document.getElementById('ResSummWC|2|Paid|TD|').innerText + "|" + document.getElementById('ResSummWC|2|PrevBal|TD|').innerText + "|" + document.getElementById('ResSummWC|2|NewBal|TD|').innerText + "|" + document.getElementById('ResSummWC|2|NewInc|TD|').innerText + "|" + document.getElementById('ResSummWC|2|ResChg|TD|').innerText +
		";2|" + document.getElementById('ResSummWC|3|Paid|TD|').innerText + "|" + document.getElementById('ResSummWC|3|PrevBal|TD|').innerText + "|" + document.getElementById('ResSummWC|3|NewBal|TD|').innerText + "|" + document.getElementById('ResSummWC|3|NewInc|TD|').innerText + "|" + document.getElementById('ResSummWC|3|ResChg|TD|').innerText +
		";3|" + document.getElementById('ResSummWC|4|Paid|TD|').innerText + "|" + document.getElementById('ResSummWC|4|PrevBal|TD|').innerText + "|" + document.getElementById('ResSummWC|4|NewBal|TD|').innerText + "|" + document.getElementById('ResSummWC|4|NewInc|TD|').innerText + "|" + document.getElementById('ResSummWC|4|ResChg|TD|').innerText +
		";4|" + document.getElementById('ResSummWC|5|Paid|TD|').innerText + "|" + document.getElementById('ResSummWC|5|PrevBal|TD|').innerText + "|" + document.getElementById('ResSummWC|5|NewBal|TD|').innerText + "|" + document.getElementById('ResSummWC|5|NewInc|TD|').innerText + "|" + document.getElementById('ResSummWC|5|ResChg|TD|').innerText +
		";5|" + document.getElementById('ResSummWC|6|Paid|TD|').innerText + "|" + document.getElementById('ResSummWC|6|PrevBal|TD|').innerText + "|" + document.getElementById('ResSummWC|6|NewBal|TD|').innerText + "|" + document.getElementById('ResSummWC|6|NewInc|TD|').innerText + "|" + document.getElementById('ResSummWC|6|ResChg|TD|').innerText +
		";6|" + document.getElementById('ResSummWC|7|Paid|TD|').innerText + "|" + document.getElementById('ResSummWC|7|PrevBal|TD|').innerText + "|" + document.getElementById('ResSummWC|7|NewBal|TD|').innerText + "|" + document.getElementById('ResSummWC|7|NewInc|TD|').innerText + "|" + document.getElementById('ResSummWC|7|ResChg|TD|').innerText +
		";7|" + document.getElementById('ResSummWC|8|Paid|TD|').innerText + "|" + document.getElementById('ResSummWC|8|PrevBal|TD|').innerText + "|" + document.getElementById('ResSummWC|8|NewBal|TD|').innerText + "|" + document.getElementById('ResSummWC|8|NewInc|TD|').innerText + "|" + document.getElementById('ResSummWC|8|ResChg|TD|').innerText;
		
		
	document.getElementById('hdnResSummWCList').value = sNewList;
	
	return false;
}

function TabChange(objname)
{
	document.forms[0].hTabName.value = objname;	
	var tmpName="";
	//Deselect All
	for(i=0;i<m_TabList.length;i++)
	{
		tmpName=m_TabList[i].id.substring(4);
		eval('document.all.FORMTAB'+tmpName+'.style.display="none"');
		eval('document.all.TABS'+tmpName+'.className="NotSelected"');
		eval('document.all.LINKTABS'+tmpName+'.className="NotSelected1"');
	}
	//Select the tab requested
	eval('document.all.FORMTAB'+objname+'.style.display=""');
	eval('document.all.TABS'+objname+'.className="Selected"');
	eval('document.all.LINKTABS'+objname+'.className="Selected"');
	setDefaultFocus();
	
	Comm_Blur();
	//commented by shobhana
	//
	//CalcResAna();
	CreateReserveSummary();
	
    //ResReasonMedWC, ResReasonMedWCbtn, ResReasonMedWC_cid
    if (document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (document.getElementById('ResReasonMedWC') != null)
        {
            document.getElementById('ResReasonMedWC').disabled = true;
		    document.getElementById('ResReasonMedWCbtn').disabled = true;
		}
    }
    
    //TemWCResReason, ResReasonTemWCbtn, ResReasonTemWC_cid
    if (document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (document.getElementById('ResReasonTemWC') != null)
        {
            document.getElementById('ResReasonTemWC').disabled = true;
		    document.getElementById('ResReasonTemWCbtn').disabled = true;
		}
    }
    
    //PerWCResReason, ResReasonPerWCbtn, ResReasonPerWC_cid
    if (document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (document.getElementById('ResReasonPerWC') != null)
        {
            document.getElementById('ResReasonPerWC').disabled = true;
		    document.getElementById('ResReasonPerWCbtn').disabled = true;
		}
    }
    
    //ResReasonVocWC, ResReasonVocWCbtn, ResReasonVocWC_cid
    if (document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (document.getElementById('ResReasonVocWC') != null)
        {
            document.getElementById('ResReasonVocWC').disabled = true;
		    document.getElementById('ResReasonVocWCbtn').disabled = true;
		}
    }
    
    //ResReasonLegWC, ResReasonLegWCbtn, ResReasonLegWC_cid
    if (document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (document.getElementById('ResReasonLegWC') != null)
        {
            document.getElementById('ResReasonLegWC').disabled = true;
		    document.getElementById('ResReasonLegWCbtn').disabled = true;
		}
    }
    
    //ResReasonAllWC, ResReasonAllWCbtn, ResReasonAllWC_cid
    if (document.getElementById('hdnMode').value=='ReadOnly')
    {
        if (document.getElementById('ResReasonAllWC') != null)
        {
            document.getElementById('ResReasonAllWC').disabled = true;
		    document.getElementById('ResReasonAllWCbtn').disabled = true;
	    }
    }
	
	return true;
}

function ConfirmSave()
{
	var ret = false;
	if(m_DataChanged){
		ret = confirm('Data has changed. Do you want to save changes?');
		if (ret==false)
			m_DataChanged = false;
	}
	return ret;
}

function RSWsetDataChanged(b)
{
    m_DataChanged=b;
    return b;
}

function CalculateResAmount(dNewBalance, dPaid, dCollect)
{

 var dTemp = 0.0; 
 var dNewResAmt = 0.0;
 
 //Calculate New Reserve Amount
 if (document.getElementById('hdnCollInRsvBal').value == "True")
 {
	dTemp = dPaid - dCollect;	
    if (dTemp < 0) 
        dTemp = 0.0;        
    dNewResAmt = dNewBalance + dTemp;
 }
 else
	dNewResAmt = dNewBalance + dPaid;

 return dNewResAmt;
}

function CalculateIncurred(dNewBalance, dPaid, dCollect)
{//debugger;
 var dTemp = 0.0;
 var dNewIncurred = 0.0;
  
 //Calculate New Incurred Amount
 if (document.getElementById('hdnCollInRsvBal').value == "True")
 {
  dTemp = dPaid - dCollect;
  if (dTemp < 0)
      dTemp = 0.0;
  if (dNewBalance < 0)
      dNewIncurred = dTemp;
  else
      dNewIncurred = dNewBalance + dTemp;
 }
 else
 {
    if (dNewBalance < 0)
        dNewIncurred = dPaid;
    else
        dNewIncurred = dNewBalance + dPaid;
 }
    
 if (document.getElementById('hdnCollInIncurredBal').value == "True")
 {
    dNewIncurred = dNewIncurred - dCollect;
 }

 if (dNewIncurred < 0)
    dNewIncurred = 0.0;
    
 return dNewIncurred;    
}

function AddMedicalWC() {
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;

 //Aman Multi Currency-- Start


 //Hospital Expense
 if (document.getElementById('Amt1MedWC') != null) 
 {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt1MedWC'));
     //dTotNewAmt += ConvertStrToDbl(document.getElementById('Amt1MedWC').value);
     dTotNewAmt += Number(document.getElementById('Amt1MedWC').getAttribute("Amount"));
 }
 //Physician Expense
 if (document.getElementById('Amt2MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt2MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt2MedWC').getAttribute("Amount"));
 }
 //Physical Therapy /Chiro/Work Hardening
 if (document.getElementById('Amt3MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt3MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt3MedWC').getAttribute("Amount"));
 }
 //Medical Case Management
 if (document.getElementById('Amt4MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt4MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt4MedWC').getAttribute("Amount"));
 }
 //Utilization Review
 if (document.getElementById('Amt5MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt5MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt5MedWC').getAttribute("Amount"));
 }
 //Diagnostic Testing
 if (document.getElementById('Amt6MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt6MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt6MedWC').getAttribute("Amount"));
 }
 //Drugs, Prosthetics, etc.
 if (document.getElementById('Amt7MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt7MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt7MedWC').getAttribute("Amount"));
 }
 //2nd Opinion/Consult (not legal)
 if (document.getElementById('Amt8MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt8MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt8MedWC').getAttribute("Amount"));
 }
 //Mileage
 if (document.getElementById('Amt9MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt9MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt9MedWC').getAttribute("Amount"));
 }
 //Equipment
 if (document.getElementById('Amt10MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt10MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt10MedWC').getAttribute("Amount"));
 }
 //Legal Exams/Procedures
 if (document.getElementById('Amt11MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt11MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt11MedWC').getAttribute("Amount"));
 }
 //Other (Future Medical, etc.)
 if (document.getElementById('Amt12MedWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt12MedWC'));
     dTotNewAmt += Number(document.getElementById('Amt12MedWC').getAttribute("Amount"));
 }
//debugger;
if (document.getElementById('PaidAmtMedWC') != null &&  dTotNewAmt > 0.00) {
    RSW_MultiCurrencyToDecimal(document.getElementById('PaidAmtMedWC'));
    dPaid = Number(document.getElementById('PaidAmtMedWC').getAttribute("Amount"));
    dCollect = Number(document.getElementById('hdnCollMedWC').value);
 
    //document.getElementById('NewBalAmtMedWC').innerText = ConvertDblToCurr(dTotNewAmt);
    //  document.getElementById('hdn_NewBalAmtMedWC').innerText = ConvertDblToCurr(dTotNewAmt);

    document.getElementById('NewBalAmtMedWC').value = dTotNewAmt;
    RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtMedWC'));
    document.getElementById('hdn_NewBalAmtMedWC').value = dTotNewAmt;

    dNewBalance = dTotNewAmt;
 
    dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
    document.getElementById('hdnNewResMedWC').value = dNewResAmt;
     
    dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
    if (dNewIncurred != dPaid)
    {
//       document.getElementById('NewIncAmtMedWC').innerText = ConvertDblToCurr(dNewIncurred);
//       document.getElementById('hdn_NewIncAmtMedWC').innerText = ConvertDblToCurr(dNewIncurred);
           document.getElementById('NewIncAmtMedWC').value = dNewIncurred;
           RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtMedWC'));
           document.getElementById('hdn_NewIncAmtMedWC').value = dNewIncurred;
      }
    else
      {
      // document.getElementById('NewIncAmtMedWC').innerText = ConvertDblToCurr(0.00);
//       document.getElementById('hdn_NewIncAmtMedWC').innerText = ConvertDblToCurr(0.00);
         document.getElementById('NewIncAmtMedWC').value = 0;
         RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtMedWC'));
         document.getElementById('hdn_NewIncAmtMedWC').value = 0;
       }
 }
 else if (document.getElementById('PaidAmtMedWC') != null && dTotNewAmt == 0.00)
 {
//     document.getElementById('NewBalAmtMedWC').innerText = (document.getElementById('BalAmtMedWC').value);
//    document.getElementById('hdn_NewBalAmtMedWC').innerText = ( document.getElementById('BalAmtMedWC').value);
//    document.getElementById('NewIncAmtMedWC').innerText = (document.getElementById('hdnIncurredMedWC').value);
//    document.getElementById('hdn_NewIncAmtMedWC').innerText = (document.getElementById('hdnIncurredMedWC').value);
     //    document.getElementById('hdnNewResMedWC').innerText = ConvertDblToCurr(dNewResAmt);

     document.getElementById('NewBalAmtMedWC').value = (document.getElementById('BalAmtMedWC').getAttribute("Amount"));
     RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtMedWC'));
     document.getElementById('hdn_NewBalAmtMedWC').value = (document.getElementById('BalAmtMedWC').getAttribute("Amount"));
     document.getElementById('NewIncAmtMedWC').value = (document.getElementById('hdnIncurredMedWC').value);
     RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtMedWC'));
     document.getElementById('hdn_NewIncAmtMedWC').value = (document.getElementById('hdnIncurredMedWC').value);
     document.getElementById('hdnNewResMedWC').value = dNewResAmt;

 }
 //shobhana
     m_arrReasonsToBeChecked['MedicalWC'] = 'MedicalWC';
            m_arrCommentsToBeChecked['MedicalWC'] = 'MedicalWC'; 
 
}

function AddTempDisWC()
 { 

 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;

 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 //debugger;
 //Aman Multi Currency-- Start
 if (document.getElementById('txtAmt1TemWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt1TemWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt1TemWC').getAttribute("Amount"));
 }
 if (document.getElementById('txtAmt2TemWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt2TemWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt2TemWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt3TemWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt3TemWC'));
     dTotNewAmt += Number(document.getElementById('Amt3TemWC').getAttribute("Amount"));
 }
 
 if (document.getElementById('PaidAmtTemWC') != null && dTotNewAmt > 0.00)
  {
    //dPaid = ConvertStrToDbl(document.getElementById('PaidAmtTemWC').value);
    //dCollect = ConvertStrToDbl(document.getElementById('hdnCollTemWC').value);
      RSW_MultiCurrencyToDecimal(document.getElementById('PaidAmtTemWC'));
      dPaid = Number(document.getElementById('PaidAmtTemWC').getAttribute("Amount"));
      dCollect = Number(document.getElementById('hdnCollTemWC').value);

      document.getElementById('NewBalAmtTemWC').value = Number(dTotNewAmt);
      RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtTemWC'));
    document.getElementById('hdn_NewBalAmtTemWC').value = Number(dTotNewAmt);
    dNewBalance = dTotNewAmt;
 
    dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
    document.getElementById('hdnNewResTemWC').value = Number(dNewResAmt);
 
    dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
    if (dNewIncurred != dPaid) {  
//      document.getElementById('NewIncAmtTemWC').innerText = ConvertDblToCurr(dNewIncurred);
        //          document.getElementById('hdn_NewIncAmtTemWC').innerText = ConvertDblToCurr(dNewIncurred);
        document.getElementById('NewIncAmtTemWC').value = Number(dNewIncurred);
        RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtTemWC'));
        document.getElementById('hdn_NewIncAmtTemWC').value = Number(dNewIncurred);
      }
    else
     {
        //document.getElementById('NewIncAmtTemWC').innerText = ConvertDblToCurr(0.00);
         //document.getElementById('hdn_NewIncAmtTemWC').innerText = ConvertDblToCurr(0.00);
         document.getElementById('NewIncAmtTemWC').value = 0;
         RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtTemWC'));
         document.getElementById('hdn_NewIncAmtTemWC').value = 0;
     }
  }
  else if (document.getElementById('PaidAmtTemWC') != null && dTotNewAmt == 0.00)
  {
//    document.getElementById('NewBalAmtTemWC').innerText = ( document.getElementById('BalAmtTemWC').value);
//    document.getElementById('hdn_NewBalAmtTemWC').innerText = ( document.getElementById('BalAmtTemWC').value);
//    document.getElementById('NewIncAmtTemWC').innerText = (document.getElementById('hdnIncurredTemWC').value);
//    document.getElementById('hdn_NewIncAmtTemWC').innerText = (document.getElementById('hdnIncurredTemWC').value);
//    document.getElementById('hdnNewResTemWC').innerText = ConvertDblToCurr(dNewResAmt);

      document.getElementById('NewBalAmtTemWC').value = (document.getElementById('BalAmtTemWC').getAttribute("Amount"));
      RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtTemWC'));
      document.getElementById('hdn_NewBalAmtTemWC').value = (document.getElementById('BalAmtTemWC').getAttribute("Amount"));
      document.getElementById('NewIncAmtTemWC').value = (document.getElementById('hdnIncurredTemWC').value);
      RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtTemWC'));
      document.getElementById('hdn_NewIncAmtTemWC').value = (document.getElementById('hdnIncurredTemWC').value);
      document.getElementById('hdnNewResTemWC').value = Number(dNewResAmt);   
  }//Aman Multi Currency-- End
//  debugger;
//shobhana
     m_arrReasonsToBeChecked['TemporaryDisabilityWC'] = 'TemporaryDisabilityWC';
            m_arrCommentsToBeChecked['TemporaryDisabilityWC'] = 'TemporaryDisabilityWC'; 
}

function AddPermDisWC()
{ 
//debugger;
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 //Aman Multi Currency-- Start
 if (document.getElementById('txtAmt1PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt1PerWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt1PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('txtAmt2PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt2PerWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt2PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('txtAmt3PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt3PerWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt3PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('txtAmt4PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt4PerWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt4PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('txtAmt5PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt5PerWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt5PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('txtAmt6PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt6PerWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt6PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt7PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt7PerWC'));
     dTotNewAmt += Number(document.getElementById('Amt7PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt8PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt8PerWC'));
     dTotNewAmt += Number(document.getElementById('Amt8PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('AmtPerTWC9') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('AmtPerTWC9'));
     dTotNewAmt += Number(document.getElementById('AmtPerTWC9').getAttribute("Amount"));
 }
 if (document.getElementById('txtAmt10PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt10PerWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt10PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('txtAmt11PerWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt11PerWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt11PerWC').getAttribute("Amount"));
 }
 if (document.getElementById('AmtPerWC12') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('AmtPerWC12'));
     dTotNewAmt += Number(document.getElementById('AmtPerWC12').getAttribute("Amount"));
 }
 if (document.getElementById('AmtPerWC13') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('AmtPerWC13'));
     dTotNewAmt += Number(document.getElementById('AmtPerWC13').getAttribute("Amount"));
 }
 if (document.getElementById('AmtPerWC14') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('AmtPerWC14'));
     dTotNewAmt += Number(document.getElementById('AmtPerWC14').getAttribute("Amount"));
 }
 if (document.getElementById('PaidAmtPerWC') != null && dTotNewAmt > 0.00) {

    //dPaid = ConvertStrToDbl(document.getElementById('PaidAmtPerWC').value);
    //dCollect = ConvertStrToDbl(document.getElementById('hdnCollPerWC').value);

    RSW_MultiCurrencyToDecimal(document.getElementById('PaidAmtPerWC'));
    dPaid = Number(document.getElementById('PaidAmtPerWC').getAttribute("Amount"));
    dCollect = Number(document.getElementById('hdnCollPerWC').value);

    document.getElementById('NewBalAmtPerWC').value = dTotNewAmt;
    RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtPerWC'));
    document.getElementById('hdn_NewBalAmtPerWC').value = dTotNewAmt;
    dNewBalance = dTotNewAmt;
 //debugger;
    dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
    document.getElementById('hdnNewResPerWC').value = dNewResAmt;
 
    dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
    if (dNewIncurred != dPaid) 
    {            
       //document.getElementById('NewIncAmtPerWC').innerText = ConvertDblToCurr(dNewIncurred);
        //document.getElementById('hdn_NewIncAmtPerWC').innerText = ConvertDblToCurr(dNewIncurred);
        document.getElementById('NewIncAmtPerWC').value = dNewIncurred;
        RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtPerWC'));
        document.getElementById('hdn_NewIncAmtPerWC').value = dNewIncurred;
    }
    else 
    {  
      //document.getElementById('hdn_NewIncAmtPerWC').innerText = ConvertDblToCurr(0.00);
      //document.getElementById('NewIncAmtPerWC').innerText = ConvertDblToCurr(0.00);
      document.getElementById('hdn_NewIncAmtPerWC').value = 0;
      document.getElementById('NewIncAmtPerWC').value = 0;
      RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtPerWC'));
    }
  }
  else if (document.getElementById('PaidAmtPerWC') != null && dTotNewAmt == 0.00)
  {
//      document.getElementById('NewBalAmtPerWC').innerText = (document.getElementById('BalAmtPerWC').value);
//    document.getElementById('hdn_NewBalAmtPerWC').innerText = ( document.getElementById('BalAmtPerWC').value);
//    document.getElementById('NewIncAmtPerWC').innerText = (document.getElementById("hdnIncurredPerWC").value);
//    document.getElementById('hdn_NewIncAmtPerWC').innerText = (document.getElementById("hdnIncurredPerWC").value);
      //    document.getElementById('hdnNewResPerWC').innerText = ConvertDblToCurr(dNewResAmt);
      RSW_MultiCurrencyToDecimal(document.getElementById('BalAmtPerWC'));
      document.getElementById('NewBalAmtPerWC').value = (document.getElementById('BalAmtPerWC').getAttribute("Amount"));
      RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtPerWC'));
      document.getElementById('hdn_NewBalAmtPerWC').value = (document.getElementById('BalAmtPerWC').getAttribute("Amount"));
      document.getElementById('NewIncAmtPerWC').value = (document.getElementById("hdnIncurredPerWC").value);
      RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtPerWC'));
      document.getElementById('hdn_NewIncAmtPerWC').value = (document.getElementById("hdnIncurredPerWC").value);
      document.getElementById('hdnNewResPerWC').value = ConvertDblToCurr(dNewResAmt);

  }//Aman Multi Currency-- End
  //shobhana
   m_arrReasonsToBeChecked['PermanentDisabilityWC'] = 'PermanentDisabilityWC';
            m_arrCommentsToBeChecked['PermanentDisabilityWC'] = 'PermanentDisabilityWC'; 
}

function AddVocRehabWC()
{ 
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 //Aman Multi Currency --Start
 if (document.getElementById('txtAmt1VocWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt1VocWC'));
     dTotNewAmt += Number(document.getElementById('txtAmt1VocWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt2VocWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt2VocWC'));
     dTotNewAmt += Number(document.getElementById('Amt2VocWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt3VocWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt3VocWC'));
     dTotNewAmt += Number(document.getElementById('Amt3VocWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt4VocWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt4VocWC'));
     dTotNewAmt += Number(document.getElementById('Amt4VocWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt5VocWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt5VocWC'));
     dTotNewAmt += Number(document.getElementById('Amt5VocWC').getAttribute("Amount"));
 }
 if (document.getElementById('PaidAmtVocWC') != null && dTotNewAmt > 0.00)
 {
    //dPaid = ConvertStrToDbl(document.getElementById('PaidAmtVocWC').value);
     //dCollect = ConvertStrToDbl(document.getElementById('hdnCollVocWC').value);

     RSW_MultiCurrencyToDecimal(document.getElementById('PaidAmtVocWC'));
     dPaid = Number(document.getElementById('PaidAmtVocWC').getAttribute("Amount"));
     dCollect = Number(document.getElementById('hdnCollVocWC').value);
     document.getElementById('NewBalAmtVocWC').value = dTotNewAmt;
     RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtVocWC'));
    document.getElementById('hdn_NewBalAmtVocWC').value = dNewIncurred;}
    dNewBalance = dTotNewAmt;
    
    dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
    document.getElementById('hdnNewResVocWC').value = dNewResAmt;
    
    dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
 if (document.getElementById('PaidAmtVocWC') != null )
 {
    //dPaid = ConvertStrToDbl(document.getElementById('PaidAmtVocWC').value);
    //dCollect = ConvertStrToDbl(document.getElementById('hdnCollVocWC').value);
     RSW_MultiCurrencyToDecimal(document.getElementById('PaidAmtVocWC'));
     dPaid = Number(document.getElementById('PaidAmtVocWC').getAttribute("Amount"));
     dCollect = Number(document.getElementById('hdnCollVocWC').value);

     document.getElementById('NewBalAmtVocWC').value = dTotNewAmt;
     RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtVocWC'));
     document.getElementById('hdn_NewBalAmtVocWC').value = dTotNewAmt;
     dNewBalance = dTotNewAmt;
    
    dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
    document.getElementById('hdnNewResVocWC').value = dNewResAmt;
    
    dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
    if (dNewIncurred != dPaid) {
        document.getElementById('NewIncAmtVocWC').value = dNewIncurred;
        RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtVocWC'));
       document.getElementById('hdn_NewIncAmtVocWC').value = dNewIncurred;
        }
    else {
        document.getElementById('NewIncAmtVocWC').value = 0;
        RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtVocWC'));
       document.getElementById('hdn_NewIncAmtVocWC').value = 0;
      }
 }
 else if (document.getElementById('PaidAmtVocWC') != null && dTotNewAmt == 0.00)
 {
//    document.getElementById('NewBalAmtVocWC').innerText = ( document.getElementById('BalAmtVocWC').value);
//    document.getElementById('hdn_NewBalAmtVocWC').innerText = ( document.getElementById('BalAmtVocWC').value);
//    document.getElementById('NewIncAmtVocWC').innerText = (document.getElementById("hdnIncurredVocWC").value);
//    document.getElementById('hdn_NewIncAmtVocWC').innerText = (document.getElementById("hdnIncurredVocWC").value);
     //    document.getElementById('hdnNewResVocWC').innerText = ConvertDblToCurr(dNewResAmt);

     RSW_MultiCurrencyToDecimal(document.getElementById('BalAmtVocWC'));
     document.getElementById('NewBalAmtVocWC').value = (document.getElementById('BalAmtVocWC').getAttribute("Amount"));
     RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtVocWC'));
     document.getElementById('hdn_NewBalAmtVocWC').value = (document.getElementById('BalAmtVocWC').getAttribute("Amount"));
     document.getElementById('NewIncAmtVocWC').value = (document.getElementById("hdnIncurredVocWC").value);
     RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtVocWC'));
     document.getElementById('hdn_NewIncAmtVocWC').value = (document.getElementById("hdnIncurredVocWC").value);
     document.getElementById('hdnNewResVocWC').value = dNewResAmt;
} //Aman Multi Currency --End
 //shobhana
     m_arrReasonsToBeChecked['VocationalRehabilitationWC'] = 'VocationalRehabilitationWC';
            m_arrCommentsToBeChecked['VocationalRehabilitationWC'] = 'VocationalRehabilitationWC'; 
}

function AddLegalWC()
{ 
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 //Aman Multi Currency --Start
 if (document.getElementById('Amt1LegWC') != null)//Litigation Costs
 {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt1LegWC'));
     dTotNewAmt += Number(document.getElementById('Amt1LegWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt2LegWC') != null)//Expense
 {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt2LegWC'));
     dTotNewAmt += Number(document.getElementById('Amt2LegWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt3LegWC') != null)//Early Intervention
 {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt3LegWC'));
     dTotNewAmt += Number(document.getElementById('Amt3LegWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt4LegWC') != null)//Medical Witness
 {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt4LegWC'));
     dTotNewAmt += Number(document.getElementById('Amt4LegWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt5LegWC') != null)//Other (Describe)
 {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt5LegWC'));
     dTotNewAmt += Number(document.getElementById('Amt5LegWC').getAttribute("Amount"));
 }
//debugger;
 if (document.getElementById('PaidAmtLegWC') != null && dTotNewAmt > 0.00 )
 {
     //dPaid = ConvertStrToDbl(document.getElementById('PaidAmtLegWC').value);
     //dCollect = ConvertStrToDbl(document.getElementById('hdnCollLegWC').value);

     RSW_MultiCurrencyToDecimal(document.getElementById('PaidAmtLegWC'));
     dPaid = Number(document.getElementById('PaidAmtLegWC').getAttribute("Amount"));
     dCollect = Number(document.getElementById('hdnCollLegWC').value);

     document.getElementById('NewBalAmtLegWC').value = dTotNewAmt;
     RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtLegWC'));
     document.getElementById('hdn_NewBalAmtLegWC').value = dTotNewAmt;
     dNewBalance = dTotNewAmt;
     
     dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
     document.getElementById('hdnNewResLegWC').value = dNewResAmt;
     
     dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
     if (dNewIncurred != dPaid) { 
//       document.getElementById('NewIncAmtLegWC').innerText = ConvertDblToCurr(dNewIncurred);
         //        document.getElementById('hdn_NewIncAmtLegWC').innerText = ConvertDblToCurr(dNewIncurred);
         document.getElementById('NewIncAmtLegWC').value = dNewIncurred;
         RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtLegWC'));
         document.getElementById('hdn_NewIncAmtLegWC').value = dNewIncurred;
        }
     else {
         document.getElementById('NewIncAmtLegWC').value = 0;
         RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtLegWC'));
        document.getElementById('hdn_NewIncAmtLegWC').value = 0;
        }
 }
 else if (document.getElementById('PaidAmtLegWC') != null && dTotNewAmt == 0.00)
 {
//     document.getElementById('NewBalAmtLegWC').innerText = (document.getElementById('BalAmtLegWC').value);
//     document.getElementById('hdn_NewBalAmtLegWC').innerText = (document.getElementById('BalAmtLegWC').value);
//    document.getElementById('NewIncAmtLegWC').innerText = (document.getElementById("hdnIncurredLegWC").value);
//    document.getElementById('hdn_NewIncAmtLegWC').innerText = (document.getElementById("hdnIncurredLegWC").value);
//    document.getElementById('hdnNewResLegWC').innerText = ConvertDblToCurr(dNewResAmt);
     
     RSW_MultiCurrencyToDecimal(document.getElementById('BalAmtLegWC'));
     document.getElementById('NewBalAmtLegWC').value = (document.getElementById('BalAmtLegWC').getAttribute("Amount"));
     RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtLegWC'));
     document.getElementById('hdn_NewBalAmtLegWC').value = (document.getElementById('BalAmtLegWC').getAttribute("Amount"));
     document.getElementById('NewIncAmtLegWC').value = (document.getElementById("hdnIncurredLegWC").value);
     RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtLegWC'));
     document.getElementById('hdn_NewIncAmtLegWC').value = (document.getElementById("hdnIncurredLegWC").value);
     document.getElementById('hdnNewResLegWC').value = dNewResAmt;
} //Aman Multi Currency --End
 //shobhana
    m_arrReasonsToBeChecked['LegalWC'] = 'LegalWC';
            m_arrCommentsToBeChecked['LegalWC'] = 'LegalWC'; 
}

function AddAllExpWC()
{ 
 var dTotNewAmt = 0.0;
 var dTemp = 0.0;
 var dPaid = 0.0;
 var dCollect = 0.0;
 
 var dNewResAmt = 0.0;
 var dNewIncurred = 0.0;
 //Aman Multi Currency--Start
 if (document.getElementById('Amt1AllWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt1AllWC'));
     dTotNewAmt += Number(document.getElementById('Amt1AllWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt2AllWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt2AllWC'));
     dTotNewAmt += Number(document.getElementById('Amt2AllWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt3AllWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt3AllWC'));
     dTotNewAmt += Number(document.getElementById('Amt3AllWC').getAttribute("Amount"));
 }
 if (document.getElementById('Amt4AllWC') != null) {
     RSW_MultiCurrencyToDecimal(document.getElementById('Amt4AllWC'));
     dTotNewAmt += Number(document.getElementById('Amt4AllWC').getAttribute("Amount"));
 }
 if (document.getElementById('PaidAmtAllWC') != null && dTotNewAmt > 0.00)
 {
     //dPaid = ConvertStrToDbl(document.getElementById('PaidAmtAllWC').value);
     //dCollect = ConvertStrToDbl(document.getElementById('hdnCollAllWC').value);
     
     RSW_MultiCurrencyToDecimal(document.getElementById('PaidAmtAllWC'));
     dPaid = Number(document.getElementById('PaidAmtAllWC').getAttribute("Amount"));
     dCollect = Number(document.getElementById('hdnCollAllWC').value);
     document.getElementById('NewBalAmtAllWC').value = dTotNewAmt;
     RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtAllWC'));
     document.getElementById('hdn_NewBalAmtAllWC').value = dTotNewAmt;
     
     dNewBalance = dTotNewAmt;
     
     dNewResAmt = CalculateResAmount(dNewBalance,dPaid,dCollect);
     document.getElementById('hdnNewResAllWC').value = dNewResAmt;
     
     dNewIncurred = CalculateIncurred(dNewBalance,dPaid,dCollect);
     if (dNewIncurred != dPaid) { 
       //document.getElementById('NewIncAmtAllWC').innerText = ConvertDblToCurr(dNewIncurred);
       //document.getElementById('hdn_NewIncAmtAllWC').innerText = ConvertDblToCurr(dNewIncurred);
         document.getElementById('NewIncAmtAllWC').value = dNewIncurred;
         RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtAllWC'));
       document.getElementById('hdn_NewIncAmtAllWC').value = dNewIncurred;
          }
     else {
         document.getElementById('NewIncAmtAllWC').value = 0;
         RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtAllWC'));
         document.getElementById('hdn_NewIncAmtAllWC').value = 0;
         }
        
        
         

 }
 else if (document.getElementById('PaidAmtAllWC') != null && dTotNewAmt == 0.00)
 {
//     document.getElementById('NewBalAmtAllWC').innerText = (document.getElementById('BalAmtAllWC').value);
//     document.getElementById('hdn_NewBalAmtMedWC').innerText = (document.getElementById('BalAmtAllWC').value);
//    document.getElementById('NewIncAmtAllWC').innerText = (document.getElementById("hdnIncurredAllWC").value);
//    document.getElementById('hdn_NewIncAmtAllWC').innerText = (document.getElementById("hdnIncurredAllWC").value);
     //    document.getElementById('hdnNewResAllWC').innerText = ConvertDblToCurr(dNewResAmt);

     RSW_MultiCurrencyToDecimal(document.getElementById('BalAmtAllWC'));
     document.getElementById('NewBalAmtAllWC').value = (document.getElementById('BalAmtAllWC').getAttribute("Amount"));
     RSW_MultiCurrencyOnBlur(document.getElementById('NewBalAmtAllWC'));
     document.getElementById('hdn_NewBalAmtMedWC').value = (document.getElementById('BalAmtAllWC').getAttribute("Amount"));
     document.getElementById('NewIncAmtAllWC').value = (document.getElementById("hdnIncurredAllWC").value);
     RSW_MultiCurrencyOnBlur(document.getElementById('NewIncAmtAllWC'));
     document.getElementById('hdn_NewIncAmtAllWC').value = (document.getElementById("hdnIncurredAllWC").value);
     document.getElementById('hdnNewResAllWC').value = dNewResAmt;
      
 // if (dNewIncurred != txtCurrentTransTypeIncurredAmt.value) 
//{
//shobhana
            m_arrReasonsToBeChecked['AllocatedExpenseWC'] = 'AllocatedExpenseWC';
            m_arrCommentsToBeChecked['AllocatedExpenseWC'] = 'AllocatedExpenseWC';             
//hdnCurrentTransTypeIncurredAmt.value = txtCurrentTransTypeIncurredAmt.value;
     //   }
        } //Aman Multi Currency--End
}

function RSWOnClosePromptForPopUp()
{	 	 
	 //get the current URL 
     var url = self.parent.window.opener.location.toString();
     var parentScreen = "";
     //get the parameters 
     url.match(/\?(.+)$/); 
     var params = RegExp.$1; 
     //split up the query string and store in an associative array 
     var params = params.split("&"); 
     var queryStringList = {};  
     for(var i=0;i<1;i++) 
     {     
        var tmp = params[i].split(",");
        if (tmp[0].indexOf("ReserveApproval") > 0)
            parentScreen = "ReserveApproval";
        else
            parentScreen = "ReserveListing";
     }
	 
	 if (parentScreen == "ReserveListing")
	 {
	 
	     if (document.title=='Reserve Worksheet')
         {
            if (self.frames["MainFrame"].location.href.indexOf("MainFrame",1) > 0)
            {
                if (document.forms[0]!=null)
                {
                    if (document.forms[0].action!=null)
                    {
                     if (document.forms[0].action.value=="Save")
                         return;
                     else if(document.forms[0].action.value == "MemoSave")
                        return;                     
                    }
                }
                
                var sMsg = "Please confirm that you have completed work within this pop-up.";    
                
                if ((window.event.clientX < 0) || (window.event.clientY < 0))
                {
	                if(window.event!=null)
                    window.event.returnValue=sMsg;  
                } 
             }
         }
     }
}

function CorrectNegAmt(CtrlVal)
{
	var dbl=new String(CtrlVal);
	CtrlVal = ConvertStrToDbl(CtrlVal);
	
	if(dbl.indexOf('(') >= 0)
	{
	    dbl=dbl.replace(/[\(\)]/g ,"");
	    dbl=-1 * dbl;
	    return dbl;
	}
	
	if (CtrlVal < 0)
	{
	    dbl = dbl.replace(/[,\-]/g ,"");
	    return "(" + dbl + ")";
	}
	else
	    return ConvertDblToCurr(CtrlVal);
}

function CheckRatesBSave()
{
    //Aman Multi Currency--Start
    RSW_MultiCurrencyToDecimal(document.getElementById('txtClaimTempTotalRate'));
    RSW_MultiCurrencyToDecimal(document.getElementById('TempPartialRate'));
    RSW_MultiCurrencyToDecimal(document.getElementById('txtClaimPPDRate'));
    RSW_MultiCurrencyToDecimal(document.getElementById('MaxDurationPTDRate'));
    RSW_MultiCurrencyToDecimal(document.getElementById('txtClaimSurvivorshipRate'));
    RSW_MultiCurrencyToDecimal(document.getElementById('DependencyRate'));
    RSW_MultiCurrencyToDecimal(document.getElementById('txtClaimDisfigurementRate'));
    RSW_MultiCurrencyToDecimal(document.getElementById('LifePensionRate'));
    RSW_MultiCurrencyToDecimal(document.getElementById('txtClaimVocRehabTDRate'));

    if ((document.getElementById('txtClaimTempTotalRate').getAttribute("Amount") != '' &&
    document.getElementById('txtClaimTempTotalRate').getAttribute("Amount") != '0.00') ||
    (document.getElementById('TempPartialRate').getAttribute("Amount") != '' &&
    document.getElementById('TempPartialRate').getAttribute("Amount") != '0.00') ||
	(document.getElementById('txtClaimPPDRate').getAttribute("Amount") != '' &&
	document.getElementById('txtClaimPPDRate').getAttribute("Amount") != '0.00') ||
	(document.getElementById('MaxDurationPTDRate').getAttribute("Amount") != '' &&
	document.getElementById('MaxDurationPTDRate').getAttribute("Amount") != '0.00') ||
	(document.getElementById('txtClaimSurvivorshipRate').getAttribute("Amount") != '' &&
	document.getElementById('txtClaimSurvivorshipRate').getAttribute("Amount") != '0.00') ||
	(document.getElementById('DependencyRate').getAttribute("Amount") != '' &&
	document.getElementById('DependencyRate').getAttribute("Amount") != '0.00') ||
	(document.getElementById('txtClaimDisfigurementRate').getAttribute("Amount") != '' &&
	document.getElementById('txtClaimDisfigurementRate').getAttribute("Amount") != '0.00') ||
	(document.getElementById('LifePensionRate').getAttribute("Amount") != '' &&
	document.getElementById('LifePensionRate').getAttribute("Amount") != '0.00') ||
	(document.getElementById('txtClaimVocRehabTDRate').getAttribute("Amount") != '' &&
	document.getElementById('txtClaimVocRehabTDRate').getAttribute("Amount") != '0.00'))   //Aman Multi Currency--End
	    return true;
	else
	    return false;
}

function CheckMedicalWCBSave()
{
    if (document.getElementById('Amt1MedWC') != null) {
    //Aman Multi Currency--Start
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt1MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt2MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt3MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt4MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt5MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt6MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt7MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt8MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt9MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt10MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt11MedWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt12MedWC'));

        if ((document.getElementById('Amt1MedWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt1MedWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('Amt2MedWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt2MedWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('Amt3MedWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt3MedWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt4MedWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt4MedWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt5MedWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt5MedWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt6MedWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt6MedWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt7MedWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt7MedWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt8MedWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt8MedWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt9MedWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt9MedWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt10MedWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt10MedWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt11MedWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt11MedWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt12MedWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt12MedWC').getAttribute("Amount") != '0.00'))    //Aman Multi Currency--End
	        return true;
	    else
	        return false;
	}
}

function CheckTempDisWCBSave()
{
    if (document.getElementById('txtAmt1TemWC') != null) {
        //Aman Multi Currency--Start
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt1TemWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt2TemWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt3TemWC'));

        if ((document.getElementById('txtAmt1TemWC').getAttribute("Amount") != '' &&
        document.getElementById('txtAmt1TemWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('txtAmt2TemWC').getAttribute("Amount") != '' &&
        document.getElementById('txtAmt2TemWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('Amt3TemWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt3TemWC').getAttribute("Amount") != '0.00'))  //Aman Multi Currency--End
	        return true;
	    else
	        return false;
    }
}

function CheckPermDisWCBSave()
{
    if (document.getElementById('txtAmt1PerWC') != null) {
        //Aman Multi Currency--Start
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt1PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt2PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt3PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt4PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt5PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt6PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt7PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt8PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('AmtPerTWC9'));
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt10PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt11PerWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('AmtPerWC12'));
        RSW_MultiCurrencyToDecimal(document.getElementById('AmtPerWC13'));
        RSW_MultiCurrencyToDecimal(document.getElementById('AmtPerWC14'));

        if ((document.getElementById('txtAmt1PerWC').getAttribute("Amount") != '' &&
        document.getElementById('txtAmt1PerWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('txtAmt2PerWC').getAttribute("Amount") != '' &&
        document.getElementById('txtAmt2PerWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('txtAmt3PerWC').getAttribute("Amount") != '' &&
        document.getElementById('txtAmt3PerWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('txtAmt4PerWC').getAttribute("Amount") != '' &&
	    document.getElementById('txtAmt4PerWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('txtAmt5PerWC').getAttribute("Amount") != '' &&
	    document.getElementById('txtAmt5PerWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('txtAmt6PerWC').getAttribute("Amount") != '' &&
	    document.getElementById('txtAmt6PerWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt7PerWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt7PerWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt8PerWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt8PerWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('AmtPerTWC9').getAttribute("Amount") != '' &&
	    document.getElementById('AmtPerTWC9').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('txtAmt10PerWC').getAttribute("Amount") != '' &&
	    document.getElementById('txtAmt10PerWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('txtAmt11PerWC').getAttribute("Amount") != '' &&
	    document.getElementById('txtAmt11PerWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('AmtPerWC12').getAttribute("Amount") != '' &&
	    document.getElementById('AmtPerWC12').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('AmtPerWC13').getAttribute("Amount") != '' &&
	    document.getElementById('AmtPerWC13').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('AmtPerWC14').getAttribute("Amount") != '' &&
	    document.getElementById('AmtPerWC14').getAttribute("Amount") != '0.00'))   //Aman Multi Currency--End
	        return true;
	    else
	        return false;
    }
}

function CheckVocRehabWCBSave()
{
    if (document.getElementById('txtAmt1VocWC') != null)
    {
        //Aman Multi Currency --Start
        RSW_MultiCurrencyToDecimal(document.getElementById('txtAmt1VocWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt2VocWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt3VocWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt4VocWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt5VocWC'));

        if ((document.getElementById('txtAmt1VocWC').getAttribute("Amount") != '' &&
        document.getElementById('txtAmt1VocWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('Amt2VocWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt2VocWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('Amt3VocWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt3VocWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt4VocWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt4VocWC').getAttribute("Amount") != '0.00') ||
	    (document.getElementById('Amt5VocWC').getAttribute("Amount") != '' &&
	    document.getElementById('Amt5VocWC').getAttribute("Amount") != '0.00'))  //Aman Multi Currency --End
	        return true;
	    else
	        return false;
	}
}

function CheckLegalWCBSave()
{
    if (document.getElementById('Amt1LegWC') != null)
    {
        //Aman Multi Currency --Start
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt1LegWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt2LegWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt3LegWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt4LegWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt5LegWC'));

        if ((document.getElementById('Amt1LegWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt1LegWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('Amt2LegWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt2LegWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('Amt3LegWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt3LegWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('Amt4LegWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt4LegWC').getAttribute("Amount") != '0.00') ||
        (document.getElementById('Amt5LegWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt5LegWC').getAttribute("Amount") != '0.00'))  //Aman Multi Currency --End
	        return true;
	    else
	        return false;
	}
}

function CheckAllExpWCBSave()
{
    if (document.getElementById('Amt1AllWC') != null)
    {
        //Aman Multi Currency --Start
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt1AllWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt2AllWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt3AllWC'));
        RSW_MultiCurrencyToDecimal(document.getElementById('Amt4AllWC'));
         
        if ((document.getElementById('Amt1AllWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt1AllWC').getAttribute("Amount") != '0.00') || 
        (document.getElementById('Amt2AllWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt2AllWC').getAttribute("Amount") != '0.00') || 
        (document.getElementById('Amt3AllWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt3AllWC').getAttribute("Amount") != '0.00') || 
        (document.getElementById('Amt4AllWC').getAttribute("Amount") != '' &&
        document.getElementById('Amt4AllWC').getAttribute("Amount") != '0.00'))   //Aman Multi Currency --End
	        return true;
	    else
	        return false;
	}
}

function CheckReserveApprovalScreen()
{
    //get the current URL 
    var url = self.parent.window.opener.location.toString();
    //get the parameters 
    url.match(/\?(.+)$/); 
    var params = RegExp.$1; 
    
    //split up the query string and store in an associative array 
    var params = params.split("&"); 
    var queryStringList = {};  
    for(var i=0;i<1;i++)
    {
        var tmp = params[i].split(",");
        if (tmp[0].indexOf("ReserveApproval") > 0)
        {
            return true
        }
    }
    return false;
}

function RSWpercentLostFocus(objCtrl) {   
	if (!objCtrl.getAttribute('readonly'))
    {
  //  debugger;
	    var dbl=new String(objCtrl.value);
	    dbl=dbl.replace(/[,\$]/g ,"");
	    
	    if(dbl.length==0)
	    {
	        objCtrl.value="";
		    return false;
	    }
	    
	    if(isNaN(parseFloat(dbl)))
	    {
		    objCtrl.value="";
		    return false;
		}
		
		//for Est. Collection 			
	    if(dbl.indexOf('(') >= 0)
	        dbl=dbl.replace(/[\(\)]/g ,"");
	     
		if(dbl.indexOf('-') >= 0)
		    dbl=dbl.replace(/[\-]/g ,"");
		
		dbl=parseFloat(dbl);
    	
	    objCtrl.value=dbl.toFixed(2);
	    
	    if(objCtrl.value > 100)
            objCtrl.value = "0.00%";
        else if (objCtrl.id == "Amt2PercentApportionPerWC")
        {
            if (objCtrl.value < 70)
                objCtrl.value = "0.00%";
            else
                objCtrl.value = objCtrl.value  + "%"
        }
        else if (objCtrl.value < 0)
            objCtrl.value = "0.00%";
        else
            objCtrl.value = objCtrl.value  + "%"
            
	    AutoCalApport(objCtrl);
	    return true;
	}
	else
	    return false;
}

//Safeway: Aman: For the percent textboxes PPD on lost focus Event --Start
function RSWPPDpercentLostFocus(objCtrl) {  
    if (!objCtrl.getAttribute('readonly')) {
        //  debugger;
        var dbl = new String(objCtrl.value);
        dbl = dbl.replace(/[,]/g, "");

        if (dbl.length == 0) {
            objCtrl.value = "";
            return false;
        }

        if (isNaN(parseFloat(dbl))) {
            objCtrl.value = "";
            return false;
        }
        
        //for Est. Collection 			
        if (dbl.indexOf('(') >= 0)
            dbl = dbl.replace(/[\(\)]/g, "");

        if (dbl.indexOf('-') >= 0)
            dbl = dbl.replace(/[\-]/g, "");

        dbl = parseFloat(dbl);
               
        objCtrl.value = dbl.toFixed(2);

        if (objCtrl.value > 100)
            objCtrl.value = "0.00%";
        else if (objCtrl.id == "Amt2PercentApportionPerWC") {
            if (objCtrl.value < 70)
                objCtrl.value = "0.00%";
            else
                objCtrl.value = objCtrl.value + "%"
        }
        else if (objCtrl.value < 0)
            objCtrl.value = "0.00%";
        else
            objCtrl.value = objCtrl.value + "%"

        AutoCalApport(objCtrl);
        return true;
    }
    else
        return false;
}
//Safeway: Aman: For the percent textboxes on lost focus --End

function AutoCalApport(objctrl)
{    
    var ival = objctrl.value;

    if (objctrl.id == "Amt1PercentPerWCPerGP")
    {        
        //document.getElementById('Amt1PercentAfterApportionPerWC').innerText = ConvertDblToPer((ConvertStrToDbl(ival)/100)*(1-(ConvertStrToDbl(document.getElementById('Amt1ApportionPerWC').value)/100)));
        //        document.getElementById('hdn_Amt1PercentAfterApportionPerWC').innerText = ConvertDblToPer((ConvertStrToDbl(ival)/100)*(1-(ConvertStrToDbl(document.getElementById('Amt1ApportionPerWC').value)/100)));
        document.getElementById('Amt1PercentAfterApportionPerWC').value = ConvertDblToPer((parseFloat(ival) / 100) * (1 - (ConvertStrToDbl(document.getElementById('Amt1ApportionPerWC').value) / 100)));
        document.getElementById('hdn_Amt1PercentAfterApportionPerWC').value = ConvertDblToPer((parseFloat(ival) / 100) * (1 - (ConvertStrToDbl(document.getElementById('Amt1ApportionPerWC').value) / 100)));
    }
    if (objctrl.id == "Amt1ApportionPerWC") 
    {      
//      document.getElementById('Amt1PercentAfterApportionPerWC').innerText = ConvertDblToPer((ConvertStrToDbl(document.getElementById('Amt1PercentPerWCPerGP').value)/100)*(1-(ConvertStrToDbl(ival)/100))*100);
        //       document.getElementById('hdn_Amt1PercentAfterApportionPerWC').innerText = ConvertDblToPer((ConvertStrToDbl(ival)/100)*(1-(ConvertStrToDbl(document.getElementById('Amt1ApportionPerWC').value)/100)));
        document.getElementById('Amt1PercentAfterApportionPerWC').value = ConvertDblToPer((ConvertStrToDbl(document.getElementById('Amt1PercentPerWCPerGP').value) / 100) * (1 - (parseFloat(ival) / 100)) * 100);
        document.getElementById('hdn_Amt1PercentAfterApportionPerWC').value = ConvertDblToPer((parseFloat(ival) / 100) * (1 - (ConvertStrToDbl(document.getElementById('Amt1ApportionPerWC').value) / 100)));
    }
    //Nothing for Amt2PercentApportionPerWC

    if (objctrl.id == "AmtPercent9PerWCPerGP") {
        //document.getElementById('AmtPerTWC9').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('AmtWeeks9PerWC').value) * ConvertStrToDbl(document.getElementById('txtAmtRate9PerWC').value)) / 100));
        document.getElementById('AmtPerTWC9').value = Math.round((parseFloat(ival) * Number(document.getElementById('AmtWeeks9PerWC').value) * Number(document.getElementById('txtAmtRate9PerWC').getAttribute("Amount"))) / 100);
        RSW_MultiCurrencyOnBlur(document.getElementById('AmtPerTWC9'));
    }

    if (objctrl.id == "AmtPercent10PerWCPerGP") {
        //   document.getElementById('txtAmt10PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('AmtWeeks10PerWC').value) * ConvertStrToDbl(document.getElementById('AmtRate10PerWC').value)) / 100));
        document.getElementById('txtAmt10PerWC').value = Math.round((parseFloat(ival) * Number(document.getElementById('AmtWeeks10PerWC').value) * Number(document.getElementById('AmtRate10PerWC').getAttribute("Amount"))) / 100);
        RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt10PerWC'));
    }

    if (objctrl.id == "AmtPercent11PerWCPerGP") {
        //   document.getElementById('txtAmt11PerWC').innerText = ConvertDblToCurr(Math.round((ConvertStrToDbl(ival) * ConvertStrToDbl(document.getElementById('AmtWeeks11PerWC').value) * ConvertStrToDbl(document.getElementById('AmtRate11PerWC').value)) / 100));
        document.getElementById('txtAmt11PerWC').value = Math.round((parseFloat(ival) * Number(document.getElementById('AmtWeeks11PerWC').value) * Number(document.getElementById('AmtRate11PerWC').getAttribute("Amount"))) / 100);
        RSW_MultiCurrencyOnBlur(document.getElementById('txtAmt11PerWC'));
    }
}
//Aman Multicurrency --Start
function RSW_MultiCurrencyOnBlur(objCtrl) {
    functionname = objCtrl.getAttributeNode("onblur").value;
    functionname = functionname.split('(')[0];
    window[functionname](objCtrl);
}

function RSW_MultiCurrencyToDecimal(objCtl) {
    var functionname = objCtl.getAttributeNode("onfocus").value;
    //remove 'format_' word
    functionname = functionname.replace("Format_", "");
    functionname = functionname.split('(')[0];
    window[functionname](objCtl);
}
function RSW_HiddentoMultiCurrencyType(mcControl, objCtl) {
    var functionname = mcControl.getAttributeNode("onblur").value;
    //remove 'format_' word
    functionname = functionname.replace("Format_", "");
    functionname = functionname.split('(')[0];
    objCtl.value = window[functionname](objCtl);
}
//Aman Multicurrency --End