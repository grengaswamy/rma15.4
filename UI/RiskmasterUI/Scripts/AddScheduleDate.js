﻿function AddSchedule() 
{

    if (document.forms[0].Date.value == "" )
     {
       
        alert('Please Enter Schedule Date ');
        document.forms[0].Date.focus();
        return false;
    }

         var sDescription=document.forms[0].Description.value;
         var sText = new String(sDescription);
         sText = sText.replace(/(^\s+)(\s+$)/, "");
        
           if(sText.length ==0)
            {
                alert('Please Enter Description');
                document.forms[0].Description.focus();
                return false;
            }
      


    window.opener.document.forms[0].hdnNewScheduleDate.value = document.forms[0].Date.value;
    window.opener.document.forms[0].hdnNewScheduleDescrip.value = document.forms[0].Description.value;

    window.opener.document.forms[0].hdnAction.value = "Add";
    window.opener.document.forms[0].hidden_DataChanged.value = "true";
    window.opener.document.forms[0].hdnCriteria.value = "Y";
    //window.opener.document.forms[0].submit();
}

function RefereshParentPage()
 {
    window.opener.document.forms[0].submit();
    self.close();
}

function MaxLength() 
{
        var sDescription=document.forms[0].Description.value;
        var sText = new String(sDescription);
         sText = sText.replace(/(^\s+)(\s+$)/, "");
       
         if (sText.length > 100 )
          {
              alert("Description can not be greater then 100 Characters.");
              return false;
         }

}