﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.ServiceHelpers;
//mbahl3 using Riskmaster.UI.XMLImport;
using System.IO;
using System.ServiceModel;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using System.Text;
using System.Xml;
using Riskmaster.Models;




namespace Riskmaster.BusinessHelpers
{
    public class XMLImportBusinessHelper
    {

        public bool SaveXML(XMLInput objXML,out XMLOutPut oOutPut)//logFileContent Added for MITS 29711 by bsharma33
        {
            XMLImportServiceHelper objModel = null;
            bool returnValue = false;
            try
            {
                objModel = new XMLImportServiceHelper();
                objXML.Token = AppHelper.GetSessionId();



                returnValue = objModel.SaveXML(objXML, out oOutPut);//logFileContent Added for MITS 29711 by bsharma33

            }
            catch (Exception ee)
            {
                throw ee;
            }

            return returnValue;
        }

        public XMLTemplate GetSuppTemplate(XMLTemplate objXML)
        {
            XMLImportServiceHelper objModel = null;
            XMLTemplate returnDoc = null;
            try
            {
                objModel = new XMLImportServiceHelper();
                objXML.Token = AppHelper.GetSessionId();

                returnDoc = new XMLTemplate();

                returnDoc = objModel.GetSuppTemplate(objXML);

            }
            catch (Exception ee)
            {
                throw ee;
            }

            return returnDoc;
        }

    }
}