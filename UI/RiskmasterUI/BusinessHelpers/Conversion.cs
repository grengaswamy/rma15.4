using System;
using System.Text.RegularExpressions;
using Riskmaster.Common.Extensions;

// <summary> Riskmaster.Common is a namespace containing class libraries 
// of functionality common to many Riskmaster development tasks.
//   All such commonly reusable utility Riskmaster code should reside here.
// </summary>
namespace Riskmaster.Common
{
	
     //<summary>
     //Conversion is a static class library of commonly used conversion functionality within Riskmaster .
     //</summary>
    public partial class Conversion
    {

		

        //BSB 11.16.2007 Perf ~ 4.5% of total execution cycles were being spent 
        // reconstructing this silly regex expression object repeatedly.
        private static Regex m_IsNumericRegEx = new Regex("[^0-9\\-]");


        /// <summary>
        /// Determines whether or not a specified string value
        /// matches a Numeric RegEx expression
        /// </summary>
        /// <param name="s"></param>
        /// <returns>boolean indicating whether or not the string is completely numeric/contains numbers</returns>
        public static bool IsNumeric(string s)
        {
            if (string.IsNullOrEmpty(s))
                return false;

            //Contains non-numeric characters?
            return !(m_IsNumericRegEx.Matches(s).Count > 0);
        }

        /// <summary>
        /// Determines whether or not a specified object
        /// matches a Numeric RegEx expression
        /// </summary>
        /// <param name="o"></param>
        /// <returns>boolean indicating whether or not the object is completely numeric/contains numbers</returns>
        public static bool IsNumeric(object o) 
        { 
            return IsNumeric(o.ToString()); 
        }


		
		
		
        /// <summary>
        /// Create money groups for the given scale.
        /// </summary>
        /// <param name="p_iIntegerPart">Integer Part</param>
        /// <param name="p_sScale">Scale Value, Possible values : "Billion" , "Million" , "Thousand" and "" </param>
        /// <param name="p_sValue">Reference string in which group is need to add</param>
        /// <param name="p_arrUnits">Units Array</param>
        /// <param name="p_arrTens">Tens Array</param>
        private static void CreateMoneyGroup( int p_iIntegerPart , string p_sScale , ref string p_sValue , string[] p_arrUnits , string[] p_arrTens )
        {
			
                if( p_sValue != "" )
                    p_sValue += " " ;

                if( p_iIntegerPart >= 100 )
                {
                    p_sValue += p_arrUnits[p_iIntegerPart/100] + " Hundred ";
                    p_iIntegerPart %= 100 ;
                }

                if( p_iIntegerPart >= 20 )
                {
                    p_sValue += p_arrTens[(p_iIntegerPart-20)/10] ;
                    if( (p_iIntegerPart %= 10 ) != 0 )
                        p_sValue += "-" + p_arrUnits[p_iIntegerPart] + " ";
                    else
                        p_sValue += " " ;
                }
                else
                {
                    if( p_iIntegerPart != 0 )
                    {
                        p_sValue += p_arrUnits[p_iIntegerPart] + " " ;
                    }
                }

                p_sValue += p_sScale ;
			
        }

		

        #region Rounds Double values
        /// <summary>
        /// Performs the equivalent translation of the Math.Round function
        /// </summary>
        /// <param name="dAmt"></param>
        /// <param name="iDec"></param>
        /// <returns>doulbe indicating the rounded value</returns>
        public static double Round(Double dAmt, int iDec)
        {

            //     ----------------------------------------------------------------------------
            //'Function : dRound
            //'
            //' Author    : Divya
            //' Purpose   : Round func tion in  rounds differently for Odd and Even values.
            //'             For instance round(1.5) rounds it to 2 but round(2.5) rounds it to 2
            //'             This function is created because we want 2.5 to be rounded to 3 but not 2.
            //'             It implements the same logic as 'dround' function in RMWorld
            //'---------------------------------------------------------------------------------------
            int i = 0, iAfterDecCnt = 0, y = 0, iBeforeDecCnt = 0, d = 0, q = 0, lLen = 0, l = 0;
            bool bDecimalFound = false, bEqualsNine = false, bExpFound = false;
            string strAmt = "";


            //Find the number of digits before and after decimal point

            try
            {
                lLen = dAmt.ToString().Length;
                strAmt = dAmt.ToString();



                for (i = 0; i < lLen; i++)
                {
                    string sVal = strAmt.Substring(i, 1);
                    if (IsNumeric(sVal) == true && bDecimalFound == true && bExpFound == false)
                        iAfterDecCnt = iAfterDecCnt + 1;
                    else
                    {
                        if (((IsNumeric(sVal)) == true) && (bDecimalFound == false))
                            iBeforeDecCnt = iBeforeDecCnt + 1;
                        else
                            if (IsNumeric(sVal) == false)
                            {
                                if (sVal == ".")
                                    bDecimalFound = true;
                                else
                                    if (sVal == "E")
                                        bExpFound = true;

                            }
                    }

                }


                if (bExpFound == true)
                {
                    y = iAfterDecCnt;
                    iAfterDecCnt = iAfterDecCnt + Convert.ToInt32(strAmt.Substring(0, 2));
                }


                if (iAfterDecCnt > iDec)
                    if (iAfterDecCnt == 2 && iDec == 0)
                    {
                        if (Convert.ToInt32(strAmt.Substring(strAmt.Length - 2, 2)) > 49)
                        {
                            dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 1 - iAfterDecCnt));

                            if (dAmt >= 0)
                                dAmt = dAmt + 1;
                            else
                                if (dAmt < 0)
                                    dAmt = dAmt - 1;
                        }

                        else
                        {
                            if ((dAmt < 1) && (dAmt > 0))
                                dAmt = 0;
                            else
                                dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 1 - iAfterDecCnt));

                        }
                    }
                    else
                    {
                        if (bExpFound == false)
                        {
                            d = Convert.ToInt32(strAmt.Substring(strAmt.Length - 1, 1));
                            if (d > 4)
                            {
                                if (iAfterDecCnt > 1)
                                {
                                    q = Convert.ToInt32(strAmt.Substring(lLen - 2, 1));
                                    if (q == 9)
                                    {
                                        bEqualsNine = true;
                                        while (bEqualsNine == true)
                                        {
                                            l = l + 1;
                                            if (IsNumeric(strAmt.Substring(lLen - 2 - l, 1)) == true)
                                            {
                                                q = Convert.ToInt32(strAmt.Substring(lLen - 2 - l, 1));
                                                if (q != 9)
                                                    bEqualsNine = false;
                                            }
                                            else
                                            {
                                                dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - l - 1)) + 1;
                                                return dAmt;
                                            }

                                        }

                                        dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 2 - l) + Convert.ToString(q + 1));
                                        dAmt = Round(dAmt, iDec);
                                    }
                                    else
                                    {
                                        dAmt = Convert.ToDouble((strAmt.Substring(0, lLen - 2)) + Convert.ToString((Convert.ToDouble(strAmt.Substring(lLen - 2, 1)) + 1)));
                                        dAmt = Round(dAmt, iDec);
                                    }
                                }
                                else
                                {
                                    dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 1));
                                    if (dAmt >= 0)
                                        dAmt = dAmt + 1;
                                    else
                                    {
                                        if (dAmt < 0)
                                            dAmt = dAmt - 1;
                                    }
                                }
                            }
                            else
                            {
                                if (iAfterDecCnt == 1 && dAmt < 1 && dAmt > 0)

                                    dAmt = 0;
                                else
                                {
                                    dAmt = Convert.ToDouble(strAmt.Substring(0, lLen - 1));
                                    dAmt = Round(dAmt, iDec);
                                }
                            }
                        }
                        else
                        {
                            d = Convert.ToInt32(strAmt.Substring(y + iBeforeDecCnt, 1));
                            if (d > 4)
                            {
                                if (dAmt > 0)
                                    dAmt = dAmt + (10 - (Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1)) / (10 ^ iAfterDecCnt)));
                                else
                                    dAmt = dAmt - (10 - (Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1)) / (10 ^ iAfterDecCnt)));

                                dAmt = Round(dAmt, iDec);
                            }
                            else
                            {
                                if (dAmt > 0)
                                    dAmt = dAmt - ((Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1))) / (10 ^ iAfterDecCnt));
                                else
                                    dAmt = dAmt + ((Convert.ToDouble(strAmt.Substring(y + iBeforeDecCnt, 1)) / (10 ^ iAfterDecCnt)));

                                dAmt = Round(dAmt, iDec);
                            }
                        }
                    }

                return dAmt;
            }

            catch
            {
                return dAmt;
            }
        }
        #endregion


        #region Method to replace Speecial Character with their corresponding words and vice versa
        /// <summary>
        /// Replace Special Character into the their corresponding words
        /// </summary>
        /// <param name="strReplace"></param>
        /// <returns>replaced string with thier corresponding word representation</returns>
        public static string ReplaceSpecialCharacterToWords(string strReplace)
        {
            string strRepalceString = strReplace.Replace(" ", "SBLANK").Replace("/", "SBSLASH")
                    .Replace("~", "STILED").Replace("!", "SXCLAIMATION").Replace("@", "SATADD")
                    .Replace("#", "SHASH").Replace("$", "SDOLLAR").Replace("%", "SPERCENT").Replace("^", "SCAP")
                    .Replace("&", "SAMPERSAND").Replace("*", "SSTAR").Replace("(", "SUBRACE").Replace(")", "SLBRACE")
                    .Replace("-", "SDASH").Replace("\"", "SFSLASH").Replace("?", "SQUES").Replace(".", "SDOTS");
            return strRepalceString;
        }

        /// <summary>
        /// Replace Special Character words into the Character itself
        /// </summary>
        /// <param name="strReplace"></param>
        /// <returns>replaced string with special character</returns>
        public static string ReplaceSpecialWordsToChar(string strReplace)
        {
            string strRepalceString = strReplace.Replace("SBLANK", " ").Replace("SBSLASH", "/")
                    .Replace("STILED", "~").Replace("SXCLAIMATION", "!").Replace("SATADD", "@")
                    .Replace("SHASH", "#").Replace("SDOLLAR", "$").Replace("SPERCENT", "%").Replace("SCAP", "^")
                    .Replace("SAMPERSAND", "&").Replace("SSTAR", "*").Replace("SUBRACE", "(").Replace("SLBRACE", ")")
                    .Replace("SDASH", "-").Replace("SFSLASH", "\"").Replace("SQUES", "?").Replace("SDOTS", ".");
            return strRepalceString;
        }
        #endregion

    }//class

    public partial class Conversion
    {
        #region Conversion Functions
        /// <summary>
        /// Return passed string value as Date in yyyymmdd format
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetDate(string s)
        {
            DateTime dttm = DateTime.MinValue;
            bool boolErr = false;

            if (string.IsNullOrEmpty(s))
                return string.Empty;
            try
            {
                //Get a Date if this is a valid representation for the current culture.
                dttm = System.DateTime.Parse(s);
            }
            catch (FormatException)
            {
                boolErr = true;
            }

            if (boolErr) // Try is passed date in yyyymmdd format already ?
                if (s.Length == 8)
                    try { dttm = ToDate(s); }
                    catch { }

            if (dttm != DateTime.MinValue)
                return dttm.ToString("yyyyMMdd");

            //All attempts at conversion failed return blank.
            return "";

        }

        /// <summary>
        /// Converts a string back to a DateTime representation
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetDateTime(string s)
        {
            DateTime dttm = DateTime.MinValue;
            bool boolErr = false;
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            //Get a DateTime if this is a valid representation for the current culture.
            try { dttm = System.DateTime.Parse(s); }
            catch (FormatException) { boolErr = true; }

            if (boolErr) // Try is passed date in yyyymmdd format already ?
                if (s.Length == 8 || s.Length == 14)
                    try { dttm = ToDate(s); }
                    catch { }


            if (dttm != DateTime.MinValue)
                return dttm.ToString("yyyyMMddHHmmss");

            //All attempts at conversion failed return blank.
            return "";
        }
        /// <summary>
        /// Takes a Riskmaster Database formatted date string "YYYYMMDD" and returns a strongly typed DateTime value.</summary>
        /// <param name="s">The "YYYYMMDD" string to convert into a DateTime value.</param>
        /// <returns>String s as a DateTime or if s is invalid returns DateTime.MinValue.</returns>
        public static DateTime ToDate(string s)
        {
            if (s == "" || s == null)
                return DateTime.MinValue;
            //added by Amitosh for mits 23186 (06/03/2011)
            
            if (s.Length == 10)
            {
                //Deb MITS 27666
                //return Convert.ToDateTime(s);
                return new DateTime(int.Parse(s.Substring(6, 4)), int.Parse(s.Substring(0, 2)), int.Parse(s.Substring(3, 2)));
                //Deb MITS 27666
            }
            //end  Amitosh
            if (s.Length == 8)
                return new DateTime(int.Parse(s.Substring(0, 4)), int.Parse(s.Substring(4, 2)), int.Parse(s.Substring(6, 2)));
            if (s.Length == 14)
                return new DateTime(int.Parse(s.Substring(0, 4)), int.Parse(s.Substring(4, 2)), int.Parse(s.Substring(6, 2)), int.Parse(s.Substring(8, 2)), int.Parse(s.Substring(10, 2)), int.Parse(s.Substring(12, 2)));
            return DateTime.MinValue;
        }




        /// <summary>
        ///Takes a pure numeric string 1234567 or 1234567890 or 1234567890999...
        /// </summary>
        /// <param name="s"></param>
        /// <returns>Returns a formatted phone number 123-4567 or (123) 456-7890 or (123) 456-7890 Ext:999</returns>
        public static string ToPhoneNumber(string s)
        {
            //Nothing here to work with.
            if (s == "" || s == null)
                return "";

            //Too Short
            if (s.Length < 7)
                return s;

            //Already contains formatting characters.
            Regex reg = new Regex("[^1-9]");
            if (reg.Matches(s).Count > 0)
                return s;

            //pankaj 4/23/07, Issue 109- mcic. Take care of large phone numbers that exceed the int size
            long val = Int64.Parse(s);
            long val1 = 0;
            string s1 = "";

            if (val == 0) //Non-Numeric string return empty.
                return "";

            if (s.Length == 7)
                return val.ToString("(   ) 000-0000");

            if (s.Length == 10)
                return val.ToString("(000) 000-0000");

            s1 = s.Substring(1, 10);
            val1 = Int64.Parse(s1);
            s = s.Substring(11);

            return val1.ToString("(000) 000-0000 Ext:") + s;
        }

        /// <summary>
        /// Gets a string representation of a TimeSpan
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetTime(string s)
        {
            DateTime dttm = DateTime.MinValue;
            bool boolErr = false;

            if (s == null)
                return "";

            //Get a DateTime if this is a valid representation for the current culture.
            try { dttm = System.DateTime.Parse(s); }
            catch (FormatException) { boolErr = true; }

            if (boolErr) // Try is passed date in yyyymmdd format already ?
                if (s.Length == 14)
                {
                    try { dttm = ToDate(s); }
                    catch { }
                }
                else if (s.Length == 6)
                {
                    //MITS 7778 ..Raman Bhatia ..Following code is not assigning the time portion correctly
                    /*
                    dttm =  DateTime.Today;  //set to the date of this instance, with the time part set to 00:00:00.
                    dttm.AddHours(int.Parse(s.Substring(0,2)));
                    dttm.AddMinutes(int.Parse(s.Substring(2,2)));
                    dttm.AddSeconds(int.Parse(s.Substring(4,2)));
                    */
                    dttm = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, int.Parse(s.Substring(0, 2)), int.Parse(s.Substring(2, 2)), int.Parse(s.Substring(4, 2)));
                }

            if (dttm != DateTime.MinValue)
                return dttm.ToString("HHmmss");

            //All attempts at conversion failed return blank.
            return "";
        }

        /// <summary>
        /// Returns time in HHMM00 and takes time in HH:MM
        /// </summary>
        /// <param name="s">Format HH:MM</param>
        /// <returns>HHMM00</returns>
        public static string GetTimeHHMM00(string s)
        {
            string sTemp = "000000";

            if (s == null || s == "")
                return sTemp;

            try
            {
                if (s.Length == 5)
                {
                    sTemp = s.Substring(0, 2);
                    sTemp += s.Substring(3, 2);
                    sTemp += "00";
                }
            }
            catch
            {
                return sTemp;
            }

            return sTemp;
        }

	   

        /// <summary>		
        /// This method will convert a string to a long value 		
        /// </summary>		
        /// <param name="p_sValue">The string value that will be converted to a long value</param>												
        /// <returns>A long value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static long ConvertStrToLong(string p_sValue)
        {
            long lRetVal = 0;
            try
            {

                if ((p_sValue == null) || (p_sValue == ""))
                {
                    return lRetVal;
                }
                else
                {
                    lRetVal = Convert.ToInt64(p_sValue);
                }
            }
            catch { return 0; }
            return lRetVal;
        }

        /// <summary>		
        /// This method will convert the passed object to a string value
        /// </summary>		
        /// <param name="p_obj">The object that will be converted to a string value</param>												
        /// <returns>A string value</returns>
        public static string ConvertObjToStr(object p_obj)
        {
            return p_obj.ConvertToString();
        }
        /// <summary>		
        /// This method will convert a string to an integer value 		
        /// </summary>		
        /// <param name="p_sValue">The string value that will be converted to an integer value</param>												
        /// <returns>An integer value</returns>
        public static int ConvertStrToInteger(string p_sValue)
        {
            return p_sValue.ConvertToInt32();
        }

        /// <summary>		
        /// This method will convert a valid date string in the specified date format		
        /// </summary>		
        /// <param name="p_sDataValue">Valid date string that will be formatted</param>	
        /// <param name="p_sFormat">The format in which the date string will be converted</param>	
        /// <returns>It will return date string in the specified format</returns>
        public static string GetDBDateFormat(string p_sDataValue, string p_sFormat)
        {
            string sReturnString = string.Empty;
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if (string.IsNullOrEmpty(p_sDataValue))
                    return string.Empty;
                else if (!string.IsNullOrEmpty(p_sDataValue.Trim()))
                {

                    datDataValue = new DateTime(int.Parse(p_sDataValue.Substring(0, 4)), int.Parse(p_sDataValue.Substring(4, 2)), int.Parse(p_sDataValue.Substring(6, 2)));
                    sReturnString = datDataValue.ToString(p_sFormat);
                }
            }
            catch { return string.Empty; }
            return sReturnString;
        }
        /// <summary>		
        /// This method will return a valid date and time in the specified format		
        /// </summary>		
        /// <param name="p_sDataValue">A valid date time string that will be formatted</param>	
        /// <param name="p_sDateFormat">The format in which the date part will be formatted</param>	
        /// <param name="p_sTimeFormat">The format in which the time part will be formatted</param>	
        /// <returns>It returns date time string the specified format</returns>
        public static string GetDBDTTMFormat(string p_sDataValue, string p_sDateFormat, string p_sTimeFormat)
        {
            string sReturnString = string.Empty, sDatePart = string.Empty, sTimePart = string.Empty;
            string sTempDataValue = string.Empty;
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if (string.IsNullOrEmpty(p_sDataValue))
                    return string.Empty;
                else if (!string.IsNullOrEmpty(p_sDataValue.Trim()))
                {
                    sTempDataValue = p_sDataValue;
                    sTempDataValue = sTempDataValue.Substring(4, 2) + "-" + sTempDataValue.Substring(6, 2) + "-" + sTempDataValue.Substring(0, 4);
                    datDataValue = System.DateTime.Parse(sTempDataValue);
                    sDatePart = datDataValue.ToString(p_sDateFormat);

                    sTempDataValue = p_sDataValue;
                    sTempDataValue = sTempDataValue.Substring(8, 2) + ":" + sTempDataValue.Substring(10, 2) + ":" + sTempDataValue.Substring(12, 2);
                    datDataValue = System.DateTime.Parse(sTempDataValue);
                    sTimePart = datDataValue.ToString(p_sTimeFormat);
                    sReturnString = sDatePart + " " + sTimePart;
                }
            }
            catch { return string.Empty; }
            return sReturnString;
        }
        /// <summary>		
        /// This method will return a valid time in the specified format		
        /// </summary>		
        /// <param name="p_sDataValue">A valid date time string that will be formatted</param>			
        /// <param name="p_sFormat">The format in which the time part will be converted</param>	
        /// <returns>It will return time in the specified format</returns>
        public static string GetDBTimeFormat(string p_sDataValue, string p_sFormat)
        {
            string sReturnString = "";
            DateTime datDataValue = DateTime.MinValue;
            try
            {
                if ((p_sDataValue == null) || (p_sDataValue.Trim() == ""))
                    return "";
                else if (p_sDataValue.Trim() != "")
                {
                    p_sDataValue = p_sDataValue.Substring(0, 2) + ":" + p_sDataValue.Substring(2, 2) + ":" + p_sDataValue.Substring(4, 2);
                    datDataValue = System.DateTime.Parse(p_sDataValue);
                    sReturnString = datDataValue.ToString(p_sFormat);
                }
            }
            catch { return ""; }
            return sReturnString;
        }
        /// <summary>		
        /// This method returns a value in the percent format		
        /// </summary>		
        /// <param name="p_dblValue">Value that has to be converted in percent format</param>					
        /// <returns>It returns value in the percent format</returns>
        public static string GetPercentFormat(double p_dblValue)
        {
            string sReturnValue = "";
            try
            {
                sReturnValue = string.Format("{0:P}", p_dblValue);
            }
            catch { return ""; }
            return sReturnValue;
        }

        /// <summary>
        /// Convert string into double
        /// </summary>
        /// <param name="p_sValue">Input string value</param>
        /// <returns>Double value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static double ConvertStrToDouble(string p_sValue)
        {
            double dblRetVal = 0;
            string str = p_sValue;
            try
            {

                if ((p_sValue == null) || (p_sValue == ""))
                {
                    return dblRetVal;
                }
                else
                {
                    //pmahli MITS 9327 6/15/2007 "()" replaced by "-"
                    str = p_sValue.Replace(",", "").Replace("$", "").Replace("(", "-").Replace(")", "");
                    dblRetVal = Convert.ToDouble(str);
                }
            }
            catch
            {
                return (0);
            }
            return (dblRetVal);
        }

        #region Converts bool to int
        /// <summary>
        /// Converts boolean value to integer
        /// </summary>
        /// <param name="p_bVal">boolean value to convert to integer</param>
        /// <returns>Converted integer value</returns>
        [Obsolete("Please use the Generic CastToType method instead.")]
        public static int ConvertBoolToInt(bool p_bVal)
        {
            int iRetVal = 0;
			
                if (!string.IsNullOrEmpty(p_bVal.ToString()))
                {
                    iRetVal = Convert.ToInt32(p_bVal);
                }
			
            return iRetVal;
        }

		

        #endregion

        #region Convert long to bool
        /// <summary>
        /// Converts long value To boolean
        /// </summary>
        /// <param name="p_lValue">long value to convert</param>
        /// <returns>Converted boolean value</returns>
        public static bool ConvertLongToBool(long p_lValue)
        {
            return p_lValue.ConvertToBoolean();
        }
        #endregion

        #region Convert object to Integer
        /// <summary>
        /// Convert passed object to integer value
        /// </summary>
        /// <param name="p_objValue">object to convert</param>
        /// <returns>Converted integer value</returns>
        public static Int32 ConvertObjToInt(object p_objValue)
        {

            return p_objValue.ConvertToInt32();
        }
        #endregion
        /// <summary>
        /// Converts Null or DBNull object to Int64
        /// </summary>
        /// <param name="p_ObjValue">object to convert</param>
        /// <returns>Converted Int64 value</returns>
        public static Int64 ConvertObjToInt64(object p_ObjValue)
        {

            return p_ObjValue.ConvertToInt64();
        }

		#region Converts Object to bool
		/// <summary>
		/// Converts Object value to bool
		/// </summary>
		/// <param name="p_objVal">Object to convert to bool</param>
		/// <returns>Converted bool value</returns>
		public static bool ConvertObjToBool(object p_objVal)
		{
            return p_objVal.ConvertToBoolean();
		}
		#endregion
        #region Converts Object to Double
        /// <summary>
        /// Converts Object value to Double
        /// </summary>
        /// <param name="p_objVal">Object to convert to Double</param>
        /// <returns>Converted Double value</returns>
        public static Double ConvertObjToDouble(object p_objVal)
        {
            return p_objVal.ConvertToDouble();
        }
        #endregion

        /// <summary>
        /// Uses Generic types to perform casting to a specified 
        /// output type using the TryParse method as an alternative to 
        /// throwing exceptions since throwing exceptions is a very
        /// expensive operation
        /// </summary>
        /// <typeparam name="T">Generic type parameter indicating the
        /// required output type</typeparam>
        /// <param name="strUncastValue">string containing the value to be strongly typed</param>
        /// <param name="blnSuccess">boolean indicating whether or not the data type
        /// cast succeeded or failed</param>
        /// <returns>Generic type which has been cast to its appropriate data type</returns>
        /// <example>
        /// <code>
        /// int intReturnValue = 0;
        /// int intParamValue = 20;
        /// bool blnSuccess = false;
        /// intReturnValue = CastToType&lt;int&gt;(intParamValue.ToString(), out blnSuccess);
        /// </code>
        /// </example>
        /// <remarks>This conversion method does not yet support DateTime and TimeSpan conversions</remarks>
        public static T CastToType<T>(string strUncastValue, out bool blnSuccess) where T : IConvertible
        {
            //Get the type of the Generic type
            Type typToConvert = typeof(T);
            blnSuccess = false;

            //Get the Generic Type name to determine which cast operation to perform
            switch (typToConvert.Name)
            {
                case "Int32":
                    int intReturnValue = 0;
                    blnSuccess = Int32.TryParse(strUncastValue, out intReturnValue);
                    return (T)Convert.ChangeType(intReturnValue, typeof(T));
                    //break;
                case "Int64":
                    Int64 int64ReturnValue = 0;
                    blnSuccess = Int64.TryParse(strUncastValue, out int64ReturnValue);
                    return (T)Convert.ChangeType(int64ReturnValue, typeof(T));
                    //break;
                case "long": //64-bit Integer
                    long lngReturnValue = 0;
                    blnSuccess = long.TryParse(strUncastValue, out lngReturnValue);
                    return (T)Convert.ChangeType(lngReturnValue, typeof(T));
                    //break;
                case "Double":
                    double dblReturnValue = 0.0;
                    blnSuccess = Double.TryParse(strUncastValue, out dblReturnValue);
                    return (T)Convert.ChangeType(dblReturnValue, typeof(T));
                    //break;
                case "Single":
                    Single sngReturnValue = 0.0F;
                    blnSuccess = Single.TryParse(strUncastValue, out sngReturnValue);
                    return (T)Convert.ChangeType(sngReturnValue, typeof(T));
                    //break;
                case "float":
                    float flReturnValue = 0.0F;
                    blnSuccess = float.TryParse(strUncastValue, out flReturnValue);
                    return (T)Convert.ChangeType(flReturnValue, typeof(T));
                    //break;
                case "Boolean":
                    bool blnReturnValue = false;
                    blnSuccess = Boolean.TryParse(strUncastValue, out blnReturnValue);
                    return (T)Convert.ChangeType(blnReturnValue, typeof(T));
                    //break;
                case "Decimal": //decimal for money/financial conversions
                    decimal decReturnValue = 0.0M;
                    blnSuccess = Decimal.TryParse(strUncastValue, out decReturnValue);
                    return (T)Convert.ChangeType(decReturnValue, typeof(T));
                default:
                    return (T)Convert.ChangeType(string.Empty, typeof(T));
            }//switch
        }//method: CastToType<T>()

		
        /// <summary>
        /// This method will take in the time string (in the format HH24MMSS).
        /// It will return the time in AM/PM corresponding to that time string.
        /// </summary>
        /// <param name="p_sTimeString">Time string (in HH24MMSS format)</param>
        /// <example>
        ///		Returns 12:00 AM for 120000
        ///		Returns 12:00 AM for 000000
        ///		Returns 5:00  PM  for 170000
        /// </example>
        /// <returns>Time string (in AM/PM format)</returns>
        public static string GetTimeAMPM(string p_sTimeString)
        {
            int iTime = 0;
            string sReturnTimeString = "";
            try
            {
                if ((p_sTimeString == null) || (p_sTimeString.Trim() == ""))
                    return "";

                iTime = ConvertStrToInteger(p_sTimeString.Substring(0, 2));
                if (iTime >= 12)
                {
                    iTime = iTime - 12;
                    if (iTime == 0)
                        sReturnTimeString = "00" + ":" + p_sTimeString.Substring(2, 2) + " PM";
                    else
                        sReturnTimeString = iTime.ToString() + ":" + p_sTimeString.Substring(2, 2) + " PM";
                }
                else
                {
                    if (p_sTimeString.Substring(0, 2) == "00")
                        p_sTimeString = "12" + p_sTimeString.Substring(2, 2);

                    sReturnTimeString = p_sTimeString.Substring(0, 2) + ":"
                        + p_sTimeString.Substring(p_sTimeString.Length - 4, 2) + " AM";
                }
            }
            catch
            {
                return "";
            }
            return sReturnTimeString;
        }

        /// <summary>
        /// Convert string value to boolean value.
        /// </summary>
        /// <param name="p_sValue">Input string to convert to boolean value</param>
        /// <returns>Converted boolean value </returns>
        public static bool ConvertStrToBool(string p_sValue)
        {
            return p_sValue.ConvertToBoolean();
        }

        /// <summary>
        /// Returns English-like sentence string equivalent To the input integer and decimal part of the money.
        /// Example : 
        /// 1234.89 will be given input by assigning 1234 as Integer part, and 89 as decimal part.
        /// The return string will be "One Thousand Two Hundred Thirty Four and 89/100 Dollars".
        /// </summary>
        /// <param name="p_iIntgerPart">Integer Part</param>
        /// <param name="p_iDecimalPart">Decimal Part</param>
        /// <returns>Returns the English-like sentence string.</returns>
        public static string ConvertToMoneyString(int p_iIntgerPart, int p_iDecimalPart)
        {
            string[] arrUnits = {
                                    "Zero", "One", "Two", "Three", "Four",
                                    "Five", "Six", "Seven", "Eight", "Nine",
                                    "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
                                    "Fifteen", "Sixteen", "Seventeen", "Eighteen",
                                    "Nineteen"
                                };

            string[] arrTens = {
                                   "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
                                   "Seventy", "Eighty", "Ninety"
                               };

            bool bNegative = false;
            int iTemp = 0;
            string sReturnValue = "";

			
                if (p_iIntgerPart < 0)
                {
                    p_iIntgerPart *= -1;
                    p_iDecimalPart *= -1;
                    bNegative = true;
                    sReturnValue = "(";
                }

                iTemp = p_iIntgerPart / 1000000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Billion", ref sReturnValue, arrUnits, arrTens);
                    p_iIntgerPart = p_iIntgerPart % 1000000000;
                }

                iTemp = p_iIntgerPart / 1000000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Million", ref sReturnValue, arrUnits, arrTens);
                    p_iIntgerPart = p_iIntgerPart % 1000000;
                }

                iTemp = p_iIntgerPart / 1000;
                if (iTemp != 0)
                {
                    CreateMoneyGroup(iTemp, "Thousand", ref sReturnValue, arrUnits, arrTens);
                    p_iIntgerPart = p_iIntgerPart % 1000;
                }

                CreateMoneyGroup(p_iIntgerPart, "", ref sReturnValue, arrUnits, arrTens);

                if (sReturnValue == "")
                    sReturnValue = arrUnits[0] + " ";

                sReturnValue += "and " + string.Format("{0:00}", p_iDecimalPart) + "/100";
                sReturnValue += " Dollars";

                if (bNegative)
                    sReturnValue += " )";
			
            return (sReturnValue);
        }
        #endregion
    }//class

	/// <summary>
	/// Account Status for login
	/// </summary>
	public enum ACCOUNT_STATUS
	{
		Default = 0,
		Locked = 1,
		UserPasswordExpired = 2,
		ForcedPasswordExpired = 3,
		PasswordExpiringSoon = 4
	}
}//namespace