﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Riskmaster.ServiceHelpers;
//using Riskmaster.UI.MDAService; //mbahl3 jira [RMACLOUD-159]
using System.IO;
using System.ServiceModel;
using System.Configuration;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;


namespace Riskmaster.BusinessHelpers
{
    public class MDABusinessHelper
    {
        public string GetMDATopics(string medicalcode, string jobclass) 
        {

            MDAServiceHelper objModel = null;

            string retval = "";
            try
            {
                objModel = new MDAServiceHelper();

                retval = objModel.GetMDATopics(medicalcode, jobclass); 
            }
            finally
            {
                objModel = null;
            }
            return retval;
        }
    }
}
