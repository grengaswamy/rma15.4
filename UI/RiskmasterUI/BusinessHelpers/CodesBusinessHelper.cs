﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 03/14/2014 | 35039  | pgupta93   | Changes req for search code description on lookup
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Riskmaster.ServiceHelpers;
//using Riskmaster.UI.CodesService;
using System.IO;
using System.ServiceModel;
using System.Configuration;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Models;
namespace Riskmaster.BusinessHelpers
{
    public class CodesBusinessHelper
    {
        public void Index()
        {
            // Add action logic here
        }
        //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
		//sJurisdiction added by swati
        public CodeListType GetCode(string deptEid, string sTableName, string sLOB, string sJurisdiction,
                                    string sTypeLimits, string sFormName, string sTriggerDate, string sEventDate,
                                    string sSessionDsnId, string sSessionLOB, string sSessionClaimId,string sTitle, string pagenumber, string sInsuredEid,
                                    string sEventId, string sParentCodeid,string sFilter,string sShowCheckBox,string sPolicyId,string sCovTypeCodeId,string sTransId,
                                    string sClaimantEid, string IsFirstFinal, string PolUnitRowId, string sRcRowId, string sPolicyLOB, string sCovgSeqNum,
                                    string sRsvStatusParent,
                                    string sTransSeqNum, string sSortColumn, string sSortOrder, string sField, string sCoverageKey, string sLossTypeCodeId, string sPayCol) //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17) //Added by Tushar for Code Filter //Mona:Adding codefilter like showing codes whith parent "Open":R6
        //rsushilaggar Handled  paging for checkbox functionality  MITS 21600
        //rupal: added sISFPmt to handle first & final payments for getting coverage type

        {
            CodesServiceHelper objModel = null;
            CodeTypeRequest objCode = null;
            CodeListType objList = null;
            //List<CodeType> result =null;
            CodeListType result = null;
            List<CodeType> codeBlank = null;
            CodeType ct = new CodeType(); 
            //Function to test if session exists/passed
           

            try
            {
                objModel = new CodesServiceHelper();
                objCode = new CodeTypeRequest();
                objCode.TableName = sTableName;
                ct.Desc = "";
                ct.Id = 0;
                ct.ParentCode = "";
                ct.parentDesc="";
                ct.parentText="";
                ct.ShortCode = "";
                objCode.LOB = sLOB;
                objCode.Jurisdiction = sJurisdiction;   //added by swati for AIC Gap 7 MITS # 36929
                objCode.Token = AppHelper.GetSessionId();
                objCode.FormName = sFormName;
                objCode.TriggerDate = sTriggerDate;
                objCode.TypeLimits = sTypeLimits;
                objCode.DeptEId = deptEid;
                objCode.SessionLOB = sSessionLOB;
                objCode.EventDate = sEventDate;
                objCode.SessionClaimId = sSessionClaimId;
                objCode.RecordCount = "";
                objCode.PageNumber = pagenumber;
                objCode.Title = sTitle;
                objCode.InsuredEid = sInsuredEid; //PJS MITS 15220 
                objCode.EventId = sEventId; //MITS 17449 Umesh
  				objCode.ParentCodeID = sParentCodeid;//added a code to retrieve parent code id--stara 5/18/09
                objCode.Filter = sFilter; //Added By Tushar for Code Filter //Mona:Adding codefilter like showing codes whith parent "Open":R6
                objCode.ShowCheckBox = sShowCheckBox; //rsushilaggar Handled  paging for checkbox functionality  MITS 21600    
                objCode.PolicyId = sPolicyId;
                objCode.CovTypeCodeId = sCovTypeCodeId;
                objCode.LossTypeCodeId = sLossTypeCodeId;       //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                objCode.TransId = sTransId;
                objCode.ClaimantEid = sClaimantEid;
                //rupal:start for First & Final Payment
                objCode.IsFFPmt = IsFirstFinal;
                objCode.sPayCol = sPayCol;//AA
                //rupal:end for First & Final Payment
                objCode.PolUnitRowId = PolUnitRowId;
                objCode.CovgSeqNum = sCovgSeqNum;//rupal:policy system interface
                objCode.RcRowId = sRcRowId;
                objCode.PolicyLOB = sPolicyLOB;
                objCode.RsvStatusParent = sRsvStatusParent;
                objCode.TransSeqNum = sTransSeqNum;
                objCode.CoverageKey = sCoverageKey;     //Ankit Start : Worked on MITS - 34297
                objCode.PolicyLOB = sPolicyLOB;
                objCode.SortColumn = sSortColumn;//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                objCode.SortOrder = sSortOrder;//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17
                objCode.FieldName = sField; //igupta3: Ensuring field is supp or not

                objList = objModel.GetCodes(objCode);

                if (objList != null)
                {
                    List<CodeType> temp=objList.Codes.ToList();
                    temp.Add(ct);    // blank Code passed so that grid shows up when no code is there in table
                    result = objList;
                    objList.Codes = temp.ToList<CodeType>();//todo Deb
                    //List<CodeType> codes = objList.Codes.ToList();
                    //if (codes.Count > 0)
                    //{
                    //    result = codes;
                    //}
                }
             }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
               // this.TempData["Error"] = ee.Detail.Errors;
                return result; 
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                //this.TempData["Error"] = ErrorHelper.formatUIErrorXML(err);
                return result;
            }
            finally
            {
                objModel = null;
            }
            return result;

        }
        //public List<CodeType> QuickLookup(string index, string lookupstring, string codetype, string lob, string typeLimits,
        //string descSearch, string filter, string sessionlob,
        //string deptEid, string formname, string triggerDate, string eventdate)
        //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
        //jurisdiction added by swati MITS # 36929
        //public CodeListType QuickLookup(string codetype, string lookupstring, string descSearch, string filter, string lob, string sessionlob,
        public CodeListType QuickLookup(string codetype, string lookupstring, string descSearch, string filter, string lob, string jurisdiction, string sessionlob,
                                        string deptEid, string formname, string triggerDate, string eventdate, string title, string sessionClaimId, string orgLevel, 
                                        string sInsuredEid,string sEventId, string sParentCodeid, Int32 iCurrentPageNumber, string sPolicyId, string sCovTypeCodeId,
                                        string sTransId, string sClaimantEid, string IsFirstFinal, string PolUnitRowId,string sPolicyLOB,string sCovgSeqNum,
                                        string sRsvStatusParent, //rupal:added IsFirstFinal for r8 first & final pmt enh, 4 july 2011
                                        string sTransSeqNum, string sFieldName, string sCoverageKey, string sLossTypeCodeId,string sPayCol) //igupta3 added fieldname for supp identification
        {
            CodesServiceHelper objModel = null;
            QuickLookupRequest objrequest = null;
            CodeListType objList = null;
            CodeListType result = null;
            //Function to test if session exists/passed
           
            try
            {
                objModel = new CodesServiceHelper();
                objrequest = new QuickLookupRequest();
                objrequest.CurrentPageNumber = iCurrentPageNumber;
                objrequest.Token = AppHelper.GetSessionId();
                objrequest.DeptEId = deptEid;
                //if (index == null)
                //    index = "";
                objrequest.Index = "";
                objrequest.LookupString = lookupstring;
                objrequest.LookupType = codetype;      
                objrequest.LOB = lob;
                //added by swati MITS # 36929
                objrequest.Jurisdiction = jurisdiction;
                //end by swati
                //if (typeLimits == null)
                //    typeLimits = "";
                objrequest.TypeLimits = "";
                objrequest.DescriptionSearch = descSearch;
                //if (filter == null)
                //    filter = "";
                objrequest.Filter = filter;
                //if (sessionlob == null)
                //    sessionlob = "";
                objrequest.SessionLOB =sessionlob;
                //if (formname == null)
                //    formname = "";
                objrequest.FormName = formname;
                //if (triggerDate == null)
                //    triggerDate = "";
                objrequest.TriggerDate = triggerDate;
                //if (eventdate == null)
                //    eventdate = "";
                objrequest.EventDate = eventdate;
                objrequest.Title = title;
                objrequest.SessionClaimId = sessionClaimId;
                objrequest.orgLevel = orgLevel;                
                objrequest.ParentCodeID = sParentCodeid;//added a code to retrieve parent code id--stara 5/18/09
                objrequest.InsuredEid = sInsuredEid; //PJS MITS 15220   
                objrequest.EventId = sEventId; //MITS 17449 :Umesh
                objrequest.PolicyId = sPolicyId;
                objrequest.CovTypeCodeId = sCovTypeCodeId;
                objrequest.LossTypeCodeId = sLossTypeCodeId;        //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                objrequest.ClaimantEid = sClaimantEid;
                objrequest.TransId = sTransId;
                //rupal:start for First & Final Payment
                objrequest.IsFFPmt = IsFirstFinal;
				objrequest.PolUnitRowId = PolUnitRowId;
                //rupal:end for First & Final Payment
                objrequest.sPayCol = sPayCol;//AA
                objrequest.CovgSeqNum = sCovgSeqNum;
                objrequest.PolicyLOB = sPolicyLOB;
                objrequest.RsvStatusParent = sRsvStatusParent;
                objrequest.TransSeqNum = sTransSeqNum;
                objrequest.CoverageKey = sCoverageKey;      //Ankit Start : Worked on MITS - 34297
				objrequest.FieldName = sFieldName; //igupta3 field added
                objList = objModel.GetQuickLookUp(objrequest);
                objList.TableName = codetype;
                objList.SearchText = lookupstring;
               
                if (objList!= null)
                {
                    if (codetype == "code.orgh")
                    {
                        int iCount = objList.Codes.Count();
                        for (int i = 0; i < iCount; i++)
                        {
                            if ((orgLevel.ToLower().IndexOf("department") != -1) || (orgLevel == "undefined"))
                            for (int j = 0; j < 7; j++)
                            {
                                objList.Codes[i].Desc = objList.Codes[i].Desc.Remove(0, objList.Codes[i].Desc.IndexOf(" ") + 1);
                            }
                            //mkaran2 : RMA-4053 -MITS 37139 ; reverted back code for mits 35039
                            //if (objList.Codes[i].Desc.Split('-').Length - 1 >= 2)
                            //{
                            //    objList.Codes[i].Desc = objList.Codes[i].Desc.Remove(0, objList.Codes[i].Desc.IndexOf("-") + 1);
                            //}
                        }
                        //MITS:35039 START
                        var temp = (from a in objList.Codes
                                    where (a.Desc.ToLower().StartsWith(lookupstring.ToLower()) || a.Desc.Substring((a.Desc.IndexOf("-") + 1), a.Desc.Length - (a.Desc.IndexOf("-") + 1)).ToLower().StartsWith(lookupstring.ToLower()))
                                    select a).Cast<CodeType>().ToArray();
                        objList.Codes = temp.ToList<CodeType>();
                        //MITS:35039 END
                    }
                    //zmohammad MITs 35151 : For description based searches
                    if (objrequest.DescriptionSearch != null && objrequest.DescriptionSearch == "true")
                    {
                        var temp = (from a in objList.Codes
                                    where (a.Desc.ToLower().Contains(lookupstring.ToLower()))
                                    select a).Cast<CodeType>().ToArray();
                        objList.Codes = temp.ToList<CodeType>();
                    }
                    //MITS:35039 START
                    //else
                    //{
                    //    var temp = (from a in objList.Codes
                    //                where (a.Desc.ToLower().StartsWith(lookupstring.ToLower()) || a.Desc.Substring((a.Desc.IndexOf("-") + 1), a.Desc.Length - (a.Desc.IndexOf("-") + 1)).ToLower().StartsWith(lookupstring.ToLower()))
                    //                select a).Cast<CodeType>().ToArray();
                    //    objList.Codes = temp;
                    //}
                    //MITS:35039 END
                  
                    
                    result = objList;
                }
            }
            catch (FaultException<RMException> ee)
            {
                ErrorHelper.logErrors(ee);
                //this.TempData["Error"] = ee.Detail.Errors;
                //return View("QuickLookup", objList);
                return result;

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
               // this.TempData["Error"] = ErrorHelper.formatUIErrorXML(err);
                //return View("QuickLookup", objList);
                return result;

            }
            finally
            {
                objModel = null;
            }
            return result;
        }




    }
}
