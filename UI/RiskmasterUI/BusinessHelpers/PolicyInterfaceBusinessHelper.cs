﻿using System;
using Riskmaster.ServiceHelpers;
using Riskmaster.UI.PolicyInterfaceService;


namespace Riskmaster.BusinessHelpers
{
    public class PolicyInterfaceBusinessHelper
    {
        public PolicySystemList GetPolicySystemList()
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicySystemList oResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                PolicySystemList oRequest = new PolicySystemList();
                oRequest.Token = AppHelper.GetSessionId();

                oResponse = objModel.GetPolicySystemList(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oResponse;
        }

		//abhal3 MITS 36046 start
        public string ExportMapping(ExportLossCodeMapping oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            string sFileContent = string.Empty;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                sFileContent = objModel.ExportMapping(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return sFileContent;
        }
        public LossCodeMappingOutput ImportMappingFromFile(LossCodeMappingFileContent oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            LossCodeMappingOutput oResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                oResponse = objModel.ImportMappingFromFile(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return oResponse;
        }
        public LossCodeMappingOutput ReplicateMapping(ReplicateLossCodeMapping oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            LossCodeMappingOutput oResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                oResponse = objModel.ReplicateMapping(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return oResponse;
        }
		
		//abhal3 MITS 36046 end
        public PolicySearch GetPolicySearchResults(PolicySearch oSearchRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicySearch oSearchResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oSearchRequest.Token = AppHelper.GetSessionId();

                oSearchResponse = objModel.GetPolicySearchResult(oSearchRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oSearchResponse;
        }
        

        public DisplayFileData GetXml()
        {
            PolicyInterfaceServiceHelper objModel = null;
            DisplayFileData returnDoc = null;
            RMServiceType objRMServiceType = null;
            try
            {
                objRMServiceType = new RMServiceType();
                objModel = new PolicyInterfaceServiceHelper();
                
                objRMServiceType.Token = AppHelper.GetSessionId();

                returnDoc = objModel.GetXml(objRMServiceType);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return returnDoc;
        }
        public DisplayFileData GetUnitXml()
        {
            PolicyInterfaceServiceHelper objModel = null;
            DisplayFileData returnDoc = null;
            RMServiceType objRMServiceType = null;
            try
            {
                objRMServiceType = new RMServiceType();
                objModel = new PolicyInterfaceServiceHelper();

                objRMServiceType.Token = AppHelper.GetSessionId();

                returnDoc = objModel.GetUnitXml(objRMServiceType);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return returnDoc;
        }
        public DisplayFileData GetDuplicateOptions(SaveDownloadOptions oSaveDownloadOptions)
        {
            PolicyInterfaceServiceHelper objModel = null;
            DisplayFileData returnDoc = null;
           
            try
            {
                
                objModel = new PolicyInterfaceServiceHelper();

                oSaveDownloadOptions.Token = AppHelper.GetSessionId();

                returnDoc = objModel.GetDuplicateOptions(oSaveDownloadOptions);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return returnDoc;
        }

        public bool SaveOptions(ref SaveDownloadOptions oSaveDownloadOptions)
        {
            PolicyInterfaceServiceHelper objModel = null;
            bool returnVal = false;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oSaveDownloadOptions.Token = AppHelper.GetSessionId();
                returnVal = objModel.SaveOptions(ref oSaveDownloadOptions);
            }
            catch (Exception ee)
            {
                throw ee;
            }
           
            return returnVal;
        }
        
        

        public void DeleteOldFiles(DisplayMode omode)
        {
            PolicyInterfaceServiceHelper objModel = null;
            bool returnVal = false;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                omode.Token = AppHelper.GetSessionId();
                objModel.DeleteOldFiles(omode);
            }
            catch (Exception ee)
            {
                throw ee;
            }

           
        }

        public PolicyEnquiry GetPolicyEnquiryResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oEnquiryResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oEnquiryRequest.Token = AppHelper.GetSessionId();

                oEnquiryResponse = objModel.GetPolicyEnquiryResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oEnquiryResponse;
        }

        public PolicyEnquiry GetEndorsementData(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oEnquiryResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oEnquiryRequest.Token = AppHelper.GetSessionId();

                oEnquiryResponse = objModel.GetEndorsementData(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oEnquiryResponse;
        }

        public PolicyEnquiry GetUnitDetailResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oEnquiryResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oEnquiryRequest.Token = AppHelper.GetSessionId();

                oEnquiryResponse = objModel.GetUnitDetailResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oEnquiryResponse;
        }

        public PolicyEnquiry GetPropertyUnitDetailResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oEnquiryResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oEnquiryRequest.Token = AppHelper.GetSessionId();

                //oEnquiryResponse = objModel.GetPropertyUnitDetailResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oEnquiryResponse;
        }

        public PolicyEnquiry GetAutoUnitDetailResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oEnquiryResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oEnquiryRequest.Token = AppHelper.GetSessionId();

                //oEnquiryResponse = objModel.GetAutoUnitDetailResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oEnquiryResponse;
        }

        //public PolicyEnquiry GetAutoCoverageDetailResult(PolicyEnquiry oEnquiryRequest)
        //{
        //    PolicyInterfaceServiceHelper objModel = null;
        //    PolicyEnquiry oEnquiryResponse = null;
        //    try
        //    {
        //        objModel = new PolicyInterfaceServiceHelper();
        //        oEnquiryRequest.Token = AppHelper.GetSessionId();

        //        oEnquiryResponse = objModel.GetAutoCoverageDetailResult(oEnquiryRequest);
        //    }
        //    catch (Exception ee)
        //    {
        //        throw ee;
        //    }

        //    return oEnquiryResponse;
        //}
        //public PolicyEnquiry GetPropertyCoverageDetailResult(PolicyEnquiry oEnquiryRequest)
        //{
        //    PolicyInterfaceServiceHelper objModel = null;
        //    PolicyEnquiry oEnquiryResponse = null;
        //    try
        //    {
        //        objModel = new PolicyInterfaceServiceHelper();
        //        oEnquiryRequest.Token = AppHelper.GetSessionId();

        //        oEnquiryResponse = objModel.GetPropertyCoverageDetailResult(oEnquiryRequest);
        //    }
        //    catch (Exception ee)
        //    {
        //        throw ee;
        //    }

        //    return oEnquiryResponse;
        //}

        public PolicyEnquiry GetPolicyInterestDetailResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oEnquiryResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oEnquiryRequest.Token = AppHelper.GetSessionId();

                oEnquiryResponse = objModel.GetPolicyInterestDetailResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oEnquiryResponse;
        }

        public PolicySaveRequest SaveExternalPolicy(PolicySaveRequest oPolicySaveRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicySaveRequest oPolicySaveResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oPolicySaveRequest.Token = AppHelper.GetSessionId();

                oPolicySaveResponse = objModel.SaveExternalPolicy(oPolicySaveRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oPolicySaveResponse;
        }


        public PolicyValidateResponse PolicyValidation(PolicyValidateInput oPolicyValidateInput)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyValidateResponse oPolicyValidateResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oPolicyValidateInput.Token = AppHelper.GetSessionId();

                oPolicyValidateResponse = objModel.PolicyValidation(oPolicyValidateInput);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oPolicyValidateResponse;
        }

        

        public PolicyEnquiry GetPolicyDriverDetails(PolicyEnquiry oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oEnquiryResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();

                oEnquiryResponse = objModel.GetPolicyDriverDetailResult(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oEnquiryResponse;
        }

        public PolicyDownload GetDownLoadXMLData(PolicyDownload oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyDownload oResponse;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                oResponse = objModel.GetDownLoadXMLData(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return oResponse;
        }

        public PolicyEnquiry GetUnitCoverageListResult(PolicyEnquiry oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                oResponse = objModel.GetUnitCoverageListResult(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return oResponse;
        }

        public PolicyEnquiry GetCoverageDetail(PolicyEnquiry oRequest)
        {
            PolicyInterfaceServiceHelper oHelper;
            PolicyEnquiry oResponse;
            try
            {

                oHelper = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                oResponse = oHelper.GetCoverageDetail(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            
            return oResponse;
        }

        public PolicyDownload GetEndorsementDataForTracking(PolicyDownload oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyDownload oResponse;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                oResponse = objModel.GetEndorsementDataForTracking(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return oResponse;
        }

        public PolicyEnquiry GetUnitInterestListResult(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oEnquiryResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oEnquiryRequest.Token = AppHelper.GetSessionId();

                oEnquiryResponse = objModel.GetUnitInterestListResult(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oEnquiryResponse;
        }
         public PolicyEnquiry GetPolicyUnitInterestList(PolicyEnquiry oEnquiryRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            PolicyEnquiry oEnquiryResponse = null;
            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oEnquiryRequest.Token = AppHelper.GetSessionId();

                oEnquiryResponse = objModel.GetPolicyUnitInterestList(oEnquiryRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oEnquiryResponse;
        }
        


        public UnitListing SearchPolicyUnits(UnitSearch oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            UnitListing oResposne;

            try
            {
                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                oResposne = objModel.SearchPolicyUnits(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
           
            return oResposne;
        }
        public FetchPolicyDetails GetPolicyDetails(FetchPolicyDetails oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            FetchPolicyDetails oResposne;

            try
            {

                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                oResposne = objModel.GetPolicyDetails(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return oResposne;
        }
		//RMA-12047
        public CheckDuplicateRes CheckDuplicatePolicy(CheckDuplicateReq oRequest)
        {
            PolicyInterfaceServiceHelper objModel = null;
            CheckDuplicateRes oResponse = null;

            try
            {

                objModel = new PolicyInterfaceServiceHelper();
                oRequest.Token = AppHelper.GetSessionId();
                oResponse = objModel.CheckDuplicatePolicy(oRequest);
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                objModel = null;
            }
            return oResponse;
        }
    //RMA-12047
    }
}
