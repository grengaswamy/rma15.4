﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
//using Riskmaster.UI.RMSSO;//Change by kuladeep for Jira-156
using Riskmaster.Models;

/// <summary>
/// Summary description for ConnectionTypes
/// </summary>
[DataObject(true)]
public class ConnectionTypes
{
	public ConnectionTypes()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// Gets the available types of authentication types for SSO providers
    /// </summary>
    /// <returns>Generic Dictionary collection containing a list of 
    /// available authentication types such as SSL and Basic (No SSL)</returns>
    public static Dictionary<string, string> SelectConnectionTypes()
    {

        //Code change by kuladeep for Jira-156 Start---Make Rest service.

        //SSOServiceClient objSvcClient = new SSOServiceClient();
        //Dictionary<string, string> dictSSLAuthTypes = new Dictionary<string, string>();
        //objSvcClient.Open();
        ////Open the service connection
        //dictSSLAuthTypes = objSvcClient.GetSSLAuthTypes();
        //objSvcClient.Close();
        //return dictSSLAuthTypes;

        RMServiceType oRMServiceType = null;
        Dictionary<string, string> dictSSLAuthTypes = new Dictionary<string, string>();
        oRMServiceType = new RMServiceType();
        oRMServiceType.Token = AppHelper.GetSessionId();
        oRMServiceType.ClientId = AppHelper.ClientId;
        dictSSLAuthTypes = AppHelper.GetResponse<Dictionary<string, string>>("RMService/SSO/types", AppHelper.HttpVerb.POST, "application/json", oRMServiceType);
        return dictSSLAuthTypes;
        //Code change by kuladeep for Jira-156 End

    }//method: SelectConnectionTypes()
}
