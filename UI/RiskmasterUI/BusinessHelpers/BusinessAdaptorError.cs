﻿using System;
using System.Collections;
using Riskmaster.Common;


namespace Riskmaster.BusinessAdaptor.Common
{
    /// <summary>
    /// BusinessAdaptorErrorType represents the types of errors that can be sent.
    /// </summary>
    public enum BusinessAdaptorErrorType : byte
    {
        SystemError = 1,
        Error = 2,
        Warning = 3,
        Message = 4,
        PopupMessage = 5
    }

    /// <summary>
    /// BusinessAdaptorError represents an error in the business adaptor.
    /// </summary>
    public class BusinessAdaptorError
    {
        public string ErrorCode = string.Empty, ErrorDescription = string.Empty;
        public BusinessAdaptorErrorType ErrorType;
        public Exception oException = null;

        /// <summary>
        /// Default class constructor
        /// </summary>
        public BusinessAdaptorError()
        {
        }

        /// <summary>
        /// OVERLOADED: Class constructor
        /// </summary>
        /// <param name="ErrCode"></param>
        /// <param name="ErrDescription"></param>
        /// <param name="ErrType"></param>
        public BusinessAdaptorError(string ErrCode, string ErrDescription,
            BusinessAdaptorErrorType ErrType)
        {
            ErrorCode = ErrCode;
            ErrorDescription = ErrDescription;
            ErrorType = ErrType;
            oException = null;
        }

        /// <summary>
        /// OVERLOADED: Class constructor
        /// </summary>
        /// <param name="e"></param>
        /// <param name="ErrType"></param>
        public BusinessAdaptorError(Exception e, BusinessAdaptorErrorType ErrType)
        {
            ErrorCode = "";         // Exception based messages always use assembly + . + exception type for error code
            ErrorDescription = "";  // "" means use message in exception.
            ErrorType = ErrType;
            oException = e;
        }

        /// <summary>
        /// OVERLOADED: Class constructor
        /// </summary>
        /// <param name="e"></param>
        /// <param name="AltErrDescription"></param>
        /// <param name="ErrType"></param>
        public BusinessAdaptorError(Exception e, string AltErrDescription,
            BusinessAdaptorErrorType ErrType)
        {
            ErrorCode = "";  // Exception based messages always use assembly + . + exception type for error code
            ErrorDescription = AltErrDescription;
            ErrorType = ErrType;
            oException = e;
        }

    }

    /// <summary>
    /// BusinessAdaptorErrors is a container class for adding errors that have occurred in
    /// the Business Adaptor functions. These are extracted by the common web service and
    /// formatted into the response XML message from the web service.
    /// </summary>
    public class BusinessAdaptorErrors : IEnumerable
    {
        protected ArrayList m_Errors = null;
        protected string m_LoginName = "unknown";
        protected string m_TargetDSN = "unknown";

        public BusinessAdaptorErrors()
        {
            m_Errors = new System.Collections.ArrayList();
        }

        

        /// <summary>
        /// Count returns the number of errors in the collection.
        /// </summary>

        public int Count
        {
            get
            {
                return m_Errors.Count;
            }
        }

        /// <summary>
        /// Default indexer allows get and set by index into the list.
        /// </summary>

        public BusinessAdaptorError this[int index]
        {
            get { return (BusinessAdaptorError)m_Errors[index]; }
            set
            {
                m_Errors[index] = value;
            }
        }

        /// <summary>
        /// Allows a complete BusinessAdaptorError object to be added to the list.
        /// </summary>
        public int Add(BusinessAdaptorError value)
        {
            return m_Errors.Add(value);
        }

        /// <summary>
        /// Allows an error to be added by its base information (code, desc., type) rather
        /// than requiring a BusinessAdaptorError object to be created.
        /// </summary>
        public int Add(string ErrorCode, string ErrorDescription,
            BusinessAdaptorErrorType ErrorType)
        {
            return m_Errors.Add(new BusinessAdaptorError(ErrorCode, ErrorDescription,
                                    ErrorType));
        }


        /// <summary>
        /// Adds an exception-based message to the error collection.
        /// </summary>
        public int Add(Exception e, BusinessAdaptorErrorType ErrorType)
        {
            return m_Errors.Add(new BusinessAdaptorError(e, ErrorType));
        }

        /// <summary>
        /// Adds an exception-based message to the error collection. Allows an alternate
        /// message to be specified to use instead of the exception message.
        /// </summary>
        public int Add(Exception e, string AltErrorDescription,
            BusinessAdaptorErrorType ErrorType)
        {
            return m_Errors.Add(new BusinessAdaptorError(e, AltErrorDescription,
                ErrorType));
        }

        /// <summary>
        /// Removes all items from the error list.
        /// </summary>
        public void Clear()
        {
            m_Errors.Clear();
        }

        /// <summary>
        /// Removes error at given index.
        /// </summary>
        public void RemoveAt(int index)
        {
            m_Errors.RemoveAt(index);
        }

        /// <summary>
        /// Returns the enumerator for the error list. Supports for-each iteration.
        /// </summary>
        public IEnumerator GetEnumerator()
        { return m_Errors.GetEnumerator(); }

        /// <summary>
        /// This function checks if the errors collection has only 'WarningScriptErrors'. If all the
        /// errors in the collection are Warning errors for script, it should not be treated as an error
        /// but should execute the complete flow as normal.
        /// </summary>
        /// <returns></returns>
        public bool OnlyWarningErrors()
        {
            foreach (BusinessAdaptorError objError in m_Errors)
                if (objError.ErrorCode != "WarningScriptError")
                    return false;
            return true;
        }

    }
}
