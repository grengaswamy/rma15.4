﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riskmaster.BusinessHelpers
{
    public class GridDataHelper
    {
        public List<IDictionary<String, object>> Data { get; set; }
        public List<ColumnDefHelper> ColumnDef { get; set; }
        public List<ColumnDefHelper> ColumnPosition { get; set; }
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public string SearchCat { get; set; }
        public string admtable { get; set; } //RMA-11641 - msampathkuma
        //RMA-344       achouhan3       Added for Sort Order in case of client side grid Starts
        public string SortField { get; set; }
        public string SortDirection { get; set; }
        //RMA-344       achouhan3       Added for Sort Order in case of client side grid Starts
    }
}