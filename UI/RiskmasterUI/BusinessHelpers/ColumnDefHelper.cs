﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riskmaster.BusinessHelpers
{
    public class ColumnDefHelper
    {
        public string field { get; set; }
        public string displayName { get; set; }
        public bool visible { get; set; }
        public string cellTemplate { get; set; }
        public string width { get; set; }
        public string headerCellTemplate { get; set; }
        public int Position { get; set; }
        public bool Key { get; set; }
    }
}