﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Riskmaster.UI.UI
{
    public partial class AsyncrhonousCallBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //tkatsarski: 11/17/15 Start changes for RMA-17378: User is getting logged out of the application on refreshing the browser
            string sEndUserSessionWhenRMPageIsClosed = System.Configuration.ConfigurationManager.AppSettings["EndUserSessionWhenRMPageIsClosed"];
            bool bEndUserSessionWhenRMPageIsClosed = true;
            if (!string.IsNullOrEmpty(sEndUserSessionWhenRMPageIsClosed))
            {
                bEndUserSessionWhenRMPageIsClosed = Convert.ToBoolean(sEndUserSessionWhenRMPageIsClosed);
            }

            if (bEndUserSessionWhenRMPageIsClosed)
            {
                // akasuhik5 Changed for RMA-12054 Starts
                //LogoutSession();
                AppHelper.LogoutSession();
                AppHelper.RemoveCookie();
                System.Web.Security.FormsAuthentication.SignOut();
                HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                cookie1.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(cookie1);
                // akasuhik5 Changed for RMA-12054 Ends
            }
            //tkatsarski: 11/17/15 End changes for RMA-17378: 
        }
        
        // akasuhik5 Commented for RMA-12054 Starts
        //private void LogoutSession()
        //{
        //    Riskmaster.UI.RMAuthentication.AuthenticationServiceClient authService = new Riskmaster.UI.RMAuthentication.AuthenticationServiceClient();
        //    //Open the call to the Service
        //    authService.Open();
        //    string strSessionID = AppHelper.ReadCookieValue("SessionId");
        //    //Log out the user
        //    authService.LogOutUser(strSessionID);
        //    //Remove the user's cookie
        //    AppHelper.RemoveCookie();
        //   //Remove the Forms authentication ticket
        //  System.Web.Security.FormsAuthentication.SignOut();
        //}
        // akasuhik5 Commented for RMA-12054 Ends
    }

}