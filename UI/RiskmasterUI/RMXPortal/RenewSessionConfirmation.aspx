﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RenewSessionConfirmation.aspx.cs" Inherits="Riskmaster.RMXPortal.RenewSessionConfirmation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Riskmaster</title>
    <link href='/RiskmasterUI/Content/rmnet.css' rel="stylesheet" type="text/css" />
</head>
<body>
    <script type="text/javascript">
        var wndParentForm = window.opener;

        ReFocus(window);

        window.onunload = function () { WindowIsClosing(); }
        window.onblur = function () { OnBlur(); }

        function WindowIsClosing() {
            if (wndParentForm != null) {
                wndParentForm.OnWindowIsClosing();
            }
        }

        function OnBlur() {
            if (wndParentForm != null) {
                setTimeout(function () {
                    if (wndParentForm.ParentHasFocus()) {
                        ReFocus(window);
                    }
                }, 20);
            }
        }

        function ReFocus(wnd) {
            setTimeout(function () {
                wnd.focus()
            }, 20);
        }

        function RenewSession() {
            if (wndParentForm != null) {
                wndParentForm.OnRenewSession();
            }
        }

        function TerminateSession() {
            if (wndParentForm != null) {
                wndParentForm.OnTerminateSession();
            }
        }
    </script>
    <div align="center">
        <form id="form1" runat="server">
            <div align="center" style="font-weight: bold; font-size: 11pt; font-family: Tahoma, Arial, Helvetica, sans-serif; text-align:center; margin-top:20px;">
                <div>
                    <asp:Label ID="lblAlertMessage" runat="server" />
                </div>
                <div style="margin-top:12px;">
                    <input type="button" class="button" style="width:74px;" value="  OK  " onclick="RenewSession();" id="OKbutton" />
                    <input type="button" class="button" style="width:74px;" value="  Cancel  " onclick="TerminateSession();" />
                </div>
            </div>
        </form>
    </div>
</body>
</html>
