﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Riskmaster.AppHelpers;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.UI.RMAuthentication;
using Riskmaster.UI.App_Master;
using System.Collections.Specialized;
using System.Collections;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.RMXPortal
{
    public partial class RenewSessionConfirmation : System.Web.UI.Page
    {
        /// <summary>
        /// Event to handle the Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lblAlertMessage.Text = "Your current Session is about to be over in " + AppHelper.GetQueryStringValue("timeOut") + " minute(s). Do you want to renew?";
            }
        }
    }
}
