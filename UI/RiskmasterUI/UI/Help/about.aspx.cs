﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using AppHelper;

namespace Riskmaster.UI.UI.Help
{
    public partial class about : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblBuildInfo.Text = AppHelper.GetReleaseBuildInfo();
            lblCopyrightEndingYear.Text = DateTime.Now.Year.ToString();
        }
    }
}
