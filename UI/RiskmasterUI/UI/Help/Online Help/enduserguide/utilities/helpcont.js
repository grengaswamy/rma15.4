<!-- Display and hide content -->
		function toggle( targetId ){
  			target = document.all( targetId );
  				if (target.style.display == "none"){
  					target.style.display = "";
  				} else {
  					target.style.display = "none";
  				}
  		}
 	
<!-- Set background image on topic -->
        function resetLogo() {
            document.all.Logo.style.posTop = document.body.scrollTop;
            document.all.Logo.style.posLeft =
               document.body.scrollLeft;
         }
		 
<!-- ******************************************************************************-->
		 
<!-- Java Script to flip static images to dynamic animated *.gif files on mouseover event -->
	function flipImage(url){
    						if (window.event.srcElement.tagName == "IMG" ) {
        					window.event.srcElement.src = url;    }}
							
<!--****************************************************************************-->

<!--****************************************************************************-->
<!--SET LAST PICKS:  initializes the "last picked" variables prior to calling the various toggle functions-->
function set_last_picks(){
	last_btn = null;
	last_select = null;
	last_DIV = null;
	last_inner_DIV = null;
	last_inner2_DIV = null;
	
	last_text = null;
	last_inner_text = null;
	last_inner2_text = null;
	
	is_clicked = null;
	Inner_is_clicked = null;
	Inner2_is_clicked = null;
	
	count = 1;
	last_bwsr = document.all('bwsr_0');
	
//alert("last DIV " + last_DIV);
	//return last_pick;
}
<!--******************************************************************************-->
<!--ACTIVATE LINK****************************************************************-->
function activate_link(text_ID){
	paragraph = text_ID;
	textcolor = paragraph.style.color;
	
	if(textcolor != "red"){
		paragraph.style.color = "blue";
	}
	
}
<!--DEACTIVATE LINK ******************************************************-->

function deactivate_link(text_ID){
	paragraph = text_ID;
	textcolor = paragraph.style.color;
	if(textcolor == "red"){
		paragraph.style.color = "red";
		 
	} else {
		paragraph.style.color = "black";
		paragraph.style.fontStyle = "normal";
		}
}

<!--****************************************************************************-->
<!--RADIO TOGGLE: toggles open and closed the contents of a DIV container with the given target_ID and closes last open container-->
<!-- used to display and hide DIV data on a help topic or coaching card-->

function radio_toggle(targetID){
//get the object ID specified in function call, store in variable
		target = document.all(targetID);
				
//initial selection of first radio button that sets last_pick to non-null
			//DEBUG alert("last pick now seen as set to " + last_pick);
			if (last_DIV == null) {
			target.style.display = "";
			last_DIV = target;
			}
//after the first selection, last_pick is not null, so begin toggle function
		 else {
//check if user clicked same radio button...		
		if (target == last_DIV && target.style.display == "none"){
			target.style.display = "";
			last_DIV = target;
			}
	
//but if they clicked a different button then do this...
			 else {
		        target.style.display = "";
				last_DIV.style.display = "none";
				last_DIV = target;
			}
	}}

<!--*****************************************************************************-->

//<!--SUPER RADIO TOGGLE: toggles open and closed the contents of a DIV container with the given target_ID and closes last open container and changes the style of the text in the toggle to indicate select status.-->

//targetID - the object ID of the DIV container to be toggled
//text_ID  - the object ID of the text to be changed on mouseover and toggle


function super_radio_toggle(targetID, text_ID){
//get the DIV ID and paragraph text ID, store in variables
		target = document.all(targetID);
		targetText = document.all(text_ID);
		
				
//initial selection of first radio button that sets last_pick to non-null
			//DEBUG alert("last pick now seen as set to " + last_pick);
			if (last_DIV == null) {
			target.style.display = "";
			targetText.style.color = "red";
			targetText.style.fontStyle = "italic";
			last_DIV = target;
			last_text = targetText;
			is_clicked = "true";
			}

//after the first selection, last_pick is not null, so begin toggle function

		 else {

//check if user clicked same radio button and its DIV is open...	
	
		if (target == last_DIV && targetText.style.color == "red"){
			target.style.display = "none";
			targetText.style.color = "black";
			targetText.style.fontStyle = "normal";
			is_clicked ="false";
			last_DIV = target;
			last_text = targetText;
			}
	
//but if its DIV is closed then do this...

			 else if(target == last_DIV && is_clicked =="false"){
					
		        	target.style.display = "";
					targetText.style.color = "red";
					targetText.style.fontStyle = "italic";
					is_clicked = "true";
					last_DIV = target;
					last_text = targetText;
			} else {

			last_DIV.style.display = "none";
		    target.style.display = "";
			targetText.style.color = "red";
			targetText.style.fontStyle = "italic";
			last_text.style.color = "black";
			last_text.style.fontStyle = "normal";
			last_DIV = target;
			last_text = targetText;
			is_clicked = "true";
			}
	}}
//***************************************************************************
<!--*HIGHLIGHT and DEHIGHLIGHT-MODULE:toggles image border color on mouseover*********************-->
function highlight_toolbar_modules(btn_ID){
	target = document.all(btn_ID);
	target.style.borderColor = "blue";
}

function de_highlight_toolbar_modules(btn_ID){
	target = document.all(btn_ID);
	target.style.borderColor = "white";
	
}
//********************************************************************************************
<!--************FIT VERTICAL SCROLL BAR TO WINDOW WIDTH AND LENGTH***********************-->
function resizedivision()
{
	if (document.all.scroll_div != null){
        	document.all.scroll_div.style.pixelWidth = document.body.clientWidth;
    		document.all.scroll_div.style.pixelHeight = document.body.clientHeight - document.body.scrollHeight;
	}
}
//****************************************************************************			

//**********************************************************************		
		function window.onbeforeprint()
		   {
		    // Walk through all the elements in the documentwith
		    // CLASS="collapsed" and set it to "expanded" justfor printing.
		   var coll = document.all.tags("DIV");
		   if (coll!=null)
		   {
		      for (i=0; i<coll.length; i++) 
			     if (coll[i].className == "collapsed")
				 {  
		           coll[i].className = "expanded";
				   
		        // After printing, make sure to set CLASS="collapsed" 
		        // only for those that were expanded just for printing.
				   coll[i].bExpandedForPrinting = true;
				 }
				
				 else if (coll[i].className =="expanded")
				    coll[i].bExpandedForPrinting =false;
		   }
		}

		function window.onafterprint()
		{
		   // Walk through all the elements in the doc with CLASS="expanded"
		   // and set it to "collapsed" if expanded just for  // printing.
		   var coll = document.all.tags("DIV");
		   if (coll!=null)
		   {
		      for (i=0; i < coll.length; i++) 
			     if ((coll[i].className == "expanded") &&
				     (coll[i].bExpandedForPrinting))
				 {  
		           coll[i].className = "collapsed";
				   coll[i].bExpandedForPrinting = false;
				 }
		   }
		}

	
//**********************************************************************		

		//--RADIO TOGGLE: toggles open and closed the contents of a DIV container with the given target_ID and closes last open container-->
		//-- used to display and hide DIV data on a help topic or coaching card-->

		function class_radio_toggle(targetID){
		//get the object ID specified in function call, store in variable 
		target = document.all(targetID);
						
		//initial selection of first radio button that sets last_pick to non-null
					
					
					if (last_DIV == null) {
					target.className = "expanded";
					last_DIV = target;
					}
		//after the first selection, last_pick is not null, so begin toggle function
				 else {
		//check if user clicked same radio button...		
				if (target == last_DIV && target.className == "collapsed")
					{
					target.className = "expanded";
					last_DIV = target;
					}
			
		//but if they clicked a different button then do this...
					 else {
				
				        target.className = "expanded";
						last_DIV.className ="collapsed";
						last_DIV = target;
						  }
					  }}
			
			//end function

			// Suppress horizontal scrolling in the body
 
			//	document.body.scrollleft=0;
 