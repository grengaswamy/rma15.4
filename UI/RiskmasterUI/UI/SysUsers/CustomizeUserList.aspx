﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomizeUserList.aspx.cs" Inherits="Riskmaster.UI.SysUsers.CustomizeUserList" %>
<%@ Import Namespace ="System.Xml"%>
<%@ Import Namespace ="System.Xml.Linq"%>
<%@ Import Namespace ="System.Xml.XPath"%>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head runat="server">
    <title>System User(s)</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>    
    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="JavaScript" src="../../Scripts/cul.js" type="text/javascript"></script>
    <script language="JavaScript" src="../../Scripts/CULscrollabletable.js" type="text/javascript"></script>
    <script language="JavaScript" src="../../Scripts/jquery/jquery-1.8.0.js" type="text/javascript"></script><!-- Mits 35697 -->

      <style type="text/css">
  .hiddencol
  {
    display: none;
  }
</style>
  <script language="javascript" type="text/javascript">
  <!--Changed by skhare7 for perf changes : start  -->
      $(document).ready(function () {         
          //hdnUserGroups = 1 => Groups
          //hdnUserGroups = 0 => Users
          //hdnMultiSelect = 1 => Multiple selection
          //hdnMultiSelect = 0 => Single selection
          //sharishkumar Jira 6415 starts
          if ($('#hdnUserGroups') != null && $('#hdnUserGroups').val() == 1) {
          
              $("input[id*='cbUserListGrid']:checkbox").prop('checked', true);
              $("input[id*='cbUserListGrid']:checkbox").prop('disabled', true);        
              //Single Group case
              if ($('#hdnMultiSelect') != null && $('#hdnMultiSelect').val() == 0) {                
                  $('#lstUserGroups').removeAttr('multiple');
              }             
                  
          }
          else {
              //sharishkumar Jira 6415 ends
              $("input[id*='cbUserListGrid']:checkbox").change(function () {
               
                  //sharishkumar Jira 6415 starts
                  if ($('#hdnMultiSelect') != null && $('#hdnMultiSelect').val() === "0") {
                      $('input[id*="cbUserListGrid"]:checkbox').prop('checked', false);
                      $(this).prop('checked', true);
                      document.getElementById('hdnUserData').value = '';
                  }
                  //sharishkumar Jira 6415 ends
                  var Uid = $(this).val();
                  var loginname = $(this).closest('tr').children('td:nth-child(2)').text();
                  var name = $(this).closest('tr').children('td:nth-child(3)').text();
                  var email = $(this).closest('tr').children('td:nth-child(5)').text();
                  if (email == '')
                      var sdata = '/' + Uid + ',' + name + ',' + loginname;
                  else
                      var sdata = '/' + Uid + ',' + name + ',' + loginname + ',' + email;
                  if ($(this).is(':checked')) {
                      document.getElementById('hdnUserData').value = document.getElementById('hdnUserData').value + sdata;
                  }
                  else {
                      document.getElementById('hdnUserData').value = document.getElementById('hdnUserData').value.replace(sdata, '');
                  }
              });
          }
         
          if (document.getElementById('hdnParentform')) {
              if (document.getElementById('hdnParentform').value == 'creatediary' || document.getElementById('hdnParentform').value == 'routediary' || document.getElementById('hdnParentform').value == 'autodiarysetup'  || document.getElementById('hdnParentform').value == 'limittracking') {
                  $('#tdUserGroups').show();
              }
          }

          $('#lstUserGroups').change(function () {
              onLstUserGroupsChange();
          });
      });

      function onLstUserGroupsChange() {
          //group is selected => store in hidden field
          if ($('#rdGroup').prop('checked') === true) {
              $('#hdnLstGroups').val('');
              $("#lstUserGroups > option:selected").each(function (index) {
                  //For ALL & NULL value is 0
                  if ($(this).val() === 0) {
                      $('#lstUserGroups > option[value=0]').prop('selected', false);
                  }
                  else {
                      if (index === 0) {
                          $('#hdnLstGroups').val($(this).val() + '|' + this.text);
                      }
                      else {
                          $('#hdnLstGroups').val($('#hdnLstGroups').val() + '~' + $(this).val() + '|' + this.text);
                      }
                  }
              });
          }
      }   

      //This function is called when group radio is selected
      function HideUsersListBox(radioname) {
          var selectedTemp;
          var selected;
          var sId = "";
          var sUsersTemp = "";
          var radio = document.getElementById(radioname);
          if (radio.checked == true) {
              $("input[id*='cbUserListGrid']:checkbox").prop('checked', true);
              $("input[id*='cbUserListGrid']:checkbox").prop('disabled', true);

              //Disable all and null values
              $('#lstUserGroups > option[value=0]').prop('selected', false);
              $('#lstUserGroups > option[value=0]').prop('disabled', true);

              //Select values in listbox based on hidden field values
              var selectedValues = $('#hdnLstGroups').val().split('~');
              if ($('#hdnLstGroups').val() == '') {
                  $('#lstUserGroups > option').prop('selected', false);
              }
              else {
                  $.each(selectedValues, function (index, value) {
                      var valueToSelect = value.split('|')[0];
                      $('#lstUserGroups > option[value=' + valueToSelect + ']').prop('selected', true);
                  });
              }

              if (document.getElementById('hdnUserData'))
                  var scount = document.getElementById('hdnUserData').value.split('/');
              $('#hdnLstUsers').val('');
              for (var i = 0 ; i < scount.length; i++) {
                  if (scount[i] != '') {
                      selected = scount[i];
                      selectedTemp = new String(selected);
                      sId = selectedTemp.substring(0, selectedTemp.indexOf(','));
                      sUsersTemp = selectedTemp.substring(selectedTemp.indexOf(',') + 1, selectedTemp.length);
                      sUsersTemp = sUsersTemp.split(",");

                      if (document.getElementById('hdnLstUsers')) {
                          if (document.getElementById('hdnLstUsers').value == '') {
                              document.getElementById('hdnLstUsers').value = sId + '|' + sUsersTemp[2];
                          }
                          else {
                              document.getElementById('hdnLstUsers').value = document.getElementById('hdnLstUsers').value + '~' + sId + '|' + sUsersTemp[2];
                          }
                      }
                  }
              }
          }
      }

      //This function is called when "user" radio button is selected
      function ShowUsersListBox(radioname) {
          var radio = document.getElementById(radioname);
          if (radio.checked == true) {

              $("input[id*='cbUserListGrid']:checkbox").prop('checked', false);
              $("input[id*='cbUserListGrid']:checkbox").prop('disabled', false);
              
              $('#lstUserGroups > option[value=0]').prop('disabled', false);
              $('#lstUserGroups > option').prop('selected', false);
              $('#lstUserGroups > option[value=0]').first().prop('selected', true);

              var selectedUserValues = $('#hdnLstUsers').val().split('~');
              $.each(selectedUserValues, function (index, value) {
                  var valueToSelect = value.split('|')[0];
                  $('#gvUserList input[type=checkbox][value=' + valueToSelect + ']').prop('checked', true);
              });
          }
      }
    </script>
     <!--Changed by skhare7 for perf changes : end  -->
</head>
  <body class="rowlight" onresize="ResizeGrid();" onload="eval('javascript:onCULPageLoaded()')">
  <form id="frmData" name="frmData" runat="server">
  <table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
     <td class="ctrlgroup" colspan="4"> <asp:Label ID="lblUserListSearch" runat="server" Text="<%$ Resources:lblUserListSearchResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td width="20%"><font face="arial" size="-3"> <asp:Label ID="lblSearchType" runat="server" Text="<%$ Resources:lblSearchTypeResrc %>"></asp:Label></font></td>
    </tr>
    <tr>
     <td width="3%"><asp:Label ID="lblFirstName" runat="server" Text="<%$ Resources:lblFirstNameResrc %>"></asp:Label></td>
     <td width="3%"><asp:Label ID="lblUserType" runat="server" Text="<%$ Resources:lblUserTypeResrc %>"></asp:Label></td>
        <td width ="5%" id="tdUserGroups" style="display:none">
                                        <asp:RadioButton GroupName="optionReserveusergroup" Text="Users" value="Users" ID="rdUser"
                                            Checked="true" onclick="ShowUsersListBox('rdUser');"                                       
                                            runat="server" />
                                        <asp:RadioButton onclick="return HideUsersListBox('rdGroup');"
                                            GroupName="optionReserveusergroup" Text="Groups" value="Groups" ID="rdGroup"                                            
                                            runat="server" />                        
         </td>
    </tr>
    <tr>
     <td width="8%"><input type="text" name="firstname" value="" size="30" onkeyup="filter2()" id="firstname" cancelledvalue=""></td>
    <td rowspan="3"><select id="lstUserGroups" runat="server" name="lstUserGroups" size="5" appearance="compact" onClick="filter2()" onkeyup="filter2()" style="width=&#34;203px&#34;" multiple="">      
    </select>
    </td>
    </tr>
    <tr>
     <td width="3%"><asp:Label ID="lblLastName" runat="server" Text="<%$ Resources:lblLastNameResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td width="5%"><input type="text" name="lastname" value="" size="30" onkeyup="filter2()" id="lastname" cancelledvalue=""></td>
    </tr>
    <tr>
     <td><br></td>
    </tr>
    <tr>
    <td>     
    <asp:TextBox Style="display: none" runat="server" id="sysuserlist" rmxref="Instance/Document/SystemUserList" rmxtype="hidden"/>      
    <asp:TextBox Style="display: none" runat="server" id="optionflag" rmxref="Instance/Document/SystemUserList/OptionFlag" rmxtype="hidden"/> 
    <asp:TextBox Style="display: none" runat="server" id="parentform" rmxref="Instance/Document/SystemUserList/Parentform" rmxtype="hidden"/> 
    <asp:TextBox Style="display: none" runat="server" id="systemusergroup" rmxref="Instance/Document/SystemUserList/SystemUserGroup" rmxtype="hidden"/>     
    <input type="text" runat="server" style="display:none" id="hdnParentControlname"/> 
    <input type="text" runat="server" style="display:none" id="hdnSelected"/>
    <input type="text" runat="server" style="display:none" id="hdnAction"/>
    <input type="text" runat="server" style="display:none" id="hdnSystemCount"/>
    <input type="text" runat="server" style="display:none" id="hdnParentform"/>
    <input type="text" runat="server" style="display:none" id="hdnUserlistname"/>
    <input type="text" runat="server" style="display:none" id="hdnUseridstr"/>
    <input type="text" runat="server" style="display:none" id="hdnUsernamestr"/>
            <input type="text" runat="server" style="display:none" id="hdnUserData"/>
    <input type="text" runat="server" style="display:none" value="rmsysuser" id="Hidden1"/>  
        <input type="text" runat="server" style="display:none" id="hdnMultiSelect"/><%--sharishkumar Jira 6415--%>
        <input type="text" runat="server" style="display:none" id="hdnUserGroups"/><%--sharishkumar Jira 6415--%>
        <input type="text" runat="server" style="display:none" id="hdnLstGroups"/>
        <input type="text" runat="server" style="display:none" id="hdnLstUsers"/>

        <input type="text" runat="server" style="display:none" id="hdnParentUserStr"/>
        <input type="text" runat="server" style="display:none" id="hdnParentGroupStr"/>
        <input type="text" runat="server" style="display:none" id="hdnParentLstGroups"/>
        <input type="text" runat="server" style="display:none" id="hdnParentLstUsers"/>

        <%--npadhy JIRA 6415, The separator in case of Search and Supplementals are different. if We need to have the Comma as separator, then we will pass true in this field--%>
        <input type="text" runat="server" style="display:none" id="hdnCommaSeparator"/> 
    <!--Changed by Gagan for MITS 19725 : start  -->
    <input type="text" runat="server" style="display:none" id="hdnLookup"/>
    <!--Changed by Gagan for MITS 19725 : end  -->
    <uc1:ErrorControl ID="ErrorControl" runat="server" />  
     </td>
    </tr>
   </table>
   <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblList">
    <tr>
             <td style="text-align:left">
     <input type="button" class="button"  runat="server" value="<%$ Resources:btnListAllResrc %>" id="btnListAllResrc" onclick="ListAll();"/>
     <%if (sOptionType != "0" || sParent == "routediary" || sParent == "autodiarysetup")
     {%>      
        <input type="button" class="button" runat="server" value="<%$ Resources:btnSelectAllResrc %>" id="btnSelectAllResrc" onclick="SelectAll();"/>
        <input type="button" class="button" runat="server" value="<%$ Resources:btnInvertSelectResrc %>" id="btnInvertSelectResrc" onclick="InvertSelection();"/>     
     <%}%>
     </td>
    </tr>
   </table>
   <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblListInfo">
    <tr>
     <tr>
      <td width="20%"><font face="arial" size="-3"><asp:Label ID="lblShowing" runat="server" Text="<%$ Resources:lblShowingResrc %>" ></asp:Label>&nbsp;</font><font face="arial" size="-3"><span id="recOf">0</span></font><font face="arial" size="-3">&nbsp;<asp:Label ID="lblOf" runat="server" Text="<%$ Resources:lblOfResrc %>" ></asp:Label>&nbsp; </font><font face="arial" size="-3"><span id="recTo">0</span></font><font face="arial" size="-3">&nbsp;<asp:Label ID="lblRecords" runat="server" Text="<%$ Resources:lblRecordsResrc %>" ></asp:Label></font></td>
     </tr>
    </tr>
    <tr>
     <td class="ctrlgroup" colspan="4"><asp:Label ID="lblSysUsers" runat="server" Text="<%$ Resources:lblSysUsersResrc %>" ></asp:Label></td>
    </tr>
   </table>
      <!-- Mits 35697 starts  -->
      <div id ="divgrid" style="overflow:scroll; height:150px;">
      <asp:GridView ID="gvUserList" runat="server" AutoGenerateColumns="False"
                                    Font-Names="Tahoma" Font-Size="Smaller" CssClass="singleborder" Width="98%"
                                    CellPadding="4" ForeColor="#333333" HorizontalAlign="Left" 
                                    EnableModelValidation="True"  >
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField>
                                        
                                            <ItemTemplate>
                                                <input id="cbUserListGrid" name="UserListGrid" runat="server" type="checkbox"
                                                  value='<%# DataBinder.Eval(Container, "DataItem.UserId")%>'  />
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="colheader5" Width="10%" />
                                            <ItemStyle Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="LoginName" HeaderText="<%$ Resources:lblLoginNameResrc %>" HeaderStyle-CssClass="colheader5" >
                                            <HeaderStyle CssClass="colheader5" Width="20%"  ></HeaderStyle>
                                        <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Name" HeaderText="<%$ Resources:lblNameResrc %>" HeaderStyle-CssClass="colheader5" >
                                            <HeaderStyle CssClass="colheader5" Width="20%"></HeaderStyle>
                                        <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UserType" HeaderText="<%$ Resources:lblSysUserTypeResrc %>" HeaderStyle-CssClass="colheader5" >
                                            <HeaderStyle CssClass="colheader5" Width="50%"></HeaderStyle>
                                        <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                          <asp:BoundField DataField="Email" HeaderText="Email Address"  ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" >
                                            <HeaderStyle CssClass="hiddencol" Width="50%"></HeaderStyle>
                                        <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:TemplateField Visible ="false">
                                                 <HeaderTemplate>                                  
                                                 </HeaderTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle BackColor="#507CD1" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle CssClass="colheader3" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
          </div>
      <!-- Mits 35697 ends -->
      <!-- Mits 35697 Commented below code  -->
   <%-- <thead>
     <tr>
      <td class="colheader3" width="10%"></td>
      <td class="colheader3" width="15%"><asp:Label ID="lblLoginName" runat="server" Text="<%$ Resources:lblLoginNameResrc %>" ></asp:Label></td>
      <td class="colheader3" width="25%"><asp:Label ID="lblName" runat="server" Text="<%$ Resources:lblNameResrc %>" ></asp:Label></td>
      <td class="colheader3" width="30%"><asp:Label ID="lblSysUserType" runat="server" Text="<%$ Resources:lblSysUserTypeResrc %>" ></asp:Label></td>
     </tr>
    </thead>
    <tbody>

        <%int i = 0; foreach (XElement item in result)
       {
        string rowclass = "";
        if ((i % 2) == 1) rowclass = "rowdark1";
        else rowclass = "rowlight1";
        i++;
       %>
                                
      <tr class="<%=rowclass%>">                          
      <td>
         <%if (sOptionType == "0")
         {%>                                           
           <input type="radio" id="PageId" name="PageId"  title="System User List" value="<%=item.Element("UserID").Value %>,<%=item.Element("Name").Value %>,<%=item.Element("LoginName").Value %>,<%=item.Element("Email").Value %>"/>                             
         <%}%>
         <%else
         {%>
           <input type="checkbox" id="PageId" name="PageId"  title="System User List" value="<%=item.Element("UserID").Value %>,<%=item.Element("Name").Value %>,<%=item.Element("LoginName").Value %>,<%=item.Element("Email").Value %>"/>         
         <%}%>
       </td>                                                                                                 
      <td >
         <%=item.Element("LoginName").Value%>
      </td>
      <td >
         <%=item.Element("Name").Value%>
         </td>                       
         <td>
             <%=item.Element("UserType").Value%>
         </td>                                                          
         </tr>                                                       
     <%}%>
    </tbody>--%>
   <!-- Mits 35697 Comments end  -->
   <table border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td><input type="button" id="btnOkResrc" class="button" runat="server" value="  <%$ Resources:btnOkResrc %>  " onClick="OnOK();"></td>
     <td><input type="button" id="btnCancelResrc" class="button" runat="server" value="  <%$ Resources:btnCancelResrc %>  " onClick="OnCancel();"></td>
     </tr>
   </table>   
   </form>
 </body>
</html>
