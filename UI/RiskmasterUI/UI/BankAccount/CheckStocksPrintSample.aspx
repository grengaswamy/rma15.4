﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckStocksPrintSample.aspx.cs" Inherits="Riskmaster.UI.BankAccount.CheckStocksPrintSample" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>  
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Print Check Stock Sample</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <asp:textbox style="display: none" runat="server" id="StockId" 
                    rmxtype="hidden" />
                    <%--Ashish Ahuja : Pay to Order Config--%>
<asp:textbox style="display: none" runat="server" id="SampleText" 
                    rmxtype="hidden" />
    <table>       
      <tr>
       <td colspan="2">
        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
       </td>
      </tr> 
    </table>
    
    </div>
    </form>
</body>
</html>
