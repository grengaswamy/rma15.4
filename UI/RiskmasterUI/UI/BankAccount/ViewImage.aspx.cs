﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Text;

namespace Riskmaster.UI.BankAccount
{
    public partial class ViewImage : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";

            try
            {                              
                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWS("CheckStockSetupAdaptor.ViewImage", XmlTemplate, out sCWSresponse, true, false);

                    XmlTemplate = XElement.Parse(sCWSresponse.ToString());                   

                    XElement oEle = XmlTemplate.XPathSelectElement("//file");
                    
                    //XElement oEle = XmlTemplate.
                    if (oEle != null)
                    {
                        string sFileContent = oEle.Value;
                        byte[] byteOrg = Convert.FromBase64String(sFileContent);

                        Response.Clear();
                        Response.Charset = "";
                        Response.ContentType = "application/jpeg";
                        Response.AddHeader("Content-Disposition", "inline;filename=ViewImage.emf");
                        Response.BinaryWrite(byteOrg);
                        Response.End();
                    }

                    ErrorControl1.errorDom = sCWSresponse;
                

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }



        /// <summary>
        /// Returns Message Template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");


            sXml = sXml.Append("<CheckStock>");
            sXml = sXml.Append("<StockId>");
            sXml = sXml.Append(AppHelper.GetQueryStringValue("stockid"));
            sXml = sXml.Append("</StockId>");
            sXml = sXml.Append("<ImageId>");
            sXml = sXml.Append(AppHelper.GetQueryStringValue("imageid"));            
            sXml = sXml.Append("</ImageId>");
            sXml = sXml.Append("<TempFileName>");
            sXml = sXml.Append(AppHelper.GetQueryStringValue("tempfilename"));
            //sXml = sXml.Append(AppHelper.GetQueryStringValue("imageid"));            
            sXml = sXml.Append("</TempFileName>");
            sXml = sXml.Append("</CheckStock>");      
            
            sXml = sXml.Append("</Document>");
            sXml = sXml.Append("</Message>");


            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }   
    }
}
