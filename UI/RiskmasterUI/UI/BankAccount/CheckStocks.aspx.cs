﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using System.Text;
using System.Xml;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.BankAccount
{
    public partial class CheckStocks : NonFDMBasePageCWS 
    {

        int m_iNavigation = 1;

        /// <summary>
        /// Page Load Method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {            

            try
            {
                AccountId.Text = AppHelper.GetQueryStringValue("AccountId");
                //Rakhel ML Changes -start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("CheckStocks.aspx"), "CheckStocksValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "CheckStocksValidations", sValidationResources, true);
                //Rakhel ML Changes -end
                if (!IsPostBack)
                {
                    NavigateFirst(null, null);                                                   
                    return;
                }

                if (test.Text == "Last")
                {
                    NavigateLast(null, null);
                    test.Text = "";
                }

                //if (test.Text == "Save")
                //{
                //    test.Text = "";
                //    NavigateSave(null, null);                    
                //}

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }        

        
                      
        /// <summary>
        /// Navigate to First Record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateFirst(object sender, ImageClickEventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";  

            try
            {
                if (test.Text == "Save")
                {
                    NavigateSave(null, null);
                    test.Text = "";
                    //Changed by Saurabh Arora for MITS 18908:Start
                    //return;
                    //Changed by Saurabh Arora for MITS 18908:End

                }

                m_iNavigation = 1;
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWS("CheckStockSetupAdaptor.MoveAndGetStockDetails", XmlTemplate, out sCWSresponse, false, true);


            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }



        /// <summary>
        /// Navigate to Previous Record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigatePrev(object sender, ImageClickEventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";  

            try
            {
                if (test.Text == "Save")
                {
                    NavigateSave(null, null);
                    test.Text = "";
                    
                    //Changed by Saurabh Arora for MITS 18908:Start
                    //return;
                    //Changed by Saurabh Arora for MITS 18908:End
                }
                m_iNavigation = 2;
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWS("CheckStockSetupAdaptor.MoveAndGetStockDetails", XmlTemplate, out sCWSresponse, false, true);


            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }



        /// <summary>
        /// Navigate to Next Record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateNext(object sender, ImageClickEventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";  
            try
            {
                if (test.Text == "Save")
                {
                    NavigateSave(null, null);
                    test.Text = "";
                    //Changed by Saurabh Arora for MITS 18908:Start
                    //return;
                    //Changed by Saurabh Arora for MITS 18908:End
                }

                m_iNavigation = 3;
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWS("CheckStockSetupAdaptor.MoveAndGetStockDetails", XmlTemplate, out sCWSresponse, false, true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        protected void NavigateLast(object sender, ImageClickEventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";  

            try
            {
                if (test.Text == "Save")
                {
                    NavigateSave(null, null);
                    test.Text = "";
                    //Changed by Saurabh Arora for MITS 18908:Start
                    //return;
                    //Changed by Saurabh Arora for MITS 18908:End
                }

                m_iNavigation = 4;
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWS("CheckStockSetupAdaptor.MoveAndGetStockDetails", XmlTemplate, out sCWSresponse, false, true);
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        /// <summary>
        /// Deletes the current record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateDelete(object sender, ImageClickEventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";  

            try
            {             
                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWS("CheckStockSetupAdaptor.DeleteStockDetails", XmlTemplate, out sCWSresponse, false, true);
                ClearControls();
                //deb : MITS 24065
                ClientScriptManager csm = Page.ClientScript;
                //Rakhel ML Changes -start
                //csm.RegisterClientScriptBlock(this.GetType(), "dd", "<Script>alert('The record has been successfully deleted!');</Script>");
                string sMessage = RMXResourceProvider.GetSpecificObject("RecordDeleted", RMXResourceProvider.PageId("CheckStocks.aspx"), ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                string str = String.Format("<script>alert('{0}');</script>", sMessage);
                csm.RegisterClientScriptBlock(this.GetType(), "dd", str);
                //Rakhel ML Changes -end
                NavigateFirst(null, null);
                return;
                //deb 
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        /// <summary>
        /// Navigate to a new record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateNew(object sender, ImageClickEventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            //Changed by Saurabh P Arora for MITS 16780: Start
            string sUnit= string.Empty;
            XmlDocument xmlUnitDoc = new XmlDocument();
            //Changed by Saurabh P Arora for MITS 16780: End
            try
            {
                if (test.Text == "Save")
                {
                    NavigateSave(null, null);
                }
                else
                {
                    m_iNavigation = 1;
                    XmlTemplate = GetMessageTemplate();
                    bReturnStatus = CallCWS("CheckStockSetupAdaptor.NewCheckStock", XmlTemplate, out sCWSresponse, false, true);
                    
                    //Changed by Saurabh P Arora for MITS 16780: Start 
                    xmlUnitDoc.LoadXml(sCWSresponse);
                    sUnit = xmlUnitDoc.SelectSingleNode("//Unit").InnerText;
                    Units.Text = sUnit;
                    //Changed by Saurabh P Arora for MITS 16780: End
                    
                    ClearControls();
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }




        /// <summary>
        /// Save and Navigate to saved record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateSave(object sender, ImageClickEventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            XmlDocument xmlServiceDoc = null;

            try
            {                
                XmlTemplate = GetMessageTemplate2();
                if (StockId.Text == "")
                {
                    bReturnStatus = CallCWS("CheckStockSetupAdaptor.NewCheckStockDetails", XmlTemplate, out sCWSresponse, false, true);
                }
                else
                {                 
                    bReturnStatus = CallCWS("CheckStockSetupAdaptor.UpdateCheckStockDetails", XmlTemplate, out sCWSresponse, false, true);
                }

                test.Text = "";
                xmlServiceDoc = new XmlDocument();
                xmlServiceDoc.LoadXml(sCWSresponse);

                if (FromBacktoBankAccounts.Text == "True")
                {
                    FromBacktoBankAccounts.Text = "";                    

                    if ((xmlServiceDoc.SelectSingleNode("//MsgStatusCd").InnerText == "Success"))
                    {
                        string sUrl = "urlNavigate('/RiskmasterUI/UI/FDM/bankaccount.aspx?recordID="  + AccountId.Text + "')";
                        ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>"+ sUrl+"</script>");
                    }

                }
                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }







        /// <summary>
        /// Returns Message Template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");

            sXml = sXml.Append("<CheckStockSetup>");
            sXml = sXml.Append("<StockId>");
            sXml = sXml.Append(StockId.Text);         
            sXml = sXml.Append("</StockId>");
            sXml = sXml.Append("<AccountId>");
            sXml = sXml.Append(AccountId.Text);
            sXml = sXml.Append("</AccountId>");
            sXml = sXml.Append("<Navigation>");
            sXml = sXml.Append(m_iNavigation.ToString() );
            sXml = sXml.Append("</Navigation>");
            sXml = sXml.Append("</CheckStockSetup></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());
            return oTemplate;
        }






        /// <summary>
        /// Returns Message Template
        /// </summary>
        /// <returns></returns>
        private XElement GetMessageTemplate2()
        {
            string sStockName = string.Empty;

            sStockName = StockName.Text;
            if (sStockName.IndexOf("&") > 0)
            {
                sStockName = sStockName.Replace("&", "&amp;");
            }
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            sXml = sXml.Append("<CheckStockSetup>");
            sXml = sXml.Append("<StockId>" + StockId.Text + "</StockId>");
            sXml = sXml.Append("<CheckStockDetail>");
            sXml = sXml.Append("<Data>");
            sXml = sXml.Append("<AccountId>" + AccountId.Text + "</AccountId>");
            sXml = sXml.Append("<StockName>" + sStockName + "</StockName>");
            sXml = sXml.Append("<FontName>" + FontName.Text + "</FontName>");
            sXml = sXml.Append("<FontSize>" + FontSize.Text + "</FontSize>");
            sXml = sXml.Append("<MICRFlag>");
            if(MICREnabled.Checked) 
                sXml = sXml.Append("True");
            else 
                sXml = sXml.Append("False");
            sXml = sXml.Append("</MICRFlag>");

            sXml = sXml.Append("<MICRSequence>" + HttpUtility.HtmlEncode(MICRNumber.Text) + "</MICRSequence>");
            sXml = sXml.Append("<MICRType>");
            //rsushilaggar MITS 25309 Date 10/07/2011
            //if(RxLaser.Checked)
            //    sXml = sXml.Append("0");
            //else if(TrueTypeFont.Checked)
              sXml = sXml.Append("1");
              sXml = sXml.Append("</MICRType>");

              sXml = sXml.Append("<MICRFont>" + MICRFontName.Text + "</MICRFont>");
              sXml = sXml.Append("<MICRFontSize>" + MICRFontSize.Text + "</MICRFontSize>");
              sXml = sXml.Append("<RXLaserFlag>" + hdnRXLaserFlag.Text + "</RXLaserFlag>");
              sXml = sXml.Append("<SoftForm>" + hdnSoftForm.Text + "</SoftForm>"); 
              sXml = sXml.Append("<RXLaserForm>" + RegularCheck.Text + "</RXLaserForm>");
              sXml = sXml.Append("<RXVoidForm>" + NonnegotiableorVoid.Text  + "</RXVoidForm>");
              sXml = sXml.Append("<RXSpecialForm>" + SpecialCheck.Text + "</RXSpecialForm>");


              sXml = sXml.Append("<SpecialFlag>");
              if (RxLaserhardwarecartridgeorDIMM.Checked == true)
              {
                  if (UseSpecialChecka.Checked)
                      sXml = sXml.Append("True");
                  else
                      sXml = sXml.Append("False");
              }
              else
              {
                  if (UseSpecialCheckb.Checked)
                      sXml = sXml.Append("True");
                  else
                      sXml = sXml.Append("False");
              }

              sXml = sXml.Append("</SpecialFlag>");
              

              sXml = sXml.Append("<SpecialAmount>");
              if (RxLaserhardwarecartridgeorDIMM.Checked == true) 
                sXml.Append(ActivatewhenAmounta.Text);
              else
                sXml.Append(ActivatewhenAmountb.Text);

              sXml = sXml.Append("</SpecialAmount>");                  

              sXml = sXml.Append("<DateX>" + DateXa.Text + "</DateX>");
              sXml = sXml.Append("<DateY>" + DateYa.Text + "</DateY>"); 
              sXml = sXml.Append("<CheckNumberX>" + CheckNumberXa.Text + "</CheckNumberX>");
              sXml = sXml.Append("<CheckNumberY>" + CheckNumberYa.Text + "</CheckNumberY>");
              sXml = sXml.Append("<CheckAmountX>" + AmountXa.Text + "</CheckAmountX>");
              sXml = sXml.Append("<CheckAmountY>" + AmountYa.Text + "</CheckAmountY>");
              sXml = sXml.Append("<CheckAmountTextX>" + AmountTextXa.Text + "</CheckAmountTextX>");
              sXml = sXml.Append("<CheckAmountTextY>" + AmountTextYa.Text + "</CheckAmountTextY>");
              sXml = sXml.Append("<ClaimNumberOffsetX>" + ClaimNumberXa.Text + "</ClaimNumberOffsetX>");
              sXml = sXml.Append("<ClaimNumberOffsetY>" + ClaimNumberYa.Text + "</ClaimNumberOffsetY>");

              sXml = sXml.Append("<MemoX>" + MemoTextXa.Text + "</MemoX>");
              sXml = sXml.Append("<MemoY>" + MemoTextYa.Text + "</MemoY>");
              sXml = sXml.Append("<CheckDtlX>" + TransDetailXa.Text + "</CheckDtlX>");
              sXml = sXml.Append("<CheckDtlY>" + TransDetailYa.Text + "</CheckDtlY>");
              sXml = sXml.Append("<CheckPayerX>" + PayerXa.Text + "</CheckPayerX>");
              sXml = sXml.Append("<CheckPayerY>" + PayerYa.Text + "</CheckPayerY>");
              sXml = sXml.Append("<CheckPayeeX>" + PayeeXa.Text + "</CheckPayeeX>");
              sXml = sXml.Append("<CheckPayeeY>" + PayeeYa.Text + "</CheckPayeeY>");
              sXml = sXml.Append("<CheckOffsetX>" + CheckOffsetXa.Text + "</CheckOffsetX>");
              sXml = sXml.Append("<CheckOffsetY>" + CheckOffsetYa.Text + "</CheckOffsetY>");
              sXml = sXml.Append("<CheckMICRX>" + MICROffsetXa.Text + "</CheckMICRX>");
              sXml = sXml.Append("<CheckMICRY>" + MICROffsetYa.Text + "</CheckMICRY>");
              sXml = sXml.Append("<SecondOffsetX>" + tndOffsetXa.Text + "</SecondOffsetX>");
              sXml = sXml.Append("<SecondOffsetY>" + tndOffsetYa.Text + "</SecondOffsetY>");
            //asingh263 mits 32712 starts
              sXml = sXml.Append("<EventDateX>" + EventDateXa.Text + "</EventDateX>");
              sXml = sXml.Append("<EventDateY>" + EventDateYa.Text + "</EventDateY>");
              sXml = sXml.Append("<ClaimDateX>" + ClaimDateXa.Text + "</ClaimDateX>");
              sXml = sXml.Append("<ClaimDateY>" + ClaimDateYa.Text + "</ClaimDateY>");
              sXml = sXml.Append("<PolicyNumberX>" + PolicyNumberXa.Text + "</PolicyNumberX>");
              sXml = sXml.Append("<PolicyNumberY>" + PolicyNumberYa.Text + "</PolicyNumberY>");
              sXml = sXml.Append("<InsuredNameX>" + InsuredNameXa.Text + "</InsuredNameX>");
              sXml = sXml.Append("<InsuredNameY>" + InsuredNameYa.Text + "</InsuredNameY>");
              sXml = sXml.Append("<PayeeAddressX>" + PayeeAddressXa.Text + "</PayeeAddressX>");
              sXml = sXml.Append("<PayeeAddressY>" + PayeeAddressYa.Text + "</PayeeAddressY>");
              sXml = sXml.Append("<PayeeAddOnCheckX>" + PayeeAddOnCheckXa.Text + "</PayeeAddOnCheckX>");
              sXml = sXml.Append("<PayeeAddOnCheckY>" + PayeeAddOnCheckYa.Text + "</PayeeAddOnCheckY>");
              //sXml = sXml.Append("<PayeeAddOnStubX>" + PayeeAddOnCheckXa.Text + "</PayeeAddOnStubX>");
              //sXml = sXml.Append("<PayeeAddOnStubY>" + PayeeAddOnCheckYa.Text + "</PayeeAddOnStubY>");
              sXml = sXml.Append("<AgentAddressX>" + AgentAddressXa.Text + "</AgentAddressX>");
              sXml = sXml.Append("<AgentAddressY>" + AgentAddressYa.Text + "</AgentAddressY>");
            //asingh263 mits 32712 ends

              sXml = sXml.Append("<Unit>" + Units.Text + "</Unit>");

              if (WindowsMetafilesoftwaregenerated.Checked == true)
              {
                  sXml = sXml.Append("<NewVisioVersion>");
                  if (NewVisioVersion.Checked)
                      sXml = sXml.Append("True");
                  else
                      sXml = sXml.Append("False");
                  sXml = sXml.Append("</NewVisioVersion>");
              }
              sXml = sXml.Append("<DollarSign>");

              if(SuppressSign.Checked)
                  sXml = sXml.Append("True");
              else 
                  sXml = sXml.Append("False");
                  
              sXml = sXml.Append("</DollarSign>");

              sXml = sXml.Append("<PrintAddress>");
              if(PrintPayeeAddressonCheck.Checked)
                  sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");
              sXml = sXml.Append("</PrintAddress>");
              //asingh263 32712 starts
              sXml = sXml.Append("<PrntClaimDateOnCheck>");
              if (PrintDateofClaimonCheck.Checked)
                  sXml = sXml.Append("True");
              else
                  sXml = sXml.Append("False");
              sXml = sXml.Append("</PrntClaimDateOnCheck>");

              sXml = sXml.Append("<PrintAgentNameAddressOnStub>");
             if(AgentNameAddressonStub.Checked)
                  sXml = sXml.Append("True");
             else
                  sXml = sXml.Append("False");
              sXml = sXml.Append("</PrintAgentNameAddressOnStub>");

              sXml = sXml.Append("<PrintDateofEvent>");
              if (PrintDateofEventonCheck.Checked)
                  sXml = sXml.Append("True");
              else
                  sXml = sXml.Append("False");
              sXml = sXml.Append("</PrintDateofEvent>");

              sXml = sXml.Append("<PrintInsuredNameOnCheck>");
              if (PrintInsuredNameonCheck.Checked)
                  sXml = sXml.Append("True");
              else
                  sXml = sXml.Append("False");
              sXml = sXml.Append("</PrintInsuredNameOnCheck>");

              sXml = sXml.Append("<PrintFullPolicyNumber>");
              if (PrintFullPolicyNoonCheck.Checked)
                  sXml = sXml.Append("True");
              else
                  sXml = sXml.Append("False");
              sXml = sXml.Append("</PrintFullPolicyNumber>");

              sXml = sXml.Append("<PrintFullPolicyNumberonStub>");
              if (FullPolicyNumberNoID.Checked)
                  sXml = sXml.Append("True");
              else
                  sXml = sXml.Append("False");
              sXml = sXml.Append("</PrintFullPolicyNumberonStub>");

              sXml = sXml.Append("<PrintClaimDateOnStub>");
              if (DateofClaimOnStub.Checked)
                  sXml = sXml.Append("True");
              else
                  sXml = sXml.Append("False");
              sXml = sXml.Append("</PrintClaimDateOnStub>");

              sXml = sXml.Append("<PrintPayeeAddressOnStub>");
              if (PrintPayeeAddressOnStub.Checked)
                  sXml = sXml.Append("True");
              else
                  sXml = sXml.Append("False");
              sXml = sXml.Append("</PrintPayeeAddressOnStub>");
            //asingh263 32712 ends

              sXml = sXml.Append("<PrintTaxIDOnStub>");
              if(PrintPayeeTaxIDoncheckStub.Checked)
                  sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");
                          
              sXml = sXml.Append("</PrintTaxIDOnStub>"); 


              sXml = sXml.Append("<PrintAdjNamePhoneOnStub>");             
              if(PrintAdjusterNameandPhoneoncheckStub.Checked)
                sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");
                          
              sXml = sXml.Append("</PrintAdjNamePhoneOnStub>");             
             


              sXml = sXml.Append("<PreNumberedCheckStock>");
              if (PreNumberedCheckStock.Checked)
                sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");              
              sXml = sXml.Append("</PreNumberedCheckStock>");


              sXml = sXml.Append("<PrintMemo>");
              if (PrintMemoonCheck.Checked)
                sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");
              sXml = sXml.Append("</PrintMemo>");



              sXml = sXml.Append("<PreNumStock>");
              if (PreNumberedCheckStock.Checked)
                sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");
              sXml = sXml.Append("</PreNumStock>");
             


              sXml = sXml.Append("<PrntEventOnStub>");
              if(PrintEventoncheckStub.Checked)
                sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");              
              sXml = sXml.Append("</PrntEventOnStub>");             

              sXml = sXml.Append("<FormatDateOfCheck>");
              if (FormatDateOfCheck.Checked)
                  sXml = sXml.Append("True");
              else
                  sXml = sXml.Append("False");
              sXml = sXml.Append("</FormatDateOfCheck>");
          
              sXml = sXml.Append("<PrintInsurer>");
              if(PrintClaiminsurerinsteadofPayer.Checked)
                sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");              
              sXml = sXml.Append("</PrintInsurer>");             

              
              sXml = sXml.Append("<PrintClaimNumberOnCheck>");
              if(PrintClaimNoonCheck.Checked)
                sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");              
              sXml = sXml.Append("</PrintClaimNumberOnCheck>");             

              
              sXml = sXml.Append("<PrintCtlNumberOnStub>");
              if(PrintControlonCheckStub.Checked)
                sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");              
              sXml = sXml.Append("</PrintCtlNumberOnStub>");             
              
              sXml = sXml.Append("<PrintCheckDateOnStub>");
              if(PrintDateOfCheck.Checked)
                sXml = sXml.Append("True");
              else 
                sXml = sXml.Append("False");              
              sXml = sXml.Append("</PrintCheckDateOnStub>");     

              sXml = sXml.Append("<PayerLevel>");
              sXml = sXml.Append(PayerLevelForChecks.SelectedValue);             
              sXml = sXml.Append("</PayerLevel>");
              sXml = sXml.Append("<CheckDateFormat>");
              sXml = sXml.Append(CheckDateFormat.SelectedValue);
              sXml = sXml.Append("</CheckDateFormat>");       
              sXml = sXml.Append("<MemoFontName>" + memoFontName.Text + "</MemoFontName>");
              sXml = sXml.Append("<MemoFontSize>" + memoFontSize.Text + "</MemoFontSize>");
              sXml = sXml.Append("<PayeeAddressFont>" + ddpayeeaddresscheckfont.Text + "</PayeeAddressFont>");//asharma326 jira 9288
              sXml = sXml.Append("<MemoRows>" + NoofRowsForMemo.Text + "</MemoRows>");
              sXml = sXml.Append("<MemoRowLength>" + NoofCharactersperMemoRow.Text  + "</MemoRowLength>");
            //Ashish Ahuja - Pay To The Order Config starts
              sXml = sXml.Append("<PayToOrderFontName>" + payToOrderFontName.SelectedValue + "</PayToOrderFontName>");
              sXml = sXml.Append("<PayToOrderFontSize>" + payToOrderFontSize.Text + "</PayToOrderFontSize>");
              sXml = sXml.Append("<PayToOrderRows>" + NoofRowsForPayToOrder.Text + "</PayToOrderRows>");
              sXml = sXml.Append("<PayToOrderRowLength>" + NoofCharactersperPayToOrderRow.Text + "</PayToOrderRowLength>");
              //Ashish Ahuja - Pay To The Order Config ends
              sXml = sXml.Append("<AddressFlag>" + hdnAddressFlag.Text + "</AddressFlag>");

              sXml = sXml.Append("<CheckLayout/>");

              sXml = sXml.Append("<CheckStockForms>");
              sXml = sXml.Append("<CheckStockForm>");
              sXml = sXml.Append("<StockId>" + StockId1.Text + "</StockId>");
              sXml = sXml.Append("<ImageIdx>0</ImageIdx>");
              sXml = sXml.Append("<FormType>" + "0" + "</FormType>");
              sXml = sXml.Append("<FormSource filename=" + @"""" +  filename1.Text + @"""" + ">" + file1.Text + "</FormSource>");
              sXml = sXml.Append("</CheckStockForm>");
              
              sXml = sXml.Append("<CheckStockForm>");
              sXml = sXml.Append("<StockId>" + StockId2.Text + "</StockId>");
              sXml = sXml.Append("<ImageIdx>1</ImageIdx>");
              sXml = sXml.Append("<FormType>" + "0" + "</FormType>");
              sXml = sXml.Append("<FormSource filename=" + @"""" +  filename2.Text + @"""" + ">" + file2.Text + "</FormSource>");
              sXml = sXml.Append("</CheckStockForm>");
              
              sXml = sXml.Append("<CheckStockForm>");
              sXml = sXml.Append("<StockId>" + StockId3.Text + "</StockId>");
              sXml = sXml.Append("<ImageIdx>2</ImageIdx>");
              sXml = sXml.Append("<FormType>" + "0" + "</FormType>");
              sXml = sXml.Append("<FormSource filename=" + @"""" +  filename3.Text + @"""" + ">" + file3.Text + "</FormSource>");
              sXml = sXml.Append("</CheckStockForm>");

              sXml = sXml.Append("</CheckStockForms>");
              sXml = sXml.Append("</Data>");
              sXml = sXml.Append("</CheckStockDetail>");
              sXml = sXml.Append("</CheckStockSetup>");
              sXml = sXml.Append("</Document>");
              sXml = sXml.Append("</Message>");

              XElement oTemplate = XElement.Parse(sXml.ToString());
              return oTemplate;
        }


        private void ClearControls()
        {
            try
            {
                Type controlType ;
                string sType;
                int index;                

                foreach (Control oCtrl in this.Form.Controls)
                {
                   if (oCtrl is WebControl)
                   {

                    controlType = oCtrl.GetType();
                    sType = oCtrl.ToString();
                    index = sType.LastIndexOf(".");
                    sType = sType.Substring(index + 1);   

                        switch(sType)
                        {
                            case "TextBox":

                                //Changed by Saurabh P Arora for MITS 16780: End
                                // (TextBox)oCtrl).Text = "";
                                if ((((TextBox)oCtrl).ID != "Units") & (((TextBox)oCtrl).ID != "hdnUnit"))
                                ((TextBox)oCtrl).Text = "";
                                //Changed by Saurabh P Arora for MITS 16780: End

                                break;
                            case "CheckBox":
                                ((CheckBox)oCtrl).Checked = false;                                
                                break;
                            case "DropDownList":
                                ((DropDownList)oCtrl).SelectedIndex = -1;
                                break;
                            case "RadioButton":
                                ((RadioButton)oCtrl).Checked = false;
                                break;                                
                        }
                    }               
                }

                AccountId.Text = AppHelper.GetQueryStringValue("AccountId");

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        //Changed by Saurabh Arora for MITS	18908:Start
         protected void NavigateBackToBankAccounts(object sender, EventArgs e)
        {
            if (test.Text == "Save")
            {
                NavigateSave(null, null);
                test.Text = "";
            }
            string sUrl = "urlNavigate('/RiskmasterUI/UI/FDM/bankaccount.aspx?recordID=" + AccountId.Text + "')";
            ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script type='text/javascript'>" + sUrl + "</script>");
        }
        //Changed by Saurabh Arora for MITS	18908:End

        //Rakhel ML Changes -start
         private string GetResourceValue(string strResourceKey, string strResourceType)
         {
             return RMXResourceManager.RMXResourceProvider.GetSpecificObject(strResourceKey, RMXResourceManager.RMXResourceProvider.PageId("CheckStocks.aspx"), strResourceType).ToString();
         }
        //Rakhel ML Changes -end
    }
}
