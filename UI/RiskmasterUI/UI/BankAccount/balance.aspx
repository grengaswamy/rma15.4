﻿<%@ page language="C#" autoeventwireup="true" codebehind="balance.aspx.cs" inherits="Riskmaster.UI.BankAccount.balance" %>

<%@ register src="~/UI/Shared/Controls/ErrorControl.ascx" tagname="ErrorControl"
    tagprefix="uc3" %>
<%@ register src="~/UI/Shared/Controls/CommonTasks.ascx" tagname="CommonTasks" tagprefix="uc4" %>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl"  TagPrefix="mc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bank Account Reconciliation</title>
    
     <script src="../../Scripts/form.js" type="text/javascript"></script>
    <!--Praveen ML-->    
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <!--Praveen ML-->

    <uc4:commontasks id="CommonTasks1" runat="server" />

    <script type="text/javascript">

        function OnProcess() {
            var filePath;

            if (document.forms[0].UploadFile != null) {
                filePath = document.forms[0].UploadFile.value;
            }
            else {
                filePath = document.forms[0].filename.value;
            }

            if (filePath.indexOf('.') == -1) {
                //alert('Please select a file to upload.');
                alert(balanceValidations.ValidSelectFileToUpload);
                return false;
            }

            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
            var validExtension = 'txt';
            if (ext != validExtension) {
                //                  alert('Incorrect file selected. Only files with extension .'
                //                     + validExtension.toUpperCase() + ' are allowed.');
                alert(balanceValidations.ValidationIncorrectFormat + validExtension.toUpperCase()); //MITS 30997
                return false;
            }


            document.forms[0].processflag.value = "process";
            if ((document.forms[0].UploadFile != null && document.forms[0].UploadFile.value != "")
                || document.forms[0].filename.value != "") {
                document.forms[0].ouraction.value = "process";
                //document.forms[0].files.value = document.forms[0].UploadFile.value;
            }
            else {
                return false;
            }
        }

        function OnCancel() {
            self.close();
            return true;
        }

        function ValForm() {
            document.forms[0].ouraction.value = "save";

            if (replace(document.forms[0].txtBeginDate.value, " ", "") == "") {
                //alert("The Statement Begin Date Is Required.");
                alert(balanceValidations.ValidBeginDate);
                document.forms[0].txtBeginDate.focus();
                return false;
            }
            if (replace(document.forms[0].txtEndDate.value, " ", "") == "") {
                //alert("The Statement End Date Is Required.");
                alert(balanceValidations.ValidEndDate);
                document.forms[0].txtEndDate.focus();
                return false;
            }
            //sgoel6 MITS 15275 04/18/2009
            if (!dateCompare())
            { return false; }
            return true;
        }

        function replace(sSource, sSearchFor, sReplaceWith) {
            var arr = new Array();
            arr = sSource.split(sSearchFor);
            return arr.join(sReplaceWith);
        }

        function ClosePageIfAdded() {
            //Amit 11/23/2009: Content, file name etc. are being handled using javascript in place of session variables Starts
            //var hdFileContent = document.forms[0].hdFileContent.value;
            var filename = document.forms[0].filename.value;
            //var accountlength;          

            //if(document.forms[0].AccountNoLength.checked)
            // accountlength = "1";
            //else
            //  accountlength = "";
            //Amit 11/23/2009: Content, file name etc. are being handled using javascript in place of session variables Ends


            if (document.forms[0].processflag.value == 'process' && filename != "") {
                //Amit 11/23/2009: Content, file name etc. are being handled using javascript in place of session variables 
                window.open("/RiskmasterUI/UI/BankAccount/reconciliationreport.aspx", '', "width=500,height=400,top=" + (screen.availHeight - 400) / 2 + ",left=" + (screen.availWidth - 500) / 2 + ",resizable=yes,scrollbars=yes");
                //window.open('home?pg=riskmaster/BankAccount/ReconciliationReport','',"width=500,height=400,top="+(screen.availHeight-400)/2+",left="+(screen.availWidth-500)/2+",resizable=yes,scrollbars=yes");
                document.forms[0].processflag.value = "";
                return true;
            }

            if (document.forms[0].ouraction.value == 'save') {
                document.forms[0].ouraction.value = '';
                self.close();
            }
        }
        //sgoel6 MITS 15275 04/18/2009
        function dateCompare() {
            var objBeginDate = eval('document.forms[0].txtBeginDate');
            var objEndDate = eval('document.forms[0].txtEndDate');
            if (objBeginDate == undefined) {
                var sBegDate = new String('');
            }
            else {
                var sBegDate = new String(objBeginDate.value);
            }

            if (objEndDate == undefined) {
                var sEndDate = new String('');
            }
            else {
                var sEndDate = new String(objEndDate.value);
            }

            if (sBegDate != '' && sEndDate != '') {
                //mbahl3 Mits  30940
                var target = $("#txtBeginDate");
                var sectarget = $("#txtEndDate");
                var inst = $.datepicker._getInst(target[0]);
                var secinst = $.datepicker._getInst(sectarget[0]);
                // var Date1 = new Date(objBeginDate.value);
                // var Date2 = new Date(objEndDate.value);
                fdate = inst.selectedDay;
                fmonth = inst.selectedMonth;
                fyear = inst.selectedYear;

                secdate = secinst.selectedDay;
                secmonth = secinst.selectedMonth;
                secyear = secinst.selectedYear;

                var effDate = new Date();
                effDate.setFullYear(fyear, fmonth, fdate);

                var expDate = new Date();
                expDate.setFullYear(secyear, secmonth, secdate);

                if (effDate > expDate) {
                    //mbahl3  Mits  30940
                    alert(balanceValidations.ValidBeginAndEndDate);
                    return false;
                } 
					 } 
					   return true;
			    }
         
        
    </script>
    
</head>
<body onload="ClosePageIfAdded();">
    <form id="frmData" name="frmData" runat="server" enctype="multipart/form-data">
    <asp:scriptmanager id="SMgr" runat="server" />
    <div>
        <table>
            <tr>
                <td colspan="2">
                    <uc3:errorcontrol id="ErrorControl1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="msgheader">
                    <asp:Label ID="lblBankAccReconciliation" runat="server" Text="<%$ Resources:lblBankAccReconciliationResrc %>" />
                </td>
            </tr>
            <tr>
                <td class="required">
                    <asp:Label ID="lblBeginDate" class="required" runat="server" Text="<%$ Resources:lblBeginDateResrc %>" />
                </td>
                <td>
                    <asp:textbox runat="server" formatas="date" id="txtBeginDate" rmxtype="date" onblur="dateLostFocus(this.id);" />
                    <script type="text/javascript">
                        $(function () {
                            $("#txtBeginDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });     
                    </script>
                </td>
            </tr>
            <tr>
                <td class="required">
                    <asp:Label ID="lblEndDate" class="required" runat="server" Text="<%$ Resources:lblEndDateResrc %>" />
                </td>
                <td>
                    <asp:textbox runat="server" formatas="date" id="txtEndDate" rmxtype="date" onblur="dateLostFocus(this.id);" />
                    <script type="text/javascript">
                        $(function () {
                            $("#txtEndDate").datepicker({
                                showOn: "button",
                                buttonImage: "../../Images/calendar.gif",
                                buttonImageOnly: true,
                                showOtherMonths: true,
                                selectOtherMonths: true,
                                changeYear: true
                            });
                        });
                     </script>
                    
                </td>
            </tr>
            <tr>
                <td class="required" nowrap="nowrap">
                    <asp:Label ID="lblBalance" class="required" runat="server" Text="<%$ Resources:lblBalanceResrc %>" />
                </td>
                <td>
                    <mc:CurrencyTextbox runat="server" size="30" onchange="setDataChanged(true);" id="txtBalance"
                        maxlength="50"  />
                </td>
            </tr>
            <tr>
                <td colspan="2" nowrap="nowrap">
                    <asp:label runat="server" id="lblPath" text="<%$ Resources:lblPathResrc %>" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:fileupload id="UploadFile" runat="server"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:checkbox id="AccountNoLength" runat="server" Text="<%$ Resources:chkAccountNoLengthResrc %>" rmxref="Instance/Document/AccountNoLength" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:label runat="server" id="lblCheckUncheck" text="<%$ Resources:lblCheckUncheckResrc %>" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <input id="Submit1" type="submit" name="ProcessFile" runat="server" value="<%$ Resources:btnProcessFileResrc %>" class="button" onclick="return OnProcess();; " />
                    <input id="Submit2" type="submit" name="SaveClose" runat="server" value="<%$ Resources:btnSaveCloseResrc %>" class="button" onclick="return ValForm();; " />
                    <input id="Submit3" type="submit" name="Cancel" runat="server" value="<%$ Resources:btnCancelResrc %>" class="button" onclick="return OnCancel();; " />
                </td>
                <asp:textbox id="ouraction" style="display: none" rmxref="Instance/Document/Action"
                    runat="server" />
                <asp:textbox id="accountid" style="display: none" rmxref="Instance/Document/AccountId"
                    runat="server" />
                <asp:textbox id="filename" style="display: none" rmxref="Instance/Document/UploadFileName"
                    runat="server" />
                <asp:textbox id="processflag" style="display: none" rmxref="Instance/Document/ProcessFlag"
                    runat="server" />
                <asp:textbox id="hdFileContent" style="display: none" rmxref="Instance/Document/files/file"
                    runat="server" />
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
