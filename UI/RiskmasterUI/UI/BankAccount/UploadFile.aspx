﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="Riskmaster.UI.BankAccount.UploadFile" %>

 <%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>    

<html>
 <head id="Head1" runat="server">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
  <title>Upload File</title>
  <script language="JavaScript" src="../../Scripts/CheckStockSetup.js">      { var i; } </script>
  <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
  <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
  <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
  
  <script language="JavaScript">
      function checkFileExtension() {
          var filePath = document.getElementById('UploadFile1').value;

          document.getElementById('FileSelected').value = 'True';

          if (filePath.indexOf('.') == -1) {
              alert(UploadFileValidations.SelectFileID);
              return false;
          }
          var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

          var validExtension = 'emf';
          if (ext == validExtension)
              return true;
          alert(String(UploadFileValidations.AlertMsgID1) + validExtension.toUpperCase() + String(UploadFileValidations.AlertMsgID2));
          return false;
      }
        </script>
        <%--<script src="csc-Theme/riskmaster/common/javascript/zapatec/utils/zapatec.js" type="text/javascript"></script><script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/window.js" type="text/javascript"></script><script src="csc-Theme/riskmaster/common/javascript/zapatec/zpwin/src/dialog.js" type="text/javascript"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/lang/calendar-en.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpcal/src/calendar-setup.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/calendar-alias.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-xml.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-editable.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/zapatec/zpgrid/src/zpgrid-query.js"></script><script type="text/javascript" src="csc-Theme/riskmaster/common/javascript/rmx-common-ref.js"></script>--%>
        </head>
 <body onload="return FileTransfer();">
     <form id="frmData" runat="server">
     <table>
     <tr>
     <td>
     <uc3:ErrorControl ID="ErrorControl1" runat="server" />
     </td>
     </tr>
     </table>
  <table>
    <tr>
      <td colspan="2">
        
       </td>
    </tr> 
    </table>  
  <input type="hidden" id="wsrp_rewrite_action_1" name="" value=""/>
  <table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
     <td class="ctrlgroup">
         <asp:Label ID="lblUploadFileID" runat="server" Text="<%$ Resources:lblUploadFileID %>"></asp:Label></td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td>
         <asp:Label ID="lblSetImageID" runat="server" Text="<%$ Resources:lblSetImageID %>"></asp:Label>
      								<br/> 
     </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td>
         <asp:FileUpload id="UploadFile1" runat="server" />
     </td>
     
    <%-- <input type="file" style="width:85%" id="UploadFile"></td>--%>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td colspan="2" align="center">
     
 <%--    <xforms:submit xxforms:appearance="button" xhtml:class="button" xhtml:id="btnSubmit" xhtml:onClick="Javascript:return checkFileExtension();">
										<xforms:setvalue ref="/Instance/Document/FileSelected">True</xforms:setvalue>
										<xforms:label>Submit</xforms:label>
										</xforms:submit> --%>    
    
     <asp:button type="submit" runat="server" class="button" id="btnSubmit" text="<%$ Resources:btnSubmit %>" OnClientClick="Javascript:return checkFileExtension();" />&nbsp;
<%--    
    	<xforms:setvalue ref="/Instance/Document/FileSelected">True</xforms:setvalue>
		<xforms:label>Submit</xforms:label>	--%>
          
     </td>
    </tr>
   </table>
   
   <asp:TextBox runat="server" ref="/Instance/Document/TempFileCreated" style="display:none" id="TempFileCreated"/>
   <asp:TextBox runat="server" ref="/Instance/Document/image" style="display:none" id="image"/>
   <asp:TextBox runat="server" ref="/Instance/Document/FileSelected" style="display:none" id="FileSelected"/>
   <asp:TextBox runat="server" ref="/Instance/Document/files/file" style="display:none" id="file"/>
   <asp:TextBox runat="server" ref="/Instance/Document/files/file/@filename" style="display:none" id="filename"/>
   <asp:TextBox runat="server" ref="/Instance/Document/formmode" style="display:none" id="formmode"/>
   <asp:TextBox runat="server" ref="/Instance/Document/files/type" style="display:none" id="filetype"/>
   <asp:TextBox runat="server" ref="/Instance/ui/action" style="display:none" id="screenaction"/>
   <asp:TextBox runat="server" ref="/Instance/Document/refresh" style="display:none" id="refreshParent"/>   
   
   
     </form>
 </body>
</html>