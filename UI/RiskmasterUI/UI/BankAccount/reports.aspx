﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="reports.aspx.cs" Inherits="Riskmaster.UI.BankAccount.reports" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
   
     <title>Reports</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/system.css" type="text/css"/>
     <uc4:CommonTasks ID="CommonTasks1" runat="server" />           
     
     <script type="text/javascript">

         function OnPrint() {

             var sReportType = "";
             var sAccountId = "0";
             if (document.getElementById('Type1').checked)
                 sReportType = "1";
             else if (document.getElementById('Type2').checked)
                 sReportType = "2";
             else if (document.getElementById('Type3').checked)
                 sReportType = "3";
             else if (document.getElementById('Type4').checked)
                 sReportType = "4";

             sAccountId = document.getElementById('accountid').value;

             //window.open('http://localhost/RiskmasterUI/UI/BankAccount/showreport.aspx?AccountId=' + sAccountId + '&ReportSelected=' + sReportType);

             window.open('/RiskmasterUI/UI/BankAccount/showreport.aspx?AccountId=' + sAccountId + '&ReportSelected=' + sReportType, 'Reports',
							'width=650,height=550,top=' + (screen.availHeight - 550) / 2 + ',left=' + (screen.availWidth - 650) / 2 + ',resizable=yes,scrollbars=yes');

             return false;

         }
         function OnCancel() {
             self.close();
             return true;
         }
         function OnLoad() {
             document.forms[0].Type2.checked = true;
         }			
					
	</script>
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table border="0" align="center"> 
    <tr>
      <td colspan="2">
        <uc3:ErrorControl ID="ErrorControl1" runat="server" />
       </td>
    </tr>  
    <tr>   
     <td colspan="1" class="ctrlgroup">
         <asp:Label ID="lblChooseReportToProcessID" runat="server" Text="<%$ Resources:lblChooseReportToProcessID %>"></asp:Label></td>
    </tr>
    <tr>
     <td colspan="1" class="required">
         <asp:Label ID="lblIntExtBalRepID" runat="server" Text="<%$ Resources:lblIntExtBalRepID %>"></asp:Label></td>
    </tr>
    <tr>
     <td><input type="radio" name="$node^19" value="1" id="Type1"/><asp:Label ID="lblMasterAccntBalRepID"
             runat="server" Text="<%$ Resources:lblMasterAccntBalRepID %>"></asp:Label><br/>
     <input type="radio" name="$node^19" value="2" id="Type2" checked="true"/><asp:Label
         ID="lblMonBnkReconRepID" runat="server" Text="<%$ Resources:lblMonBnkReconRepID %>"></asp:Label></td>
    </tr>
    <tr>
     <td colspan="1" class="required">
         <asp:Label ID="lblIntSysBalRepID" runat="server" Text="<%$ Resources:lblIntSysBalRepID %>"></asp:Label></td>
    </tr>
    <tr>
     <td>
     <input type="radio" name="$node^19" value="3" id="Type3"/><asp:Label ID="lblChkAccntMasBalRepID"
         runat="server" Text="<%$ Resources:lblChkAccntMasBalRepID %>"></asp:Label><br/>
     <input type="radio" name="$node^19" value="4" id="Type4"/><asp:Label ID="lblMonChkAccntReconRepID"
         runat="server" Text="<%$ Resources:lblMonChkAccntReconRepID %>"></asp:Label></td>
    </tr>
    <tr>
     <td colspan="2" align="center">
         
         
         <input type="submit" id="btnPrint" name="btnPrint" value="<%$ Resources:btnPrint %>" runat="server" class="button" onclick="return OnPrint();;"/>
     <input type="submit" id="btnCancel" name="btnCancel" value="<%$ Resources:btnCancel %>" runat="server" class="button" onclick="return OnCancel();;"/>
     </td>
    </tr>
   </table>         
     <asp:textbox ID="accountid" style="display:none" RMXRef="Instance/Document/AccountId" runat="server" />     
    </div>
    </form>
</body>
</html>
