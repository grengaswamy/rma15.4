﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.BankAccount
{
    public partial class MoneyMarketAccountBalanceInfo : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";

            try
            {
                subaccountid.Text = AppHelper.GetQueryStringValue("SubAccountId");
                monmktacc.Text = AppHelper.GetQueryStringValue("MoneyMktAccount");
                startdate.Text = AppHelper.GetQueryStringValue("StartDate");
                enddate.Text = AppHelper.GetQueryStringValue("EndDate");


                XmlTemplate = GetMessageTemplate();
                bReturnStatus = CallCWSFunctionBind("BankAccountManagementAdaptor.LoadMoneyMarketAccountBalance", out sCWSresponse, XmlTemplate);

                //btnLoadPriorStatementDate.Attributes.Add("onclick", "LoadPriorStatementDate('" + sdate.Text + "');return false;");

                XmlTemplate = XElement.Parse(sCWSresponse);



                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns></returns>
        private XElement GetMessageTemplate()
        {       
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document>");

                sXml = sXml.Append("<AccountBalance>");
                sXml = sXml.Append("<SubAccountId>");
                sXml = sXml.Append(subaccountid.Text);
                sXml = sXml.Append("</SubAccountId>");
                sXml = sXml.Append("<MoneyMktAccount>");
                sXml = sXml.Append(monmktacc.Text);
                sXml = sXml.Append("</MoneyMktAccount>");
                sXml = sXml.Append("<StartDate>");
                sXml = sXml.Append(startdate.Text);
                sXml = sXml.Append("</StartDate>");
                sXml = sXml.Append("<EndDate>");
                sXml = sXml.Append(enddate.Text);
                sXml = sXml.Append("</EndDate>");            
                sXml = sXml.Append("</AccountBalance></Document></Message>");

                XElement oTemplate = XElement.Parse(sXml.ToString());
                return oTemplate;
            }


    }
}
