﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisbursementAccountBalanceInfo.aspx.cs" Inherits="Riskmaster.UI.BankAccount.DisbursementAccountBalanceInfo" %>
<%@ Register Src="~/UI/Shared/Controls/ErrorControl.ascx" TagName="ErrorControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Disbursement Account Balance Information</title>
     <uc4:CommonTasks ID="CommonTasks1" runat="server" /> 
     <link rel="StyleSheet" href="../../Content/rmnet.css" type="text/css" />  
</head>
<body>
    <form id="frmData" name="frmData" runat="server">    
    <div>
    <table>   
    <tr>
            <td colspan="2">
              <uc3:ErrorControl ID="ErrorControl1" runat="server" />
            </td>
         </tr>
    
    <tr>
     <td colspan="8" class="ctrlgroup">
         <asp:Label ID="lblDisbursementAccntBalInfoID" runat="server" Text="<%$ Resources:lblDisbursementAccntBalInfoID %>"></asp:Label></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblPriorBalID" runat="server" Text="<%$ Resources:lblPriorBalID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox1" runat="server" RMXRef="Instance/Document/DisbursementAccountInfo/@PriorBalance" text="$0.00" Enabled="false"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblStatementDatesID" runat="server" Text="<%$ Resources:lblStatementDatesID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox id="sdate" RMXRef="Instance/Document/DisbursementAccountInfo/@StatementBeginDate" runat="server" type="text" text="N/A" Enabled="false"/>--<asp:textbox ID="edate" RMXRef="Instance/Document/DisbursementAccountInfo/@StatementEndDate" runat="server" type="text" text="N/A" Enabled="false"  />
	 <asp:button runat="server" class="CodeLookupControl" id="btnLoadPriorStatementDate"/>
     </td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblDepInTransitID" runat="server" Text="<%$ Resources:lblDepInTransitID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox3" runat="server" RMXRef="Instance/Document/DisbursementAccountInfo/@TransitDeposits" Enabled="false" type="text" name="$node^27" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblDepositsID" runat="server" Text="<%$ Resources:lblDepositsID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox4" runat="server" RMXRef="Instance/Document/DisbursementAccountInfo/@NumOfDeposits" type="text" Enabled="false" name="$node^34" value="0" size="10" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblOutstandingChksID" runat="server" Text="<%$ Resources:lblOutstandingChksID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox5" runat="server" RMXRef="Instance/Document/DisbursementAccountInfo/@OutstandingChecks" type="text" Enabled="false" name="$node^28" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblChecksID" runat="server" Text="<%$ Resources:lblChecksID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox6" runat="server" RMXRef="Instance/Document/DisbursementAccountInfo/@NoOfChecks" type="text" Enabled="false" name="$node^35" value="0" size="10" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblReconciledItemsID" runat="server" Text="<%$ Resources:lblReconciledItemsID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox7" runat="server" RMXRef="Instance/Document/DisbursementAccountInfo/@ReconciledItem" type="text" Enabled="false" name="$node^29" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblReconNumChksID" runat="server" Text="<%$ Resources:lblReconNumChksID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox8" RMXRef="Instance/Document/DisbursementAccountInfo/@ReconcilitaionNumChecks" runat="server" type="text" Enabled="false" name="$node^36" value="0" size="10" disabled="true"/><asp:textbox ID="Textbox9" RMXRef="/Instance/Document/DisbursementAccountInfo/@ClearedDeposits" runat="server" type="text" name="$node^37" value="$0.00" disabled="true"/></td>
    </tr>      
    <tr>
     <td align="right"><asp:Label id="lblSubTotalID" runat="server" Text="<%$ Resources:lblSubTotalID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox16" runat="server" RMXRef="Instance/Document/DisbursementAccountInfo/@SubTotal" type="text" Enabled="false" name="$node^29" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>     
     <td align="right"><asp:Label id="lblReconTotChksID" runat="server" Text="<%$ Resources:lblReconTotChksID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox11" RMXRef="Instance/Document/DisbursementAccountInfo/@ReconcilitaionNumChecks" runat="server" type="text" name="$node^38" value="0" size="10" disabled="true"/><asp:textbox ID="Textbox12" RMXref="/Instance/Document/DisbursementAccountInfo/@ClearedChecks" runat="server" type="text" name="$node^39" text="$0.00" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblCurrentBalanceID" runat="server" Text="<%$ Resources:lblCurrentBalanceID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox13" RMXRef="Instance/Document/DisbursementAccountInfo/@CurrentBalance" runat="server" type="text" name="$node^31" value="$0.00" disabled="true"/></td>
     <td class="datatd"></td>
     <td align="right"><asp:Label id="lblReconTotAdjustmentsID" runat="server" Text="<%$ Resources:lblReconTotAdjustmentsID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox14" RMXRef="Instance/Document/DisbursementAccountInfo/@ReconcilitaionNumAdjustments" runat="server" type="text" name="$node^40" value="0" size="10" disabled="true"/><asp:textbox ID="Textbox15" RMXRef="Instance/Document/DisbursementAccountInfo/@Adjustments" runat="server" type="text" name="$node^41" value="$0.00" disabled="true"/></td>
    </tr>
    <tr>
     <td align="right"><asp:Label id="lblOutOfBalanceID" runat="server" Text="<%$ Resources:lblOutOfBalanceID %>"></asp:Label></td>
     <td class="datatd"><asp:textbox ID="Textbox10" RMXRef="Instance/Document/DisbursementAccountInfo/@OutOfBalance" runat="server" type="text" name="$node^31" value="$0.00" disabled="true"/></td>     
    </tr>
    <tr>
     <td></td>
     <td></td>
     <td></td>
     <td align="center">
     <asp:button  runat="server" text="<%$ Resources:btnMoneyMarketAcc %>" class="button" id="btnMoneyMarketAcc" OnClientClick="return LoadMoneyMarketAccountBalance();"/>     
     <asp:button  runat="server" text="<%$ Resources:btnButton1 %>" class="button" id="Button1" OnClientClick="window.close(); return false;"/>          
     </td>
    </tr>
   </table>        
    <asp:textbox ID="hdStatementCount" style="display:none" RMXRef="Instance/Document/DisbursementAccountInfo/Statements/@Count" runat="server" />
    <asp:textbox ID="ouraction" style="display:none" runat="server" />
    
    <asp:textbox ID="accountid" style="display:none" runat="server" />
    <asp:textbox ID="recordid" style="display:none" runat="server" />
    <asp:textbox ID="refreshpage" style="display:none" runat="server" />        
    <asp:textbox ID="subaccountid" style="display:none" runat="server" />
    <asp:textbox ID="monmktacc" style="display:none" runat="server" />
	
    
    <%--<asp:textbox ID="accountid" style="display:none" RMXRef="Instance/Document/AccountId" runat="server" />
    <asp:textbox ID="recordid" style="display:none" RMXRef="Instance/Document/RecordId" runat="server" />
    <asp:textbox ID="refreshpage" style="display:none" RMXRef="Instance/Document/RefreshPage" runat="server" />    
    
    <asp:textbox ID="subaccountid" style="display:none" RMXRef="Instance/Document/SubAccountId" runat="server" />
    <asp:textbox ID="monmktacc" style="display:none" RMXRef="Instance/Document/MonMktAcc" runat="server" />     --%>
						
    </div>  
    
    
    </form>
</body>
</html>
