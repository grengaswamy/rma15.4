﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using System.Threading;
using MultiCurrencyCustomControl;
using System.Globalization;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.BankAccount
{
    public partial class balance : NonFDMBasePageCWS
    {       
        protected void Page_Load(object sender, EventArgs e)
        {

            //Praveen: ML Changes
                string sCulture = AppHelper.GetCulture().ToString();
                if (sCulture != "en-US")
                {
                    //Register Date Script
                    AppHelper.CalenderClientScript(sCulture, this);
                    //Register Time Script
                    AppHelper.TimeClientScript(sCulture, this);
                }

                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("balance.aspx"), "balanceValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "balanceValidations", sValidationResources, true);
            //Praveen: ML Changes
            
            XElement XmlTemplate = null;
            bool bReturnStatus = false;
            string sCWSresponse = "";
            Stream ioStream = UploadFile.FileContent;
            string base64String;
            byte[] binaryData;
            string scurrencytype = string.Empty;

            try
            {
                //Pen testing  changes : atavaragiri   MITS 27789
                //accountid.Text = AppHelper.GetQueryStringValue("AccountId");
                //scurrencytype = AppHelper.GetQueryStringValue("curtype"); 

                accountid.Text =AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("AccountId"));
                scurrencytype = AppHelper.HTMLCustomEncode(AppHelper.GetQueryStringValue("curtype"));
                // End pen testing changes end mits 27789

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(scurrencytype.Split('|')[1]);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(scurrencytype.Split('|')[1]);
                

                if(UploadFile.PostedFile != null)
                    filename.Text = UploadFile.PostedFile.FileName;
                binaryData = new Byte[ioStream.Length];
                long bytesRead = ioStream.Read(binaryData, 0,
                                    (int)ioStream.Length);
                base64String = System.Convert.ToBase64String(binaryData, 0,binaryData.Length);
                ioStream.Flush();
                ioStream.Close();
                hdFileContent.Text = base64String;

                //Amit 11/23/2009: Content, file name etc. are being handled using javascript in place of session variables
                ////Amit: content sent in session variable in place of query string 11/23/2009
                //Session["FileContent"] = hdFileContent.Text;
                
                XmlTemplate = GetMessageTemplate(base64String);

                if (filename.Text != "")
                {
                    lblPath.Text = "Selected File Path : " + filename.Text;
                    lblPath.Visible = true;
                    UploadFile.Visible = false;
                }
                
                if(ouraction.Text == "save")
                    bReturnStatus = CallCWS("BankAccountManagementAdaptor.SaveNewBalanceStatementInformation", XmlTemplate,out sCWSresponse, true, false);                                
                
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

       


        private XElement GetMessageTemplate(string base64String)
        {       
                StringBuilder sXml = new StringBuilder("<Message>");
                sXml = sXml.Append("<Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>");
                sXml = sXml.Append("<Call><Function></Function></Call><Document>");

                sXml = sXml.Append("<AccountBalance>");
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(accountid.Text);
                sXml = sXml.Append("</AccountId>");
                sXml = sXml.Append("<BeginDate>");
                sXml = sXml.Append(txtBeginDate.Text);
                sXml = sXml.Append("</BeginDate>");
                sXml = sXml.Append("<EndDate>");
                sXml = sXml.Append(txtEndDate.Text);
                sXml = sXml.Append("</EndDate>");
                sXml = sXml.Append("<Balance>");
                sXml = sXml.Append(txtBalance.Amount);
                sXml = sXml.Append("</Balance>");
                sXml = sXml.Append("<Action>");
                sXml = sXml.Append(ouraction.Text);
                sXml = sXml.Append("</Action>");
                sXml = sXml.Append("<AccountNoLength>");
                if(AccountNoLength.Checked == true)
                    sXml = sXml.Append("1");
                sXml = sXml.Append("</AccountNoLength>");
                sXml = sXml.Append("<file>");
                sXml = sXml.Append(base64String);
                sXml = sXml.Append("</file>");
                sXml = sXml.Append("</AccountBalance></Document></Message>");

                XElement oTemplate = XElement.Parse(sXml.ToString());
                return oTemplate;
            }

      

    }
}
