﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadFiles.aspx.cs" Inherits="Riskmaster.UI.UploadFiles.UploadFiles" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>

<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <script language="javascript" type="text/javascript" src="../../../../Scripts/Utilities.js"></script>
    <script language="javascript" type="text/javascript" src="../../../../Scripts/form.js"></script>
    <script language="JavaScript" src="../../../../Scripts/WaitDialog.js"></script>
</head>
<body onload="parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
    <div>
    
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
    <tr>
     <td class="ctrlgroup"><asp:Label ID="lblTitle" runat="server" Text="Uploading files"></asp:Label></td>
    </tr>
    <tr>
     <td>
       
          <asp:Label ID="lbSuccess" runat="server" Visible="false" Text="The  Files have been successfully uploaded" Style="font-weight: bold;
                        color: blue"></asp:Label>
       
         <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
        </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <CuteWebUI:UploadAttachments  ID="UploadDocumentAttachments" runat="server" InsertText="Click here to upload Files" OnAttachmentRemoveClicked="OnAttachmentRemoveClicked_Click" >
            </CuteWebUI:UploadAttachments>
        </td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td colspan="2" align="center">
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" onclick="SaveData_Click" UseSubmitBehavior="true" OnClientClick="return FileTransfer()" />&nbsp;
        &nbsp;
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClientClick="window.close(); return false;"  />
     </td>
    </tr>
   </table><input type="hidden" value="" id="file">
   <input type="hidden" value="" id="filename" runat="server">
   <input type="hidden" value="" id="hdAction">
   <input type="hidden" value="" id="SysWindowId">
   <uc2:PleaseWaitDialog ID="pleaseWait" runat="server"  CustomMessage="Uploading"/>
    
    </div>
    </form>
</body>
</html>
