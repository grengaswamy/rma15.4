﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="DeductibleGroupDetails.aspx.cs" Inherits="Riskmaster.UI.UI.Deductible.DeductibleGroupDetails" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl" TagPrefix="mc" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
    </title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.2.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                if (parent.pleaseWait != null) {
                    parent.pleaseWait("stop");
                }
                var hdn_Function = $('#hdn_FunctionToCall')[0];
                if (hdn_Function != null && hdn_Function.value != "") {
                    hdn_Function.value = "";
                    var oSysPageDataChanged = $('#SysPageDataChanged')[0];
                    if (oSysPageDataChanged != null) {
                        oSysPageDataChanged.value = "true";
                    }
                }
                else {
                    parent.MDIScreenLoaded();
                }

                $('#btnCancel').click(BackToReservesGrid);
                $('#btnBack').click(BackToDeductibleGrid);
            });

            function BackToReservesGrid() {
                var btnSave = $('#btnSave');
                if (ConfirmSave()) {
                    if (btnSave != null) {
                        btnSave.click();
                    }
                }
                else {
                    var sClaimId = $('#txtClaimId')[0].value;
                    //RMA-8490 START
                    // window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=' + sClaimId;
                    window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=' + sClaimId + "&ClaimantListClaimantEID=" + document.getElementById('ClaimantListClaimantEID').value;
                    //RMA-8490 END
                }
                return false;
            }

            function BackToDeductibleGrid() {
                var btnSave = document.getElementById('btnSave');
                if (ConfirmSave()) {
                    if (btnSave != null) {
                        btnSave.click();
                        return true;
                    }
                }
            }

            function IsDataChanged() {
                var oSysPageDataChanged = $('#SysPageDataChanged')[0];
                if (oSysPageDataChanged != null && oSysPageDataChanged.value == "true") {
                    return true;
                }
                else {
                    return false;
                }
            }
    
        </script>
</head>
<body>
    <form id="frmData" runat="server">
    <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td colspan="7">
                    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_save_active.png" 
                        class="bold" ToolTip="<%$ Resources:btnSaveTip %>"
                        runat="server" OnClientClick="return IsDataChanged();parent.pleaseWait('start');" 
                        onclick="btnSave_Click"/>
                    <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_backtofinancials_active.png" class="bold" ToolTip="<%$ Resources:btnCancelTip %>"
                        runat="server"/>

                    <!-- Deepak Dhiman added, 10/14/2014 -->
                    <asp:ImageButton ID="btnBack" ImageUrl="~/Images/tb_backtosummary_active.png" class="bold" ToolTip="<%$ Resources:btnBack %>"
                        runat="server" onclick="btnBack_Click"/>
                    <asp:ImageButton ID="btnHistory" ImageUrl="~/Images/tb_viewhistory_active.png" class="bold" ToolTip="<%$ Resources:btnHistory %>"
                        runat="server" onclick="btnHistory_Click"/>
                    <!-- End Deepak Dhiman added, 10/14/2014 -->
                </td>
            </tr>
            <tr>
                <td><input type="text" name="" value="deductiblegroupdetails" id="SysFormName"  style="display:none" /></td>
                <td><input type="text" name="" value="" id="SysPageDataChanged"  style="display:none" /></td>
            </tr>
            <tr>
                <td colspan="7" class="ctrlgroup">
                    <asp:Label ID="lblDeductibleDetailsHeader" runat="server" Text="<%$ Resources:lblAggDedLimitHeader %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td align="left">
                    <asp:Label ID="lblCovGroupId" runat="server" Text="<%$ Resources:lblCvgGroup %>"></asp:Label>
                </td>
                <td >
                    <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);" ID="CovGroupId" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CovGroupId']" CodeTable="COVERAGE_GROUP" ControlName="CovGroupId"  RMXType="code" Filter= "(CG.STATE_ROW_ID = (UnitStateRowId) OR CG.STATE_ROW_ID =0)" CodeFilter=""/>
                </td>
                <td align="left" >
                    <asp:Label ID="lblAggLimit" runat="server" Text="<%$ Resources:lblDedLimit %>"></asp:Label>
                </td>
                <td >
                    <mc:CurrencyTextbox id="txtAggLimit" OnChange="setDataChanged(true)" runat="server" size="20" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='AggregateLimit']"/>
                </td>
            </tr>

            <tr>
			 <td align="left" >
                    <asp:Label runat="server" ID="lblDedPerEvent" Text="<%$ Resources:lblDedPerEvent %>" />
                    
                </td>
                <td >
                    <mc:CurrencyTextbox id="txtDedPerEvent" OnChange="setDataChanged(true)" runat="server" size="20" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='DedPerEvent']"/>
                </td>
                <td align="left" >
                    <asp:Label ID="lblAggBalance" runat="server" Text="<%$ Resources:lblRemainingAggAmount %>"></asp:Label>
                </td>
                <td >
                    <mc:CurrencyTextbox id="txtAggBalance" runat="server" size="20" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='RemainingAggregateAmount']" ReadOnly="true"/>
                </td>
               
            </tr>
            
            <tr>
                <td align="left"><asp:Label ID="SetGroupIdAtPolicyLevel" runat="server" Text="<%$ Resources:lblSetGroupAtLvl %>"></asp:Label>
                </td>
                <td>
                <asp:CheckBox runat="server" ID="cbSetAtlevel" OnChange="setDataChanged(true)" Text="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='AllCoverageChk']"/>    
                </td>
              <%--  ExpenseFlagAddition - nbhatia6 - 07/03/14--%>
                <%--<td></td>--%>
                <td align="left">
                    <asp:Label ID="lblExclExpensePmt" runat="server" Text="<%$ Resources:lblExclExpensePmt %>"></asp:Label>
                </td>
                <td>
                    <asp:CheckBox runat="server" OnChange="setDataChanged(true)" id="chkExcludeExpense" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='ExcludeExpenseFlag']"/>
                </td>
              <%--  ExpenseFlagAddition - nbhatia6 - 07/03/14--%>
            </tr>

            <tr><td>&nbsp;</td></tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td><%-- ddhiman commented 10/14/2014
                    <asp:Button ID="btnBack" Text="<%$ Resources:btnBack %>" runat="server" 
                class="button" onclick="btnBack_Click" />
                    &nbsp;<asp:Button ID="btnHistory" Text="<%$ Resources:btnHistory %>" runat="server" 
                        class="button" onclick="btnHistory_Click" />--%>
                </td>          
            </tr>
        </table>
        <asp:TextBox runat="server" id="claimid" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/ClaimId"></asp:TextBox>
        <asp:TextBox runat="server" id="CoverageGroupId" visible="false" rmxref="/Instance/Document/form/group/displaycolumn/Pol_Grp_Id"></asp:TextBox>
        <asp:TextBox runat="server" id="txtClaimId" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/ClaimId"></asp:TextBox>
        <asp:TextBox runat="server" id="txtPolicyId" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/PolicyId"></asp:TextBox>
        <asp:TextBox runat="server" id="txtEventId" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/EventId"></asp:TextBox>
        <asp:TextBox runat="server" id="txtDeductibleId" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/DedId"></asp:TextBox>
        <asp:TextBox runat="server" id="txtCovGroupUnitStateRowId" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/UnitStateRowId"></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_FunctionToCall" Style="display:none"></asp:TextBox>
        <asp:TextBox runat="server" id="BaseCurrency" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/BaseCurrencyType"></asp:TextBox>
         <asp:TextBox Style="display: none" ID="ClaimantListClaimantEID" runat="server"></asp:TextBox><%--RMA-8490--%>
    </div>
    </form>
</body>
</html>
