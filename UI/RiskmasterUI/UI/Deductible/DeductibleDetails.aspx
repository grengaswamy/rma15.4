﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="DeductibleDetails.aspx.cs" Inherits="Riskmaster.UI.UI.Deductible.DeductibleDetails" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register Assembly="MultiCurrencyCustomControl" Namespace="MultiCurrencyCustomControl" TagPrefix="mc" %>
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" tagname="CodeLookUp" tagprefix="uc" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">            { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }  </script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js">               { var i; }  </script>

    <script type="text/javascript">
        function BackToReservesGrid() {
            var btnSave = document.getElementById('btnSave');

            if (ConfirmSave()) {
                if (btnSave!=null) {
                    btnSave.click();
                }
            }
            else {
                var sClaimId = document.getElementById('ClaimId').value;
                window.location.href = '/RiskmasterUI/UI/Reserves/ReserveListingBOB.aspx?ClaimId=' + sClaimId;
            }
            return false;
        }

        function BackToDeductibleGrid() {
            var btnSave = document.getElementById('btnSave');
            if (ConfirmSave()) {
                if (btnSave != null) {
                    btnSave.click();
                    return true;
                }
            }
        }

        function IsDataChanged() {
            var oSysPageDataChanged = document.getElementById("SysPageDataChanged");
            if (oSysPageDataChanged != null && oSysPageDataChanged.value == "true") {
                return true;
            }
            else {
                return false;
            }
        }

        function UpdateDiminishingFields() {
            var dimType = document.getElementById('DiminishingTypeCode_codelookup');
            var dimPercent = document.getElementById('txtDiminishingPercentage');
            var dedAmount = document.getElementById('txtDeductibleAmt');
            var reducedAmount = document.getElementById('txtReducedAmt');
            var remainingAmount = document.getElementById('txtRemainingAmt');
            var hdnDimCode = document.getElementById('hdn_DimTypeCode');
            if (dimType.value == "") {
                if (dimPercent != null) {
                    dimPercent.value = 0.00;
                    dimPercent.readonly = 'true';
                    dimPercent.style.backgroundColor = "#F2F2F2"
                    hdnDimCode.value = 0;
                }
                if (dedAmount != null && reducedAmount != null && remainingAmount != null) {
                    reducedAmount.value = dedAmount.value;
                    remainingAmount.value = dedAmount.value;
                }
            }
            else {
                if (dimPercent != null) {
                    dimPercent.readonly = 'false';
                    dimPercent.style.backgroundColor = "#FFFFFF"
                }
            }
        }

        function AddDecimals() {
            var dimPercent = document.getElementById('txtDiminishingPercentage');
            if (dimPercent != null && dimPercent.value.indexOf('.')==-1) {
                dimPercent.value = dimPercent.value + ".00";
            }
        }

        function onDeductibleScreenLoaded() {
            var dimType = document.getElementById('DiminishingTypeCode_codelookup');
            var dimPercent = document.getElementById('txtDiminishingPercentage');
            var hdn_FunctionToCall = document.getElementById('hdn_FunctionToCall');
            var oSysPageDataChanged = document.getElementById("SysPageDataChanged");
            var oErrorControl = document.getElementById("ErrorControl1_lblError");
            if (hdn_FunctionToCall != null && (hdn_FunctionToCall.value == 'loadDiminishingData' || hdn_FunctionToCall.value == 'loadDdeductibleData')) {
                hdn_FunctionToCall.value = '';

                if (oSysPageDataChanged != null) {
                    setDataChanged(true);
                }                
            }
            else if (oErrorControl != null && oErrorControl.innerText != "") {
                if (oSysPageDataChanged != null) {
                    setDataChanged(true);
                }
            }
            else {
                parent.MDIScreenLoaded();
            }
            //ddhiman added Null Check 10/15/2014
            if (dimType != null) {
                if (dimType.value == "") {
                    if (dimPercent != null) {
                        dimPercent.value = 0.00;
                        dimPercent.readonly = 'true';
                        dimPercent.style.backgroundColor = "#F2F2F2"
                    }
                }
            }
            
            if (parent.pleaseWait != null) {
                parent.pleaseWait("stop");
            }
        }
    
    </script>
</head>
<body onload="onDeductibleScreenLoaded();" >
    <form id="frmData" runat="server">
    <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td colspan="7">
                    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:ImageButton ID="btnSave" ImageUrl="~/Images/tb_save_active.png" 
                        class="bold" ToolTip="<%$ Resources:btnSaveTip %>"
                        runat="server" OnClick="btnSave_Click" OnClientClick="return IsDataChanged()"/>
                    <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/tb_backtofinancials_active.png" class="bold" ToolTip="<%$ Resources:btnCancelTip %>"
                        runat="server" OnClientClick="return BackToReservesGrid();" />
                    <!-- Deepak Dhiman added, 10/14/2014 -->
                    <asp:ImageButton ID="btnBack" ImageUrl="~/Images/tb_backtosummary_active.png" class="bold" ToolTip="<%$ Resources:btnBack %>" runat="server" OnClientClick="return BackToDeductibleGrid();" onclick="btnBack_Click" />
                    <!-- End Deepak Dhiman added, 10/14/2014 -->
                </td>
            </tr>
            <tr>
                <td><input type="text" name="" value="deductibleDetails" id="SysFormName"  style="display:none" /></td>
                <td><input type="text" name="" value="" id="SysPageDataChanged"  style="display:none" /></td>
            </tr>
            <tr>
                <td colspan="7" class="ctrlgroup">
                    <asp:Label ID="lblDeductibleDetailsHeader" runat="server" Text="<%$ Resources:lblDeductibleDetailsHeader %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            
            <tr>
                <td align="left" class="required">
                    <asp:Label ID="lblClaim" runat="server" Text="<%$ Resources:lblClaim %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="txtClaim" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name = 'ClaimName']" ReadOnly="true" class="disabled"></asp:TextBox>
                </td>
                <td align="left" class="required">
                    <asp:Label ID="lblPolicy" runat="server" Text="<%$ Resources:lblPolicy %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="txtPolicy" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name = 'PolicyName']" ReadOnly="true" class="disabled" ></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td align="left">
                    <asp:Label ID="lblCustomerNumber" runat="server" Text="<%$ Resources:lblCustomerNumber %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " id="TextBox4" runat="server" size="35" readonly="true" class="disabled" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CustomerNum']"></asp:TextBox>
                </td>
                <td class="required">
                    <asp:Label ID="lblPolicyLob" runat="server" Text="<%$ Resources:lblPolicyLob %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " id="TextBox5" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='PolicyLOB']" readonly="true" class="disabled"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td align="left" class="required">
                    <asp:Label ID="lblUnit" runat="server" Text="<%$ Resources:lblUnit %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="txtUnit" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name = 'UnitName']" ReadOnly="true" class="disabled"></asp:TextBox>
                </td>
                <td align="left" class="required">
                    <asp:Label ID="lblCvgType" runat="server" Text="<%$ Resources:lblCvgType %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="txtCvgType" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CoverageType']" ReadOnly="true" class="disabled"></asp:TextBox>
                </td>
            </tr>

          
            
            <tr>
                <td align="left">
                    <asp:Label ID="lblDeductibleType" runat="server" Text="<%$ Resources:lblDeductibleType %>"></asp:Label>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);  " ID="DedTypeCode" CodeTable="DEDUCTIBLE_TYPE" ControlName="DedTypeCode" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='DedTypeCode']" RMXType="code" CodeFilter=""/>
                </td>
                <td align="left">
                    <asp:Label ID="lblDiminishingType" runat="server" Text="<%$ Resources:lblDiminishingType %>"></asp:Label>
                </td>
                <td>
                    <uc:CodeLookUp runat="server" OnChange="setDataChanged(true);UpdateDiminishingFields()" ID="DiminishingTypeCode" CodeTable="DIMINISHING_TYPE" 
                        ControlName="DiminishingTypeCode" RMXRef="/Instance/Document/form/group/displaycolumn/control[@name ='DiminishingTypeCode']" RMXType="code" CodeFilter=""/>
                </td>

            </tr>

            <tr>
                <td align="left">
                    <asp:Label ID="lblDedSirAmt" runat="server" Text="<%$ Resources:lblDedSirAmt %>"></asp:Label>
                </td>
                <td>
                    <mc:CurrencyTextbox OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " id="txtDeductibleAmt" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='SirDedAmt']" />
                </td>
                <td>
                    <asp:Label ID="lblDimPercent" runat="server" Text="<%$ Resources:lblDimPercent %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox onblur="numLostFocusNonNegative(this);AddDecimals()" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " id="txtDiminishingPercentage" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DiminishingPercentage']" ></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblReducedAmt" runat="server" Text="<%$ Resources:lblReducedAmt %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="txtReducedAmt" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='ReducedAmt']" ReadOnly = "true" BackColor="#F2F2F2" ></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="lblCvgEvalDate" runat="server" Text="<%$ Resources:lblCvgEvalDate %>"></asp:Label>
                </td>
                <td>
                    <div id="divCvgEvlDate" runat="server">
                        <asp:TextBox OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CoverageEvaluationDate']" id="txtCvgEvalDate" runat="server" size="30" readonly="true"  class="disabled" style="background-color: #F2F2F2;"></asp:TextBox>
                        <input type="button"  class="DateLookupControl" value="..." name="datebtn"/>
                    </div>

                </td>
            </tr>

            <tr>
                <td align="left">
                    <asp:Label ID="lblRemainingAmt" runat="server" Text="<%$ Resources:lblRemainingAmt %>"></asp:Label>
                </td>
                <td>
                    <mc:CurrencyTextbox id="txtRemainingAmt" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='RemainingAmt']" ReadOnly="true"/>
                </td>                    
                <td>
                    <asp:Label ID="lblDimEvalDate" runat="server" Text="<%$ Resources:lblDimEvalDate %>"></asp:Label>
                </td>
                <td>
                    <div id="divDimEvlDate" runat="server">
                        <asp:TextBox OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DiminishingEvaluationDate']" id="txtDimEvalDate" runat="server" size="30" readonly="true"  class="disabled" style="background-color: #F2F2F2;"></asp:TextBox>
                        <input type="button"  class="DateLookupControl" value="..." name="datebtn"/>
                    </div>
                </td>
            </tr>

            <tr>
                <td align="left">
                    <asp:Label ID="lblInsuranceLine" runat="server" Text="<%$ Resources:lblInsuranceLine %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox id="txtInsuranceLine" runat="server" size="35" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='InsuranceLine']" ReadOnly = "true" BackColor="#F2F2F2" ></asp:TextBox>
                </td>
                <td align="left">
                    <asp:Label ID="lblGroupId" runat="server" Text="<%$ Resources:lblGroupId %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox runat="server" OnChange="setDataChanged(true);" ID ="txtCovGroupId" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CovGroupId']" size="35" ReadOnly="true" class="disabled" BackColor="#F2F2F2" />
                    <%--<uc:CodeLookUp runat="server" OnChange="setDataChanged(true);  " ID="CovGroupId" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CovGroupId']" CodeTable="POL_GROUP_ID" ControlName="CovGroupId"  RMXType="code" CodeFilter=""/>--%>
                </td>
            </tr>
              <tr>
                <td align="left">
                
                      <asp:Label ID="lblExclExpensePmt" runat="server" Text="<%$ Resources:lblExclExpensePmt %>"></asp:Label>
                   <%-- <asp:Label ID="lblClaimant" runat="server" Text="<%$ Resources:lblClaimant %>"></asp:Label>--%>
                </td>
                <td>
                 <asp:CheckBox runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " id="chkExcludeExpense" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='ExcludeExpenseFlag']"/>
                   <%-- <asp:DropDownList OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " ID="cboClaimants" type="combobox" runat="server" 
                        rmxref="/Instance/Document/form/group/displaycolumn/control[@name='Claimant']/@value" 
                        ItemSetRef="/Instance/Document/form/group/displaycolumn/control[@name='Claimant']" />--%>
                </td>
                <td>
                   <%-- <asp:Label ID="lblExclExpensePmt" runat="server" Text="<%$ Resources:lblExclExpensePmt %>"></asp:Label>--%>
                </td>
                <td>
                   <%-- <asp:CheckBox runat="server" OnChange="setDataChanged(true);&#xA;&#x9;&#x9;  " id="chkExcludeExpense" rmxref="/Instance/Document/form/group/displaycolumn/control[@name='ExcludeExpenseFlag']"/>--%>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
            <td><%-- ddhiman commented 10/14/2014
                <asp:Button ID="btnBack" Text="<%$ Resources:btnBack %>" runat="server" 
            class="button" OnClientClick="BackToDeductibleGrid()" onclick="btnBack_Click" />--%></td>             
            </tr>          
        </table>
        <asp:TextBox runat="server" id="DedId" visible="false" rmxref="/Instance/Document/form/group/displaycolumn/CLM_X_POL_DED_ID"></asp:TextBox>
        <asp:TextBox runat="server" id="CvgId" visible="false" rmxref="/Instance/Document/form/group/displaycolumn/POLCVG_ROW_ID"></asp:TextBox>
        <asp:TextBox runat="server" id="ClaimId" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/ClaimId"></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_DimTypeCode" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DiminishingTypeCode']" rmxignoreset="true"></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_FunctionToCall" Style="display:none"></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_DimPercent" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DiminishingPercentage']" rmxignoreset="true"></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_CvgEvalDate" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CoverageEvaluationDate']" rmxignoreset="true"></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_DimEvalDate" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='DiminishingEvaluationDate']" rmxignoreset="true"></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_FirstParty" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/FirstParty"></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_DimTypeText" Style="display:none" ></asp:TextBox>
      <%--//jira# RMA-6135.Multicurrency--%>
          <asp:TextBox runat="server" id="BaseCurrency" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/BaseCurrencyType"></asp:TextBox>
    </div>
    <div id="ThirdPartyData" style="display:none" >
        <asp:TextBox runat="server" id="hdn_CovGroup" Style="display:none" IsDedPerEventEnabled="" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdn_CovGroup']" ></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_CovRemainingAmt" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdn_CovRemainingAmt']" ></asp:TextBox>
        <asp:TextBox runat="server" id="hdn_CovSirDedAmt" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='hdn_CovSirDedAmt']" ></asp:TextBox>
         <asp:TextBox runat="server" ID ="hdn_CovGroupCode" rmxref="/Instance/Document/form/group/displaycolumn/control[@name ='CovGroupId']" size="35" Style="display:none" />
           <asp:TextBox runat="server" id="hdn_ThirdParty" Style="display:none" rmxref="/Instance/Document/form/group/displaycolumn/ThirdParty"></asp:TextBox>
    </div>
    </form>
</body>
</html>
