﻿using System;
using System.Web.UI;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.ServiceHelpers;
using Riskmaster.UI.PolicyInterfaceService;
using Riskmaster.Common;
//jira# RMA-6135.Multicurrency
using System.Threading;
using System.Globalization;
//jira# RMA-6135.Multicurrency
namespace Riskmaster.UI.UI.Deductible
{
    public partial class DeductibleDetails : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["DedId"] !=null)
            {
                DedId.Text = Request.QueryString["DedId"];
            }
            if (Request.QueryString["CvgId"] != null)
            {
                CvgId.Text = Request.QueryString["CvgId"];
            }
            if (Request.QueryString["ClaimId"] != null)
            {
                ClaimId.Text = Request.QueryString["ClaimId"];
            }
            if (!IsPostBack)
            {
                LoadInitialData();
            }
            else
            {
                if (string.Compare(hdn_FunctionToCall.Text,"loadDiminishingData",true) == 0)
                {
                    LoadDiminishingData();
                }
                else if (string.Compare(hdn_FunctionToCall.Text, "loadDdeductibleData", true) == 0)
                {
                    LoadDeductibleData();
                }
            }
        }
        /// <summary>
        /// LoadDeductibleData
        /// </summary>
        /// <param name=""></param>
        private void LoadDeductibleData()
        {
            XmlDocument xDoc = null;
            XmlNode xRoot = null;            
            string sCWSresponse = string.Empty;

            if (string.Compare(hdn_FirstParty.Text, DedTypeCode.CodeId) == 0)
            {
                DiminishingTypeCode.CodeId = hdn_DimTypeCode.Text;
                DiminishingTypeCode.CodeText = hdn_DimTypeText.Text;
                txtDiminishingPercentage.Text = hdn_DimPercent.Text;
                txtCvgEvalDate.Text = hdn_CvgEvalDate.Text;
                txtDimEvalDate.Text = hdn_DimEvalDate.Text;
                txtCovGroupId.Text = "NA";
                txtDeductibleAmt.ReadOnly = false;
                txtDeductibleAmt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");
            }
            else
            {
                hdn_DimTypeText.Text = DiminishingTypeCode.CodeText;
                DiminishingTypeCode.CodeText = string.Empty;
                DiminishingTypeCode.CodeId = string.Empty;
                txtDiminishingPercentage.Text = string.Empty;
                txtCvgEvalDate.Text = string.Empty;
                txtDimEvalDate.Text = string.Empty;
                //PCR changes by Nikhil on 17/12/2013 for retaining Coverage group - Start
                // txtCovGroupId.Text = string.Empty;
                if (string.Compare(hdn_ThirdParty.Text, DedTypeCode.CodeId) == 0 && !String.IsNullOrEmpty(hdn_CovGroup.Text))
                {
                    txtCovGroupId.Text = hdn_CovGroupCode.Text;
                }
                else
                {
                    txtCovGroupId.Text = string.Empty;
                }
                //PCR changes by Nikhil on 17/12/2013 for retaining Coverage group - End
                if (!string.IsNullOrEmpty(hdn_CovGroup.Text))
                {
                    //Start:added by Nitin goel, For PCRs Changes.
                    //txtDeductibleAmt.ReadOnly = false;
                    //txtDeductibleAmt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                    //CallCWS("DeductibleManagementAdaptor.LoadThirdPartyData", null, out sCWSresponse, true, true);                    
                        if (hdn_CovGroup.Attributes["IsDedPerEventEnabled"] != null)
                        {
                            if (string.Compare(hdn_CovGroup.Attributes["IsDedPerEventEnabled"], "-1") == 0)
                            {
                                txtDeductibleAmt.ReadOnly = true;
                                txtDeductibleAmt.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                                CallCWS("DeductibleManagementAdaptor.LoadThirdPartyData", null, out sCWSresponse, true, true);
                            }
                            else
                            {
                                txtDeductibleAmt.ReadOnly = false;
                            }
                        }                   
                    //End: added by Nitin goel, For PCRs changes.
                   
                }
                
            }

            
        }
        /// <summary>
        /// LoadInitialData
        /// </summary>
        /// <param name=""></param>
        private void LoadInitialData()
        {
            XmlDocument xDoc = null;
            XmlNode xRoot = null;
            string sCWSresponse = string.Empty;
            bool bPageReadOnly = false;
            bool bControlReadOnly = false;
            bool bEnableAmount = false;
            XmlNode xhdn_CovGroup = null;

            //ddhiman 10/13/2014, SMS Settings for deductible fields
            bool bDedTypReadOnly = false;
            bool bDedAmtReadOnly = false;
            //End ddhiman
         

            CallCWS("DeductibleManagementAdaptor.LoadDeductiblesForID", null, out sCWSresponse, true, true);
            xDoc = new XmlDocument();
            xDoc.LoadXml(sCWSresponse);
            xRoot = xDoc.SelectSingleNode("//displaycolumn");
            //jira# RMA-6135.Multicurrency
            if ((BaseCurrency != null) && (!string.IsNullOrEmpty(BaseCurrency.Text)))
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(BaseCurrency.Text);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(BaseCurrency.Text);
            }
            //jira# RMA-6135.Multicurrency
            if (xRoot != null && xRoot.Attributes["PageReadOnly"] != null && xRoot.Attributes["ControlsReadOnly"] != null)
            {
                bPageReadOnly = (string.Compare(xRoot.Attributes["PageReadOnly"].Value, "-1") == 0);
                bControlReadOnly = (string.Compare(xRoot.Attributes["ControlsReadOnly"].Value, "0") != 0);
                bEnableAmount = (string.Compare(xRoot.Attributes["ControlsReadOnly"].Value, "2") == 0);

                //ddhiman 10/13/2014, SMS Settings for deductible fields
                if (xRoot.Attributes["DeductibleTypEdit"] != null && xRoot.Attributes["DeductibleAmtEdit"] != null)
                {
                    bDedTypReadOnly = (string.Compare(xRoot.Attributes["DeductibleTypEdit"].Value, "0") == 0);
                    bDedAmtReadOnly = (string.Compare(xRoot.Attributes["DeductibleAmtEdit"].Value, "0") == 0);
                }
                //End ddhiman
            }

            //bPageReadOnly - based on SMS setting
            //bDedTypReadOnly - based on SMS setting
            //bDedAmtReadOnly - based on SMS setting
            //bEnableAmount - If no payment - this should be true

            if (bPageReadOnly || bControlReadOnly)
            {
                txtDeductibleAmt.ReadOnly = true;
                txtDeductibleAmt.Enabled = false;
                DedTypeCode.Enabled = false;
                DiminishingTypeCode.Enabled = false;
                //cboClaimants.Enabled = false;
                txtDiminishingPercentage.ReadOnly = true;
                //txtDiminishingPercentage.Enabled = false;
                txtDiminishingPercentage.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");

                if (bPageReadOnly)
                {
                    chkExcludeExpense.Enabled = false;
                }
                else if (bEnableAmount)
                {
                    txtDeductibleAmt.ReadOnly = false;
                    txtDeductibleAmt.Enabled = true;
                    DedTypeCode.Enabled = true;
                    DiminishingTypeCode.Enabled = true;
                }
                
            }
            //ddhiman 10/13/2014, SMS Settings for deductible fields
            //if (!bPageReadOnly)
            else
            {
                if (bDedTypReadOnly)
                {
                    DedTypeCode.Enabled = false;
                    DiminishingTypeCode.Enabled = false;
                }
                else
                {
                    DedTypeCode.Enabled = true;
                    DiminishingTypeCode.Enabled = true;
                }

                if (bDedAmtReadOnly)
                {
                    txtDeductibleAmt.ReadOnly = true;
                    txtDeductibleAmt.Enabled = false;
                }
                else
                {
                    txtDeductibleAmt.ReadOnly = false;
                    txtDeductibleAmt.Enabled = true;
                }
            }
            //End ddhiman

            if (string.IsNullOrEmpty(DiminishingTypeCode.CodeText))
            {
                txtDiminishingPercentage.ReadOnly = true;
                //txtDiminishingPercentage.Enabled = false;
                txtDiminishingPercentage.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
            }

            hdn_DimTypeCode.Text = DiminishingTypeCode.CodeId;
            // Changed by Nikhil on 09/22/14.Code Review changes
           // if (DedTypeCode.CodeId != hdn_FirstParty.Text)
            if (string.Compare(DedTypeCode.CodeId,hdn_FirstParty.Text,true) != 0)
            {
                txtCvgEvalDate.Text = string.Empty;
                txtDimEvalDate.Text = string.Empty;
            }

            if (!string.IsNullOrEmpty(txtCovGroupId.Text) && string.Compare(txtCovGroupId.Text,"NA",true)!=0)
            {
                //Start: added by Nitin goel, For NI PCRs changes.
                //txtDeductibleAmt.ReadOnly = true;
                //txtDeductibleAmt.Enabled = false;
                xhdn_CovGroup = xDoc.SelectSingleNode("//control[@name='hdn_CovGroup']");
                if (xhdn_CovGroup != null)
                {
                    if (xhdn_CovGroup.Attributes["IsDedPerEventEnabled"] != null)
                    {
                        if (hdn_CovGroup != null)
                        {
                            if (hdn_CovGroup.Attributes["IsDedPerEventEnabled"] != null)
                            {
                                hdn_CovGroup.Attributes["IsDedPerEventEnabled"] = xhdn_CovGroup.Attributes["IsDedPerEventEnabled"].Value;
                            }
                        }            
                        //nikhil
                  //      if (string.Compare(xhdn_CovGroup.Attributes["IsDedPerEventEnabled"].Value, "-1") == 0)
                        //ddhiman added bDedAmtReadOnly ||bPageReadOnly to if condition : 10/14/2014
                        if ((string.Compare(xhdn_CovGroup.Attributes["IsDedPerEventEnabled"].Value, "-1") == 0) || (string.Compare(xRoot.Attributes["ControlsReadOnly"].Value, "1") == 0) || bDedAmtReadOnly ||bPageReadOnly)
                        {
                            txtDeductibleAmt.ReadOnly = true;
                            txtDeductibleAmt.Enabled = false;
                        }
                        else
                        {
                            txtDeductibleAmt.ReadOnly = false;
                            txtDeductibleAmt.Enabled = true;
                        }
                    }
                    //Start - ExpenseFlagAddition - nbhatia6 - 07/03/14
                    chkExcludeExpense.Enabled = false;
                    //End - ExpenseFlagAddition - nbhatia6 - 07/03/14
                }
                //End: added by Nitin goel, For PCRs changes.
            }
            //ddhiman added for NI : 10/15/2014
            if (xDoc.SelectSingleNode("//SHARED_AGG_DED_FLAG") != null)
            {
                if (xDoc.SelectSingleNode("//SHARED_AGG_DED_FLAG").InnerText != "-1")
                {
                    txtCovGroupId.Visible = false;
                    lblGroupId.Visible = false;
                }
                else
                {
                    txtCovGroupId.Visible = true;
                    lblGroupId.Visible = true;
                }
            }

            if (xDoc.SelectSingleNode("//DIMNISHING_FLAG") != null)
            {
                if (xDoc.SelectSingleNode("//DIMNISHING_FLAG").InnerText != "-1")
                {
                    lblDiminishingType.Visible = false;
                    DiminishingTypeCode.Visible = false;
                    lblDimPercent.Visible = false;
                    txtDiminishingPercentage.Visible = false;
                    lblCvgEvalDate.Visible = false;
                    divCvgEvlDate.Visible = false;
                    lblDimEvalDate.Visible = false;
                    divDimEvlDate.Visible = false;
                }
                else
                {
                    lblDiminishingType.Visible = true;
                    DiminishingTypeCode.Visible = true;
                    lblDimPercent.Visible = true;
                    txtDiminishingPercentage.Visible = true;
                    lblCvgEvalDate.Visible = true;
                    divCvgEvlDate.Visible = true;
                    lblDimEvalDate.Visible = true;
                    divDimEvlDate.Visible = true;
                }
            }
            //End ddhiman
        }


        /// <summary>
        /// GetMessageTemplate
        /// </summary>
        /// <param name=""></param>
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization>27551b50-b2f4-4381-a669-d514e21ce3cf</Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <Doc>
                        <ClaimId>" + ClaimId.Text + @"</ClaimId>
                        <CLM_X_POL_DED_ID>" + DedId.Text + @"</CLM_X_POL_DED_ID>
                        <control name=""DiminishingTypeCode"" codeid=""" + DiminishingTypeCode.CodeId + @""">" + DiminishingTypeCode.CodeText + @"</control>
                        <control name=""SirDedAmt"">" + txtDeductibleAmt.Text + @"</control>
                        <control name=""ReducedAmt"">" + txtDeductibleAmt.Text + @"</control>
                        <control name=""RemainingAmt"">" + txtRemainingAmt.Text + @"</control>
                        <control name=""DiminishingPercentage"">" + txtDiminishingPercentage.Text + @"</control>
                        <control name=""CoverageEvaluationDate"">" + @"</control>
                        <control name=""DiminishingEvaluationDate"">" + @"</control>
                    </Doc>
                </Document>
            </Message>
            ");

            return oTemplate;
        }
        /// <summary>
        /// Save deductible details
        /// </summary>
        /// <param name="sender"></param>
        ///  <param name="e"></param>
        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            string sCWSresponse = string.Empty;
            //CallCWS("DeductibleManagementAdaptor.LoadDeductiblesForID", null, out sCWSresponse, true, true);
            CallCWSFunction("DeductibleManagementAdaptor.SaveDeductible");
        }
        /// <summary>
        /// to redirect to deductible summary screen
        /// </summary>
        /// <param name="sender"></param>
        ///  <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            string sRedirectString = String.Format("../Deductible/DeductibleGrid.aspx");
            Server.Transfer(sRedirectString);
        }
        /// <summary>
        /// LoadDiminishingData
        /// </summary>
        private void LoadDiminishingData()
        {
            PolicyEnquiry oRequest = null;
            PolicyInterfaceServiceHelper interfaceHelper = null;
            XElement xReturnDoc = null;
            XElement xCvgEvaluationDate = null;
            XElement xDimEvalDate = null;
            XElement xDimPercent = null;
            XElement xReducedAmt = null;
            XElement xRemainingAmt = null;
            bool bSuccess = false;

            oRequest = new PolicyEnquiry();
            interfaceHelper = new PolicyInterfaceServiceHelper();

            oRequest.Token = AppHelper.GetSessionId();
            xReturnDoc = interfaceHelper.GetDimEvalDate(GetMessageTemplate(), oRequest);

            xCvgEvaluationDate = xReturnDoc.XPathSelectElement("//control[@name='CoverageEvaluationDate']");
            xDimEvalDate = xReturnDoc.XPathSelectElement("//control[@name='DiminishingEvaluationDate']");
            xDimPercent = xReturnDoc.XPathSelectElement("//control[@name='DiminishingPercentage']");
            xReducedAmt = xReturnDoc.XPathSelectElement("//control[@name='RemainingAmt']");
            xRemainingAmt = xReturnDoc.XPathSelectElement("//control[@name='ReducedAmt']");

            if (xCvgEvaluationDate!=null)
            {
                txtCvgEvalDate.Text = xCvgEvaluationDate.Value;
            }
            if (xDimEvalDate != null)
            {
                txtDimEvalDate.Text = xDimEvalDate.Value;
            }
            if (xDimPercent != null)
            {
                txtDiminishingPercentage.Text = xDimPercent.Value;
                txtDiminishingPercentage.ReadOnly = false;
                //txtDiminishingPercentage.Enabled = true;
                txtDiminishingPercentage.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");
            }
            if (xReducedAmt != null)
            {
                if (!string.IsNullOrEmpty(xReducedAmt.Value))
                {
                    if (string.Compare(xReducedAmt.Value,"NA") == 0)
                    {
                        txtReducedAmt.Text = xReducedAmt.Value;
                    }
                    else
                    {
                        txtReducedAmt.Text = string.Format("{0:c}", Conversion.CastToType<double>(xReducedAmt.Value, out bSuccess));
                    }
                }
                
            }
            if (xRemainingAmt != null)
            {
                txtRemainingAmt.Amount = Conversion.CastToType<decimal>(xRemainingAmt.Value, out bSuccess);

            }

            hdn_DimTypeCode.Text = DiminishingTypeCode.CodeId;
            
        }
    }
}