﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;
//using Riskmaster.UI.ReserveWorksheetService;//Change by kuladeep for Cloud-154
using Riskmaster.Models;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.UI.ReserveApproval
{
    public partial class trans : NonFDMBasePageCWS
    {
        public XmlDocument Model = null;
        public string sInXml = "<Message><Document><valueupper /><valuelower /><test /><RswId>-1</RswId><ReserveApproval><ShowAllItems /><CallFor /><ReserveApprovals /></ReserveApproval><AppRejReqCom></AppRejReqCom></Document></Message>";
        protected void Page_Load(object sender, EventArgs e)
        {
            
            ReserveWorksheetRequest oReserveWorkSheetRequest = null;
            ReserveWorksheetResponse objReserveWorkSheetResponse = null;
            //ReserveWorksheetServiceClient objClient = null;//Change by kuladeep for Cloud-154
            Model = new XmlDocument();
            // RMA-8253 pgupta93 START
            lblErrors.Text = string.Empty;
            if (!(string.IsNullOrEmpty(AppHelper.GetValue("Parent"))))
            {
                if (AppHelper.GetValue("Parent") == "MyWork")
                {
                    if (HttpContext.Current.Request.Browser != null && String.Compare(HttpContext.Current.Request.Browser.Browser, "IE", true) == 0
                                    && HttpContext.Current.Request.Browser.MajorVersion < 10)
                    {
                        lblErrors.Text = RMXResourceProvider.GetSpecificObject("lblBrowsercheckError", RMXResourceProvider.PageId("trans.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                        Browsercheck.Attributes.Add("style", "display:none");
                        return;
                    }
                }
            }
            // RMA-8253 pgupta93 END
            try
            {
                if (!Page.IsPostBack)
                {
                    if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet") //MGaba2:R8:SupervisoryApproval
                    {
                        hdnScreenName.Text = "Reserve WorkSheet";
                        btnPrint.Visible = true;//MGaba2:R8:SupervisoryApproval
                        XmlDocument outputXmlDoc = null;
                        oReserveWorkSheetRequest = new ReserveWorksheetRequest();
                        //objClient = new ReserveWorksheetServiceClient();//Change by kuladeep for Cloud-154


                        oReserveWorkSheetRequest.InputXmlString = sInXml;
                        oReserveWorkSheetRequest.Token = AppHelper.ReadCookieValue("SessionId");

                        //Change by kuladeep for Cloud-154-Start
                        //objReserveWorkSheetResponse = objClient.ListFundsTransactions(oReserveWorkSheetRequest);
                        oReserveWorkSheetRequest.ClientId = AppHelper.ClientId;
                        objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/ListFundsTransactions", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                        //Change by kuladeep for Cloud-154-End

                        Model.LoadXml(objReserveWorkSheetResponse.OutputXmlString);
                        //SetValuesFromService();
                        BindpageControls(objReserveWorkSheetResponse.OutputXmlString);
                    }
                    else
                    {//MGaba2:R8:SupervisoryApproval/skhare7 R8 SuperVisory approval
                        txtAppRejReqCom.Attributes.Add("rmxref", "/Instance/Document/Reserve/ApproveReasonCom");
                        //skhare7 R8 SuperVisory approval End
                        hdnScreenName.Text = "Reserve";
                        btnPrint.Visible = false; 
                        NonFDMCWSPageLoad("ReserveFundsAdaptor.GetReservesForApproval");                     
                        BindpageControls(Data.OuterXml);                        
                    }
                } // if

            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch

        }


        private void SetValuesFromService()
        {
            throw new NotImplementedException();
        }

        private string CreateServiceCall(string sMessageTemplate, string sAction)
        {
            if (sAction == "Deny" || sAction == "Approve" || sAction == "Void")
            {
                string sTransId = AppHelper.GetFormValue("hdnTransIdToUpper");
                sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//TransId", sTransId);

                return sMessageTemplate;
            } // if
            if (sAction == "DenialDeny")
            {
                string sTransId = AppHelper.GetFormValue("hdnTransIdToLower");
                sMessageTemplate = AppHelper.ChangeMessageValue(sMessageTemplate, "//TransId", sTransId);

                return sMessageTemplate;
            } // if

            return "";
        }

        private void BindpageControls(string sCWSresponse)
        {
            XmlDocument usersXDoc = new XmlDocument();
            XmlElement xmlIn = null;
            DataColumn objColumn = null;
            DataRow objRow = null;
            BoundField bfGridColumn = null;
            usersXDoc.LoadXml(sCWSresponse);
            xmlIn = (XmlElement)usersXDoc.SelectSingleNode("//ReserveApprovals/Approvals");
            if (xmlIn != null)
            {
                DataTable dtGridData = new DataTable();

                objColumn = new DataColumn();
                objColumn.ColumnName = "Id";                    
                dtGridData.Columns.Add(objColumn);

                foreach (XmlElement xmlElem in xmlIn)
                {

                    objColumn = new DataColumn();
                    objColumn.ColumnName = xmlElem.Name;

                    if(xmlElem.Name.ToUpper().Equals("DATEOFCLAIM"))
                        objColumn.Caption = "Date Of Claim";
                    else if (xmlElem.Name.ToUpper().Equals("DATEOFEVENT"))
                        objColumn.Caption = "Date Of Event";
                    else if (xmlElem.Name.ToUpper().Equals("CLAIM_NUMBER"))
                        objColumn.Caption = "Claim Number";
                    else if (xmlElem.Name.ToUpper().Equals("PRIMARYCLAIMANT"))
                        objColumn.Caption = "Primary Claimant";
                    else if (xmlElem.Name.ToUpper().Equals("CURRENTADJUSTER"))
                        objColumn.Caption = "Current Adjuster";
                    else if (xmlElem.Name.ToUpper().Equals("TOTALPAID"))
                        objColumn.Caption = "Total Paid To Date";
                    else if (xmlElem.Name.ToUpper().Equals("RESERVE_TYPE_CODE")) //tmalhotra3- RMA 6333
                        objColumn.Caption = "Reserve Type Code";
                    else if (xmlElem.Name.ToUpper().Equals("RESERVE_AMOUNT"))
                        objColumn.Caption = "Reserve Amount";
                    else if (xmlElem.Name.ToUpper().Equals("SUBMITTED_TO"))
                        objColumn.Caption = "Submitted To";
                    else if (xmlElem.Name.ToUpper().Equals("LINE_OF_BUS_CODE"))
                        objColumn.Caption = "Line Of Bus Code";  
                    else if (xmlElem.Name.ToUpper().Contains("RESERVE"))
                        objColumn.Caption = xmlElem.Name.ToUpper().Replace("RESERVE", " RESERVE");
                    else if (xmlElem.Name.ToUpper().Equals("HOLDREASON"))
                        objColumn.Caption = "Hold Reason";  //JIRA 6385 Snehal
                    else
                        objColumn.Caption = xmlElem.Name;

                    //overriding the caption for hidden columns
                    if (xmlElem.Attributes["hidden"].Value == "true")
                        objColumn.Caption = "hidden";
                    dtGridData.Columns.Add(objColumn);

                    bfGridColumn = new BoundField();
                    bfGridColumn.DataField = objColumn.ColumnName;

                    // Assign the HeaderText
                    bfGridColumn.HeaderText = objColumn.Caption;

                    // Add the newly created bound field to the GridView. 
                    gvListReserveApproval.Columns.Add(bfGridColumn);

                }

                XmlNodeList ApprovalData = usersXDoc.SelectNodes("//ReserveApprovals/Approvals");

                foreach (XmlNode objNodes in ApprovalData)
                {
                    objRow = dtGridData.NewRow();
                    objRow["ID"] = "valueupper";
                    for (int i = 0; i < objNodes.ChildNodes.Count; i++)
                    {
                        objRow[i+1] = objNodes.ChildNodes[i].InnerText;
                    }
                    dtGridData.Rows.Add(objRow);

                   
                }

                gvListReserveApproval.DataSource = dtGridData;
                gvListReserveApproval.DataBind();

            } // if
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            ReserveWorksheetRequest oReserveWorkSheetRequest = null;
            ReserveWorksheetResponse objReserveWorkSheetResponse = null;
            //ReserveWorksheetServiceClient objClient = null; //Change by kuladeep for Cloud-154
            Model = new XmlDocument();
            try
            {
                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet") //MGaba2:R8:SupervisoryApproval
                {
                    XmlDocument outputXmlDoc = null;
                    oReserveWorkSheetRequest = new ReserveWorksheetRequest();
                    //objClient = new ReserveWorksheetServiceClient();;//Change by kuladeep for Cloud-154
                    string sRswId = hdnRswId.Value;
                    string sInXml = "<Message><Document><valueupper /><valuelower /><test /><RswId>" 
                        + sRswId + "</RswId><ReserveApproval><ShowAllItems>"+chkShowAllItem.Checked.ToString()+"</ShowAllItems><CallFor /><ReserveApprovals /></ReserveApproval><AppRejReqCom>"
                        + txtAppRejReqCom.Text + "</AppRejReqCom></Document></Message>";

                    oReserveWorkSheetRequest.InputXmlString = sInXml;
                    oReserveWorkSheetRequest.Token = AppHelper.ReadCookieValue("SessionId");
                    oReserveWorkSheetRequest.ClaimId = Convert.ToInt32(claimid.Value);

                    //Change by kuladeep for Cloud-154-Start
                    oReserveWorkSheetRequest.ClientId = AppHelper.ClientId;
                    //objReserveWorkSheetResponse = objClient.ApproveReserveWorksheetFromApprovalScreen(oReserveWorkSheetRequest);
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/ApproveReserveWorksheetFromApprovalScreen", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                    //Change by kuladeep for Cloud-154-End

                    Model.LoadXml(objReserveWorkSheetResponse.OutputXmlString);
                    if (Model.SelectSingleNode("//ReserveApproval/CallFor") != null)
                    {
                        CallFor.Value = Model.SelectSingleNode("//ReserveApproval/CallFor").InnerText;
                    }
                    if (Model.SelectSingleNode("//ReserveApproval/HoldReason") != null)
                    {
                        HoldReason.Value = Model.SelectSingleNode("//ReserveApproval/HoldReason").InnerText;
                    }
                    BindpageControls(objReserveWorkSheetResponse.OutputXmlString);
                }
                else
                {
                    //MGaba2:R8:SupervisoryApproval
                    hdnApproveorRejectRsv.Text = "Approve";
                    NonFDMCWSPageLoad("ReserveFundsAdaptor.ApproveOrRejectReserve");
                    BindpageControls(Data.OuterXml);    
                }

            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ReserveWorksheetRequest oReserveWorkSheetRequest = null;
            ReserveWorksheetResponse objReserveWorkSheetResponse = null;
            //ReserveWorksheetServiceClient objClient = null;  //Change by kuladeep for Cloud-154
            Model = new XmlDocument();
            try
            {
                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet") //MGaba2:R8:SupervisoryApproval
                {
                    XmlDocument outputXmlDoc = null;
                    oReserveWorkSheetRequest = new ReserveWorksheetRequest();
                    //objClient = new ReserveWorksheetServiceClient(); //Change by kuladeep for Cloud-154
                    string sRswId = hdnRswId.Value;
                    string sInXml = "<Message><Document><valueupper /><valuelower /><test /><RswId>"
                    + sRswId + "</RswId><ReserveApproval><ShowAllItems>" + chkShowAllItem.Checked.ToString() + "</ShowAllItems><CallFor /><ReserveApprovals /></ReserveApproval><AppRejReqCom>"
                        + txtAppRejReqCom.Text + "</AppRejReqCom></Document></Message>";

                    oReserveWorkSheetRequest.InputXmlString = sInXml;
                    oReserveWorkSheetRequest.Token = AppHelper.ReadCookieValue("SessionId");
                    oReserveWorkSheetRequest.ClaimId = Convert.ToInt32(claimid.Value);

                    //Change by kuladeep for Cloud-154-Start
                    oReserveWorkSheetRequest.ClientId = AppHelper.ClientId;
                    //objReserveWorkSheetResponse = objClient.RejectReserveWorksheetFromApprovalScreen(objReserveWorkSheetRequest);
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/RejectReserveWorksheetFromApprovalScreen", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                    //Change by kuladeep for Cloud-154-End

                    Model.LoadXml(objReserveWorkSheetResponse.OutputXmlString);
                    if (Model.SelectSingleNode("//ReserveApproval/CallFor") != null)
                    {
                        CallFor.Value = Model.SelectSingleNode("//ReserveApproval/CallFor").InnerText;
                    }
                    if (Model.SelectSingleNode("//ReserveApproval/HoldReason") != null)
                    {
                        HoldReason.Value = Model.SelectSingleNode("//ReserveApproval/HoldReason").InnerText;
                    }
                    BindpageControls(objReserveWorkSheetResponse.OutputXmlString);
                }
                else
                {
                    //MGaba2:R8:SupervisoryApproval
                    hdnApproveorRejectRsv.Text = "Reject";
                    NonFDMCWSPageLoad("ReserveFundsAdaptor.ApproveOrRejectReserve");
                    BindpageControls(Data.OuterXml);
                }

            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }

        protected void gvListReserveApproval_DataBound(object sender, EventArgs e)
        {
            DataTable objDt = (DataTable)gvListReserveApproval.DataSource;
            DataColumn objColumn = null;
            if (objDt != null)
            {
                for (int i = 0; i < objDt.Columns.Count; i++ )
                {
                    objColumn = objDt.Columns[i];
                    if (objColumn.Caption.ToUpper() == "HIDDEN")
                    {
                        gvListReserveApproval.Columns[i].HeaderStyle.CssClass = "hiderowcol";
                        gvListReserveApproval.Columns[i].ItemStyle.CssClass = "hiderowcol";
                    }

                }
            }
        }

        protected void chkShowAllItem_CheckedChanged(object sender, EventArgs e)
        {
            ReserveWorksheetRequest oReserveWorkSheetRequest = null;
            ReserveWorksheetResponse objReserveWorkSheetResponse = null;
            //ReserveWorksheetServiceClient objClient = null; //Change by kuladeep for Cloud-154
            Model = new XmlDocument();
            try
            {
                XmlDocument outputXmlDoc = null;
                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet") //MGaba2:R8:SupervisoryApproval
                {
                    oReserveWorkSheetRequest = new ReserveWorksheetRequest();
                    //objClient = new ReserveWorksheetServiceClient(); //Change by kuladeep for Cloud-154
                    oReserveWorkSheetRequest.InputXmlString = sInXml;
                    oReserveWorkSheetRequest.InputXmlString = AppHelper.ChangeMessageValue(oReserveWorkSheetRequest.InputXmlString, "//ShowAllItems", chkShowAllItem.Checked.ToString());
                    oReserveWorkSheetRequest.Token = AppHelper.ReadCookieValue("SessionId");

                    //Change by kuladeep for Cloud-154-Start
                    oReserveWorkSheetRequest.ClientId = AppHelper.ClientId;
                    //objReserveWorkSheetResponse = objClient.ListFundsTransactions(objReserveWorkSheetRequest);
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/ListFundsTransactions", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                    //Change by kuladeep for Cloud-154-End

                    Model.LoadXml(objReserveWorkSheetResponse.OutputXmlString);
                    //SetValuesFromService();
                    BindpageControls(objReserveWorkSheetResponse.OutputXmlString);
                }
                else
                {
                    txtAppRejReqCom.Attributes.Add("rmxref", "/Instance/Document/Reserve/ApproveReasonCom");
                    //skhare7 R8 SuperVisory approval End
                    hdnScreenName.Text = "Reserve";
                    btnPrint.Visible = false;
                    NonFDMCWSPageLoad("ReserveFundsAdaptor.GetReservesForApproval");
                    BindpageControls(Data.OuterXml); 
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }
    }
}
