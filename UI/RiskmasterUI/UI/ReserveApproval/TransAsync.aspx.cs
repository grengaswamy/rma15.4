﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.Models;
using Riskmaster.BusinessHelpers;
using Newtonsoft.Json;
using System.Xml.Linq;
using Riskmaster.RMXResourceManager;
using System.Xml.XPath;
using Riskmaster.Common.Extensions;
using System.Dynamic;
using System.Text; // RMA-5566      achouhan3   Added for handling Multiple selection
using System.Data;
using System.Reflection; // RMA-5566      achouhan3   Added for handling Multiple selection
using Riskmaster.Common;

namespace Riskmaster.UI.UI.ReserveApproval
{
    public partial class TransAsync : NonFDMBasePageCWS
    {
        
        RMXMessageTemplateHelper oMessageTemplate = new RMXMessageTemplateHelper();
        string Const_Width = "150";
        XElement resultDoc = null;
        XmlDocument Model = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            #region javascript registration

            string NgGridToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridToolTips", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridToolTips", NgGridToolTips, true);
            string NgGridLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridLabels", NgGridLabels, true);

            string NgGridAlertMessages = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("rmANgGrid.aspx".ToUpper()), "NgGridAlertMessages", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterClientScriptBlock(this.GetType(), "NgGridAlertMessages", NgGridAlertMessages, true);

            #endregion
            string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("TransAsync.aspx"), "CreateReserveValidations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CreateReserveValidations", sValidationResources, true);
            string sLabels = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("TransAsync.aspx"), "CreateReserveLabels", ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CreateReserveLabels", sLabels, true);
            string sToolTips = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("TransAsync.aspx"), "CreateReserveToolTip", ((int)RMXResourceProvider.RessoureType.TOOLTIP).ToString());
            ClientScript.RegisterStartupScript(this.GetType(), "CreateReserveToolTip", sToolTips, true);

            hdnSelectedVersion.Value = "1";
            if (Request.IsAjaxRequest())
            {
                if (Request.QueryString["call"] != null)
                {
                    switch (Request.QueryString["call"].ToLower())
                    {
                        case "getreserve":
                            GetReserves();
                            break;
                        case "approverejectreserve":
                            ApproveRejectReserve();
                            break;
                        case "savepreferences":
                            SavePreferences();
                            break;

                    }
                }
            }
            //RMA-8253 pgupta93 START
            //spnHeader.InnerHtml = RMXResourceProvider.GetSpecificObject("lblReserveHeader", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet")
            {
                spnHeader.InnerHtml = RMXResourceProvider.GetSpecificObject("lblReserveWorkSheetHeader", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            }
            else
            {
                spnHeader.InnerHtml = RMXResourceProvider.GetSpecificObject("lblReserveHeader", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            }
            lblReason.InnerHtml = RMXResourceProvider.GetSpecificObject("lblReason", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            spnShowAllItem.InnerHtml = RMXResourceProvider.GetSpecificObject("lblShowAllItem", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
            //RMA-8253 pgupta93 END
            ReserveWorksheetRequest oReserveWorkSheetRequest = null;
            ReserveWorksheetResponse objReserveWorkSheetResponse = null;
            Model = new XmlDocument();
            if (!Page.IsPostBack)
            {// JIRA-13983-pkumari3
                BindRejectReasonCodeDDL();
                // End JIRA-13983-pkumari3
                // Added By Nitika For JIRA 8253 
                lblPendingItems.InnerHtml = RMXResourceProvider.GetSpecificObject("lblPendingItems", RMXResourceManager.RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                // Added By Nitika For JIRA 8253 
                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet") //MGaba2:R8:SupervisoryApproval
                {
                    hdnScreenName.Text = "Reserve WorkSheet";
                    hdQuery.Value = "ReserveWorkSheet";
                    btnPrint.Visible = true;//MGaba2:R8:SupervisoryApproval
                }
                if (!(string.IsNullOrEmpty(AppHelper.GetValue("Parent"))))
                {
                    //achouhan3    Modified to handle Multiselect Checkbox in case of My pending Reserve Starts
                    if (AppHelper.GetValue("Parent") == "MyWork")
                    {
                        if (!String.IsNullOrEmpty(AppHelper.GetValue("IsRedirect")) && Convert.ToBoolean(AppHelper.GetValue("IsRedirect")))
                        {
                            divapp.Visible = true;
                            divShowItems.Visible = false;
                        }
                        else
                        {
                            divapp.Visible = false;
                            divShowItems.Visible = true;
                        }
                        //achouhan3    Modified to handle Multiselect Checkbox in case of My pending Reserve Ends
                        hdnParent.Value = AppHelper.GetValue("Parent");
                        divbottom.Visible = false;
                        btnApprove.Visible = false;
                        btnReject.Visible = false;
                    }

                    else
                    {
                        divapp.Visible = true;
                        divbottom.Visible = true;
                        divShowItems.Visible = true;
                    }
                }  
            }
        }
        // JIRA-13983-pkumari3
        private void BindRejectReasonCodeDDL()
        {
            CodesBusinessHelper cb = new CodesBusinessHelper();
            CodeListType objList = cb.GetCode("", "Reason", "", "", "", "TransAsync", "", "", "", "", "", "", "", "", "", "0", "", "", "", "", "0", "", "", "", "", "", "", "", "", "", "", "", "", "","");
            if (objList != null)
            {
                DataTable dt = CommonFunctions.ConvertToDataTable(objList.Codes);
                if (dt.Rows.Count > 0)
                {
                    rejectreasoncode.DataSource = dt;
                    rejectreasoncode.DataTextField = "Desc";
                    rejectreasoncode.DataValueField = "Id";

                    rejectreasoncode.DataBind();
                    for (int i = 0; i < rejectreasoncode.Items.Count; i++)
                    {
                        if (rejectreasoncode.Items[i].Text == "")
                            rejectreasoncode.SelectedIndex = i;
                    }

                }
            }

        }

        private void SetValuesFromService()
        {
            throw new NotImplementedException();
        }

        private ServiceResponseHelper BindDataGrid(string sCWSresponse)
        {
            XmlDocument usersXDoc = new XmlDocument();
            XmlElement xmlIn = null;
            ServiceResponseHelper objResponse = null;
            GridDataHelper objGridData = new GridDataHelper();
            List<ColumnDefHelper> lstColDef = new List<ColumnDefHelper>();
            List<IDictionary<String, object>> lstResponse = new List<IDictionary<String, object>>();
            IEnumerable<XElement> xUserPrefNodeList = null;
            string colDisplayName = String.Empty;
            bool IsColVisible = true;
            string strCategory=String.Empty;
            int colIndex = 0;
            string strSortDirection = String.Empty;
            string strSortField = String.Empty;
            string strPageSize = String.Empty;
             usersXDoc.LoadXml(sCWSresponse);

             List<UserPreferences> lstUserPreference = new List<UserPreferences>();
             if (resultDoc != null)
             {
                 if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet")
                     strCategory = "ReserveWorkSheetApproval";
                 else
                     strCategory = "ReserveApproval";
                 XElement xUserPrefNode = resultDoc.XPathSelectElement("//UserPref/"+strCategory);
                 if (xUserPrefNode != null)
                 {

                     if (xUserPrefNode.Attribute("SortType") != null)
                         strSortDirection = xUserPrefNode.Attribute("SortType").Value;
                     if (xUserPrefNode.Attribute("SortColumn") != null)
                         strSortField = xUserPrefNode.Attribute("SortColumn").Value;
                     if (String.IsNullOrEmpty(strPageSize) && xUserPrefNode.Attribute("PageSize") != null)
                         strPageSize = xUserPrefNode.Attribute("PageSize").Value;
                     xUserPrefNodeList = resultDoc.XPathSelectElements("//UserPref/" + strCategory + "/Column");
                     foreach (XElement PrefNode in xUserPrefNodeList)
                     {
                        if (PrefNode.Attribute("Name") != null)
                        {
                         lstUserPreference.Add(new UserPreferences
                         {
                             Name = Convert.ToString(PrefNode.Attribute("Name").Value),
                             ColOrder = Convert.ToString(PrefNode.Attribute("ColOrder").Value) == String.Empty ? 0 : Convert.ToInt32(PrefNode.Attribute("ColOrder").Value),
                             ColWidth = Convert.ToString(PrefNode.Attribute("ColWidth").Value)
                         });
                     }
                 }
                }
                 
             }

            xmlIn = (XmlElement)usersXDoc.SelectSingleNode("//ReserveApprovals/Approvals");
            if (xmlIn != null)
            {
                foreach (XmlElement objChildElement in xmlIn)
                {   
                    switch (objChildElement.Name.ToUpper())
                    {
                        case "DATEOFCLAIM":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblClaimDate", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "DATEOFEVENT":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblEventDate", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "CLAIM_NUMBER":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblClaimNumber", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "PRIMARYCLAIMANT":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblPrimaryClaimant", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "CURRENTADJUSTER":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblCurrentAdjuster", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "TOTALPAID":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblTotalPaidToDate", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "RESERVE_TYPE_CODE":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblReserveTypeCode", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "RESERVE_AMOUNT":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblReserveAmount", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "SUBMITTED_TO":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblSubmittedTo", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "LINE_OF_BUS_CODE":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblLOBCode", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        //RMA-10196 achouhan3   Added this column in grid starts
                        case "SUBMITTEDBY":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblSubmittedBy", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "DATESUBMITTED":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblDateSubmitted", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        //RMA-10196 achouhan3   Added this column in grid ends    
						
						case "HOLDREASON":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblHoldReasonTransAsync", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        //JIRA RMA-5572 (RMA-18438) ajohari2
                        case "INCURRED_AMOUNT":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblIncurredAmountTransAsync", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "POLICY_NAME":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblPolicyNameTransAsync", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "INSURED_NAME":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblInsuredNameTransAsync", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "TRANSACTION_CLAIMANT":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblTransactionClaimantTransAsync", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        case "ORIGINAL_RESERVE_AMOUNT":
                            colDisplayName = RMXResourceProvider.GetSpecificObject("lblOriginalReserveAmountTransAsync", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString());
                            break;
                        //JIRA RMA-5572 (RMA-18438) ajohari2 END
                        default:
                            if (objChildElement.Name.ToUpper().Contains("RESERVE"))
                                colDisplayName = objChildElement.Name.ToUpper().Replace("RESERVE", " RESERVE");
                            else
                                colDisplayName = objChildElement.Name;
                            break;

                    }
                    var PrefColumn = lstUserPreference.Where(X => X.Name.ToUpper() == colDisplayName.ToUpper());
                    if (PrefColumn != null && PrefColumn.FirstOrDefault() != null)
                    {
                        Const_Width = PrefColumn.FirstOrDefault().ColWidth;
                        colIndex = PrefColumn.FirstOrDefault().ColOrder;
                    }
                    else
                    {
                        Const_Width = "150";
                        colIndex = 0;
                    }
                    //RMA-10196 achouhan3   Added this column in grid starts
                    if (objChildElement.Attributes["hidden"] != null && objChildElement.Attributes["hidden"].Value == "true" && objChildElement.Name.ToUpper() != "DATESUBMITTED" && objChildElement.Name.ToUpper() != "SUBMITTEDBY")
                        IsColVisible = false;
                    else
                        IsColVisible = true;
                    lstColDef.Add(new ColumnDefHelper
                    {
                        field = Common.Conversion.ReplaceSpecialCharacterToWords(objChildElement.Name),
                        displayName = colDisplayName,
                        visible = IsColVisible,
                        width = Const_Width,
                        Position = colIndex,
                        headerCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
                        "<div ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
                        "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
                            //"<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 24 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>"+
                            //"<a class=\"k-grid-filter\" href=\"#\" tabindex=\"-1\" ng-click=\"filterData(col)\"><span class=\"k-icon k-filter\"></span></a>"+
                        "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
                        "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>"
                        //Position = colIndex,
                        //Key = isKey
                    });
                }

                XmlNodeList xApprovalData = usersXDoc.SelectNodes("//ReserveApprovals/Approvals");

                foreach (XmlNode objNodes in xApprovalData)
                {
                    int iCount = 0;
                    dynamic dynamicExpando = new ExpandoObject();
                    var objResponseJson = dynamicExpando as IDictionary<String, object>;
                    for (int i = 0; i < objNodes.ChildNodes.Count; i++)
                    {  
                        objResponseJson[lstColDef[iCount].field] = objNodes.ChildNodes[i].InnerText.Trim();
                        iCount++;
                    }
                    lstResponse.Add(objResponseJson);
                }
                objGridData.Data = lstResponse;
                objGridData.ColumnPosition = lstColDef;
                lstColDef = lstColDef.OrderBy(X => X.Position).ToList<ColumnDefHelper>();
                objGridData.ColumnDef = lstColDef;
                objGridData.TotalCount = lstResponse.Count();
                objGridData.SortDirection = strSortDirection == "descending" ? "desc" : "asc"; ;
                objGridData.SortField = strSortField;
                if (String.IsNullOrEmpty(strPageSize.Trim()))
                    strPageSize =Convert.ToString(lstResponse.Count());

                objGridData.PageSize = Convert.ToInt32(strPageSize);
                
            }
            else
            {
                objGridData.Data = null;
                objGridData.ColumnPosition = null;
                objGridData.Data = null;
                objGridData.ColumnDef = null;
                objGridData.TotalCount = 0;
                objGridData.PageSize = 0;
            }
            objResponse = new ServiceResponseHelper(true, null, objGridData);
            return objResponse;
        }

         /// <summary>
        /// Dev Signature      : achouhan3  
        /// Date               : 12/06/2014
        /// Description        : Ajax Method to get data for reserve approval grid
        /// <returns>JSON Object</returns>
        /// </summary>
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetReserves()
        {
            ServiceResponseHelper objResponse = null;
            XDocument messageElement = null;
            XElement xTempElement = null;
            string sReturn = String.Empty;
            try
            {
                bool IsShowAllItem = false;
                bool IsShowMyTrans = false;//RMA-8253: pgupta93
                string sCWSresponse = String.Empty;
                string strSortOrder = String.Empty;
                string strSortColumn = String.Empty;
                string strCategory = String.Empty;
                Model = new XmlDocument();
                // Added By Nitika For JIRA 8253 
                if (!(string.IsNullOrEmpty(AppHelper.GetQueryStringValue("Parent"))))
                {
                    if (AppHelper.GetQueryStringValue("Parent") == "MyWork")
                    {
                        IsShowMyTrans = true;
                    }
                }
                else
                {
                    if (Request.Form["isShowMyTrans"] != null && !String.IsNullOrEmpty(Request.Form["isShowMyTrans"]))
                        IsShowMyTrans = Convert.ToBoolean(Request.Form["isShowMyTrans"]);
                }

                if (Request.Form["isShowAllItem"] != null && !String.IsNullOrEmpty(Request.Form["isShowAllItem"]))
                {
                    if (IsShowMyTrans == false)
                        IsShowAllItem = Convert.ToBoolean(Request.Form["isShowAllItem"]);
                    else IsShowAllItem = false;
                }
                // Added By Nitika For JIRA 8253 
                if (Request.Form["sortField"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["sortField"])))
                    strSortColumn = Convert.ToString(Request.Form["sortField"]);
                if (Request.Form["sortDirection"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["sortDirection"])))
                    strSortOrder = Convert.ToString(Request.Form["sortDirection"]);

                messageElement = oMessageTemplate.GetUserPrefMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return user preference xml (saved setting) for the search.

                if (sReturn != "")
                {
                    resultDoc = XElement.Parse(sReturn);

                }
                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet")
                    strCategory = "ReserveWorkSheetApproval";
                else
                    strCategory = "ReserveApproval";
                XElement xUserPrefNode = resultDoc.XPathSelectElement("//UserPref/" + strCategory);
                if (xUserPrefNode != null && String.IsNullOrEmpty(strSortColumn))
                {
                    if (xUserPrefNode.Attribute("SortType") != null)
                        strSortOrder = xUserPrefNode.Attribute("SortType").Value;
                    if (xUserPrefNode.Attribute("SortColumn") != null)
                        strSortColumn = xUserPrefNode.Attribute("SortColumn").Value;
                }

                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet")
                {  
                    ReserveWorksheetRequest oReserveWorkSheetRequest = null;
                    ReserveWorksheetResponse objReserveWorkSheetResponse = null;
                    oReserveWorkSheetRequest = new ReserveWorksheetRequest();
                    //objClient = new ReserveWorksheetServiceClient();//Change by kuladeep for Cloud-154


                    oReserveWorkSheetRequest.InputXmlString = oMessageTemplate.GetReserveWorkSheetMessageTemplate();
                    if (!(string.IsNullOrEmpty(AppHelper.GetQueryStringValue("Parent"))))
                    {
                        if (AppHelper.GetQueryStringValue("Parent") == "MyWork")
                        {
                            IsShowMyTrans = true;
                            oReserveWorkSheetRequest.InputXmlString = AppHelper.ChangeMessageValue(oReserveWorkSheetRequest.InputXmlString, "//ShowMyTrans", IsShowMyTrans.ToString());//RMA-8253 pgupta93
                        }
                    }
                    else
                        oReserveWorkSheetRequest.InputXmlString = AppHelper.ChangeMessageValue(oReserveWorkSheetRequest.InputXmlString, "//ShowMyTrans", IsShowMyTrans.ToString());//RMA-8253 pgupta93
                    if (IsShowMyTrans == false)
                        oReserveWorkSheetRequest.InputXmlString = AppHelper.ChangeMessageValue(oReserveWorkSheetRequest.InputXmlString, "//ShowAllItems", IsShowAllItem.ToString());
                    else
                    {
                        IsShowAllItem = false;
                        oReserveWorkSheetRequest.InputXmlString = AppHelper.ChangeMessageValue(oReserveWorkSheetRequest.InputXmlString, "//ShowAllItems", IsShowAllItem.ToString());
                    }
                    oReserveWorkSheetRequest.InputXmlString = AppHelper.ChangeMessageValue(oReserveWorkSheetRequest.InputXmlString, "//SortOrder", strSortOrder);
                    oReserveWorkSheetRequest.InputXmlString = AppHelper.ChangeMessageValue(oReserveWorkSheetRequest.InputXmlString, "//SortColumn", strSortColumn);
                    oReserveWorkSheetRequest.Token = AppHelper.ReadCookieValue("SessionId");

                    //Change by kuladeep for Cloud-154-Start
                    //objReserveWorkSheetResponse = objClient.ListFundsTransactions(oReserveWorkSheetRequest);
                    oReserveWorkSheetRequest.ClientId = AppHelper.ClientId;
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/ListFundsTransactions", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                    //Change by kuladeep for Cloud-154-End

                    Model.LoadXml(objReserveWorkSheetResponse.OutputXmlString);
                    objResponse = BindDataGrid(objReserveWorkSheetResponse.OutputXmlString);
                    //SetValuesFromService();
                }
                else
                {
                    XElement xUserSearchNode = resultDoc.XPathSelectElement("//UserPref/ReserveWorkSheetApproval");
                    XElement xMessageTemplate = XElement.Parse(oMessageTemplate.GetReserveApprovalMessageTempplate());

                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/ShowAllItem");
                    xTempElement.Value = Convert.ToString(IsShowAllItem);
                    // Added By Nitika For JIRA 8253 
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/ShowMyTrans");
                    xTempElement.Value = Convert.ToString(IsShowMyTrans);
                    // Added By Nitika For JIRA 8253 
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/SortOrder");
                    xTempElement.Value = strSortOrder;
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/SortColumn");
                    xTempElement.Value = strSortColumn.Trim();

                    CallCWS("ReserveFundsAdaptor.GetReservesForApproval", xMessageTemplate, out sCWSresponse, false, false);
                    objResponse = BindDataGrid(Data.OuterXml);
                }
                
            }
            catch (Exception ex)
            {
                objResponse = new ServiceResponseHelper(false, ex.Message, null);
            }
            var json = JsonConvert.SerializeObject(objResponse);
            Response.Write(json);
            try
            {
                Response.Flush();
                Response.End();
            }
            catch (System.Threading.ThreadAbortException threadEx) { }
            finally
            {
                objResponse = null;
                messageElement = null;
                xTempElement = null;
            }
        }

        /// <summary>
        /// Dev Signature      : achouhan3  
        /// Date               : 12/06/2014
        /// Description        : Ajax Method to get data for reserve approval grid
        /// <returns>JSON Object</returns>
        /// </summary>
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ApproveRejectReserve()
        {
            ServiceResponseHelper objResponse = null;
            XElement xMessageTemplate = null;
            XDocument messageElement = null;
            XElement xResponse = null;
            XElement xTempElement = null;
            try
            {
                bool IsShowAllItem = false;
                string strApproveComment = String.Empty;
                string strReserveID = String.Empty;
                string sCWSresponse = String.Empty;
                int ClaimID = 0;
                int iOperationType = 0;
                string strOperation = String.Empty;
                string strSortOrder = String.Empty;
                string strSortColumn = String.Empty;
                string strCategory = String.Empty;
                string sReturn = String.Empty;
                string strClaimID = String.Empty;
                Model = new XmlDocument();
                // RMA-5566      achouhan3   Added for handling Multiple selection Starts
                IEnumerable<XElement> XErrorNodeList = null;
                StringBuilder strError = new StringBuilder();
                int iTotal = 1;
                int iError = 0;
                string[] arrCall = null;
                // RMA-5566      achouhan3   Added for handling Multiple selection Ends

                if (Request.Form["isShowAllItem"] != null && !String.IsNullOrEmpty(Request.Form["isShowAllItem"]))
                    IsShowAllItem = Convert.ToBoolean(Request.Form["isShowAllItem"]);
                if (Request.Form["approveComment"] != null && !String.IsNullOrEmpty(Request.Form["approveComment"]))
                    strApproveComment = Convert.ToString(Request.Form["approveComment"]);
                if (Request.Form["resvHistRowID"] != null && !String.IsNullOrEmpty(Request.Form["resvHistRowID"]))
                    strReserveID = Convert.ToString(Request.Form["resvHistRowID"]).TrimEnd(',');// RMA-5566      achouhan3   Updated for handling Multiple selection
                if (Request.Form["operationType"] != null && !String.IsNullOrEmpty(Request.Form["operationType"]))
                    int.TryParse(Convert.ToString(Request.Form["operationType"]),out iOperationType);
                if (Request.Form["ClaimID"] != null && !String.IsNullOrEmpty(Request.Form["ClaimID"]))
                    strClaimID = Request.Form["ClaimID"].TrimEnd(',');
                if (Request.Form["sortField"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["sortField"])))
                    strSortColumn = Convert.ToString(Request.Form["sortField"]);
                if (Request.Form["sortDirection"] != null && !String.IsNullOrEmpty(Convert.ToString(Request.Form["sortDirection"])))
                    strSortOrder = Convert.ToString(Request.Form["sortDirection"]);

                if (strReserveID.Split(',').Length > 1)
                    iTotal = strReserveID.Split(',').Length;

                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet")
                    strCategory = "ReserveWorkSheetApproval";
                else
                    strCategory = "ReserveApproval";
                messageElement = oMessageTemplate.GetUserPrefMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return user preference xml (saved setting) for the search.

                if (sReturn != "")
                {
                    resultDoc = XElement.Parse(sReturn);

                }
                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet")
                    strCategory = "ReserveWorkSheetApproval";
                else
                    strCategory = "ReserveApproval";
                XElement xUserPrefNode = resultDoc.XPathSelectElement("//UserPref/" + strCategory);
                if (xUserPrefNode != null && String.IsNullOrEmpty(strSortColumn))
                {
                    if (xUserPrefNode.Attribute("SortType") != null)
                        strSortOrder = xUserPrefNode.Attribute("SortType").Value;
                    if (xUserPrefNode.Attribute("SortColumn") != null)
                        strSortColumn = xUserPrefNode.Attribute("SortColumn").Value;
                }

                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet")
                {
                    if (iOperationType == 1)
                        strOperation = "RMService/RSW/ApproveReserveWorksheetFromApprovalScreen";
                    else
                        strOperation = "RMService/RSW/RejectReserveWorksheetFromApprovalScreen";
                    string[] arrClaimID = strClaimID.Split(',');
                    if (arrClaimID.Length > 1)
                        int.TryParse(arrClaimID[arrClaimID.Length - 1], out ClaimID);
                    else
                        int.TryParse(strClaimID, out ClaimID);
                    ReserveWorksheetRequest oReserveWorkSheetRequest = new ReserveWorksheetRequest();
                    ReserveWorksheetResponse objReserveWorkSheetResponse = null;
                    xMessageTemplate = XElement.Parse(oMessageTemplate.GetReserveWorkSheetMessageTemplate());
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/ReserveApproval/ShowAllItems");
                    xTempElement.Value = Convert.ToString(IsShowAllItem);
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/SortOrder");
                    xTempElement.Value = strSortOrder;
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/SortColumn");
                    xTempElement.Value = strSortColumn;

                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/AppRejReqCom");
                    xTempElement.Value = strApproveComment;

                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/RswId");
                    xTempElement.Value = strReserveID;

                    oReserveWorkSheetRequest.InputXmlString = Convert.ToString(xMessageTemplate);
                    oReserveWorkSheetRequest.Token = AppHelper.ReadCookieValue("SessionId");
                    oReserveWorkSheetRequest.ClaimId = ClaimID;

                    //Change by kuladeep for Cloud-154-Start
                    oReserveWorkSheetRequest.ClientId = AppHelper.ClientId;
                    //objReserveWorkSheetResponse = objClient.ApproveReserveWorksheetFromApprovalScreen(oReserveWorkSheetRequest);
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>(strOperation, AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                    //Change by kuladeep for Cloud-154-End

                    Model.LoadXml(objReserveWorkSheetResponse.OutputXmlString);
                    if (Model.SelectSingleNode("//ReserveApproval/CallFor") != null)
                    {
                        CallFor.Value = Model.SelectSingleNode("//ReserveApproval/CallFor").InnerText;
                    }
                    if (!String.IsNullOrEmpty(CallFor.Value))
                    {
                        strError.Append(CallFor.Value.Replace("\r\n", ""));
                        //arrCall = CallFor.Value.Split(',');
                        //if (arrCall.Length > 1)
                        //{
                        //    foreach (string sCallval in arrCall)
                        //    {
                        //        if (sCallval.Trim().ToUpper()=="NO")
                        //        {
                        //            //strError.AppendLine(RMXResourceProvider.GetSpecificObject("ValidReserveAuthority", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString()));
                        //            //strError.Append("<br/>");
                        //            iError++;
                        //        }
                        //    }
                        //}
                        //else if (CallFor.Value.Trim().ToUpper() == "NO")
                        //    strError.AppendLine(RMXResourceProvider.GetSpecificObject("ValidReserveAuthority", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString()));

                    }
                    //if (!String.IsNullOrEmpty(strError.ToString()))
                    //{
                    //    strError.Insert(0, String.Format(RMXResourceProvider.GetSpecificObject("lblReserveProcessed", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), arrCall.Length, iTotal) + "<br/>");
                    //}
                    objResponse = BindDataGrid(objReserveWorkSheetResponse.OutputXmlString);
                    objResponse.Description = strError.ToString();
                }
                else
                {
                    if (iOperationType == 1)
                        strOperation = "Approve";
                    else
                        strOperation = "Reject";
                    xMessageTemplate = XElement.Parse(oMessageTemplate.GetReserveApprovalMessageTempplate());
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/ShowAllItem");
                    xTempElement.Value = Convert.ToString(IsShowAllItem);
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/SortOrder");
                    xTempElement.Value = strSortOrder;
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/SortColumn");
                    xTempElement.Value = strSortColumn.Trim();

                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/ApproveReasonCom");
                    xTempElement.Value = strApproveComment;

                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/ResHistRowId");
                    if (xTempElement == null)
                    {
                        xMessageTemplate.XPathSelectElement("./Document/Reserve").Add(new XElement("ResHistRowId", strReserveID));
                    }
                    else
                        xTempElement.Value = strReserveID;
                    xTempElement = xMessageTemplate.XPathSelectElement("./Document/Reserve/ApproveorRejectRsv");
                    xTempElement.Value = strOperation;

                    CallCWS("ReserveFundsAdaptor.ApproveOrRejectReserve", xMessageTemplate, out sCWSresponse, false, false);
                    //RMA-7537  achouhan3  Handled to show error message Starts
                    xResponse = XElement.Parse(sCWSresponse);

                    // RMA-5566      achouhan3   Updated for handling Multiple selection
                    XErrorNodeList = xResponse.XPathSelectElements("./MsgStatus/ExtendedStatus");
                    foreach (XElement oElement in XErrorNodeList)
                    {

                        if (oElement.XPathSelectElement("ExtendedStatusDesc") != null)
                        {
                            //RMA-10217 achouhan3       Added for multple error message display
                            strError.AppendLine(oElement.XPathSelectElement("ExtendedStatusDesc").Value.Trim().Replace("\r\n", ""));
                            strError.Append(",");
                            iError++;
                        }
                    }
                    objResponse = BindDataGrid(Data.OuterXml);
                    if (!String.IsNullOrEmpty(strError.ToString()))
                    {
                        //strError.Insert(0, String.Format(RMXResourceProvider.GetSpecificObject("lblReserveProcessed", RMXResourceProvider.PageId("TransAsync.aspx"), ((int)RMXResourceProvider.RessoureType.LABEL).ToString()), iError, iTotal) + "<br/>");
                        throw new Exception(strError.ToString().TrimEnd(','));
                    }
                    //RMA-7537  achouhan3  Handled to show error message Starts
                    //else
                    //    objResponse = BindDataGrid(Data.OuterXml);
                    // RMA-5566      achouhan3   Updated for handling Multiple selection
                }
            }
            catch (Exception ex)
            {
                // RMA-5566      achouhan3   Updated for handling Multiple selection
                if (objResponse == null)
                    objResponse = new ServiceResponseHelper(false, ex.Message.Replace("\r\n", ""), null);
                objResponse.Success = false;
                objResponse.Description = ex.Message.Replace("\r\n", "");
                //objResponse = new ServiceResponseHelper(false, ex.Message, null);
                // RMA-5566      achouhan3   Updated for handling Multiple selection
            }
            var json = JsonConvert.SerializeObject(objResponse);
            Response.Write(json);
            try
            {
                Response.Flush();
                Response.End();
            }
            catch (System.Threading.ThreadAbortException threadEx) { }
            finally
            {
                objResponse = null;
                xMessageTemplate = null;
                messageElement = null;
                xResponse = null;
                xTempElement = null;
            }
        }

        /// <summary>
        /// Dev Signature      : achouhan3  
        /// Date               : 01/06/2014
        /// Description        : Ajax Method to get save user preference in XML
        /// <returns>JSON Object</returns>
        /// </summary>
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SavePreferences()
        {
            ServiceResponseHelper objResponse = null;
            XDocument messageElement = null;
            XmlElement objUserPrefElement = null;
            XElement objXmlCriteria = null;
            XElement xmlIn;
            try
            {
                string strCategory = String.Empty;
                string strPageSize = String.Empty;
                string strSortDireection = String.Empty;
                string strSortFieldIndex = String.Empty;
                string strSortField = String.Empty;
                if (Request.Form["grdPageSize"] != null && !String.IsNullOrEmpty(Request.Form["grdPageSize"]))
                    strPageSize = Request.Form["grdPageSize"];
                if (Request.Form["sortDirection"] != null && !String.IsNullOrEmpty(Request.Form["sortDirection"]))
                    strSortDireection = Convert.ToString(Request.Form["sortDirection"]);
                if (Request.Form["sortField"] != null && !String.IsNullOrEmpty(Request.Form["sortField"]))
                    strSortField = Request.Form["sortField"];
                if (Request.Form["sortFieldIndex"] != null && !String.IsNullOrEmpty(Request.Form["sortFieldIndex"]))
                    strSortFieldIndex = Request.Form["sortFieldIndex"];
                System.Web.Script.Serialization.JavaScriptSerializer reqJson = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<UserPreferences> gridColumn = new List<UserPreferences>();
                List<object> gridSortColumn = new List<object>();
                int iCount = 1;
                XElement objTempElement = null;
                XDocument xmlUserPref = null;

                if (AppHelper.GetQueryStringValue("Title") == "ReserveWorkSheet")
                    strCategory = "ReserveWorkSheetApproval";
                else
                    strCategory = "ReserveApproval";

                objTempElement = new XElement(strCategory);
                objTempElement.SetAttributeValue("PageSize", strPageSize);
                objTempElement.SetAttributeValue("SortType", strSortDireection);
                objTempElement.SetAttributeValue("SortColumn", strSortField);

                xmlUserPref = new XDocument(objTempElement);
                if (Request.Form["columnObject"] != null && !String.IsNullOrEmpty(Request.Form["columnObject"]))
                    gridColumn = reqJson.Deserialize<List<UserPreferences>>(Request.Form["columnObject"]);

                foreach (var objColumn in gridColumn)
                {
                    objTempElement = XElement.Parse("<Column/>");
                    objTempElement.SetAttributeValue("Name", objColumn.colDef.displayName);
                    if (!String.IsNullOrEmpty(objColumn.colDef.cellTemplate))
                    {
                        objTempElement.SetAttributeValue("Key", "True");
                    }
                    objTempElement.SetAttributeValue("ColOrder", iCount);
                    objTempElement.SetAttributeValue("ColWidth", objColumn.width);
                    xmlUserPref.Element(strCategory).Add(objTempElement);
                    iCount++;
                }

                XElement tempElement = null;
                string sReturn = String.Empty;

                messageElement =oMessageTemplate.SetUserPrefMessageTemplate(xmlUserPref.ToString());

                sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return bool.

                //Get User Pref XML to store and communicate later on
                messageElement = oMessageTemplate.GetUserPrefMessageTemplate();
                sReturn = AppHelper.CallCWSService(messageElement.ToString()); //return user preference xml (saved setting) for the search.

                if (sReturn != "")
                {
                    resultDoc = XElement.Parse(sReturn);

                }
                Dictionary<string, string> objUserPrefDict = new Dictionary<string, string>();
                string strUserPref = Server.UrlEncode(Server.HtmlEncode(resultDoc.ToString()));
                objUserPrefDict.Add("userPrefXML", strUserPref);
                objResponse = new ServiceResponseHelper(true, null, objUserPrefDict);
            }

            catch (Exception ex)
            {
                objResponse = new ServiceResponseHelper(false, ex.Message, null);
            }
            var json = JsonConvert.SerializeObject(objResponse);
            Response.Write(json);
            try
            {
                Response.Flush();
                Response.End();
            }
            catch (System.Threading.ThreadAbortException threadex) { }
            finally
            {
                objResponse = null;
                messageElement = null;
                objUserPrefElement = null;
                objXmlCriteria = null;
                xmlIn = null;
            }
        }
    }
}