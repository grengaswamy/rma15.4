﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LookUpClaimPolicy.aspx.cs" Inherits="Riskmaster.UI.ClaimLookups.LookUpClaimPolicy" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Policies</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />

    <script language="JavaScript" src="../../Scripts/form.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function handleUnload()
	{
		if(window.opener!=null)
			window.opener.onCodeClose();
}
// asingh263 Added for MITS 31673 Starts
function selectPolicy(lPolicyId, sPolicyName, sPolicyEffectivedate, sPolicyExpirationDate) {
        if(window.opener!=null) {
		    if (document.forms[0].AttachMultiplePolicy.value == "False") {
		        window.opener.SetClaimPolicy(lPolicyId, sPolicyName, null, null, sPolicyEffectivedate, sPolicyExpirationDate); // asingh263 Added for MITS 31673 
		    }
		    else {
		        window.opener.SetClaimPolicyList(lPolicyId, sPolicyName);
		    }
		    //Sumit Kumar. 01/04/10. MITS#19313 - Start
		    UnloadPropertyInfo();
		    //Sumit Kumar. 01/04/10. MITS#19313 - End
		}
	}
						
	function handleOnload() {
		window.opener.setFieldName();
	}	
	
	//Sumit Kumar. 01/04/10. MITS#19313 - Start
	function UnloadPropertyInfo()
	{
		if(window.opener!=null)
		{
		    //smahajan6 - 02/05/2010 MITS 18230 - Start
		    //Added Additional Criteria - Unselect Property , when ispolicyfilter flag is also true
			if(window.opener.document.forms[0].SysFormName != null && window.opener.document.forms[0].ispolicyfilter != null)
			{
			    if (window.opener.document.forms[0].SysFormName.value == "claimpc" && window.opener.document.forms[0].ispolicyfilter.value == "-1") 
                {
                    // npadhy Commented this Stuff. We do not need to pass any paramter from this function as all the values getting set is blank or 0
			        //smahajan6 - 02/05/2010 MITS 18230 - End
			//Neha Suresh Jain, 06/10/2010, arrayelement '' added for country
//			        var arrHiddenFields = new Array('','','','',0,0,'','',0,'','',0,'',0,'',''
//			                                        ,0,0,0,'','','','','','',0,'','',0,'',''
//			                                        ,0,'','',0,'','',0,'','',0,'','',0,'',''
//			                                        ,0,'','',0,'','',0,'','',0,'','',0,'',''
//			                                        ,'','','','');
//			        UnSelectProperty(0,'','','',arrHiddenFields);
                    UnSelectProperty();
			    }
			}
		}
	}
    //Sumit Kumar. 01/04/10. MITS#19313 - End


	function NoPolicy()
	{
		if(window.opener!=null) {
		        if (document.forms[0].AttachMultiplePolicy.value == "False") {
		            window.opener.SetClaimPolicy(0, '');
		        }
		        
		   
		//	window.opener.SetClaimPolicy(0,'');
			//Sumit Kumar. 01/04/10. MITS#19313 - Start
			UnloadPropertyInfo();
			//Sumit Kumar. 01/04/10. MITS#19313 - End
		}
			
    }
    function LookupComments(sId) {      //csingh7 for MITS 20092

    var sLink = '../Comments/MainPage.aspx?SysFormName=LookUpClaimPolicy&recordid=' + sId + '&CommentsFlag=true';

    window.open(sLink, 'qdWnd',
             'width=720,height=450' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 720) / 2 + ',resizable=yes,scrollbars=yes');
    return false;
    }
	</script>
</head>
<body onload="handleOnload();" onunload="handleOnload();">
    <form id="frmData" runat="server" >
        <table>
        <tr>
                <asp:HiddenField runat="server" ID="AttachMultiplePolicy" />
            </tr>
            <tr>
                <td>
                   <%-- <asp:gridview ID="gvPolicyList" runat="server" 
                        onrowdatabound="gvPolicyList_RowDataBound" AutoGenerateColumns="False" 
                        ondatabound="gvPolicyList_DataBound" CssClass="singleborder" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:gridview>--%>
                     <telerik:RadScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" />
                    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
                    <telerik:RadCodeBlock runat="server" ID="radCodeBlock">
                        <script type="text/javascript">
            </script>
                    </telerik:RadCodeBlock>
                  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="gvPolicyList">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="gvPolicyList" />
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                        </AjaxSettings>
                    </telerik:RadAjaxManager>
                   <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server">
                        <telerik:RadGrid runat="server" ID="gvPolicyList" AutoGenerateColumns="False" OnNeedDataSource="gvPolicyList_NeedDataSource"
                            PageSize="10" OnItemDataBound="gvPolicyList_ItemDataBound"  AllowSorting="True" AllowPaging="True"
                            PagerStyle-FirstPageToolTip="<%$ Resources:btnFirstPage %>" PagerStyle-PrevPageToolTip="<%$ Resources:btnPreviousPage %>" 
                            PagerStyle-NextPageToolTip="<%$ Resources:btnNextPage %>" PagerStyle-LastPageToolTip="<%$ Resources:btnLastPage %>"  
                            AllowFilteringByColumn="True" GridLines="None" OnPageIndexChanged="gvPolicyList_PageIndexChanged"  SortingSettings-SortToolTip="<%$ Resources:imgbtnFilterAltText %>"
                            Skin="Office2007" OnPreRender="gvPolicyList_PreRender"  AllowCustomPaging = "true" Height="100%" Width="100%">
                        
                            <MasterTableView Width="97.9%" >
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <Columns>
                                      
                                
                                    <telerik:GridTemplateColumn AllowFiltering="false" ItemStyle-Width="1px"  HeaderText="<%$ Resources:gvHdrPolicyId %>" UniqueName="Policy Id" FilterImageToolTip="<%$ Resources:imgbtnFilter %>" Visible ="false" DataField="Policy Id">
                                        <ItemTemplate>
                                        <asp:Label ID="lblPolicyId" CssClass="data" runat="server" Text="" Width="5"></asp:Label> 
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>                                   
                                 <telerik:GridTemplateColumn ItemStyle-CssClass="data" ItemStyle-Width="10px" FilterControlWidth ="35" DataField="Policy Number" AllowFiltering="true" FilterImageToolTip="<%$ Resources:imgbtnFilter %>" HeaderText="<%$ Resources:gvHdrPolicyNumber %>"  SortExpression="Policy Number"  >
                                    <ItemTemplate>
                                        <asp:Label ID="lblPolicyNumber" CssClass="data" runat="server" Text=""></asp:Label> <%--aravi5 RMA-12186--%>
                             
                                    </ItemTemplate>                                     
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="<%$ Resources:gvHdrPolicyName %>" FilterControlWidth ="38" AllowFiltering="true" DataField="Policy Name" FilterImageToolTip="<%$ Resources:imgbtnFilter %>"  SortExpression="Policy Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblPolicyName"  runat="server"  ForeColor="#330099" Text=""></asp:LinkButton>                                        
                                    </ItemTemplate> 
                                </telerik:GridTemplateColumn>                               
                                <telerik:GridTemplateColumn  HeaderText="<%$ Resources:gvHdrInsurer %>" ItemStyle-Width="10px"  FilterControlWidth ="28" AllowFiltering="true" FilterImageToolTip="<%$ Resources:imgbtnFilter %>"  DataField="Insurer" SortExpression="Insurer">
                                   <ItemTemplate>
                                        <asp:Label ID="lblInsurer" runat="server" Text=""></asp:Label> <%--aravi5 RMA-12186--%>
                                    </ItemTemplate> 
                                </telerik:GridTemplateColumn>                                
                                <telerik:GridTemplateColumn HeaderText="<%$ Resources:gvHdrEffectiveDate %>"  ItemStyle-Width="10px" FilterControlWidth ="35" AllowFiltering="true" FilterImageToolTip="<%$ Resources:imgbtnFilter %>"  DataField="Effective Date" SortExpression="Effective Date">
                                    <ItemTemplate >
                                            <asp:Label ID="lblEffDate" runat="server" Text="" Width="5"></asp:Label>
                                    </ItemTemplate> 
                                </telerik:GridTemplateColumn>
                                 <telerik:GridTemplateColumn HeaderText="<%$ Resources:gvHdrExpirationDate %>"  ItemStyle-Width="10px" FilterControlWidth ="35" AllowFiltering="true" FilterImageToolTip="<%$ Resources:imgbtnFilter %>"  DataField="Expiration Date" SortExpression="Expiration Date">
                                    <ItemTemplate >
                                            <asp:Label ID="lblExpDate" runat="server" Text="" Width="5"></asp:Label>
                                    </ItemTemplate> 
                                </telerik:GridTemplateColumn>                           
                                <telerik:GridTemplateColumn  HeaderText="<%$ Resources:gvHdrLimitOccur %>" ItemStyle-Width="10px"  FilterControlWidth ="35" AllowFiltering="true" FilterImageToolTip="<%$ Resources:imgbtnFilter %>"  DataField="Limit/Occur." SortExpression="Limit/Occur.">
                                    <ItemTemplate >                                       
                                        <asp:Label ID="lblLimit"  runat="server" Text="" Width="5" ></asp:Label>
                                    </ItemTemplate> 
                                </telerik:GridTemplateColumn>                           
                                <telerik:GridTemplateColumn HeaderText="<%$ Resources:gvHdrApplied %>" ItemStyle-Width="10px" FilterControlWidth ="30" AllowFiltering="true" FilterImageToolTip="<%$ Resources:imgbtnFilter %>" DataField="Applied %"  SortExpression = "Applied %">
                                    <ItemTemplate >                                        
                                        <asp:Label ID="lblApplied" runat="server" Text="" Width="5"></asp:Label>
                                    </ItemTemplate> 
                                </telerik:GridTemplateColumn>                         
                                <telerik:GridTemplateColumn HeaderText="<%$ Resources:gvHdrComments %>" ItemStyle-Width="5px" FilterControlWidth ="30" AllowFiltering="true" FilterImageToolTip="<%$ Resources:imgbtnFilter %>"  DataField="Comments" SortExpression ="Comments">
                                    <ItemTemplate >
                                        <asp:LinkButton ID="lblComments" runat="server"  Width="5" ></asp:LinkButton>
                                    </ItemTemplate> 
                                </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="<%$ Resources:gvHdrPolicySystem %>" ItemStyle-Width="5px" FilterControlWidth ="30" AllowFiltering="true" FilterImageToolTip="<%$ Resources:imgbtnFilter %>" DataField="PolicySystem" SortExpression ="PolicySystem">
                                    <ItemTemplate >
                                        <asp:Label ID="lblPolicySystem" runat="server"  Width="5" ></asp:Label>
                                    </ItemTemplate> 
                                </telerik:GridTemplateColumn>
                                 
                                </Columns>
                                <ItemStyle CssClass="datatd1" />
                                <AlternatingItemStyle CssClass="datatd" />
                                <HeaderStyle CssClass="msgheader" />
                            </MasterTableView>
                          <%--  <HeaderStyle CssClass="msgheader" />--%>
                            <PagerStyle  Mode="NextPrevAndNumeric" Position="Top"  AlwaysVisible ="true" />
                            <FilterMenu EnableTheming="True">
                                <CollapseAnimation Duration="200" Type="OutQuint" />
                            </FilterMenu>
                        </telerik:RadGrid>
       </telerik:RadAjaxPanel>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <asp:button ID="btnCancel" runat="server" text="<%$ Resources:btnCancel %>" CssClass="button" OnClientClick="window.close();return false;" />
                    <asp:button ID="btnNoPolicy" runat="server" text="<%$ Resources:btnNoPolicy %>" CssClass="button" OnClientClick="NoPolicy();return false;" />
                </td>
            </tr>


        </table>
    </form>
</body>

</html>
