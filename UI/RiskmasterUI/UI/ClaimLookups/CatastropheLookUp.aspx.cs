﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using Riskmaster.BusinessAdaptor;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.UI.ClaimLookups
{
    public partial class CatastropheLookUp : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {           
            BindToGridView();
        }
        private void BindToGridView()
        {
            DataSet dsCatData = null;
            dsCatData = GetCatastropheData();

            try
            {
                if (dsCatData != null)
                {
                    if (dsCatData.Tables.Count >= 1)
                    {
                        grdCatastropheAttach.DataSource = dsCatData.Tables[0];
                        grdCatastropheAttach.DataBind();
                    }
                    else
                    {
                        btnSubmit.Enabled = false;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private DataSet GetCatastropheData()
        {
            string strCatastrophes = string.Empty;
            XmlDocument xmlCatastropheDoc = new XmlDocument();
            XmlNode inputDocNode = null;
            XmlNode xmlNode = null;
            string serviceMethodToCall = string.Empty;
            DataSet dSet = null;
            string sCodeId = string.Empty;
            string sEventDate = string.Empty;
            string sTableName = string.Empty;
            string sCatState = string.Empty;//dvatsa JIRA RMA-4495
            try
            {
                inputDocNode = InputForCWS();                
                sCodeId = AppHelper.GetQueryStringValue("lookupdata");
                sEventDate = AppHelper.GetQueryStringValue("dateofevent");
                sTableName = AppHelper.GetQueryStringValue("tablename");
                sCatState = AppHelper.GetQueryStringValue("juriscode");//dvatsa-JIRA RMA -4495
                if (inputDocNode.SelectSingleNode("//EventDate") != null)
                {
                    inputDocNode.SelectSingleNode("//EventDate").InnerText = sEventDate;
                }
                if (inputDocNode.SelectSingleNode("//CatCodeId") != null)
                {
                    inputDocNode.SelectSingleNode("//CatCodeId").InnerText = sCodeId;
                }
                if (inputDocNode.SelectSingleNode("//TableName") != null)
                {
                    inputDocNode.SelectSingleNode("//TableName").InnerText = sTableName;
                }
                //dvatsa JIRA RMA-4495 (start)
                if (inputDocNode.SelectSingleNode("//CatState") != null)
                {
                    inputDocNode.SelectSingleNode("//CatState").InnerText = sCatState;
                }
                //dvatsa JIRA RMA-4495(end)
                strCatastrophes = AppHelper.CallCWSService(inputDocNode.InnerXml);

                ErrorControl.errorDom = strCatastrophes;

                xmlCatastropheDoc.LoadXml(strCatastrophes);

                if (strCatastrophes != null)
                {
                    dSet = new DataSet();
                    dSet.ReadXml(new XmlNodeReader(xmlCatastropheDoc.SelectSingleNode("ResultMessage/Document/CatastropheLookUpList")));
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }


            return dSet;

        }

        private XmlNode InputForCWS()
        {
            XmlDocument xmlNodeDoc = new XmlDocument();
            XElement oTemplate = XElement.Parse(@"
              <Message>
              <Authorization></Authorization>
              <Call>
                <Function>CatastropheLookUpAdaptor.Get</Function>
              </Call>
              <Document>
                   <AttachCatastrophe>
                        <listhead>
                            <EventDate></EventDate>
                            <CatCodeId></CatCodeId>
                            <TableName></TableName>
                            <RowId>RowId</RowId>  
                            <CatastropheNumber>Catastrophe No</CatastropheNumber> 
                            <LossStartDate>Loss Start Date</LossStartDate>
                            <LossEndDate>Loss End Date</LossEndDate>
                            <Type>Type</Type>
                            <Country>Country</Country>
                            <CatState>CatState</CatState>
                        </listhead>
                   </AttachCatastrophe> 
             </Document>
             </Message>  
            ");

            using (XmlReader reader = oTemplate.CreateReader())
            {
                xmlNodeDoc.Load(reader);
            }

            return xmlNodeDoc;

        }
        protected void grdCatastropheAttach_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string javascriptSelectRow = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                javascriptSelectRow = "SelectCatastropheRow('" + DataBinder.Eval(e.Row.DataItem, "RowId").ToString() + "','" + DataBinder.Eval(e.Row.DataItem, "CatCode").ToString() + "','" + DataBinder.Eval(e.Row.DataItem, "CatastropheNumber").ToString() + "','" + DataBinder.Eval(e.Row.DataItem, "Type").ToString() + "')";

                //showing details of a particular row , after on click it at ,client side
                e.Row.Attributes.Add("onclick", javascriptSelectRow);
            }
        }       
                
    }
}