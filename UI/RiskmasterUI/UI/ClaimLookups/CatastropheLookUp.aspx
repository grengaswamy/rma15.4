﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CatastropheLookUp.aspx.cs" Inherits="Riskmaster.UI.UI.ClaimLookups.CatastropheLookUp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Catastrophes</title>
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/zpcal/themes/system.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../../Scripts/form.js"></script>
    <script type ="text/jscript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;}</script> 

      <script language="javascript" type="text/javascript" >
          var m_RowId = 0;
          var m_sCatNumber = "";
          var m_sCatCode = 0;
          var m_sCodeText = "";
          var m_bOnCLose = true;
          function SelectCatastropheRow(sRowId, sCatCode, sCatNumber,sCodeText) {
              m_RowId = sRowId;
              m_sCatCode = sCatCode;
              m_sCatNumber = sCatNumber;
              m_sCodeText = sCodeText;
          }
          function OnUnload() {
              if (m_bOnCLose) {
                  var objCatRowId = window.opener.document.getElementById("CatRowId");
                  var objCatNumber = window.opener.document.getElementById("catastrophenum");
                  if (objCatRowId != null) {
                      objCatRowId.value = "0";
                  }
                  if (objCatNumber != null) {
                      objCatNumber.value = "";
                  }
              }
          }
          function CatCodeSelected() {                 
              var objrdbCatYes = document.getElementById("rdbCatYes");
              var objrdbCatNo = document.getElementById("rdbCatNo");
              var objrdbCatUnkn = document.getElementById("rdbCatUnkn");

              var objCatRowId = window.opener.document.getElementById("CatRowId");
              var objCatCode = window.opener.document.getElementById("CatCode");
              var objCatNumber = window.opener.document.getElementById("catastrophenum");

              var objCatDecision = window.opener.document.getElementById("CatDecision");
              var objCatastrophe = window.opener.document.getElementById("catastrophe_codelookup");
              var objCatastropheId = window.opener.document.getElementById("catastrophe_codelookup_cid");
              if (objrdbCatYes.checked) {
                  if (m_RowId == 0 && m_sCatCode == 0) {
                      alert("Select One of the Catastrophe Codes");
                      return false;
                  }
                  else {
                      if (objCatRowId != null) {
                          objCatRowId.value = m_RowId;
                      }
                      if (objCatCode != null) {
                          objCatCode.value = m_sCatCode;
                      }
                      if (objCatNumber != null) {
                          objCatNumber.value = m_sCatNumber;
                      }
                      if (objCatDecision != null) {
                          objCatDecision.value = "-1";
                      }                     
                      //window.opener.catastropheSelected(m_sCatCode);
                      //window.opener.setDataChanged(true);
                  }
              }
              else if (objrdbCatNo.checked) 
              {
                  if (objCatDecision != null) {
                      objCatDecision.value = "0";
                  }
                  if (objCatRowId != null) {
                      objCatRowId.value = "0";
                  }                                    
                  if (objCatNumber != null) {
                      objCatNumber.value = "";
                  }
                  
              }
              else if (objrdbCatUnkn.checked)
              {                  
                if (objCatDecision != null) {
                    objCatDecision.value = "-2";
                }
                if (objCatRowId != null) {
                    objCatRowId.value = "0";
                }               
                if (objCatNumber != null) {
                    objCatNumber.value = "";
                }                           
                //window.opener.catastropheSelected(m_sCodeText, m_sCatCode);                  
            }
            window.opener.catastropheSelected(m_sCatCode);
            m_bOnCLose = false;
            //window.opener.setDataChanged(true);
              window.close();
              return true; 

          }


        
    </script>
</head>
<body onunload = "OnUnload()">
    <form id="frmClaimCatastrophe" runat="server">
    <div>
    <uc1:ErrorControl ID="ErrorControl" runat="server" />
        <table width ="100%">
            <tr>
			    <td class="msgheader" nowrap="" colspan="">
				    Automatic Catastrophe Attach</td>
		    </tr>
             <tr>
			    <td nowrap="" colspan="">
				    &nbsp;</td>
		    </tr>
             <tr>
			    <td class="msgheader" nowrap="" colspan="">
				    <asp:RadioButton ID="rdbCatYes" runat="server" Text="Yes" TextAlign="Left" 
                        GroupName="OptionGroup" />
                    &nbsp;&nbsp;
                    <asp:RadioButton ID="rdbCatNo" runat="server" Text="No" TextAlign="Left" Checked="true" 
                        GroupName="OptionGroup" />
                    &nbsp;&nbsp;
                    <asp:RadioButton ID="rdbCatUnkn" runat="server" Text="Not known at this time" 
                        TextAlign="Left" GroupName="OptionGroup" />
				    </td>
		    </tr>
		    <tr>
		        <td>		          
                    <asp:GridView ID="grdCatastropheAttach" runat="server"   AutoGenerateColumns="false" 
               AllowPaging="false"  width="100%"   GridLines="None" 
               onrowdatabound="grdCatastropheAttach_RowDataBound">
		    <HeaderStyle CssClass="msgheader" />
		     <AlternatingRowStyle CssClass="data2" />
		    <Columns>
		        <asp:TemplateField  ItemStyle-CssClass="data" ControlStyle-Width="5%">
                    <ItemTemplate>
                       <input type="radio" id="selectrdo" name="CatastropheGrid"  />
                    </ItemTemplate> 
                </asp:TemplateField>
                        <asp:TemplateField HeaderText="Row Id" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblRowId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RowId")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Catastrophe Code" Visible="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblCatCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CatCode")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Catastrophe No." HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblCatastropheNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CatastropheNumber")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Loss Start Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblLossStDt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LossStartDate")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="Loss End Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblLossEnDt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LossEndDate")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                          <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Type")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  
                           <%-- <asp:TemplateField HeaderText="Country" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass ="headerlink2"  HeaderStyle-ForeColor="White" >
                                    <ItemTemplate>
                                      <asp:Label ID="lblCountry" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Country")%>' ></asp:Label>
                                    </ItemTemplate> 
                          </asp:TemplateField>  --%>
            </Columns>
		     
		     
		 </asp:GridView>
		        </td>
		    </tr>
               <tr>
         <td width="5%" valign="top">
          <input type="text" style="display:none" runat="server" id="currentSelRowId" value="" />
          
         </td>
       </tr>
       <tr>
                <td align="center">
                    <asp:Button ID="btnSubmit" UseSubmitBehavior="true" runat="server" Text="Submit"
                        CssClass="button"
                        onclientclick="return CatCodeSelected();" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel"  CssClass="button" 
                        onclientclick="window.close();" />
                </td>
            </tr>
		</table>
    </div>
    <asp:HiddenField EnableViewState ="true" runat="server" ID="GridSortExpression" />
	<asp:HiddenField EnableViewState ="true" runat="server" ID="GridSortDirection" Value="ASC" />
     <asp:HiddenField ID="hdnPageNumber" runat="server" Value="0" />
    </form>
</body>
</html>

