﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="restore-dcc-template.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.restore_dcc_template" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Restore DCC Template</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    <script language="javascript" src="../../../Scripts/dcc.js" type="text/javascript"></script>
</head>
<body>
  <form id="frmData" runat="server" method="post">
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />    

    <h3>DCC Net</h3>
    <table width="98%" border="0" cellspacing="0" cellpadding="2">
    <tr class="ctrlgroup">
     <td>Check to Restore A Deleted Item</td>
    </tr>
    </table>
    <asp:GridView runat="server" ID="templateDisplaygd" AutoGenerateColumns="false" CssClass="singleborder" OnPreRender="templateDisplaygd_PreRender">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <input type="checkbox" runat="server" ID="checking" value='<%#Eval("templateId")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label runat="server" ID="templatename" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <table>
    <tr>
        <td>
            <asp:Button runat="server" ID="btnrestore" Text="Restore Selected" OnClientClick="return CheckSelection('Restore','report');" OnClick="Restore" />
            <asp:Button runat="server" ID="btncancel" Text="Cancel" OnClick="Cancel"/>
        </td>
    </tr>
    </table>
    <asp:TextBox style="display:none" runat="server" ID="itemlist" rmxref="Instance/Document/DCC/ItemList" />
    <asp:TextBox style="display:none" runat="server" ID="itemtype" Text="template" rmxref="Instance/Document/DCC/ItemType" />
    <asp:TextBox runat="server" ID="selectedvalues" style="display:none" />
  </form>
</body>
</html>
