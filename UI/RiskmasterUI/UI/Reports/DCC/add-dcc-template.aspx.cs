﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class add_dcc_template : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           this.templateId.Attributes.Add("rmxignoreset", "true");
        }

        protected void Save(object sender, EventArgs e)
        {
            NonFDMCWSPageLoad("DCCAdaptor.AddUpdateTemplate");

            Response.Redirect("../DCC/edit-dcc-template.aspx?templateid="+ this.templateId.Text );
        }

        protected void Close(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/templates-listing.aspx");
        }
    }
}
