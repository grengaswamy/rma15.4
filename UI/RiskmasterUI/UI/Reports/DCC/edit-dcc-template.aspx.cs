﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class edit_dcc_template : NonFDMBasePageCWS
    {
       private XmlDocument oFDMPageDom = null;        
       private DataTable tbl = new DataTable("FilterOuter");
       private XElement xTemplate = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.templateId.Text = AppHelper.GetQueryStringValue("templateid");

                CallOnPageLoad();
            }
            tbl.Clear();
        }

        protected void GetFilterElements(Repeater repter, XElement xTemplate, string node)
        {
            
                tbl.Clear();
                
                XElement xNode = xTemplate.XPathSelectElement(node);
                if (xNode.HasElements)
                {
                    string sChildName = ((XElement)(xNode.FirstNode)).Name.ToString();
                    string sValue = xNode.ToString();

                    XElement oLB = XElement.Parse(sValue);

                    if (oLB.HasElements)
                    {
                        if (node == "/template/filters")
                        {
                            foreach (XElement oEle in oLB.Elements())
                            {
                                string sname = oEle.Attribute("name").Value;

                                tbl.Rows.Add(sname);
                            }
                            repter.DataSource = tbl;
                            repter.DataBind();

                            if (node == "/template/filters")
                            {
                                lblNoFilters.Visible = false;
                            }
                            
                        }
                        else if (node == "/template/outputs")
                        {
                            foreach (XElement oEle in oLB.Elements())
                            {
                                string sname = oEle.Value.ToString();

                                tbl.Rows.Add(sname);
                            }
                            repter.DataSource = tbl;
                            repter.DataBind();
                           
                            if (node == "/template/outputs")
                            {
                                lblNoOutputs.Visible = false;
                            }
                        }
                    }
                }
                else
                {
                    if (node == "/template/filters")
                    {
                        lblNoFilters.Visible = true;
                        repeaterFilters.Controls.Clear();
                    }
                    else if (node == "/template/outputs")
                    {
                        lblNoOutputs.Visible = true;
                        repeaterOutputs.Controls.Clear();
                    }
                }
            

        }


        protected void Save(object sender, EventArgs e)
        {
            this.percent.Attributes.Remove("rmxignoreset");
            this.graph.Attributes.Remove("rmxignoreset");
            this.templatename.Attributes.Remove("rmxignoreset");
            
            NonFDMCWSPageLoad("DCCAdaptor.AddUpdateTemplate");
        }

        protected void Close(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/templates-listing.aspx");
        }

        protected void AddFilter(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/add-dcc-filter.aspx?templateid="+ this.templateId.Text);
        }

        protected void AddOutput(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/add-dcc-output.aspx?templateid="+ this.templateId.Text);
        }

        

        protected void Delete(object sender, EventArgs e)
        {
            bool bRetVal = false;
            string sCWSresponse = string.Empty;
            string sFiltersOutputs=string.Empty;

            try
            {
                sFiltersOutputs = "|F|";

                for (int lcount = 0; lcount < repeaterFilters.Items.Count; lcount++)
                {
                    CheckBox chkFilters = (CheckBox)repeaterFilters.Items[lcount].FindControl("chkFilters");
                    LinkButton lnkFilters = (LinkButton)repeaterFilters.Items[lcount].FindControl("lnkFilters");
                    if (chkFilters.Checked)
                    {
                        sFiltersOutputs += lnkFilters.Text + ",";
                    }
                }

                if (repeaterFilters.Items.Count > 0)
                {
                    if (sFiltersOutputs != "|F|")
                    {
                        sFiltersOutputs = sFiltersOutputs.Substring(0, sFiltersOutputs.Length - 1);
                    }
                }

                sFiltersOutputs += "|O|";



                for (int lcount = 0; lcount < repeaterOutputs.Items.Count; lcount++)
                {
                    CheckBox chkOutputs = (CheckBox)repeaterOutputs.Items[lcount].FindControl("chkOutputs");
                    LinkButton lnkOutputs = (LinkButton)repeaterOutputs.Items[lcount].FindControl("lnkOutputs");
                    if (chkOutputs.Checked)
                    {
                        sFiltersOutputs += lnkOutputs.Text + ",";
                    }
                }

                if (repeaterOutputs.Items.Count > 0)
                {
                    if (sFiltersOutputs.LastIndexOf(',') == sFiltersOutputs.Length - 1)
                        sFiltersOutputs = sFiltersOutputs.Substring(0, sFiltersOutputs.Length - 1);
                }

                // we shall use three hidden controls to make the input xml 
                // itemlist, itemtype, template id

                this.itemlist.Attributes.Remove("rmxignoreset");
                this.itemtype.Attributes.Remove("rmxignoreset");

                this.itemlist.Text = sFiltersOutputs;
                this.itemtype.Text = "filteroutput";

                // adding rmxignoreget for controls not needed
                this.percent.Attributes.Add("rmxingoreset", "true");

                this.graph.Attributes.Add("rmxingoreset", "true");

                this.templatename.Attributes.Add("rmxingoreset", "true");


                //NonFDMCWSPageLoad("DCCAdaptor.DeleteItem");

                bRetVal = CallCWS("DCCAdaptor.DeleteItem", null, out sCWSresponse, true, true);

                // code of page load as we need the same bindings again
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }   

            if (bRetVal)
            {
                XmlDocument oFDMPageDom = new XmlDocument();
                oFDMPageDom.LoadXml(sCWSresponse);

                if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                {
                    CallOnPageLoad();
                }
            }
                
        }

      
        protected void repeaterFilters_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType== ListItemType.AlternatingItem))
            {
                DataRowView drvGroup = (DataRowView)e.Item.DataItem;
                
                LinkButton lnkbtn = (LinkButton)e.Item.FindControl("lnkFilters");
                lnkbtn.Text = drvGroup["name"].ToString();
                lnkbtn.PostBackUrl = "~/UI/Reports/DCC/edit-dcc-filter.aspx?templateid=" + this.templateId.Text + "&filtername=" + drvGroup["name"].ToString();

            }
        }

        protected void repeaterOutputs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                DataRowView drvGroup = (DataRowView)e.Item.DataItem;

                LinkButton lnkbtn = (LinkButton)e.Item.FindControl("lnkOutputs");
                lnkbtn.Text = drvGroup["name"].ToString();
                lnkbtn.PostBackUrl = "~/UI/Reports/DCC/edit-dcc-output.aspx?templateid=" + this.templateId.Text + "&outputname=" + drvGroup["name"].ToString();
            }

        }

        protected void CallOnPageLoad()
        {
            try
            {
                this.percent.Attributes.Add("rmxignoreset", "true");
                this.percent.Attributes.Add("rmxignoreget", "true");
                this.graph.Attributes.Add("rmxignoreset", "true");
                this.graph.Attributes.Add("rmxignoreget", "true");
                this.templatename.Attributes.Add("rmxignoreset", "true");
                this.templatename.Attributes.Add("rmxignoreget", "true");

                this.templateId.Attributes.Add("rmxignoreget", "true");

                // setting the rmxignoreset property of hidden controls meant to only fetch data

                this.templateIdHidden.Attributes.Add("rmxignoreset", "true");
                this.templateNameHidden.Attributes.Add("rmxignoreset", "true");
                this.templateShowPercentageHidden.Attributes.Add("rmxignoreset", "true");
                this.templateShowGraphHidden.Attributes.Add("rmxignoreset", "true");

                this.itemlist.Attributes.Add("rmxignoreset", "true");
                this.itemlist.Attributes.Add("rmxignoreget", "true");

                this.itemtype.Attributes.Add("rmxignoreset", "true");
                this.itemtype.Attributes.Add("rmxignoreget", "true");

                NonFDMCWSPageLoad("DCCAdaptor.GetSelectedTemplate");

                // assigning the values to actual controls from hidden controls

                this.templateId.Text = this.templateIdHidden.Text;

                this.templatename.Text = this.templateNameHidden.Text;

                if (this.templateShowGraphHidden.Text != "0")
                    this.graph.Checked = true;
                else
                    this.graph.Checked = false;

                if (this.templateShowPercentageHidden.Text != "0")
                    this.percent.Checked = true;
                else
                    this.percent.Checked = false;

                if (!tbl.Columns.Contains("name"))
                    tbl.Columns.Add("name", typeof(string));

                oFDMPageDom = Data;

                XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/Document");
                XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);

                xTemplate = oMessageRespElement.XPathSelectElement("/template");

                GetFilterElements(repeaterFilters, xTemplate, "/template/filters");

                GetFilterElements(repeaterOutputs, xTemplate, "/template/outputs");

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }   
        
        }
   }
}
