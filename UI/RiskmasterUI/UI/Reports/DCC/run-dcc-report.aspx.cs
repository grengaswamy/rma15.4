﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class run_dcc_report : NonFDMBasePageCWS
    {
        //protected IEnumerable result = null;
        private XElement rootElement = null;
        private IEnumerable reporthead = null;
        private XmlDocument oFDMPageDom = null;
        protected XElement reportname = null;
        //protected bool bReturnStatus;
        private string[] strAttr = new string[20];
        private string contentField;
        private DataTable dtGridData = null;
        private string fieldvalue;
        private int jcount;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ReportId.Text = AppHelper.GetQueryStringValue("reportid");

            try
            {
                if (!IsPostBack)
                {
                    NonFDMCWSPageLoad("DCCAdaptor.GetReportData");
                    oFDMPageDom = new XmlDocument();
                    oFDMPageDom = Data;
                    rootElement = XElement.Parse(oFDMPageDom.OuterXml);
                    reportname = rootElement.XPathSelectElement("report");
                    reporthead = from c in rootElement.XPathSelectElements("report/columns/columnheader")
                                 select c;

                    BindGridData();

                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void BindGridData()
        {
            int lcount = 1;

            oFDMPageDom = new XmlDocument();
            oFDMPageDom = Data;
            dtGridData = new DataTable();

            if (reportname == null)
            {
                return;
            }

            foreach (XElement Item in reporthead)
            {
                contentField = Item.Attribute("name").Value;
                dtGridData.Columns.Add(contentField);
                strAttr[lcount] = contentField;
                lcount = lcount + 1;
            }

            DataRow objRow = null;
            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("//row");
            XmlNode temp = xmlNodeList[0];
            if (temp.ChildNodes.Count > dtGridData.Columns.Count)
            {
                for (int i = 0; i < (temp.ChildNodes.Count - dtGridData.Columns.Count); i++)
                {
                    contentField = " ";
                    dtGridData.Columns.Add(contentField);
                    strAttr[lcount] = contentField;
                    lcount = lcount + 1;
                }
            }

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();
                jcount = 1;
                XmlNodeList childlist = objNodes.ChildNodes;
                    
                    foreach (XmlNode childnode in childlist)
                    {
                        fieldvalue = childnode.Attributes["value"].Value.ToString();
                        contentField = strAttr[jcount];
                        objRow[contentField] = fieldvalue;
                        jcount = jcount + 1;
                    }
                             
                dtGridData.Rows.Add(objRow);
            }
            reportDisplaygd.DataSource = dtGridData;
            reportDisplaygd.DataBind();

        }

        protected void Back(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/reports-listing.aspx?");
        }
    }
}
