﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class edit_dcc_output : NonFDMBasePageCWS
    {
        private string templateidhidden;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.templateid.Text = AppHelper.GetQueryStringValue("templateid");
            this.initialOutputName.Text = AppHelper.GetQueryStringValue("outputname");

            ViewState["templateidhidden"] = this.templateid.Text;

            this.OldOutputName.Text = this.initialOutputName.Text;

            if (!IsPostBack)
            {
                try
                {
                    this.name.Attributes.Add("rmxignoreget", "true");
                    this.name.Attributes.Add("rmxignoreset", "true");
                    this.label.Attributes.Add("rmxignoreget", "true");
                    this.label.Attributes.Add("rmxignoreset", "true");
                    this.graph.Attributes.Add("rmxignoreget", "true");
                    this.graph.Attributes.Add("rmxignoreset", "true");
                    this.datafield.Attributes.Add("rmxignoreget", "true");
                    this.datafield.Attributes.Add("rmxignoreset", "true");
                    this.joinselect.Attributes.Add("rmxignoreget", "true");
                    this.joinselect.Attributes.Add("rmxignoreset", "true");
                    this.joinfrom.Attributes.Add("rmxignoreget", "true");
                    this.joinfrom.Attributes.Add("rmxignoreset", "true");
                    this.joinwhere.Attributes.Add("rmxignoreget", "true");
                    this.joinwhere.Attributes.Add("rmxignoreset", "true");
                    this.filterdepend.Attributes.Add("rmxignoreget", "true");
                    this.filterdepend.Attributes.Add("rmxignoreset", "true");
                    this.outputdepend.Attributes.Add("rmxignoreget", "true");
                    this.outputdepend.Attributes.Add("rmxignoreset", "true");

                    this.OldOutputName.Attributes.Add("rmxignoreset", "true");
                    this.OldOutputName.Attributes.Add("rmxignoreget", "true");

                    // we also need to set the rmignoreset prop for hidden controls meant to fetch data for first time
                    this.firstOutputDatafield.Attributes.Add("rmxignoreset", "true");
                    this.firstOutputFilterdepend.Attributes.Add("rmxignoreset", "true");
                    this.firstOutputGraph.Attributes.Add("rmxignoreset", "true");
                    this.firstOutputJoinfrom.Attributes.Add("rmxignoreset", "true");
                    this.firstOutputJoinselect.Attributes.Add("rmxignoreset", "true");
                    this.firstOutputJoinwhere.Attributes.Add("rmxignoreset", "true");
                    this.firstOutputLabel.Attributes.Add("rmxignoreset", "true");
                    this.firstOutputName.Attributes.Add("rmxignoreset", "true");
                    this.firstOutputOutputdepend.Attributes.Add("rmxignoreset", "true");


                    NonFDMCWSPageLoad("DCCAdaptor.GetOutputNode");

                    // assigning value of hidden controls to the controls present on the page

                    this.name.Text = this.firstOutputName.Text;
                    this.label.Text = this.firstOutputLabel.Text;
                    this.graph.Text = this.firstOutputGraph.Text;
                    this.datafield.Text = this.firstOutputDatafield.Text;
                    this.joinselect.Text = this.firstOutputJoinselect.Text;
                    this.joinfrom.Text = this.firstOutputJoinfrom.Text;
                    this.joinwhere.Text = this.firstOutputJoinwhere.Text;
                    this.filterdepend.Text = this.firstOutputFilterdepend.Text;
                    this.outputdepend.Text = this.firstOutputOutputdepend.Text;
                }
                catch (Exception ex)
                {
                    ErrorHelper.logErrors(ex);
                    BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                    err.Add(ex, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                }    
            }
        
        }

        protected void Save(object sender, EventArgs e)
        {

            try
            {
                this.name.Attributes.Remove("rmxignoreget");
                this.name.Attributes.Remove("rmxignoreset");
                this.label.Attributes.Remove("rmxignoreget");
                this.label.Attributes.Remove("rmxignoreset");
                this.graph.Attributes.Remove("rmxignoreget");
                this.graph.Attributes.Remove("rmxignoreset");
                this.datafield.Attributes.Remove("rmxignoreget");
                this.datafield.Attributes.Remove("rmxignoreset");
                this.joinselect.Attributes.Remove("rmxignoreget");
                this.joinselect.Attributes.Remove("rmxignoreset");
                this.joinfrom.Attributes.Remove("rmxignoreget");
                this.joinfrom.Attributes.Remove("rmxignoreset");
                this.joinwhere.Attributes.Remove("rmxignoreget");
                this.joinwhere.Attributes.Remove("rmxignoreset");
                this.filterdepend.Attributes.Remove("rmxignoreget");
                this.filterdepend.Attributes.Remove("rmxignoreset");
                this.outputdepend.Attributes.Remove("rmxignoreget");
                this.outputdepend.Attributes.Remove("rmxignoreset");

                this.OldOutputName.Attributes.Remove("rmxignoreget");
                this.OldOutputName.Attributes.Remove("rmxignoreset");

                NonFDMCWSPageLoad("DCCAdaptor.CreateOutput");
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }    
        }

        protected void Cancel(object sender, EventArgs e)
        {
            templateidhidden = ViewState["templateidhidden"].ToString();

            Response.Redirect("../DCC/edit-dcc-template.aspx?templateid=" + templateidhidden);
        }
    }
}
