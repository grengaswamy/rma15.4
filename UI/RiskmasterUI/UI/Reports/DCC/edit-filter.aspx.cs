﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.DCC
{
    public partial class edit_filter : NonFDMBasePageCWS
    {
       private DataTable tbl = new DataTable("ReportTable");
       private string sCWSresponse = string.Empty;
       private XElement xTemplate = null;
       protected string sFilterType = string.Empty;
       private string templateid = string.Empty;
       private string reportid = string.Empty;
       private string sFilterName = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                templateid = AppHelper.GetQueryStringValue("templateid");
                reportid = AppHelper.GetQueryStringValue("reportid");
                sFilterName = AppHelper.GetQueryStringValue("filtername");

                if (!IsPostBack)
                {
                    XElement oMessageElement = null;
                    if (oMessageElement == null)
                        oMessageElement = GetNonFDMCWSMessageTemplateForReport();

                    oMessageElement.XPathSelectElement("/Document/DCC/TemplateId").Value = templateid;
                    oMessageElement.XPathSelectElement("/Document/DCC/ReportId").Value = reportid ;
                    oMessageElement.XPathSelectElement("/Document/DCC/FilterName").Value = sFilterName;


                    GetNonFDMCallCWS("DCCAdaptor.GetSelectedFilterData", oMessageElement, false,true);
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected bool GetNonFDMCallCWS(string CWSFunctionName, XElement oMsgElement, bool bGetControlData,bool bSetControlData)
        {
            
                bool bReturnStatusCWSCall = false;
               
                bReturnStatusCWSCall = CallCWS(CWSFunctionName, oMsgElement, out sCWSresponse, bGetControlData, bSetControlData);

                if (bReturnStatusCWSCall)
                {
                    XmlDocument oFDMPageDom = new XmlDocument();
                    oFDMPageDom.LoadXml(sCWSresponse);
                    if (oFDMPageDom.SelectSingleNode("/ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Success")
                    {
                        XmlNode oInstanceNode = oFDMPageDom.SelectSingleNode("/ResultMessage/Document/Selectedfilter");
                        if (oInstanceNode == null)
                        {                           
                            EnableDisableControls("-1");
                            return true;                            
                        }
                        XElement oMessageRespElement = XElement.Parse(oInstanceNode.OuterXml);

                        xTemplate = oMessageRespElement.XPathSelectElement("/filter");
                        if (xTemplate == null)
                        {                            
                            EnableDisableControls("-1");
                            return true;
                        }

                        if (xTemplate.HasAttributes)
                        {
                            sFilterType = xTemplate.Attribute("type").Value.ToString();

                            if (xTemplate.Attribute("name").Value.ToString() != "")
                                lblName.Text = xTemplate.Attribute("name").Value.ToString();

                            ViewState["FilterType"] = sFilterType;

                            if (sFilterType == "1")
                            {                         
                                EnableDisableControls(sFilterType);
                                return true;
                            }
                            if (xTemplate.Attribute("checkall").Value != "")
                                chkAll.Checked = Convert.ToBoolean(Convert.ToInt32(xTemplate.Attribute("checkall").Value));

                            if (sFilterType == "2")
                            {
                                if (chkAll.Checked)
                                {
                                    for (int lcount = 0; lcount < selList.Items.Count; lcount++)
                                    {
                                        selList.Items[lcount].Selected = true;
                                    }
                                }
                            }

                        }
                        else 
                        {                            
                            EnableDisableControls("-1");
                        }

                        if(sFilterType!="")
                            EnableDisableControls(sFilterType);
                                                
                        return true;  
                    }
                }            
            return false;       
        }

        private void EnableDisableControls(string sFilterType)
        {
            HtmlTableRow TR = (HtmlTableRow)Page.FindControl("TRFilterType2");

            switch (sFilterType)
            {
                case "1":
                    TR.Visible = false;
                    this.lblType.Text = "Yes/No";
                    this.txtTopX.Visible = false;
                    this.chkAll.Visible = false;
                    this.chkType1.Visible = true;
                    this.lblType.Visible = true;
                    break;
                case "2":
                    TR.Visible = true;
                    lblType.Visible = true;
                    lblType.Text = "All";
                    lblFilterType2.Text = "List:";
                    this.txtTopX.Visible = false;
                    GetFilterElements(selList, xTemplate, "/filter");
                    this.chkType1.Visible = false;
                    this.chkAll.Visible = true;
                    break;
                case "3":
                    TR.Visible = false;
                    lblType.Text = "TopX:";
                    this.chkAll.Visible = false;
                    this.txtTopX.Visible = true;
                    this.chkType1.Visible = false;
                    this.lblType.Visible = true;
                    break;
                default:
                    TR.Visible = false;
                    this.chkAll.Visible = false;
                    this.txtTopX.Visible = false;
                    lblType.Visible = false;
                    this.chkType1.Visible = false;
                    break;

            }

        }


        private bool GetNonFDMCallToCWS(string CWSFunctionName, XElement oMsgElement, string sCWSresponse, bool bGetControlData,bool bSetControlData)
        {
            Boolean bReturnStatusCWSCall = false;

            bReturnStatusCWSCall = CallCWS(CWSFunctionName, oMsgElement, out sCWSresponse, bGetControlData, bSetControlData);
            return bReturnStatusCWSCall;

        }


        protected void GetFilterElements(ListBox lstContainer, XElement Template, string node)
        {

            try
            {
               
                if (!tbl.Columns.Contains("data"))
                    tbl.Columns.Add(new DataColumn("data", typeof(string)));
                if (!tbl.Columns.Contains("value"))
                    tbl.Columns.Add(new DataColumn("value", typeof(int)));

                tbl.Clear();
                XElement FilterElement = Template.XPathSelectElement(node);
                if (FilterElement.HasElements)
                {
                    string sChildName = ((XElement)(FilterElement.FirstNode)).Name.ToString();
                    string sValue = FilterElement.ToString();

                    XElement oLB = XElement.Parse(sValue);

                    if (oLB.HasElements)
                    {
                        foreach (XElement oEle in oLB.Elements())
                        {
                            string sname = oEle.Attribute("data").Value;
                            string svalue = oEle.Attribute("value").Value;
                            
                            tbl.Rows.Add(sname,svalue);
                        }
                        lstContainer.DataSource = tbl;
                        lstContainer.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
            tbl.Clear();
        }

        protected void Save(object sender, EventArgs e)
        {
            try
            {
                bool bRetval = false;

                XElement oMessageElement = null;
                if (oMessageElement == null)
                    oMessageElement = GetNonFDMCWSMessageTemplateForReport();

                sFilterType = ViewState["FilterType"].ToString();

                if (sFilterType == "2")
                {
                    GetSelectedselListBoxDataAndCheckBox();
                }
                else if (sFilterType == "3")
                {
                    txthndTopX.Text = txtTopX.Text;
                }


                oMessageElement.XPathSelectElement("/Document/DCC/TemplateId").Value = templateid;
                oMessageElement.XPathSelectElement("/Document/DCC/ReportId").Value = reportid;
                oMessageElement.XPathSelectElement("/Document/DCC/FilterName").Value = sFilterName;
                oMessageElement.XPathSelectElement("/Document/DCC/FilterType").Value = sFilterType;

                bRetval=GetNonFDMCallCWS("DCCAdaptor.UpdateFilterData", oMessageElement, true, false);

                if (bRetval)
                {
                    bRetval = GetNonFDMCallCWS("DCCAdaptor.GetSelectedFilterData", oMessageElement, false, true);
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private void GetSelectedselListBoxDataAndCheckBox()
        {
            txtSelList.Text = "";
            txtchkAll.Text = "";

            for (int lcount = 0; lcount < selList.Items.Count; lcount++)
            {
                if (selList.Items[lcount].Selected)
                {
                    txtSelList.Text += selList.Items[lcount].Value + ",";
                }                
            }
            if(txtSelList.Text!="")
                txtSelList.Text = txtSelList.Text.ToString().Substring(0, txtSelList.Text.Length - 1);

            if (chkAll.Checked)
            {
                txtchkAll.Text = "1";
            }
            else
            {
                txtchkAll.Text = "0";
            }

        }

        private XElement GetNonFDMCWSMessageTemplateForReport1()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <DCC>
                        <ReportId></ReportId> 
                        <TemplateId></TemplateId>    
                        <FilterName></FilterName>                                            
                    </DCC>
                </Document>
            </Message>
            ");
            return oTemplate;
        }

        private XElement GetNonFDMCWSMessageTemplateForReport()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
                <Authorization></Authorization>
                <Call>
                    <Function></Function>
                </Call>
                <Document>
                    <DCC>
                        <ReportId></ReportId> 
                        <TemplateId></TemplateId>    
                        <FilterName></FilterName>  
                        <FilterType></FilterType>                                          
                    </DCC>
                </Document>
            </Message>
            ");
            return oTemplate;
        }

        protected void Close(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/Edit-dcc-report.aspx?templateid=" + templateid + "&reportid=" + reportid);           
        }
    }
}
