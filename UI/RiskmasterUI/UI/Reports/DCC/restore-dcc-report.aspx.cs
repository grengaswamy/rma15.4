﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class restore_dcc_report : NonFDMBasePageCWS
    {
        private string sCWSresponse = "";
        private bool bReturnStatus;
        private XmlDocument oFDMPageDom = null;
        private IEnumerable result = null;
        private XElement rootElement = null;
        private DataTable dtGridData = null;

        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    XElement oMessageElement = GetMessageTemplate();

                    bReturnStatus = CallCWS("DCCAdaptor.LoadDeletedItem", oMessageElement, out sCWSresponse, false, true);


                    if (bReturnStatus)
                    {
                        BindGridData();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <Message>
              <Authorization></Authorization> 
              <Call>
               <Function></Function> 
              </Call>
              <Document>
               <DCC>
                 <ItemType>report</ItemType>
               </DCC> 
             </Document>
           </Message>


            ");

            return oTemplate;
        }

        private void BindGridData()
        {
            oFDMPageDom = new XmlDocument();

            oFDMPageDom = Data;

            dtGridData = new DataTable();

            dtGridData.Columns.Add("reportId");
            dtGridData.Columns.Add("reportName");
            

            DataRow objRow = null;

            XmlNodeList xmlNodeList = oFDMPageDom.SelectNodes("//item");

            foreach (XmlNode objNodes in xmlNodeList)
            {
                objRow = dtGridData.NewRow();
                string reportId = objNodes.Attributes["id"].Value.ToString();
                string reportName = objNodes.Attributes["name"].Value.ToString();

                objRow["reportId"] = reportId;
                objRow["reportName"] = reportName;
                
                dtGridData.Rows.Add(objRow);
            }


            reportDisplaygd.DataSource = dtGridData;
            reportDisplaygd.DataBind();

        }

        protected void reportDisplaygd_PreRender(object sender, EventArgs e)
        {
            for (int i = 0; i < dtGridData.Rows.Count; i++)
            {
                Label report = ((Label)(reportDisplaygd.Rows[i].FindControl("reportname")));
                report.Text = dtGridData.Rows[i]["reportName"].ToString();

            }

        }

        protected void Restore(object sender, EventArgs e)
        { 
            this.itemlist.Text = this.selectedvalues.Text;
            this.itemtype.Text = "report";

            NonFDMCWSPageLoad("DCCAdaptor.RestoreItem");

            Response.Redirect("../DCC/reports-listing.aspx");
        }

        protected void Cancel(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/reports-listing.aspx");
        }
    }
}
