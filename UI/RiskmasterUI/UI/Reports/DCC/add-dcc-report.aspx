<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add-dcc-report.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.add_dcc_report" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <title>Add New Report</title>
  <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
  <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
  <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
  <script type="text/javascript">
    
            function drpDownValue() {
                
                //Validation for the report name
                if (document.getElementById("reportName").value == null || trimAll(document.getElementById("reportName").value) == "") {
                    alert("Report Name Field cannot be left blank");
                    document.forms[0].reportName.focus();
                    return false;
                }
                
                var selTemplate = "";
				selTemplate = document.forms[0].templateId[document.forms[0].templateId.selectedIndex].value;
				document.getElementById('templateIdHidden').value = selTemplate;
				
				pleaseWait.Show();
                return true;
            }
            function Validation() {
                
                if (document.getElementById("reportName").value == null || trimAll(document.getElementById("reportName").value) == "") {
                    alert("Report Name Field cannot be left blank");
                    document.forms[0].reportName.focus();
                    return false;
                }
                return true;
            }
            function trimAll(sString) {
                while (sString.substring(0, 1) == ' ') {
                    sString = sString.substring(1, sString.length);
                }
                while (sString.substring(sString.length - 1, sString.length) == ' ') {
                    sString = sString.substring(0, sString.length - 1);
                }
                return sString;
            }    
  </script>  
 </head>
 <body>
  <form id="frmData" method="post" runat="server">
  
  <uc1:ErrorControl ID="ErrorControl1" runat="server" />
  <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" /> 
    
  <p align="center">
    <table cellspacing="2" cellpadding="2" class="singleborder">
     <tr>
      <td colspan="2" class="ctrlgroup">Add New Report</td>
     </tr>
     <tr>
      <td class="datatd"><b>Template Name:</b></td>
      <td class="datatd">
        <asp:DropDownList runat="server" ID="templateId" rmxref="Instance/Document/templates" ItemSetRef="Instance/Document/templates/template" />
        <asp:TextBox style="display:none" runat="server" ID="templateIdHidden" rmxref="Instance/Document/DCC/TemplateId" />
      </td>
     </tr>
     <tr>
      <td class="datatd"><b>Report Name:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="reportName" rmxref="Instance/Document/DCC/ReportName" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Report Title:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="reportTitle" rmxref="Instance/Document/DCC/ReportTitle" /></td>
     </tr>
     <tr>
      <td colspan="2" align="center">
      <asp:Button runat="server" ID="btnSave" type="submit" Text="Save" class="button" OnClick="Save" OnClientClick="return drpDownValue();" /> 
      <asp:Button runat="server" type="submit" ID="btnCancel" Text="Cancel" class="button" OnClick="Refresh" /></td>
     </tr>
    </table>
   </p>
   </form>
 </body>
</html>
