﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster.UI.Reports.Other_Reports
{
    public partial class delete_template_confirm : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Itemlist.Text = AppHelper.GetQueryStringValue("selectedvalues");
        }

        protected void deleteTemplate(object sender, EventArgs e)
        {
            NonFDMCWSPageLoad("DCCAdaptor.DeleteItem");

            Response.Redirect("../DCC/templates-listing.aspx");
        }

        protected void Cancel(object sender, EventArgs e)
        {
            Response.Redirect("../DCC/templates-listing.aspx");        
        }
    }
}
