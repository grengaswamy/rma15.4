﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="edit-dcc-output.aspx.cs" Inherits="Riskmaster.UI.Reports.Other_Reports.edit_dcc_output" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" tagname="PleaseWaitDialog" tagprefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DCC Output</title>
    <link rel="stylesheet" href="../../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../../Content/system.css" type="text/css" />
    
    <script type ="text/javascript" language="JavaScript" src="../../../Scripts/WaitDialog.js">{var i;} </script>
    
    <script type="text/javascript">    
        function ShowPleaseWait()
        {
            pleaseWait.Show();
            return true;
        }
    </script> 
    
</head>
<body>
    <form id="frmData" runat="server" method="post">
    
    <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    <uc:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server" CustomMessage="" />
    
    <p align="center">
    <table cellspacing="2" cellpadding="2" class="singleborder">
     <tr>
      <td colspan="2" class="ctrlgroup">Edit Output</td>
     </tr>
     <tr>
      <td class="datatd"><b>Name:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="name" rmxref="Instance/Document/DCC/NewOutputName" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Label:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="label" rmxref="Instance/Document/DCC/OutputLabel"  size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Graph:</b></td>
      <td class="datatd"><asp:DropDownList runat="server" ID="graph" rmxref="Instance/Document/DCC/GraphType">
        <asp:ListItem value="X">X</asp:ListItem>
        <asp:ListItem value="Y">Y</asp:ListItem></asp:DropDownList></td>
     </tr>
     <tr>
      <td class="datatd"><b>Data Field:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="datafield" rmxref="Instance/Document/DCC/DataField" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Join Select:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="joinselect" rmxref="Instance/Document/DCC/JoinSelect" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Join From:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="joinfrom" rmxref="Instance/Document/DCC/JoinFrom"  size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Join Where:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="joinwhere" rmxref="Instance/Document/DCC/JoinWhere" size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Filter Dependencies:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="filterdepend" rmxref="Instance/Document/DCC/FilterDependencies"  size="50" /></td>
     </tr>
     <tr>
      <td class="datatd"><b>Output Dependencies:</b></td>
      <td class="datatd"><asp:TextBox runat="server" type="text" ID="outputdepend" rmxref="Instance/Document/DCC/OutputDependencies"  size="50" /></td>
     </tr>
     <tr>
      <td colspan="2" align="center">
        <asp:Button runat="server" type="submit" ID="btnSave" Text="Save" class="button" OnClick="Save" OnClientClick ="return ShowPleaseWait();"/>  
        <asp:Button runat="server" type="submit" ID="btnCancel" Text="Cancel" class="button" OnClick="Cancel" />
      </td>
     </tr>
    </table>
   </p>
   <asp:TextBox style="display:none" runat="server" ID="firstOutputName"  rmxref="Instance/Document/output/@name" />
   <asp:TextBox style="display:none" runat="server" ID="firstOutputLabel" rmxref="Instance/Document/output/@label" />
   <asp:TextBox style="display:none" runat="server" ID="firstOutputGraph" rmxref="Instance/Document/output/@graph" />
   <asp:TextBox style="display:none" runat="server" ID="firstOutputDatafield" rmxref="Instance/Document/output/datafield" />
   <asp:TextBox style="display:none" runat="server" ID="firstOutputJoinselect" rmxref="Instance/Document/output/joinselect" />
   <asp:TextBox style="display:none" runat="server" ID="firstOutputJoinfrom" rmxref="Instance/Document/output/joinfrom" />
   <asp:TextBox style="display:none" runat="server" ID="firstOutputJoinwhere" rmxref="Instance/Document/output/joinwhere" />
   <asp:TextBox style="display:none" runat="server" ID="firstOutputFilterdepend" rmxref="Instance/Document/output/fdependencies" />
   <asp:TextBox style="display:none" runat="server" ID="firstOutputOutputdepend" rmxref="Instance/Document/output/odependencies" />
   <asp:TextBox style="display:none" runat="server" ID="templateid" rmxref="Instance/Document/DCC/TemplateId" />
   <asp:TextBox style="display:none" runat="server" ID="initialOutputName" rmxref="Instance/Document/DCC/OutputName" />
   <asp:TextBox style="display:none" runat="server" ID="OldOutputName" rmxref="Instance/Document/DCC/OldOutputName" />
  </form>
</body>
</html>
