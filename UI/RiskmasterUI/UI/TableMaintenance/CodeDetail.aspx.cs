﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;


namespace Riskmaster.UI.Utilities
{
    public partial class CodeDetail : NonFDMBasePageCWS
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool bReturnStatus = false;
            string sreturnValue = "";
            DataSet tableRecordsSet = null;
            XmlDocument codeXDoc = new XmlDocument();
            XElement XmlTemplate = null;
            string typecode, codeID, tableID, mode;
            string iLangCode = string.Empty;
            string sLanguage = string.Empty;
            string sTableName = "";//7810
            string sPshortCode = "";//7810
            //Bijender has changed for Mits 15474
            //if (AppHelper.GetQueryStringValue("Mode") == "new")
            //    DeleteButton.Visible = false;
            //else
            //    DeleteButton.Visible = true;
            ////End                       

            // npadhy RMA JIRA 11075 in case of an exception in edit mode, the Delete Button is kept visible...so by default we will disable the Delete Button and once
            // the request is back from service we will show the DeleteButton
            DeleteButton.Visible = false;
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);
            }
            if (!IsPostBack)
            {
                try
                {
                    typecode = Request.QueryString["TypeCode"];
                    codeID = Request.QueryString["ID"];
                    tableID = Request.QueryString["Table"];
                    mode = Request.QueryString["Mode"];
                    //Use values from get unless there are not any then use default values
                    if (typecode != "" && typecode != null)
                        this.TypeCode.Text = typecode;
                    if (codeID != "" && codeID != null)
                        this.ID.Text = codeID;
                    if (tableID != "" && tableID != null)
                        this.TID.Text = tableID;
                    XmlTemplate = GetMessageTemplate();

                    CancelButton.Attributes.Add("onClick", "javascript:return formHandler1('CodeList.aspx?TypeCode=" + TypeCode.Text + "&Table=" + TID.Text + "','','','normal','');");
                    DeleteButton.Attributes.Add("onClick", "javascript:return Delete();");

                    if (mode == "Edit") //For Editing a code load data from DB
                        bReturnStatus = CallCWSFunction("CodeUtilityAdaptor.GetXMLData", out sreturnValue, XmlTemplate);
                    else //Enable the onBlur events for the buttons, but don't load data
                    {
                        ID.Text = TID.Text;
                        bReturnStatus = CallCWSFunction("CodeUtilityAdaptor.GetXMLData", out sreturnValue, XmlTemplate);
                        pCode.Attributes.Add("onBlur", "javascript:return onLostFocus('" + TID.Text + "','pCode');");
                        pCodeButton.Attributes.Add("onClick", "window.open('CodeLookUp.aspx?TypeCode=pCode&ID=" + TID.Text + "','','resizable=yes,scrollbars=yes')");
                        Ind_Standard.Attributes.Add("onBlur", "javascript:return onLostFocus('" + TID.Text + "','Ind_Standard');");
                        Ind_StandardButton.Attributes.Add("onClick", "window.open('CodeLookUp.aspx?TypeCode=Ind_Standard&ID=" + TID.Text + "','','resizable=yes,scrollbars=yes')");
                        Line_of_Bus.Attributes.Add("onBlur", "javascript:return onLostFocus('" + TID.Text + "','Line_of_Bus');");
                        Line_of_BusButton.Attributes.Add("onClick", "window.open('CodeLookUp.aspx?TypeCode=Line_of_Bus&ID=" + TID.Text + "','','resizable=yes,scrollbars=yes')");
                    }
                    if (bReturnStatus)
                    {
                        codeXDoc.LoadXml(sreturnValue);
                        tableRecordsSet = ConvertXmlDocToDataSet(codeXDoc);
                        // npadhy RMA JIRA 11075 in case of an exception in edit mode, the Delete Button is kept visible...so by default we will disable the Delete Button and once
                        // the request is back from service we will show the DeleteButton
                        //Added by Amitosh for payee phrase
                        if ((codeXDoc.SelectSingleNode("//RemoveDelButton") != null) && string.Equals(codeXDoc.SelectSingleNode("//RemoveDelButton").InnerText, "true", StringComparison.InvariantCultureIgnoreCase))
                        {
                            DeleteButton.Visible = false;
                        }
                        else
                        {
                            if(mode == "Edit")
                                DeleteButton.Visible = true;
                        }
                        //end delete
                        if (tableRecordsSet.Tables["control"] != null)
                        {
                            foreach (DataRow row in tableRecordsSet.Tables["control"].Rows)
                            {
                                if (row["name"].ToString() == "TableId")
                                {
                                    TID.Text = row["value"].ToString(); //use TID for saving
                                    CancelButton.Attributes.Add("onClick", "javascript:return formHandler1('CodeList.aspx?TypeCode=" + TypeCode.Text + "&Table=" + TID.Text + "','','','normal','');");
                                }
                                if (row["name"].ToString() == "req_fields")//Parijat: MITS 20198
                                {
                                    req_fields.Text = row["value"].ToString();
                                }
                                //Aman ML Change
                                if (row["name"].ToString() == "LanguageCode")
                                {
                                    iLangCode = row["value"].ToString();
                                }
                                foreach (Control oCtrl in this.Form.Controls)
                                {
                                    foreach (Control oCtrl2 in oCtrl.Controls)
                                    {
                                        if (oCtrl2.ID == row["name"].ToString())
                                        {
                                            switch (oCtrl2.ID)  //set events for the fields and the values for the fields
                                            {
                                                case "Code":
                                                    {
                                                        Code.Text = row["value"].ToString();

                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            Code.Enabled = true;
                                                        else
                                                            Code.Enabled = false;
                                                        break;
                                                    }
                                                case "Code_Desc":
                                                    {
                                                        Code_Desc.Text = row["value"].ToString();
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            Code_Desc.Enabled = true;
                                                        else
                                                            Code_Desc.Enabled = false;
                                                        break;
                                                    }
                                                case "pCode":
                                                    {
                                                        this.pCodeButton.Attributes.Add("onClick", "window.open('CodeLookUp.aspx?TypeCode=pCode&ID=" + row["tableid"].ToString() + "','','resizable=yes,scrollbars=yes')");

                                                        pCode.Text = row["value"].ToString();
                                                        pCode_cid.Text = row["id"].ToString();
                                                        pCode.Attributes.Add("onBlur", "javascript:return onLostFocus('" + row["codetable"].ToString() + "','pCode');");
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                        {
                                                            pCode.Enabled = true;
                                                            pCodeButton.Enabled = true;
                                                        }
                                                        else
                                                        {
                                                            pCode.Enabled = false;
                                                            pCodeButton.Enabled = false;
                                                        }
                                                        break;
                                                    }
                                                case "Ind_Standard":
                                                    {
                                                        this.Ind_StandardButton.Attributes.Add("onClick", "window.open('CodeLookUp.aspx?TypeCode=Ind_Standard&ID=" + row["tableid"].ToString() + "','','resizable=yes,scrollbars=yes')");
                                                        Ind_Standard.Text = row["value"].ToString();
                                                        Ind_Standard_cid.Text = row["id"].ToString();
                                                        Ind_Standard.Attributes.Add("onBlur", "javascript:return onLostFocus('" + row["codetable"].ToString() + "','Ind_Standard');");
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                        {
                                                            Ind_Standard.Enabled = true;
                                                            Ind_StandardButton.Enabled = true;
                                                        }
                                                        else
                                                        {
                                                            Ind_Standard.Enabled = false;
                                                            Ind_StandardButton.Enabled = false;
                                                        }
                                                        break;
                                                    }
                                                case "Line_of_Bus":
                                                    {
                                                        this.Line_of_BusButton.Attributes.Add("onClick", "window.open('CodeLookUp.aspx?TypeCode=Line_of_Bus&ID=" + row["tableid"].ToString() + "','','resizable=yes,scrollbars=yes')");
                                                        Line_of_Bus.Text = row["value"].ToString();
                                                        Line_of_Bus_cid.Text = row["id"].ToString();
                                                        Line_of_Bus.Attributes.Add("onBlur", "javascript:return onLostFocus('" + row["codetable"].ToString() + "','Line_of_Bus');");
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                        {
                                                            Line_of_Bus.Enabled = true;
                                                            Line_of_BusButton.Enabled = true;
                                                        }
                                                        else
                                                        {
                                                            Line_of_Bus.Enabled = false;
                                                            Line_of_BusButton.Enabled = false;
                                                        }
                                                        break;
                                                    }
                                                case "trigger_date":
                                                    {
                                                        trigger_date.DataSource = tableRecordsSet.Tables["TriggerDates"];
                                                        trigger_date.DataValueField = "tvalue";
                                                        trigger_date.DataTextField = "tdate";
                                                        trigger_date.DataBind();
                                                        trigger_date.Items.Insert(0, "");
                                                        if (tableRecordsSet.Tables["TriggerDates"].Columns["selected"] != null)
                                                        {
                                                            foreach (DataRow row2 in tableRecordsSet.Tables["TriggerDates"].Rows)
                                                            {
                                                                if (row2["selected"].ToString() == "1")
                                                                {
                                                                    trigger_date.SelectedValue = row2["tvalue"].ToString();
                                                                }
                                                            }
                                                        }
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                            trigger_date.Enabled = true;
                                                        else
                                                            trigger_date.Enabled = false;
                                                        break;
                                                    }
                                                case "e_StartDate":
                                                    {
                                                        e_StartDate.Text = row["value"].ToString();
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                        {
                                                            e_StartDate.Enabled = true;
                                                            //e_StartDateButton.Enabled = true;
                                                        }
                                                        else
                                                        {
                                                            e_StartDate.Enabled = false;
                                                           // e_StartDateButton.Enabled = false;
                                                        }
                                                        break;
                                                    }
                                                case "e_EndDate":
                                                    {
                                                        e_EndDate.Text = row["value"].ToString();
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                        {
                                                            e_EndDate.Enabled = true;
                                                           // e_EndDateButton.Enabled = true;
                                                        }
                                                        else
                                                        {
                                                            e_EndDate.Enabled = false;
                                                            //e_EndDateButton.Enabled = false;
                                                        }
                                                        break;
                                                    }
                                                case "e_Org":
                                                    {
                                                        e_Org.Text = row["value"].ToString();
                                                        e_Org_cid.Text = row["id"].ToString();
                                                        //Start vsharma65 MITS 23567
                                                        //string[] str = row["value"].ToString().Split(',');
                                                        //string[] strid = row["id"].ToString().Split(' ');
                                                        //for (int i = 0; i < str.Length; i++)
                                                        //{
                                                        //    if(strid[i] != "0")
                                                        //    {
                                                        //        e_Org.Items.Add(new ListItem(str[i].ToString(), strid[i].ToString()));
                                                        //        e_Org_lst.Text = row["id"].ToString();
                                                        //    }
                                                        //}

                                                        //e_Org.Text = row["value"].ToString();
                                                        //e_Org_cid.Text = row["id"].ToString();
                                                        //Raman: MITS 14898
                                                        //e_OrgButton.Attributes.Add("onClick", "javascript:return select_option('../OrganisationHierarchy/OrgHierarchyLookup.aspx?lob=8', '', 'e_Org');");
                                                        e_Orgbtn.Attributes.Add("onClick", "return selectCode('orgh','e_Org','ALL');");
                                                        e_Org.Attributes.Add("onblur", "codeLostFocus(this.name);");
                                                        e_Org.Attributes.Add("onchange", "lookupTextChanged(this);");
                                                        //End vsharma65 MITS 23567
                                                        if (row["lock"].ToString() == "false" || row["lock"].ToString() == "")
                                                        {
                                                            e_Org.Enabled = true;
                                                            e_Orgbtn.Enabled = true;
                                                        }
                                                        else
                                                        {
                                                            e_Org.Enabled = false;
                                                            e_Orgbtn.Enabled = false;
                                                        }
                                                        break;
                                                    }
                                                    //Aman ML Changes                                                
                                                case "language":
                                                    {
                                                        if (tableRecordsSet.Tables["option"] != null)
                                                        {                                                                                                                       
                                                            DataView langDataView = new DataView(tableRecordsSet.Tables["option"]);
                                                            language.DataSource = langDataView;
                                                            language.DataTextField = "option_Text";
                                                            language.DataValueField = "value";
                                                            if(!string.IsNullOrEmpty(iLangCode))
                                                               language.SelectedValue = iLangCode;
                                                            else
                                                                language.SelectedValue = AppHelper.GetBaseLanguageCodeWithCulture().ToString().Split('|')[0];  //replace with the base language
                                                            //if (AppHelper.GetQueryStringValue("Mode") == "new")
                                                            //{
                                                               // language.SelectedValue = AppHelper.GetBaseLanguageCodeWithCulture().ToString().Split('|')[0]; //"1033";  //replace with the base language
                                                                language.Enabled = false;
                                                            //}
                                                            language.DataBind();
                                                        }
                                                    }
                                                    break;
                                                //Aman ML Changes
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (codeXDoc.SelectSingleNode("//VisibleCurrencyLink") != null)
                        {
                            linkCurrencyCode.Visible = Convert.ToBoolean(codeXDoc.SelectSingleNode("//VisibleCurrencyLink").InnerText);
                            linkCurrencyCode.Attributes.Add("onClick", "window.open('CodeLookUp.aspx?TypeCode=Currency_Type&ID=" + Convert.ToString(tableID) + "','','resizable=yes,scrollbars=yes')");
                        }
                    }
                }
                catch (Exception objException)
                {
                    ErrorHelper.logErrors(objException);
                    BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                    baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                    ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
                }//catch

            }
        }

        private XElement GetMessageTemplate() //XML template for CWS call
        {
            string mode = "";
            mode = Request.QueryString["Mode"];
            //Use values from get unless there are not any then use default values
            if (mode != "" && mode != null)
                this.Mode.Text = mode;
            if (mode == "new")
            {
                this.FormName.Text = "frmaddcode";
            }
            else if (mode == "Edit")
            {
                this.FormName.Text = "frmeditcode";
            }
            //Changed by Amitosh for Payee Phrase
            //Aman ML change added the language node
            XElement oTemplate = XElement.Parse(@"
            <Message>
	            <Authorization>a040d3a6-650f-413d-b206-22a0407d6075</Authorization>
	            <Call>
		            <Function>CodeUtilityAdaptor.GetXMLData</Function>
	            </Call>
	            <Document>
		            <CodeUtility>
			            <FormName></FormName>
			            <Mode></Mode>
			            <TypeCode></TypeCode>
			            <ID></ID>
			            <Alpha/>
                        <XML>
						    <form actionname='' bcolor='white' name='frmData' sid='150' title='Enter Code Detail' topbuttons='1'>
							    <body1 func='pageloaded();' req_func='yes'/>
                                <RemoveDelButton/>
							    <group name='code_detail' title='Code Detail' >
								    <error desc='' value='0'/>
								    <control isrequ='yes' name='Code' title='Code' type='text' value='' lock='false'/>
								    <control isrequ='yes' name='Code_Desc' title='Description' type='text' value=''/>
								    <control codetable='' id='' linkpage='home?pg=riskmaster/TableMaintenance/CodeListPopUp&amp;List=pCode&amp;TableId=' lock='false' lostfocusfunc='yes' name='pCode' tableid='' title='Parent Code' type='textandlist' value=''/>
								    <control codetable='' id='' linkpage='home?pg=riskmaster/TableMaintenance/CodeListPopUp&amp;List=Ind_Standard&amp;TableId=' lock='false' lostfocusfunc='yes' name='Ind_Standard' tableid='' title='Industry Standard' type='textandlist' value=''/>
								    <control codetable='' id='' linkpage='home?pg=riskmaster/TableMaintenance/CodeListPopUp&amp;List=Line_of_Bus&amp;TableId=' lock='false' lostfocusfunc='yes' name='Line_of_Bus' tableid='' title='Line Of Business' type='textandlist' value=''/>
								    <control name='trigger_date' optionname='TriggerDates' title='Effective Date Trigger' type='combobox' lock='false'/>
								    <control name='e_StartDate' title='Effective Start Date' type='date' value='' lock='false'/>
								    <control name='e_EndDate' title='Effective End Date' type='date' value='' lock='false'/>
								    <control id='' linkpage='home?pg=riskmaster/OrgHierarchy/Org&amp;tablename=nothing&amp;rowid=0&amp;lob=0&amp;searchOrgId=&amp;section=' lostfocusfunc='yes' name='e_Org' reftable='organization' title='Effective For' type='textandlist' lock='false'/>
								    <control name='action' type='id' value=''/>
								    <control name='TableId' type='id' value=''/>
                                    <control name='TableName' type='id' value=''/>
								    <control name='req_fields' type='id' value='Code|Code_Desc'/>
                                    <control name='LanguageCode' type='' value=''/>
                                    <control name='language' type='' value=''/>
							    </group>
							    <button linkto='' name='btnsave' param='' title='Save' type='save' windowClose='false'/>
							    <button linkto='home?pg=riskmaster/TableMaintenance/CodeList&amp;TypeCode=3&amp;TableId=' name='btncancel' param='' title='Cancel' type='normal'/>
							    <button linkto='' name='btndelete' param='' title='Delete' type='delete' windowClose='false'/>
						    </form>
			            </XML>
		            </CodeUtility>
	            </Document>
            </Message>
            ");

            return oTemplate;
        }


        private XElement SaveMessageTemplate() //SML template for save CWS call
        {
            if (Mode.Text=="new")
                TableId.Text = TID.Text;
            else
                TableId.Text=ID.Text;   //R4 stored code ID in table ID field when editing a code....                
            if (Ind_Standard_cid.Text == "")
                IndStdCode.Text="0";
            else
                IndStdCode.Text = Ind_Standard_cid.Text;
            if (Line_of_Bus_cid.Text == "")
                LineOfBusCode.Text = "0";
            else
                LineOfBusCode.Text = Line_of_Bus_cid.Text;
            ShortCode.Text = Code.Text;
            CodeDesc.Text = Code_Desc.Text;
            if (pCode_cid.Text == "")
                RelatedCode.Text = "0";
            else
                RelatedCode.Text = pCode_cid.Text;
            TriggerDt.Text = trigger_date.Text;
            StartDt.Text = e_StartDate.Text;
            EndDt.Text = e_EndDate.Text;
            //if (e_Org_lst.Text == "")
            if (e_Org_cid.Text == "")
                OrgId.Text = "0";
            else
                OrgId.Text = e_Org_cid.Text;
                //OrgId.Text = e_Org_lst.Text;


            XElement oTemplate = XElement.Parse(@"
				<Message>
					<Authorization>3ac889ae-6fe3-42a2-acad-3a9ab0557c63</Authorization>
					<Call>
						<Function>CodeUtilityAdaptor.SaveCode</Function>
					</Call>
					<Document>
						<CodeUtility>
							<Mode></Mode>
							<InputData>
								<Code>
                                    <ID></ID>
									<TableId></TableId>
									<IndStdCode></IndStdCode>
									<LineOfBusCode></LineOfBusCode>
									<ShortCode></ShortCode>
									<CodeDesc></CodeDesc>
									<RelatedCode></RelatedCode>
									<TriggerDt/>
									<StartDt/>
									<EndDt/>
									<OrgId></OrgId>
									<LanguageCode>1033</LanguageCode>
								</Code>
							</InputData>
						</CodeUtility>
					</Document>
				</Message>
            ");

            return oTemplate;
        }

        protected void SaveCode(object sender, EventArgs e)
        {
            try
            {
                bool bReturnStatus = false;
                string sreturnValue = "";
                XElement XmlTemplate = null;
                if (FlagForRefresh.Text != "0")
                {
                    XmlTemplate = SaveMessageTemplate();
                    if (XmlTemplate.XPathSelectElement("//LanguageCode") != null)
                    {
                        XmlTemplate.XPathSelectElement("//LanguageCode").Value = AppHelper.GetBaseLanguageCodeWithCulture().ToString().Split('|')[0];
                    }
                    if(Code_cid.Text != "")
                     CodeDesc.Text = CodeDesc.Text + "|" + Code_cid.Text.Split('|')[1];
                    bReturnStatus = CallCWSFunction("CodeUtilityAdaptor.SaveCode", out sreturnValue, XmlTemplate);
                    //MITS 15488
                    if (ErrorHelper.IsCWSCallSuccess(sreturnValue))
                    {
                        Response.Write("<script>location.href = 'CodeList.aspx?TypeCode=" + TypeCode.Text + "&Table=" + TID.Text + "';</script>");
                    }
                    
                    
                }
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        }
        protected void DeleteCode(object sender, EventArgs e)
        {
            try
            {
                Mode.Text = "delete";
                SaveCode(sender, e);
                //Start vsharma65 MITS 23653
                //Response.Write("<script>location.href = 'CodeList.aspx?TypeCode=" + TypeCode.Text + "&Table=" + TID.Text + "';</script>");
                //End vsharma65 MITS 23653
            }
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors baeObject = new BusinessAdaptorErrors();
                baeObject.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(baeObject);
            }//catch
        }
    }
}