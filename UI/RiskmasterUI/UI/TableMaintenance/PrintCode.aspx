﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintCode.aspx.cs" Inherits="Riskmaster.UI.Utilities.PrintCode" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Executive summary</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <%--color to be made red here--%>
    <%--<asp:textbox style="display: none;" runat="server" id="error" rmxref="Instance/Document/ExecutiveSummaryReport/EventId" Text="15" rmxtype="hidden" />--%>
    </div>
	<asp:textbox style="display: none;" runat="server" id="TableId" rmxref="Instance/Document/PrintCodes/TableId" Text="" rmxtype="hidden" />
	<asp:textbox style="display: none;" runat="server" id="TypeCode" rmxref="Instance/Document/PrintCodes/TypeCode" Text="" rmxtype="hidden" />
	<asp:textbox style="display: none;" runat="server" id="Alpha" rmxref="Instance/Document/PrintCodes/Alpha" Text="" rmxtype="hidden" />
	<asp:textbox style="display: none;" runat="server" id="File" rmxref="Instance/Document/PrintCodeList/File" Text="" rmxtype="hidden" />
	<div>
        <uc1:ErrorControl ID="ErrorControl1" runat="server" />
    </div>
    </form>
</body>
</html>