﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="TableDetail.aspx.cs" Inherits="Riskmaster.UI.Utilities.TableDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
    <title>Enter Table Detail</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
    <style type="text/css">
    .center
    {
        text-align: center
    }
    </style>    
     <script language="JavaScript" type="text/javascript">

         window.onload = datachanged; //Parijat: MITS 20198
         function datachanged() {//Parijat: MITS 20198
             var obj2 = document.getElementById('pTable');
             var obj = document.getElementById('pTable_Required');
             if (obj2.value != '') {

                 obj.disabled = false;
             }
             else {
                 obj.disabled = true;
             }

         }
        

    </script>
</head>
<body>
    <form id="frmData" runat="server">
        <div class="headertext3"><span lang="en-us">Enter Table Detail</span></div>
        <div class="center" colspan="2">
        <asp:Button ID="btSaveTable" onclick = "SaveTable" runat="server" name="SaveTable" Text="Save"  class="button" style="width:100"/>  
        <asp:Button ID="btCancel" runat="server" name="Cancel" Text="Cancel"  class="button" style="width:100"/>
        </div>
        <table width="100%" cellspacing="0" cellpadding="4" border="0">
            <tr>
                <td colspan="2">
                    <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                </td>
            </tr>
        </table>    
        <div runat="server" class="full" id="div_stable">
            <span id="span1" runat="server" class="required" width="100%">System Table Name</span>
            <span class="formw"><asp:textbox runat="server" onchange="javascript:return datachanged('true');" size="30" id="stable"/>
            </span></div>
        <div runat="server" class="full" id="div_utable" xmlns="">
            <span class="required">User Table Name</span><span class="formw">
                <asp:textbox runat="server" size="30" onchange="javascript:return datachanged('true');" id="utable"/>
            </span></div>
        <div runat="server" class="full" id="div_pTable" xmlns="">
            
                 <span class="label">Parent Name</span><span class="formw"><%--Parijat: MITS 20198--%>
                <asp:DropDownList id="pTable" runat="server" onchange="javascript:return setchanges('pTable');"></asp:DropDownList><%--"datachanged(this)"--%>
            </span></div>
        <div runat="server" class="full" id="div2_pTable_Required" xmlns="">
            <span class="label">Parent Table Required</span>
            <span class="formw"><asp:checkbox runat="server" id="pTable_Required" onchange="javascript:return datachanged('true');"/>
            </span></div>
        <div runat="server" class="full" id="div_ind_stand_table" xmlns="">
            <span class="label">Industry Standard Table</span><span class="formw">
                <asp:DropDownList id="ind_stand_table" rmxref="" runat="server" onchange="javascript:return setchanges('ind_stand_table');"></asp:DropDownList>
            </span></div>
        <div runat="server" class="full" id="div_Ind_Tables_Required" xmlns="">
            <span class="label">Industry Standard Table Required</span><span class="formw">
                <asp:checkbox runat="server" id="Ind_Tables_Required" onchange="javascript:return datachanged('true');"/>
            </span></div>
        <div runat="server" class="full" id="div_allow_attach" xmlns="">
            <span class="label">Allow Attachment</span><span class="formw">
                <asp:checkbox runat="server" id="allow_attach" onchange="javascript:return datachanged('true');"/>
            </span></div>
        <div runat="server" class="full" id="div_tree_disp" xmlns="">
            <span class="label">Tree Display</span><span class="formw">
                <asp:checkbox runat="server" id="tree_disp" onchange="javascript:return datachanged('true');"/>
            </span></div>
        <div runat="server" class="full" id="div_line_of_bus" xmlns="">
            <span class="label">Line Of Business Required</span><span class="formw">
                <asp:checkbox runat="server" id="line_of_bus" onchange="javascript:return datachanged('true');"/>
            </span></div>
        <asp:textbox style="display: none" runat="server" id="FormName" rmxref="Instance/Document/CodeUtility/FormName" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="Mode" rmxref="Instance/Document/CodeUtility/Mode" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TypeCode" rmxref="Instance/Document/CodeUtility/TypeCode" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="ID" rmxref="Instance/Document/CodeUtility/ID" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="Alpha" rmxref="Instance/Document/CodeUtility/Alpha" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="req_fields" Text="stable|utable" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="FlagForRefresh" Text="0" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableId" rmxref="Instance/Document/CodeUtility/InputData/Table/TableId" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TypeCode2" rmxref="Instance/Document/CodeUtility/InputData/Table/TypeCode" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="SysTableName" rmxref="Instance/Document/CodeUtility/InputData/Table/SysTableName" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TableName" rmxref="Instance/Document/CodeUtility/InputData/Table/TableName" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="RelatedId" rmxref="Instance/Document/CodeUtility/InputData/Table/RelatedId" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="RelTableFlag" rmxref="Instance/Document/CodeUtility/InputData/Table/RelTableFlag" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="IndTableId" rmxref="Instance/Document/CodeUtility/InputData/Table/IndTableId" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="AttchFlag" rmxref="Instance/Document/CodeUtility/InputData/Table/AttchFlag" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="TreeFlag" rmxref="Instance/Document/CodeUtility/InputData/Table/TreeFlag" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="LineOfBusFlag" rmxref="Instance/Document/CodeUtility/InputData/Table/LineOfBusFlag" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="ReqIndTable" rmxref="Instance/Document/CodeUtility/InputData/Table/ReqIndTable" Text="" rmxtype="hidden" />

    </form>
</body>
</html>