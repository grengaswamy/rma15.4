﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TableList.aspx.cs" Inherits="Riskmaster.UI.Utilities.TableList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Code Tables</title>
  <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css"/>
    <link rel="stylesheet" href="../../Content/system.css" type="text/css"/>
  <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>
  <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js"></script>
    <style type="text/css">
        .center
        {
            text-align: center;
        }
        .full
        {
            width: 100%;
        }
        .smokeBG
        {
            background-color: #f5f5f5;
            width: 100%;
            height: 24px;
        }
        .whiteBG
        {
            background-color: #ffffff;
            width: 100%;
            height: 24px;
        }
    </style>
</head>
<%--MITS 15831 MAC : changed onload to TableListOnLoad()--%>
<body id="bodyTableList" onload="TableListOnLoad()">
<form id="frmData" runat="server">
       <table border="0" width= "99%">
        <tbody>
         <tr>
            <td class="headertext3" colspan="1" width="100%">Code Tables</td>
         </tr>
          <td class="center">
            <asp:Button ID="Button1" OnClientClick = "javascript:formHandler1('TableDetail.aspx?mode=new', '', 'new', 'swindow')" runat="server" name="AddTable" Text="Add Table"  class="button" style="width:100"/>  
          </td>            
         <tr>
         <%--bkumarr33: cosmetic Mits 47 ref. Mits 16442--%>
          <td>         
            <asp:DataList ID="editTableList" runat="server" showfooter="False" 
                  showheader="False" Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                  Font-Strikeout="False" Font-Underline="False" Width="100%">
                <ItemTemplate>
                    <table width="100%" >
                        <tr>
                            <td class="full" >
                                <div>
                                    <%# DataBinder.Eval(Container, "DataItem.value")%>                       
                                </div>
                            </td>
                            <td class="full">
                                <div>
                                    <asp:Button ID="editTable" runat="server" 
                                    CommandArgument='<%# DataBinder.Eval(Container, "DataItem.tableid") %>' 
                                    CommandName="edit" OnCommand="editTableList_ItemCommand" text="..." Width="24px" class="EllipsisControl"/>
                                </div>
                            </td>
                        </tr>
                   </table>
                </ItemTemplate>
                    <ItemStyle height="24px" backcolor="white" />
                    <AlternatingItemStyle height="24px" backcolor="WhiteSmoke"/>
            </asp:DataList>                       
          </td>
          <%--end--%>
         </tr>
        </tbody>
        
        <asp:HiddenField ID="hdnTableId" runat="server" />
        
        <asp:textbox style="display: none" runat="server" id="TypeCode" rmxref="Instance/Document/CodeUtility/TypeCode" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="id" rmxref="Instance/Document/CodeUtility/ID" Text="0" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="alpha" rmxref="Instance/Document/CodeUtility/Alpha" Text="" rmxtype="hidden" />
        <asp:textbox style="display: none" runat="server" id="form_name" rmxref="Instance/Document/CodeUtility/FormName" Text="ctnames" rmxtype="hidden" />
       </table>
       <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
    </form>
    </body>
</html>
