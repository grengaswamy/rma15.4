﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CodeListNonBase.aspx.cs" Inherits="Riskmaster.UI.Utilities.CodeListNonBase" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Non Base Code Texts</title>

    <script type="text/javascript" language="JavaScript" src="../../Scripts/TableMaint.js"></script>
    <script type="text/javascript" language="JavaScript" src="../../Scripts/form.js"></script>   
    <script language = "javascript" type = "text/javascript">
        var m_CodeId = 0;
        var m_iLangCode = 0;
        function selectCodeforEdit() {
            var objCtrl = document.getElementById("hdnCodeId");
            var objLangCtrl = document.getElementById("hdnLangCode");
            var objTotalRows = document.getElementById("hdTotalRows");
            if (objTotalRows != null && objTotalRows.value == "0")
                return false;
            if (m_CodeId == 0) {
                alert("Please select one of the codes");
                return false;
            }
            if (objCtrl != null)
                objCtrl.value = m_CodeId;
            if (objLangCtrl != null)
                objLangCtrl.value = m_iLangCode;
            return true;
        }
        function setValuesForCode(codeId,langCode) {
            m_CodeId = codeId;
            m_iLangCode = langCode;
        }
    </script>
    <style type="text/css">
        .style1
        {
            width: 510px;
        }
        .style2
        {
            width: 390px;
        }
        .style3
        {
            width: 108%;
        }
        .center
        {
            text-align: center;
        }
    </style>
</head>
<body>
      <div style="overflow: auto; height: 99%;">
    <form id="frmData" runat="server">
   <table border="0" style="width: 97%;">
        <tbody>
            <tr>
                <td>
                    <table border="0" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td class="headertext3" colspan="1">
                                    Code Texts
                                </td>
                            </tr>
                            <td class="center" colspan="2">
                                <asp:button id="btnAddAdditionalLangDesc" runat="server" 
                                    name="AddAdditionalLangDesc" text="Add New Language Description" class="button"
                                    style="width: 100" />
                                <asp:button id="btnEditCode" runat="server" name="EditCode" text="Edit Code Description" class="button"
                                    style="width: 100"  OnClientClick = "return selectCodeforEdit();" onClick="EditCode"/> 
                                     <asp:Button ID="CancelButton" runat="server" Text="Cancel"  class="button" style="width:100"/>                                                        
          </td>
        <table width="100%" cellspacing="0" cellpadding="4" border="0">
            <tr>
                <td colspan="2">
                    <rmcontrol:ErrorControl runat="server" ID="ErrorControl1" />
                </td>
            </tr>
        </table>
         <tr>
                                <td>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 100%;">
                                                <div style="width: 100%;">
               										<table width="100%" class="noborder" cellspacing="0" cellpadding="0" border="0">
                                                        <tr>
                                                            <td style="width: 100%;">
                                                                
<table width="100%" cellspacing="0" cellpadding="0" border="0" valign="top">
<tr>
<td><asp:Label ID="lblPagerTop" runat="server"></asp:Label></td>

<td align="right">
<asp:Label ID="lblPageRangeTop" runat="server"></asp:Label>
  <asp:LinkButton id="lnkFirstTop"   ForeColor="black" runat="server"  OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> 
  <asp:LinkButton id="lnkPrevTop" runat="server" ForeColor="black" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
    <asp:LinkButton id="lnkNextTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
    <asp:LinkButton id="lnkLastTop" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="4">
									<tr>
										<td class="msgheader" nowrap="" colspan="">
											Search 
										</td>
									</tr>
						
</table>

    <asp:GridView ID="grdCodeMaintNonBase" runat="server" AllowSorting="true" OnRowDataBound="grdCodeMaintNonBase_RowDataBound"
        CellPadding="4" AllowPaging = "false" AutoGenerateColumns = "false" EnableViewState = "true" GridLines="None" Width = "100%" >
        <pagersettings firstpagetext="&quot;First&quot;" lastpagetext="Last" mode="NextPreviousFirstLast"
                                                                        position="Top" nextpagetext="Next" />
                                                                    <headerstyle cssclass="msgheader" />
                                                                    <alternatingrowstyle cssclass="data2" />
                                                                    <columns>
                                                                        <asp:TemplateField  ItemStyle-CssClass="data">
                                                                        <ItemTemplate>
                                                                            <input type="radio" id="selectrdo" name="CodesGrid"  />
                                                                        </ItemTemplate> 
                                                                        </asp:TemplateField>
                                                                        <asp:templatefield headertext="Code" itemstyle-cssclass="data">
                                                                            <itemtemplate>
                                                                                <span id="spanCode" class="data">
                                                                                    <%# DataBinder.Eval(Container, "DataItem.code")%></span> <span style="display: none"
                                                                                        runat="server" id="fileName">
                                                                                        <%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                                                            </itemtemplate>
                                                                        </asp:templatefield>
                                                                        <asp:templatefield headertext="Non Base Language Description" sortexpression="Description" itemstyle-cssclass="data"
                                                                            itemstyle-wrap="false">
                                                                            <itemtemplate>
                                                                                <span id="spanDesc" class="data">
                                                                                    <%# DataBinder.Eval(Container, "DataItem.Description")%></span> <span style="display: none"
                                                                                        runat="server" id="fileName">
                                                                                        <%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                                                            </itemtemplate>
                                                                        </asp:templatefield> 
                                                                         <asp:templatefield headertext="Language" itemstyle-cssclass="data" Visible = "false">
                                                                            <itemtemplate>
                                                                                <span id="spanCode" class="data">
                                                                                    <%# DataBinder.Eval(Container, "DataItem.LanguageCode")%></span> <span style="display: none"
                                                                                        runat="server" id="fileName">
                                                                                        <%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                                                            </itemtemplate>
                                                                        </asp:templatefield>      
                                                                         <asp:templatefield headertext="Language" itemstyle-cssclass="data">
                                                                            <itemtemplate>
                                                                                <span id="spanCode" class="data">
                                                                                    <%# DataBinder.Eval(Container, "DataItem.Language")%></span> <span style="display: none"
                                                                                        runat="server" id="fileName">
                                                                                        <%# DataBinder.Eval(Container, "DataItem.id")%></span>
                                                                            </itemtemplate>
                                                                        </asp:templatefield>                                                                                                                                                                                                                                                                                                                                                         
                                                                    </columns>
    </asp:GridView>
    <div runat="server" id="treeview"></div>


								


<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td><asp:Label ID="lblPagerDown" runat="server"></asp:Label></td>

<td align="right">
<asp:Label ID="lblPageRangeDown" runat="server"></asp:Label>  
  <asp:LinkButton id="lnkFirstDown"   ForeColor="black" runat="server"  OnCommand = "LinkCommand_Click" CommandName="First">First|</asp:LinkButton> <asp:LinkButton id="lnkPrevDown" runat="server" ForeColor="black" OnCommand = "LinkCommand_Click" CommandName="Prev">Previous</asp:LinkButton>
    <asp:LinkButton id="lnkNextDown" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click" CommandName="Next">|Next</asp:LinkButton>
    <asp:LinkButton id="lnkLastDown" ForeColor="black" runat="server" OnCommand = "LinkCommand_Click"  CommandName="Last">|Last</asp:LinkButton>
</td>
</tr>
</table>

    <br></br>
      
    <asp:HiddenField ID="hdCriteriaXml" runat="server"/>
    <asp:HiddenField ID="hdTotalPages" runat="server"/>
    <asp:HiddenField ID="hdCurrentPage" runat="server"/>
    <asp:HiddenField ID="hdPageSize" runat="server"/>
    <asp:HiddenField ID="hdTotalRows" runat="server"/>
                                                            </td>
                        								</tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:TextBox Style="display: none" runat="server" ID="TID" rmxref="" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="TypeCode" rmxref="Instance/Document/CodeUtility/TypeCode"
                        text="3" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="Table" rmxref="Instance/Document/CodeUtility/Table"
                        text="" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="hdnTableId" text="" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="alpha" rmxref="Instance/Document/CodeUtility/Alpha"
                        text="" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="hdnCodeId" text="" rmxtype="hidden" />
                    <asp:textbox style="display: none" runat="server" id="hdnLangCode" text="" rmxtype="hidden" />
    </form>
	</div>
</body>
</html>
