﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.UI.SupervisoryApproval
{
    public partial class PrintReport : System.Web.UI.Page
    {
        XElement objTempElement = null;
        //public XmlDocument Model = null;
        //rsushilaggar MITS 20938 06/03/2010
        //RMA-14719 achouhan3   Modified to get displayed records
        //JIRA RMA-5572 (RMA-14832) ajohari2 
        private string sPrintReportTemplate = "<Message><Authorization>34880aa1-ee69-4766-8c95-2afd5fe434d2</Authorization><Call>" +
         "<Function>SupervisorApprovalAdaptor.CreateReportAllTransactions</Function></Call><Document><SA><Type/><ShowAllItems/><ShowPayeeOnHold/><CurrencyType></CurrencyType></SA></Document></Message>";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    //rsushilaggar MITS 20938 06/03/2010
                    string sTransType = Request.QueryString["TransType"];
                    sPrintReportTemplate = AppHelper.ChangeMessageValue(sPrintReportTemplate, "//Type", sTransType);
                    //RMA-14719 achouhan3   Added to get displayed records
                    if (Request.QueryString["IsShowAllItem"] != null && !String.IsNullOrEmpty(Request.QueryString["IsShowAllItem"]))
                        sPrintReportTemplate = AppHelper.ChangeMessageValue(sPrintReportTemplate, "//ShowAllItems", Request.QueryString["IsShowAllItem"]);

                    //JIRA RMA-5572 (RMA-14832) ajohari2 Start
                    if (Request.QueryString["CurrencyType"] != null && !String.IsNullOrEmpty(Request.QueryString["CurrencyType"]))
                        sPrintReportTemplate = AppHelper.ChangeMessageValue(sPrintReportTemplate, "//CurrencyType", Request.QueryString["CurrencyType"]);
                    //JIRA RMA-5572 (RMA-14832) ajohari2 END

                    //sachin Jira 14871 starts
                    if (Request.QueryString["ShowPayeeOnHold"] != null && !String.IsNullOrEmpty(Request.QueryString["ShowPayeeOnHold"]))
                        sPrintReportTemplate = AppHelper.ChangeMessageValue(sPrintReportTemplate, "//ShowPayeeOnHold", Request.QueryString["ShowPayeeOnHold"]);
                    //sachin Jira 14871 ends
                    string sReturn = AppHelper.CallCWSService(sPrintReportTemplate.ToString());
                    //Model.LoadXml(sReturn);
                    objTempElement = XElement.Parse(sReturn);
                    objTempElement = objTempElement.XPathSelectElement("./Document/SupervisorApproval/File");
                    if (objTempElement != null)
                    {
                        if (objTempElement.Value != "")
                        {
                            byte[] pdfbytes = Convert.FromBase64String(objTempElement.Value);
                            if (pdfbytes != null)
                            {
                                Response.Buffer = true;
                                Response.Clear();
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Charset = "";
                                Response.AppendHeader("Content-Encoding", "none;");
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("Content-Disposition", string.Format("inline; filename=REPORT.PDF"));
                                Response.AddHeader("Accept-Ranges", "bytes");
                                Response.BinaryWrite(pdfbytes);
                                Response.Flush();
                                Response.Close();
                            }
                        }
                    }
                    else
                    {
                        lblNoreport.Visible = true;
                    }
                    //SetValuesFromService();
                    //BindpageControls(sReturn);
                } // if

            } // try
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            } // catch
        }
    }
}
