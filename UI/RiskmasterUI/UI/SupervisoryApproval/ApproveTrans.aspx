
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApproveTrans.aspx.cs" Inherits="Riskmaster.UI.UI.SupervisoryApproval.ApproveTrans" ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/CodeLookUp.ascx" TagName="CodeLookUp" TagPrefix="uc" %>
<%@ Register Src="~/UI/Shared/Controls/CommonTasks.ascx" TagName="CommonTasks" TagPrefix="uc4" %>


<%@ Register TagPrefix="uc" TagName="ErrorControl" Src="~/UI/Shared/Controls/ErrorControl.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Approve Transactions</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/system.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Content/ng-grid.css" />
    <script src="../../Scripts/jquery/jquery-1.8.0.min.js"></script>
    <script src="../../Scripts/jquery/jquery-ui-1.9.2.min.js"></script>
    <script src="../../Scripts/angularjs/angular.min.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/Directives/rma-ng-grid.js"></script>
     <script type="text/javascript" src="../../Scripts/angularjs/lib/ng-grid.debug.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/ng-grid-layout.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/controllers/PaymentApprovalController.js"></script>
    <script type="text/javascript" src="../../Scripts/angularjs/lib/ngGridExportPlugin.js"></script>
    <script type="text/javascript" src="../../Scripts/form.js"></script>
    <script type="text/javascript" src="../../Scripts/WaitDialog.js"> { var i; }</script>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
   
    <script type="text/javascript">
        //rsushilaggar MITS 20938 06/03/2010
        function WindowOpen() {
            //window.open('home?pg=riskmaster/SupervisoryApproval/PrintReport', 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
            var isChkChecked = $("#chkShowAllItem").is(":checked") ? "True" : "False";
            var isChkShowPayeeOnHoldChecked = $("#chkdisplaypayment").is(":checked") ? "true" : "false";   //sachin Jira 14871     
            //JIRA RMA-5572 (RMA-14832) ajohari2 Start
            var CurrencyType = document.getElementById("hdnCurrencyType").value;
            window.open('PrintReport.aspx?TransType=P&IsShowAllItem=' + isChkChecked + '&ShowPayeeOnHold=' + isChkShowPayeeOnHoldChecked + '&CurrencyType=' + CurrencyType, 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 1000) / 2 + ',resizable=yes,scrollbars=yes');//sachin Jira 14871
            //JIRA RMA-5572 (RMA-14832) ajohari2 End
            return false;
        }
        //rsushilaggar MITS 20938 06/03/2010
        function WindowOpenCol() {
            //window.open('home?pg=riskmaster/SupervisoryApproval/PrintReport', 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 290) / 2 + ',left=' + (screen.availWidth - 500) / 2 + ',resizable=yes,scrollbars=yes');
            var isChkChecked = $("#chkShowAllItem").is(":checked") ? "True" : "False";
            var isChkShowPayeeOnHoldChecked = $("#chkdisplaypayment").is(":checked") ? "true" : "false";//sachin Jira 14871
            //JIRA RMA-5572 (RMA-14832) ajohari2 Start
            var CurrencyType = document.getElementById("hdnCurrencyType").value;
            window.open('PrintReport.aspx?TransType=C&IsShowAllItem=' + isChkChecked + '&ShowPayeeOnHold=' + isChkShowPayeeOnHoldChecked + '&CurrencyType=' + CurrencyType, 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 1000) / 2 + ',resizable=yes,scrollbars=yes');//sachin Jira 14871
            //JIRA RMA-5572 (RMA-14832) ajohari2 End
            return false;
        }
        function WindowOpenDenial() {//ajohari2 RMA-13878(RMA 6404 and RMA 9875)
            var isChkShowPayeeOnHoldChecked = $("#chkdisplaypayment").is(":checked") ? "true" : "false";   //sachin Jira 14871     
            //JIRA RMA-5572 (RMA-14832) ajohari2 Start
            var CurrencyType = document.getElementById("hdnCurrencyType").value;
            if ($('#hdnParent').val() == "true") {
                window.open('DenialPrintReport.aspx?IsShowMyTrans=true&ShowPayeeOnHold=' + isChkShowPayeeOnHoldChecked + '&CurrencyType=' + CurrencyType, 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 1000) / 2 + ',resizable=yes,scrollbars=yes');
            }
            else {
                var isChkChecked = $("#chkShowAllItem").is(":checked") ? "True" : "False";
                var isChkShowPayeeOnHoldChecked = $("#chkdisplaypayment").is(":checked") ? "true" : "false";//sachin Jira 14871
                window.open('DenialPrintReport.aspx?IsShowAllItem=' + isChkChecked + '&ShowPayeeOnHold=' + isChkShowPayeeOnHoldChecked + '&CurrencyType=' + CurrencyType, 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 1000) / 2 + ',resizable=yes,scrollbars=yes');
                //window.open('DenialPrintReport.aspx?ShowPayeeOnHold=' + isChkShowPayeeOnHoldChecked, 'Print', 'width=1000,height=600' + ',top=' + (screen.availHeight - 600) / 2 + ',left=' + (screen.availWidth - 1000) / 2 + ',resizable=yes,scrollbars=yes');
            }
            //JIRA RMA-5572 (RMA-14832) ajohari2 End
            return false;
        }

        //Start Debabrata Biswas MCIC r6 Retrofit
        function tabChange(objname) {
            //Start rsushilaggar MITS 22009 DATE 09/27/2010
            var tabSelected = document.getElementById("DefaultTabSelected").value;
            if (tabSelected != "" && objname == "") {
                objname = tabSelected;
            }
            if (tabSelected == "" && objname == "") {
                objname = 'Payments';
            }
            //End rsushilaggar 
            document.forms[0].hTabName.value = objname;
            var tmpName = "";
            var m_TabList = ['Payments', 'CollectionsfromLSS'];

            //Deselect All
            for (i = 0; i < m_TabList.length; i++) {
                tmpName = m_TabList[i];

                if (eval('document.all.FORMTAB' + tmpName) != null)
                    eval('document.all.FORMTAB' + tmpName + '.style.display="none"');

                if (eval('document.all.FORMTABBUTTONLIST' + tmpName) != null)
                    eval('document.all.FORMTABBUTTONLIST' + tmpName + '.style.display="none"');

                if (eval('document.all.FORMTABDENIAL' + tmpName) != null)
                    eval('document.all.FORMTABDENIAL' + tmpName + '.style.display="none"');

                if (eval('document.all.FORMTABBUTTONLISTDENIAL' + tmpName) != null)
                    eval('document.all.FORMTABBUTTONLISTDENIAL' + tmpName + '.style.display="none"');
                //rsushilaggar MITS 21399 04-Aug-2010
                if (eval('document.all.TABS' + tmpName) != null)
                    eval('document.all.TABS' + tmpName + '.className="NotSelected"');
                if (eval('document.all.LINKTABS' + tmpName) != null)
                    eval('document.all.LINKTABS' + tmpName + '.className="NotSelected1"');
            }
            //Select the tab requested
            if (eval('document.all.FORMTAB' + objname) != null)
                eval('document.all.FORMTAB' + objname + '.style.display=""');

            if (eval('document.all.FORMTABBUTTONLIST' + objname) != null)
                eval('document.all.FORMTABBUTTONLIST' + objname + '.style.display=""');

            if (eval('document.all.FORMTABDENIAL' + objname) != null)
                eval('document.all.FORMTABDENIAL' + objname + '.style.display=""');

            if (eval('document.all.FORMTABBUTTONLISTDENIAL' + objname) != null)
                eval('document.all.FORMTABBUTTONLISTDENIAL' + objname + '.style.display=""');

            //rsushilaggar MITS 21399 04-Aug-2010
            if (eval('document.all.TABS' + objname) != null)
                eval('document.all.TABS' + objname + '.className="Selected"');
            if (eval('document.all.LINKTABS' + objname) != null)
                eval('document.all.LINKTABS' + objname + '.className="Selected"');

            document.getElementById("DefaultTabSelected").value = objname;

            return true;
        }


        function TabSelected(ctrl) {
            //Start rsushilaggar MITS 22009 DATE 09/27/2010
            var tabSelected = document.getElementById("DefaultTabSelected").value;
            if (tabSelected != "" && ctrl == "") {
                ctrl = document.getElementById("DefaultTabSelected");
            }
            if (tabSelected == "" && ctrl == null) {
                ctrl = document.getElementById("Payments");
            }
            //End rsushilaggar
            try {
                if (ctrl.className = "Selected") {
                    document.forms[0].SysTabSelected.value = ctrl.name + "**" + ctrl.className;
                }
            } catch (ex) { }
        }

        //End Debabrata Biswas MCIC r6 Retrofit

        function LookupComments(sId) {      //csingh7 for MITS 20092
            var sLink = '../Comments/MainPage.aspx?SysFormName=SupervisorApproval&recordid=' + sId + '&CommentsFlag=true';

            window.open(sLink, 'qdWnd',
            'width=720,height=450' + ',top=' + (screen.availHeight - 400) / 2 + ',left=' + (screen.availWidth - 720) / 2 + ',resizable=yes,scrollbars=yes');
            return false;
        }

        //PJS:MITS # 21392 - Added Reset function to clear the screen
        function Reset() {
            document.forms[0].txtPayeeLastName.value = "";
            document.forms[0].lstPayeeLastName.value = "=";
            document.forms[0].txtPayeeFirstName.value = "";
            document.forms[0].lstPayeeFirstName.value = "=";
            document.forms[0].txtPrimaryClaimantLastName.value = "";
            document.forms[0].lstPrimaryClaimantLastName.value = "=";
            document.forms[0].txtPrimaryClaimantFirstName.value = "";
            document.forms[0].lstPrimaryClaimantFirstName.value = "=";
            document.forms[0].TransDatestart.value = "";
            document.forms[0].TransDateend.value = "";
            document.forms[0].TransDateop.value = "between";
            var PayeeStatus_codelookup = document.getElementById('PayeeStatus_codelookup');
            if (PayeeStatus_codelookup != null)
                PayeeStatus_codelookup.value = "";
        }

        function SubmitQuery(btnSelected) {
            document.getElementById("IsSearchEnabled").value = "true";
            var imgLoadingSearch = $("#pleaseWaitFrame", parent.document.body);
            imgLoadingSearch.css({ display: "block" });
        }
        $(document).ready(function () {
            $('#divSearch').hide();
        });
        function SearchClick() {
            $('#divSearch').slideToggle(2000);
        };
    </script>
    <!--RMA-13876,RMA-13933 achouhan3   fix for scroll issue starts-->
    <style>
    
        .divstyles{
        BORDER-TOP-WIDTH: 0px;
        PADDING-RIGHT: 0px;
        PADDING-LEFT: 0px;
        BORDER-LEFT-WIDTH: 0px;
        BORDER-BOTTOM-WIDTH: 0px;
        PADDING-BOTTOM: 0px;
        PADDING-TOP: 0px;
        FONT-FAMILY: Arial;
        BORDER-RIGHT-WIDTH: 0px;
        font-size: 10pt;
        font-family:Tahoma,Arial,Helvetica,sans-serif;
            }

    </style>
     <!--RMA-13876,RMA-13933 achouhan3   fix for scroll issue Ends-->
</head>
<body onload="parent.MDIScreenLoaded();tabChange('');TabSelected(null);return false;">
    <form id="frmData" name="frmData" runat="server">
        <div ng-app="rmaApp" id="ng-app">
            <input type="hidden" runat="server" id="hdnGridName" value="ApproveTransaction" />
            <rmcontrol:ErrorControl ID="ErrorControl1" runat="server" />
            <%try
              {
            %><% if (Model.SelectSingleNode("//WarningTransactions") != null)
                 {%>
            <td align="left" colspan="4">
                <table>
                    <tr style="font-size: small">
                        <td colspan="2" class="warntextheader">Following Warnings were reported:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="warntext">
                            <%=Model.SelectSingleNode("//WarningTransactions").InnerText%>
                        </td>
                    </tr>
                </table>
            </td>
            <%}%>
            <% if (Model.SelectSingleNode("//ErrorTransactions") != null)
               {%>
            <%foreach (System.Xml.XmlNode node in Model.SelectNodes("//ErrorTransactions"))
              {
                  int iCount = 0; %>
            <tr>
                <td align="left" colspan="4">
                    <font size="4" color="red">
				<%=node.InnerText %>
			</font>
                </td>
            </tr>
            <%iCount++;%>
            <%}%>
            <%         } %>
            <%}
              catch (Exception ee)
              {
                  ErrorHelper.logErrors(ee);
                  lblErrors.Text = ErrorHelper.FormatErrorsForUI("Error getting data for Supervisor Approval Of Funds Transactions");
              }%>
            <%if ((Model.SelectSingleNode("//ApproveTransactionSearch") != null) && Model.SelectSingleNode("//ApproveTransactionSearch").InnerText == "true")
              { %>
            <a href="#" onclick="SearchClick();" style="text-decoration: none !important; color: black !important;" id="lnkSearchView">
                <div style="width: 100%; font-size: 12px;">
                    <img src="../../Images/Search_Criteria.png" />
                   <span id="spnSearchCriteria" runat="server"></span></div>
            </a>

            <div id="divSearch" runat="server" style="padding: 0 0 5px;">
                <table width="100%" cellspacing="0" cellpadding="2" border="0">
                    <tr>
                        <td colspan="10" class="msgheader" bgcolor="#D5CDA4"><span id="spnHeaderPayment" runat="server"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <% if (Model.SelectSingleNode("//PutLSSCollectionOnHold") != null && Model.SelectSingleNode("//PutLSSCollectionOnHold").InnerText == "true")
                               { %>
                            <span id="spnPayeePayorLName" runat="server"></span>:
				
				<% }
                               else
                               {%>
                            <span id="spnPayeeLastName" runat="server"></span>:
			   
				<%}%>
                        </td>
                        <td align="left" width="10%">
                            <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PayeeLastNameComparison"
                                ID="lstPayeeLastName" itemsetref="/Instance/Document/SearchResults/Select">
                                <asp:ListItem Value="=">=</asp:ListItem>
                                <asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;">&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
                                <asp:ListItem Value="&lt;">&lt;</asp:ListItem>
                                <asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/PayeeLastName"
                                size="30" ID="txtPayeeLastName" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <% if (Model.SelectSingleNode("//PutLSSCollectionOnHold") != null && Model.SelectSingleNode("//PutLSSCollectionOnHold").InnerText == "true")
                               { %>
                            <span id="spnPayeePayorFName" runat="server"></span>:
				<% }
                               else
                               {%>
                            <span id="spnPayeeFName" runat="server"></span>:
				<%} %>
                        </td>
                        <td align="left" width="5%">
                            <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PayeeFirstNameComparison"
                                ID="lstPayeeFirstName" itemsetref="/Instance/Document/SearchResults/Select">
                                <asp:ListItem Value="=">=</asp:ListItem>
                                <asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;">&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
                                <asp:ListItem Value="&lt;">&lt;</asp:ListItem>
                                <asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/PayeeFirstName"
                                size="30" ID="txtPayeeFirstName" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span id="spnPrimaryClmntLName" runat="server"></span>:
                        </td>
                        <td align="left" width="5%">
                            <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PrimaryClaimantLastNameComparison"
                                ID="lstPrimaryClaimantLastName" itemsetref="/Instance/Document/SearchResults/Select">
                                <asp:ListItem Value="=">=</asp:ListItem>
                                <asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;">&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
                                <asp:ListItem Value="&lt;">&lt;</asp:ListItem>
                                <asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/PrimaryClaimantLastName"
                                size="30" ID="txtPrimaryClaimantLastName" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span id="spnPrimaryClmntFName" runat="server"></span>:
                        </td>
                        <td align="left" width="5%">
                            <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PrimaryClaimantFirstNameComparison"
                                ID="lstPrimaryClaimantFirstName" itemsetref="/Instance/Document/SearchResults/Select">
                                <asp:ListItem Value="=">=</asp:ListItem>
                                <asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;">&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
                                <asp:ListItem Value="&lt;">&lt;</asp:ListItem>
                                <asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/PrimaryClaimantFirstName"
                                size="30" ID="txtPrimaryClaimantFirstName" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span id="spnTransactionDate" runat="server"></span>:
                        </td>
                        <td align="left" width="5%">
                            <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/FromDateComparison"
                                ID="TransDateop" itemsetref="/Instance/Document/SearchResults/Select">
                                <asp:ListItem Value="between">Between</asp:ListItem>
                                <asp:ListItem Value="=">=</asp:ListItem>
                                <asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;">&gt;</asp:ListItem>
                                <asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
                                <asp:ListItem Value="&lt;">&lt;</asp:ListItem>
                                <asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/FromDate"
                                value="" onblur="dateLostFocus(this.id);" ID="TransDatestart" size="12" TabIndex="1" />
                            <%--<input type="button" class="DateLookupControl" value="..." name="btnoneTransDate" id="btnoneTransDate" />

				<script type="text/javascript">
				    Zapatec.Calendar.setup(
									{
									    inputField: "TransDatestart",
									    ifFormat: "%m/%d/%Y",
									    button: "btnoneTransDate"
									}
									);
				</script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#TransDatestart").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../Images/calendar.gif",
                                        // buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "2");//vkumar258 ML Changes
                                });
                            </script>

                            <asp:TextBox runat="server" type="text" rmxref="/Instance/Document/SearchParam/ToDate"
                                value="" ID="TransDateend" size="12" TabIndex="3" />
                            <%--<input type="button" class="DateLookupControl" value="..." name="btntwoTransDate" id="btntwoTransDate" />

				<script type="text/javascript">
				    Zapatec.Calendar.setup(
									{
									    inputField: "TransDateend",
									    ifFormat: "%m/%d/%Y",
									    button: "btntwoTransDate"
									}
									);
				</script>--%>
                            <script type="text/javascript">
                                $(function () {
                                    $("#TransDateend").datepicker({
                                        showOn: "button",
                                        buttonImage: "../../Images/calendar.gif",
                                        //buttonImageOnly: true,
                                        showOtherMonths: true,
                                        selectOtherMonths: true,
                                        changeYear: true
                                    }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");//vkumar258 ML Changes
                                });
                            </script>

                        </td>
                    </tr>
                    <%if ((Model.SelectSingleNode("//UseEntityPaymentApproval") != null) && Model.SelectSingleNode("//UseEntityPaymentApproval").InnerText == "true")
                      { %>
                    <tr>
                        <td align="left" width="20%"><span id="spnPayeeApprovalSts" runat="server"></span>:
                        </td>
                        <td align="left" width="5%">
                            <asp:DropDownList runat="server" type="combobox" rmxref="/Instance/Document/SearchParam/PayeeStatusComparison"
                                ID="lstPayeeStatus" itemsetref="/Instance/Document/SearchResults/Select">
                                <asp:ListItem Value="=">=</asp:ListItem>
                                <asp:ListItem Value="&lt;&gt;">&lt;&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <uc:codelookup runat="server" onchange="setDataChanged(true);"
                                id="PayeeStatus" codetable="ENTITY_APPRV_REJ" controlname="PayeeStatus"
                                rmxref="/Instance/Document/SearchParam/PayeeStatus" rmxtype="code" tabindex="20" />
                        </td>
                    </tr>
                    <%} %>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" OnClientClick="SubmitQuery('search');" Text=""
                                class="button" />
                            <asp:Button runat="server" type="button" ID="reset" Text="" OnClientClick="Reset();return false;" class="button" />
                        </td>
                    </tr>
                </table>
            </div>
            <%} %>

            <div id="divPayment" ng-controller="supervisoryApprovalController">
                <div class="tabGroup" id="TabsDivGroup" runat="server">
                    <asp:HiddenField ID="hTabName" runat="server" Value='' />
                    <asp:HiddenField ID="SysTabSelected" runat="server" Value='' />
                    <asp:HiddenField ID="DefaultTabSelected" runat="server" Value='' />
                    <div class="Selected" nowrap="true" runat="server" name="TABSPayments"
                        id="TABSPayments">
                        <a class="Selected" href="#" runat="server" onclick="tabChange(this.name);TabSelected(this);return false;"
                            rmxref="" name="Payments" id="LINKTABSPayments">Payments</a>
                    </div>
                    <div class="tabSpace" runat="server" id="TABSPayments_">
                        <nbsp />
                        <nbsp />
                    </div>
                    <!-- start rsushilaggar MITS 20938 06/02/2010 -->
                    <% if (Model.SelectSingleNode("//PutLSSCollectionOnHold") != null && Model.SelectSingleNode("//PutLSSCollectionOnHold").InnerText == "true")
                       { %>
                    <div class="NotSelected" nowrap="true" runat="server" name="TABSCollectionsfromLSS"
                        id="TABSCollectionsfromLSS">
                        <a class="NotSelected1" href="#" runat="server" onclick="tabChange(this.name);TabSelected(this);return false;"
                            rmxref="" name="CollectionsfromLSS" id="LINKTABSCollectionsfromLSS"></a>
                    </div>
                    <div class="tabSpace" runat="server" id="TABSCollectionsfromLSS_">
                        <nbsp />
                        <nbsp />
                    </div>
                    <!-- end rsushilaggar MITS 20938 06/02/2010 -->
                    <%} %>

                    <table style="float: right;">
                        <tr>
                            <td>
                                <div id="divShowItems" style="float: left;" runat="server">
                                    <input id="chkShowAllItem"type="checkbox" ng-model="paymentModel.isShowAllItems" ng-change="GetAllItems()" />
                                    <span id="spnShowAllItem" runat="server" style="width: 100%"></span>
                                </div>
                            </td>
                            <td>
                                <!--RMA-7810 -->
                                <div id="divDisplayPayeeStatus" style="float: right;" runat="server">
                                    <%--<asp:CheckBox ID="chkdisplaypayment"  RMXRef="Instance/Funds/Display" onchange="setDataChanged(true);" runat ="server" checked ="false" AutoPostBack="true" OnCheckedChanged="chkdisplaypayment_CheckedChanged"/>--%>
                                    <input id="chkdisplaypayment" type="checkbox" ng-model="paymentModel.isPayeeStatus" ng-change="GetAllItems()" />
                                    <span id="spnDisplayStatus" runat="server"></span>
                                </div>
                            </td>
                            <td>
                                <!--RMA-9875        achouhan3   Added for Show my transaction-->
                                <div id="divCheckItems" style="float: right;" runat="server">
                                    <input id="chkTrans" type="checkbox" ng-model="paymentModel.isShowMyTrans" ng-change="ShowMyTrans()" />
                                    <span id="lblPendingItems" runat="server"></span>
                                </div>
                            </td>
                        </tr>
                    </table>

                </div>
                 <%--//JIRA RMA-5572 (RMA-14832) ajohari2 START--%>
                <div style="float: right;" >
                     <table>
                        <tr runat="server" id="trCurrencyType">
                            <td colspan="2">
                                
                                    <asp:Label runat="server" class="label" ID="lblCurrencyType" Text="Currency Type:" />
                                    <span class="formw">
                                        <select id="drdCurrencyType" name ="drdCurrencyType" runat="server" ng-model="paymentModel.isCurrencyType" ng-change="GetAllItems()">
                                      
                                         </select>
                                    </span>
                                
                            </td>
                        </tr>
                         </table>
                </div>
                <%-- //JIRA RMA-5572 (RMA-14832) ajohari2 END--%>

                <div id="FORMTABPayments">
                    <table width="100%" id="FORMTABPayments_" cellspacing="0" cellpadding="2"
                        border="0">
                        <tr>
                            <td class="msgheader" bgcolor="#D5CDA4"><span id="spnMsgHeader" runat="server"></span>
                            </td>
                        </tr>
                        <%--  <% if (!String.IsNullOrEmpty(hdnJsonData_gridPaymentApproval.Value.Trim()) && hdnJsonData_gridPaymentApproval.Value.Trim() != "[]")
                           {
                               //if(Model.SelectSingleNode("//Transaction").Attributes["UtilitySetting"].Value == "H")
                               //{
                        %>--%>

                        <tr class="colheader3">
                            <!--ctrlgroup-->
                            <td><span id="spnFundsTransPayment" runat="server"></span>
                            </td>
                        </tr>
                        
                        <%--   <%} %>--%>
                    </table>
                     <!--RMA-13876,RMA-13933 achouhan3   fix for scroll issue starts-->
                    <div style="clear:both;"></div>
                    <div id="divrmaPaymentGrid" class="divstyles">
                                 <input type="hidden" id="hdnJsonData_gridPaymentApproval" runat="server" />
                                <input type="hidden" id="hdnJsonUserPref_gridPaymentApproval" runat="server" />
                                <input type="hidden" id="hdnJsonAdditionalData_gridPaymentApproval" runat="server" />
                                <rma-grid id="gridPaymentApproval" name="PaymentTransaction" multiselect="selectionModel.enableMultiSelect" showselectioncheckbox="selectionModel.enableSelectionCheckBox" ></rma-grid>

                    </div>

                </div>
                <div id="FORMTABBUTTONLISTPayments">
                    <table width="100%" id="FORMTABBUTTONLISTPayments_" cellspacing="0" cellpadding="2"
                        border="0">
                        <tr class="ctrlgroup">
                            <td colspan="10"></td>

                        </tr>
                        <%--Added by Amitosh for R8 enhancement of OverRide Authority--%>
                        <section id="secOperation" runat="server">
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_overrideamount" Text="">:</asp:Label>
                                    &nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox runat="Server" ID="tboverrideamount" rmxref="Instance/Funds/OverRideAmount" onchange="setDataChanged(true);" MaxLength="10" onblur="numLostFocus(this);" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_applyoverride" Text=""></asp:Label>
                                    &nbsp;&nbsp;
                                </td>
                                <td>
                                    <!--RMA-13492     achouhan3   Added to corrrect override validation-->
                                    <input type="checkbox" runat="server" id="applyoverride" ng-model="isAllowOverride"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lbl_applyoffset" Text=""></asp:Label>
                                    &nbsp;&nbsp;
                                </td>
                                <td>
                                     <!--RMA-13492     achouhan3   Added to corrrect override validation-->
                                    <input type="checkbox" runat="server" id="applyoffset" ng-model="isAllowOffset"/>
                                </td>


                            </tr>
                            <%--Working on JIRA-11705--%>
                            <tr>
                                <td>
                                    <%--Working on JIRA-18270 start--%>
                                    <asp:label runat="server" class="label" id="lblVoidcodereason" text="" />
                                    <%--Working on JIRA-18270 end--%>


                                </td>
                                <td>
                                    <asp:dropdownlist id="voidcodereason" runat="server" onchange="setDataChanged(true);VoidCodeReasonChanged(this);GetSelectedTextValue(this)"></asp:dropdownlist>
                                </td>
                                 <%--Working on JIRA-18229 start--%>
                                <asp:textbox style="display: none" runat="server" id="loginname" />
                                <asp:textbox style="display: none" runat="server" id="prefixdatetimestamp" />
                                 <%--Working on JIRA-18229 end--%>
                            </tr>
                            <%--End Working on JIRA-11705--%>
                        </section>
                        <%--End amitosh--%>
                        <tr class="datatd">
                            <%--<td colspan="5" width="80%">
				</td>--%>
                            <section id="secVoidReason" runat="server">
                                <td><u>
                                    <asp:Label runat="server" ID="lbl_txtVoidReason" Text=""></asp:Label>
                                </u>&nbsp;&nbsp;</td>
                                <td>
                                    <span class="formw">
                                        <asp:TextBox runat="Server" ID="voidreason" rmxref="Instance/Funds/VoidReason" rmxtype="memo" ReadOnly="true" Style="background-color: #F2F2F2;" onchange="setDataChanged(true);" TabIndex="29" TextMode="MultiLine" Columns="30" Rows="3" />
                                        <asp:Button runat="server" class="MemoButton" name="voidreasonbtnMemo" ID="voidreasonbtnMemo" OnClientClick="return EditHTMLMemo('voidreason','yes');" />
                                        <asp:TextBox Style="display: none" runat="server" rmxref="Instance/Funds/VoidReason_HTMLComments" ID="voidreason_HTML" />
                                    </span>
                                </td>
                            </section>
                            <!-- Commented by Shivendu for MITS 18517. Deny button should always be visible. -->
                            <%--<% if (Model.SelectSingleNode("//Transaction/@UtilitySetting").Value == "Q")
			   {%>--%>
                            <td id="tddeny" runat="server" width="5%">
                                <input type="button" value="" id="btnDeny" runat="server" class="button" style="width: 100px" ng-click="PaymentAction('Deny')" />
                                <%-- <asp:button runat="server" id="btnDeny" class="button" style="width: 100px" text="Deny"
                                ng-click="Test()" />--%>
                            </td>
                            <%-- <%}%>--%>
                            <td id="tdapprove" runat="server" width="5%">
                                <input type="button" value="" runat="server" id="btnApprove" class="button" style="width: 100px" ng-click="PaymentAction('Approve')" />
                                <%-- <asp:button runat="server" id="btnApprove" class="button" style="width: 100px" text="Approve"
                                ng-click="Test()" />--%>
                            </td>
                            <td id="tdvoid" runat="server" width="5%">
                                <input type="button" value="" id="btnVoid" runat="server" class="button" style="width: 100px" ng-click="PaymentAction('Void')" />
                                <%--<asp:button runat="server" id="btnVoid" class="button" style="width: 100px" text="Void"
                                ng-click="Test()" />--%>
                            </td>
                            <td width="5%" >
                                <!--RMA-13502     achouhan3   Added fot print funcationality-->
                                <input type="button" value="" runat="server" id="btnPrint" class="button" style="width: 100px" />
                                <%--<asp:Button runat="server" ID="btnPrint" class="button" Style="width: 100px" Text="" />--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- start rsushilaggar MITS 20938 06/02/2010 -->
           <% if (Model.SelectSingleNode("//PutLSSCollectionOnHold") != null && Model.SelectSingleNode("//PutLSSCollectionOnHold").InnerText == "true")
			   {%>
            
            <div id="divCollection" ng-controller="collectionController">
                <div id="FORMTABCollectionsfromLSS" style="display: none">
                    <table width="100%" id="FORMTABCollectionsfromLSS_" cellspacing="0" cellpadding="2"
                        border="0">
                        <tr>
                            <td class="msgheader" bgcolor="#D5CDA4">
                                <%--Supervisor Approval Of Funds Transactions--%>
                                <span id="spnHeaderCollection" runat="server"></span>
                            </td>
                        </tr>
                        <%--     <% if (!String.IsNullOrEmpty(hdnJsonData_gridCollectionApproval.Value.Trim()) && hdnJsonData_gridCollectionApproval.Value.Trim() != "[]")
                       {
                           //if(Model.SelectSingleNode("//Transaction").Attributes["UtilitySetting"].Value == "H")
                           //{
                    %>--%>

                        <tr class="colheader3">
                            <!--ctrlgroup-->
                            <td>
                                <%--Funds Transactions Available For Approval, Void, Or Denial--%>
                                <span id="spnCollectionText" runat="server"></span>
                            </td>
                        </tr>
                    </table>
                     <!--RMA-13876,RMA-13933 achouhan3   fix for scroll issue starts-->
                    <div style="clear:both;"></div>
                    <div id="divrmaCollectionGrid" class="divstyles">
                    <input type="hidden" id="hdnJsonData_gridCollectionApproval" runat="server" />
                                <input type="hidden" id="hdnJsonUserPref_gridCollectionApproval" runat="server" />
                                <input type="hidden" id="hdnJsonAdditionalData_gridCollectionApproval" runat="server" />
                                <rma-grid id="gridCollectionApproval" name="CollectionTransaction" multiselect="selectionModel.enableMultiSelect" showselectioncheckbox="selectionModel.enableSelectionCheckBox" ></rma-grid>
                        </div>
                </div>
                <div id="FORMTABBUTTONLISTCollectionsfromLSS" style="display: none">
                    <table width="100%" id="FORMTABBUTTONLISTCollectionsfromLSS_" cellspacing="0" cellpadding="2"
                        border="0">
                        <tr class="ctrlgroup">
                            <td colspan="10"></td>

                        </tr>
                        <tr class="datatd">
                            <td colspan="5" width="80%"></td>
                            <!-- Commented by Shivendu for MITS 18517. Deny button should always be visible. -->
                            <%--<% if (Model.SelectSingleNode("//Transaction/@UtilitySetting").Value == "Q")
			   {%>--%>
                            <section id="secColButton" runat="server">
                                <td width="5%">
                                    <input type="button" value="" id="btnColDeny" runat="server" class="button" style="width: 100px" ng-click="CollectionAction('Deny')" />
                                    <%--  <asp:Button runat="server" ID="btnColDeny" class="button" Style="width: 100px" Text="Deny" ng-click="CollectionAction('Deny')"/>--%>
                                </td>
                                <%-- <%}%>--%>
                                <td width="5%">
                                    <input type="button" value="" id="btnColApprove" runat="server" class="button" style="width: 100px" ng-click="CollectionAction('Approve')" />
                                    <%--  <asp:Button runat="server" ID="btnColApprove" class="button" Style="width: 100px" Text="Approve" ng-click="CollectionAction('Approve')"/>--%>
                                </td>
                                <td width="5%">
                                    <input type="button" value="" id="btnColVoid" runat="server" class="button" style="width: 100px" ng-click="CollectionAction('Void')" />
                                    <%--<asp:Button runat="server" ID="btnColVoid" class="button" Style="width: 100px" Text="Void" ng-click="CollectionAction('Void')"/>--%>
                                </td>
                            </section>
                            <td width="5%">
                                <!--RMA-13502     achouhan3   Added fot print funcationality-->
                                <input type="button" value="" runat="server" id="btnColPrint" class="button" style="width: 100px" />
                                <%--<asp:Button runat="server" ID="btnColPrint" class="button" Style="width: 100px" Text="" />--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
                
        <%} %>
            <% if (Model.SelectSingleNode("//ErrorTransactions") == null && ((Model.SelectSingleNode("//SupervisorApproval//DenyTransactions//IsQueuePayment") != null && Model.SelectSingleNode("//SupervisorApproval//DenyTransactions//IsQueuePayment").InnerText == "True") || (!string.IsNullOrEmpty(AppHelper.GetValue("MyTrans")) && AppHelper.GetValue("MyTrans")=="true")))
               {%>
            <div id="divDenyPayment" ng-controller="denyPaymentController">
                <div id="FORMTABDENIALPayments">
                    <table width="100%" id="FORMTABDENIALPayments_" cellspacing="0" cellpadding="2"
                        border="0">
                        <tr class="colheader3">
                            <td><span id="spnDenialHeader" runat="server"></span>
                            </td>
                        </tr>
                    </table>
                     <!--RMA-13876,RMA-13933 achouhan3   fix for scroll issue starts-->
                    <div style="clear:both"></div>
                    <div class="divstyles">
                        <input type="hidden" id="hdnJsonData_gridDenialPayment" runat="server" />
                                <input type="hidden" id="hdnJsonUserPref_gridDenialPayment" runat="server" />
                                <input type="hidden" id="hdnJsonAdditionalData_gridDenialPayment" runat="server" />
                                <rma-grid id="gridDenialPayment" name="DeniedTransaction" multiselect="selectionModel.enableMultiSelect" showselectioncheckbox="selectionModel.enableMultiSelect" ></rma-grid>

                    </div>
                </div>

                <div id="FORMTABBUTTONLISTDENIALPayments">
                    <table width="100%" id="FORMTABBUTTONLISTDENIALPayments_" cellspacing="0"
                        cellpadding="2" border="0">
                        <tr class="ctrlgroup">
                            <td colspan="10"></td>
                        </tr>
                        <tr class="datatd">
                            <td colspan="7" width="90%"></td>
                            <% if (Model.SelectSingleNode("//ErrorTransactions") == null && !String.IsNullOrEmpty(hdnJsonData_gridDenialPayment.Value.Trim()) && hdnJsonData_gridDenialPayment.Value.Trim() != "[]")
                               {%>
                            <td width="5%">
                                <input type="button" value="" id="btnDenialDeny" runat="server" class="button" style="width: 100px" ng-click="PaymentAction('DenialDeny')" />
                                <%-- <asp:Button runat="server" ID="btnDenialDeny" class="button" Style="width: 100px"
                                    Text="Deny" />--%>
                            </td>
                            <% } %>
                            <td width="5%">
                                <!--RMA-13502     achouhan3   Added fot print funcationality-->
                                <%-- <asp:Button runat="server" ID="btnDenialPrint" class="button" Style="width: 100px"
                                    Text="" />--%>
                                <input type="button" value="" runat="server" id="btnDenialPrint" class="button" style="width: 100px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <%} %>


            <% if (Model.SelectSingleNode("//Transaction") != null)
               {%> <%Model = null;%> <%} %>
            <table>
                <tr id="trReason">
                    <td width="20%"><span id="spnReason" runat="server"></span>:   </td>
                    <td>
                        <asp:TextBox ID="reason" onchange="setDataChanged(true);" size="40" MaxLength="255" type="text" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblErrors" runat="server"></asp:Label>
            <asp:TextBox ID="hdnParent" runat="server" type="text" rmxref="/Instance/Document/SearchParam/ShowMyTrans" EnableViewState="true" Style="display: none" />
             <asp:CheckBox ID="chkHiddenShowAllItem" runat="server" rmxref="/Instance/Document/SearchParam/ShowAllItems"   Style="display: none"/>
            <asp:TextBox ID="IsSearchEnabled" runat="server" type="text" rmxref="/Instance/Document/SearchParam/SearchEnabled" EnableViewState="true" Style="display: none" />
            <asp:TextBox ID="hdnLangCode" runat="server" type="text" rmxref="/Instance/Document/SearchParam/LangCode" Style="display: none" />
            <asp:TextBox ID="txtContent" runat="server" type="text" rmxref="/Instance/Document/SearchParam/ContentType" Style="display: none" />
            <asp:TextBox ID="txtGridID" runat="server" type="text" rmxref="/Instance/Document/SearchParam/GridId" Style="display: none" />
            <asp:TextBox ID="txtPageName" runat="server" type="text" rmxref="/Instance/Document/SearchParam/PageName" Style="display: none" />
            <input type="hidden" id="hdnEntityApprovalFlag"  value="" runat="server"/>
            <asp:TextBox ID="hdnCurrencyType" runat="server" type="text" rmxref="/Instance/Document/SearchParam/CurrencyType" Style="display: none" /><%--//JIRA RMA-5572 (RMA-14832) ajohari2 --%>
            <input type="hidden" id="hdnUseMultiCurrency"  value="" runat="server"/><%--//JIRA RMA-5572 (RMA-14832) ajohari2 --%>
            <input type="hidden" id="hdnVoidReasonReqd"  value="" runat="server"/>  <%--rkaur27 : RMA-18653--%>
        </div>

    </form>
</body>
</html>
