﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Xml.XPath;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.UI.UI.SupervisoryApproval
{
    public partial class PayeeCheckReview : NonFDMBasePageCWS
    {
        private string sCWSresponse;
        private bool bReturnStatus;
        int m_tabindex = 1;
        int pagenumber_tab1 = 1;
        int pagenumber_tab2 = 1;
        int pagenumber_tab3 = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    XElement oMessageElement = GetMessageTemplate();
                    tabindex.Text = m_tabindex.ToString();
                    oMessageElement.XPathSelectElement("./Document/Review/TabIndex").Value = "1";
                    oMessageElement.XPathSelectElement("./Document/Review/PageNumber").Value = "1";

                    bReturnStatus = CallCWS("SupervisorApprovalAdaptor.GetPayeeCheckData", oMessageElement, out sCWSresponse, false, false);

                    if (bReturnStatus)
                        BindPageControls(1);
                    
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"<Message>
                                                    <Authorization></Authorization> 
                                                    <Call>
                                                        <Function></Function> 
                                                    </Call>
                                                    <Document>
                                                       <Review> 
                                                        <TabIndex></TabIndex>
                                                        <PageNumber></PageNumber>
                                                        <TotalNoOfPages></TotalNoOfPages>
                                                        <PayeeReleaseList></PayeeReleaseList>
                                                        <CheckReleaseList></CheckReleaseList>
                                                        <BatchReleaseList></BatchReleaseList>
                                                       </Review> 
                                                    </Document>
                                                  </Message>");

            return oTemplate;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            
            try
            {
                XElement oMessageElement = GetMessageTemplate();


                if (tabindex.Text != null && tabindex.Text != "")
                    m_tabindex = Convert.ToInt32(tabindex.Text);

                oMessageElement.XPathSelectElement("./Document/Review/TabIndex").Value = m_tabindex.ToString();

                PopulateXmlIn(m_tabindex, ref oMessageElement);

                bReturnStatus = CallCWS("SupervisorApprovalAdaptor.SaveAndReleasePayeeCheckData", oMessageElement, out sCWSresponse, false, false);
                
                
                if (ErrorHelper.IsCWSCallSuccess(sCWSresponse))
                {
                        oMessageElement = GetMessageTemplate();
                        oMessageElement.XPathSelectElement("./Document/Review/TabIndex").Value = m_tabindex.ToString();

                        PopulateXmlIn(m_tabindex, ref oMessageElement);

                        DestroyViewStates(m_tabindex);

                        bReturnStatus = CallCWS("SupervisorApprovalAdaptor.GetPayeeCheckData", oMessageElement, out sCWSresponse, false, false);


                        if (ErrorHelper.IsCWSCallSuccess(sCWSresponse))
                        {
                            BindPageControls(m_tabindex);
                            
                        }
                    
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                switch (m_tabindex)
                {
                    case 1:
                        ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
                        break;
                    case 2:
                        ErrorControl2.errorDom = ErrorHelper.formatUIErrorXML(err);
                        break;
                    case 3:
                        ErrorControl3.errorDom = ErrorHelper.formatUIErrorXML(err);
                        break;
                    default:
                        break;
                }
            }

        }
        protected void TabButton1_Click(object sender, EventArgs e)
        {
            XmlDocument objxmldoc = null;

            try
            {

                m_tabindex = Convert.ToInt32(tabindex.Text);

                XElement oMessageElement = GetMessageTemplate();

                oMessageElement.XPathSelectElement("./Document/Review/TabIndex").Value = m_tabindex.ToString();
                oMessageElement.XPathSelectElement("./Document/Review/PageNumber").Value = Tab1PageNumber.Text.ToString();
                bReturnStatus = CallCWS("SupervisorApprovalAdaptor.GetPayeeCheckData", oMessageElement, out sCWSresponse, false, false);

                
                if (ErrorHelper.IsCWSCallSuccess(sCWSresponse))
                {
                    BindPageControls(m_tabindex);
                    
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        protected void TabButton2_Click(object sender, EventArgs e)
        {
            string buttonId = string.Empty;
            int PageNum = -1;
            
            try
            {
                buttonId = ((Control)sender).ID.ToString();

                if (ViewState["Tab2PageNumber"] != null && ViewState["Tab2PageNumber"].ToString() != "")
                    Tab2PageNumber.Text = ViewState["Tab2PageNumber"].ToString();


                if (buttonId == "LinkButton1") //First Button
                {
                    Tab2PageNumber.Text = "1";
                    tabindex.Text = "2";
                    DestroyViewStates(2);
                }
                else if (buttonId == "LinkButton2") // Next Button
                {
                    PageNum = Convert.ToInt32(ViewState["Tab2PageNumber"].ToString()) + 1;
                    Tab2PageNumber.Text = PageNum.ToString();
                    tabindex.Text = "2";
                    DestroyViewStates(2);
                }
                else if (buttonId == "LinkButton3") //Previous Button
                {
                    PageNum = Convert.ToInt32(ViewState["Tab2PageNumber"].ToString()) - 1;
                    Tab2PageNumber.Text = PageNum.ToString();
                    tabindex.Text = "2";
                    DestroyViewStates(2);
                }
                else if (buttonId == "LinkButton4") //Last Button
                {
                    Tab2PageNumber.Text = ViewState["Tab2LastPage"].ToString();
                    tabindex.Text = "2";
                    DestroyViewStates(2);
                }

                m_tabindex = Convert.ToInt32(tabindex.Text);

                XElement oMessageElement = GetMessageTemplate();

                oMessageElement.XPathSelectElement("./Document/Review/TabIndex").Value = m_tabindex.ToString();

                if (Tab2PageNumber.Text != "")
                    oMessageElement.XPathSelectElement("./Document/Review/PageNumber").Value = Tab2PageNumber.Text.ToString();
                else
                    oMessageElement.XPathSelectElement("./Document/Review/PageNumber").Value = "1";

                bReturnStatus = CallCWS("SupervisorApprovalAdaptor.GetPayeeCheckData", oMessageElement, out sCWSresponse, false, false);

                
                if (ErrorHelper.IsCWSCallSuccess(sCWSresponse))
                {
                    BindPageControls(m_tabindex);
                    
                }

            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl2.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        protected void TabButton3_Click(object sender, EventArgs e)
        {
            string buttonId = string.Empty;
            int PageNum = -1;
            XmlDocument objxmldoc = null;
            try
            {
                buttonId = ((Control)sender).ID.ToString();

                if (ViewState["Tab3PageNumber"] != null && ViewState["Tab3PageNumber"].ToString() != "")
                    Tab3PageNumber.Text = ViewState["Tab3PageNumber"].ToString();

                if (buttonId == "LinkButton5") //First Button
                {
                    Tab3PageNumber.Text = "1";
                    tabindex.Text = "3";
                    DestroyViewStates(3);
                }
                else if (buttonId == "LinkButton6") // Next Button
                {
                    PageNum = Convert.ToInt32(ViewState["Tab3PageNumber"].ToString()) + 1;
                    Tab3PageNumber.Text = PageNum.ToString();
                    tabindex.Text = "3";
                    DestroyViewStates(3);
                }
                else if (buttonId == "LinkButton7") //Previous Button
                {
                    PageNum = Convert.ToInt32(ViewState["Tab3PageNumber"].ToString()) - 1;
                    PageNum = Convert.ToInt32(Tab3PageNumber.Text) - 1;
                    Tab3PageNumber.Text = PageNum.ToString();
                    tabindex.Text = "3";
                    DestroyViewStates(3);
                }
                else if (buttonId == "LinkButton8") //Last Button
                {
                    Tab3PageNumber.Text = ViewState["Tab3LastPage"].ToString();
                    tabindex.Text = "3";
                    DestroyViewStates(3);
                }

                m_tabindex = Convert.ToInt32(tabindex.Text);

                XElement oMessageElement = GetMessageTemplate();

                oMessageElement.XPathSelectElement("./Document/Review/TabIndex").Value = m_tabindex.ToString();

                if (Tab3PageNumber.Text != "")
                    oMessageElement.XPathSelectElement("./Document/Review/PageNumber").Value = Tab3PageNumber.Text.ToString();
                else
                    oMessageElement.XPathSelectElement("./Document/Review/PageNumber").Value = "1";

                bReturnStatus = CallCWS("SupervisorApprovalAdaptor.GetPayeeCheckData", oMessageElement, out sCWSresponse, false, false);

                if (ErrorHelper.IsCWSCallSuccess(sCWSresponse))
                {
                    BindPageControls(m_tabindex);
                    
                }
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl3.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        protected void BindPageControls(int tab_index)
        { 
            if(tab_index == 1)
            {
                //Bind the Payee List Grid
                BindPayeeCheckGridData();
            }
            else if( tab_index == 2)
            {
                //Bind the Check Grid
                BindCheckGridData();
            }
            else if (tab_index == 3)
            {
                //Bind the AutoCheck Grid
                BindAutoCheckGridData();
            }
        }
        protected void BindPayeeCheckGridData()
        {
            XmlDocument ObjOut = new XmlDocument();
            XmlNode PageValues = null;

            try
            {
                ObjOut.LoadXml(sCWSresponse);

                DataTable GridData = new DataTable();

                GridData.Columns.Add("EntityId");
                GridData.Columns.Add("DttmFreeze");
                GridData.Columns.Add("LastName");
                GridData.Columns.Add("FirstName");
                GridData.Columns.Add("TaxId");
                GridData.Columns.Add("Type");
                GridData.Columns.Add("OfficePhone");
                GridData.Columns.Add("HomePhone");
                GridData.Columns.Add("BirthDate");

                DataRow objRow = null;

                XmlNodeList xmlNodeList = ObjOut.SelectNodes("//Payee");

                foreach (XmlNode XmlNode in xmlNodeList)
                {
                    objRow = GridData.NewRow();

                    objRow["EntityId"] = XmlNode.Attributes["EntityId"].Value.ToString();
                    objRow["DttmFreeze"] = XmlNode.Attributes["Dttm_Freeze"].Value.ToString();
                    objRow["LastName"] = XmlNode.Attributes["Last_Name"].Value.ToString();
                    objRow["FirstName"] = XmlNode.Attributes["First_Name"].Value.ToString();
                    objRow["Type"] = XmlNode.Attributes["Type"].Value.ToString();
                    objRow["TaxId"] = XmlNode.Attributes["Tax_Id"].Value.ToString();
                    objRow["OfficePhone"] = XmlNode.Attributes["Office_Phone"].Value.ToString();
                    objRow["HomePhone"] = XmlNode.Attributes["Home_Phone"].Value.ToString();
                    if (!string.IsNullOrEmpty(XmlNode.Attributes["Birth_Date"].Value))
                    {
                        objRow["BirthDate"] = (Convert.ToDateTime(XmlNode.Attributes["Birth_Date"].Value).ToShortDateString()).ToString();
                    }
                    else
                    {
                        objRow["BirthDate"] = string.Empty;
                    }
                    
                    GridData.Rows.Add(objRow);
                }

                gvPayeeFreezeList.DataSource = GridData;
                gvPayeeFreezeList.DataBind();

                PageValues = ObjOut.SelectSingleNode("//Payees");

                Tab1PageNumber.Text = PageValues.Attributes["PageNumber"].Value;
                
                Label1.Text = "Page " + PageValues.Attributes["PageNumber"].Value + " of " + PageValues.Attributes["TotalPages"].Value;
                Tab1LastPage.Text = PageValues.Attributes["TotalPages"].Value;

                ViewState["Tab1PageNumber"] = PageValues.Attributes["PageNumber"].Value;

                EnableDisableLinkButtons(PageValues.Attributes["PageNumber"].Value, PageValues.Attributes["TotalPages"].Value, m_tabindex);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void BindCheckGridData()
        {
            XmlDocument ObjOut = new XmlDocument();
            XmlNode PageValues = null;

            try
            {
                ObjOut.LoadXml(sCWSresponse);

                DataTable GridData = new DataTable();

                GridData.Columns.Add("TransId");
                GridData.Columns.Add("DateOfCheck");
                GridData.Columns.Add("LastName");
                GridData.Columns.Add("FirstName");
                GridData.Columns.Add("CtlNumber");
                GridData.Columns.Add("ClaimNumber");
                GridData.Columns.Add("Amount");
                GridData.Columns.Add("TransDate");
                GridData.Columns.Add("DttmHold");

                DataRow objRow = null;

                XmlNodeList xmlNodeList = ObjOut.SelectNodes("//Check");

                foreach (XmlNode XmlNode in xmlNodeList)
                {
                    objRow = GridData.NewRow();

                    objRow["TransId"] = XmlNode.Attributes["TransId"].Value.ToString();
                    objRow["DateOfCheck"] = (Convert.ToDateTime(XmlNode.Attributes["DateOfCheck"].Value).ToShortDateString()).ToString();
                    objRow["LastName"] = XmlNode.Attributes["Last_Name"].Value.ToString();
                    objRow["FirstName"] = XmlNode.Attributes["First_Name"].Value.ToString();
                    objRow["CtlNumber"] = XmlNode.Attributes["CtlNumber"].Value.ToString();
                    objRow["ClaimNumber"] = XmlNode.Attributes["ClaimNumber"].Value.ToString();
                    objRow["Amount"] = XmlNode.Attributes["Amount"].Value.ToString();
                    objRow["TransDate"] = (Convert.ToDateTime(XmlNode.Attributes["TransDate"].Value).ToShortDateString()).ToString();
                    objRow["DttmHold"] = XmlNode.Attributes["DttmHold"].Value.ToString();

                    GridData.Rows.Add(objRow);
                }

                gvHoldChecksList.DataSource = GridData;
                gvHoldChecksList.DataBind();

                PageValues = ObjOut.SelectSingleNode("//Checks");

                Tab2PageNumber.Text = PageValues.Attributes["PageNumber"].Value;
                
                Label2.Text = "Page " + PageValues.Attributes["PageNumber"].Value + " of " + PageValues.Attributes["TotalPages"].Value;
                Tab2LastPage.Text = PageValues.Attributes["TotalPages"].Value;

                pagenumber_tab2 = Convert.ToInt32(PageValues.Attributes["PageNumber"].Value);

                ViewState["Tab2PageNumber"] = PageValues.Attributes["PageNumber"].Value;
                ViewState["Tab2LastPage"] = PageValues.Attributes["TotalPages"].Value;

                EnableDisableLinkButtons(PageValues.Attributes["PageNumber"].Value, PageValues.Attributes["TotalPages"].Value, m_tabindex);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl2.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void BindAutoCheckGridData()
        {
            XmlDocument ObjOut = new XmlDocument();
            XmlNode PageValues = null;

            try
            {
                ObjOut.LoadXml(sCWSresponse);

                DataTable GridData = new DataTable();

                GridData.Columns.Add("AutoBatchId");
                GridData.Columns.Add("PrintDate");
                GridData.Columns.Add("LastName");
                GridData.Columns.Add("FirstName");
                GridData.Columns.Add("ClaimNumber");
                GridData.Columns.Add("Amount");
                GridData.Columns.Add("DttmFreeze");

                DataRow objRow = null;

                XmlNodeList xmlNodeList = ObjOut.SelectNodes("//AutoCheck");

                foreach (XmlNode XmlNode in xmlNodeList)
                {
                    objRow = GridData.NewRow();

                    objRow["AutoBatchId"] = XmlNode.Attributes["AutoBatchId"].Value.ToString();
                    objRow["PrintDate"] = (Convert.ToDateTime(XmlNode.Attributes["PrintDate"].Value).ToShortDateString()).ToString();
                    objRow["LastName"] = XmlNode.Attributes["Last_Name"].Value.ToString();
                    objRow["FirstName"] = XmlNode.Attributes["First_Name"].Value.ToString();
                    objRow["ClaimNumber"] = XmlNode.Attributes["ClaimNumber"].Value.ToString();
                    objRow["Amount"] = XmlNode.Attributes["Amount"].Value.ToString();
                    objRow["DttmFreeze"] = XmlNode.Attributes["DttmFreeze"].Value.ToString();

                    GridData.Rows.Add(objRow);
                }

                gvHoldBatchesList.DataSource = GridData;
                gvHoldBatchesList.DataBind();

                PageValues = ObjOut.SelectSingleNode("//AutoChecks");

                Tab3PageNumber.Text = PageValues.Attributes["PageNumber"].Value;
                
                Label3.Text = "Page " + PageValues.Attributes["PageNumber"].Value + " of " + PageValues.Attributes["TotalPages"].Value;
                Tab3LastPage.Text = PageValues.Attributes["TotalPages"].Value;

                ViewState["Tab3PageNumber"] = PageValues.Attributes["PageNumber"].Value;
                ViewState["Tab3LastPage"] = PageValues.Attributes["TotalPages"].Value;

                EnableDisableLinkButtons(PageValues.Attributes["PageNumber"].Value, PageValues.Attributes["TotalPages"].Value, m_tabindex);
            }
            catch (Exception ex)
            {
                ErrorHelper.logErrors(ex);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ex, BusinessAdaptorErrorType.SystemError);
                ErrorControl3.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void DestroyViewStates(int i_TabIndex)
        {
            switch (i_TabIndex)
            { 
                case 1:
                    ViewState.Remove("Tab1PageNumber");
                    ViewState.Remove("Tab1LastPage");
                    break;
                case 2:
                    ViewState.Remove("Tab2PageNumber");
                    ViewState.Remove("Tab2LastPage");   
                    break;
                case 3:
                    ViewState.Remove("Tab3PageNumber");
                    ViewState.Remove("Tab3LastPage");
                    break;
                default:
                    break;
            }
                       
        }

        protected void PopulateXmlIn(int i_TabIndex, ref XElement oMessageElement)
        {
            switch (i_TabIndex)
            {
                case 1:
                    Tab1PageNumber.Text = ViewState["Tab1PageNumber"].ToString();
                    oMessageElement.XPathSelectElement("./Document/Review/PayeeReleaseList").Value = selectedvalues.Text;
                    oMessageElement.XPathSelectElement("./Document/Review/PageNumber").Value = Tab1PageNumber.Text.ToString();    
                    break;
                case 2:
                    Tab2PageNumber.Text = ViewState["Tab2PageNumber"].ToString();
                    oMessageElement.XPathSelectElement("./Document/Review/CheckReleaseList").Value = selectedvalues.Text;
                    oMessageElement.XPathSelectElement("./Document/Review/PageNumber").Value = Tab2PageNumber.Text.ToString();    
                    break;
                case 3:
                    Tab3PageNumber.Text = ViewState["Tab3PageNumber"].ToString();
                    oMessageElement.XPathSelectElement("./Document/Review/BatchReleaseList").Value = selectedvalues.Text;
                    oMessageElement.XPathSelectElement("./Document/Review/PageNumber").Value = Tab3PageNumber.Text.ToString();    
                    break;
                default:
                    break;
            }
                        
        }

        protected void EnableDisableLinkButtons(string PageNumber, string LastPage, int i_TabIndex)
        {
            if (PageNumber == "1") //First Page of the Grid
            {
                if (i_TabIndex == 1)
                {
                    lnkFirst.Enabled = false;
                    lnkPrev.Enabled = false;
                    lnkNext.Enabled = true;
                    lnkLast.Enabled = true;
                }
                else if (i_TabIndex == 2)
                {
                    LinkButton1.Enabled = false;
                    LinkButton3.Enabled = false;
                    LinkButton2.Enabled = true;
                    LinkButton4.Enabled = true;
                }
                else if (i_TabIndex == 3)
                {
                    LinkButton5.Enabled = false;
                    LinkButton7.Enabled = false;
                    LinkButton6.Enabled = true;
                    LinkButton8.Enabled = true;
                }
            }
            if (PageNumber == LastPage) //Last Page of the Grid
            {
                if (i_TabIndex == 1)
                {
                    lnkLast.Enabled = false;
                    lnkNext.Enabled = false;
                    lnkFirst.Enabled = true;
                    lnkPrev.Enabled = true;
                }
                else if (i_TabIndex == 2)
                {
                    LinkButton2.Enabled = false;
                    LinkButton4.Enabled = false;
                    LinkButton1.Enabled = true;
                    LinkButton3.Enabled = true;
                }
                else if (i_TabIndex == 3)
                {
                    LinkButton6.Enabled = false;
                    LinkButton8.Enabled = false;
                    LinkButton5.Enabled = true;
                    LinkButton7.Enabled = true;
                }
            }

            if (PageNumber == "1" && LastPage == "1") //Only 1 Page in the Grid
            {
                if (i_TabIndex == 1)
                {
                    lnkFirst.Enabled = false;
                    lnkPrev.Enabled = false;
                    lnkNext.Enabled = false;
                    lnkLast.Enabled = false;
                }
                else if (i_TabIndex == 2)
                {
                    LinkButton2.Enabled = false;
                    LinkButton4.Enabled = false;
                    LinkButton1.Enabled = false;
                    LinkButton3.Enabled = false;
                }
                else if (i_TabIndex == 3)
                {
                    LinkButton6.Enabled = false;
                    LinkButton8.Enabled = false;
                    LinkButton5.Enabled = false;
                    LinkButton7.Enabled = false;
                }
            }

            if (Convert.ToInt32(PageNumber) > 1 && Convert.ToInt32(PageNumber) < Convert.ToInt32(LastPage))
            {
                if (i_TabIndex == 1)
                {
                    lnkFirst.Enabled = true;
                    lnkPrev.Enabled = true;
                    lnkNext.Enabled = true;
                    lnkLast.Enabled = true;
                }
                else if (i_TabIndex == 2)
                {
                    LinkButton2.Enabled = true;
                    LinkButton4.Enabled = true;
                    LinkButton1.Enabled = true;
                    LinkButton3.Enabled = true;
                }
                else if (i_TabIndex == 3)
                {
                    LinkButton6.Enabled = true;
                    LinkButton8.Enabled = true;
                    LinkButton5.Enabled = true;
                    LinkButton7.Enabled = true;
                }
            }

            return;
        }

        //protected void Test(object sender, EventArgs e)
        //{
        //    string script = "pleaseWait.Show()";

        //    ScriptManager.RegisterStartupScript(this.up1, typeof(string), "handleload", script, true);

        //}
    }
}
