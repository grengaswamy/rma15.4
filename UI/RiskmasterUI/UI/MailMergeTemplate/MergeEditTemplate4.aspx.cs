﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.RMXResourceManager;


namespace Riskmaster.UI.MailMerge
{
    public partial class MergeEditTemplate4 : System.Web.UI.Page
    {
        string MergeMessageGetTemplate = "<Message> " +
        "<Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization>" +
         " <Call>" +
            "<Function>MailMergeAdaptor.GetCurrentTemplateInfo</Function>" +
          "</Call>" +
          "<Document>" +
            "<Template><TemplateId></TemplateId></Template>" +
          "</Document>" +
        "</Message>";
        string MergeNewSaveMessageTemplate = "<Message> " +
 "<Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization> " +
"<Call>" +
 "<Function>MailMergeAdaptor.StoreTemplate</Function>   </Call>" +
"<Document>" +
"<Template><TemplateId>0</TemplateId> <FormInfomation><form AllStatesSelected='' LOB='' MergeDocumentFormat='' MergeDocumentType='' StateId='' catid='' formDesc='' formname='' id='' tablequalify='' usertypeid='' emailCheck='' mailRecipient='' /></FormInfomation><FileName/>" +
 "<FormDefinationData><form fieldid=',2030,2033' itemtype='' mergeparam='' supplierflag='' /></FormDefinationData>" +
  "<FormPermissionData><form groupid='' userid='' /> </FormPermissionData><FileContent/>" +
"</Template></Document></Message>";
        string MergeMessageTemplate = "<Message> " +
 "<Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization> " +
"<Call>" +
 "<Function>MailMergeAdaptor.GenerateRTF</Function>   </Call>" +
"<Document>" +
"<Template><TemplateId /> <Name /> <Attach /> <DirectDisplay /> <Subject /> <Keywords /> <Notes /> <Class /> <Category /> <Type /> <Comments /> " +
" <FileName></FileName> " +
 "<RecordId /> <RowIds /> <IsPreFab>true</IsPreFab> </Template></Document></Message>";
        private void SaveValuesFromPreviousScreen()
        {
            if (!Page.IsPostBack)
            {
                lettername.Value = AppHelper.GetFormValue("lettername");
                lob.Value = AppHelper.GetFormValue("lob");
                letterdesc.Value = AppHelper.GetFormValue("letterdesc");
                allstatesselected.Value = AppHelper.GetFormValue("allstatesselected");
                lettertype.Value = AppHelper.GetFormValue("lettertype");
                txtState.Value = AppHelper.GetFormValue("txtState");
                //spahariya MITS 28867
                lstMailRecipient.Value = AppHelper.GetFormValue("lstMailRecipient");
                EmailCheck.Value = AppHelper.GetFormValue("EmailCheck");
                //spahariya
                //added by swati MITS # 36930
                hdnSelectedUser.Value = AppHelper.GetFormValue("hdnSelectedUser");
                hdnSelectedUserID.Value = AppHelper.GetFormValue("hdnSelectedUserID");
                //change end here by swati
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                prefab.Value = AppHelper.GetFormValue("prefab");
                hdnSelectedFieldsID.Value = AppHelper.GetFormValue("hdnSelectedFieldsID");
                hdnSelectedFields.Value = AppHelper.GetFormValue("hdnSelectedFields");
                selectedfields.Value = AppHelper.GetFormValue("hdnSelectedFields");
                hdnselectedperms.Value = AppHelper.GetFormValue("hdnselectedperms");
                hdnselectedpermsids.Value = AppHelper.GetFormValue("hdnselectedpermsids");
                hdnfilename.Value = AppHelper.GetFormValue("hdnfilename");
                hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                hdnselectedpermsusers.Value = AppHelper.GetFormValue("hdnselectedpermsusers");
                hdnselectedpermsgroups.Value = AppHelper.GetFormValue("hdnselectedpermsgroups");
                fieldcategory.Value = AppHelper.GetFormValue("fieldcategory");
                TemplateId.Value = AppHelper.GetFormValue("TemplateId");
                hdncatid.Value = AppHelper.GetFormValue("hdncatid");
                hdnexecutepipeline.Value = AppHelper.GetFormValue("hdnexecutepipeline");
                hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                hdnformfilecontent.Value = AppHelper.GetFormValue("hdnformfilecontent");
                lblFilename.Text = hdnfilename.Value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SaveValuesFromPreviousScreen();
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeEditTemplate4.aspx"), "MergeEditTemplate4Validations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeEditTemplate4ValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end

                if (!Page.IsPostBack)
                {
                    if (hdnformfilecontent.Value.Trim() == "")
                    {
                        MergeMessageGetTemplate = AppHelper.ChangeMessageValue(MergeMessageGetTemplate, "//Template/TemplateId", AppHelper.GetFormValue("TemplateId"));
                        string sReturn = AppHelper.CallCWSService(MergeMessageGetTemplate.ToString());
                        XmlDocument Model = new XmlDocument();
                        Model.LoadXml(sReturn);
                        XmlNode node = Model.SelectSingleNode("//Template");
                        if (node != null)
                        {
                            if (node.Attributes["FormFileContent"].Value != "")
                            {
                                hdnformfilecontent.Value = node.Attributes["FormFileContent"].Value;
                            }
                        }
                        hdnSilver.Value = Model.SelectSingleNode("//UseSilverlight").InnerText; //mudabbir added 36022
						if(string.Compare(hdnSilver.Value,"true",true)!=0){
							// RMA-8407 : MITS-36022 : bkuzhanthim
	                        System.Web.HttpBrowserCapabilities browser = Request.Browser;
	                        string sBrowser = browser.Browser;
	                        if (sBrowser != "IE" && sBrowser != "InternetExplorer")
	                            hdnSilver.Value = "true";
						}
                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }
        protected void btnFinish_Click(object sender, EventArgs e)
        {
            try
            {
                SaveMergeTemplate();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            Server.Transfer("MergeTemplateSaved.aspx");


        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeEditTemplate3.aspx");
        }
        private void SaveMergeTemplate()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(MergeNewSaveMessageTemplate);
            //This is all for form information

            XmlNode TemplateIdNode = doc.SelectSingleNode("//Document/Template/TemplateId");
            TemplateIdNode.InnerText = TemplateId.Value;
            XmlNode allstateselectednode = doc.SelectSingleNode("//FormInfomation/form/@AllStatesSelected");
            if (allstatesselected.Value == "on")
                allstateselectednode.InnerText = "true";
           
            XmlNode LOBNode = doc.SelectSingleNode("//FormInfomation/form/@LOB");
            LOBNode.InnerText = lob.Value;
            XmlNode DocFormatNode = doc.SelectSingleNode("//FormInfomation/form/@MergeDocumentFormat");
            DocFormatNode.InnerText = txtmergedocumentformat_cid.Value;
            XmlNode DocTypeNode = doc.SelectSingleNode("//FormInfomation/form/@MergeDocumentType");
            DocTypeNode.InnerText = txtmergedocumenttype_cid.Value;
            XmlNode StateIdNode = doc.SelectSingleNode("//FormInfomation/form/@StateId");
            
            StateIdNode.InnerText = txtState_cid.Value;
            if (allstatesselected.Value == "on")
            {
                StateIdNode.InnerText = "0";
            }
            XmlNode catidNode = doc.SelectSingleNode("//FormInfomation/form/@catid");
            catidNode.InnerText = hdncatid.Value;
            XmlNode formDescNode = doc.SelectSingleNode("//FormInfomation/form/@formDesc");
            formDescNode.InnerText = letterdesc.Value;
            XmlNode formnameNode = doc.SelectSingleNode("//FormInfomation/form/@formname");
            formnameNode.InnerText = lettername.Value;
            XmlNode idNode = doc.SelectSingleNode("//FormInfomation/form/@id");
            idNode.InnerText =TemplateId.Value ;
            XmlNode tablequalifyNode = doc.SelectSingleNode("//FormInfomation/form/@tablequalify");
            tablequalifyNode.InnerText = fieldcategory.Value;
            //This is all for form information

            //File name
            XmlNode FileNode = doc.SelectSingleNode("//FileName");
            FileNode.InnerText = hdnfilename.Value;
            //File name

            //Form Defination Data
            XmlNode selectedfieldsnode = doc.SelectSingleNode("//FormDefinationData/form/@fieldid");
            selectedfieldsnode.InnerText = hdnSelectedFieldsID.Value;
            //Form Defination Data

            //Form Permission Data
            XmlNode groupidNode = doc.SelectSingleNode("//FormPermissionData/form/@groupid");
            groupidNode.InnerText = hdnselectedpermsgroups.Value;
            XmlNode useridNode = doc.SelectSingleNode("//FormPermissionData/form/@userid");
            useridNode.InnerText = hdnselectedpermsusers.Value;
            //Form Permission Data
            //spahariya MITS 28867 - start
            XmlNode formEmailCheckNode = doc.SelectSingleNode("//FormInfomation/form/@emailCheck");
            formEmailCheckNode.InnerText = EmailCheck.Value;
            XmlNode formRecipientNode = doc.SelectSingleNode("//FormInfomation/form/@mailRecipient");
            formRecipientNode.InnerText = lstMailRecipient.Value;
            //spahariya - end
            //added by swati MITS # 36930
            XmlNode selecteduserfieldsnode = doc.SelectSingleNode("//FormInfomation/form/@usertypeid");
            selecteduserfieldsnode.InnerText = hdnSelectedUserID.Value;
            //change end here by swati
            XmlNode FileContentNode = doc.SelectSingleNode("//FileContent");
            FileContentNode.InnerText = hdnformfilecontent.Value;

            
            string sReturn = AppHelper.CallCWSService(doc.OuterXml);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplates.aspx");
        }
    }
}
