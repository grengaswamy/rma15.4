﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeTemplates.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeTemplates"%>
<%@ Import Namespace="System.Xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>



<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
      <title>Riskmaster</title>
<uc4:CommonTasks ID="CommonTasks1" runat="server" />
<script  type="text/javascript" language="javascript" src="../../Scripts/merge.js"></script>
</head>
<body onload="if(parent.MDIScreenLoaded != null) parent.MDIScreenLoaded();">
    <form id="frmData" runat="server">
     <div id="maindiv" style="height:100%;width:99%;overflow:auto">
    <table width="100%">
    <tr class="msgheader">
     <td colspan="2"><asp:Label ID="lblMiscAdmin" runat="server" Text="<%$ Resources:lblMiscAdminResrc %>"></asp:Label></td>
    </tr>
    <tr class="ctrlgroup">
     <td colspan="2"><asp:Label ID="lblMergeFormLetters" runat="server" Text="<%$ Resources:lblMergeFormLettersResrc %>"></asp:Label></td>
    </tr>
   </table>
     <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
   <table>
 
     <tr>
     <td> <asp:Button ID="btnNew" runat="server" 
             Text="<%$    Resources:btnNewResrc    %>"  CssClass=button 
             onclick="btnNew_Click" /> </td>
     <td> <asp:Button ID="btnDelete" runat="server" 
             Text="<%$    Resources:btnDeleteResrc    %>"  CssClass=button UseSubmitBehavior="true"  
             OnClientClick="JavaScript:return DeleteSelected();" onclick="btnDelete_Click" /> </td>
    
    </tr>
   </table>
   <div id="divForms" class="divScroll">
		
		<table width="100%">
		   <% if (Model.SelectSingleNode("//@Catid")!=null && Model.SelectSingleNode("//@Catid").Value != "")
        {
            foreach (XmlElement ele in Model.SelectNodes("//Category"))
            {%>
                    <tr>
							<td colspan="2" class="colheader5">
						        <%=ele.GetAttribute("CategoryDesc")%>	
							</td>
					</tr>
					 <%foreach (XmlElement eleTemplate in ele.SelectNodes("./Template"))
        {%>
                        <tr>
								<td width="16">		
								<input type="checkbox" id="letterid_<%=eleTemplate.GetAttribute("TemplateId")%>" value="<%=eleTemplate.GetAttribute("TemplateId")%>" />
								</td>
								<td>
								    <a href="javascript:SelectTemplate('<%=eleTemplate.GetAttribute("TemplateId")%>','<%=ele.GetAttribute("CategoryDesc")%>')"><%=eleTemplate.GetAttribute("TemplateDesc")%></a>
								</td>
						</tr>
                    <%}
            }
        }
            %>
									
				<%else
               { %>
						<tr><td colspan="2"><asp:Label ID="lblNoTemplates" runat="server" Text="<%$ Resources:lblNoTemplatesResrc %>"></asp:Label></td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						<tr><td>&#160;</td></tr>
						
			<%} %>
            <asp:HiddenField ID="hdnDeletedItems" runat="server" />
		</table>
	</div>
		<ul>
			<li><asp:Label ID="lblClickNew" runat="server" Text="<%$ Resources:lblClickNewResrc %>"></asp:Label></li>
			<li><asp:Label ID="lblClickName" runat="server" Text="<%$ Resources:lblClickNameResrc %>"></asp:Label></li>
			<li><asp:Label ID="lblClickDelete" runat="server" Text="<%$ Resources:lblClickDeleteResrc %>"></asp:Label></li>
		</ul>
		</div>
    </form>
</body>
</html>
