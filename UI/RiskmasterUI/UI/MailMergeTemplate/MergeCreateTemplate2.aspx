﻿<%@ Page Language="C#" validateRequest = "false" AutoEventWireup="true" CodeBehind="MergeCreateTemplate2.aspx.cs" Inherits="Riskmaster.UI.MailMerge.MergeCreateTemplate2"%>


<%@ Import Namespace="System.Xml" %>

<%@ Register src="~/UI/Shared/Controls/CodeLookUp.ascx" Tagname="CodeLookUp" tagprefix="uc1" %>

<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title>Riskmaster</title>

    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='../../Scripts/merge.js'></script>
</head>
<body onload="InitPageSettings_MergeTemplate();">
    <form id="frmData" runat="server">
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
<table width="100%" border="0">
    <tr class="msgheader">
     <td colspan="3"> <asp:Label ID="lblMiscAdmin" runat="server" Text="<%$ Resources:lblMiscAdminResrc %>"></asp:Label></td>
    </tr>
    <tr class="ctrlgroup">
     <td colspan="3"><asp:Label ID="lblEditMergeLetter" runat="server" Text="<%$ Resources:lblEditMergeLetterResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td colspan="3" class="colheader3"><asp:Label ID="lblDataFields" runat="server" Text="<%$ Resources:lblDataFieldsResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td nowrap="1"><asp:Label ID="lblAvailableFields" runat="server" Text="<%$ Resources:lblAvailableFieldsResrc %>"></asp:Label></td>
     <td></td>
     <td><asp:Label ID="lblFieldsToMerge" runat="server" Text="<%$ Resources:lblFieldsToMergeResrc %>"></asp:Label></td>
    </tr>
    <tr>
     <td>
         <asp:DropDownList ID="fieldcategory" runat="server">
         </asp:DropDownList>
         <asp:ListBox ID="fields_" runat="server" Rows=8></asp:ListBox>
         <%  foreach (XmlElement ele in Model.SelectNodes("//Field"))
        { %>
        <select ondblclick='AddSingle(this.options[this.selectedIndex]);' multiple="true" style='display:none' size="8" id='fields_<%=ele.Attributes["DisplayCat"].Value %>'>
            <%  foreach (XmlElement eleChild in ele.SelectNodes("./Value"))
                {
                string fieldvalue="";
                if (eleChild.Attributes["MergeParam"].Value == "")
                {
                    fieldvalue = ele.Attributes["DisplayCat"].Value + "_" + eleChild.Attributes["FieldId"].Value + "_" + eleChild.Attributes["FieldName"].Value;
                }
                else
                {
                    fieldvalue = ele.Attributes["DisplayCat"].Value + "_" + eleChild.Attributes["FieldId"].Value + "|" + eleChild.Attributes["MergeParam"].Value+"_"+eleChild.Attributes["FieldName"].Value;
                }    
                    %>
            <option value='<%=fieldvalue%>' > <%=eleChild.Attributes["FieldName"].Value%></option>
             <%} %>
        </select>
        <%} %>
       </td>
     <td width="1"><input type="button" value=">>" class="button" onclick="javascript:return AddSelected();"><input type="button" value="<<" class="button" onclick="javascript:return RemoveSelected();"></td>
     <td nowrap="1" valign="top" align="left" width="100%"><br>
         
     <select id="selectedfields" runat=server multiple="true" style="width:162px" ondblclick="javascript:return RemoveSingle(this.options[this.selectedIndex]);" size="10"></select></td>
    </tr>
    <tr class="ctrlgroup">
     <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
     <td colspan="3">    <asp:Button ID="Button1" onclick="btnBack_Click" runat="server" Text="<%$ Resources:btnBackResrc %>"  CssClass=button UseSubmitBehavior="true" OnClientClick="return Validate_MergeCreateTemplate2Back();"/>
         <asp:Button ID="Button2" runat="server" Text="<%$ Resources:btnNextResrc %>"  onclick="btnNext_Click" UseSubmitBehavior="false"  CssClass=button OnClientClick="javascript: Validate_MergeCreateTemplate2();"/>
         <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:btnCancelResrc %>" CssClass=button 
             EnableTheming="False" onclick="btnCancel_Click" /></td>
    </tr>
    <tr> <asp:HiddenField ID="hdnisallowallperms" runat="server"  Value="true"/>
        <asp:HiddenField ID="lettername" runat="server" />
        <asp:HiddenField ID="lob" runat="server" />
        <asp:HiddenField ID="letterdesc" runat="server" />
        <asp:HiddenField ID="allstatesselected" runat="server" />
        <asp:HiddenField ID="lettertype" runat="server" />
        <asp:HiddenField ID="txtState" runat="server" />
        <asp:HiddenField ID="txtState_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype" runat="server" />
        <asp:HiddenField ID="txtmergedocumenttype_cid" runat="server" />
        <asp:HiddenField ID="txtmergedocumentformat" runat="server" />
         <asp:HiddenField ID="txtmergedocumentformat_cid" runat="server" />
           <asp:HiddenField ID="prefab" runat="server" />
                 <asp:HiddenField ID="hdnselectedperms" runat="server" />
           <asp:HiddenField ID="hdnselectedpermsids" runat="server" />
         <asp:HiddenField ID="hdnfilename" runat="server" />
         <asp:HiddenField ID="hdnformfilecontent" runat="server" />
         <asp:HiddenField ID="hdnselectedpermsusers" runat="server" />
            <asp:HiddenField ID="hdnselectedpermsgroups" runat="server" />
               <asp:HiddenField ID="hdnLetterType" runat="server" />
               <asp:HiddenField ID="EmailCheck" runat="server" />
        <asp:HiddenField ID="lstMailRecipient" runat="server" />
        <%--added by swati MITS # 36930--%>
                <asp:HiddenField ID="hdnSelectedUser" runat="server" />
                <asp:HiddenField ID="hdnSelectedUserID" runat="server" />
    </tr>
    <input type="hidden"  value="" id="hdnaction"/><input type="hidden" runat=server  value="" id="hdnSelectedFields"/><input type="hidden" runat=server  value="" id="hdnSelectedFieldsID">
   </table>
   
   <ol class="gen">
    			<asp:Label ID="lblPlaceFields" runat="server" Text="<%$ Resources:lblPlaceFieldsResrc %>"></asp:Label>
    			<br>
    			<asp:Label ID="lblChooseCatagory" runat="server" Text="<%$ Resources:lblChooseCatagoryResrc %>"></asp:Label>
    </form>
</body>
</html>
