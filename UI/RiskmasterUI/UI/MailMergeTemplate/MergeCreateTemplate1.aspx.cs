﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using Riskmaster;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections;
using Riskmaster.RMXResourceManager;

namespace Riskmaster.UI.MailMerge
{
    public partial class MergeCreateTemplate1 : System.Web.UI.Page
    {
        string MergeMessageTemplate = "<Message> " +
           "<Authorization>f58d39cc-80ef-4004-9757-acda33b65d7c</Authorization>" +
            " <Call>" +
               "<Function>MailMergeAdaptor.GetCategoryListPrefabTemplateListServerUniqueFileName</Function>" +
             "</Call>" +
             "<Document>" +
               "<Template />" +
             "</Document>" +
           "</Message>";
        private void PopulateLetterDataSource(XmlDocument doc)
        {
            foreach (XmlElement ele in doc.SelectNodes("//GetCategoryList/Categories/Category"))
            {
                ListItem item = new ListItem();
                item.Text = ele.Attributes["Category"].Value;
                item.Value = ele.Attributes["CategoryId"].Value;
                lettertype.Items.Add(item);
            }
        }
        private void SaveValuesFromPreviousScreen()
        {
            if (!Page.IsPostBack)
            {
                
                lettername.Value = AppHelper.GetFormValue("lettername");
                lob.SelectedValue = AppHelper.GetFormValue("lob");
                letterdesc.Value = AppHelper.GetFormValue("letterdesc");
                hdnLetterType.Value = AppHelper.GetFormValue("hdnLetterType");
                lettertype.SelectedValue = AppHelper.GetFormValue("lettertype");
                txtState.Value = AppHelper.GetFormValue("txtState");
                txtState_cid.Value = AppHelper.GetFormValue("txtState_cid");
                if (txtState_cid.Value == "")
                    allstatesselected.Checked = true;
                else
                    allstatesselected.Checked = false;
                txtmergedocumenttype.Value = AppHelper.GetFormValue("txtmergedocumenttype");
                txtmergedocumenttype_cid.Value = AppHelper.GetFormValue("txtmergedocumenttype_cid");
                txtmergedocumentformat.Value = AppHelper.GetFormValue("txtmergedocumentformat");
                txtmergedocumentformat_cid.Value = AppHelper.GetFormValue("txtmergedocumentformat_cid");
                prefab.SelectedValue = AppHelper.GetFormValue("prefab");
                //spahariya 28867
                //commented by swati
                //lstMailRecipient.SelectedValue = AppHelper.GetFormValue("lstMailRecipient");                
                if (AppHelper.GetFormValue("EmailCheck") == "true")
                {
                    EmailCheck.Checked = true;
                }
                else
                {
                    EmailCheck.Checked = false;
                    lstMailRecipient.Enabled = false;
                }
                //spahariya - end
                //added by swati for AIC MITS# 36930                
                hdnSelectedUser.Value = AppHelper.GetFormValue("hdnSelectedUser");
                hdnSelectedUserID.Value = AppHelper.GetFormValue("hdnSelectedUserID");
                //change end here
                hdnSelectedFieldsID.Value = AppHelper.GetFormValue("hdnSelectedFieldsID");
                hdnSelectedFields.Value = AppHelper.GetFormValue("hdnSelectedFields");
                hdnselectedperms.Value = AppHelper.GetFormValue("hdnselectedperms");
                hdnselectedpermsids.Value = AppHelper.GetFormValue("hdnselectedpermsids");
                fieldcategory.Value = AppHelper.GetFormValue("fieldcategory");
                if (Page.PreviousPage != null)
                {
                    string FileName = this.PreviousPage.AppRelativeVirtualPath;
                    FileName = FileName.Substring(FileName.LastIndexOf("/") + 1);
                    if (FileName != "MergeTemplates.aspx")
                    {
                        hdnisallowallperms.Value = AppHelper.GetFormValue("hdnisallowallperms");
                    }
                }
                hdnselectedpermsusers.Value = AppHelper.GetFormValue("hdnselectedpermsusers");
                hdnselectedpermsgroups.Value = AppHelper.GetFormValue("hdnselectedpermsgroups");
                hdnformfilecontent.Value = AppHelper.GetFormValue("hdnformfilecontent");
            }
        }
        private void PopulateLOB(XmlDocument doc)
        {
            ListItem item = new ListItem();
            item.Text = "ALL - ALL LOBs";
            item.Value = "-1";
            lob.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//GetLOBList/LOBs/LOB"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["shortcode"].Value + " - " + ele.Attributes["codedesc"].Value;
                item.Value = ele.Attributes["codeid"].Value;
                lob.Items.Add(item);
            }
        }
        private void PopulateDocumentTemplate(XmlDocument doc)
        {
            ListItem item = new ListItem();
            item.Text = "N/A";
            item.Value = "n/a";
            prefab.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//GetPrefabTemplateList/PrefabTemplates/Template"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["PrefabTemplateDesc"].Value + "(" + ele.Attributes["PrefabTemplateFile"].Value+")";
                item.Value = ele.Attributes["PrefabTemplateFile"].Value;
                prefab.Items.Add(item);
            }
        }
        private void PopulateFileName(XmlDocument doc)
        {
            XmlNode node=doc.SelectSingleNode("//UniqueFileName/ServerUniqueFileName");
            if (node != null)
            {
                hdnfilename.Value = node.Attributes["FileName"].Value;
            }
        }
        //abansal23 : MITS 14952 on 03/25/2009 Start
        private void PopulateTemplateNames(XmlDocument doc)
        {
            XmlNode node = doc.SelectSingleNode("//TemplateNames");
            if (node != null)
            {
                hdnTemplateNames.Value = node.InnerText;
            }
        }
        //abansal23 : MITS 14952 on 03/25/2009 End
        //spahariya : MITS 28867 on 06/26/2012 Start
        private void PopulateMailRecipient(XmlDocument doc)
        {
            ListItem item = new ListItem();
            item.Text = "";
            item.Value = "";
            lstMailRecipient.Items.Add(item);
            foreach (XmlElement ele in doc.SelectNodes("//GetMailRecipient/Recipients/Recipient"))
            {
                item = new ListItem();
                item.Text = ele.Attributes["codedesc"].Value;
                item.Value = ele.Attributes["codeid"].Value;
                lstMailRecipient.Items.Add(item);
            }
        }
        //spahariya : MITS 28867 on 06/26/2012 End
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Amandeep MultiLingual Changes--start
                string sValidationResources = JavaScriptResourceHandler.ProcessRequest(this.Context, RMXResourceProvider.PageId("MergeCreateTemplate1.aspx"), "MergeCreateTemplate1Validations", ((int)RMXResourceProvider.RessoureType.ALERTMESSAGE).ToString());
                ClientScript.RegisterStartupScript(this.GetType(), "MergeCreateTemplate1ValidationsScripts", sValidationResources, true);
                //Amandeep MultiLingual Changes--end

                if (!Page.IsPostBack)
                {
                    string sReturn = AppHelper.CallCWSService(MergeMessageTemplate.ToString());
                    XmlDocument Model = new XmlDocument();
                    Model.LoadXml(sReturn);
                    PopulateLetterDataSource(Model);
                    PopulateLOB(Model);
                    PopulateDocumentTemplate(Model);
                    PopulateMailRecipient(Model);
                    hdnfilename.Value = AppHelper.GetFormValue("hdnfilename");
                    if (hdnfilename.Value == "")
                        PopulateFileName(Model);
                    //abansal23 : MITS 14952 on 03/25/2009 Start
                    PopulateTemplateNames(Model);
                    //abansal23 : MITS 14952 on 03/25/2009 End
                }
                SaveValuesFromPreviousScreen();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
            
        } 

        protected void btnNext_Click(object sender, EventArgs e)
        {
           
                if (lettertype.SelectedValue == "20")
                {
                    Server.Transfer("MergeCreateAdminTrackingTemplate.aspx");
                }
                else
                    Server.Transfer("MergeCreateTemplate2.aspx");
            }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplates.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Server.Transfer("MergeTemplates.aspx");
        }
    }
}
