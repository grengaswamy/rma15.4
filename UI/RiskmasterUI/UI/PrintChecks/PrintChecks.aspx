<%--/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/--%>

<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="PrintChecks.aspx.cs" Inherits="Riskmaster.UI.PrintChecks.PrintChecks" ValidateRequest="false" %>

<%@ Register Src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" TagName="PleaseWaitDialog"  TagPrefix="uc2" %>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Checks</title>
    <link rel="stylesheet" href="../../Content/rmnet.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/dhtml-div.css" type="text/css" />
    <link rel="stylesheet" href="../../Content/zpcal/themes/system.css" type="text/css" />

    <script language="javaScript" src="../../Scripts/form.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../Scripts/drift.js" type="text/javascript"></script>

    <script language="javaScript" src="../../Scripts/PrintChecks.js" type="text/javascript"></script>

    <script language="JavaScript" src="../../Scripts/WaitDialog.js" type="text/javascript">        { var i; }
    </script>
    <%--vkumar258 - RMA-6037 - Starts --%>
    <%--<script type="text/javascript" src="../../Scripts/zapatec/utils/zapatec.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/src/calendar.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpcal/lang/calendar-en.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-xml.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-editable.js">        { var i; }
    </script>

    <script type="text/javascript" src="../../Scripts/zapatec/zpgrid/src/zpgrid-query.js">        { var i; }
    </script>--%>
    <link rel="stylesheet" href="../../Scripts/jquery/themes/cupertino/jquery.ui.all.css" />
    <link rel="stylesheet" href="../../Scripts/jquery/demos.css" />
    <script type="text/javascript" src="../../Scripts/jquery/jquery-1.8.0.js">        { var i; }  </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.core.js">        { var i; } </script>
    <script type="text/javascript" src="../../Scripts/jquery/ui/jquery.ui.datepicker.js">        { var i; } </script>
    <%--vkumar258 - RMA-6037 - End --%>

    <script language="JavaScript" src="../../Scripts/rmx-common-ref.js" type="text/javascript">        { var i; }
    </script>
 
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body class="10pt" onload="Javascript : PrintCheckOnLoad()">
    <form id="frmData" name="frmData" method="post" runat="server">
    <div>
        <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />
<%--        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />--%>
        <asp:HiddenField runat="server" ID="hdnCheckedids" Value="" />
        <asp:HiddenField runat="server" ID="hdnRequest" Value="" /> <%-- igupta3 mits 28566--%>
        <asp:TextBox Style="display: none" runat="server" name="hTabName" ID="TextBox1" />
        <div class="msgheader" id="div_formtitle" runat="server">
            <asp:Label ID="Label1" runat="server" Text="Print Checks" />
            <asp:Label ID="formsubtitle" runat="server" Text="" />
        </div>
        <div id="Div1" class="errtextheader" runat="server">
            <asp:Label ID="formdemotitle" runat="server" Text="" />
        </div>
    </div>
    <br />
    <table border="0">
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="NotSelected" nowrap="true" name="TABSprecheckregister" id="TABSprecheckregister">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="precheckregister"
                                id="LINKTABSprecheckregister"><span style="text-decoration: none">Precheck Register</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSprintcheckbatch" id="TABSprintcheckbatch">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="printcheckbatch"
                                id="LINKTABSprintcheckbatch"><span style="text-decoration: none">Print Check Batch</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td class="NotSelected" nowrap="true" name="TABSpostcheckregister" id="TABSpostcheckregister">
                            <a class="NotSelected1" href="#" onclick="tabChange(this.name);return false;" name="postcheckregister"
                                id="LINKTABSpostcheckregister"><span style="text-decoration: none">Postcheck Register</span></a>
                        </td>
                        <td nowrap="true" style="border-bottom: none; border-left: none; border-right: none;
                            border-top: none;">
                            &nbsp;&nbsp;
                        </td>
                        <td valign="top" nowrap="true">
                        </td>
                    </tr>
                </table>
                <div class="singletopborder" style="position: relative; left: 0; top: 0; width: 800px;
                    height: 100%; overflow: auto">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABprecheckregister"
                                    id="FORMTABprecheckregister">
                                    <tr id="">
                                        <td>
                                            Bank Account:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="bankaccountpre" runat="server" onchange="BankAccountChange('pre');" AutoPostBack ="true" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                                     So introducing Distribution Type and removing out EFTPayment below--%>
                                     <tr id="">
                                        <td>
                                            Distribution Type:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDistributionTypePre" runat="server" AutoPostBack ="true" onchange="BankAccountChange('pre');">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            Order Field:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <%-- Debabrata Biswas Batch Printing Retrofit r6 MITS# 19715/20050 Date: 03/17/2010 --%>
                                            <asp:DropDownList runat="server" ID="orderfieldpre" TabIndex="1" onchange="setDataChanged(true);">
                                                <asp:ListItem Value="(None)">(None)</asp:ListItem>
                                                <asp:ListItem Value="Check Total">Check Total</asp:ListItem>
                                                <asp:ListItem Value="Claim Number">Claim Number</asp:ListItem>
                                                <asp:ListItem Value="Control Number">Control Number</asp:ListItem>
                                                <asp:ListItem Value="Payee Name">Payee Name</asp:ListItem>
                                                <asp:ListItem Value="Transaction Date">Transaction Date</asp:ListItem>
                                                <asp:ListItem Value="Check Number">Check Number</asp:ListItem>
                                                <asp:ListItem Value="Sub Bank Account">Sub Bank Account</asp:ListItem>
                                                <asp:ListItem Value="Payer Level">Payer Level</asp:ListItem>
                                                <asp:ListItem Value="Current Adjuster">Current Adjuster</asp:ListItem>
                                                <asp:ListItem Value="Org Hierarchy">Org. Hierarchy</asp:ListItem> 
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            From Date:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" size="30" RMXRef="/Instance/Document/PrecheckRegister/FromDateTemp"
                                                onblur="dateLostFocus(this.id);" onchange="CheckSetChanged();"
                                                ID="fromdatetemp" TabIndex="3" />
                                                <%--vkumar258 - RMA_6037- Starts --%>
                                                <%--<asp:Button runat="server" class="DateLookupControl" ID="fromdatetempbtn" TabIndex="4" />

                                            <script type="text/javascript">
                                                Zapatec.Calendar.setup(
											{
											    inputField: "fromdatetemp",
											    ifFormat: "%m/%d/%Y",
											    button: "fromdatetempbtn"
											}
											);
                                            </script>--%>
                                                <script type="text/javascript">
                                                    $(function () {
                                                        $("#fromdatetemp").datepicker({
                                                            showOn: "button",
                                                            buttonImage: "../../Images/calendar.gif",
                                                            //buttonImageOnly: true,
                                                            showOtherMonths: true,
                                                            selectOtherMonths: true,
                                                            changeYear: true
                                                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");
                                                    });
                                                </script>
                                                <%--vkumar258 - RMA_6037- End --%>
                                            </td>
                                            <td>&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:checkbox runat="server" rmxref="/Instance/Document//FromDateFlag"
                                                    id="fromdateflag" onchange="ApplyBool(this);" onclick="CheckSetChanged();" />
                                            </td>
                                        </tr>
                                        <tr id="">
                                            <td>To Date:&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:textbox runat="server" value="" size="30" rmxref="/Instance/Document/PrecheckRegister/ToDateTemp"
                                                    onblur="dateLostFocus(this.id);" onchange="CheckSetChanged();"
                                                    id="todatetemp" tabindex="5" />
                                                <%--vkumar258 - RMA_6037- Starts --%>
                                                <%-- <asp:Button runat="server" class="DateLookupControl" ID="todatetempbtn" TabIndex="6" />

                                            <script type="text/javascript">
                                                var cal = new Zapatec.Calendar.setup(
											{
											    inputField: "todatetemp",
											    ifFormat: "%m/%d/%Y",
											    button: "todatetempbtn"
											}
											);
                                            </script>--%>
                                                <script type="text/javascript">
                                                    $(function () {
                                                        $("#todatetemp").datepicker({
                                                            showOn: "button",
                                                            buttonImage: "../../Images/calendar.gif",
                                                            //buttonImageOnly: true,
                                                            showOtherMonths: true,
                                                            selectOtherMonths: true,
                                                            changeYear: true
                                                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "6");
                                                    });
                                                </script>
                                                <%--vkumar258 - RMA_6037- End --%>
                                            </td>
                                            <td>&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:checkbox runat="server" rmxref="/Instance/Document//ToDateFlag"
                                                    tabindex="4" id="todateflag" onchange="ApplyBool(this);" onclick="CheckSetChanged()" />
                                            </td>
                                        </tr>
                                        <%-- Start Debabrata Biswas Batch Printing Retrofit r6 MITS# 19715/20050 Date: 03/17/2010 --%>
                                        <tr id="">
                                            <td>Org. Hierarchy:&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:listbox selectionmode="Multiple" rows="3" orgid="" id="orghierarchypre" runat="server" rmxref="/Instance/Document//OrgHierarchy" itemsetref="/Instance/Document//OrgHierarchy" />
                                                <asp:textbox style="display: none" id="orghierarchypre_lst" runat="server" />
                                                <asp:textbox style="display: none" id="orghierarchypre_cid" runat="server" />
                                                <asp:button id="orghierarchyprebtn" onclientclick="return selectCode('orgh','orghierarchypre','ALL')" text="..." runat="server" />
                                                <asp:button id="orghierarchyprebtndel" onclientclick="return deleteSelCode('orghierarchypre')" text="-" runat="server" />
                                            </td>
                                        </tr>
                                        <tr id="">
                                            <td>Org Hierarchy Level(For Display Only):&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:dropdownlist runat="server" id="orghierarchylevelpre" tabindex="1" onchange="setDataChanged(true);">
                                				<asp:ListItem Value="1005">Client</asp:ListItem>
                                                <asp:ListItem Value="1006">Company</asp:ListItem>
                                                <asp:ListItem Value="1007">Operation</asp:ListItem>
                                                <asp:ListItem Value="1008">Region</asp:ListItem>
                                                <asp:ListItem Value="1009">Division</asp:ListItem>
                                                <asp:ListItem Value="1010">Location</asp:ListItem>
                                                <asp:ListItem Value="1011">Facility</asp:ListItem>
                                                <asp:ListItem Value="1012">Department</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <%-- End Debabrata Biswas Batch Printing Retrofit r6 MITS# 19715/20050 Date: 03/17/2010 --%>
                                    <tr id="">
                                        <td colspan="2">
                                            All Checks
                                            <asp:RadioButton runat="server" value="false" RMXRef="/Instance/Document//UsingSelection"
                                                onclick="CheckSetChanged(this.id);" ID="allchecks" AutoPostBack="true"/>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">
                                            Select Checks:
                                                <asp:RadioButton runat="server" value="true" RMXRef="/Instance/Document//UsingSelection"
                                                onclick="CheckSetChanged(this.id)" ID="selectchecks" autopostback="true"/>
                                        </td>
                                        <td colspan="2">

                                            <script type="text/javascript" language="JavaScript" src="">{var i;}</script>

                                            <asp:Button runat="server" class="button" ID="choose" OnClientClick="OpenChecksList()"
                                                Text="Choose" TabIndex="7" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            Include Auto Payments?:&nbsp;&nbsp;
                                        </td>
                                        <td>
                      <asp:CheckBox runat="server" appearance="full" RMXRef="/Instance/Document//IncludeAutoPaymentsFlag"
                                                TabIndex="6" ID="includeautopayments" onchange="ApplyBool(this);" onclick="CheckSetChanged()" AutoPostBack="true" />
                                        </td>
                                    </tr>
                                  
                                  <%--  skhare7 R8 enhancement--%>
                                    <tr id="">
                                        <td>
                                            Include Combined Payments?:&nbsp;&nbsp;</td>
                                        <td>
                      <asp:CheckBox runat="server" appearance="full" RMXRef="/Instance/Document//IncludeCombinedPaymentsFlag"
                                                TabIndex="20" ID="includecombinedpayments" onchange="ApplyBool(this);" 
                                                onclick="CheckSetChanged()" AutoPostBack="true" />
                                        </td>
                                    </tr>
                                   <%--  skhare7 R8 enhancement End--%>
                                    <%--npadhy JIRA 6418 removed the EFT Payment as introduced Distribution type above--%>
                                      <%--JIRA:438 START: ajohari2--%>
                                    <%--<tr>
                                        <td>EFT Payment:&nbsp;&nbsp;</td> 
                                        <td>
                                            <asp:CheckBox runat="server" appearance="full" RMXRef="/Instance/Document//EFTPayment"
                                                TabIndex="7" ID="chkEFTPayment" onchange="ApplyBool(this);" onclick="CheckSetChanged()" AutoPostBack="true" />
                                        </td>
                                    </tr>--%>
                                    <%--JIRA:438 END--%>  
                                    <tr id="">
                                        <td>
                                            # of Prechecks:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                                <asp:TextBox runat="server" size="30" ID="numebrofPrechecks" RMXRef="/Instance/Document//NumberOfPrechecks"
                                                TabIndex="7" ReadOnly="true" Style="background-color:silver"/>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            Total Amounts:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                                <asp:TextBox runat="server" size="30" ID="totalamount" RMXRef="/Instance/Document//TotalAmounts"
                                                TabIndex="8" Style="background-color:silver" ReadOnly="true" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">

                                            <script type="text/javascript" language="JavaScript" src="">{var i;}</script>

                                            <asp:Button runat="server" class="button" ID="printpre" OnClientClick="PrintPre()"
                                                Text="Print" TabIndex="9" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABprintcheckbatch" id="FORMTABprintcheckbatch"
                                    style="display: none;">
                                    <tr id="">
                                        <td>
                                            Bank Account:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="bankaccountprint" runat="server" TabIndex="0" onchange="BankAccountChange('print');" AutoPostBack="true" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            Check Stock:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="checkstock" runat="server" onchange="setDataChanged(true);">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
									<%--npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks--%>  
                                    <tr id="">
                                        <td>
                                            Distribution Type:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDistributionTypePrint" runat="server" AutoPostBack ="true" onchange="BankAccountChange('print');">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
									<%--npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks--%>  
                                    <tr id="">
                                        <td>
                                            First Check #:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <div title="" style="padding: 0px; margin: 0px">
                                                    <asp:TextBox runat="server" size="30" RMXRef="/Instance/Document//FirstCheckNum"
                                                    TabIndex="2" ID="firstcheck" onchange="setDataChanged(true);" MaxLength="18"/></div>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            Check Date:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                        <!--pmittal5 Mits 17988 10/07/09 - Removed Readonly attribute and added button to select date.
                                            Control should be readonly only if "Allow Post Date" is enabled. -->
                                                <%--<asp:TextBox runat="server" size="30" ID="checkdate" RMXRef="/Instance/Document//CheckDate"
                                                TabIndex="3" ReadOnly="true" Style="background-color:silver"/>--%>
                                                <asp:TextBox runat="server" size="30" ID="checkdate" RMXRef="/Instance/Document//CheckDate"
                                                TabIndex="3" onblur="dateLostFocus(this.id);" onchange="validateCheckDate(this.id)"/>
                                                <%--vkumar258 - RMA_6037- Starts --%>

                                                <%--<input runat="server" class="button" id="checkdatebtn" type="button" value="..." />
                                                
                                                <script type="text/javascript">
                                                Zapatec.Calendar.setup(
											    {
											        inputField: "checkdate",
											        ifFormat: "%m/%d/%Y",
											        button: "checkdatebtn"
											    }
											    );
                                                </script>--%>
                                                <!-- End - pmittal5-->
                                                <script type="text/javascript">
                                                    $(function () {
                                                        $("#checkdate").datepicker({
                                                            showOn: "button",
                                                            buttonImage: "../../Images/calendar.gif",
                                                            //buttonImageOnly: true,
                                                            showOtherMonths: true,
                                                            selectOtherMonths: true,
                                                            changeYear: true
                                                        }).next('button.ui-datepicker-trigger').css({ border: 'none', background: 'none' }).attr("tabIndex", "4");
                                                    });
                                                </script>
                                                <%--vkumar258 - RMA_6037- End --%>
                                            </td>
                                        </tr>
                                        <tr id="">
                                            <td>Check Batch:&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <div title="" style="padding: 0px; margin: 0px">
                                                    <asp:textbox runat="server" size="30" rmxref="/Instance/Document//CheckBatchChangedPre/CheckBatch"
                                                        tabindex="4" id="checkbatchpre" onchange="setDataChanged(true);" onblur="CheckBatchChangedPre()" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr id="">
                                            <td>Precheck Date:&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:textbox runat="server" size="30" id="precheckdate" rmxref="/Instance/Document//DatePreCheck"
                                                    tabindex="5" readonly="true" style="background-color: silver" />
                                            </td>
                                        </tr>
                                        <tr id="">
                                            <td># of Checks:&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:textbox runat="server" size="30" id="numberofchecks" rmxref="/Instance/Document//CheckBatchChangedPre/NumberOfChecks"
                                                    tabindex="6" readonly="true" style="background-color: silver" />
                                            </td>
                                        </tr>
									<%--npadhy JIRA 6418 Starts  - We need the Total amount to be printed in the Check in Print Chect Tab as well--%>  
                                    <tr id="">
                                        <td>
                                            Total Amounts:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                                <asp:TextBox runat="server" size="30" ID="totalamountprint" RMXRef="/Instance/Document//TotalAmountsPrint"
                                                TabIndex="8" Style="background-color:silver" ReadOnly="true" />
                                        </td>
                                    </tr>
									<%--npadhy JIRA 6418 Starts  - We need the Total amount to be printed in the Check in Print Chect Tab as well--%>  									
                                    <tr id="">
                                            <td>Order Field:&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:textbox runat="server" size="30" id="orderfieldbatch" rmxref="/Instance/Document//OrderPreCheck"
                                                    tabindex="7" readonly="true" style="background-color: silver" />
                                            </td>
                                        </tr>
                                        <tr id="">
                                            <td colspan="2">

                                            <script type="text/javascript" language="JavaScript" src="">{var i;}</script>

                                            <%--tanwar2 - mits 30910 - runat server probably is causing postback - start--%>
                                            <%--<asp:Button runat="server" class="button" ID="printbatch" OnClientClick="PrintBatch()"
                                                Text="Print" TabIndex="9" />--%>
                                                <input type="button" class="button" id="printbatch" onclick="PrintBatch()" value="Print" tabindex="9" />
                                            <%--tanwar2 - mits 30910 - end--%>
                                        </td>
                                    </tr>
                                    <%--igupta3 Mits : 28566 --%>
                                    <tr id="">
                                        <td colspan="2">
                                            <asp:Label ID="lblError" runat="server" Text="" Visible="false" Font-Size="Medium" Font-Bold="True" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" name="FORMTABpostcheckregister"
                                    id="FORMTABpostcheckregister" style="display: none;">
                                    <tr id="">
                                        <td>
                                            Bank Account:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="bankaccountpost" runat="server" TabIndex="0" onchange="BankAccountChange('post');" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
									<%--npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks--%>  
                                    <tr id="">
                                        <td>
                                            Distribution Type:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDistributionTypePost" runat="server" AutoPostBack ="true" onchange="BankAccountChange('post');">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
									<%--npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks--%>  
                                    <tr id="">
                                        <td>
                                            Check Batch:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <div title="" style="padding: 0px; margin: 0px">
                                                    <asp:TextBox runat="server" size="30" TabIndex="1" RMXRef="/Instance/Document//CheckBatchChangedPost/CheckBatch"
                                                    ID="checkbatchpost" onchange="setDataChanged(true);" onblur="CheckBatchChangedPost()" />
                                                    </div>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            # of Payments:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                                 <asp:TextBox runat="server" size="30" ID="numberofpayments" TabIndex="2" RMXRef="/Instance/Document//NumberOfPayments" ReadOnly="true" Style="background-color:silver" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            Postcheck Date:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                                <asp:TextBox runat="server" size="30" ID="postcheckdate" TabIndex="3" RMXRef="/Instance/Document//DatePostCheck" ReadOnly="true" Style="background-color:silver" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            # of Checks:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                                <asp:TextBox runat="server" size="30" ID="postnumberofchecks" RMXRef="/Instance/Document//CheckBatchChangedPost/NumberOfChecks" ReadOnly="true" Style="background-color:silver" TabIndex="4" />
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            Order Field:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="orderfieldpost" TabIndex="5" onchange="setDataChanged(true);">
                                                <asp:ListItem Value="(None)">(None)</asp:ListItem>
                                                <asp:ListItem Value="Check Total">Check Total</asp:ListItem>
                                                <asp:ListItem Value="Claim Number">Claim Number</asp:ListItem>
                                                <asp:ListItem Value="Control Number">Control Number</asp:ListItem>
                                                <asp:ListItem Value="Payee Name">Payee Name</asp:ListItem>
                                                <asp:ListItem Value="Transaction Date">Transaction Date</asp:ListItem>
                                                <asp:ListItem Value="Check Number">Check Number</asp:ListItem>
                                                <asp:ListItem Value="Sub Bank Account">Sub Bank Account</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">
                                                <asp:RadioButton runat="server" ID="detail" RMXRef="/Instance/Document//RegisterType"
                                                RMXType="radio" onclick="UncheckOthers(this.id);" Checked="true"/>Detail
                                                <asp:RadioButton runat="server" ID="summary" RMXRef="/Instance/Document//RegisterType" 
                                                RMXType="radio" onclick="UncheckOthers(this.id);" />Summary
                                                <asp:RadioButton runat="server" ID="subaccount" RMXRef="/Instance/Document//RegisterType" 
                                                RMXType="radio" onclick="UncheckOthers(this.id);" />Sub Account
                                        </td>
                                    </tr>
                                    <tr id="">
                                        <td colspan="2">

                                            <script type="text/javascript" language="JavaScript" src="">{var i;}</script>

                                            <asp:Button runat="server" class="button" ID="printpost" OnClientClick="PrintPost()"
                                                Text="Print" TabIndex="9" />
                                        </td>
                                        <td colspan="2">

                                            <script type="text/javascript" language="JavaScript" src="">{var i;}</script>

                                            <asp:Button runat="server" class="button" ID="printeob" OnClientClick="PrintEOB()"
                                                Text="Print EOB" TabIndex="10" />
                                               
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
                   <asp:TextBox runat="server"  ID="txtIsPrinterSelected"  style="display: none"/>                                                
                <asp:TextBox runat="server" style="display: none" id="fromdate" rmxref="/Instance/Document//FromDate" />
                <asp:TextBox runat="server" style="display: none" id="todate" rmxref="/Instance/Document//ToDate" />
                <%-- Debabrata Biswas Batch Printing Filter R6 Retrofit MITS 19715/20050 Date: 03/17/2010--%>
                <asp:TextBox runat="server" style="display: none" id="orghierarchy" />
                <asp:TextBox runat="server" value="" style="display: none" id="selectedchecksids"/>
                <asp:TextBox runat="server" value="" style="display: none" id="selectedautochecksids"/>
                <asp:TextBox runat="server" style="display: none" id="printpreflag" rmxref="/Instance/Document//PrintFlag" />
                <asp:TextBox runat="server" style="display: none" id="printbatchflag" rmxref="/Instance/Document//PrintBatch" />
                <asp:TextBox runat="server" style="display: none" id="printpostflag" rmxref="/Instance/Document//PrintPost" />
                <asp:TextBox runat="server" value="" style="display: none" id="insufficientfund" rmxref="/Instance/Document//InsufficientFund" />
                <asp:TextBox runat="server" value="" style="display: none" id="TransDateNull" /><!-- rsushilaggar MITS 37250 11/18/2014 -->
                <asp:TextBox runat="server" value="False" style="display: none" id="usesubbankaccounts" rmxref="/Instance/Document//UseFundsSubAccounts"/>
                <asp:TextBox runat="server" value="" style="display: none" id="FirstFailedCheckNumber" />
                <asp:TextBox runat="server" value="" style="display: none" id="PrintCheckDetails" />
                <asp:TextBox runat="server" value="" style="display: none" id="FileNameAndType" />
                <asp:TextBox runat="server" value="" style="display: none" id="EOBFilesNames" />
               <!--Add by kuladeep for mits:22878 If user select all checks through selection in that case we pass flag only,no check ids-->
                <asp:HiddenField ID="hdnAllCheckSelected" Value="false" runat="server" />         

                <!--pmittal5 Mits 17988 10/07/09 - Added control to store the value of CheckDateType node returned from webservice-->
                <asp:TextBox runat="server" value="" style="display: none" id="CheckDateType" rmxref="/Instance/Document//CheckDateType"/>
                              <asp:TextBox runat="server" value="" style="display: none" id="tbIsEFTAccount" RMXRef="/Instance/Document//IsEFTAccount"/><%--added by Amitosh for EFT Payment--%>
                <input type="text" runat="server" value="PrintChecksAdaptor.OnLoad" style="display: none"
                    id="functiontocall" />
                    <input type="hidden" name="hdnShowPleaseWait" id="hdnShowPleaseWait" />
                    <asp:TextBox runat="server" style="display: none" id="fileandconsolidate"  rmxref="/Instance/Document//FileAndConsolidate"/>
                     <input type="hidden" name="hTabName" id="hTabName"  runat=server  value="precheckregister" />
                    <input type="hidden" name="hdnPrintBatchOk" id="hdnPrintBatchOk"  runat=server  />
            </td>
        </tr>
        <tr>
			<td>
			 <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"  CustomMessage=""/>
			</td>
			</tr>
			<tr>
			<td>
			<script language=javascript type="text/javascript">
			    document.forms[0].onsubmit = showmessage;

			    function showmessage() {
			        
			        if (document.forms[0].hdnShowPleaseWait.value == "")
			        pleaseWait.Show();
			    }
    </script>
			</td>
			</tr>
    </table>
    </form>
</body>
</html>
