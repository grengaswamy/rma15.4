﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Collections;
using System.IO;
using System.Data;

namespace Riskmaster.UI.PrintChecks
{
    public partial class PrintChecks : NonFDMBasePageCWS
    {
        XElement m_objMessageElement = null;
        private string sCWSresponse = "";
        private string sFunctionToCall = string.Empty;
            
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement objTempElement = null;
            //bankaccountpre.Attributes.Add("onchange", "BankAccountChange('pre')");
            sFunctionToCall = this.functiontocall.Value;      
            bool bReturnStatus = false;
            XmlDocument objResponseDoc=null;
            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScriptNonFDM(sCulture, this, 2);//vkumar258 - ML Changes
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);

            }

            try
            {

                //Add by kuladeep for mits:22878 Start---If user select all checks through selection in that case we pass flag only,no check ids.
                if (hdnAllCheckSelected.Value == "true")
                {
                    selectchecks.Checked = false;
                }
                //Add by kuladeep for mits:22878 End
       
                m_objMessageElement = GetMessageTemplate();

                // TODO : Get the session id from the cookie.
                //if (HttpContext.Current.Request.Cookies["SessionId"] != null)
                //{
                //    objTempElement = m_objMessageElement.XPathSelectElement("./Authorization");
                //    objTempElement.Value = HttpContext.Current.Request.Cookies["SessionId"].Value;
                //}

                //BindNonFDMCWSControlCollection2Data(this.Form.Controls, m_objMessageElement);
                if(sFunctionToCall != "")
                {
                    //bReturnStatus = CallCWSFunctionNoBind(sFunctionToCall, out sCWSresponse, m_objMessageElement);
                    //igupta3 Mits : 28566 To restrict duplicate requests while print checks. PageLoad() is called twice.
                    if ((sFunctionToCall != "PrintChecksAdaptor.UpdateStatusForPrintedChecks") || (sFunctionToCall == "PrintChecksAdaptor.UpdateStatusForPrintedChecks" && this.hdnRequest.Value != "START"))
                    {
                        this.hdnRequest.Value = "START";

                        bReturnStatus = CallCWS(sFunctionToCall, m_objMessageElement, out sCWSresponse, false, true);
                        //Start rsushilaggar MITS 37250 11/18/2014
                        if (string.Equals(sFunctionToCall, "PrintChecksAdaptor.GetInsufficientFundsPreEditFlag"))
                        {

                            objResponseDoc = new XmlDocument();

                            objResponseDoc.LoadXml(sCWSresponse);

                            if (objResponseDoc.SelectSingleNode("ResultMessage/MsgStatus/MsgStatusCd").InnerText == "Error")
                            {
                                TransDateNull.Text = "True";
                            }
                            else
                            {
                                TransDateNull.Text = "False";
                                if (ErrorControl1.FindControl("lblError") != null)
                                {
                                    ((Label)(ErrorControl1.FindControl("lblError"))).Text = "";
                                }

                            }

                        }
                        else
                        {
                            TransDateNull.Text = "False";
                            if (ErrorControl1.FindControl("lblError") != null)
                            {
                                ((Label)(ErrorControl1.FindControl("lblError"))).Text = "";
                            }
                        }
                        //End rsushilaggar MITS 37250 11/18/2014
                        if (sFunctionToCall == "PrintChecksAdaptor.PostCheckDetail" || sFunctionToCall == "PrintChecksAdaptor.PostCheckSummary" || sFunctionToCall == "PrintChecksAdaptor.PostCheckSubAccount")
                        {
                            objTempElement = XElement.Parse(sCWSresponse);
                            objTempElement = objTempElement.XPathSelectElement("./Document/PrintChecks/File");
                            //TextBox txtPdfFile = (TextBox)this.Form.FindControl("File");

                        byte[] pdfbytes = Convert.FromBase64String(objTempElement.Value);

                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Charset = "";
                        Response.AppendHeader("Content-Encoding", "none;");
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", string.Format("attachment; filename=PrintCheck.pdf"));
                        Response.AddHeader("Accept-Ranges", "bytes");
                        Response.BinaryWrite(pdfbytes);
                        Response.Flush();
                        //Response.Close();//JIRA 12179 not working in case of IE 11 
                        try
                        {
                            Response.End();
                        }
                        catch { }
                    } // if
                        //igupta3 Mits : 28566 Changes starts
                        string TemError = String.Empty;
                        XmlDocument usersXDocTemp = new XmlDocument();
                        usersXDocTemp.LoadXml(sCWSresponse);
                        if (usersXDocTemp.SelectSingleNode("//WorklossError") != null)
                        {
                            lblError.Visible = false;
                            lblError.Text = usersXDocTemp.SelectSingleNode("//WorklossError").InnerText;
                        }
                        this.hdnRequest.Value = string.Empty;
                        //igupta3 Mits : 28566 Changes ends
                    }
                } // if

                if (!IsPostBack)
                {
                    BindpageControls(sCWSresponse);
                    objTempElement = XElement.Parse(sCWSresponse);
                    objTempElement = objTempElement.XPathSelectElement("//IsPrinterSelected");
                    txtIsPrinterSelected.Text = objTempElement.Value;

                    fromdate.Text = AppHelper.GetDate(DateTime.Now.ToShortDateString());
                   todate.Text = AppHelper.GetDate(DateTime.Now.ToShortDateString());
                   checkdate.Text = AppHelper.GetDate(DateTime.Now.ToShortDateString());



                } // if
                else
                {
                    if (sFunctionToCall == "PrintChecksAdaptor.AccountChanged")
                    {
                        BindpageControls(sCWSresponse);

                    }
                    checkdate.Text = AppHelper.GetDate(DateTime.Now.ToShortDateString());
                }
            } // try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

         private XElement GetMessageTemplate()
        {
            //b5848889-63e9-4ce9-8f26-eba5593d61f0
            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
            if (sFunctionToCall == "PrintEOBAdaptor.DeleteEobFiles")
            {
                sXml = sXml.Append("<PostCheckEOB>");
                sXml = sXml.Append("<FileNames>");
                sXml = sXml.Append(this.EOBFilesNames.Text);
                sXml = sXml.Append("</FileNames>");
                sXml = sXml.Append("</PostCheckEOB></Document></Message>");

                XElement oTemplateEOB = XElement.Parse(sXml.ToString());

                return oTemplateEOB;
            }
            sXml = sXml.Append("<PrintChecks>");

            if (sFunctionToCall == "PrintChecksAdaptor.AccountChanged")
            {
                sXml = sXml.Append("<FromDateFlag>");
                sXml = sXml.Append(this.fromdateflag.Checked.ToString());
                sXml = sXml.Append("</FromDateFlag>");
                sXml = sXml.Append("<ToDateFlag>");
                sXml = sXml.Append(this.todateflag.Checked.ToString());
                sXml = sXml.Append("</ToDateFlag>");
                sXml = sXml.Append("<AttachedClaimOnly>");
                sXml = sXml.Append("</AttachedClaimOnly>");
                sXml = sXml.Append("<IncludeAutoPayment>");
                sXml = sXml.Append(this.includeautopayments.Checked.ToString());
                sXml = sXml.Append("</IncludeAutoPayment>");
                //skhare7 R8 enhancement
                sXml = sXml.Append("<IncludeCombinedPayment>");
                sXml = sXml.Append(this.includecombinedpayments.Checked.ToString());
                sXml = sXml.Append("</IncludeCombinedPayment>");
                //skhare7 R8 Enhancement End
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                //JIRA:438 START: ajohari2
                //sXml = sXml.Append("<PrintEFTPayment>");
                //sXml = sXml.Append(true);
                //sXml = sXml.Append("</PrintEFTPayment>");
                //JIRA:438 End: 
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePre.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                sXml = sXml.Append("<GenerateBatchNo>");
                //if (hTabName.Value == "precheckregister")
                //    sXml = sXml.Append("true");
                //else
                    sXml = sXml.Append("false");
                sXml = sXml.Append("</GenerateBatchNo>");
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                sXml = sXml.Append("<FromDate>");
                sXml = sXml.Append(this.fromdate.Text);
                sXml = sXml.Append("</FromDate>");
                sXml = sXml.Append("<ToDate>");
                sXml = sXml.Append(this.todate.Text);
                sXml = sXml.Append("</ToDate>");
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountpre.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                //Start Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010
                sXml = sXml.Append("<OrgHierarchy>");
                sXml = sXml.Append(this.orghierarchy.Text);
                sXml = sXml.Append("</OrgHierarchy>");
                sXml = sXml.Append("<OrgHierarchyLevel>");
                sXml = sXml.Append(this.orghierarchylevelpre.SelectedValue.ToString());
                sXml = sXml.Append("</OrgHierarchyLevel>");
                //End Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010
            } // if

            if (sFunctionToCall == "PrintChecksAdaptor.CheckSetChanged")
            {
                sXml = sXml.Append("<FromDateFlag>");
                sXml = sXml.Append(this.fromdateflag.Checked.ToString());
                sXml = sXml.Append("</FromDateFlag>");
                sXml = sXml.Append("<ToDateFlag>");
                sXml = sXml.Append(this.todateflag.Checked.ToString());
                sXml = sXml.Append("</ToDateFlag>");
                sXml = sXml.Append("<AttachedClaimOnly>");
                sXml = sXml.Append("</AttachedClaimOnly>");
                sXml = sXml.Append("<IncludeAutoPayment>");
                sXml = sXml.Append(this.includeautopayments.Checked.ToString());
                sXml = sXml.Append("</IncludeAutoPayment>");
                //skhare7 R8 enhancement
                sXml = sXml.Append("<IncludeCombinedPayment>");
                sXml = sXml.Append(this.includecombinedpayments.Checked.ToString());
                sXml = sXml.Append("</IncludeCombinedPayment>");
                //skhare7 R8 Enhancement End
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                //JIRA:438 START: ajohari2
                //sXml = sXml.Append("<PrintEFTPayment>");
                //sXml = sXml.Append(this.chkEFTPayment.Checked.ToString());
                //sXml = sXml.Append("</PrintEFTPayment>");
                //JIRA:438 End: 
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePre.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
				// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                sXml = sXml.Append("<FromDate>");
                sXml = sXml.Append(this.fromdate.Text);
                sXml = sXml.Append("</FromDate>");
                sXml = sXml.Append("<ToDate>");
                sXml = sXml.Append(this.todate.Text);
                sXml = sXml.Append("</ToDate>");
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountpre.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                //Start Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010
                sXml = sXml.Append("<OrgHierarchy>");
                sXml = sXml.Append(this.orghierarchy.Text);
                sXml = sXml.Append("</OrgHierarchy>");
                sXml = sXml.Append("<OrgHierarchyLevel>");
                sXml = sXml.Append(this.orghierarchylevelpre.SelectedValue.ToString());
                sXml = sXml.Append("</OrgHierarchyLevel>");
                //End Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010
                sXml = sXml.Append("<UsingSelection>");
                if(!this.selectchecks.Checked)
                {
                    sXml = sXml.Append("false");
                } // if   
                else
                {
                    sXml = sXml.Append("true");
                } // else
                sXml = sXml.Append("</UsingSelection>");
                sXml = sXml.Append("<TransIds>");
                sXml = sXml.Append(this.selectedchecksids.Text);
                sXml = sXml.Append("</TransIds>");
                sXml = sXml.Append("<AutoTransIds>");
                sXml = sXml.Append(this.selectedautochecksids.Text);
                sXml = sXml.Append("</AutoTransIds>");
            } // if

            if (sFunctionToCall == "PrintChecksAdaptor.CheckBatchChangedPre")
            {
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountprint.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                sXml = sXml.Append("<BatchNumber>");
                sXml = sXml.Append(this.checkbatchpre.Text);
                sXml = sXml.Append("</BatchNumber>");
                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePrint.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            } // if

            if (sFunctionToCall == "PrintChecksAdaptor.UpdateStatusForPrintedChecks")
            {
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountprint.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                sXml = sXml.Append("<BatchNumber>");
                sXml = sXml.Append(this.checkbatchpre.Text);
                sXml = sXml.Append("</BatchNumber>");
                sXml = sXml.Append("<FirstFailedCheckNumber>");
                sXml = sXml.Append(this.FirstFailedCheckNumber.Text);
                sXml = sXml.Append("</FirstFailedCheckNumber>");
                sXml = sXml.Append("<PrintCheckDetails>");
                //sXml = sXml.Append(AppHelper.HTMLCustomDecode(this.PrintCheckDetails.Text));// Reverting : 35827 bkuzhanthaim
				sXml = sXml.Append(this.PrintCheckDetails.Text);
                sXml = sXml.Append("</PrintCheckDetails>");
                sXml = sXml.Append("<FilesNameAndType>");
                sXml = sXml.Append(this.FileNameAndType.Text);
                sXml = sXml.Append("</FilesNameAndType>");
                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePrint.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks 
            } // if

            if (sFunctionToCall == "PrintChecksAdaptor.CheckBatchChangedPost")
            {
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountpost.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                sXml = sXml.Append("<BatchNumber>");
                sXml = sXml.Append(this.checkbatchpost.Text);
                sXml = sXml.Append("</BatchNumber>");
                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePost.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            } // if

            if (sFunctionToCall == "PrintChecksAdaptor.SavePreCheckDetail")
            {
                sXml = sXml.Append("<FromDateFlag>");
                sXml = sXml.Append(this.fromdateflag.Checked.ToString());
                sXml = sXml.Append("</FromDateFlag>");
                sXml = sXml.Append("<ToDateFlag>");
                sXml = sXml.Append(this.todateflag.Checked.ToString());
                sXml = sXml.Append("</ToDateFlag>");
                sXml = sXml.Append("<AttachedClaimOnly>");
                sXml = sXml.Append("</AttachedClaimOnly>");
                sXml = sXml.Append("<IncludeAutoPayment>");
                sXml = sXml.Append(this.includeautopayments.Checked.ToString());
                sXml = sXml.Append("</IncludeAutoPayment>");
                //skhare7 R8 enhancement
                sXml = sXml.Append("<IncludeCombinedPayment>");
                sXml = sXml.Append(this.includecombinedpayments.Checked.ToString());
                sXml = sXml.Append("</IncludeCombinedPayment>");
                //skhare7 R8 Enhancement End
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                //JIRA:438 START: ajohari2
                //sXml = sXml.Append("<PrintEFTPayment>");
                //sXml = sXml.Append(this.chkEFTPayment.Checked.ToString());
                //sXml = sXml.Append("</PrintEFTPayment>");
                //JIRA:438 End: 
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePre.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                sXml = sXml.Append("<FromDate>");
                sXml = sXml.Append(this.fromdate.Text);
                sXml = sXml.Append("</FromDate>");
                sXml = sXml.Append("<ToDate>");
                sXml = sXml.Append(this.todate.Text);
                sXml = sXml.Append("</ToDate>");
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountpre.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                //Start Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010
                sXml = sXml.Append("<OrgHierarchy>");
                sXml = sXml.Append(this.orghierarchy.Text);
                sXml = sXml.Append("</OrgHierarchy>");
                sXml = sXml.Append("<OrgHierarchyLevel>");
                sXml = sXml.Append(this.orghierarchylevelpre.SelectedValue.ToString());
                sXml = sXml.Append("</OrgHierarchyLevel>");
                //End Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010
                sXml = sXml.Append("<UsingSelection>");
                if (!this.selectchecks.Checked)
                {
                    sXml = sXml.Append("false");
                } // if   
                else
                {
                    sXml = sXml.Append("true");
                } // else
                sXml = sXml.Append("</UsingSelection>");
                sXml = sXml.Append("<TransIds>");
                sXml = sXml.Append(this.selectedchecksids.Text);
                sXml = sXml.Append("</TransIds>");
                sXml = sXml.Append("<AutoTransIds>");
                sXml = sXml.Append(this.selectedautochecksids.Text);
                sXml = sXml.Append("</AutoTransIds>");
                sXml = sXml.Append("<OrderBy>");
                sXml = sXml.Append(this.orderfieldpre.SelectedValue.ToString());
                sXml = sXml.Append("</OrderBy>");
                sXml = sXml.Append("<InsufficientFund>");
                sXml = sXml.Append(this.insufficientfund.Text);
                sXml = sXml.Append("</InsufficientFund>");
            } // if


            if (sFunctionToCall == "PrintChecksAdaptor.PostCheckDetail" || sFunctionToCall == "PrintEOBAdaptor.PostCheckEOB")
            {
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountpost.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                sXml = sXml.Append("<BatchNumber>");
                sXml = sXml.Append(this.checkbatchpost.Text);
                sXml = sXml.Append("</BatchNumber>");
                sXml = sXml.Append("<PostcheckDate>");
                sXml = sXml.Append(this.postcheckdate.Text);
                sXml = sXml.Append("</PostcheckDate>");
                sXml = sXml.Append("<OrderBy>");
                sXml = sXml.Append(this.orderfieldpost.Text);
                sXml = sXml.Append("</OrderBy>");
                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePost.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            } // if

            if (sFunctionToCall == "PrintChecksAdaptor.PostCheckSummary" || sFunctionToCall == "PrintChecksAdaptor.PostCheckSubAccount")
            {
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountpost.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                sXml = sXml.Append("<BatchNumber>");
                sXml = sXml.Append(this.checkbatchpost.Text);
                sXml = sXml.Append("</BatchNumber>");
                sXml = sXml.Append("<PostcheckDate>");
                sXml = sXml.Append(this.postcheckdate.Text);
                sXml = sXml.Append("</PostcheckDate>");
                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePost.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            } // if

            if (sFunctionToCall == "PrintChecksAdaptor.GetInsufficientFundsPreEditFlag" || sFunctionToCall == "PrintChecksAdaptor.PostCheckSubAccount")
            {
                sXml = sXml.Append("<FromDateFlag>");
                sXml = sXml.Append(this.fromdateflag.Checked.ToString());
                sXml = sXml.Append("</FromDateFlag>");
                sXml = sXml.Append("<ToDateFlag>");
                sXml = sXml.Append(this.todateflag.Checked.ToString());
                sXml = sXml.Append("</ToDateFlag>");
                sXml = sXml.Append("<IncludeAutoPayment>");
                sXml = sXml.Append(this.includeautopayments.Checked.ToString());
                sXml = sXml.Append("</IncludeAutoPayment>");
                //skhare7 R8 enhancement
                sXml = sXml.Append("<IncludeCombinedPayment>");
                sXml = sXml.Append(this.includecombinedpayments.Checked.ToString());
                sXml = sXml.Append("</IncludeCombinedPayment>");
                //skhare7 R8 Enhancement End
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                //JIRA:438 START: ajohari2
                //sXml = sXml.Append("<PrintEFTPayment>");
                //sXml = sXml.Append(this.chkEFTPayment.Checked.ToString());
                //sXml = sXml.Append("</PrintEFTPayment>");
                //JIRA:438 End: 
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePre.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
                // So introducing Distribution Type and removing out EFTPayment
                sXml = sXml.Append("<FromDate>");
                sXml = sXml.Append(this.fromdate.Text);
                sXml = sXml.Append("</FromDate>");
                sXml = sXml.Append("<ToDate>");
                sXml = sXml.Append(this.todate.Text);
                sXml = sXml.Append("</ToDate>");
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountpre.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                //Start Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010
                sXml = sXml.Append("<OrgHierarchy>");
                sXml = sXml.Append(this.orghierarchy.Text);
                sXml = sXml.Append("</OrgHierarchy>");
                sXml = sXml.Append("<OrgHierarchyLevel>");
                sXml = sXml.Append(this.orghierarchylevelpre.SelectedValue.ToString());
                sXml = sXml.Append("</OrgHierarchyLevel>");
                //End Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010
                if(sFunctionToCall == "PrintChecksAdaptor.GetInsufficientFundsPreEditFlag")
                {
                    sXml = sXml.Append("<UsingSelection>");
                    sXml = sXml.Append(this.selectchecks.Checked);
                    sXml = sXml.Append("</UsingSelection>");
                    sXml = sXml.Append("<TransIds>");
                    sXml = sXml.Append(this.selectedchecksids.Text);
                    sXml = sXml.Append("</TransIds>");
                    //skhare7 R8 enhancement
                    sXml = sXml.Append("<IncludeCombinedPayment>");
                    sXml = sXml.Append(this.includecombinedpayments.Checked.ToString());
                    sXml = sXml.Append("</IncludeCombinedPayment>");
                    //skhare7 R8 Enhancement End
                    sXml = sXml.Append("<AutoTransIds>");
                    sXml = sXml.Append(this.selectedautochecksids.Text);
                    sXml = sXml.Append("</AutoTransIds>");
                } // if

            } // if

            sXml = sXml.Append("</PrintChecks></Document></Message>");

            XElement oTemplate = XElement.Parse(sXml.ToString());


            return oTemplate;
        }

        private void BindpageControls(string sreturnValue)
        {
            DataSet usersRecordsSet = null;
            XmlDocument usersXDoc = new XmlDocument();
            usersXDoc.LoadXml(sreturnValue);
            if (usersXDoc.SelectSingleNode("//CheckNumberChangePrermission") != null)
            {
                if (usersXDoc.SelectSingleNode("//CheckNumberChangePrermission").InnerText == "true")
                {
                    this.firstcheck.ReadOnly = true;
                    this.firstcheck.Style.Add("background", "silver");
                }
            }
            //Debabrata Biswas : MCIC Batch Printing Filter MITS 20050 Date: 04/22/2010
            DropDownList lstOrgHierarchy = (DropDownList)this.Form.FindControl("orghierarchylevelpre");
            if (usersXDoc.SelectSingleNode("//OrgHierarchyLevel") != null)
            {
                string sOrgHierarcy = usersXDoc.SelectSingleNode("//OrgHierarchyLevel").InnerText;
                lstOrgHierarchy.SelectedValue = sOrgHierarcy;

            }
            //JIRA-12397
            //check for bank sub account order by crashing when we select bank sub account so add bank sub account to order by only when its setting is ON 
            if (usesubbankaccounts.Text.ToLower() == "false")
            {
                //orderfieldpre.SelectedIndex = 0;
                DropDownList ddorderfieldpre = (DropDownList)this.Form.FindControl("orderfieldpre");
                if (ddorderfieldpre!=null)
                {
                    ddorderfieldpre.Items.Remove(ddorderfieldpre.Items.FindByValue("Sub Bank Account"));
                }
                DropDownList ddorderfieldpost = (DropDownList)this.Form.FindControl("orderfieldpost");
                if (ddorderfieldpost != null)
                {
                    ddorderfieldpost.Items.Remove(ddorderfieldpost.Items.FindByValue("Sub Bank Account"));
                }
            }

            //JIRA:438 START: ajohari2
            if (usersXDoc.SelectSingleNode("//IsEFTAccount") != null)
            {
                if (usersXDoc.SelectSingleNode("//IsEFTAccount").InnerText.ToLower() == "true")
                {
					// npadhy JIRA 6418 Ends As we have removed the chkEFTPayment control from page, we remove any refernce of the control from the code behind as well
                    //this.chkEFTPayment.Enabled = true;
                    if (usersXDoc.SelectSingleNode("//PrintEFTPayment") != null && usersXDoc.SelectSingleNode("//PrintEFTPayment").InnerText.ToLower() == "true")
                    {
                        //this.chkEFTPayment.Checked = true;
                        ddlDistributionTypePre.SelectedValue = usersXDoc.SelectSingleNode("//DistributionType").InnerText;
                    }
					// npadhy JIRA 6418 Ends As we have removed the chkEFTPayment control from page, we remove any refernce of the control from the code behind as well
                }
            }
            //JIRA:438 End: 

            //Debabrata end
            usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
            //if (stateRecordsSet.Tables[5] != null)
            //{
            //    grdStateMaint.DataSource = stateRecordsSet.Tables[5];
            //    grdStateMaint.DataBind();
            //}
            DropDownList lstBankAccountPre = (DropDownList)this.Form.FindControl("bankaccountpre");
            DropDownList lstBankAccountPrint = (DropDownList)this.Form.FindControl("bankaccountprint");
            DropDownList lstBankAccountPost = (DropDownList)this.Form.FindControl("bankaccountpost");
            if (usersRecordsSet.Tables["Account"] != null)
            {
               
                lstBankAccountPre.DataSource = usersRecordsSet.Tables["Account"].DefaultView;
                lstBankAccountPre.DataTextField = "AccountName";
                lstBankAccountPre.DataValueField = "AccountID";
                lstBankAccountPre.DataBind();

                if (usersRecordsSet.Tables["Account"] != null)
                {
                    lstBankAccountPrint.DataSource = usersRecordsSet.Tables["Account"].DefaultView;
                    lstBankAccountPrint.DataTextField = "AccountName";
                    lstBankAccountPrint.DataValueField = "AccountID";
                    lstBankAccountPrint.DataBind();
                }

               
                lstBankAccountPost.DataSource = usersRecordsSet.Tables["Account"].DefaultView;
                lstBankAccountPost.DataTextField = "AccountName";
                lstBankAccountPost.DataValueField = "AccountID";
                lstBankAccountPost.DataBind();
            }
            DropDownList lstCheckStock = (DropDownList)this.Form.FindControl("checkstock");
            if (usersRecordsSet.Tables["Stock"] != null)
            {
                lstCheckStock.DataSource = usersRecordsSet.Tables["Stock"].DefaultView;
                lstCheckStock.DataTextField = "Name";
                lstCheckStock.DataValueField = "ID";
                lstCheckStock.DataBind();
            }
            else
            {
                lstCheckStock.Items.Clear();
            }
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            // Bind Distribution Type as well 

            if (usersRecordsSet.Tables["DistributionType"] != null)
            {

                ddlDistributionTypePre.DataSource = usersRecordsSet.Tables["DistributionType"].DefaultView;
                ddlDistributionTypePre.DataTextField = "DistributionTypeDesc";
                ddlDistributionTypePre.DataValueField = "DistributionTypeId";
                ddlDistributionTypePre.DataBind();

                ddlDistributionTypePrint.DataSource = usersRecordsSet.Tables["DistributionType"].DefaultView;
                ddlDistributionTypePrint.DataTextField = "DistributionTypeDesc";
                ddlDistributionTypePrint.DataValueField = "DistributionTypeId";
                ddlDistributionTypePrint.DataBind();

                ddlDistributionTypePost.DataSource = usersRecordsSet.Tables["DistributionType"].DefaultView;
                ddlDistributionTypePost.DataTextField = "DistributionTypeDesc";
                ddlDistributionTypePost.DataValueField = "DistributionTypeId";
                ddlDistributionTypePost.DataBind();
            }
            // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            if (usersRecordsSet != null)
            {
                usersRecordsSet.Dispose();
            }

        }
    }    
}
