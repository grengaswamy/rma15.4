﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostCheckEob.aspx.cs" Inherits="Riskmaster.UI.PrintChecks.PostCheckEob"%>

<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="Riskmaster.BusinessAdaptor.Common" %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

 <base target=_self>
    <title></title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/PrintChecks.js")%>'></script>
    
    <script language='javascript' type="text/javascript">
    
        function checkReadyState() {

            if (document.forms[0].hdnFileRequest.value == "1") {
                //JIRA RMA-9120 - ajohari2 Start

                var strreadyState = null;
                if (document.readyState != null) {
                    strreadyState = document.readyState;
                }
                //while (document.all.file.document.readyState != "complete") {
                while (strreadyState != "complete") {
                    status += "."; // just wait ...
                }
                //JIRA RMA-9120 - ajohari2 End

                pleaseWait.pleaseWait('stop');
            }
        }
             

    function submitthisPage()
    {
    
        if (document.forms[0].hdnFirstTime != null) {
            if (document.forms[0].hdnFirstTime.value == "0") {
                document.forms[0].hdnFirstTime.value = "1";
                document.forms[0].hdnPrintRequest.value = "1";
                document.forms[0].submit();
                pleaseWait.Show();

            }
        }
        
     }   
    </script>
</head>
<body onload="javascript:submitthisPage();" onunload="PrintEOBOnClose()">

  
    <form id="frmData" runat="server"  >
   
   
       
      <table width="100%" cellspacing="0" cellpadding="2" border="0">  
     
    		 <%try
       {
           %>
       
					<% if (Model.SelectSingleNode("//PrintEOB") != null)
        {%>  
          <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />		    
		<tr class="msgheader">
						<td colspan="2">
								Post Check EOB Report
						</td>
    				</tr>
        <%if (Model.SelectSingleNode("//PrintEOB").Attributes["PrintDirectlyToPrinter"].Value == "false")
          { %>
	            
	             <%if (Model.SelectSingleNode("//PrintEOB").Attributes["FileNames"].Value.Trim() != "") { %>
	            <tr>
					<td>
								The Following Post Check EOB report is attached to this check batch :
								</td>
							
							</tr>
				<%}else{ %>
				<tr>	
				<td>
								 No Post Check EOB report has been found in this check batch
								</td>
							</tr>
				 <%}%>
						    <%if (Model.SelectSingleNode("//PrintEOB").Attributes["FileNames"].Value.Trim() != "") { %>  
						<tr>
									<td>
										<div style="position:relative;width:420px;height:70px;overflow:auto" class="divScroll">
											<table>
												<ul>
													
														<tr>
															<td width="5%">
															</td>
															<td>
																<li>

																<% string sFileName=Model.SelectSingleNode("//PrintEOB").Attributes["FileNames"].Value;%>
																<a  id='eob' href="javascript:PrintSelectedReport('eob','<%=sFileName%>')">
															     EOB Report 													
																</a>
																</li>				
															</td>
															
														</tr>
													
												</ul>
											</table>
										</div>
									</td>
								</tr>
                                <%}%>
							
<%         } %>
        
        <%else
            { %>
        <tr>
            <td>
                EOB reports are printed sucessfully.
            </td>
        </tr>
        <%         } %>
						<tr>
						<td>
						<br />
							<asp:Button ID="btnOk" runat="server"  OnClientClick="return PrintEOBOk();"
             Text="   Ok   "  CssClass=button 
              /> 				   <asp:Button ID="Button1" runat="server" Text="Delete"  onclick="btnDelete_Click" Visible=false/>										
						</td>							
					</tr>
					
					<%         } %>	
				
			<%        Model = null; %>
				<%}
       catch (Exception ee)
       {
           ErrorHelper.logErrors(ee);
           //BusinessAdaptorErrors err = new BusinessAdaptorErrors();
           //err.Add(ee, BusinessAdaptorErrorType.SystemError);
           //lblErrors.Text =ErrorHelper.FormatServiceErrors( ErrorHelper.formatUIErrorXML(err));
           lblErrors.Text =ErrorHelper.FormatErrorsForUI("Error Printing EOB report.");
       }%>		
					<tr>
						<td>
                         
					   <asp:HiddenField ID="hdnIsBatchPrint" runat="server" />
              
           <asp:HiddenField ID="hdnBatchNumber" runat="server" />
           <asp:HiddenField ID="hdnAccountId" runat="server" />
       
         <asp:HiddenField ID="hdnOrderBy" runat="server" />
          <asp:HiddenField ID="hdnPostCheckDate" runat="server" />  
        <%--npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks--%>          
        <asp:HiddenField ID="hdnDistributionType" runat="server" />
        <%--npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks--%>          
         <asp:HiddenField ID="hdnFirstTime" runat="server"  Value="0"/>
         <asp:HiddenField ID="hdnFileNames" runat="server"  Value="0"/>
         <input type='hidden' id="hdnModelXml" runat=server ></input>
          	<input type='hidden' id="hdnFileRequest" runat=server ></input>
						<input type='hidden' id="hdnPrintRequest" runat=server  value="0"></input>
					<input type='hidden' id="hdnFileName" runat=server ></input>
					<input type='hidden' id="hdnFileType" runat=server ></input>        
					<tr>
					<td>
			<iframe frameborder="0" width="0" height="0" src="" id="file"></iframe>
					 <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"   CustomMessage="Printing" />
					 </td>
					</tr>
				</table>
			
        <asp:Label ID="lblErrors" runat=server></asp:Label>		
    </form>
</body>
</html>