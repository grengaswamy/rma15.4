﻿<%--/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintChecksBatch.aspx.cs" Inherits="Riskmaster.UI.PrintChecks.PrintChecksBatch"%>
<%@ Import Namespace="System.Xml" %>

 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register src="~/UI/Shared/Controls/CommonTasks.ascx" Tagname="CommonTasks" tagprefix="uc4" %>
<%@ Register src="~/UI/Shared/Controls/PleaseWaitDialog.ascx" Tagname="PleaseWaitDialog" tagprefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat=server>

 <base target=_self>
    <title><%#ChangeTitle()%></title>
    <uc4:CommonTasks ID="CommonTasks1" runat="server" />
    <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/PrintChecks.js")%>'></script>
    <script  type="text/javascript" language="javascript" src='<%#Page.ResolveUrl("~/Scripts/jquery/jquery-1.8.0.js")%>'></script>
    
    <script language='javascript' type="text/javascript">
        var sJobid = '';
        var sDsn = '';
        //bkuzhanthaim : RMA-10271,10491,RMA-10253 : show modal issue in chrome
       $(document).ready(function () {
            var IEbrowser = false || !!document.documentMode; //work upto IE6;
            if (!IEbrowser) {
                var grandParentDoc = window.opener.parent.parent.document;//window.dialogArguments.parent.document;
                if (grandParentDoc.getElementById("overlaydiv") == null) {
                    $('#cphHeaderBody', grandParentDoc).prepend("<div id=\"overlaydiv\" class=\"overlay\"> </div>");
                    $('.overlay', grandParentDoc).show();
                }
            }
        });
        function PrinttCheckLocal() {
            wnd = window.open('alert:CSCJOBID=' + sJobid + ' CSCDSNID=' + sDsn, 'PrintCheck', 'scrollbars=no,resizable=no,width=50,height=50' + ',top=' + (screen.availHeight - 50) / 2 + ',left=' + (screen.availWidth - 50) / 2);
            try {
                wnd.focus();
                var t = setTimeout(function () { wnd.close() }, 3000);//RMA-19619
                wnd.close();
            }
            catch (e) {
            }
            return false;
        }
        
        function checkReadyState() {
            
            if (document.forms[0].hdnFileRequest.value == "1") {
                //JIRA RMA-9120 - ajohari2 Start

                var strreadyState = null;
                if (document.readyState != null)
                {
                    strreadyState = document.readyState;
                }
                //while (document.all.file.document.readyState != "complete") {
                while (strreadyState != "complete") {
                    status += "."; // just wait ...
                }
                //JIRA RMA-9120 - ajohari2 End

                pleaseWait.pleaseWait('stop');
            }
        }
             

    function submitthisPage() {

       
        if (document.forms[0].hdnFirstTime != null)
         {
             if (document.forms[0].hdnFirstTime.value == "0") 
            {
                document.forms[0].hdnFirstTime.value = "1";
                document.forms[0].hdnPrintRequest.value = "1";
                document.forms[0].submit();
                pleaseWait.Show();

            }
        }

        //tanwar2 - mits 30910 - start
        var btnCancel = document.getElementById('btnCancel');
        var hdnZeroChecks = document.getElementById('hdnZeroChecks');

        if (btnCancel != null && hdnZeroChecks != null) {
            if (hdnZeroChecks.value=="2") {
                btnCancel.style.visibility = "visible";
            }
        }
        //tanwar2 - mits 30910 - end
    }

    function PrintBatchCancel() {
        var hdnZeroChecks = document.getElementById('hdnZeroChecks');
        if (hdnZeroChecks != null) {
            hdnZeroChecks.value = "2";
        }
        window.close();
    }
    function HideOverlaydiv() {
        //aravi5 : RMA-10253,RMA-10491 : show modal issue in chrome
        var IEbrowser = false || !!document.documentMode; // At least IE6
        if (!IEbrowser) {
            if (window.opener.parent.parent.document.getElementById("overlaydiv") != null) {
                window.opener.parent.parent.document.getElementById("overlaydiv").remove();
            }
            return false;
        }
    }
    </script>
</head>
<body onpagehide="HideOverlaydiv(); return false;" onload="javascript:submitthisPage();" onUnload="PrintBatchOnClose();"><%-- aravi5 RMA-10253 --%>

  
    <form id="frmData" runat="server"  >
   
    <rmcontrol:ErrorControl ID="ErrorControl1" Runat="server" />  
   
    				<table width="100%" cellspacing="0" cellpadding="2" border="0">  
    				          
    			
					<input type='hidden' id="StartNum" runat=server ></input>
					<input type='hidden' id="EndNum" runat=server ></input>
					<input type='hidden' id="hdnBatchPrint" runat=server ></input>
					<input type='hidden' id="hdnFileRequest" runat=server ></input>
					<input type='hidden' id="hdnPrintRequest" runat=server  value="0"></input>
					<input type='hidden' id="hdnFileName" runat=server ></input>
					<input type='hidden' id="hdnFileType" runat=server ></input>
					<input type='hidden' id="hdnModelXml" runat=server ></input>
					<input type='hidden' id="hdnPrintCheckDetails" runat=server ></input>
                    <input type='hidden' id="hdnPrintMode" runat=server ></input>
                    <input type='hidden' id="hdnRollUpId" runat=server ></input>
                    <input type="hidden" id="hdnPostBack" value="<%=Page.IsPostBack.ToString()%>" />
					<%try
       {
           %>
       
					<% if (Model.SelectSingleNode("//PrintCheckDetails") != null)
        {%>
					<tr>
						<td colspan="2">
							&#160;
						</td>
					</tr>
					<tr class="msgheader" id ="checkandeob" runat="server">
						<td colspan="2" >
							Checks and EOB Reports 
						</td>
                        	</tr>
                    <%--tanwar2 - mits 30910 - start--%>
                     <%--Changed by Nikhil on 09/22/14.Code Review changes--%>
                   <%-- <%if (Model.SelectSingleNode("//ZeroChecksDueToDeductible") != null && Model.SelectSingleNode("//ZeroChecksDueToDeductible").InnerText == "-1")--%>
                    <%if (Model.SelectSingleNode("//ZeroChecksDueToDeductible") != null && string.Compare(Model.SelectSingleNode("//ZeroChecksDueToDeductible").InnerText.ToString(),"-1") == 0)
                      {
                                    if (string.Compare(hdnBatchPrint.Value,"SingleCheck",true) == 0)
                                    {
                                        Response.Write(Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificObject("lblSingleCheckNotPrinted", Riskmaster.RMXResourceManager.RMXResourceProvider.PageId("PrintChecksBatch.aspx"), ((int)Riskmaster.RMXResourceManager.RMXResourceProvider.RessoureType.LABEL).ToString()));
                                        //Response.Write("<tr><td colspan='2'>$0.00 check will not be printed. This payment will be updated to P -  Printed.</td></tr>");
                                        Response.Write("<tr><td><input type='hidden' id='hdnZeroChecks' value='2' /></td></tr>");
                                        Response.Write("<tr><td>&nbsp;</td></tr>");
                                    }
                                    else
                                    {
                                        if (Model.SelectSingleNode("//ZeroChecksDueToDeductible") != null && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["CltNumbers"] != null)
                                        {
                                            string scltNumbers = Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["CltNumbers"].Value;
                                            Response.Write(string.Format(Riskmaster.RMXResourceManager.RMXResourceProvider.GetSpecificObject("lblBatchCheckNotPrinted", Riskmaster.RMXResourceManager.RMXResourceProvider.PageId("PrintChecksBatch.aspx"), ((int)Riskmaster.RMXResourceManager.RMXResourceProvider.RessoureType.LABEL).ToString()), scltNumbers));
                                            //Response.Write("<tr><td width='100%'>$0.00 check(s) with control number(s) (" + scltNumbers + ") will not be printed. Please note that the status has been updated to P - Printed.</td></tr>");
                                            Response.Write("<tr><td>&nbsp;</td></tr>");
                                        }
                                        
                                    }
                                 }%>
                    <%--tanwar2 - mits 30910 - end--%>

					<%--<tr  id ="EFTSuccess" runat="server">
						<td colspan="2" >
							EFT File has been successfully created.
						</td>
                        	</tr>	--%>				
					<%if (Model.SelectSingleNode("//PrintChecks").Attributes["PrintDirectlyToPrinter"].Value == "false" && Model.SelectSingleNode("//Jobid") == null)
       { %>
						
							<tr>
                        <%--tanwar2 - mits 30910 - start--%>
						<%-- if(Model.SelectSingleNode("//File").Attributes["FileType"].Value =="eft")--%>
                            <%--Changed by Nikhil on 09/22/14.Code Review changes--%>
                      <%--  <% if (Model.SelectSingleNode("//File") != null && Model.SelectSingleNode("//File").Attributes["FileType"] != null 
                               && Model.SelectSingleNode("//File").Attributes["FileType"].Value == "eft")    --%>
                                 <% if (Model.SelectSingleNode("//File") != null && Model.SelectSingleNode("//File").Attributes["FileType"] != null 
                               && string.Compare(Model.SelectSingleNode("//File").Attributes["FileType"].Value,"eft",true) == 0)   
                        //tanwar2 - mits 30910 - end
                            {
                            %>
                            <asp:Label ID="lbEFT" Text="The EFT File has been successfully created." runat="server" tyle="font-weight: bold;
                        color: blue"/>
                            <%
                            }
                            if (Model.SelectSingleNode("//ExportToFile").InnerText == "0" || Model.SelectSingleNode("//ExportToFile").InnerText == "2" || Model.SelectSingleNode("//ExportToFile").InnerText == "4")
        {
                               //tanwar2 - mits 30910 - start
                              //Changed by Nikhil on 09/22/14.Code Review changes
                                //if (Model.SelectSingleNode("//ZeroChecksDueToDeductible") != null && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"] != null
                                //    && Model.SelectSingleNode("//ZeroChecksDueToDeductible").InnerText == "-1" && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"].Value == "-1")
                                      if (Model.SelectSingleNode("//ZeroChecksDueToDeductible") != null && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"] != null
                                    && string.Compare(Model.SelectSingleNode("//ZeroChecksDueToDeductible").InnerText.ToString(), "-1") == 0 && string.Compare(Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"].Value.ToString(), "-1" )== 0)

                                {
                                    Response.Write("<tr><td><input type='hidden' id='hdnZeroChecks' value='3' /></td></tr>");
                                 }
                                else
                                {
                                %>
                                <%--tanwar2 - mits 30910 - end--%>
								<td>
								The Following Check and EOB reports are attached with this check 
								Batch:
								</td>
							</tr>
						<%
                            foreach (XmlNode node in Model.SelectNodes("//File"))
         {
           
             int iCount = 0;%>
						<tr>
									<td>
										<div style="position:relative;width:420px;height:70px;overflow:auto" class="divScroll">
											<table>
												<ul>
													
														<tr>
															<td width="5%">
															</td>
															<td>
																<li>
																<a  id='<%=node.Attributes["Name"].Value %>' href="javascript:PrintSelectedReport('<%=node.Attributes["FileType"].Value %>','<%=node.Attributes["FileName"].Value %>')" >
																<% if (node.Attributes["Name"].Value == "holdPayments")
                                                                    {%>  Hold Payments<%} %>
                                                                    <%else if (node.Attributes["Name"].Value != "AddlDetail" && node.Attributes["Name"].Value != "PrintCheck")
                                                                    {%> EOB Report For Claim Number : <%=node.Attributes["Name"].Value %><%} %>
                                                                <%else if (node.Attributes["Name"].Value == "AddlDetail") {%>  Additional Details for Check Batch<%} %>
                                                                <%else if (node.Attributes["Name"].Value == "PrintCheck") {%> <%#CheckSingleOrCheckBatch()%><%} %>
                                                                </a>
																</li>				
															</td>
															
														</tr>
													
												</ul>
											</table>
										</div>
									</td>
								</tr>
							<%iCount++;%>
<%         } %>
<%        } 
}
%>

        <%else
                        {%>
            <%if ((Model.SelectSingleNode("//ExportToFile").InnerText == "1") ||(Model.SelectSingleNode("//ExportToFile").InnerText == "3"))
              {
                  //tanwar2 - mits 30910 - start
                  //Changed by Nikhil on 09/22/14.Code Review changes
                  //if (Model.SelectSingleNode("//ZeroChecksDueToDeductible") != null && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"] != null
                  //    && Model.SelectSingleNode("//ZeroChecksDueToDeductible").InnerText == "-1" && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"].Value == "-1")
                if (Model.SelectSingleNode("//ZeroChecksDueToDeductible") != null && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"] != null
                      && string.Compare(Model.SelectSingleNode("//ZeroChecksDueToDeductible").InnerText.ToString(),"-1") == 0  && string.Compare(Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"].Value.ToString() , "-1") == 0)
                  {
                      Response.Write("<tr><td><input type='hidden' id='hdnZeroChecks' value='3' /></td></tr>");
                  }
                  else
                  {
                                %>
                                <%--tanwar2 - mits 30910 - end--%>
                  
            <tr>
								<td >
									<br></br>
									<%#CheckSingleOrCheckBatch()%>
									printed to the file successfully.
									<br></br><br></br>
								</td>
							</tr>
             <%         }
              } %>
        
        <%         } %>
			<%         } %>
			<%else
                        { 
           
            //tanwar2 - mits 30910 - start
             //Changed by Nikhil on 09/22/14.Code Review changes
                            //if (Model.SelectSingleNode("//ZeroChecksDueToDeductible") != null && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"] != null
                            //    && Model.SelectSingleNode("//ZeroChecksDueToDeductible").InnerText == "-1" && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"].Value == "-1")
             if (Model.SelectSingleNode("//ZeroChecksDueToDeductible") != null && Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"] != null
                                && string.Compare(Model.SelectSingleNode("//ZeroChecksDueToDeductible").InnerText.ToString() , "-1") == 0  && string.Compare(Model.SelectSingleNode("//ZeroChecksDueToDeductible").Attributes["NoChecksPrinted"].Value, "-1") == 0)
                            {
                                Response.Write("<tr><td><input type='hidden' id='hdnZeroChecks' value='3' /></td></tr>");
                            }
                            else
                            {
                                %>
                                <%--tanwar2 - mits 30910 - end--%>
           	
				<tr>
					
						<td>
                            <% if (Model.SelectSingleNode("//Jobid") != null)
                            {
                            %>
                            <script type="text/javascript">
                                <!--
                                sJobid = '<%=Model.SelectSingleNode("//Jobid").InnerText %>';
                                sDsn = '<%=AppHelper.ReadCookieValue("DsnName") %>';
                                //-->
                            </script>
                            <br />
						     	&nbsp;&nbsp;&nbsp;
						     	<a id="aa" href="#" onclick="PrinttCheckLocal();">Print using Local Printer</a>
                             <br />
                            <%
                            }
                            else
                            {%>
								Checks and EOB reports are printed sucessfully.
                            <% } %>	
						</td>
						</tr>
			<%        }
                        } %>	
			
			<tr>
						<td >
						<%--tanwar2 - mits 30910 - start--%>
							<%--<asp:Button ID="btnOk" runat="server"  OnClientClick="return PrintBatchOk();"
             Text="   Ok   "  CssClass=button  onclick="btnOk_Click" UseSubmitBehavior="true"
              />--%> 
                      <input type="button" class="button" id="btnOk" onclick="return PrintBatchOk();" value="   OK   "/>
                      <%--tanwar2 - mits 30910 - end--%>
						</td>
                        <td width="80%"><input type="button" class="button" id="btnCancel" onclick="return PrintBatchCancel();" value="Cancel" style="visibility:hidden"/></td>
					</tr>
					<tr>
					<%         } %>	
			<%        Model = null; %>	
					<td>
					   <asp:HiddenField ID="hdnIsBatchPrint" runat="server" />
              <asp:HiddenField ID="hdnCheckId" runat="server" />
           <asp:HiddenField ID="hdnBatchId" runat="server" />
           <asp:HiddenField ID="hdnAccountId" runat="server" />
            <asp:HiddenField ID="hdnCheckDate" runat="server" />
        <asp:HiddenField ID="hdnIncludeAutoPayment" runat="server" />   
        <asp:HiddenField ID="hdnCheckStockId" runat="server" />   
        <asp:HiddenField ID="hdnFirstCheckNum" runat="server" />   
         <asp:HiddenField ID="hdnOrderBy" runat="server" />
          <asp:HiddenField ID="hdnNumAutoChecksToPrint" runat="server" />   
        <asp:HiddenField ID="hdnUsingSelection" runat="server" />   
        <asp:HiddenField ID="hdnType" runat="server" />   
        <asp:HiddenField ID="hdnIncludeClaimantInPayee" runat="server" />   
         <asp:HiddenField ID="hdnFirstTime" runat="server"  Value="0"/>
         <asp:HiddenField ID="hdnFileNameAndType" runat="server"  Value="0"/>
            <asp:HiddenField ID="hdnIncludeCombPay" runat="server" />
        <%--npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
            So introducing Distribution Type and removing out EFTPayment below--%>
            <%--//JIRA:438 START: ajohari2--%>  
            <%--<asp:HiddenField ID="hdnPrintEFTPayment" runat="server" />  --%>
            <%--//JIRA:438 End: --%>
        <asp:HiddenField ID="hdnDistributionType" runat="server" />  
        <%--npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. 
        So introducing Distribution Type and removing out EFTPayment below--%>
					</td>
					</tr>
					<tr>
					<td>
			<iframe frameborder="0" width="0" height="0" src="" id="file"></iframe>
					
					 <uc2:PleaseWaitDialog ID="PleaseWaitDialog1" runat="server"   CustomMessage="Printing" />
					 </td>
					</tr>
				</table>
				<%}
       catch (Exception ee)
       {
           ErrorHelper.logErrors(ee);
           lblErrors.Text = ErrorHelper.FormatErrorsForUI("Error in printing checks");
       }%>	
        <asp:Label ID="lblErrors" runat=server></asp:Label>		
    </form>
</body>
</html>
