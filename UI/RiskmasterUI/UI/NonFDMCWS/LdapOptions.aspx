﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LdapOptions.aspx.cs" Inherits="Riskmaster.UI.NonFDMCWS.LdapOptions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Ldap/Active directory Options</title>
    <style type="text/css">
        .style1
        {
        }
        .style2
        {
            width: 340px;
        }
        .style3
        {
            width: 51px;
        }
        .style4
        {
            height: 38px;
        }
        .style5
        {
            height: 30px;
        }
        .style6
        {
            width: 51px;
            height: 30px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>        
        <cc1:TabContainer ID="LdapADOptions" runat="server" ActiveTabIndex="1" 
            Height="577px" Width="780px" >
            <cc1:TabPanel runat="server" HeaderText="Ldap options" ID="LdapServerOptions"  >            
                <ContentTemplate>
                    <asp:CheckBox ID="chkLdapStatus" runat="server" Text="LDAP Status" />
                    <br />
                    Please enter the LDAP host you are using:<br />
                    <br />
                    Add LDAP host (host name: port) :
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    &nbsp;
                    <asp:Button ID="btnAddLdapHost" runat="server" Text="Add" />
                    <br />                    
                    <br />
                    <asp:TextBox ID="txtLdapHostList" runat="server" Height="67px" Width="331px"></asp:TextBox>
                    <asp:Button ID="btnEdit" runat="server" Text="Edit" />
                    <asp:Button ID="Button2" runat="server" style="width: 56px" Text="Delete" />
                    <br />
                    <br />
                    <br />
                    Choose the type of the LDAP directory you are using:
                    <br />
                    LDAP Server Type:&nbsp;
                    <asp:DropDownList ID="ddLdapServerType" runat="server" Height="16px" 
                        Width="169px">
                        <asp:ListItem Selected="True" Value="0">Lotus Domino server
                        </asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <br />
                    Please choose the type of Secure Sockets Layer (SSL) authentication used by the 
                    LDAP host:
                    <br />
                    Type of SSL Authentication:&nbsp;
                    <asp:DropDownList ID="DropDownList1" runat="server" Height="16px" Width="157px">
                        <asp:ListItem Value="0">Basic (No SSL)</asp:ListItem>
                        <asp:ListItem Value="1">Secured (SSL)</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <br />
                    Please enter the base LDAP distinguished name you would like to use:
                    <br />
                    Base LDAP distinguished name:<asp:TextBox ID="txtLdapName" runat="server" 
                        Height="21px" Width="196px"></asp:TextBox>
                    <br />
                    <br />
                    LDAP Server Attribute Mappings:
                    <br />
                    User Name:<asp:TextBox ID="TextBox2" runat="server" Height="22px" Width="246px"></asp:TextBox>
                    <br />
                    <br />
                    Please enter the credentials required by the LDAP host:
                    <br />
                    LDAP Server Admin credentials:
                    <br />
                    Distinguished Name:<asp:TextBox ID="txtLdapServerUserName" runat="server"></asp:TextBox>
                    <br />
                    Password:<asp:TextBox ID="txtLdapServerPassword" runat="server"></asp:TextBox>                
                </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel runat="server" HeaderText="Active Directory Options" ID="ActiveDirectoryOptions">
                <ContentTemplate>
                    <table style="width:100%;border:0;">
                        <tr>
                            <td class="style2">
                                <asp:CheckBox ID="chkADStatus" runat="server" Text="Active Directory Enabled" />
                            </td>
                            <td class="style3">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style2">
                                &nbsp;</td>
                            <td class="style3">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1" colspan="2">
                                Please enter the Windows Domains you are using:</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1" colspan="2">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Add Windows Domain :
                                <asp:TextBox ID="txtWinDomain" runat="server" ></asp:TextBox>
                                &nbsp;</td>
                            <td class="style3">
                                <asp:Button ID="AddWinDomain" runat="server" Text="Add" />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1" rowspan="2">
                                <asp:TextBox ID="txtWinDomainList" runat="server" Height="67px" Width="331px"></asp:TextBox>
                            </td>
                            <td class="style3">
                                <asp:Button ID="EditWinDomain" runat="server" Text="Edit" />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style3">
                                <asp:Button ID="DeleteWinDomain" runat="server" style="width: 56px" 
                                    Text="Delete" />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1">
                                &nbsp;</td>
                            <td class="style3">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1">
                                &nbsp;</td>
                            <td class="style3">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style4" colspan="2">
                                Please enter the credentials required by the Windows domain(s):
                            </td>
                            <td class="style4">
                            </td>
                            <td class="style4">
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                &nbsp;</td>
                            <td class="style3">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1" colspan="4">
                                Please choose the type of Secure Sockets Layer (SSL) authentication used by the 
                                Windows domain:
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                &nbsp;</td>
                            <td class="style3">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style5">
                                Type of SSL Authentication:&nbsp;</td>
                            <td class="style6">
                                <asp:DropDownList ID="ddWinDomain" runat="server" Height="20px" Width="157px">
                                    <asp:ListItem Value="0">Basic (No SSL)</asp:ListItem>
                                    <asp:ListItem Value="1">Secured (SSL)</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="style5">
                                </td>
                            <td class="style5">
                                </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                &nbsp;</td>
                            <td class="style3">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Domain Admin credentials:
                            </td>
                            <td class="style3">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1">
                                User ID:</td>
                            <td class="style3">
                                <asp:TextBox ID="txtADUserId" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Password:</td>
                            <td class="style3">
                                <asp:TextBox ID="txtADPassword" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                    <br />                  
                    &nbsp;<br /><br />
                    <br /> 
                        
                </ContentTemplate>
            </cc1:TabPanel>
        </cc1:TabContainer>
        
        <asp:Button ID="btnOk" runat="server" Text="OK" />
    </div>
    </form>
</body>
</html>
