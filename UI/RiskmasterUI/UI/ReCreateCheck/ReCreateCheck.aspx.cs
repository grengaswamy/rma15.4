﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.AppHelpers;
using System.Collections;
using System.IO;
using System.Data;

namespace Riskmaster.UI.UI.ReCreateCheck
{
    public partial class ReCreateCheck : NonFDMBasePageCWS
    
    {
          XElement m_objMessageElement = null;
        private string sCWSresponse = "";
        private string sFunctionToCall = string.Empty;
            
        protected void Page_Load(object sender, EventArgs e)
        {
            XElement objTempElement = null;

            string sCulture = AppHelper.GetCulture().ToString();
            if (sCulture != "en-US")
            {
                //Register Date Script
                AppHelper.CalenderClientScript(sCulture, this);
                //Register Time Script
                AppHelper.TimeClientScript(sCulture, this);

               
            }

            

            sFunctionToCall = this.functiontocall.Value;      
            bool bReturnStatus = false;
            try
            {
                m_objMessageElement = GetMessageTemplate();

             
                if(sFunctionToCall != "")
                {
                 
                    bReturnStatus = CallCWS(sFunctionToCall, m_objMessageElement, out sCWSresponse, false, true);
                    ////if (sFunctionToCall == "PrintChecksAdaptor.PostCheckDetail" || sFunctionToCall == "PrintChecksAdaptor.PostCheckSummary" || sFunctionToCall == "PrintChecksAdaptor.PostCheckSubAccount")
                    ////{
                    ////    objTempElement = XElement.Parse(sCWSresponse);
                    ////    objTempElement = objTempElement.XPathSelectElement("./Document/PrintChecks/File");
                      
                    ////    byte[] pdfbytes = Convert.FromBase64String(objTempElement.Value);

                    ////    Response.Buffer = true;
                    ////    Response.Clear();
                    ////    Response.ClearContent();
                    ////    Response.ClearHeaders();
                    ////    Response.Charset = "";
                    ////    Response.AppendHeader("Content-Encoding", "none;");
                    ////    Response.ContentType = "application/pdf";
                    ////    Response.AddHeader("Content-Disposition", string.Format("attachment; filename=PrintCheck.pdf"));
                    ////    Response.AddHeader("Accept-Ranges", "bytes");
                    ////    Response.BinaryWrite(pdfbytes);
                    ////    Response.Flush();
                    ////    Response.Close();
                    ////}
                } 

                if (!IsPostBack)
                {
                    BindpageControls(sCWSresponse);
                    objTempElement = XElement.Parse(sCWSresponse);
                    objTempElement = objTempElement.XPathSelectElement("//IsPrinterSelected");
                    txtIsPrinterSelected.Text = objTempElement.Value;

                    checkdate.Text = AppHelper.GetDate(DateTime.Now.ToShortDateString());

                } // if
                else
                {
                    if (sFunctionToCall == "PrintChecksAdaptor.AccountChanged")
                    {
                        BindpageControls(sCWSresponse);

                        checkdate.Text = AppHelper.GetDate(DateTime.Now.ToShortDateString());
                    }
                }
            } // try
            catch (Exception objException)
            {
                ErrorHelper.logErrors(objException);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(objException, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

        }

        private XElement GetMessageTemplate()
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Authorization></Authorization>");
            sXml = sXml.Append("<Call><Function></Function></Call><Document>");
         
            sXml = sXml.Append("<PrintChecks>");

            if (sFunctionToCall == "PrintChecksAdaptor.AccountChanged")
            {
                sXml = sXml.Append("<FromDateFlag>");
                //as    sXml = sXml.Append(this.fromdateflag.Checked.ToString());
                sXml = sXml.Append("</FromDateFlag>");
                sXml = sXml.Append("<ToDateFlag>");
                //as      sXml = sXml.Append(this.todateflag.Checked.ToString());
                sXml = sXml.Append("</ToDateFlag>");
                sXml = sXml.Append("<AttachedClaimOnly>");
                sXml = sXml.Append("</AttachedClaimOnly>");
                sXml = sXml.Append("<IncludeAutoPayment>");
                //as    sXml = sXml.Append(this.includeautopayments.Checked.ToString());
                sXml = sXml.Append("</IncludeAutoPayment>");
                sXml = sXml.Append("<FromDate>");
                sXml = sXml.Append(this.fromdate.Text);
                sXml = sXml.Append("</FromDate>");
                sXml = sXml.Append("<ToDate>");
                sXml = sXml.Append(this.todate.Text);
                sXml = sXml.Append("</ToDate>");
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountprint.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                //Start Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010
                sXml = sXml.Append("<OrgHierarchy>");
                //sXml = sXml.Append(this.orghierarchy.Text);
                sXml = sXml.Append("</OrgHierarchy>");
                sXml = sXml.Append("<OrgHierarchyLevel>");
                //as    sXml = sXml.Append(this.orghierarchylevelpre.SelectedValue.ToString());
                sXml = sXml.Append("</OrgHierarchyLevel>");

                //Added by Ashutosh to fetch those checks which are already printed , it will Re-print /Re-Create the chek file.
                sXml = sXml.Append("<IsReCreate>true</IsReCreate>");
                //End Debabrata Biswas Batch Printing filter r6 retofit MITS 19715/20050 Date: 03/17/2010

                // npadhy - JIRA RMA-11632 Starts. Pass Distribution Type for Recreate Checks as well.
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePrint.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                sXml = sXml.Append("<GenerateBatchNo>");
                    sXml = sXml.Append("false");
                sXml = sXml.Append("</GenerateBatchNo>");
                // npadhy - JIRA RMA-11632 Ends. Pass Distribution Type for Recreate Checks as well.
                // So introducing Distribution Type and removing out EFTPayment
            } // if



            if (sFunctionToCall == "PrintChecksAdaptor.CheckBatchChangedPre")
            {
                sXml = sXml.Append("<AccountId>");
                sXml = sXml.Append(this.bankaccountprint.SelectedValue.ToString());
                sXml = sXml.Append("</AccountId>");
                sXml = sXml.Append("<BatchNumber>");
                sXml = sXml.Append(this.checkbatchpre.Text);

                sXml = sXml.Append("</BatchNumber>");
                //Added by Ashutosh to fetch those checks which are already printed , it will Re-print /Re-Create the chek file.
                sXml = sXml.Append("<IsReCreate>true</IsReCreate>");
                // npadhy - JIRA RMA-11632 Starts. Pass Distribution Type for Recreate Checks as well.
                sXml = sXml.Append("<DistributionType>");
                sXml = sXml.Append(this.ddlDistributionTypePrint.SelectedValue.ToString());
                sXml = sXml.Append("</DistributionType>");
                // npadhy - JIRA RMA-11632 Ends. Pass Distribution Type for Recreate Checks as well.
            } // if

            // npadhy RMA 11632 - On Page load, the system was not behaving for Recreate. Rather it was retriving the Checks which were printed, rather transactions which need to be printed
            if (sFunctionToCall == "PrintChecksAdaptor.OnLoad")
            {
                sXml = sXml.Append("<IsReCreate>true</IsReCreate>");
            }
            sXml = sXml.Append("</PrintChecks></Document></Message>");
            XElement oTemplate = XElement.Parse(sXml.ToString());

            return oTemplate;
        }

         
         

        private void BindpageControls(string sreturnValue)
        {
            DataSet usersRecordsSet = null;
            XmlDocument usersXDoc = new XmlDocument();
            usersXDoc.LoadXml(sreturnValue);
           
            usersRecordsSet = ConvertXmlDocToDataSet(usersXDoc);
            DropDownList lstBankAccountPrint = (DropDownList)this.Form.FindControl("bankaccountprint");
            if (usersRecordsSet.Tables["Account"] != null)
            {
                if (usersRecordsSet.Tables["Account"] != null)
                {
                    lstBankAccountPrint.DataSource = usersRecordsSet.Tables["Account"].DefaultView;
                    lstBankAccountPrint.DataTextField = "AccountName";
                    lstBankAccountPrint.DataValueField = "AccountID";
                    lstBankAccountPrint.DataBind();
                }
            }

            // npadhy JIRA 11632 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            // Bind Distribution Type as well 

            if (usersRecordsSet.Tables["DistributionType"] != null)
            {
                ddlDistributionTypePrint.DataSource = usersRecordsSet.Tables["DistributionType"].DefaultView;
                ddlDistributionTypePrint.DataTextField = "DistributionTypeDesc";
                ddlDistributionTypePrint.DataValueField = "DistributionTypeId";
                ddlDistributionTypePrint.DataBind();
            }
            // npadhy JIRA 11632 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

            // npadhy JIRA 11632 During load also the appropriate Batch Number should come.
            //if (sFunctionToCall == "PrintChecksAdaptor.OnLoad")
            //{
            //    checkbatchpre.Text = "";
            //}

           
            if (usersRecordsSet != null)
            {
                usersRecordsSet.Dispose();
            }
        }
    }    

}
