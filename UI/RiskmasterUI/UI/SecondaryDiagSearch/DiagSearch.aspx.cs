﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Riskmaster.RMXResourceManager; //MITS 34254 - hlv

namespace Riskmaster.SecondaryDiagSearch
{
    public partial class DiagSearch : System.Web.UI.Page
    {

        private string pageID = RMXResourceProvider.PageId("DiagSearch.aspx"); //MITS 34254 - hlv

        //gagnihotri MITS 19359 01/13/2010
        protected void Page_PreInit(object sender, EventArgs e)
        {
            string sTableName = string.Empty;
            // pen  testing changes :atavaragiri MITS 27870
            if(!String.IsNullOrEmpty(Request.QueryString.Get("TableName")))
            //sTableName = Request.QueryString.Get("TableName").ToString();
            sTableName =AppHelper.HTMLCustomEncode( Request.QueryString.Get("TableName").ToString());
            // END:pen testing atavaragiri 
            if(sTableName != "")
                DIAGNOSIS_CODE.CodeTable = sTableName;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //gagnihotri MITS 19359 01/13/2010
        protected void Page_PreRender(object sender, EventArgs e)
        {
            switch (DIAGNOSIS_CODE.CodeTable)
            {
                case ("CDC_DIAGNOSIS_CODE"):
                    {
                        /*
                        Page.Title = "CDC ICD-9 Diagnosis Codes Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = "CDC ICD-9 Diagnosis Codes";
                        lblName.Text = "Diagnosis:";
                        */

                        //MITS 34254 hlv begin
                        Page.Title = "CDC ICD-9 Diagnosis Codes Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = AppHelper.GetResourceValue(this.pageID, "lblSubTitleA", "0");
                        lblName.Text = AppHelper.GetResourceValue(this.pageID, "lblDiagnosis", "0");
                        //MITS 34254 hlv end
                        break;
                    }
                //rsushilaggar MITS 25692 date 08/31/2011
                case ("CDC_EVENT_CODE"):
                    {
                        /*
                        Page.Title = "CDC ICD-9 Event Codes Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = "CDC ICD-9 Event Codes";
                        lblName.Text = "Event:";
                        */

                        //MITS 34254 hlv begin
                        Page.Title = "CDC ICD-9 Event Codes Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = AppHelper.GetResourceValue(this.pageID, "lblSubTitleB", "0");
                        lblName.Text = AppHelper.GetResourceValue(this.pageID, "lblNameB", "0");
                        //MITS 34254 hlv end
                        break;
                    }
                //prashbiharis ICD10 Changes MITS: 32423
                case ("CDC_ICD10_EVENT_CODE"):
                    {
                        //Page.Title = "CDC ICD-10 Event Codes Search";
                        //Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        //lblSubTitle.Text = "CDC ICD-10 Event Codes";
                        //lblName.Text = "Event:";
                        //MITS 34254 hlv begin
                        Page.Title = "CDC ICD-10 Event Codes Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = AppHelper.GetResourceValue(this.pageID, "lblCDCICD10SubTitleB", "0");//JIRA RMA 1013 ajohari2
                        lblName.Text = AppHelper.GetResourceValue(this.pageID, "lblNameB", "0");
                        //MITS 34254 hlv end
                        break;
                    }
                case ("CDC_ICD10_DIAGNOSIS_CODE"):
                    {
                        //MITS 34254 sanoopsharma start
                        //Page.Title = "CDC ICD-10 Diagnosis Codes Search";
                        //Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        //lblSubTitle.Text = "CDC ICD-10 Diagnosis Codes";
                        //lblName.Text = "Diagnosis:";                        
                        Page.Title = "CDC ICD-10 Diagnosis Codes Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = AppHelper.GetResourceValue(this.pageID, "lblCDCICD10DiagCodes", "0");
                        lblName.Text = AppHelper.GetResourceValue(this.pageID, "lblDiagnosis", "0");
                        //MITS 34254 sanoopsharma end                        
                        break;
                    }
                case ("DIAGNOSIS_CODE"):
                    {
                        //MITS 34254 sanoopsharma start 
                        //Page.Title = "ICD-9 Diagnosis Codes Search";
                        //Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        //lblSubTitle.Text = "ICD-9 Diagnosis Codes";
                        //lblName.Text = "Diagnosis:";
                        Page.Title = "ICD-9 Diagnosis Codes Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = AppHelper.GetResourceValue(this.pageID, "lblICD9DiagnosisCodes", "0");
                        lblName.Text = AppHelper.GetResourceValue(this.pageID, "lblDiagnosis", "0");
                        //MITS 34254 sanoopsharma end 
                        break;
                    }
                case ("Diagnosis_Code_ICD10"):
                    {
                        //MITS 34254 sanoopsharma start 
                        //Page.Title = "ICD-10 Diagnosis Codes Search";
                        //Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        //lblSubTitle.Text = "ICD-10 Diagnosis Codes";
                        //lblName.Text = "Diagnosis:";
                        Page.Title = "ICD-10 Diagnosis Codes Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = AppHelper.GetResourceValue(this.pageID, "lblICD10DiagnosisCodes", "0");
                        lblName.Text = AppHelper.GetResourceValue(this.pageID, "lblDiagnosis", "0");
                        //MITS 34254 sanoopsharma end
                        break;
                    }
                //prashbiharis ICD10 Changes MITS: 32423
                default:
                    {
                        /*
                        Page.Title = "Secondary Diagnosis Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = "Secondary Diagnosis Code";
                        lblName.Text = "Diagnosis:";
                        */

                        //MITS 34254 hlv begin
                        Page.Title = "Secondary Diagnosis Search";
                        Label lblSubTitle = (Label)Page.FindControl("lblSubTitle");
                        lblSubTitle.Text = AppHelper.GetResourceValue(this.pageID, "lblSubTitleC", "0");
                        lblName.Text = AppHelper.GetResourceValue(this.pageID, "lblDiagnosis", "0");
                        //MITS 34254 hlv end
                        break;
                    }
            }
        }
    }
}
