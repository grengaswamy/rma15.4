﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Security.Cryptography.X509Certificates;
using Riskmaster.AppHelpers;
using System.IO;
using System.Xml.XPath;
using System.Xml;
using ComponentSoft.Saml2;
using ComponentSoft.Licensing.Saml;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml.Linq;
using Riskmaster.Security.Authentication;

namespace Riskmaster.UI.UI.Strataware
{

    public partial class StrataWare : NonFDMBasePageCWS
    {

        private string ConsumerServiceUrl = String.Empty;
        private string ServiceProviderUrl = String.Empty;



        // Get the previously loaded certificate
        const string PrivateKeyPassword = "riskmaster";

        private const string CertKeyName = "certificate.P7B";
        private const string PrivateKeyName = "Key.pfx";
        //mbahl3 mits 34646
        private bool bEmailIdFound = false;
		  //mbahl3 mits 34646
        protected override void OnLoad(EventArgs e)
        {
            //Response.Redirect("http://localhost/RiskmasterUI/UI/Strataware/StrataWare.aspx",true);
            //  Server.Transfer("../Strataware/StrataWare.aspx");
            base.OnLoad(e);
            Issuer issuer = null;
            SamlAssertionInfo assertionInfo = null;
            Assertion samlAssertion = null;
            string script = "<script>LoadMDI();</script>";
            X509Certificate2 x509Certificate = null;
            string fileName = string.Empty;
            try
            {  //mbahl3 mits 34646
                // Load the certificate.
                if (bEmailIdFound)   
                {
				  //mbahl3 mits 34646
                    fileName = Path.Combine(HttpRuntime.AppDomainAppPath, PrivateKeyName);
                    x509Certificate = new X509Certificate2(fileName, PrivateKeyPassword, X509KeyStorageFlags.MachineKeySet);

                    //X509Certificate2 x509Certificate = (X509Certificate2)Application[StratewareHelper.CertKeyName];

                    // Create a SAML response object.
                    ComponentSoft.Saml2.Response samlResponse = new ComponentSoft.Saml2.Response();
                    // Assign the consumer service url.
                    samlResponse.Destination = ConsumerServiceUrl;
                    issuer = new Issuer(GetAbsoluteUrl("~/"));
                    samlResponse.Issuer = issuer;
                    samlResponse.Status = new Status(SamlPrimaryStatusCode.Success, null);

                    assertionInfo = new SamlAssertionInfo()
                     {
                         SamlIssuer = issuer,
                         SigningCertificate = x509Certificate,
                         UserName = UserEmail.Value,
                         SubjectRecipient = ConsumerServiceUrl
                     };

                    assertionInfo.SetAudienceUri(ConsumerServiceUrl);
                    assertionInfo.ClaimSetInfo = new Dictionary<string, string>();
                    assertionInfo.ClaimSetInfo.Add("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", UserName.Value);
                    assertionInfo.ClaimSetInfo.Add("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress", UserEmail.Value);

                    samlAssertion = SamlAuthentication.GenerateSamlAssertion(assertionInfo);

                    samlResponse.Assertions.Add(samlAssertion);

                    // Sign the SAML response with the certificate.
                    samlResponse.Sign(x509Certificate);

                    // Send the SAML response to the service provider.
                    samlResponse.SendPostBindingForm(Response.OutputStream, ConsumerServiceUrl, ServiceProviderUrl);

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "myscript", script);
                }   //mbahl3 mits 34646
            }

            catch (Exception ee)
            {
                ErrorControl1.Visible = true;
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }

            finally
            {
                 issuer = null;
                 assertionInfo = null;
                 samlAssertion = null;
                  x509Certificate =null;
            }
        }

        private string GetAbsoluteUrl(string relativeUrl)
        {
            Uri u = new Uri(Request.Url, ResolveUrl(relativeUrl));
            return u.ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {


            string sResponse = string.Empty;
            bool bReturnStatus = true;
           //mbahl3 mits 34646
		    try
            {
			//mbahl3 mits 34646
                CallCWS("StratawareAdaptor.GetInfo", GetMessageTemplate(), out sResponse, false, false);
                XmlDocument objDoc = new XmlDocument();

              
                if (!string.IsNullOrEmpty(sResponse))
                {
                    objDoc.LoadXml(sResponse);
                   //mbahl3 mits 34646
				    if (string.Equals(objDoc.SelectSingleNode("//MsgStatusCd").InnerText, "Success", StringComparison.InvariantCultureIgnoreCase))
                    {
					//mbahl3 mits 34646
                        UserId.Value = objDoc.SelectSingleNode("//UserId").InnerText;
                        UserName.Value = objDoc.SelectSingleNode("//UserName").InnerText;
                        UserEmail.Value = objDoc.SelectSingleNode("//UserEmail").InnerText;
                        ServiceProviderUrl = objDoc.SelectSingleNode("//ServiceURL").InnerText;
                        ConsumerServiceUrl = objDoc.SelectSingleNode("//ConsumerURL").InnerText;
                      //mbahl3 mits 34646
					    bEmailIdFound = true; 
                    }
                    else
                    {
                        bEmailIdFound = false;
						  //mbahl3 mits 34646
                    }
                }
                objDoc = null;
           //mbahl3 mits 34646
		    }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
			//mbahl3 mits 34646
        }


        private XElement GetMessageTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                                                                <Message>
                                                                  <Authorization>acc6ce9f-a22e-49a6-b1bf-33e1f6510901</Authorization> 
                                                                 <Call>
                                                                  <Function>StratawareAdaptor.GetInfo</Function> 
                                                                  </Call>
                                                                    <Document>
                                                                        <Stratware>
                                                                                <UserId/>
                                                                                <UserName/>
                                                                                <ConsumerURL/>
                                                                                <ServiceURL/>
                                                                                <UserEmail/>
                                                                         </Stratware>
                                                                    </Document>
                                                                </Message>");
            return oTemplate;
        }
    }
}

