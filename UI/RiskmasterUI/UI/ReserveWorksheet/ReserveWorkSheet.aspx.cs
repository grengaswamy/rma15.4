﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Riskmaster.BusinessHelpers;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.UI.Shared.Controls;
using Riskmaster.BusinessAdaptor.Common;
using MultiCurrencyCustomControl;
//using Riskmaster.UI.ReserveWorksheetService;//Change by kuladeep for Cloud-154
using Riskmaster.Models;

namespace Riskmaster.UI.UI.ReserveWorksheet
{
    public partial class ReserveWorkSheet : NonFDMBasePageCWS
    {
        //MITS 22202 skhare7
        bool bException = false;
        string sExceptionMsg = null;
        //MITS 22202 skhare7
        string RSWType = "";
        #region Page Events
        protected override void LoadViewState(object savedState)
        {
            try
            {
                base.LoadViewState(savedState);
                if (IsPostBack)
                {
                    if (ViewState["divContainer_controls"] != null)
                    {

                        Control ctrl = this.Form.FindControl("divContainer");
                        if (ctrl != null)
                        {
                            if (ViewState["divContainer_controls"].ToString() != "")
                            {
                                string response = AppHelper.UnZip((string)ViewState["divContainer_controls"]);
                                Control ctrl2 = ParseControl(response);
                                ctrl.Controls.Clear();
                                ctrl.Controls.Add(ctrl2);
                            }
                        }

                    }
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }
        public void Page_init(Object sender, EventArgs e)
        {
            String csurl = "";
            Type cstype = null;
            ClientScriptManager cs = null;
            try
            {

                // Define the name, type and url of the client script on the page.

                MakeOnLoadSettings();
                String csname = "Script";
                if (RswTypeName.Value == "Customize")
                {
                    csurl = "../../Scripts/rsw_customize.js";
                }
                else if (RswTypeName.Value == "Generic" || string.IsNullOrEmpty(RswTypeName.Value)) //tmalhotra2 mits 27272
                {
                    csurl = "../../Scripts/rsw.js";
                }
              
                cstype = this.GetType();

                // Get a ClientScriptManager reference from the Page class.
                cs = Page.ClientScript;

                // Check to see if the include script exists already.
                if (!cs.IsClientScriptIncludeRegistered(cstype, csname))
                {
                    cs.RegisterClientScriptInclude(cstype, csname, ResolveClientUrl(csurl));
                }
            }
          
            catch (Exception ee)
            {
                bException = true;
                sExceptionMsg = ee.Message.ToString();
            }
            //MITS 22202 skhare7
        }

        

        protected void Page_Load(object sender, EventArgs e)
        {
            TextBox hdnRswXmlTypeControl = null;
            try
            {
               
                ErrorControl.errorDom = "";
                hdnApprovedBtnClicked.Value = "false";
                hdnRejectedBtnClicked.Value = "false";                   //RMA:19660

                if (RswTypeName.Value == "Customize")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "SetWipedoutAfterApprove();Rsw_Load();pageLoaded();RefreshRSWScreen();", true);
                    btn_Show.Visible = false;
                }

                else if (RswTypeName.Value == "Generic")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "SetWipedoutAfterApprove();pageLoaded();RefreshRSWScreen();", true);
                    btn_Show.Visible = true; ;

                }
                //MITS 22202 skhare7
                else 
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "script", "parent.MDIRefreshCurrentNode();", true);

                }
         
                if (bException)
                {
                    throw new Exception(sExceptionMsg);

                }
                //MITS 22202 skhare7
                if (!IsPostBack)
                {
                    if (Request.QueryString["RSWId"] != null)
                    {
                        ViewState["RSWId"] = Request.QueryString["RSWId"].ToString();
                        //setting the value to hidden field to be used by MDIrswid.Value = ViewState["RSWId"].ToString();     
                        //MITS:22297
                        rswid.Value = ViewState["RSWId"].ToString();
                        //MITS:22297
                        //MITS:22299 skhare7
                        btn_Show.Enabled = false;
                        //MITS:22299 skhare7
                    }
                    else
                    {
                        throw new Exception("ReserveWorksheet Id cannot be NUll");
                    }

                    SysFormName.Value = "reserveworksheet";
                    SysFormIdName.Value = "rswid";
                    //Making onload settings
                    //MakeOnLoadSettings();
                    
                    HandleRequestFromMDI();
                }
                else
                {
                    if (hdnAproveFromClientSide.Value == "true")
                    {
                        //call aprove worksheet function
                        hdnAproveFromClientSide.Value = "";
                        ApproveWorkSheet();
                    }
                   if (hdnXMLValue.Value == "Yes")
                    {

                        if (Request.QueryString["RSWId"] != null)
                        {
                            ViewState["RSWId"] = Request.QueryString["RSWId"].ToString();
                            //setting the value to hidden field to be used by MDIrswid.Value = ViewState["RSWId"].ToString();                            
                            //MITS:22297
                            rswid.Value = ViewState["RSWId"].ToString();
                            //MITS:22297
                            //MITS:22299 skhare7
                            btn_Show.Enabled = false;
                            //MITS:22299 skhare7
                        }
                        else
                        {
                            throw new Exception("ReserveWorksheet Id cannot be NUll");
                        }

                        SysFormName.Value = "reserveworksheet";
                        SysFormIdName.Value = "rswid";


                        //Making onload settings
                        //MakeOnLoadSettings();

                        HandleRequestFromMDI();
                        hdnXMLValue.Value = "";
                    }
                }

            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ReserveWorksheetRequest objRequest = null;
            ReserveWorksheetResponse objResponse = null;
            XElement formDataElement = null;

            try
            {
                formDataElement = BindTextBoxesToRequestData();

                objRequest = new ReserveWorksheetRequest();
                objRequest.InputXmlString = formDataElement.ToString();
                objRequest.RSWReservesXML = hdnCustomTransXML.Value;
                objResponse = HitReserveWorksheetWebservice(objRequest, "Submit");

                if (objResponse != null)
                {
                    LoadXslControls(objResponse.OutputXmlString);
                    Control ctrl = this.Form.FindControl("divContainer");
                    BindResponseDataToTextboxes(ctrl.Controls, "Pending Approval");
                }
                else
                {
                    throw new NullReferenceException("Null Response Has Obtained");
                }
                //MITS:22299 skhare7
                hdnCustomTransXML.Value = string.Empty;
                btn_Show.Enabled = false;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ReserveWorksheetRequest objRequest = null;
            ReserveWorksheetResponse objResponse = null;
            XElement formDataElement = null;
            Control oControl = null;
            string sRWSId = string.Empty;
            
            try
            {
                formDataElement = BindTextBoxesToRequestData();
                objRequest = new ReserveWorksheetRequest();
                objRequest.RSWReservesXML = hdnCustomTransXML.Value;
                objRequest.InputXmlString = formDataElement.ToString();
                objResponse = HitReserveWorksheetWebservice(objRequest, "SaveData");

                if (objResponse != null)
                {
                    ViewState["FormXmlData"] = objResponse.OutputXmlString;
                    //binding latest form xml to the cotrols at form
                    Control ctrl = this.Form.FindControl("divContainer");
                    BindResponseDataToTextboxes(ctrl.Controls, "Pending");

                    oControl = this.FindControl("hdnRSWid");

                    if (oControl != null)
                    {
                        sRWSId = ((TextBox)oControl).Text;
                        //setting the value to hidden field to be used by MDI
                        rswid.Value = sRWSId;
                        oControl = null;
                    }
                    //MITS:22299 skhare7
                    hdnCustomTransXML.Value = string.Empty;
                    btn_Show.Enabled = false;
                    //MITS:22299 skhare7
                }
                else
                {
                    throw new NullReferenceException("Null Response Has Obtained");
                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

       
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteWorkSheet();
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                hdnApprovedBtnClicked.Value = "true";
                ApproveWorkSheet();
                //MITS:22299 skhare7

                hdnCustomTransXML.Value = string.Empty;
                btn_Show.Enabled = false;
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            ReserveWorksheetRequest objRequest = null;
            ReserveWorksheetResponse objResponse = null;
            XElement formDataElement = null;

            try
            {
                hdnRejectedBtnClicked.Value = "true";                          //RMA:19660
                objRequest = new ReserveWorksheetRequest();
                formDataElement = BindTextBoxesToRequestData();
                objRequest.RSWReservesXML = hdnCustomTransXML.Value;
                objRequest.InputXmlString = formDataElement.ToString();

                objResponse = HitReserveWorksheetWebservice(objRequest, "Reject");
                if (objResponse != null)
                {
                    //MGaba2:R6:Now we need to open rejected worksheet in edit mode
                    //Changed by Nitin
                    //ViewState["FormXmlData"] = objResponse.OutputXmlString;
                    LoadXslControls(objResponse.OutputXmlString);
                    //binding latest form xml to the cotrols at form
                    Control ctrl = this.Form.FindControl("divContainer");
                    BindResponseDataToTextboxes(ctrl.Controls, "Rejected");
                }
                else
                {
                    throw new NullReferenceException("Null Response Has Obtained");
                }
                //MITS:22299 skhare7
                hdnCustomTransXML.Value = string.Empty;
                btn_Show.Enabled = false;
                //MGaba2:r6:Functioning of Reject Button:End
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }

        #endregion

        #region Private Methods

        private void CreateWorkSheet(bool bIsViewExistWorksheet)
        {
            ReserveWorksheetRequest objRequest = null;
            ReserveWorksheetResponse objResponse = null;
            int iSCompare=0;
            objRequest = new ReserveWorksheetRequest();
            objRequest.ClaimId = Convert.ToInt32(ViewState["ClaimId"]);
            hdnLOB.Value = ViewState["ClaimId"].ToString();
            objRequest.ClaimantEId = Convert.ToInt32(ViewState["ClaimantId"]);
            objRequest.UnitId = Convert.ToInt32(ViewState["UnitId"]);

            try
            {
                if (bIsViewExistWorksheet == false)
                {
                    if (hdnApprovedRWSId.Value != "0")
                    {
                        objRequest.FromApproved = hdnApprovedRWSId.Value;
                    }
                    objRequest.RSWId = 0;
                }
                else
                {
                    objRequest.RSWId = Convert.ToInt32(ViewState["RSWId"]);
                }

                objResponse = HitReserveWorksheetWebservice(objRequest, "GetReserveWorkSheet");
                //for Displaying the approved aur history value of differnt worksheet in config

                XElement htmlDataDoc = null;
                htmlDataDoc = XElement.Parse(objResponse.OutputXmlString);
                //tmalhotra2 mits 27272
                string sRSWTypeName = string.Empty;
                if (htmlDataDoc.XPathSelectElement("//control[@name='hdnRSWXmlType']") != null)
                {
                    sRSWTypeName = htmlDataDoc.XPathSelectElement("//control[@name='hdnRSWXmlType']").Value;
                }
                else
                {
                    sRSWTypeName = "Generic";
                }
                string sRSWStatus = htmlDataDoc.XPathSelectElement("//control[@name='hdnRSWStatus']").Value;
                //string sClaimCurrencyType = string.Empty;
                //if (htmlDataDoc.XPathSelectElement("//control[@name='hdn_currencytype']") != null)                    //Aman Multi Currency
                //{ 
                //    sClaimCurrencyType = htmlDataDoc.XPathSelectElement("//control[@name='hdn_currencytype']").Value;
                //}
                //currencytype.Value = sClaimCurrencyType.ToString();
                if (sRSWTypeName != RswTypeName.Value && (sRSWStatus == "Approved" || sRSWStatus == "History"))
                {
                    RswTypeName.Value = sRSWTypeName;
                
                }
                
                //MITS:22299 skhare7

                iSCompare = string.Compare(sRSWStatus, "New");
                if (iSCompare == 0)
                {
                    btn_Show.Enabled = true;
                }
                else
                {
                    btn_Show.Enabled = false;
                }

               // InitializeCulture();
                if (objResponse != null)
                {
                    LoadXslControls(objResponse.OutputXmlString);
                }
                else
                {
                    throw new NullReferenceException("Null Response Has Obtained");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadXslControls(string htmXmlData)
        {
            PowerViewUpgradeBusinessHelper pn = null;
            string convertedString = string.Empty;
            XElement htmlDataDoc = null;
            XElement reserveSummaryElement = null;
            string hdnReserveTypeTabList = string.Empty;
            string[] reserveTabArr;
            string respHTableName = string.Empty;
            string respTabGroup = string.Empty;
            string respFormTabRserverSum = string.Empty;
            string response = string.Empty;
            string namespaces = string.Empty;
            StringBuilder reserveTabs = null;
            StringBuilder convertedAspx = null;
            string respClaimInfo = string.Empty;

            try
            {
                htmlDataDoc = XElement.Parse(htmXmlData);

                if (htmlDataDoc.ToString() != string.Empty)
                {
                    pn = new PowerViewUpgradeBusinessHelper();
                    string converted = pn.UpgradeXmlTagToAspxForm(false, false, htmXmlData, "");
                    
                    convertedAspx = new StringBuilder(converted);
                    if (RswTypeName.Value == "Customize")
                    { 
                        reserveSummaryElement = htmlDataDoc.XPathSelectElement("//group[@name='RESERVESUMMARYWC']");
                    }
                    else if (RswTypeName.Value == "Generic" || string.IsNullOrEmpty(RswTypeName.Value)) //tmalhotra2 mits 27272
                    { 
                        reserveSummaryElement = htmlDataDoc.XPathSelectElement("//group[@name='RESERVESUMMARY']"); 
                    }
                   
                    hdnReserveTypeTabList = reserveSummaryElement.XPathSelectElement("//control[@name='hdnReserveTypeTabs']").Value;

                    reserveTabArr = hdnReserveTypeTabList.Split(',');

                    respHTableName = AppHelper.HTMLElementExtractor(convertedAspx, "hTabName");
                    respTabGroup = AppHelper.HTMLElementExtractor(convertedAspx, "TabsDivGroup");

                    reserveTabs = new StringBuilder();

                    for (int iCnt = 0; iCnt < reserveTabArr.GetUpperBound(0); iCnt++)
                    {
                        reserveTabs.Append(AppHelper.HTMLElementExtractor(convertedAspx, "FORMTAB" + reserveTabArr[iCnt]));
                    }

                    //string respFORMTABMEDICAL = AppHelper.HTMLElementExtractor(convertedAspx, "FORMTABMedicalWC");
                    if (RswTypeName.Value == "Generic")
                    { 
                        respFormTabRserverSum = AppHelper.HTMLElementExtractor(convertedAspx, "FORMTABReserveSummary"); 
                    }
                
                    //Get the common section for claim info
                    if (RswTypeName.Value == "Generic")
                    {  
                        respClaimInfo = AppHelper.HTMLElementExtractor(convertedAspx, "SECTIONClaimInfo"); 
                    }

                    response = respClaimInfo + respHTableName + respTabGroup + reserveTabs.ToString() + respFormTabRserverSum;
                    
                    //Importing the namespaces in order to parse the string
                    namespaces = "<%@ Import Namespace=\"System.Data\" %> <%@ Import Namespace=\"System.Xml\" %> <%@ Register Assembly=\"AjaxControlToolkit\" Namespace=\"AjaxControlToolkit\" TagPrefix=\"cc1\" %>";
                    namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" TagName=\"CodeLookUp\" TagPrefix=\"uc\" %>";
                    namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" TagName=\"UserControlDataGrid\" TagPrefix=\"dg\" %>";
                    namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" TagName=\"MultiCode\" TagPrefix=\"uc\" %>";
                    namespaces = namespaces + "<%@ Register Src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" TagName=\"PleaseWaitDialog\"   TagPrefix=\"uc\" %>";
                    namespaces = namespaces + "<%@ Register Assembly=\"MultiCurrencyCustomControl\" Namespace=\"MultiCurrencyCustomControl\"  TagPrefix=\"mc\" %>";
                    response = namespaces + response;
                    Control ctrl = this.Form.FindControl("divContainer");
                    try
                    {
                        Control ctrl2 = ParseControl(response);
                        ctrl.Controls.Clear();
                        ctrl.Controls.Add(ctrl2);

                        //Control objControl =  Page.ParseControl(response);
                        //Page.Controls.Add(objControl);

                        ViewState["divContainer_controls"] = AppHelper.Zip(response);
                        ViewState["FormXmlData"] = htmXmlData;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        private XElement GetReserveWorkSheetTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
            <ReserveWorksheet>
                <RSWId></RSWId>
                <ClaimId></ClaimId>
                <ExistApproved/>

                <ExistPending/>
                <ExistPendingApproval/>
                <ExistRejected/>
                <EditRejected />
                <FromApproved />
                <SMSCheckToUpdate/>
                <IsClaimAllowedToAccess/>                
                <Mode></Mode>
                <Status></Status>
            </ReserveWorksheet>
            ");

            return oTemplate;
        }

        private ReserveWorksheetResponse HitReserveWorksheetWebservice(ReserveWorksheetRequest oReserveWorkSheetRequest, string methodName)
        {
            ReserveWorksheetResponse objReserveWorkSheetResponse = null;
            //ReserveWorksheetServiceClient objClient = null;//Change by kuladeep for Cloud-154
            oReserveWorkSheetRequest.RSWType = RswTypeName.Value;
            oReserveWorkSheetRequest.RSWReservesXML = hdnCustomTransXML.Value;
            try
            {
                oReserveWorkSheetRequest.Token = AppHelper.ReadCookieValue("SessionId");
                oReserveWorkSheetRequest.ClientId = AppHelper.ClientId;//Change by kuladeep for Cloud-154

                //objClient = new ReserveWorksheetServiceClient();//Change by kuladeep for Cloud-154

                if (methodName == "GetReserveWorkSheet")
                {
                    //objReserveWorkSheetResponse = objClient.GetReserveWorkSheet(objReserveWorkSheetRequest);//Change by kuladeep for Cloud-154
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/GetReserveWorkSheet", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                }
                else if (methodName == "SaveData")
                {
                    //objReserveWorkSheetResponse = objClient.SaveReserveWorkSheet(objReserveWorkSheetRequest);//Change by kuladeep for Cloud-154
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/SaveReserveWorkSheet", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                }
                else if (methodName == "Submit")
                {
                    //objReserveWorkSheetResponse = objClient.SubmitReserveWorkSheet(objReserveWorkSheetRequest);//Change by kuladeep for Cloud-154
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/SubmitReserveWorkSheet", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                }
                else if (methodName == "OnLoadInformation")
                {
                    //objReserveWorkSheetResponse = objClient.OnLoadInformation(objReserveWorkSheetRequest);//Change by kuladeep for Cloud-154
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/OnLoadInformation", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                }
                else if (methodName == "Delete")
                {
                    //objReserveWorkSheetResponse = objClient.DeleteReserveWorkSheet(objReserveWorkSheetRequest);//Change by kuladeep for Cloud-154
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/DeleteReserveWorkSheet", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                }
                else if (methodName == "Approve")
                {
                    //objReserveWorkSheetResponse = objClient.ApproveReserveWorkSheet(objReserveWorkSheetRequest);//Change by kuladeep for Cloud-154
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/ApproveReserveWorkSheet", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                }
                else if (methodName == "Reject")
                {
                    //objReserveWorkSheetResponse = objClient.RejectReserveWorkSheet(objReserveWorkSheetRequest);//Change by kuladeep for Cloud-154
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/RejectReserveWorkSheet", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                }

                //this value would be -1 in case error occured in webservice
                SysIsServiceError.Value = "1";

                return objReserveWorkSheetResponse;
            }
            catch (Exception ex)
            {
                SysIsServiceError.Value = "-1";
                throw ex;
            }
            finally
            {
                //objClient.Close();//Change by kuladeep for Cloud-154
                //objClient = null;
            }
        }

        private void BindResponseDataToTextboxes(ControlCollection oControls, string p_Status)
        {
            string sRMXRef = string.Empty;
            string sformData = string.Empty;
            XElement formDataElement = null;
            XElement xmlConrolElement = null;
            CodeLookUp codeLookupControl = null;
            TextBox rmxTextboxControl = null;
            //Control tempContrl = null;

            formDataElement = GetStoredFormData();

            foreach (Control oCtrl in oControls)
            {
                if (oCtrl.Controls.Count == 0)
                {
                    if (oCtrl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
                    {
                        rmxTextboxControl = (TextBox)oCtrl;

                        //Added in order to make the textbox as Right aligned
                        if (rmxTextboxControl.Attributes["rmxtype"] != null)
                        {
                            if (rmxTextboxControl.Attributes["rmxtype"] == "currency")
                            {
                                if (rmxTextboxControl.Attributes["TEXT-ALIGN"] == null)
                                {
                                    rmxTextboxControl.Attributes.CssStyle.Add("TEXT-ALIGN", "right");
                                }
                            }
                        }
                         
                        if (rmxTextboxControl.Attributes["RMXRef"] != null)
                        {
                            sRMXRef = rmxTextboxControl.Attributes["rmxref"];

                            //  sRMXRef = rmxTextboxControl.Attributes["rmxref"];
                            if (sRMXRef != string.Empty)
                            {
                                //fetch values from ref
                                if (formDataElement != null)
                                {
                                    xmlConrolElement = formDataElement.XPathSelectElement(sRMXRef);
                                    if (xmlConrolElement != null)
                                    {

                                        rmxTextboxControl.Text = xmlConrolElement.Value;
                                        xmlConrolElement = null;
                                        rmxTextboxControl = null;


                                    }

                                }
                            }

                        }
                        else
                        {
                            if (rmxTextboxControl.ID == "codelookup")
                            {
                                if (formDataElement != null)
                                {
                                    string clientid = "";
                                    clientid = oCtrl.ClientID;

                                    sRMXRef = "//control[@name=" + "'" + clientid.Substring(0, 14) + "']";
                                    //control[@name='ResReasonTemWC']"
                                    rmxTextboxControl.Attributes.Remove("RMXRef");
                                    rmxTextboxControl.Attributes.Add("RMXRef", sRMXRef);
                                    sRMXRef = rmxTextboxControl.Attributes["RMXRef"];
                                    xmlConrolElement = formDataElement.XPathSelectElement(sRMXRef);
                                    if (xmlConrolElement != null)
                                    {
                                        rmxTextboxControl.Text = xmlConrolElement.Value;
                                        xmlConrolElement = null;
                                        rmxTextboxControl = null;

                                    }


                                }



                            }
                        }

                    }
                    else if (oCtrl.GetType().ToString() == "MultiCurrencyCustomControl.CurrencyTextbox")  //Aman 
                    {
                        CurrencyTextbox txtCtrl1 = (CurrencyTextbox)oCtrl;
                        sRMXRef = txtCtrl1.Attributes["rmxref"];
                        //sRMXRef = sRMXRef.Replace("txt_", "hdn_");
                        //Added in order to make the textbox as Right aligned
                        if (txtCtrl1.Attributes["rmxtype"] != null)
                        {
                            if (txtCtrl1.Attributes["TEXT-ALIGN"] == null)
                            {
                                txtCtrl1.Attributes.CssStyle.Add("TEXT-ALIGN", "right");
                            }

                        }
                        if (sRMXRef != string.Empty)
                        {
                            //fetch values from ref
                            if (formDataElement != null)
                            {
                                xmlConrolElement = formDataElement.XPathSelectElement(sRMXRef);
                                if (xmlConrolElement != null)
                                {
                                    //tempContrl = this.Form.FindControl("hdn_" + oCtrl.ID.Substring(4));
                                    // txtCtrl1.Text = xmlConrolElement.Value;
                                    //  ((CurrencyTextbox)oCtrl).SetProperties(xmlConrolElement.Value, xmlConrolElement.Value);
                                    if (xmlConrolElement.Attribute("CurrencyValue") != null)
                                    {
                                        txtCtrl1.AmountInString = xmlConrolElement.Attribute("CurrencyValue").Value;
                                    }
                                    else
                                    {
                                        // mcapps2 03/30/2013 MITS 32127 Start
                                        string sTempValue = xmlConrolElement.Value;
                                        int iPos = 0;
                                        iPos = sTempValue.IndexOf("$-,");
                                        if (iPos != -1)
                                        {
                                            xmlConrolElement.Value = sTempValue.Replace("$-,","$-");
                                        }
                                        iPos = sTempValue.IndexOf("$,");
                                        if (iPos != -1)
                                        {
                                            xmlConrolElement.Value = sTempValue.Replace("$,","$");
                                        }
                                        if (sTempValue == "&")
                                        {
                                            xmlConrolElement.Value = "$0.00";
                                        }
                                        // mcapps2 03/30/2013 MITS 32127 End
                                        txtCtrl1.AmountInString = xmlConrolElement.Value;
                                    }
                                    xmlConrolElement = null;
                                    //rmxTextboxControl = null;
                                }

                            }
                        }

                    }
                }
                else if (oCtrl.Controls.Count > 0)
                {
                    BindResponseDataToTextboxes(oCtrl.Controls, p_Status);

                    //Enabled and Disabled Codelookup control
                    if (oCtrl.GetType().ToString() == "ASP.ui_shared_controls_codelookup_ascx")
                    {
                        codeLookupControl = ((CodeLookUp)oCtrl);
                        SetCodeLookUpBehaviour(codeLookupControl, p_Status);
                    }
                }
            }
        }
       

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private XElement BindTextBoxesToRequestData()
        {
            Control hdnCtrlToClear = null;
            Control tempContrl = null;
            Control tempContrl1 = null;
            //added by shobhana
            Control tempcontrol3 = null;
            //
            ControlCollection oControls;
            CodeLookUp codeLookupControl = null;
            TextBox txtCtrl = null;
            TextBox hdnResereveTypeControl = null;
            XElement formDataElement = null;
            XElement xmlControlElement = null;
            string[] arrCtrlToClear;
            string sRMXRef = string.Empty;
            string strControlType = string.Empty;

            formDataElement = GetStoredFormData();
            hdnCtrlToClear = this.Form.FindControl("hdnCtrlToClear");

            if (hdnCtrlToClear != null)
            {
                arrCtrlToClear = ((TextBox)hdnCtrlToClear).Text.Split(',');
                for (int iCnt = 0; iCnt < arrCtrlToClear.GetUpperBound(0); iCnt++)
                {
                    tempContrl = this.Form.FindControl(arrCtrlToClear[iCnt]);
                    if (tempContrl != null)
                    {
                        strControlType = "";
                        strControlType = tempContrl.GetType().ToString();

                        if (strControlType == "ASP.ui_shared_controls_codelookup_ascx")
                        {
                            codeLookupControl = ((CodeLookUp)tempContrl);
                            oControls = codeLookupControl.Controls;

                            foreach (Control oCtrl in oControls)
                            {
                                if (oCtrl.GetType().ToString() == "System.Web.UI.WebControls.TextBox")
                                {
                                    txtCtrl = (TextBox)oCtrl;

                                    if (txtCtrl.Attributes["rmxref"] != null)
                                    {
                                        sRMXRef = txtCtrl.Attributes["rmxref"];
                                        if (formDataElement != null)
                                        {
                                            xmlControlElement = formDataElement.XPathSelectElement(sRMXRef);
                                            if (xmlControlElement != null)
                                            {
                                                xmlControlElement.Value = txtCtrl.Text;
                                                xmlControlElement.SetAttributeValue("reasoncode", codeLookupControl.CodeId);
                                                xmlControlElement = null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (strControlType == "MultiCurrencyCustomControl.CurrencyTextbox")
                        {
                            CurrencyTextbox txtCtrl1 = (CurrencyTextbox)tempContrl;
                            if (txtCtrl1.Attributes["rmxref"] != null)
                            {
                                sRMXRef = txtCtrl1.Attributes["rmxref"];
                                if (formDataElement != null)
                                {
                                    xmlControlElement = formDataElement.XPathSelectElement(sRMXRef);
                                    if (xmlControlElement != null && xmlControlElement.Attribute("CurrencyValue") != null)
                                    {
                                        
                                        xmlControlElement.Attribute("CurrencyValue").Value = txtCtrl1.AmountInString;
                                                    xmlControlElement.Value = txtCtrl1.Text;
                                                    xmlControlElement = null;
                                                 
                                    }
                                }
                            }
                        }
                        else if (strControlType == "System.Web.UI.WebControls.TextBox")
                        {
                            txtCtrl = (TextBox)tempContrl;
                            string id = "";
                            id = txtCtrl.ID.Substring(4);
                            if (txtCtrl != null)
                            {
                                if (txtCtrl.Attributes["rmxref"] != null)
                                {
                                    sRMXRef = txtCtrl.Attributes["rmxref"];
                                    if (formDataElement != null)
                                    {
                                        xmlControlElement = formDataElement.XPathSelectElement(sRMXRef);
                                        if (xmlControlElement != null)
                                        {
                                            // //filling input xml for Newoutstanding and NewInccured textboxes with their respective hidden fields
                                            if ((txtCtrl.ID.Contains("NEWTOTALINCURRED") && txtCtrl.ID.Contains("txt")) || (txtCtrl.ID.Contains("NEWOutstanding") && txtCtrl.ID.Contains("txt")) || (txtCtrl.ID.Contains("Comments") && txtCtrl.ID.Contains("txt")))
                                            {
                                                tempContrl1 = this.Form.FindControl("hdn_" + txtCtrl.ID.Substring(4));
                                                //// tempContrl1 = this.Form.FindControl("hdn_" + txtCtrl.ID.Substring(4));
                                                if (tempContrl1 != null)
                                                {
                                                    hdnResereveTypeControl = (TextBox)tempContrl1;
                                                    xmlControlElement.Value = hdnResereveTypeControl.Text;
                                                    xmlControlElement = null;
                                                    tempContrl1 = null;
                                                }
                                            }
                                            //added by shobhana
                                            //hdn_Amt1RatePerWC
                                            else if ((txtCtrl.ID.StartsWith("Comm")) || (txtCtrl.ID.StartsWith("Amt") && txtCtrl.ID.Contains("Rate")) || (txtCtrl.ID.StartsWith("txtAmt") && txtCtrl.ID.Contains("Rate")) || (txtCtrl.ID.StartsWith("NewIncAmt")) || (txtCtrl.ID.StartsWith("NewBalAmt")))
                                            {
                                                tempContrl1 = this.Form.FindControl("hdn_" + txtCtrl.ID);
                                                //// tempContrl1 = this.Form.FindControl("hdn_" + txtCtrl.ID.Substring(4));
                                                if (tempContrl1 != null)
                                                {
                                                    hdnResereveTypeControl = (TextBox)tempContrl1;
                                                    xmlControlElement.Value = hdnResereveTypeControl.Text;
                                                    xmlControlElement = null;
                                                    tempContrl1 = null;
                                                }
                                            }
                                            else if (txtCtrl.ID.StartsWith("txt_RESERVESUMMARY"))
                                            {
                                                tempContrl1 = this.Form.FindControl("hdn_" + txtCtrl.ID.Substring(4));
                                                //// tempContrl1 = this.Form.FindControl("hdn_" + txtCtrl.ID.Substring(4));
                                                if (tempContrl1 != null)
                                                {
                                                    hdnResereveTypeControl = (TextBox)tempContrl1;
                                                    xmlControlElement.Value = hdnResereveTypeControl.Text;
                                                    xmlControlElement = null;
                                                    tempContrl1 = null;
                                                }
                                            }

                                            else if ((txtCtrl.ID.StartsWith("AmtPerTWC")) || (txtCtrl.ID.StartsWith("Amt1PercentAfterApportion")) || (txtCtrl.ID.StartsWith("txtAmt10PerWC")) || (txtCtrl.ID.StartsWith("NewIncAmt")) || (txtCtrl.ID.StartsWith("txtAmt11PerWC")))
                                            {
                                                tempContrl1 = this.Form.FindControl("hdn_" + txtCtrl.ID);
                                                //// tempContrl1 = this.Form.FindControl("hdn_" + txtCtrl.ID.Substring(4));
                                                if (tempContrl1 != null)
                                                {
                                                    hdnResereveTypeControl = (TextBox)tempContrl1;
                                                    xmlControlElement.Value = hdnResereveTypeControl.Text;
                                                    xmlControlElement = null;
                                                    tempContrl1 = null;
                                                }


                                            }
                                            else if ((txtCtrl.ID.StartsWith("txtAmt")))
                                            {

                                                tempContrl1 = this.Form.FindControl("hdn_" + txtCtrl.ID.Substring(3));
                                                //////for (int j = 0; j < arrCtrlToClear.GetUpperBound(0); j++)
                                                //////{ tempcontrol3 = this.Form.FindControl(arrCtrlToClear[j]);
                                                if ((tempContrl1 != null))
                                                {
                                                    hdnResereveTypeControl = (TextBox)tempContrl1;
                                                    xmlControlElement.Value = hdnResereveTypeControl.Text;
                                                    xmlControlElement = null;
                                                    tempContrl1 = null;
                                                }


                                            }
                                            else
                                            {
                                                xmlControlElement.Value = txtCtrl.Text;
                                                xmlControlElement = null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return formDataElement;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private XElement GetStoredFormData()
        {
            XElement formDataElement = null;
            string sformData = string.Empty;
           
            sformData = ViewState["FormXmlData"].ToString();
            if (sformData != string.Empty)
            {
                formDataElement = XElement.Parse(sformData);
            }
            return formDataElement;
        }


        private void DeleteWorkSheet()
        {
            ReserveWorksheetRequest objRequest = null;
            ReserveWorksheetResponse objResponse = null;
            
            Control oControl = null;
            string sRWSId = string.Empty;

            try
            {
                objRequest = new ReserveWorksheetRequest();
                objRequest.RSWReservesXML = hdnCustomTransXML.Value;
                oControl = this.FindControl("hdnRSWid");

                if (oControl != null)
                {
                    sRWSId = ((TextBox)oControl).Text;
                    objRequest.RSWId = Convert.ToInt32(sRWSId);
                    oControl = null;
                }
                else
                {
                    throw new ApplicationException("Woksheet to be deleted is not found");
                }

                objResponse = HitReserveWorksheetWebservice(objRequest, "Delete");

                //Updating RSWStatus
                if (objResponse != null)
                {
                    if (objResponse.Status == true)
                    {
                        ViewState["divContainer_controls"] = "";
                        oControl = this.Form.FindControl("divContainer");
                        oControl.Controls.Clear();

                        //ReSetting values of hidden fields
                        hdnPenapprovalRWSId.Value = "0";
                        hdnPendingRWSId.Value = "0";
                        hdnRejectRWSId.Value = "0";
                    }
                    else
                    {
                        throw new ApplicationException("Error in Deleting Record");
                    }
                }
                else
                {
                    throw new NullReferenceException("Null Response Has Obtained");
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ApproveWorkSheet()
        {
            ReserveWorksheetRequest objRequest = null;
            ReserveWorksheetResponse objResponse = null;
            XElement formDataElement = null;
            
            objRequest = new ReserveWorksheetRequest(); 
            objRequest.RSWReservesXML = hdnCustomTransXML.Value;
            formDataElement = BindTextBoxesToRequestData();
            objRequest.InputXmlString = formDataElement.ToString();

            objResponse = HitReserveWorksheetWebservice(objRequest, "Approve");
            if (objResponse != null)
            {
                //MGaba2:Changed it as controls need to be made as readonly once the sheet is approved
                //Changed by Nitin
                //ViewState["FormXmlData"] = objResponse.OutputXmlString;
                LoadXslControls(objResponse.OutputXmlString);
                //binding latest form xml to the cotrols at form
                Control ctrl = this.Form.FindControl("divContainer");
                BindResponseDataToTextboxes(ctrl.Controls, "Approved");
            }
            else
            {
                throw new NullReferenceException("Null Response Has Obtained");
            }

            //MGaba2:r6:Functioning of Approve Button:End
        }

        private void MakeOnLoadSettings()
        {
            ReserveWorksheetRequest objRequest = null;
            ReserveWorksheetResponse objResponse = null;
            XmlDocument objXMLOutputString = null; //Aman Multi Currency
            XmlNode objXMLNode = null;
            string sClaimCurrency = string.Empty;
            try
            {
                objRequest = new ReserveWorksheetRequest();
                if (Request.QueryString["ClaimId"] != null)
                {
                    ViewState["ClaimId"] = Request.QueryString["ClaimId"].ToString();
                    SysClassName.Value = "claim";
                    SysFormPIdName.Value = ViewState["ClaimId"].ToString();
                }
                else
                {
                    ViewState["ClaimId"] = "0";
                }

                if (Request.QueryString["ClaimantId"] != null)
                {
                    ViewState["ClaimantId"] = Request.QueryString["ClaimantId"].ToString();

                    SysClassName.Value = "claimant";
                    SysFormPIdName.Value = ViewState["ClaimantId"].ToString();
                }
                else
                {
                    ViewState["ClaimantId"] = "0";
                }

                if (Request.QueryString["UnitId"] != null)
                {
                    ViewState["UnitId"] = Request.QueryString["UnitId"].ToString();

                    SysClassName.Value = "unit";
                    SysFormPIdName.Value = ViewState["UnitId"].ToString();
                }
                else
                {
                    ViewState["UnitId"] = "0";
                }
                if (Request.QueryString["RSWId"] != null)
                {
                    ViewState["RSWId"] = Request.QueryString["RSWId"].ToString();
                    //setting the value to hidden field to be used by MDIrswid.Value = ViewState["RSWId"].ToString();                            
                    rswid.Value = ViewState["RSWId"].ToString();     
                }

                SysFormName.Value = "reserveworksheet";
                SysFormIdName.Value = "rswid";
                objRequest.ClaimId = Convert.ToInt32(ViewState["ClaimId"]);
                objRequest.ClaimantEId = Convert.ToInt32(ViewState["ClaimantId"]);
                objRequest.UnitId = Convert.ToInt32(ViewState["UnitId"]);
                //shobhana
                if (ViewState["RSWId"] != null)
                {
                    objRequest.RSWId = Convert.ToInt32(ViewState["RSWId"]);
                }
                objResponse = HitReserveWorksheetWebservice(objRequest,"OnLoadInformation");
               
                if (objResponse != null)
                {
                    hdnApprovedRWSId.Value = objResponse.ExistApproved.ToString();
                    hdnPendingRWSId.Value = objResponse.ExistPending.ToString();
                    hdnPenapprovalRWSId.Value = objResponse.ExistPendingApproval.ToString();
                    hdnRejectRWSId.Value = objResponse.ExistRejected.ToString();
                   
                    RswTypeName.Value = objResponse.RSWType.ToString();

                    //Aman Multi Currency--Start
                    objXMLOutputString = new XmlDocument();
                    objXMLOutputString.LoadXml(objResponse.OutputXmlString);  
                    objXMLNode = objXMLOutputString.SelectSingleNode("//ClaimCurrency");
                    if (objXMLNode != null)
                    {
                        sClaimCurrency = objXMLNode.InnerText.ToString();
                    }
                    currencytype.Value = sClaimCurrency;
                    //Aman Multi Currency--End

                    if ( hdnPendingRWSId.Value == "0" && hdnPenapprovalRWSId.Value == "0")
                    {
                        btn_Show.Enabled = true;

                    }
                    else
                    {
                        btn_Show.Enabled = false;
                    
                    }

           
                    //Enable and Disable the tool bar buttons on the basis of Permission of utility setting
                    if (!objResponse.IsClaimAllowedToAccess)
                    {
                        btnSave.Enabled = false;
                        btnSubmit.Enabled = false;
                        btnApprove.Enabled = false;
                        btnReject.Enabled = false;
                        btnDelete.Enabled = false;
                        btnClear.Enabled = false;
                        return;
                    }
                    else
                    {
                        btnSave.Enabled = true;
                        btnSubmit.Enabled = true;
                        btnApprove.Enabled = true;
                        btnReject.Enabled = true;
                        btnDelete.Enabled = true;
                        btnClear.Enabled = true;
                    }
                    InitializeCulture();
                }
                else
                {
                    throw new NullReferenceException("Null Response Has Obtained");
                }
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
        }

        private void HandleRequestFromMDI()
        {
            int i_RSWId = 0;
            Control ctrl = null;
            TextBox hdnUnderLimitCtrl = null;
            TextBox hdnUnderClaimIncLimitCtrl = null;//sachin 6385
            TextBox hdnClaimIncLimitreason = null;//sachin 6385
            string hdnStatusValue = string.Empty;

            try
            {
                //if rswid is -ve then it means new to open 
                i_RSWId = Convert.ToInt32(ViewState["RSWId"]);

                if (i_RSWId < 0)
                {
                    //Create New ReserveWorksheet
                    CreateWorkSheet(false);
                }
                else
                {
                    //Open Already Exist ReserveWorsheet
                    CreateWorkSheet(true);
                }
                
                ctrl = this.Form.FindControl("divContainer");
                hdnStatusValue = GetStoredFormData().XPathSelectElement("//control[@name='hdnRSWStatus']").Value;

                BindResponseDataToTextboxes(ctrl.Controls,hdnStatusValue);

                hdnUnderClaimIncLimitCtrl = (TextBox)this.Form.FindControl("hdnUnderClaimIncLimit");//sachin 6385
                hdnUnderLimitCtrl = (TextBox)this.Form.FindControl("hdnUnderLimit");
                if (hdnUnderLimitCtrl.Text == "NoAuthEdit")
                {
                    throw new Exception("You are not authorized to edit this worksheet.");
                }

                //sachin 6385 starts
                if (hdnUnderClaimIncLimitCtrl.Text == "NoAuthEdit")
                {
                    throw new Exception("You are not authorized to edit this worksheet.");
                }
                //sachin 6385 ends
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl.errorDom = ErrorHelper.formatUIErrorXML(err);
            }
        }


        private void SetCodeLookUpBehaviour(CodeLookUp reasonCtrl,string p_Status)
        {
            TextBox hdnUnderLimit = null;
            TextBox hdnUnderClaimIncLimit = null;//sachin 6385
            if (p_Status == "Pending" || p_Status == "Rejected" || p_Status == "New")
            {
                if (p_Status == "Rejected")
                {
                    hdnUnderLimit = (TextBox)this.Form.FindControl("hdnUnderLimit");
                    if (hdnUnderLimit.Text == "NoAuthEdit")
                    {
                        reasonCtrl.Enabled = false;
                        hdnUnderLimit = null;
                        return;
                    }

                    //sachin 6385 starts
                    hdnUnderClaimIncLimit = (TextBox)this.Form.FindControl("hdnUnderClaimIncLimit");
                    if (hdnUnderClaimIncLimit.Text == "NoAuthEdit")
                    {
                        reasonCtrl.Enabled = false;
                        hdnUnderClaimIncLimit = null;
                        return;
                    }
                    //sachin 6385 ends
                }
                reasonCtrl.Enabled = true;
            }
            else if (p_Status == "Pending Approval" || p_Status == "Approved") 
            {
                reasonCtrl.Enabled = false;
            }
        }

        #endregion
        //Aman Mult Currency --Start
        protected override void InitializeCulture()
        {
            if (Request.Form["currencytype"] != null)
            {
                if (!string.IsNullOrEmpty(Request.Form["currencytype"]))
                {
                    Culture = Request.Form["currencytype"].Split('|')[1];
                    UICulture = Request.Form["currencytype"].Split('|')[1];
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.Form["currencytype"].Split('|')[1]);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(Request.Form["currencytype"].Split('|')[1]);
                }
            }
            else if ((currencytype != null) && (!string.IsNullOrEmpty(currencytype.Value)))
            {
                Culture = currencytype.Value.ToString().Split('|')[1];
                UICulture = currencytype.Value.ToString().Split('|')[1];
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(currencytype.Value.ToString().Split('|')[1]);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(currencytype.Value.ToString().Split('|')[1]);
            }
            base.InitializeCulture();    
        }
        //Aman Mult Currency --End
    }
}
