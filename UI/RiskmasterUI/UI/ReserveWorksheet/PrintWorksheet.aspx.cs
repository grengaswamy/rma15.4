﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
//using Riskmaster.UI.ReserveWorksheetService;//Change by kuladeep for Cloud-154
using Riskmaster.Models;

namespace Riskmaster.UI.UI.ReserveWorksheet
{
    public partial class PrintWorksheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ReserveWorksheetRequest oReserveWorkSheetRequest = null;
            ReserveWorksheetResponse objReserveWorkSheetResponse = null;
            //ReserveWorksheetServiceClient objClient = null; //Change by kuladeep for Cloud-154
            XmlDocument outputXmlDoc = null;
            XElement rootElement = null;
            XmlDocument formDoc = null;
            string sRswtypename = string.Empty;
            string sInXml = string.Empty;
            string sRSWCustomXMl = string.Empty;
            XElement oEle = null;
            string sFileContent = string.Empty;
            string sFileName = string.Empty;

            try
            {
                if (!Page.IsPostBack)
                {

                    oReserveWorkSheetRequest = new ReserveWorksheetRequest();
                    //objClient = new ReserveWorksheetServiceClient();//Change by kuladeep for Cloud-154

                     sInXml = "<form><control name='RSWId'>" + AppHelper.GetQueryStringValue("rswid") + "</control></form>";
                     sRswtypename = AppHelper.GetQueryStringValue("RswTypeName");
                     sRSWCustomXMl = AppHelper.GetQueryStringValue("RSWCustomXML");
                     //MGaba2: MITS 22225:Print functionality was not working from Reserve Approval Screen
                     //Value of ClaimId needs to be set from Reserve Approval Screen
                    if(AppHelper.GetQueryStringValue("ClaimId") !=  string.Empty)
                    {
                        oReserveWorkSheetRequest.ClaimId = Convert.ToInt32(AppHelper.GetQueryStringValue("ClaimId"));
                    }
                    oReserveWorkSheetRequest.RSWType = sRswtypename;
                    oReserveWorkSheetRequest.RSWReservesXML = sRSWCustomXMl;

                    oReserveWorkSheetRequest.InputXmlString = sInXml;
                    oReserveWorkSheetRequest.Token = AppHelper.ReadCookieValue("SessionId");

                    //Change by kuladeep for Cloud-154-Start
                    oReserveWorkSheetRequest.ClientId = AppHelper.ClientId;
                    //objReserveWorkSheetResponse = objClient.PrintReserveWorkSheet(objReserveWorkSheetRequest);
                    objReserveWorkSheetResponse = AppHelper.GetResponse<ReserveWorksheetResponse>("RMService/RSW/PrintReserveWorkSheet", AppHelper.HttpVerb.POST, "application/json", oReserveWorkSheetRequest);
                    //Change by kuladeep for Cloud-154-End

                    if (objReserveWorkSheetResponse.OutputXmlString  != string.Empty)
                    {
                        outputXmlDoc = new XmlDocument();
                        outputXmlDoc.LoadXml(objReserveWorkSheetResponse.OutputXmlString);

                        rootElement = XElement.Parse(outputXmlDoc.OuterXml);

                         oEle = rootElement.XPathSelectElement(".//File");
                        if (oEle != null)
                        {
                             sFileContent = oEle.Value;
                             sFileName = oEle.Attribute("Filename").Value;
                            byte[] byteOrg = Convert.FromBase64String(sFileContent);
                            Response.Clear();
                            Response.Charset = "";
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-Disposition", "inline;");
                            Response.BinaryWrite(byteOrg);                           
                            Response.Flush();
                            //Response.Close();
                        }
                    }

                }
            }
            catch (Exception ee)
            {
                ErrorHelper.logErrors(ee);
                BusinessAdaptorErrors err = new BusinessAdaptorErrors();
                err.Add(ee, BusinessAdaptorErrorType.SystemError);
                ErrorControl1.errorDom = ErrorHelper.formatUIErrorXML(err);

            }
        }

        
    }
}
