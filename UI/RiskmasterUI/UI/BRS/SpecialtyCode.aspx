﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpecialtyCode.aspx.cs" Inherits="Riskmaster.UI.UI.BRS.SpecialtyCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>BRS Specialty Code</title>
    <script language="JavaScript" type="text/javascript" src="../../Scripts/brs.js"></script>
</head>
<body onload="SpecCodePageLoaded();">
    <form id="frmData" runat="server">
    <div>
    <table>
		<tr>
			<td class="ctrlgroup">The procedure code entered spans more than one specialty.  Please refer to the State notes supplied by ingenix/Medicode for a complete description of the codes.  Select a code from the following list:</td>
		</tr>
		<tr>				
            <td>
                <asp:DropDownList runat="server" ID="CmboSpecCds"></asp:DropDownList>
            </td>
		</tr>
		<tr>
		    <td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center">
			    <asp:Button runat="server" class="button" ID="btnOK" OnClientClick="return ReturnSpecCode();" Text="     OK     " />
			</td>
		</tr>
	</table>
    </div>
    </form>
</body>
</html>
