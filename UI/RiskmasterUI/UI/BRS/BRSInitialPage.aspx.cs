﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Riskmaster.UI.BRS
{
    public partial class BRSInitialPage : System.Web.UI.Page
    {
        //private const string m_GridModeClone = "clone";
        private bool m_bFirstBRSItem = false;

        private string m_ref = "/Instance/UI/FormVariables/SysExData/FundsSplits/option";
        protected void Page_Load(object sender, EventArgs e)
        {
            string sSplitRowId = string.Empty;

            if (!IsPostBack)
            {
                gridname.Text = AppHelper.GetQueryStringValue("gridname");
                selectedrowposition.Text = AppHelper.GetQueryStringValue("selectedrowposition");
                gridmode.Text = AppHelper.GetQueryStringValue("mode");
            }
            else
            {
                //BRS split XML template
                XElement oBRSMessage = XElement.Parse(@"<Message>
	                    <Authorization></Authorization>
	                    <Call>
		                    <Function>BRSAdaptor.FetchBRSSplitXml</Function>
	                    </Call>
	                    <Document></Document></Message>
                    ");

                string sSplitListData = txtData.Text;
                
                //Try to retrieve BRS split data from hidden field for an existing split
                XElement oBRSSplit = null;
                if (!string.IsNullOrEmpty(sSplitListData))
                {
                    //sSplitListData = sSplitListData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                    oBRSSplit = XElement.Parse(sSplitListData);
                    oBRSSplit = BRSCommon.SetSplitItemIndex(oBRSSplit);
                }

                //Put the values from the funds page to the blank option element for the 1st time
                XElement oEmptyOption = oBRSSplit.XPathSelectElement("/option[idx='-1']");
                int iSplitCount = oBRSSplit.Elements("option").Count();
                if( iSplitCount <= 1 )
                {
                    m_bFirstBRSItem = true;
                }

                //Set zip code
                XElement oZip = oEmptyOption.XPathSelectElement("./zip");
                oZip.Value = zip.Text;

                //    Mona: R8.2 : BRS in Carrier claim
                XElement oPolicyId = oEmptyOption.XPathSelectElement("./PolicyID");
                oPolicyId.Value = PolicyID.Text;
                XElement oPolCvgRowId = oEmptyOption.XPathSelectElement("./CoverageId");
                oPolCvgRowId.Value = PolCvgID.Text;
                XElement oRCRowId = oEmptyOption.XPathSelectElement("./RCRowId");
                oRCRowId.Value = RcRowID.Text;
                XElement oclm_entityid = oEmptyOption.XPathSelectElement("./clm_entityid");
                oclm_entityid.Value = clm_entityid.Text;
                XElement oisfirstfinalquerystring = oEmptyOption.XPathSelectElement("./IsFirstFinalQueryString");
                oisfirstfinalquerystring.Value = IsFirstFinalQueryString.Text;
                XElement oCvgLossID = oEmptyOption.XPathSelectElement("./CvgLossID");
                oCvgLossID.Value = CvgLossID.Text;
                //    Mona: R8.2 : BRS in Carrier claim
                XElement oClaimID = oEmptyOption.XPathSelectElement("./claimid");
                oClaimID.Value = claimid.Text;

                XElement oTransID = oEmptyOption.XPathSelectElement("./transid");
                oTransID.Value = transid.Text;

                //for existing item, the provider eid is not set.
                foreach (XElement oProviderEid in oBRSSplit.XPathSelectElements("/option/providereid"))
                {
                    oProviderEid.Value = providereid.Text;
                }

                //set brsmode value to each item
                foreach (XElement oBRSMode in oBRSSplit.XPathSelectElements("/option/mode"))
                {
                    oBRSMode.Value = brsmode.Text;
                }

                //set resubmit value to each item
                foreach (XElement oResubmitMode in oBRSSplit.XPathSelectElements("/option/resubmitmode"))
                {
                    oResubmitMode.Value = resubmitflag.Text;
                }

                XElement oLob = oEmptyOption.XPathSelectElement("./lineofbusinesscode");
                oLob.Value = lineofbusinesscode.Text;
                foreach (XElement olineofbusinesscode in oBRSSplit.XPathSelectElements("/option/lineofbusinesscode"))
                {
                    olineofbusinesscode.Value = lineofbusinesscode.Text;
                }

                XElement oResubmitFlag = oEmptyOption.XPathSelectElement("./resubmitmode");
                oResubmitFlag.Value = resubmitflag.Text;

                //Put values for mode, gridname to the xml
                string sMode = gridmode.Text;
                string sLastRowPosition = lastrowposition.Text;
                string sSelectedRowPosition = selectedrowposition.Text;
                oBRSSplit.SetAttributeValue("mode", sMode);
                oBRSSplit.SetAttributeValue("brsmode", brsmode.Text);
                oBRSSplit.SetAttributeValue("gridname", gridname.Text);
                oBRSSplit.SetAttributeValue("lastrowposition", sLastRowPosition);
                oBRSSplit.SetAttributeValue("selectedrowposition", sSelectedRowPosition);
                oBRSSplit.SetAttributeValue("resubmitmode", resubmitflag.Text);
                //    Mona: R8.2 : BRS in Carrier claim
                oBRSSplit.SetAttributeValue("PaymentOrCollection", PaymentOrCollection.Text);
                //Add BRS split to the document message
                XElement oDocElement = oBRSMessage.XPathSelectElement("./Document");
                oDocElement.Add(XElement.Parse(oBRSSplit.ToString()));

                if (sMode == "add" || sMode == "clone")
                {
                    BRSCommon.AddNewBRSItem(sMode, oBRSMessage, true);
                }

                //Put BRSSplit data to context cache
                Context.Items["BRSSplitData"] = oBRSMessage.ToString();
                Server.Transfer("BRSMain.aspx");
            }
        }
    }
}
