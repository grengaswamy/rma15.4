﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiaryActivity.aspx.cs" Inherits="Riskmaster.UI.Diaries.DiaryActivity" %>
<%@ Register TagPrefix="uc1" TagName="ErrorControl"  Src="~/UI/Shared/Controls/ErrorControl.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Create Diary</title>
    <link href="../../Content/system.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/rmnet.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/wpamessages.js" type="text/javascript"></script>
    <script src="../../Scripts/rmx-common-ref.js" type="text/javascript"></script>
    <script type="text/javascript">
	    function pageLoaded()
	    {
		    var i;

		    for (i=0;i<document.forms[0].length;i++)			
		    {	 				
			    if((document.forms[0].item(i).type=="text") || (document.forms[0].item(i).type=="select-one")|| (document.forms[0].item(i).type=="textarea"))
			    {
				    document.forms[0].item(i).focus();
				    break;
			    }
		    }
	    }

        function OnOK()
        {
	        if(replace(document.forms[0].txtActivity.value," ","")=="" && document.forms[0].lstActivity.selectedIndex<=0)
                  {
                      //alert("Please enter or select the Activity.");
                      alert(DiaryActivityValidations.ValidActivity); //Rakhel ML Changes
                  return false;
                  }
                  var s="", lId="0";
                  if(replace(document.forms[0].txtActivity.value," ","")!="")
                  {
                  //Parijat : Mits 9390 .An extention to the issue .
                  var iIndex=document.forms[0].lstActivity.selectedIndex;
                  lId = document.forms[0].lstActivity.options[iIndex].value;
                  s=document.forms[0].txtActivity.value;
                  }
                  else
                  {
                  var iIndex=document.forms[0].lstActivity.selectedIndex;
                  s=document.forms[0].lstActivity.options[iIndex].text;
                  lId=document.forms[0].lstActivity.options[iIndex].value;
                  }
                  if(window.opener!=null && window.opener!=self)
		        window.opener.document.OnAddActivity(lId,s);
                  window.opener.m_DataChanged = true; //issue fixes RMA-19339
        }

        function OnCancel()
        {
	        self.close();
	        return true;
        }

        function handleUnload()
        {
	        if(window.opener!=null && window.opener!=self)
		        window.opener.document.onCodeClose();
        }

        function replace(sSource, sSearchFor, sReplaceWith)
        {
	        var arr = new Array();
	        arr=sSource.split(sSearchFor);
	        return arr.join(sReplaceWith);
        }
        
	</script>
</head>
<body>
    <form id="frmData" runat="server">
        <uc1:ErrorControl ID="ErrorControl" runat="server" />
    <p>
      <asp:Label ID="lblValidateMsgResrc" runat="server" Text="<%$ Resources:lblValidateMsgResrc %>"></asp:Label> 
      <br/>
        
        <asp:DropDownList  ID="lstActivity" runat="server" Width="274px"></asp:DropDownList>
        <br/>
        <input type="text"  runat="server" id="txtActivity" name="txtActivity" size="50" maxlength="50"/>
    </p>
    <p align="center">
    <!--Aman MITS 31225 removed the width attribute -->
        <asp:Button ID="btnOk" runat="server" CssClass="button"
         Text="<%$ Resources:btnOk %>"  OnClientClick="OnOK()" />
        <asp:Button ID="btnCancel" runat="server"   Text="<%$ Resources:btnCancel %>"   CssClass="button"
            OnClientClick="OnCancel();" />
    </p>
    </form>
</body>
</html>
